Geometry file
Geometry file
node id assign
element id assign
coordinates
      44
 0.00000e+00-1.00000e+00-1.00000e+00
 2.00000e+00-1.00000e+00-1.00000e+00
 4.00000e+00-1.00000e+00-1.00000e+00
 6.00000e+00-1.00000e+00-1.00000e+00
 8.00000e+00-1.00000e+00-1.00000e+00
 1.00000e+01-1.00000e+00-1.00000e+00
 1.20000e+01-1.00000e+00-1.00000e+00
 1.40000e+01-1.00000e+00-1.00000e+00
 1.60000e+01-1.00000e+00-1.00000e+00
 1.80000e+01-1.00000e+00-1.00000e+00
 2.00000e+01-1.00000e+00-1.00000e+00
 0.00000e+00 1.00000e+00-1.00000e+00
 2.00000e+00 1.00000e+00-1.00000e+00
 4.00000e+00 1.00000e+00-1.00000e+00
 6.00000e+00 1.00000e+00-1.00000e+00
 8.00000e+00 1.00000e+00-1.00000e+00
 1.00000e+01 1.00000e+00-1.00000e+00
 1.20000e+01 1.00000e+00-1.00000e+00
 1.40000e+01 1.00000e+00-1.00000e+00
 1.60000e+01 1.00000e+00-1.00000e+00
 1.80000e+01 1.00000e+00-1.00000e+00
 2.00000e+01 1.00000e+00-1.00000e+00
 0.00000e+00-1.00000e+00 1.00000e+00
 2.00000e+00-1.00000e+00 1.00000e+00
 4.00000e+00-1.00000e+00 1.00000e+00
 6.00000e+00-1.00000e+00 1.00000e+00
 8.00000e+00-1.00000e+00 1.00000e+00
 1.00000e+01-1.00000e+00 1.00000e+00
 1.20000e+01-1.00000e+00 1.00000e+00
 1.40000e+01-1.00000e+00 1.00000e+00
 1.60000e+01-1.00000e+00 1.00000e+00
 1.80000e+01-1.00000e+00 1.00000e+00
 2.00000e+01-1.00000e+00 1.00000e+00
 0.00000e+00 1.00000e+00 1.00000e+00
 2.00000e+00 1.00000e+00 1.00000e+00
 4.00000e+00 1.00000e+00 1.00000e+00
 6.00000e+00 1.00000e+00 1.00000e+00
 8.00000e+00 1.00000e+00 1.00000e+00
 1.00000e+01 1.00000e+00 1.00000e+00
 1.20000e+01 1.00000e+00 1.00000e+00
 1.40000e+01 1.00000e+00 1.00000e+00
 1.60000e+01 1.00000e+00 1.00000e+00
 1.80000e+01 1.00000e+00 1.00000e+00
 2.00000e+01 1.00000e+00 1.00000e+00
part       1
MeshLabel_0
tria3
      80
       2      13       1
       1      13      12
       3      14       2
       2      14      13
       4      15       3
       3      15      14
       5      16       4
       4      16      15
      17       5       6
      17      16       5
      18       6       7
      17       6      18
      19       7       8
      18       7      19
      20       8       9
      19       8      20
      21       9      10
      20       9      21
      22      10      11
      21      10      22
      24      23      35
      35      23      34
      25      24      36
      36      24      35
      26      25      37
      37      25      36
      27      26      38
      38      26      37
      28      27      39
      39      27      38
      28      40      29
      39      40      28
      29      41      30
      40      41      29
      30      42      31
      41      42      30
      31      43      32
      42      43      31
      32      44      33
      43      44      32
       1      24       2
       1      23      24
       2      25       3
       2      24      25
       3      26       4
       3      25      26
       4      27       5
       4      26      27
       6       5      28
       5      27      28
       7       6      29
      29       6      28
       8       7      30
      30       7      29
       9       8      31
      31       8      30
      10       9      32
      32       9      31
      11      10      33
      33      10      32
      12      13      35
      34      12      35
      13      14      36
      35      13      36
      14      15      37
      36      14      37
      15      16      38
      37      15      38
      39      16      17
      38      16      39
      18      40      17
      17      40      39
      19      41      18
      18      41      40
      20      42      19
      19      42      41
      21      43      20
      20      43      42
      22      44      21
      21      44      43
part       2
MeshLabel_1
tetra4
      60
       3      37       4      15
      17      39       6      40
      33      10      32      44
       2      35       1      24
       7       6      29      40
      21       9      10      43
       2      36       3      14
      21      10      22      44
      17       6      18      40
      10       9      32      43
      22      10      11      44
      20       9      21      43
      20       8       9      42
      11      10      33      44
       3      14      37      15
      31       8      30      42
       8      41      30      42
      32       9      31      43
      18       6       7      40
      21      43      10      44
      36      35       2      24
      10      43      32      44
       4      37      38      15
       9       8      31      42
      19       8      20      42
      19      41       8      42
       4      15      38      16
       5      38      39      16
      19       7       8      41
       8       7      30      41
      18       7      19      41
      18      40       7      41
      30       7      29      41
       7      40      29      41
      28      39       6       5
      20      42       9      43
       9      42      31      43
       6      39      17       5
       2      35      36      13
      29       6      28      40
       6      39      28      40
       2      13      36      14
      17       5      39      16
       3      26      37      25
       3      36      37      14
       1      12      35      13
       1      34      35      12
       1      35       2      13
       4      38       5      16
       2      25      36      24
      37      36       3      25
       3      36       2      25
      35      34       1      23
       1      24      35      23
       5      38       4      27
      39       5      28      27
      39      38       5      27
       4      37       3      26
      38      37       4      26
       4      27      38      26
tria3
       2
       1      12      34
      34      23       1
part       3
MeshLabel_2
tria3
       2
      11      44      22
      33      44      11
