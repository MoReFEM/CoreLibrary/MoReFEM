[TOC]

# Prerequisites

To run Doxygen build, you need to install at least [Doxygen](https://www.doxygen.nl/index.html) and [GraphViz](https://graphviz.org). 

The continuous integration build relies on the default packages installed by a recent Ubuntu distribution:

````
apt install doxygen graphviz libjs-mathjax
````

# Commands 

## Generate the documentation

To run a Doxygen, you just need to use the `doxygen` command and add the specific _dox_ file you want (more on this below).

Building the most complete version is done through the command:

````
doxygen complete_dev_guide.dox
````

Doxygen is far to be as elaborate as a compiler and will spend time at each call; however the very first call may long several minutes as many graphes are generated throui GraphViz.

On subsequent calls, only the modified ones are regenerated and the build is much quicker, even if Doxygen parses again all the files contrary to what a decent compiler would do for code.

The outputs are written in the directory pointed at in `OUTPUT_DIRECTORY` field of the dox file. Only html output is requested by default: you don't want a pdf with more than 10000 pages which LaTeX source may break for slight default in the Doxygen instructions in your source files.

For instance to see the generated page of the precedent command is reachable through:

````
open Complete/html/index.html
````

which should fireup your preferred browser.

## Analyse the log

Few years ago, an upgrade to Doxygen version resulted to a drastic increase of warnings detected (from 0 to more than 5000...)

Some were completely right and should have been detected by earlier version, but others are really nitpick that would have take much time to solve without improving the quality of the documentation (for instance for one simple methods such as accessor new version requires to give two different fields for return value and brief summary, which would in practice duplicate insightful sentences such as 'Get the value of X'...)

I therefore chose to solve the meaningful warnings, and to filter out the others. A dedicated Python script has been written to that purpose and is used in CI to check no new warning arose. To run them locally in command line, you need to run the following command:

````
python ../../Scripts/Tools/find_warning_in_doxygen_log.py --log complete.log 
````

(assuming you run it from _Documentation/Doxygen_ directory - adapt the paths if not).

If none found, the script will just return an empty list `[]`. 
If there are some, the list will be displayed and a file named `filtered_doxygen_log.txt` will be generated with the remaining warnings (terminal will give you this information).


# Release version

At each MoReFEM release, the complete Doxygen documentation is generated through Gitlab pages and accessible through [this link](https://morefem.gitlabpages.inria.fr/CoreLibrary/MoReFEM/) (also displayed in the banner on the [main page](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) of the project.)