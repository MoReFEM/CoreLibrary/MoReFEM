\chapter{Object factory}\label{ChapObjectFactory}

\section{Introduction}

In HappyHeart geometry, meshes are typically read from an input file written, and we also want to be able to perform the reverse operation as well: write the mesh that has been built into a file.
\medskip

Many formats do exist to store such mesh information; so far, Happy Heart (and also FELiScE) handles two of them: Medit and Ensight (see \refsection{SecMeshFormat}).
\medskip

At very first sight, it might seem somewhat easy, even trivial but rather quickly the code may slip into a jungle of conditions, redundancies while attempting to be able to handle all the cases in the same place. This is for instance the case of Medit support in FELiScE: Lm5 API is somewhat clumsy to generalize due to its use of variadic functions\footnote{A variadic function is a function with an unspecified number of arguments, such as C \code{printf}. It is generally better to avoid using them; see \textbf{Add reference} for a detailed discussion about it.}, and the file \code{medit.cpp} is a cascade of almost but not quite identical blocks for each of the element type handled.
\medskip

Fortunately, the issue is a well-known one, and there are elegant solutions available (see for instance \citep{ParashiftSerial} or chapter 8 of \citep{Alexandrescu2001}). One of those has been implemented in HappyHeart for \code{GeometricElementFactory}.
\medskip

As we shall see briefly in this chapter, a factory enables a total decoupling between the code that handle serialization/deserialization and the kind of elements that are being read or written. So for instance for the geometric element factory presented below, adding a new type of geometric element doesn't meddle in any way with the code in charge of processing the mesh file.
\medskip

The benefit provided by a factory is that the code is very safe, not redundant and very easy to maintain.

\section{Object factory in Happy Heart}

There is currently one object factory in HappyHeart, named \code{GeometricElementFactory}, which specific roles are defined in \refsection{SecGeometricElementFactory}. 
\medskip

This object is not intended to be used widely in the code: creation of geometric elements is typically a task that happens once at mesh creation, and query about which objects are present in the mesh are encapsulated within another class.
\medskip

So a typical user of HappyHeart shouldn't have to worry much about it, except in three cases:
\begin{itemize}
	\item Introducing a new file format (see \refsection{SecAddNewFormat}).
	\item Introducing a new type of geometric element (see \refsection{SecAddNewGeoElement}). However, the interaction with the factory is here limited to registration, as presented in \refsection{SecRegistration}.
	\item It is used in the internals of \code{GeometricElementList} where all geometric elements are stored to a quick access. So a developer modifying this class might have to tackle with it.
\end{itemize}

In a nutshell, if you aren't currently concerned by any if this cases, you can safely dismiss this chapter and be sure not to miss an important notion of HappyHeart's usage. 
\medskip

If you have to deal with it, no need to worry: albeit the actual implementation is quite subtle, the interface is simple and most can be made by looking at the existing code and adapting it to your needs.


\section{Tasks assigned to GeometricElementFactory}\label{SecGeometricElementFactory}

The \code{GeometricElementFactory} is basically a singleton\footnote{A singleton is an object that can only have one single instance in the given program. In our case, it is created before the \code{main} function begins and deleted at the very end of the program. See chapter 6 of \citep{Alexandrescu2001} for a complete discussion about it.} class that fulfills two purposes:
\begin{itemize}
	\item Register all the type of geometric elements supported by HappyHeart.
	\item Enable the creation of any element of one of these types.
\end{itemize}

As this class is quite low-level, it had been put in a \code{Private} namespace.

\subsection{Registration}\label{SecRegistration}

The beauty of the factory is that it's up to each object to add itself to the factory(see \refsection{SecAddNewGeoElement}), rather than to modify the factory to add it. For instance below is the code to register a \code{Triangle3} object:

\begin{lstlisting}[label=CodeFactoryAnonymous,caption=Self registration of Triangle3.]
// Triangle3.cpp
namespace // anonymous
{
    
    GeometricElement::Ptr CreateEnsight(std::istream& in)
    {
        GeometricElement::Ptr pointer(new Triangle3(in));
        return pointer;
    }
    
    
    GeometricElement::Ptr CreateDefault()
    {
        GeometricElement::Ptr pointer(new Triangle3);
        return pointer;
    }
    
    const bool registered = Private::GeometricElementFactory::Instance().RegisterGeometricElement<Triangle3>(CreateDefault, CreateEnsight);
   
} // namespace anonymous
\end{lstlisting}

So what is done in that less than straightforward code? Let's decompose the code to understand each part:

\begin{itemize}
	\item \code{Private::GeometricElementFactory::Instance()} is just an invocation of the singleton.
	\item \code{const bool registered} is a trick to make the statement run by the program before the beginning of the code. The registration is to occur before the function \code{main} is even called\footnote{If not, we couldn't be sure the object is properly registered when another compilation unit will request to build it: we can't make any assumption upon the order the compilation unit are compiled! (see also \refsection{SecStaticInitialization})}. Without the \code{const bool} part, we would obtain a compilation error of the like \textbf{Expected function body after function declarator}.
	\item The namespace anonymous is explained in \refsection{SecAnonymousNamespace}.
	\item Free functions \code{CreateDefault} and \code{CreateEnsight} that create a \code{Triangle3} object calling respectively the default constructor and a constructor from a stream, intended to stem from an Ensight file. Should Ensight not be supported by the geometric element considered, \code{std::nullptr\_t} can be provided instead.
\end{itemize}

Internally, the factory stores in several \code{std::map} or \code{std::unordered\_map} these creation functions with some identifiers as key. 
\medskip

Currently up to 3 identifiers can be used for a given geometric element, each of them being given to the class by a static method:

\begin{itemize}
	\item String identifier used by Ensight. It should be provided by \code{Triangle3::EnsightName()}.
	\item Medit enum identifier, that should be provided by \code{Triangle3::MeditId()}.
	\item A HappyHeart specific identifier, provided by \code{Triangle3::Identifier()}. Its need stem from the fact that we can't assume a format will support absolutely all the types of geometric elements. This identifier will be discussed in \refsection{SecFactoryEnum}.
\end{itemize}

Such an identifier can't be shared by any other \code{GeometricElement} object, as they are what will be used to decide which type is really the new object created. 
\medskip

It should be noticed that error handling when two objects share the same identifier is a bit special: the check occurs at registration, which happens before the main program actually starts. So an exception wouldn't do (it can't be caught for instance). Therefore the code will crash in this case after having printed the reason of its demise on the screen.

\subsection{Creation of a geometric element}\label{SecObjectCreation}

Most has already been said in the registration section: now objects of a given type can be created using one of the identifiers. Currently two methods are provided and used in HappyHeart:

\begin{lstlisting}
std::istream fileIn; // file being read
... 
GeometricElement::Ptr geometric_element = Private::GeometricElementFactory::Instance().CreateFromEnsightName("Triangle3", fileIn);
\end{lstlisting}

and 

\begin{lstlisting}
GeometricElement::Ptr geometric_element = Private::GeometricElementFactory::Instance().CreateFromIdentifier(Tria3);
\end{lstlisting}

After one of this call, \code{geometric\_element} is a pointer to a \code{Triangle3} object.

\section{Safety considerations}

\code{GeometricElementFactory} design makes it very safe to use: the only mishaps that may occur would stem from an ill-formed constructor of the \code{GeometricElement} object to build. Other possible issues are already covered by the following safeguards:

\begin{itemize}
	\item Uniqueness of identifier is performed before the \code{main} is reached, as specified in \refsection{SecRegistration}.
	\item If no function is provided whereas it should, same type of very early runtime error will occur.
	\item If some required method is forgotten, compilation error will occur. On the contrary, if for instance a geometric element doesn't support Ensight \code{EnsightName()} is not required.	
\end{itemize}


\section{Breaking a part of the factory magic for efficiency}\label{SecFactoryEnum}

\textbf{\textit{Note: this section is about design and efficiency consideration and can easily be ignored.}}

A part of the strength of the factory is that the code is really highly decentralized: you can create a new geometric element type with recompiling nothing or next to nothing.
\medskip

However, as already specified there is the need to provide a new unique identifier for each new type to add to the factory. The only choice here that would not break in any respect the extreme decoupling of the code would be to use a \code{std::string} as identifier: a simple convention as choosing the class name as identifier prevent any possible duplicate.
\medskip

The problem with this choice is that string comparisons aren't efficient at all, and with huge meshes we would like to avoid such a burden. 
\medskip

An integer value is not really an option either: it is both unclear\footnote{\code{CreateFromIdentifier(5)} isn't as compelling as \code{CreateFromIdentifier("Triangle3")}} and tedious\footnote{One has to check existing indexes before adding a new one}.
\medskip

The answer is an enumeration, which provides both computation efficiency and clarity. However, it comes at a slight cost: all geometric element classes must include the header in which this \code{enum} is defined, and therefore when adding a new type all has to be recompiled. Still, in HappyHeart compilation times are rather fast and adding new type is not an everyday operation, so this cost is extremely slight.



