%
%  untitled
%
%  Created by Sebastien Gilles on 2013-05-16.
%  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
%
\documentclass[]{article}

% Use utf-8 encoding for foreign characters
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

% Setup for fullpage use
\usepackage{fullpage}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% More symbols
%\usepackage{amsmath}
%\usepackage{amssymb}
%\usepackage{latexsym}

% Surround parts of graphics with box
\usepackage{boxedminipage}

% Package for including code in the document
\usepackage{listings}
\usepackage{verbatim}
\usepackage{color}
\usepackage{indentfirst}

% Package for biblio
\usepackage[authoryear,round,comma]{natbib}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=C++,                    % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}





% If you want to generate a toc for each chapter (use with book)
\usepackage{minitoc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

%\newif\ifpdf
%\ifx\pdfoutput\undefined
%\pdffalse % we are not running PDFLaTeX
%\else
%\pdfoutput=1 % we are running PDFLaTeX
%\pdftrue
%\fi

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi



\title{Current state of geometric elements, reference elements and finite elements in HappyHeart}
\author{Sébastien Gilles}

%\date{2013-05-23}

\begin{document}
	
\newcommand{\refchapter}[1]{chapter \ref{#1}}
\newcommand{\refsection}[1]{section \ref{#1}}
\newcommand{\refcode}[1]{code excerpt \ref{#1}}
\newcommand{\path}[1]{\textit{#1}}
\newcommand{\code}[1]{\textit{#1}}
\renewcommand{\lstlistingname}{Code excerpt}

\newcommand{\subsubsubsection}[1]
{
\bigskip
\textbf{#1}
\bigskip
}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle

\section*{Introduction}

Current Happy Heart structure does not reflect a healthy modeling of finite elements: for instance geometric elements hold a too important place in the algorithm.
\medskip

The current structure is mostly inherited from the original Felisce code, but not entirely: some of it has been introduced by myself to avoid some redundancies in Felisce code (for instance basis functions were provided for both geometric and reference element).




\section{GeometricElement} \label{section_geometric_element}

\subsection{Introduction}

Currently the heart of the elements modeling: each other element class\footnote{except \lstinline{CurrentFiniteElement} - see \refsection{section_finite_element}} is based on \lstinline{GeometricElement} through composition.

We will describe each policy of its actual implementation; the main job afterwards will be to decide which one is relevant and which one should be addressed by another class.

\subsection{A sketch of geometric elements implementation}

GeometricElement objects are defined on three layers:
\begin{itemize}
	\item A \code{GeometricElement} class, which is dedicated to be used polymorphically\footnote{In \lstinline{GeometricMeshRegion} every geometric element read in the mesh is stored within the same \lstinline{std::vector}.}. This class is abstract: no object of this type can be created directly. It features many virtual pure methods to overload in inherited classes.
	\item A \code{Private::TGeometricElement}, which purpose is to define all pure virtual methods of \code{GeometricElement}. The T prefix in its name stands for template: this class requires three template parameters, which are used as traits and CRTP. All pure virtual methods are overloaded and tagged as \code{final}, meaning an inherited class can't overload it any further. 
	\item Instantiations of the different type of geometric elements. For instance:
\begin{lstlisting}
class Triangle3 : public Private::TGeometricElement<Triangle3, GeoRefNS::Triangle3, Format::Triangle3>
\end{lstlisting} Few additional static methods are added at this stage.
\end{itemize}

So the general idea is that at creation the program must take care of filling the vector of smart pointers in \code{GeometricMeshRegion} (this is typically done while reading a mesh from an Ensight or a Medit file) and that afterwards this is this vector which is actually manipulated. 
\medskip

The \code{GeometricElement} base class is intended to fulfill all the public interface: there is no need in HappyHeart to perform a dynamic\_cast\footnote{Or more exactly its equivalent for smart pointers.} to transform the \code{GeometricElement} into the RTTI\footnote{Run-time type information.} type of the element (e.g. \code{Triangle3}).

\subsection{An example of implementation: Triangle3}

\subsubsection{Static members defined inside \lstinline{Triangle3} class}\label{section_static_members_tria3}

Besides the constructors and destructor, the body of \lstinline{Triangle3} is:

\begin{lstlisting}
 //! Name of such an object in Ensight 6 files
static const std::string EnsightName() { return "tria3"; }

//! Medit code for this object
static GmfKwdCod MeditId() { return GmfTriangles; }

//! Class name.
static const std::string Name() { return "Triangle3"; }

//! Identifier.
static GeometricElementEnum Identifier() { return GeometricElementEnum::Tria3; }

/*!
 * \brief Types of boundary elements.
 *
 * \return List of boundary elements as an array of smart pointers.
 */
static const std::array<GeometricElementType::shared_ptr, 3>& BoundaryElementList_();

\end{lstlisting}

The meaning of each of them is rather straightforward: it is mostly the identifiers that are required to recognize the object. The last container is there to say that the boundary elements of a \lstinline{Triangle3} are 3 \lstinline{Segment2}.

\subsubsection{Third template argument: Format}

\begin{lstlisting}
namespace Format
{
    
    namespace FormatNS = Private::Format;

    typedef FormatNS::Traits
    <
        FormatNS::Support<FormatNS::Medit, true>,
        FormatNS::Support<FormatNS::Ensight, true>
    > Triangle3;
        
} // namespace Format
\end{lstlisting}

The third template argument is quite straightforward too: it is there to specify whether the geometric element is supported by the two mesh formats currently implemented in HappyHeart.

The static members (see \refsection{section_static_members_tria3}) required to make the code compile depends on the value specified there: if for instance Medit support was \textit{false} then \lstinline{MeditId()} was not required in the class definition.

\subsubsection{First template argument}

The first template argument is the name of the class; it is for a C++ idiom called a \textit{curiously recurrent template pattern}. No need to bother about it in the present documentation.

\subsubsection{Second template argument: GeoRef-related quantities}

The second argument holds quantities that are closely related to the associated reference element. For implementation sake only (to avoid a too crowded class) this template argument is in fact itself split in two parts:

\begin{lstlisting}
namespace GeoRefNS
{

    typedef Private::GeoRefElement<GeoRefWithoutBasisNS::TriangleP1,    BasisFunctionNS::TriangleP1> Triangle3;


} // namespace GeoRefNS
\end{lstlisting}

This arbitrary separation has been made because for complex elements the definition of basis functions can be quite heavy.


\subsubsubsection{BasisFunctionNS::TriangleP1}

This trait class contains the definition of the basis functions and its first and second derivates. 

Its implementation is:


\begin{lstlisting}
// Function that takes a quadrature point as argument and returns a double.
typedef std::function<double(const QuadraturePoint&)> BasisFunctionType;  
    
const std::array<BasisFunctionType, 3>& TriangleP1::BasisFunction_()
{
    static std::array<BasisFunctionType, 3> ret
    {
        {
            [](const QuadraturePoint& pt) { return 1. - pt.x() - pt.y(); },
            [](const QuadraturePoint& pt) { return pt.x(); },
            [](const QuadraturePoint& pt) { return pt.y(); }
        }
    };

    return ret;
};



const std::array<BasisFunctionType, 6>& TriangleP1::FirstDerivateBasisFunction_()
{
    using namespace Private;

    static std::array<BasisFunctionType, 6> ret
    {
        {
            Constant<-1>(), Constant<-1>(),
            Constant<1>(), Constant<0>(),
            Constant<0>(), Constant<1>()
        }
    };


    return ret;
};


const std::array<BasisFunctionType, 12>& TriangleP1::SecondDerivateBasisFunction_()
{
    using namespace Private;

    static std::array<BasisFunctionType, 12> ret
    {
        {
            Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
            Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
            Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
        }
    };

    return ret;
};
\end{lstlisting}

where \lstinline{Constant} is a constant function that returns the template parameter as a double.


\subsubsubsection{GeoRefWithoutBasisNS::TriangleP1}

The first part of the implementation of this is the indexing of the vertices and interfaces (if relevant):

\begin{lstlisting}
 /************************************************************************
 *  geoElementTriangleP1
 *
 *     2
 *     | \
 *     |   \
 *     |     \
 *     |       \
 *     |         \
 *     0-----------1
 *         *************************************************************************/
const std::array<Point, TriangleP1::Npoint_>& TriangleP1::Coordinates()
{
    static std::array<Point, TriangleP1::Npoint_> ret
    {
        {
            Point(0., 0., 0.),
            Point(1., 0., 0.),
            Point(0., 1., 0.)
        }
    };

    return ret;
};


const std::array<TriangleP1::EdgeContent, TriangleP1::Shape::Nedge_>& TriangleP1::ReferenceEdgeList()
{
    static std::array<TriangleP1::EdgeContent, TriangleP1::Shape::Nedge_> ret
    {
        {
            { { 0, 1 } },
            { { 1, 2 } },
            { { 2, 0 } }
        }
    };

    return ret;
};


const std::array<TriangleP1::FaceContent, TriangleP1::Shape::Nface_>& TriangleP1::ReferenceFaceList()
{
    static std::array<TriangleP1::FaceContent, TriangleP1::Shape::Nface_> ret
    {
        { { {  0, 1, 2 } } }
    };
    
    return ret;
};


const std::vector<unsigned int>& TriangleP1::ReferenceNodeOnVertex()
{
    static std::vector<unsigned int> ret
    {
        0, 1, 2
    };

    return ret;
}


const std::vector<unsigned int>& TriangleP1::ReferenceNodeOnEdge()
{
    static std::vector<unsigned int> empty;
    return empty;
}


const std::vector<unsigned int>& TriangleP1::ReferenceNodeOnFace()
{
    static std::vector<unsigned int> empty;
    return empty;
}        
    
    
    
\end{lstlisting}

In the implementation above there is several instances of TriangleP1::Shape struct.

This struct holds several geometric quantities that are related to the type of the object but not on its degree (saying it otherwise properties that are the same for a Triangle3 and a Triangle6).

These properties are:

\begin{itemize}
    \item The dimension of the triangle (2)
    \item Its number of summits (3)
    \item The number of sides (3)
    \item The number of geometric face (1)
    \item The quadrature rules.
\end{itemize}

\subsubsection{Quadrature rules}

The quadrature rules are stored for each geometric shape as a list. The choice about which rule to use rely on the degree of exactness given in the input parameter file: the quadrature rule chosen is the cheapest one ensuring that polynoms of given degree are computed exactly.


\section{GeometricElementType}

Most of what we have seen in \refsection{section_geometric_element} are static members; but \lstinline{GeometricElement} class refers actually to a given geometric element of the mesh, and so some data members track which one is involved (the index of the geometric element, the indexes of each of its vertices and so on).

However sometimes we want to address a geometric element generically: we want to specify we consider \lstinline{Triangle3} as a generic type (for instance if we want to track what types are the elements in the boundaries of the mesh).

That is the role of \lstinline{GeometricElementType}; this class is defined above \lstinline{GeometricElement} by composition:

\begin{lstlisting}
class GeometricElementType final
{
    public:
    ...
        
        //! Return the value of the i-th basis function for the given point.
        double BasisFunction(unsigned int i, const QuadraturePoint& pt) const;
    ...
     
    private:

        //! Underlying GeometricElement object.
        GeometricElement::unique_ptr impl_;
}


 inline double GeometricElementType::BasisFunction(unsigned int i, const QuadraturePoint& pt) const
 {
     return impl_->BasisFunction(i, pt);
 }
\end{lstlisting}

So this new class takes only the part of the interface of \lstinline{GeometricElement} that makes sense for a generic standpoint.

This is not the more logical way (the other way around would be even more sensical) but it was done this way as \lstinline{GeometricElementType} was introduced much more lately in HappyHeart.

\section{ReferenceElement}\label{section_reference_element}

In HappyHeart (and Felisce) algorithm, the assembling is done by iterating through all the geometric elements found inside the mesh. For each geometric element and each unknown, a \lstinline{ReferenceElement} is determined and will be requested to known which basis function to use for instance.

This reference element is built on a composition upon GeometricElement:

\begin{lstlisting}
/*!
 * \brief Class in charge of reference element.
 */
class ReferenceElement final
{

public:

    //! Smart pointer.
    typedef std::shared_ptr<ReferenceElement> shared_ptr;

    //! Vector of smart pointers.
    typedef std::vector<shared_ptr> vector_shared_ptr;
    

public:

    /*!
     * \brief Constructor.
     *
     */
    ReferenceElement(const GeometricElementType& geometric_element_type);

    //! Destructor.
    ~ReferenceElement();

    //! Return the value of the i-th basis function on given point.
    double BasisFunction(unsigned int i, const QuadraturePoint& pt) const;

    //! Return the value of the icoor-th derivative of the i-th basis function on given point.
    double FirstDerivateBasisFunction(unsigned int i, unsigned int icoor, const QuadraturePoint& pt) const;

    //! Return the value of the (icoor, jcoor)-th second derivative of the i-th basis function on given point.
    double SecondDerivateBasisFunction(unsigned int i, unsigned int icoor, unsigned int jcoor, const QuadraturePoint& pt) const;

    //! Number of nodes.
    unsigned int Nnode() const;

    //! Number of nodes on vertices.
    unsigned int NnodeOnVertex() const;

    //! Number of nodes on edges.
    unsigned int NnodeOnEdge() const;
    
    //! Get the identifier of the related geometric element.
    GeometricElementEnum GetIdentifier() const;
    
    //! Get the dimension of the geometric element.
    unsigned int GetDimension() const;
    
    //! Get quadrature rule.
    const QuadratureRule& GetQuadratureRule(unsigned int degree) const;


private:

    //! Underlying GeometricElementType object.
    GeometricElement::unique_ptr impl_;
};
\end{lstlisting}


When considering a \lstinline{GeometricElementType} and an \lstinline{Unknown}, the \lstinline{ReferenceElement} is determined according to the rule:


\begin{itemize}
    \item If the finite element type is linear, create a \lstinline{ReferenceElement} object based upon the same geometric type as the geometric element. So for instance for a \lstinline{Triangle3} a \lstinline{Triangle3} will be provided to \lstinline{ReferenceElement}. constructor.
    \item If the finite element type is quadratic, look into a map which type should be associated. For instance for a \lstinline{Triangle3} a \lstinline{Triangle6} will be provided to \lstinline{ReferenceElement}.
    
\end{itemize}




\section{CurrentFiniteElement}\label{section_finite_element}

The last type of element in HappyHeart is the \lstinline{CurrentFiniteElement}. This class is the one that performs the operation described in \refsection{section_reference_element} to determine the \lstinline{ReferenceElement} to use.

The \lstinline{CurrentFiniteElement} is really the object upon which the operators will be applied; it is for instance there that the jacobian will be computed.

The class exactly the same as in Felisce: there are no container that store all the finite elements. Instead, in the assembling loop, we iterate on all geometric elements sort by their types. For a given geometric type and unknown, a \lstinline{CurrentFiniteElement} is invoked and it is the same object that will be used for all geometric elements of the same type for the chosen unknown.

For each \lstinline{GeometricElement} of the same type, the internals of the \lstinline{CurrentFiniteElement} are adjusted (for instance if relevant the jacobians are computed).

This way of doing things makes sense as the vectors and matrices required for the calculation are allocated just once per type instead of one per element.




\end{document}