[TOC]

# Introduction

MoReFEM uses up Gitlab-CI for its continuous integration (shortened as 'CI'): the highlight of this solution is that the setup is very easy to put on motion and the git repository versions directly the Yaml files used to run the CI.

The configuration files related to CI are all put in the `ExternalTools/Gitlab-CI` directory - I didn't respect the usual convention to put a `gitlab-ci.yml` file at the root of the repository, as I used several files to gain in clarity. This adds an additional step in setting up Gitlab-CI for your own fork - see dedicated section below.

# Installation

CI is properly set for the [main MoReFEM project](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM).

If you want to do so on a fork of the project, you have three steps to follow, two of which are straightforward.

## Setting up the Yaml file in Gitlab

As we do not use the conventional `.gitlab-ci.yml` file, we need to tell Gitlab where is the file to use.

Go in your fork's `Settings > CI/CD > General pipelines` and choose `ExternalTools/Gitlab-CI/gitlab-ci.yml` in the field `CI/CD configuration file`.

If `CI/CD` doesn't appear in your interface, go to `Settings > General > Visibility, project features, permissions` and check `CI/CD` is properly enabled.

If you don't want to bother with macOS runners, `ExternalTools/Gitlab-CI/gitlab-ci-no-macos.yml` instead.


## Setting up shared runners (to run Docker containers)

Go in your fork's `Settings > CI/CD > Runners` and ensure `Enable shared runners for this project` is enabled.

One it's done, you are able to run all the CI jobs that are using up Docker containers.

Using these shared runners is much more easier that former procedure that required to deploy VM and run personal runners on them (the procedure was dropped from MoReFEM tutorial in May 2022).

## Setting up macOS (shell) runners

You need to define a shell runner on a macOS machine; to run modern versions of AppleClang you need ARM architecture for your mac.

The complete procedure to do so from scratch is detailed [here](SetUpmacOSVM.md); you need to dispose of the administration rights on the computer to be able to do all of them.


# Content of MoReFEM CI

MoReFEM CI is composed of many jobs (more than twenty of them...) but not all of them are built each time. We will detail each target and when they are intended to be run (or course in the end it is the yaml files which are right if there is a discrepancy...).

There are three specific branches in MoReFEM (for the [main repository](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) - the fine-tuning of tests described below is only for this repository, not for the forks):

- The `main` branch, which is the state of the latest release. There are no tests run with this branch - the idea is that the push should be done when the `release-candidate` has been approved by the integration manager. Modification of this branch triggers the update of the [Doxygen documentation](https://morefem.gitlabpages.inria.fr/CoreLibrary/MoReFEM).
- The `develop` branch, which is the working branch managed by the integration manager. Typically issues that have been validated by the integration manager but are not yet incorporated in a full release are there.
- The `release-candidate` branch, which should be pushed before a push to `main`. There are more extensive tests run here.


## Stages build_and_test and check_warnings

Those two first stages are the ones that are most often run: basically they are at each push toward gitlab.

### build_and_test

There are two different types of jobs here:

- Compilation ones
- Doxygen ones

#### Compilation and tests

The library is compiled on three different OS (macOS, Ubuntu and Fedora) with different configurations:

- clang, gcc or Apple Clang as compilers.
- Static or shared library.
- Debug or release mode.
- MoReFEM compiled as one big library or 10 smaller ones.

Of course all combinations aren't tested, but currently 8 of them are nonetheless.

If you want to skip entirely this stage, use **noci** or **nobuild** in the name of the branch pushed to Gitlab.

#### Doxygen

The three levels of documentation (see [here](../../Doxygen/README.md#levels-of-infirmation)) are generated.

### check_warnings

The purpose of this second stage is to sift through the logs of the jobs of the first stage to identify whether there are warnings (be it from compilation or from Doxygen).

There is the same number of jobs as in first stage; the analysis is made by custom scripts that are in `Scripts/Tools ` directory (respectively `find_warning_in_compilation_log.py` and `find_warning_in_doxygen_log.py`).

These jobs don't result on complete failure (the tests are in orange in this case) but they must pass completely before being integrated in a full release.

It should be noticed that Gitlab has evolved and it is now possible to start a job in the second stage without having to run all the jobs in first stage (thanks to the `needs` instruction in Yaml files).

If you want to skip entirely this stage, use **noci** or **nodox** in the name of the branch pushed to Gitlab.

## Stage Valgrind

At this stage, all the model instances are run with Valgrind to check there are no memory leaks (or more precisely that there are no memory leaks that are knowingly related to the third party library used - see [this page](../../../ExternalTools/Valgrind/README.md) for more details about the command used).

The base Docker image used here is generated using the dedicated [AnalysisTools](https://gitlab.inria.fr/MoReFEM/analysistools) project; the idea here is just to add Valgrind to one of the image used in previous stage (namely the Fedora image with clang compiler).

This stage is intended to be run much less often; one of the three conditions must be met:

- The branch pushed is `release-candidate` on the main project (thus concerns only the integration manager)
- The branch pushed includes **valgrind** in its name.
- The branch pushed includes **full_ci** in its name.

## Stages analysis and Sonarqube

These stages are akin to Valgrind one and are run only when one of the three conditions below is met:

- The branch pushed is `release-candidate` on the main project (thus concerns only the integration manager)
- The branch pushed includes **sonarqube** in its name.
- The branch pushed includes **full_ci** in its name.

Analysis stage runs some analysis tools using specific Docker images created in the [AnalysisTools](https://gitlab.inria.fr/MoReFEM/analysistools) project; the results from these tools are then aggregated by Sonarqube.

These steps may be very long: cpp checks runs over two hours and sonar-scanner takes at least half an hour.

The result is published on Inria Sonarqube instance at [this link](https://sonarqube.inria.fr/sonarqube/dashboard?id=m3disim%3Amorefem%3Adevelop) (please don't bother about the branch name: I started working on this branch without realizing it couldn't be renamed later...)



## (Former) stage Linter

This stage... no longer exists. I indicate it here to explain why.

Two jobs were associated to it: one to run include what you use, and the other to run clang-format.

Unfortunately, it proved to be very unwieldy and time consuming for the integration manager: even when ensuring the version used for these software were the same, the output did not match exactly all the time.

The jobs have therefore be removed and both those software are run locally by the integration manager at least once before each release.

If you want to run them at some point:

### Include-what-you-use

A README explain how to work with it [here](../../../ExternalTools/IncludeWhatYouUse/README.md).

You may force some behaviour with comments that look like:

````
#include <cstddef> // IWYU pragma: keep
````

There are other commands than `keep`; check the [IWYU documentation](https://github.com/include-what-you-use/include-what-you-use/blob/master/docs/IWYUPragmas.md) to find out existing pragmas.

### clang-format

Make sure clang-format is installed on your system, and then run from root directory:

```
python Scripts/Tools/run_clang_format.py
```

(preferably starting from a clean git status to be able to check what was changed).

You may deactivate it in some portions of the code with 

```
 // clang-format off
```

which neutralize it until the opposite

```
 // clang-format on
```

is written.












