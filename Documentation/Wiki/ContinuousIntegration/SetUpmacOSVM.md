[TOC]

# Install Miniconda environment


```shell
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh
conda update conda
conda create -n Python3  python=3
conda activate Python3
conda install six
python -m pip install distro
```

# Install third party libraries

```shell
git clone https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory.git
```

Edit morefem_macos_clang.py:

```
    target_directory = "/Volumes/Data/ci/opt/clang_{}".format(mode)
    python = "/Users/ci/miniconda3/bin/python"
```

In Conda environment Python3:

```shell
python morefem_macos_clang.py
```

# Install gitlab-runner

## As an administrator:

```shell
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64

sudo chmod +x /usr/local/bin/gitlab-runner
```

## As CI user:

- Go in the interface of your Gitlab repository, choose "Settings > CI/CD > Runners > New project runner". 

- As tag choose `macos`; name your runner so that in the `config.toml` file you may identify it (in the `.gitlab-runner` directory that will be created shortly on your home directory).

When you're done click on "Create runner".

- Choose "macOS" as operating system.

- Copy the command given in step 1, e.g.

```shell
gitlab-runner register --url https://gitlab.inria.fr --token XXXXXX
```

and run it on your computer with macOS.

Make sure to delete newlines: if you do it right you shouldn't have to type url or token.

- Choose "shell" as executor.


- The runner should now appear in gitlab, but possibly with a grey dot instead of a green one. In this case, type:

```shell
sudo gitlab-runner run 
```

which should make the dot green within a minute.