[Go back to the model tutorial main page](../model_tutorial.md)


## Prerequisites

The prerequisites are:


- [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) is properly installed.

Make sure the command to install it (`make install` or `ninja install`) has been properly run; you should have generated somewhere (given by the `--install_directory` flag you used to compile MoReFEM) a directory named `MoReFEM` which contains several subdirectories (`bin`, `cmake`, `include`, `lib`, `share` at the time of this writing).

In the following we will assume the environment variable `MOREFEM_INSTALL_PREFIX` points to this directory (you may skip the environment variable and provide the path whenever required of course).

- _(optional)_ `ninja` is installed. If not, remove the `-G Ninja` in later commands and use `make` whenever `ninja` is specified.

We will also assume an environment variable `MOREFEM_MODEL_TUTORIAL` has been defined to point to the directory in which our new project will be created (this is of course entirely optional but is helpful for the sake of this tutorial to avoid periphrasis).


## Creating a (very bare) project

- Create the directory `${MOREFEM_MODEL_TUTORIAL}` for your project wherever you want on your filesystem and place yourself there.

- Copy a basic `CMakeLists.txt` there:

```shell
cp ${MOREFEM_INSTALL_PREFIX}/cmake/ExternalModelCMakeLists.txt ${MOREFEM_MODEL_TUTORIAL}/CMakeLists.txt
```

Edit this file to fulfill the field `project()`, e.g. with `project(ModelTutorial)` (the important is to choose a name without spaces or unsavory special characters). 

This file won't be modified more.

- _(optional)_ Copy the clang-format configuration file:

```shell
cp ${MOREFEM_INSTALL_PREFIX}/share/clang-format ${MOREFEM_MODEL_TUTORIAL}/.clang-format 
```



- Create the skeleton files for the build:

```shell
mkdir Sources
touch Sources/CMakeLists.txt
```

- Build the very bare project:

```shell
mkdir build && cd build
python ${MOREFEM_INSTALL_PREFIX}/cmake/configure_cmake_external_model.py --morefem_install_dir=${MOREFEM_INSTALL_PREFIX} --cmake_args="-G Ninja"
```

You should get a line indicating build files have been written; `ninja` command should work and inform you there is no work to do.

It's now time to issue our very first commit:

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

```shell
echo build > .gitignore
git init
git add CMakeLists.txt Sources .gitignore .clang-format
git commit -m "Initial commit."
```



## Creating barebone source and header files

### `CMakeLists.txt`

We will now add:

- A main file to create the executable of the model.
- A library that will include the bulk of the model. Using a library will permit to reuse the same code for other executables, such as one to post-process the data and transform them into Ensight format or another to update the content of the Lua file used to provided input data to the model.

We'll start by writing the `Sources/CMakeLists.txt` file we created previously with:

```cmake
add_library(ModelTutorial_lib ${LIBRARY_TYPE} "")

target_sources(ModelTutorial_lib
	PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
    	${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
    	${CMAKE_CURRENT_LIST_DIR}/Model.hpp
    	${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
    	${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
    	${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx
)

target_link_libraries(ModelTutorial_lib                          
                     $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MAIN_LIBS}>)

add_executable(ModelTutorial ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(ModelTutorial
                      ModelTutorial_lib)
```

These commands:

- Create a library `ModelTutorial_lib` with all the listed files (one of which must be a compiled file - i.e. one with `cpp` extension following MoReFEM coding standards; if not you should use instead the `INTERFACE` syntax - see for instance this [CMakeLists.txt](https://gitlab.inria.fr/MoReFEM/Models/ReactionDiffusion/-/blob/master/Sources/CMakeLists.txt) to see how it should be used). This library may be static or dynamic, choosing the same convention as the one used to build MoReFEM library (and present here through the `LIBRARY_TYPE` environment variable).
- Link this library to the main MoReFEM library. Please keep the `LINK_LIBRARY:WHOLE_ARCHIVE` command; it is required to make the linking work in all build configurations (static or dynamic library, in macOS or in Linux).
- Create an executable `ModelTutorial` which is linked against `ModelTutorial_lib`.


### Creating barebone files

MoReFEM comes with a utility that is used to create default source and header files; using it is of course absolutely optional but it's advisable to use it as it does some of the boilerplate for you and prepare specific blocks that may be filled automatically by scripts (for instance to fill copyright notice at the beginning of the file or header guards).

As we will use this utility with the very same options quite a few times, I suggest defining an alias for it:

```shell
alias new_file_in_tutorial='python ${MOREFEM_INSTALL_PREFIX}/share/Scripts/Tools/new_file.py --project="ModelTutorial" --sources_dir=${MOREFEM_MODEL_TUTORIAL}/Sources --copyright_notice=${MOREFEM_MODEL_TUTORIAL}/CopyrightNotice.txt'
```

This script comes with three command line arguments:

- The name of the project.
- The directory that will be used as reference for includes (basically the one you would provide to the compiler through the `-I` option)[^header_ambiguity].
- A file with text to put in the beginning at the file in the copyright notice. You have to specify such a file; if you don't know yet what to put inside don't worry: there is a script to update after the fact all of your source files[^automated_block_script]. The one used in MoReFEM is available if you need inspiration [^copyright_notice]. Here for this tutorial we can just use the command:

[^header_ambiguity]:Make sure when creating a file locally there is no ambiguity with a file from the main library (e.g. don't create a file named `Model.hpp` locally in a directory named `Model` as _${MOREFEM_INSTALL_PREFIX}/include/Model/Model.hpp_ does already exist). 

```shell
echo 'Placeholder text for copyright.' > ${MOREFEM_MODEL_TUTORIAL}/CopyrightNotice.txt
```


[^automated_block_script]: Available here: `${MOREFEM_INSTALL_PREFIX}/share/Scripts/Tools/generate_automated_blocks_content_in_source_files.py`.

[^copyright_notice]: Available here: `${MOREFEM_INSTALL_PREFIX}/share/CopyrightNotice.txt`.

To use the script, place yourself in the directory where you want to create the file and call the command we've just defined:

```shell
cd ${MOREFEM_MODEL_TUTORIAL}/Sources
new_file_in_tutorial
```

We will do so several times:

- `main`:  Choose "M" and a `main.cpp` will be added.
- `Model`: Choose "mo", don't modify the default name and choose `ModelTutorialNS` as supplementary namespace; three files `Model.cpp`, `Model.hpp` and `Model.hxx` will be created.
- `VariationalFormulation`: Choose "v" and same namespace and three files `VariationalFormulation.cpp`, `VariationalFormulation.hpp` and `VariationalFormulation.hxx` will be created.
- `InputData.hpp`/`ModelSettings.cpp`: Choose "i" and same namespace and these files will be created.

(don't worry we will explain each of them in due time).

We need to provide few adjustments in `main.cpp` to make everything compile:

- Replace commented include line by `#include "Model.hpp"`
- Replace the template argument of `Main`: `return MoReFEM::ModelNS::Main<MoReFEM::ModelTutorialNS::Model>(argc, argv);`

It's now time to commit!

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

```shell
git add Sources CopyrightNotice.txt
git commit -m "Add skeleton versions of the files needed to define the model."
```

## _(optional)_ Generate XCode project 


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

```shell
echo XCode >> .gitignore
git commit -a -m "Add XCode directory to .gitignore."
mkdir XCode && cd XCode
python ${MOREFEM_INSTALL_PREFIX}/cmake/configure_cmake_external_model.py --morefem_install_dir=${MOREFEM_INSTALL_PREFIX} --cmake_args="-G Xcode"
```

You now get a `ModelTutorial.xcodeproj` in this directory, that may be used to edit, compile and run the code.
It is possible to refine the presentation of the file hierarchy inside - this has been done for the generated MoReFEM XCode project; hints about how to do it are documented [here](../../Tooling/CMake_XCode.md).





[Go back to the model tutorial main page](../model_tutorial.md)