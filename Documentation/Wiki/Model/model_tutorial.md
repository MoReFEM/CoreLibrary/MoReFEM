[TOC]

# Introduction

The goal of this tutorial is to build step by step a rather simple model using MoReFEM: elastic deformation of a bar upon which a force is applied; time step is constant and volumic mass is the same throughout the bar.  

The library was first built with a use in macOS and XCode in mind, but it has been extended since and the procedure should guide you to make the project work in both macOS and Linux environments.


# Description of the mechanical problem

[On this dedicated wiki page](ModelTutorial/MechanicalProblem.md)

# Creating the new project

[On this dedicated wiki page](ModelTutorial/CreatingProject.md)

# Writing the model

[On this dedicated wiki page](ModelTutorial/WriteModel.md)

Don't be afraid of the length of this tutorial:

- Steps are detailed in depth.
- Some concepts of the library are explained along the way.

And don't hesite to contact morefem-maint@inria.fr if you need help!|

# Tips and tooling

## Running in parallel

MoReFEM is designed so that a model written in sequential should work directly in parallel.

You may choose a parallel run with:

```shell
${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/bin/mpirun -np 2 Sources/ModelTutorial -i ../Sources/demo.lua --overwrite_directory
```

where environment variable `MOREFEM_THIRD_PARTY_LIBRARIES_DIR` should be set to the value it held in `${MOREFEM_INSTALL_PREFIX}/cmake/PreCache.cmake` file and `np` indicates how many processors should be used (don't choose too high a value: we may run into errors when the number of processors is overdimensioned compared to the size of the model considered).

The data are written in a different subdirectory as the sequential run; when aggregated (for instance with `EnsightOutput` executable - see below) the values should be comparable to the sequential ones.


## Ensight output

For all our models, we usually provide a post processing executable which generates output files in Ensight format.

Let's create a new file `main_ensight_output.cpp` in `Sources` directory. We may use once again our dedicated script:

```shell
cd ${MOREFEM_MODEL_TUTORIAL}/Sources 
new_file_in_tutorial
```

and choose "e".

This will create a `main_ensight_output.cpp` file; let's edit it and fill the proper values:


<p><strong><font color="green">In main_ensight_output:</font></strong></p>

- In include lists, add 

```c++
#include "Model.hpp"
``` 

- In the bulk of the function:

```c++
{
    constexpr auto mesh_id = AsMeshId(ModelTutorialNS::MeshIndex::solid);

    std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId(
        ModelTutorialNS::NumberingSubsetIndex::displacement) };

    std::vector<std::string> unknown_list{ "displacement" };

    return MoReFEM::ModelNS::MainEnsightOutput<ModelTutorialNS::Model>(argc, argv, mesh_id, numbering_subset_id_list, unknown_list);
}
```

<p><strong><font color="green">In Sources/CMakeLists.txt:</font></strong></p>

```cmake
add_executable(ModelTutorial-EnsightOutput
              ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)
              
target_link_libraries(ModelTutorial-EnsightOutput
                      ModelTutorial_lib
                      MoReFEM_post_processing)  
```

Of course check it works as expected:

<p><strong><font color="red">In a terminal in build directory:</font></strong></p>

```shell
ninja
Sources/ModelTutorial-EnsightOutput -i ${MOREFEM_RESULT_DIR}/ModelTutorial/Rank_0/input_data.lua
```

(the Lua file used has been copied there in the output directory.)

This will generate files in `$MOREFEM_RESULT_DIR/ModelTutorial/Rank_0/Mesh_1/Ensight6` directory; check the generated scl files are very close to the ones in `Sources/ModelInstances/Elasticity/ExpectedResults/Ascii/2D/Mesh_1/Ensight6` in MoReFEM.

You may use the files hence produced to visualize the result with Paraview or Ensight.

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

```shell
git add Sources
git commit -m "EnsightOutput executable added."
```


## UpdateLuaFile

Another facility that is rather handy is a program that enables upgrading of the Lua file (for instance to reflect the changes in automated comments). This is a beta program at best and don't cover all the cases that might have modified the Lua files, but it may still be of use.

```shell
cd ${MOREFEM_MODEL_TUTORIAL}/Sources 
new_file_in_tutorial
```

and choose "l".

<p><strong><font color="green">In main_update_lua_file.cpp:</font></strong></p>

- In include lists, add 

```c++
#include "Model.hpp"
``` 

- Replace template argument by `ModelTutorialNS::Model`.

<p><strong><font color="green">In Sources/CMakeLists.txt:</font></strong></p>

```cmake
add_executable(ModelTutorial-UpdateLuaFile
              ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)
              
target_link_libraries(ModelTutorial-UpdateLuaFile
                      ModelTutorial_lib
                      MoReFEM_post_processing)
```


<p><strong><font color="red">In a terminal in build directory:</font></strong></p>

```shell
ninja
Sources/ModelTutorial-UpdateLuaFile -i ../Sources/demo.lua 
```

If you're already familiar with the expected Lua syntax, you may use the `--skip-comments` flag which will remove most of the comments of the file, leaving only those you really need (for instance the list of unknowns in a finite element space remain because you need to know their ordering to assign properly the shape functions).


```shell
Sources/ModelTutorial-UpdateLuaFile -i ../Sources/demo.lua  --skip-comments
```

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

```shell
git add Sources
git commit -m "UpdateLuaFile executable added; demo.lua file features now much less comments."
```


## Integration tests

If your model is to be kept, it is advised to also set up integration tests to check the results keep stable over time, especially when libraries (both MoReFEM and third party ones) have been updated.

This tutorial has already been long and dense enough so we won't delve into this here and now, but you may have a look to existing models to see how it is usually done (for instance [here](https://gitlab.inria.fr/MoReFEM/Models/AcousticWave) for acoustic wave model).

The geist of the numeric comparison is a bit of cheating: we actually compare for the time beings the results from the `EnsightOutput` post-processing executable, as this format lose some numeric precision and we do not trigger a "discrepancy" when there is a neglictible numeric difference (especially considering the `ExpectedResults` were generated for a given OS on a given computer and we want to reach the same result regardless of the computer and OS used).