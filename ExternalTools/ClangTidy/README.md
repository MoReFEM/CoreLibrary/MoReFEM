To run clang-tidy:

- A build command must be called with the `-DCMAKE_EXPORT_COMPILE_COMMANDS=ON` option so that a `compile_commands.json`
file is available. This option may be added in the `--cmake_args` of `configure_cmake.py` script 
(see [main README.md](../../README.md) for this).

- Then in this build directory run 

```shell
ls ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/include/mpi.h && run-clang-tidy  -extra-arg="-I${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/include" 2>&1 | tee tidy.log 
```

where:

- `clang-tidy` and `run-clang-tidy` are present in the PATH
- `MOREFEM_THIRD_PARTY_LIBRARIES_DIR` points where the third party libraries are installed.
- First part of the command is there just to check environment variable `MOREFEM_THIRD_PARTY_LIBRARIES_DIR` is defined
with a legit value.

An introduction of clang-tidy was presented in september 2024 by Sébastien; the support is
 [here](https://gitlab.inria.fr/sgilles/clang-tidy-feedback).