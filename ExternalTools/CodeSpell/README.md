The idea is to run regularly codespell to check spelling.

Unfortunately, the facility to ignore words 


````
codespell --ignore-words=ExternalTools/CodeSpell/Ignore.txt
````

doesn't seem to work properly (see [this issue](https://github.com/codespell-project/codespell/issues/2451)).

So the plan is to run commands such as:

````
find . -name "*.cpp" | xargs codespell --ignore-words=ExternalTools/CodeSpell/Ignore.txt
find . -name "*.hpp" | xargs codespell --ignore-words=ExternalTools/CodeSpell/Ignore.txt
find . -name "*.hxx" | xargs codespell --ignore-words=ExternalTools/CodeSpell/Ignore.txt
find . -name "*.md" | xargs codespell --ignore-words=ExternalTools/CodeSpell/Ignore.txt
````

and to fix manually the outputs (we'll reconsider when filtering fully works).
