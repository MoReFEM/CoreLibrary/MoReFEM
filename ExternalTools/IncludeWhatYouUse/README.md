[TOC]

# Install IWYU

Rather obviously, first install https://include-what-you-use.org/ on your computer, e.g. with:

```shell
brew install include-what-you-use
```

on macOS.

# How to set up include-what-you-use

Then following what is suggested on their (messy) [documentation](https://github.com/include-what-you-use/include-what-you-use/blob/master/README.md), we will create a specific build directory for it, named `build_iwyu` here.

```shell
mkdir build_iwyu
cd build_iwyu
```

Then we will call CMake with our usual script, providing several additional options:

```shell
python3 ../cmake/Scripts/configure_cmake.py  --third_party_directory=/Users/sgilles/opt/clang_debug --cache_file=../cmake/PreCache/macos_apple_clang.cmake --cmake_args="-DCMAKE_C_COMPILER=/opt/homebrew/opt/llvm/bin/clang -DCMAKE_CXX_COMPILER=/opt/homebrew/opt/llvm/bin/clang++ -G Ninja  -DCMAKE_CXX_INCLUDE_WHAT_YOU_USE='/opt/homebrew/Cellar/include-what-you-use/0.22/bin/include-what-you-use;-Xiwyu;--mapping_file=../ExternalTools/IncludeWhatYouUse/morefem.imp;-Xiwyu;--no_comments;-Xiwyu;--cxx17ns;-Xiwyu;--max_line_length=120' -DMOREFEM_IGNORE_TESTS=True"  --morefem_as_single_library=False 
```

Few notes:
- Of course replace the paths to match your configuration:
  . The cache file
  . include-what-you-use paths (even on macOS Homebrew path has been changed recently from `/usr/local/` to `/opt/homebrew`...)
  . clang and clang++ paths
- `-G Ninja` is a matter of taste: both `ninja` and `make` work with include-what-you-use.
- Make sure to use the vanilla clang and clang++, and not the Openmpi wrappers we usually used in the build of MoReFEM. The reason is that if you use Openmpi ones include-what-you-use will be stuck as the path to Openmpi includes is removed when those wrappers are used.
- MOREFEM_IGNORE_TESTS is a macro inside CMake build that is not publicized but disables most of the integration tests (only the test tools library and the tests related to Models remain).



# How to use it

Just invoke your build command (`ninja` or `make`). This will both compile the file and run include-what-you-use on it.

It behaves as a true build command: on repeated runs only the files modified will be recompiled / handled.

You may use:

````
ninja 2>&1 | tee iwyu.log
`````

if you also want to print information in a file.

Then calling the script:

````
python ../Scripts/CI/IncludeWhatYouUse/interpret_log.py --log iwyu.log
````

will help you extract the information you need from the log and reframe it in a more easily useful way.