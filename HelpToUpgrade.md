# From v24.42 to v24.51

- [#1989](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1989): Remove `TimeManager` template from `InitScalarParameterFromInputData`, `InitVectorialParameterFromInputData`, `InitMatricialParameterFromInputData` and `Init3DCompoundParameterFromInputData` instances with `morefem_data` argument.
- [#1990](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1990):
    * `AbstractMatrix`: `GetProcessorWiseSize()` and `GetProgramWiseSize()` now return strong types.
    * `Vector`: `GetProcessorWiseSize()` and `GetProgramWiseSize()` now return strong types.
    * `Vector`: `SetValue()` and `GetValue()` now use strong type as argument.
    * `AbstractMatrix`: `GetValue()` and `GetRow()` now use strong type as argument.

# From v24.24 to v24.42

- [#1937](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1937): If one of your model fails with message `Assertion failed: (is_malloc_allowed() && "heap allocation is forbidden (EIGEN_RUNTIME_NO_MALLOC is defined and g_is_malloc_allowed is false)"), function check_that_malloc_is_allowed, file Memory.h, line 164.`, please open an issue to tell which operator(s) are used in your model, so that they may be patched and added properly in the test suite.
- [#1948](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1948):
    * `SpatialPoint::GetCoordinateList()` -> `SpatialPoint::GetUnderlyingVector()`.
    * `space_unit` argument is now a string type.
- [#1929](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1929):
    * Free functions `ComputeGradientDisplacementMatrix()`, `ExtractSubVector()`, `ExtractSubMatrix()` are now methods of `RefFEltInLocalOperator`.
- [#1840](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1840): Eigen has replaced Xtensor. To update an operator, the best is to have a look at _Sources/Test/ThirdParty/Eigen/test.cpp_ file which showcases the syntax that replaced Xtensor ones. Feel free to ask [Sébastien](sebastien.gilles@inria.fr) for help! (Eigen provides a very nice option that asserts if there is memory allocation but this might be tricky to solve properly if you don't know how to - see [#1937](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1937)).
    * **Beware!** If you have defined in your model a policy for SecondPiolaKichhoffTensor, parameter `transposed_De` is now `De`. Don't forget to adjust your operator in consequence!
- [#1941](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1941) - [#1930]([#1941](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1930)):
    * Functions `ExtractGradientBasedBlockRowMatrix`, `ExtractGradientBasedBlockColumnMatrix` and `ExtractGradientBasedBlock` are now replaced by Eigen slicing. See the ticket for the replacement.
- [#1938](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1938):
    * These are no longer `LocalVector` and `LocalMatrix` aliases: we strive now to use the most efficient storage to each case (telling when maximum size is known at compile time for instance). The generic cases are `Eigen::VectorXd` and `Eigen::MatrixXd`, but there are many locations where you can use better alternatives (some useful aliases are defined in `Wrappers::EigenNS`).
- [#1933](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1933): Use a strong type for geometric element dimension.
- [#1967](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1967): Derivates relative to invariants have been renamed, e.g. `SecondDerivativeWSecondInvariant` -> `d2WdI2dI2`.

# From v24.20 to v24.24

- [#1847](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1847): Replace `std::size_t` rank arguments by `MoReFEM::rank_type` strong type.
- [#1898](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1898): Replace `std::size_t` row and column indexes in small matrices by the new strong types `small_matrix_row_index_type` and `small_matrix_col_index_type`.
- [#1920](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1920): Remove from your PreCache file METIS_INCL_DIR, which is no longer used.


# From v24.06 to v24.20

- [#1889](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1889): Some PETSc matrix operations (mostly those involving matrix-matrix products) now result in a new type named `GlobalMatrixOpResult`.
- [#1896](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1896):
  * Consider replacing operations from `Wrappers::Petsc` namespace by their equivalent in new `GlobalLinearAlgebraNS` namespace (additional checks performed in debug mode).
  * `MatTranspose` wrapper has been dropped; I don't think it was used in any model, and there is an ambiguity with `MatCreateTranspose`. Please issue a ticket if you need it back.
  * `DoReuseMatrix` have been dropped and are handled automatically by the library.
- [#1906](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1906): Renaming the enum class `IsMatrixOrVector` `LinearAlgebraNS::type` (the include was also changed to `Utilities/LinearAlgebra/Type.hpp`).
- [#1899](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1899): Script `header_guards.py` has been discontinued and is now replaced by the broader `generate_automated_blocks_content_in_source_files.py`.

# From v24.04 to v24.06

- [#1885](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1885): The `Extract` functions from `InputDataNS` namespace have been modified:
    * Most are now in `Internal` namespace, as we want to use primarily those that may take values from both `input_data` and `model_settings` without assuming all fields are in one or the other (or the syntactic sugar version that takes `morefem_data` instead).
    * `InputDataNS::ExtractLeafFromInputDataOrModelSettings` has been renamed `InputDataNS::ExtractLeaf`, as there is no longer any ambiguity given the pre-existing namesakes are now in a different (and internal) namespace. `model_settings` argument should be provided if it was not the case (some calls might still refer only to `input_data`), or the  syntactic sugar version that takes `morefem_data` may be used as well.
    * The existing suffix `InputDataNS::ExtractLeaf<XXX>::Value` and `InputDataNS::ExtractLeaf<XXX>::Path` should respectively be replaced by `InputDataNS::ExtractLeaf<XXX>` and `InputDataNS::ExtractLeafAsPath<XXX>`.
    * Some functions such as `ApplyInitialConditionToVector` saw their signature change to use `morefem_data` instead of `input_data`.
# From v24.03 to v24.04

- [#1877](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1877): `VariationalFormulation` arguments have been modified:
    * `morefem_data` is no longer `const`, and has been moved at the end.
    * `time_manager` is no longer there
- [#1878](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1878): You may have to add `model_settings` argument to some `TimeManager` related functions.
- [#1879](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1879): Now in most functions `model_settings` must come before `input_data`
- [#1864](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1864): 
    * Replace `#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"` by  `#include "Core/InputData/Instances/Core/NumberingSubset.hpp"`
    * Probably remove `NumberingSubset` section from `input_data_tuple` or `model_settings_tuple`; replace it with its leaf `DoMoveMesh` if your model required to set this value as `true`.
    * Remove the `DoMoveMesh` in Lua files or from `ModelSettings::Init()` (unless you need it set to true for your model)


# From v23.45 to v24.03

- [#1859](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1859) - [#1866](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1866):
    * Add in `InputData.hpp` (or similar) a trait to the `TimeManager` instance to use; for most usual policies (`ConstantTimeStep` and `VariableTimeStep`) there is now an argument telling whether restart mode is supported.
    * Add in `MoReFEMData` template arguments the aforementioned trait.
    * Remove `TimeManager` tmplate argument from `Model`.
    * `Model` constructor argument should lose the `const` qualifier for `MoReFEMData`
    * `policy_to_adapt_time_step` has been renamed `adapt_time_step`.

# From v23.37 to v23.45

- [#1852](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1852):
    . Add in `InputData` the type of `TimeManager` to use, e.g. `using time_manager_type = TimeManager<TimeManagerNS::Policy::ConstantTimeStep>;`. Don't forget the includes! (`#include "Core/TimeManager/TimeManager.hpp"` and `#include "Core/TimeManager/Policy/ConstantTimeStep.hpp"` - for our example).
    . Add this alias to `Model` and `VariationalFormulation` template arguments (and in the traits alias there such as `parent`).
    . If you're an Advanced user and defined yourself some `Operator` or `Parameter`, they may need the additional `TimeManagerT` template argument (have a look at others already in the library - all `Parameter` will need the change, as well as `Operators` that store `Parameter` as data attribute)..
    . `UpdateFiberDeformation` arguments ordering has been changed to respect coding standard.


# From v23.37 to v23.45

- [#1686](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1686) Replace `invoking_line / invoking_file` by a `std::source_location` object. Some function prototypes have been changed so that new argument `std::source_location` comes last this permitting to use `std::source_location::current()` as a default value.
  - In `Wrappers::Petsc::PrintMessageOnFirstProcessor`, you have to add `std::source_location::current()` as third argument.


# From v23.11.2 to v23.37

- [#1801](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1801) `MainEnsightOutput` now takes the model as template argument, not just the `InputData`.
- [#1750](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1750) A concept for `MoReFEMData` has been introduced; some `class InputDataT` must be replaced by `::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT`.
- [#1796](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1796) `InputData` has been split so that some values may be defined only by the author of the model. At minimum you should introduce a `ModelSettings` object which should defined the `IndexedSectionDescription` leaves of the indexed section object - but I advise you to take the time to fully use what this new object enables. There is an extended explanation of it in `${MOREFEM_ROOT}/Documentation/Wiki/Utilities/InputData.md`; you may also see the `${MOREFEM_ROOT}/Documentation/Wiki/Model/ModelTutorial.md` which has been updated to use this.
- [#1809](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1809) The way library dependencies is handled has been modified; you may get link errors for some of the executables. The best is to mimic in your CMakeLists.txt the dependencies used in the example models in `ModelInstances` in MoReFEM library. In particular, make sure your model dependency uses up properly something as `$<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MAIN_LIBS}>` rather than just `${MOREFEM_MAIN_LIBS}`.
- [#1807](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1807) TransientSource in Lua files has been renamed: VectorialTransientSource or ScalarTransientSource.

# From v22.47 to v23.11.2

- [#1771](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1771) File::SubstituteEnvironmentVariables() has been removed, you may just delete the line (operation is automatically done in constructor now).
- [#1762](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1762) This is a complete overhaul to the class; check the issue for details! (for instance `ResetTimeAtPreviousTimeStep()` has been decommissioned and should be replaced by `DecreaseTime()`).
- [#1776](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1776) Rename  `VariationalFormulation` method `InitializeVectorSystemSolution` either `SetInitialSystemSolution()` or `ApplyInitialConditionToVector()` depending whether it is applied on the system solution or on any vector.
- [#1780](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1780) Replace `#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"` by `"Utilities/InputData/Extract.hpp"` and `Internal::InputDataNS::ExtractLeaf` by `InputDataNS::ExtractLeafFromSection`.
- [#1784](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1784) Replace `InputDataNS::Extract<>` by `InputDataNS::ExtractLeaf<>`.
- [#1787](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1787) Add __FILE__ and __LINE__ in all calls for `DirichletBoundaryConditionManager::GetDirichletBoundaryCondition()` and likewise functions.
- [#1755](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1755) Replace `Utilities::InputDataNS` by `InputDataNS`, `public Crtp::Section` by `public ::MoReFEM::Advanced::InputDataNS::Crtp::Section` and `public Crtp::InputData` by `public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf`.


# From v22.37 to v22.47

- [#1749](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1749) Add if relevant for your model path to Slepc library (and its include).

- [#1751](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1751) Modify the path to PETSc (and possibly Slepc) library in your precache files: a subdirectory `morefem_config` should be added (provided you're using the ThirdPartyCompilationFactory facility to install third party dependencies.) SO for instance `${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/lib` becomes `${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/lib/morefem_config`


# From v21.31 to v22.37

- [(#1566)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1566) XCode: modify the MoReFEM path in the workspace settings if you're using it.

- Change includes:

`Core/InputData/Instances/Parameter/Tools/Advanced/UsualDescription.hpp` -> `Core/InputData/Instances/Parameter/Internal/ParameterFields.hpp` [(#1724)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1724)

`Test/Tools/CompareEnsightFiles.hpp` -> `Test/Tools/CompareDataFiles.hpp` [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696)
`FixtureNS::Environment` -> `FixtureNS::TestEnvironment`  [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696) 
`Test/Tools/Fixture/Environment.hpp` -> `Test/Tools/Fixture/TestEnvironment.hpp` [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696) 


Remove `#include "Utilities/InputData/LuaFunction.hpp"` [(#1623)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1623)

- [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) In `InputData.hpp` for your model, explicitly make the enum class contains a `std::size_t`, e.g.:

````
enum class MeshIndex : std::size_t { mesh = 1 };
````

instead of:

````
enum class MeshIndex { mesh = 1 };
````


- Make the following find/replace:

`GetDomain(EnumUnderlyingType(DomainIndex::` -> `GetDomain(AsDomainId(DomainIndex::`  [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetUnknown(EnumUnderlyingType(UnknownIndex::` -> `GetUnknown(AsUnknownId(UnknownIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetUnknownPtr(EnumUnderlyingType(UnknownIndex::` -> `GetUnknownPtr(AsUnknownId(UnknownIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::` -> `GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetGodOfDof(EnumUnderlyingType(MeshIndex::` -> `GetGodOfDof(AsMeshId(MeshIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::` -> `GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetNumberingSubsetPtr(EnumUnderlyingType(NumberingSubsetIndex::` -> `GetNumberingSubsetPtr(AsNumberingSubsetId(NumberingSubsetIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetFiberList(EnumUnderlyingType(FiberIndex::`-> `GetFiberList(AsFiberListId(FiberIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetNonCstFiberList(EnumUnderlyingType(FiberIndex::`-> `GetNonCstFiberList(AsFiberListId(FiberIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
If you were using directly integers instead of enums (e.g. `GetNumberingSubset(1)`), this is no longer admissible and you must know use an enum.



`TestNS::CompareEnsightFile` -> `TestNS::CompareDataFiles<MeshNS::Format::Ensight>` [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696) 


- [(#1724)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1724) Initialization of `Parameter` from input data has changed: calls such as `  InitParameterFromInputData<TypeT>::template Perform<parameter_type>(...)` must now specifically tell whether it is a scalar, vectorial or matricial `Parameter`, e.g.:
`InitScalarParameterFromInputData<parameter_type>(...)`

You must explicitly do the `if constexpr` branching if the same line was both used for scalar and vectorial

(new call site a lighter but slightly less generic than previous one - but the previous one didn't work as intended in all cases...)                  

For the vectorial ones, the function to use is `Init3DCompoundParameterFromInputData` (`InitVectorialParameterFromInputData` also exists but do not use exactly the same interface; the new one is rather useful but is something that wasn't existing previously - you can safely bet the function fr your code that worked in MoReFEM v21.31 is the former). This requires the header `#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"`


- [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) In `main_ensight_output` files, use strong types for unique ids, e.g.

````
 std::vector<std::size_t> numbering_subset_id_list{ EnumUnderlyingType(NumberingSubsetIndex::monolithic) };
 ````

 becomes

 ````
std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId (NumberingSubsetIndex::monolithic) };
 ````

 and third argument of ` ModelNS::MainEnsightOutput` must be a `AsMeshId` (so something as `AsMeshId(MeshIndex::mesh)` works).


 - [(#1692)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1692)  Check upon the existence of directories on the filesystem has changed.

Now it should look like:

````
FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);
output_dir.DoExist();
````

- [(#1692)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1692) Several methods of `FileSystemNS::Directory` (such as `AddSubdirectory`  or even constructors) no longer take `invoking_file` and `invoking_line` as argument.

- [(#1705)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1705) Use `SuperLU_dist` in place of `Mumps` in Lua files.