A HPC finite element library developed for the implementation of the models of M3DISIM Inria team.

[TOC]

# Licence

MoReFEM is distributed under the LGPL v3.0-only license; the text of the license is provided in the `LICENSE` file.

# Installation

## Requirements

### Compilers

MoReFEM relies on modern C++ and required features shipped with  standard C++ 20 (only for a few of them at the time of this writing - April 2022 - due to lack of widespread support for some expected features). It is routinely tested with *very* recent versions of gcc, clang and AppleClang.

Some of MoReFEM third party dependencies also require a Fortran compiler.

You may find more about required compilers and how to install them on this [dedicated page](Documentation/Wiki/InstallingCompilers.md).


### Third-party libraries

MoReFEM relies on several third party libraries:

- An optimized Blas library. On macOS you may use the Accelerate framework; [Openblas](https://www.openblas.net) has also been successfully tested and is used on Linux OS.
- [Lua](http://www.lua.org/)
- [BoostTest](https://www.boost.org/doc/libs/1_79_0/libs/test/doc/html/index.html)
- [Openmpi](https://www.open-mpi.org/)
- [PT-Scotch](https://www.labri.fr/perso/pelegrin/scotch/) used to partition the meshes.
- [PETSc](https://petsc.org): The linear algebra library in charge of the large matrices and vectors. Most of the mpi heavy-lifting is in fact handled by this library. MUMPS, ScaLAPACK and SuperLU\_dist are installed through their Petsc embedded version.
- [TClap](http://tclap.sourceforge.net), a header-only library to handle command line arguments.
- [Eigen](https://eigen.tuxfamily.org), a linear algebra C++ library used to handle small matrices and vectors in MoReFEM.
- [Libmeshb](http://pages.saclay.inria.fr/loic.marechal/libMeshb.html), a mesh utility.

There is so far one optional dependency:

- [Slepc](https://slepc.upv.es): An eigen problem library, that relies heavily upon PETSc. If you want to activate it, you must:
  - Either set MOREFEM_WITH_SLEPC to True in the pre-cache CMake file (see below).
  - Or choose the command line option `--morefem_with_slepc=True` when calling `_configure_cmake.py` script.


The [ThirdPartyCompilationFactory](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory) project on gitlab provides smooth ways to install these dependencies: either by running directly a Python script or by using one of the given Docker images.


## MoReFEM compilation

MoReFEM is now compiled only through CMake: the XCode project which was there since the beginning was rather difficult to keep up-to-date, and there were painful issues when merging different branches that had modified it. It is however possible to generate a working XCode project through CMake (see [below](#generation-of-the-xcode-project)).


### CMake

To compile the code:

- Go in `cmake/PreCache` directory and have a look at the predefined files. You may either copy one and modify what you need in the copy directly, or use one of them directly if it suits your needs.
- Then go to the place into which you want to build your code (say a `build` directory at the root of MoReFEM library) and run the command (here for a single static MoReFEM library built for macOS with AppleClang compiler in debug mode):

````
mkdir build && cd build
python ../cmake/Scripts/configure_cmake.py --cache_file=*your precache file* --cmake_args="-G Ninja"  --install_directory=*your installation directory* --morefem_with_slepc=True
````


This command calls the adequate CMake command which will configure your project; it will especially create a PreCache file which will keep your actual settings and install it along with the library. The command above uses up Ninja build (which is said to be one of the most efficient - and so far I have no reason to doubt it) but you can specify another generator - if you skip entirely the `cmake_args` option above Makefiles will be used.

The option `morefem_with_slepc` is optional; if skip False is assumed.

To compile the project, proceed with your chosen generator, e.g. for the command above:

````
ninja -j 8
ninja install
````

Once done, you should first check the tests run correctly:

## Testing MoReFEM

From v18.16 onward, a CTest integration has been introduced. So once your code is compiled, you should run:

````
ctest
````

and both unit tests, integration tests and sanity checks for the basic models should be run. Those tests are designed to run in few minutes (with of course strong dependency of the compilation mode chosen).

### Generation of the XCode project

If you want to use XCode as IDE, some work has been done so that CMake may generate a usable project through:

````
mkdir XCode 
cd XCode
python ../cmake/Scripts/configure_cmake.py --cache_file=*your precache file* --cmake_args="-G Xcode"
```` 

If you do not want to be overwhelmed by the tests (there is one XCode scheme per test...) you may deactivate them in the project:

````
mkdir XCode 
cd XCode
python ../cmake/Scripts/configure_cmake.py --cache_file=*your precache file* --cmake_args="-G Xcode  -DMOREFEM_IGNORE_TESTS=True"
```` 

If this didn't work due to a message 

> error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

use the following commands to fix it (from [this GitHub issue](https://github.com/nodejs/node-gyp/issues/569)):

````
sudo xcode-select -s /Applications/Xcode.app/Contents/Developer
````

Please notice the generated XCode project may not display all the warnings: for some reason CMake XCode generator enables or disables some warnings regardless of the CMakeLists.txt content (see [this StackOverflow question](https://stackoverflow.com/questions/37550199/creating-xcode-project-from-cmake-contains-unwanted-compiler-flags) for more details). I have performed a work around to try to stay as close as possible to the expected behaviour (you make have a look at this [dedicated file](cmake/XCode.cmake) to see how I did it) but it is not entirely future-proof with future versions of XCode; if at some point a warning found in CI or command line wasn't found within XCode the command line should be trusted more (and the aforementioned file should be updated to mirror the command line behaviour).


# Docker images

Recently, we started releasing Docker images at each tag.

Currently they are located in a [sibling project](https://gitlab.inria.fr/MoReFEM/CoreLibrary/generatedockerimages/container_registry); they should be in registry of current project as soon as the functionality is available in Gitlab (ticket #1509).


# Using MoReFEM

## Levels of users

We have identified 4 levels of users:

- **Model user**: someone that takes an already implemented Model (e.g. the ones defined in `Sources/ModelInstances`, or some external ones in the [dedicated Model group on Gitlab](https://gitlab.inria.fr/MoReFEM/Models)) and provides their own Lua file to compute on their own data.
- **Model developer**: someone able to implement his/her own model, using for that purpose existent parameters and operators. All the Models defined in Sources/ModelInstances are at this level of expertise.
- **Advanced model developer**: Same as Model developer, but who also uses up more advanced features (that are in _Advanced_ namespace). Typically, an advanced model user might develop new operators tailored for its own purpose, or even new finite elements.
- **Library developer**: someone that writes code involved in low level operations of the library (typically in namespace _Internal_).

If you intend to be a contributor, make sure to run first:

````
python Scripts/init_morefem.py
````

which will set things up such as providing minimal git configuration (if no gitconfig file present on your account) or providing the XCode templates for new files if you're working on macOS.


## Tutorial

Most users so far belong to the *Model developer* category; a lengthy [tutorial](Documentation/Wiki/Model/model_tutorial.md) has been devised to present how to construct a model which solves an Elasticity problem.


## Structure of the source code

MoReFEM is a library made from several modules; it may be compiled either as a single library (if BUILD_MOREFEM_UNIQUE_LIBRARY is set to True) or as 10 different libraries with a hierarchy (the exact structure of the library and its module is detailed in a [dedicated page](Documentation/Wiki/CodeStructure.md)).

Upon build, ancillary libraries (for post-processing for instance) and executables (test and basic models) are also constructed; more information are provided on the [aforementioned page]((Documentation/Wiki/CodeStructure.md)).


# How to contribute

As explained [earlier](#levels-of-users), most of the time you shouldn't have to contribute directly to the library and should be able to define locally in your own model what you need. 

However, if you have some fixes to provide or maybe something to share (for instance if you're an advanced model developer and have written a new operator that may be of interest for other models), feel free to submit it through a [merge request](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/merge_requests/new) on our gitlab.

Please check your branch passes correctly the continuous integration before issuing the merge request.

MoReFEM follows the [integration manager](https://git-scm.com/book/it/v2/Distributed-Git-Distributed-Workflows) workflow: merge requests are reviewed by the integration manager and integrated in the subsequent release if accepted.

# Documentation

## Doxygen

Doxygen documentation is comprehensive and up-to-date. For more information, see the dedicated [README.md](Documentation/Doxygen/README.md).

Version generated at the last MoreFEM tag is available at [this address](https://morefem.gitlabpages.inria.fr/CoreLibrary/MoReFEM).

## Introduction talks

Some talks done to present the library are available in a [dedicated Gitlab project](https://gitlab.inria.fr/MoReFEM/CoreLibrary/introductiontalks).


## Wiki

In this directory a wiki is in progress to serve as a guide about specific concepts used in MoReFEM. For each of them, there is an indication which level of user is concerned.

Among the already existing files, it is worth mentioning again the [model tutorial](Documentation/Wiki/Model/model_tutorial.md)) that gives steps to build from scratch a working elastic model.


## Coding standards

Coding standards are provided in _Documentation/CodingStandards_. They are mostly inspired by those of [Verdandi](verdandi.sourceforge.net).


# Getting help

The best if you have a specific problem is to simply [open an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues) about it.

You may also contact engineers currently involved (part-time) on the project, both of which are *library developer* 

- [Sébastien Gilles](mailto:sebastien.gilles@inria.fr) - integration manager 
- [Jérôme Diaz](mailto:jerome.diaz@inria.fr) - contributor to several models and to the library

If you're part of M3DISIM team, please ask your team leader or Jérôme to add you in the internal Mattermost channel dedicated to the project.

