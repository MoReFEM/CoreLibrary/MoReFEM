# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

# This script is assumed to be called within continous integration, just after a call to CMake to generate a file
# named 'compile_commands.json'.

import os
import time
import subprocess


if __name__ == "__main__":
    
    project_root_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..")
    json_file = os.path.join(project_root_dir, "build", "compile_commands.json")
    
    cmd = \
        [
            "cppcheck",
            "-j",
            "4",
            "--enable=all",
            "-v",
            "--language=c++",
            "--platform=unix64",
            "--xml",
            "--xml-version=2", 
            "--project={}".format(json_file), 
            ]
        

    FILE=open('morefem-cppcheck.xml', 'w')

    start = time.time()

    subprocess.Popen(cmd, shell=False, stdout=None, stderr=FILE).communicate()
    end = time.time()
    
    print("Output written in morefem-cppcheck.xml; analysis performed in {} seconds".format(round(end - start, 1)))