# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import subprocess



if __name__ == "__main__":

    morefem_source_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..", "Sources")

    cmd = \
        [
            "rats",
            "--quiet",
            "--xml",
            "-w 3",
            morefem_source_dir
            ]
        

    xml_output = open('morefem-rats.xml', 'w')

    subprocess.Popen(cmd, shell=False, stdout=xml_output, stderr=xml_output).communicate()

    print("Output written in morefem-rats.xml")