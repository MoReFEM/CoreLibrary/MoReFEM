# create the sonarqube config file

if [ "$CI_PROJECT_NAMESPACE" = "MoReFEM/CoreLibrary" ]; then
  export SONARQUBE_PROJECT="m3disim:morefem:check_sonar_server_connection"
else
  namespace=`echo "$CI_PROJECT_NAMESPACE" | awk '{print tolower($0)}'`
  export SONARQUBE_PROJECT="$namespace:morefem:check_sonar_server_connection"
fi


cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.inria.fr/sonarqube
sonar.login=38a5102208b9b34fb90fdc89358289239488da30

sonar.links.homepage=$CI_PROJECT_URL
sonar.links.scm=$CI_REPOSITORY_URL
sonar.scm.disabled=true
sonar.scm.provider=git

sonar.projectKey=$SONARQUBE_PROJECT
sonar.projectDescription=Hack to ensure connection is allright before running the full scanner
sonar.projectVersion=0.1

sonar.sources=cmake/PreCache
sonar.sourceEncoding=UTF-8

EOF

echo "sonar-project.properties file for quick sonar server connection check written."
exit
# run sonar analysis
STARTTIME=$(date +%s)
sonar-scanner -X 2>&1 | tee sonar_check_connectivity.log
ENDTIME=$(date +%s)

failure_found=`grep "EXECUTION FAILURE" sonar_check_connectivity.log | wc -l`

if [ ${failure_found} -ge 1 ]; then
  echo "Execution failure for Sonar scanner."
  exit 1
fi

success_found=`grep "EXECUTION SUCCESS" sonar_check_connectivity.log | wc -l`

if [ ${success_found} -ne 1 ]; then
  echo "Please check the sonar scanner log: no success was reported (or the way it is reported has been changed...)."
  exit 1
else
  echo "Connexion to Sonar scanner seems alright."
fi
