# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os

from run_valgrind_tools import MoReFEMRootDir, RunValgrind

if __name__ == "__main__":

    morefem_root_dir = MoReFEMRootDir()
    morefem_model_instances_dir = os.path.join(morefem_root_dir, "Sources", "ModelInstances")

    lua_file = os.path.join(morefem_model_instances_dir, "Stokes", "demo.lua")

    RunValgrind("Sources/MoReFEM4Stokes",  lua_file, os.path.join(morefem_root_dir, "memcheck_stokes.txt"))
