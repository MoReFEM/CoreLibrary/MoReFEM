# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os

from run_verrou_tools import MoReFEMRootDir, RunVerrou

if __name__ == "__main__":

    morefem_root_dir = MoReFEMRootDir()
    lua_file = os.path.join(morefem_root_dir, "Sources", "Test", "ThirdParty", "PETSc", "MatrixOperations", "demo.lua")

    RunVerrou("Sources/MoReFEMTestPetscMatrixOperations",  lua_file, os.path.join(morefem_root_dir, "verrou_test_matrix_operations.txt"), is_model = False)
