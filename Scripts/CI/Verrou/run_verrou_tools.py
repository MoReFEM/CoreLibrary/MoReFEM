# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import pathlib
import sys
import shutil
import subprocess


def MoReFEMRootDir():
    """Returns the path to the root dir of MoReFEM.
    
    This uses up the fact the directory in which present script is stored is known.
    """
    
    return pathlib.Path(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..").resolve()
    

class RunVerrou:
    """Compile the sources related to one executable and then run the Verrou analysis on the executable.
    
    @param executable Executable to compile (e.g. MoReFEM4Elasticity).
    @param lua_file Lua file with the data required to run the model.
    @param output File in which output of the Valgrind analysis is written.
    @param is_model True if a model is involved, False otherwise.
    """

    def __init__(self, executable, lua_file, output, is_model = True):
        self.__executable = executable
        self.__lua_file = lua_file
        self.__output = output
        self.__morefem_root_dir = MoReFEMRootDir()
        self.__is_model = is_model
        
        try:
            self._callCMake()        
            self._compile()
            self._RunVerrou()
        except subprocess.CalledProcessError as e:
            print("Error in process: {}".format(e))
            sys.exit(e.returncode)
        
        
    def _callCMake(self):
                
        morefem_cmake_dir = os.path.join(self.__morefem_root_dir, "cmake")
        
        cmd = ("python3",
               f"{morefem_cmake_dir}/Scripts/configure_cmake.py",
               f"--cache_file={morefem_cmake_dir}/PreCache/linux.cmake",
               f'--cmake_args=-G Ninja',
               "--third_party_directory=/opt",
               '--mode=debug')
        
        subprocess.run(cmd, shell = False).check_returncode()


    def _compile(self):
        cmd = ("ninja", self.__executable)
        subprocess.run(cmd, shell = False).check_returncode()
        
    
    def _RunVerrou(self):     

        os.environ["MOREFEM_ROOT"] = str(self.__morefem_root_dir)

        cmd = ["/opt/bin/valgrind",
               "--error-exitcode=1",               
               f"--log-file={self.__output}",
               "--tool=verrou",
               "--rounding-mode=random",
               self.__executable,
               "-i",
               self.__lua_file,               
               "-e",
               'MOREFEM_RESULT_DIR=/home/non_root_user/MoReFEMOutputs/Results',
               "-e",
               'MOREFEM_PREPARTITIONED_DATA_DIR=/home/non_root_user/MoReFEMOutputs/Prepartitioned']

        if self.__is_model:
            cmd.append("--overwrite_directory")

        subprocess.run(cmd, shell = False).check_returncode()

        print(f"Log of Verrou run is in {self.__output}")
