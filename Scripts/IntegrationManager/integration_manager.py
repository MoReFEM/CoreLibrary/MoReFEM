# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import copy
import re
import numpy as np
import gitlab
import pydriller
from contextlib import suppress

def extract_issue_ids(str):
    """
    Find all the ticket numbers found in @str

    @str String in which the ticket numbers are sought

    @return The list of all ticket numbers found
    """
    trailing_chars =  r" :\-_"

    prog = re.compile(f"#[0-9]+[{trailing_chars}]+")
    result = prog.findall(str)

    if not result:
        return None

    return [int(item[1:].rstrip(trailing_chars)) for item in result]


def sort_unique_and_remove_zero_in_id_list(mylist):
    """
    Take the input list and:

    - Remove duplicate
    - Remove 0 if found
    - Sort it

    If @list is None, return None

    @return The list with all three steps used.
    """
    if not mylist:
        return None

    issue_id_list = list(set(mylist))
    issue_id_list.sort()

    # see https://stackoverflow.com/questions/4915920/how-to-delete-an-item-in-a-list-if-it-exists
    with suppress(ValueError, AttributeError):
        issue_id_list.remove(0)

    return issue_id_list

class integration_manager_script(object):

    def __init__(self, private_token, latest_tag, new_tag, url = "https://gitlab.inria.fr", project_id = 5350):
        """
        Check the next tag is properly self consistent: issues mentioned as added On integration manager branch should
        appear in the commits of the release, and no todo related to them should remain in the codebase.

        The point here is to automate some of the hassle of all these checks; however the ambition is not
        to completely bypass the integration manager who has the task to decide what to do with the warnings
        that may be emitted by the current object.

        @url URL of the gitlab instance used.
        @latest_tag Name of the tag of the previous release (something like "v23.11.2").
        @new_tag Name of the tag to be issued. Is used if Sonarqube is run afterwards; put something with "rc" preffix 
        if release is not imminent.
        @project_id Id of MoReFEM in the Gitlab instance.
        @private_token A _user_ private token.
        """

        # We assume current script is in ${MOREFEM_ROOT}/Scripts/IntegrationManager
        self.__morefem_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", ".."))        
        assert(os.path.exists(os.path.join(self.__morefem_root, "Changelog.md")))


        self.__private_token = private_token
        gl = gitlab.Gitlab(url=url, private_token=private_token)
        gl.auth()
        self.__gitlab_project = gl.projects.get(project_id)
        self.__latest_tag = latest_tag

        # Write the tag in Sonarqube analysus file.
        with open(os.path.join(self.__morefem_root, "ExternalTools", "Sonarqube", "Tag.txt"), "w") as f:
            f.write(new_tag)
                  


    def get_tagged_issues(self, label_list=['On integration manager branch']):
        """
        Query Gitlab instance to extract all the issues that are both opened and with the label
        "On integration manager branch".

        @label_list Filter in all the issues that have at least one of these labels.

        @return The list of issues as prescribed by the Python gitlab module
        """
        tagged_issues = self.__gitlab_project.issues.list(state='opened', labels=label_list, get_all=True)
        return tagged_issues


    def get_tagged_issues_ids(self, label_list=['On integration manager branch']):
        """
        Extract the project ids from the result of _extract_issues_on_develop_branch_.

        @label_list Filter in all the issues that have at least one of these labels.

        @return The list of the ids found by the Python gitlab module with the tag "On integration manager branch".
        """
        ret = [issue.iid for issue in self.get_tagged_issues(label_list)]
        ret.sort()
        return ret


    def print_for_changelog(self):
        """
        List all the tickets found by _extract_issues_on_develop_branch_ and prepare a draft that can be
        used to write the entries in Changelog.md

        Some workk remains to be done:
        - Add the proper qualifier ("Documentation", "Test", "Feature", etc...)
        - Sort the entries in the sublibraries of MoReFEM
        """
        tagged_issues = self.get_tagged_issues()

        print("==================================")
        print("== Draft for Changelog entries ")
        print("==================================\n")

        for issue in tagged_issues:
            print(f"- #{issue.iid}: {issue.title}.")


    def extract_issue_ids_from_commits(self):
        """
        Skim through all the commits since latest release and extract from the beginning of their messages all the reference to Gitlab issues ('#' followed by a number). 

        #0 are excluded on purpose.""

        @return All the issue numbers found in the commit messages (save 0)
        """
        issue_id_list = []

        latest_release_commit = self.__gitlab_project.tags.get(self.__latest_tag).commit["id"]

        for commit in pydriller.Repository(self.__morefem_root, from_commit=latest_release_commit).traverse_commits():
            msg = commit.msg

            # Remove all that is after the first alpha character found
            msg = msg[:msg.find(next(filter(str.isalpha, msg)))]

            issue_ids_from_commit = extract_issue_ids(msg)

            if issue_ids_from_commit:
                issue_id_list.extend(issue_ids_from_commit)

        return sort_unique_and_remove_zero_in_id_list(issue_id_list)
    

    def extract_issue_ids_from_changelog(self):
        """
        Extract from the Changelog.md file all the issue ids referenced before self.__latest_tag
        is found. 

        This assumes that the Changelog.md was properly filled in previous release; if not
        an exception is thrown.

        @return List of all issues ids references in the Changelog (save 0)
        """
        
        issue_id_list = []

        latest_tag = self.__latest_tag

        with open(os.path.join(self.__morefem_root, "Changelog.md")) as f:

            found = False

            while not found:
                line = f.readline()

                if latest_tag in line:
                    found = True

                ids_from_current_line = extract_issue_ids(line)

                if ids_from_current_line:
                    issue_id_list.extend(ids_from_current_line)

        if not found:
            raise Exception(f"Tag {latest_tag} not found in Changelog...")

        return sort_unique_and_remove_zero_in_id_list(issue_id_list)
    
    def find_issue_ids_in_files(self, issue_id_list):
        """
        Skim through all files within `Sources` subdirectory and check there are no instance
        of #iid inside where iid is an issue number that is in @issue_id_list

        @issue_id_list List of issues ids that we don't wan't to find in the files

        @return List of issues_ids that were found somewhere in the subdirectories.

        Current script is just an helper tool and is not expected to fix it itself; that's why
        the location where the issue ids were found is not kept (it is assumed an IDE or a command )
        such as `grep` in command line will be used to retrieve those instantly.

        It should be noticed there might be false positive (reference we want to keep in the code) - 
        current script is an helper for the integration manager, not something intended to work 
        directly in a CI pipeline.
        """
        sources_dir = os.path.join(self.__morefem_root, "Sources")

        issue_id_found_in_sources = []

        ignore_list = (".DS_Store", "Changelog.md")


        for issue_id in issue_id_list:

            pattern = f"#{issue_id}"

            for root, dirs, files in os.walk(sources_dir):

                found = False

                for myfile in files:

                    if myfile in ignore_list:
                        continue

                    extension = os.path.splitext(myfile)[1]

                    if extension not in (".cpp", ".hpp", ".hxx", ".md", ".py", ".txt", ".cmake", ".lua"):
                        continue

                    with open(os.path.join(root, myfile)) as f:

                        for line in f:
                            if pattern in line:
                                found = True
                                break

                if found:
                    issue_id_found_in_sources.append(issue_id)
                    break
        
        return issue_id_found_in_sources
    

    def check_ready_for_release(self):
        """
        Check all the issues are properly accounted for everywhere.
        """
        issues_from_gitlab = self.get_tagged_issues()
        issues_ids_from_gitlab = [issue.iid for issue in issues_from_gitlab]
        issues_ids_from_changelog = self.extract_issue_ids_from_changelog()
        issues_ids_from_commits = self.extract_issue_ids_from_commits()

        # Check all ids are properly referenced in Changelog
        not_in_changelog = np.setdiff1d(issues_ids_from_gitlab, issues_ids_from_changelog)
        
        if not_in_changelog.any():
            print("==== WARNING: there are some issues marked as On integration manager branch that are not in Changelog.md.")
            print("Suggestion: complete following content: ")

            for issue in issues_from_gitlab:
                if issue.iid in not_in_changelog:
                    print(f"- #{issue.iid}: {issue.title}.")

        # Check all ids from commits are related to an issue in Gitlab.
        # Might not be the case if ticket is not marked as to be close, but integration manager should
        # in this case properly mark the issue as ongoing issue in Changelog.
        not_in_gitlab = np.setdiff1d(issues_ids_from_commits, issues_ids_from_gitlab)
        if not_in_gitlab.any():
            print(f"\n==== WARNING: there are commits that mention issues {not_in_gitlab} that are not tagged as "
                  "'On integration manager branch' on Gitlab. It doesn't mean there is a problem - but you should have a look.")
            
        not_in_commits = np.setdiff1d(issues_ids_from_gitlab, issues_ids_from_commits)
        if not_in_commits.any():
            print(f"\n==== WARNING: there seemed to be no commits related to issues {not_in_commits} whereas these "
                  "were marked as 'On integration manager branch'. Please check - it may mean a branch was not properly merged "
                  "in develop and the issue related development might not be properly integrated On integration manager branch.")

        # Check there are no undue #iid references in the Source files
        # (some might be legit in which case just ignore them)
        all_issues_ids = copy.copy(issues_ids_from_gitlab)
        all_issues_ids.extend(issues_ids_from_changelog)
        all_issues_ids.extend(issues_ids_from_commits)
        all_issues_ids = sort_unique_and_remove_zero_in_id_list(all_issues_ids)

        issues_ids_in_sources_dir = self.find_issue_ids_in_files(all_issues_ids)

        if issues_ids_in_sources_dir:
            print(f"\n==== WARNING The following issues ids were found in files in Sources subdirectory: {issues_ids_in_sources_dir}")


