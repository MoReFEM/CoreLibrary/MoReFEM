# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

#!/usr/bin/python

# To make it work, create in .git/hooks a symbolic link named 'commit-msg' that points to this file, and modify the first line to match the path of your Python command.

import sys
import subprocess


class InvalidBranchName(Exception):

    def __init__(self):
        self.expr = "[POLICY] Invalid branch name: branch should be a number followed by a '_' and then whatever you wish."

    def __str__(self):
        return self.expr
        
class InvalidMessage(Exception):

    def __init__(self):
        self.expr = "[POLICY] Either your commit message should start with # followed by the ticket number AND a space, or by something entirely different (in which case the ticket number is added automatically). You can't start with a # followed by anything else than the ticket number."

    def __str__(self):
        return self.expr        
        

def ProcessCommit(special_branch_names):
    '''Process the commit if the branch name is valid.
    
    Valid branches should be of the format: *index*_*whatever name you want*
    
    \\param special_branch_names Branch names that do not comply with the convention but should be accepted nonetheless. e.g. 'main'.
    '''
    if len(sys.argv) < 2:
        raise Exception("Missing argument! Format should be '*this Python script* *file with commit message*'.")
    
    commit_file = sys.argv[1]

    # Extract the name of the branch.
    branch_name = str(subprocess.Popen("git symbolic-ref HEAD", shell = True, stdout=subprocess.PIPE).communicate()[0].decode('UTF-8').strip()).split('/')[-1]
    
    if branch_name in special_branch_names:
        return
        
    # Extract the number of the ticket from the branch name.
    # The expected format is *n*_*whatever name you wish* where n is the number.
    splitted = branch_name.split('_')
    
    is_invalid_branch_name = False
    
    if len(splitted) == 1:
        is_invalid_branch_name = True

    try:
        index = int(splitted[0])
    except:
        is_invalid_branch_name = True

    # So now check whether the message in the file began with #*n*, and if not add it.
    with open(commit_file) as FILE:
        content = FILE.read()
        content = content.lstrip("\t\n ")

    # If first character is a '#', keep the message intact, and issue a warning if the ticket number is not on par with the branch name.
    if content.startswith('#'):
        str_index_in_content = content[1:].split(" ")[0]
        try:
            index_in_content = int(str_index_in_content)
        except:  
            raise InvalidMessage()
            
        if not is_invalid_branch_name and index_in_content != index:
            print("[WARNING] The ticket number given (namely {0}) is not the same as the one suggested by the branch name (namely {1})).".format(index_in_content, index))
        
        return
    elif is_invalid_branch_name:
        raise InvalidBranchName()
        
        
    # Add to the commit message the ticket number.
    with open(commit_file, 'w') as FILE:
        FILE.write("#{0} ".format(index))
        FILE.write(content)


if __name__ == "__main__":

    try:
        ProcessCommit(special_branch_names = ('main', 'release-candidate'))
         
    except Exception as e:
        print("{0}".format(e))
        
        # git convention when the script fails.
        sys.exit(1)
        



