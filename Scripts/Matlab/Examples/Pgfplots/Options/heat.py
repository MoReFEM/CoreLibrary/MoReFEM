# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #


#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 

############ Global Options to change the 1D case
print("Creating Heat") # Message to tell what you are doing
project_name = "Example/Heat" # Path of the folder in which will be created the latex files.

# num_time_step : Number of time step wanted, might be less than the real number, to combine with increment_time_step.
# increment_time_step : enables to pick every n time step instead of every time step with 1. Total of time step = num_time_step*increment_time_step
# Number of files should be num_time_step*increment_time_step + 1 because of the initial condition so the last time step is dealt with at the end.
num_time_step = 20
increment_time_step = 1

# If using an _ in the title use \_ to avoid errors in Latex.
title = "Heat"
xmin = 0.0
xmax = 10.0
manual_y_bounds = False; # or True 
ymin = 20.
ymax = 20.
xtick = "{0,2,...,100}"
ytick = "{-100,-1,0,1,...,100}"
x_init = 0.0
x_final = 10.0 
dx = 0.5

########## Options for every plot, to add a new plot just copy and paste the 6 lines below
 
# Plot 1
dir_hh.append('~/Codes/MoReFEM/Scripts/Matlab/Examples/Plot1D/Results/Mesh_1/NumberingSubset_1/') # the / is important at the end
name_data_output.append("solution") # name of the output of matlab script
color.append("myblueT") # colors are defined in output_plot.tex
legend.append("Heat")
extension.append("hhdata") # If using MoReFEM results always hhdata.
name_input.append("solution") # If using MoReFEM results always solution.
