function [Mat_out, indices_out, Mat_in] = MoReFEMMatrixRenumbering(path_hh, mesh_number, numbering_subset_number, matrix_in_name_file, dimension)

Mat_in = LoadMatrixIn(matrix_in_name_file);

[interfaces_indices, dof_infos_indices] = LoadInterfacesAndDofsInfosMoReFEM(path_hh, mesh_number, numbering_subset_number);

indices_out = CreateIndicesOutMoReFEMToMatlab(interfaces_indices, dof_infos_indices, dimension);

Mat_out = MatrixRenumbering(Mat_in, indices_out);

end