    [[CLASS_NAME]]::[[CLASS_NAME]](morefem_data_type& morefem_data)
    : parent(morefem_data)
    { }

    
    void [[CLASS_NAME]]::SupplInitialize()
    {
        // TODO: Fill the content with whatever initialization is required by the problem.
        // What is already done in base method Initialize() (and therefore must not be repeated here) is:           
        // - Creation of the meshes.
        // - Definitions of the GodOfDof
        // What typically might be added is initialization of variational formulation(s), print on screen,
        // run of the static case, ...
    }


    void [[CLASS_NAME]]::Forward()
    {
        // TODO: Define the forward operations. In case of a variational problem; it probably means delegating 
        // work to an underlying variational formulation object.
    }


    void [[CLASS_NAME]]::SupplFinalizeStep()
    {
        // TODO: Put there the steps to perform at the end of each time step.
        // Base class FinalizeStep() just update the time; it does so AFTER the call to current method
        // (so in current method you can still rely upon time information related to the time step
        // being finalized).
    }
    


    void [[CLASS_NAME]]::SupplFinalize()
    { 
        // TODO: Put there what to do when all the time steps are done
    }

