    inline const std::string& [[CLASS_NAME]]::ClassName()
    {
        static std::string name("[[CLASS_NAME]]");
        return name;
    }


    inline bool [[CLASS_NAME]]::SupplHasFinishedConditions() const
    {
        // TODO: Put here any additional conditions that may trigger the end of the dynamic run.
        // If there is one, return true.
        // If there is none (i.e. the sole ending condition is to reach last time step) simply return false.
        return false; // ie no additional condition
    }


    inline void [[CLASS_NAME]]::SupplInitializeStep()
    {
        // Base class InitializeStep() update the time for next iteration and print it on screen. If you
        // need additional operations, this is the place for them.
    }
