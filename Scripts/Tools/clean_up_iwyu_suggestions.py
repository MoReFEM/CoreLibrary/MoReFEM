# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import sys
import re
import argparse
import functools


class CleanUpIWYUSuggestion():
    """Reorder and clean up suggestions by IWYU for a given file.

    The expected input file should be directly lifted from its output and look like:

    The full include-list for myfile.cpp: // this line is expected
    #include "Utilities/fileA.hpp"
    #include <algorithm>
    #include <array>
    #include <cassert>
    #include <type_traits>
    #include "Geometry/fileBB.hpp"
    ...
    namespace MoReFEM::Advanced::LocalVariationalOperatorNS { class InformationsAtQuadraturePoint; }
    --- // this line is expected as well

    The purpose of this file is to clean it up according to MoReFEM quirks and conventions:

    - Remove the hxx file (if any). This file is to be called at the bottom of the hpp file, not at its beginning.
    - Use the C++ versions of C header. I have configured IWYU to substitute <math.h> by <cmath>, but it doesn't do it all the time...
    - Sort the MoReFEM headers in specific groups ('Utilities', 'ThirdParty', 'Core', etc...) separated each by a space.
    - Add the dedicated block comment before and after forward declarations.

    The script has really a mostly internal purpose and has not been written to be as robust as possible - this is the reason the input file expected format is very strict.
    """

    def __init__(self, input_file):
        
        self.__input_file = input_file
        self.__content = []
       
        with open(input_file) as FILE_in:

            self.__CheckFirstLine(FILE_in.readline())    
            self.__content = [ line.strip() for line in FILE_in ] 
            self.__CheckLastLine()

        self.__Reorganize()


    def __CheckFirstLine(self, line):
        expected_first_sentence = "The full include-list for"

        if not line.startswith(expected_first_sentence):

            msg = f"Input file '{self.__input_file}' is expected to start with a line telling " \
                 f"'{expected_first_sentence}' whereas first line found was: '{line.rstrip()}'."

            raise Exception(msg)

        self.__filename = line.split(expected_first_sentence)[1][:-2]


    def __CheckLastLine(self):
        expected_last_line = "---"

        line = self.__content[-1]

        if line != expected_last_line:
            msg = f"Input file '{self.__input_file}' is expected to end with a line ' " \
                 f"'{expected_last_line}' whereas last line found was: '{line}'."

            raise Exception(msg)

        self.__content = self.__content[:-1]        

    def __Reorganize(self):

        self.__stl_include = []
        self.__morefem_include = []
        self.__forward_decl = []

        for line in self.__content:

            assert(line)

            if line.startswith("#include <"):
                self.__stl_include.append(line)
            elif line.startswith('#include "'):
                self.__morefem_include.append(line)
            elif line.startswith('namespace'):
                self.__forward_decl.append(line)
            else:
                msg = f"Couldn't identify the nature of line '{line}'"
                raise Exception(msg)
        
        self.__SortSTLIncludes()
        self.__SortMoReFEMIncludes()
        self.__ForwardDeclaration()
      
    def __SortMoReFEMIncludes(self):

        self.__morefem_group_list = ("Utilities", "ThirdParty", "Core", "Geometry", "FiniteElement", "Operators", "Parameters", "OperatorInstances", "ParameterInstances", "FormulationSolver", "Model", "ModelInstances", "PostProcessing", "Test")
        
        self.__morefem_include_per_group = self.__GenerateMoReFEMGroupDict()


    def __GenerateMoReFEMGroupDict(self):
        """Group all the internal MoReFEM includes by group"""
        include_per_group_dict = { }
        morefem_group_list = self.__morefem_group_list

        for group in morefem_group_list:
            include_per_group_dict[group] = []
        
        for include_ in self.__morefem_include:
            if ".hxx" in include_ or "Utilities/Warnings/Internal/IgnoreWarning" in include_:
                continue
            
            group = self.__ExtractMoReFEMGroupName(include_)
            if group not in morefem_group_list:
                raise Exception(f"{group} not found in the potential values for groups, while interpreting {include_}. If a new library was read, the reason is likely that IncludeWhatYouUse .imp files were not properly updated; please add one to tell which header from IncludeWithoutWarning should be used instead.")            

            include_per_group_dict[group].append(include_)

        for group,include_list in include_per_group_dict.items():
            include_list.sort()

        return include_per_group_dict


    def __ExtractMoReFEMGroupName(self, include):
        
        match = re.findall('"(.*)"', include)                    
        assert(len(match) == 1)

        return match[0].split('/')[0]

    def __SortSTLIncludes(self):
        
        self.__stl_include = [self.__CppStyleInclude(item) for item in self.__stl_include]
        self.__stl_include.sort() # clang-format would do it but it's cheap to do here as well...
        

    def __CppStyleInclude(self, text):
        
        match = re.findall("<(.*)>", text)            
        assert(len(match) == 1)

        content = match[0]

        if content.endswith(".h"):
            return f"#include <c{content[:-2]}>"
        else:
            return text

    def __ForwardDeclaration(self):
        self.__forward_decl.sort()


    def Print(self):

        print("====================================")
        print(f"Headers for {self.__filename}:")
        print("====================================\n")

        print("\n".join(self.__stl_include))
        
        print("")
        self.__PrintMoReFEMIncl()

        print("")
        self.__PrintFwdDecl()
       
    def __PrintMoReFEMIncl(self):
        
        morefem_group_list = self.__morefem_group_list        
        morefem_include_per_group = self.__morefem_include_per_group
        
        for group in morefem_group_list:
            include_list = morefem_include_per_group[group]
            if include_list:
                print("\n".join(include_list))
                print("")


    def __PrintFwdDecl(self):

        if self.__forward_decl:
            print("// ============================")
            print("// clang-format off")
            print("//! \\cond IGNORE_BLOCK_IN_DOXYGEN")
            print("// Forward declarations.")
            print("// ============================")
            print("")
            print("\n".join(self.__forward_decl))
            print("")
            print("// ============================")
            print("// End of forward declarations.")
            print("//! \\endcond IGNORE_BLOCK_IN_DOXYGEN")
            print("// clang-format on")
            print("// ============================")


       
            
class ParseLog():
    """Parse the command line and call CleanUpIWYUSuggestion.
    """
    
    def __init__(self, args):
        parser = argparse.ArgumentParser(
            description='Reorganize properly the suggestions of includes and forward declaration issued by IWYU', \
            formatter_class=argparse.RawDescriptionHelpFormatter)
            
        parser.add_argument(
            '--input',
            required=True,
            help= 'File which contents the IWYU suggestion.'
        )
        
        parsed_args = parser.parse_args(args)
        
        iwyu = CleanUpIWYUSuggestion(parsed_args.input)

        iwyu.Print()
            


if __name__ == "__main__":
    ParseLog(sys.argv[1:])
