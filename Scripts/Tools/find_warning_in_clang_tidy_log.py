# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import sys
import re
import argparse
from collections import Counter


class FindWarningInClangTidyLog():
    """Interpret the clang tidy log and count how many warnings were found and which ones.

    We distinguish two levels of warnings:

    - Warnings - when there are some of them, the CI job should be in orange (i.e. failure but failure is allowed in CI)
    - Code smells - these warnings are deemed useful, but they can be fixed later on (typically 
    'readability-function-cognitive-complexity'). The idea is that they do not make the dedicated CI job fail but 
    they still appear in Sonarqube code smells.
    """    

    CODE_SMELL_LIST = ("readability-function-cognitive-complexity", )
    REGEX_IDENTIFY_TIDY_DIAGNOSTIC= re.compile(r'warning:.+\[(.+?)\]')        
    REGEX_EXTRACT_BRACE_CONTENT = re.compile(r'\[(.+?)\]')

    def __init__(self, log):
        self.__CreateWarningList(log)
        self.__Filter()


    def __Filter(self):
        """
        Sift through all warnings found and separate them in:

        - Warnings
        - Code smells.

        The difference is that any warning will indicate in CI the job as failed (but failure is allowed for 
        clang-tidy job) whereas codesmell won't; the code smell will only appear in Sonarqube diagnostic.

        This was made for checks such as readability-function-cognitive-complexity: it is nice to be informed of
        that but it's not enough by itself to make a fix immediate.

        List of warnings that are tag as code smells is in CODE_SMELL_LIST static attribute.
        """
        self._warning_list = Counter((item for item in self.__all_warnings_found if item not in self.CODE_SMELL_LIST))
        self._code_smell_list = Counter((item for item in self.__all_warnings_found if item in self.CODE_SMELL_LIST))


    def Nwarning(self):
        """Return the total number of warnings found."""
        return self._warning_list.total()
        
    def Ncode_smell(self):
        """Return the total number of code smells found."""
        return self._code_smell_list.total()
    
    def PrintCodeSmellList(self):
        """Print all code smells found, sort from the most occurrences to the least occurrences."""

        print("{} code smells were found (code smells will appear in Sonarqube analysis but don't make the clang-tidy job fail)".format(self.Ncode_smell()))    

        for (k,v) in self._code_smell_list.most_common():
            print("\t{} : {}".format(k, v))


    def PrintWarningsList(self):
        """Print all warnings found (code smells excluded), sort from the most occurrences to the least occurrences."""

        print("{} warnings were found - they should be corrected or neutralized before the branch is submitted for integration.".format(self.Nwarning()))    

        for (k,v) in self._warning_list.most_common():
            print("\t{} : {}".format(k, v))

    def __CreateWarningList(self, log):
        """
        Go through the 'log'file and extract all the warnings found (that fit the regular expression).

        We collect every one of them at this stage; sorting will occur later.
        """
        
        self.__all_warnings_found = []
        
        with open(log) as FILE_in:
    
            for line in FILE_in:
                result = self.__FindWarningInLine(line)
            
                if result:                
                    self.__all_warnings_found.append(result)


    def __FindWarningInLine(self, line):
        """Look in 'line' to find the regular expression given by 'reg_obj'.
    
        @param[in] line Line under investigation.
    
        Return: None if no warning was found, or the matched pattern if it was (e.g 'bugprone-unused-raii').
        """
        try:
            selected_line = self.REGEX_IDENTIFY_TIDY_DIAGNOSTIC.search(line).group(0)

            # Trick to avoid false selection of C++ code such as [[nodiscard]]
            selected_line = selected_line.replace("[[", "")

            potential_return = self.REGEX_EXTRACT_BRACE_CONTENT.search(selected_line).group(0)[1:-1] # to remove [ and ]

            if potential_return.isdigit():
                return None
            
            return potential_return

        except AttributeError:
            return None
            
       
            
class ParseLog():
    """Just a facility to call FindWarningInClangTidyLog from command line.
    
    return The error code to be transmitted to sys.exit().
    """
    
    def __init__(self, args):
        parser = argparse.ArgumentParser(
            description='Check whether the clang tidy log given in input contains warnings.', \
            formatter_class=argparse.RawDescriptionHelpFormatter)
            
        parser.add_argument(
            '--log',
            required=True,
            help= 'Path to the log file to analyze.'
        )
        
        parsed_args = parser.parse_args(args)
        
        result = FindWarningInClangTidyLog(parsed_args.log)        

        if result.Nwarning() == 0:
            self.__error_code  = 0
        else:
            result.PrintWarningsList()
            self.__error_code = os.EX_SOFTWARE

        if result.Ncode_smell() > 0:
            result.PrintCodeSmellList()
            
    def error_code(self):
        return self.__error_code


if __name__ == "__main__":
    sys.exit(ParseLog(sys.argv[1:]).error_code())
    
            
