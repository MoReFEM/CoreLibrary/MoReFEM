# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import sys
import re
import argparse
from collections import Counter


class FindWarningInCompilationLog():
    """Interpret the compilation log and count how many warnings were found and which ones.

    This utility is expected to be used in the CI process which uses Ninja as the CMake generator; so the log file is
    expected to be a Ninja output.
    """    

    def __init__(self, log):
        self.__reg_obj = re.compile(r'\[-W(.+?)\]')
        
        self._CreateWarningList(log)
        
        
    def Nwarning(self):
        """Return the total number of warnings found."""
        return len(self.__warning_list)
        
        
    def IsPerfect(self):
        """Returns true if no warning were found."""
        if self.__warning_list:
            return False
            
        return True
        
        
    def AsDict(self):
        """Returns the summary of warnings found and the occurences of each as a dictionary."""
        return dict(Counter(self.__warning_list))
        
        

    def _CreateWarningList(self, log):
        """Go through the 'log'file and extract all the warnings found (that fit the regular expression).
        """
        
        self.__warning_list = []
        
        with open(log) as FILE_in:
    
            for line in FILE_in:
                result = self._FindWarningInLine(line)
            
                if result:
                    self.__warning_list.append(result)


    def _FindWarningInLine(self, line):
        """Look in 'line' to find the regular expression given by 'reg_obj'.
    
        @param[in] line Line under investigation.
    
        Return: None if no warning was found, or the matched pattern if it was (e.g '[-Wreturn-std-move-in-c++11]').
        """
        try:
            return self.__reg_obj.search(line).group(0)[1:-1] # I remove the '[' and ']'.
        except AttributeError:
            return None
            
       
            
class ParseLog():
    """Just a facility to call FindWarningInCompilationLog from command line.
    
    return The error code to be transmitted to sys.exit().
    """
    
    def __init__(self, args):
        parser = argparse.ArgumentParser(
            description='Check whether the compilation log given in input contains compilation warnings.', \
            formatter_class=argparse.RawDescriptionHelpFormatter)
            
        parser.add_argument(
            '--log',
            required=True,
            help= 'Path to the log file to analyze.'
        )
        
        parsed_args = parser.parse_args(args)
        
        result = FindWarningInCompilationLog(parsed_args.log)
        
        if result.IsPerfect():
            self.__error_code = 0
        else:
            print("{} warnings were found:".format(result.Nwarning()))
            
            warning_list = result.AsDict()
            
            for (k,v) in warning_list.items():
                print("\t{} : {}\n".format(k, v))

            self.__error_code = os.EX_SOFTWARE
            
            
    def error_code(self):
        return self.__error_code


if __name__ == "__main__":
    sys.exit(ParseLog(sys.argv[1:]).error_code())
    
            
