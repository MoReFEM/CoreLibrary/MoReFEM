# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import argparse
import os

from enum import Enum

from generate_automated_blocks_content_in_source_files import ExistingBlockDelimiters
from run_clang_format import ApplyClangFormatOnFile
from generate_automated_blocks_content_in_source_files import GenerateAutomatedBlocksContentInSourceFiles
from generate_automated_blocks_content_in_source_files import morefem_copyright_notice

class SourceFilePath:
    """
    Small object which stores:
    - The absolute path of the file
    - The relative path from the directory used as reference for includes.
    """

    def __init__(self, absolute_path, relative_path):
        self.absolute_path = absolute_path
        self.relative_path = relative_path


class NewSourceFile:
    """
    The purpose of this class is to speed up the creation of the skeleton of a new file, doing
    most of the boilerplate for the end user.

    WARNING: This utility does NOT modify the content of CMake files; you have to do it manually.
    The reason is that main code and tests aren't handled the same way - one uses up automatically
    generated CMake files and the other not.

    """
    def __init__(self, project = "MoReFEM", project_namespace = "MoReFEM", copyright_notice = morefem_copyright_notice, sources_dir = None):
        """
        @param project Name of the project ('MoReFEM' for core library)

        @param copyright_notice Path to a file which specifies the text that should appear in the banner at the top of each source and header files. May be completed by information useful for Doxygen in C++ files.
        @param project_namespace Enclosing namespace for all content of the project. Is "MoReFEM" for main library,
        but might be something else for external model (including something as "MoReFEM::MyAwesomeExternalModel").
        @param sources_dir Directory in the project from which relative path for includes are 
        defined. For MoReFEM library itself please keep the 'None' value, which tells the 
        script to infer it from the location of current script. For external projects, please
        specify absolute path of the equivalent of 'Sources' directory in MoReFEM.
        """        
        is_external = sources_dir is not None
        self.existing_block_delimiters = ExistingBlockDelimiters(project, is_external=is_external)
        self._project_namespace = project_namespace
        self._identify_relative_directories(project, copyright_notice, sources_dir)
        self.FileType = Enum('FileType', ['model', 'variational_formulation', 'generic_class', 'header', 'input_data', 'main',
                                          'main_ensight_output', 'main_update_lua_file'])

        choice = self._choose_type_of_file()
        
        match choice:
            case self.FileType.generic_class:
                self._create_generic_class()
            case self.FileType.variational_formulation:
                self._create_variational_formulation()
            case self.FileType.model:
                self._create_model()
            case self.FileType.input_data:
                self._create_input_data()
            case self.FileType.main:
                self._create_main()
            case self.FileType.main_ensight_output:
                self._create_main_ensight_output()
            case self.FileType.main_update_lua_file:
                self._create_main_update_lua_file()
            case self.FileType.header:
                self._create_header_file()


    def _create_generic_class(self):
        """
        Method in charge of the creation of a generic class in MoReFEM.
        """
        name = input("How do you want to name your class? ")

        self._choose_namespace()
        self._ask_forward_declaration_block()

        self._create_generic_cpp(name)
        self._create_generic_hpp(name)
        self._create_generic_hxx(name)

    def _create_variational_formulation(self):
        """
        Method in charge of the creation of a VariationalFormulation class in MoReFEM.
        """
        name = input("Default name is 'VariationalFormulation', please enter another if you want so (leave empty "
                     "if default one is ok): ").strip()
        
        if not name:
            name = "VariationalFormulation"

        self._choose_namespace(do_query_advanced_or_internal=False)

        self._create_variational_formulation_cpp(name)
        self._create_variational_formulation_hpp(name)
        self._create_variational_formulation_hxx(name)


    def _create_model(self):
        """
        Method in charge of the creation of a Model class in MoReFEM.
        """
        name = input("Default name is 'Model', please enter another if you want so (leave empty "
                     "if default one is ok): ").strip()
        
        if not name:
            name = "Model"

        self._choose_namespace(do_query_advanced_or_internal=False)

        self._create_model_cpp(name)
        self._create_model_hpp(name)
        self._create_model_hxx(name)

    def _create_input_data(self):
        """
        Method in charge of the creation of InputData.hpp / ModelSettings.cpp files.
        """
        self._choose_namespace(do_query_advanced_or_internal=False)

        self._create_model_settings_cpp()
        self._create_input_data_hpp()


    def _create_generic_cpp(self, name):
        """
        Create generic cpp file.
        """
        file = self._determine_filename(name, "cpp")
        hpp_file = file.relative_path.replace("cpp", "hpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f'#include "{hpp_file}"')
            f.write("\n\n")
            f.write(self._forward_declaration_block)
            f.write(f"namespace {self._namespace}\n")
            f.write("{\n\n")
            f.write(f"}} // namespace {self._namespace}\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_generic_hpp(self, name):
        """
        Create generic hpp file.
        """
        file = self._determine_filename(name, "hpp")
        hxx_file = file.relative_path.replace("hpp", "hxx")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write('#include <memory>\n')
            f.write('#include <vector>\n\n')

            f.write(self._forward_declaration_block)

            f.write(f"namespace {self._namespace}\n")
            f.write("{\n\n")

            content_file = os.path.join(self._script_dir, "FileTemplate", "GenericHpp.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line.replace("[[CLASS_NAME]]", name))

            f.write(f"\n\n}} // namespace {self._namespace}\n\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

            f.write(f'#include "{hxx_file}" // IWYU pragma: export\n\n')

            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_generic_hxx(self, name):
        """
        Create generic hxx file.
        """
        file = self._determine_filename(name, "hxx")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write(self._forward_declaration_block)

            f.write(f"namespace {self._namespace}\n")
            f.write("{\n\n")
            f.write(f"}} // namespace {self._namespace}\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")
            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_variational_formulation_cpp(self, name):
        """
        Create variational formulation cpp file.
        """
        file = self._determine_filename(name, "cpp")
        hpp_file = file.relative_path.replace("cpp", "hpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f'#include "{hpp_file}"')
            f.write("\n\n")
            f.write(f"namespace {self._namespace}\n{{\n\n")

            content_file = os.path.join(self._script_dir, "FileTemplate", "VariationalFormulationCpp.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line.replace("[[CLASS_NAME]]", name))
    
            f.write(f"\n\n}} // namespace {self._namespace}\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_variational_formulation_hpp(self, name):
        """
        Create variational formulation hpp file.
        """
        file = self._determine_filename(name, "hpp")
        hxx_file = file.relative_path.replace("hpp", "hxx")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write('#include <memory>\n')
            f.write('#include <type_traits> // IWYU pragma: export \n')
            f.write('#include <vector>\n\n')

            f.write('#include "Geometry/Domain/Domain.hpp"\n\n')

            f.write('#include "FormulationSolver/VariationalFormulation.hpp"\n')

            input_data_file = file.relative_path.replace(f"{name}.hpp", "InputData.hpp")
            f.write(f'#include "{input_data_file}" // fix here if not matching your name and/or location choice! \n\n')

            f.write(f"namespace {self._namespace}\n")
            f.write("{\n\n")

            content_file = os.path.join(self._script_dir, "FileTemplate", "VariationalFormulationHpp.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line.replace("[[CLASS_NAME]]", name))

            f.write(f"\n\n}} // namespace {self._namespace}\n\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

            f.write(f'#include "{hxx_file}" // IWYU pragma: export\n\n')

            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_variational_formulation_hxx(self, name):
        """
        Create variational formulation hxx file.
        """
        file = self._determine_filename(name, "hxx")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write(f"namespace {self._namespace}\n{{\n\n")
            
            content_file = os.path.join(self._script_dir, "FileTemplate", "VariationalFormulationHxx.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line.replace("[[CLASS_NAME]]", name))

            f.write(f"\n\n}} // namespace {self._namespace}\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")
            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)

    def _create_model_cpp(self, name):
        """
        Create model cpp file.
        """
        file = self._determine_filename(name, "cpp")
        hpp_file = file.relative_path.replace("cpp", "hpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f'#include "{hpp_file}"')
            f.write("\n\n")
            f.write(f"namespace {self._namespace}\n{{\n\n")

            content_file = os.path.join(self._script_dir, "FileTemplate", "ModelCpp.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line.replace("[[CLASS_NAME]]", name))
    
            f.write(f"\n\n}} // namespace {self._namespace}\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_model_hpp(self, name):
        """
        Create model hpp file.
        """
        file = self._determine_filename(name, "hpp")
        hxx_file = file.relative_path.replace("hpp", "hxx")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write('#include <memory>\n')
            f.write('#include <type_traits> // IWYU pragma: export \n')
            f.write('#include <vector>\n\n')

            f.write('#include "Model/Model.hpp"\n\n')

            input_data_file = file.relative_path.replace(f"{name}.hpp", "InputData.hpp")
            f.write(f'#include "{input_data_file}" // fix here if not matching your name and/or location choice! \n\n')

            f.write(f"namespace {self._namespace}\n")
            f.write("{\n\n")

            content_file = os.path.join(self._script_dir, "FileTemplate", "ModelHpp.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line.replace("[[CLASS_NAME]]", name))

            f.write(f"\n\n}} // namespace {self._namespace}\n\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

            f.write(f'#include "{hxx_file}" // IWYU pragma: export\n\n')

            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_model_hxx(self, name):
        """
        Create model hxx file.
        """
        file = self._determine_filename(name, "hxx")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")            
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write(f"namespace {self._namespace}\n{{\n\n")
            
            content_file = os.path.join(self._script_dir, "FileTemplate", "ModelHxx.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line.replace("[[CLASS_NAME]]", name))

            f.write(f"\n\n}} // namespace {self._namespace}\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")
            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_model_settings_cpp(self):
        """
        Create ModelSettings.cpp file.
        """
        name = "ModelSettings"
        file = self._determine_filename(name, "cpp")
        hpp_file = file.relative_path.replace("ModelSettings.cpp", "InputData.hpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f'#include "{hpp_file}"')
            f.write("\n\n")
            f.write(f"namespace {self._namespace}\n")
            f.write("{\n\n")

            content_file = os.path.join(self._script_dir, "FileTemplate", "ModelSettingsCpp.txt")

            with open(content_file) as bulk:

                for line in bulk:
                    f.write(line)

            f.write(f"\n\n}} // namespace {self._namespace}\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_input_data_hpp(self):
        """
        Create InputData.hpp file.
        """
        name = "InputData"

        file = self._determine_filename(name, "hpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write('#include <memory>\n')
            f.write('#include <vector>\n\n')

            content_file = os.path.join(self._script_dir, "FileTemplate", "InputDataHeaderListHpp.txt")

            with open(content_file) as bulk:
                for line in bulk:
                    f.write(line)

            f.write(f"\n\nnamespace {self._namespace}\n")
            f.write("{\n\n")

            content_file = os.path.join(self._script_dir, "FileTemplate", "InputDataHpp.txt")

            with open(content_file) as bulk:
                for line in bulk:
                    f.write(line)

            f.write(f"\n\n}} // namespace {self._namespace}\n\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")
            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_main(self):

        name = "main"

        file = self._determine_filename(name, "cpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")

            f.write('#include "Model/Main/Main.hpp"\n\n')

            f.write('// \\todo Include here path to your Model.hpp (or equivalent) file!"\n\n')

            f.write('\nusing namespace MoReFEM;\n\n');\

            f.write("int main(int argc, char** argv)\n{\n")
            f.write('return MoReFEM::ModelNS::Main<**\\todo Add your model class here!**>(argc, argv);\n}\n\n')

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_main_update_lua_file(self):
        name = "main_update_lua_file"

        file = self._determine_filename(name, "cpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")

            f.write('#include "Model/Main/MainUpdateLuaFile.hpp"\n\n')

            f.write('// \\todo Include here path to your Model.hpp (or equivalent) file!"\n\n')

            f.write('\nusing namespace MoReFEM;\n\n');

            f.write("int main(int argc, char** argv)\n{\n")
            f.write('return MoReFEM::ModelNS::MainUpdateLuaFile<**\\todo Add your model class here!**>(argc, argv);\n}\n\n')

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _create_main_ensight_output(self):
        name = "main_ensight_output"

        file = self._determine_filename(name, "cpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")

            f.write('#include "Model/Main/MainEnsightOutput.hpp"\n\n')

            f.write('// \\todo Include here path to your Model.hpp (or equivalent) file!"\n\n')

            f.write('\nusing namespace MoReFEM;\n\n');

            f.write("int main(int argc, char** argv)\n{\n")

            f.write("constexpr auto mesh_id = AsMeshId(MeshIndex::***);\n")
            f.write("// < Complete here with relevant mesh moniker\n\n")

            f.write("std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ };\n")
            f.write("// < Complete here with relevant numbering subsets, with syntax "
                    "'AsNumberingSubsetId(NumberingSubsetIndex::***)'\n\n")
            
            f.write("std::vector<std::string> unknown_list{ };\n")
            f.write("// < Complete here with relevant unknowns\n\n")

            f.write('return MoReFEM::ModelNS::MainEnsightOutput<**\\todo Add your model class here!**>(argc, argv, mesh_id, numbering_subset_id_list, unknown_list);\n}\n\n')

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)

    def _create_header_file(self):
        """
        Method in charge of the creation of an isolated header file (typically for concept or strong type)
        """
        name = input("How do you want to name your file? ")

        self._choose_namespace()
        self._ask_forward_declaration_block()

        file = self._determine_filename(name, "hpp")

        with open(file.absolute_path, "w") as f:
            f.write(f"{self.existing_block_delimiters.cpp_banner}\n\n")
            f.write(f"{self.existing_block_delimiters.header_guards}\n\n")

            f.write(self._forward_declaration_block)

            f.write(f"namespace {self._namespace}\n")
            f.write("{\n\n")

            f.write(f"\n\n}} // namespace {self._namespace}\n\n\n")

            f.write(f"{self.existing_block_delimiters.doxygen_end_of_group}\n\n")

            f.write(f"{self.existing_block_delimiters.end_header_guards}\n\n")

        self.generate_automated_block_helper.CorrectAutomatedBlocksInOneFile(file.absolute_path)
        ApplyClangFormatOnFile(file.absolute_path)


    def _determine_filename(self, name, extension):
        """
        Return a SourceFilePath object with information regarding the file generated.
        """

        name_with_ext = f"{name}.{extension}"

        path = os.path.join(self._current_dir, name_with_ext)

        if os.path.exists(path):
            raise Exception(f"File {path} already exists!")
        
        return SourceFilePath(path, os.path.normpath(os.path.join(self._relative_path_to_sources, name_with_ext)))


    def _choose_type_of_file(self):
        """
        Query the user to know which kind of file they want to create.

        @return The type of file we want to create.
        """
        choice_list = [ "Do you want to create: ", "A generic [C]lass?", "A [H]eader file", "A [Mo]del?", "A [V]ariational formulation?", "A [I]nputData / ModelSettings?", "A [M]ain for a model?", "A main for [E]nsight output?", "A main for "
                       "updating [L]ua files?"]

        while True:
            choice = input(f'{"\n - ".join(choice_list)}\n').upper()

            if choice in ("C", "MO", "V", "I", "M", "E", "L", "H"):
                break

        match choice:
            case "C":
                return self.FileType.generic_class
            case "MO":
                return self.FileType.model
            case "V":
                return self.FileType.variational_formulation
            case "I":
                return self.FileType.input_data
            case "M":
                return self.FileType.main
            case "E":
                return self.FileType.main_ensight_output
            case "L":
                return self.FileType.main_update_lua_file
            case "H":
                return self.FileType.header

            

    def _identify_relative_directories(self, project, copyright_notice, sources_dir):
        """
        Identify where the executable is called from (the files will be created there) and where is the root
        of the project (it is determined from the location of this very script: two levels removed from 
        current one).

        This will be required for instance to properly set the includes in the generated files.

        @param sources_dir Directory in the project from which relative path for includes are 
        defined. For MoReFEM library itself please keep the 'None' value, which tells the 
        script to infer it from the location of current script. For external projects, please
        specify absolute path of the equivalent of 'Sources' directory in MoReFEM.
        """
        self._script_dir = os.path.normpath(os.path.dirname(__file__))

        if sources_dir:
            is_external = True
        else:
            is_external = False
            sources_dir = os.path.normpath(os.path.join(self._script_dir, "..", "..", "Sources"))

        current_dir = os.getcwd()

        self._current_dir = current_dir
        self._relative_path_to_sources = os.path.relpath(current_dir, start=sources_dir)

        if self._relative_path_to_sources.startswith(".."):
            raise Exception("This script should be called from a directory nested in 'Sources' directory of MoReFEM!")
        
        self.generate_automated_block_helper = GenerateAutomatedBlocksContentInSourceFiles(sources_dir, project = project, copyright_notice = copyright_notice, is_external = is_external)


    def _choose_namespace(self, do_query_advanced_or_internal = True):
        """
        Query end user to determine in which the namespace their new object will be located

        @param do_query_advanced_or_internal If True, ask end user whether namespace is within 'Advanced' or
        'Internal' namespace.

        In output, self._namespace is set.
        """
        namespace = self._project_namespace

        if do_query_advanced_or_internal:
            while True:
                namespace_choice_list = [ "Is it intended to be in : ", "[A]dvanced namespace?", "[I]nternal namespace?", "[] None of them?"]
                namespace_choice = input(f'{"\n - ".join(namespace_choice_list)}\n').upper()

                match namespace_choice:
                    case "A":
                        namespace = f"{namespace}::Advanced"
                    case "I":
                        namespace = f"{namespace}::Internal"

                if namespace_choice in ("A", "I", ""):
                    break

        other_namespace = input("Please specify here eventual supplementary namespaces (separate them by '::') ").strip()

        if other_namespace:
            namespace = f"{namespace}::{other_namespace.lstrip()}"

        self._namespace = namespace

    def _ask_forward_declaration_block(self):
        """
        Query end user to determine whether a forward declaration block must be foreseen or not.

        In output, self._forward_declaration_block is set (possibly to empty string).
        """
        while True:

            choice = input(f'Do you want a forward declaration block? [y/n]').upper()

            match choice:
                case "Y":
                    list = (\
                    "// ============================",
                    "// clang-format off",
                    r"//! \cond IGNORE_BLOCK_IN_DOXYGEN",
                    "// Forward declarations.",
                    "// ============================",
                    "",
                    "namespace MoReFEM { }",
                    "",
                    "// ============================",
                    "// End of forward declarations.",
                    r"//! \endcond IGNORE_BLOCK_IN_DOXYGEN",
                    "// clang-format on",
                    "// ============================",
                    "",
                    "",
                    "")

                    self._forward_declaration_block = '\n'.join(list)
                    
                    break
                    
                case "N":
                    self._forward_declaration_block = ""
                    break

            


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
                    prog="new_file",
                    description='Facility to create skeleton of source and header files, limiting boilerplate involved If called for an external project most if not all command line options should be provided!')
    
    parser.add_argument('--project', help = "Name of the project. If none 'MoReFEM' is assumed.") 
    parser.add_argument('--project_namespace', help = "Enclosing namespace for the project. If none 'MoReFEM' is assumed.") 
    parser.add_argument('--sources_dir', help = "Absolute path to the directory used as baseline for includes "
                        "in the program or library. For instance in MoReFEM all includes are given related to "
                        "the 'Sources' directory; the path of this directoru would be expected here. If none "
                        "specified, it is assumed the script is called for MoReFEM core library.") 
    parser.add_argument('--copyright_notice', help = "Path to a file which specifies the text that should appear "
                        "in the banner at the top of each source and header files. If none specified, default "
                        "MoReFEM copyright notice is used.")
    
    args = parser.parse_args()

    project = args.project or "MoReFEM"

    project_namespace = args.project_namespace or "MoReFEM"

    if args.sources_dir and not os.path.exists(args.sources_dir):
        raise Exception(f"Invalid directory: {args.sources_dir} does not exist!")

    copyright_notice = args.copyright_notice or morefem_copyright_notice

    NewSourceFile(project = project, project_namespace = project_namespace, copyright_notice = copyright_notice, sources_dir = args.sources_dir)