# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #


# Important note: clang-format does not yield exactly the very same result when run from Fedora and macOS, even when 
# the exact same version is used on both sides. For this reason clang-format is not automatically present in CI; it is
# run very frequently (at the very least once before each release) by the integration manager.

import os
import sys
import subprocess
import shutil
import hashlib


def md5(fname):
    """
    From https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file.

    The checksum is just to evaluate if the file was modified or not - there might be easier way to do so but it is
    good enough for my purpose here. Likewise, I don't care about safety issues concerning MD5 given the intended
    purpose here.
    """
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def ApplyClangFormatOnFile(myfile):
    """
    Apply clang-format inplace on a given file.

    @return True if the file was modified by clang-format.
    """
    if not shutil.which("clang-format"):
        print("clang-format not found!", file=sys.stderr)
        return False

    checksum_before = md5(myfile)
    subprocess.run(["clang-format", "-style=file", "-i", myfile], check=True)
    checksum_after = md5(myfile)

    if checksum_before != checksum_after:
        print(f'\t clang-format modified: {myfile}')
        return True

    return False


def ApplyClangFormat(morefem_directory, pause_after_each_directory = False):
    """
    The idea here is to apply clang-format upon all of the source files of MoReFEM. The pause_after_each_directory
    parameter is important: in first run I want to be able to check I am satisfied with the formatting as it goes;
    for later run we will probably be more inspired to silence it - and reactivate it if for some reason too Many
    changes are found (possible when we upgrade the clang-format used).

    - param[in] morefem_directory Path to the MoReFEM directory.
    - param[in] pause_after_each_directory If True, stop after each directory and ask whether the user
    is happy with the changes. If not, offer abortion of the process.
    """
    source_directory = os.path.realpath(os.path.join(morefem_directory, "Sources"))

    tab_prefix = "\t\t"

    modified_files_list = []

    for root, dirs, files in os.walk(source_directory):

        print("Looking for all files in {}".format(os.path.realpath(root)))

        files = [item for item in files if os.path.splitext(item)[1] in (".cpp", ".hpp", ".hxx")]

        if not files:
            continue

        is_any_file_modified_in_current_dir = False

        for myfile in files:
            complete_path = os.path.join(root, myfile)

            if ApplyClangFormatOnFile(complete_path):
                modified_files_list.append(complete_path)
                is_any_file_modified_in_current_dir = True


        if pause_after_each_directory and is_any_file_modified_in_current_dir:
            answer = ""
            while answer.lower() not in (("y", "n", "yes", "no")):
                answer = input('Do you want to proceed to next directory? [y(es),n(o)] ')

            if answer.lower()[0] == "y":
                print("Let's go!")
            else:
                raise Exception("Script end required by the user.")

    return modified_files_list


if __name__ == "__main__":

    morefem_root_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..")

    modified_files_list = ApplyClangFormat(morefem_root_directory, pause_after_each_directory = False)

    if len(modified_files_list) == 0:
        print("No files modified!")
        sys.exit(0)
    else:
        print(f"The {len(modified_files_list)} following files were modified:")
        print("\n\t- {}".format("\n\t- ".join(modified_files_list)))
        sys.exit(1)
