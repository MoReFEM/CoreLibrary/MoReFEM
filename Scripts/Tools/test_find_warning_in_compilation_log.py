# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import unittest
from tempfile import TemporaryDirectory

import find_warning_in_compilation_log as fw


class TestFindWarningInCompilationLog(unittest.TestCase):
    
    
    def test_no_warning(self):

        with TemporaryDirectory() as d:          
            filename = os.path.join(d, "temporary.log")            
            
            with open(filename, "w") as fp:
                fp.write("[0/315] blahblah")
                
            result = fw.FindWarningInCompilationLog(fp.name)
            self.assertTrue(result.IsPerfect())
            self.assertEqual(result.Nwarning(), 0)
            


    def test_one_warning(self):

        with TemporaryDirectory() as d:            
            filename = os.path.join(d, "temporary.log")            
            with open(filename, "w") as fp:
                fp.write("[0/315] blahblah\n")
                fp.write("blahblah [-Wfirst_warning]")
                
            result = fw.FindWarningInCompilationLog(fp.name)
            self.assertFalse(result.IsPerfect())            
            self.assertEqual(result.Nwarning(), 1)
            
            
    def test_separate_warnings(self):

        with TemporaryDirectory() as d:            
            filename = os.path.join(d, "temporary.log")            
            with open(filename, "w") as fp:
                fp.write("[0/315] blahblah\n")
                fp.write("blahblah [-Wfirst_warning]\n")
                fp.write("blahblah [-Wsecond_warning]\n")
                fp.write("blahblah [-Wfirst_warning]\n")
                fp.write("blahblah [-Wthird_warning]\n")
                
            result = fw.FindWarningInCompilationLog(fp.name)
            self.assertFalse(result.IsPerfect())    
            self.assertEqual(result.Nwarning(), 4)
            
            expected_content = dict()
            expected_content["-Wfirst_warning"] = 2
            expected_content["-Wsecond_warning"] = 1
            expected_content["-Wthird_warning"] = 1                        
            
            self.assertDictEqual(result.AsDict(), expected_content)
            
    def test_not_existing_file(self):
        with self.assertRaises(FileNotFoundError):
            fw.FindWarningInCompilationLog("hopefully_unexisting_file_1525")
      
      
    def test_parse_command_line_warnings_found(self):

        with TemporaryDirectory() as d:
            filename = os.path.join(d, "temporary.log")

            with open(filename, "w") as fp:
                fp.write("[0/315] blahblah\n")
                fp.write("blahblah [-Wfirst_warning]\n")
                
            args = ["--log", filename ]    

            result = fw.ParseLog(args)

            self.assertEqual(result.error_code(), os.EX_SOFTWARE)
            
            
    def test_parse_command_line_no_warning(self):
        
        with TemporaryDirectory() as d:          
            filename = os.path.join(d, "temporary.log")
            
            with open(filename, "w") as fp:
                fp.write("[0/315] blahblah")
                
            args = ["--log", filename ]
                
            result = fw.ParseLog(args)
            
            self.assertEqual(result.error_code(), 0)
                