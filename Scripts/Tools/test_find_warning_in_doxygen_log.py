# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #


import os
import unittest
from tempfile import TemporaryDirectory

import find_warning_in_doxygen_log as analyze_doxygen_log


class TestFindWarningInDoxygenLog(unittest.TestCase):
    
    
    def test_no_warning_first_exclusion(self):

        with TemporaryDirectory() as d:          
            filename = os.path.join(d, "temporary.log")            
            
            with open(filename, "w") as fp:
                fp.write("/Users/sebastien/Codes/MoReFEM/CoreLibrary/Sources/OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hpp:57: warning: return type of member MoReFEM::Advanced::LocalVariationalOperatorNS::NonlinearMembrane::ClassName is not documented")
                
            result = analyze_doxygen_log.FindWarningInDoxygenLog(fp.name)
            self.assertTrue(result.IsPerfect())
            self.assertEqual(result.Nwarning(), 0)
            
    def test_no_warning_second_exclusion(self):

        with TemporaryDirectory() as d:          
            filename = os.path.join(d, "temporary.log")            
            
            with open(filename, "w") as fp:
                fp.write("/Users/sebastien/Codes/MoReFEM/CoreLibrary/README.md:322: warning: unable to resolve reference to `https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/tree/master/Documentation/Wiki/Model/model_tutorial.md' for \\ref command")
                
            result = analyze_doxygen_log.FindWarningInDoxygenLog(fp.name)
            self.assertTrue(result.IsPerfect())
            self.assertEqual(result.Nwarning(), 0)
            
            


    def test_one_warning(self):

        with TemporaryDirectory() as d:            
            filename = os.path.join(d, "temporary.log")            
            with open(filename, "w") as fp:                
                fp.write("whatever")
                
            result = analyze_doxygen_log.FindWarningInDoxygenLog(fp.name)
            self.assertFalse(result.IsPerfect())            
            self.assertEqual(result.Nwarning(), 1)
            
            
    def test_content(self):
        with TemporaryDirectory() as d:            
            filename = os.path.join(d, "temporary.log")            
            with open(filename, "w") as fp:                
                fp.write("whatever\n")
                
            result = analyze_doxygen_log.FindWarningInDoxygenLog(fp.name)

            self.assertEqual(len(result.content), 1)
            self.assertEqual(result.content[0], "whatever\n")
            
            
    def test_not_existing_file(self):
        with self.assertRaises(FileNotFoundError):
            analyze_doxygen_log.FindWarningInDoxygenLog("hopefully_unexisting_file_1525")
      
      
    def test_parse_command_line_warnings_found(self):

        with TemporaryDirectory() as d:
            filename = os.path.join(d, "temporary.log")

            with open(filename, "w") as fp:
                fp.write("whatever\n")
                
            args = ["--log", filename ]    

            result = analyze_doxygen_log.ParseLog(args)

            self.assertEqual(result.error_code(), os.EX_SOFTWARE)
            
            
    def test_parse_command_line_no_warning(self):
        
        with TemporaryDirectory() as d:          
            filename = os.path.join(d, "temporary.log")
            
            with open(filename, "w") as fp:
                fp.write("Foo file: warning: return type of whatever object")
                
            args = ["--log", filename ]
                
            result = analyze_doxygen_log.ParseLog(args)
            
            self.assertEqual(result.error_code(), 0)
