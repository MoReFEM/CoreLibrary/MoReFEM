# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
# 
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
#######################################################################################################################
# *** MoReFEM copyright notice *** < #


import os
import sys
import stat
import platform

macOS_abspath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "macOS")
sys.path.append(macOS_abspath)

class InitMoReFEM:
    """Init properly MoReFEM.

    This is intended to be called just after the MoReFEM.git folder has been cloned.
    """

    def __init__(self):

        self.main_folder = [os.getcwd()]

        current_folder = os.path.split(self.main_folder[0])[1]

        if current_folder not in ("MoReFEM", "CoreLibrary"):
            raise Exception(
                "This script must be run from MoReFEM root folder!")

        self.set_commit_hook()
        self.git_config()

    def set_commit_hook(self):
        """Set the commit hook so that all commits messages respect the expected format."""

        print("=== Placing git commit hook. ===")

        morefem_folder = self.main_folder[0]

        target = os.path.join(morefem_folder, ".git", "hooks", "commit-msg")

        # Open the commit hook file.The 'w' is on purpose: the newly generated
        # one must replace any pre-existing file (there are no reasons to keep
        # an older or a modified version of it).
        with open(target, 'w') as myfile:
            # Write the python she-bang.
            myfile.write("#!/bin/bash\n")

            # Call the python script.
            myfile.write("{0} {1} $1".format(sys.executable, \
                                             os.path.join(morefem_folder, \
                                             "Scripts", "Internal", "commit_hook.py")))

        os.chmod(target, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)

    def git_config(self):
        """Check whether the user already has a gitconfig, and if not propose a default one."""
        print("=== Check gitconfig. ===")

        gitconfig_path = os.path.join(os.path.expanduser("~"), ".gitconfig")

        if os.path.exists(gitconfig_path):
            print("\tAlready existing, do nothing.")
            return

        print(
            "No gitconfig was found; would you like to install a default one? (y/n)"
        )

        myinput = input()

        while myinput not in ("y", "n"):
            myinput = input()

        if myinput == "y":
            print(
                "Please enter your first and last name (this will appear in your git commits)."
            )
            name = input()
            print(
                "Please enter your e-mail address (this will appear in your git commits)."
            )
            email = input()

            with open(gitconfig_path, 'w') as gitconfig_file:
                gitconfig_file.write("[user]\n")
                gitconfig_file.write("\tname = {0}\n".format(name))
                gitconfig_file.write("\temail = {0}\n\n".format(email))

                with open(
                        os.path.join(
                            self.main_folder[0], "Scripts", "Internal",
                            "gitconfig_without_user")) as input_content:
                    for line in input_content:
                        gitconfig_file.write(line)

            print("Your git config has been set; you can edit it at {0}.".
                  format(gitconfig_path))


if __name__ == "__main__":

    InitMoReFEM()
