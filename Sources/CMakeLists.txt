if (BUILD_MOREFEM_UNIQUE_LIBRARY)
    add_library(MoReFEM ${LIBRARY_TYPE} "")
endif()

morefem_library_module(MOREFEM_UTILITIES MoReFEM_utilities)
morefem_library_module(MOREFEM_CORE MoReFEM_core)
morefem_library_module(MOREFEM_GEOMETRY MoReFEM_geometry)
morefem_library_module(MOREFEM_FELT MoReFEM_felt)
morefem_library_module(MOREFEM_PARAM MoReFEM_param)
morefem_library_module(MOREFEM_OP MoReFEM_op)
morefem_library_module(MOREFEM_PARAM_INSTANCES MoReFEM_param_instances)
morefem_library_module(MOREFEM_OP_INSTANCES MoReFEM_op_instances)
morefem_library_module(MOREFEM_FORMULATION_SOLVER MoReFEM_formulation_solver)
morefem_library_module(MOREFEM_MODEL MoReFEM_model)

add_library(MoReFEM_post_processing ${LIBRARY_TYPE} "")
set(MOREFEM_POST_PROCESSING MoReFEM_post_processing)

if(LIBRARY_TYPE MATCHES SHARED)
target_link_libraries(${MOREFEM_POST_PROCESSING}
                      PUBLIC $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_UTILITIES}>)
endif()

if (BUILD_MOREFEM_UNIQUE_LIBRARY)
    target_link_libraries(MoReFEM ${MOREFEM_COMMON_DEP})
else()
    target_link_libraries(MoReFEM_utilities ${MOREFEM_COMMON_DEP})
    target_link_libraries(${MOREFEM_CORE} $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_UTILITIES}>)
    target_link_libraries(${MOREFEM_GEOMETRY} $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)
    target_link_libraries(${MOREFEM_FELT} $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_GEOMETRY}>)
    target_link_libraries(${MOREFEM_PARAM} $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>)
    target_link_libraries(${MOREFEM_OP} $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_PARAM}>)
    target_link_libraries(${MOREFEM_PARAM_INSTANCES} $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_OP}>)
    target_link_libraries(${MOREFEM_OP_INSTANCES} $<LINK_LIBRARY:WHOLE_ARCHIVE, ${MOREFEM_PARAM_INSTANCES}>)
    target_link_libraries(${MOREFEM_FORMULATION_SOLVER} $<LINK_LIBRARY:WHOLE_ARCHIVE, ${MOREFEM_OP_INSTANCES}>)
    target_link_libraries(${MOREFEM_MODEL} $<LINK_LIBRARY:WHOLE_ARCHIVE, ${MOREFEM_FORMULATION_SOLVER}>)
endif()

include(Utilities/CMakeLists.txt)

include(ThirdParty/CMakeLists.txt)

include(Core/CMakeLists.txt)

include(Geometry/CMakeLists.txt)

include(FiniteElement/CMakeLists.txt)

include(Parameters/CMakeLists.txt)

include(Operators/CMakeLists.txt)

include(ParameterInstances/CMakeLists.txt)

include(OperatorInstances/CMakeLists.txt)

include(FormulationSolver/CMakeLists.txt)

include(Model/CMakeLists.txt)

include(PostProcessing/CMakeLists.txt)


if (BUILD_MOREFEM_UNIQUE_LIBRARY)
    morefem_install(MoReFEM MoReFEM_post_processing)
else()
    morefem_install(MoReFEM_utilities MoReFEM_core MoReFEM_geometry MoReFEM_felt MoReFEM_param MoReFEM_op MoReFEM_param_instances MoReFEM_op_instances MoReFEM_formulation_solver MoReFEM_model MoReFEM_post_processing)    
endif()



include(Test/CMakeLists.txt)

morefem_install(MoReFEM_extensive_test_tools)

include(ModelInstances/CMakeLists.txt)


install(DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/ DESTINATION ${MOREFEM_INSTALL_DIR_INCL}
        FILES_MATCHING PATTERN "*.hpp")

install(DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/ DESTINATION ${MOREFEM_INSTALL_DIR_INCL}
        FILES_MATCHING PATTERN "*.hxx")


#
# #### Lifted and adapted from Xtl
#
add_library(morefem_cmake INTERFACE)

target_include_directories(morefem_cmake INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> $<INSTALL_INTERFACE:${PROJECT_NAME}/include>  ${OPEN_MPI_INCL_DIR} ${PETSC_INCL_DIR} ${PETSC_INSTALL_INCL_DIR} ${PARMETIS_INCL_DIR} ${LUA_INCL_DIR} ${BOOST_INCL_DIR} ${LIBMESH_INCL_DIR} ${EIGEN_INCL_DIR} ${TCLAP_INCL_DIR} )

target_link_libraries(morefem_cmake INTERFACE ${MOREFEM_POST_PROCESSING} ${MOREFEM_EXTENSIVE_TEST_TOOLS})

set_property(TARGET morefem_cmake PROPERTY INTERFACE_COMPILE_DEFINITIONS ${MOREFEM_COMPILE_DEFINITIONS})


# #
#
#
# # Installation
# # ============
#

include(CMakePackageConfigHelpers)

install(TARGETS morefem_cmake ${MOREFEM_MAIN_LIBS} ${MOREFEM_POST_PROCESSING} ${MOREFEM_EXTENSIVE_TEST_TOOLS} ${MOREFEM_BASIC_TEST_TOOLS}
        EXPORT ${PROJECT_NAME}-targets
        LIBRARY DESTINATION ${MOREFEM_INSTALL_DIR_LIB}
        ARCHIVE DESTINATION ${MOREFEM_INSTALL_DIR_LIB}
    )
#
# # Makes the project importable from the build directory
export(EXPORT ${PROJECT_NAME}-targets
       FILE "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Targets.cmake")

configure_package_config_file(../cmake/${PROJECT_NAME}Config.cmake.in
                              "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
                              INSTALL_DESTINATION ${MOREFEM_INSTALL_DIR_CMAKE})


install(FILES
            ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}Config.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/CustomCommands.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/TestCustomCommands-Boost.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/TestCustomCommands.cmake     
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/TestCustomCommands-RunModel.cmake 
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/MoReFEMSettings.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/ExternalModelCMakeLists.txt
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/XCode.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/../../cmake/Scripts/configure_cmake_external_model.py            
            ${CMAKE_CURRENT_BINARY_DIR}/../PreCacheFile.cmake
        DESTINATION ${MOREFEM_INSTALL_DIR_CMAKE})

install(DIRECTORY
            ${CMAKE_CURRENT_BINARY_DIR}/../../Scripts/Utilities
        DESTINATION ${MOREFEM_INSTALL_DIR_SHARE}/Scripts)

install(DIRECTORY
            ${CMAKE_CURRENT_BINARY_DIR}/../../Scripts/Tools/FileTemplate
        DESTINATION ${MOREFEM_INSTALL_DIR_SHARE}/Scripts/Tools)

install(FILES
            ${CMAKE_CURRENT_BINARY_DIR}/../../ExternalTools/ClangFormat/clang-format 
        DESTINATION ${MOREFEM_INSTALL_DIR_SHARE})

install(FILES
            ${CMAKE_CURRENT_BINARY_DIR}/../../Documentation/CopyrightNotice.txt 
       DESTINATION ${MOREFEM_INSTALL_DIR_SHARE})        

install(FILES
            ${CMAKE_CURRENT_BINARY_DIR}/../../Scripts/Tools/find_warning_in_compilation_log.py
            ${CMAKE_CURRENT_BINARY_DIR}/../../Scripts/Tools/generate_automated_blocks_content_in_source_files.py
            ${CMAKE_CURRENT_BINARY_DIR}/../../Scripts/Tools/new_file.py
            ${CMAKE_CURRENT_BINARY_DIR}/../../Scripts/Tools/run_clang_format.py
        DESTINATION ${MOREFEM_INSTALL_DIR_SHARE}/Scripts/Tools)

install(EXPORT ${PROJECT_NAME}-targets
        FILE ${PROJECT_NAME}Targets.cmake
        DESTINATION ${MOREFEM_INSTALL_DIR_CMAKE})
