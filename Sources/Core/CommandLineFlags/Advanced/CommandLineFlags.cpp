// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>

#include "Core/CommandLineFlags/Advanced/CommandLineFlags.hpp"

#include "ThirdParty/Wrappers/Petsc/Advanced/Enum.hpp"

#include "Core/CommandLineFlags/Internal/CommandLineFlags.hpp"


namespace MoReFEM::Advanced::CommandLineFlagsNS
{


    auto DoOverwriteDirectory() -> overwrite_directory
    {
        decltype(auto) command_line_flags_singleton = ::MoReFEM::Internal::CommandLineFlags::GetInstance();
        return command_line_flags_singleton.DoOverwriteDirectory();
    }


    auto DoPrintLinearAlgebraDestruction() -> Advanced::Wrappers::Petsc::print_linalg_destruction
    {
        decltype(auto) command_line_flags_singleton = ::MoReFEM::Internal::CommandLineFlags::GetInstance();
        return command_line_flags_singleton.DoPrintLinearAlgebraDestruction();
    }


    void InitCommandLineFlagsForTests(std::source_location location)
    {
        ::MoReFEM::Internal::CommandLineFlags::CreateOrGetInstance(
            location,
            Advanced::CommandLineFlagsNS::overwrite_directory::yes,
            Advanced::Wrappers::Petsc::print_linalg_destruction::yes);
    }


} // namespace MoReFEM::Advanced::CommandLineFlagsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
