// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_COMMANDLINEFLAGS_ADVANCED_COMMANDLINEFLAGS_DOT_HPP_
#define MOREFEM_CORE_COMMANDLINEFLAGS_ADVANCED_COMMANDLINEFLAGS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>
#include <string>

#include "Core/CommandLineFlags/Advanced/Enum.hpp" // IWYU pragma: export
#include "Core/CommandLineFlags/Internal/CommandLineFlags.hpp"


namespace MoReFEM::Advanced::CommandLineFlagsNS
{

    /*!
     * \brief Set the singleton that stores command line flags in case of a model with values adapted for tests.
     *
     * \internal This may seem a bit convoluted, but unfortunately tests are driven by Boost::Test and I can't use the TCLAP interface used
     * for models (it was the same with Catch2 facility beforehand...).
     *
     * \copydoc doxygen_hide_source_location
     */
    void InitCommandLineFlagsForTests(std::source_location location = std::source_location::current());

    /*!
     * \brief Whether there was a command line flag telling that pre-existing output directory may be silently removed.
     *
     * \return If no, the code will prompt the user about what to do or exist gracefully.
     *
     * \attention For tests we bypass this and assume it is allowed; this function is really keyed to be used for a model executable.
     *
     * \internal This function is merely syntactic sugar over a call to the singleton that keeps the command line data.
     */
    overwrite_directory DoOverwriteDirectory();


    /*!
     * \brief Whether a message on standard output should tell whenever a global linear algebra is deleted.
     *
     * \return If yes, a line will be written givin away the name with which the object was set up.
     *
     * \internal This function is merely syntactic sugar over a call to the singleton that keeps the command line data.
     */
    Advanced::Wrappers::Petsc::print_linalg_destruction DoPrintLinearAlgebraDestruction();


} // namespace MoReFEM::Advanced::CommandLineFlagsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_COMMANDLINEFLAGS_ADVANCED_COMMANDLINEFLAGS_DOT_HPP_
// *** MoReFEM end header guards *** < //
