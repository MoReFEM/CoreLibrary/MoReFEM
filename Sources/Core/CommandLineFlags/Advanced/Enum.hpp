// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_COMMANDLINEFLAGS_ADVANCED_ENUM_DOT_HPP_
#define MOREFEM_CORE_COMMANDLINEFLAGS_ADVANCED_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Advanced::CommandLineFlagsNS
{


    /*!
     * \brief Enum to dictate how to handle already existing output directory at initialization.
     *
     * In the 'model' program type, default behaviour is to ask the user whether he wants to erase the output
     * directory if it already exists. This enum class acknowledges another choice might be made
     * (if --overwrite_directory is given on command line).
     */
    enum class overwrite_directory { yes, no };


} // namespace MoReFEM::Advanced::CommandLineFlagsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_COMMANDLINEFLAGS_ADVANCED_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
