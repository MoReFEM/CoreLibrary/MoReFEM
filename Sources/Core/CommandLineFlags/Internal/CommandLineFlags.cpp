// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Core/CommandLineFlags/Internal/CommandLineFlags.hpp"


namespace MoReFEM::Internal
{


    const std::string& CommandLineFlags::ClassName()
    {
        static const std::string ret("CommandLineFlags");
        return ret;
    }


    CommandLineFlags::CommandLineFlags(Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory,
                                       Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction)
    : do_overwrite_directory_{ do_overwrite_directory }, do_print_linalg_destruction_{ do_print_linalg_destruction }
    { }


    CommandLineFlags::~CommandLineFlags() = default;


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
