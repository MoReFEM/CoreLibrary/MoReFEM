// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_CRTP_NUMBERINGSUBSETFORMATRIX_DOT_HPP_
#define MOREFEM_CORE_CRTP_NUMBERINGSUBSETFORMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Crtp
{


    /*!
     * \brief This Crtp add two data attributes (const references to row and column numbering subsets) and accessors
     * to them.
     */
    template<class DerivedT>
    class NumberingSubsetForMatrix
    {

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] row_numbering_subset \a NumberingSubset to use to define the numbering of rows.
        //! \param[in] col_numbering_subset \a NumberingSubset to use to define the numbering of columns.
        explicit NumberingSubsetForMatrix(const NumberingSubset& row_numbering_subset,
                                          const NumberingSubset& col_numbering_subset);

        //! Destructor.
        ~NumberingSubsetForMatrix() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NumberingSubsetForMatrix(const NumberingSubsetForMatrix& rhs);

        //! \copydoc doxygen_hide_move_constructor
        NumberingSubsetForMatrix(NumberingSubsetForMatrix&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NumberingSubsetForMatrix& operator=(const NumberingSubsetForMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NumberingSubsetForMatrix& operator=(NumberingSubsetForMatrix&& rhs) = delete;

        ///@}


        //! Numbering subset used to describe rows.
        const NumberingSubset& GetRowNumberingSubset() const;

        //! Numbering subset used to describe columns.
        const NumberingSubset& GetColNumberingSubset() const;


      private:
        //! Numbering subset used to describe rows.
        const NumberingSubset& row_numbering_subset_;

        //! Numbering subset used to describe columns.
        const NumberingSubset& col_numbering_subset_;
    };


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/Crtp/NumberingSubsetForMatrix.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_CRTP_NUMBERINGSUBSETFORMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
