// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_CRTP_NUMBERINGSUBSETFORMATRIX_DOT_HXX_
#define MOREFEM_CORE_CRTP_NUMBERINGSUBSETFORMATRIX_DOT_HXX_
// IWYU pragma: private, include "Core/Crtp/NumberingSubsetForMatrix.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Crtp
{


    template<class DerivedT>
    NumberingSubsetForMatrix<DerivedT>::NumberingSubsetForMatrix(const NumberingSubset& row_numbering_subset,
                                                                 const NumberingSubset& col_numbering_subset)
    : row_numbering_subset_(row_numbering_subset), col_numbering_subset_(col_numbering_subset)
    { }


    template<class DerivedT>
    NumberingSubsetForMatrix<DerivedT>::NumberingSubsetForMatrix(const NumberingSubsetForMatrix<DerivedT>& rhs)
    : row_numbering_subset_(rhs.row_numbering_subset_), col_numbering_subset_(rhs.col_numbering_subset_)
    { }


    template<class DerivedT>
    inline const NumberingSubset& NumberingSubsetForMatrix<DerivedT>::GetRowNumberingSubset() const
    {
        return row_numbering_subset_;
    }


    template<class DerivedT>
    inline const NumberingSubset& NumberingSubsetForMatrix<DerivedT>::GetColNumberingSubset() const
    {
        return col_numbering_subset_;
    }


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_CRTP_NUMBERINGSUBSETFORMATRIX_DOT_HXX_
// *** MoReFEM end header guards *** < //
