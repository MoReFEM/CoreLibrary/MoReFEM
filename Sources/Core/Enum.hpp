// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_ENUM_DOT_HPP_
#define MOREFEM_CORE_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <cstdlib>


namespace MoReFEM
{


    //! Very high value to use in penalization schemes.
    constexpr double penalisation_very_high_value = 1.e20;


    /*!
     * \brief Whether models should compute the very first processor-wise local2global array for each
     * LocalFEltSpace/numbering subset combination found in FEltSpace.
     *
     */
    enum class DoConsiderProcessorWiseLocal2Global { no, yes };

    /*!
     * \brief If DoConsiderProcessorWiseLocal2Global is yes, each operator might decide whether it actually requires
     * the processor-wise local2global array or not.
     */
    enum class DoComputeProcessorWiseLocal2Global { no, yes };

    //! \todo #900 Temporary trick to choose default quadrature rules.
    constexpr const std::size_t DEFAULT_DEGREE_OF_EXACTNESS = 5;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
