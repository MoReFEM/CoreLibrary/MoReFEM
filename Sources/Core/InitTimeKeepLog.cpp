// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <fstream>
#include <ostream> // IWYU pragma: keep
#include <source_location>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Core/InitTimeKeepLog.hpp"

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/TimeKeep/TimeKeep.hpp"


namespace MoReFEM
{


    void InitTimeKeepLog(const FilesystemNS::Directory& result_directory)
    {
        assert(result_directory.DoExist());

        auto file = result_directory.AddFile("time_log.hhdata");

        std::ofstream out{ file.NewContent() };

        TimeKeep::CreateOrGetInstance(std::source_location::current(), std::move(out));
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
