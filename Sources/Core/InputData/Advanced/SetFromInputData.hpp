// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_ADVANCED_SETFROMINPUTDATA_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_ADVANCED_SETFROMINPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    /*!
     * \brief Create all the instances read from the input data of the type held by \a ManagerT.
     *
     * \code
     * SetFromInputDataAndModelSettings<DomainManager>(model_settings, input_data, domain_manager);
     * \endcode
     *
     * iterates through the entries of \a input_data and create all the domains encountered doing so.
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     * \copydoc doxygen_hide_cplusplus_variadic_args
     * These variadic arguments are here transmitted to the Create() static method arguments, not to \a ManagerT
     * constructor.
     *
     *
     * \param[in,out] manager The (singleton) manager which content will be set through this function. The singleton
     * must be created beforehand: this was not the case before but could lead to issues for singletons that takes
     * constructor arguments.
     */
    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        typename... Args
    >
    // clang-format on
    void SetFromInputDataAndModelSettings(const ModelSettingsT& model_settings,
                                          const InputDataT& input_data,
                                          ManagerT& manager,
                                          Args&&... args);


    /*!
     * \brief Create all the instances read from the input data of the type held by \a ManagerT in the
     * case we are loading prepartitioned data.
     *
     * For instance in case of \a DomainManager, it looks like:
     *
     * \code
     * SetFromPreprocessedData<DomainManager>(model_settings, input_data, domain_manager);
     * \endcode
     *
     * iterates through the entries of \a input_data and create all the domains encountered doing so.
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * \copydoc doxygen_hide_cplusplus_variadic_args
     * These variadic arguments are here transmitted to the Create() static method arguments, not to \a ManagerT
     * constructor.
     *
     * \param[in,out] manager The (singleton) manager which content will be set through this function. The singleton
     * must be created beforehand: this was not the case before but could lead to issues for singletons that takes
     * constructor arguments.
     */
    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        typename... Args
    >
    // clang-format on
    void SetFromPreprocessedData(const MoReFEMDataT& morefem_data, ManagerT& manager, Args&&... args);


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InputData/Advanced/SetFromInputData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_ADVANCED_SETFROMINPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
