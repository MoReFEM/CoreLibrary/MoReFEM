// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_ADVANCED_SETFROMINPUTDATA_DOT_HXX_
#define MOREFEM_CORE_INPUTDATA_ADVANCED_SETFROMINPUTDATA_DOT_HXX_
// IWYU pragma: private, include "Core/InputData/Advanced/SetFromInputData.hpp"
// *** MoReFEM header guards *** < //


#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        typename... Args
    >
    // clang-format on
    void SetFromInputDataAndModelSettings(const ModelSettingsT& model_settings,
                                          const InputDataT& input_data,
                                          ManagerT& manager,
                                          Args&&... args)
    {
        auto create =
            [&model_settings, &input_data, &manager, &args...](const auto& indexed_section_description) -> void
        {
            manager.Create(indexed_section_description, model_settings, input_data, std::forward<Args>(args)...);
        };


        // clang-format off
        using model_settings_tuple_iteration =
            ::MoReFEM::Internal::InputDataNS::TupleIteration
            <
                typename ModelSettingsT::underlying_tuple_type,
                0UL
            >;
        // clang-format on

        model_settings_tuple_iteration::template ActIfLeafInheritsFrom<typename ManagerT::indexed_section_tag>(
            model_settings.GetTuple(), input_data, create);
    }


    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        typename... Args
    >
    // clang-format on
    void SetFromPreprocessedData(const MoReFEMDataT& morefem_data, ManagerT& manager, Args&&... args)
    {
        auto load = [&morefem_data, &manager, &args...](const auto& indexed_section_description) -> void
        {
            manager.LoadFromPrepartitionedData(indexed_section_description, morefem_data, std::forward<Args>(args)...);
        };

        // clang-format off
        using model_settings_tuple_iteration =
            ::MoReFEM::Internal::InputDataNS::TupleIteration
            <
                typename MoReFEMDataT::model_settings_type::underlying_tuple_type,
                0UL
            >;
        // clang-format on

        model_settings_tuple_iteration::template ActIfLeafInheritsFrom<typename ManagerT::indexed_section_tag>(
            morefem_data.GetModelSettings().GetTuple(), morefem_data.GetInputData(), load);
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_ADVANCED_SETFROMINPUTDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
