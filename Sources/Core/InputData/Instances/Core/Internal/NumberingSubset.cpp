// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Core/Internal/NumberingSubset.hpp"


namespace MoReFEM::Internal::InputDataNS::NumberingSubsetNS
{

    const std::string& DoMoveMesh::NameInFile()
    {
        static const std::string ret("do_move_mesh");
        return ret;
    }


    const std::string& DoMoveMesh::Description()
    {
        static const std::string ret("Whether a vector defined on this numbering subset might be used to compute "
                                     "a movemesh. If true, a FEltSpace featuring this numbering subset will "
                                     "compute additional quantities to enable fast computation. This should be "
                                     "false for most numbering subsets, and when it's true the sole "
                                     "unknown involved should be a displacement.");
        return ret;
    }


    const std::string& DoMoveMesh::DefaultValue()
    {
        static const std::string ret("false");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
