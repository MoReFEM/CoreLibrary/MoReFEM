// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_CORE_NUMBERINGSUBSET_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_CORE_NUMBERINGSUBSET_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/Core/Internal/NumberingSubset.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct NumberingSubset : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                 NumberingSubset<IndexT>,
                                 IndexT,
                                 Internal::InputDataNS::NumberingSubsetNS::Tag,
                                 ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "NumberingSubset";
        }

        //! Convenient alias.
        using self = NumberingSubset<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Whether a vector defined on this numbering subset might be used to compute a
         * movemesh.
         */
        struct DoMoveMesh : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DoMoveMesh, self, bool>,
                            public ::MoReFEM::Internal::InputDataNS::NumberingSubsetNS::DoMoveMesh
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            // Left empty: in most cases DoMoveMesh if not used!
        >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct NumberingSubset


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

/*!
 * \brief Macro used in most models when defining a \a NumberingSubset in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, we do not want the user to meddle at all with numbering subset and the whole section is in \a
 * ModelSettings.
 *
 * \attention There are no NumberingSubset section here; as mandatory content is empty!
 * (DoMoveMesh is an optional field)
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(enum_class_id)                                           \
    ::MoReFEM::InputDataNS::NumberingSubset<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_CORE_NUMBERINGSUBSET_DOT_HPP_
// *** MoReFEM end header guards *** < //
