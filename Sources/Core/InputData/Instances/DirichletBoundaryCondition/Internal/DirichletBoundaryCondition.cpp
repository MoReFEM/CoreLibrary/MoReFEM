// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/DirichletBoundaryCondition/Internal/DirichletBoundaryCondition.hpp"


namespace MoReFEM::Internal::InputDataNS::BoundaryConditionNS
{


    const std::string& UnknownName::NameInFile()
    {
        static const std::string ret("unknown");
        return ret;
    }


    const std::string& UnknownName::Description()
    {
        static const std::string ret("Name of the unknown addressed by the boundary condition.");
        return ret;
    }


    const std::string& Component::NameInFile()
    {
        static const std::string ret("component");
        return ret;
    }


    const std::string& Component::Description()
    {
        static const std::string ret("Comp1, Comp2 or Comp3");
        return ret;
    }

    const std::string& Component::Constraint()
    {
        static const std::string ret(
            "value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})");
        return ret;
    }


    const std::string& Values::NameInFile()
    {
        static const std::string ret("value");
        return ret;
    }


    const std::string& Values::Description()
    {
        static const std::string ret("Values at each of the relevant component.");
        return ret;
    }

    const std::string& DomainIndex::NameInFile()
    {
        static const std::string ret("domain_index");
        return ret;
    }


    const std::string& DomainIndex::Description()
    {
        static const std::string ret("Index of the domain onto which essential boundary condition is defined.");
        return ret;
    }

    const std::string& IsMutable::NameInFile()
    {
        static const std::string ret("is_mutable");
        return ret;
    }


    const std::string& IsMutable::Description()
    {
        static const std::string ret("Whether the values of the boundary condition may vary over time.");
        return ret;
    }


    const std::string& IsMutable::DefaultValue()
    {
        static const std::string ret("false");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
