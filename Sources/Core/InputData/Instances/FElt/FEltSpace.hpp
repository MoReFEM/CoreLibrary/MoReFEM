// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_FELT_FELTSPACE_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_FELT_FELTSPACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

// IWYU pragma: begin_exports
#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/FElt/Internal/FEltSpace.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct FEltSpace : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                           FEltSpace<IndexT>,
                           IndexT,
                           Internal::InputDataNS::FEltSpaceNS::Tag,
                           ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "FiniteElementSpace";
        }

        //! Convenient alias.
        using self = FEltSpace<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
            FEltSpace<IndexT>,
            IndexT,
            Internal::InputDataNS::FEltSpaceNS::Tag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Indicates the god of dof that possess the finite element space.
         *
         * God of dof index is the same as mesh one.
         */
        struct GodOfDofIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<GodOfDofIndex, self, std::size_t>,
                               public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::GodOfDofIndex
        { };


        //! Indicates the domain upon which the finite element space is defined. This domain must be
        //! uni-dimensional.
        struct DomainIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndex, self, std::size_t>,
                             public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::DomainIndex
        { };


        //! Indicates which unknowns are defined on the finite element space.
        struct UnknownList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<UnknownList, self, std::vector<std::string>>,
          public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::UnknownList
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates for each unknowns the shape function to use.
        struct ShapeFunctionList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ShapeFunctionList, self, std::vector<std::string>>,
          public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::ShapeFunctionList
        { };


        //! Indicates the numbering subset to use for each unknown.
        struct NumberingSubsetList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<NumberingSubsetList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::NumberingSubsetList
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            GodOfDofIndex,
            DomainIndex,
            UnknownList,
            ShapeFunctionList,
            NumberingSubsetList
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct FEltSpace


/*!
 * \brief Macro used in most models when defining a \a FEltSpace in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, finite element spaces are tightly defined by the author of the model and the only field upon which we
 * want to let the end user deal with is the shape function to use.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(enum_class_id)                                                 \
    ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription,                   \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::GodOfDofIndex,                           \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::UnknownList,                             \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::DomainIndex,                             \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::NumberingSubsetList

/*!
 * \brief Macro used in most models when defining a \a FEltSpace in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, the only freedom we want to let about the finite element spaces is the shape function to use.
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(enum_class_id)                                                     \
    ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::ShapeFunctionList


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_FELT_FELTSPACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
