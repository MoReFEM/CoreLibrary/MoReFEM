// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/FElt/Internal/FEltSpace.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::Internal::InputDataNS::FEltSpaceNS
{

    const std::string& GodOfDofIndex::NameInFile()
    {
        static const std::string ret("god_of_dof_index");
        return ret;
    }


    const std::string& GodOfDofIndex::Description()
    {
        static const std::string ret("Index of the god of dof into which the finite element space is defined.");
        return ret;
    }


    const std::string& DomainIndex::NameInFile()
    {
        static const std::string ret("domain_index");
        return ret;
    }


    const std::string& DomainIndex::Description()
    {
        static const std::string ret("Index of the domain onto which the finite element space is defined. "
                                     "This domain must be unidimensional.");

        return ret;
    }


    const std::string& UnknownList::NameInFile()
    {
        static const std::string ret("unknown_list");
        return ret;
    }


    const std::string& UnknownList::Description()
    {
        static const std::string ret("List of all unknowns defined in the finite element space. "
                                     "Unknowns here must be defined in this file as an 'Unknown' block; "
                                     "expected name/identifier is the name given there.");

        return ret;
    }


    const std::string& ShapeFunctionList::NameInFile()
    {
        static const std::string ret("shape_function_list");
        return ret;
    }


    const std::string& ShapeFunctionList::Description()
    {
        static const std::string ret("List of the shape function to use for each unknown;");
        return ret;
    }


    const std::string& NumberingSubsetList::NameInFile()
    {
        static const std::string ret("numbering_subset_list");
        return ret;
    }


    const std::string& NumberingSubsetList::Description()
    {
        static const std::string ret("List of the numbering subset to use for each unknown;");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
