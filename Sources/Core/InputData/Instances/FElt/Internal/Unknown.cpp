// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/FElt/Internal/Unknown.hpp"


namespace MoReFEM::Internal::InputDataNS::UnknownNS
{


    const std::string& Name::NameInFile()
    {
        static const std::string ret("name");
        return ret;
    }


    const std::string& Name::Description()
    {
        static const std::string ret("Name of the unknown (used for displays in output).");
        return ret;
    }


    const std::string& Nature::NameInFile()
    {
        static const std::string ret("nature");
        return ret;
    }


    const std::string& Nature::Description()
    {
        static const std::string ret("Index of the god of dof into which the finite element space is defined.");
        return ret;
    }


    const std::string& Nature::Constraint()
    {
        static const std::string ret("value_in(v, {'scalar', 'vectorial'})");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::UnknownNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
