// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_COORDSMATCHINGFILE_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_COORDSMATCHINGFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Geometry/Internal/CoordsMatchingFile.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    class CoordsMatchingFile : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                   CoordsMatchingFile<IndexT>,
                                   IndexT,
                                   Internal::InputDataNS::CoordsMatchingFileNS::Tag,
                                   ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
      public:
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "CoordsMatchingFile";
        }


        //! Convenient alias.
        using self = CoordsMatchingFile<IndexT>;

        //! Friendship to section parent.
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Indicates the mesh upon which the domain is defined (if any).
        struct Path : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Path, self, std::string>,
                      public ::MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS::Path
        { };


        //! Indicates both meshes involved in the interpolation
        struct DoComputeReverse : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DoComputeReverse, self, bool>,
                                  public ::MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS::DoComputeReverse
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                Path,
                DoComputeReverse
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Domain


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_COORDSMATCHINGFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
