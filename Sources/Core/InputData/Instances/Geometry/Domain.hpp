// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_DOMAIN_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_DOMAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/InputData/Instances/Geometry/Internal/Domain.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    class Domain : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                       Domain<IndexT>,
                       IndexT,
                       Internal::InputDataNS::DomainNS::Tag,
                       ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
      public:
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "Domain";
        }

        //! Convenient alias.
        using self = Domain<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
            Domain<IndexT>,
            IndexT,
            Internal::InputDataNS::DomainNS::Tag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Indicates the mesh upon which the domain is defined (if any).
        struct MeshIndexList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshIndexList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::DomainNS::MeshIndexList
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates the list of dimensions considered in the domain (if left empty all of them).
        struct DimensionList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DimensionList, self, std::vector<Eigen::Index>>,
          public ::MoReFEM::Internal::InputDataNS::DomainNS::DimensionList
        { };


        //! Indicates the list of mesh labels considered in the domain (if left empty all of them).
        struct MeshLabelList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshLabelList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::DomainNS::MeshLabelList
        { };


        //! Indicates the list of geometric element types considered in the domain (if left empty all of them).
        struct GeomEltTypeList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<GeomEltTypeList, self, std::vector<std::string>>,
          public ::MoReFEM::Internal::InputDataNS::DomainNS::GeomEltTypeList
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            MeshIndexList,
            DimensionList,
            MeshLabelList,
            GeomEltTypeList
        >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Domain


/*!
 * \brief Macro used in most models when defining a \a Domain in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, when a \a Domain is defined we do not want to let the meshes that are covered by the \a Domain be
 * modified by the end users.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(enum_class_id)                                                     \
    ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription,                      \
        ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(enum_class_id)>::MeshIndexList

/*!
 * \brief Macro used in most models when defining a \a Domain in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, the following fields are usually  modifiable by the end-user:
 * - The dimension(s) considered
 * - The mesh labels (called 'references' in the scientific computing community) concerned, which are specific to the
 * mesh that is provided by the end-user.
 * - The type of geometric elements that are considered (this field is usually left empty to indicate no restrictions
 * are applied).
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(enum_class_id)                                                         \
    ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(enum_class_id)>::DimensionList,                                  \
        ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(enum_class_id)>::MeshLabelList,                              \
        ::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(enum_class_id)>::GeomEltTypeList


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_DOMAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
