// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Geometry/Internal/CoordsMatchingFile.hpp"


namespace MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS
{


    const std::string& Path::NameInFile()
    {
        static const std::string ret("path");
        return ret;
    }


    const std::string& Path::Description()
    {
        static const std::string ret("File that gives for each Coords on the first mesh on the interface the index of "
                                     "the equivalent Coords in the second mesh.");

        return ret;
    }


    const std::string& DoComputeReverse::NameInFile()
    {
        static const std::string ret("do_compute_reverse");
        return ret;
    }


    const std::string& DoComputeReverse::Description()
    {
        static const std::string ret(
            "In the interpolation file, there are two meshes involved: one 'source' and one "
            "'target'. If this field is set to true, two CoordsMatching objects are created: "
            "one source -> target and one target -> source. If false, only the former is built. ");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
