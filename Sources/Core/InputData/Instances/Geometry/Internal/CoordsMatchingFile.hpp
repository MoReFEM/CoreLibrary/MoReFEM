// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_INTERNAL_COORDSMATCHINGFILE_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_INTERNAL_COORDSMATCHINGFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS
{


    //! \copydoc doxygen_hide_indexed_section_tag_alias
    struct Tag
    { };


    //! Helper class that defines non template static functions of namesake class in CoordsMatchingFile
    //! namespace.
    struct Path
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    //! Helper class that defines non template static functions of namesake class in CoordsMatchingFile
    //! namespace.
    struct DoComputeReverse
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


} // namespace MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_INTERNAL_COORDSMATCHINGFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
