// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Core/InputData/Instances/Geometry/Internal/LightweightDomainList.hpp"


namespace MoReFEM::Internal::InputDataNS::LightweightDomainListNS
{

    const std::string& MeshIndex::NameInFile()
    {
        static const std::string ret("mesh_index");
        return ret;
    }


    const std::string& MeshIndex::Description()
    {
        static const std::string ret("Index of the mesh onto which current domains are defined.");
        return ret;
    }


    const std::string& NumberInDomainList::NameInFile()
    {
        static const std::string ret("number_in_domain_list");
        return ret;
    }


    const std::string& NumberInDomainList::Description()
    {
        static const std::string ret("Number of mesh labels to consider in each domain. Sum of these numbers "
                                     "must be equal to the number of entries in mesh label list.");

        return ret;
    }


    const std::string& DomainIndexList::NameInFile()
    {
        static const std::string ret("domain_index_list");
        return ret;
    }


    const std::string& DomainIndexList::Description()
    {
        static const std::string ret("Give an unique id to each of the shorthand domains defined. These must not "
                                     "clash with each other or with domains defined by a more conventional way.");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::LightweightDomainListNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
