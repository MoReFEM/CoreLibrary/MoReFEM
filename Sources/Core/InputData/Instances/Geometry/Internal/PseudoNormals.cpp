// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Geometry/Internal/PseudoNormals.hpp"


namespace MoReFEM::Internal::InputDataNS::PseudoNormalsNS
{


    const std::string& MeshIndexImpl::NameInFile()
    {
        static const std::string ret("mesh_index");
        return ret;
    }


    const std::string& MeshIndexImpl::Description()
    {
        static const std::string ret("Index of the geometric mesh upon which to compute pseudo-normals."
                                     "At most one value is expected here.");
        return ret;
    }


    const std::string& DomainIndexListImpl::NameInFile()
    {
        static const std::string ret("domain_index_list");
        return ret;
    }

    const std::string& DomainIndexListImpl::Description()
    {
        static const std::string ret("Index of the domain of the mesh upon which to compute pseudo-normals."
                                     "Might be left empty if the computation is not limited to one domain.");
        return ret;
    }


    const std::string& DomainIndexListImpl::DefaultValue()
    {
        static const std::string ret("{}");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::PseudoNormalsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
