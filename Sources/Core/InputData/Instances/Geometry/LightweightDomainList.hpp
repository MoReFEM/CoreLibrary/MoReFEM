// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_LIGHTWEIGHTDOMAINLIST_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_LIGHTWEIGHTDOMAINLIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Geometry/Internal/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Internal/LightweightDomainList.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    class LightweightDomainList : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                      LightweightDomainList<IndexT>,
                                      IndexT,
                                      Internal::InputDataNS::LightweightDomainListNS::Tag,
                                      ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
      public:
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "LightweightDomainList";
        }

        //! Convenient alias.
        using self = LightweightDomainList<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
            LightweightDomainList<IndexT>,
            IndexT,
            Internal::InputDataNS::LightweightDomainListNS::Tag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Indicates the mesh upon which the domain is defined (if any).
        struct MeshIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshIndex, self, std::size_t>,
                           public Internal::InputDataNS::LightweightDomainListNS::MeshIndex
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates the list of mesh labels considered in the variable domains. Separators then will split
        //! them into different domains.
        struct MeshLabelList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshLabelList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::DomainNS::MeshLabelList
        { };


        struct DomainIndexList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndexList, self, std::vector<std::size_t>>,
          public Internal::InputDataNS::LightweightDomainListNS::DomainIndexList
        { };


        struct NumberInDomainList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<NumberInDomainList, self, std::vector<std::size_t>>,
          public Internal::InputDataNS::LightweightDomainListNS::NumberInDomainList
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                MeshIndex,
                MeshLabelList,
                DomainIndexList,
                NumberInDomainList
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // class LightweightDomainList


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_LIGHTWEIGHTDOMAINLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
