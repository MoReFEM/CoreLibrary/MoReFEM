// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_MESH_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_MESH_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/InputData/Instances/Geometry/Internal/Mesh.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    /*!
     * \brief Holds information related to the input datum Mesh::inputMesh.
     *
     * \tparam IndexT Several meshes can be present in the input data file, provided they each
     * use a different value for this template parameter. For instance:
     *
     * \code
     * Mesh1 = {
     * mesh = "/Volumes/Data/sebastien/Freefem/Elasticity/Data/Medit/elasticity_Nx50_Ny20_force_label.mesh",
     * ...
     * }
     *
     * Mesh2 = {
     * mesh = "/Volumes/Data/sebastien/Freefem/Elasticity/Data/Medit/elasticity_Nx50_Ny20.mesh",
     * ...
     * }
     * \endcode
     */
    // clang-format off
    template<std::size_t IndexT>
    struct Mesh
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection
    <
        Mesh<IndexT>,
        IndexT,
        Internal::InputDataNS::MeshNS::Tag,
        ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection
    >
    // clang-format on
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "Mesh";
        }

        //! Convenient alias.
        using self = Mesh<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Path of the mesh file.
         */
        struct Path : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Path, self, std::filesystem::path>,
                      public ::MoReFEM::Internal::InputDataNS::MeshNS::PathImpl
        { };


        /*!
         * \brief Format of the mesh file.
         */
        struct Format : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Format, self, std::string>,
                        public ::MoReFEM::Internal::InputDataNS::MeshNS::FormatImpl
        { };


        //! Holds information related to the format of the mesh.
        struct Dimension : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Dimension, self, Eigen::Index>,
                           public ::MoReFEM::Internal::InputDataNS::MeshNS::DimensionImpl
        { };

        //! Holds information related to the format of the mesh.
        struct SpaceUnit : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<SpaceUnit, self, double>,
                           public ::MoReFEM::Internal::InputDataNS::MeshNS::SpaceUnitImpl
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Path,
            Format,
            Dimension,
            SpaceUnit
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Mesh


/*!
 * \brief Macro used in most models when defining a \a Mesh in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all mesh-related settings are defined in the input data tuple (save the \a IndexedSectionDescription
 * expected in \a ModelSettings).
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(enum_class_id)                                                       \
    ::MoReFEM::InputDataNS::Mesh<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription


/*!
 * \brief Macro used in most models when defining a \a Mesh in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all mesh-related settings are defined in the input data tuple (save the \a IndexedSectionDescription
 * expected in \a ModelSettings).
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(enum_class_id)                                                           \
    ::MoReFEM::InputDataNS::Mesh<EnumUnderlyingType(enum_class_id)>


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_MESH_DOT_HPP_
// *** MoReFEM end header guards *** < //
