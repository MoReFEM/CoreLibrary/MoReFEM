// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_PSEUDONORMALS_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_PSEUDONORMALS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Geometry/Internal/PseudoNormals.hpp"


namespace MoReFEM::InputDataNS
{


    struct PseudoNormalsTag
    { };


    /*!
     * \brief Holds information related to the input datum PseudoNormals.
     *
     * \tparam IndexT Several PseudoNormals can be present in the input data file, provided they each
     * use a different value for this template parameter. For instance:
     *
     * \code
     * PseudoNormals1 = {
     * mesh_index = 1
     * ...
     * }
     *
     * PseudoNormals2 = {
     * mesh_index = 2
     * ...
     * }
     * \endcode
     */
    template<std::size_t IndexT>
    struct PseudoNormals : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                               PseudoNormals<IndexT>,
                               IndexT,
                               PseudoNormalsTag,
                               ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "PseudoNormals";
        }

        //! Convenient alias.
        using self = PseudoNormals<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Indicates the mesh upon which the pseudo-normals are defined (if any).
         */
        struct MeshIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshIndex, self, std::size_t>,
                           public ::MoReFEM::Internal::InputDataNS::PseudoNormalsNS::MeshIndexImpl
        { };


        /*!
         * \brief Indicates the domain upon which the pseudo-normals are defined (if any).
         */
        struct DomainIndexList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndexList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::PseudoNormalsNS::DomainIndexListImpl
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            MeshIndex,
            DomainIndexList
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct PseudoNormals


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_GEOMETRY_PSEUDONORMALS_DOT_HPP_
// *** MoReFEM end header guards *** < //
