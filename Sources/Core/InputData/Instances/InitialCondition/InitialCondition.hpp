// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_INITIALCONDITION_INITIALCONDITION_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_INITIALCONDITION_INITIALCONDITION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/ThreeDimensionalCompoundParameter.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    struct InitialConditionTag
    { };


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    // clang-format off
    template<std::size_t IndexT>
    struct InitialCondition
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection
    <
        InitialCondition<IndexT>,
        IndexT,
        InitialConditionTag,
        ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection
    >
    // clang-format on
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "InitialCondition";
        }

        //! Convenient alias.
        using self = InitialCondition<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        // clang-format off
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection
        <
            InitialCondition<IndexT>,
            IndexT,
            InitialConditionTag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection
        >;
        // clang-format on

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());
        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Choose how is described the initial condition (through a scalar, a function, etc...)
         */
        struct Nature : public Internal::InputDataNS::ParamNS::
                            ThreeDimensionalCompoundNature<Nature, self, ::MoReFEM::ParameterNS::Type::vector>
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        /*!
         * \brief Value associated to the initial condition.
         */
        struct Value : Internal::InputDataNS::ParamNS::ThreeDimensionalCompoundValue<Value, Nature>
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            Value
        >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct InitialCondition


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

/*!
 * \brief Macro used in most models when defining a \a InitialCondition section in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all fields save the \a IndexedSectionDescription  one are modifiable by the end-user.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_INITIAL_CONDITION(enum_class_id)                                          \
    ::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription


/*!
 * \brief Macro used in most models when defining a \a InitialCondition section in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all mesh-related settings are defined in the input data tuple (save the \a IndexedSectionDescription
 * expected in \a ModelSettings).
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_INITIAL_CONDITION(enum_class_id)                                              \
    ::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(enum_class_id)>::Nature,                               \
        ::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(enum_class_id)>::Value


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_INITIALCONDITION_INITIALCONDITION_DOT_HPP_
// *** MoReFEM end header guards *** < //
