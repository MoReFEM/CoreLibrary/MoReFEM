// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/InitialConditionGate.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& InitialConditionGate::GetName()
    {
        static const std::string ret("InitialConditionGate");
        return ret;
    }


    const std::string& InitialConditionGate::Value::NameInFile()
    {
        static const std::string ret("initial_condition_gate");
        return ret;
    }


    const std::string& InitialConditionGate::Value::Description()
    {
        static const std::string ret("Value of Initial Condition Gate in ReactionDiffusion.");
        return ret;
    }


    const std::string& InitialConditionGate::Value::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }


    const std::string& InitialConditionGate::WriteGate::NameInFile()
    {
        static const std::string ret("write_gate");
        return ret;
    }


    const std::string& InitialConditionGate::WriteGate::Description()
    {
        static const std::string ret("Either Write the Gate at each time step or not.");
        return ret;
    }


    const std::string& InitialConditionGate::WriteGate::DefaultValue()
    {
        static const std::string ret("true");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
