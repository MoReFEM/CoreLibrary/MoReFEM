/*!
 * \class doxygen_hide_core_input_data_section
 *
 * \brief Define a section for the input data file.
 *
 * It should include a private tuple named "section_content_" which includes the enclosed sections or parameters
 * and should inherit from InputData::Crtp::Section template class.
 */


/*!
 * \class doxygen_hide_core_input_data_section_with_index
 *
 * \copydoc doxygen_hide_core_input_data_section
 *
 * \tparam IndexT Index of the section considered, when several might be present in the input data file.
 *  For instance, as there might be several domains, Domain is a template class with the index as a template argument.
 * Within the input data file, the section name is for instance "Domain1" for Domain<1>.
 */


/*!
 * \class doxygen_hide_core_input_data_parameter
 *
 * \brief Define a parameter for the input data file.
 *
 * It should define the following static methods:
 * - static const std::string& NameInFile();
 * - static const std::string& Description();
 * - static const std::string& Constraint();
 * - static const std::string& DefaultValue();
 */
