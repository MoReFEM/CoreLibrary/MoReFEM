// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_INTERPOLATOR_COORDSMATCHINGINTERPOLATOR_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_INTERPOLATOR_COORDSMATCHINGINTERPOLATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <string>
#include <vector>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Interpolator/Internal//CoordsMatchingInterpolator.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{

    /*!
     * \brief Holds information related to the input datum CoordsMatchingInterpolator.
     *
     */
    template<std::size_t IndexT>
    struct CoordsMatchingInterpolator : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                            CoordsMatchingInterpolator<IndexT>,
                                            IndexT,
                                            Internal::InputDataNS::CoordsMatchingInterpolatorNS::Tag,
                                            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "CoordsMatchingInterpolator";
        }

        //! Convenient alias.
        using self = CoordsMatchingInterpolator<IndexT>;


        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Source finite element space.
         */
        struct SourceFEltSpaceIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<SourceFEltSpaceIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::SourceFEltSpaceIndexImpl
        { };


        /*!
         * \brief \a NumberingSubset for the source.
         */
        struct SourceNumberingSubsetIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<SourceNumberingSubsetIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::SourceNumberingSubsetIndexImpl
        { };


        /*!
         * \brief Target finite element space.
         */
        struct TargetFEltSpaceIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TargetFEltSpaceIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::TargetFEltSpaceIndexImpl
        { };


        /*!
         * \brief \a NumberingSubset for the target.
         */
        struct TargetNumberingSubsetIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TargetNumberingSubsetIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::TargetNumberingSubsetIndexImpl
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                SourceFEltSpaceIndex,
                SourceNumberingSubsetIndex,
                TargetFEltSpaceIndex,
                TargetNumberingSubsetIndex
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Mesh


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_INTERPOLATOR_COORDSMATCHINGINTERPOLATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
