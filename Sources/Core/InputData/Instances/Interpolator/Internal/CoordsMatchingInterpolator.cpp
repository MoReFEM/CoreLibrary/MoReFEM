// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Interpolator/Internal//CoordsMatchingInterpolator.hpp"


namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS
{


    const std::string& SourceFEltSpaceIndexImpl::NameInFile()
    {
        static const std::string ret("source_finite_element_space");
        return ret;
    }


    const std::string& SourceFEltSpaceIndexImpl::Description()
    {
        static const std::string ret(
            "Source finite element space for which the dofs index will be associated to each Coords.");
        return ret;
    }


    const std::string& SourceNumberingSubsetIndexImpl::NameInFile()
    {
        static const std::string ret("source_numbering_subset");
        return ret;
    }


    const std::string& SourceNumberingSubsetIndexImpl::Description()
    {
        static const std::string ret(
            "Source numbering subset for which the dofs index will be associated to each Coords.");
        return ret;
    }

    const std::string& TargetFEltSpaceIndexImpl::NameInFile()
    {
        static const std::string ret("target_finite_element_space");
        return ret;
    }


    const std::string& TargetFEltSpaceIndexImpl::Description()
    {
        static const std::string ret(
            "Target finite element space for which the dofs index will be associated to each Coords.");
        return ret;
    }


    const std::string& TargetNumberingSubsetIndexImpl::NameInFile()
    {
        static const std::string ret("target_numbering_subset");
        return ret;
    }


    const std::string& TargetNumberingSubsetIndexImpl::Description()
    {
        static const std::string ret(
            "Target numbering subset for which the dofs index will be associated to each Coords.");
        return ret;
    }

} // namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
