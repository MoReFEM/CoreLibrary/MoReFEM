// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_INTERPOLATOR_INTERNAL_COORDSMATCHINGINTERPOLATOR_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_INTERPOLATOR_INTERNAL_COORDSMATCHINGINTERPOLATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS
{


    //! \copydoc doxygen_hide_indexed_section_tag_alias
    struct Tag
    { };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct SourceFEltSpaceIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct SourceNumberingSubsetIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct TargetFEltSpaceIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct TargetNumberingSubsetIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


} // namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_INTERPOLATOR_INTERNAL_COORDSMATCHINGINTERPOLATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
