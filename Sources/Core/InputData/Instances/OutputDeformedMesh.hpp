// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_OUTPUTDEFORMEDMESH_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_OUTPUTDEFORMEDMESH_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{


    struct OutputDeformedMeshTag
    { };


    //! \copydoc doxygen_hide_core_input_data_section
    template<std::size_t IndexT>
    struct OutputDeformedMesh : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                    OutputDeformedMesh<IndexT>,
                                    IndexT,
                                    OutputDeformedMeshTag,
                                    ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! Convenient alias.
        using self = OutputDeformedMesh<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<OutputDeformedMesh,
                                                            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "OutputDeformedMesh";
        }

        //! Output directory in which results will be written.
        struct OutputName : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputName, self, std::string>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Output directory in which results will be written.
        struct OutputFormat : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputFormat, self, std::string>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some text
             * from \a OptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Output directory in which results will be written.
        struct OutputSpaceUnit : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputSpaceUnit, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };

        //! Output directory in which results will be written.
        struct OutputOffset : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputOffset, self, std::size_t>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };

        //! Output frequence to create meshes.
        struct OutputFrequence : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputFrequence, self, std::size_t>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                OutputName,
                OutputFormat,
                OutputSpaceUnit,
                OutputOffset,
                OutputFrequence
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Result


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InputData/Instances/OutputDeformedMesh.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_OUTPUTDEFORMEDMESH_DOT_HPP_
// *** MoReFEM end header guards *** < //
