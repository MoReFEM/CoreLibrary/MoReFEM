// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_OUTPUTDEFORMEDMESH_DOT_HXX_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_OUTPUTDEFORMEDMESH_DOT_HXX_
// IWYU pragma: private, include "Core/InputData/Instances/OutputDeformedMesh.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::InputDataNS
{


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputName::NameInFile()
    {
        static const std::string ret("output_name");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputName::Description()
    {
        static const std::string ret("Name of the output deformed mesh.");
        return ret;
    }

    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputName::DefaultValue()
    {
        static const std::string ret("\"output_mesh\"");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputFormat::NameInFile()
    {
        static const std::string ret("output_format");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputFormat::Description()
    {
        static const std::string ret("Format of the output deformed mesh.");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputFormat::Constraint()
    {
        static const std::string ret("value_in(v, {'Ensight', 'Medit'})");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputFormat::DefaultValue()
    {
        static const std::string ret("\"Medit\"");
        return ret;
    }

    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputSpaceUnit::NameInFile()
    {
        static const std::string ret("output_space_unit");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputSpaceUnit::Description()
    {
        static const std::string ret("Space unit of the output mesh relative to the real unit of the input mesh, "
                                     "(output_space_unit / mesh_space_unit).");
        return ret;
    }

    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputSpaceUnit::DefaultValue()
    {
        static const std::string ret("1.");
        return ret;
    }

    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputOffset::NameInFile()
    {
        static const std::string ret("output_offset");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputOffset::Description()
    {
        static const std::string ret("Index of the iteration you want to start the output.");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputOffset::DefaultValue()
    {
        static const std::string ret("1");
        return ret;
    }

    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputFrequence::NameInFile()
    {
        static const std::string ret("output_frequence");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputFrequence::Description()
    {
        static const std::string ret("Frequence of the iterations you want the output.");
        return ret;
    }

    template<std::size_t IndexT>
    const std::string& OutputDeformedMesh<IndexT>::OutputFrequence::DefaultValue()
    {
        static const std::string ret("1");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_OUTPUTDEFORMEDMESH_DOT_HXX_
// *** MoReFEM end header guards *** < //
