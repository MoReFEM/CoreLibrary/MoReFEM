// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Parallelism/Internal/Parallelism.hpp"


namespace MoReFEM::Internal::InputDataNS::ParallelismNS
{


    const std::string& Policy::NameInFile()
    {
        static const std::string ret("policy");
        return ret;
    }


    const std::string& Policy::Description()
    {
        static const std::string ret(
            "What should be done for a parallel run. There are 4 possibilities:\n"
            " 'Precompute': Precompute the data for a later parallel run and stop once it's done.\n"
            " 'ParallelNoWrite': Run the code in parallel without using any pre-processed data "
            "and do not write down the processed data.\n"
            " 'Parallel': Run the code in parallel without using any pre-processed data and write "
            "down the processed data.\n"
            " 'RunFromPreprocessed': Run the code in parallel using pre-processed data.");
        return ret;
    }


    const std::string& Policy::Constraint()
    {
        static const std::string ret(
            "value_in(v, {'Precompute', 'ParallelNoWrite', 'Parallel', 'RunFromPreprocessed'})");
        return ret;
    }


    const std::string& Policy::DefaultValue()
    {
        static const std::string ret("'ParallelNoWrite'");
        return ret;
    }


    const std::string& Directory::NameInFile()
    {
        static const std::string ret("directory");
        return ret;
    }


    const std::string& Directory::Description()
    {
        static const std::string ret(
            "Directory in which parallelism data will be written or read (depending on the policy).");
        return ret;
    }


    const std::string& Directory::DefaultValue()
    {
        static const std::string ret("''"); // not empty string on purpose: I want the default value to be this!
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::ParallelismNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
