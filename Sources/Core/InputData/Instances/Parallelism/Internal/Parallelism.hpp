// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARALLELISM_INTERNAL_PARALLELISM_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARALLELISM_INTERNAL_PARALLELISM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Internal::InputDataNS::ParallelismNS
{


    /*!
     * \brief Choice of the parallel behaviour.
     *
     * \copydoc doxygen_hide_parallelism_input_data_cases
     */
    struct Policy
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some text from
         * \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no value has
         * been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    /*!
     * \brief Path that might be useful (depending on the \a Policy choice).
     */
    struct Directory
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no value has
         * been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::Internal::InputDataNS::ParallelismNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARALLELISM_INTERNAL_PARALLELISM_DOT_HPP_
// *** MoReFEM end header guards *** < //
