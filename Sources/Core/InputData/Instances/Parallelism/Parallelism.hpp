// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARALLELISM_PARALLELISM_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARALLELISM_PARALLELISM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Parallelism/Internal/Parallelism.hpp"


namespace MoReFEM::InputDataNS
{


    /*!
     * \brief Input data which says how the program should do in its run.
     *
     * \copydoc doxygen_hide_parallelism_input_data_cases
     */
    struct Parallelism
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Parallelism,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        /*!
         * \brief Return the name of the section in the input datum.
         *
         * e.g. 'Mesh1' for IndexT = 1.
         *
         * \return Name of the section in the input datum.
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = Parallelism;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Choice of the parallel behaviour.
         *
         * \copydoc doxygen_hide_parallelism_input_data_cases
         */
        struct Policy : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Policy, self, std::string>,
                        public Internal::InputDataNS::ParallelismNS::Policy
        { };


        /*!
         * \brief Path that might be useful (depending on the \a Policy choice).
         */
        struct Directory : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Directory, self, std::string>,
                           public Internal::InputDataNS::ParallelismNS::Directory
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Policy,
            Directory
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;
    };


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARALLELISM_PARALLELISM_DOT_HPP_
// *** MoReFEM end header guards *** < //
