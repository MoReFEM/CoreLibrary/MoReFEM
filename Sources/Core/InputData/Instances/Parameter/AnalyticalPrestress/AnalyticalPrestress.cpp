// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Parameter/AnalyticalPrestress/AnalyticalPrestress.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& AnalyticalPrestress::GetName()
    {
        static const std::string ret("AnalyticalPrestress");
        return ret;
    }


    const std::string& AnalyticalPrestress::Contractility::GetName()
    {
        static const std::string ret("Contractility");
        return ret;
    }


    const std::string& AnalyticalPrestress::InitialCondition::GetName()
    {
        static const std::string ret("InitialCondition");
        return ret;
    }


    const std::string& AnalyticalPrestress::InitialCondition::ActiveStress::NameInFile()
    {
        static const std::string ret("ActiveStress");
        return ret;
    }


    const std::string& AnalyticalPrestress::InitialCondition::ActiveStress::Description()
    {
        static const std::string ret("Active Stress initial condition.");
        return ret;
    }


    const std::string& AnalyticalPrestress::InitialCondition::ActiveStress::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
