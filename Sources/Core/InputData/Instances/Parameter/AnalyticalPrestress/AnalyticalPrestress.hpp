// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_ANALYTICALPRESTRESS_ANALYTICALPRESTRESS_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_ANALYTICALPRESTRESS_ANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"    // IWYU pragma: export
#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export

namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct AnalyticalPrestress
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<AnalyticalPrestress,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('ActiveStress' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = AnalyticalPrestress;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! \copydoc doxygen_hide_core_input_data_section
        struct Contractility
        : public Internal::InputDataNS::ParamNS::ScalarParameter<Contractility, AnalyticalPrestress>
        {


            //! Convenient alias.
            using self = Contractility;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, AnalyticalPrestress>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
            static const std::string& GetName();


        }; // struct Contractility


        //! \copydoc doxygen_hide_core_input_data_section
        struct InitialCondition
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<InitialCondition, AnalyticalPrestress>
        {

            /*!
             * \brief Return the name of the section in the input datum ('InitialCondition' here).
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = InitialCondition;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, AnalyticalPrestress>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct ActiveStress : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ActiveStress, self, double>
            {


                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            //! Alias to the tuple of structs.
            // clang-format off
                using section_content_type = std::tuple
                <
                    ActiveStress
                >;
            // clang-format on

          private:
            //! Content of the section.
            section_content_type section_content_;

        }; // struct InitialCondition


        //! Alias to the tuple of structs.
        using section_content_type = std::tuple<Contractility, InitialCondition>;

      private:
        //! Content of the section.
        section_content_type section_content_;

    }; // struct AnalyticalPrestress


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_ANALYTICALPRESTRESS_ANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM end header guards *** < //
