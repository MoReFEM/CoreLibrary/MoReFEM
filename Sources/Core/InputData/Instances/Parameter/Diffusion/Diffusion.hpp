// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_DIFFUSION_DIFFUSION_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_DIFFUSION_DIFFUSION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct Diffusion
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Diffusion,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        //! Return the name of the section in the input datum.
        static const std::string& GetName();

        //! Convenient alias.
        using self = Diffusion;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());


        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<std::size_t IndexT>
        struct Tensor : public Internal::InputDataNS::ParamNS::IndexedScalarParameter<Tensor<IndexT>, IndexT, Diffusion>
        {
            //! \copydoc doxygen_hide_indexed_section_basename
            static std::string BaseName()
            {
                return "Tensor";
            }

            //! Convenient alias.
            using self = Tensor<IndexT>;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::IndexedScalarParameter<self, IndexT, Diffusion>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        }; // struct Tensor


        //! \copydoc doxygen_hide_core_input_data_section
        struct Density : public Internal::InputDataNS::ParamNS::ScalarParameter<Density, Diffusion>
        {


            //! Convenient alias.
            using self = Density;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Diffusion>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
            static const std::string& GetName();


        }; // struct Density


        //! \copydoc doxygen_hide_core_input_data_section
        struct TransfertCoefficient
        : public Internal::InputDataNS::ParamNS::ScalarParameter<TransfertCoefficient, Diffusion>
        {

            //! Convenient alias.
            using self = TransfertCoefficient;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Diffusion>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
            static const std::string& GetName();


        }; // struct TransfertCoefficient


    }; // struct Diffusion


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_DIFFUSION_DIFFUSION_DOT_HPP_
// *** MoReFEM end header guards *** < //
