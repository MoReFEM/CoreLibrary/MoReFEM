// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Parameter/ElectricalActivation/ElectricalActivation.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& ElectricalActivation::GetName()
    {
        static const std::string ret("ElectricalActivation");
        return ret;
    }


    const std::string& ElectricalActivation::Delay::NameInFile()
    {
        static const std::string ret("Delay");
        return ret;
    }


    const std::string& ElectricalActivation::Delay::Description()
    {
        static const std::string ret("Delay of the ElectricalActivation. Irrelevant if analytic.");
        return ret;
    }

    const std::string& ElectricalActivation::Delay::DefaultValue()
    {
        static const std::string ret("-0.02.");
        return ret;
    }


    const std::string& ElectricalActivation::AmplitudeMax::NameInFile()
    {
        static const std::string ret("AmplitudeMax");
        return ret;
    }


    const std::string& ElectricalActivation::AmplitudeMax::Description()
    {
        static const std::string ret("Amplitude Max of the ElectricalActivation.");
        return ret;
    }


    const std::string& ElectricalActivation::AmplitudeMax::DefaultValue()
    {
        static const std::string ret("15.");
        return ret;
    }


    const std::string& ElectricalActivation::AmplitudeMin::NameInFile()
    {
        static const std::string ret("AmplitudeMin");
        return ret;
    }


    const std::string& ElectricalActivation::AmplitudeMin::Description()
    {
        static const std::string ret("Amplitude Min of the ElectricalActivation.");
        return ret;
    }

    const std::string& ElectricalActivation::AmplitudeMin::DefaultValue()
    {
        static const std::string ret("-5.");
        return ret;
    }


    const std::string& ElectricalActivation::DepolarizationDuration::NameInFile()
    {
        static const std::string ret("DepolarizationDuration");
        return ret;
    }


    const std::string& ElectricalActivation::DepolarizationDuration::Description()
    {
        static const std::string ret("Depolarization Duration of the ElectricalActivation.");
        return ret;
    }


    const std::string& ElectricalActivation::DepolarizationDuration::DefaultValue()
    {
        static const std::string ret("0.04");
        return ret;
    }


    const std::string& ElectricalActivation::PlateauDuration::NameInFile()
    {
        static const std::string ret("PlateauDuration");
        return ret;
    }


    const std::string& ElectricalActivation::PlateauDuration::Description()
    {
        static const std::string ret("Plateau Duration of the ElectricalActivation. Irrelevant if analytic.");
        return ret;
    }


    const std::string& ElectricalActivation::PlateauDuration::DefaultValue()
    {
        static const std::string ret("0.25");
        return ret;
    }


    const std::string& ElectricalActivation::RepolarizationDuration::NameInFile()
    {
        static const std::string ret("RepolarizationDuration");
        return ret;
    }


    const std::string& ElectricalActivation::RepolarizationDuration::Description()
    {
        static const std::string ret("Repolarization Duration of the ElectricalActivation.");
        return ret;
    }


    const std::string& ElectricalActivation::RepolarizationDuration::DefaultValue()
    {
        static const std::string ret("0.2");
        return ret;
    }


    const std::string& ElectricalActivation::Type::NameInFile()
    {
        static const std::string ret("Type");
        return ret;
    }


    const std::string& ElectricalActivation::Type::Description()
    {
        static const std::string ret(
            "Type of the ElectricalActivation. With prescribed need two fibers for delay and plateau_duration.");
        return ret;
    }

    const std::string& ElectricalActivation::Type::Constraint()
    {
        static const std::string ret("value_in(v, {'analytic', 'prescribed'})");
        return ret;
    }


    const std::string& ElectricalActivation::Type::DefaultValue()
    {
        static const std::string ret("\"analytic\"");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
