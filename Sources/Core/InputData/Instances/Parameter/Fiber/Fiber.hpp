// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_FIBER_FIBER_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_FIBER_FIBER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <tuple>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Parameter/Fiber/Internal/Fiber.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::InputDataNS
{


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    struct FiberTag
    { };


    /*!
     * \copydoc doxygen_hide_core_input_data_section_with_index
     * \tparam TypeT Type of the ]a parameter.
     */
    template<std::size_t IndexT, ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    struct Fiber : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                       Fiber<IndexT, FiberPolicyT, TypeT>,
                       IndexT,
                       FiberTag<FiberPolicyT, TypeT>,
                       ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "Fiber_" + ::MoReFEM::ParameterNS::Name<TypeT>() + "_";
        }

        //! Convenient alias.
        using self = Fiber<IndexT, FiberPolicyT, TypeT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Ensight file from which data are read.
         */
        struct EnsightFile
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<EnsightFile, self, FiberNS::EnsightFile::storage_type>,
          public FiberNS::EnsightFile
        { };


        /*!
         * \brief Index of the \a Domain onto which fiber is defined. It is expected this parameter
         * is compatible with the fiber file.
         */
        struct DomainIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndex, self, FiberNS::DomainIndex::storage_type>,
          public FiberNS::DomainIndex
        { };


        /*!
         * \brief Index of the finite element space  onto which fiber is defined.
         */
        struct FEltSpaceIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                    Leaf<FEltSpaceIndex, self, FiberNS::FEltSpaceIndex::storage_type>,
                                public FiberNS::FEltSpaceIndex
        { };


        /*!
         * \brief Name of the (fictitious) unknown used to define the parameter.
         *
         * A dof might only be defined in relation to a dof.
         */
        struct UnknownName
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<UnknownName, self, FiberNS::UnknownName::storage_type>,
          public FiberNS::UnknownName
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                EnsightFile,
                DomainIndex,
                FEltSpaceIndex,
                UnknownName
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Fiber


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_FIBER_FIBER_DOT_HPP_
// *** MoReFEM end header guards *** < //
