// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Parameter/Fiber/Internal/Fiber.hpp"


namespace MoReFEM::InputDataNS::FiberNS
{


    const std::string& EnsightFile::NameInFile()
    {
        static const std::string ret("ensight_file");
        return ret;
    }


    const std::string& EnsightFile::Description()
    {
        static const std::string ret("Path to Ensight file.");
        return ret;
    }


    const std::string& DomainIndex::NameInFile()
    {
        static const std::string ret("domain_index");
        return ret;
    }


    const std::string& DomainIndex::Description()
    {
        static const std::string ret("Index of the domain upon which parameter is defined.");
        return ret;
    }


    const std::string& FEltSpaceIndex::NameInFile()
    {
        static const std::string ret("felt_space_index");
        return ret;
    }


    const std::string& FEltSpaceIndex::Description()
    {
        static const std::string ret("Index of the finite element space upon which parameter is defined.");
        return ret;
    }

    const std::string& UnknownName::NameInFile()
    {
        static const std::string ret("unknown");
        return ret;
    }


    const std::string& UnknownName::Description()
    {
        static const std::string ret("Name of the unknown used to describe the dofs. Might be a fictitious one (see "
                                     "documentation for more details).");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
