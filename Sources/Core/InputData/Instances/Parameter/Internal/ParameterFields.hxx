// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_PARAMETERFIELDS_DOT_HXX_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_PARAMETERFIELDS_DOT_HXX_
// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Internal/ParameterFields.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/String/EmptyString.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& Nature<DerivedT, EnclosingSectionT, TypeT>::NameInFile()
    {
        static const std::string ret("nature");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& Nature<DerivedT, EnclosingSectionT, TypeT>::Description()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
        {
            static const std::string ret(
                "How is given each component of the parameter (as a constant, as a Lua function"
                ", per quadrature point, etc...). Choose \"ignore\" if you do not want this "
                "parameter (in this case it will stay at nullptr).");
            return ret;
        } else
        {
            static const std::string ret("How is given the parameter (as a constant, per "
                                         "quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                         "parameter (in this case it will stay at nullptr).");
            return ret;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& Nature<DerivedT, EnclosingSectionT, TypeT>::Constraint()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
        {
            static const std::string ret("value_in(v, {"
                                         "'ignore', "
                                         "'constant', "
                                         "'lua_function',"
                                         "'piecewise_constant_by_domain'})");
            return ret;
        } else
        {
            static const std::string ret("value_in(v, {"
                                         "'ignore', "
                                         "'constant', "
                                         "'piecewise_constant_by_domain'})");
            return ret;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& VectorDimension<DerivedT, EnclosingSectionT>::NameInFile()
    {
        static const std::string ret("vector_dimension");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& VectorDimension<DerivedT, EnclosingSectionT>::Description()
    {
        static const std::string ret(
            "Number of elements expected in each local vector (returned by Parameter::GetValue() at "
            "a local coords and for a given GeometricElt). The values "
            "given in the option file must match exactly this constraint - there is no padding "
            "whatsoever if your data are inconsistent and an exception is thrown in this case).");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& MatrixDimension<DerivedT, EnclosingSectionT>::NameInFile()
    {
        static const std::string ret("matrix_dimension");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& MatrixDimension<DerivedT, EnclosingSectionT>::Description()
    {
        static const std::string ret(
            "Number of rows and columns (in that order) expected in each local matrix (returned by "
            "Parameter::GetValue() at a local coords and for a given GeometricElt). The values "
            "given in the option file must match exactly this constraint - there is no padding "
            "whatsoever if your data are inconsistent and an exception is thrown in this case).");
        return ret;
    }


    template<class DerivedT, class NatureT, class NeltsT>
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    auto Value<DerivedT, NatureT, NeltsT>::Selector(const ModelSettingsT& model_settings, const InputDataT* input_data)
        -> storage_type
    {
        const auto nature = ::MoReFEM::InputDataNS::ExtractLeaf<NatureT>(model_settings, *input_data);
        static_assert(Utilities::IsSpecializationOf<std::variant, storage_type>());
        return SelectorHelper<parameter_type_enum>(nature);
    }


    template<class DerivedT, class NatureT, class NeltsT>
    const std::string& Value<DerivedT, NatureT, NeltsT>::NameInFile()
    {
        static const std::string ret("value");
        return ret;
    }


    template<class DerivedT, class NatureT, class NeltsT>
    const std::string& Value<DerivedT, NatureT, NeltsT>::Description()
    {
        static const std::string ret =
            ::MoReFEM::Internal::InputDataNS::ParamNS::SelectorDescription<parameter_type_enum>();
        return ret;
    }

    template<class DerivedT, class NatureT, class NeltsT>
    const std::string& Value<DerivedT, NatureT, NeltsT>::DefaultValue()
    {
        if constexpr (parameter_type_enum == ::MoReFEM::ParameterNS::Type::scalar)
            return Utilities::EmptyString();
        else
        {
            static const std::string ret("{}");
            return ret;
        }
    }


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_PARAMETERFIELDS_DOT_HXX_
// *** MoReFEM end header guards *** < //
