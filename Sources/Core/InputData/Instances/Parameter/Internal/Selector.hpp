// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_SELECTOR_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_SELECTOR_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    /*!
     * \brief Returns the description of the Selector class.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     *
     * \param[in] do_print_header If true, print the introductory header which explains format for scalar, vector or matrix.
     * Currently choose false for a Compound 3D parameter and true for any other cases.
     *
     * \return The description as a new std::string.
     */
    template<::MoReFEM::ParameterNS::Type TypeT>
    std::string SelectorDescription(bool do_print_header = true);


    /*!
     * \brief Helper function to return the proper value for the variant depending on the \a nature provided
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     *
     * \param[in] nature The nature of the parameter in the input datafile (see the description inside this file to see what are the possibilities).
     *
     * \return The relevant value.
     */
    template<::MoReFEM::ParameterNS::Type TypeT>
    typename Internal::ParameterNS::Traits<TypeT, Internal::ParameterNS::DefaultStorage<TypeT>>::variant_type
    SelectorHelper(const std::string& nature);


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InputData/Instances/Parameter/Internal/Selector.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_SELECTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
