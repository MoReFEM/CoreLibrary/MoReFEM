// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_SELECTOR_DOT_HXX_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_SELECTOR_DOT_HXX_
// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/String/EmptyString.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/Parameter/Internal/Traits.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    template<::MoReFEM::ParameterNS::Type TypeT>
    std::string SelectorDescription(bool do_print_header)
    {
        std::ostringstream oconv;

        decltype(auto) empty_str = Utilities::EmptyString();

        if (do_print_header)
        {
            if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
                oconv << "The value for the parameter, which type depends directly on the nature chosen in the "
                         "namesake field:\n";
            else if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::vector)
                oconv << "The values of the vectorial parameter; expected format is a table (opening = '{', "
                         "closing = '} and separator = ',') and each item depends on the nature specified at the "
                         "namesake field:\n";
            else if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::matrix)
                oconv << "The values of the matricial parameter; expected format is a table (opening = '{', "
                         "closing = '} and separator = ',') and each item depends on the nature specified at the "
                         "namesake field (values are given as a vector and redispatched with help of "
                         "'matrix_dimension' field; the content is to be given row by row.):\n";
        }

        oconv << "\n If nature is 'constant', expected format is "
              << Internal::InputDataNS::Traits::Format<double>::Print(empty_str);
        oconv << "\n If nature is 'piecewise_constant_by_domain', expected format is "
              << Internal::InputDataNS::Traits::Format<std::map<std::size_t, double>>::Print(empty_str);

        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
            oconv << "\n If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block:\n"
                     "[[\n"
                     "function(x, y, z) \n"
                     "return x + y - z\n"
                     "end\n"
                     "]]\n"
                     "where x, y and z are global coordinates. "
                     "sin, cos, tan, exp and so forth require a 'math.' prefix.\n";

        std::string ret(oconv.str());
        return ret;
    }


    template<::MoReFEM::ParameterNS::Type TypeT>
    auto SelectorHelper(const std::string& nature) ->
        typename Internal::ParameterNS::Traits<TypeT, Internal::ParameterNS::DefaultStorage<TypeT>>::variant_type
    {
        using traits = Internal::ParameterNS::Traits<TypeT, Internal::ParameterNS::DefaultStorage<TypeT>>;

        if constexpr (!std::is_same_v<typename traits::lua_function_type, std::false_type>)
        {
            if (nature == "lua_function")
                return typename traits::lua_function_type();
        }

        if (nature == "constant")
            return typename traits::constant_in_lua_file_type();
        else if (nature == "piecewise_constant_by_domain")
            return typename traits::piecewise_constant_in_lua_file_type();
        else if (nature == "ignore")
            return nullptr;
        else
        {
            std::cerr << "Choice '" << nature << "' is invalid!" << std::endl;
            assert(false
                   && "Possible choices should all be dealt with here; the possibilities should have been "
                      "checked by the Constraint in the option file.");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_SELECTOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
