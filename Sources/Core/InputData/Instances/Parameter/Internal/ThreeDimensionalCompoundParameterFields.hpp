// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_THREEDIMENSIONALCOMPOUNDPARAMETERFIELDS_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_THREEDIMENSIONALCOMPOUNDPARAMETERFIELDS_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Advanced/Crtp/Section.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/InputData/Instances/Parameter/Internal/ParameterFields.hpp"
#include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    /*!
     * \brief Choose how is described the \a Parameter (through a scalar, a function, etc...)
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    struct ThreeDimensionalCompoundNature
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
             <
                 DerivedT,
                 EnclosingSectionT,
                 std::vector<std::string>
             >
    // clang-format on
    {

        //! Useful alias for defining associated field 'Value'.
        using enclosing_section_type = EnclosingSectionT;

        //! Useful alias for defining associated field 'Value'.
        static constexpr auto parameter_type_enum = TypeT;

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some text from \a
         * OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no value has
         * been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    /*!
     * \brief The value of the parameter.
     *
     * A std::variant is used here: the value might be a scalar, an associative container or a LuaFunction
     * depending on what the user chose as 'NatureT'.
     *
     * A std::variant might be read with a visitor (see std::visit) or std::get<> calls; anyway these calls
     * aren't supposed to be exposed for user interface.
     *
     * \tparam NatureT Another field of the input data file, which should provide the type of expected data.
     * The associated string read should be "constant", "piecewise_constant_by_domain" or "lua_function".
     *
     * Typically it should be a sibling field in the same section, e.g.:
     *
     \verbatim
     Kappa1 =
     {
        nature = "constant",
        value = 5.
     }
     \endverbatim
     * where nature is the field interpreted by NatureT and value the field interpreted by current struct.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    // clang-format off
    template<class DerivedT, class NatureT>
    struct ThreeDimensionalCompoundValue
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
    <
        DerivedT,
        typename NatureT::enclosing_section_type, std::vector<typename Internal::ParameterNS::Traits<::MoReFEM::ParameterNS::Type::scalar, Internal::ParameterNS::DefaultStorage<::MoReFEM::ParameterNS::Type::scalar>>::variant_type>
    >
    // clang-format on
    {
        //! Useful alias.
        using scalar_variant_type =
            typename Internal::ParameterNS::Traits<::MoReFEM::ParameterNS::Type::scalar>::variant_type;

        //! Each of the component is stored as a \a ScalarParameter. A \a std::vector is used as it is what is supported
        //! by \a OptionFile but exactly three elements are expected here.
        using storage_type_for_compound = std::vector<scalar_variant_type>;

        /*!
         * \brief Selector which role is to set the std::variant with the proper type pointed by \a NatureT.
         *
         * The std::variant is filled with default value: the point is not the actual value put there, but
         * the fact that the type is known (e.g. assigning a double if \a NatureT points to 'constant').
         *
         * \tparam InputDataT Type of \a input_data.
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \return The std::variant properly filled with a non important value of the correct type (e.g. double
         * if NatureT gives away "constant", std::map<std::size_t, double> if NatureT is
         * "piecewise_constant_by_domain".
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        static storage_type_for_compound Selector(const ModelSettingsT& model_settings, const InputDataT* input_data);


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no value has
         * been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InputData/Instances/Parameter/Internal/ThreeDimensionalCompoundParameterFields.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_THREEDIMENSIONALCOMPOUNDPARAMETERFIELDS_DOT_HPP_
// *** MoReFEM end header guards *** < //
