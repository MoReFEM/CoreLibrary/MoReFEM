// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_THREEDIMENSIONALCOMPOUNDPARAMETERFIELDS_DOT_HXX_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_THREEDIMENSIONALCOMPOUNDPARAMETERFIELDS_DOT_HXX_
// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Internal/ThreeDimensionalCompoundParameterFields.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/String/EmptyString.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::NameInFile()
    {
        static const std::string ret("nature");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::Description()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
        {
            static const std::string ret(
                "How is given each component of the parameter (as a constant, as a Lua function"
                ", per quadrature point, etc...). Choose \"ignore\" if you do not want this "
                "parameter (in this case it will stay at nullptr).");
            return ret;
        } else
        {
            static const std::string ret("How is given the parameter (as a constant, per "
                                         "quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                         "parameter (in this case it will stay at nullptr) - in this case all "
                                         "components must be 'ignore'.");
            return ret;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::Constraint()
    {
        static const std::string ret("value_in(v, {"
                                     "'ignore', "
                                     "'constant', "
                                     "'lua_function',"
                                     "'piecewise_constant_by_domain'})");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::DefaultValue()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
            return Utilities::EmptyString();
        else
        {
            static const std::string ret("{}");
            return ret;
        }
    }


    // clang-format off
    template<class DerivedT, class NatureT>
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    auto ThreeDimensionalCompoundValue<DerivedT, NatureT>::Selector(const ModelSettingsT& model_settings,
                                                                    const InputDataT* input_data)
        -> storage_type_for_compound
    {
        auto selector = ::MoReFEM::InputDataNS::ExtractLeaf<NatureT>(model_settings, *input_data);

        static_assert(std::is_same<decltype(selector), std::vector<std::string>>());
        assert(selector.size() == 3UL);

        return { SelectorHelper<::MoReFEM::ParameterNS::Type::scalar>(selector[0]),
                 SelectorHelper<::MoReFEM::ParameterNS::Type::scalar>(selector[1]),
                 SelectorHelper<::MoReFEM::ParameterNS::Type::scalar>(selector[2]) };
    }


    template<class DerivedT, class NatureT>
    const std::string& ThreeDimensionalCompoundValue<DerivedT, NatureT>::NameInFile()
    {
        static const std::string ret("value");
        return ret;
    }


    template<class DerivedT, class NatureT>
    const std::string& ThreeDimensionalCompoundValue<DerivedT, NatureT>::Description()
    {
        static const std::string ret = "For each 'VALUE*n* (see 'Expected format' below), the format may be: "
                                       + SelectorDescription<::MoReFEM::ParameterNS::Type::scalar>(false);

        return ret;
    }


    template<class DerivedT, class NatureT>
    const std::string& ThreeDimensionalCompoundValue<DerivedT, NatureT>::DefaultValue()
    {
        static const std::string ret("{}");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_INTERNAL_THREEDIMENSIONALCOMPOUNDPARAMETERFIELDS_DOT_HXX_
// *** MoReFEM end header guards *** < //
