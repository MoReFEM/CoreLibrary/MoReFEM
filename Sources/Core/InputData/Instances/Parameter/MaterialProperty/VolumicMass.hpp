// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_MATERIALPROPERTY_VOLUMICMASS_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_MATERIALPROPERTY_VOLUMICMASS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"


namespace MoReFEM::InputDataNS::MaterialProperty
{


    //! \copydoc doxygen_hide_core_input_data_section
    template<class EnclosingTypeT>
    struct VolumicMass
    : public Internal::InputDataNS::ParamNS::ScalarParameter<VolumicMass<EnclosingTypeT>, EnclosingTypeT>
    {


        //! Convenient alias.
        using self = VolumicMass<EnclosingTypeT>;

        //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
        using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, EnclosingTypeT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship to an underlying internal class, which is required for the internal mechanics of
        //! interpreting the content of input data file.
        friend typename parent::section_type;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
        static const std::string& GetName();


    }; // struct VolumicMass


} // namespace MoReFEM::InputDataNS::MaterialProperty


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InputData/Instances/Parameter/MaterialProperty/VolumicMass.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_MATERIALPROPERTY_VOLUMICMASS_DOT_HPP_
// *** MoReFEM end header guards *** < //
