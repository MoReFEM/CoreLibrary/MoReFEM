// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_MICROSPHERE_MICROSPHERE_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_MICROSPHERE_MICROSPHERE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct Microsphere
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Microsphere,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('ActiveStress' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = Microsphere;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! \copydoc doxygen_hide_core_input_data_section
        struct InPlaneFiberDispersionI4
        : public Internal::InputDataNS::ParamNS::ScalarParameter<InPlaneFiberDispersionI4, Microsphere>
        {


            //! Convenient alias.
            using self = InPlaneFiberDispersionI4;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();


        }; // struct InPlaneFiberDispersionI4


        //! \copydoc doxygen_hide_core_input_data_section
        struct OutOfPlaneFiberDispersionI4
        : public Internal::InputDataNS::ParamNS::ScalarParameter<OutOfPlaneFiberDispersionI4, Microsphere>
        {


            //! Convenient alias.
            using self = OutOfPlaneFiberDispersionI4;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();


        }; // struct OutOfPlaneFiberDispersionI4


        //! \copydoc doxygen_hide_core_input_data_section
        struct FiberStiffnessDensityI4
        : public Internal::InputDataNS::ParamNS::ScalarParameter<FiberStiffnessDensityI4, Microsphere>
        {


            //! Convenient alias.
            using self = FiberStiffnessDensityI4;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();

        }; // struct FiberStiffnessDensityI4


        //! \copydoc doxygen_hide_core_input_data_section
        struct InPlaneFiberDispersionI6
        : public Internal::InputDataNS::ParamNS::ScalarParameter<InPlaneFiberDispersionI6, Microsphere>
        {


            //! Convenient alias.
            using self = InPlaneFiberDispersionI6;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();


        }; // struct InPlaneFiberDispersionI6


        //! \copydoc doxygen_hide_core_input_data_section
        struct OutOfPlaneFiberDispersionI6
        : public Internal::InputDataNS::ParamNS::ScalarParameter<OutOfPlaneFiberDispersionI6, Microsphere>
        {


            //! Convenient alias.
            using self = OutOfPlaneFiberDispersionI6;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();

        }; // struct OutOfPlaneFiberDispersionI6


        //! \copydoc doxygen_hide_core_input_data_section
        struct FiberStiffnessDensityI6
        : public Internal::InputDataNS::ParamNS::ScalarParameter<FiberStiffnessDensityI6, Microsphere>
        {


            //! Convenient alias.
            using self = FiberStiffnessDensityI6;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();

        }; // struct FiberStiffnessDensityI6


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
               InPlaneFiberDispersionI4,
               OutOfPlaneFiberDispersionI4,
               FiberStiffnessDensityI4,
               InPlaneFiberDispersionI6,
               OutOfPlaneFiberDispersionI6,
               FiberStiffnessDensityI6
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;

    }; // struct Microsphere


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_MICROSPHERE_MICROSPHERE_DOT_HPP_
// *** MoReFEM end header guards *** < //
