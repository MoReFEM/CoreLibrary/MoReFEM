// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Solid::C0::GetName()
    {
        static const std::string ret("C0");
        return ret;
    }


    const std::string& Solid::C1::GetName()
    {
        static const std::string ret("C1");
        return ret;
    }


    const std::string& Solid::C2::GetName()
    {
        static const std::string ret("C2");
        return ret;
    }


    const std::string& Solid::C3::GetName()
    {
        static const std::string ret("C3");
        return ret;
    }


    const std::string& Solid::C4::GetName()
    {
        static const std::string ret("C4");
        return ret;
    }


    const std::string& Solid::C5::GetName()
    {
        static const std::string ret("C5");
        return ret;
    }


    const std::string& Solid::Mu1::GetName()
    {
        static const std::string ret("Mu1");
        return ret;
    }


    const std::string& Solid::Mu2::GetName()
    {
        static const std::string ret("Mu2");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
