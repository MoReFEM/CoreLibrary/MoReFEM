// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM::InputDataNS
{

    const std::string& Solid::CheckInvertedElements::NameInFile()
    {
        static const std::string ret("CheckInvertedElements");
        return ret;
    }


    const std::string& Solid::CheckInvertedElements::Description()
    {
        static const std::string ret("If the displacement induced by the applied loading is strong enough"
                                     "it can lead to inverted elements: some vertices of the element are moved"
                                     "such that the volume of the finite element becomes negative. This means"
                                     "that the resulting deformed mesh is no longer valid. This parameter enables a"
                                     "computationally expensive test on all quadrature points to check that the volume"
                                     "of all finite elements remains positive throughout the computation.");

        return ret;
    }


    const std::string& Solid::CheckInvertedElements::DefaultValue()
    {
        static const std::string ret("false");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
