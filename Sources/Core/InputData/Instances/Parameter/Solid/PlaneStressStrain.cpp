// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM::InputDataNS
{

    const std::string& Solid::PlaneStressStrain::NameInFile()
    {
        static const std::string ret("PlaneStressStrain");
        return ret;
    }


    const std::string& Solid::PlaneStressStrain::Description()
    {
        static const std::string ret("For 2D operators, which approximation to use.");
        return ret;
    }

    const std::string& Solid::PlaneStressStrain::Constraint()
    {
        static const std::string ret("value_in(v, {'irrelevant', 'plane_strain', 'plane_stress'})");
        return ret;
    }


    const std::string& Solid::PlaneStressStrain::DefaultValue()
    {
        static const std::string ret("\"plane_strain\"");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
