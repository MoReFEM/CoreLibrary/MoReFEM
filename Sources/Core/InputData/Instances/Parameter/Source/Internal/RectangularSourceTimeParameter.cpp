// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Parameter/Source/Internal/RectangularSourceTimeParameter.hpp"


namespace MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS
{


    const std::string& InitialTimeOfActivationList::NameInFile()
    {
        static const std::string ret("initial_time_of_activation");
        return ret;
    }


    const std::string& InitialTimeOfActivationList::Description()
    {
        static const std::string ret("Value of the initial time to activate the source.");
        return ret;
    }

    const std::string& InitialTimeOfActivationList::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }


    const std::string& FinalTimeOfActivationList::NameInFile()
    {
        static const std::string ret("final_time_of_activation");
        return ret;
    }


    const std::string& FinalTimeOfActivationList::Description()
    {
        static const std::string ret("Value of the final time to activate the source.");
        return ret;
    }


    const std::string& FinalTimeOfActivationList::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
