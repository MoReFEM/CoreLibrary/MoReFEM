// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_PRESSURE_DOT_HXX_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_PRESSURE_DOT_HXX_
// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <string>


namespace MoReFEM::InputDataNS::Source
{


    template<std::size_t IndexT>
    const std::string& PressureFromFile<IndexT>::FilePath::NameInFile()
    {
        static const std::string ret("FilePath");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& PressureFromFile<IndexT>::FilePath::Description()
    {
        static const std::string ret("Path of the file to use. "
                                     "Format: "
                                     "time pressure "
                                     "value1 value1 "
                                     "value2 value2 "
                                     "... ");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::Source


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_PRESSURE_DOT_HXX_
// *** MoReFEM end header guards *** < //
