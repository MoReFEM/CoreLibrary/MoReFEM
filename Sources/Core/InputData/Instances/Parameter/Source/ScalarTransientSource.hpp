// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_SCALARTRANSIENTSOURCE_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_SCALARTRANSIENTSOURCE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct ScalarTransientSource : public Internal::InputDataNS::ParamNS::IndexedScalarParameter<
                                       ScalarTransientSource<IndexT>,
                                       IndexT,
                                       ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "ScalarTransientSource";
        }

        //! Convenient alias.
        using self = ScalarTransientSource<IndexT>;


        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    }; // struct TransientSource


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
/*!
 * \brief Macro used in most models when defining a \a ScalarTransientSource in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, only the \a IndexedSectionDescription  used internally to store the description is put in \a
 * ModelSettinga,
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(enum_class_id)                                    \
    ::MoReFEM::InputDataNS::ScalarTransientSource<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription


/*!
 * \brief Macro used in most models when defining a \a ScalarTransientSource in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, the whole section (which is only two leaves...) is up to the end user:
 * - What is the nature of the input data (constant, piecewise constant by domain, Lua function, etc...)
 * - What is the value.
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(enum_class_id)                                        \
    ::MoReFEM::InputDataNS::ScalarTransientSource<EnumUnderlyingType(enum_class_id)>


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_SCALARTRANSIENTSOURCE_DOT_HPP_
// *** MoReFEM end header guards *** < //
