// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SPATIALFUNCTION_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SPATIALFUNCTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Core/InputData/Instances/Parameter/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{

    /*!
     * \brief Input datum which wraps a Lua function with three arguments for spatial coordinates.
     *
     * \tparam CoordsTypeT Whether we deal with local or global coordinates for the function.
     */
    template<CoordsType CoordsTypeT>
    struct SpatialFunction
    {

        //! Returns the type of Coords.
        static CoordsType GetCoordsType();

        //! Part of the description related to the format of the spatial function.
        static const std::string& DescriptionCoordsType();
    };


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InputData/Instances/Parameter/SpatialFunction.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SPATIALFUNCTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
