// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"


namespace MoReFEM::InputDataNS::ReactionNS
{


    const std::string& MitchellSchaeffer::GetName()
    {
        static const std::string ret("ReactionMitchellSchaeffer");
        return ret;
    };


    const std::string& MitchellSchaeffer::TauIn::NameInFile()
    {
        static const std::string ret("tau_in");
        return ret;
    }


    const std::string& MitchellSchaeffer::TauIn::Description()
    {
        static const std::string ret("Value of Tau_in in MitchellSchaeffer.");
        return ret;
    }

    const std::string& MitchellSchaeffer::TauIn::DefaultValue()
    {
        static const std::string ret("0.2");
        return ret;
    }


    const std::string& MitchellSchaeffer::TauOut::NameInFile()
    {
        static const std::string ret("tau_out");
        return ret;
    }


    const std::string& MitchellSchaeffer::TauOut::Description()
    {
        static const std::string ret("Value of Tau_out in MitchellSchaeffer.");
        return ret;
    }

    const std::string& MitchellSchaeffer::TauOut::DefaultValue()
    {
        static const std::string ret("6");
        return ret;
    }


    const std::string& MitchellSchaeffer::TauOpen::NameInFile()
    {
        static const std::string ret("tau_open");
        return ret;
    }


    const std::string& MitchellSchaeffer::TauOpen::Description()
    {
        static const std::string ret("Value of Tau_open in MitchellSchaeffer.");
        return ret;
    }

    const std::string& MitchellSchaeffer::TauOpen::DefaultValue()
    {
        static const std::string ret("120");
        return ret;
    }


    const std::string& MitchellSchaeffer::TauClose::GetName()
    {
        static const std::string ret("tau_close");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialGate::NameInFile()
    {
        static const std::string ret("u_gate");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialGate::Description()
    {
        static const std::string ret("Value of U_gate in MitchellSchaeffer.");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialGate::DefaultValue()
    {
        static const std::string ret("0.13");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialMin::NameInFile()
    {
        static const std::string ret("u_min");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialMin::Description()
    {
        static const std::string ret("Value of U_min in MitchellSchaeffer.");
        return ret;
    }

    const std::string& MitchellSchaeffer::PotentialMin::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialMax::NameInFile()
    {
        static const std::string ret("u_max");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialMax::Description()
    {
        static const std::string ret("Value of U_max in MitchellSchaeffer.");
        return ret;
    }


    const std::string& MitchellSchaeffer::PotentialMax::DefaultValue()
    {
        static const std::string ret("1.");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::ReactionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
