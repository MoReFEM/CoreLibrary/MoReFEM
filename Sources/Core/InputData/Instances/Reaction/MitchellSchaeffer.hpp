// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_REACTION_MITCHELLSCHAEFFER_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_REACTION_MITCHELLSCHAEFFER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS::ReactionNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct MitchellSchaeffer
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<MitchellSchaeffer,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        //! Return the name of the section in the input datum.
        static const std::string& GetName();

        //! Convenient alias.
        using self = MitchellSchaeffer;


        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Coefficient Tau_in in MitchellSchaeffer.
        struct TauIn : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TauIn, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient Tau_out in MitchellSchaeffer.
        struct TauOut : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TauOut, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient Tau_open in MitchellSchaeffer.
        struct TauOpen : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TauOpen, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! \copydoc doxygen_hide_core_input_data_section
        struct TauClose : public Internal::InputDataNS::ParamNS::ScalarParameter<TauClose, MitchellSchaeffer>
        {


            //! Convenient alias.
            using self = TauClose;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, MitchellSchaeffer>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the name of the section in the input datum.
            static const std::string& GetName();


        }; // struct TauClose


        //! Coefficient U_gate in MitchellSchaeffer.
        struct PotentialGate : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<PotentialGate, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient U_min in MitchellSchaeffer.
        struct PotentialMin : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<PotentialMin, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient U_max in MitchellSchaeffer.
        struct PotentialMax : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<PotentialMax, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Reaction Coefficient in FitzHughNagumo.
        struct ReactionCoefficient
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ReactionCoefficient, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            TauIn,
            TauOut,
            TauOpen,
            TauClose,
            PotentialGate,
            PotentialMin,
            PotentialMax
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct MitchellSchaeffer


} // namespace MoReFEM::InputDataNS::ReactionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_REACTION_MITCHELLSCHAEFFER_DOT_HPP_
// *** MoReFEM end header guards *** < //
