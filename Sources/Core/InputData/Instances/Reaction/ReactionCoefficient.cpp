// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Reaction/ReactionCoefficient.hpp"


namespace MoReFEM::InputDataNS::ReactionNS
{


    const std::string& ReactionCoefficient::NameInFile()
    {
        static const std::string ret("ReactionCoefficient");
        return ret;
    }


    const std::string& ReactionCoefficient::Description()
    {
        static const std::string ret("Value of R.");
        return ret;
    }

    const std::string& ReactionCoefficient::DefaultValue()
    {
        static const std::string ret("1.");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::ReactionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
