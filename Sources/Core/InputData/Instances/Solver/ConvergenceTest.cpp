// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Solver/ConvergenceTest.hpp"


namespace MoReFEM::InputDataNS
{

    const std::string& ConvergenceTest::GetName()
    {
        static const std::string ret("ConvergenceTest");
        return ret;
    }


    const std::string& ConvergenceTest::ResidualNormMax::NameInFile()
    {
        static const std::string ret("ResidualNormMax");
        return ret;
    }


    const std::string& ConvergenceTest::ResidualNormMax::Description()
    {
        static const std::string ret("Maximum residual norm evaluation in newton loop.");
        return ret;
    }


    const std::string& ConvergenceTest::ResidualNormMax::DefaultValue()
    {
        static const std::string ret("1.e5");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
