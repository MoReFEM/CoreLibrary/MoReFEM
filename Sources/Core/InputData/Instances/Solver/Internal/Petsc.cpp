// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <sstream>
#include <string>
#include <string_view>

#include "Core/InputData/Instances/Solver/Internal/Petsc.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Quoted.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"


namespace MoReFEM::Internal::InputDataNS::PetscNS
{

    namespace // anonymous
    {


        std::string GenerateConstraint()
        {
            decltype(auto) solver_factory = Internal::Wrappers::Petsc::SolverNS::Factory::CreateOrGetInstance();

            std::ostringstream oconv;

            Utilities::PrintContainer<Utilities::PrintPolicyNS::Quoted<>>::Do(
                solver_factory.GenerateSolverList(),
                oconv,
                ::MoReFEM::PrintNS::Delimiter::separator(", "),
                ::MoReFEM::PrintNS::Delimiter::opener("value_in(v, {"),
                ::MoReFEM::PrintNS::Delimiter::closer(" })"));

            std::string ret = oconv.str();
            return ret;
        }


    } // namespace


    const std::string& Solver::NameInFile()
    {
        static const std::string ret("solver");
        return ret;
    }


    const std::string& Solver::Description()
    {
        static const std::string ret("Solver to use. The list includes all the solvers which have been "
                                     "explicitly activated in MoReFEM. However, it does not mean all are "
                                     "actually available in your local environment: some require that "
                                     "some external dependencies were added along with PETSc. The "
                                     "ThirdPartyCompilationFactory facility set them up by default.");
        // chebychev cg gmres preonly bicg python };\n\t-- To use Mumps choose preonly.");
        return ret;
    }

    const std::string& Solver::Constraint()
    {
        static const std::string ret{ GenerateConstraint() };
        return ret;
    }


    const std::string& Solver::DefaultValue()
    {
        static const std::string ret("'SuperLU_dist'");
        return ret;
    }


    const std::string& GmresRestart::NameInFile()
    {
        static const std::string ret("gmresRestart");
        return ret;
    }


    const std::string& GmresRestart::Description()
    {
        static const std::string ret("gmresStart");
        return ret;
    }

    const std::string& GmresRestart::Constraint()
    {
        static const std::string ret("v >= 0");
        return ret;
    }


    const std::string& GmresRestart::DefaultValue()
    {
        static const std::string ret("200");
        return ret;
    }


    const std::string& Preconditioner::NameInFile()
    {
        static const std::string ret("preconditioner");
        return ret;
    }


    const std::string& Preconditioner::Description()
    {
        static const std::string ret("Preconditioner to use. Must be lu for any direct solver.");
        return ret;
    }

    const std::string& Preconditioner::Constraint()
    {
        static const std::string ret("value_in(v, {'lu', 'none'})");
        // 'jacobi', 'sor', 'lu', 'bjacobi', 'ilu', 'asm', 'cholesky'})");
        return ret;
    }


    const std::string& Preconditioner::DefaultValue()
    {
        static const std::string ret("'lu'");
        return ret;
    }


    const std::string& RelativeTolerance::NameInFile()
    {
        static const std::string ret("relativeTolerance");
        return ret;
    }


    const std::string& RelativeTolerance::Description()
    {
        static const std::string ret("Relative tolerance");
        return ret;
    }

    const std::string& RelativeTolerance::Constraint()
    {
        static const std::string ret("v > 0.");
        return ret;
    }


    const std::string& RelativeTolerance::DefaultValue()
    {
        static const std::string ret("1e-9");
        return ret;
    }


    const std::string& AbsoluteTolerance::NameInFile()
    {
        static const std::string ret("absoluteTolerance");
        return ret;
    }


    const std::string& AbsoluteTolerance::Description()
    {
        static const std::string ret("Absolute tolerance");
        return ret;
    }

    const std::string& AbsoluteTolerance::Constraint()
    {
        static const std::string ret("v > 0.");
        return ret;
    }


    const std::string& AbsoluteTolerance::DefaultValue()
    {
        static const std::string ret("1e-50");
        return ret;
    }


    const std::string& StepSizeTolerance::NameInFile()
    {
        static const std::string ret("stepSizeTolerance");
        return ret;
    }


    const std::string& StepSizeTolerance::Description()
    {
        static const std::string ret("Step size tolerance");
        return ret;
    }

    const std::string& StepSizeTolerance::Constraint()
    {
        static const std::string ret("v > 0.");
        return ret;
    }


    const std::string& StepSizeTolerance::DefaultValue()
    {
        static const std::string ret("1e-8");
        return ret;
    }


    const std::string& MaxIteration::NameInFile()
    {
        static const std::string ret("maxIteration");
        return ret;
    }


    const std::string& MaxIteration::Description()
    {
        static const std::string ret("Maximum iteration");
        return ret;
    }

    const std::string& MaxIteration::Constraint()
    {
        static const std::string ret("v > 0");
        return ret;
    }


    const std::string& MaxIteration::DefaultValue()
    {
        static const std::string ret("1000");
        return ret;
    }

} // namespace MoReFEM::Internal::InputDataNS::PetscNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
