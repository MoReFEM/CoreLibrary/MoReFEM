// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Core/InputData/Instances/TimeManager/Restart.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Restart::GetName()
    {
        static const std::string ret("Restart");
        return ret;
    }


    const std::string& Restart::TimeIndex::NameInFile()
    {
        static const std::string ret("time_iteration");
        return ret;
    }


    const std::string& Restart::TimeIndex::Description()
    {
        static const std::string ret(
            "Time iteration from which we should restart the model (should be present in the first "
            "column of the 'time_iteration.hhdata' file of the previous run. Put 0 if no restart "
            "intended. This value makes only sense in 'RunFromPreprocessed' mode.");
        return ret;
    }


    const std::string& Restart::TimeIndex::DefaultValue()
    {
        static const std::string ret("0");
        return ret;
    }


    const std::string& Restart::DataDirectory::NameInFile()
    {
        static const std::string ret("data_directory");
        return ret;
    }


    const std::string& Restart::DataDirectory::Description()
    {
        static const std::string ret(
            "Result directory of the previous run (as it was written in Result::OutputDirectory "
            "field - so without the 'Rank_*'subfolder that is automatically added). Leave empty "
            "if no restart intended.");
        return ret;
    }


    const std::string& Restart::DataDirectory::DefaultValue()
    {
        static const std::string ret("''"); // not empty string on purpose: I want the default value to be this!
        return ret;
    }


    const std::string& Restart::DoWriteRestartData::NameInFile()
    {
        static const std::string ret("do_write_restart_data");
        return ret;
    }


    const std::string& Restart::DoWriteRestartData::Description()
    {
        static const std::string ret(
            "Whether restart data are written or not. Such data are written only on root processor "
            "and only in binary format.");
        return ret;
    }

    const std::string& Restart::DoWriteRestartData::DefaultValue()
    {
        static const std::string ret("true");
        return ret;
    }

} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
