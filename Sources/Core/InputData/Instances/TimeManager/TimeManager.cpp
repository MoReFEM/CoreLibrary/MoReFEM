// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& TimeManager::GetName()
    {
        static const std::string ret("transient");
        return ret;
    }


    const std::string& TimeManager::TimeStep::NameInFile()
    {
        static const std::string ret("timeStep");
        return ret;
    }


    const std::string& TimeManager::TimeStep::Description()
    {
        static const std::string ret("Time step between two iterations, in seconds.");
        return ret;
    }

    const std::string& TimeManager::TimeStep::Constraint()
    {
        static const std::string ret("v > 0.");
        return ret;
    }


    const std::string& TimeManager::TimeStep::DefaultValue()
    {
        static const std::string ret("0.1");
        return ret;
    }

    const std::string& TimeManager::MinimumTimeStep::NameInFile()
    {
        static const std::string ret("minimum_time_step");
        return ret;
    }


    const std::string& TimeManager::MinimumTimeStep::Description()
    {
        static const std::string ret("Minimum time step between two iterations, in seconds.");
        return ret;
    }

    const std::string& TimeManager::MinimumTimeStep::Constraint()
    {
        static const std::string ret("v > 0.");
        return ret;
    }


    const std::string& TimeManager::MinimumTimeStep::DefaultValue()
    {
        static const std::string ret("1.e-6");
        return ret;
    }

    const std::string& TimeManager::TimeInit::NameInFile()
    {
        static const std::string ret("init_time");
        return ret;
    }


    const std::string& TimeManager::TimeInit::Description()
    {
        static const std::string ret("Time at the beginning of the code (in seconds).");
        return ret;
    }

    const std::string& TimeManager::TimeInit::Constraint()
    {
        static const std::string ret("v >= 0.");
        return ret;
    }


    const std::string& TimeManager::TimeInit::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }


    const std::string& TimeManager::TimeMax::NameInFile()
    {
        static const std::string ret("timeMax");
        return ret;
    }


    const std::string& TimeManager::TimeMax::Description()
    {
        static const std::string ret("Maximum time, if set to zero run a static case.");
        return ret;
    }


    const std::string& TimeManager::TimeMax::Constraint()
    {
        static const std::string ret("v >= 0.");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
