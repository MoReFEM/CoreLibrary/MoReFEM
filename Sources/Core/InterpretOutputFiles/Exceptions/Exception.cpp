// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef>
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"

#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{


    std::string InvalidFormatInFileMsg(const ::MoReFEM::FilesystemNS::File& file, const std::string& description);


    std::string InvalidFormatInLineMsg(const std::string& line);


    std::string InvalidFormatInLineMsg(const std::string& line, const std::string& description);


    std::string IndexNotFoundMsg(std::size_t sought_index,
                                 const MoReFEM::FilesystemNS::File& file,
                                 std::string_view file_description);


} // namespace


namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS
{


    InvalidFormatInFile::~InvalidFormatInFile() = default;


    InvalidFormatInFile::InvalidFormatInFile(const FilesystemNS::File& file,
                                             const std::string& description,
                                             const std::source_location location)
    : ::MoReFEM::Exception(InvalidFormatInFileMsg(file, description), location)
    { }


    InvalidFormatInLine::~InvalidFormatInLine() = default;


    InvalidFormatInLine::InvalidFormatInLine(const std::string& line, const std::source_location location)
    : ::MoReFEM::Exception(InvalidFormatInLineMsg(line), location)
    { }


    InvalidFormatInLine::InvalidFormatInLine(const std::string& line,
                                             const std::string& description,
                                             const std::source_location location)
    : ::MoReFEM::Exception(InvalidFormatInLineMsg(line, description), location)
    { }


    IndexNotFound::~IndexNotFound() = default;

    IndexNotFound::IndexNotFound(const std::size_t sought_index,
                                 const FilesystemNS::File& file,
                                 std::string_view file_description,
                                 const std::source_location location)
    : ::MoReFEM::Exception(IndexNotFoundMsg(sought_index, file, file_description), location)
    { }


} // namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string InvalidFormatInFileMsg(const ::MoReFEM::FilesystemNS::File& file, const std::string& description)
    {
        std::ostringstream oconv;
        oconv << "Invalid file (" << file << "): " << description << '.';

        return oconv.str();
    }


    std::string InvalidFormatInLineMsg(const std::string& line)
    {
        std::ostringstream oconv;
        oconv << "Invalid format in " << line;

        return oconv.str();
    }


    std::string InvalidFormatInLineMsg(const std::string& line, const std::string& description)
    {
        std::ostringstream oconv;
        oconv << InvalidFormatInLineMsg(line) << ": " << description;

        return oconv.str();
    }


    std::string IndexNotFoundMsg(const std::size_t sought_index,
                                 const MoReFEM::FilesystemNS::File& file,
                                 std::string_view file_description)
    {
        std::ostringstream oconv;
        oconv << "Index " << sought_index << " was sought in " << file_description << " file " << file
              << " but it was not found." << '\n';

        return oconv.str();
    }


} // namespace
