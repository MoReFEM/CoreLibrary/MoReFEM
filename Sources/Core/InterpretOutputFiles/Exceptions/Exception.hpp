// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INTERPRETOUTPUTFILES_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_CORE_INTERPRETOUTPUTFILES_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS
{


    //! Exception when file is ill-formatted.
    class InvalidFormatInFile final : public ::MoReFEM::Exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] file File for which the problem occurred.
         * \param[in] description The issue might be explained more explicitly there as a string.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidFormatInFile(const FilesystemNS::File& file,
                                     const std::string& description,
                                     const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~InvalidFormatInFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidFormatInFile(const InvalidFormatInFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidFormatInFile(InvalidFormatInFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidFormatInFile& operator=(const InvalidFormatInFile& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidFormatInFile& operator=(InvalidFormatInFile&& rhs) = default;

        ///@}
    };


    //! Exception when format line is invalid.
    class InvalidFormatInLine final : public ::MoReFEM::Exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] line Line in which the problem occurred.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidFormatInLine(const std::string& line,
                                     const std::source_location location = std::source_location::current());

        /*!
         * \brief Constructor with a more detailed explanation.
         *
         * \param[in] line Line in which the problem occurred.
         * \param[in] description String used to describe more deeply what went wrong.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidFormatInLine(const std::string& line,
                                     const std::string& description,
                                     const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~InvalidFormatInLine() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidFormatInLine(const InvalidFormatInLine& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidFormatInLine(InvalidFormatInLine&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidFormatInLine& operator=(const InvalidFormatInLine& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidFormatInLine& operator=(InvalidFormatInLine&& rhs) = default;

        ///@}
    };


    //! Exception when a data with a specific index was not found.
    class IndexNotFound final : public ::MoReFEM::Exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor
         *
         * \param[in] sought_index Index which data was sought in the file.
         * \param[in] file File in which the sought index was not found.
         * \param[in] file_description Description of the type of file considered, to enrich the error message.
         * For instance 'TimeIteration'.
         * \copydoc doxygen_hide_source_location
         */
        explicit IndexNotFound(const std::size_t sought_index,
                               const FilesystemNS::File& file,
                               std::string_view file_description,
                               const std::source_location location = std::source_location::current());


        //! Destructor.
        virtual ~IndexNotFound() override;

        //! \copydoc doxygen_hide_copy_constructor
        IndexNotFound(const IndexNotFound& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        IndexNotFound(IndexNotFound&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        IndexNotFound& operator=(const IndexNotFound& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        IndexNotFound& operator=(IndexNotFound&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INTERPRETOUTPUTFILES_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
