// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INTERPRETOUTPUTFILES_TIMEITERATION_TIMEITERATION_DOT_HXX_
#define MOREFEM_CORE_INTERPRETOUTPUTFILES_TIMEITERATION_TIMEITERATION_DOT_HXX_
// IWYU pragma: private, include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Core/NumberingSubset/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterpretOutputFilesNS::Data
{


    inline std::size_t TimeIteration::GetIteration() const noexcept
    {
        return time_iteration_;
    }


    inline double TimeIteration::GetTime() const noexcept
    {
        return time_;
    }


    inline const FilesystemNS::File& TimeIteration::GetSolutionFilename() const noexcept
    {
        return solution_filename_;
    }


    inline NumberingSubsetNS::unique_id TimeIteration::GetNumberingSubsetId() const noexcept
    {
        return numbering_subset_id_;
    }


} // namespace MoReFEM::InterpretOutputFilesNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INTERPRETOUTPUTFILES_TIMEITERATION_TIMEITERATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
