// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef>
#include <fstream>
#include <memory>
#include <source_location>
#include <string>

#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"

#include "Utilities/Filesystem/File.hpp" // IWYU pragma: keep
#include "Utilities/String/String.hpp"

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"
#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"


namespace MoReFEM::InterpretOutputFilesNS
{


    TimeIterationFile::TimeIterationFile(const FilesystemNS::File& input_file) : input_file_(input_file)
    {
        std::ifstream stream{ input_file.Read() };

        std::string line;

        while (getline(stream, line))
        {
            if (Utilities::String::StartsWith(line, "#"))
                continue;

            time_iteration_list_.emplace_back(std::make_unique<Data::TimeIteration>(line));
        }
    }


    const Data::TimeIteration& TimeIterationFile::GetTimeIteration(std::size_t index,
                                                                   const std::source_location location) const
    {
        decltype(auto) time_iteration_list = GetTimeIterationList();

        const auto end = time_iteration_list.cend();

        auto it = std::find_if(time_iteration_list.begin(),
                               end,
                               [index](const auto& time_iteration_ptr)
                               {
                                   assert(!(!time_iteration_ptr));
                                   return time_iteration_ptr->GetIteration() == index;
                               });

        if (it == end)
            throw ExceptionNS::InterpretOutputFilesNS::IndexNotFound(index, GetInputFile(), "TimeIteration", location);


        return *(*it);
    }


} // namespace MoReFEM::InterpretOutputFilesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
