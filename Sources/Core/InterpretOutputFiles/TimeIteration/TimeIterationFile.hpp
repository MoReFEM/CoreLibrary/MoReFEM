// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INTERPRETOUTPUTFILES_TIMEITERATION_TIMEITERATIONFILE_DOT_HPP_
#define MOREFEM_CORE_INTERPRETOUTPUTFILES_TIMEITERATION_TIMEITERATIONFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <source_location>

#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterpretOutputFilesNS
{


    /*!
     * \brief Class which holds the information obtained from time_iteration.hhdata output file.
     *
     * There is one such file per mesh.
     *
     * Example:
     * \verbatim
     # Time iteration; time; numbering subset id; filename
     1;0.001;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_*
     /Mesh_1/NumberingSubset_10/fluid_velocity_time_00001.hhdata
     2;0.002;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_*
     /Mesh_1/NumberingSubset_10/fluid_velocity_time_00002.hhdata
     ...
     \endverbatim
     *
     * (no space after Rank_* - but I had to add it to avoid compiler mistaking it for an end of a C comment).
     *
     */
    class TimeIterationFile final
    {
      public:
        //! Alias to most relevant smart pointer.
        using const_unique_ptr = std::unique_ptr<const TimeIterationFile>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] input_file Result file of MoReFEM program which gives the time related information;
        //! it is named 'time_iteration.hhdata'.
        explicit TimeIterationFile(const FilesystemNS::File& input_file);

        //! Destructor.
        ~TimeIterationFile() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TimeIterationFile(const TimeIterationFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeIterationFile(TimeIterationFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TimeIterationFile& operator=(const TimeIterationFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TimeIterationFile& operator=(TimeIterationFile&& rhs) = delete;

        ///@}


        /*!
         * \brief Get the \a Data::TimeIteration which index is \a index. If none found, throws an exception.
         *
         * \param[in] index Index of the sought time iteration.
         * \copydoc doxygen_hide_source_location
         */
        const Data::TimeIteration&
        GetTimeIteration(std::size_t index,
                         const std::source_location location = std::source_location::current()) const;


      public:
        //! Accessor to the list of time iterations.
        const Data::TimeIteration::vector_const_unique_ptr& GetTimeIterationList() const;

        //! Number of steps.
        std::size_t Nstep() const;

      private:
        //! Returns the file from which the data were loaded.
        const FilesystemNS::File& GetInputFile() const noexcept;

      private:
        //! Input file considered.
        const FilesystemNS::File& input_file_;

        //! List of all time iterations considered.
        Data::TimeIteration::vector_const_unique_ptr time_iteration_list_;
    };


} // namespace MoReFEM::InterpretOutputFilesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INTERPRETOUTPUTFILES_TIMEITERATION_TIMEITERATIONFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
