// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef>
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Core/LinearAlgebra/Assertion/Assertion.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::GlobalLinearAlgebraNS::AssertionNS
{


#ifndef NDEBUG

    namespace
    {


        std::string MismatchNumberingSubsetMsg(const NumberingSubset& expected_row,
                                               const NumberingSubset& expected_col,
                                               const NumberingSubset& given_row,
                                               const NumberingSubset& given_col,
                                               std::string&& matrix_name);

        std::string MismatchNumberingSubsetMsg(const NumberingSubset& expected,
                                               const NumberingSubset& given,
                                               std::string&& vector_name);

        std::string MismatchNumberingSubsetInMatrixProductMsg(const NumberingSubset& first_matrix_col_ns,
                                                              const NumberingSubset& second_matrix_row_ns);

        std::string ForbiddenSameVectorMsg(std::string_view operation_name,
                                           std::pair<std::size_t, std::size_t>&& arg_position_list);

    } // namespace


    MismatchNumberingSubset::~MismatchNumberingSubset() = default;

    MismatchNumberingSubset::MismatchNumberingSubset(const NumberingSubset& expected_row,
                                                     const NumberingSubset& expected_col,
                                                     const NumberingSubset& given_row,
                                                     const NumberingSubset& given_col,
                                                     std::string&& matrix_name,
                                                     const std::source_location location)
    : ::MoReFEM::Advanced::Assertion(
          MismatchNumberingSubsetMsg(expected_row, expected_col, given_row, given_col, std::move(matrix_name)),
          location)
    { }


    MismatchNumberingSubset::MismatchNumberingSubset(const NumberingSubset& expected,
                                                     const NumberingSubset& given,
                                                     std::string&& vector_name,
                                                     const std::source_location location)
    : ::MoReFEM::Advanced::Assertion(MismatchNumberingSubsetMsg(expected, given, std::move(vector_name)), location)
    { }


    MismatchNumberingSubsetInMatrixProduct::~MismatchNumberingSubsetInMatrixProduct() = default;

    MismatchNumberingSubsetInMatrixProduct::MismatchNumberingSubsetInMatrixProduct(
        const NumberingSubset& first_matrix_col_ns,
        const NumberingSubset& second_matrix_row_ns,
        const std::source_location location)
    : ::MoReFEM::Advanced::Assertion(
          MismatchNumberingSubsetInMatrixProductMsg(first_matrix_col_ns, second_matrix_row_ns),
          location)
    { }


    ForbiddenSameVector::~ForbiddenSameVector() = default;

    ForbiddenSameVector::ForbiddenSameVector(std::string_view operation_name,
                                             std::pair<std::size_t, std::size_t>&& arg_position_list,
                                             const std::source_location location)
    : ::MoReFEM::Advanced::Assertion(ForbiddenSameVectorMsg(operation_name, std::move(arg_position_list)), location)
    { }


    namespace
    {


        std::string MismatchNumberingSubsetMsg(const NumberingSubset& expected_row,
                                               const NumberingSubset& expected_col,
                                               const NumberingSubset& given_row,
                                               const NumberingSubset& given_col,
                                               std::string&& matrix_name)
        {
            std::ostringstream oconv;
            oconv << "Expected numbering subsets for the matrix '" << matrix_name << "' was ("
                  << expected_row.GetUniqueId() << ", " << expected_col.GetUniqueId()
                  << ") but what was actually provided was (" << given_row.GetUniqueId() << ", "
                  << given_col.GetUniqueId() << ")." << '\n';
            return oconv.str();
        }


        std::string MismatchNumberingSubsetMsg(const NumberingSubset& expected,
                                               const NumberingSubset& given,
                                               std::string&& vector_name)
        {
            std::ostringstream oconv;
            oconv << "Expected numbering subset for the vector '" << vector_name << "' was (" << expected.GetUniqueId()
                  << ") but what was actually provided was (" << given.GetUniqueId() << ")." << '\n';
            return oconv.str();
        }


        std::string MismatchNumberingSubsetInMatrixProductMsg(const NumberingSubset& first_matrix_col_ns,
                                                              const NumberingSubset& second_matrix_row_ns)
        {
            std::ostringstream oconv;
            oconv << "In matrix - matrix product, numbering subsets don't match: numbering subset for columns of "
                     "first matrix is "
                  << first_matrix_col_ns.GetUniqueId()
                  << " whereas the one for rows of second "
                     "matrix is "
                  << second_matrix_row_ns.GetUniqueId() << '.';

            return oconv.str();
        }


        std::string ForbiddenSameVectorMsg(std::string_view operation_name,
                                           std::pair<std::size_t, std::size_t>&& arg_position_list)
        {
            std::ostringstream oconv;
            oconv << "In " << operation_name << " operation, two arguments (in position " << arg_position_list.first
                  << " and " << arg_position_list.second
                  << ") were the same whereas they "
                     "shouldn't be (either because PETSc doesn't support it explicitly, or because nothing is said "
                     "about it in PETSc documentation but in practice we figured out the operation leads to issues).";

            return oconv.str();
        }


    } // namespace


#endif // NDEBUG

} // namespace MoReFEM::GlobalLinearAlgebraNS::AssertionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
