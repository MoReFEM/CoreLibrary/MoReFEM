// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_ASSERTION_ASSERTION_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_ASSERTION_ASSERTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
#include <source_location>
#include <utility>
// IWYU pragma: no_include <string>

// IWYU pragma: begin_exports
#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp"
// IWYU pragma: end_exports

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalLinearAlgebraNS::AssertionNS
{


#ifndef NDEBUG


    //! When an internal PETSc object should have not been initialized and was notetheless.
    struct MismatchNumberingSubset : public ::MoReFEM::Advanced::Assertion
    {
        /*!
         * \brief Constructor when checking expectation regarding a matrix.
         *
         * \param[in] expected_row  `NumberingSubset` which was expected for the row.
         * \param[in] expected_col  `NumberingSubset` which was expected for the column.
         * \param[in] given_row  `NumberingSubset` which was actually provided for the row.
         * \param[in] given_col  `NumberingSubset` which was actually provided f for the column.
         * \param[in] matrix_name Name of the variable representing the matrix for which the check fails. Might be useful if operations where several matrices are
         * involved to tell exactly which one triggered the assertion.
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit MismatchNumberingSubset(const NumberingSubset& expected_row,
                                         const NumberingSubset& expected_col,
                                         const NumberingSubset& given_row,
                                         const NumberingSubset& given_col,
                                         std::string&& matrix_name,
                                         const std::source_location location = std::source_location::current());


        /*!
         * \brief Constructor when checking expectation regarding a vector.
         *
         * \param[in] expected  `NumberingSubset` which was expected.
         * \param[in] given  `NumberingSubset` which was actually provided.
         * \param[in] vector_name Name of the variable representing the vector for which the check fails. Might be useful if operations where several vectors are
         * involved to tell exactly which one triggered the assertion.
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit MismatchNumberingSubset(const NumberingSubset& expected,
                                         const NumberingSubset& given,
                                         std::string&& vector_name,
                                         const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~MismatchNumberingSubset() override;
    };


    //! When an internal PETSc object should have not been initialized and was notetheless.
    struct MismatchNumberingSubsetInMatrixProduct : public ::MoReFEM::Advanced::Assertion
    {
        /*!
         * \brief Constructor when checking expectation regarding a matrix.
         *
         * \param[in] first_matrix_col_ns  `NumberingSubset` used for columns of first matrix.
         * \param[in] second_matrix_row_ns NumberingSubset& used for rows of second matrix.
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit MismatchNumberingSubsetInMatrixProduct(
            const NumberingSubset& first_matrix_col_ns,
            const NumberingSubset& second_matrix_row_ns,
            const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~MismatchNumberingSubsetInMatrixProduct() override;
    };


    //! When a vector is provided at two different locations for a given linear algebra operation
    //! that doesn't support it.
    struct ForbiddenSameVector : public ::MoReFEM::Advanced::Assertion
    {
        /*!
         * \brief Constructor when checking expectation regarding a matrix.
         *
         * \param[in] operation_name Name of the operation involved, e.g. 'MatMultTransposeAdd`
         * \param[in] arg_position_list Position in the function of the two duplicate parameters.
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit ForbiddenSameVector(std::string_view operation_name,
                                     std::pair<std::size_t, std::size_t>&& arg_position_list,
                                     const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~ForbiddenSameVector() override;
    };


#endif // NDEBUG

} // namespace MoReFEM::GlobalLinearAlgebraNS::AssertionNS

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_ASSERTION_ASSERTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
