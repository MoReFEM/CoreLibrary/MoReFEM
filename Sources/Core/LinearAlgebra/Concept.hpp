// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_CONCEPT_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Petsc/Matrix/Concept.hpp" // IWYU pragma: export


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify a type which is a global linear algebra object with information related to `NumberingSubset`.
     *
     * The idea is to couple this with concept such as `PetscStrictlyMatrix` to indicate we need a PETSc matrix with the
     * additional `NumberingSubset` information defined in `Core` module - this is for instace used in the higher level
     * free functions defining matrix operations, that typically will use the extra `NumberingSubset` information to
     * check the intended operation makes sense (and if not issue an `Assertion`.
     */
    template<typename T>
    concept GlobalLinearAlgebra = requires {
        { T::ConceptIsGlobalLinearAlgebra };
    };


    /*!
     * \class doxygen_hide_global_linear_algebra_matrix_concept_keyword
     *
     * \brief Keyword to indicate that this class qualify for the Concept::GlobalLinearAlgebra.
     *
     */


    /*!
     * \brief Concept to indicate the object may be used in matrix operations at any place and that this object includes `NumberingSubset` information.
     *
     * This is typically a `GlobalMatrix`, but `GlobalMatrixOpResult` also qualifies.
     *
     * \internal Some lower level functions, defined in the `MoReFEM::Wrappers::Petsc` namespace, don't qualify for the `NumberingSubset`
     * condition of this concept.
     */
    template<typename T>
    concept GlobalMatrixLike = GlobalLinearAlgebra<T> && PetscMatrix<T>;


    /*!
     * \brief Concept to indicate this matrix type is intended to store result of some PETSc computations and includes `NumberingSubset` information.
     */
    template<typename T>
    concept GlobalMatrixOpResult = GlobalLinearAlgebra<T> && PetscMatrixOperationResult<T>;


} // namespace MoReFEM::Concept

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
