// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <optional>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#ifndef NDEBUG
#include <iostream>
#endif // NDEBUG

#include <cassert>

#include "Core/CommandLineFlags/Advanced/CommandLineFlags.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {

        /*!
         * \brief Assign chosen string name if one was provided, or generate a default one if not.
         */
        std::string AssignName(std::optional<std::string> name,
                               const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& col_numbering_subset);

    } // namespace


    GlobalMatrix::~GlobalMatrix() = default;


    GlobalMatrix::GlobalMatrix(const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& col_numbering_subset,
                               std::optional<std::string> name)
    : petsc_parent(AssignName(std::move(name), row_numbering_subset, col_numbering_subset),
                   Advanced::CommandLineFlagsNS::DoPrintLinearAlgebraDestruction()),
      numbering_subset_parent(row_numbering_subset, col_numbering_subset)
    { }


    // NOLINTBEGIN(cppcoreguidelines-slicing) - the sliced part is handled properly with numbering subset argument.
    GlobalMatrix::GlobalMatrix(const GlobalMatrix& rhs, std::optional<std::string> name)
    : petsc_parent(rhs, AssignName(std::move(name), rhs.GetRowNumberingSubset(), rhs.GetColNumberingSubset())),
      numbering_subset_parent(rhs)
    { }
    // NOLINTEND(cppcoreguidelines-slicing)


    GlobalMatrix::GlobalMatrix(const Wrappers::Mpi& mpi,
                               const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& col_numbering_subset,
                               Internal::WrapPetscMat wrap_petsc_matrix)
    : petsc_parent("from_petsc_Mat"), numbering_subset_parent(row_numbering_subset, col_numbering_subset)
    {
        parent::SetDoCallPetscDestroy(call_petsc_destroy::no);
        parent::ChangeInternal(mpi, wrap_petsc_matrix.GetContent());
    }


    void Swap(GlobalMatrix& A, GlobalMatrix& B)
    {
        // We swap the content of two matrices that share the same numbering subsets.
        assert(A.GetColNumberingSubset() == B.GetColNumberingSubset());
        assert(A.GetRowNumberingSubset() == B.GetRowNumberingSubset());

        using parent = GlobalMatrix::petsc_parent;

        Swap(static_cast<parent&>(A), static_cast<parent&>(B));
    }


#ifndef NDEBUG
    void AssertSameNumberingSubset(const GlobalMatrix& matrix1, const GlobalMatrix& matrix2)
    {
        assert(matrix1.GetRowNumberingSubset() == matrix2.GetRowNumberingSubset());
        assert(matrix1.GetColNumberingSubset() == matrix2.GetColNumberingSubset());
    }


    void PrintNumberingSubset(std::string_view matrix_name, const GlobalMatrix& matrix)
    {
        std::cout << "Numbering subsets for matrix '" << matrix_name << "': row -> "
                  << matrix.GetRowNumberingSubset().GetUniqueId() << " and col -> "
                  << matrix.GetColNumberingSubset().GetUniqueId() << '\n';
    }
#endif // NDEBUG


    namespace // anonymous
    {

        std::string AssignName(std::optional<std::string> name,
                               const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& col_numbering_subset)
        {
            if (name.has_value())
                return name.value();

            std::ostringstream oconv;
            oconv << "Matrix_ns_" << row_numbering_subset.GetUniqueId() << '_' << col_numbering_subset.GetUniqueId();
            std::string ret = oconv.str();
            return ret;
        }

    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
