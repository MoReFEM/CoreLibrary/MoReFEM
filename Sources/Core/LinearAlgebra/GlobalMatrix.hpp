// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_GLOBALMATRIX_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_GLOBALMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

// IWYU pragma: begin_exports
#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"

#include "Core/Crtp/NumberingSubsetForMatrix.hpp"
#include "Core/LinearAlgebra/Internal/WrapPetscObject.hpp"
// IWYU pragma: end_exports


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

namespace MoReFEM::Internal::SolverNS
{

    template<class VariationalFormulationT>
    struct SnesInterface; // IWYU pragma: keep

} // namespace MoReFEM::Internal::SolverNS

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Class which encapsulates both the Petsc matrix and the numbering subsets used to described its
     * rows and columns.
     */
    class GlobalMatrix : public Wrappers::Petsc::Matrix, public Crtp::NumberingSubsetForMatrix<GlobalMatrix>
    {

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<GlobalMatrix>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to an array of unique pointers.
        template<std::size_t I>
        using array_unique_ptr = std::array<unique_ptr, I>;

        //! Alias to parent.
        using petsc_parent = Wrappers::Petsc::Matrix;

        //! Alias to other parent.
        using numbering_subset_parent = Crtp::NumberingSubsetForMatrix<GlobalMatrix>;

        //! Friendship (to enable ChangeInternal usage).
        template<class VariationalFormulationT>
        friend struct Internal::SolverNS::SnesInterface;

        //! \copydoc doxygen_hide_global_linear_algebra_matrix_concept_keyword
        static inline constexpr bool ConceptIsGlobalLinearAlgebra = true;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] row_numbering_subset \a NumberingSubset to use to define the numbering of rows.
         * \param[in] col_numbering_subset \a NumberingSubset to use to define the numbering of columns.
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         * If name is not provided, a default name featuring the numbering subset number is used.
         */
        explicit GlobalMatrix(const NumberingSubset& row_numbering_subset,
                              const NumberingSubset& col_numbering_subset,
                              std::optional<std::string> name = std::nullopt);

        //! Destructor.
        ~GlobalMatrix() override;

        /*!
         * \copydoc doxygen_hide_copy_constructor
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         * If name is not provided, a default name featuring the numbering subset numbers is used.
         */
        GlobalMatrix(const GlobalMatrix& rhs, std::optional<std::string> name = std::nullopt);

        //! \copydoc doxygen_hide_move_constructor
        GlobalMatrix(GlobalMatrix&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalMatrix& operator=(const GlobalMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalMatrix& operator=(GlobalMatrix&& rhs) = delete;

        ///@}

      private:
        /*!
         * \brief Constructor to provide a thin-wrapper over a \a Mat object.
         *
         * This constructor is not meant to be used widely - hence its private status - : it is used only when
         * defining a Snes interface for a non-linear solver in PETSc. In this case, PETSc provides a \a Mat
         * object to compute quantities such as residual or evaluation_state; we want to be able to manipulate
         * them with high-level PETSc wrapper defined in MoReFEM.
         * PETSc owns these linear algebra quantities, so we do not call \a MatDestroy at destruction.
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] row_numbering_subset \a NumberingSubset to use to define the numbering of rows.
         * \param[in] col_numbering_subset \a NumberingSubset to use to define the numbering of columns.
         * \param[in] wrap_petsc_matrix A very thin wrapper object over a `Mat`. The sole purpose
         * of the class used here is to make apparent at call site that it is really a very special constructor
         * that is used.
         */
        explicit GlobalMatrix(const Wrappers::Mpi& mpi,
                              const NumberingSubset& row_numbering_subset,
                              const NumberingSubset& col_numbering_subset,
                              Internal::WrapPetscMat wrap_petsc_matrix);

      private:
        // ===========================================================================
        // \attention Do not forget to update Swap() if a new data member is added!
        // =============================================================================
    };


#ifndef NDEBUG


    /*!
     * \brief Assert two matrices share the same \a NumberingSubset.
     *
     * \param[in] matrix1 First matrix.
     * \param[in] matrix2 Second matrix.
     */
    void AssertSameNumberingSubset(const GlobalMatrix& matrix1, const GlobalMatrix& matrix2);


    /*!
     * \brief Debug tool to print the unique ids of row and column \a NumberingSubset.
     *
     * \param[in] matrix_name Tag to identify the matrix which \a NumberingSubset information will be written.
     * \param[in] matrix Matrix under investigation.
     */
    void PrintNumberingSubset(std::string_view matrix_name, const GlobalMatrix& matrix);


#endif // NDEBUG


    /*!
     * \brief Swap two matrices.
     *
     * The Petsc content of the matrices is swapped; however the numbering subsets must be the same on both ends
     * (we expect here to swap only matrices with same structure).
     *
     * \attention Do not use it until #530 is resolved; Petsc's defined swap might have to be used.
     *
     * \param[in] A One of the matrix to swap.
     * \param[in] B The other matrix.
     */
    void Swap(GlobalMatrix& A, GlobalMatrix& B);


    /*!
     * \class doxygen_hide_global_linear_algebra_forbidden_swap_function
     *
     * \brief Declared but do not defined: we do not want to be able to do this but we also want to avoid the
     * 'slicing effect' (i.e. attributes of child class ignored entirely).
     */

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalMatrix&, GlobalMatrix::petsc_parent&);

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalMatrix::petsc_parent&, GlobalMatrix&);


    /*!
     * \brief Useful alias to avoid cluttering the main programs with too low-level C++.
     *
     * \code
     * GlobalMatrixWithCoefficient(global_matrix, 1.)
     * \endcode
     *
     * is probably easier to grasp than either:
     * \code
     * std::pair<GlobalMatrix&, double>(global_matrix, 1.)
     * \endcode
     *
     * or
     *
     * \code
     * std::make_pair(std::ref(global_matrix, 1.)
     * \endcode
     */
    using GlobalMatrixWithCoefficient = std::pair<GlobalMatrix&, double>;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_GLOBALMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
