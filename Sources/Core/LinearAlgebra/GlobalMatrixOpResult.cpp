// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <optional>
#include <sstream>
#include <string>
#include <utility>
#ifndef NDEBUG
#endif // NDEBUG

#include <cassert>

#include "Core/CommandLineFlags/Advanced/CommandLineFlags.hpp"
#include "Core/LinearAlgebra/GlobalMatrixOpResult.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {

        /*!
         * \brief Assign chosen string name if one was provided, or generate a default one if not.
         */
        std::string AssignName(std::optional<std::string> name,
                               const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& col_numbering_subset);

    } // namespace


    GlobalMatrixOpResult::GlobalMatrixOpResult(const NumberingSubset& row_numbering_subset,
                                               const NumberingSubset& col_numbering_subset,
                                               std::optional<std::string> name)
    : petsc_parent(AssignName(std::move(name), row_numbering_subset, col_numbering_subset),
                   Advanced::CommandLineFlagsNS::DoPrintLinearAlgebraDestruction()),
      numbering_subset_parent(row_numbering_subset, col_numbering_subset)
    { }


    GlobalMatrixOpResult::~GlobalMatrixOpResult() = default;


    namespace // anonymous
    {

        std::string AssignName(std::optional<std::string> name,
                               const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& col_numbering_subset)
        {
            if (name.has_value())
                return name.value();

            std::ostringstream oconv;
            oconv << "MatrixOpResult_ns_" << row_numbering_subset.GetUniqueId() << '_'
                  << col_numbering_subset.GetUniqueId();
            std::string ret = oconv.str();
            return ret;
        }

    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
