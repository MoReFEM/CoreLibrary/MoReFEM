// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_GLOBALMATRIXOPRESULT_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_GLOBALMATRIXOPRESULT_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOpResult.hpp" // IWYU pragma: export

#include "Core/Crtp/NumberingSubsetForMatrix.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief This is a thin wrapper over `Wrappers::Petsc::MatrixOpResult`, which aims to store the result of matrix matrix products.
     *
     * The only addition here is the `NumberingSubset` data attributes; beware as there are no direct check whether the
     * \a NumberingSubset involved match the content of the computed matrix.
     *
     * However, if you provide wrong numbering subset you will get errors from PETSc if you try to use the result in an
     * inadequate operation.
     */
    class GlobalMatrixOpResult : public Wrappers::Petsc::MatrixOpResult,
                                 public Crtp::NumberingSubsetForMatrix<GlobalMatrixOpResult>
    {

      public:
        //! Alias to self.
        using self = GlobalMatrixOpResult;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to an array of `unique_ptr`.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;

        //! Alias to parent.
        using petsc_parent = Wrappers::Petsc::MatrixOpResult;

        static_assert(std::is_convertible<self*, petsc_parent*>());

        //! Alias to other parent.
        using numbering_subset_parent = Crtp::NumberingSubsetForMatrix<self>;

        static_assert(std::is_convertible<self*, numbering_subset_parent*>());

        //! \copydoc doxygen_hide_global_linear_algebra_matrix_concept_keyword
        static inline constexpr bool ConceptIsGlobalLinearAlgebra = true;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] row_numbering_subset \a NumberingSubset to use to define the numbering of rows.
         * \param[in] col_numbering_subset \a NumberingSubset to use to define the numbering of columns.
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         * If name is not provided, a default name featuring the numbering subset number is used.
         */
        explicit GlobalMatrixOpResult(const NumberingSubset& row_numbering_subset,
                                      const NumberingSubset& col_numbering_subset,
                                      std::optional<std::string> name = std::nullopt);

        //! Destructor.
        ~GlobalMatrixOpResult() override;

        /*!
         * \copydoc doxygen_hide_copy_constructor
         */
        GlobalMatrixOpResult(const GlobalMatrixOpResult& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalMatrixOpResult(GlobalMatrixOpResult&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalMatrixOpResult& operator=(const GlobalMatrixOpResult& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalMatrixOpResult& operator=(GlobalMatrixOpResult&& rhs) = delete;

        ///@}
    };


    /*!
     * \class doxygen_hide_global_linear_algebra_forbidden_swap_function
     *
     * \brief Declared but do not defined: we do not want to be able to do this but we also want to avoid the
     * 'slicing effect' (i.e. attributes of child class ignored entirely).
     */

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalMatrixOpResult&, GlobalMatrixOpResult::petsc_parent&);

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalMatrixOpResult::petsc_parent&, GlobalMatrixOpResult&);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_GLOBALMATRIXOPRESULT_DOT_HPP_
// *** MoReFEM end header guards *** < //
