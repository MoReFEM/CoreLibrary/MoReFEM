// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <optional>
#include <string>
#include <utility>
#ifndef NDEBUG
#include <iostream>
#endif // NDEBUG

#include <cassert>
#include <sstream>

#include "Core/CommandLineFlags/Advanced/CommandLineFlags.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {

        /*!
         * \brief Assign chosen string name if one was provided, or generate a default one if not.
         */
        std::string AssignName(std::optional<std::string> name, const NumberingSubset& numbering_subset);

    } // namespace


    GlobalVector::~GlobalVector() = default;


    GlobalVector::GlobalVector(const NumberingSubset& numbering_subset, std::optional<std::string> name)
    : parent(AssignName(std::move(name), numbering_subset),
             Advanced::CommandLineFlagsNS::DoPrintLinearAlgebraDestruction()),
      numbering_subset_(numbering_subset)
    { }


    GlobalVector::GlobalVector(const NumberingSubset& numbering_subset, Internal::WrapPetscVec wrap_petsc_vector)
    : parent("from_petsc_Vec"), numbering_subset_(numbering_subset)
    {
        parent::SetDoNotDestroyPetscVector();
        parent::ChangeInternal(wrap_petsc_vector.GetContent());
    }


    // NOLINTBEGIN(cppcoreguidelines-slicing) - the sliced part is handled properly with numbering subset argument.
    GlobalVector::GlobalVector(const GlobalVector& rhs, std::optional<std::string> name)
    : parent(rhs, AssignName(std::move(name), rhs.GetNumberingSubset())), numbering_subset_(rhs.numbering_subset_)
    // NOLINTEND(cppcoreguidelines-slicing)
    { }

    void Swap(GlobalVector& A, GlobalVector& B)
    {
        assert(A.GetNumberingSubset() == B.GetNumberingSubset());

        using parent = GlobalVector::parent;

        Swap(static_cast<parent&>(A), static_cast<parent&>(B));
    }


#ifndef NDEBUG
    void PrintNumberingSubset(std::string&& vector_name, const GlobalVector& vector)
    {
        std::cout << "Numbering subsets for vector '" << vector_name
                  << "': " << vector.GetNumberingSubset().GetUniqueId() << '\n';
    }
#endif // NDEBUG


    namespace // anonymous
    {

        std::string AssignName(std::optional<std::string> name, const NumberingSubset& numbering_subset)
        {
            if (name.has_value())
                return name.value();

            std::ostringstream oconv;
            oconv << "Vector_ns_" << numbering_subset.GetUniqueId();
            std::string ret = oconv.str();
            return ret;
        }

    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
