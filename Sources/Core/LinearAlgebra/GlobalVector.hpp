// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_GLOBALVECTOR_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_GLOBALVECTOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <optional>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp" // IWYU pragma: export

#include "Core/LinearAlgebra/Internal/WrapPetscObject.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

namespace MoReFEM::Internal::SolverNS
{

    template<class VariationalFormulationT>
    struct SnesInterface; // IWYU pragma: keep

} // namespace MoReFEM::Internal::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Class which encapsulates both the Petsc vector and the numbering subset used to described it.
     */
    class GlobalVector final : public Wrappers::Petsc::Vector
    {

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<GlobalVector>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to array of unique pointers.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;

        //! Alias to parent.
        using parent = Wrappers::Petsc::Vector;

        //! \copydoc doxygen_hide_alias_self
        using self = GlobalVector;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship (to enable ChangeInternal usage).
        template<class VariationalFormulationT>
        friend struct Internal::SolverNS::SnesInterface;

        //! \copydoc doxygen_hide_global_linear_algebra_matrix_concept_keyword
        static inline constexpr bool ConceptIsGlobalLinearAlgebra = true;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] numbering_subset \a NumberingSubset to use to define the numbering of the vector.
         * \copydetails doxygen_hide_petsc_vector_name_arg
         * If name is not provided, a default name featuring the numbering subset number is used.
         */
        explicit GlobalVector(const NumberingSubset& numbering_subset, std::optional<std::string> name = std::nullopt);


        //! Destructor.
        virtual ~GlobalVector() override;

        /*!
         * \copydoc doxygen_hide_copy_constructor
         *
         * \copydetails doxygen_hide_petsc_vector_name_arg
         * If name is not provided, a default name featuring the numbering subset number is used.
         */
        GlobalVector(const GlobalVector& rhs, std::optional<std::string> name = std::nullopt);

      private:
        /*!
         * \brief Constructor to provide a thin-wrapper over a \a Vec object.
         *
         * This constructor is not meant to be used widely - hence its private status - : it is used only when
         * defining a Snes interface for a non-linear solver in PETSc. In this case, PETSc provides a \a Vec
         * object to compute quantities such as residual or evaluation_state; we want to be able to manipulate
         * them with high-level PETSc wrapper defined in MoReFEM.
         * PETSc owns these linear algebra quantities, so we do not call \a VecDestroy at destruction.
         *
         * \param[in] numbering_subset \a NumberingSubset to use to define the numbering of the vector.
         * \param[in] wrap_petsc_vector A very thin wrapper object over a `Vec`. The sole purpose
         * of the class used here is to make apparent at call site that it is really a very special constructor
         * that is used.
         */
        explicit GlobalVector(const NumberingSubset& numbering_subset, Internal::WrapPetscVec wrap_petsc_vector);

      public:
        /*!
         * \copydoc doxygen_hide_move_constructor
         */
        GlobalVector(GlobalVector&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalVector& operator=(const GlobalVector& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalVector& operator=(GlobalVector&& rhs) = delete;

        ///@}

        //! Numbering subset used to describe vector.
        const NumberingSubset& GetNumberingSubset() const;

      private:
        // ===========================================================================
        // \attention Do not forget to update Swap() if a new data member is added!
        // =============================================================================

        //! Numbering subset used to describe vector.
        const NumberingSubset& numbering_subset_;
    };


#ifndef NDEBUG

    /*!
     * \brief Debug tool to print the unique id of \a NumberingSubset.
     *
     * \param[in] vector_name Tag to identify the vector which \a NumberingSubset information will be written.
     * \param[in] vector Vector under investigation.
     */
    void PrintNumberingSubset(std::string&& vector_name, const GlobalVector& vector);


#endif // NDEBUG


    /*!
     * \brief Swap two vectors.
     *
     * The Petsc content of the vectors is swapped; however the numbering subsets must be the same on both ends
     * (we expect here to swap only vectors with same structure).
     *
     * \attention Do not use it until #530 is resolved; Petsc's defined swap might have to be used.
     *
     * \param[in] A One of the vector to swap.
     * \param[in] B The other matrix.
     */
    void Swap(GlobalVector& A, GlobalVector& B);

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalVector&, GlobalVector::parent&);

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalVector::parent&, GlobalVector&);


    /*!
     * \brief Useful alias to avoid cluttering the main programs with too low-level C++.
     *
     * \code
     * GlobalVectorWithCoefficient(vm.GetNonCstForce(), 1.)
     * \endcode
     *
     * is probably easier to grasp than either:
     * \code
     * std::pair<GlobalVector&, double>(vm.GetNonCstForce(), 1.)
     * \endcode
     *
     * or
     *
     * \code
     * std::make_pair(std::ref(vm.GetNonCstForce()), 1.)
     * \endcode
     */
    using GlobalVectorWithCoefficient = std::pair<GlobalVector&, double>;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/LinearAlgebra/GlobalVector.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_GLOBALVECTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
