// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_INTERNAL_WRAPPETSCOBJECT_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_INTERNAL_WRAPPETSCOBJECT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/Type.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp" // IWYU pragma: export
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GlobalMatrix; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal
{


    /*!
     * \brief A *very* thin wrapper over PETSc `Vec` or `Mat` object.
     *
     * The point here is really just to ensure there's no ambiguity in  constructor called;
     *  for instance:
     *
     \code
     Vec petsc_residual;
     ...
     GlobalVector residual(row_numbering_subset, WrapPetscVec(petsc_residual));
     \endcode
     * (where \a WrapPetscVec is an alias defined in current file)
     *
     * Class itself just transmit the underlying PETSc vector or matrix.
     *
     * The ideal solution would have been to hide this constructor in a free function, but as \a GlobalMatrix
     * and \a GlobalVector got reference data attributes they are not movable or copyable, so the next best
     * thing is to make the constructor used in the edge case be as expressive as possible in its signature.
     *
     * \attention This class is NOT in charge of deleting the underlying Petsc object in any way whatsoever!
     * (on the contrary when it's use - in \a Snesinterface - the point is NOT to call destruction on a linear algebra
     * object that PETSc snes owns).
     */
    template<LinearAlgebraNS::type TypeT>
    class WrapPetscObject
    {
      public:
        //! \copydoc doxygen_hide_alias_self
        using self = WrapPetscObject<TypeT>;

        //! Underlying type
        using storage_type = std::conditional_t<TypeT == LinearAlgebraNS::type::vector, Vec, Mat>;

        //! Friendship to the class that may actually use current one.
        friend class MoReFEM::GlobalVector;

        //! Friendship to the class that may actually use current one.
        friend class MoReFEM::GlobalMatrix;


      public:
        /// \name Special members.
        ///@{
        /*!
         * \brief Constructor.
         *
         * \param[in] petsc_object \a Vec or \a Mat object to be covered by the wrapper.
         */
        explicit WrapPetscObject(storage_type petsc_object);

        //! Destructor.
        ~WrapPetscObject() = default;

        //! \copydoc doxygen_hide_copy_constructor
        WrapPetscObject(const WrapPetscObject& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        WrapPetscObject(WrapPetscObject&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        WrapPetscObject& operator=(const WrapPetscObject& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        WrapPetscObject& operator=(WrapPetscObject&& rhs) = delete;

        ///@}

      private:
        /*!
         * \brief Accessor to the wrapped object.
         *
         * \internal As this object is in fact an alias to a pointer pass-by-value is enough.
         *
         * \internal Is private as the whole point of this class is to help \a GlobalMatrix and \a GlobalVector
         * construction; should not be used in any other context.
         */
        storage_type GetContent() const noexcept;

      private:
        //! The wrapper PETSc object (either a \a Vec or a \a Mat depending on template parameter).
        storage_type content_;
    };


    //! Convenient alias.
    using WrapPetscVec = WrapPetscObject<LinearAlgebraNS::type::vector>;

    //! Convenient alias.
    using WrapPetscMat = WrapPetscObject<LinearAlgebraNS::type::matrix>;


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/LinearAlgebra/Internal/WrapPetscObject.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_INTERNAL_WRAPPETSCOBJECT_DOT_HPP_
// *** MoReFEM end header guards *** < //
