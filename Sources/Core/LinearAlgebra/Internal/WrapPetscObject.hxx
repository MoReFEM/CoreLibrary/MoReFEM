// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_INTERNAL_WRAPPETSCOBJECT_DOT_HXX_
#define MOREFEM_CORE_LINEARALGEBRA_INTERNAL_WRAPPETSCOBJECT_DOT_HXX_
// IWYU pragma: private, include "Core/LinearAlgebra/Internal/WrapPetscObject.hpp"
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/Type.hpp" // IWYU pragma: export


namespace MoReFEM::Internal
{


    template<LinearAlgebraNS::type TypeT>
    WrapPetscObject<TypeT>::WrapPetscObject(storage_type petsc_object) : content_{ petsc_object }
    { }


    template<LinearAlgebraNS::type TypeT>
    auto WrapPetscObject<TypeT>::GetContent() const noexcept -> storage_type
    {
        return content_;
    }

} // namespace MoReFEM::Internal

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_INTERNAL_WRAPPETSCOBJECT_DOT_HXX_
// *** MoReFEM end header guards *** < //
