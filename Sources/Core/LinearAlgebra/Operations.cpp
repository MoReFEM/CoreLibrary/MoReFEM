// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>

#include "Core/LinearAlgebra/Operations.hpp"

#include "Core/LinearAlgebra/Assertion/Assertion.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::GlobalLinearAlgebraNS
{


    void AXPY(PetscScalar alpha,
              const GlobalVector& x,
              GlobalVector& y,
              const std::source_location location,
              update_ghost do_update_ghost)
    {
#ifndef NDEBUG
        {
            decltype(auto) x_ns = x.GetNumberingSubset();
            decltype(auto) y_ns = y.GetNumberingSubset();

            if (x_ns != y_ns)
                throw AssertionNS::MismatchNumberingSubset(x_ns, y_ns, "y", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::AXPY(alpha, x, y, location, do_update_ghost);
    }


    double DotProduct(const GlobalVector& x, const GlobalVector& y, const std::source_location location)
    {
#ifndef NDEBUG
        {
            decltype(auto) x_ns = x.GetNumberingSubset();
            decltype(auto) y_ns = y.GetNumberingSubset();

            if (x_ns != y_ns)
                throw AssertionNS::MismatchNumberingSubset(x_ns, y_ns, "y", location);
        }
#endif // NDEBUG

        return Wrappers::Petsc::DotProduct(x, y, location);
    }


} // namespace MoReFEM::GlobalLinearAlgebraNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
