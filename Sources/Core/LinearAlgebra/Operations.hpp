// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_OPERATIONS_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_OPERATIONS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <source_location>

#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"

#include "Core/LinearAlgebra/Concept.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"


namespace MoReFEM::GlobalLinearAlgebraNS
{


    /*!
     * \copydoc doxygen_hide_mat_create_transpose
     *
     * \copydoc doxygen_hide_global_linear_algebra_overload
     */
    template<Concept::GlobalMatrixLike MatrixT, Concept::GlobalMatrixOpResult MatrixU>
    inline void MatCreateTranspose(const MatrixT& A,
                                   MatrixU& transpose,
                                   const std::source_location location = std::source_location::current());


    /*!
     * \copydoc doxygen_hide_mat_mult
     *
     * \copydoc doxygen_hide_global_linear_algebra_overload
     */
    template<Concept::GlobalMatrixLike MatrixT>
    inline void MatMult(const MatrixT& matrix,
                        const GlobalVector& v1,
                        GlobalVector& v2,
                        const std::source_location location = std::source_location::current(),
                        update_ghost do_update_ghost = update_ghost::yes);


    //! \copydoc doxygen_hide_mat_shift
    template<Concept::GlobalMatrixLike MatrixT>
    inline void MatShift(const PetscScalar a,
                         MatrixT& matrix,
                         const std::source_location location = std::source_location::current());


    /*!
     * \copydoc doxygen_hide_matrix_axpy
     *
     * \attention There are safeties added for the `NonZeroPattern::same` case but not for the `NonZeroPattern::subset` one (that would
     * require much more work than a straight `NumberingSubset` comparison). For `NonZeroPattern::different` no checks
     * would be helpful there in any case...
     */
    template<NonZeroPattern NonZeroPatternT, Concept::GlobalMatrixLike MatrixT, Concept::GlobalMatrixLike MatrixU>
    void AXPY(PetscScalar a,
              const MatrixT& X,
              MatrixU& Y,
              const std::source_location location = std::source_location::current());


    //! \copydoc doxygen_hide_mat_mult_add
    template<Concept::GlobalMatrixLike MatrixT>
    void MatMultAdd(const MatrixT& matrix,
                    const GlobalVector& v1,
                    const GlobalVector& v2,
                    GlobalVector& v3,
                    const std::source_location location = std::source_location::current(),
                    update_ghost do_update_ghost = update_ghost::yes);


    //! \copydoc doxygen_hide_mat_mult_transpose
    template<Concept::GlobalMatrixLike MatrixT>
    void MatMultTranspose(const MatrixT& matrix,
                          const GlobalVector& v1,
                          GlobalVector& v2,
                          const std::source_location location = std::source_location::current(),
                          update_ghost do_update_ghost = update_ghost::yes);

    //! \copydoc doxygen_hide_mat_mult_transpose_add
    template<Concept::GlobalMatrixLike MatrixT>
    void MatMultTransposeAdd(const MatrixT& matrix,
                             const GlobalVector& v1,
                             const GlobalVector& v2,
                             GlobalVector& v3,
                             const std::source_location location = std::source_location::current(),
                             update_ghost do_update_ghost = update_ghost::yes);

    /*!
     * \copydoc doxygen_hide_mat_mat_mult
     *

     */
    template<Concept::GlobalMatrixLike MatrixT,
             Concept::GlobalMatrixLike MatrixU,
             Concept::GlobalMatrixOpResult MatrixV>
    void MatMatMult(const MatrixT& m1,
                    const MatrixU& m2,
                    MatrixV& m3,
                    const std::source_location location = std::source_location::current());

    //! \copydoc doxygen_hide_mat_transpose_mat_mult
    template<Concept::GlobalMatrixLike MatrixT,
             Concept::GlobalMatrixLike MatrixU,
             Concept::GlobalMatrixOpResult MatrixV>
    void MatTransposeMatMult(const MatrixT& m1,
                             const MatrixU& m2,
                             MatrixV& m3,
                             const std::source_location location = std::source_location::current());


    //! \copydoc doxygen_hide_mat_mat_transpose_mult
    template<Concept::GlobalMatrixLike MatrixT,
             Concept::GlobalMatrixLike MatrixU,
             Concept::GlobalMatrixOpResult MatrixV>
    void MatMatTransposeMult(const MatrixT& matrix1,
                             const MatrixU& matrix2,
                             MatrixV& matrix3,
                             const std::source_location location = std::source_location::current());


    //! \copydoc doxygen_hide_mat_mat_mat_mult
    // clang-format off
    template
    <
        Concept::GlobalMatrixLike MatrixT,
        Concept::GlobalMatrixLike MatrixU,
        Concept::GlobalMatrixLike MatrixV,
        Concept::GlobalMatrixOpResult MatrixW
    >
    // clang-format on
    void MatMatMatMult(const MatrixT& m1,
                       const MatrixU& m2,
                       const MatrixV& m3,
                       MatrixW& m4,
                       const std::source_location location = std::source_location::current());


    //! \copydoc doxygen_hide_mat_pt_a_p
    template<Concept::GlobalMatrixLike MatrixT,
             Concept::GlobalMatrixLike MatrixU,
             Concept::GlobalMatrixOpResult MatrixV>
    void PtAP(const MatrixT& A,
              const MatrixU& P,
              MatrixV& out,
              const std::source_location location = std::source_location::current());


    //! \copydoc doxygen_hide_vec_axpy
    void AXPY(PetscScalar alpha,
              const GlobalVector& x,
              GlobalVector& y,
              const std::source_location location = std::source_location::current(),
              update_ghost do_update_ghost = update_ghost::yes);


    //! \copydoc doxygen_hide_dot_product_function
    double DotProduct(const GlobalVector& x,
                      const GlobalVector& y,
                      const std::source_location location = std::source_location::current());


} // namespace MoReFEM::GlobalLinearAlgebraNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Core/LinearAlgebra/Operations.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_OPERATIONS_DOT_HPP_
// *** MoReFEM end header guards *** < //
