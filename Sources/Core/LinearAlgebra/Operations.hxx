// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_OPERATIONS_DOT_HXX_
#define MOREFEM_CORE_LINEARALGEBRA_OPERATIONS_DOT_HXX_
// IWYU pragma: private, include "Core/LinearAlgebra/Operations.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: begin_exports
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"

#include "Core/LinearAlgebra/Assertion/Assertion.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::GlobalLinearAlgebraNS
{


    template<Concept::GlobalMatrixLike MatrixT, Concept::GlobalMatrixOpResult MatrixU>
    void MatCreateTranspose(const MatrixT& A, MatrixU& transpose, const std::source_location location)
    {
#ifndef NDEBUG
        if ((transpose.GetRowNumberingSubset() != A.GetColNumberingSubset())
            || (transpose.GetColNumberingSubset() != A.GetRowNumberingSubset()))
            throw AssertionNS::MismatchNumberingSubset(A.GetColNumberingSubset(),
                                                       A.GetRowNumberingSubset(),
                                                       transpose.GetRowNumberingSubset(),
                                                       transpose.GetColNumberingSubset(),
                                                       "transpose",
                                                       location);

#endif // NDEBUG

        Wrappers::Petsc::MatCreateTranspose(A, transpose, location);
    }


    template<Concept::GlobalMatrixLike MatrixT>
    void MatMult(const MatrixT& matrix,
                 const GlobalVector& v1,
                 GlobalVector& v2,
                 const std::source_location location,
                 update_ghost do_update_ghost)
    {
#ifndef NDEBUG
        {
            decltype(auto) row_ns = matrix.GetRowNumberingSubset();
            decltype(auto) col_ns = matrix.GetColNumberingSubset();
            decltype(auto) v1_ns = v1.GetNumberingSubset();
            decltype(auto) v2_ns = v2.GetNumberingSubset();

            if (col_ns != v1_ns)
                throw AssertionNS::MismatchNumberingSubset(col_ns, v1_ns, "v1", location);

            if (row_ns != v2_ns)
                throw AssertionNS::MismatchNumberingSubset(row_ns, v2_ns, "v2", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatMult(matrix, v1, v2, location, do_update_ghost);
    }


    template<Concept::GlobalMatrixLike MatrixT>
    void MatShift(const PetscScalar a, MatrixT& matrix, const std::source_location location)
    {
        Wrappers::Petsc::MatShift(a, matrix, location);
    }


    template<NonZeroPattern NonZeroPatternT, Concept::GlobalMatrixLike MatrixT, Concept::GlobalMatrixLike MatrixU>
    void AXPY(PetscScalar a, const MatrixT& X, MatrixU& Y, const std::source_location location)
    {
#ifndef NDEBUG
        if constexpr (NonZeroPatternT == NonZeroPattern::same)
        {
            decltype(auto) x_row_ns = X.GetRowNumberingSubset();
            decltype(auto) x_col_ns = X.GetColNumberingSubset();
            decltype(auto) y_row_ns = Y.GetRowNumberingSubset();
            decltype(auto) y_col_ns = Y.GetColNumberingSubset();

            if ((x_row_ns != y_row_ns) || (x_col_ns != y_col_ns))
                throw AssertionNS::MismatchNumberingSubset(x_row_ns, x_col_ns, y_row_ns, y_col_ns, "Y", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::AXPY<NonZeroPatternT>(a, X, Y, location);
    }


    template<Concept::GlobalMatrixLike MatrixT>
    void MatMultAdd(const MatrixT& matrix,
                    const GlobalVector& v1,
                    const GlobalVector& v2,
                    GlobalVector& v3,
                    const std::source_location location,
                    update_ghost do_update_ghost)
    {
#ifndef NDEBUG
        {
            decltype(auto) v2_ns = v2.GetNumberingSubset();
            decltype(auto) v3_ns = v3.GetNumberingSubset();

            if (v3_ns != v2_ns)
                throw AssertionNS::MismatchNumberingSubset(v2_ns, v3_ns, "v3", location);

            decltype(auto) mat_col_ns = matrix.GetColNumberingSubset();
            decltype(auto) mat_row_ns = matrix.GetRowNumberingSubset();
            decltype(auto) v1_ns = v1.GetNumberingSubset();

            if ((mat_col_ns != v1_ns) || (mat_row_ns != v2_ns))
                throw AssertionNS::MismatchNumberingSubset(v2_ns, v1_ns, mat_row_ns, mat_col_ns, "matrix", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatMultAdd(matrix, v1, v2, v3, location, do_update_ghost);
    }


    template<Concept::GlobalMatrixLike MatrixT>
    void MatMultTranspose(const MatrixT& matrix,
                          const GlobalVector& v1,
                          GlobalVector& v2,
                          const std::source_location location,
                          update_ghost do_update_ghost)
    {
#ifndef NDEBUG
        {
            decltype(auto) mat_col_ns = matrix.GetColNumberingSubset();
            decltype(auto) mat_row_ns = matrix.GetRowNumberingSubset();
            decltype(auto) v1_ns = v1.GetNumberingSubset();
            decltype(auto) v2_ns = v2.GetNumberingSubset();

            if ((v1_ns != mat_row_ns) || (v2_ns != mat_col_ns))
                throw AssertionNS::MismatchNumberingSubset(v1_ns, v2_ns, mat_row_ns, mat_col_ns, "matrix", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatMultTranspose(matrix, v1, v2, location, do_update_ghost);
    }


    template<Concept::GlobalMatrixLike MatrixT>
    void MatMultTransposeAdd(const MatrixT& matrix,
                             const GlobalVector& v1,
                             const GlobalVector& v2,
                             GlobalVector& v3,
                             const std::source_location location,
                             update_ghost do_update_ghost)
    {
#ifndef NDEBUG
        {
            decltype(auto) mat_col_ns = matrix.GetColNumberingSubset();
            decltype(auto) mat_row_ns = matrix.GetRowNumberingSubset();
            decltype(auto) v1_ns = v1.GetNumberingSubset();
            decltype(auto) v2_ns = v2.GetNumberingSubset();
            decltype(auto) v3_ns = v3.GetNumberingSubset();

            if ((v1_ns != mat_row_ns) || (v3_ns != mat_col_ns))
                throw AssertionNS::MismatchNumberingSubset(v1_ns, v3_ns, mat_row_ns, mat_col_ns, "matrix", location);

            if (v2_ns != v3_ns)
                throw AssertionNS::MismatchNumberingSubset(v3_ns, v2_ns, "v2", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatMultTransposeAdd(matrix, v1, v2, v3, location, do_update_ghost);
    }


    template<Concept::GlobalMatrixLike MatrixT,
             Concept::GlobalMatrixLike MatrixU,
             Concept::GlobalMatrixOpResult MatrixV>
    void MatMatMult(const MatrixT& m1, const MatrixU& m2, MatrixV& m3, const std::source_location location)
    {
#ifndef NDEBUG
        {
            decltype(auto) m1_col_ns = m1.GetColNumberingSubset();
            decltype(auto) m1_row_ns = m1.GetRowNumberingSubset();
            decltype(auto) m2_col_ns = m2.GetColNumberingSubset();
            decltype(auto) m2_row_ns = m2.GetRowNumberingSubset();
            decltype(auto) m3_col_ns = m3.GetColNumberingSubset();
            decltype(auto) m3_row_ns = m3.GetRowNumberingSubset();

            if (m1_col_ns != m2_row_ns)
                throw AssertionNS::MismatchNumberingSubsetInMatrixProduct(m1_col_ns, m2_row_ns, location);

            if ((m3_row_ns != m1_row_ns) || (m3_col_ns != m2_col_ns))
                throw AssertionNS::MismatchNumberingSubset(m1_row_ns, m2_col_ns, m3_row_ns, m3_col_ns, "m3", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatMatMult(m1, m2, m3, location);
    }


    template<Concept::GlobalMatrixLike MatrixT,
             Concept::GlobalMatrixLike MatrixU,
             Concept::GlobalMatrixOpResult MatrixV>
    void MatTransposeMatMult(const MatrixT& m1, const MatrixU& m2, MatrixV& m3, const std::source_location location)
    {
#ifndef NDEBUG
        {
            decltype(auto) m1_col_ns = m1.GetColNumberingSubset();
            decltype(auto) m1_row_ns = m1.GetRowNumberingSubset();
            decltype(auto) m2_col_ns = m2.GetColNumberingSubset();
            decltype(auto) m2_row_ns = m2.GetRowNumberingSubset();
            decltype(auto) m3_col_ns = m3.GetColNumberingSubset();
            decltype(auto) m3_row_ns = m3.GetRowNumberingSubset();

            if (m1_row_ns != m2_row_ns)
                throw AssertionNS::MismatchNumberingSubsetInMatrixProduct(m1_row_ns, m2_row_ns, location);

            if ((m3_row_ns != m1_col_ns) || (m3_col_ns != m2_col_ns))
                throw AssertionNS::MismatchNumberingSubset(m1_col_ns, m2_col_ns, m3_row_ns, m3_col_ns, "m3", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatTransposeMatMult(m1, m2, m3, location);
    }


    template<Concept::GlobalMatrixLike MatrixT,
             Concept::GlobalMatrixLike MatrixU,
             Concept::GlobalMatrixOpResult MatrixV>
    void MatMatTransposeMult(const MatrixT& m1, const MatrixU& m2, MatrixV& m3, const std::source_location location)
    {
#ifndef NDEBUG
        {
            decltype(auto) m1_col_ns = m1.GetColNumberingSubset();
            decltype(auto) m1_row_ns = m1.GetRowNumberingSubset();
            decltype(auto) m2_col_ns = m2.GetColNumberingSubset();
            decltype(auto) m2_row_ns = m2.GetRowNumberingSubset();
            decltype(auto) m3_col_ns = m3.GetColNumberingSubset();
            decltype(auto) m3_row_ns = m3.GetRowNumberingSubset();

            if (m1_col_ns != m2_col_ns)
                throw AssertionNS::MismatchNumberingSubsetInMatrixProduct(m1_col_ns, m2_col_ns, location);

            if ((m3_row_ns != m1_row_ns) || (m3_col_ns != m2_row_ns))
                throw AssertionNS::MismatchNumberingSubset(m1_row_ns, m2_row_ns, m3_row_ns, m3_col_ns, "m3", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatMatTransposeMult(m1, m2, m3, location);
    }


    // clang-format off
    template
    <
        Concept::GlobalMatrixLike MatrixT,
        Concept::GlobalMatrixLike MatrixU,
        Concept::GlobalMatrixLike MatrixV,
        Concept::GlobalMatrixOpResult MatrixW
    >
    // clang-format on
    void MatMatMatMult(const MatrixT& m1,
                       const MatrixU& m2,
                       const MatrixV& m3,
                       MatrixW& m4,
                       const std::source_location location)
    {
#ifndef NDEBUG
        {
            decltype(auto) m1_col_ns = m1.GetColNumberingSubset();
            decltype(auto) m1_row_ns = m1.GetRowNumberingSubset();
            decltype(auto) m2_col_ns = m2.GetColNumberingSubset();
            decltype(auto) m2_row_ns = m2.GetRowNumberingSubset();
            decltype(auto) m3_col_ns = m3.GetColNumberingSubset();
            decltype(auto) m3_row_ns = m3.GetRowNumberingSubset();
            decltype(auto) m4_col_ns = m4.GetColNumberingSubset();
            decltype(auto) m4_row_ns = m4.GetRowNumberingSubset();

            if (m1_col_ns != m2_row_ns)
                throw AssertionNS::MismatchNumberingSubsetInMatrixProduct(m1_col_ns, m2_row_ns, location);

            if (m2_col_ns != m3_row_ns)
                throw AssertionNS::MismatchNumberingSubsetInMatrixProduct(m2_col_ns, m3_row_ns, location);


            if ((m4_row_ns != m1_row_ns) || (m4_col_ns != m3_col_ns))
                throw AssertionNS::MismatchNumberingSubset(m1_row_ns, m3_col_ns, m4_row_ns, m4_col_ns, "m4", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::MatMatMatMult(m1, m2, m3, m4, location);
    }


    // clang-format off
    template
    <
        Concept::GlobalMatrixLike MatrixT,
        Concept::GlobalMatrixLike MatrixU,
        Concept::GlobalMatrixOpResult MatrixV
    >
    // clang-format on
    void PtAP(const MatrixT& A, const MatrixU& P, MatrixV& out, const std::source_location location)
    {
#ifndef NDEBUG
        {
            decltype(auto) p_row_ns = P.GetRowNumberingSubset();
            decltype(auto) p_col_ns = P.GetColNumberingSubset();
            decltype(auto) a_row_ns = A.GetRowNumberingSubset();
            decltype(auto) out_col_ns = out.GetColNumberingSubset();
            decltype(auto) out_row_ns = out.GetRowNumberingSubset();

            if (p_row_ns != a_row_ns)
                throw AssertionNS::MismatchNumberingSubsetInMatrixProduct(p_row_ns, a_row_ns, location);

            if ((out_row_ns != p_col_ns) || (out_col_ns != p_col_ns))
                throw AssertionNS::MismatchNumberingSubset(p_col_ns, p_col_ns, out_row_ns, out_col_ns, "out", location);
        }
#endif // NDEBUG

        Wrappers::Petsc::PtAP(A, P, out, location);
    }


} // namespace MoReFEM::GlobalLinearAlgebraNS

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_OPERATIONS_DOT_HXX_
// *** MoReFEM end header guards *** < //
