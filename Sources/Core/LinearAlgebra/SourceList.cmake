### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/GlobalDiagonalMatrix.cpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalMatrix.cpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalMatrixOpResult.cpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalVector.cpp
		${CMAKE_CURRENT_LIST_DIR}/Operations.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Concept.hpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalDiagonalMatrix.hpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalDiagonalMatrix.hxx
		${CMAKE_CURRENT_LIST_DIR}/GlobalMatrix.hpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalMatrixOpResult.hpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalVector.hpp
		${CMAKE_CURRENT_LIST_DIR}/GlobalVector.hxx
		${CMAKE_CURRENT_LIST_DIR}/LinearAlgebra.doxygen
		${CMAKE_CURRENT_LIST_DIR}/Operations.hpp
		${CMAKE_CURRENT_LIST_DIR}/Operations.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Assertion/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
