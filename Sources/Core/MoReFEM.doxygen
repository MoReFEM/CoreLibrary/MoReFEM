/// \namespace MoReFEM
/// \brief Main MoReFEM namespace.

/*!
 * \class doxygen_hide_matrix_numbering_subset_arg
 *
 * \param[in] row_numbering_subset Numbering subset onto which dofs on the row of the matrix are defined.
 * \param[in] col_numbering_subset Numbering subset onto which dofs on the columns of the matrix are defined.
 */


/*!
* \class doxygen_hide_vector_numbering_subset_arg
*
* \param[in] numbering_subset Numbering subset onto which dofs on the vector are defined.
*/


/*!
* \class doxygen_hide_do_consider_processor_wise_local_2_global
*
* \param[in] do_consider_processor_wise_local_2_global If no, only the program-wise local2global array
* is computed. If yes, both program- and processor-wise ones are computed. By default it is no: some global
* operators don't need it so it's best to avoid cluttering memory when it doesn't matter.
*/

/*!
* \class doxygen_hide_do_consider_processor_wise_local_2_global_tparam
*
* \tparam DoConsiderProcessorWiseLocal2Global If no, only the program-wise local2global array
* is computed. If yes, both program- and processor-wise ones are computed. By default it is no: some global
* operators don't need it so it's best to avoid cluttering memory when it doesn't matter.
*/


/*!
* \class doxygen_hide_do_compute_processor_wise_local_2_global_arg
*
* \param[in] do_compute_processor_wise_local_2_global If yes, compute the processor-wise local2global as well.
* Program-wise one is generated in any case.
*/



/// \namespace MoReFEM::Advanced
/// \brief Namespace with content that should be of use only for advanced users of the library.
///
/// You may need stuff in this namespace if you need to tailor something to a specific need: for instance
/// if you need to define a new operator yourself, or a new finite element.


/// \namespace MoReFEM::Internal
/// \brief Namespace with content that should be of use only for developers of the library.
///
/// If you're writing your own model, you shouldn't have to use anything from this namespace.
/// If there is something you really need that is there, please contact a maintainer of the library and/or
/// open an issue in Gitlab: it might indicate something was poorly labeled and should have been in 
/// `Advanced` namespace.


/// \namespace MoReFEM::ExceptionNS
/// \brief Namespace for MoReFEM exceptions.