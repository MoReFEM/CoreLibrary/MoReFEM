// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_ADVANCED_CONCEPT_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <optional>

#include "Utilities/Miscellaneous.hpp"

#include "Core/MoReFEMData/Enum.hpp"


namespace MoReFEM::Advanced::Concept
{


    /*!
     * \brief Defines a concept to identify a type is a section of an \a MoReFEMData object.
     *
     */
    template<typename T>
    concept MoReFEMDataType = requires(T object) {
        (T::ConceptIsMoReFEMData == is_morefem_data::yes) || (T::ConceptIsMoReFEMData == is_morefem_data::for_test);
        object.GetInputData();
        object.GetModelSettings();
    };


} // namespace MoReFEM::Advanced::Concept


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
