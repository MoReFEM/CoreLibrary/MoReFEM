// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_ADVANCED_PARALLELISMSTRATEGY_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_ADVANCED_PARALLELISMSTRATEGY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    //! Enum class which specifies the possible behaviour related to parallelism.
    enum class parallelism_strategy { none, precompute, parallel_no_write, parallel, run_from_preprocessed };


    /*!
     * \brief Extract from \a morefem_data the parallelism strategy in use.
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * \return The \a parallelism_strategy is there is a Parallelism block in the file, or parallelism_strategy::none otherwise.
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    parallelism_strategy ExtractParallelismStrategy(const MoReFEMDataT& morefem_data);


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_ADVANCED_PARALLELISMSTRATEGY_DOT_HPP_
// *** MoReFEM end header guards *** < //
