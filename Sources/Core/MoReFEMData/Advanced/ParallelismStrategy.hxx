// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_ADVANCED_PARALLELISMSTRATEGY_DOT_HXX_
#define MOREFEM_CORE_MOREFEMDATA_ADVANCED_PARALLELISMSTRATEGY_DOT_HXX_
// IWYU pragma: private, include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"
// *** MoReFEM header guards *** < //


#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    parallelism_strategy ExtractParallelismStrategy(const MoReFEMDataT& morefem_data)
    {
        if constexpr (!MoReFEMDataT::HasParallelismField())
        {
            assert(morefem_data.GetParallelismPtr() == nullptr);
            return parallelism_strategy::none;
        } else
        {
            const auto parallelism_ptr = morefem_data.GetParallelismPtr();
            assert(!(!parallelism_ptr));
            return parallelism_ptr->GetParallelismStrategy();
        }
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_ADVANCED_PARALLELISMSTRATEGY_DOT_HXX_
// *** MoReFEM end header guards *** < //
