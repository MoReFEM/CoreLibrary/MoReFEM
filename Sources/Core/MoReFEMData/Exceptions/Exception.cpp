// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>

#include "Core/MoReFEMData/Exceptions/Exception.hpp"

#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{


    std::string NonExistingLuaFileMsg(const MoReFEM::FilesystemNS::File& input_data_file);


} // namespace


namespace MoReFEM::ExceptionNS::MoReFEMDataNS
{


    NonExistingLuaFile::~NonExistingLuaFile() = default;


    NonExistingLuaFile::NonExistingLuaFile(const FilesystemNS::File& input_data_file,
                                           const std::source_location location)
    : MoReFEM::Exception(NonExistingLuaFileMsg(input_data_file), location), input_data_file_(input_data_file)
    { }


    const FilesystemNS::File& NonExistingLuaFile::GetInputDataFile() const noexcept
    {
        return input_data_file_;
    }

} // namespace MoReFEM::ExceptionNS::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string NonExistingLuaFileMsg(const MoReFEM::FilesystemNS::File& input_data_file)
    {
        std::ostringstream oconv;
        oconv << "Provided Lua file " << input_data_file << " wasn't found by the root processor.";
        return oconv.str();
    }


} // namespace
