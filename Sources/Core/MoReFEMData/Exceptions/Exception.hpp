// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"


namespace MoReFEM::ExceptionNS::MoReFEMDataNS
{


    //! Called when a method makes no sense for a given time step policy.,
    class NonExistingLuaFile : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] input_data_file Input data file not found.
         * \copydoc doxygen_hide_source_location
         */
        explicit NonExistingLuaFile(const FilesystemNS::File& input_data_file,
                                    const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~NonExistingLuaFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        NonExistingLuaFile(const NonExistingLuaFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonExistingLuaFile(NonExistingLuaFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonExistingLuaFile& operator=(const NonExistingLuaFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonExistingLuaFile& operator=(NonExistingLuaFile&& rhs) = delete;

        //! Get the path of the Lua file that was not found.
        const FilesystemNS::File& GetInputDataFile() const noexcept;


      private:
        //! Lua file that was not found.
        FilesystemNS::File input_data_file_;
    };


} // namespace MoReFEM::ExceptionNS::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
