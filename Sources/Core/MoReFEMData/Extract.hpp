// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_EXTRACT_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_EXTRACT_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: begin_exports
#include "Utilities/InputData/Extract.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::InputDataNS
{


    /*!
     * \brief Check whether a field is found either in \a ModelSettings or in \a InputData  -related tuples.
     *
     * \return True if the field is found in one of them; false otherwise.
     *
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::SectionOrLeafType SoughtLeafOrSectionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    constexpr bool Find();


    /*!
     * \brief Check whether a field is found either in \a ModelSettings or in \a InputData  -related tuples extracted
     * from \a MoReFEMDataT
     *
     * \return True if the field is found in one of them; false otherwise.
     *
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::SectionOrLeafType SoughtLeafOrSectionT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    constexpr bool Find();


    /*!
     * \brief Extract a model-related specific information from a leaf either from input data (exclusive-)OR from model settings.
     *
     * This is syntactic sugar over a namesake function in `Utilities` which takes two arguments: \a input_data and
     * \a model_settings. Here we take directly both those objects from \a morefem_data one.
     *
     * \tparam LeafT Full type of the leaf which value is sought. Might be for instance
     * InputDataNS::FEltSpace<3>::DomainIndex.
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * \return Value found either in InputData or ModelSettings objects stored in \a morefem_data.Can't be found in both
     * - in this case the model would not even compile.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    decltype(auto) ExtractLeaf(const MoReFEMDataT& morefem_data);


    /*!
     * \brief Extract a model-related specific information from a leaf either from input data (exclusive-)OR from model settings.
     *
     * This is syntactic sugar over a namesake function in `Utilities` which takes two arguments: \a input_data and
     * \a model_settings. Here we take directly both those objects from \a morefem_data one.
     *
     * \tparam LeafT Full type of the leaf which value is sought. Might be for instance
     * InputDataNS::FEltSpace<3>::DomainIndex.
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * \return Value found either in InputData or ModelSettings objects stored in \a morefem_data.Can't be found in both
     * - in this case the model would not even compile.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        SubstituteEnvironmentVariables DoSubstituteEnvironmentVariablesT = SubstituteEnvironmentVariables::yes
    >
    // clang-format on
    decltype(auto) ExtractLeafAsPath(const MoReFEMDataT& morefem_data);


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Core/MoReFEMData/Extract.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_EXTRACT_DOT_HPP_
// *** MoReFEM end header guards *** < //
