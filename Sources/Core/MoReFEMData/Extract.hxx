// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_EXTRACT_DOT_HXX_
#define MOREFEM_CORE_MOREFEMDATA_EXTRACT_DOT_HXX_
// IWYU pragma: private, include "Core/MoReFEMData/Extract.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Environment/Environment.hpp"


namespace MoReFEM::InputDataNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::SectionOrLeafType SoughtLeafOrSectionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    constexpr bool Find()
    {
        constexpr bool found_in_model_settings = ModelSettingsT::template Find<SoughtLeafOrSectionT>();
        constexpr bool found_in_input_data = InputDataT::template Find<SoughtLeafOrSectionT>();

        static_assert(!(found_in_model_settings && found_in_input_data),
                      "A field can't be both in ModelSettings and InputData!");

        return (found_in_model_settings || found_in_input_data);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::SectionOrLeafType SoughtLeafOrSectionT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    constexpr bool Find()
    {
        return Find
               <
                    SoughtLeafOrSectionT,
                    typename MoReFEMDataT::model_settings_type,
                    typename MoReFEMDataT::input_data_type
               >();
    }
    // clang-format on


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    decltype(auto) ExtractLeaf(const MoReFEMDataT& morefem_data)
    {
        return ExtractLeaf<LeafNameT>(morefem_data.GetModelSettings(), morefem_data.GetInputData());
    }


    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
             ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
             SubstituteEnvironmentVariables DoSubstituteEnvironmentVariablesT>

    decltype(auto) ExtractLeafAsPath(const MoReFEMDataT& morefem_data)
    {

        return ExtractLeafAsPath<LeafNameT,
                                 typename MoReFEMDataT::model_settings_type,
                                 typename MoReFEMDataT::input_data_type,
                                 DoSubstituteEnvironmentVariablesT>(morefem_data.GetModelSettings(),
                                                                    morefem_data.GetInputData());
    }
    // clang-format on


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_EXTRACT_DOT_HXX_
// *** MoReFEM end header guards *** < //
