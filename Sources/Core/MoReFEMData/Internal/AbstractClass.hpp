// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_INTERNAL_ABSTRACTCLASS_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_INTERNAL_ABSTRACTCLASS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Datetime/Now.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"     // IWYU pragma: export
#include "Utilities/InputData/ModelSettings.hpp" // IWYU pragma: export
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp" // IWYU pragma: export

#include "Core/InitTimeKeepLog.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/Enum.hpp"
#include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"
#include "Core/MoReFEMData/Internal/Helper.hpp"      // IWYU pragma: export
#include "Core/MoReFEMData/Internal/Parallelism.hpp" // IWYU pragma: export
#include "Core/TimeManager/Instances.hpp"            // IWYU pragma: export


namespace MoReFEM::Internal::MoReFEMDataNS
{


    /*!
     * \brief Abstract class for \a MoReFEMData.
     *
     * \a MoReFEMData should be used in all models, but for tests it is now more convenient to use a special form
     * which doesn't bother with some parallelism-related fields and doesn't expect a Lua file (all can be put in
     * `ModelSettings` for a test). This abstract class provides the backbone of both the standard \a MoReFEMData
     * and the lighter version used for most of the tests.
     *
     * \tparam ProgramTypeT Type of the program run. For instance for post-processing there is no removal of the
     * existing result directory, contrary to what happens in model run. If update_lua_file, the result directory can't
     * be queried at all
     *
     * http://tclap.sourceforge.net gives a nice manual of how to add additional argument on the command lines.
     * By default, there is one mandatory argument (--input_data *lua file*) and one optional that might be
     * repeated to define pseudo environment variables (-e KEY=VALUE).
     */
    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT = ::MoReFEM::InputDataNS::DoTrackUnusedFields::yes
    >
    // clang-format on
    class AbstractClass
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        // clang-format off
        using self = AbstractClass
                     <
                         ProgramTypeT,
                         TimeManagerT,
                         ModelSettingsT,
                         InputDataT,
                         DoTrackUnusedFieldsT
                     >;
        // clang-format on

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_model_settings_type_alias
        using model_settings_type = ModelSettingsT;

        //! \copydoc doxygen_hide_input_data_type_alias
        using input_data_type = InputDataT;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * This is the staple constructor used in models: at the beginning of the main the object is constructed and
         * initializes under the hood stuff like the initialization of MPI (which requires the \a argc and \a argv
         * values).
         *
         * \param[in] argc Number of argument in the command line (including the program name).
         * \param[in] argv List of arguments read.
         */
        explicit AbstractClass(int argc, char** argv);


        /*!
         * \brief Constructor.
         *
         * Useful for tests.
         *
         * \attention This constructor does not initialize \a Internal::PetscNS::RAII singleton and assumes this has already be done!
         */
        explicit AbstractClass();


        //! Destructor.
        virtual ~AbstractClass() = 0;

        //! \copydoc doxygen_hide_copy_constructor
        AbstractClass(const AbstractClass& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AbstractClass(AbstractClass&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        AbstractClass& operator=(const AbstractClass& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AbstractClass& operator=(AbstractClass&& rhs) = delete;

        ///@}

        //! Accessor to underlying mpi object.
        const ::MoReFEM::Wrappers::Mpi& GetMpi() const noexcept;

        /*!
         * \brief Accessor to the result directory, in which all the outputs of MoReFEM should be written.
         *
         * \return Result directory.
         */
        const ::MoReFEM::FilesystemNS::Directory& GetResultDirectory() const noexcept;


        //! Accessor to underlying \a model_settings_type object.
        const model_settings_type& GetModelSettings() const noexcept;

        //! Accessor to underlying \a input_data_type object.
        const input_data_type& GetInputData() const noexcept;

        //! Constant accessor to the object in charge of time management.
        const time_manager_type& GetTimeManager() const noexcept;

        //! Non constant access to the object in charge of time management
        time_manager_type& GetNonCstTimeManager() noexcept;


      protected:
        /*!
         * \brief Determine the behaviour to adopt for output directory creation.
         *
         * This behaviour depends both on the \a do_overwrite_directory argument and on the type of program you are
         * running (model, test, update a Lua file, etc...)
         *
         * \param[in] do_overwrite_directory Whether an eventual pre-existing output directory should be overwritten.
         *
         * \return Behaviour to adopt for output directory creation.
         */
        ::MoReFEM::FilesystemNS::behaviour DetermineDirectoryBehaviour(
            Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory) const noexcept;

        /*!
         * \brief Set the directory into which outputs will be written.
         *
         * \param[in] path Path on your filesystem to the directory in which the outputs should be written. It should be determined internally
         * in the class derived from this one (\a MoReFEMData or \a MoReFEMDataForTest so far are possible).
         * \param[in] do_overwrite_directory Whether an eventual pre-existing output directory should be overwritten.
         */
        void SetResultDirectory(std::filesystem::path path,
                                Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory);


        /*!
         * \brief Init properly the \a TimeManagerT instance
         *
         * It is not done automatically because it requires some data that are to be defined in the constructors of
         * child classes.
         *
         * \attention Must be called **after** \a SetInputData!
         */
        void SetTimeManager();

        /*!
         * \brief Set the \a input_data_ object.
         *
         * Even \a MoReFEMDataTest child class which doesn't rely on it provides a dummy object and must do this call.
         *
         * \param[in] input_data The input_data object, that should be initialized in derived classes constructors.
         */
        void SetInputData(typename input_data_type::const_unique_ptr&& input_data);


        /*!
         * \brief Init the `CheckInvertedElements` singletons, depending whether there is a field or not in the
         * input data or model settings.
         */
        void InitCheckInvertedElementsSingleton();

      private:
        /*!
         * \brief Helper function used in constructors.
         */
        void Construct();

      private:
        //! Directory into which model results will be written,
        ::MoReFEM::FilesystemNS::Directory::const_unique_ptr result_directory_{ nullptr };

        //! \copydoc doxygen_hide_model_settings_attribute
        ModelSettingsT model_settings_;

        //! Time management facility.
        //! \attention \a SetTimeManager() must be called explicitly in derived classes constructors!
        typename TimeManagerT::unique_ptr time_manager_{ nullptr };

        //! Holds InputData.
        typename input_data_type::const_unique_ptr input_data_{ nullptr };
    };


} // namespace MoReFEM::Internal::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/MoReFEMData/Internal/AbstractClass.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_INTERNAL_ABSTRACTCLASS_DOT_HPP_
// *** MoReFEM end header guards *** < //
