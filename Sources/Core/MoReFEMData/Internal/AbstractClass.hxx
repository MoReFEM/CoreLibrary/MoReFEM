// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_INTERNAL_ABSTRACTCLASS_DOT_HXX_
#define MOREFEM_CORE_MOREFEMDATA_INTERNAL_ABSTRACTCLASS_DOT_HXX_
// IWYU pragma: private, include "Core/MoReFEMData/Internal/AbstractClass.hpp"
// *** MoReFEM header guards *** < //

#include "Utilities/StaticAssert/StaticAssertFalse.hpp"

#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/MoReFEMData/Exceptions/Exception.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::AbstractClass(int argc, char** argv)
    // clang-format on
    {
        Internal::PetscNS::RAII::CreateOrGetInstance(std::source_location::current(), argc, argv);

#ifdef MOREFEM_WITH_SLEPC
        Internal::SlepcNS::RAII::CreateOrGetInstance();
#endif // MOREFEM_WITH_SLEPC

        Construct();
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::AbstractClass()
    // clang-format on
    {
        static_assert(ProgramTypeT == program_type::test, "This constructor is intended only for tests.");

        // Environment variables are defined by parsing command line options for a model, but for a test
        // - which is the sole intent of present constructor - we need to do it explicitly here.
        try
        {
            decltype(auto) mpi = GetMpi();

            Internal::MoReFEMDataNS::DefineEnvironmentVariable(mpi);

            mpi.Barrier();
        }
        catch (const ExceptionNS::GracefulExit& e)
        {
            std::cout << e.what() << std::endl;
            throw;
        }

        Construct();
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    void AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::Construct()
    // clang-format on
    {
#ifndef NDEBUG
        {
            // Code to detect in debug mode whether this singleton has properly been defined.
            // (would have triggered error handling even without these lines but the reason would have been less clear
            // in the error message).
            [[maybe_unused]] decltype(auto) raii = Internal::PetscNS::RAII::GetInstance();
        }
#endif // NDEBUG

        model_settings_.Init();
        model_settings_.CheckTupleCompletelyFilled();
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::~AbstractClass()
    // clang-format on
    {

        // Flush all the standard outputs. Catch all exceptions: exceptions in destructor are naughty!
        const auto& mpi = GetMpi();

        try
        {
            ::MoReFEM::Wrappers::Petsc::SynchronizedFlush(mpi, stdout);
            ::MoReFEM::Wrappers::Petsc::SynchronizedFlush(mpi, stderr);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Untimely exception caught in ~AbstractClass(): " << e.what() << std::endl;
            assert(false && "No exception in destructors!");
        }
        catch (...)
        {
            assert(false && "No exception in destructors!");
        }

        mpi.Barrier(); // to better handle the possible MpiAbort() on one of the branch...
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    inline const ::MoReFEM::Wrappers::Mpi&
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::GetMpi() const noexcept
    // clang-format on
    {
        decltype(auto) raii = Internal::PetscNS::RAII::GetInstance();
        return raii.GetMpi();
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    const ::MoReFEM::FilesystemNS::Directory&
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::GetResultDirectory() const noexcept
    // clang-format on
    {
        static_assert(ProgramTypeT != program_type::update_lua_file,
                      "The result directory is not used in this executable and thus its status is unknown, so it's "
                      "best to forbid it to be queried at all.");

        assert(
            !(!result_directory_)
            && "If this occur in a test, ensure MoReFEMData::SetResultDirectory() has been "
               "correctly called! If in a model, check InputDataNS::Result::OutputDirectory is correcly defined in the "
               "input data tuple");
        return *result_directory_;
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    void AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::SetResultDirectory(std::filesystem::path path, Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory)
    // clang-format on
    {
        static_assert(ProgramTypeT != program_type::update_lua_file);

        result_directory_ = std::make_unique<const ::MoReFEM::FilesystemNS::Directory>(
            GetMpi(), path, DetermineDirectoryBehaviour(do_overwrite_directory));

        result_directory_->ActOnFilesystem();

        InitTimeKeepLog(GetResultDirectory());
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    ::MoReFEM::FilesystemNS::behaviour
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >
    ::DetermineDirectoryBehaviour(Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory) const noexcept
    // clang-format on
    {
        ::MoReFEM::FilesystemNS::behaviour ret{ ::MoReFEM::FilesystemNS::behaviour::ignore };

        if constexpr (ProgramTypeT == program_type::model)
            ret = do_overwrite_directory == Advanced::CommandLineFlagsNS::overwrite_directory::yes
                      ? ::MoReFEM::FilesystemNS::behaviour::overwrite
                      : ::MoReFEM::FilesystemNS::behaviour::ask;
        else if constexpr (ProgramTypeT == program_type::test)
        {
            ret = ::MoReFEM::FilesystemNS::behaviour::overwrite;
        } else if constexpr (ProgramTypeT == program_type::post_processing)
        {
            ret = ::MoReFEM::FilesystemNS::behaviour::read;
        } else
            // #1921 Workaround until C++ 23.
            static_assert(Advanced::FalseEnum<program_type, ProgramTypeT>(), "Please choose the behaviour to adopt!");

        return ret;
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    auto AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::GetModelSettings() const noexcept
        // clang-format on
        -> const model_settings_type&
    {
        return model_settings_;
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    void AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::SetTimeManager()
    // clang-format on
    {
        if constexpr (ProgramTypeT == program_type::update_lua_file
                      || std::is_same<TimeManagerT, ::MoReFEM::TimeManagerNS::Instance::None>())
            time_manager_ = std::make_unique<TimeManagerT>(
                GetMpi(), GetModelSettings(), GetInputData(), ProgramTypeT, std::nullopt);
        else
            time_manager_ = std::make_unique<TimeManagerT>(
                GetMpi(), GetModelSettings(), GetInputData(), ProgramTypeT, GetResultDirectory());
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    auto AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::GetTimeManager() const noexcept
    -> const time_manager_type&
    // clang-format on
    {
        assert(!(!time_manager_) && "Check SetTimeManager() was called in derived class!");
        return *time_manager_;
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    auto AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::GetNonCstTimeManager() noexcept
    -> time_manager_type&
    // clang-format on
    {
        return const_cast<time_manager_type&>(GetTimeManager());
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    auto
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::GetInputData() const noexcept
    -> const input_data_type&
    // clang-format on
    {
        assert(!(!input_data_));
        return *input_data_;
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    void
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::SetInputData(typename input_data_type::const_unique_ptr&& input_data)
    // clang-format on
    {
        assert(!input_data_ && "Should only be called once!");
        input_data_ = std::move(input_data);
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    void
    AbstractClass
    <
        ProgramTypeT,
        TimeManagerT,
        ModelSettingsT,
        InputDataT,
        DoTrackUnusedFieldsT
    >::InitCheckInvertedElementsSingleton()
    // clang-format on
    {
        if constexpr (::MoReFEM::InputDataNS::
                          Find<::MoReFEM::InputDataNS::Solid::CheckInvertedElements, ModelSettingsT, InputDataT>())
        {
            const auto& do_check_inverted_elements =

                ::MoReFEM::InputDataNS::ExtractLeaf<::MoReFEM::InputDataNS::Solid::CheckInvertedElements>(
                    GetModelSettings(), GetInputData());
            ::MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements ::CreateOrGetInstance(
                std::source_location::current(), do_check_inverted_elements);
        } else
        {
            // Create it nonetheless - but will trigger an exception if the value is called.
            ::MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements::CreateOrGetInstance();
        }
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_INTERNAL_ABSTRACTCLASS_DOT_HXX_
// *** MoReFEM end header guards *** < //
