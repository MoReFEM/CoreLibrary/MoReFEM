// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"

#include "Core/MoReFEMData/Internal/EnumInvertedElements.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    CheckInvertedElements::~CheckInvertedElements() = default;


    CheckInvertedElements::CheckInvertedElements(bool do_check_inverted_elements)
    : check_inverted_elements_policy_(do_check_inverted_elements ? check_inverted_elements_policy::do_check
                                                                 : check_inverted_elements_policy::no_check)
    { }


    const std::string& CheckInvertedElements::ClassName()
    {
        static const std::string ret("CheckInvertedElements");
        return ret;
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
