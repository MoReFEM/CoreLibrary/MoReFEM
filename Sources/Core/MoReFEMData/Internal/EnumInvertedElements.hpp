// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_INTERNAL_ENUMINVERTEDELEMENTS_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_INTERNAL_ENUMINVERTEDELEMENTS_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM
{


    //! Enum class to choose between doing a (relatively) expensive check on the Jacobian of the mapping from reference
    //! to deformed configuration
    enum class check_inverted_elements_policy {
        from_input_data, // takes the value specified in the input data file.
        do_check,
        no_check,
        unspecified // if there was no Solid field in the input data file.
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_INTERNAL_ENUMINVERTEDELEMENTS_DOT_HPP_
// *** MoReFEM end header guards *** < //
