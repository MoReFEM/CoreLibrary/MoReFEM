// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <sstream>
#include <string_view>
#include <utility>
#include <vector>

#include "Core/MoReFEMData/Internal/Helper.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Datetime/Now.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    void DefineEnvironmentVariable(const ::MoReFEM::Wrappers::Mpi& mpi)
    {
        const char* const result_dir = "MOREFEM_RESULT_DIR";

        auto& environment = Utilities::Environment::CreateOrGetInstance();

        if (!environment.DoExist(result_dir))
        {
            const char* const default_value("/Volumes/Data/${USER}/MoReFEM/Results");

            environment.SetEnvironmentVariable(std::make_pair("MOREFEM_RESULT_DIR", default_value));

            std::cout << "[WARNING] Environment variable '" << result_dir << "' was not defined; default value '"
                      << default_value
                      << "' has therefore be provided. This environment variable may appear in mesh "
                         "directory defined in input data file; if not it is in fact unused."
                      << '\n';
        }

        const char* const morefem_dir = "MOREFEM_ROOT";

        if (!environment.DoExist(morefem_dir))
        {
            const char* const default_value("${HOME}/Codes/MoReFEM/CoreLibrary");

            environment.SetEnvironmentVariable(std::make_pair("MOREFEM_ROOT", default_value));

            std::cout << "[WARNING] Environment variable '" << morefem_dir << "' was not defined; default value '"
                      << default_value
                      << "' has therefore be provided. This environment variable may appear in output "
                         "directory defined in input data file; if not it is in fact unused."
                      << '\n';
        }

        const std::string start_time = "MOREFEM_START_TIME";

        if (!environment.DoExist(start_time))
        {
            environment.SetEnvironmentVariable(std::make_pair(start_time, ::MoReFEM::Utilities::Now(mpi)));
            mpi.Barrier();
        }
    }


    void CheckExistingForAllRank(const ::MoReFEM::Wrappers::Mpi& mpi, const FilesystemNS::File& input_data_file)
    {
        // Check the input data file can be found on each processor.
        const bool do_file_exist_for_rank = input_data_file.DoExist();
        const std::vector<bool> sent_data{ do_file_exist_for_rank };
        std::vector<bool> gathered_data;

        mpi.Gather(sent_data, gathered_data);

        // Root processor sent true if all files exist for all ranks, false otherwise.
        // Beware (#461): it doesn't check here the file is consistent!
        bool do_file_exist_for_all_ranks{ true };
        std::vector<std::size_t> rank_without_file;

        if (mpi.IsRootProcessor())
        {
            for (std::size_t i = 0UL, Nproc = gathered_data.size(); i < Nproc; ++i)
            {
                if (!gathered_data[i])
                    rank_without_file.push_back(i);
            }

            if (!rank_without_file.empty())
                do_file_exist_for_all_ranks = false;
        }

        mpi.Broadcast(do_file_exist_for_all_ranks);

        if (mpi.IsRootProcessor())
        {

            if (!rank_without_file.empty())
            {
                std::ostringstream oconv;
                oconv << "The following ranks couldn't find the input data file: ";
                Utilities::PrintContainer<>::Do(rank_without_file,
                                                oconv,
                                                ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                ::MoReFEM::PrintNS::Delimiter::opener(""),
                                                ::MoReFEM::PrintNS::Delimiter::closer(".\n"));

                throw Exception(oconv.str());
            }
        } else
        {
            if (!do_file_exist_for_all_ranks)
                throw Exception("Input data file not found for one or more rank; see root processor "
                                "exception for more details!");
        }
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
