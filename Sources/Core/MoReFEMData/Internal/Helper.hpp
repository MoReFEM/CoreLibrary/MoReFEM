// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_INTERNAL_HELPER_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"      // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class program_type; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MoReFEMDataNS
{


    /*!
     * \brief Parse the command line and extract its information.
     *
     * \tparam AdditionalCommandLineArgumentsPolicyT Policy if you need additional arguments on the command line.
     * To see a concrete example of this possibility, have a look at Test/Core/MoReFEMData/test_command_line_options.cpp
     * which demonstrate the possibility. If none, use std::false_type.
     * \tparam ProgramTypeT Type of the program. If 'model', an additional flag --overwrite_directory is enabled on
     * command line.
     *
     * \param[in] argc Number of argument in the command line (including the program name).
     * \param[in] argv List of arguments read.
     * \param[in] additional If AdditionalCommandLineArgumentsPolicyT is not nullptr, give it the this pointer of
     * MoReFEMData
     *
     * \return Path of the input data file.
     */
    template<program_type ProgramTypeT, class AdditionalCommandLineArgumentsPolicyT>
    ::MoReFEM::FilesystemNS::File
    ParseCommandLine(int argc, char** argv, AdditionalCommandLineArgumentsPolicyT* additional = nullptr);


    /*!
     * \brief Define the few environment variables required to make MoReEM work properly.
     *
     * \copydetails doxygen_hide_mpi_param
     */
    void DefineEnvironmentVariable(const ::MoReFEM::Wrappers::Mpi& mpi);


    /*!
     * \brief Check the input data file was properly found for all ranks.
     *
     * If not, an exception is thrown.
     *
     * \copydetails doxygen_hide_mpi_param
     * \param[in] input_data_file Path to the input data file to be created.
     */
    void CheckExistingForAllRank(const ::MoReFEM::Wrappers::Mpi& mpi,
                                 const ::MoReFEM::FilesystemNS::File& input_data_file);


} // namespace MoReFEM::Internal::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Core/MoReFEMData/Internal/Helper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
