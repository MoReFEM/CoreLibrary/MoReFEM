// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_INTERNAL_HELPER_DOT_HXX_
#define MOREFEM_CORE_MOREFEMDATA_INTERNAL_HELPER_DOT_HXX_
// IWYU pragma: private, include "Core/MoReFEMData/Internal/Helper.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <filesystem>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Core/CommandLineFlags/Internal/CommandLineFlags.hpp"
#include "Core/MoReFEMData/Enum.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    template<program_type ProgramTypeT, class AdditionalCommandLineArgumentsPolicyT>
    ::MoReFEM::FilesystemNS::File
    ParseCommandLine(int argc, char** argv, [[maybe_unused]] AdditionalCommandLineArgumentsPolicyT* additional)
    {
        try
        {
            TCLAP::CmdLine cmd("MoReFEM executable");
            cmd.setExceptionHandling(false);

            TCLAP::ValueArg<std::string> input_data_file_arg(
                "i", "input_data", "Input data file (Lua)", true, "", "string", cmd);

            TCLAP::MultiArg<::MoReFEM::Wrappers::Tclap::StringPair> env_arg(
                "e", "env", "environment_variable", false, "string=string", cmd);

            Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory;

            std::unique_ptr<TCLAP::SwitchArg> overwrite_directory_arg = nullptr;

            if constexpr (ProgramTypeT == program_type::model)
            {
                overwrite_directory_arg =
                    std::make_unique<TCLAP::SwitchArg>("",
                                                       "overwrite_directory",
                                                       "If this flag is set, the output directory will be removed "
                                                       "silently "
                                                       "if it already exists.",
                                                       cmd,
                                                       false);
            } else
                do_overwrite_directory = Advanced::CommandLineFlagsNS::overwrite_directory::no;

            auto print_linalg_destruction_arg =
                TCLAP::SwitchArg("",
                                 "print_linalg_destruction",
                                 "If this flag is set, each time a GlobalMatrix or "
                                 "GlobalVector object is deleted a line is printed "
                                 "on standard output (useful to investigate PETSc "
                                 "errors - less no when everything is running smoothly).",
                                 cmd,
                                 false);

            if constexpr (!std::is_same<AdditionalCommandLineArgumentsPolicyT, std::false_type>())
            {
                assert(!(!additional));
                additional->Add(cmd);
            }

            cmd.parse(argc, argv);

            if constexpr (ProgramTypeT == program_type::model)
            {
                assert(!(!overwrite_directory_arg));
                do_overwrite_directory = overwrite_directory_arg->getValue()
                                             ? Advanced::CommandLineFlagsNS::overwrite_directory::yes
                                             : Advanced::CommandLineFlagsNS::overwrite_directory::no;
            }

            auto do_print_linalg_destruction = print_linalg_destruction_arg.getValue()
                                                   ? Advanced::Wrappers::Petsc::print_linalg_destruction::yes
                                                   : Advanced::Wrappers::Petsc::print_linalg_destruction::no;


            // Set environment variables
            {
                decltype(auto) env_var_list = env_arg.getValue();

                auto& environment = Utilities::Environment::CreateOrGetInstance();

                for (const auto& pair : env_var_list)
                    environment.SetEnvironmentVariable(pair.GetValue());
            }

            // Create here the singleton with command line flags.
            ::MoReFEM::Internal::CommandLineFlags::CreateOrGetInstance(
                std::source_location::current(), do_overwrite_directory, do_print_linalg_destruction);

            return ::MoReFEM::FilesystemNS::File{ std::filesystem::path(input_data_file_arg.getValue()) };
        }
        catch (TCLAP::ArgException& e) // catch any exceptions
        {
            std::ostringstream oconv;
            oconv << "Caught command line exception '" << e.error() << "' for argument " << e.argId() << std::endl;
            throw Exception(oconv.str());
        }
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_INTERNAL_HELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
