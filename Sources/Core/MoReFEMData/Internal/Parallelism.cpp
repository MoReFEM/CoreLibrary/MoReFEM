// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <string_view>

#include "Core/MoReFEMData/Internal/Parallelism.hpp"


namespace MoReFEM::Internal
{


    void Parallelism::SetParallelismStrategy(std::string_view policy)
    {
        if (policy == "Precompute")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::precompute;
        else if (policy == "ParallelNoWrite")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::parallel_no_write;
        else if (policy == "Parallel")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::parallel;
        else if (policy == "RunFromPreprocessed")
            parallelism_strategy_ = ::MoReFEM::Advanced::parallelism_strategy::run_from_preprocessed;
        else
        {
            assert(false && "Constraint in Lua file should have ruled out this case sooner!");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
