// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_INTERNAL_PARALLELISM_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_INTERNAL_PARALLELISM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>
#include <memory>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"

#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp" // IWYU pragma: export


namespace MoReFEM::Internal
{


    /*!
     * \brief Holds the interpreted content of the section 'Parallelism' of the input data file.
     */
    class Parallelism
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Parallelism;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_model_settings_input_data_arg
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \param[in] behaviour Behaviour to use when the subdirectory to create already exist. Irrelevant for policies
         * that only read existing directories.
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        explicit Parallelism(const ::MoReFEM::Wrappers::Mpi& mpi,
                             const ModelSettingsT& model_settings,
                             const InputDataT& input_data,
                             ::MoReFEM::FilesystemNS::behaviour behaviour);

        //! Destructor.
        ~Parallelism() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Parallelism(const Parallelism& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Parallelism(Parallelism&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Parallelism& operator=(const Parallelism& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Parallelism& operator=(Parallelism&& rhs) = delete;

        ///@}

        //! Get the path of the parallelism directory.
        const ::MoReFEM::FilesystemNS::Directory& GetDirectory() const noexcept;

        //! Get the parallelism strategy.
        ::MoReFEM::Advanced::parallelism_strategy GetParallelismStrategy() const noexcept;

      private:
        /*!
         * \brief Set the parallelism strategy from the value read in the Lua file.
         *
         * \param[in] policy Value read in the Lua file. Must be "Precompute", "ParallelNoWrite", "Parallel" or
         * "RunFromPreprocessed" (other choices must be flltered out before the constructor of this class is called).
         */
        void SetParallelismStrategy(std::string_view policy);


      private:
        //! The chosen parallelism strategy.
        ::MoReFEM::Advanced::parallelism_strategy parallelism_strategy_ =
            ::MoReFEM::Advanced::parallelism_strategy::none;

        //! Path to the parallelism directory.
        ::MoReFEM::FilesystemNS::Directory::const_unique_ptr directory_ = nullptr;
    };


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Core/MoReFEMData/Internal/Parallelism.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_INTERNAL_PARALLELISM_DOT_HPP_
// *** MoReFEM end header guards *** < //
