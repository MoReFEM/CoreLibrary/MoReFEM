// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_INTERNAL_PARALLELISM_DOT_HXX_
#define MOREFEM_CORE_MOREFEMDATA_INTERNAL_PARALLELISM_DOT_HXX_
// IWYU pragma: private, include "Core/MoReFEMData/Internal/Parallelism.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <memory>
#include <string>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"


namespace MoReFEM::Internal
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    Parallelism::Parallelism(const ::MoReFEM::Wrappers::Mpi& mpi,
                             const ModelSettingsT& model_settings,
                             const InputDataT& input_data,
                             ::MoReFEM::FilesystemNS::behaviour behaviour)
    {
        using Parallelism = ::MoReFEM::InputDataNS::Parallelism;

        SetParallelismStrategy(::MoReFEM::InputDataNS::ExtractLeaf<Parallelism::Policy>(model_settings, input_data));

        switch (parallelism_strategy_)
        {
        case ::MoReFEM::Advanced::parallelism_strategy::precompute:
        case ::MoReFEM::Advanced::parallelism_strategy::parallel:
        {
            using directory = ::MoReFEM::InputDataNS::Parallelism::Directory;
            std::string path = ::MoReFEM::InputDataNS::ExtractLeaf<directory>(model_settings, input_data);

            directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(mpi, path, behaviour);
            directory_->ActOnFilesystem();

            break;
        }
        case ::MoReFEM::Advanced::parallelism_strategy::parallel_no_write:
            break;
        case ::MoReFEM::Advanced::parallelism_strategy::run_from_preprocessed:
        {
            using directory = ::MoReFEM::InputDataNS::Parallelism::Directory;
            std::string path = ::MoReFEM::InputDataNS::ExtractLeaf<directory>(model_settings, input_data);

            directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(
                mpi, path, ::MoReFEM::FilesystemNS::behaviour::read);
            directory_->ActOnFilesystem();

            break;
        }
        case ::MoReFEM::Advanced::parallelism_strategy::none:
            break;
        }
    }


    inline const ::MoReFEM::FilesystemNS::Directory& Parallelism::GetDirectory() const noexcept
    {
        assert(!(!directory_));
        return *directory_;
    }


    inline ::MoReFEM::Advanced::parallelism_strategy Parallelism::GetParallelismStrategy() const noexcept
    {
        assert(parallelism_strategy_ != ::MoReFEM::Advanced::parallelism_strategy::none && "Must be initialized!");
        return parallelism_strategy_;
    }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_INTERNAL_PARALLELISM_DOT_HXX_
// *** MoReFEM end header guards *** < //
