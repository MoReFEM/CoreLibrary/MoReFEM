// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_MOREFEMDATA_DOT_HPP_
#define MOREFEM_CORE_MOREFEMDATA_MOREFEMDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Datetime/Now.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"     // IWYU pragma: export
#include "Utilities/InputData/ModelSettings.hpp" // IWYU pragma: export
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp" // IWYU pragma: export

#include "Core/InitTimeKeepLog.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp" // IWYU pragma: export
#include "Core/MoReFEMData/Enum.hpp"
#include "Core/MoReFEMData/Extract.hpp"                // IWYU pragma: export
#include "Core/MoReFEMData/Internal/AbstractClass.hpp" // IWYU pragma: export
#include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"
#include "Core/MoReFEMData/Internal/Helper.hpp"      // IWYU pragma: export
#include "Core/MoReFEMData/Internal/Parallelism.hpp" // IWYU pragma: export
#include "Core/TimeManager/Instances.hpp"            // IWYU pragma: export


namespace MoReFEM
{

    /*!
     * \brief Init MoReFEM: initialize mpi and read the input data file.
     *
     * \warning As mpi is not assumed to exist until the constructor has done is job, the exceptions there that might
     * happen only on some of the ranks don't lead to a call to MPI_Abort(), which can lead to a dangling program.
     * Make sure each exception is properly communicated to all ranks so that each rank can gracefully throw
     * an exception and hence allow the program to stop properly.
     *
     * \tparam ProgramTypeT Type of the program run. For instance for post-processing there is no removal of the
     * existing result directory, contrary to what happens in model run. If update_lua_file, the result directory can't
     * be queried at all
     * \tparam AdditionalCommandLineArgumentsPolicyT Policy if you need additional arguments on the
     * command line. To see a concrete example of this possibility, have a look at
     * Test/Core/MoReFEMData/test_command_line_options.cpp which demonstrate the possibility. If none, use
     * std::false_type.
     *
     * http://tclap.sourceforge.net gives a nice manual of how to add additional argument on the command lines.
     * By default, there is one mandatory argument (--input_data *lua file*) and one optional that might be
     * repeated to define pseudo environment variables (-e KEY=VALUE).
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT = InputDataNS::DoTrackUnusedFields::yes,
        class AdditionalCommandLineArgumentsPolicyT = std::false_type
    >
    // clang-format on
    class MoReFEMData
    : public Internal::MoReFEMDataNS::AbstractClass<ProgramTypeT, TimeManagerT, ModelSettingsT, InputDataT>,
      public AdditionalCommandLineArgumentsPolicyT
    {

      public:
        // clang-format off

        //! \copydoc doxygen_hide_alias_self
        using self = MoReFEMData
                     <
                         ModelSettingsT,
                         InputDataT,
                         TimeManagerT,
                         ProgramTypeT,
                         DoTrackUnusedFieldsT,
                         AdditionalCommandLineArgumentsPolicyT
                     >;

        //! Alias to parent.
        using parent =
            Internal::MoReFEMDataNS::AbstractClass
            <
                ProgramTypeT,
                TimeManagerT,
                ModelSettingsT,
                InputDataT
            >;

        // clang-format on

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_input_data_type_alias
        using input_data_type = InputDataT;

        //! Alias to \a ModelSettingsT
        using model_settings_type = ModelSettingsT;

        static_assert(std::is_same<typename parent::model_settings_type, ModelSettingsT>());

        //! Alias to \a TimeManagerT.
        using time_manager_type = TimeManagerT;

        static_assert(std::is_same<typename parent::time_manager_type, TimeManagerT>());

        //! Whether there is a Parallelism block in MoReFEM input data.
        //! \return True if there is one.
        static constexpr bool HasParallelismField();

      public:
        //! Helper variable to define the \a MoReFEMDataType concept.
        static inline constexpr is_morefem_data ConceptIsMoReFEMData = is_morefem_data::yes;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * This is the staple constructor used in models: at the beginning of the main the object is constructed and
         * initializes under the hood stuff like the initialization of MPI (which requires the \a argc and \a argv
         * values).
         *
         * \param[in] argc Number of argument in the command line (including the program name).
         * \param[in] argv List of arguments read.
         */
        explicit MoReFEMData(int argc, char** argv);


        /*!
         * \brief Constructor from a Lua file.
         *
         * Useful for tests.
         *
         * \attention This constructor does not initialize \a Internal::PetscNS::RAII singleton and assumes this has already be done!
         *
         * \param[in] lua_file Input data file.
         */
        explicit MoReFEMData(FilesystemNS::File&& lua_file);


        //! Destructor.
        virtual ~MoReFEMData() override;

        //! \copydoc doxygen_hide_copy_constructor
        MoReFEMData(const MoReFEMData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MoReFEMData(MoReFEMData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MoReFEMData& operator=(const MoReFEMData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MoReFEMData& operator=(MoReFEMData&& rhs) = delete;

        ///@}


        //! Accessor to the object which keeps the data related to parallelism strategy.  Might be nullptr if none
        //! specified in the input lua file.
        const Internal::Parallelism* GetParallelismPtr() const noexcept;


      private:
        //! Accessor to the object which keeps the data related to parallelism strategy.
        //! No pointer here, but it assumes parallelism_ is not nullptr.
        const Internal::Parallelism& GetParallelism() const noexcept;

        /*!
         * \brief Helper function used in constructors.
         *
         * \param[in] lua_file Input data file.
         * \param[in] do_overwrite_directory Whether the program may overwrite result directory or not. In the general case it is the
         * value read from command line (if any) but for tests it is arbitrarily set to yes.
         */
        void Construct(FilesystemNS::File&& lua_file,
                       Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory);

        /*!
         * \brief Check all the expected \a IndexedSectionDescription class in the model-specific \a ModelSettings are properly
         * present.
         *
         * If there is a leaf or a section  either in the model-specific \a ModelSettings or in the
         * model-specific \a InputData that is related to an \a IndexedSection, then the related
         * \a IndexedSectionDescription should be declared in the model-specific \a ModelSettings. Current method
         * checks that this is the case, and if not provide a clear error in runtime to help the author
         * of the model to fix it quickly.
         *
         */
        void CheckNoMissingIndexedSectionDescriptions() const;


      private:
        //! Object which holds the parallelism strategy to use.
        Internal::Parallelism::unique_ptr parallelism_ = nullptr;
    };


    /*!
     * \brief Provide the precompute exit.
     *
     * When the parallelism strategy is "precompute", once all the data are written we want to exit the program as
     * soon as possible.
     * The proper way to do so is to throw a GracefulExit exception, which should be properly trapped in the main.
     *
     * This function does so IF the parallelism strategy is "precompute"; if not it does nothing (including the case
     * there are no parallelism policy specified in the Lua file).
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void PrecomputeExit(const MoReFEMDataT& morefem_data);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/MoReFEMData/MoReFEMData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_MOREFEMDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
