// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_MOREFEMDATA_MOREFEMDATA_DOT_HXX_
#define MOREFEM_CORE_MOREFEMDATA_MOREFEMDATA_DOT_HXX_
// IWYU pragma: private, include "Core/MoReFEMData/MoReFEMData.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/InputData/Internal/Extract.hpp"

#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"

#include "Core/CommandLineFlags/Advanced/CommandLineFlags.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Exceptions/Exception.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    constexpr bool
    MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::HasParallelismField()
    // clang-format on
    {
        return InputDataNS::Find<InputDataNS::Parallelism, ModelSettingsT, InputDataT>();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::MoReFEMData(int argc, char** argv)
    : parent(argc, argv)
    // clang-format on
    {
        self* ptr{ nullptr };

        if constexpr (!std::is_same<AdditionalCommandLineArgumentsPolicyT, std::nullptr_t>())
            ptr = this;

        try
        {
            // clang-format off
            auto input_data_file =
                ParseCommandLine
                <
                    ProgramTypeT,
                    AdditionalCommandLineArgumentsPolicyT
                >(argc, argv, ptr);
            // clang-format on

            Construct(std::move(input_data_file), Advanced::CommandLineFlagsNS::DoOverwriteDirectory());
        }
        catch (const ExceptionNS::GracefulExit& e)
        {
            std::cout << e.what() << std::endl;
            throw;
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::MoReFEMData(FilesystemNS::File&& input_data_file)
    : parent()
    // clang-format on
    {
        static_assert(std::is_same_v<AdditionalCommandLineArgumentsPolicyT, std::false_type>,
                      "This constructor is dedicated for simple tests with no additional arguments on command line.");

        Construct(std::move(input_data_file), Advanced::CommandLineFlagsNS::overwrite_directory::yes);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    void MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::Construct(FilesystemNS::File&& input_data_file,
                 Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory)
    // clang-format on
    {
        try
        {
            decltype(auto) mpi = parent::GetMpi();

            if (mpi.IsRootProcessor())
            {
                if (!input_data_file.DoExist())
                    throw ::MoReFEM::ExceptionNS::MoReFEMDataNS::NonExistingLuaFile(input_data_file);
            }

            Internal::MoReFEMDataNS::CheckExistingForAllRank(mpi, input_data_file);

            // We can be here only if the file exists...
            parent::SetInputData(
                std::make_unique<InputDataT>(parent::GetModelSettings(), input_data_file, DoTrackUnusedFieldsT));

            CheckNoMissingIndexedSectionDescriptions();

            {
                if constexpr (ProgramTypeT != program_type::update_lua_file)
                {
                    if constexpr (InputDataNS::Find<InputDataNS::Result::OutputDirectory, ModelSettingsT, InputDataT>())
                    {
                        using Result = InputDataNS::Result;
                        auto path = InputDataNS::ExtractLeafAsPath<Result::OutputDirectory>(parent::GetModelSettings(),
                                                                                            parent::GetInputData());

                        parent::SetResultDirectory(std::move(path), do_overwrite_directory);
                    }
                }

                if constexpr (InputDataNS::Find<InputDataNS::Result::BinaryOutput, ModelSettingsT, InputDataT>())
                {
                    const auto& binary_output = InputDataNS::ExtractLeaf<InputDataNS::Result::BinaryOutput>(
                        parent::GetModelSettings(), parent::GetInputData());


                    Utilities::AsciiOrBinary::CreateOrGetInstance(std::source_location::current(), binary_output);
                }

                if constexpr (ProgramTypeT != program_type::update_lua_file)
                {
                    // Parallelism is an optional field: it might not be present in the Lua file (for tests for instance
                    // it is not meaningful).
                    // If we are updating a Lua file, we don't want to create it: doing so requires to interpret
                    // the environment variables, and we don't want that in the updated Lua file (the goal is
                    // just to update comments and field, not to replace environment variables by their specific
                    // values on the machine onto which update_lua_file program is called).
                    if constexpr (HasParallelismField())
                        parallelism_ = std::make_unique<Internal::Parallelism>(
                            mpi,
                            parent::GetModelSettings(),
                            parent::GetInputData(),
                            parent::DetermineDirectoryBehaviour(do_overwrite_directory));
                }
            }

            mpi.Barrier();

            parent::InitCheckInvertedElementsSingleton();

            parent::SetTimeManager();
        }
        catch (const ExceptionNS::GracefulExit& e)
        {
            std::cout << e.what() << std::endl;
            throw;
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::~MoReFEMData() = default;


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const Internal::Parallelism*
    MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetParallelismPtr() const noexcept
    // clang-format on
    {
        if (!parallelism_)
            return nullptr;

        return parallelism_.get();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const Internal::Parallelism&
    MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetParallelism() const noexcept
    // clang-format on
    {
        assert(!(!parallelism_));
        return *parallelism_;
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void PrecomputeExit(const MoReFEMDataT& morefem_data)
    {
        const auto parallelism_ptr = morefem_data.GetParallelismPtr();

        if (!(!parallelism_ptr))
        {
            switch (parallelism_ptr->GetParallelismStrategy())
            {
            case Advanced::parallelism_strategy::precompute:
            {
                if (morefem_data.GetMpi().IsRootProcessor())
                    std::cout << "The parallelism elements have been precomputed; the program will now end."
                              << std::endl;

                throw ExceptionNS::GracefulExit();
            }
            case Advanced::parallelism_strategy::run_from_preprocessed:
            case Advanced::parallelism_strategy::parallel:
            case Advanced::parallelism_strategy::parallel_no_write:
                break;
            case Advanced::parallelism_strategy::none:
            {
                assert(false && "Should not be possible (if so parallelism_ptr should have been nullptr).");
                exit(EXIT_FAILURE);
            }
            }
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    void MoReFEMData
    <
        ModelSettingsT,
        InputDataT,
        TimeManagerT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::CheckNoMissingIndexedSectionDescriptions() const
    // clang-format on
    {
        decltype(auto) input_data = parent::GetInputData();
        decltype(auto) model_settings = parent::GetModelSettings();

        Internal::InputDataNS::CheckNoMissingIndexedSectionDescriptions(model_settings, input_data);
        Internal::InputDataNS::CheckNoMissingIndexedSectionDescriptions(model_settings, model_settings);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_MOREFEMDATA_MOREFEMDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
