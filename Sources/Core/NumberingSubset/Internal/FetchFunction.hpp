// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FETCHFUNCTION_DOT_HPP_
#define MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FETCHFUNCTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::NumberingSubsetNS
{

    /*!
     * \brief Returns the requested numbering subset from \a object when T is a pointer or smart pointer.
     *
     * \param[in] pointer Pointer or pointer-like to an object that must define a method with prototype:
     *
     * \code
     * const NumberingSubset& GetNumberingSubset() const;
     * \endcode
     *
     * \return Reference returned by pointer->GetNumberingSubset().
     */
    template<class T>
    std::enable_if_t<Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                     const NumberingSubset&>
    FetchNumberingSubset(const T& pointer) noexcept;


    /*!
     * \brief Returns the requested numbering subset from \a object when T is <b>not</b> a pointer or smart
     * pointer.
     *
     * \param[in] object Object that must define a method with prototype:
     *
     * \code
     * const NumberingSubset& GetNumberingSubset() const;
     * \endcode
     *
     * \return Object returned by object.GetNumberingSubset().
     */
    template<class T>
    std::enable_if_t<!(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                     const NumberingSubset&>
    FetchNumberingSubset(const T& object) noexcept;


    /*!
     * \brief Returns the requested numbering subset pair from \a object when T is a pointer or smart pointer.
     *
     * \param[in] pointer Pointer or pointer-like to an object that must define methods with following
     * prototypes:
     *
     * \code
     * const NumberingSubset& GetRowNumberingSubset() const;
     * const NumberingSubset& GetColNumberingSubset() const;
     * \endcode
     *
     * \return A tuple with two references to respectively NumberingSubset to use for rows and columns.
     */
    template<class T>
    std::enable_if_t<Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                     std::pair<const NumberingSubset&, const NumberingSubset&>>
    FetchNumberingSubsetPair(const T& pointer) noexcept;


    /*!
     * \brief Returns the requested numbering subset pair from \a object when T is <b>not</b> a pointer or smart
     * pointer.
     *
     * \param[in] object Object that must define methods with following prototypes:
     *
     * \code
     * const NumberingSubset& GetRowNumberingSubset() const;
     * const NumberingSubset& GetColNumberingSubset() const;
     * \endcode
     *
     * \return A tuple with two references to respectively NumberingSubset to use for rows and columns.
     */

    template<class T>
    std::enable_if_t<!(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                     std::pair<const NumberingSubset&, const NumberingSubset&>>
    FetchNumberingSubsetPair(const T& object) noexcept;


} // namespace MoReFEM::Internal::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/NumberingSubset/Internal/FetchFunction.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FETCHFUNCTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
