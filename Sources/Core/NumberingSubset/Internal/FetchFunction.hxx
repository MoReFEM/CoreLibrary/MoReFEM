// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FETCHFUNCTION_DOT_HXX_
#define MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FETCHFUNCTION_DOT_HXX_
// IWYU pragma: private, include "Core/NumberingSubset/Internal/FetchFunction.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::NumberingSubsetNS
{


    template<class T>
    inline std::enable_if_t<Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                            const NumberingSubset&>
    FetchNumberingSubset(const T& object) noexcept
    {
        assert(!(!object));
        return FetchNumberingSubset(*object);
    }


    template<class T>
    inline std::enable_if_t<!(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                            const NumberingSubset&>
    FetchNumberingSubset(const T& object) noexcept
    {
        return object.GetNumberingSubset();
    }


    template<class T>
    std::enable_if_t<Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>(),
                     std::pair<const NumberingSubset&, const NumberingSubset&>>
    FetchNumberingSubsetPair(const T& object) noexcept
    {
        assert(!(!object));
        return FetchNumberingSubsetPair(*object);
    }


    template<class T>
    std::enable_if_t<!(Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>() || std::is_pointer<T>()),
                     std::pair<const NumberingSubset&, const NumberingSubset&>>
    FetchNumberingSubsetPair(const T& object) noexcept
    {
        return std::make_pair(std::cref(object.GetRowNumberingSubset()), std::cref(object.GetColNumberingSubset()));
    }


} // namespace MoReFEM::Internal::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FETCHFUNCTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
