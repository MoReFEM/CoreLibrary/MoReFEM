// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FINDFUNCTOR_DOT_HPP_
#define MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FINDFUNCTOR_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Core/NumberingSubset/Internal/FetchFunction.hpp"


namespace MoReFEM::Internal::NumberingSubsetNS
{


    /*!
     * \brief Yields the unary condition required to find with std::find_if an object \a T in a list provided
     * its numbering subset is given.
     *
     * \tparam T Type of the object being compared; T (or the underlying object - see after) is expected to
     * contain a method with prototype const NumberingSubset& GetNumberingSubset() const;
     * T may be a pointer type, as in the example below.
     *
     * To find the GlobalVector in global_vector_list_ of GlobalVector::unique_ptr with numbering subset ns the
     * syntax is:
     *
     * \code
     * auto it = std::find_if(global_vector_list_.cbegin(),
     *                        global_vector_list_.cend(),
     *                        NumberingSubsetNS::FindIfCondition<GlobalVector::unique_ptr>(ns));
     * \endcode
     *
     */
    template<class T>
    class FindIfCondition
    {
      public:
        //! Constructor.
        //! \param[in] numbering_subset The \a NumberingSubset that will be used as equality criteria
        //! in std::find_if algorithm.
        explicit FindIfCondition(const NumberingSubset& numbering_subset);

        //! Destructor.
        ~FindIfCondition() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FindIfCondition(const FindIfCondition& rhs) = default; // required by gcc; clang makes do with only move one.

        //! \copydoc doxygen_hide_move_constructor
        FindIfCondition(FindIfCondition&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        FindIfCondition& operator=(const FindIfCondition& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FindIfCondition& operator=(FindIfCondition&& rhs) = delete;

        //! Prototype to compare objects shielded by unique_ptr.
        //! \param[in] item The (unary) element which will be compared to the stored \a numbering_subset_.
        bool operator()(const T& item) const noexcept;

      private:
        //! Numbering subset used to describe the vector..
        const NumberingSubset& numbering_subset_;
    };


    /*!
     * \brief Yields the unary condition required to find with std::find_if an object \a T in a list provided
     * its pair of numbering subsets are given.
     *
     * \tparam T Type of the object being compared; T (or the underlying object - see after) is expected to
     * contain methods with prototype const NumberingSubset& GetRowNumberingSubset() const and
     * const NumberingSubset& GetColNumberingSubset() const.
     *
     * T may be a pointer type, as in the example below.
     *
     * To find the GlobalMatrix in global_matrix_list_ of GlobaMatrix::unique_ptr with numbering subset ns the
     * syntax is:
     *
     * \code
     * auto it = std::find_if(global_matrix_list_.cbegin(),
     *                        global_matrix_list_.cend(),
     *                        NumberingSubsetNS::FindIfCondition<GlobalMatrix::unique_ptr>(ns));
     * \endcode
     *
     */
    template<class T>
    class FindIfConditionForPair
    {
      public:
        //! Constructor.
        //! \param[in] row_numbering_subset The \a NumberingSubset that will be used as equality criteria
        //! for the rows in std::find_if algorithm.
        //! \param[in] col_numbering_subset The \a NumberingSubset that will be used as equality criteria
        //! for the columns in std::find_if algorithm.
        FindIfConditionForPair(const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& col_numbering_subset);

        //! Destructor.
        ~FindIfConditionForPair() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FindIfConditionForPair(const FindIfConditionForPair& rhs) =
            default; // required by gcc; clang makes do with only move one.

        //! \copydoc doxygen_hide_move_constructor
        FindIfConditionForPair(FindIfConditionForPair&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        FindIfConditionForPair& operator=(const FindIfConditionForPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FindIfConditionForPair& operator=(FindIfConditionForPair&& rhs) = delete;

        //! Prototype to compare objects shielded by unique_ptr.
        //! \param[in] item The (unary) element which will be compared to the stored numbering_subsets.
        bool operator()(const T& item) const noexcept;

      private:
        //! Numbering subset used reespectively to describe the rows and columns.
        std::pair<const NumberingSubset&, const NumberingSubset&> numbering_subset_pair_;
    };


} // namespace MoReFEM::Internal::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/NumberingSubset/Internal/FindFunctor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FINDFUNCTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
