// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FINDFUNCTOR_DOT_HXX_
#define MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FINDFUNCTOR_DOT_HXX_
// IWYU pragma: private, include "Core/NumberingSubset/Internal/FindFunctor.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::NumberingSubsetNS
{


    template<class T>
    FindIfCondition<T>::FindIfCondition(const NumberingSubset& numbering_subset) : numbering_subset_(numbering_subset)
    { }


    template<class T>
    bool FindIfCondition<T>::operator()(const T& rhs) const noexcept
    {
        return FetchNumberingSubset(rhs) == numbering_subset_;
    }


    template<class T>
    FindIfConditionForPair<T>::FindIfConditionForPair(const NumberingSubset& row_numbering_subset,
                                                      const NumberingSubset& col_numbering_subset)
    : numbering_subset_pair_(std::make_pair(std::cref(row_numbering_subset), std::cref(col_numbering_subset)))
    { }


    template<class T>
    bool FindIfConditionForPair<T>::operator()(const T& rhs) const noexcept
    {
        assert(!(!rhs));
        return FetchNumberingSubsetPair(rhs) == numbering_subset_pair_;
    }


} // namespace MoReFEM::Internal::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_FINDFUNCTOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
