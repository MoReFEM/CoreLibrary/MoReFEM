// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <iosfwd>
#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>      // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep

#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::NumberingSubsetNS
{


    NumberingSubsetManager::~NumberingSubsetManager() = default;


    const std::string& NumberingSubsetManager::ClassName()
    {
        static const std::string ret("NumberingSubsetManager");
        return ret;
    }


    NumberingSubset::vector_const_shared_ptr::const_iterator
    NumberingSubsetManager ::GetIterator(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const
    {
        const auto& list = GetList();

        return std::ranges::find_if(list,

                                    [unique_id](const auto& numbering_subset_ptr)
                                    {
                                        assert(!(!numbering_subset_ptr));
                                        return unique_id == numbering_subset_ptr->GetUniqueId();
                                    });
    }


    void NumberingSubsetManager::Create(::MoReFEM::NumberingSubsetNS::unique_id unique_id, bool do_move_mesh)
    {
        auto ptr = Internal::WrapSharedToConst(new NumberingSubset(unique_id, do_move_mesh));

        assert(ptr->GetUniqueId() == unique_id);

        auto it = GetIterator(unique_id);

        auto& list = GetNonCstList();

        if (it != list.cend())
            throw Exception("Two numbering subset objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id.Get()) + ").");

        list.push_back(ptr);
    }


    bool NumberingSubsetManager::DoExist(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const
    {
        auto it = GetIterator(unique_id);
        return (it != GetList().cend());
    }


    void NumberingSubsetManager::Clear()
    {
        GetNonCstList().clear();
        NumberingSubset::ClearUniqueIdList();
    }


} // namespace MoReFEM::Internal::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
