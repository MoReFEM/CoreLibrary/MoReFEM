// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_NUMBERINGSUBSETMANAGER_DOT_HPP_
#define MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_NUMBERINGSUBSETMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Core/Internal/NumberingSubset.hpp"
#include "Core/InputData/Instances/Core/NumberingSubset.hpp" // IWYU pragma: keep
#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/TimeManager/Concept.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS { template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> struct ClearSingletons; } // IWYU pragma: keep

namespace MoReFEM::TestNS { struct NumberingSubsetCreator; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::NumberingSubsetNS
{


    /*!
     * \brief Object that is aware of all existing \a NumberingSubset.
     *
     * \internal <b><tt>[internal]</tt></b> Contrary to other managers, this one is really meant to be
     * hidden to users and developers: NumberingSubset should be queried against \a GodOfDof objects.
     * \endinternal
     */
    class NumberingSubsetManager : public Utilities::Singleton<NumberingSubsetManager>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::NumberingSubsetNS::Tag;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        friend struct MoReFEM::TestNS::ClearSingletons;

        //! Convenient friendship to be able to create simply numbering subsets in basic tests.
        friend struct MoReFEM::TestNS::NumberingSubsetCreator;

      public:
        /*!
         * \brief Create a \a NumberingSubset object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);

        /*!
         * \class doxygen_hide_numbering_subset_manager_unique_id_arg
         *
         * \param[in] unique_id Unique identifier of the NumberingSubset, e.g. 5 for what is written
         * in input data file NumberingSubset5.
         */


        /*!
         * \brief Fetch the \a NumberingSubset object associated with \a unique_id unique identifier.
         *
         * \copydoc doxygen_hide_numbering_subset_manager_unique_id_arg
         * \return Reference to the numbering subset.
         */
        const NumberingSubset& GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const;

        /*!
         * \brief Fetch the NumberingSubset object associated with \a unique_id unique identifier.
         *
         * \copydoc doxygen_hide_numbering_subset_manager_unique_id_arg
         *
         * \return Shared pointer to the NumberingSubset.
         */
        NumberingSubset::const_shared_ptr
        GetNumberingSubsetPtr(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const;

        /*!
         * \brief Whether numbering subset \a unique_id is handled by the NumberingSubsetManager.
         *
         * \copydoc doxygen_hide_numbering_subset_manager_unique_id_arg
         *
         * \return True if a \a NumberingSubset with \a unique_id do exist.
         */
        bool DoExist(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const;


      public:
        /*!
         * \brief Get access to the list of existing numbering subset.
         *
         * \internal This method is public solely because of its occasional usefulness in debug; you shouldn't
         * have to use it while writing a \a Model.
         * \endinternal
         *
         * \return List of pointers to the \a NumberingSubset available throughout the program.
         */
        const NumberingSubset::vector_const_shared_ptr& GetList() const;


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        NumberingSubsetManager() = default;

        //! Destructor.
        virtual ~NumberingSubsetManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<NumberingSubsetManager>;
        ///@}


      private:
        /*!
         * \brief Create a new NumberingSubset object.
         *
         * \param[in] unique_id Unique identifier of the NumberingSubset, which is also what tags the
         * NumberingSubset in the input data file (e.g. 5 in 'NumberingSubset5').
         * \param[in] do_move_mesh Whether the numbering subset covers a displacement unknown that might be
         * used to make the mesh move. Should be false in most cases; see \a MovemeshHelper for more details.
         */
        void Create(::MoReFEM::NumberingSubsetNS::unique_id unique_id, bool do_move_mesh);

        //! \copydoc doxygen_hide_manager_clear
        void Clear();

        //! Get non constant access to the list of existing numbering subset.
        NumberingSubset::vector_const_shared_ptr& GetNonCstList();

        //! Get the iterator to the element matching \a unique_id.
        //! \param[in] unique_id The unique identifier which iterator is sought.
        NumberingSubset::vector_const_shared_ptr::const_iterator
        GetIterator(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const;


      private:
        //! Store the NumberingSubset objects by their unique identifier.
        NumberingSubset::vector_const_shared_ptr list_;
    };


} // namespace MoReFEM::Internal::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_NUMBERINGSUBSETMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
