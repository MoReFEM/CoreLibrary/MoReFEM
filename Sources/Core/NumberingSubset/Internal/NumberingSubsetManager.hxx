// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_NUMBERINGSUBSETMANAGER_DOT_HXX_
#define MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_NUMBERINGSUBSETMANAGER_DOT_HXX_
// IWYU pragma: private, include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp" // IWYU pragma: keep

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::NumberingSubsetNS
{


    template<class IndexedSectionDescriptionT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
             ::MoReFEM::Concept::InputDataType InputDataT>
    void NumberingSubsetManager::Create(const IndexedSectionDescriptionT&,
                                        const ModelSettingsT& model_settings,
                                        const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        using model_settings_tuple_iteration =
            Internal::InputDataNS::TupleIteration<typename ModelSettingsT::underlying_tuple_type, 0UL>;
        using input_data_tuple_iteration =
            Internal::InputDataNS::TupleIteration<typename InputDataT::underlying_tuple_type, 0UL>;

        using leaf_type = typename section_type::DoMoveMesh;

        constexpr auto is_in_model_settings = model_settings_tuple_iteration::template Find<leaf_type>();
        constexpr auto is_in_input_data = input_data_tuple_iteration::template Find<leaf_type>();

        bool do_move_mesh{ false };

        // We are here in a very rare case: the argument is silently set to false unless the
        // model explicitly tells through `InputData` or `ModelSettings` tuple that it is
        // defined. MoReFEM usually avoids this kind of stuff (to avoid "hidden" data that gets
        // default values under the hood) but in this very specific case a model developer
        // should really know what they are doing when they introduce movement for meshes.
        // It is documented in the wiki.
        if constexpr (is_in_model_settings || is_in_input_data)
        {
            do_move_mesh = ::MoReFEM::InputDataNS::ExtractLeaf<leaf_type>(model_settings, input_data);
        } else
        {
            static_assert(!input_data_tuple_iteration::template Find<section_type>(),
                          "NumberingSubset is a very peculiar input data section: there are no "
                          "mandatory fields in it. So you should not put directly a NumberingSubset "
                          "section in input data tuple; please check the one used for your model!");

            static_assert(!model_settings_tuple_iteration::template Find<section_type>(),
                          "NumberingSubset is a very peculiar input data section: there are no "
                          "mandatory fields in it. So you should not put directly a NumberingSubset "
                          "section in model settings tuple; please check the one used for your model!");
        }

        Create(::MoReFEM::NumberingSubsetNS::unique_id{ section_type::GetUniqueId() }, do_move_mesh);
    }


    inline const NumberingSubset::vector_const_shared_ptr& NumberingSubsetManager::GetList() const
    {
        return list_;
    }


    inline NumberingSubset::vector_const_shared_ptr& NumberingSubsetManager::GetNonCstList()
    {
        return const_cast<NumberingSubset::vector_const_shared_ptr&>(GetList());
    }


    inline const NumberingSubset&
    NumberingSubsetManager::GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const
    {
        return *(GetNumberingSubsetPtr(unique_id));
    }


    inline NumberingSubset::const_shared_ptr
    NumberingSubsetManager ::GetNumberingSubsetPtr(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const
    {
        auto it = GetIterator(unique_id);
        assert(it != GetList().cend());
        assert(!(!(*it)));
        return *it;
    }


} // namespace MoReFEM::Internal::NumberingSubsetNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_INTERNAL_NUMBERINGSUBSETMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
