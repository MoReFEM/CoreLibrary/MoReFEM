// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
#include <vector>
// IWYU pragma: no_include <iosfwd>

#include "ThirdParty/Wrappers/Mpi/IsSameForAllRanks.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{


    const std::string& NumberingSubset::ClassName()
    {
        static const std::string ret("NumberingSubset");
        return ret;
    }


    NumberingSubset::NumberingSubset(NumberingSubsetNS::unique_id id, bool do_move_mesh)
    : unique_id_parent(id), do_move_mesh_(do_move_mesh)
    { }


#ifndef NDEBUG
    bool IsConsistentOverRanks(const Wrappers::Mpi& mpi,
                               const NumberingSubset::vector_const_shared_ptr& numbering_subset_list)
    {
        std::vector<std::size_t> numbering_subset_index_list(numbering_subset_list.size());

        std::ranges::transform(numbering_subset_list,

                               numbering_subset_index_list.begin(),
                               [](const auto& numbering_subset_ptr)
                               {
                                   assert(!(!numbering_subset_ptr));
                                   return numbering_subset_ptr->GetUniqueId().Get();
                               });

        return IsSameForAllRanks(mpi, numbering_subset_index_list);
    }
#endif // NDEBUG


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
