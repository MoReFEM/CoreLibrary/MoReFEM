// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_NUMBERINGSUBSET_DOT_HPP_
#define MOREFEM_CORE_NUMBERINGSUBSET_NUMBERINGSUBSET_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "Core/NumberingSubset/UniqueId.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::NumberingSubsetNS { class NumberingSubsetManager; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Descriptor of a dof numbering.
     *
     * \a NumberingSubsets are most prominently used in global linear algebra description: numbering used for rows
     * and columns in a GlobalMatrix are for instance tagged by a NumberingSubset.
     *
     * \todo #9 Explain more precisely numbering subset here!
     */
    class NumberingSubset
    : public Crtp::UniqueId<NumberingSubset, NumberingSubsetNS::unique_id, UniqueIdNS::AssignationMode::manual>
    {

      public:
        //! Alias for shared pointer to a constant object.
        using const_shared_ptr = std::shared_ptr<const NumberingSubset>;

        //! Alias for a vector of const_shared_ptr.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Alias for the parent.
        using unique_id_parent =
            Crtp::UniqueId<NumberingSubset, NumberingSubsetNS::unique_id, UniqueIdNS::AssignationMode::manual>;

        //! Friendship!
        friend class Internal::NumberingSubsetNS::NumberingSubsetManager;

        //! Name of the class.
        static const std::string& ClassName();


      public:
        //! Create function.

        /// \name Special members.
        ///@{

      private:
        /*!
         * \brief Constructor.
         *
         * \param[in] id Unique identifier of the numbering subset.
         * \param[in] do_move_mesh Whether a vector defined on this numbering subset might be used to compute a
         * movemesh. If true, a FEltSpace featuring this numbering subset will compute additional quantities
         * to enable fast computation. This should be false for most numbering subsets, and when it's true the sole
         * unknown involved should be a displacement.
         */
        explicit NumberingSubset(NumberingSubsetNS::unique_id id, bool do_move_mesh);

      public:
        //! Destructor.
        ~NumberingSubset() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NumberingSubset(const NumberingSubset& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NumberingSubset(NumberingSubset&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NumberingSubset& operator=(const NumberingSubset& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NumberingSubset& operator=(NumberingSubset&& rhs) = delete;

        ///@}

        //! Whether a vector defined on this numbering subset might be used to compute a movemesh.
        bool DoMoveMesh() const noexcept;


      private:
        /*!
         * \brief Whether a vector defined on this numbering subset might be used to compute a movemesh.
         *
         * If true, a FEltSpace featuring this numbering subset will compute additional quantities
         * to enable fast computation. This should be false for most numbering subsets, and when it's true the sole
         * unknown involved should be a displacement.
         */
        const bool do_move_mesh_;
    };


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * Criterion chosen is the unique identifier of the \a NumberingSubset.
     */
    bool operator==(const NumberingSubset& lhs, const NumberingSubset& rhs);

    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * Criterion chosen is the unique identifier of the \a NumberingSubset.
     */
    bool operator<(const NumberingSubset& lhs, const NumberingSubset& rhs);


    /*!
     * \brief Convert into the \a NumberingSubsetNS::unique_id strong type the enum value defined in the input data file.
     *
     * In a typical Model, indexes related to fields (e.g. NumberingSubsetNS) are defined in an enum class named
     * typically \a NumberingSubsetIndex. This facility is a shortcut to convert this enum into a proper \a
     * NumberingSubsetNS::unique_id that is used in the library to identify \a NumberingSubset.
     *
     * \param[in] enum_value The index related to the \a NumberingSubset, stored in an enum class. See a typical 'InputData.hpp' file
     * to understand it better.
     *
     * \return The identifier for a \a NumberingSubset as used in the library.
     */
    template<class EnumT>
    constexpr NumberingSubsetNS::unique_id AsNumberingSubsetId(EnumT enum_value);


#ifndef NDEBUG

    /*!
     * \brief Check the \a NumberingSubset list is the same on all ranks - same items and same ordering.
     *
     * This function is intended to be used in asserts - if the condition below is false, it would result in mpi
     * hangout.
     *
     * \copydoc doxygen_hide_mpi_param
     * \param[in] numbering_subset_list The list which consistency is checked.
     *
     * \return True if all ranks properly get the exact same list.
     */
    bool IsConsistentOverRanks(const Wrappers::Mpi& mpi,
                               const NumberingSubset::vector_const_shared_ptr& numbering_subset_list);
#endif // NDEBUG


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/NumberingSubset/NumberingSubset.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_NUMBERINGSUBSET_DOT_HPP_
// *** MoReFEM end header guards *** < //
