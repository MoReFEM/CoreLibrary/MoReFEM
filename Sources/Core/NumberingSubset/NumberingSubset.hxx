// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_NUMBERINGSUBSET_NUMBERINGSUBSET_DOT_HXX_
#define MOREFEM_CORE_NUMBERINGSUBSET_NUMBERINGSUBSET_DOT_HXX_
// IWYU pragma: private, include "Core/NumberingSubset/NumberingSubset.hpp"
// *** MoReFEM header guards *** < //


#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/UniqueId/UniqueId.hpp"

#include "Core/NumberingSubset/UniqueId.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline bool operator==(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


    inline bool operator<(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return lhs.GetUniqueId() < rhs.GetUniqueId();
    }


    inline bool operator!=(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return !(operator==(lhs, rhs));
    }


    inline bool NumberingSubset::DoMoveMesh() const noexcept
    {
        return do_move_mesh_;
    }


    template<class EnumT>
    constexpr NumberingSubsetNS::unique_id AsNumberingSubsetId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return NumberingSubsetNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_NUMBERINGSUBSET_NUMBERINGSUBSET_DOT_HXX_
// *** MoReFEM end header guards *** < //
