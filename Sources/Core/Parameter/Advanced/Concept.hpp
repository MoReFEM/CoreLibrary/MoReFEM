// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_PARAMETER_ADVANCED_CONCEPT_DOT_HPP_
#define MOREFEM_CORE_PARAMETER_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <type_traits>

#include "ThirdParty/Wrappers/Eigen/Internal/Concept.hpp"


namespace MoReFEM::Advanced::Concept::ParameterNS
{


    /*!
     * \class doxygen_hide_tparam_parameter_concept_storage
     *
     * \tparam StorageT This template parameter specifies which type is used to store the actual content of a `Parameter` for a given `GeometricElt` /
     * `QuadraturePoint` combination.
     *
     * Default case is to use:
     * - `double` for a scalar `Parameter`
     * - `Eigen::VectorXd` for a vectorial `Parameter`
     * - `Eigen::MatrixXd` for a matricial `Parameter`
     *
     * However, in some cases we might want to use something else, e.g. a matrix which size is known at compile time. In
     * this case, you may provide in this argument the type you want to use.
     */


    /*!
     * \brief Concept to define underlying storage used to store the data of the `Parameter`.
     *
     * For a scalar `Parameter`, this is typically a `double` (but any arithmetic type is actually possible).
     * For a vectorial or matricial `Parameter`, an instantiation of an `Eigen::Matrix` object is expected.
     *
     * We don't check eveything here (for instance a `EigenMatrix` for a scalar `Parameter` would be dubious) but it
     * already helps clarifying the intent for the template parameter used for storage.
     */
    template<typename T>
    concept Storage = std::is_arithmetic_v<T> || ::MoReFEM::Internal::Concept::IsEigenMatrixInstantiation<T>;


} // namespace MoReFEM::Advanced::Concept::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_PARAMETER_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
