// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <string>

#include "Core/Parameter/FiberEnum.hpp"


namespace MoReFEM::FiberNS
{


    std::string AsString(AtNodeOrAtQuadPt value)
    {
        switch (value)
        {
        case AtNodeOrAtQuadPt::at_node:
            return "at_node";
        case AtNodeOrAtQuadPt::at_quad_pt:
            return "at_quad_pt";
        case AtNodeOrAtQuadPt::irrelevant:
            return "irrelevant";
        case AtNodeOrAtQuadPt::none:
            return "none";
        }

        assert(false && "All values should be considered in the switch.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
