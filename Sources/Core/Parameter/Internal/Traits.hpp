// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HPP_
#define MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

// IWYU pragma: no_include <__nullptr>

#include "Utilities/LinearAlgebra/StrongType.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/Parameter/Advanced/Concept.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Domain/UniqueId.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    //! Convenient alias.
    using Type = ::MoReFEM::ParameterNS::Type;

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================
    namespace Impl
    {

        template<ParameterNS::Type TypeT>
        struct DefaultStorage;

        template<>
        struct DefaultStorage<ParameterNS::Type::scalar>
        {
            using type = double;
        };

        template<>
        struct DefaultStorage<ParameterNS::Type::vector>
        {
            using type = Eigen::VectorXd;
        };

        template<>
        struct DefaultStorage<ParameterNS::Type::matrix>
        {
            using type = Eigen::MatrixXd;
        };

    } // namespace Impl

    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================

    /*!
     * \brief Alias to define default storage to use for each `Type` of `Parameter`.
     *
     */
    template<ParameterNS::Type TypeT>
    using DefaultStorage = typename Impl::DefaultStorage<TypeT>::type;


    /*!
     * \class doxygen_hide_param_traits_value_type
     *
     * \brief Type used to express the content of a \a Parameter at each \a LocalCoords.
     */

    /*!
     * \class doxygen_hide_param_traits_return_type
     *
     * \brief Type used to return the content of a \a Parameter at each \a LocalCoords.
     *
     * \internal It is not exactly the same as \a value_type as we may use constant reference ti avoir unneeded copy.
     */


    /*!
     * \class doxygen_hide_param_traits_value_type
     *
     * \brief Type used to express the content of a \a Parameter at each \a LocalCoords.
     */


    /*!
     * \class doxygen_hide_param_traits_constant_in_lua_file_type
     *
     * \brief Type used to interpret the information read from the Lua file.
     *
     * It is not necessarily the same as the one used for actual storage (for instance Eigen types
     * are used typically but \a OptionFile uses up a \a std::vector<double> which content is transformed into the
     * actual storage).
     */


    /*!
     * \class doxygen_hide_param_traits_piecewise_constant_in_lua_file_type
     *
     * \brief Type used to interpret the information read from the Lua file for the case of a piecewise constant by domain \a Parameter.
     *
     */


    /*!
     * \class doxygen_hide_param_traits_piecewise_constant_by_domain_type
     *
     * \brief Type used to store data related to a  piecewise constant by domain \a Parameter.
     *
     */

    /*!
     * \class doxygen_hide_param_traits_variant_type
     *
     * \brief Type of the variant giving away all the possible types that might be used to express the content of the \a Parameter.
     *
     * \a nullptr is to be used when the \a Parameter is actually not to be defined in the \a Model.
     *
     */

    /*!
     * \class doxygen_hide_param_traits_lua_function_type
     *
     * \brief \a std::false_type if the type of \a Parameter doesn't support Lua function, or the MoReFEM::Wrappers::Lua::spatial_function otherwise.
     *
     */

    /*!
     * \brief Traits class that yields relevant C++ types to use for each \a TypeT.
     *
     * \copydoc doxygen_hide_tparam_parameter_concept_storage
     *
     */
    template<Type TypeT, ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = DefaultStorage<TypeT>>
    struct Traits
    {
        //! \copydoc doxygen_hide_param_traits_value_type
        using value_type = StorageT;

        //! \copydoc doxygen_hide_param_traits_return_type
        using return_type = std::conditional_t<std::is_trivial_v<StorageT>, StorageT, const StorageT&>;

        //! \copydoc doxygen_hide_param_traits_constant_in_lua_file_type
        using constant_in_lua_file_type = std::conditional_t<TypeT == Type::scalar, double, std::vector<double>>;

        //! Self explaining.
        using non_constant_reference = value_type&;

        //! \copydoc doxygen_hide_param_traits_piecewise_constant_in_lua_file_type
        using piecewise_constant_in_lua_file_type = std::map<::MoReFEM::DomainNS::unique_id, constant_in_lua_file_type>;

        //! \copydoc doxygen_hide_param_traits_piecewise_constant_by_domain_type
        using piecewise_constant_by_domain_type = std::map<::MoReFEM::DomainNS::unique_id, value_type>;

        // clang-format off
        //! \copydoc doxygen_hide_param_traits_lua_function_type
        using lua_function_type =
        std::conditional_t
        <
            TypeT == Type::scalar,
            ::MoReFEM::Wrappers::Lua::spatial_function,
            std::false_type
        >;

        //! \copydoc doxygen_hide_param_traits_variant_type
        using variant_type =
        std::conditional_t
        <
            TypeT== Type::scalar,
            std::variant
            <
                constant_in_lua_file_type, piecewise_constant_in_lua_file_type, lua_function_type, std::nullptr_t
            >,
            std::variant
            <
                constant_in_lua_file_type, piecewise_constant_in_lua_file_type, std::nullptr_t
            >
        >;
        // clang-format on

        //! Returns 0.
        //! \param[in] Nrow Unused
        //! \param[in] Ncol Unused
        static value_type AllocateDefaultValue([[maybe_unused]] small_matrix_row_index_type Nrow,
                                               [[maybe_unused]] small_matrix_col_index_type Ncol) noexcept;
    };


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/Parameter/Internal/Traits.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HPP_
// *** MoReFEM end header guards *** < //
