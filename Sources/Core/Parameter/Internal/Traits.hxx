// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HXX_
#define MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HXX_
// IWYU pragma: private, include "Core/Parameter/Internal/Traits.hpp"
// *** MoReFEM header guards *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    template<Type TypeT, ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline auto
    Traits<TypeT, StorageT>::AllocateDefaultValue([[maybe_unused]] small_matrix_row_index_type Nrow,
                                                  [[maybe_unused]] small_matrix_col_index_type Ncol) noexcept
        -> value_type
    {
        if constexpr (TypeT == Type::scalar)
            return 0.;
        else if (TypeT == Type::vector)
        {
            auto ret = value_type(Nrow.Get());
            ret.setZero();
            return ret;
        } else
        {
            assert(TypeT == Type::matrix);
            auto ret = value_type(Nrow.Get(), Ncol.Get());
            ret.setZero();
            return ret;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HXX_
// *** MoReFEM end header guards *** < //
