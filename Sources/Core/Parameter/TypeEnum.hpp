// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_PARAMETER_TYPEENUM_DOT_HPP_
#define MOREFEM_CORE_PARAMETER_TYPEENUM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <string>


namespace MoReFEM::ParameterNS
{


    /*!
     * \class doxygen_hide_tparam_parameter_type
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */


    //! Type of the parameter to build.
    enum class Type { scalar, vector, matrix };


    /*!
     * \brief Returns a name as a string that describes the type considered.
     *
     * \note This function is used to populate the field of the Traits class in Parameter library.
     *
     * \tparam TypeT \a Type for which we want a string moniker.
     *
     * \return String moniker associated to \a TypeT.
     */
    template<Type TypeT>
    std::string Name();


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Core/Parameter/TypeEnum.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_PARAMETER_TYPEENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
