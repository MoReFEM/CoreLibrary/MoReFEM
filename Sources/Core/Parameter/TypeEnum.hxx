// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_PARAMETER_TYPEENUM_DOT_HXX_
#define MOREFEM_CORE_PARAMETER_TYPEENUM_DOT_HXX_
// IWYU pragma: private, include "Core/Parameter/TypeEnum.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>


namespace MoReFEM::ParameterNS
{


    template<Type TypeT>
    std::string Name()
    {
        if constexpr (TypeT == Type::scalar)
            return "scalar";
        else if constexpr (TypeT == Type::vector)
            return "vector";
        else if constexpr (TypeT == Type::matrix)
            return "matrix";
    }


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_PARAMETER_TYPEENUM_DOT_HXX_
// *** MoReFEM end header guards *** < //
