// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_SOLVER_SOLVER_DOT_HPP_
#define MOREFEM_CORE_SOLVER_SOLVER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/InputData/InputData.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Solver/Petsc.hpp"


namespace MoReFEM
{


    //! Whether a non linear solver might be used in the variational formulation
    enum class enable_non_linear_solver { no, yes };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_SOLVER_SOLVER_DOT_HPP_
// *** MoReFEM end header guards *** < //
