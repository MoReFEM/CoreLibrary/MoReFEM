// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_CONCEPT_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify a type is an instantiation of `TimeManager` template class.
     *
     */
    template<typename T>
    concept TimeManager = requires { T::ConceptIsTimeManager; };


#ifndef MOREFEM_GCC

//! See #1858
#if defined(__apple_build_version__)
#define TIME_MANAGER_TEMPLATE_KEYWORD class // no longer work with XCode 16 ::MoReFEM::Concept::TimeManager
#else
#define TIME_MANAGER_TEMPLATE_KEYWORD class
#endif
#else

//! See #1858
#define TIME_MANAGER_TEMPLATE_KEYWORD class

#endif


    /*!
     * \brief Concept to tell a class is intended to be  used as a `TimeManager` policy.
     */
    template<typename T>
    concept TimeManagerTimeStepPolicy = requires { T::ConceptIsTimeManagerEvolutionPolicy == true; };


} // namespace MoReFEM::Concept


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
