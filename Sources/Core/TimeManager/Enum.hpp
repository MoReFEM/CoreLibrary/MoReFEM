// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_ENUM_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/TimeManager/StaticOrDynamic.hpp" // IWYU pragma: export


namespace MoReFEM::TimeManagerNS
{


    /*!
     * \brief Keyword used to tell whether the internals of the class is modifiable or not.
     *
     * Used at the moment only for `TimeStep` Crtp.
     */
    enum class mode { read_only, modifiable };


    /*!
     * \brief Handy enumeration to increase or decrease time step.
     */
    enum class adapt_time_step { decreasing, increasing };


    /*!
     * \brief Enumeration that tells whether restart mode is supported or not.
     */
    enum class support_restart_mode { no, yes };

    /*!
     * \class doxygen_hide_support_restart_mode_template_arg
     *
     * \tparam DoSupportRestartModeT Whether restart mode is supported or not.
     */


    /*!
     * \class doxygen_hide_support_restart_mode_static_attribute
     *
     * \brief Enum that tells whether restart mode is supported or not.
     */


    /*!
     * \class doxygen_hide_concept_helper_time_manager_support_result_directory
     *
     * \brief Whether the time manager instance will expect a `ResultDirectory` field or not
     *
     */

} // namespace MoReFEM::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
