// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>
#include <string_view>

#include "Core/TimeManager/Exceptions/Exception.hpp"

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{


    std::string InvalidMethodForPolicyMsg(std::string_view method, std::string_view policy);


    std::string TimeStepAdaptationMinimumReachedMsg(double minimum_time_step);


    std::string ImproperRestartTimeForConstantTimeStepMsg(double sought_time, double initial_time, double time_step);


    std::string MissingFileMsg(const MoReFEM::FilesystemNS::File& filename);


    std::string RestartDirShouldntBeInResultDirMsg(const MoReFEM::FilesystemNS::Directory& restart_directory,
                                                   const MoReFEM::FilesystemNS::Directory& result_directory);

    std::string NegativeTimeStepMsg(double time_step);


} // namespace


namespace MoReFEM::ExceptionNS::TimeManagerNS
{


    InvalidMethodForPolicy::~InvalidMethodForPolicy() = default;


    InvalidMethodForPolicy::InvalidMethodForPolicy(std::string_view method,
                                                   std::string_view policy,
                                                   const std::source_location location)
    : MoReFEM::Exception(InvalidMethodForPolicyMsg(method, policy), location)
    { }


    TimeStepAdaptationMinimumReached::~TimeStepAdaptationMinimumReached() = default;


    TimeStepAdaptationMinimumReached::TimeStepAdaptationMinimumReached(double minimum_time_step,
                                                                       const std::source_location location)
    : MoReFEM::Exception(TimeStepAdaptationMinimumReachedMsg(minimum_time_step), location)
    { }


    ImproperRestartTimeForConstantTimeStep::~ImproperRestartTimeForConstantTimeStep() = default;

    ImproperRestartTimeForConstantTimeStep::ImproperRestartTimeForConstantTimeStep(double sought_time,
                                                                                   double initial_time,
                                                                                   double time_step,
                                                                                   const std::source_location location)
    : MoReFEM::Exception(ImproperRestartTimeForConstantTimeStepMsg(sought_time, initial_time, time_step), location)
    { }


    MissingFile::~MissingFile() = default;

    MissingFile::MissingFile(const FilesystemNS::File& filename, const std::source_location location)
    : MoReFEM::Exception(MissingFileMsg(filename), location)
    { }


    RestartDirShouldntBeInResultDir::~RestartDirShouldntBeInResultDir() = default;

    RestartDirShouldntBeInResultDir::RestartDirShouldntBeInResultDir(const FilesystemNS::Directory& restart_directory,
                                                                     const FilesystemNS::Directory& result_directory,
                                                                     const std::source_location location)
    : MoReFEM::Exception(RestartDirShouldntBeInResultDirMsg(restart_directory, result_directory), location)
    { }


    NegativeTimeStep::~NegativeTimeStep() = default;

    NegativeTimeStep::NegativeTimeStep(double time_step, const std::source_location location)
    : MoReFEM::Exception(NegativeTimeStepMsg(time_step), location)
    { }


} // namespace MoReFEM::ExceptionNS::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string InvalidMethodForPolicyMsg(std::string_view method, std::string_view policy)
    {
        std::ostringstream oconv;
        oconv << "TimeManager::" << method << " method was called for policy " << policy
              << " which doesn't support it.";
        return oconv.str();
    }


    std::string TimeStepAdaptationMinimumReachedMsg(double minimum_time_step)
    {
        std::ostringstream oconv;
        oconv << "Your simulation tries to decrease the time step to less than the minimum "
                 "time step allowed ("
              << minimum_time_step << " s).";
        return oconv.str();
    }


    std::string ImproperRestartTimeForConstantTimeStepMsg(double sought_time, double initial_time, double time_step)
    {
        std::ostringstream oconv;
        oconv << "Restart time read from restart data is " << sought_time
              << ", but it does not "
                 "seem to be a proper value (allowed values for constant time step policy must respect "
              << initial_time << " + n * " << time_step
              << ", where n is an integer). It is likely you are trying to use the wrong restart data.";
        return oconv.str();
    }


    std::string MissingFileMsg(const MoReFEM::FilesystemNS::File& filename)
    {
        std::ostringstream oconv;
        oconv << "Expected file " << filename << " couldn't be found.";
        return oconv.str();
    }


    std::string RestartDirShouldntBeInResultDirMsg(const MoReFEM::FilesystemNS::Directory& restart_directory,
                                                   const MoReFEM::FilesystemNS::Directory& result_directory)
    {
        std::ostringstream oconv;
        oconv << "Restart directory (" << restart_directory << ") can't be enclosed in result directory ("
              << result_directory << "). Please fix your input data file.";
        return oconv.str();
    }


    std::string NegativeTimeStepMsg(double time_step)
    {
        std::ostringstream oconv;
        oconv << "Time step " << time_step << " was provided but is negative!";
        return oconv.str();
    }


} // namespace
