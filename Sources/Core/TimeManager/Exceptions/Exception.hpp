// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::TimeManagerNS
{


    //! Called when a method makes no sense for a given time step policy.,
    class InvalidMethodForPolicy : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] policy Policy for which the exception was thrown.
         * \param[in] method Name of the method not supported by the policy.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidMethodForPolicy(std::string_view method,
                                        std::string_view policy,
                                        const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidMethodForPolicy() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidMethodForPolicy(const InvalidMethodForPolicy& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InvalidMethodForPolicy(InvalidMethodForPolicy&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidMethodForPolicy& operator=(const InvalidMethodForPolicy& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InvalidMethodForPolicy& operator=(InvalidMethodForPolicy&& rhs) = delete;
    };


    //! Called when time step adaptation reaches too small a value.
    class TimeStepAdaptationMinimumReached : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] minimum_time_step Minimum time step, in seconds
         * \copydoc doxygen_hide_source_location
         */
        explicit TimeStepAdaptationMinimumReached(
            double minimum_time_step,
            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~TimeStepAdaptationMinimumReached() override;

        //! \copydoc doxygen_hide_copy_constructor
        TimeStepAdaptationMinimumReached(const TimeStepAdaptationMinimumReached& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeStepAdaptationMinimumReached(TimeStepAdaptationMinimumReached&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TimeStepAdaptationMinimumReached& operator=(const TimeStepAdaptationMinimumReached& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TimeStepAdaptationMinimumReached& operator=(TimeStepAdaptationMinimumReached&& rhs) = delete;
    };


    //! Called when restart data can't be interpreted correctly.
    class ImproperRestartTimeForConstantTimeStep : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] sought_time Time which was attempted, in seconds. Should be \a initial_time + n * \a time_step...
         * but if the exception is called it is not the case.
         * \param[in] initial_time Initial time for the model with the constant step policy, in seconds
         * \param[in] time_step Time step of  the constant step policy, in seconds
         * \copydoc doxygen_hide_source_location
         */
        explicit ImproperRestartTimeForConstantTimeStep(
            double sought_time,
            double initial_time,
            double time_step,
            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~ImproperRestartTimeForConstantTimeStep() override;

        //! \copydoc doxygen_hide_copy_constructor
        ImproperRestartTimeForConstantTimeStep(const ImproperRestartTimeForConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ImproperRestartTimeForConstantTimeStep(ImproperRestartTimeForConstantTimeStep&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ImproperRestartTimeForConstantTimeStep& operator=(const ImproperRestartTimeForConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ImproperRestartTimeForConstantTimeStep& operator=(ImproperRestartTimeForConstantTimeStep&& rhs) = delete;
    };


    //! Called when restart data file couldn't be found.
    class MissingFile : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] filename Name of the file that couldn't be found.
         * \copydoc doxygen_hide_source_location
         */
        explicit MissingFile(const FilesystemNS::File& filename,
                             const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~MissingFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        MissingFile(const MissingFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MissingFile(MissingFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MissingFile& operator=(const MissingFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MissingFile& operator=(MissingFile&& rhs) = delete;
    };


    //! Called when restart directory is poorly located.
    class RestartDirShouldntBeInResultDir : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] restart_directory The directory which encloses the read only data required to run restart mode.
         * \param[in] result_directory The directory in write mode into which outputs will be written. Rank last subdir has been stripped beforehand.
         * \copydoc doxygen_hide_source_location
         */
        explicit RestartDirShouldntBeInResultDir(const FilesystemNS::Directory& restart_directory,
                                                 const FilesystemNS::Directory& result_directory,
                                                 const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~RestartDirShouldntBeInResultDir() override;

        //! \copydoc doxygen_hide_copy_constructor
        RestartDirShouldntBeInResultDir(const RestartDirShouldntBeInResultDir& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        RestartDirShouldntBeInResultDir(RestartDirShouldntBeInResultDir&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        RestartDirShouldntBeInResultDir& operator=(const RestartDirShouldntBeInResultDir& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RestartDirShouldntBeInResultDir& operator=(RestartDirShouldntBeInResultDir&& rhs) = delete;
    };


    //! Called when a negative time step is provided.
    class NegativeTimeStep : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] time_step Time step provided.
         * \copydoc doxygen_hide_source_location
         */
        explicit NegativeTimeStep(double time_step,
                                  const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~NegativeTimeStep() override;

        //! \copydoc doxygen_hide_copy_constructor
        NegativeTimeStep(const NegativeTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NegativeTimeStep(NegativeTimeStep&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NegativeTimeStep& operator=(const NegativeTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NegativeTimeStep& operator=(NegativeTimeStep&& rhs) = delete;
    };


} // namespace MoReFEM::ExceptionNS::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
