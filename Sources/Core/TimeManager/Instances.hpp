// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_INSTANCES_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_INSTANCES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp"
#include "Core/TimeManager/Policy/Evolution/None.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/Policy/Evolution/VariableTimeStep.hpp"
#include "Core/TimeManager/TimeManager.hpp" // IWYU pragma: export


namespace MoReFEM::TimeManagerNS::Instance
{


    //! Convenient alias for time manager with no evolution with time and no reading from input data
    using None = TimeManager<TimeManagerNS::Policy::None>;

    //! Convenient alias for time manager with no evolution with time but a time set from input data.
    using Static = TimeManager<TimeManagerNS::Policy::Static>;

    //! Convenient alias for time manager with constant time step evolution.

    template<support_restart_mode DoSupportRestartModeT = support_restart_mode::no>
    using ConstantTimeStep = TimeManager<TimeManagerNS::Policy::ConstantTimeStep<DoSupportRestartModeT>>;

    //! Convenient alias for time manager with variable time step evolution (time step may be reduced if model
    //! fails to converge).
    template<support_restart_mode DoSupportRestartModeT = support_restart_mode::no>
    using VariableTimeStep = TimeManager<TimeManagerNS::Policy::VariableTimeStep<DoSupportRestartModeT>>;


} // namespace MoReFEM::TimeManagerNS::Instance


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/TimeManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_INSTANCES_DOT_HPP_
// *** MoReFEM end header guards *** < //
