// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CONSTANTTIMESTEP_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CONSTANTTIMESTEP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <limits>
#include <string>

#include "Utilities/InputData/Concept.hpp"

// IWYU pragma: begin_exports
#include "Core/TimeManager/Concept.hpp"
#include "Core/TimeManager/Policy/Evolution/Crtp/Restart.hpp"
#include "Core/TimeManager/Policy/Evolution/Crtp/TimeStep.hpp"
#include "Core/TimeManager/Policy/Evolution/Internal/NoRestart.hpp"
#include "Core/TimeManager/TimeAccess/TimeAccess.hpp"
// IWYU pragma: end_exports


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }
namespace MoReFEM::TestNS::TimeManagerNS { template <TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> class Viewer; } // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{


    /*!
     * \brief TimeManager policy when time step is constant.
     *
     * So in input file time step and maximum time are enough to describe the whole time evolution.
     */
    // clang-format off
    template<support_restart_mode DoSupportRestartModeT = support_restart_mode::no>
    class ConstantTimeStep
    : public TimeAccess,
    public Crtp::TimeStep<ConstantTimeStep<DoSupportRestartModeT>, mode::read_only>,
    public std::conditional_t
    <
        DoSupportRestartModeT == support_restart_mode::yes,
        Crtp::Restart<ConstantTimeStep<DoSupportRestartModeT>>,
        ::MoReFEM::Internal::TimeManagerNS::NoRestart
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = ConstantTimeStep<DoSupportRestartModeT>;

        //! Convenient alias.
        static inline std::string ClassName = "ConstantTimeStep";

        // \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship used only for tests.
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
        friend class MoReFEM::TestNS::TimeManagerNS::Viewer;
        // \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! \copydoc doxygen_hide_concept_helper_is_time_manager_time_step_policy
        static inline constexpr bool ConceptIsTimeManagerEvolutionPolicy = true;

        //! \copydoc doxygen_hide_concept_helper_time_manager_support_result_directory
        static inline constexpr bool ConceptSupportResultDirectory = true;

        //! \copydoc doxygen_hide_support_restart_mode_static_attribute
        static inline constexpr support_restart_mode do_support_restart_mode = DoSupportRestartModeT;

        //! Alias to \a TimeAccess object.
        using time_access_parent = TimeManagerNS::TimeAccess;

        static_assert(std::is_convertible<self*, time_access_parent*>());

        //! Alias to Crtp that stores and provides access to time step.
        using time_step_parent = Crtp::TimeStep<ConstantTimeStep, mode::read_only>;

        static_assert(std::is_convertible<self*, time_step_parent*>());

        //! Alias to restart parent.
        using restart_parent = std::conditional_t<DoSupportRestartModeT == support_restart_mode::yes,
                                                  Crtp::Restart<self>,
                                                  ::MoReFEM::Internal::TimeManagerNS::NoRestart>;

        static_assert(std::is_convertible<self*, restart_parent*>());

        //! Friendship to allow this parent to call protected members we don't want to expose
        //! completely in \a TimeManager interface.
        friend restart_parent;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydetails doxygen_hide_mpi_param
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        ConstantTimeStep(const Wrappers::Mpi& mpi, const ModelSettingsT& model_settings, const InputDataT& input_data);

        //! Destructor.
        ~ConstantTimeStep() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ConstantTimeStep(const ConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ConstantTimeStep(ConstantTimeStep&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ConstantTimeStep& operator=(const ConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ConstantTimeStep& operator=(ConstantTimeStep&& rhs) = delete;

        ///@}

        /*!
         * \brief Increment the time.
         *
         * Time is incremented with current time step. Tn+1 = Tn + dt.
         * Current time step is stored in the class.
         *
         */
        void IncrementTime();

        /*!
         * \brief Decrement the time.
         *
         * Time is decremented with current time step. Tn+1 = Tn - dt.
         * Current time step is stored in the class.
         *
         */
        void DecrementTime();

        //! Returns true if current time is equal or beyond maximum time.
        bool HasFinished() const;

        //! Returns whether the time step is constant (obviously true for this class!).
        static constexpr bool IsTimeStepConstant() noexcept;

        //! Returns whether there is a time evolution at all.
        static constexpr bool IsTimeEvolving() noexcept;

        //! Get the maximum time (in seconds).
        double GetMaximumTime() const noexcept;

        /*!
         * \copydoc doxygen_hide_time_manager_reset_time_manager_at_initial_time
         */
        void ResetTimeManagerAtInitialTime();

        //!\copydoc doxygen_hide_time_manager_set_restart
        void SetRestartForEvolutionPolicy(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data)
            requires(DoSupportRestartModeT == support_restart_mode::yes);


      private:
        //! Get the number of iterations done from the start.
        std::size_t& TimeStepIndex() noexcept;

        //! Get the initial time.
        double GetInitialTime() const noexcept;

        /*!
         * \brief Compute current time depending of time step index.
         *
         * For this policy time (in seconds) is initial_time + \a current_time_step_index * time_step.
         *
         * \param[in] current_time_step_index Current time step index.
         *
         * \return Time (in seconds).
         */
        double ComputeTime(std::size_t current_time_step_index) const noexcept;

      private:
        //! Maximum time.
        double maximum_time_ = std::numeric_limits<double>::lowest();

        /*!
         * \brief Number of iterations that separate the initial time to current time step.
         *
         * \attention It is NOT the same as the \a Ntimes_modified that is present in \a TimeManager class:
         * when \a DecrementTime is called for instance \a time_step_index_ decreases... but \a Ntimes_modified
         * increases).
         */
        std::size_t time_step_index_{ 0UL };

        //! Initial time.
        double initial_time_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::TimeManagerNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CONSTANTTIMESTEP_DOT_HPP_
// *** MoReFEM end header guards *** < //
