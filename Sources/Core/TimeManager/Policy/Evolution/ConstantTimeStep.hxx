// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CONSTANTTIMESTEP_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CONSTANTTIMESTEP_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef>
#include <limits>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/TimeManager/TimeAccess/TimeAccess.hpp"


namespace MoReFEM::TimeManagerNS::Policy
{


    template<support_restart_mode DoSupportRestartModeT>
    void ConstantTimeStep<DoSupportRestartModeT>::SetRestartForEvolutionPolicy(
        const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data)
        requires(DoSupportRestartModeT == support_restart_mode::yes)
    {
        const auto sought_time = restart_time_data.GetTime();

        double iteration_index_as_double = (sought_time - GetInitialTime()) / time_step_parent::GetTimeStep();

        time_step_index_ = static_cast<std::size_t>(std::round(iteration_index_as_double));

        if (!NumericNS::AreEqual(ComputeTime(time_step_index_), sought_time))
            throw ExceptionNS::TimeManagerNS::ImproperRestartTimeForConstantTimeStep(
                sought_time, GetInitialTime(), time_step_parent::GetTimeStep());
    }


    template<support_restart_mode DoSupportRestartModeT>
    template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT, ::MoReFEM::Concept::InputDataType InputDataT>
    ConstantTimeStep<DoSupportRestartModeT>::ConstantTimeStep(const Wrappers::Mpi& mpi,
                                                              const ModelSettingsT& model_settings,
                                                              const InputDataT& input_data)
    : time_step_parent(
          ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::TimeManager::TimeStep>(model_settings, input_data)),
      restart_parent(mpi, model_settings, input_data)
    {
        maximum_time_ =
            ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::TimeManager::TimeMax>(model_settings, input_data);
        initial_time_ =
            ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::TimeManager::TimeInit>(model_settings, input_data);

        assert(time_step_parent::GetTimeStep() > 1.e-9
               && "Should have been already checked when reading the input data "
                  "hence the assert rather than an exception.");
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline void ConstantTimeStep<DoSupportRestartModeT>::IncrementTime()
    {
        auto& time_step_index = TimeStepIndex();
        ++time_step_index;

        time_access_parent::SetStaticOrDynamic(StaticOrDynamic::dynamic_);
        time_access_parent::GetNonCstTime() = ComputeTime(time_step_index);
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline void ConstantTimeStep<DoSupportRestartModeT>::DecrementTime()
    {
        auto& time_step_index = TimeStepIndex();
        --time_step_index;

        time_access_parent::GetNonCstTime() = ComputeTime(time_step_index);
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline double ConstantTimeStep<DoSupportRestartModeT>::GetMaximumTime() const noexcept
    {
        assert(!NumericNS::AreEqual(maximum_time_, std::numeric_limits<double>::lowest()));
        return maximum_time_;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline bool ConstantTimeStep<DoSupportRestartModeT>::HasFinished() const
    {
        decltype(auto) time = time_access_parent::GetTime();

        return time + NumericNS::DefaultEpsilon<double>() >= GetMaximumTime();
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline constexpr bool ConstantTimeStep<DoSupportRestartModeT>::IsTimeEvolving() noexcept
    {
        return true;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline constexpr bool ConstantTimeStep<DoSupportRestartModeT>::IsTimeStepConstant() noexcept
    {
        return true;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline std::size_t& ConstantTimeStep<DoSupportRestartModeT>::TimeStepIndex() noexcept
    {
        return time_step_index_;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline double ConstantTimeStep<DoSupportRestartModeT>::GetInitialTime() const noexcept
    {
        return initial_time_;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline void ConstantTimeStep<DoSupportRestartModeT>::ResetTimeManagerAtInitialTime()
    {
        GetNonCstTime() = initial_time_;
        time_step_index_ = 0;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline double
    ConstantTimeStep<DoSupportRestartModeT>::ComputeTime(std::size_t current_time_step_index) const noexcept
    {
        return GetInitialTime() + static_cast<double>(current_time_step_index) * time_step_parent::GetTimeStep();
    }


} // namespace MoReFEM::TimeManagerNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CONSTANTTIMESTEP_DOT_HXX_
// *** MoReFEM end header guards *** < //
