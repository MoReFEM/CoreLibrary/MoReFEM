// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_RESTART_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_RESTART_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <limits>

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/TimeManager/Enum.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Crtp
{


    /*!
     * \class doxygen_hide_time_manager_set_restart
     *
     * \brief Specific restart operations related to the time manager policy chosen.
     *
     * \param[in] restart_time_data Restart data.
     */


    /*!
     * \brief CRTP class that provides restart-related functionalities to a time evolution policy.
     */
    template<class DerivedT>
    class Restart
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_model_settings_input_data_arg
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        explicit Restart(const Wrappers::Mpi& mpi, const ModelSettingsT& model_settings, const InputDataT& input_data);

        //! Destructor.
        ~Restart() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Restart(const Restart& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Restart(Restart&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Restart& operator=(const Restart& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Restart& operator=(Restart&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Tells whether the model is run in restart mode or not.
         *
         * \return True if model is in restart mode.
         */
        bool IsInRestartMode() const noexcept;


        /*!
         * \brief If there is restart data in the input datafile, set it up properly.
         *

         */
        void SetRestartIfRelevant();

        /*!
         * \brief Returns the time index used to tag the data that should be loaded at the beginning of the restart run.
         *
         * This index is the one used in time_iteration.hhdata line of the original run from which we want to restart.
         *
         * \return Index at which the model should begin.
         */
        std::size_t GetRestartTimeIndex() const noexcept;

        /*!
         * \brief Returns the file which contains the time iteration from previous run data.
         *
         * \return File which time iteration data.
         */
        const FilesystemNS::File& GetRestartTimeIterationFile() const noexcept;


        /*!
         * \brief Return the result directory from a previous run of the model, which we may want to restart from.
         *
         * If not in restart mode, just set it to nullopt.
         *
         * \return Directory which already exists and contains the data from a previous run.
         */
        const FilesystemNS::Directory& GetRestartDataDirectory() const noexcept;


      private:
        //! \brief Time index used to tag the data that should be loaded at the beginning of the restart run.
        //!
        //! This index is the one used in time_iteration.hhdata line of the original run from which we want to restart.
        std::size_t restart_time_index_{ 0UL };

        /*!
         * \brief Result directory from a previous run of the model, which we may want to restart from.
         *
         * If not in restart mode, just set it to nullopt.
         */
        std::optional<FilesystemNS::Directory> restart_data_directory_ = std::nullopt;

        /*!
         * \brief File which contains the time iteration from previous run data.
         */
        FilesystemNS::File restart_time_iteration_file_;
    };


} // namespace MoReFEM::TimeManagerNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/Policy/Evolution/Crtp/Restart.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_RESTART_DOT_HPP_
// *** MoReFEM end header guards *** < //
