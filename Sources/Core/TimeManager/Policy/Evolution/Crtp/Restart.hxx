// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_RESTART_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_RESTART_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/Policy/Evolution/Crtp/Restart.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"


namespace MoReFEM::TimeManagerNS::Crtp
{


    template<class DerivedT>
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    Restart<DerivedT>::Restart(const Wrappers::Mpi& mpi,
                               const ModelSettingsT& model_settings,
                               const InputDataT& input_data)
    {
        restart_time_index_ =
            ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Restart::TimeIndex>(model_settings, input_data);

        if (restart_time_index_ != 0UL) // input data file explicitly tells to choose this value if no restart
        {
            decltype(auto) result_dir_path =
                ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Result::OutputDirectory>(model_settings,
                                                                                                input_data);

            decltype(auto) restart_dir_path =
                ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Restart::DataDirectory>(model_settings,
                                                                                               input_data);

            restart_data_directory_.emplace(
                FilesystemNS::Directory(mpi, restart_dir_path, FilesystemNS::behaviour::read));

            // We need to check restart data aren't in the output directory
            {
                auto restart_directory = GetRestartDataDirectory();

                auto result_directory = FilesystemNS::Directory(result_dir_path, FilesystemNS::behaviour::read);
                assert(result_directory.DoExist());

                if (FilesystemNS::IsFirstSubfolderOfSecond(restart_directory, result_directory))
                    throw ExceptionNS::TimeManagerNS::RestartDirShouldntBeInResultDir(restart_directory,
                                                                                      result_directory);
            }

            restart_time_iteration_file_ = restart_data_directory_.value().AddFile("time_iteration.hhdata");

            if (!restart_time_iteration_file_.DoExist())
                throw ExceptionNS::TimeManagerNS::MissingFile(restart_time_iteration_file_);
        }
    }


    template<class DerivedT>
    inline std::size_t Restart<DerivedT>::GetRestartTimeIndex() const noexcept
    {
        return restart_time_index_;
    }


    template<class DerivedT>
    inline const FilesystemNS::File& Restart<DerivedT>::GetRestartTimeIterationFile() const noexcept
    {
        return restart_time_iteration_file_;
    }


    template<class DerivedT>
    inline bool Restart<DerivedT>::IsInRestartMode() const noexcept
    {
        return restart_time_index_ != 0UL;
    }


    template<class DerivedT>
    inline const FilesystemNS::Directory& Restart<DerivedT>::GetRestartDataDirectory() const noexcept
    {
        assert(restart_data_directory_ != std::nullopt
               && "If this accessor is called it should have been initialized!");
        return restart_data_directory_.value();
    }


    template<class DerivedT>
    void Restart<DerivedT>::SetRestartIfRelevant()
    {
        if (IsInRestartMode())
        {
            InterpretOutputFilesNS::TimeIterationFile time_iteration_file(GetRestartTimeIterationFile());

            decltype(auto) time_iteration_data = time_iteration_file.GetTimeIteration(restart_time_index_);
            assert(time_iteration_data.GetIteration() == restart_time_index_);

            static_cast<DerivedT&>(*this).SetInitialTime(time_iteration_data.GetTime());
            static_cast<DerivedT&>(*this).SetNtimeModifiedForRestart(restart_time_index_);

            static_cast<DerivedT&>(*this).SetStaticOrDynamic(StaticOrDynamic::dynamic_);
            static_cast<DerivedT&>(*this).SetRestartForEvolutionPolicy(time_iteration_data);
        }
    }


} // namespace MoReFEM::TimeManagerNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_RESTART_DOT_HXX_
// *** MoReFEM end header guards *** < //
