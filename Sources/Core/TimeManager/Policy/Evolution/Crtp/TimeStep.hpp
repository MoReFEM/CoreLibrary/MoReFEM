// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_TIMESTEP_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_TIMESTEP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <limits>

#include "Core/TimeManager/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::TimeManagerNS::Crtp
{


    /*!
     * \brief This CRTP class provides time step member and access to to some evolution policies of `TimeManager`.
     *
     * \tparam ModeT Specify here whether time step may evolve or if value set at construction is kept unchanged throughout
     *  your model.
     */
    template<class DerivedT, mode ModeT>
    class TimeStep
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] time_step Time step (in seconds) at the beginning of your model.
         *
         */
        explicit TimeStep(double time_step);

        //! Destructor.
        ~TimeStep() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TimeStep(const TimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeStep(TimeStep&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TimeStep& operator=(const TimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TimeStep& operator=(TimeStep&& rhs) = delete;

        ///@}

        //! Get the size of a time step, in seconds.
        double GetTimeStep() const noexcept;

        //! Get the inverse of time step.
        //!  It is assumed time step can't be zero, so no check in release mode.
        double GetInverseTimeStep() const noexcept;

        /*!
         * \brief Set a new time step (in seconds).
         *
         * \param[in] time_step New value. It is just checked that this value is positive; it may be greater or lower than previous one
         * (it is inside the policy for which the CRTP is used that you have to check for instance if there is a maximum
         * value not to overcome). \copydoc doxygen_hide_source_location
         *
         */
        void SetTimeStep(double time_step, const std::source_location location = std::source_location::current())
            requires(ModeT == mode::modifiable);

      protected:
        //! Non cnstant accessor to the time step value.
        double& GetNonCstTimeStep() noexcept
            requires(ModeT == mode::modifiable);

      private:
        //! Size of a time step.
        double time_step_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::TimeManagerNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/Policy/Evolution/Crtp/TimeStep.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_TIMESTEP_DOT_HPP_
// *** MoReFEM end header guards *** < //
