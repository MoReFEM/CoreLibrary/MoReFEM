// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_TIMESTEP_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_TIMESTEP_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/Policy/Evolution/Crtp/TimeStep.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/TimeManager/Exceptions/Exception.hpp"


namespace MoReFEM::TimeManagerNS::Crtp
{


    template<class DerivedT, mode ModeT>
    TimeStep<DerivedT, ModeT>::TimeStep(double time_step) : time_step_(time_step)
    { }


    template<class DerivedT, mode ModeT>
    inline double TimeStep<DerivedT, ModeT>::GetTimeStep() const noexcept
    {
        assert(!NumericNS::AreEqual(time_step_, std::numeric_limits<double>::lowest()));
        return time_step_;
    }


    template<class DerivedT, mode ModeT>
    inline double TimeStep<DerivedT, ModeT>::GetInverseTimeStep() const noexcept
    {
        assert(!NumericNS::IsZero(GetTimeStep()));
        return 1. / GetTimeStep();
    }


    template<class DerivedT, mode ModeT>
    inline void TimeStep<DerivedT, ModeT>::SetTimeStep(double time_step, const std::source_location location)
        requires(ModeT == mode::modifiable)
    {
        if (time_step < 0.)
            throw ExceptionNS::TimeManagerNS::NegativeTimeStep(time_step, location);

        time_step_ = time_step;
    }


    template<class DerivedT, mode ModeT>
    inline double& TimeStep<DerivedT, ModeT>::GetNonCstTimeStep() noexcept
        requires(ModeT == mode::modifiable)
    {
        return time_step_;
    }


} // namespace MoReFEM::TimeManagerNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_CRTP_TIMESTEP_DOT_HXX_
// *** MoReFEM end header guards *** < //
