// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_INTERNAL_NORESTART_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_INTERNAL_NORESTART_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/InputData/Concept.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::TimeManagerNS
{


    /*!
     * \brief Helper class for time evolution policies when restart mode is not supported.
     *
     * \internal When restart is supported, the policy inherits from a CRTP class. Current class is there
     * as a placeholder for the opposite case.
     */
    class NoRestart
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * It does absolutely nothing - its sole purpose is to provide expected interface in
         * TimeManager policies.
         *
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        NoRestart(const ::MoReFEM::Wrappers::Mpi&, const ModelSettingsT&, const InputDataT&)
        { }

        //! Destructor.
        ~NoRestart() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NoRestart(const NoRestart& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NoRestart(NoRestart&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NoRestart& operator=(const NoRestart& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NoRestart& operator=(NoRestart&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::Internal::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_INTERNAL_NORESTART_DOT_HPP_
// *** MoReFEM end header guards *** < //
