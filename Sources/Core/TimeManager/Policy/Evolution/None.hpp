// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_NONE_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_NONE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>

#include "Utilities/InputData/Concept.hpp"

#include "Core/TimeManager/Concept.hpp"               // IWYU pragma: export
#include "Core/TimeManager/TimeAccess/TimeAccess.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class adapt_time_step; }
namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }
namespace MoReFEM::TestNS::TimeManagerNS { template <TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> class Viewer; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{

    /*!
     * \class doxygen_hide_static_time_policy_method_that_throws
     *
     * \brief This method is expected by `TimeManager` interface, but isn't relevant
     * for current policy - so it throws.
     */


    /*!
     * \brief TimeManager policy for static models.
     *
     */
    class None
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = None;

        //! Convenient alias.
        static inline std::string ClassName = "None";

        //! \copydoc doxygen_hide_concept_helper_is_time_manager_time_step_policy
        static inline constexpr bool ConceptIsTimeManagerEvolutionPolicy = true;

        //! \copydoc doxygen_hide_support_restart_mode_static_attribute
        static inline constexpr support_restart_mode do_support_restart_mode = support_restart_mode::no;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor that does strictly nothing!
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydetails doxygen_hide_mpi_param
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        explicit None(const Wrappers::Mpi& mpi, const ModelSettingsT& model_settings, const InputDataT& input_data);

        //! Defaut constructor, to use only in tests!
        None() = default;

        //! Destructor.
        ~None() = default;

        //! \copydoc doxygen_hide_copy_constructor
        None(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        None(None&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        None& operator=(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        None& operator=(None&& rhs) = delete;


        ///@}

        //! Convenient method in templates; by nature returns 0UL.
        static constexpr std::size_t NtimeModified();

        //! Tells no time evolution for this policy.
        static constexpr bool IsTimeEvolving() noexcept;
    };


} // namespace MoReFEM::TimeManagerNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/Policy/Evolution/None.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_NONE_DOT_HPP_
// *** MoReFEM end header guards *** < //
