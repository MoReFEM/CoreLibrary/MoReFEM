### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/ConstantTimeStep.hpp
		${CMAKE_CURRENT_LIST_DIR}/ConstantTimeStep.hxx
		${CMAKE_CURRENT_LIST_DIR}/None.hpp
		${CMAKE_CURRENT_LIST_DIR}/None.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/Static.hpp
		${CMAKE_CURRENT_LIST_DIR}/Static.hxx
		${CMAKE_CURRENT_LIST_DIR}/VariableTimeStep.hpp
		${CMAKE_CURRENT_LIST_DIR}/VariableTimeStep.hxx
)

include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
