// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_STATIC_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_STATIC_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/Policy/Evolution/Static.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/InputData/Concept.hpp"


namespace MoReFEM::TimeManagerNS::Policy
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    Static::Static(const Wrappers::Mpi&, const ModelSettingsT&, const InputDataT&)
    { }


    inline constexpr bool Static::IsTimeStepConstant()
    {
        return true;
    }


    inline constexpr bool Static::IsTimeEvolving() noexcept
    {
        return false;
    }


} // namespace MoReFEM::TimeManagerNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_STATIC_DOT_HXX_
// *** MoReFEM end header guards *** < //
