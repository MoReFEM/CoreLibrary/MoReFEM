// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_VARIABLETIMESTEP_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_VARIABLETIMESTEP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <limits>
#include <string>

#include "Utilities/InputData/Concept.hpp"

// IWYU pragma: begin_exports
#include "Core/TimeManager/Concept.hpp"
#include "Core/TimeManager/Policy/Evolution/Crtp/Restart.hpp"
#include "Core/TimeManager/Policy/Evolution/Crtp/TimeStep.hpp"
#include "Core/TimeManager/Policy/Evolution/Internal/NoRestart.hpp"
#include "Core/TimeManager/TimeAccess/TimeAccess.hpp"
// IWYU pragma: end_exports


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class adapt_time_step; }
namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{


    /*!
     * \brief Policy to use when time step may be adapted
     *
     * This typically occurs in models in which we want to try again with a smaller time step if the solver doesn't
     * converge.
     *
     * \copydoc doxygen_hide_support_restart_mode_template_arg
     */

    // clang-format off
    template<support_restart_mode DoSupportRestartModeT = support_restart_mode::no>
    class VariableTimeStep
    : public TimeAccess,
    public Crtp::TimeStep<VariableTimeStep<DoSupportRestartModeT>, mode::modifiable>,
    public std::conditional_t
    <
        DoSupportRestartModeT == support_restart_mode::yes,
        Crtp::Restart<VariableTimeStep<DoSupportRestartModeT>>,
        ::MoReFEM::Internal::TimeManagerNS::NoRestart
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = VariableTimeStep<DoSupportRestartModeT>;

        //! Alias to \a TimeAccess object.
        using time_access_parent = TimeManagerNS::TimeAccess;

        static_assert(std::is_convertible<self*, time_access_parent*>());

        //! Convenient alias.
        static inline std::string ClassName = "VariableTimeStep";

        //! \copydoc doxygen_hide_concept_helper_is_time_manager_time_step_policy
        static inline constexpr bool ConceptIsTimeManagerEvolutionPolicy = true;

        //! \copydoc doxygen_hide_concept_helper_time_manager_support_result_directory
        static inline constexpr bool ConceptSupportResultDirectory = true;

        //! \copydoc doxygen_hide_support_restart_mode_static_attribute
        static inline constexpr support_restart_mode do_support_restart_mode = DoSupportRestartModeT;

        //! Alias to Crtp that stores and provides access to time step.
        using time_step_parent = Crtp::TimeStep<self, mode::modifiable>;

        static_assert(std::is_convertible<self*, time_step_parent*>());

        //! Alias to restart parent.
        using restart_parent = std::conditional_t<DoSupportRestartModeT == support_restart_mode::yes,
                                                  Crtp::Restart<self>,
                                                  ::MoReFEM::Internal::TimeManagerNS::NoRestart>;

        static_assert(std::is_convertible<self*, restart_parent*>());

        //! Friendship to allow this parent to call protected members we don't want to expose
        //! completely in \a TimeManager interface.
        friend restart_parent;


      protected:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydetails doxygen_hide_mpi_param
         */
        template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT, ::MoReFEM::Concept::InputDataType InputDataT>
        VariableTimeStep(const Wrappers::Mpi& mpi, const ModelSettingsT& model_settings, const InputDataT& input_data);

        //! Destructor.
        ~VariableTimeStep() = default;

        //! \copydoc doxygen_hide_copy_constructor
        VariableTimeStep(const VariableTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VariableTimeStep(VariableTimeStep&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariableTimeStep& operator=(const VariableTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariableTimeStep& operator=(VariableTimeStep&& rhs) = delete;

        ///@}

      public:
        //! Constant accessor on maximum time step.
        double GetMaximumTimeStep() const noexcept;

      public:
        /*!
         * \brief Increment the time.
         *
         * Time is incremented with current time step. Tn+1 = Tn + dt.
         * Current time step is stored in the class.
         *
         */
        void IncrementTime();

        /*!
         * \brief Decrement the time.
         *
         * Time is decremented with current time step. Tn+1 = Tn - dt.
         * Current time step is stored in the class.
         *
         */
        void DecrementTime();

        //! Returns true if current time is equal or beyond maximum time.
        bool HasFinished() const;

        //! Returns whether the time step is constant (obviously false for this class!).
        static constexpr bool IsTimeStepConstant() noexcept;

        //! Returns whether there is a time evolution at all.
        static constexpr bool IsTimeEvolving() noexcept;

        //! Get the maximum time (in seconds).
        double GetMaximumTime() const;

        /*!
         * \brief Adapt the time step for the simulation.
         *
         * This method should be called if needed in your Model's FinalizeStep() method (typically when a Newton
         * didn't converge and you want to retry with a smaller step).
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \param[in] a_adapt_time_step \a adapt_time_step parameter, either increasing
         * or decreasing.
         *
         */
        void AdaptTimeStep(const Wrappers::Mpi& mpi, adapt_time_step a_adapt_time_step);


        //! Constant accessor on minimum time step.
        double GetMinimumTimeStep() const noexcept;

        //!\copydoc doxygen_hide_time_manager_set_restart

        void SetRestartForEvolutionPolicy(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data)
            requires(DoSupportRestartModeT == support_restart_mode::yes);

        //! \copydoc doxygen_hide_time_manager_reset_time_manager_at_initial_time
        void ResetTimeManagerAtInitialTime();


      private:
        //! Maximum time.
        double maximum_time_ = std::numeric_limits<double>::lowest();

        //! Minimum time step.
        double minimum_time_step_ = std::numeric_limits<double>::lowest();

        //! Maximum time step ie the time step given by the user at the beginning.
        double maximum_time_step_ = std::numeric_limits<double>::lowest();

        //! Initial time.
        double initial_time_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::TimeManagerNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/Policy/Evolution/VariableTimeStep.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_VARIABLETIMESTEP_DOT_HPP_
// *** MoReFEM end header guards *** < //
