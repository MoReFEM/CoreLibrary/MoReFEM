// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_VARIABLETIMESTEP_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_VARIABLETIMESTEP_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/Policy/Evolution/VariableTimeStep.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <limits>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/TimeManager/TimeAccess/TimeAccess.hpp"


namespace MoReFEM::TimeManagerNS::Policy
{


    template<support_restart_mode DoSupportRestartModeT>
    template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT, ::MoReFEM::Concept::InputDataType InputDataT>
    VariableTimeStep<DoSupportRestartModeT>::VariableTimeStep(const Wrappers::Mpi& mpi,
                                                              const ModelSettingsT& model_settings,
                                                              const InputDataT& input_data)
    : time_step_parent{ InputDataNS::ExtractLeaf<InputDataNS::TimeManager::TimeStep>(model_settings, input_data) },
      restart_parent(mpi, model_settings, input_data)
    {
        using TimeManager = InputDataNS::TimeManager;

        maximum_time_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeMax>(model_settings, input_data);
        minimum_time_step_ =
            ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::MinimumTimeStep>(model_settings, input_data);
        maximum_time_step_ = time_step_parent::GetTimeStep();
        initial_time_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeInit>(model_settings, input_data);

        assert(time_step_parent::GetTimeStep() > 1.e-9
               && "Should have been already checked when reading the input data "
                  "hence the assert rather than an exception.");
    }


    template<support_restart_mode DoSupportRestartModeT>
    void VariableTimeStep<DoSupportRestartModeT>::AdaptTimeStep(const Wrappers::Mpi& mpi,
                                                                adapt_time_step a_adapt_time_step)
    {
        auto& time_step = time_step_parent::GetNonCstTimeStep();
        auto minimum_time_step = GetMinimumTimeStep();

        decltype(auto) time = time_access_parent::GetTime();

        switch (a_adapt_time_step)
        {
        case adapt_time_step::decreasing:
        {
            assert(time_step + NumericNS::DefaultEpsilon<double>() >= minimum_time_step
                   && "I have not written the code here, but it was clearly an assumption...");

            if (NumericNS::AreEqual(time_step, minimum_time_step))
                throw ExceptionNS::TimeManagerNS::TimeStepAdaptationMinimumReached(minimum_time_step);

            if (time_step > minimum_time_step)
            {
                time_step = std::max(0.5 * time_step, minimum_time_step);

                if (mpi.IsRootProcessor())
                    std::cout << std::endl << "Time step subiteration with time step " << time_step << "." << std::endl;
            }

            break;
        }
        case adapt_time_step::increasing:
        {
            const auto maximum_time_step = GetMaximumTimeStep();

            const double modulus = time - maximum_time_step * std::floor(time / maximum_time_step);

            bool condition_max_time_step = ((maximum_time_step - modulus) < minimum_time_step);
            condition_max_time_step |=
                NumericNS::AreEqual(maximum_time_step - modulus, minimum_time_step, 0.5 * minimum_time_step);

            const double new_time_step =
                condition_max_time_step ? maximum_time_step : std::min(maximum_time_step - modulus, maximum_time_step);


            if (new_time_step >= time_step)
            {
                time_step = new_time_step;

                if (mpi.IsRootProcessor())
                    std::cout << std::endl
                              << "Time step re-adaptation with time step " << new_time_step << "." << std::endl;
            } else
            {
                // \todo #1753 proper error handling required here!
                std::cerr << new_time_step << " is lower than previous one " << time_step << std::endl;
                assert(NumericNS::AreEqual(time_step, maximum_time_step)
                       && "Only configuration in which the test should happen!");
            }
            break;
        }
        }
    }


    template<support_restart_mode DoSupportRestartModeT>
    void VariableTimeStep<DoSupportRestartModeT>::SetRestartForEvolutionPolicy(
        [[maybe_unused]] const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data)
        requires(DoSupportRestartModeT == support_restart_mode::yes)
    {
        // \todo Nothing done yet; see it along with #1763 but I would say it can be left empty.
    }


    template<support_restart_mode DoSupportRestartModeT>
    void VariableTimeStep<DoSupportRestartModeT>::ResetTimeManagerAtInitialTime()
    {
        GetNonCstTime() = initial_time_;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline void VariableTimeStep<DoSupportRestartModeT>::IncrementTime()
    {
        decltype(auto) time = time_access_parent::GetNonCstTime();
        time_access_parent::SetStaticOrDynamic(StaticOrDynamic::dynamic_);
        time += time_step_parent::GetTimeStep();
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline void VariableTimeStep<DoSupportRestartModeT>::DecrementTime()
    {
        decltype(auto) time = time_access_parent::GetNonCstTime();
        time -= time_step_parent::GetTimeStep();
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline double VariableTimeStep<DoSupportRestartModeT>::GetMaximumTime() const
    {
        assert(!NumericNS::AreEqual(maximum_time_, std::numeric_limits<double>::lowest()));
        return maximum_time_;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline bool VariableTimeStep<DoSupportRestartModeT>::HasFinished() const
    {
        decltype(auto) time = time_access_parent::GetTime();
        return ((time > GetMaximumTime()) || NumericNS::AreEqual(time, GetMaximumTime(), 0.5 * GetMinimumTimeStep()));
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline constexpr bool VariableTimeStep<DoSupportRestartModeT>::IsTimeEvolving() noexcept
    {
        return true;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline constexpr bool VariableTimeStep<DoSupportRestartModeT>::IsTimeStepConstant() noexcept
    {
        return false;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline double VariableTimeStep<DoSupportRestartModeT>::GetMaximumTimeStep() const noexcept
    {
        return maximum_time_step_;
    }


    template<support_restart_mode DoSupportRestartModeT>
    inline double VariableTimeStep<DoSupportRestartModeT>::GetMinimumTimeStep() const noexcept
    {
        return minimum_time_step_;
    }


} // namespace MoReFEM::TimeManagerNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_VARIABLETIMESTEP_DOT_HXX_
// *** MoReFEM end header guards *** < //
