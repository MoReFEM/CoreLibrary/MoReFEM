// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>

#include "Core/TimeManager/TimeAccess/TimeAccess.hpp"


namespace MoReFEM::TimeManagerNS
{


    void TimeAccess::SetInitialTime(double time)
    {
        assert(NtimeModified() == 0UL);
        time_ = time;
    }


    void TimeAccess::SetNtimeModifiedForRestart(std::size_t value)
    {
        assert(NtimeModified() == 0UL);
        Ntime_modified_ = value;
    }


    void TimeAccess::SetStaticOrDynamic(StaticOrDynamic value)
    {
        static_or_dynamic_ = value;
    }


    StaticOrDynamic TimeAccess::GetStaticOrDynamic() const
    {
        return static_or_dynamic_;
    }


} // namespace MoReFEM::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
