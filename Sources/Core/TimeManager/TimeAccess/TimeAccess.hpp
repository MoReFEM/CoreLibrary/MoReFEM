// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_TIMEACCESS_TIMEACCESS_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_TIMEACCESS_TIMEACCESS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>

#include "Core/TimeManager/Enum.hpp"            // IWYU pragma: export
#include "Core/TimeManager/StaticOrDynamic.hpp" // IWYU pragma: export


namespace MoReFEM::TimeManagerNS
{


    /*!
     * \brief Internal class in charge of handling the access to the time data.
     *
     * To handle properly outputs and avoid overwriting them, we need to ensure that each time the time is modified,
     * there is an internal index that is increased and that will be used to tag outputs. This index is increased each
     * time `GetNonCstTime()` is called - even if in the end the developer does nothing to modify the time
     * with this call. The evolution policies should all inherit from current class - they need access to this data
     * and it's better to do so without explicitly making them call their `TimeManager` derived class as an
     * argument.
     */
    class TimeAccess
    {
      public:
        //! Convenient alias.
        using self = TimeAccess;

      public:
        /// \name Canonical methods for class
        ///@{

        //! Defaut constructor, to use only in tests!
        TimeAccess() = default;

        //! Destructor.
        ~TimeAccess() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TimeAccess(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeAccess(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        self& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}

        //! Get the current time (in seconds).
        double GetTime() const;

        /*!
         * \brief Number of times the time has been modified.
         *
         * It doesn't necessarily mean an increasement of time: in data assimilation it might happen we go backward.
         * It is just an index that is used to tag the output files; for instance a file which tag 45 is the 46th
         * (possible) generated file of the same nature (possible as we might imagine some of the steps might not be
         * written on disk).
         *
         * \return Index that tags the current time iteration.
         */
        std::size_t NtimeModified() const;

        //! Returns whether the system is currently static or dynamic.
        StaticOrDynamic GetStaticOrDynamic() const;


      protected:
        //! Current time (in seconds).
        //! Calling this method also increment \a Ntime_modified_, ensuring there won't be any overwriting in output
        //! data.
        double& GetNonCstTime();

        /*!
         * \brief Increment the number of times the time has been modified.
         *
         * Due to the nature of the data attribute tracked, it is the only allowed operations on it.
         */
        void IncrementNtimeModified();

        /*!
         * \brief Set initial time.
         *
         * \attention Ntime_modified_ must be zero when this is called!
         *
         * \param[in] time Value to set.
         */
        void SetInitialTime(double time);


        /*!
         * \brief Set Ntime_modified_
         *
         * \attention Should be used only for restart mode, and it is expected Ntime_modified fo be zero when
         * this function is called (checked by an assert)
         *
         * \param[in] value Value to set.
         */
        void SetNtimeModifiedForRestart(std::size_t value);

        /*!
         * \brief Set whether the system is static or dynamic.
         *
         * \internal <b><tt>[internal]</tt></b> Only a Model should be able to call this method.
         * \endinternal
         *
         * \param[in] value Value to assign.
         */
        void SetStaticOrDynamic(StaticOrDynamic value);


      private:
        //! Current time (in seconds).
        //! \internal \attention Except in the class constructor, please <strong>never</strong> call directly this attribute
        //! (this is true in all of MoReFEM, but even more so here). If you need the value, please call `GetTime()`; if
        //! you need to modify it please call `GetNonCstTime()`.
        double time_{};

        /*!
         * \brief Number of times the time has been modified.
         *
         * It doesn't necessarily mean an increasement of time: in data assimilation it might happen we go backward.
         * It is just an index that is used to tag the output files; for instance a file which tag 45 is the 46th
         * (possible) generated file of the same nature (possible as we might imagine some of the steps might not be
         * written on disk).
         */
        std::size_t Ntime_modified_{ 0UL };


        //! Whether the system is currently running static or dynamic case.
        StaticOrDynamic static_or_dynamic_ = StaticOrDynamic::static_;
    };


} // namespace MoReFEM::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/TimeAccess/TimeAccess.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_TIMEACCESS_TIMEACCESS_DOT_HPP_
// *** MoReFEM end header guards *** < //
