// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_TIMEACCESS_TIMEACCESS_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_TIMEACCESS_TIMEACCESS_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/TimeAccess/TimeAccess.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef>


namespace MoReFEM::TimeManagerNS
{


    inline std::size_t TimeAccess::NtimeModified() const
    {
        return Ntime_modified_;
    }


    inline double TimeAccess::GetTime() const
    {
        return time_;
    }


    inline double& TimeAccess::GetNonCstTime()
    {
        IncrementNtimeModified();
        return time_;
    }


    inline void TimeAccess::IncrementNtimeModified()
    {
        ++Ntime_modified_;
    }


} // namespace MoReFEM::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_TIMEACCESS_TIMEACCESS_DOT_HXX_
// *** MoReFEM end header guards *** < //
