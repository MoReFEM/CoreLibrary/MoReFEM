// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_TIMEMANAGER_DOT_HPP_
#define MOREFEM_CORE_TIMEMANAGER_TIMEMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <optional>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/InputData.hpp" // IWYU pragma: keep

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: keep // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/TimeManager/Concept.hpp"
#include "Core/TimeManager/Enum.hpp"
#include "Core/TimeManager/TimeAccess/TimeAccess.hpp"
// IWYU pragma: end_exports


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Class in charge of managing the elapsing of time within the simulation.
     *
     * \internal <b><tt>[internal]</tt></b> Must not be confused with TimeKeep() which is a chronometer to evaluate
     * efficiency of the code.
     * \endinternal
     *
     * Restart mode may be enabled only if \a EvolutionPolicyT declares a method with interface
     * `SetRestartForEvolutionPolicy(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data);`
     *
     */
    template<Concept::TimeManagerTimeStepPolicy EvolutionPolicyT>
    class TimeManager : public EvolutionPolicyT
    {
      public:
        //! Convenient alias.
        using self = TimeManager<EvolutionPolicyT>;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<TimeManager>;

        //! Alias to the template argument used to define how happens time evolution.
        using evolution_policy_parent = EvolutionPolicyT;

        //! Helper variable to define the \a MoReFEM::Advanced::Concept::InputDataNS concept.
        static inline constexpr bool ConceptIsTimeManager = true;

      public:
        /// \name Canonical methods for class
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \param[in] the_program_type Whether a model, a test, a post-processing program is run.
         *
         * \param[in] result_directory Result directory into which all outputs will be written. May be left
         * undefined (for tests)
         *
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        TimeManager(const Wrappers::Mpi& mpi,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data,
                    program_type the_program_type,
                    std::optional<std::reference_wrapper<const FilesystemNS::Directory>> result_directory);

        //! Defaut constructor, to use only in tests!
        TimeManager() = default;

        //! Destructor.
        ~TimeManager() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TimeManager(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeManager(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        self& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}

        //! Path to the time iteration file in which the files written are recorded with their time index.
        const FilesystemNS::File& GetTimeIterationFile() const noexcept;

      private:
        //! Path to the time iteration file in which the files written are recorded with their time index.
        FilesystemNS::File time_iteration_file_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/TimeManager/TimeManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_TIMEMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
