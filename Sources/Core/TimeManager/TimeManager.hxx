// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_TIMEMANAGER_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_TIMEMANAGER_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/TimeManager.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <optional>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class StaticOrDynamic; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<Concept::TimeManagerTimeStepPolicy EvolutionPolicyT>
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    TimeManager<EvolutionPolicyT>::TimeManager(
        const Wrappers::Mpi& mpi,
        const ModelSettingsT& model_settings,
        const InputDataT& input_data,
        program_type the_program_type,
        std::optional<std::reference_wrapper<const FilesystemNS::Directory>> result_directory)
    : evolution_policy_parent(mpi, model_settings, input_data)
    {
        using time_manager_input_data_type = InputDataNS::TimeManager;

        if constexpr (requires { EvolutionPolicyT::ConceptSupportResultDirectory; })
        {
            switch (the_program_type)
            {
            case program_type::model:
            case program_type::test:
            {
                assert(result_directory.has_value()
                       && "result_directory argument may be optional but only for time "
                          "manager policies that don't require it (currently only 'None').");
                time_iteration_file_ = result_directory.value().get().AddFile("time_iteration.hhdata");

                if (time_iteration_file_.DoExist())
                {
                    std::cerr
                        << "[WARNING] A file named " << time_iteration_file_
                        << " already existed; it has been removed "
                           "and recreated from scratch. Please consider using ${MOREFEM_START_TIME} in your result "
                           "directory path "
                           "to guarantee each run gets its outputs written in an empty directory."
                        << std::endl;

                    time_iteration_file_.Remove();
                }

                std::ofstream stream{ time_iteration_file_.NewContent() };
                stream << "# Time iteration; time; numbering subset id; filename" << std::endl;
                break;
            }
            case program_type::update_lua_file:
            case program_type::post_processing:
                break;
            }
        }


        // Field `TimeManager<EvolutionPolicyT>::TimeInit` may not be present for static models.
        if constexpr (InputDataNS::Find<time_manager_input_data_type::TimeInit, ModelSettingsT, InputDataT>())
            evolution_policy_parent::SetInitialTime(
                ::MoReFEM::InputDataNS::ExtractLeaf<time_manager_input_data_type::TimeInit>(model_settings,
                                                                                            input_data));
        else
            std::cout << "[WARNING] No TimeInit field in InputData tuple; initial time set to 0." << std::endl;
    }

    template<Concept::TimeManagerTimeStepPolicy EvolutionPolicyT>
    inline const FilesystemNS::File& TimeManager<EvolutionPolicyT>::GetTimeIterationFile() const noexcept
    {
        assert(time_iteration_file_.DoExist());
        return time_iteration_file_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_TIMEMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
