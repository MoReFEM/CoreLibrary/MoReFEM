// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITION_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITION_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <utility>
#include <vector>

#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "Geometry/StrongType.hpp"

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"
#include "FiniteElement/BoundaryConditions/Internal/DofStorage.hpp"
#include "FiniteElement/BoundaryConditions/UniqueId.hpp" // IWYU pragma: export
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class DirichletBoundaryConditionManager; }
namespace MoReFEM { class Domain; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class Unknown; }

namespace MoReFEM::Internal::FEltSpaceNS { struct ReduceToProcessorWise; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Choice of the method used to apply Dirichlet boundary conditions.
     *
     */
    enum class BoundaryConditionMethod { pseudo_elimination, penalization };


    /*!
     * \brief Clear the initial values of all essential boundary conditions.
     *
     * At the beginning of a model, boundary conditions are initialized with a value read in the input data
     * file. This value is retained for homogeneous boundary condition, and modified for other cases; whichever
     * the case the initial value is not needed anymore, as the storage is different in all cases. So it's possible
     * to spare (very few) memory by dropping now unused values.
     */
    void ClearAllBoundaryConditionInitialValueList();


    /*!
     * \brief Class in charge of Dirichlet boundary conditions.
     *
     * This class is still relatively close to its counterpart in Felisce; a move heavy refactoring is
     * not excluded at all.
     *
     */
    class DirichletBoundaryCondition final : public Crtp::UniqueId<DirichletBoundaryCondition,
                                                                   BoundaryConditionNS::unique_id,
                                                                   UniqueIdNS::AssignationMode::manual>
    {
      public:
        //! Shared smart pointer.
        using shared_ptr = std::shared_ptr<DirichletBoundaryCondition>;

        //! Vector of smart pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Friendship to GodOfDof, which is the only allowed to call some of the private methods.
        friend GodOfDof;

        //! Class name.
        static const std::string& ClassName();

        //! Friendship to the manager, that will erase some temporary data.
        friend void ClearAllBoundaryConditionInitialValueList();

        //! Friendship to Manager class.
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend DirichletBoundaryConditionManager;

        //! Friendship.
        friend Internal::FEltSpaceNS::ReduceToProcessorWise;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


      private:
        //! Alias to parent.
        using unique_id_parent = Crtp::
            UniqueId<DirichletBoundaryCondition, BoundaryConditionNS::unique_id, UniqueIdNS::AssignationMode::manual>;


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_boundary_condition_constructor_args
         */
        explicit DirichletBoundaryCondition(BoundaryConditionNS::unique_id unique_id,
                                            const Domain& domain,
                                            const Unknown& unknown,
                                            const std::vector<double>& value_per_component,
                                            const std::string& component,
                                            bool is_mutable);

      public:
        //! Destructor.
        ~DirichletBoundaryCondition() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DirichletBoundaryCondition(const DirichletBoundaryCondition& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        DirichletBoundaryCondition(DirichletBoundaryCondition&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DirichletBoundaryCondition& operator=(const DirichletBoundaryCondition& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DirichletBoundaryCondition& operator=(DirichletBoundaryCondition&& rhs) = delete;

        ///@}


        //! Unknown considered in the boundary condition.
        const Unknown& GetUnknown() const noexcept;

        //! Domain upon which the condition is applied.
        const Domain& GetDomain() const noexcept;

        //! Access to the list of Dofs encompassed by the Dirichlet boundary condition.
        const Dof::vector_shared_ptr& GetDofList() const noexcept;

        /*!
         * \brief Whether a given numbering subset is relevant for the boundary condition.
         *
         * The relevancy is here program-wise: it might return true even if no dofs are managed by the current
         * processor.
         *
         * \param[in] numbering_subset Numbering subset considered.
         *
         * \return Whether boundary condition is related to \a numbering_subset.
         */
        bool IsNumberingSubset(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Replace the values to apply on dofs by the ones given in \a new_values.
         *
         * \param[in] new_values Global vector from which new boundaruy condition values should be extracted.
         *
         * \attention All numbering subsets are updated if they share some of the dofs involved.
         */
        void UpdateValues(const GlobalVector& new_values);

        /*!
         * \brief Replace the values to apply on dofs by the ones given in \a new_values for \a numbering subset.
         *
         * \copydetails UpdateValues()
         *
         * \param[in] numbering_subset Numbering subset considered.
         *
         */
        void UpdateValues(const NumberingSubset& numbering_subset, std::vector<PetscScalar>&& new_values);


      private:
        //! \name Methods to be called by friend GodOfDof.
        ///@{

        /*!
         * \brief Keep only the dofs that are related to current processor (i.e. either processor-wise or ghost).
         *
         * Should be called only by GodOfDof.
         * \param[in] god_of_dof God of dof that possess the dofs considered in the boundary condition.
         */
        void ShrinkToProcessorWise(const GodOfDof& god_of_dof);


        /*!
         * \brief Fill properly the storage for a given \a numbering_subset.
         *
         * Should be called only by GodOfDof.
         *
         * \param[in] numbering_subset NumberingSubset considered.
         * \param[in] dof_list Dof list to associate to \a numbering_subset.
         */
        void SetDofListForNumberingSubset(const NumberingSubset& numbering_subset, Dof::vector_shared_ptr&& dof_list);

        /*!
         * \brief Tell a numbering subset is relevant for the boundary condition, even if not present processor-wisely.
         *
         * Should be called only by GodOfDof.
         * \param[in] numbering_subset NumberingSubset to consider.
         */
        void ConsiderNumberingSubset(const NumberingSubset& numbering_subset);

        /*!
         * \brief Compute the list of dofs and their associated values.
         *
         * Should be called only by GodOfDof.
         */
        void ComputeDofList();


        /*!
         * \brief Access to component manager.
         *
         * Should be called only by GodOfDof.
         *
         * \return Component manager.
         */
        const Internal::BoundaryConditionNS::ComponentManager& GetComponentManager() const noexcept;


        /*!
         * \brief Return the value of the requested boundary condition.
         *
         * \param[in] component  Index of the active component considered. If for instance Comp13, component = 0
         * will yield x and component = 1 will yield z.
         *
         * \return Value of the boundary condition for \a component -th component.
         */
        double GetValueForComponent(::MoReFEM::GeometryNS::dimension_type component) const;


        ///@}

      private:
        /*!
         * \brief Reduce: keep only \a NodeBearer that are either processor-wise or ghost.
         *
         * \param[in] processor_wise_node_bearer_in_god_of_dof List of processor-wise \a NodeBearer in the \a GodOfDof for which
         * current method is called.
         * \param[in] ghost_node_bearer_in_god_of_dof List of ghost \a NodeBearer in the \a GodOfDof
         * for which current method is called.
         */
        void Reduce(const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_in_god_of_dof,
                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_in_god_of_dof);


        /*!
         * \brief Set the list of \a NodeBearer
         *
         * \param[in] node_bearer_list The list of \a NodeBearer considered.
         *
         * \return The \a NodeBearer list.
         *
         * \internal The return value is the same content as the argument: it is a trick so that rvalue ay be used to
         * initialise the list.
         *
         */
        const NodeBearer::vector_shared_ptr& SetNodeBearerList(NodeBearer::vector_shared_ptr&& node_bearer_list);

        /*!
         * \brief Get the list of  \a NodeBearer.
         */
        const NodeBearer::vector_shared_ptr& GetNodeBearerList() const noexcept;

        /*!
         * \brief Get the list of  \a NodeBearer.
         */
        NodeBearer::vector_shared_ptr& GetNonCstNodeBearerList() noexcept;

        //! Non constant access to the list of Dofs encompassed by the Dirichlet boundary condition.
        Dof::vector_shared_ptr& GetNonCstDofList() noexcept;

        //! \accessor{Storage of dof for all numbering subsets}.
        const std::vector<std::pair<NumberingSubsetNS::unique_id, Internal::BoundaryConditionNS::DofStorage>>&
        GetDofStorage() const noexcept;

        //! \non_cst_accessor{Storage of dof for all numbering subsets}.
        std::vector<std::pair<NumberingSubsetNS::unique_id, Internal::BoundaryConditionNS::DofStorage>>&
        GetNonCstDofStorage() noexcept;

        //! Access to the storage of dof for a given numbering subset.
        //! \param[in] numbering_subset \a NumberingSubset used as filter.
        const Internal::BoundaryConditionNS::DofStorage&
        GetDofStorage(const NumberingSubset& numbering_subset) const noexcept;

        //! Non constant access to the storage of dof for a given numbering subset.
        //! \param[in] numbering_subset \a NumberingSubset used as filter.
        Internal::BoundaryConditionNS::DofStorage&
        GetNonCstDofStorage(const NumberingSubset& numbering_subset) noexcept;

        //! Accessor to the dof and their associated values.
        const std::vector<std::pair<Dof::shared_ptr, double>>& GetInitialDofValueList() const noexcept;

        //! Non constant accessor to the dof and their associated values.
        std::vector<std::pair<Dof::shared_ptr, double>>& GetNonCstInitialDofValueList() noexcept;

        //! Clear the initial values storage.
        void ClearInitialDofValueList();

      private:
        //! Domain upon which the condition is applied.
        const Domain& domain_;

        //! variable concerned by this DirichletBoundaryCondition
        const Unknown& unknown_;

        //! Object which can tell which components are encompassed by the boundary condition.
        Internal::BoundaryConditionNS::ComponentManager::const_shared_ptr component_manager_ = nullptr;

        /*!
         * \brief Initial values associated to each dof.
         *
         * \internal <b><tt>[internal]</tt></b> When this data is created, dofs aren't yet given their program-wise
         * index, so both std::map and std::unordered_map would be very unreliable here.
         * \endinternal
         *
         * All dofs are considered here, regardless of their numbering subset.
         * \todo #619 This container should disappear: if dofs are generated after the reduction to processor-wise
         * the initial values can be retrieved in the same time and current container could hence be avoided.
         *
         * \attention This container is cleared once it has been used!
         */
        std::vector<std::pair<Dof::shared_ptr, double>> initial_dof_value_list_;

        //! List of all \a NodeBearer  encompassed by the boundary condition.
        NodeBearer::vector_shared_ptr node_bearer_list_;

        //! List of all dofs encompassed by the boundary condition.
        Dof::vector_shared_ptr dof_list_;

        /*!
         * \brief Helpful vector when penalization is to be used, which stores the penalization value for each dof.
         *
         * This vector may be given directly to Petsc::Matrix::SetValues().
         */
        std::vector<double> penalization_array_;

        /*!
         * \brief For each numbering subset, store the relevant information about the dofs involved in the boundary
         * condition.
         *
         * Key is unique id of the numbering subset; value is the object that stored the information about the dofs.
         *
         * \attention The value might not include any dofs: the convention is that a boundary condition must get
         * a numbering subset key as soon as it is relevant. So if for instance a boundary condition encompasses n dofs
         * all stored on processor i, all processors will nonetheless get a pair in the present list, even if there
         * are no associated dofs.
         */
        std::vector<std::pair<NumberingSubsetNS::unique_id, Internal::BoundaryConditionNS::DofStorage>>
            dof_storage_per_numbering_subset_;


        //! Initial value per component (should not be stored past #619).
        std::vector<double> value_per_component_;

        //! Whether the dof list has been properly computed or not (through a call to \a ComputeDofList().
        bool is_dof_list_computed_ = false;


#ifndef NDEBUG
        //! Whether ClearInitialDofValueList() has already been called or not.
        bool is_cleared_ = false;

        //! Whether the values may vary over time.
        bool is_mutable_ = false;
#endif // NDEBUG
    };


    /*!
     * \brief Convert into the \a BoundaryCondition::unique_id strong type the enum value defined in the input data file.
     *
     * In a typical Model, indexes related to fields (e.g. DirichletBoundaryCondition) are defined in an enum class
     * named typically \a BoundaryConditionIndex. This facility is a shortcut to convert this enum into a proper \a
     * BoundaryCondition::unique_id that is used in the library to identify a \a BoundaryCondition.
     *
     * \param[in] enum_value The index related to the \a BoundaryCondition, stored in an enum class. See a typical 'InputData.hpp' file
     * to understand it better.
     *
     * \return The identifier for a \a BoundaryCondition as used in the library.
     */
    template<class EnumT>
    constexpr BoundaryConditionNS::unique_id AsBoundaryConditionId(EnumT enum_value);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITION_DOT_HPP_
// *** MoReFEM end header guards *** < //
