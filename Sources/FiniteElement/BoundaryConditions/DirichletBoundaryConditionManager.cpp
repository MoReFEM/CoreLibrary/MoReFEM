// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <source_location>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp" // IWYU pragma: associated

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/BoundaryConditions/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    DirichletBoundaryConditionManager::~DirichletBoundaryConditionManager() = default;


    const std::string& DirichletBoundaryConditionManager::ClassName()
    {
        static const std::string ret("DirichletBoundaryConditionManager");
        return ret;
    }


    void DirichletBoundaryConditionManager ::RegisterDirichletBoundaryCondition(
        const DirichletBoundaryCondition::shared_ptr& boundary_condition_ptr)
    {
        assert(!(!boundary_condition_ptr));

        const auto& boundary_condition_list = GetList();

        const auto& unique_id = boundary_condition_ptr->GetUniqueId();

        if (std::ranges::find_if(boundary_condition_list,

                                 [unique_id](const auto& boundary_condition_in_list_ptr)
                                 {
                                     assert(!(!boundary_condition_in_list_ptr));
                                     return boundary_condition_in_list_ptr->GetUniqueId() == unique_id;
                                 })
            != boundary_condition_list.cend())
            throw Exception("Two different boundary_conditions can't share the same unique id (namely "
                            + std::to_string(unique_id.Get()) + ")");

        boundary_condition_list_.push_back(boundary_condition_ptr);
    }


    DirichletBoundaryCondition::shared_ptr
    DirichletBoundaryConditionManager ::GetDirichletBoundaryConditionPtr(BoundaryConditionNS::unique_id unique_id,
                                                                         const std::source_location location) const
    {
        const auto& boundary_condition_list = GetList();

        auto it = std::ranges::find_if(boundary_condition_list,

                                       [unique_id](const DirichletBoundaryCondition::shared_ptr& boundary_condition_ptr)
                                       {
                                           assert(!(!boundary_condition_ptr));
                                           return boundary_condition_ptr->GetUniqueId() == unique_id;
                                       });

        if (it == boundary_condition_list.cend())
            throw ExceptionNS::BoundaryConditionNS::UnknownBoundaryCondition(unique_id, location);

        return *it;
    }


    void DirichletBoundaryConditionManager::Create(BoundaryConditionNS::unique_id unique_id,
                                                   const Domain& domain,
                                                   const Unknown& unknown,
                                                   const std::vector<double>& value_per_component,
                                                   const std::string& component,
                                                   const bool is_mutable)
    {
        RegisterDirichletBoundaryCondition(Internal::WrapShared(
            new DirichletBoundaryCondition(unique_id, domain, unknown, value_per_component, component, is_mutable)));
    }


    void ClearAllBoundaryConditionInitialValueList()
    {
        const auto& manager = DirichletBoundaryConditionManager::GetInstance();

        const auto& list = manager.GetList();

        for (const auto& boundary_condition_ptr : list)
        {
            assert(!(!boundary_condition_ptr));
            boundary_condition_ptr->ClearInitialDofValueList();
        }
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
