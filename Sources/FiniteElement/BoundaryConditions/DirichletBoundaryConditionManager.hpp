// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITIONMANAGER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITIONMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp" // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/Internal/DirichletBoundaryCondition.hpp"

#include "Geometry/Domain/DomainManager.hpp" // IWYU pragma: keep

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp" // IWYU pragma: export
#include "FiniteElement/Unknown/UnknownManager.hpp"                        // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{

    /*!
     * \brief Singleton class which is aware of all essential boundary conditions that might be considered within
     * a model.
     *
     * These conditions are defined in the input data file.
     */
    class DirichletBoundaryConditionManager final : public Utilities::Singleton<DirichletBoundaryConditionManager>
    {

      public:
        //! Return the name of the class.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::BoundaryConditionNS::Tag;

      private:
        /// \name Special members
        ///@{

        //! Destructor.
        virtual ~DirichletBoundaryConditionManager() override;

        //! \copydoc doxygen_hide_copy_constructor
        DirichletBoundaryConditionManager(const DirichletBoundaryConditionManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        DirichletBoundaryConditionManager(DirichletBoundaryConditionManager&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DirichletBoundaryConditionManager& operator=(const DirichletBoundaryConditionManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DirichletBoundaryConditionManager& operator=(DirichletBoundaryConditionManager&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Create a \a DirichletBoundaryCondition object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);

        //! Returns the number of boundary conditions.
        std::size_t NboundaryCondition() const noexcept;

        //! Get the boundary condition associated with \a unique_id.
        //!
        //! \unique_id_param_in_accessor{DirichletBoundaryCondition}
        //!
        //! \copydoc doxygen_hide_source_location
        const DirichletBoundaryCondition&
        GetDirichletBoundaryCondition(BoundaryConditionNS::unique_id unique_id,
                                      const std::source_location location = std::source_location::current()) const;

        //! Get the boundary condition associated with \a unique_id.
        //!
        //! \unique_id_param_in_accessor{DirichletBoundaryCondition}
        //!
        //! \copydoc doxygen_hide_source_location
        DirichletBoundaryCondition&
        GetNonCstDirichletBoundaryCondition(BoundaryConditionNS::unique_id unique_id,
                                            const std::source_location location = std::source_location::current());

        //! Get the boundary condition associated with \a unique_id as a smart pointer.
        //!
        //! \unique_id_param_in_accessor{DirichletBoundaryCondition}
        //!
        //! \copydoc doxygen_hide_source_location
        DirichletBoundaryCondition::shared_ptr
        GetDirichletBoundaryConditionPtr(BoundaryConditionNS::unique_id unique_id,
                                         const std::source_location location = std::source_location::current()) const;

        //! Access to the list of boundary conditions.
        const DirichletBoundaryCondition::vector_shared_ptr& GetList() const noexcept;


      private:
        /*!
         *
         * \copydetails doxygen_hide_boundary_condition_constructor_args
         */
        void Create(BoundaryConditionNS::unique_id unique_id,
                    const Domain& domain,
                    const Unknown& unknown,
                    const std::vector<double>& value_per_component,
                    const std::string& component,
                    const bool is_mutable);

      private:
        /// \name Singleton requirements.
        ///@{

        //! Constructor.
        DirichletBoundaryConditionManager() = default;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<DirichletBoundaryConditionManager>;
        ///@}


      private:
        //! Register properly boundary condition in use.
        //! \param[in] ptr Shared pointernton the new boundary condition to register.
        void RegisterDirichletBoundaryCondition(const DirichletBoundaryCondition::shared_ptr& ptr);


      private:
        //! List of boundary conditions.
        DirichletBoundaryCondition::vector_shared_ptr boundary_condition_list_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITIONMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
