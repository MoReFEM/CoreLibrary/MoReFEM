// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITIONMANAGER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITIONMANAGER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Domain/UniqueId.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void DirichletBoundaryConditionManager::Create(const IndexedSectionDescriptionT&,
                                                   const ModelSettingsT& model_settings,
                                                   const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) domain_id =
            DomainNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::DomainIndex>(model_settings,
                                                                                                         input_data) };
        decltype(auto) unknown_name =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::UnknownName>(model_settings, input_data);
        decltype(auto) component =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Component>(model_settings, input_data);
        decltype(auto) value_per_component =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Values>(model_settings, input_data);
        decltype(auto) is_mutable =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::IsMutable>(model_settings, input_data);

        const auto& domain = DomainManager::GetInstance().GetDomain(domain_id);

        const auto& unknown = UnknownManager::GetInstance().GetUnknown(unknown_name);

        Create(BoundaryConditionNS::unique_id{ section_type::GetUniqueId() },
               domain,
               unknown,
               value_per_component,
               component,
               is_mutable);
    }


    inline std::size_t DirichletBoundaryConditionManager::NboundaryCondition() const noexcept
    {
        return boundary_condition_list_.size();
    }


    inline const DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetDirichletBoundaryCondition(BoundaryConditionNS::unique_id id,
                                                                      const std::source_location location) const
    {
        return *GetDirichletBoundaryConditionPtr(id, location);
    }


    inline DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetNonCstDirichletBoundaryCondition(BoundaryConditionNS::unique_id id,
                                                                            const std::source_location location)
    {
        return const_cast<DirichletBoundaryCondition&>(GetDirichletBoundaryCondition(id, location));
    }


    inline const DirichletBoundaryCondition::vector_shared_ptr&
    DirichletBoundaryConditionManager ::GetList() const noexcept
    {
        return boundary_condition_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_DIRICHLETBOUNDARYCONDITIONMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
