// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <sstream>
#include <string>

#include "FiniteElement/BoundaryConditions/Exceptions/Exception.hpp"

#include "FiniteElement/BoundaryConditions/UniqueId.hpp"


namespace // anonymous
{

    std::string UnknownBoundaryConditionMsg(const std::string& name);

    std::string UnknownBoundaryConditionMsg(MoReFEM::BoundaryConditionNS::unique_id id);


} // namespace


namespace MoReFEM::ExceptionNS::BoundaryConditionNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    UnknownBoundaryCondition::~UnknownBoundaryCondition() = default;


    UnknownBoundaryCondition::UnknownBoundaryCondition(const std::string& name, const std::source_location location)
    : Exception(UnknownBoundaryConditionMsg(name), location)
    { }


    UnknownBoundaryCondition::UnknownBoundaryCondition(::MoReFEM::BoundaryConditionNS::unique_id id,
                                                       const std::source_location location)
    : Exception(UnknownBoundaryConditionMsg(id), location)
    { }


} // namespace MoReFEM::ExceptionNS::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string UnknownBoundaryConditionMsg(const std::string& name)
    {
        std::ostringstream oconv;
        oconv << "Sought boundary condition which name is '" << name
              << "' was not found in the manager. "
                 "Please check the name provided in the Lua input file match the expected one in your model.";
        return oconv.str();
    }


    std::string UnknownBoundaryConditionMsg(MoReFEM::BoundaryConditionNS::unique_id id)
    {
        std::ostringstream oconv;
        oconv
            << "Sought boundary condition which id underlying numerical value is '" << id
            << "' was not found  "
               "in the manager. Please check the way the model is written (there is probably an issue in which the "
               "model "
               "was written but a mere assert would not convey enough informations - please contact the author of the "
               "model you're using if you need assistance).";
        return oconv.str();
    }


} // namespace
