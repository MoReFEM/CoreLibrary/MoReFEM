// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <iosfwd>
#include "FiniteElement/BoundaryConditions/Internal/Component/Comp2.hpp"

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"
#include "FiniteElement/BoundaryConditions/Internal/Component/FwdForCpp.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    namespace // anonymous
    {


        ComponentManager::const_shared_ptr Create()
        {
            return std::make_unique<Comp2>();
        }


        // Register the geometric element in the 'ComponentFactory' singleton.
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        // NOLINTBEGIN(cert-err58-cpp)
        // We accept here exception might occur and not be caught - it is both very unlikely
        // and would happen anyway before we enter the main() program if it happens nonetheless..
        [[maybe_unused]] const bool registered =
            Internal::BoundaryConditionNS::ComponentFactory::CreateOrGetInstance().Register<Comp2>(Create);
        // NOLINTEND(cert-err58-cpp)


    } // anonymous namespace


    Comp2::Comp2() = default;


    const std::string& Comp2::Name()
    {
        static const std::string ret("Comp2");
        return ret;
    }


    const std::bitset<3>& Comp2::IsActivated()
    {
        static const std::bitset<3> ret("010");
        return ret;
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
