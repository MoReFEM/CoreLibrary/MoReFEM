// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMP23_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMP23_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <bitset>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    /*!
     * \brief The class in charge of managing Comp23 behaviour.
     *
     * \attention Make sure not to define any data attribute in the class!
     * Mother class doesn't get a virtual destructor, and is used polymorphically, so we must make sure
     * not to add any data attribute that would induce memory leaks (it shouldn't be required anyway:
     * the derived classes are designed to be very lightweight and add no additional functionalities...)
     *
     */
    struct Comp23 final : public TComponentManager<Comp23>
    {

        /*!
         * \brief Constructor.
         */
        explicit Comp23();


        /*!
         * \copydoc doxygen_hide_component_static_name_method
         */
        static const std::string& Name();


        /*!
         * \copydoc doxygen_hide_component_static_is_activated_method
         */
        static const std::bitset<3>& IsActivated();
    };


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMP23_DOT_HPP_
// *** MoReFEM end header guards *** < //
