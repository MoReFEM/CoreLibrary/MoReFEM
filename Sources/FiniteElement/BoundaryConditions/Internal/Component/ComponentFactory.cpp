// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <functional>
#include <map>
#include <string>
#include <utility>

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp"

#include "Utilities/Exceptions/Factory.hpp"

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    ComponentFactory::~ComponentFactory() = default;


    const std::string& ComponentFactory::ClassName()
    {
        static const std::string ret("ComponentFactory");
        return ret;
    }


    ComponentFactory::ComponentFactory() = default;


    ComponentManager::const_shared_ptr ComponentFactory::CreateFromName(const std::string& component_name) const
    {
        auto it = callbacks_.find(component_name);

        if (it == callbacks_.cend())
            throw ExceptionNS::Factory::UnregisteredName(component_name, "component");

        ComponentManager::const_shared_ptr ret = it->second();

        return ret;
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
