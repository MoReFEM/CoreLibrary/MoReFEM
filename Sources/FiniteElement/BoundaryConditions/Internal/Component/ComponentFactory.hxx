// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTFACTORY_DOT_HXX_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTFACTORY_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Exceptions/Factory.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    template<class ComponentT>
    bool ComponentFactory::Register(FunctionPrototype callback)
    {
        if (!callbacks_.insert(make_pair(ComponentT::Name(), callback)).second)
        {
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(ComponentT::Name(), "component"));
        }

        return true;
    }


    inline ComponentFactory::CallBack::size_type ComponentFactory::Nvariable() const
    {
        return callbacks_.size();
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTFACTORY_DOT_HXX_
// *** MoReFEM end header guards *** < //
