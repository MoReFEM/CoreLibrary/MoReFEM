// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <bitset>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"

#include "Utilities/Containers/Print.hpp"

#include "Geometry/StrongType.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    ComponentManager::ComponentManager(const std::bitset<3>& is_activated) : is_activated_(is_activated)
    { }


    std::string ComponentManager::AsString() const
    {
        if (!is_activated_.any())
            return "NA";

        std::vector<char> buf;

        if (IsActive(0))
            buf.push_back('x');

        if (IsActive(1))
            buf.push_back('y');

        if (IsActive(2))
            buf.push_back('z');

        std::ostringstream oconv;

        Utilities::PrintContainer<>::Do(buf,
                                        oconv,
                                        ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                        ::MoReFEM::PrintNS::Delimiter::opener(""),
                                        ::MoReFEM::PrintNS::Delimiter::closer(""));
        return oconv.str();
    }


    ::MoReFEM::GeometryNS::dimension_type
    ComponentManager::ActiveComponent(::MoReFEM::GeometryNS::dimension_type i) const
    {
        std::size_t ret = 0;
        const auto index = static_cast<std::size_t>(i.Get());

        assert(index < is_activated_.count());

        for (std::size_t Nactive_read = 0; Nactive_read <= index; ++ret)
        {
            if (IsActive(ret))
                ++Nactive_read;
        }

        return ::MoReFEM::GeometryNS::dimension_type{ static_cast<Eigen::Index>(ret) - 1 };
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
