// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTMANAGER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <bitset>
#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>
#include <memory>

#include "Geometry/StrongType.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    /*!
     * \brief Helper class to handle which components are to be used in an essential boundary condition.
     *
     * This allows in particular to for instance consider only the second component of a vectorial unknown.
     *
     * \internal <b><tt>[internal]</tt></b> Management of components should probably be overhauled completely
     * at some point; current class is still heavily inspired by Felisce interface. However so far it worked
     * for all the cases we considered and so it's quite low in our current todo list.
     * \endinternal
     */
    class ComponentManager
    {
      public:
        //! Shared smart pointer.
        using const_shared_ptr = std::shared_ptr<const ComponentManager>;

        //! Constructor.
        //! \param[in] is_activated The mask telling which one(s) is/are active.
        explicit ComponentManager(const std::bitset<3>& is_activated);

        //! Destructor
        ~ComponentManager() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ComponentManager(const ComponentManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ComponentManager(ComponentManager&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ComponentManager& operator=(const ComponentManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ComponentManager& operator=(ComponentManager&& rhs) = delete;

        //! Returns whether the \a i -th component is active.
        //! \param[in] i Index of the component: 0 for x, 1 for y, 2 for z.
        bool IsActive(std::size_t i) const;

        //! Returns the number of active components.
        ::MoReFEM::GeometryNS::dimension_type Nactive() const;

        //! Return as a string the components available, separated by a comma (e.g. "x, y").
        std::string AsString() const;

        /*!
         * \brief Return the position of the \a i -th active component in the bitset (in C numbering).
         *
         *
         * For instance, if bitset = 101 (and remember bitsets are read from the right to the left!):
         * \code
         * ActiveComponent(0); // yields 0
         * ActiveComponent(1); // yields 2
         * ActiveComponent(2); // ERROR!
         * \endcode
         *
         * \param[in] i Index of the active component to consider; should be in [0, Nactive_component[.
         *
         * \return Which is the \a i -th active component.
         */
        ::MoReFEM::GeometryNS::dimension_type ActiveComponent(::MoReFEM::GeometryNS::dimension_type i) const;


      private:
        /*!
         * \brief Which component are activated.
         *
         * BEWARE: a bitset is read from the right to the left... So "100" means z component is active, not x
         * one.
         */
        std::bitset<3> is_activated_;
    };


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
