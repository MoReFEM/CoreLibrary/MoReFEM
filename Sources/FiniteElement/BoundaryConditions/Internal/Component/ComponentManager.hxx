// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTMANAGER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTMANAGER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"
// *** MoReFEM header guards *** < //


#include <bitset>
#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Geometry/StrongType.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    inline bool ComponentManager::IsActive(std::size_t i) const
    {
        assert(i < is_activated_.size());
        return is_activated_[i];
    }


    inline ::MoReFEM::GeometryNS::dimension_type ComponentManager::Nactive() const
    {
        return ::MoReFEM::GeometryNS::dimension_type{ static_cast<Eigen::Index>(is_activated_.count()) };
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
