// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_FWDFORCPP_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_FWDFORCPP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <bitset>     // IWYU pragma: export
#include <functional> // IWYU pragma: export
#include <memory>     // IWYU pragma: export
#include <string>     // IWYU pragma: export

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hpp"  // IWYU pragma: export
#include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hpp" // IWYU pragma: export

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_FWDFORCPP_DOT_HPP_
// *** MoReFEM end header guards *** < //
