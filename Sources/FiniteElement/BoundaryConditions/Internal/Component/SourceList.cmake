### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Comp1.cpp
		${CMAKE_CURRENT_LIST_DIR}/Comp12.cpp
		${CMAKE_CURRENT_LIST_DIR}/Comp123.cpp
		${CMAKE_CURRENT_LIST_DIR}/Comp13.cpp
		${CMAKE_CURRENT_LIST_DIR}/Comp2.cpp
		${CMAKE_CURRENT_LIST_DIR}/Comp23.cpp
		${CMAKE_CURRENT_LIST_DIR}/Comp3.cpp
		${CMAKE_CURRENT_LIST_DIR}/CompNA.cpp
		${CMAKE_CURRENT_LIST_DIR}/ComponentFactory.cpp
		${CMAKE_CURRENT_LIST_DIR}/ComponentManager.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Comp1.hpp
		${CMAKE_CURRENT_LIST_DIR}/Comp12.hpp
		${CMAKE_CURRENT_LIST_DIR}/Comp123.hpp
		${CMAKE_CURRENT_LIST_DIR}/Comp13.hpp
		${CMAKE_CURRENT_LIST_DIR}/Comp2.hpp
		${CMAKE_CURRENT_LIST_DIR}/Comp23.hpp
		${CMAKE_CURRENT_LIST_DIR}/Comp3.hpp
		${CMAKE_CURRENT_LIST_DIR}/CompNA.hpp
		${CMAKE_CURRENT_LIST_DIR}/Component.doxygen
		${CMAKE_CURRENT_LIST_DIR}/ComponentFactory.hpp
		${CMAKE_CURRENT_LIST_DIR}/ComponentFactory.hxx
		${CMAKE_CURRENT_LIST_DIR}/ComponentManager.hpp
		${CMAKE_CURRENT_LIST_DIR}/ComponentManager.hxx
		${CMAKE_CURRENT_LIST_DIR}/FwdForCpp.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/TComponentManager.hpp
		${CMAKE_CURRENT_LIST_DIR}/TComponentManager.hxx
)

