// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_TCOMPONENTMANAGER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_TCOMPONENTMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::BoundaryConditionNS
{


    /*!
     * \brief Each Component to be added to ComponentManager should derive from this class.
     *
     * This class is to be used as a CRTP:
     *
     * \code
     *  struct Comp12 final : public TComponentManager<Comp12>
     * \endcode
     *
     */
    template<class ComponentT>
    struct TComponentManager : public ComponentManager
    {

        //! Constructor.
        explicit TComponentManager();

        //! Destructor
        ~TComponentManager() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TComponentManager(const TComponentManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TComponentManager(TComponentManager&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TComponentManager& operator=(const TComponentManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TComponentManager& operator=(TComponentManager&& rhs) = delete;
    };


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_TCOMPONENTMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
