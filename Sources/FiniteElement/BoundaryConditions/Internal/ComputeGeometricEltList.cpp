// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <memory>
#include <tuple>

#include "FiniteElement/BoundaryConditions/Internal/ComputeGeometricEltList.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    GeometricElt::vector_shared_ptr ComputeGeometricEltList(const Mesh& mesh,
                                                            const DirichletBoundaryCondition& boundary_condition)
    {
        const auto& boundary_condition_domain = boundary_condition.GetDomain();

        using iterator_geometric_element = Mesh::iterator_geometric_element;
        iterator_geometric_element begin;
        iterator_geometric_element end;

        GeometricElt::vector_shared_ptr ret;

        for (auto dimension = mesh.GetDimension() - ::MoReFEM::GeometryNS::dimension_type{ 1 };
             dimension >= ::MoReFEM::GeometryNS::dimension_type{ 0 };
             --dimension)
        {
            // As this function is called before reduction; processor-wise encompass in fact all prgram-wise data...
            const auto& bag_of_boundary_domain = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>(dimension);

            for (const auto& geometric_elt_type_ptr : bag_of_boundary_domain)
            {
                const auto& geom_elt_type = *geometric_elt_type_ptr;

                if (!boundary_condition_domain.DoRefGeomEltMatchCriteria(geom_elt_type))
                    continue;

                std::tie(begin, end) = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(geom_elt_type);

                for (auto it = begin; it != end; ++it)
                {
                    const auto& geometric_elt_ptr = *it;
                    assert(!(!geometric_elt_ptr));
                    const auto& geom_elt = *geometric_elt_ptr;
                    assert(geom_elt.GetIdentifier() == geom_elt_type.GetIdentifier()
                           && "If not, GetSubsetGeometricEltList() above is buggy!");

                    if (!boundary_condition_domain.IsGeometricEltInside(geom_elt))
                        continue;

                    ret.push_back(geometric_elt_ptr);
                }
            }
        }

        return ret;
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
