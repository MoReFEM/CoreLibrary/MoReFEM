// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPUTEGEOMETRICELTLIST_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPUTEGEOMETRICELTLIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Geometry/GeometricElt/GeometricElt.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class DirichletBoundaryCondition; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::BoundaryConditionNS
{


    /*!
     * \brief Define all \a GeometricElt involved within the given \a boundary_condition.
     *
     * \internal This function is expected to be called on a standard vanilla parallel run BEFORE the reduction
     * occurred.
     *
     * \param[in] mesh \a Mesh for which the involved \a GeometricElt are sought.
     * \param[in] boundary_condition The \a DirichletBoundaryCondition which related \a GeometricElt we seek.
     *
     * \return List of all the relevant \a GeometricElt.
     *
     */
    GeometricElt::vector_shared_ptr ComputeGeometricEltList(const Mesh& mesh,
                                                            const DirichletBoundaryCondition& boundary_condition);


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

#include "FiniteElement/BoundaryConditions/Internal/ComputeGeometricEltList.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPUTEGEOMETRICELTLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
