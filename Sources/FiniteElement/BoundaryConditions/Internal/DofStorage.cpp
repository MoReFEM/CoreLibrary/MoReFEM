// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>     // IWYU pragma: keep
#include <vector>

#include "FiniteElement/BoundaryConditions/Internal/DofStorage.hpp"

#include "Utilities/Containers/PointerComparison.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::BoundaryConditionNS
{


    DofStorage ::DofStorage(const NumberingSubset& numbering_subset,
                            Utilities::PointerComparison::Map<Dof::shared_ptr, PetscScalar>&& value_per_dof)
    {
        const auto size = value_per_dof.size();
        program_wise_dof_index_list_.reserve(size);
        processor_wise_dof_index_list_.reserve(size);
        dof_value_list_.reserve(size);
        dof_list_.reserve(size);

        for (const auto& pair : value_per_dof)
        {
            const auto& dof_ptr = pair.first;
            assert(!(!dof_ptr));
            dof_list_.push_back(dof_ptr);

            program_wise_dof_index_list_.push_back(
                static_cast<PetscInt>(dof_ptr->GetProgramWiseIndex(numbering_subset).Get()));
            processor_wise_dof_index_list_.push_back(
                static_cast<PetscInt>(dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset).Get()));
            dof_value_list_.push_back(pair.second);
        }
    }


    void DofStorage::UpdateValueList(std::vector<PetscScalar>&& new_values)
    {
        assert(new_values.size() == dof_value_list_.size());
        std::swap(dof_value_list_, new_values);
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
