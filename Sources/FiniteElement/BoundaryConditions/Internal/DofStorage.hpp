// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_DOFSTORAGE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_DOFSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class DirichletBoundaryCondition; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::BoundaryConditionNS
{


    /*!
     * \brief Store there for a pair boundary condition/numbering subset the dofs involved in
     * the boundary condition and their associated values.
     */
    class DofStorage
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = DofStorage;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        /*!
         * \brief Friendship to the class that owns it.
         */
        friend DirichletBoundaryCondition;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] numbering_subset Numbering subset for which current object is built.
         * \param[in] value_per_dof Value of the boundary condition for each dof inside.
         */
        explicit DofStorage(const NumberingSubset& numbering_subset,
                            Utilities::PointerComparison::Map<Dof::shared_ptr, PetscScalar>&& value_per_dof);

        //! Default constructor, for cases in which numbering subset is relevant but no dofs are on current
        //! processor.
        DofStorage() = default;

        //! Destructor.
        ~DofStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DofStorage(const DofStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        DofStorage(DofStorage&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DofStorage& operator=(const DofStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DofStorage& operator=(DofStorage&& rhs) = delete;

        ///@}

        //! Get the list of all the dofs relevant for boundary condition and numbering subset.
        const Dof::vector_shared_ptr& GetDofList() const noexcept;

        //! Get the list of all dof program-wise index (within the numbering subset for which current class
        //! exists).
        const std::vector<PetscInt>& GetProgramWiseDofIndexList() const noexcept;

        //! Get the list of all dof processor-wise index (within the numbering subset for which current class
        //! exists).
        const std::vector<PetscInt>& GetProcessorWiseDofIndexList() const noexcept;

        //! Get the list of all dof values.
        const std::vector<PetscScalar>& GetDofValueList() const noexcept;

        /*!
         * \brief Update the values to apply to the dofs in the boundary condition.
         *
         * \param[in] new_values New values to apply. This vector should be the sme size as \a dof_value_list_.
         * The legwork to create \a new_values is performed by DirichletBoundaryCondition class (the values are
         * extracted from a GlobalVector).
         */
        void UpdateValueList(std::vector<PetscScalar>&& new_values);

      private:
        //! Get the list of all dof values.
        std::vector<PetscScalar>& GetNonCstDofValueList() noexcept;


      private:
        /*!
         * \brief List of all dofs in the combination boundary condition/numbering subset.
         *
         */
        Dof::vector_shared_ptr dof_list_;

        //! List of all dof program-wise index (within the numbering subset for which current class exists).
        std::vector<PetscInt> program_wise_dof_index_list_;

        //! List of all dof processor-wise index (within the numbering subset for which current class exists).
        std::vector<PetscInt> processor_wise_dof_index_list_;

        //! List of all dof values.
        std::vector<PetscScalar> dof_value_list_;
    };


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/BoundaryConditions/Internal/DofStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_DOFSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
