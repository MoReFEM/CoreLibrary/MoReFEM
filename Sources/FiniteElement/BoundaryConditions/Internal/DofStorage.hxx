// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_DOFSTORAGE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_DOFSTORAGE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/BoundaryConditions/Internal/DofStorage.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    inline const std::vector<PetscInt>& DofStorage::GetProgramWiseDofIndexList() const noexcept
    {
        return program_wise_dof_index_list_;
    }


    inline const std::vector<PetscInt>& DofStorage::GetProcessorWiseDofIndexList() const noexcept
    {
        return processor_wise_dof_index_list_;
    }


    inline const std::vector<PetscScalar>& DofStorage::GetDofValueList() const noexcept
    {
        assert(GetProgramWiseDofIndexList().size() == dof_value_list_.size());
        return dof_value_list_;
    }


    inline const Dof::vector_shared_ptr& DofStorage::GetDofList() const noexcept
    {
        assert(GetProgramWiseDofIndexList().size() == dof_list_.size());
        return dof_list_;
    }


    inline std::vector<PetscScalar>& DofStorage::GetNonCstDofValueList() noexcept
    {
        return const_cast<std::vector<PetscScalar>&>(GetDofValueList());
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_DOFSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
