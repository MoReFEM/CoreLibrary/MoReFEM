/*!
 * \defgroup FiniteElementGroup Finite element
 * 
 * \brief Large module to encompass stuff closely related to finite elements: nodes and dofs,
 * boundary conditions, finite element spaces, unknowns and so forth...
 *
 */



/*!
 * \class doxygen_hide_god_of_dof_prepartitioned_data_param
 *
 * \param[in] god_of_dof_prepartitioned_data Lua file which gives the data needed to reconstruct the data related
 * to \a GodOfDof from pre-computed partitioned data. Note: it is not const as such objects relies on a Lua stack which is modified for
 * virtually each operation but it should not be considered as an output parameter.
 */


/// \addtogroup FiniteElementGroup
///@{

/// \namespace MoReFEM::Internal::BoundaryConditionNS
/// \brief Namespace that enclose internals related to boundary conditions.

/// \namespace MoReFEM::Internal::FEltSpaceNS
/// \brief Namespace that enclose library's developer tools related to \a FEltSpace.

/// \namespace MoReFEM::ExceptionNS::Dof
/// \brief Namespace that enclose Dof-related exceptions.

/// \namespace MoReFEM::QuadratureNS
/// \brief Namespace that enclose quadrature points and rules.

/// \namespace MoReFEM::ExceptionNS::QuadratureRuleListNS
/// \brief Namespace that enclose quadrature rule list-related exceptions.

/// \namespace MoReFEM::Internal::GaussQuadratureNS
/// \brief Namespace that enclose helper formula used in Gauss quadrature rules.
///
/// At the moment it is not documented as these are third party functions barely adapted
/// I have very few knowledge of.


/// \namespace MoReFEM::Internal::FEltNS
/// \brief Namespace that enclose helper classes of FElt and LocalFEltSpace.

/// \namespace MoReFEM::RefFEltNS
/// \brief Namespace that enclose the available reference finite elements.

/// \namespace MoReFEM::Internal::RefFEltNS
/// \brief Namespace that enclose helper classes of RefFElt.

/// \namespace MoReFEM::Internal::ShapeFunctionNS
/// \brief Namespace that enclose helper classes concerning shape functions.

///@}



