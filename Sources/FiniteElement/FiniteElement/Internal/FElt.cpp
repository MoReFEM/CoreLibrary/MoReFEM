// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>

#include "FiniteElement/FiniteElement/Internal/FElt.hpp"

#include "FiniteElement/Nodes_and_dofs/Node.hpp"


namespace MoReFEM::Internal
{


    FElt::FElt(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt) : ref_felt_(ref_felt)
    { }


    void FElt::AddNode(const Node::shared_ptr& node_ptr)
    {
        assert(!(!node_ptr));
        node_list_.push_back(node_ptr);
    }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
