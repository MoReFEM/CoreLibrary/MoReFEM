// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENT_LOCALFELTSPACE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENT_LOCALFELTSPACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Mutex/Mutex.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "Core/Parameter/Advanced/Concept.hpp" // IWYU pragma: export
#include "Core/Parameter/TypeEnum.hpp" // IWYU pragma: keep // IWYU doesn't manage gracefully this enum as forward declaration
#include "Core/TimeManager/Concept.hpp" // IWYU pragma: export

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { enum class DoComputeProcessorWiseLocal2Global; }
namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }
namespace MoReFEM { enum class MpiScale; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatchInterfaceNodeBearer; }
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }
namespace MoReFEM::ParameterNS::Policy
{

    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    class AtDof;
} // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief This class holds all Finite elements related to a given GeometricElt.
     *
     * There might be several: there is in fact one FElt per Unknown present in the LocalFEltSpace.
     *
     * This is also this class that is in charge of giving away the local2global arrays.
     *
     */
    class LocalFEltSpace final : private Crtp::Mutex<LocalFEltSpace>
    {

      public:
        //! Alias to shared pointer.
        using shared_ptr = std::shared_ptr<LocalFEltSpace>;

        //! Alias to vector of shared_pointer.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Friendship to FEltSpace, the only class entitled to build a new LocalFEltSpace.
        friend class FEltSpace;

        //! Friendship to the only class entitled to add finite elements to the local felt space.
        friend class Internal::FEltSpaceNS::MatchInterfaceNodeBearer;

        //! Convenient storage: key is the index of the geometric element, value the actual pointer to the
        //! LocalFEltSpace.
        using per_geom_elt_index = std::unordered_map<::MoReFEM::GeomEltNS::index_type, LocalFEltSpace::shared_ptr>;

        //! Friendship to a specific \a Parameter policy (that needs access to private \a FEllt).
        // clang-format off
        template
        <
            ParameterNS::Type TypeT,
            TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
            ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
            Eigen::Index NfeltSpaceT
        >
        // clang-format on
        friend class ParameterNS::Policy::AtDof;

      private:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] ref_felt_space \a RefLocalFEltSpace upon which the object is built.
        //! \param[in] geometric_element Concrete \a GeometricElt for which the object is built.
        explicit LocalFEltSpace(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                GeometricElt::shared_ptr geometric_element);

      public:
        //! Destructor.
        ~LocalFEltSpace();

        //! \copydoc doxygen_hide_copy_constructor
        LocalFEltSpace(const LocalFEltSpace& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LocalFEltSpace(LocalFEltSpace&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LocalFEltSpace& operator=(const LocalFEltSpace& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LocalFEltSpace& operator=(LocalFEltSpace&& rhs) = delete;


        ///@}


        //! Access to the geometric element as a const reference.
        const GeometricElt& GetGeometricElt() const noexcept;

        //! Access to the geometric element as a smart pointer.
        GeometricElt::shared_ptr GetGeometricEltPtr() const noexcept;

        //! Access to the list of nodes bearer.
        const NodeBearer::vector_shared_ptr& GetNodeBearerList() const noexcept;

        //! Set the list of node bearers.
        //! \param[in] node_bearer_list List of \a NodeBearer computed outside the class.
        void SetNodeBearerList(NodeBearer::vector_shared_ptr&& node_bearer_list);

        /*!
         * \brief Init for each numbering subset a global local2global array.
         *
         * In this array all the unknowns within the numbering subset are considered; their ordering is the same
         * as the ordering in \a felt_list_.
         *
         * This array is not accessible publicly; it is used only to generate the local2global required by the
         * operators (that are computed through a call to \a ComputeLocal2Global, issued in each
         * GlobalVariationalOperator constructor).
         *
         * \copydoc doxygen_hide_do_consider_processor_wise_local_2_global
         * \param[in] numbering_subset_list List of all numbering subsets for which local2global array should be
         * computed.
         *
         * \internal <b><tt>[internal]</tt></b> Processor- and program-wise local2global arrays are actually stored in
         * two different containers.
         * \endinternal
         */
        void InitLocal2Global(const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                              DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global);


        /*!
         * \brief Compute Local2GLobal for a subset of the unknowns.
         *
         * This method is supposed to be called by each operator, that asks upon its creation that the
         * local2global it will need are correctly built.
         *
         * It assumes that \a InitLocal2Global has been correctly called beforehand.
         *
         * \param[in] extended_unknown_list List of unknowns for which the local2global array is required. They must all
         * belong to the same numbering subset.
         * \copydetails doxygen_hide_do_compute_processor_wise_local_2_global_arg
         */
        void ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                 DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global);

        /*!
         * \brief Get the local2global array related to a list of unknowns.
         *
         * \tparam MpiScaleT Whether processor- or program-wise local2global array is required.
         *
         * \warning Beware: processor-wise one must have been explicitly built with a dedicated argument
         * in \a ComputeLocal2Global.
         *
         * \param[in] extended_unknown_list List of unknowns considered, along with their associated \a NumberingSubset
         * in the \a FEltSpace.
         *
         * \return Local2global array related to a list of unknowns.
         */
        template<MpiScale MpiScaleT>
        const std::vector<PetscInt>&
        GetLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list) const;


        /*!
         * \brief Get the local2global array related to a single unknown (in other terms, it is the local2global
         * related to a single finite element).
         *
         * \tparam MpiScaleT Whether processor- or program-wise local2global array is required.
         *
         * \warning Beware: processor-wise one must have been explicitly built with a dedicated argument in
         * \a ComputeLocal2Global.
         *
         * \param[in] extended_unknown Couple unknown/numbering subset for which the local -> global array is sought.
         *
         * \return Local2global array related to \a extended_unknown.
         */
        template<MpiScale MpiScaleT>
        const std::vector<PetscInt>& GetLocal2Global(const ExtendedUnknown& extended_unknown) const;

      private:
        /*!
         * \brief Access to the FElt matching the given \a unknown.
         * \param[in] unknown \a Unknown used as filter.
         *
         */
        const Internal::FElt& GetFElt(const Unknown& unknown) const;

        //! Access to the FElt matching the given \a extended_unknown.
        //! \param[in] extended_unknown \a ExtendedUnknown used as filter.
        const Internal::FElt& GetFElt(const ExtendedUnknown& extended_unknown) const;

        //! Non-constant access to the FElt matching the given \a extended_unknown.
        //! \param[in] extended_unknown \a ExtendedUnknown used as filter.
        Internal::FElt& GetNonCstFElt(const ExtendedUnknown& extended_unknown);


        //! Access to all FElts.
        const Internal::FElt::vector_shared_ptr& GetFEltList() const noexcept;


        /*!
         * \brief Add a new finite element and returns a non-constant reference to it.
         *
         * \internal <b><tt>[internal]</tt></b> This method should only be called within friend
         * MatchInterfaceNodeBearer. \endinternal
         *
         * \param[in] ref_felt Description of a reference finite element associated to an \a Unknown, a \a
         * NumberingSubset and a \a RefGeomElt.
         *
         * \return Reference to the newly created \a FElt.
         */
        Internal::FElt& AddFElt(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt);


        //! Non constant access to all FElts.
        Internal::FElt::vector_shared_ptr& GetNonCstFEltList() noexcept;

        //! Access to the reference felt space.
        const Internal::RefFEltNS::RefLocalFEltSpace& GetRefLocalFEltSpace() const noexcept;

        //! Constant access to local_2_global_per_numbering_subset_.
        //! \param[in] numbering_subset \a NumberingSubset for which the local->global is sought.
        template<MpiScale MpiScaleT>
        const Internal::FEltNS::Local2GlobalStorage&
        GetLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset) const;

        //! Non constant access to local_2_global_per_numbering_subset_.
        //! \param[in] numbering_subset \a NumberingSubset for which the local->global is sought.
        template<MpiScale MpiScaleT>
        Internal::FEltNS::Local2GlobalStorage&
        GetNonCstLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset);

        //! Constant access to the whole local_2_global storage per numbering subset.
        template<MpiScale MpiScaleT>
        const std::vector<std::pair<NumberingSubsetNS::unique_id, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>&
        GetLocal2GlobalStorage() const noexcept;

        //! Clear the temporary data used to build properly the Internal::FEltNS::Local2GlobalStorage objects.
        void ClearTemporaryData() const noexcept;


      private:
        //! Reference to the related RefLocalFEltSpace.
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space_;

        /*!
         * \brief Finite element list
         *
         * There is one finite element per relevant unknown: a same unknown can be associated to only one
         * numbering subset in a given (global) finite element space.
         */
        Internal::FElt::vector_shared_ptr felt_list_;

        //! Pointer to the related GeometricElt.
        GeometricElt::shared_ptr geometric_elt_;

        //! List of nodes that belong to the LocalFEltSpace.
        NodeBearer::vector_shared_ptr node_bearer_list_;

        /*!
         * \brief Store for each numbering subset all the required program-wise local2global arrays.
         *
         * Key is the unique id of the numbering subset.
         * Value is the object in charge of holding the information.
         */
        std::vector<std::pair<NumberingSubsetNS::unique_id, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>
            local_2_global_program_wise_per_numbering_subset_;

        /*!
         * \brief Store for each numbering subset all the required processor-wise local2global arrays.
         *
         * Key is the unique id of the numbering subset.
         * Value is the object in charge of holding the information.
         */
        std::vector<std::pair<NumberingSubsetNS::unique_id, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>
            local_2_global_processor_wise_per_numbering_subset_;


#ifndef NDEBUG

        //! Whether the local -> global (processor-wise) must also be built. If not local_2_global_processor_wise_
        //! remains empty.
        DoComputeProcessorWiseLocal2Global do_consider_processor_wise_local_2_global_;

#endif // NDEBUG
    };


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * We are using directly the underlying \a GeometricElt for the comparison.
     */
    bool operator<(const LocalFEltSpace& lhs, const LocalFEltSpace& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * We are using directly the underlying \a GeometricElt for the comparison.
     *
     * \internal When C++ 20 is out, use spaceship operator here...
     */
    bool operator==(const LocalFEltSpace& lhs, const LocalFEltSpace& rhs) noexcept;


    /*!
     * \brief Returns whether the \a local_felt_space is in \a domain or not.
     *
     * \internal <b><tt>[internal]</tt></b> felt object here just returns its underlying geometric_element;
     * it's upon this one the test is really performed (domain is a purely geometric concept).
     * \endinternal
     *
     * \param[in] local_felt_space \a LocalFEltSpace being considered (here the only relevant information
     * about it is the \a GeometricElement to which it is related).
     * \param[in] domain Domain considered.
     *
     * \return True if the underlying \a GeometricElement of \a local_felt_space is in \a domain.
     */
    bool IsLocalFEltSpaceInDomain(const LocalFEltSpace& local_felt_space, const Domain& domain) noexcept;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElement/LocalFEltSpace.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENT_LOCALFELTSPACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
