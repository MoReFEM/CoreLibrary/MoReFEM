// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENT_LOCALFELTSPACE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENT_LOCALFELTSPACE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Miscellaneous.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const GeometricElt& LocalFEltSpace::GetGeometricElt() const noexcept
    {
        assert(!(!geometric_elt_));
        return *geometric_elt_;
    }


    inline GeometricElt::shared_ptr LocalFEltSpace::GetGeometricEltPtr() const noexcept
    {
        assert(!(!geometric_elt_));
        return geometric_elt_;
    }


    inline const Internal::FElt& LocalFEltSpace::GetFElt(const ExtendedUnknown& extended_unknown) const
    {
        auto it = std::find_if(felt_list_.cbegin(),
                               felt_list_.cend(),
                               [&extended_unknown](const auto& felt_ptr)
                               {
                                   assert(!(!felt_ptr));
                                   return felt_ptr->GetExtendedUnknown() == extended_unknown;
                               });
        assert(it != felt_list_.cend());
        return *(*it);
    }


    inline const Internal::FElt& LocalFEltSpace::GetFElt(const Unknown& unknown) const
    {
        auto it = std::find_if(felt_list_.cbegin(),
                               felt_list_.cend(),
                               [&unknown](const auto& felt_ptr)
                               {
                                   assert(!(!felt_ptr));
                                   return felt_ptr->GetExtendedUnknown().GetUnknown() == unknown;
                               });
        assert(it != felt_list_.cend());
        return *(*it);
    }


    inline Internal::FElt& LocalFEltSpace::GetNonCstFElt(const ExtendedUnknown& extended_unknown)
    {
        return const_cast<Internal::FElt&>(GetFElt(extended_unknown));
    }


    inline const Internal::FElt::vector_shared_ptr& LocalFEltSpace::GetFEltList() const noexcept
    {
        return felt_list_;
    }


    inline Internal::FElt::vector_shared_ptr& LocalFEltSpace::GetNonCstFEltList() noexcept
    {
        return const_cast<Internal::FElt::vector_shared_ptr&>(GetFEltList());
    }


    inline const NodeBearer::vector_shared_ptr& LocalFEltSpace::GetNodeBearerList() const noexcept
    {
        assert(!node_bearer_list_.empty());
        assert(std::none_of(
            node_bearer_list_.cbegin(), node_bearer_list_.cend(), Utilities::IsNullptr<NodeBearer::shared_ptr>));

        return node_bearer_list_;
    }


    inline const Internal::RefFEltNS::RefLocalFEltSpace& LocalFEltSpace::GetRefLocalFEltSpace() const noexcept
    {
        return ref_felt_space_;
    }


    template<MpiScale MpiScaleT>
    inline const Internal::FEltNS::Local2GlobalStorage&
    LocalFEltSpace ::GetLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto numbering_subset_id = numbering_subset.GetUniqueId();

        const auto& local2global_storage = GetLocal2GlobalStorage<MpiScaleT>();

        const auto it = std::find_if(local2global_storage.cbegin(),
                                     local2global_storage.cend(),
                                     [numbering_subset_id](const auto& pair)
                                     {
                                         return pair.first == numbering_subset_id;
                                     });

        assert(it != local2global_storage.cend());
        assert(!(!it->second));

        return *(it->second);
    }


    template<MpiScale MpiScaleT>
    inline Internal::FEltNS::Local2GlobalStorage&
    LocalFEltSpace ::GetNonCstLocal2GlobalStorageForNumberingSubset(const NumberingSubset& numbering_subset)
    {
        return const_cast<Internal::FEltNS::Local2GlobalStorage&>(
            GetLocal2GlobalStorageForNumberingSubset<MpiScaleT>(numbering_subset));
    }


    template<MpiScale MpiScaleT>
    const std::vector<PetscInt>&
    LocalFEltSpace ::GetLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list) const
    {
        assert(!unknown_list.empty());

        assert(std::none_of(
            unknown_list.cbegin(), unknown_list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));

        const auto& numbering_subset = unknown_list.back()->GetNumberingSubset();

        assert(std::all_of(unknown_list.cbegin(),
                           unknown_list.cend(),
                           [&numbering_subset](const auto& unknown_ptr)
                           {
                               return unknown_ptr->GetNumberingSubset() == numbering_subset;
                           })
               && "All unknowns in the list should belong to the same numbering subset!");


        const auto& local2global_storage = GetLocal2GlobalStorageForNumberingSubset<MpiScaleT>(numbering_subset);

        return local2global_storage.GetLocal2Global(unknown_list);
    }


    template<MpiScale MpiScaleT>
    const std::vector<PetscInt>& LocalFEltSpace ::GetLocal2Global(const ExtendedUnknown& unknown) const
    {
        const auto& local2global_storage =
            GetLocal2GlobalStorageForNumberingSubset<MpiScaleT>(unknown.GetNumberingSubset());

        return local2global_storage.GetLocal2Global(unknown);
    }


    template<MpiScale MpiScaleT>
    inline const std::vector<
        std::pair<NumberingSubsetNS::unique_id, Internal::FEltNS::Local2GlobalStorage::unique_ptr>>&
    LocalFEltSpace::GetLocal2GlobalStorage() const noexcept
    {
        switch (MpiScaleT)
        {
        case MpiScale::program_wise:
            return local_2_global_program_wise_per_numbering_subset_;
        case MpiScale::processor_wise:
            return local_2_global_processor_wise_per_numbering_subset_;
        }

        assert(false);
        return local_2_global_program_wise_per_numbering_subset_; // to avoid warnings
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENT_LOCALFELTSPACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
