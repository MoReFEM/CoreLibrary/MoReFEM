// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/LinearAlgebra/GlobalDiagonalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{


    void AllocateGlobalMatrix(const GodOfDof& god_of_dof, GlobalMatrix& matrix)
    {
        const auto& row_numbering_subset = matrix.GetRowNumberingSubset();
        const auto& col_numbering_subset = matrix.GetColNumberingSubset();

        const auto& matrix_pattern = god_of_dof.GetMatrixPattern(row_numbering_subset, col_numbering_subset);

        const auto& mpi = god_of_dof.GetMpi();

        const auto Nrow_processor_wise_dof =
            row_processor_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprocessorWiseDof(row_numbering_subset)) };
        const auto Ncolumn_processor_wise_dof =
            col_processor_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprocessorWiseDof(col_numbering_subset)) };

        if (mpi.IsSequential())
        {
            matrix.InitSequentialMatrix(Nrow_processor_wise_dof, Ncolumn_processor_wise_dof, matrix_pattern, mpi);
        } else
        {
            const auto Nrow_program_wise_dof =
                row_program_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprogramWiseDof(row_numbering_subset)) };
            const auto Ncolumn_program_wise_dof =
                col_program_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprogramWiseDof(col_numbering_subset)) };

            matrix.InitParallelMatrix(Nrow_processor_wise_dof,
                                      Ncolumn_processor_wise_dof,
                                      Nrow_program_wise_dof,
                                      Ncolumn_program_wise_dof,
                                      matrix_pattern,
                                      mpi);
        }

        matrix.ZeroEntries();
    }


    void AllocateGlobalMatrix(const GodOfDof& god_of_dof, GlobalDiagonalMatrix& matrix)
    {
        const auto& numbering_subset = matrix.GetRowNumberingSubset();
        assert(numbering_subset == matrix.GetColNumberingSubset() && "Square matrix expected!");

        // ===================================================
        // Determine the (simplistic) matrix pattern of the diagonal matrix.
        // ===================================================
        const auto Nrow_proc_wise =
            row_processor_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprocessorWiseDof(numbering_subset)) };
        const auto Ncol_proc_wise = col_processor_wise_index_type{ Nrow_proc_wise.Get() };

        std::vector<std::vector<PetscInt>> non_zero_slots_per_local_row(static_cast<std::size_t>(Nrow_proc_wise.Get()));

        const auto& dof_list = god_of_dof.GetProcessorWiseDofList();

        for (const auto& dof_ptr : dof_list)
        {
            assert(!(!dof_ptr));
            const auto& dof = *dof_ptr;

            if (!dof.IsInNumberingSubset(numbering_subset))
                continue;

            const auto proc_wise_index = dof.GetProcessorWiseOrGhostIndex(numbering_subset);
            assert(proc_wise_index
                   < DofNS::processor_wise_or_ghost_index{ static_cast<std::size_t>(Nrow_proc_wise.Get()) });
            assert(non_zero_slots_per_local_row[proc_wise_index.Get()].empty() && "Each should be filled once!");

            const auto program_wise_index = static_cast<PetscInt>(dof.GetProgramWiseIndex(numbering_subset).Get());
            non_zero_slots_per_local_row[proc_wise_index.Get()] = { program_wise_index };
        }

        const Wrappers::Petsc::MatrixPattern diagonal_pattern(non_zero_slots_per_local_row);

        // ===================================================
        // Now fill the structure of the matrix as in AllocateGlobalMatrix().
        // ===================================================

        decltype(auto) mpi = god_of_dof.GetMpi();

        if (mpi.IsSequential())
        {
            matrix.InitSequentialMatrix(Nrow_proc_wise, Ncol_proc_wise, diagonal_pattern, mpi);
        } else
        {
            const auto Nrow_program_wise =
                row_program_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprogramWiseDof(numbering_subset)) };
            const auto Ncol_program_wise = col_program_wise_index_type{ Nrow_program_wise.Get() };

            matrix.InitParallelMatrix(
                Nrow_proc_wise, Ncol_proc_wise, Nrow_program_wise, Ncol_program_wise, diagonal_pattern, mpi);
        }

        matrix.ZeroEntries();
    }


    void AllocateGlobalVector(const GodOfDof& god_of_dof, GlobalVector& vector)
    {
        const auto& mpi = god_of_dof.GetMpi();

        const auto& numbering_subset = vector.GetNumberingSubset();

        const auto Nprocessor_wise_dof =
            vector_processor_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprocessorWiseDof(numbering_subset)) };

        if (mpi.IsSequential())
            vector.InitSequentialVector(mpi, Nprocessor_wise_dof);
        else
        {
            std::vector<PetscInt> ghost_dof_index_list;

            const auto Nprogram_wise_dof =
                vector_program_wise_index_type{ static_cast<PetscInt>(god_of_dof.NprogramWiseDof(numbering_subset)) };

            {
                const auto& ghost_dof_list = god_of_dof.GetGhostDofList();
                ghost_dof_index_list.reserve(ghost_dof_list.size());

                for (const auto& dof_ptr : ghost_dof_list)
                {
                    assert((!(!dof_ptr)));
                    const auto& dof = *dof_ptr;

                    if (dof.IsInNumberingSubset(numbering_subset))
                    {
                        ghost_dof_index_list.push_back(
                            static_cast<PetscInt>(dof.GetProgramWiseIndex(numbering_subset).Get()));
                    }
                }
            }

            assert(std::ranges::is_sorted(ghost_dof_index_list));

            vector.InitMpiVectorWithGhost(mpi, Nprocessor_wise_dof, Nprogram_wise_dof, ghost_dof_index_list);
        }

        vector.ZeroEntries();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
