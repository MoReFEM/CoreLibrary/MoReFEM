// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <fstream>
#include <iterator>
#include <memory>
#include <ostream>
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/Output/WritePrepartitionedData.hpp"
#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    GodOfDof::GodOfDof(const Wrappers::Mpi& mpi, Mesh& mesh)
    : Crtp::CrtpMpi<GodOfDof>(mpi), unique_id_parent(mesh.GetUniqueId()), mesh_(mesh)
    { }


    const std::string& GodOfDof::ClassName()
    {
        static const std::string ret("GodOfDof");
        return ret;
    }


    void GodOfDof::PrintDofInformation(const NumberingSubset& numbering_subset, std::ostream& stream) const
    {
        const auto& node_bearer_list = GetProcessorWiseNodeBearerList();

        stream << "Ndof (processor_wise) = " << NprocessorWiseDof(numbering_subset) << '\n';
        stream << "# First column: program-wise index." << '\n';
        stream << "# Second column: processor-wise index." << '\n';
        stream << "# Third column: the interface upon which the dof is located. Look at the interface file in same "
                  "directory to relate it to the initial mesh."
               << '\n';
        stream << "# Fourth column: unknown and component involved." << '\n';
        stream << "# Fifth column: shape function label." << '\n';

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            const auto& node_bearer = *node_bearer_ptr;

            std::ostringstream oconv;
            oconv << node_bearer.GetNature() << ' ' << node_bearer.GetInterface().GetProgramWiseIndex();

            const std::string nature(oconv.str());

            const auto& node_list = node_bearer.GetNodeList();

            // Beware: pointer itself is constant, but underlying object is modified!
            for (const auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;

                if (!node.IsInNumberingSubset(numbering_subset))
                    continue;

                const auto& unknown = node.GetUnknown();

                const std::string unknown_name(unknown.GetName());

                const auto& dof_list = node.GetDofList();

                const std::size_t Ndof_in_node = dof_list.size();

                const auto& shape_function_label = node.GetShapeFunctionLabel();

                for (std::size_t i = 0UL; i < Ndof_in_node; ++i)
                {
                    const auto& dof_ptr = dof_list[i];
                    assert(!(!dof_ptr));

                    stream << dof_ptr->GetProgramWiseIndex(numbering_subset) << ';';
                    stream << dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset) << ';';
                    stream << nature << ';';
                    stream << unknown_name << ' ' << i << ';';
                    stream << shape_function_label;
                    stream << '\n';
                }
            }
        }
    }


    void PrintNodeBearerList(const NodeBearer::vector_shared_ptr& node_bearer_list,
                             const rank_type rank,
                             std::ostream& stream)
    {
        std::vector<NodeBearerNS::program_wise_index_type> indexes;
        indexes.reserve(node_bearer_list.size());

        std::vector<rank_type> on_proc;
        on_proc.reserve(node_bearer_list.size());

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            indexes.push_back(node_bearer_ptr->GetProgramWiseIndex());
            on_proc.push_back(node_bearer_ptr->GetProcessor());
        }

        std::ostringstream oconv;
        oconv << "Node bearers on processor " << rank << " -> [";

        Utilities::PrintContainer<>::Do(indexes,
                                        stream,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener(oconv.str()),
                                        PrintNS::Delimiter::closer("]"));
    }


    auto GodOfDof::GetIteratorFEltSpace(FEltSpaceNS::unique_id felt_space_unique_id) const
    {
        const auto& felt_space_list = GetFEltSpaceList();

        return std::ranges::find_if(felt_space_list,

                                    [felt_space_unique_id](const auto& felt_space_ptr)
                                    {
                                        assert(!(!felt_space_ptr));
                                        return felt_space_ptr->GetUniqueId() == felt_space_unique_id;
                                    });
    }


    bool GodOfDof::IsFEltSpace(FEltSpaceNS::unique_id felt_space_unique_id) const
    {
        return GetIteratorFEltSpace(felt_space_unique_id) != GetFEltSpaceList().cend();
    }


    const FEltSpace& GodOfDof::GetFEltSpace(FEltSpaceNS::unique_id felt_space_unique_id) const
    {
        auto it = GetIteratorFEltSpace(felt_space_unique_id);

        assert(it != GetFEltSpaceList().cend() && "If not something went awry in GodOfDof initialization!");

        return *(*it);
    }


    const NumberingSubset::vector_const_shared_ptr& GodOfDof::ComputeNumberingSubsetList()
    {
        const auto& felt_space_list = GetFEltSpaceList();

        assert(numbering_subset_list_.empty() && "Should be initialized only once!");

        for (const auto& felt_space_ptr : felt_space_list)
        {
            decltype(auto) numbering_subset_list_in_felt_space = felt_space_ptr->GetNumberingSubsetList();

            std::ranges::move(numbering_subset_list_in_felt_space,

                              std::back_inserter(numbering_subset_list_));
        }

        Utilities::EliminateDuplicate(numbering_subset_list_,
                                      Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                      Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

        assert(!numbering_subset_list_.empty() && "There must be at least one...");

        return numbering_subset_list_;
    }


    const Wrappers::Petsc::MatrixPattern&
    GodOfDof::GetMatrixPattern(const NumberingSubset& row_numbering_subset,
                               const NumberingSubset& column_numbering_subset) const
    {
        auto it = std::ranges::find_if(matrix_pattern_per_numbering_subset_,

                                       [&row_numbering_subset, &column_numbering_subset](const auto& item)
                                       {
                                           assert(!(!item));
                                           return item->GetRowNumberingSubset() == row_numbering_subset
                                                  && item->GetColumnNumberingSubset() == column_numbering_subset;
                                       });

        assert(it != matrix_pattern_per_numbering_subset_.cend());
        assert(!(!*it));
        return (*it)->GetPattern();
    }


    const NumberingSubset::const_shared_ptr&
    GodOfDof::GetNumberingSubsetPtr(NumberingSubsetNS::unique_id unique_id) const
    {
        const auto& numbering_subset_list = GetNumberingSubsetList();

        auto it = std::ranges::find_if(numbering_subset_list,

                                       [unique_id](const auto& numbering_subset_ptr)
                                       {
                                           assert(!(!numbering_subset_ptr));
                                           return numbering_subset_ptr->GetUniqueId() == unique_id;
                                       });

        // \todo #608 Should be an exception (currently assert is fine but for some model in which subsets
        // may be monolithic or not it's not that sure...)
        assert(Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance().DoExist(unique_id)
               && "The requested numbering subset is not even defined in the InputData tuple!");

        assert(it != numbering_subset_list.cend()
               && "Numbering subset was not found in the GodOfDof but do exists in the InputData tuple; "
                  "the most likely explanation is that no FEltSpace in the GodOfDof is actually using it.");

        assert(!(!*it));

        return *it;
    }


    void GodOfDof::PrepareOutput(const Internal::Parallelism* parallelism)
    {
        decltype(auto) numbering_subset_list = GetNumberingSubsetList();

        decltype(auto) mpi = GetMpi();

        // First create all the relevant folders; make sure this is done before any processor attempts to create a file.
        {
            for (const auto& numbering_subset_ptr : numbering_subset_list)
            {
                assert(!(!numbering_subset_ptr));
                const auto& numbering_subset = *numbering_subset_ptr;

                // Create the subfolder to store numbering subset data.
                // Root processor is in charge of it; other processors must wait until it's done.
                auto&& subfolder = GetOutputDirectoryForNumberingSubset(numbering_subset);

                // subfolder.SetBehaviour(FilesystemNS::behaviour::overwrite);
                subfolder.ActOnFilesystem();
            }
        }

        mpi.Barrier();

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            // Create the subfolder to store numbering subset data.
            // Root processor is in charge of it; other processors must wait until it's done.
            auto&& subfolder = GetOutputDirectoryForNumberingSubset(numbering_subset);

            // Report the dof information.
            std::ostringstream oconv;
            oconv << subfolder << "/dof_information.hhdata";

            const FilesystemNS::File file(oconv.str());
            std::ofstream out{ file.NewContent() };

            PrintDofInformation(numbering_subset, out);
        }

        // Write if required the parallelism strategy.
        if (parallelism != nullptr)
        {
            const auto parallelism_strategy = parallelism->GetParallelismStrategy();

            decltype(auto) mesh = GetMesh();

            switch (parallelism_strategy)
            {
            case Advanced::parallelism_strategy::parallel:
            case Advanced::parallelism_strategy::precompute:
            {
                const FilesystemNS::Directory mesh_subdir(parallelism->GetDirectory(),
                                                          "Mesh_" + std::to_string(mesh.GetUniqueId().Get()));

                mesh_subdir.ActOnFilesystem();

                const Advanced::MeshNS::WritePrepartitionedData partition_data_facility(
                    mesh, mesh_subdir, mesh.GetInitialFormat());

                Internal::GodOfDofNS::WritePrepartitionedData(*this, mesh_subdir);

                break;
            }
            case Advanced::parallelism_strategy::none:
            case Advanced::parallelism_strategy::run_from_preprocessed:
            case Advanced::parallelism_strategy::parallel_no_write:
                break;
            }
        }
    }


    void GodOfDof::ClearTemporaryData() const noexcept
    {
        const auto& felt_list = GetFEltSpaceList();

        for (const auto& felt_ptr : felt_list)
        {
            assert(!(!felt_ptr));
            felt_ptr->ClearTemporaryData();
        }
    }


#ifndef NDEBUG
    void AssertMatrixRespectPattern(const GodOfDof& god_of_dof,
                                    const GlobalMatrix& matrix,
                                    const std::source_location location)
    {
        decltype(auto) mpi = god_of_dof.GetMpi();

        try
        {
            decltype(auto) pattern =
                god_of_dof.GetMatrixPattern(matrix.GetRowNumberingSubset(), matrix.GetColNumberingSubset());

            const auto Nrow = DofNS::processor_wise_or_ghost_index{ static_cast<std::size_t>(
                matrix.GetProcessorWiseSize(location).first.Get()) };

            if (Nrow.Get() != static_cast<std::size_t>(pattern.Nrow().Get()))
                throw Exception("Number of rows in pattern and matrix is not the same!", location);

            decltype(auto) iCsr = pattern.GetICsr();
            decltype(auto) jCsr = pattern.GetJCsr();

            assert(iCsr.size() == Nrow.Get() + 1 && "This would highlight a bug within MatrixPattern class");

            auto current_jcsr_index = 0UL;

            decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();

            std::vector<PetscInt> position_list_in_matrix;
            std::vector<PetscScalar> value_list_in_matrix;

            decltype(auto) row_numbering_subset = matrix.GetRowNumberingSubset();


            Dof::vector_shared_ptr processor_wise_dof_list_in_row_numbering_subset;


            std::ranges::copy_if(processor_wise_dof_list,

                                 std::back_inserter(processor_wise_dof_list_in_row_numbering_subset),
                                 [&row_numbering_subset](const auto& dof_ptr)
                                 {
                                     assert(!(!dof_ptr));
                                     return dof_ptr->IsInNumberingSubset(row_numbering_subset);
                                 });

            for (auto row_index = DofNS::processor_wise_or_ghost_index{ 0UL }; row_index < Nrow; ++row_index)
            {
                position_list_in_matrix.clear();
                value_list_in_matrix.clear();

                const auto it = std::ranges::find_if(
                    processor_wise_dof_list_in_row_numbering_subset,

                    [&row_numbering_subset, row_index](const auto& dof_ptr)
                    {
                        assert(!(!dof_ptr));
                        return dof_ptr->GetProcessorWiseOrGhostIndex(row_numbering_subset) == row_index;
                    });
                assert(it != processor_wise_dof_list.cend());
                const auto& dof_ptr = (*it);
                assert(!(!(dof_ptr)));


                const auto program_wise_index = row_program_wise_index_type{ static_cast<PetscInt>(
                    dof_ptr->GetProgramWiseIndex(row_numbering_subset).Get()) };

                matrix.GetRow(program_wise_index, position_list_in_matrix, value_list_in_matrix, location);

                const auto Nvalue_in_pattern_row =
                    static_cast<std::size_t>(iCsr[static_cast<std::size_t>(row_index.Get() + 1)]
                                             - iCsr[static_cast<std::size_t>(row_index.Get())]);

                std::vector<PetscInt> position_list_in_pattern;

                for (auto j = 0UL; j < Nvalue_in_pattern_row; ++j)
                {
                    assert(current_jcsr_index + j < jCsr.size());
                    position_list_in_pattern.push_back(static_cast<PetscInt>(jCsr[current_jcsr_index + j]));
                }


                current_jcsr_index += Nvalue_in_pattern_row;

                if (!std::ranges::includes(position_list_in_pattern,

                                           position_list_in_matrix))
                {
                    PrintNumberingSubset("matrix", matrix);

                    std::ostringstream oconv;
                    oconv << "Position in the actual matrix must match those defined in the pattern! Not the case "
                             "for row "
                          << program_wise_index << " (program-wise numbering) on processor " << mpi.GetRank<int>();

                    Utilities::PrintContainer<>::Do(position_list_in_matrix,
                                                    oconv,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("\n\t- Position in matrix -> ["),
                                                    PrintNS::Delimiter::closer("]"));

                    Utilities::PrintContainer<>::Do(position_list_in_pattern,
                                                    oconv,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("\t- Position in pattern -> ["),
                                                    PrintNS::Delimiter::closer("]"));

                    throw Exception(oconv.str(), location);
                }
            }
        }
        catch (const Exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
#endif // NDEBUG


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
