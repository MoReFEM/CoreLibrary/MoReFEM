// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOFMANAGER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOFMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <unordered_map>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "Geometry/Mesh/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::TestNS { template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> struct ClearSingletons; } // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief This class is used to create and retrieve GodOfDof objects.
     *
     * GodOfDof objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * GodOfDof object given its unique id (which is the one that appears in the input data file).
     *
     */
    class GodOfDofManager : public Utilities::Singleton<GodOfDofManager>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        friend struct MoReFEM::TestNS::ClearSingletons;

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::MeshNS::Tag;

        //! Convenient alias.
        using storage_type = std::unordered_map<MeshNS::unique_id, GodOfDof::shared_ptr>;

      public:
        /*!
         * \brief Create a \a GodOfDof object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydetails doxygen_hide_mpi_param
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data,
                    const Wrappers::Mpi& mpi);


        /*!
         * \brief Create a new GodOfDof object from a \a Mesh.
         *
         * Mesh is not a typo here: there is one god of dof per mesh and so GodOfDofs are created by tracking mesh
         * sections in the input data.
         *
         * This  method should NOT be used directly: it is introduced for the sake of tests. Ue instead the template
         * namesake method.
         *
         * \param[in,out] mesh \a Mesh to which the \a GodOfDof to be created is related.
         * \copydetails doxygen_hide_mpi_param
         *
         */
        void Create(const Wrappers::Mpi& mpi, Mesh& mesh);


        //! Fetch the god of dof object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{GodOfDof}
        const GodOfDof& GetGodOfDof(MeshNS::unique_id unique_id) const;

        //! Fetch the god of dof object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{GodOfDof}
        GodOfDof& GetNonCstGodOfDof(MeshNS::unique_id unique_id);

        //! Fetch the god of dof object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{GodOfDof}
        GodOfDof::shared_ptr GetGodOfDofPtr(MeshNS::unique_id unique_id) const;

        //! Fetch the god of dof object associated with \a Mesh.
        //! \param[in] mesh Mesh associated to the \a GodOfDof (by construct both share the same unique id).
        const GodOfDof& GetGodOfDof(const Mesh& mesh) const;

        //! Access to the storage.
        const storage_type& GetStorage() const noexcept;


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        GodOfDofManager();

        //! Destructor.
        virtual ~GodOfDofManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<GodOfDofManager>;
        ///@}

        //! Non constant access to the storage.
        storage_type& GetNonCstStorage() noexcept;

        //! \copydoc doxygen_hide_manager_clear
        void Clear();

      private:
        //! Store the god of dof objects by their unique identifier.
        storage_type storage_;
    };


    //! Clear temporary data for each god of dof.
    void ClearGodOfDofTemporaryData();


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOFMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
