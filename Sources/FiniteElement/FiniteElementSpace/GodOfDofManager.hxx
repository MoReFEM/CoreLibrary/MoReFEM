// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOFMANAGER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOFMANAGER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/InputData/Concept.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void GodOfDofManager ::Create(const IndexedSectionDescriptionT&,
                                  const ModelSettingsT&,
                                  const InputDataT&,
                                  const Wrappers::Mpi& mpi)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        const auto unique_id = MeshNS::unique_id{ section_type::GetUniqueId() };

        auto& mesh = Internal::MeshNS::MeshManager::GetInstance().GetNonCstMesh(unique_id);

        Create(mpi, mesh);
    }


    inline const GodOfDof& GodOfDofManager::GetGodOfDof(MeshNS::unique_id unique_id) const
    {
        return *(GetGodOfDofPtr(unique_id));
    }


    inline GodOfDof& GodOfDofManager::GetNonCstGodOfDof(MeshNS::unique_id unique_id)
    {
        return const_cast<GodOfDof&>(GetGodOfDof(unique_id));
    }


    inline const GodOfDof& GodOfDofManager::GetGodOfDof(const Mesh& mesh) const
    {
        return GetGodOfDof(mesh.GetUniqueId());
    }


    inline auto GodOfDofManager::GetStorage() const noexcept -> const storage_type&
    {
        return storage_;
    }


    inline auto GodOfDofManager::GetNonCstStorage() noexcept -> storage_type&
    {
        return const_cast<storage_type&>(GetStorage());
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOFMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
