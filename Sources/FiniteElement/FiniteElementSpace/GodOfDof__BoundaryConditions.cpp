// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <iterator>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/BoundaryConditions/Internal/DofStorage.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: keep

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    namespace // anonymous
    {


        // Very high value for penalization.
        constexpr const double very_high_value = 1.e30;


    } // namespace


    // #1581 First attempt, dropped because I realized refactoring of boundary condition can wait (a bit) as I
    // understood the issue I had with Stokes model results from prepartitioned data.
    //
    //    void GodOfDof::ExperimentalDetermineBCNodeBearer(const DirichletBoundaryCondition& boundary_condition)
    //    {
    //        decltype(auto) felt_space_list = GetFEltSpaceList();
    //
    //        decltype(auto) domain = boundary_condition.GetDomain();
    //
    //        Purpose: extract the full list of interfaces
    //        for (const auto& felt_space_ptr : felt_space_list)
    //        {
    //            assert(!(!felt_space_ptr));
    //            const auto& felt_space = *felt_space_ptr;
    //            TODO: Add role on processor here!
    //            decltype(auto) local_felt_space_list = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace(domain);
    //
    //        }
    //
    //        then keep the node bearer that are on those interfaces
    //        Open question: separate processor wise and ghost?
    //
    //
    //    }


    void GodOfDof::SetBoundaryConditions()
    {
        const auto& boundary_condition_list = DirichletBoundaryConditionManager::GetInstance().GetList();

        const auto& numbering_subset_list = GetNumberingSubsetList();

        const auto& mpi = GetMpi();

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            // Beware: pointer itself is constant, but underlying object is modified!
            for (const auto& boundary_condition_ptr : boundary_condition_list)
            {
                assert(!(!boundary_condition_ptr));
                auto& boundary_condition = *boundary_condition_ptr;

                const auto& dof_list = boundary_condition.GetDofList();

                Dof::vector_shared_ptr relevant_dof_for_current_bc_list;
                relevant_dof_for_current_bc_list.reserve(dof_list.size());

                Dof::vector_shared_ptr relevant_ghost_dof_for_current_bc_list;
                relevant_ghost_dof_for_current_bc_list.reserve(dof_list.size());


                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    const auto& dof = *dof_ptr;

                    // Keep only the dofs that are encompassed by current numbering subset.
                    if (!dof.IsInNumberingSubset(numbering_subset))
                        continue;

                    // We keep on purpose ghost ones here. But in #1581 this might be settled differently.
                    relevant_dof_for_current_bc_list.push_back(dof_ptr);
                }

                // Fill appropriately the storage of dofs related to the pair boundary condition/numbering subset.
                const bool is_not_empty = !relevant_dof_for_current_bc_list.empty();

                const bool is_relevant_for_any_processor = mpi.AllReduce(is_not_empty, Wrappers::MpiNS::Op::LogicalOr);

                if (is_not_empty)
                {
                    assert(is_relevant_for_any_processor
                           && "If current processor is relevant reduction should "
                              "automatically be as well...");
                    boundary_condition.SetDofListForNumberingSubset(numbering_subset,
                                                                    std::move(relevant_dof_for_current_bc_list));
                } else if (is_relevant_for_any_processor)
                    boundary_condition.ConsiderNumberingSubset(numbering_subset);
            }
        }
    }


    Dof::vector_shared_ptr GodOfDof::GetBoundaryConditionDofList(const NumberingSubset& numbering_subset)
    {
        const auto& boundary_condition_list = DirichletBoundaryConditionManager::GetInstance().GetList();

        Dof::vector_shared_ptr ret;

        for (const auto& boundary_condition_ptr : boundary_condition_list)
        {
            assert(!(!boundary_condition_ptr));
            const auto& local_dof_storage = boundary_condition_ptr->GetDofStorage(numbering_subset);

            const auto& dof_list = local_dof_storage.GetDofList();

            std::ranges::copy(dof_list, std::back_inserter(ret));
        }

        return ret;
    }


    void GodOfDof::ApplyPseudoElimination(const DirichletBoundaryCondition& dirichlet_bc, GlobalMatrix& matrix)
    {
        const auto& row_numbering_subset = matrix.GetRowNumberingSubset();

        assert(dirichlet_bc.IsNumberingSubset(row_numbering_subset));

        // Note: ZeroRows is collective so each processor must call it, even if there are no rows to zero
        // on that processor.
        const auto& dof_storage = dirichlet_bc.GetDofStorage(row_numbering_subset);

        const auto& dof_index_list = dof_storage.GetProgramWiseDofIndexList();

        matrix.ZeroRows(dof_index_list, 1.);
    }


    void GodOfDof::ApplyPenalization(const DirichletBoundaryCondition& dirichlet_bc, GlobalMatrix& matrix)
    {
        const auto& row_numbering_subset = matrix.GetRowNumberingSubset();

        assert(dirichlet_bc.IsNumberingSubset(row_numbering_subset));

        const auto& dof_storage = dirichlet_bc.GetDofStorage(row_numbering_subset);
        const auto& dof_index_list = dof_storage.GetProgramWiseDofIndexList();

        // \todo SetValues should be used here!
        for (auto program_wise_index : dof_index_list)
            matrix.SetValue(program_wise_index, program_wise_index, very_high_value, INSERT_VALUES);
    }


    void GodOfDof::ApplyPenalization(const DirichletBoundaryCondition& dirichlet_bc, GlobalVector& vector)
    {
        const auto& numbering_subset = vector.GetNumberingSubset();

        assert(dirichlet_bc.IsNumberingSubset(numbering_subset));

        const auto& dof_storage = dirichlet_bc.GetDofStorage(numbering_subset);
        const auto& dof_index_list = dof_storage.GetProcessorWiseDofIndexList();
        decltype(auto) dof_value_list = dof_storage.GetDofValueList();

        assert(dof_index_list.size() == dof_value_list.size());
        const auto Nbc_value = dof_value_list.size();

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector);

#ifndef NDEBUG
            const auto size = content.GetSize();
#endif // NDEBUG

            for (auto i = 0UL; i < Nbc_value; ++i)
            {
                const auto processor_wise_index = dof_index_list[i];
                assert(processor_wise_index < size.Get());
                content[vector_processor_wise_index_type{ processor_wise_index }] = dof_value_list[i] * very_high_value;
            }
        }

        vector.UpdateGhosts();
    }


    void GodOfDof::ApplyPseudoElimination(const DirichletBoundaryCondition& dirichlet_bc, GlobalVector& vector)
    {
        const auto& numbering_subset = vector.GetNumberingSubset();

        assert(dirichlet_bc.IsNumberingSubset(numbering_subset));
        const auto& dof_storage = dirichlet_bc.GetDofStorage(numbering_subset);

        const auto& dof_index_list = dof_storage.GetProgramWiseDofIndexList();

        // VecSetValues is not collective: no need to call it on a processor without any dof related to the
        // boundary condition.
        if (dof_index_list.empty())
            return;

        const auto& value_list = dof_storage.GetDofValueList();

        vector.SetValues(dof_index_list, value_list.data(), INSERT_VALUES);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
