// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <cstdint>
#include <iostream>
#include <iterator>
#include <memory>
#include <numeric>
#include <optional>
#include <sstream>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

// IWYU pragma: no_include "SetIndex.hpp"

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: keep
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/IdentifyGhostNodeBearerFromCoordsMatching.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/AssignGeomEltToProcessor.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Partition.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/ReduceToProcessorWise.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/SetIndex.hpp" // IWYU pragma: export // IWYU pragma: keep

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::Internal { class Parallelism; }
namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    namespace // anonymous
    {


        //! Print information about the partition that has been performed.
        void PrintInformation(const FEltSpace::vector_unique_ptr& felt_space_list,
                              const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                              const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                              std::size_t Nprogram_wise_dof,
                              std::size_t Nprocessor_wise_dof,
                              const Wrappers::Mpi& mpi);


        void CreateNodeBearerListHelper(const LocalFEltSpacePerRefLocalFEltSpace& felt_storage,
                                        NodeBearer::vector_shared_ptr& node_bearer_for_current_god_of_dof,
                                        Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer);


        void ProduceDofList(const NodeBearer::vector_shared_ptr& node_bearer_list, Dof::vector_shared_ptr& dof_list);

        DofNS::program_wise_index FirstProgramWiseIndexOnRank(const Wrappers::Mpi& mpi,
                                                              const Internal::FEltSpaceNS::NdofHolder& Ndof_holder,
                                                              const NumberingSubset& numbering_subset);

        void FillGhostDofProgramWiseIndexes(const Wrappers::Mpi& mpi,
                                            const NumberingSubset& numbering_subset,
                                            const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                            const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);

        /*!
         * \brief Share with other ranks how many \a Dof will be sent by \a sender_rank.
         *
         * This is a mpi-collective function: must be sent for each mpi ranks, including the \a sender_rank.
         *
         * \param[in] Ndof_to_send_for_current_rank For the current mpi rank, how many dofs will be sent to other ranks.
         * The position in the vector represents the rank (e.g. second element of the vector is for rank 1); the rank
         * itself is also present (but should logically be zero!)
         *
         * \param[out] Ndof_sent_by_sender_rank_for_current_rank  Number of dofs sent by \a sender_rank for current rank.
         * This is used to perform sanity checks but the bulk of the work is to share all the data, not compute this
         * returning value; that's the reason it's not a return value.
         */
        void ShareNdofSentToEachRank(const Wrappers::Mpi& mpi,
                                     rank_type sender_rank,
                                     const std::vector<std::size_t>& Ndof_to_send_for_current_rank,
                                     std::size_t& Ndof_sent_by_sender_rank_for_current_rank);

        /*!
         * \brief Just iterate over \a ghosted_dof_index_list_to_send_to_each_rank and gives how many elements handled by current rank
         * are expected for each rank.
         *
         * \return Each index is the rank of the processor to which data is to be sent.
         */
        std::vector<std::size_t> ComputeNdofToSentToEachRank(
            const std::vector<std::vector<DofNS::program_wise_index>>& ghosted_dof_index_list_to_send_to_each_rank);


        /*!
         * \brief Generate for a given \a NodeBearer the list of all the dofs inside a \a numbering_subset .
         *
         * \param[in] processor_wise_ghosted_node_bearer \a NodeBearer identified to be handled by current rank AND which
         * information is ghosted by at least one other rank.
         *
         * \return The list of program-wise dof indexes within the \a numbering_subset for \a processor_wise_node_bearer.
         */
        std::vector<DofNS::program_wise_index> GenerateGhostedDofIndexList(const NodeBearer& processor_wise_node_bearer,
                                                                           const NumberingSubset& numbering_subset);


        /*!
         * \brief Prepare the list of program-wise dof indexes for a given \a NumberingSubset to send to other ranks.
         *
         * \attention The \a Dof sent are all processor-wise, not ghost!
         *
         * \param[in] processor_wise_node_bearer_list List of all processor-wise \a NodeBearer
         * \param[in] numbering_subset \a NumberingSubset considered; program-wise indexes are
         * computed within this one and \a NodeBearer not inside it are skipped.
         *
         * \return The outer vector is per rank: index \a i  represents the \a i -th rank. Inner vector is the list of
         * program-wise indexes to send to the given rank. An index is added for a given rank only if this rank is
         * marked as a ghost in the list returned by \a NodeBearer::GetGhostProcessorList().
         */
        std::vector<std::vector<DofNS::program_wise_index>> GenerateGhostedDofIndexListToSendToEachRank(
            const Wrappers::Mpi& mpi,
            const NumberingSubset& numbering_subset,
            const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list);


        /*!
         * \brief Sift through \a ghost_node_bearer_list and sort ghost \a Dofs that are in \a numbering_subset
         * so that each is in a vector related to the rank that owns the \a Dof.
         *
         * \param[in] ghost_node_bearer_list List of all ghost \a NodeBearer on current rank
         *
         * \return Outer vector is for all ranks involved. Inner vector is all the ghost \a Dof that are primarily owned
         * by the rank which shares index with outer vector (e.g. index 0 of outer vector contains all the ghost which
         * are handled by rank 0) and that are within \a numbering_subset.
         *
         */
        std::vector<Dof::vector_shared_ptr>
        GenerateGhostDofListPerRank(const Wrappers::Mpi& mpi,
                                    const NumberingSubset& numbering_subset,
                                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);


        /*!
         * \brief Check the data received from other ranks and the one computed on current one are in agreement.
         *
         * We are at a really low level here and the code performs quite tricky operations: basically we order
         * the ghost dof on current rank in a very specific order, and then collect data from other ranks
         * (program-wise indexes for a very specific numbering subset) that will be use to set properly
         * indexes for the local \a Dof objects in the ghost dof list for current rank. Obviously it requires
         * very careful agencing of data to ensure we correctly set the right indexes for the right dofs
         * (but if we do not it will be glaring as parallel runs won't yield same result as sequential ones).
         *
         * A very simplistic check that will rule out many possible bugs is just to check the number of data
         * received is the same as the amount expected; that is the role of this funcion.
         */
        void CheckConsistency(const Wrappers::Mpi& mpi,
                              rank_type sender_rank,
                              std::size_t Ndof_sent_by_sender_rank_for_current_rank,
                              std::size_t Nghost_dof_list_primarily_managed_by_sender_rank);

        /*!
         * \brief Set the program-wise indexes related to \a numbering_subset of the ghost \a Dof of current rank
         * - these indexes are fetched and exchanged by Mpi in this very function.
         *
         * In previous functions the data to send were prepared and there were some communications between ranks
         * about the size of the data to share, but it's really in this function that the bulk of mpi communication is
         * performed.
         *
         * See the length explanation in \a CheckConsistency - the complex operation described there is performed
         * in the current function.
         */
        void ShareGhostDofIndexes(
            const Wrappers::Mpi& mpi,
            rank_type sender_rank,
            const NumberingSubset& numbering_subset,
            const std::vector<std::vector<DofNS::program_wise_index>>& ghosted_dof_index_list_to_send_to_each_rank,
            const Dof::vector_shared_ptr& ghost_dof_list_primarily_managed_by_sender_rank);


        /*!
         * \brief The class holding the method that will actually compute the dof indexes for all the nodes of a list.
         *
         * There is a struct to ease friendship declaration.
         *
         */
        struct ComputeDofIndexesHelper final
        {

            enum class in_numbering_subset : std::uint8_t { yes, no };

            /*!
             * \brief Static method that does the actual work.
             *
             * \tparam TypeDofIndexT Whether we are computing program_wise or processor (or ghost)-wise indexes.
             * \tparam DoProduceDofListT Whether \a complete_dof_list is filled or not.
             *
             * \param[in] node_bearer_list List of node bearers for which dof indexes are computed. It is expected the
             * structure of the node bearers (its nodes, the exact number of dofs) is already initialized.
             * \param[in] dof_numbering_scheme When considering a vectorial unknown, this argument reflects the choice of
             * numbering. Two possibilities at the moment: components of a same Node are given contiguous indexes or on
             * the contrary all dofs related to a same component (for instance displacement/x) are contiguous, then all
             * of the following component.
             * \param[in,out] current_dof_index In input, the starting point of the
             * numbering. In output, the highest dof index attributed. The reason for this is simply for processor-wise
             * and ghosts: there are two consecutive calls, the first on node on processor, the second on ghost nodes,
             * and the numbering of ghosts is expected to begin where the one of node ended. As this is a low-level
             * function there are no strong type: this is used by higher level functions for both processor-wise and
             * program-wise indexes.
             * \param[in] numbering_subset_ptr Pointer to a numbering subset if computation is performed for a numbering subset. If
             * not, put nullptr and no numbering subset filtering will be applied.
             * \param[out] complete_dof_list List of
             * all dofs created, in the order of their creation. Only if DoProduceDofListT is yes.
             */
            template<in_numbering_subset IndexInNumberingSubsetT, RoleOnProcessor RoleOnProcessorT>
            static void Perform(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                DofNumberingScheme dof_numbering_scheme,
                                std::size_t& current_dof_index,
                                const NumberingSubset::const_shared_ptr& numbering_subset_ptr,
                                std::optional<DofNS::program_wise_index> first_program_wise_index = std::nullopt);
        };


    } // namespace


    void GodOfDof::SetFEltSpaceList(FEltSpace::vector_unique_ptr&& felt_space_list)
    {
        assert(!HasInitBeenCalled() && "Must be called only once per GodOfDof!");

#ifndef NDEBUG
        has_init_been_called_ = true;

        assert(std::ranges::none_of(felt_space_list, Utilities::IsNullptr<FEltSpace::unique_ptr>));
#endif // NDEBUG

        felt_space_list_ = std::move(felt_space_list);
    }


    void GodOfDof::InitOutputDirectories(const FilesystemNS::Directory& output_directory)
    {
        decltype(auto) numbering_subset_list = ComputeNumberingSubsetList();

        output_directory_storage_ =
            std::make_unique<Internal::GodOfDofNS::OutputDirectoryStorage>(output_directory, numbering_subset_list);

        output_directory_wildcard_storage_ = std::make_unique<Internal::GodOfDofNS::OutputDirectoryStorage>(
            output_directory, numbering_subset_list, Internal::GodOfDofNS::wildcard_for_rank::yes);
    }


    Internal::FEltSpaceNS::MatchInterfaceNodeBearer GodOfDof::InitNodeBearers()
    {
        const auto& mpi = GetMpi();

        // Create the nodes for all finite element spaces.
        auto match_interface_node_bearer = CreateNodeBearers();

        auto& node_bearer_list = GetNonCstProcessorWiseNodeBearerList();

        // Compute the required elements to perform the partition. Parmetis call for instance is done there.
        // Node bearer list is still the same length in output (reduction not yet done) but its elements are sort
        // differently: those to be on first processor comes first, then those on second one, and so on...
        // Program-wise numbering is also applied: each of them gets as program-wise index its position in the new
        // vector.
        if (mpi.Nprocessor<int>() > 1)
        {
            Internal::FEltSpaceNS::PreparePartition(mpi, GetFEltSpaceList(), node_bearer_list);
        } else
        {
            for (auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                node_bearer_ptr->SetProcessor(rank_type{ 0UL });
            }
        }

        return match_interface_node_bearer;
    }


    void
    GodOfDof::Reduce(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer,
                     std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id,
                                        Internal::FEltSpaceNS::connectivity_type>& connectivity_per_numbering_subset)
    {
        decltype(auto) mpi = GetMpi();
        const auto rank = mpi.GetRank();

        // Assign a processor for each \a GeometricElt.
        const Internal::FEltSpaceNS::AssignGeomEltToProcessor assign_geom_elt_to_processor(
            *this, mpi.Nprocessor(), match_interface_node_bearer);

        // Reduce the list of node bearers to the ones present on processor.
        // Pattern computation will correctly address those not in the list as it will query finite elements to find
        // the adequate columns to fill with non-zero.
        Internal::FEltSpaceNS::ReduceNodeBearerList(rank, GetNonCstProcessorWiseNodeBearerList());

        // - Reduce the list of finite elements in each finite element space to the processor-wise ones only.
        // - Compute the ghost node bearers for each finite element space.
        // - Compute the reduced mesh, limited to processor-wise geometric elements.
        if (mpi.Nprocessor<int>() > 1)
            ReduceToProcessorWise(
                assign_geom_elt_to_processor, match_interface_node_bearer, connectivity_per_numbering_subset);
    }


    void GodOfDof ::ComputeGhostNodeBearerListForCoordsMatching(
        const MeshNS::InterpolationNS::CoordsMatching& coords_matching,
        const NumberingSubset& source_numbering_subset,
        Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        decltype(auto) mpi = GetMpi();

        assert(coords_matching.GetSourceMeshId() == GetUniqueId());

        Internal::NodeBearerNS::node_bearer_per_coords_index_list_type ghost_from_coords_matching_node_bearer_list;
        if (mpi.Nprocessor<int>() > 1)
        {
            ghost_from_coords_matching_node_bearer_list =
                Internal::GodOfDofNS::IdentifyGhostNodeBearerFromCoordsMatching(coords_matching);

            match_interface_node_bearer.SetFromCoordsMatchingNodeBearers(
                mpi, source_numbering_subset, std::move(ghost_from_coords_matching_node_bearer_list));
        }
    }


    void GodOfDof::CreateNodesAndDofs(Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        decltype(auto) mpi = GetMpi();

        decltype(auto) felt_space_list = GetFEltSpaceList();
        decltype(auto) node_bearer_list = GetProcessorWiseNodeBearerList();
        decltype(auto) numbering_subset_list = GetNumberingSubsetList();

        {
            const auto& god_of_dof = *this;
            Internal::FEltSpaceNS::CreateNodes(god_of_dof, match_interface_node_bearer);
        }

        ProduceDofList(node_bearer_list, GetNonCstProcessorWiseDofList());

        ProduceDofList(GetGhostNodeBearerList(), GetNonCstGhostDofList());

        Ndof_holder_ =
            std::make_unique<Internal::FEltSpaceNS::NdofHolder>(mpi, GetProcessorWiseDofList(), numbering_subset_list);

        ComputeDofIndexes();

        matrix_pattern_per_numbering_subset_ = Internal::FEltSpaceNS::ComputeMatrixPattern::Perform(
            felt_space_list, node_bearer_list, numbering_subset_list, GetNdofHolder());

        {
            for (const auto& felt_space_ptr : felt_space_list)
                felt_space_ptr->ComputeDofList();
        }

        {
            const auto& boundary_condition_list = DirichletBoundaryConditionManager::GetInstance().GetList();

            // Beware: pointer itself is constant, but underlying object is modified!
            for (const auto& boundary_condition_ptr : boundary_condition_list)
            {
                assert(!(!boundary_condition_ptr));
                boundary_condition_ptr->ComputeDofList();
            }
        }
    }


    void GodOfDof::FinalizeInitialization(DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                                          const Internal::Parallelism* parallelism)
    {
#ifndef NDEBUG
        do_consider_processor_wise_local_2_global_ = do_consider_processor_wise_local_2_global;
#endif // NDEBUG

        decltype(auto) mpi = GetMpi();

        decltype(auto) felt_space_list = GetFEltSpaceList();
        decltype(auto) node_bearer_list = GetProcessorWiseNodeBearerList();

        // Finish the reduction to processor-wise: remove dof in boundary conditions that are not processor-wise.
        if (mpi.Nprocessor<int>() > 1)
        {
            // Reduce boundary conditions to processor-wise and ghost ones.
            // Beware: container itself is constant, but the elements it contains are modified by current method!
            const auto& bc_list = DirichletBoundaryConditionManager::GetInstance().GetList();

            // Beware: pointer itself is constant, but underlying object is modified!
            for (const auto& bc_ptr : bc_list)
            {
                assert(!(!bc_ptr));
                auto& bc = *bc_ptr;

                // God of dof should only reduce its own boundary conditions.
                if (bc.GetDomain().GetMeshIdentifier() == GetUniqueId())
                    bc.ShrinkToProcessorWise(*this);
            }
        }

        PrintInformation(
            felt_space_list, node_bearer_list, GetGhostNodeBearerList(), NprogramWiseDof(), NprocessorWiseDof(), mpi);

        // Init the local2global for each local finite element space in each finite element space.
        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            felt_space_ptr->InitLocal2Global(do_consider_processor_wise_local_2_global);
        }

        // Prepare if relevant the data to compute movemeshes in finite element spaces.
        const auto mesh_dimension = GetMesh().GetDimension();

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            auto& felt_space = *felt_space_ptr;

            // It makes little sense to move only contours of the mesh!
            if (mesh_dimension != felt_space.GetDimension())
                continue;

            felt_space.SetMovemeshData();
        }

        SetBoundaryConditions();

        // Prepare the output directories.
        PrepareOutput(parallelism);

#ifndef NDEBUG
        if (mpi.Nprocessor<int>() == 1)
            assert(NprocessorWiseDof() == NprogramWiseDof());
        else
        {
            assert(mpi.AllReduce(NprocessorWiseDof(), Wrappers::MpiNS::Op::Sum) == NprogramWiseDof());
            //            assert(mpi.AllReduce(NnodeBearer(), Wrappers::MpiNS::Op::Sum) == Nprogram_wise_node_bearer);
        }
#endif // NDEBUG
    }


    Internal::FEltSpaceNS::MatchInterfaceNodeBearer GodOfDof::CreateNodeBearers()
    {
        Internal::FEltSpaceNS::MatchInterfaceNodeBearer ret;

        const auto& felt_space_list = GetFEltSpaceList();

        {
            auto& node_bearer_list = GetNonCstProcessorWiseNodeBearerList(); // no processor-wise or ghost at this stage
                                                                             // - current call is expected to be before
                                                                             // reduction.
            assert(node_bearer_list.empty());

            for (const auto& felt_space_ptr : felt_space_list)
            {
                assert(!(!felt_space_ptr));

                CreateNodeBearerListHelper(
                    felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>(),
                    node_bearer_list,
                    ret);

                // This one may really be needed when we are working from preprocessed data (in a normal run the
                // method is called before reduction and therefore the first argument is empty).
                CreateNodeBearerListHelper(
                    felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::ghost>(),
                    node_bearer_list,
                    ret);
            }
        }

        // Constitute here the list of indexes concerned by BC.
        // \todo #619 This is rather awkward: it would be better to set completely the boundary condition
        // after the reduction to processor-wise, at the beginning of the method SetBoundaryCondition().
        // However it means severing the dependency here upon 'ret'; actually only two data attributes are
        // used in the method ComputeNodeBearerListOnBoundary() and it should be relatively easy to avoid this.
        const auto& mesh = GetMesh();

        const auto& boundary_condition_list = DirichletBoundaryConditionManager::GetInstance().GetList();

        std::unordered_map<GeometricElt::shared_ptr, NodeBearer::vector_shared_ptr> bc_data;

        // Beware: pointer itself is constant, but underlying object is modified!
        for (const auto& boundary_condition_ptr : boundary_condition_list)
        {
            assert(!(!boundary_condition_ptr));
            auto& boundary_condition = *boundary_condition_ptr;

            auto&& node_bearer_list = ret.ComputeNodeBearerListOnBoundary(mesh, boundary_condition, bc_data);

            if (!node_bearer_list.empty()) // Happen for instance when several meshes involved in the model.
                boundary_condition.SetNodeBearerList(std::move(node_bearer_list));
        }

        ret.SetBoundaryConditionData(std::move(bc_data));

        return ret;
    }


    void GodOfDof::ComputeDofIndexes()
    {
        decltype(auto) node_bearer_list = GetProcessorWiseNodeBearerList();
        decltype(auto) ghost_node_bearer_list = GetGhostNodeBearerList();
        const auto& numbering_subset_list = GetNumberingSubsetList();

        decltype(auto) Ndof_holder = GetNdofHolder();
        decltype(auto) mpi = GetMpi();

        {
            std::size_t current_processor_wise_or_ghost_dof_index = 0UL;

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::no,
                                              RoleOnProcessor::processor_wise>(
                node_bearer_list,
                DofNumberingScheme::contiguous_per_node,
                current_processor_wise_or_ghost_dof_index,
                nullptr);

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::no, RoleOnProcessor::ghost>(
                ghost_node_bearer_list,
                DofNumberingScheme::contiguous_per_node,
                current_processor_wise_or_ghost_dof_index,
                nullptr);
        }

        assert(IsConsistentOverRanks(mpi, numbering_subset_list));

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            std::size_t current_processor_wise_or_ghost_dof_index = 0UL;

            const auto first_program_wise_index_on_rank =
                FirstProgramWiseIndexOnRank(mpi, Ndof_holder, *numbering_subset_ptr);

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::yes,
                                              RoleOnProcessor::processor_wise>(
                node_bearer_list,
                DofNumberingScheme::contiguous_per_node,
                current_processor_wise_or_ghost_dof_index,
                numbering_subset_ptr,
                first_program_wise_index_on_rank);

            ComputeDofIndexesHelper ::Perform<ComputeDofIndexesHelper::in_numbering_subset::yes,
                                              RoleOnProcessor::ghost>(ghost_node_bearer_list,
                                                                      DofNumberingScheme::contiguous_per_node,
                                                                      current_processor_wise_or_ghost_dof_index,
                                                                      numbering_subset_ptr,
                                                                      first_program_wise_index_on_rank);


            FillGhostDofProgramWiseIndexes(mpi, numbering_subset, node_bearer_list, ghost_node_bearer_list);
        }
    }


    void GodOfDof ::ReduceToProcessorWise(
        const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
        Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer,
        std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, Internal::FEltSpaceNS::connectivity_type>&
            connectivity_per_numbering_subset)
    {
        const auto& mpi = GetMpi();

        assert(GetMpi().Nprocessor<int>() > 1 && "Should not be called otherwise!");
        assert(GetGhostNodeBearerList().empty());

        Internal::FEltSpaceNS::ReduceToProcessorWise::Perform(mpi,
                                                              assign_geom_elt_to_processor,
                                                              GetFEltSpaceList(),
                                                              GetProcessorWiseNodeBearerList(),
                                                              connectivity_per_numbering_subset,
                                                              match_interface_node_bearer,
                                                              GetNonCstGhostNodeBearerList(),
                                                              GetNonCstMesh());

        decltype(auto) numbering_subset_list = GetNumberingSubsetList();

        const auto mpi_rank = mpi.GetRank();

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));

            NodeBearer::vector_shared_ptr processor_wise_node_bearer_list;
            NodeBearer::vector_shared_ptr ghost_node_bearer_list;

            auto it = connectivity_per_numbering_subset.find(numbering_subset_ptr->GetUniqueId());

            if (it != connectivity_per_numbering_subset.cend())
            {
                processor_wise_node_bearer_list.reserve(GetProcessorWiseNodeBearerList().size());
                ghost_node_bearer_list.reserve(GetGhostNodeBearerList().size());

                const auto& connectivity_for_numbering_subset = it->second;

                for (const auto& [processor_wise_node_bearer, connected_node_bearer_list] :
                     connectivity_for_numbering_subset)
                {
                    processor_wise_node_bearer_list.push_back(processor_wise_node_bearer);

                    for (const auto& connected : connected_node_bearer_list)
                    {
                        if (connected->GetProcessor() != mpi_rank)
                            ghost_node_bearer_list.push_back(connected);
                    }
                }
            }

            Utilities::EliminateDuplicate(processor_wise_node_bearer_list,
                                          Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                          Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());

            Utilities::EliminateDuplicate(ghost_node_bearer_list,
                                          Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                          Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());

            Internal::FEltSpaceNS::BroadcastGhostNodeBearerIndexList(
                mpi, *numbering_subset_ptr, processor_wise_node_bearer_list, ghost_node_bearer_list);
        }
    }


    namespace // anonymous
    {


        void PrintInformation(const FEltSpace::vector_unique_ptr& felt_space_list,
                              const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                              const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                              const std::size_t Nprogram_wise_dof,
                              const std::size_t Nprocessor_wise_dof,
                              const Wrappers::Mpi& mpi)
        {
            std::size_t Nfelt = 0;

            {
                for (const auto& felt_space_ptr : felt_space_list)
                {
                    const auto& felt_list_per_type =
                        felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

                    for (const auto& pair : felt_list_per_type)
                    {
                        const auto& local_felt_space_list = pair.second;

                        Nfelt += local_felt_space_list.size();
                    }
                }
            }

            const auto rank = mpi.GetRank();

            if (mpi.IsRootProcessor())
                std::cout << "Total (program-wise) number of dofs = " << Nprogram_wise_dof << '\n';

            std::cout << "On processor " << rank << " -> " << Nfelt << " finite elements, "
                      << processor_wise_node_bearer_list.size() << " node_bearers, " << Nprocessor_wise_dof << " dofs, "
                      << ghost_node_bearer_list.size() << " ghost node_bearers." << '\n';
        }


        DofNS::program_wise_index FirstProgramWiseIndexOnRank(const Wrappers::Mpi& mpi,
                                                              const Internal::FEltSpaceNS::NdofHolder& Ndof_holder,
                                                              const NumberingSubset& numbering_subset)
        {
            const auto Nprocessor_wise_index_per_rank =
                mpi.CollectFromEachProcessor(Ndof_holder.NprocessorWiseDof(numbering_subset));

            const auto begin = Nprocessor_wise_index_per_rank.cbegin();

            return DofNS::program_wise_index{ std::accumulate(
                begin, begin + mpi.GetRank<decltype(Nprocessor_wise_index_per_rank)::difference_type>(), 0UL) };
        }


        std::vector<DofNS::program_wise_index> GenerateGhostedDofIndexList(const NodeBearer& processor_wise_node_bearer,
                                                                           const NumberingSubset& numbering_subset)
        {
            assert(processor_wise_node_bearer.IsGhosted(numbering_subset)
                   && "If not function shouldn't have been called!");

            decltype(auto) node_list = processor_wise_node_bearer.GetNodeList();

            std::vector<DofNS::program_wise_index> ret;

            for (const auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;

                if (!node.IsInNumberingSubset(numbering_subset))
                    continue;

                decltype(auto) dof_list = node.GetDofList();

                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    ret.push_back(dof_ptr->GetProgramWiseIndex(numbering_subset));
                }
            }

            return ret;
        }


        std::vector<std::vector<DofNS::program_wise_index>> GenerateGhostedDofIndexListToSendToEachRank(
            const Wrappers::Mpi& mpi,
            const NumberingSubset& numbering_subset,
            const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list)
        {
            const auto Nprocessor = mpi.Nprocessor<std::size_t>();
            std::vector<std::vector<DofNS::program_wise_index>> ret(Nprocessor);

            for (const auto& node_bearer_ptr : processor_wise_node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;
                if (!node_bearer.IsGhosted(numbering_subset))
                    continue;

                const auto dof_index_list = GenerateGhostedDofIndexList(node_bearer, numbering_subset);

                decltype(auto) ghost_processor_list = node_bearer.GetGhostProcessorList(numbering_subset);

                const auto begin = dof_index_list.cbegin();
                const auto end = dof_index_list.cend();

                for (const auto processor : ghost_processor_list)
                {
                    assert(processor.Get() < ret.size());
                    auto& full_dof_index_list = ret[processor.Get()];
                    std::copy(begin, end, std::back_inserter(full_dof_index_list));
                }
            }
            return ret;
        }


        std::vector<Dof::vector_shared_ptr>
        GenerateGhostDofListPerRank(const Wrappers::Mpi& mpi,
                                    const NumberingSubset& numbering_subset,
                                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        {
            const auto Nprocessor = mpi.Nprocessor<std::size_t>();

            std::vector<Dof::vector_shared_ptr> ret(Nprocessor);

            for (const auto& node_bearer_ptr : ghost_node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;
                const auto processor = node_bearer.GetProcessor();
                assert(processor != mpi.GetRank());

                if (!node_bearer.IsInNumberingSubset(numbering_subset))
                    continue;

                decltype(auto) node_list = node_bearer.GetNodeList();

                Dof::vector_shared_ptr recipient_dof_list_for_node_bearer;

                for (const auto& node_ptr : node_list)
                {
                    assert(!(!node_ptr));
                    const auto& node = *node_ptr;

                    if (!node.IsInNumberingSubset(numbering_subset))
                        continue;

                    decltype(auto) dof_list = node.GetDofList();

                    std::ranges::copy(dof_list, std::back_inserter(recipient_dof_list_for_node_bearer));
                }

                assert(!recipient_dof_list_for_node_bearer.empty());

                assert(processor.Get() < ret.size());
                auto& recipient_dof_list = ret[processor.Get()];

                std::ranges::copy(recipient_dof_list_for_node_bearer,

                                  std::back_inserter(recipient_dof_list));
            }

            assert(mpi.GetRank<std::size_t>() < Nprocessor);
            assert(ret[mpi.GetRank<std::size_t>()].empty());

            return ret;
        }


        // NOLINTBEGIN(bugprone-easily-swappable-parameters)
        void FillGhostDofProgramWiseIndexes(const Wrappers::Mpi& mpi,
                                            const NumberingSubset& numbering_subset,
                                            const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                            const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        // NOLINTEND(bugprone-easily-swappable-parameters)
        {
            mpi.Barrier();

            const auto Nprocessor = mpi.Nprocessor();

            const auto ghosted_dof_index_list_to_send_to_each_rank =
                GenerateGhostedDofIndexListToSendToEachRank(mpi, numbering_subset, processor_wise_node_bearer_list);

            std::vector<Dof::vector_shared_ptr> ghost_dof_list_per_rank =
                GenerateGhostDofListPerRank(mpi, numbering_subset, ghost_node_bearer_list);

            const auto Ndof_to_send_for_current_rank =
                ComputeNdofToSentToEachRank(ghosted_dof_index_list_to_send_to_each_rank);

            mpi.Barrier();

            std::size_t Ndof_sent_by_sender_rank_for_current_rank{};
            for (auto sender_rank = rank_type{ 0UL }; sender_rank < Nprocessor; ++sender_rank)
            {
                ShareNdofSentToEachRank(
                    mpi, sender_rank, Ndof_to_send_for_current_rank, Ndof_sent_by_sender_rank_for_current_rank);
                const auto& ghost_dof_list_primarily_managed_by_sender_rank =
                    ghost_dof_list_per_rank[sender_rank.Get()];
                CheckConsistency(mpi,
                                 sender_rank,
                                 Ndof_sent_by_sender_rank_for_current_rank,
                                 ghost_dof_list_primarily_managed_by_sender_rank.size());

                assert(ghosted_dof_index_list_to_send_to_each_rank.size() == Nprocessor.Get());

                ShareGhostDofIndexes(mpi,
                                     sender_rank,
                                     numbering_subset,
                                     ghosted_dof_index_list_to_send_to_each_rank,
                                     ghost_dof_list_primarily_managed_by_sender_rank);
            }
        }


        void ProduceDofList(const NodeBearer::vector_shared_ptr& node_bearer_list, Dof::vector_shared_ptr& dof_list)
        {
            for (const auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;
                const auto& node_list = node_bearer.GetNodeList();

                for (const auto& node_ptr : node_list)
                {
                    assert(!(!node_ptr));
                    const auto& node = *node_ptr;

                    const auto& node_dof_list = node.GetDofList();

                    for (const auto& dof_ptr : node_dof_list)
                    {
                        assert(!(!dof_ptr));
                        dof_list.push_back(dof_ptr);
                    }
                }
            }
        }


        void CreateNodeBearerListHelper(const LocalFEltSpacePerRefLocalFEltSpace& felt_storage,
                                        NodeBearer::vector_shared_ptr& node_bearer_for_current_god_of_dof,
                                        Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer)
        {
            for (const auto& [ref_local_felt_space_ptr, local_felt_space_list] : felt_storage)
            {
                assert(!(!ref_local_felt_space_ptr));
                const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

                for (const auto& [geom_elt_index, local_felt_space_ptr] : local_felt_space_list)
                {
                    assert(!(!local_felt_space_ptr));
                    auto& local_felt_space = *local_felt_space_ptr;

                    match_interface_node_bearer.AddNodeBearerList(
                        ref_local_felt_space, node_bearer_for_current_god_of_dof, local_felt_space);
                }
            }
        }


        template<ComputeDofIndexesHelper::in_numbering_subset IndexInNumberingSubsetT, RoleOnProcessor RoleOnProcessorT>
        void ComputeDofIndexesHelper ::Perform(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                               DofNumberingScheme dof_numbering_scheme,
                                               std::size_t& current_dof_index,
                                               const NumberingSubset::const_shared_ptr& numbering_subset_ptr,
                                               std::optional<DofNS::program_wise_index> first_program_wise_index)
        {
            assert(dof_numbering_scheme == DofNumberingScheme::contiguous_per_node
                   && "Other schemes not yet implemented (need to be discussed in code meeting).");

#ifndef NDEBUG
            if constexpr (IndexInNumberingSubsetT == in_numbering_subset::yes)
                assert(!(!numbering_subset_ptr));
            else
                assert(numbering_subset_ptr == nullptr);
#endif // NDEBUG

            for (const auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;

                const auto& node_list = node_bearer.GetNodeList();

                for (const auto& node_ptr : node_list)
                {
                    assert(!(!node_ptr));
                    const auto& node = *node_ptr;

                    // Filter out nodes not encompassed by numbering subset if this quantity is
                    // relevant (otherwise it is nullptr...)
                    if constexpr (IndexInNumberingSubsetT == in_numbering_subset::yes)
                    {
                        assert(!(!numbering_subset_ptr));
                        if (!node.IsInNumberingSubset(*numbering_subset_ptr))
                            continue;
                    }

                    const auto& dof_list = node.GetDofList();

                    // Beware: pointer itself is constant, but underlying object is modified!
                    for (const auto& dof_ptr : dof_list)
                    {
                        assert(!(!dof_ptr));

                        auto& dof = *dof_ptr;

                        switch (IndexInNumberingSubsetT)
                        {
                        case in_numbering_subset::yes:
                            Internal::DofNS::SetIndex::ProcessorWiseOrGhostIndex(
                                *numbering_subset_ptr, DofNS::processor_wise_or_ghost_index{ current_dof_index }, dof);

                            if (RoleOnProcessorT == RoleOnProcessor::processor_wise)
                            {
                                assert(first_program_wise_index.has_value());
                                // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
                                Internal::DofNS::SetIndex::ProgramWiseIndex(
                                    *numbering_subset_ptr,
                                    first_program_wise_index.value() + DofNS::program_wise_index{ current_dof_index },
                                    dof);
                            }
                            break;
                        case in_numbering_subset::no:
                            Internal::DofNS::SetIndex::InternalProcessorWiseOrGhostIndex(
                                Internal::DofNS::internal_processor_wise_or_ghost_index{ current_dof_index }, dof);
                            break;
                        }

                        switch (dof_numbering_scheme)
                        {
                        case DofNumberingScheme::contiguous_per_node:
                            current_dof_index++;
                            break;
                        }
                    }
                }
            }
        }


        void ShareNdofSentToEachRank(const Wrappers::Mpi& mpi,
                                     const rank_type sender_rank,
                                     const std::vector<std::size_t>& Ndof_to_send_for_current_rank,
                                     std::size_t& Ndof_sent_by_sender_rank_for_current_rank)
        {
            const auto Nprocessor = mpi.Nprocessor<std::size_t>();
            const auto current_rank = mpi.GetRank();

            assert(Ndof_to_send_for_current_rank.size() == Nprocessor);
            assert(Ndof_to_send_for_current_rank[current_rank.Get()] == 0UL);

            std::vector<std::size_t> Ndof_to_send_for_sender_rank;

            if (sender_rank == current_rank)
                Ndof_to_send_for_sender_rank = Ndof_to_send_for_current_rank;
            else
                Ndof_to_send_for_sender_rank.resize(Nprocessor);

            mpi.Broadcast(Ndof_to_send_for_sender_rank, sender_rank);

            Ndof_sent_by_sender_rank_for_current_rank = Ndof_to_send_for_sender_rank[current_rank.Get()];
        }


        std::vector<std::size_t> ComputeNdofToSentToEachRank(
            const std::vector<std::vector<DofNS::program_wise_index>>& ghosted_dof_index_list_to_send_to_each_rank)
        {
            std::vector<std::size_t> ret(ghosted_dof_index_list_to_send_to_each_rank.size());

            std::ranges::transform(ghosted_dof_index_list_to_send_to_each_rank,

                                   ret.begin(),
                                   [](const auto& vector)
                                   {
                                       return vector.size();
                                   });

            return ret;
        }


        void CheckConsistency(const Wrappers::Mpi& mpi,
                              const rank_type sender_rank,
                              std::size_t Ndof_sent_by_sender_rank_for_current_rank,
                              std::size_t Nghost_dof_list_primarily_managed_by_sender_rank)
        {


            if (Ndof_sent_by_sender_rank_for_current_rank != Nghost_dof_list_primarily_managed_by_sender_rank)
            {
                std::ostringstream oconv;
                oconv << mpi.GetRankPrefix()
                      << " Discrepancy between the number of ghost dofs that are "
                         "primarily handled by processor "
                      << sender_rank << " - " << Nghost_dof_list_primarily_managed_by_sender_rank
                      << " were expected but sender rank actually sent " << Ndof_sent_by_sender_rank_for_current_rank
                      << " dof indexes." << '\n';

                throw Exception(oconv.str());
            }
        }


        void ShareGhostDofIndexes(
            const Wrappers::Mpi& mpi,
            rank_type sender_rank,
            const NumberingSubset& numbering_subset,
            const std::vector<std::vector<DofNS::program_wise_index>>& ghosted_dof_index_list_to_send_to_each_rank,
            const Dof::vector_shared_ptr& ghost_dof_list_primarily_managed_by_sender_rank)
        {
            const auto Nprocessor = mpi.Nprocessor();
            const auto current_rank = mpi.GetRank();
            const auto Nghost_dof_list_primarily_managed_by_sender_rank =
                ghost_dof_list_primarily_managed_by_sender_rank.size();

            for (auto recipient_rank = rank_type{ 0UL }; recipient_rank < Nprocessor; ++recipient_rank)
            {
                if (recipient_rank == sender_rank)
                    continue;

                if (current_rank == sender_rank)
                {
                    if (!ghosted_dof_index_list_to_send_to_each_rank[recipient_rank.Get()].empty())
                    {
                        mpi.SendContainer(recipient_rank,
                                          ghosted_dof_index_list_to_send_to_each_rank[recipient_rank.Get()]);
                    }
                } else if (current_rank == recipient_rank)
                {
                    if (Nghost_dof_list_primarily_managed_by_sender_rank > 0)
                    {
                        auto dof_index_list =
                            mpi.Receive<std::size_t>(sender_rank, Nghost_dof_list_primarily_managed_by_sender_rank);

                        assert(ghost_dof_list_primarily_managed_by_sender_rank.size()
                               == Nghost_dof_list_primarily_managed_by_sender_rank);
                        assert(ghost_dof_list_primarily_managed_by_sender_rank.size() == dof_index_list.size());

                        for (auto i = 0UL; i < Nghost_dof_list_primarily_managed_by_sender_rank; ++i)
                            Internal::DofNS::SetIndex::ProgramWiseIndex(
                                numbering_subset,
                                DofNS::program_wise_index{ dof_index_list[i] },
                                *ghost_dof_list_primarily_managed_by_sender_rank[i]);
                    }
                }
            }
        }


    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
