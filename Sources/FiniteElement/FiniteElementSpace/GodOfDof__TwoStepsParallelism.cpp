// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <__tree>

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <set>
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"                                    // IWYU pragma: keep
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    namespace // anonymous
    {

        /*!
         * \brief Helper class which stores SOME of the content of the GodOfDof prepartitioned file.
         *
         * Some date from this file are extracted directly by the functions that need them (for instance which key
         * depends directly from a numbering subset unique id).
         */
        struct PrepartitionedDataFileContent
        {
            PrepartitionedDataFileContent(::MoReFEM::Wrappers::Lua::OptionFile& god_of_dof_prepartitioned_data,
                                          const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                                          const Wrappers::Mpi& mpi);

            //! Program-wise index of the first \a NodeBearer on the current rank.
            std::size_t node_bearer_first_program_wise_index{};

            //! The \a NodeBearer list as indicated in the partition file: first letter stands for the interface, and
            //! related integer is the program-wise index for that interface. This list encompasses the \a NodeBearer on
            //! the local processor with the exception of the ghost ones.
            std::vector<std::string> interface_per_node_bearer;

            //! Same as \a interface_per_node_bearer for the ghost ones.
            std::vector<std::string> interface_per_ghost_node_bearer;

            //! List of program-wise indexes of ghost \a NodeBearer (these aren't continuous
            //! contrary to the ones for processor-wise, for which just \a first_program_wise_index is enough)
            std::vector<NodeBearerNS::program_wise_index_type> ghost_node_bearer_index_list;

            //! Mpi rank for each ghost \a NodeBearer
            std::vector<rank_type> ghost_node_bearer_mpi_rank_list;

            /*!
             * \brief Keep track of all the processors that ghost a given \a NodeBearer and a
             * given \a NumberingSubset
             *
             * Inner value is the list of all the processors that ghost the chosen combination of \a NodeBearer
             * and \a NumberingSubset.
             */
            std::unordered_map<NodeBearerNS::program_wise_index_type,
                               std::unordered_map<NumberingSubsetNS::unique_id, std::set<rank_type>>>
                ghost_processor_per_node_bearer_and_numbering_subset;
        };


        std::string MatchExceptionErrorMsg(const FilesystemNS::File& filename, std::string_view node_bearer_shorthand);


        struct MatchException : public Exception
        {


            /*!
             * \brief Constructor with simple message.
             *
             * \param[in] msg Message.
             * \copydoc doxygen_hide_source_location
             */
            explicit MatchException(const FilesystemNS::File& filename,
                                    std::string_view node_bearer_shorthand,
                                    std::source_location location = std::source_location::current());

            //! Destructor
            ~MatchException() override;

            //! \copydoc doxygen_hide_copy_constructor
            MatchException(const MatchException& rhs) = default;

            MatchException(MatchException&& rhs) = delete;
            MatchException& operator=(const MatchException& rhs) = delete;
            MatchException& operator=(MatchException&& rhs) = delete;
        };


        struct FetchResult
        {

            FetchResult(NodeBearerNS::program_wise_index_type a_node_bearer_index, bool a_is_ghost)
            : node_bearer_index(a_node_bearer_index), is_ghost(a_is_ghost)
            { }

            const NodeBearerNS::program_wise_index_type node_bearer_index;

            const bool is_ghost;
        };


        //! Class which purpose is to help reassign the original \a NodeBearer program-wise index, using to do so the
        //! underlying interface index.
        class Match
        {
          public:
            //! Constructor.
            //! \param[in] filename Path to the Lua option file which contains the god of dof prepartitioned data.
            Match(const FilesystemNS::File& filename,
                  const PrepartitionedDataFileContent& prepartitioned_data_file_content);

            /*!
             * \brief Fetch the \a NodeBearer program-wise index that should be associated to the given \a Interface.
             *
             * \return A \a FetchResult object.
             */
            [[nodiscard]] FetchResult Fetch(const Interface& interface) const;

          private:
            //! Do the treatment over processor-wise \a NodeBearer list.
            void ProcessProcessorWise(const std::vector<std::string>& selected_node_bearer_list);

            //! Do the treatment over ghost \a NodeBearer list.
            void ProcessGhost(const std::vector<std::string>& ghost_node_bearer_list,
                              const std::vector<NodeBearerNS::program_wise_index_type>& ghost_node_bearer_index_list);

            //! Return the proper list of interface depending on the chosen nature.
            [[nodiscard]] const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type,
                                                   NodeBearerNS::program_wise_index_type>&
            GetInterfaceMatch(InterfaceNS::Nature nature) const;

            //! 'Convert' a shorthand into a interface nature and its associated index.
            void ConvertShortHand(const std::string& shorthand,
                                  char& interface_type,
                                  ::MoReFEM::InterfaceNS::program_wise_index_type& interface_program_wise_index) const;

            //! Whether the \a NodeBearer is a ghost or not.
            //! The way to identify this is the index:
            //! If it is in [first_program_wise_index_,  first_program_wise_index_ + Nprocessor_wise_node_bearer_[, it
            //! is a processor-wise If not it is a ghost.
            [[nodiscard]] bool IsGhost(NodeBearerNS::program_wise_index_type node_bearer_index) const noexcept;

          private:
            //! Program-wise index for the first processor-wise \a NodeBearer (by construct their index is incremented
            //! and therefore all are contiguous).
            const NodeBearerNS::program_wise_index_type first_program_wise_index_on_processor_;

            //! Number of processor-wise \a NodeBearer.
            const std::size_t Nprocessor_wise_node_bearer_;

            //! Filename of the OptionFile.
            const FilesystemNS::File& filename_;

            //! Key: the program-wise index of the \a Vertex.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                coords_matching_;

            //! Key: the program-wise index of the \a Edge.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                edge_matching_;

            //! Key: the program-wise index of the \a Face.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                face_matching_;

            //! Key: the program-wise index of the \a Volume.
            //! Value: the program-wise index of the \a NodeBearer.
            std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearerNS::program_wise_index_type>
                volume_matching_;
        };


        /*!
         * \brief Split the \a NodeBearers read into processor-wise and ghost ones and renumber them to  match original
         * numbering.
         *
         * \param[in] node_bearer_list_before_processing The \a NodeBearer list reconstructed from partitioned data before
         * renumbering - this list contains both processor-wise and ghost ones.
         * \param[out] processor_wise_node_bearer_list The \a NodeBearer handled directly by current rank, sort the same way as in
         * original partition.
         * \param[out] ghost_node_bearer_list Same for ghosts.
         */
        // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
        void ProcessNodeBearerList(const GodOfDof& god_of_dof,
                                   const NodeBearer::vector_shared_ptr& node_bearer_list_before_processing,
                                   const PrepartitionedDataFileContent& prepartitioned_data_file_content,
                                   const Match& match,
                                   NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                   NodeBearer::vector_shared_ptr& ghost_node_bearer_list);


    } // namespace
    // NOLINTEND(misc-non-private-member-variables-in-classes)


    void GodOfDof::InitFromPreprocessedDataHelper(::MoReFEM::Wrappers::Lua::OptionFile& god_of_dof_prepartitioned_data)
    {
        auto match_interface_node_bearer = CreateNodeBearers();

        SetUpNodeBearersFromPrepartitionedData(god_of_dof_prepartitioned_data);

        CreateNodesAndDofs(match_interface_node_bearer);
    }


    void GodOfDof::SetUpNodeBearersFromPrepartitionedData(
        ::MoReFEM::Wrappers::Lua::OptionFile& god_of_dof_prepartitioned_data,
        Internal::FEltSpaceNS::optional_ref_connectivity_per_numbering_subset_type connectivity_per_numbering_subset)
    {
        decltype(auto) numbering_subset_list = GetNumberingSubsetList();

        const PrepartitionedDataFileContent prepartitioned_data_file_content(
            god_of_dof_prepartitioned_data, numbering_subset_list, GetMpi());

        const Match match(god_of_dof_prepartitioned_data.GetFilename(), prepartitioned_data_file_content);

        {
            NodeBearer::vector_shared_ptr processor_wise_node_bearer_list;
            NodeBearer::vector_shared_ptr ghost_node_bearer_list;

            decltype(auto) unsort_node_bearer_list = GetProcessorWiseNodeBearerList();
            // < When the function is called this accessor does not contain yet what is on the tag:
            // < ghost are still there.

            ProcessNodeBearerList(*this,
                                  unsort_node_bearer_list,
                                  prepartitioned_data_file_content,
                                  match,
                                  processor_wise_node_bearer_list,
                                  ghost_node_bearer_list);


            ComputeNodeBearerConnectivity(GetFEltSpaceList(),
                                          processor_wise_node_bearer_list.size(),
                                          Internal::FEltSpaceNS::KeepSelfconnection::yes,
                                          connectivity_per_numbering_subset);

            GetNonCstProcessorWiseNodeBearerList().swap(processor_wise_node_bearer_list);
            GetNonCstGhostNodeBearerList().swap(ghost_node_bearer_list);
        }
    }


    namespace // anonymous
    {


        PrepartitionedDataFileContent::PrepartitionedDataFileContent(
            ::MoReFEM::Wrappers::Lua::OptionFile& god_of_dof_prepartitioned_data,
            const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
            const Wrappers::Mpi& mpi)
        {
            const auto rank = mpi.GetRank();
            const auto Nprocessor = mpi.Nprocessor();

            assert(rank < Nprocessor);

            god_of_dof_prepartitioned_data.Read(
                "node_bearer_first_program_wise_index", "", node_bearer_first_program_wise_index);
            god_of_dof_prepartitioned_data.Read("interface_per_node_bearer", "", interface_per_node_bearer);
            god_of_dof_prepartitioned_data.Read("interface_per_ghost_node_bearer", "", interface_per_ghost_node_bearer);
            god_of_dof_prepartitioned_data.Read("ghost_node_bearer_indexes", "", ghost_node_bearer_index_list);

            god_of_dof_prepartitioned_data.Read("ghost_node_bearer_mpi_rank", "", ghost_node_bearer_mpi_rank_list);

            {
                ghost_processor_per_node_bearer_and_numbering_subset.max_load_factor(Utilities::DefaultMaxLoadFactor());

                std::ostringstream oconv;

                for (auto proc = rank_type{ 0UL }; proc < Nprocessor; ++proc)
                {
                    if (rank == proc)
                        continue;

                    for (const auto& numbering_subset_ptr : numbering_subset_list)
                    {
                        oconv.str("");

                        assert(!(!numbering_subset_ptr));
                        const auto numbering_subset_id = numbering_subset_ptr->GetUniqueId();

                        oconv << "ghosted_processor_wise_node_bearer_on_processor_" << proc << "_for_numbering_subset_"
                              << numbering_subset_id;

                        std::vector<NodeBearerNS::program_wise_index_type>
                            ghosted_processor_wise_node_bearer_for_processor;

                        god_of_dof_prepartitioned_data.Read(
                            oconv.str(), "", ghosted_processor_wise_node_bearer_for_processor);

                        // We really want here to create on the spot if not existing, and to complete it if already
                        // existing.
                        for (const auto& node_bearer_index : ghosted_processor_wise_node_bearer_for_processor)
                            ghost_processor_per_node_bearer_and_numbering_subset[node_bearer_index][numbering_subset_id]
                                .insert(proc);
                    }
                }
            }


            if (interface_per_ghost_node_bearer.size() != ghost_node_bearer_index_list.size())
            {
                std::ostringstream oconv;
                oconv << "Invalid prepartitioned file (" << god_of_dof_prepartitioned_data.GetFilename()
                      << "): the "
                         "number of items in interface_per_ghost_node_bearer is not the same as the one in "
                         "ghost_node_bearer_indexes"
                         " (respectively "
                      << interface_per_ghost_node_bearer.size() << " and " << ghost_node_bearer_index_list.size()
                      << ").";
                throw Exception(oconv.str());
            }

            if (interface_per_ghost_node_bearer.size() != ghost_node_bearer_mpi_rank_list.size())
            {
                std::ostringstream oconv;
                oconv << "Invalid prepartitioned file (" << god_of_dof_prepartitioned_data.GetFilename()
                      << "): the "
                         "number of items in interface_per_ghost_node_bearer is not the same as the one in "
                         "ghost_node_bearer_mpi_rank_list (respectively "
                      << interface_per_ghost_node_bearer.size() << " and " << ghost_node_bearer_mpi_rank_list.size()
                      << ").";
                throw Exception(oconv.str());
            }
        }


        Match::Match(const FilesystemNS::File& filename,
                     const PrepartitionedDataFileContent& prepartitioned_data_file_content)
        : first_program_wise_index_on_processor_(prepartitioned_data_file_content.node_bearer_first_program_wise_index),
          Nprocessor_wise_node_bearer_(prepartitioned_data_file_content.interface_per_node_bearer.size()),
          filename_(filename)
        {
            coords_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            edge_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            face_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            volume_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());

            ProcessProcessorWise(prepartitioned_data_file_content.interface_per_node_bearer);
            ProcessGhost(prepartitioned_data_file_content.interface_per_ghost_node_bearer,
                         prepartitioned_data_file_content.ghost_node_bearer_index_list);
        }


        void
        Match::ConvertShortHand(const std::string& shorthand,
                                char& interface_type,
                                ::MoReFEM::InterfaceNS::program_wise_index_type& interface_program_wise_index) const
        {
            if (shorthand.empty())
                throw MatchException(filename_, shorthand);

            std::istringstream iconv(shorthand);

            iconv >> interface_type;

            std::size_t tmp = 0;
            iconv >> tmp;

            if (!iconv)
                throw MatchException(filename_, shorthand);

            interface_program_wise_index = static_cast<::MoReFEM::InterfaceNS::program_wise_index_type>(tmp);
        }


        void Match::ProcessProcessorWise(const std::vector<std::string>& node_bearer_list)
        {
            auto current_node_bearer_program_wise_index{ first_program_wise_index_on_processor_ };

            for (const auto& node_bearer_shorthand : node_bearer_list)
            {
                char interface_type{};
                ::MoReFEM::InterfaceNS::program_wise_index_type interface_program_wise_index{ 0UL };
                ConvertShortHand(node_bearer_shorthand, interface_type, interface_program_wise_index);

                switch (interface_type)
                {
                case 'V':
                    coords_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'E':
                    edge_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'F':
                    face_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'G':
                    volume_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                default:
                    throw MatchException(filename_, node_bearer_shorthand);
                }

                ++current_node_bearer_program_wise_index;
            }
        }


        void Match::ProcessGhost(const std::vector<std::string>& ghost_node_bearer_list,
                                 const std::vector<NodeBearerNS::program_wise_index_type>& ghost_node_bearer_index_list)
        {
            assert(ghost_node_bearer_list.size() == ghost_node_bearer_index_list.size()
                   && "Should never happen: already tested by an exception beforehand");

            auto it = ghost_node_bearer_index_list.cbegin();

            for (const auto& node_bearer_shorthand : ghost_node_bearer_list)
            {
                char interface_type{};
                ::MoReFEM::InterfaceNS::program_wise_index_type interface_program_wise_index{};
                ConvertShortHand(node_bearer_shorthand, interface_type, interface_program_wise_index);

                const auto current_node_bearer_program_wise_index = *it;

                switch (interface_type)
                {
                case 'V':
                    coords_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'E':
                    edge_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'F':
                    face_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                case 'G':
                    volume_matching_.insert({ interface_program_wise_index, current_node_bearer_program_wise_index });
                    break;
                default:
                    throw MatchException(filename_, node_bearer_shorthand);
                }
                ++it;
            }

            assert(it == ghost_node_bearer_index_list.cend());
        }


        const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type,
                                 NodeBearerNS::program_wise_index_type>&
        Match::GetInterfaceMatch(InterfaceNS::Nature nature) const
        {
            switch (nature)
            {
            case InterfaceNS::Nature::vertex:
                return coords_matching_;
            case InterfaceNS::Nature::edge:
                return edge_matching_;
            case InterfaceNS::Nature::face:
                return face_matching_;
            case InterfaceNS::Nature::volume:
                return volume_matching_;
            case InterfaceNS::Nature::none:
            case InterfaceNS::Nature::undefined:
                assert(false);
                exit(EXIT_FAILURE);
            }

            // Stupid line to please gcc...
            return volume_matching_;
        }


        bool Match::IsGhost(NodeBearerNS::program_wise_index_type node_bearer_index) const noexcept
        {
            if (node_bearer_index < first_program_wise_index_on_processor_)
                return true;

            if (node_bearer_index >= first_program_wise_index_on_processor_
                                         + NodeBearerNS::program_wise_index_type(Nprocessor_wise_node_bearer_))
                return true;

            return false;
        }


        FetchResult Match::Fetch(const Interface& interface) const
        {
            decltype(auto) interface_list = GetInterfaceMatch(interface.GetNature());

            const auto it = interface_list.find(interface.GetProgramWiseIndex());

            if (it == interface_list.cend())
                throw Exception("Unable to match the recreated NodeBearer to one of the pre-existing one before "
                                "partitioning");

            FetchResult result(it->second, IsGhost(it->second));
            return result;
        }


        MatchException::~MatchException() = default;


        MatchException::MatchException(const FilesystemNS::File& filename,
                                       std::string_view node_bearer_shorthand,
                                       const std::source_location location)
        : Exception(MatchExceptionErrorMsg(filename, node_bearer_shorthand), location)
        { }


        std::string MatchExceptionErrorMsg(const FilesystemNS::File& filename, std::string_view node_bearer_shorthand)
        {
            std::ostringstream oconv;
            oconv << "Invalid node bearer shorthand in " << filename << ": '" << node_bearer_shorthand
                  << "' does not follow the expected format (which is a letter among { V, E, F, G } followed "
                     "by an integer.)";
            throw Exception(oconv.str());
        }


        void ProcessNodeBearerList(const GodOfDof& god_of_dof,
                                   const NodeBearer::vector_shared_ptr& node_bearer_list_before_processing,
                                   const PrepartitionedDataFileContent& prepartitioned_data_file_content,
                                   const Match& match,
                                   NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                   NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
        {
            decltype(auto) mpi = god_of_dof.GetMpi();
            assert(processor_wise_node_bearer_list.empty());
            assert(ghost_node_bearer_list.empty());

            const auto& interface_per_node_bearer = prepartitioned_data_file_content.interface_per_node_bearer;
            const auto& ghost_node_bearer_index_list = prepartitioned_data_file_content.ghost_node_bearer_index_list;
            const auto& ghost_node_bearer_mpi_rank_list =
                prepartitioned_data_file_content.ghost_node_bearer_mpi_rank_list;

            const auto Nprocessor_wise_node_bearer =
                static_cast<NodeBearer::vector_shared_ptr::difference_type>(interface_per_node_bearer.size());

            assert(node_bearer_list_before_processing.size() >= interface_per_node_bearer.size());
            const auto Nghost_node_bearer = ghost_node_bearer_index_list.size();

            assert(Nghost_node_bearer == node_bearer_list_before_processing.size() - interface_per_node_bearer.size());

            // reserve/resize here is not a mistake: we do not proceed the same way for each of them.
            processor_wise_node_bearer_list.reserve(static_cast<std::size_t>(Nprocessor_wise_node_bearer));

            ghost_node_bearer_list.resize(Nghost_node_bearer, nullptr);

            const auto mpi_rank = mpi.GetRank();

            const auto& ghost_processor_per_node_bearer_and_numbering_subset =
                prepartitioned_data_file_content.ghost_processor_per_node_bearer_and_numbering_subset;

            // Beware: pointer itself is constant, but underlying `NodeBearer` object is modified!
            for (const auto& node_bearer_ptr : node_bearer_list_before_processing)
            {
                assert(!(!node_bearer_ptr));
                auto& node_bearer = *node_bearer_ptr;

                decltype(auto) interface = node_bearer.GetInterface();

                const auto result = match.Fetch(interface);
                node_bearer.SetProgramWiseIndex(result.node_bearer_index);

                if (result.is_ghost)
                {
                    // Put the \a NodeBearer in the exact same position it was in the original run.
                    auto it = std::ranges::find(ghost_node_bearer_index_list,

                                                result.node_bearer_index);

                    assert(it != ghost_node_bearer_index_list.cend());
                    const auto position = static_cast<std::size_t>(it - ghost_node_bearer_index_list.cbegin());
                    assert(ghost_node_bearer_list[position] == nullptr);
                    ghost_node_bearer_list[position] = node_bearer_ptr;
                    node_bearer.SetProcessor(ghost_node_bearer_mpi_rank_list[position]);
                } else
                {
                    node_bearer.SetProcessor(mpi_rank);
                    processor_wise_node_bearer_list.push_back(node_bearer_ptr);

                    auto it =
                        ghost_processor_per_node_bearer_and_numbering_subset.find(node_bearer.GetProgramWiseIndex());

                    if (it != ghost_processor_per_node_bearer_and_numbering_subset.cend())
                    {
                        const auto& ghost_processor_per_numbering_subset = it->second;

                        for (const auto& [numbering_subset_id, ghost_processor_list] :
                             ghost_processor_per_numbering_subset)
                        {
                            for (const auto processor : ghost_processor_list)
                                node_bearer.SetGhost(god_of_dof.GetNumberingSubset(numbering_subset_id), processor);
                        }
                    }
                }
            }

            std::ranges::sort(processor_wise_node_bearer_list,

                              [](const auto& lhs_ptr, const auto& rhs_ptr)
                              {
                                  assert(!(!lhs_ptr));
                                  assert(!(!rhs_ptr));
                                  return lhs_ptr->GetProgramWiseIndex() < rhs_ptr->GetProgramWiseIndex();
                              });

            assert(processor_wise_node_bearer_list.size() == interface_per_node_bearer.size());

            assert(std::ranges::none_of(ghost_node_bearer_list,

                                        Utilities::IsNullptr<NodeBearer::shared_ptr>));
        }


    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
