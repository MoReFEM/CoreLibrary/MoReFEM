// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATOR_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATOR_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    inline const FEltSpace& CoordsMatchingInterpolator::GetSourceFEltSpace() const noexcept
    {
        return source_felt_space_;
    }


    inline const NumberingSubset& CoordsMatchingInterpolator::GetSourceNumberingSubset() const noexcept
    {
        return source_numbering_subset_;
    }


    inline const FEltSpace& CoordsMatchingInterpolator::GetTargetFEltSpace() const noexcept
    {
        return target_felt_space_;
    }


    inline const NumberingSubset& CoordsMatchingInterpolator::GetTargetNumberingSubset() const noexcept
    {
        return target_numbering_subset_;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
