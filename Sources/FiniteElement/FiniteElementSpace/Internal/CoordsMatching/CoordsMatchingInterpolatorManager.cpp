// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp" // IWYU pragma: associated

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/Exceptions/Exception.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FindFEltSpaceFromIndex.hpp"
#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    namespace // anonymous
    {


        void ThrowIfInconsistentNumberingSubset(const GodOfDof& god_of_dof,
                                                ::MoReFEM::NumberingSubsetNS::unique_id numbering_subset_index);


    } // namespace


    CoordsMatchingInterpolatorManager::~CoordsMatchingInterpolatorManager() = default;


    const std::string& CoordsMatchingInterpolatorManager::ClassName()
    {
        static const std::string ret("CoordsMatchingInterpolatorManager");
        return ret;
    }


    void
    CoordsMatchingInterpolatorManager::Create(const std::size_t unique_id,
                                              ::MoReFEM::FEltSpaceNS::unique_id source_felt_space_unique_id,
                                              ::MoReFEM::NumberingSubsetNS::unique_id source_numbering_subset_index,
                                              ::MoReFEM::FEltSpaceNS::unique_id target_felt_space_unique_id,
                                              ::MoReFEM::NumberingSubsetNS::unique_id target_numbering_subset_index)
    {
        decltype(auto) source_felt_space = Internal::FEltSpaceNS::FindFEltSpaceFromIndex(source_felt_space_unique_id);
        decltype(auto) target_felt_space = Internal::FEltSpaceNS::FindFEltSpaceFromIndex(target_felt_space_unique_id);

        decltype(auto) source_god_of_dof_ptr = source_felt_space.GetGodOfDofFromWeakPtr();
        decltype(auto) target_god_of_dof_ptr = target_felt_space.GetGodOfDofFromWeakPtr();

        if (source_god_of_dof_ptr == target_god_of_dof_ptr)
        {
            std::ostringstream oconv;

            oconv << "Invalid CoordsMatchingInterpolator data: both finite element spaces ("
                  << source_felt_space_unique_id << " and " << target_felt_space_unique_id
                  << " are located within the same GodOfDof.";

            throw FromCoordsMatchingNS::Exception(oconv.str());
        }

        ThrowIfInconsistentNumberingSubset(*source_god_of_dof_ptr, source_numbering_subset_index);
        ThrowIfInconsistentNumberingSubset(*target_god_of_dof_ptr, target_numbering_subset_index);


        decltype(auto) source_numbering_subset =
            source_god_of_dof_ptr->GetNumberingSubset(source_numbering_subset_index);
        decltype(auto) target_numbering_subset =
            target_god_of_dof_ptr->GetNumberingSubset(target_numbering_subset_index);

        // make_unique is not accepted here: it makes the code yell about private status of the constructor
        // with both clang and gcc.
        auto&& ptr = Internal::WrapUniqueToConst(new CoordsMatchingInterpolator(
            unique_id, source_felt_space, source_numbering_subset, target_felt_space, target_numbering_subset));

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        auto insert_return_value = list_.insert(std::move(pair));

        if (!insert_return_value.second)
            throw FromCoordsMatchingNS::Exception(
                "Two CoordsMatchingInterpolator objects can't share the same unique identifier! (namely "
                + std::to_string(unique_id) + ").");
    }


    const CoordsMatchingInterpolator&
    CoordsMatchingInterpolatorManager::GetCoordsMatchingInterpolator(std::size_t unique_id) const
    {
        const auto& list = GetStorage();

        auto it = list.find(unique_id);
        assert(it != list.cend());

        assert(!(!(it->second)));

        return *(it->second);
    }


    namespace // anonymous
    {


        void ThrowIfInconsistentNumberingSubset(const GodOfDof& god_of_dof,
                                                ::MoReFEM::NumberingSubsetNS::unique_id numbering_subset_index)
        {
            decltype(auto) numbering_subset_list = god_of_dof.GetNumberingSubsetList();

            const auto end = numbering_subset_list.cend();

            auto it = std::find_if(numbering_subset_list.cbegin(),
                                   end,
                                   [numbering_subset_index](const auto& ptr)
                                   {
                                       assert(!(!ptr));
                                       return ptr->GetUniqueId() == numbering_subset_index;
                                   });

            if (it == end)
            {
                std::ostringstream oconv;

                oconv << "Invalid CoordsMatchingInterpolator data: there is an association between GodOfDof "
                      << god_of_dof.GetUniqueId() << " and numbering subset " << numbering_subset_index
                      << " whereas "
                         "the latter doesn't belong to the former.";

                throw FromCoordsMatchingNS::Exception(oconv.str());
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
