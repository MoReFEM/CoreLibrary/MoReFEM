// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATORMANAGER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATORMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <unordered_map>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Interpolator/CoordsMatchingInterpolator.hpp"
#include "Core/NumberingSubset/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"
#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief This class is used to create and retrieve \a CoordsMatchingInterpolator
     * objects.
     *
     * CoordsMatchingInterpolator objects get private constructor and can only be created
     * through this class.
     */
    class CoordsMatchingInterpolatorManager : public Utilities::Singleton<CoordsMatchingInterpolatorManager>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::Tag;

      public:
        /*!
         * \brief Create a \a CoordsMatchingInterpolator object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);

        //! Fetch the object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{CoordsMatchingInterpolator}
        const CoordsMatchingInterpolator& GetCoordsMatchingInterpolator(std::size_t unique_id) const;

        //! Access to the storage.
        const auto& GetStorage() const noexcept;

      private:
        //! Access to the storage.
        auto& GetNonCstStorage() noexcept;

        /*!
         * \brief Create from a finite element space and a numbering subset.
         *
         * \param[in] unique_id Unique identifier to give to the \a
         * CoordsMatchingInterpolator object being created.
         * \param[in] source_felt_space Source \a FEltSpace.
         * \param[in] source_numbering_subset Source \a NumberingSubset .
         * \param[in] target_felt_space Target \a FEltSpace.
         * \param[in] target_numbering_subset Target \a NumberingSubset .
         */
        void Create(std::size_t unique_id,
                    ::MoReFEM::FEltSpaceNS::unique_id source_felt_space,
                    ::MoReFEM::NumberingSubsetNS::unique_id source_numbering_subset,
                    ::MoReFEM::FEltSpaceNS::unique_id target_felt_space,
                    ::MoReFEM::NumberingSubsetNS::unique_id target_numbering_subset);


      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         */
        CoordsMatchingInterpolatorManager() = default;

        //! Destructor.
        virtual ~CoordsMatchingInterpolatorManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<CoordsMatchingInterpolatorManager>;
        ///@}


      private:
        //! Store the \a CoordsMatchingInterpolatorManager per their unique id.
        //! \internal The nature of the storage is for conveniency: we could have stored the objects in a vector
        //! as the key is also what is returned if you use \a GetUniqueId() method on the second member of the
        //! pair.
        std::unordered_map<std::size_t, CoordsMatchingInterpolator::const_unique_ptr> list_;
    };


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATORMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
