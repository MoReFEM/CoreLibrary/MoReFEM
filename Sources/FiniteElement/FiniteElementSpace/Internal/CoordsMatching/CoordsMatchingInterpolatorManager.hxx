// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATORMANAGER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATORMANAGER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/NumberingSubset/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void CoordsMatchingInterpolatorManager ::Create(const IndexedSectionDescriptionT&,
                                                    const ModelSettingsT& model_settings,
                                                    const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        const auto source_felt_space_unique_id = ::MoReFEM::FEltSpaceNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::SourceFEltSpaceIndex>(model_settings, input_data)
        };
        const auto target_felt_space_unique_id = ::MoReFEM::FEltSpaceNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::TargetFEltSpaceIndex>(model_settings, input_data)
        };

        const auto source_numbering_subset_index = ::MoReFEM::NumberingSubsetNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::SourceNumberingSubsetIndex>(model_settings,
                                                                                                   input_data)
        };
        const auto target_numbering_subset_index = ::MoReFEM::NumberingSubsetNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::TargetNumberingSubsetIndex>(model_settings,
                                                                                                   input_data)
        };


        Create(section_type::GetUniqueId(),
               source_felt_space_unique_id,
               source_numbering_subset_index,
               target_felt_space_unique_id,
               target_numbering_subset_index);
    }


    inline const auto& CoordsMatchingInterpolatorManager::GetStorage() const noexcept
    {
        return list_;
    }


    inline auto& CoordsMatchingInterpolatorManager::GetNonCstStorage() noexcept
    {
        return list_;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_COORDSMATCHINGINTERPOLATORMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
