// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::FromCoordsMatchingNS
{


    //! Generic class for FromCoordsMatching exceptions (useful in some tests).
    class Exception : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


} // namespace MoReFEM::Internal::FromCoordsMatchingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
