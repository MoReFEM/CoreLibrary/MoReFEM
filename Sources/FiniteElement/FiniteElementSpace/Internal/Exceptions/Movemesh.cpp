// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <source_location>
#include <sstream>
#include <string>

#include "FiniteElement/FiniteElementSpace/Internal/Exceptions/Movemesh.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"


namespace MoReFEM::Internal::ExceptionsNS::Movemesh
{


    namespace // anonymous
    {


        std::string NumberOfUnknownsMsg(const FEltSpace& felt_space,
                                        const NumberingSubset& numbering_subset,
                                        long Nmatching_unknown);

        std::string ScalarUnknownMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset);

        std::string NoMovemeshDataMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset);


    } // namespace


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : ::MoReFEM::Exception(msg, location)
    { }


    NumberOfUnknowns::~NumberOfUnknowns() = default;

    NumberOfUnknowns::NumberOfUnknowns(const FEltSpace& felt_space,
                                       const NumberingSubset& numbering_subset,
                                       long Nmatching_unknown,
                                       const std::source_location location)
    : Exception(NumberOfUnknownsMsg(felt_space, numbering_subset, Nmatching_unknown), location)
    { }


    ScalarUnknown::~ScalarUnknown() = default;

    ScalarUnknown::ScalarUnknown(const FEltSpace& felt_space,
                                 const NumberingSubset& numbering_subset,
                                 const std::source_location location)
    : Exception(ScalarUnknownMsg(felt_space, numbering_subset), location)
    { }


    NoMovemeshData::~NoMovemeshData() = default;

    NoMovemeshData::NoMovemeshData(const FEltSpace& felt_space,
                                   const NumberingSubset& numbering_subset,
                                   const std::source_location location)
    : Exception(NoMovemeshDataMsg(felt_space, numbering_subset), location)
    { }


    namespace // anonymous
    {


        std::string NumberOfUnknownsMsg(const FEltSpace& felt_space,
                                        const NumberingSubset& numbering_subset,
                                        long Nmatching_unknown)
        {
            std::ostringstream oconv;
            oconv << "Numbering subset " << numbering_subset.GetUniqueId()
                  << " in finite "
                     "element space "
                  << felt_space.GetUniqueId()
                  << " should cover exactly one (vectorial) "
                     "unknown; "
                  << Nmatching_unknown
                  << " unknowns were found in it. Your Lua "
                     "input file is likely ill-constructed.";

            return oconv.str();
        }


        std::string ScalarUnknownMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset)
        {
            std::ostringstream oconv;

            oconv << "The unknown in numbering subset " << numbering_subset.GetUniqueId() << " in finite element space "
                  << felt_space.GetUniqueId()
                  << " should be vectorial (as "
                     "it is expected to be a displacement) but was declared otherwise. Your Lua "
                     "input file is likely ill-constructed.";
            return oconv.str();
        }


        std::string NoMovemeshDataMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset)
        {
            std::ostringstream oconv;
            oconv << "You called 'FEltSpace::MoveMesh()' for finite element space " << felt_space.GetUniqueId()
                  << " with a vector which numbering subset is " << numbering_subset.GetUniqueId()
                  << ". No movemesh data were found for "
                     "this one; make sure the option 'do_move_mesh' is set to true in the input "
                     "lua file for this numbering subset (if it already is please submit a bug report)";
            return oconv.str();
        }


    } // namespace


} // namespace MoReFEM::Internal::ExceptionsNS::Movemesh


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
