// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_EXCEPTIONS_MOVEMESH_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_EXCEPTIONS_MOVEMESH_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ExceptionsNS::Movemesh
{


    //! Generic exception thrown for movemesh related operations.
    class Exception : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location

         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Specific movemesh exception.
    class NumberOfUnknowns : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] felt_space \a FEltSpace involved.
         * \param[in] numbering_subset \a NumberingSubset involved.
         * \param[in] Nmatching_unknown Number of matching unknowns found.
         * \copydoc doxygen_hide_source_location
         */
        explicit NumberOfUnknowns(const FEltSpace& felt_space,
                                  const NumberingSubset& numbering_subset,
                                  long Nmatching_unknown,
                                  const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~NumberOfUnknowns() override;
    };


    //! Specific movemesh exception.
    class ScalarUnknown : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] felt_space \a FEltSpace involved.
         * \param[in] numbering_subset \a NumberingSubset involved.
         * \copydoc doxygen_hide_source_location
         */
        explicit ScalarUnknown(const FEltSpace& felt_space,
                               const NumberingSubset& numbering_subset,
                               const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~ScalarUnknown() override;
    };


    //! Specific movemesh exception.
    class NoMovemeshData : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] felt_space \a FEltSpace involved.
         * \param[in] numbering_subset \a NumberingSubset involved.
         * \copydoc doxygen_hide_source_location
         */
        explicit NoMovemeshData(const FEltSpace& felt_space,
                                const NumberingSubset& numbering_subset,
                                const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~NoMovemeshData() override;
    };


} // namespace MoReFEM::Internal::ExceptionsNS::Movemesh


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_EXCEPTIONS_MOVEMESH_DOT_HPP_
// *** MoReFEM end header guards *** < //
