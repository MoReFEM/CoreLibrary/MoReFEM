// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
#ifndef NDEBUG

#include "FiniteElement/FiniteElementSpace/Internal/ExtractMpi.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"


namespace MoReFEM::Internal
{


    const ::MoReFEM::Wrappers::Mpi& ExtractMpi(const GeometricElt& geom_elt)
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(geom_elt.GetMeshIdentifier());

        return god_of_dof.GetMpi();
    }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
#endif // NDEBUG
