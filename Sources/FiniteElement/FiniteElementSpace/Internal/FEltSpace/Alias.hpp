// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_ALIAS_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_ALIAS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Provides for all the \a RefLocalFEltSpace the list of associated \a LocalFEltSpace.
     *
     * \internal <b><tt>[internal]</tt></b> This container behaves almost like a map, except that there
     * is no ordering relation on the keys (that's why a std::map was not used in the first place).
     * \endinternal
     */
    // clang-format off
    using LocalFEltSpacePerRefLocalFEltSpace =
    std::vector
    <
        std::pair<Internal::RefFEltNS::RefLocalFEltSpace::const_shared_ptr, LocalFEltSpace::per_geom_elt_index>
    >;
    // clang-format on


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_ALIAS_DOT_HPP_
// *** MoReFEM end header guards *** < //
