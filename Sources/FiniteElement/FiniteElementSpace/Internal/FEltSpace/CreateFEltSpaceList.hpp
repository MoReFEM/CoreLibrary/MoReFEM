// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_CREATEFELTSPACELIST_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_CREATEFELTSPACELIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Create all the \a FEltSpace objects from the input data file.
     *
     * \internal <b><tt>[internal]</tt></b> Should not be called outside of InitAllGodOfDof.
     * \endinternal
     *
     * \copydoc doxygen_hide_model_settings_input_data_arg
     *
     * \return The list of  all \a FEltSpace detailed in the input
     * data file and store them in this container which key is the index of the \a GodOfDof (which are
     * not yet created when this function is called - hence the index rather than the pointer of the object).
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    std::map<::MoReFEM::MeshNS::unique_id, FEltSpace::vector_unique_ptr>
    CreateFEltSpaceList(const ModelSettingsT& model_settings, const InputDataT& input_data);


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_CREATEFELTSPACELIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
