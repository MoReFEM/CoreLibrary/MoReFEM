// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_CREATEFELTSPACELIST_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_CREATEFELTSPACELIST_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Geometry/Domain/UniqueId.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::FEltSpaceNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    std::map<::MoReFEM::MeshNS::unique_id, FEltSpace::vector_unique_ptr>
    CreateFEltSpaceList(const ModelSettingsT& model_settings, const InputDataT& input_data)
    {
        std::map<::MoReFEM::MeshNS::unique_id, FEltSpace::vector_unique_ptr> ret;

        auto create = [&ret, &model_settings, &input_data](const auto& indexed_section_description) -> void
        {
            using felt_space_section_type =
                typename std::decay_t<decltype(indexed_section_description)>::enclosing_section_type;

            static_assert(!std::is_same<felt_space_section_type, Advanced::InputDataNS::NoEnclosingSection>());

            const auto unique_id = ::MoReFEM::FEltSpaceNS::unique_id{ felt_space_section_type::GetUniqueId() };

            decltype(auto) god_of_dof_index = ::MoReFEM::MeshNS::unique_id{
                ::MoReFEM::InputDataNS::ExtractLeaf<typename felt_space_section_type::GodOfDofIndex>(model_settings,
                                                                                                     input_data)
            };

            decltype(auto) domain_index = ::MoReFEM::DomainNS::unique_id{
                ::MoReFEM::InputDataNS::ExtractLeaf<typename felt_space_section_type::DomainIndex>(model_settings,
                                                                                                   input_data)
            };

            auto&& extended_unknown_list =
                MoReFEM::Internal::FEltSpaceNS::ExtractExtendedUnknownList<felt_space_section_type>(model_settings,
                                                                                                    input_data);

            decltype(auto) god_of_dof_ptr = GodOfDofManager::GetInstance().GetGodOfDofPtr(god_of_dof_index);

            decltype(auto) domain_manager = DomainManager::GetInstance();

            try
            {
                const auto& domain = domain_manager.GetDomain(domain_index);

                auto felt_space_ptr =
                    std::make_unique<FEltSpace>(god_of_dof_ptr, domain, unique_id, std::move(extended_unknown_list));

                ret[god_of_dof_index].emplace_back(std::move(felt_space_ptr));
            }
            catch (const Exception& e)
            {
                std::ostringstream oconv;
                oconv << "Ill-defined finite element space " << unique_id << ": " << e.GetRawMessage();
                throw Exception(oconv.str());
            }
        };


        // clang-format off
        using model_settings_tuple_iteration =
            Internal::InputDataNS::TupleIteration
            <
                typename ModelSettingsT::underlying_tuple_type,
                0UL
            >;
        // clang-format on

        model_settings_tuple_iteration::template ActIfLeafInheritsFrom<
            MoReFEM::Internal::InputDataNS::FEltSpaceNS::Tag>(model_settings.GetTuple(), input_data, create);

        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_CREATEFELTSPACELIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
