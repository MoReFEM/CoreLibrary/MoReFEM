// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp" // IWYU pragma: export

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Extract the list of \a ExtendedUnknown related to a given\a FEltSpaceSection from\a model_settings and/or \a input_data.
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     */

    template<class FEltSpaceSection,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
             ::MoReFEM::Concept::InputDataType InputDataT>
    // clang-format on
    ExtendedUnknown::vector_const_shared_ptr ExtractExtendedUnknownList(const ModelSettingsT& model_settings,
                                                                        const InputDataT& input_data);


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpace.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
