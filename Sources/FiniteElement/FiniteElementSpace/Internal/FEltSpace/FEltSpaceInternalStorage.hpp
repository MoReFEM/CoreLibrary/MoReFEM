// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACEINTERNALSTORAGE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACEINTERNALSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/Mpi/Mpi.hpp"
#include "Utilities/Mutex/Mutex.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Alias.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Find in a LocalFEltSpacePerRefLocalFEltSpace the \a RefLocalFEltSpace matching the \a RefGeomElt
     * which underlying index is I.
     *
     * \tparam I Underlying index beneath a \a GeometricEltEnum
     *
     * \param[in] container The container into which the search is performed.
     *
     * \return The \a RefLocalFEltSpace adequate for the given \a GeometricEltEnum if one is defined, nullptr
     * otherwise. The latter is not an error: it might just be for instance the search was performed for a
     * \a RefGeometricElt not present in the model currently run.
     *
     */
    template<std::size_t I>
    const Internal::RefFEltNS::RefLocalFEltSpace*
    FindRefLocalFEltSpace(const LocalFEltSpacePerRefLocalFEltSpace& container);


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceInternalStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACEINTERNALSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
