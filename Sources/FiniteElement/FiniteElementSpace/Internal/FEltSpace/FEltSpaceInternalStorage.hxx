// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACEINTERNALSTORAGE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACEINTERNALSTORAGE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceInternalStorage.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    template<std::size_t I>
    const Internal::RefFEltNS::RefLocalFEltSpace*
    FindRefLocalFEltSpace(const LocalFEltSpacePerRefLocalFEltSpace& container)
    {
        for (const auto& pair : container)
        {
            const auto& ref_local_felt_space_ptr = pair.first;
            assert(!(!ref_local_felt_space_ptr));

            decltype(auto) ref_geom_elt = ref_local_felt_space_ptr->GetRefGeomElt();
            const auto ref_geom_elt_id = EnumUnderlyingType(ref_geom_elt.GetIdentifier());

            if (ref_geom_elt_id == I)
                return ref_local_felt_space_ptr.get();
        }

        return nullptr;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACEINTERNALSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
