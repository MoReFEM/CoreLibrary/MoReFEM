// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACESTORAGE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACESTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <unordered_map>

#include "Utilities/Mpi/Mpi.hpp"
#include "Utilities/Mutex/Mutex.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp" // IWYU pragma: keep

#include "Geometry/Domain/UniqueId.hpp" // IWYU pragma: export

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceInternalStorage.hpp"      // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class FEltSpace; }
namespace MoReFEM::Internal::FEltSpaceNS { struct ReduceToProcessorWise; } // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief The class in charge of storing finite elements and dofs structure of a given finite element space.
     *
     * There are in fact two different types of storage in this class:
     * - The first is really the list of all the finite elements covered by the FEltSpace.
     * - The second is the subset of this list for every relevant Domain. The reason for this is that
     * the test to know whether a FElt (or in fact its related GeometricElt) belongs to a given
     * Domain is not cheap, and therefore doing it only once is appealing.
     */
    class Storage final : public ::MoReFEM::Crtp::Mutex<Storage>, public ::MoReFEM::Crtp::CrtpMpi<Storage>
    {

      public:
        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const Storage>;

        //! Friendship to Internal class ReduceToProcessorWise, which needs access to non constant accessor.
        friend struct ReduceToProcessorWise;

        //! Friendship
        friend class ::MoReFEM::FEltSpace;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] processor_wise_local_felt_space_list_per_type List of processor-wise \a LocalFEltSpace per
         * \a RefLocalFEltSpace. This container is computed by FEltSpace::SetFEltList(), which is the method in
         * charge of building current object to store and give quick access to relevant finite element data.
         * \param[in] ghost_local_felt_space_list_per_type Same as \a
         * processor_wise_local_felt_space_list_per_type for ghosts.
         */
        explicit Storage(const ::MoReFEM::Wrappers::Mpi& mpi,
                         LocalFEltSpacePerRefLocalFEltSpace&& processor_wise_local_felt_space_list_per_type,
                         LocalFEltSpacePerRefLocalFEltSpace&& ghost_local_felt_space_list_per_type);

        //! Destructor.
        ~Storage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Storage(const Storage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Storage(Storage&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Storage& operator=(const Storage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Storage& operator=(Storage&& rhs) = delete;


        ///@}

        //! Access to the complete finite element list.
        template<RoleOnProcessor RoleOnProcessorT>
        const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept;

        //! Access to the finite element list related to a given Domain.
        //! \param[in] domain \a Domain used as filter.
        template<RoleOnProcessor RoleOnProcessorT>
        const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace(const Domain& domain) const;

        //! Whether there are finite elements in the storage or not
        //! \return True if there are none, false if there is at least one as processor-wise or as ghost.
        bool IsEmpty() const noexcept;

        /*!
         * \brief Set the new storage after the reduction occurred.
         *
         * The computation is done in \a FEltSpace::Reduce() method; the present accessor is there to
         * acknowledge the result and update accordingly the \a Storage object. The data per domain are cleared
         * - they will be recomputed on the fly if need be, but we don't want to muddle program-wise data here
         * whereas the rest of the object has been properly reduced.
         *
         * \param[in] processor_wise_content Processor-wise \a LocalFEtSpace.
         * \param[in] ghost_content Ghost \a LocalFEtSpace.
         */
        void SetReducedData(LocalFEltSpacePerRefLocalFEltSpace&& processor_wise_content,
                            LocalFEltSpacePerRefLocalFEltSpace&& ghost_content);

      private:
        //! Access to the underlying object that handles truly the list of finite element and possibly dofs.
        template<RoleOnProcessor RoleOnProcessorT>
        const Impl::InternalStorage& GetStorage() const noexcept;

        //! Non constant access to the underlying object that handles truly the list of finite element and
        //! possibly dofs.
        template<RoleOnProcessor RoleOnProcessorT>
        Impl::InternalStorage& GetNonCstStorage() noexcept;

        //! Non constant access to the list of finite element per reference finite element space.
        template<RoleOnProcessor RoleOnProcessorT>
        LocalFEltSpacePerRefLocalFEltSpace& GetNonCstLocalFEltSpacePerRefLocalFEltSpace() noexcept;

        /*!
         * \brief Non constant accessor to the finite element storage per domain.
         *
         * \internal <b><tt>[internal]</tt></b> This method is nonetheless marked const as the quantity
         * accessed is mutable.
         * \endinternal
         *
         * The container covers only processor-wise data.
         *
         * \return Reference to the finite element storage per domain.
         *
         */
        template<RoleOnProcessor RoleOnProcessorT>
        std::unordered_map<::MoReFEM::DomainNS::unique_id, Impl::InternalStorage>&
        GetNonCstFEltStoragePerDomain() const noexcept;


      private:
        /*!
         * \brief Return the internal storage matching the given \a domain. If the domain doesn't exist yet,
         * compute it on the fly.
         *
         * \copydoc doxygen_hide_mpi_param
         * \param[in] domain We actually want the subset of the finite element space that is included in this
         * domain.
         *
         * \return The storage associated to \a domain.
         */
        template<RoleOnProcessor RoleOnProcessorT>
        Impl::InternalStorage& AddOrFetchDomainStorage(const Domain& domain, const ::MoReFEM::Wrappers::Mpi& mpi) const;

        /*!
         * \brief An helper function of AddOrFetchDomainStorage() in charge of computing a new storage for a
         * domain that wasn't stored yet.
         *
         * \param[in] full_content List of all \a LocalFEltSpace in the \a FEltSpace, regardless of \a Domain.
         * \param[in] domain \a Domain for which we seek the list of \a LocalFEltSpace.
         *
         * \return The list of \a LocalFEltSpace within the chosen \a domain.
         */
        static LocalFEltSpacePerRefLocalFEltSpace
        ComputeFEltListForDomain(const LocalFEltSpacePerRefLocalFEltSpace& full_content, const Domain& domain);


      private:
        //! The complete list of finite elements inside a \a FEltSpace, regardless of \a Domain.
        Impl::InternalStorage internal_storage_;

        //! The complete list of ghost finite elements inside a \a FEltSpace, regardless of \a Domain.
        Impl::InternalStorage ghost_internal_storage_;

        /*!
         * \class doxygen_hide_felt_storage_per_domain
         *
         * The key is the UniqueId of a \a Domain, which is given by the method Domain::GetUniqueId().
         *
         * \internal <b><tt>[internal]</tt></b> The mutable keyword is there because
         * GetLocalFEltSpacePerRefLocalFEltSpace(const Domain&) may have
         * to compute its content on the fly. No other method should be allowed to modify it!
         * \endinternal
         */


        /*!
         * \brief The list of finite elements and dof structure sort per domain.
         *
         * \copydoc doxygen_hide_felt_storage_per_domain
         */
        mutable std::unordered_map<::MoReFEM::DomainNS::unique_id, Impl::InternalStorage>
            processor_wise_felt_storage_per_domain_;

        /*!
         * \brief The list of finite elements and dof structure sort per domain for ghost \a LocalFEltSpace
         *
         * \copydoc doxygen_hide_felt_storage_per_domain
         */
        mutable std::unordered_map<::MoReFEM::DomainNS::unique_id, Impl::InternalStorage>
            ghost_felt_storage_per_domain_;
    };


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACESTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
