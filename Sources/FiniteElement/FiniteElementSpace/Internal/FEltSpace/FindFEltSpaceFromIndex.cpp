// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
#include <cassert>
#include <source_location>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FindFEltSpaceFromIndex.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    const FEltSpace& FindFEltSpaceFromIndex(const ::MoReFEM::FEltSpaceNS::unique_id felt_space_unique_id,
                                            const std::source_location location)
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(location);
        decltype(auto) god_of_dof_storage = god_of_dof_manager.GetStorage();

        for (const auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            const auto& god_of_dof = *god_of_dof_ptr;
            if (god_of_dof.IsFEltSpace(felt_space_unique_id))
                return god_of_dof.GetFEltSpace(felt_space_unique_id);
        }

        throw Exception("Couldn't find in any GodOfDof a FEltSpace with id = "
                            + std::to_string(felt_space_unique_id.Get()),
                        location);
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
