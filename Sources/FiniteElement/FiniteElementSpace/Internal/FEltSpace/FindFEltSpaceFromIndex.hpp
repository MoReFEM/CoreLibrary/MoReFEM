// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FINDFELTSPACEFROMINDEX_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FINDFELTSPACEFROMINDEX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <source_location>

#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM {  class FEltSpace; };


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Find a \a FEltSpace given only its index.
     *
     * \internal This is really internal stuff: the intended  way to access a \a FEltSpace is to use the dedicated accessor in its \a GodOfDof. However,
     * it may happen in initialization phase that we need to find one without knowing directly its \a GodOfDof (for
     * instance when initializing
     * \a CoordsMarchingInterpolator); current function may be used in this case. It is really shallow: it iterates in all \a GodOfDof until the \a FEltSpace is
     * found; if not an exception is thrown.
     *
     * \param[in] felt_space_unique_id \a Unique id of the sought \a FEltSpace.
     * \copydoc doxygen_hide_source_location
     *
     * \return Constant reference to the \a FEltSpace. If none matches, an exception is thrown.
     */
    const FEltSpace& FindFEltSpaceFromIndex(::MoReFEM::FEltSpaceNS::unique_id felt_space_unique_id,
                                            const std::source_location location = std::source_location::current());


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FINDFELTSPACEFROMINDEX_DOT_HPP_
// *** MoReFEM end header guards *** < //
