// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"

#include "Utilities/Mpi/Mpi.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM::Internal::FEltSpaceNS::Impl
{


    InternalStorage ::InternalStorage(const ::MoReFEM::Wrappers::Mpi& mpi,
                                      LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_ref_felt_space)
    : ::MoReFEM::Crtp::CrtpMpi<InternalStorage>(mpi),
      felt_list_per_ref_felt_space_(std::move(felt_list_per_ref_felt_space))
    { }


    bool InternalStorage::IsEmpty() const noexcept
    {
        return GetLocalFEltSpacePerRefLocalFEltSpace().empty();
    }


} // namespace MoReFEM::Internal::FEltSpaceNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
