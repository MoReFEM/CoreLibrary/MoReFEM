// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_IMPL_FELTSPACEINTERNALSTORAGE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_IMPL_FELTSPACEINTERNALSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/Mpi/Mpi.hpp"
#include "Utilities/Mutex/Mutex.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Alias.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::FEltSpaceNS::Impl
{


    /*!
     * \brief Helper class that holds finite element and dof information about either a FEltSpace
     * or a couple FEltSpace/Domain.
     *
     * Should be used only within Internal::FEltSpaceNS::Storage.
     */
    class InternalStorage final : public ::MoReFEM::Crtp::Mutex<InternalStorage>,
                                  public ::MoReFEM::Crtp::CrtpMpi<InternalStorage>
    {

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] felt_list_per_ref_felt_space Finite element list per reference finite element space.
         * It's just stored in the current class; it is actually computed elsewhere.
         */
        explicit InternalStorage(const ::MoReFEM::Wrappers::Mpi& mpi,
                                 LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_ref_felt_space);


        //! Destructor.
        ~InternalStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InternalStorage(const InternalStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InternalStorage(InternalStorage&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InternalStorage& operator=(const InternalStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InternalStorage& operator=(InternalStorage&& rhs) = delete;

        ///@}

      public:
        //! Get the list of all finite element sort per reference felt space and local felt space.
        const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept;

        //! Non constant access to the list of all finite element sort per reference felt space and local
        //! felt space.
        LocalFEltSpacePerRefLocalFEltSpace& GetNonCstFEltListPerRefLocalFEltSpace() noexcept;

        //! Whether there are finite elements in the storage.
        bool IsEmpty() const noexcept;


      private:
        //! List of all finite element sort per reference felt space and local felt space.
        LocalFEltSpacePerRefLocalFEltSpace felt_list_per_ref_felt_space_;
    };


} // namespace MoReFEM::Internal::FEltSpaceNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_IMPL_FELTSPACEINTERNALSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
