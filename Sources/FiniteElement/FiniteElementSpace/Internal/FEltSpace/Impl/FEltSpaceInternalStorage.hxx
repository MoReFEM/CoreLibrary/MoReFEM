// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_IMPL_FELTSPACEINTERNALSTORAGE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_IMPL_FELTSPACEINTERNALSTORAGE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep


namespace MoReFEM::Internal::FEltSpaceNS::Impl
{


    inline const LocalFEltSpacePerRefLocalFEltSpace&
    InternalStorage::GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept
    {
        return felt_list_per_ref_felt_space_;
    }


    inline LocalFEltSpacePerRefLocalFEltSpace& InternalStorage::GetNonCstFEltListPerRefLocalFEltSpace() noexcept
    {
        return const_cast<LocalFEltSpacePerRefLocalFEltSpace&>(GetLocalFEltSpacePerRefLocalFEltSpace());
    }


} // namespace MoReFEM::Internal::FEltSpaceNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_IMPL_FELTSPACEINTERNALSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
