// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// IWYU pragma: no_include <__tree>
#include <algorithm>
#include <cassert>
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/NumberingSubset/UniqueId.hpp"

#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"
#include "Geometry/Mesh/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal { class Parallelism; }
namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    namespace // anonymous
    {


        using mesh_output_directory_storage_type =
            std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>;


    } // namespace


    void InitAllGodOfDof::StandardInit()
    {
        InitNodeBearers();

        ComputeGhostNodeBearerListForCoordsMatching();

        std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, Internal::FEltSpaceNS::connectivity_type>
            connectivity_per_numbering_subset;

        Reduce(connectivity_per_numbering_subset);

        CreateNodesAndDofs();
    }


    void InitAllGodOfDof::InitOutputDirectories(mesh_output_directory_storage_type&& mesh_output_directory_storage)
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance().GetStorage();
        assert(mesh_output_directory_storage.size() == god_of_dof_storage.size());

        // Beware: pointer itself is constant, but underlying object is modified!
        for (const auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            auto& god_of_dof = *god_of_dof_ptr;

            const auto it = mesh_output_directory_storage.find(god_of_dof_id);
            assert(it != mesh_output_directory_storage.cend());
            const auto& mesh_directory_ptr = it->second;

            god_of_dof.InitOutputDirectories(*mesh_directory_ptr);
        }
    }


    void InitAllGodOfDof::FinalizeInitialization(
        const Internal::Parallelism* parallelism,
        DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global)
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance().GetStorage();

        // Beware: pointer itself is constant, but underlying object is modified!
        for (const auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->FinalizeInitialization(do_consider_processor_wise_local_2_global, parallelism);
        }
    }


    void InitAllGodOfDof::InitNodeBearers()
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance().GetStorage();

        // Beware: pointer itself is constant, but underlying object is modified!
        for (const auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            match_interface_node_bearer_per_god_of_dof_.emplace(
                std::make_pair(god_of_dof_ptr, god_of_dof_ptr->InitNodeBearers()));
        }
    }


    void InitAllGodOfDof ::ComputeGhostNodeBearerListForCoordsMatching()
    {
        // The code below is not pretty - it is really a way to circumvent the fact that so far there is no
        // direct link between the _geometric_ singleton handling coords matching and the _finite element_
        // one for the interpolators. That is not ideal and should be corrected later on, e.g. along with
        // #1848.
        decltype(auto) match_interface_node_bearer_per_god_of_dof = GetNonCstMatchInterfaceNodeBearerPerGodOfDof();

        decltype(auto) coords_matching_manager = Internal::MeshNS::CoordsMatchingManager::GetInstance();

        decltype(auto) coords_matching_interpolator_manager =
            Internal::FEltSpaceNS::CoordsMatchingInterpolatorManager::GetInstance();

        decltype(auto) coords_matching_interpolator_list = coords_matching_interpolator_manager.GetStorage();

        decltype(auto) coords_matching_list = coords_matching_manager.GetList();


        for (auto& [god_of_dof_ptr, match_interface_node_bearer] : match_interface_node_bearer_per_god_of_dof)
        {
            assert(!(!god_of_dof_ptr));
            const auto& god_of_dof = *god_of_dof_ptr;
            const auto source_mesh_id = god_of_dof.GetUniqueId();

            decltype(auto) felt_space_list = god_of_dof.GetFEltSpaceList();
            const auto end_felt_space_list = felt_space_list.cend();

            for (const auto& [id, coords_matching_interpolator_ptr] : coords_matching_interpolator_list)
            {
                assert(!(!coords_matching_interpolator_ptr));
                const auto& coords_matching_interpolator = *coords_matching_interpolator_ptr;

                const auto source_felt_space_id = coords_matching_interpolator.GetSourceFEltSpace().GetUniqueId();

                auto it = std::find_if(felt_space_list.cbegin(),
                                       end_felt_space_list,
                                       [source_felt_space_id](const auto& felt_space_ptr)
                                       {
                                           assert(!(!felt_space_ptr));
                                           return felt_space_ptr->GetUniqueId() == source_felt_space_id;
                                       });

                decltype(auto) target_felt_space = coords_matching_interpolator.GetTargetFEltSpace();

                const auto target_mesh_id = target_felt_space.GetGodOfDofFromWeakPtr()->GetUniqueId();

                if (it != end_felt_space_list)
                {
                    const auto it_geometric =
                        std::ranges::find_if(coords_matching_list,

                                             [source_mesh_id, target_mesh_id](const auto& coords_matching_ptr)
                                             {
                                                 assert(!(!coords_matching_ptr));
                                                 return coords_matching_ptr->GetSourceMeshId() == source_mesh_id
                                                        && coords_matching_ptr->GetTargetMeshId() == target_mesh_id;
                                             });

                    assert(it_geometric != coords_matching_list.cend());

                    const auto& coords_matching_ptr = *it_geometric;

                    god_of_dof_ptr->ComputeGhostNodeBearerListForCoordsMatching(
                        *coords_matching_ptr,
                        coords_matching_interpolator.GetSourceNumberingSubset(),
                        match_interface_node_bearer);
                }
            }
        }
    }


    void InitAllGodOfDof::Reduce(
        std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, Internal::FEltSpaceNS::connectivity_type>&
            connectivity_per_numbering_subset)
    {
        decltype(auto) match_interface_node_bearer_per_god_of_dof = GetNonCstMatchInterfaceNodeBearerPerGodOfDof();

        for (auto& [god_of_dof_ptr, match_interface_node_bearer] : match_interface_node_bearer_per_god_of_dof)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->Reduce(match_interface_node_bearer, connectivity_per_numbering_subset);
        }
    }


    void InitAllGodOfDof::CreateNodesAndDofs()
    {
        decltype(auto) match_interface_node_bearer_per_god_of_dof = GetNonCstMatchInterfaceNodeBearerPerGodOfDof();

        for (auto& [god_of_dof_ptr, match_interface_node_bearer] : match_interface_node_bearer_per_god_of_dof)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->CreateNodesAndDofs(match_interface_node_bearer);
        }
    }


} // namespace MoReFEM::Internal::GodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
