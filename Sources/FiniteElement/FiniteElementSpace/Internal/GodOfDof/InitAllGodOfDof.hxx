// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_GODOFDOF_INITALLGODOFDOF_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_GODOFDOF_INITALLGODOFDOF_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp" // IWYU pragma: keep

#include "Geometry/Mesh/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    //  clang-format off
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    //  clang-format on
    InitAllGodOfDof::InitAllGodOfDof(
        const MoReFEMDataT& morefem_data,
        const ModelSettingsT& model_settings,
        DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
        std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
            mesh_output_directory_storage)
    {
        InitFEltSpaceList(morefem_data, model_settings);

        InitOutputDirectories(std::move(mesh_output_directory_storage));

        {
            auto& manager = Internal::FEltSpaceNS::CoordsMatchingInterpolatorManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, morefem_data.GetInputData(), manager);
        }

        decltype(auto) parallelism_strategy = Advanced::ExtractParallelismStrategy(morefem_data);

        switch (parallelism_strategy)
        {
        case Advanced::parallelism_strategy::parallel:
        case Advanced::parallelism_strategy::precompute:
        case Advanced::parallelism_strategy::none:
        case Advanced::parallelism_strategy::parallel_no_write:
        {
            StandardInit();
            break;
        }
        case Advanced::parallelism_strategy::run_from_preprocessed:
        {
            InitFromPreprocessedData(morefem_data);
            break;
        }
        }

        FinalizeInitialization(morefem_data.GetParallelismPtr(), do_consider_processor_wise_local_2_global);
    }


    //  clang-format off
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    //  clang-format on
    void InitAllGodOfDof::InitFEltSpaceList(const MoReFEMDataT& morefem_data, const ModelSettingsT& model_settings)
    {
        decltype(auto) input_data = morefem_data.GetInputData();
        auto felt_space_list_per_god_of_dof_index = FEltSpaceNS::CreateFEltSpaceList(model_settings, input_data);

        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance().GetStorage();

        assert(felt_space_list_per_god_of_dof_index.size() == god_of_dof_storage.size()
               && "It likely means you forgot the 'SetDescription' call for a finite element space "
                  "section in ModelSettings::Init(). It should however have been identified earlier...");

        // Beware: pointer itself is constant, but underlying object is modified!
        for (const auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            auto& god_of_dof = *god_of_dof_ptr;

            auto it = felt_space_list_per_god_of_dof_index.find(god_of_dof_id);
            assert(it != felt_space_list_per_god_of_dof_index.cend());

            god_of_dof.SetFEltSpaceList(std::move(it->second));
        }
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void InitAllGodOfDof::InitFromPreprocessedData(const MoReFEMDataT& morefem_data)
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance().GetStorage();

        // Beware: pointer itself is constant, but underlying object is modified!
        for (const auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->InitFromPreprocessedData(morefem_data);
        }
    }


    inline auto InitAllGodOfDof ::GetNonCstMatchInterfaceNodeBearerPerGodOfDof() noexcept -> helper_type&
    {
        return match_interface_node_bearer_per_god_of_dof_;
    }

} // namespace MoReFEM::Internal::GodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_GODOFDOF_INITALLGODOFDOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
