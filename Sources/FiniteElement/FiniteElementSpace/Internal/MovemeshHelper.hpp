// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_MOVEMESHHELPER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_MOVEMESHHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Miscellaneous.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM::Wrappers::Petsc { template <Utilities::Access AccessT> class AccessVectorContent; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Stores relevant data if the mesh may be moved at some point.
     *
     * In the input data file, it is specified whether a mesh might be moved or not (if so from a displacement
     * vector computed one way or another). In case it might be moved, \a FEltSpace class needs to store
     * additional data; \a MovemeshHelper stores these data for a given \a NumberingSubset.
     */
    struct MovemeshHelper
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MovemeshHelper;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Friendship to FEltSpace.
        friend FEltSpace;

      private:
        /*!
         * \brief Mesh might be moved two ways: either a displacement is applied on initial position, or
         * it is applied to current position.
         */
        enum class From { initial_mesh, current_mesh };


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] god_of_dof God of dof to which the finite element space that calls current class belongs
         * to. \param[in] extended_unknown Encompass the pair unknown/Numbering subset upon which the
         * displacement vector is defined.
         *
         * [DEBUG ONLY] \param[in] felt_space_processor_wise_dof_list List of processor-wise dof list present
         * in the finite element space, used only for a consistency check.
         * [DEBUG ONLY] \param[in] felt_space_ghost_dof_list Same for ghost ones.
         */
        explicit MovemeshHelper(const GodOfDof& god_of_dof,
                                const ExtendedUnknown& extended_unknown
#ifndef NDEBUG
                                ,
                                const Dof::vector_shared_ptr& felt_space_processor_wise_dof_list,
                                const Dof::vector_shared_ptr& felt_space_ghost_dof_list
#endif // NDEBUG
        );

        //! Destructor.
        ~MovemeshHelper() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MovemeshHelper(const MovemeshHelper& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MovemeshHelper(MovemeshHelper&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        MovemeshHelper& operator=(const MovemeshHelper& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MovemeshHelper& operator=(MovemeshHelper&& rhs) = delete;

        ///@}

        /*!
         * \brief Move the mesh given the displacement vector.
         *
         * \param[in] displacement \a GlobalVector which includes the displacement applied to the mesh.
         * \param[in] from Whether the displacement is applied upon the initial position or the current position
         * of the mesh.
         */
        void Movemesh(const GlobalVector& displacement, From from) const;

      private:
        //! Returns the number of coordinates considered.
        std::size_t Ncoords() const noexcept;

        //! Return the list of all dofs related to given \a coord_index.
        //! \param[in] coord_index Position of the \a Coords considered in the storage vector.
        const std::vector<::MoReFEM::DofNS::processor_wise_or_ghost_index>&
        GetDofIndexList(::MoReFEM::CoordsNS::processor_wise_position coord_index) const noexcept;

        //! Accessor to the god of dof related to the mesh to move.
        const GodOfDof& GetGodOfDof() const noexcept;

        //! Get the initial coordinates of a given coords.
        //! \param[in] coord_index Position of the \a Coords considered in the storage vector.
        const Eigen::Vector3d&
        GetInitialPosition(::MoReFEM::CoordsNS::processor_wise_position coord_index) const noexcept;


      private:
        /*!
         * \brief An helper method for the constructor.
         *
         * Constructor does basically the same work for two cases: processor-wise \a Coords and ghost ones;
         * current method is the way to respect the DRY principle.
         *
         * \param[in] node_bearer_list \a NodeBearer list to consider (either processor-wise or ghost ones).
         * \param[in] extended_unknown Encompass the pair unknown/Numbering subset upon which the displacement
         * vector is defined.
         *
         * [DEBUG ONLY] \param[in] felt_space_dof_list List of dof list present
         * in the finite element space to consider, used only for a consistency check. It might be either
         * the processor-wise one or the ghost ones.
         * [DEBUG ONLY] \param[in] mesh Mesh considered.
         */
        void ConstructHelper(const NodeBearer::vector_shared_ptr& node_bearer_list,
                             const ExtendedUnknown& extended_unknown
#ifndef NDEBUG
                             ,
                             const Mesh& mesh,
                             const Dof::vector_shared_ptr& felt_space_dof_list
#endif // NDEBUG
        );

        /*!
         * \brief An helper method of \a Movemesh method which move all the \a Coords of a given list.
         *
         * The idea is to call it twice: one for processor-wise \a Coords and another for ghost ones.
         *
         * \param[in] coords_list List of \a Coords considered.
         * \param[in] vector_content A viewer upon the \a GlobalVector which includes the displacement applied
         * to the mesh.
         * \param[in] from Whether the displacement is applied upon the initial position or the current position
         * of the mesh.
         *
         */
        void
        MoveCoords(const Coords::vector_shared_ptr& coords_list,
                   const ::MoReFEM::Wrappers::Petsc ::AccessVectorContent<Utilities::Access::read_only>& vector_content,
                   From from) const;


      private:
        //! Store the list of processor-wise or ghost dof indexes for each \a Coord.
        std::vector<std::vector<::MoReFEM::DofNS::processor_wise_or_ghost_index>> dof_index_list_per_coord_;

        //! Store the original position of each coord.
        std::vector<Eigen::Vector3d> initial_position_per_coord_;

        //! God of dof related to the mesh to move.
        const GodOfDof& god_of_dof_;

#ifndef NDEBUG
        /*!
         * \brief Numbering subset.
         *
         * \internal <b><tt>[internal]</tt></b> Not that relevant in release as in FEltSpace current object
         * is stored in a container which key is numbering subset index.
         * \endinternal
         */
        const NumberingSubset& numbering_subset_;
#endif // NDEBUG
    };


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_MOVEMESHHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
