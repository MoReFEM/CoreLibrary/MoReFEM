// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_MOVEMESHHELPER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_MOVEMESHHELPER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hpp"
// *** MoReFEM header guards *** < //


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/Coords/StrongType.hpp"

#include "FiniteElement/Nodes_and_dofs/DofIndexesTypes.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    inline std::size_t MovemeshHelper::Ncoords() const noexcept
    {
        return dof_index_list_per_coord_.size();
    }


    inline const std::vector<::MoReFEM::DofNS::processor_wise_or_ghost_index>&
    MovemeshHelper::GetDofIndexList(::MoReFEM::CoordsNS::processor_wise_position index) const noexcept
    {
        assert(index.Get() < dof_index_list_per_coord_.size());
        return dof_index_list_per_coord_[index.Get()];
    }


    inline const GodOfDof& MovemeshHelper::GetGodOfDof() const noexcept
    {
        return god_of_dof_;
    }


    inline auto
    MovemeshHelper::GetInitialPosition(::MoReFEM::CoordsNS::processor_wise_position coord_index) const noexcept
        -> const Eigen::Vector3d&
    {
        assert(coord_index.Get() < initial_position_per_coord_.size());
        return initial_position_per_coord_[coord_index.Get()];
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_MOVEMESHHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
