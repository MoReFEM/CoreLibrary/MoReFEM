// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_NDOFHOLDER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_NDOFHOLDER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <utility>

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    inline std::size_t NdofHolder::NprogramWiseDof() const noexcept
    {
        return Nprogram_wise_dof_;
    }


    inline std::size_t NdofHolder::NprocessorWiseDof() const noexcept
    {
        return Nprocessor_wise_dof_;
    }


    inline const std::map<::MoReFEM::NumberingSubsetNS::unique_id, std::size_t>&
    NdofHolder::NprocessorWiseDofPerNumberingSubset() const noexcept
    {
        assert(!Nprocessor_wise_dof_per_numbering_subset_.empty());
        return Nprocessor_wise_dof_per_numbering_subset_;
    }


    inline const std::map<::MoReFEM::NumberingSubsetNS::unique_id, std::size_t>&
    NdofHolder::NprogramWiseDofPerNumberingSubset() const noexcept
    {
        assert(!Nprogram_wise_dof_per_numbering_subset_.empty());
        return Nprogram_wise_dof_per_numbering_subset_;
    }


    inline std::size_t NdofHolder::NprocessorWiseDof(const NumberingSubset& numbering_subset) const
    {
        const auto& Nprocessor_wise_dof_per_numbering_subset = NprocessorWiseDofPerNumberingSubset();

        const auto it = Nprocessor_wise_dof_per_numbering_subset.find(numbering_subset.GetUniqueId());

        assert(it != Nprocessor_wise_dof_per_numbering_subset.cend());
        return it->second;
    }


    inline std::size_t NdofHolder::NprogramWiseDof(const NumberingSubset& numbering_subset) const
    {
        const auto& Nprogram_wise_dof_per_numbering_subset = NprogramWiseDofPerNumberingSubset();

        const auto it = Nprogram_wise_dof_per_numbering_subset.find(numbering_subset.GetUniqueId());

        assert(it != Nprogram_wise_dof_per_numbering_subset.cend());
        return it->second;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_NDOFHOLDER_DOT_HXX_
// *** MoReFEM end header guards *** < //
