// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <filesystem>
#include <memory>
#include <optional>
#include <regex>
#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{


    OutputDirectoryStorage::OutputDirectoryStorage(
        const ::MoReFEM::FilesystemNS::Directory& output_directory,
        const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
        wildcard_for_rank is_wildcard,
        std::source_location location)
    {
        if (is_wildcard == wildcard_for_rank::yes)
        {
            const std::regex re("Rank_[0-9]+");

            auto string = output_directory.GetDirectoryEntry().path().native();
            string = std::regex_replace(string, re, "Rank_*");

            output_directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(
                std::filesystem::path(string), ::MoReFEM::FilesystemNS::behaviour::ignore);
        } else
        {
            if (!output_directory.DoExist())
            {
                std::ostringstream oconv;
                oconv << "Directory " << output_directory << " is assumed to exist before this constructor!";
                throw Exception(oconv.str());
            }

            output_directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(output_directory);
            output_directory_->SetBehaviour(::MoReFEM::FilesystemNS::behaviour::ignore);
        }

        output_directory_->ActOnFilesystem();

        std::ostringstream oconv;
        output_directory_per_numbering_subset_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;
            const auto numbering_subset_id = numbering_subset.GetUniqueId();

            oconv.str("");
            oconv << "NumberingSubset_" << numbering_subset_id;

            auto subdirectory = ::MoReFEM::FilesystemNS::Directory(
                GetOutputDirectory(), oconv.str(), ::MoReFEM::FilesystemNS::behaviour::create, location);

            [[maybe_unused]] auto check = output_directory_per_numbering_subset_.insert(
                std::make_pair(numbering_subset_id, std::move(subdirectory)));
            assert(check.second);
        }
    }


    const ::MoReFEM::FilesystemNS::Directory&
    OutputDirectoryStorage ::GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto id = numbering_subset.GetUniqueId();

        decltype(auto) output_directory_per_numbering_subset = GetOutputDirectoryPerNumberingSubset();

        const auto it = output_directory_per_numbering_subset.find(id);

        assert(it != output_directory_per_numbering_subset.cend()
               && "All relevant numbering subsets should have been loaded into the map.");

        return it->second;
    }


} // namespace MoReFEM::Internal::GodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
