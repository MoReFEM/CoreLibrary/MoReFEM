// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_OUTPUTDIRECTORYSTORAGE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_OUTPUTDIRECTORYSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //


// IWYU pragma: no_include <iosfwd>

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string> // IWYU pragma: keep
#include <unordered_map>

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{

    //! Whether we want true output directory or the one in which rank is replace by a wildcard.
    enum class wildcard_for_rank { yes, no };


    /*!
     * \brief Facility to store the paths to the output directories related to a \a GodOfDof.
     */
    class OutputDirectoryStorage
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = OutputDirectoryStorage;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] output_directory The output directory in which all outputs are written. One purpose of this class
         * is to manage more finely some subdirectories of this one. This class does not create it and expects it
         * to already exist (except if wildcard_for_rank is set to yes).
         * \param[in] numbering_subset_list List of \a NumberingSubset relevant to the \a GodOfDof.
         * \param[in] is_wildcard If wildcard_for_rank::yes, the rank number is replaced by '*' in \a output_directory.
         * For instance, '/foo/.../Rank_0/...'' is replaced by '/foo/.../Rank_STAR/...' where STAR is '*'. An exception
         * is thrown if 'Rank_' is not found in \a output_directory.
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit OutputDirectoryStorage(const ::MoReFEM::FilesystemNS::Directory& output_directory,
                                        const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                                        wildcard_for_rank is_wildcard = wildcard_for_rank::no,
                                        const std::source_location location = std::source_location::current());

        //! Destructor.
        ~OutputDirectoryStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        OutputDirectoryStorage(const OutputDirectoryStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        OutputDirectoryStorage(OutputDirectoryStorage&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        OutputDirectoryStorage& operator=(const OutputDirectoryStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        OutputDirectoryStorage& operator=(OutputDirectoryStorage&& rhs) = delete;

        ///@}

      public:
        //! Path to the global output directory.
        const ::MoReFEM::FilesystemNS::Directory& GetOutputDirectory() const noexcept;

        /*!
         * \brief Returns the name of the subfolder related to a given numbering subset.
         *
         * For instance
         *
         * \code
         * *path_to_output_directory* /NumberingSubset*i*
         * \endcode
         *
         * where i is the unique id of the numbering subset.
         *
         * \param[in] numbering_subset Numbering subset for which output folder is sought.
         *
         * \return Name of the subfolder related to a given numbering subset.
         */
        const ::MoReFEM::FilesystemNS::Directory&
        GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const;

      private:
        //! Accessor to \a output_directory_per_numbering_subset_.
        const std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, ::MoReFEM::FilesystemNS::Directory>&
        GetOutputDirectoryPerNumberingSubset() const noexcept;

      private:
        //! Path to the global output directory.
        std::unique_ptr<::MoReFEM::FilesystemNS::Directory> output_directory_{ nullptr };

        /*!
         * \brief For each \a NumberingSubset, store the path to the related subdirectory.
         *
         * This is stored rather than recomputed on the fly to avoid reallocating memory each time a call is needed.
         *
         * - Key: unique id of the \a NumberingSubset (as given by GetUniqueId() method).
         * - Value: path to the dedicated
         */
        std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, ::MoReFEM::FilesystemNS::Directory>
            output_directory_per_numbering_subset_;
    };


} // namespace MoReFEM::Internal::GodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

#include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_OUTPUTDIRECTORYSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
