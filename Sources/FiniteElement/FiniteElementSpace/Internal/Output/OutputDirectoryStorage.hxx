// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_OUTPUTDIRECTORYSTORAGE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_OUTPUTDIRECTORYSTORAGE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <unordered_map>

#include "Utilities/Filesystem/Directory.hpp" // IWYU pragma: keep

#include "Core/NumberingSubset/UniqueId.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::GodOfDofNS
{


    inline const ::MoReFEM::FilesystemNS::Directory& OutputDirectoryStorage::GetOutputDirectory() const noexcept
    {
        assert(!(!output_directory_));
        return *output_directory_;
    }


    inline const std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, ::MoReFEM::FilesystemNS::Directory>&
    OutputDirectoryStorage::GetOutputDirectoryPerNumberingSubset() const noexcept
    {
        return output_directory_per_numbering_subset_;
    }


} // namespace MoReFEM::Internal::GodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_OUTPUTDIRECTORYSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
