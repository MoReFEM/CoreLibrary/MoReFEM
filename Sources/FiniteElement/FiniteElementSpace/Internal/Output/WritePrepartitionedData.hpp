// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_WRITEPREPARTITIONEDDATA_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_WRITEPREPARTITIONEDDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{

    /*!
     * \brief Write prepartitioned data related to \a GodOfDof.
     *
     * \param[in] god_of_dof \a GodOfDof for which data are written.
     * \param[in] mesh_directory Directory in which the file will be created; remember there is exactly one \a
     * GodOfDof per \a Mesh.
     */
    void WritePrepartitionedData(const GodOfDof& god_of_dof, const ::MoReFEM::FilesystemNS::Directory& mesh_directory);


} // namespace MoReFEM::Internal::GodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

#include "FiniteElement/FiniteElementSpace/Internal/Output/WritePrepartitionedData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_OUTPUT_WRITEPREPARTITIONEDDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
