// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <__tree>
#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <limits>
#include <memory>
#include <optional>
#include <set>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

#include "FiniteElement/FiniteElementSpace/Internal/Partition/AssignGeomEltToProcessor.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    namespace // anonymous
    {


        /*!
         * \brief Check whether we are in the tirival case in which all \a NodeBearer belong to the same processor.
         *
         * If so, simply return the rank. If not, return nullopt.
         */
        std::optional<rank_type> AreAllNodeBearerOnSameProcessor(const LocalFEltSpace& local_felt_space);


        /*!
         * \brief Extract all the processors involved within the \a NodeBearer list, be it as main rank or ghost.
         */
        std::set<rank_type>
        ExtractRanksFromNodeBearerList(const NodeBearer::vector_shared_ptr& geom_elt_node_bearer_list);


        /*!
         * \brief Update an existing \a ProcessorData object by (possibly) adding the ghost ranks required
         * by the \a local_felt_space in the \a FEltSpace currently considered.
         *
         * If this function is called, it means ANOTHER \a FEltSpace already dealt with the current
         * underlying \a GeomElt - but this doesn't mean this other \a FEltSpace made the same decisions
         * regarding repartition.
         *
         * \param[in] local_felt_space \a LocalFEltSpace under consideration.
         * \param[in] iterator Iterator to the data already associated to the underlying \a GeometricElt. It is
         * expected this iterator is valid (not processor_for_each_geom_elt_.cend()...)
         *
         * These data may be updated by current function.
         */
        void Update(const LocalFEltSpace& local_felt_space,
                    AssignGeomEltToProcessor::processor_for_each_geom_elt_iterator iterator);


    } // namespace


    AssignGeomEltToProcessor::AssignGeomEltToProcessor(const GodOfDof& god_of_dof,
                                                       const rank_type Nprocessor,
                                                       MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        Nlocal_felt_space_per_processor_.resize(Nprocessor.Get(), 0);
        processor_for_each_geom_elt_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        decltype(auto) felt_space_list = god_of_dof.GetFEltSpaceList();

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            auto& felt_space = *felt_space_ptr;

            // Beware: container itself is constant, but the elements it contains are modified by current method!
            const auto& felt_storage = felt_space.GetFEltStorage();

            // > At this point the 'processor-wise' should in fact encompass all the program-wise data - current
            // class is to be called before the reduction...
            // Beware: container itself is constant, but the elements it contains are modified by current method!
            const auto& felt_list_per_type =
                felt_storage.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

            for (const auto& [ref_local_felt_space_ptr, local_felt_space_per_geom_elt_index] : felt_list_per_type)
            {
                for (const auto& [geom_elt_index, local_felt_space_ptr] : local_felt_space_per_geom_elt_index)
                {
                    assert(!(!local_felt_space_ptr));
                    const auto& local_felt_space = *local_felt_space_ptr;

                    // Determine to which processor the local finite element space should be attributed.
                    AssignProcessorForCurrentLocalFEltSpace(local_felt_space);
                }
            }
        }

        // Now cover the very specific case of \a GeometricElt boundary conditions, that might not be considered yet
        // by the finite element iteration above.
        AssignBoundaryConditionGeometricElts(match_interface_node_bearer);

        AssignFromCoordsMatchingGeometricElts(god_of_dof.GetMesh(), match_interface_node_bearer);
    }


    std::optional<AssignGeomEltToProcessor::processor_for_each_geom_elt_iterator>
    AssignGeomEltToProcessor ::IsAlreadyAttributed(const LocalFEltSpace& local_felt_space)
    {
        const auto local_felt_space_id = local_felt_space.GetGeometricElt().GetIndex();

        auto it = processor_for_each_geom_elt_.find(local_felt_space_id);

        if (it != processor_for_each_geom_elt_.cend())
            return it;

        return std::nullopt;
    }


    auto AssignGeomEltToProcessor ::IsVolumicNodeBearer(const LocalFEltSpace& local_felt_space) noexcept
        -> std::optional<rank_type>
    {
        decltype(auto) node_bearer_list = local_felt_space.GetNodeBearerList();

        auto begin = node_bearer_list.cbegin();
        auto end = node_bearer_list.cend();

        auto is_node_on_volume = [](const NodeBearer::shared_ptr& node_bearer_ptr)
        {
            assert(!(!node_bearer_ptr));
            return node_bearer_ptr->GetNature() == ::MoReFEM::InterfaceNS::Nature::volume;
        };

        auto it = std::find_if(begin, end, is_node_on_volume);

        if (it != end)
        {
            assert(std::count_if(begin, end, is_node_on_volume) == 1UL
                   && "There should be at most one node bearer on volume, and a test has already be passed "
                      "to ensure there was at least one...");

            const auto& node_bearer_on_volume_ptr = *it;
            const auto ret = node_bearer_on_volume_ptr->GetProcessor();

            return ret;
        }

        return std::nullopt;
    }


    void AssignGeomEltToProcessor::NewLocalFEltSpace(const LocalFEltSpace& local_felt_space, rank_type rank)
    {
        assert(rank.Get() < Nlocal_felt_space_per_processor_.size());
        ++Nlocal_felt_space_per_processor_[rank.Get()];

        const auto local_felt_space_id = local_felt_space.GetGeometricElt().GetIndex();

        ProcessorData data;
        data.rank = rank;

        [[maybe_unused]] auto [iterator, is_properly_inserted] =
            processor_for_each_geom_elt_.insert({ local_felt_space_id, data });

        assert(is_properly_inserted);
        assert(iterator != processor_for_each_geom_elt_.cend());

        Update(local_felt_space, iterator);
    }


    void AssignGeomEltToProcessor ::DetermineRanksForSpecialGeometricElt(
        const GeometricElt& geom_elt,
        const NodeBearer::vector_shared_ptr& geom_elt_node_bearer_list)

    {
        const auto geom_elt_id = geom_elt.GetIndex();

        auto rank_list = ExtractRanksFromNodeBearerList(geom_elt_node_bearer_list);

        const auto it = processor_for_each_geom_elt_.find(geom_elt_id);

        if (it != processor_for_each_geom_elt_.cend())
        {
            auto& current_geom_elt_data = it->second;
            rank_list.erase(current_geom_elt_data.rank);

            std::ranges::copy(
                rank_list,

                std::inserter(current_geom_elt_data.ghost_rank_list, current_geom_elt_data.ghost_rank_list.begin()));
        } else
        {
            ProcessorData processor_data;

            processor_data.rank = DetermineMainRank(geom_elt_node_bearer_list);

            rank_list.erase(processor_data.rank);
            processor_data.ghost_rank_list = rank_list;

            [[maybe_unused]] auto [iterator, is_properly_inserted] =
                processor_for_each_geom_elt_.insert({ geom_elt_id, processor_data });

            assert(is_properly_inserted);
        }
    }


    void AssignGeomEltToProcessor ::AssignProcessorForCurrentLocalFEltSpace(const LocalFEltSpace& local_felt_space)
    {
        // First check whether a choice has already be made by a previous finite element space.
        // If so, stick with it - but possibly add supplementary ghost ranks if relevant
        if (auto case_1 = IsAlreadyAttributed(local_felt_space))
        {
            assert(case_1.value() != processor_for_each_geom_elt_.cend());
            Update(local_felt_space, case_1.value());
        }

        // Check whether there are \a NodeBearer on different processors - if not the choice is trivial...
        else if (auto case_2 = AreAllNodeBearerOnSameProcessor(local_felt_space))
        {
            NewLocalFEltSpace(local_felt_space, case_2.value());
        }

        // If there is a node bearer on volume, assign the finite element group to the same processor.
        else if (auto case_3 = IsVolumicNodeBearer(local_felt_space))
        {
            NewLocalFEltSpace(local_felt_space, case_3.value());
        } else
        {
            const auto main_rank = DetermineMainRank(local_felt_space);
            NewLocalFEltSpace(local_felt_space, main_rank);
        }
    }


    std::size_t AssignGeomEltToProcessor::NlocalFEltSpace(rank_type processor) const
    {
        assert(processor.Get() < Nlocal_felt_space_per_processor_.size());
        return Nlocal_felt_space_per_processor_[processor.Get()];
    }


    auto AssignGeomEltToProcessor::Nprocessor() const -> rank_type
    {
        return rank_type{ Nlocal_felt_space_per_processor_.size() };
    }


    std::vector<std::size_t>
    AssignGeomEltToProcessor ::NnodeBearerPerProcessor(const NodeBearer::vector_shared_ptr& node_bearer_list) const
    {
        const auto Nprocessor = this->Nprocessor();
        std::vector<std::size_t> ret(Nprocessor.Get(), 0UL);

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            auto node_bearer_processor = node_bearer_ptr->GetProcessor();

            assert(node_bearer_processor < Nprocessor);

            ++ret[node_bearer_processor.Get()];
        }

        return ret;
    }


    const ProcessorData& AssignGeomEltToProcessor ::GetProcessorData(const LocalFEltSpace& local_felt_space) const
    {
        return GetProcessorData(local_felt_space.GetGeometricElt());
    }


    const ProcessorData& AssignGeomEltToProcessor::GetProcessorData(const GeometricElt& geom_elt) const
    {
        const auto index = geom_elt.GetIndex();

        const auto it = processor_for_each_geom_elt_.find(index);
        assert(it != processor_for_each_geom_elt_.cend());

        return it->second;
    }


    rank_type AssignGeomEltToProcessor::DetermineMainRank(const NodeBearer::vector_shared_ptr& node_bearer_list) const
    {
        // If not, look the processor that holds the most node bearers.
        auto&& Nnode_bearer_on_each_processor = NnodeBearerPerProcessor(node_bearer_list);

        assert(Nnode_bearer_on_each_processor.size() == Nlocal_felt_space_per_processor_.size());

        auto max_position = std::ranges::max_element(Nnode_bearer_on_each_processor);

        const auto max_value = *max_position;

        const auto size = rank_type{ Nnode_bearer_on_each_processor.size() };

        auto main_rank = rank_type{ NumericNS::UninitializedIndex<std::size_t>() };

        // Is the maximum unique or not?
        if (std::count(Nnode_bearer_on_each_processor.cbegin(), Nnode_bearer_on_each_processor.cend(), max_value) == 1)
            main_rank = rank_type{ static_cast<std::size_t>(max_position - Nnode_bearer_on_each_processor.cbegin()) };
        // If not, attribute the geometric element to the one with the most ghosts already.
        else
        {
            std::vector<rank_type> candidates;

            for (auto i = rank_type{ 0UL }; i < size; ++i)
            {
                if (Nnode_bearer_on_each_processor[i.Get()] == max_value)
                    candidates.push_back(i);
            }

            // Choose among candidates the one with most ghosts.
            assert(candidates.size() >= 2);

            std::size_t Nfelt_min = std::numeric_limits<std::size_t>::max();

            for (auto candidate : candidates)
            {
                assert(candidate.Get() < Nlocal_felt_space_per_processor_.size());

                if (NlocalFEltSpace(candidate) < Nfelt_min)
                {
                    main_rank = candidate;
                    Nfelt_min = NlocalFEltSpace(candidate);
                }
            }
        } // else

        return main_rank;
    }


    rank_type AssignGeomEltToProcessor::DetermineMainRank(const LocalFEltSpace& local_felt_space) const
    {
        return DetermineMainRank(local_felt_space.GetNodeBearerList());
    }


    bool AssignGeomEltToProcessor::IsProcessorWise(const ::MoReFEM::Wrappers::Mpi& mpi,
                                                   const GeometricElt& geom_elt) const
    {
        // Important: we can't use here the GetProcessorData() method, as this method will be called for all the
        // \a GeometricElt of the original mesh - including those not kept at all after the reduction.

        const auto it = processor_for_each_geom_elt_.find(geom_elt.GetIndex());

        if (it == processor_for_each_geom_elt_.cend())
            return false;

        const auto geom_elt_rank = it->second.rank;

        return mpi.GetRank() == geom_elt_rank;
    }


    bool AssignGeomEltToProcessor::IsGhost(const ::MoReFEM::Wrappers::Mpi& mpi, const GeometricElt& geom_elt) const
    {
        // Important: we can't use here the GetProcessorData() method, as this method will be called for all the
        // \a GeometricElt of the original mesh - including those not kept at all after the reduction.

        const auto it = processor_for_each_geom_elt_.find(geom_elt.GetIndex());

        if (it == processor_for_each_geom_elt_.cend())
            return false;

        const auto& geom_elt_ghost_rank_list = it->second.ghost_rank_list;

        const auto it_rank = geom_elt_ghost_rank_list.find(mpi.GetRank());

        return it_rank != geom_elt_ghost_rank_list.cend();
    }


    namespace
    {


        /*!
         * \brief Returns the unoriented interface if OrientedInterfaceT in { \a OrientedEdge, \a OrientedFace } or the \a Vertex if
         * OrientedInterfaceT == Vertex.
         *
         * \a Volume is not possible here.
         */
        template<class OrientedInterfaceT>
        const Interface& UnorientedInterface(const OrientedInterfaceT& oriented_interface);


        /*!
         * \brief Extract the list of \a NodeBearer associated to a given \a GeometricElt and a type of interface.
         *
         * This method is expected to be called several times for a given \a GeometricElt: once per type of \a
         * Interface.
         *
         * \param[in] interface_list List of \a Interface scrutinized, typically given by \a GeometricElt::GetVertexList(),
         * \a GeometricElt::GetOrientedEdgeList() or \a GeometricElt::GetOrientedFaceList().
         * \param[in] from_coords_matching_node_bearer_list The result of MatchInterfaceNodeBearer::GetCoordsMatchingData().
         * \param[in,out] node_bearer_list_for_current_geom_elt The list \a NodeBearer related to the \a GeometricElt that
         * gave the \a interface_list.
         *
         * \return True if at least one of the item of  \a interface_list match with \a from_coords_matching_node_bearer_list.
         */
        template<class InterfaceT>
        bool ExtractGeometricEltNodeBearerList(
            const typename InterfaceT::vector_shared_ptr& interface_list,
            const Internal::NodeBearerNS::node_bearer_per_coords_index_list_type& from_coords_matching_node_bearer_list,
            NodeBearer::vector_shared_ptr& node_bearer_list_for_current_geom_elt);


    } // namespace


    void AssignGeomEltToProcessor ::AssignBoundaryConditionGeometricElts(
        const MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        const auto& bc_data = match_interface_node_bearer.boundary_conditions_data_;

        for (const auto& [geom_elt_ptr, node_bearer_list] : bc_data)
            DetermineRanksForSpecialGeometricElt(*geom_elt_ptr, node_bearer_list);
    }


    void AssignGeomEltToProcessor::AssignFromCoordsMatchingGeometricElts(
        const Mesh& mesh,
        const MatchInterfaceNodeBearer& match_interface_node_bearer)
    {
        std::unordered_map<GeometricElt::shared_ptr, NodeBearer::vector_shared_ptr> fvm_data;

        // Here add GeometricElt concerned by FromCoordsMatching...
        {
            decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();
            // < still the full program-wise list at this stage

            decltype(auto) from_coords_matching_node_bearer_list = match_interface_node_bearer.GetCoordsMatchingData();

            // clang-format off
            std::unordered_set
            <
                std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>,
                Utilities::ContainerHash
            > list;
            // clang-format on

            for (const auto& [coords_list, node_bearer_ptr] : from_coords_matching_node_bearer_list)
                list.insert(node_bearer_ptr->GetInterface()
                                .ComputeCoordsIndexList<::MoReFEM::CoordsNS::index_enum::from_mesh_file>());

            NodeBearer::vector_shared_ptr node_bearer_list_for_current_geom_elt;

            for (const auto& geom_elt_ptr : geom_elt_list)
            {
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                bool do_consider{ false };

                do_consider = ExtractGeometricEltNodeBearerList<Vertex>(geom_elt.GetVertexList(),
                                                                        from_coords_matching_node_bearer_list,
                                                                        node_bearer_list_for_current_geom_elt);

                do_consider |= ExtractGeometricEltNodeBearerList<OrientedEdge>(geom_elt.GetOrientedEdgeList(),
                                                                               from_coords_matching_node_bearer_list,
                                                                               node_bearer_list_for_current_geom_elt);

                do_consider |= ExtractGeometricEltNodeBearerList<OrientedFace>(geom_elt.GetOrientedFaceList(),
                                                                               from_coords_matching_node_bearer_list,
                                                                               node_bearer_list_for_current_geom_elt);

                if (do_consider)
                    fvm_data.insert({ geom_elt_ptr, node_bearer_list_for_current_geom_elt });
            }
        }

        for (const auto& [geom_elt_ptr, node_bearer_list] : fvm_data)
            DetermineRanksForSpecialGeometricElt(*geom_elt_ptr, node_bearer_list);
    }


    namespace // anonymous
    {


        /*!
         * \brief Check whether we are in the tirival case in which all \a NodeBearer belong to the same processor.
         *
         * If so, simply return the rank. If not, return nullopt.
         */
        std::optional<rank_type> AreAllNodeBearerOnSameProcessor(const LocalFEltSpace& local_felt_space)
        {
            decltype(auto) node_bearer_list = local_felt_space.GetNodeBearerList();

            assert(!node_bearer_list.empty());

            assert(std::ranges::none_of(node_bearer_list, Utilities::IsNullptr<NodeBearer::shared_ptr>));

            const auto rank_last_elt = node_bearer_list.back()->GetProcessor();

            if (std::ranges::all_of(node_bearer_list,

                                    [rank_last_elt](const auto& node_bearer_ptr)
                                    {
                                        return node_bearer_ptr->GetProcessor() == rank_last_elt;
                                    }))
                return rank_last_elt;

            return std::nullopt;
        }


        std::set<rank_type>
        ExtractRanksFromNodeBearerList(const NodeBearer::vector_shared_ptr& geom_elt_node_bearer_list)
        {
            std::set<rank_type> ret;

            for (const auto& node_bearer_ptr : geom_elt_node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;

                ret.insert(node_bearer.GetProcessor());

                decltype(auto) ghost_processor_list = node_bearer.ComputeGhostProcessorList();

                std::ranges::copy(ghost_processor_list, std::inserter(ret, ret.begin()));
            }

            return ret;
        }


        template<class OrientedInterfaceT>
        const Interface& UnorientedInterface(const OrientedInterfaceT& oriented_interface)
        {
            if constexpr (std::is_same<OrientedInterfaceT, Vertex>())
                return oriented_interface;
            else // without it compile error...
                return oriented_interface.GetUnorientedInterface();
        }


        template<class InterfaceT>
        bool ExtractGeometricEltNodeBearerList(
            const typename InterfaceT::vector_shared_ptr& interface_list,
            const Internal::NodeBearerNS::node_bearer_per_coords_index_list_type& from_coords_matching_node_bearer_list,
            NodeBearer::vector_shared_ptr& node_bearer_list_for_current_geom_elt)
        {
            bool ret{ false };

            for (const auto& interface_ptr : interface_list)
            {
                assert(!(!interface_ptr));
                decltype(auto) coords_list =
                    UnorientedInterface(*interface_ptr)
                        .template ComputeCoordsIndexList<::MoReFEM::CoordsNS::index_enum::from_mesh_file>();

                auto it = from_coords_matching_node_bearer_list.find(coords_list);

                if (it != from_coords_matching_node_bearer_list.cend())
                {
                    ret = true;
                    node_bearer_list_for_current_geom_elt.push_back(it->second);
                }
            }

            return ret;
        }


        void Update(const LocalFEltSpace& local_felt_space,
                    AssignGeomEltToProcessor::processor_for_each_geom_elt_iterator iterator)
        {
            assert(local_felt_space.GetGeometricElt().GetIndex() == iterator->first);

            auto& processor_data = iterator->second;

            const auto geom_elt_rank = processor_data.rank;
            auto& ghost_rank_list = processor_data.ghost_rank_list;

            decltype(auto) node_bearer_list = local_felt_space.GetNodeBearerList();

            for (const auto& node_bearer_ptr : node_bearer_list)
            {
                const auto current_rank = node_bearer_ptr->GetProcessor();
                if (current_rank != geom_elt_rank)
                    ghost_rank_list.insert(current_rank);
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
