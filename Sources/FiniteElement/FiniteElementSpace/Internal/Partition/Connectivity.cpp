// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{

    namespace // anonymous
    {


        /*!
         * \brief Fill the \a connectivity with the information from a given \a felt_space.
         *
         * \param node_bearer_filter  A functor (or lambda) which indicates which \a NodeBearer should be kept in \a connectivity.
         * The most generic case is to keep \a NodeBearer on the current processor, but for the sake of
         * \a CoordsMatching I sometime needs another rule.
         */
        template<class FunctorT>
        void ComputeNodeBearerConnectivityInFEltSpace(
            const FEltSpace& felt_space,
            const FunctorT& node_bearer_filter,
            connectivity_type& connectivity,
            ::MoReFEM::Internal::FEltSpaceNS::optional_ref_connectivity_per_numbering_subset_type
                connectivity_per_numbering_subset);

        /*!
         * \brief Helper function of ComputeNodeBearerConnectivityInFEltSpace which tracks all the key \a NodeBearer.
         *
         * To fill the \a connectivity space, we need to iterate through the \a LocalFEltSpace to identify which values
         * must be connected to a given key. However, there is a difficulty as some of these \a LocalFEltSpace might be
         * handled by another rank. So this helper function is called twice: once for processor-wise \a LocalFEltSpace
         * and another one for the ghost ones.
         *
         * \param node_bearer_filter  A functor (or lambda) which indicates which \a NodeBearer should be kept in \a connectivity.
         * The most generic case is to keep \a NodeBearer on the current processor, but for the sake of
         * \a CoordsMatching I sometime needs another rule.
         */
        template<RoleOnProcessor RoleOnProcessorT, class FunctorT>
        void IterateThroughLocalFEltSpace(
            const FEltSpace& felt_space,
            const FunctorT& node_bearer_filter,
            connectivity_type& connectivity,
            optional_ref_connectivity_per_numbering_subset_type connectivity_per_numbering_subset);


        /*!
         * \brief Sort and eliminate duplicates.
         *
         * \param[in,out] connectivity Key is the \a NodeBearer for which we track connectivity, value the list of
         * connected \a NodeBearer. In output, values are sort (according to \a NodeBearer ordering relationship) and
         * duplicates are removed.
         * \param[in] do_keep_self_connection If 'yes', the key \a NodeBearer is kept in the
         * list. We need to be able to provide both version to comply with interface of third party libraries.
         */
        void CleanUpConnectivity(connectivity_type& connectivity, KeepSelfconnection do_keep_self_connection);


    } // namespace


    connectivity_type
    ComputeNodeBearerConnectivity(const FEltSpace::vector_unique_ptr& felt_space_list,
                                  const std::size_t Nprocessor_wise_node_bearer,
                                  KeepSelfconnection do_keep_self_connection,
                                  ::MoReFEM::Internal::FEltSpaceNS::optional_ref_connectivity_per_numbering_subset_type
                                      connectivity_per_numbering_subset)
    {
        connectivity_type connectivity;
        connectivity.max_load_factor(Utilities::DefaultMaxLoadFactor());
        connectivity.reserve(Nprocessor_wise_node_bearer);

        decltype(auto) any_felt_space = felt_space_list.back();
        assert(!(!any_felt_space));
        const auto mpi_rank = any_felt_space->GetMpi().GetRank();

        auto is_on_local_processor = [mpi_rank](const NodeBearer::shared_ptr& node_bearer_ptr)
        {
            assert(!(!node_bearer_ptr));
            return node_bearer_ptr->GetProcessor() == mpi_rank;
        };

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            ComputeNodeBearerConnectivityInFEltSpace(
                *felt_space_ptr, is_on_local_processor, connectivity, connectivity_per_numbering_subset);
        }

        // Reorder properly elements and get rid of duplicates.
        CleanUpConnectivity(connectivity, do_keep_self_connection);

        if (connectivity_per_numbering_subset.has_value())
        {
            decltype(auto) container = connectivity_per_numbering_subset.value().get();

            for (auto& [numbering_subset_id, connectivity_for_ns] : container)
                CleanUpConnectivity(connectivity_for_ns, KeepSelfconnection::no);
        }


        assert("In both occasion this function is called, we are in measure to provide the exact size "
               "of the connectivity, thus avoiding possibly expensive rehashing calls."
               && connectivity.size() == Nprocessor_wise_node_bearer);

        return connectivity;
    }


    NodeBearer::vector_shared_ptr ComputeConnectedNodeBearerToKeepForDataIntegrity(
        const FEltSpace::vector_unique_ptr& felt_space_list,
        const Internal::NodeBearerNS::node_bearer_per_coords_index_list_type& node_bearer_list_to_keep,
        ::MoReFEM::Internal::FEltSpaceNS::optional_ref_connectivity_per_numbering_subset_type
            connectivity_per_numbering_subset)
    {
        connectivity_type connectivity;
        connectivity.max_load_factor(Utilities::DefaultMaxLoadFactor());
        connectivity.reserve(node_bearer_list_to_keep.size());

        auto node_bearer_filter = [&node_bearer_list_to_keep](const NodeBearer::shared_ptr& node_bearer_ptr)
        {
            assert(!(!node_bearer_ptr));
            const auto& node_bearer = *node_bearer_ptr;

            // NOLINTNEXTLINE(readability-use-anyofallof)
            for (const auto& [coords_index_list, node_bearer_to_keep_ptr] : node_bearer_list_to_keep)
            {
                assert(!(!node_bearer_to_keep_ptr));
                if (*node_bearer_to_keep_ptr == node_bearer)
                    return true;
            }

            return false;
        };

        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            ComputeNodeBearerConnectivityInFEltSpace(
                *felt_space_ptr, node_bearer_filter, connectivity, connectivity_per_numbering_subset);
        }

        assert(connectivity.size() == node_bearer_list_to_keep.size());

        NodeBearer::vector_shared_ptr ret;

        for (const auto& [node_bearer_ptr, connected_node_bearer_list] : connectivity)
        {
            ret.push_back(node_bearer_ptr);

            for (const auto& connected_node_bearer_ptr : connected_node_bearer_list)
            {
                assert(!(!connected_node_bearer_ptr));
                ret.push_back(connected_node_bearer_ptr);
            }
        }

        Utilities::EliminateDuplicate(ret,
                                      Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                      Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());

        return ret;
    }


    namespace // anonymous
    {


        template<RoleOnProcessor RoleOnProcessorT, class FunctorT>
        void IterateThroughLocalFEltSpace(
            const FEltSpace& felt_space,
            const FunctorT& node_bearer_filter,
            connectivity_type& connectivity,
            optional_ref_connectivity_per_numbering_subset_type connectivity_per_numbering_subset)

        {
            decltype(auto) felt_list_per_type = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessorT>();

            for ([[maybe_unused]] const auto& [ref_local_felt_space_ptr, local_felt_space_list_per_geom_index] :
                 felt_list_per_type)
            {
                const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

                decltype(auto) ref_felt_list = ref_local_felt_space.GetRefFEltList();

                for ([[maybe_unused]] const auto& [geom_elt_index, local_felt_space_ptr] :
                     local_felt_space_list_per_geom_index)
                {
                    assert(!(!local_felt_space_ptr));
                    const auto& local_felt_space = *local_felt_space_ptr;

                    const auto& node_bearers_in_current_local_felt_space = local_felt_space.GetNodeBearerList();

                    assert(local_felt_space.GetGeometricElt().GetIndex() == geom_elt_index);

                    for (const auto& node_bearer_ptr : node_bearers_in_current_local_felt_space)
                    {
                        if (!node_bearer_filter(node_bearer_ptr))
                            continue;

                        // Use of C++ 17 "structure binding".
                        // The following code works whether the \a node_bearer_ptr is already in the
                        // unordered_map or not.
                        [[maybe_unused]] auto [it, is_new_element] = connectivity.insert({ node_bearer_ptr, {} });

                        auto& second_member = it->second;

                        std::copy(node_bearers_in_current_local_felt_space.cbegin(),
                                  node_bearers_in_current_local_felt_space.cend(),
                                  std::back_inserter(second_member));

                        // Do connectivity per numbering subset
                        if (connectivity_per_numbering_subset.has_value())
                        {
                            decltype(auto) connectivity_per_numbering_subset_content =
                                connectivity_per_numbering_subset.value().get();

                            for (const auto& ref_felt_ptr : ref_felt_list)
                            {
                                assert(!(!ref_felt_ptr));
                                decltype(auto) numbering_subset =
                                    ref_felt_ptr->GetExtendedUnknown().GetNumberingSubset();

                                [[maybe_unused]] auto [it_conn_per_ns, unused2] =
                                    connectivity_per_numbering_subset_content.insert(
                                        { numbering_subset.GetUniqueId(), {} });

                                auto& connectivity_for_numbering_subset = it_conn_per_ns->second;

                                [[maybe_unused]] auto [it_for_ns, unused3] =
                                    connectivity_for_numbering_subset.insert({ node_bearer_ptr, {} });

                                auto& connected_node_bearer_list = it_for_ns->second;

                                std::copy(node_bearers_in_current_local_felt_space.cbegin(),
                                          node_bearers_in_current_local_felt_space.cend(),
                                          std::back_inserter(connected_node_bearer_list));
                            }
                        }
                    }
                }
            }
        }


        template<class FunctorT>
        void ComputeNodeBearerConnectivityInFEltSpace(
            const FEltSpace& felt_space,
            const FunctorT& node_bearer_filter,
            connectivity_type& connectivity,
            ::MoReFEM::Internal::FEltSpaceNS::optional_ref_connectivity_per_numbering_subset_type
                connectivity_per_numbering_subset)
        {
            // Iterate through all node_bearers and determine the list of connected node_bearers.
            // In this first approach, a same node_bearer might be listed twice in the list.
            {
                IterateThroughLocalFEltSpace<RoleOnProcessor::processor_wise>(
                    felt_space, node_bearer_filter, connectivity, connectivity_per_numbering_subset);

                IterateThroughLocalFEltSpace<RoleOnProcessor::ghost>(
                    felt_space, node_bearer_filter, connectivity, connectivity_per_numbering_subset);
            }
        }


        void CleanUpConnectivity(connectivity_type& connectivity, KeepSelfconnection do_keep_self_connection)
        {
            for (auto& [node_bearer, connected_node_bearer_list] : connectivity)
            {
                // There is in all likelihood duplicates in second member; remove them!
                Utilities::EliminateDuplicate(connected_node_bearer_list,
                                              Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());

                if (do_keep_self_connection == KeepSelfconnection::no)
                {
                    // Remove also self-connection (harmful for instance if fed to Parmetis library, and trivial to
                    // add back anyway if you need it somewhere).
                    auto it = std::ranges::find(connected_node_bearer_list, node_bearer);

                    if (it != connected_node_bearer_list.end())
                        connected_node_bearer_list.erase(it);
                }
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
