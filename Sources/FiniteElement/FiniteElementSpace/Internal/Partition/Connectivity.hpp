// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_CONNECTIVITY_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_CONNECTIVITY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <optional>
#include <unordered_map>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    //! Enum class to make the call to ComputeNodeConnectivity() more readable than with a mere boolean.
    enum class KeepSelfconnection { yes, no };


    /*!
     * \brief Convenient alias.
     *
     * \internal <b><tt>[internal]</tt></b> Unordered map use the address and not the NodeBearer object in its
     * hash table.
     * \endinternal
     */
    using connectivity_type = std::unordered_map<NodeBearer::shared_ptr, NodeBearer::vector_shared_ptr>;

    //! Container that stores connectivity information for each \a NumberingSubset.
    using connectivity_per_numbering_subset_type =
        std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, connectivity_type>;

    //! Convenient alias.
    using optional_ref_connectivity_per_numbering_subset_type =
        std::optional<std::reference_wrapper<connectivity_per_numbering_subset_type>>;

    /*!
     * \brief Compute the connectivity between \a NodeBearer.
     *
     * In MoReFEM, parallelism ensures that all nodes and dofs located on a same \a Interface are
     * managed by the same processor; that's why we need first to define a connectivity between
     * \a NodeBearer prior to the processor reduction.
     *
     * \param[in] felt_space_list List of all finite element spaces within the god of dof (current
     * function is called once per \a GodOfDof.
     * \param[in] Nprocessor_wise_node_bearer Number of processor-wise node bearers.
     * \param[in] do_keep_self_connection Whether we keep auto-association or not for each node bearer.
     * \param[in,out] connectivity_per_numbering_subset If defined, compute and store there
     * the connectivity for each \a NumberingSubset. There are some calls to this function when we don't care
     * for it - in this case `std::nullopt` tells we don't need the extra computation.
     *
     * \return For each node bearer, the list of connected node bearers.
     */
    connectivity_type
    ComputeNodeBearerConnectivity(const FEltSpace::vector_unique_ptr& felt_space_list,
                                  const std::size_t Nprocessor_wise_node_bearer,
                                  KeepSelfconnection do_keep_self_connection,
                                  ::MoReFEM::Internal::FEltSpaceNS::optional_ref_connectivity_per_numbering_subset_type
                                      connectivity_per_numbering_subset = std::nullopt);

    /*!
     * \brief Given a list of \a NodeBearer, determine the list of connected \a NodeBearer that should be kept to maintain integrity.
     *
     * \param[in] felt_space_list List of \a FEltSpace within a \a GodOfDof.
     * \param[in] node_bearer_list_to_keep List of \a NodeBearer that are for some reason tagged as needed to be kept (the
     * current use of this is for instance for the purposes of \a FromCoordsMatching interpolator).
     * \param[in,out] connectivity_per_numbering_subset If defined, compute and store there
     * the connectivity for each \a NumberingSubset. There are some calls to this function when we don't care
     * for it - in this case `std::nullopt` tells we don't need the extra computation.
     *
     * \return A list of \a NodeBearer that needs to be kept to preserve the integrity of the data structure.  By construct all the \a NodeBearer
     * from \a node_bearer_list_to_keep are there. At this point, no filtering concerning processor-wise or
     * ghost.
     */
    NodeBearer::vector_shared_ptr ComputeConnectedNodeBearerToKeepForDataIntegrity(
        const FEltSpace::vector_unique_ptr& felt_space_list,
        const Internal::NodeBearerNS::node_bearer_per_coords_index_list_type& node_bearer_list_to_keep,
        optional_ref_connectivity_per_numbering_subset_type connectivity_per_numbering_subset);


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_CONNECTIVITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
