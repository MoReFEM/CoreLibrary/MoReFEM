// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "FiniteElement/FiniteElementSpace/Internal/Partition/Impl/InterfaceSpecialization.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM::Internal::FEltSpaceNS::Impl
{


    InterfaceSpecialization<Vertex>::InterfaceListType
    InterfaceSpecialization<Vertex>::GetInterfaceList(const GeometricElt& geom_elt)
    {
        return geom_elt.GetVertexList();
    }


    InterfaceSpecialization<Vertex>::NodeListType
    InterfaceSpecialization<Vertex>::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                 [[maybe_unused]] const Vertex& vertex,
                                                 Eigen::Index local_index)
    {
        return { basic_ref_felt.GetLocalNodeOnVertexPtr(local_index) };
    }


    NodeBearer::shared_ptr InterfaceSpecialization<Vertex>::CreateNodeBearer(const Vertex::shared_ptr& vertex_ptr)
    {
        assert(!(!vertex_ptr));
        return std::make_shared<NodeBearer>(vertex_ptr);
    }


    Eigen::Index InterfaceSpecialization<Vertex>::Ninterface(const GeometricElt& geom_elt)
    {
        return static_cast<Eigen::Index>(geom_elt.Nvertex());
    }


    InterfaceSpecialization<OrientedEdge>::NodeListType
    InterfaceSpecialization<OrientedEdge>::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                       const OrientedEdge& edge,
                                                       Eigen::Index local_index)
    {
        return basic_ref_felt.GetLocalNodeOnEdgeList(local_index, edge.GetOrientation());
    }


    NodeBearer::shared_ptr
    InterfaceSpecialization<OrientedEdge>::CreateNodeBearer(const OrientedEdge::shared_ptr& edge_ptr)
    {
        assert(!(!edge_ptr));
        return std::make_shared<NodeBearer>(edge_ptr->GetUnorientedInterfacePtr());
    }


    InterfaceSpecialization<OrientedEdge>::InterfaceListType
    InterfaceSpecialization<OrientedEdge>::GetInterfaceList(const GeometricElt& geom_elt)
    {
        return geom_elt.GetOrientedEdgeList();
    }


    Eigen::Index InterfaceSpecialization<OrientedEdge>::Ninterface(const GeometricElt& geom_elt)
    {
        return static_cast<Eigen::Index>(geom_elt.Nedge());
    }


    InterfaceSpecialization<OrientedFace>::NodeListType
    InterfaceSpecialization<OrientedFace>::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                       const OrientedFace& face,
                                                       Eigen::Index local_index)
    {
        return basic_ref_felt.GetLocalNodeOnFaceList(local_index, face.GetOrientation());
    }


    NodeBearer::shared_ptr
    InterfaceSpecialization<OrientedFace>::CreateNodeBearer(const OrientedFace::shared_ptr& face_ptr)
    {
        assert(!(!face_ptr));
        return std::make_shared<NodeBearer>(face_ptr->GetUnorientedInterfacePtr());
    }


    InterfaceSpecialization<OrientedFace>::InterfaceListType
    InterfaceSpecialization<OrientedFace>::GetInterfaceList(const GeometricElt& geom_elt)
    {
        return geom_elt.GetOrientedFaceList();
    }


    Eigen::Index InterfaceSpecialization<OrientedFace>::Ninterface(const GeometricElt& geom_elt)
    {
        return static_cast<Eigen::Index>(geom_elt.Nface());
    }


    InterfaceSpecialization<Volume>::NodeListType
    InterfaceSpecialization<Volume>::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                                 [[maybe_unused]] const Volume& volume,
                                                 [[maybe_unused]] Eigen::Index local_index)
    {
        return basic_ref_felt.GetLocalNodeOnVolumeList();
    }


    InterfaceSpecialization<Volume>::InterfaceListType
    InterfaceSpecialization<Volume>::GetInterfaceList(const GeometricElt& geom_elt)
    {
        return { geom_elt.GetVolumePtr() };
    }


    NodeBearer::shared_ptr InterfaceSpecialization<Volume>::CreateNodeBearer(const Volume::shared_ptr& volume_ptr)
    {
        assert(!(!volume_ptr));
        return std::make_shared<NodeBearer>(volume_ptr);
    }


    Eigen::Index InterfaceSpecialization<Volume>::Ninterface(const GeometricElt& geom_elt)
    {
        return (geom_elt.GetDimension() >= ::MoReFEM::GeometryNS::dimension_type{ 3 } ? 1 : 0);
    }


} // namespace MoReFEM::Internal::FEltSpaceNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
