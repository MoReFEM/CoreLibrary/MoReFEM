// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_IMPL_INTERFACESPECIALIZATION_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_IMPL_INTERFACESPECIALIZATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Instances/Vertex.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS::Impl
{


    /*!
     * \brief Helper struct that acts as a dispatcher depending on the type of the interface considered.
     *
     * \copydetails doxygen_hide_oriented_interface_tparam
     */
    template<class OrientedInterfaceT>
    struct InterfaceSpecialization;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================

    template<>
    struct InterfaceSpecialization<Vertex>
    {
        /*!
         * There is only one LocalNode for Vertex, so it isn't stored as a vector in BasicRefFElt.
         * However, for genericity of AddInterfaceNodeList() function I need it as  vector; there
         * is therefore creation and copy of a vector with exactly one element inside.
         *
         */
        using NodeListType = Advanced::LocalNode::vector_const_shared_ptr;

        using InterfaceListType = const Vertex::vector_shared_ptr&;

        static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);

        static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                        const Vertex& vertex,
                                        Eigen::Index local_index);

        static NodeBearer::shared_ptr CreateNodeBearer(const Vertex::shared_ptr& vertex_ptr);

        static Eigen::Index Ninterface(const GeometricElt& geom_elt);
    };


    template<>
    struct InterfaceSpecialization<OrientedEdge>
    {
        using NodeListType = const Advanced::LocalNode::vector_const_shared_ptr&;

        using InterfaceListType = const OrientedEdge::vector_shared_ptr&;

        static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                        const OrientedEdge& edge,
                                        Eigen::Index local_index);

        static NodeBearer::shared_ptr CreateNodeBearer(const OrientedEdge::shared_ptr& edge_ptr);

        static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);

        static Eigen::Index Ninterface(const GeometricElt& geom_elt);
    };


    template<>
    struct InterfaceSpecialization<OrientedFace>
    {
        using NodeListType = const Advanced::LocalNode::vector_const_shared_ptr&;

        using InterfaceListType = const OrientedFace::vector_shared_ptr&;


        static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                        const OrientedFace& face,
                                        Eigen::Index local_index);

        static NodeBearer::shared_ptr CreateNodeBearer(const OrientedFace::shared_ptr& face_ptr);

        static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);


        static Eigen::Index Ninterface(const GeometricElt& geom_elt);
    };


    template<>
    struct InterfaceSpecialization<Volume>
    {

        using NodeListType = const Advanced::LocalNode::vector_const_shared_ptr&;


        /*!
         * There is at most one interface for Volume, so it isn't stored as a vector in GeometricElt.
         * However, for genericity of AddInterfaceNodeList() function I need it as  vector; there
         * is therefore creation and copy of a vector with 0 or 1 element inside.
         *
         */
        using InterfaceListType = Volume::vector_shared_ptr;


        static NodeListType GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                                        const Volume& volume,
                                        Eigen::Index local_index);


        static InterfaceListType GetInterfaceList(const GeometricElt& geom_elt);

        static NodeBearer::shared_ptr CreateNodeBearer(const Volume::shared_ptr& volume_ptr);


        static Eigen::Index Ninterface(const GeometricElt& geom_elt);
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::FEltSpaceNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_IMPL_INTERFACESPECIALIZATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
