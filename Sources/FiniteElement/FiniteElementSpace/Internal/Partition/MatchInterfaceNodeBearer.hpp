// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATCHINTERFACENODEBEARER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATCHINTERFACENODEBEARER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <map>
#include <unordered_map>

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/StrongType.hpp"

#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class DirichletBoundaryCondition; }
namespace MoReFEM::Internal::FEltSpaceNS { class AssignGeomEltToProcessor; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Facility which matches each \a NodeBearer by its associated \a Interface.
     *
     * This is not something we need once the initialization is done, but it is rather helpful during the
     * initialization phase.
     */
    class MatchInterfaceNodeBearer
    {
      public:
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship to this class which calls the ClearBoundaryConditionNodebearers().
        friend Internal::FEltSpaceNS::AssignGeomEltToProcessor;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

      public:
        /*!
         * \brief Constructor.
         *
         */
        explicit MatchInterfaceNodeBearer();

        //! Destructor.
        ~MatchInterfaceNodeBearer() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MatchInterfaceNodeBearer(const MatchInterfaceNodeBearer& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MatchInterfaceNodeBearer(MatchInterfaceNodeBearer&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        MatchInterfaceNodeBearer& operator=(const MatchInterfaceNodeBearer& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MatchInterfaceNodeBearer& operator=(MatchInterfaceNodeBearer&& rhs) = default;


        /*!
         * \brief Add all the \a NodeBearer  of a given finite element.
         *
         * \param[in] ref_felt_space Reference finite element space.
         * \param[in,out] local_felt_space Local finite element space to which
         * \a FElt created are added.
         * \param[in] node_bearer_for_current_god_of_dof  All the \a NodeBearer in \a GodofDof. The call to this
         * method is expected to take place before reduction - so we are speaking of program-wise ones here.
         */
        void AddNodeBearerList(const ::MoReFEM::Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                               NodeBearer::vector_shared_ptr& node_bearer_for_current_god_of_dof,
                               LocalFEltSpace& local_felt_space);

        /*!
         * \brief Add all the \a NodeBearer  of a given finite element.
         *
         * \param[in] ref_felt_space Reference finite element space.
         * \param[in,out] local_felt_space Local finite element space to which
         * \a FElt created are added.
         */
        void ComputeNodeList(const ::MoReFEM::Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                             LocalFEltSpace& local_felt_space);


        /*!
         * \brief Establish the list of nodes affected by boundary condition.
         *
         * The nodes should already have been created before the call to this method.
         *
         * \param[in] mesh Mesh upon which the node are created.
         * \param[in] boundary_condition Boundary condition for which additional node bearers might have to be
         * computed.
         * \param[out] bc_data Data related to \a BoundaryCondition.
         *
         * \return The list of node bearers.
         */
        NodeBearer::vector_shared_ptr ComputeNodeBearerListOnBoundary(
            const Mesh& mesh,
            const DirichletBoundaryCondition& boundary_condition,
            std::unordered_map<GeometricElt::shared_ptr, NodeBearer::vector_shared_ptr>& bc_data) const;

        /*!
         * \brief Reduce this helper facility to only processor-wise and ghost data.
         *
         * \param[in] processor_wise_node_bearer_list The list of processor-wise \a NodeBearer for the
         * considered \a GodOfDof. \param[in] ghost_node_bearer_list The list of ghost\a NodeBearer for the
         * considered \a GodOfDof.
         */
        void Reduce(const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_list);

        //! Set the list of \a NodeBearer on boundary condition.
        //! \param[in] bc_data The \a BoundaryCondition data computed by \a ComputeNodeBearerListOnBoundary() method.
        void
        SetBoundaryConditionData(std::unordered_map<GeometricElt::shared_ptr, NodeBearer::vector_shared_ptr>&& bc_data);

        /*!
         * \brief Set the internal data related to \a CoordsMatching interpolator.
         *
         * \copydoc doxygen_hide_mpi_param
         *
         * \param[in] value The data to store into the class. Current rank is added as ghost by this method.
         * \param[in] numbering_subset \a NumberingSubset for which we will declare extra processor thay ghost
         * \a NodeBearer -s. It is typically here the source \a NumberingSubset in the `FromCoordsMatching` interpolator.
         */
        void SetFromCoordsMatchingNodeBearers(const ::MoReFEM::Wrappers::Mpi& mpi,
                                              const NumberingSubset& numbering_subset,
                                              Internal::NodeBearerNS::node_bearer_per_coords_index_list_type&& value);

        /*!
         * \brief Accessor to the data needed to keep on current processor some \a NodeBearer involved in boundary conditions.
         *
         * \return List of \a NodeBearer sort per \a GeometricElt.
         */
        // clang-format off
        const std::unordered_map
        <
            GeometricElt::shared_ptr,
            NodeBearer::vector_shared_ptr
        >&
        GetBoundaryConditionsData() const noexcept;
        // clang-format on

        /*!
         * \brief Accessor to the data needed to keep on current processor some \a NodeBearer involved in \a FromCoordsMatching operator.
         *
         * * \return List of \a NodeBearer sort per  list of \a Coords.
         */
        const ::MoReFEM::Internal::NodeBearerNS::node_bearer_per_coords_index_list_type&
        GetCoordsMatchingData() const noexcept;


      private:
        /*!
         * \class doxygen_hide_create_node_list_helper_param_node_for_current_finite_element
         *
         * \param[in,out] node_for_current_finite_element Key is the index of the local node considered, value
         * a pointer to the associated \a Node.
         */


        /*!
         * \brief Add to \a node_for_current_finite_element all the \a ref_felt nodes of the \a GeometricElt
         * that are on a \a OrientedInterfaceT.
         *
         * \copydoc doxygen_hide_oriented_interface_tparam
         * \param[in] geom_elt Geometric element for which node are added.
         * \param[in,out] node_bearer_list_for_current_god_of_dof List of \a NodeBearer for the current \a
         * GodOfDof. \param[in,out] node_bearer_list_for_current_local_felt_space List of \a NodeBearer for the
         * current \a LocalFEltSpace. \param[in,out] interface_node_bearer_list Key is the index of the \a
         * OrientedInterfaceT, value is a pointer to the associated \a NodeBearer.
         *
         * All output parameters are also input ones because \a Node and \a NodeBearer might already exist from:
         * - Another \a GeometricElt that shares the same \a OrientedInterfaceT.
         * - Another \a Unknown that acted upon the same \a LocalFEltSpace (only for \a NodeBearer).
         *
         */
        template<class OrientedInterfaceT>
        void CreateNodeBearerList(const GeometricElt& geom_elt,
                                  NodeBearer::vector_shared_ptr& node_bearer_list_for_current_god_of_dof,
                                  NodeBearer::vector_shared_ptr& node_bearer_list_for_current_local_felt_space,
                                  std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type,
                                                     NodeBearer::shared_ptr>& interface_node_bearer_list);


        /*!
         * \brief Add list of  \a Node for current \a GeometricElt.
         *
         * \copydoc doxygen_hide_oriented_interface_tparam
         * \param[in] geom_elt \a GeometricElt for which node are added.
         * \param[in] ref_felt Reference finite element.
         * \param[in] interface_node_bearer_list Index is \a interface index, value is the associated \a
         * NodeBbearer. \param[in,out] node_for_current_finite_element The \a Node list for current \a
         * LocalFEltSpace.
         *
         */
        template<class OrientedInterfaceT>
        void AddInterfaceNodeList(const GeometricElt& geom_elt,
                                  const ::MoReFEM::Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                  const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type,
                                                           NodeBearer::shared_ptr>& interface_node_bearer_list,
                                  std::map<LocalNodeNS::index_type, Node::shared_ptr>& node_for_current_finite_element);


        /*!
         * \brief Helper method \a AddInterfaceNodeList(), which creates a new \a Node on a \a NodeBearer.
         *
         * \param[in] local_node_on_interface_list List of \a LocalNode to consider on the interface currently
         * considered in \a AddInterfaceNodeList().
         * \param[in] extended_unknown Couple (\a Unknown, \a NumberingSubset) for which new node is to be
         * created. \param[in] Ncomponent Number of dofs to create in association with the new node.
         * \param[in,out] node_bearer \a NodeBearer upon which new node is created. It is modified as well as
         * new node is actually stored there.
         * \copydoc doxygen_hide_create_node_list_helper_param_node_for_current_finite_element
         */
        template<class LocalDofOnInterfaceT>
        void CreateNodeList(const LocalDofOnInterfaceT& local_node_on_interface_list,
                            const ExtendedUnknown& extended_unknown,
                            ::MoReFEM::GeometryNS::dimension_type Ncomponent,
                            NodeBearer& node_bearer,
                            std::map<LocalNodeNS::index_type, Node::shared_ptr>& node_for_current_finite_element);

      private:
        //! List of nodes on the vertex. Key is program-wise index of the \a Vertex.
        const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
        GetVertexNodeBearerList() const noexcept;

        //! List of nodes on the edges. Key is program-wise index of the \a Edge.
        const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
        GetEdgeNodeBearerList() const noexcept;

        //! List of nodes on the faces. Key is program-wise index of the \a Face.
        const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
        GetFaceNodeBearerList() const noexcept;

        //! List of nodes on the volume. Not truly necessary; just there for symmetry purposes.
        const std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>&
        GetVolumeNodeBearerList() const noexcept;

      private:
        //! List of nodes on the vertex. Key is program-wise index of the \a Vertex.
        std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>
            vertex_node_bearer_list_;

        //! List of nodes on the edges. Key is program-wise index of the \a Edge.
        std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>
            edge_node_bearer_list_;

        //! List of nodes on the faces. Key is program-wise index of the \a Face.
        std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>
            face_node_bearer_list_;

        //! List of nodes on the volume. Not truly necessary; just there for symmetry purposes.
        std::unordered_map<::MoReFEM::InterfaceNS::program_wise_index_type, NodeBearer::shared_ptr>
            volume_node_bearer_list_;

        /*!
         * \brief List of all \a NodeBearers involved in boundary conditions sort per their associated \a GeometricElt.
         */
        // clang-format off
        std::unordered_map
        <
            GeometricElt::shared_ptr,
            NodeBearer::vector_shared_ptr
        > boundary_conditions_data_;
        // clang-format on

        /*!
         * \brief List of all \a NodeBearers involved in boundary conditions sort per their associated \a Coords list (provided by an \a Interface).
         */
        ::MoReFEM::Internal::NodeBearerNS::node_bearer_per_coords_index_list_type coords_matching_data_;
    };


    /*!
     * \brief Create  all the \a Nodes by iterating through all \a LocalFEltSpace of \a FEltSpace.
     *
     * \param[in] god_of_dof \a GodOfDof for which \a Nodes will be created.
     * \param[in,out] match_interface_node_bearer Helper object which stores data useful for the initialisation
     * phase.
     *
     * \internal This method is expected to be called AFTER the reduction process occurred. After the call, all
     * relevant \a Node are created and they are properly sorted as processor-wise or ghost.
     *
     * \internal Don't call this method directly: it is automatically called in CreateNodesAndDofs().
     *
     */
    void CreateNodes(const GodOfDof& god_of_dof,
                     Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer);


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATCHINTERFACENODEBEARER_DOT_HPP_
// *** MoReFEM end header guards *** < //
