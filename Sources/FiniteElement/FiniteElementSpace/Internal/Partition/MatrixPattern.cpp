// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Utilities/Containers/PointerComparison.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatrixPattern.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    MatrixPattern::MatrixPattern(NumberingSubset::const_shared_ptr row_numbering_subset_ptr,
                                 NumberingSubset::const_shared_ptr column_numbering_subset_ptr,
                                 const connectivity_type& connectivity,
                                 const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                 const NdofHolder& Ndof_holder)
    : row_numbering_subset_(std::move(row_numbering_subset_ptr)),
      column_numbering_subset_(std::move(column_numbering_subset_ptr))
    {
        // Now fill the number of diagonal and non-diagonal terms per row (consider only processor-wise dofs).

        const auto& row_numbering_subset = GetRowNumberingSubset();
        const auto& column_numbering_subset = GetColumnNumberingSubset();

        std::vector<std::vector<PetscInt>> content_for_each_local_row;
        content_for_each_local_row.resize(Ndof_holder.NprocessorWiseDof(row_numbering_subset));

        std::size_t buf_dof_index = 0;

        assert(processor_wise_node_bearer_list.size() == connectivity.size());
        assert(std::ranges::is_sorted(processor_wise_node_bearer_list,

                                      Utilities::PointerComparison::Less<NodeBearer::shared_ptr>()));

#ifndef NDEBUG
        {
            NodeBearer::vector_shared_ptr keys;
            keys.reserve(connectivity.size());

            for (const auto& item : connectivity)
                keys.push_back(item.first);

            std::ranges::sort(keys, Utilities::PointerComparison::Less<NodeBearer::shared_ptr>());

            assert(keys == processor_wise_node_bearer_list);
        }
#endif // NDEBUG

        for (const auto& node_bearer_ptr : processor_wise_node_bearer_list)
        {
            assert(!(!node_bearer_ptr));

            auto it = connectivity.find(node_bearer_ptr);

            assert(it != connectivity.cend()
                   && "This method is called after reduction to processor-wise of the "
                      "node bearer list.");

            // Compute the list of dofs that are connected to the current dof. As node_bearer is itself in
            // connected list, dofs on the same node_bearer are counted as well.
            std::vector<PetscInt> program_wise_dof_indexes_in_local_row;
            {
                for (const auto& connected_node_bearer_ptr : it->second)
                {
                    assert(!(!connected_node_bearer_ptr));
                    const auto& connected_node_bearer = *connected_node_bearer_ptr;

                    const auto& node_storage = connected_node_bearer.GetNodeList();

                    for (const auto& node_ptr : node_storage)
                    {
                        assert(!(!node_ptr));
                        const auto& node = *node_ptr;

                        if (!node.IsInNumberingSubset(column_numbering_subset))
                            continue;

                        decltype(auto) dof_list = node.GetDofList();

                        for (const auto& dof_ptr : dof_list)
                        {
                            assert(!(!dof_ptr));
                            const auto index = dof_ptr->GetProgramWiseIndex(column_numbering_subset).Get();
                            program_wise_dof_indexes_in_local_row.push_back(static_cast<PetscInt>(index));
                        }
                    }
                }
            }


            // Sort in increasing order, to comply with CSR format.
            std::ranges::sort(program_wise_dof_indexes_in_local_row);


            // Now copy the content in the rows.
            {
                const auto& node_list = node_bearer_ptr->GetNodeList();

                for (const auto& node_ptr : node_list)
                {
                    assert(!(!node_ptr));

                    const auto& node = *node_ptr;

                    if (!node.IsInNumberingSubset(row_numbering_subset))
                        continue;

                    const std::size_t Ndof = node_ptr->Ndof();

                    for (std::size_t i = 0; i < Ndof; ++i)
                    {
                        auto row_index = buf_dof_index++;
                        assert(row_index < content_for_each_local_row.size());
                        assert(content_for_each_local_row[row_index].empty()
                               && "A local row should be filled only once!");

                        content_for_each_local_row[row_index] = program_wise_dof_indexes_in_local_row;
                    }
                }
            }
        }

        matrix_pattern_ = std::make_unique<::MoReFEM::Wrappers::Petsc::MatrixPattern>(content_for_each_local_row);
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    MatrixPattern::MatrixPattern(NumberingSubset::const_shared_ptr row_numbering_subset,
                                 NumberingSubset::const_shared_ptr column_numbering_subset,
                                 std::vector<PetscInt>&& iCSR,
                                 std::vector<PetscInt>&& jCSR)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : row_numbering_subset_(std::move(row_numbering_subset)),
      column_numbering_subset_(std::move(column_numbering_subset)),
      matrix_pattern_(
          std::make_unique<const ::MoReFEM::Wrappers::Petsc::MatrixPattern>(std::move(iCSR), std::move(jCSR)))
    { }


    bool operator==(const MatrixPattern& lhs, const MatrixPattern& rhs)
    {
        if (lhs.GetRowNumberingSubset() != rhs.GetRowNumberingSubset())
            return false;

        if (lhs.GetColumnNumberingSubset() != rhs.GetColumnNumberingSubset())
            return false;

        return (lhs.GetPattern() == rhs.GetPattern());
    }


    MatrixPattern::vector_const_unique_ptr
    ComputeMatrixPattern ::Perform(const FEltSpace::vector_unique_ptr& felt_space_list,
                                   const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                   const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                                   const NdofHolder& Ndof_holder)
    {
        auto connectivity = ComputeNodeBearerConnectivity(
            felt_space_list, processor_wise_node_bearer_list.size(), KeepSelfconnection::yes);

        MatrixPattern::vector_const_unique_ptr ret;

        assert(std::ranges::none_of(numbering_subset_list,

                                    Utilities::IsNullptr<NumberingSubset::const_shared_ptr>));

#ifndef NDEBUG
        std::vector<std::pair<::MoReFEM::NumberingSubsetNS::unique_id, ::MoReFEM::NumberingSubsetNS::unique_id>>
            already_seen;
#endif // NDEBUG

        for (const auto& row_numbering_subset_ptr : numbering_subset_list)
        {
            for (const auto& col_numbering_subset_ptr : numbering_subset_list)
            {
#ifndef NDEBUG
                auto new_pair =
                    std::make_pair(row_numbering_subset_ptr->GetUniqueId(), col_numbering_subset_ptr->GetUniqueId());

                auto it = std::ranges::find(already_seen, new_pair);
                assert(it == already_seen.cend() && "One given numbering subset should be only once in the list!");
                already_seen.push_back(new_pair);
#endif // NDEBUG

                auto&& matrix_pattern_ptr = std::make_unique<MatrixPattern>(row_numbering_subset_ptr,
                                                                            col_numbering_subset_ptr,
                                                                            connectivity,
                                                                            processor_wise_node_bearer_list,
                                                                            Ndof_holder);

                ret.push_back(std::move(matrix_pattern_ptr));
            }
        }

        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
