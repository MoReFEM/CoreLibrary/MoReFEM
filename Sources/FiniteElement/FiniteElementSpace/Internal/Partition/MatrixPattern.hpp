// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATRIXPATTERN_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATRIXPATTERN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Intermediate object used to create and store the pattern of a GlobalMatrix.
     *
     * This is mostly a facilitator of ThirdParty::Wrappers::Petsc::MatrixPattern that can create such an object
     * with ease due to the effectiveness of numbering subsets (which are not available at ThirdParty library
     * level).
     */
    class MatrixPattern
    {

      public:
        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const MatrixPattern>;

        //! Alias to vector of unique pointers.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Computes the number of diagonal and off-diagonal terms for each row of a dof matrix.
         *
         *
         * Beware: The indexes of the row are processor-wise, whereas the values for each vector are
         * program-wise! (not my fault: it is the way Petsc is designed...)
         *
         * \param[in] connectivity For each node bearer, the list of connected node bearers. Computed by
         * ComputeNodeBearerConnectivity().
         * \param[in] processor_wise_node_bearer_list Node bearer list on the local processor. It is somewhat
         * redundant with keys of \a connectivity; it is there only because it has the advantage of being
         * already sort. \param[in] Ndof_holder Object which knows number of dofs in each numbering subset.
         * \param[in] row_numbering_subset Numbering subset over which all dofs considered in the rows of the
         * matrix are defined.
         * \param[in] column_numbering_subset Numbering subset over which all dofs considered in the rows of the
         * matrix are defined.
         */
        explicit MatrixPattern(NumberingSubset::const_shared_ptr row_numbering_subset,
                               NumberingSubset::const_shared_ptr column_numbering_subset,
                               const connectivity_type& connectivity,
                               const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                               const NdofHolder& Ndof_holder);

        /*!
         * \brief Computes the number of diagonal and off-diagonal terms for each row of a dof matrix.
         *
         *
         * Beware: The indexes of the row are processor-wise, whereas the values for each vector are
         * program-wise! (not my fault: it is the way Petsc is designed...)
         *
         * \param[in] row_numbering_subset Numbering subset over which all dofs considered in the rows of the
         * matrix are defined.
         * \param[in] column_numbering_subset Numbering subset over which all dofs considered in the rows of the
         * matrix are defined.
         * \param[in] iCSR i-part of the CSR pattern.
         * \param[in] jCSR j-part of the CSR pattern.
         */
        explicit MatrixPattern(NumberingSubset::const_shared_ptr row_numbering_subset,
                               NumberingSubset::const_shared_ptr column_numbering_subset,
                               std::vector<PetscInt>&& iCSR,
                               std::vector<PetscInt>&& jCSR);

        //! Destructor.
        ~MatrixPattern() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MatrixPattern(const MatrixPattern& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MatrixPattern(MatrixPattern&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MatrixPattern& operator=(const MatrixPattern& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MatrixPattern& operator=(MatrixPattern&& rhs) = delete;

        ///@}

        //! Get the numbering subset used for rows.
        const NumberingSubset& GetRowNumberingSubset() const;

        //! Get the numbering subset used for columns.
        const NumberingSubset& GetColumnNumberingSubset() const;

        //! Get the pattern to feed to Petsc.
        const ::MoReFEM::Wrappers::Petsc::MatrixPattern& GetPattern() const;


      private:
        //! Numbering subset over which all dofs considered in the rows of the matrix are defined.
        const NumberingSubset::const_shared_ptr row_numbering_subset_ = nullptr;

        /*!
         * \brief Numbering subset over which all dofs considered in the rows of the matrix are defined.
         *
         * Might be the same as \a row_numbering_subset_.
         */
        const NumberingSubset::const_shared_ptr column_numbering_subset_ = nullptr;

        //! Objects that stores effectively the pattern.
        ::MoReFEM::Wrappers::Petsc::MatrixPattern::const_unique_ptr matrix_pattern_ = nullptr;
    };


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * To make two \a MatrixPattern equal:
     * - Both \a NumberingSubset object should match.
     * - The information about the pattern (iCSR format) should match.
     */
    bool operator==(const MatrixPattern& lhs, const MatrixPattern& rhs);


    /*!
     * \brief Computes the matrix pattern.
     *
     * \internal <b><tt>[internal]</tt></b> This is actually a struct to ease the syntax of
     * friendship declaration in NodeBearer class.
     * \endinternal
     *
     */
    struct ComputeMatrixPattern
    {
      public:
        /*!
         * \brief Compute the pattern(s) that have to be covered within a \a GodOfDof.
         *
         * \param[in] felt_space_list The list of finite elements spaces considered.
         * \param[in,out] processor_wise_node_bearer_list The list of node bearers of processor-wise \a
         * NodeBearer. This  argument is tagged as an output parameter because the \a NodeBearer objects in the
         * list gets new information in the process. \param[in] Ndof_holder Object that knows the number of
         * dofs per numbering subset. \param[in] numbering_subset_list List of \a NumberingSubset to consider.
         *
         *
         * \return Pattern of the global matrix.
         */
        static MatrixPattern::vector_const_unique_ptr
        Perform(const FEltSpace::vector_unique_ptr& felt_space_list,
                const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                const NdofHolder& Ndof_holder);
    };


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatrixPattern.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATRIXPATTERN_DOT_HPP_
// *** MoReFEM end header guards *** < //
