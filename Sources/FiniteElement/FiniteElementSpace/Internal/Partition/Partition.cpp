// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <optional>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

#include "FiniteElement/FiniteElementSpace/Internal/Partition/Partition.hpp"

#include "Utilities/LinearAlgebra/SparseMatrix/CSRPattern.hpp"

#include "ThirdParty/Wrappers/Parmetis/Parmetis.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    namespace // anonymous
    {


        /*!
         * \brief This function provides a dumb partition to be fed to Parmetis library to generate a semsible
         * one.
         *
         * In this dumb partition, node bearers are split equally between the processors, with the last one
         * getting the rest of the euclidean division. So for instance if there are 10 nodes bearers and 3
         * processors:
         * - Processor 1 gets 0, 1, 2
         * - Processor 2 gets 3, 4, 5
         * - Processor 3 gets 6, 7, 8, 9
         *
         * \param[in] Nnode_bearer Number of node bearer program-wise.
         * \param[in] Nprocessor Number of processors involved in mpi parallelism.
         */
        std::vector<std::size_t> EuclideanPartition(std::size_t Nnode_bearer, rank_type Nprocessor);


        /*!
         * \brief  Attribute each node to a processor.
         *
         * This is done crudely to pave the way to Parmetis call: an euclidean partition is actually used.
         * That is not an entirely stupid guess: nodes are sort in a first place according to geometric
         * element. However that is not enough: a node shared by several geometric elements is represented only
         * in the first. That is there that Parmetis will provide insight to reduce bandwidth.
         *
         * Contrary to actual partitioning, all data are kept: it is really a transitory step required by
         * Parmetis!
         *
         * \param[in] euclidean_partition Partition given by EuclideanPartition().
         * \param[in] node_bearer_list List of node bearers before reduction to processor-wise. Each node bearer
         * is assigned a processor value through SetProcessor().
         * \param[in] Nprocessor Number of processors involved in mpi parallelism.
         */
        void CrudePartitioning(const std::vector<std::size_t>& euclidean_partition,
                               const NodeBearer::vector_shared_ptr& node_bearer_list,
                               rank_type Nprocessor);


        /*!
         * \brief Call Parmetis to provide a partition that limits as much as possible the number of ghosts.
         *
         * \param[in] euclidean_partition Partition given by EuclideanPartition().
         * \param[in] node_bearer_csr_pattern CSR pattern of the node bearers (NOT of the dofs!)
         * obtained from CrudePartitioning().
         * \copydetails doxygen_hide_mpi_param
         * \param[in,out] node_bearer_list In input, list of all node bearers program-wise. In output, list
         * of the same length with the node bearers sort differently: each one has been attributed to a
         * processor and all node bearers on a same processor are grouped together.
         *
         */
        void ParmetisPartitioning(const std::vector<std::size_t>& euclidean_partition,
                                  const ::MoReFEM::Wrappers::Mpi& mpi,
                                  Utilities::CSRPattern<parmetis_int>&& csr_node_bearer_pattern_for_parmetis,
                                  NodeBearer::vector_shared_ptr& node_bearer_list);


        /*!
         * \brief Prepare CSR format for the node bearers.
         *
         * This is used to feed Parmetis, so dofs aren't yet taken into account.
         *
         * \param[in] connectivity Connectivity of each node bearer.
         * \param[in] node_bearer_list Node bearer list before reduction to processor-wise.
         *
         * \return CSR pattern.
         */
        Utilities::CSRPattern<parmetis_int>
        PrepareNodeBearerCSRFormat(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                   const connectivity_type& connectivity);


    } // namespace


    void PreparePartition(const ::MoReFEM::Wrappers::Mpi& mpi,
                          const FEltSpace::vector_unique_ptr& felt_space_list,
                          NodeBearer::vector_shared_ptr& node_bearer_list)
    {
        const auto mpi_rank = mpi.GetRank();

        const auto Nprocessor = mpi.Nprocessor();
        assert(Nprocessor.Get() > 1UL);

        auto euclidean_partition = EuclideanPartition(node_bearer_list.size(), Nprocessor);

        assert(euclidean_partition.size() == Nprocessor.Get());


        // We separate the node_bearers following a simple euclidean division, without even looking the
        // connectivity. Data aren't reduced there of course!
        CrudePartitioning(euclidean_partition, node_bearer_list, Nprocessor);

        // Call Parmetis to partition the node_bearers more properly.
        auto&& connectivity =
            ComputeNodeBearerConnectivity(felt_space_list, euclidean_partition[mpi_rank.Get()], KeepSelfconnection::no);

        assert(!connectivity.empty()
               && "If it is, it's likely your finite element spaces (especially their domain) "
                  "are ill-defined. It might also be you're using too much processors for a too tiny model.");

        assert("Number of node_bearers must match what was determined by Euclidean partition"
               && connectivity.size() == euclidean_partition[mpi_rank.Get()]);

        auto csr_node_bearer_pattern_for_parmetis = PrepareNodeBearerCSRFormat(node_bearer_list, connectivity);

        ParmetisPartitioning(
            euclidean_partition, mpi, std::move(csr_node_bearer_pattern_for_parmetis), node_bearer_list);
    }


    void ReduceNodeBearerList(const rank_type mpi_rank, NodeBearer::vector_shared_ptr& node_bearer_list)
    {
        NodeBearer::vector_shared_ptr processor_wise_node_bearer_list(node_bearer_list.size());

        // Create a new list that keep tabs only on processor-wise nodes.
        auto [_, logical_end] = std::ranges::copy_if(node_bearer_list,
                                                     processor_wise_node_bearer_list.begin(),
                                                     [mpi_rank](const NodeBearer::shared_ptr& node_bearer_ptr)
                                                     {
                                                         return node_bearer_ptr->GetProcessor() == mpi_rank;
                                                     });

        processor_wise_node_bearer_list.erase(logical_end, processor_wise_node_bearer_list.end());
        node_bearer_list.swap(processor_wise_node_bearer_list);
    }


    void BroadcastGhostNodeBearerIndexList(const ::MoReFEM::Wrappers::Mpi& mpi,
                                           const NumberingSubset& numbering_subset,
                                           const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                           const NodeBearer::vector_shared_ptr& ghost_node_bearer_list)

    {
        const auto Nprocessor = mpi.Nprocessor();

        auto extract_index = [](const auto& node_bearer_ptr)
        {
            assert(!(!node_bearer_ptr));
            return node_bearer_ptr->GetProgramWiseIndex();
        };


        std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type> processor_wise_node_bearer_index_list(
            processor_wise_node_bearer_list.size());

        std::ranges::transform(processor_wise_node_bearer_list,

                               processor_wise_node_bearer_index_list.begin(),
                               extract_index);

        std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type> ghost_node_bearer_index_list(
            ghost_node_bearer_list.size());

        std::ranges::transform(ghost_node_bearer_list,

                               ghost_node_bearer_index_list.begin(),
                               extract_index);

        assert(std::ranges::is_sorted(processor_wise_node_bearer_index_list));

        const auto mpi_rank = mpi.GetRank();

        // For each rank, broadcast the ghost node bearer list. On the receiving end, check whether the ghost
        // belongs to the current processor and if so store the information that the sender is ghosting the
        // current \a NodeBearer.
        for (auto sender = rank_type{ 0UL }; sender < Nprocessor; ++sender)
        {
            std::size_t Nghost{ ghost_node_bearer_index_list.size() };

            mpi.Broadcast(Nghost, sender);

            std::vector<::MoReFEM::NodeBearerNS::program_wise_index_type> sender_ghost_node_bearer_index_list;

            if (sender == mpi_rank)
            {
                assert(Nghost == ghost_node_bearer_index_list.size());
                sender_ghost_node_bearer_index_list = ghost_node_bearer_index_list;
            } else
                sender_ghost_node_bearer_index_list.resize(Nghost);

            mpi.Broadcast(sender_ghost_node_bearer_index_list, sender);

            assert(std::ranges::is_sorted(sender_ghost_node_bearer_index_list));

            if (sender != mpi_rank)
            {
                const auto begin = sender_ghost_node_bearer_index_list.cbegin();
                const auto end = sender_ghost_node_bearer_index_list.cend();

                // Beware: pointer itself is constant, but underlying `NodeBearer` object is modified!
                for (const auto& processor_wise_node_bearer_ptr : processor_wise_node_bearer_list)
                {
                    assert(!(!processor_wise_node_bearer_ptr));
                    auto& processor_wise_node_bearer = *processor_wise_node_bearer_ptr;

                    const auto program_wise_index = processor_wise_node_bearer.GetProgramWiseIndex();

                    if (std::binary_search(begin, end, program_wise_index))
                        processor_wise_node_bearer.SetGhost(numbering_subset, sender);
                }
            }
        }
    }


    namespace // anonymous
    {


        std::vector<std::size_t> EuclideanPartition(const std::size_t Nnode_bearer, const rank_type Nprocessor)
        {
            const std::size_t estimated_Ndof_per_proc = Nnode_bearer / Nprocessor.Get();
            std::vector<std::size_t> Nnode_bearer_per_proc(Nprocessor.Get(), estimated_Ndof_per_proc);

            // Add to last processor all dof lost due to euclidean division.
            Nnode_bearer_per_proc.back() += Nnode_bearer % Nprocessor.Get();

            return Nnode_bearer_per_proc;
        }


        void CrudePartitioning(const std::vector<std::size_t>& euclidean_partition,
                               const NodeBearer::vector_shared_ptr& node_bearer_list,
                               const rank_type Nprocessor)
        {
            std::size_t global_node_bearer_index = 0;

            for (auto processor = rank_type{ 0UL }; processor < Nprocessor; ++processor)
            {
                const std::size_t Nnode_bearer_on_processor = euclidean_partition[processor.Get()];

                for (std::size_t i = 0; i < Nnode_bearer_on_processor; ++i)
                {
                    assert(global_node_bearer_index < node_bearer_list.size());

                    // Beware: pointer is constant, not underlying object.
                    const auto& node_bearer_ptr = node_bearer_list[global_node_bearer_index++];
                    node_bearer_ptr->SetProcessor(processor);
                }
            }

            assert(global_node_bearer_index == node_bearer_list.size());
        }


        void ParmetisPartitioning(const std::vector<std::size_t>& euclidean_partition,
                                  const ::MoReFEM::Wrappers::Mpi& mpi,
                                  Utilities::CSRPattern<parmetis_int>&& csr_node_bearer_pattern_for_parmetis,
                                  NodeBearer::vector_shared_ptr& program_wise_node_bearer_list)
        {
            // Call to Parmetis.
            auto&& parmetis_numbering =
                ::MoReFEM::Wrappers::Parmetis::CreateNewPartitioning(euclidean_partition,
                                                                     csr_node_bearer_pattern_for_parmetis.iCSR(),
                                                                     csr_node_bearer_pattern_for_parmetis.jCSR(),
                                                                     mpi);

            // Report for each node_bearer the processor in charge.
            const std::size_t Nnode_bearer = program_wise_node_bearer_list.size();
            assert(parmetis_numbering.size() == Nnode_bearer);

            for (std::size_t i = 0UL; i < Nnode_bearer; ++i)
            {
                auto& node_bearer_ptr = program_wise_node_bearer_list[i];
                assert(!(!node_bearer_ptr));
                node_bearer_ptr->SetProcessor(parmetis_numbering[i]);
            }

            // Now order the list of node_bearers so that all node_bearers on a given processor are contiguous.
            std::ranges::stable_sort(
                program_wise_node_bearer_list,

                [](const NodeBearer::shared_ptr& node_bearer1, const NodeBearer::shared_ptr& node_bearer2)
                {
                    assert(!(!node_bearer1));
                    assert(!(!node_bearer2));
                    return node_bearer1->GetProcessor() < node_bearer2->GetProcessor();
                });

            // Determine the definite index for each node_bearer.
            for (std::size_t i = 0UL; i < Nnode_bearer; ++i)
            {
                const auto& node_bearer_ptr = program_wise_node_bearer_list[i];
                node_bearer_ptr->SetProgramWiseIndex(::MoReFEM::NodeBearerNS::program_wise_index_type{ i });
            }
        }


        Utilities::CSRPattern<parmetis_int>
        PrepareNodeBearerCSRFormat(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                   const connectivity_type& connectivity)
        {
            std::vector<parmetis_int> iCSR{ 0 };
            std::vector<parmetis_int> jCSR;

            parmetis_int current_iCSR = 0;

#ifndef NDEBUG
            auto Nnon_zero = 0UL;

            for (const auto& [node_bearer, connected_node_bearer_list] : connectivity)
                Nnon_zero += connected_node_bearer_list.size();
#endif // NDEBUG


            // We want here to fill row by row; this is why we can't iterate directly on the unordered_map
            // connectivity.

            for (const auto& node_bearer_on_local_proc_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_on_local_proc_ptr));

                auto it = connectivity.find(node_bearer_on_local_proc_ptr);

                // At this stage, node_bearer_list is much more filled than processor-wise data (it still
                // includes data attributed to other processors). Ignore the non processor-wise data!
                if (it == connectivity.cend())
                    continue;

                const auto& connected_node_bearer_list = it->second;

                const std::size_t Nconnected_node_bearer = connected_node_bearer_list.size();

                current_iCSR += static_cast<parmetis_int>(Nconnected_node_bearer);
                iCSR.push_back(current_iCSR);

                std::vector<parmetis_int> jCSR_contribution;

                jCSR_contribution.reserve(Nconnected_node_bearer);

                for (const auto& connected_node_bearer_ptr : connected_node_bearer_list)
                {
                    assert(!(!connected_node_bearer_ptr));
                    jCSR_contribution.push_back(
                        static_cast<parmetis_int>(connected_node_bearer_ptr->GetProgramWiseIndex().Get()));
                }

                assert(jCSR_contribution.size() == Nconnected_node_bearer);
                std::ranges::sort(jCSR_contribution);


                for (auto new_jCSR : jCSR_contribution)
                    jCSR.push_back(new_jCSR);
            }

            assert(iCSR.size() == connectivity.size() + 1);
            assert(jCSR.size() == Nnon_zero);

            return Utilities::CSRPattern<parmetis_int>(std::move(iCSR), std::move(jCSR));
        }


    } // namespace


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
