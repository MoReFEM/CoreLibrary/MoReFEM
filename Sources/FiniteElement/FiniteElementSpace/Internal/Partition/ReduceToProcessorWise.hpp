// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_REDUCETOPROCESSORWISE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_REDUCETOPROCESSORWISE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
// #include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceStorage.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/Connectivity.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::Internal::FEltSpaceNS { class AssignGeomEltToProcessor; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatchInterfaceNodeBearer; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Reduce to processor-wise most data (finite elements in each finite element space, geometric
     * mesh region) and computes the list og ghost node bearers.
     *
     * \attention the list of processor-wise node bearers is not computed here because it already was
     * when the pattern of the global matrix was computed.
     *
     * \internal <b><tt>[internal]</tt></b> The only reason there is a class that encompass the two static
     * methods is privilege access: I wanted to forbid the public call of
     * FEltStorage::GetNonCstLocalFEltSpacePerRefLocalFEltSpace(). However
     * such a data was required; so FEltStorage holds a friendship to present class to circumvent that.
     * \endinternal
     */
    struct ReduceToProcessorWise
    {


        /*!
         * \brief Static method that performs the actual work.
         *
         * The enclosing class is there just to provide a simple object to befriend!
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \param[in] assign_geom_elt_to_processor The object which was used to determine which rank is in
         * charge of each \a GeometricElt.
         * \param[in,out] felt_space_list The list of finite elt spaces to
         * reduce to processor-wise data.
         * \param[in,out] mesh In input the mesh as read initially. In output,
         * the reduced mesh that includes only GeometricElts on the local processor.
         * \param[in] processor_wise_node_bearer_list The list of processor-wise node bearers, computed previously.
         * \param[in,out] ghost_node_bearer_list The list of ghost node bearers, i.e. the ones that were in
         * processor-wise finite elements but not in processor-wise node bearer list. It can also be an input
         * parameter: if several finite element spaces are involved the same list us kept for all of them.
         * \param[in,out] connectivity_per_numbering_subset \a NodeBearer connectivity for each
         * \a NumberingSubset.
         * \param[in,out] match_interface_node_bearer Helper object which stores data useful for the
         * initialisation phase.
         */
        static void Perform(
            const ::MoReFEM::Wrappers::Mpi& mpi,
            const Internal::FEltSpaceNS::AssignGeomEltToProcessor& assign_geom_elt_to_processor,
            const FEltSpace::vector_unique_ptr& felt_space_list,
            const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
            ::MoReFEM::Internal::FEltSpaceNS::connectivity_per_numbering_subset_type& connectivity_per_numbering_subset,
            Internal::FEltSpaceNS::MatchInterfaceNodeBearer& match_interface_node_bearer,
            NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
            Mesh& mesh);
    };


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_REDUCETOPROCESSORWISE_DOT_HPP_
// *** MoReFEM end header guards *** < //
