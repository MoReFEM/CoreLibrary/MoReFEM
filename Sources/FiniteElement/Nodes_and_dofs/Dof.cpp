// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <sstream>
#include <tuple>
#include <type_traits>

#include "FiniteElement/Nodes_and_dofs/Dof.hpp" // IWYU pragma: associated

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp" // IWYU pragma: keep

#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/Parameter/Advanced/Concept.hpp" // IWYU pragma: export

#include "FiniteElement/Nodes_and_dofs/Node.hpp"       // IWYU pragma: keep
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace // anonymous
    {

#ifndef NDEBUG
        // StorageT is a std::vector<std::pair<::MoReFEM::NumberingSubsetNS::unique_id, INDEXT>
        // where INDEXT is either processor wise or program wise index.
        template<class StorageT>
        void AssertNewNumberingSubset(const StorageT& storage, const NumberingSubset& numbering_subset);
#endif // NDEBUG


        //! Return the const iterator matching \a numbering_subset in \a storage.
        template<class StorageT>
        typename StorageT::const_iterator Iterator(const StorageT& storage, const NumberingSubset& numbering_subset);


        //! Returns the index stored in \a storage that matches the input \a numbering_subset.
        template<class StorageT>
        typename std::tuple_element<1, typename StorageT::value_type>::type
        FindInStorage(const StorageT& storage, const NumberingSubset& numbering_subset, const Dof& dof);


    } // namespace


    Dof::Dof(const std::shared_ptr<const Node>& node_ptr) : node_(node_ptr)
    { }


    Dof::~Dof() = default;


    void Dof::SetProgramWiseIndex(const NumberingSubset& numbering_subset, DofNS::program_wise_index index)
    {
#ifndef NDEBUG
        AssertNewNumberingSubset(program_wise_index_per_numbering_subset_, numbering_subset);
#endif // NDEBUG

        program_wise_index_per_numbering_subset_.emplace_back(numbering_subset.GetUniqueId(), index);
    }


    void Dof::SetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset,
                                           DofNS::processor_wise_or_ghost_index index)
    {
#ifndef NDEBUG
        AssertNewNumberingSubset(processor_wise_or_ghost_index_per_numbering_subset_, numbering_subset);
#endif // NDEBUG

        processor_wise_or_ghost_index_per_numbering_subset_.emplace_back(numbering_subset.GetUniqueId(), index);
    }


    void Dof::SetInternalProcessorWiseOrGhostIndex(Internal::DofNS::internal_processor_wise_or_ghost_index index)
    {
        assert(internal_processor_wise_or_ghost_index_
                   == NumericNS::UninitializedIndex<Internal::DofNS::internal_processor_wise_or_ghost_index>()
               && "Should be allocated only once!");
        internal_processor_wise_or_ghost_index_ = index;
    }


    DofNS::processor_wise_or_ghost_index
    Dof::GetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset) const
    {
        return FindInStorage(GetProcessorWiseOrGhostIndexPerNumberingSubset(), numbering_subset, *this);
    }


    DofNS::program_wise_index Dof::GetProgramWiseIndex(const NumberingSubset& numbering_subset) const
    {
        return FindInStorage(GetProgramWiseIndexPerNumberingSubset(), numbering_subset, *this);
    }


    bool Dof::IsInNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto node_ptr = GetNodeFromWeakPtr();
        assert(!(!node_ptr));
        return node_ptr->IsInNumberingSubset(numbering_subset);
    }


    namespace // anonymous
    {


#ifndef NDEBUG
        template<class StorageTypeT>
        void AssertNewNumberingSubset(const StorageTypeT& storage, const NumberingSubset& numbering_subset)
        {
            const auto id = numbering_subset.GetUniqueId();

            assert(std::find_if(storage.cbegin(),
                                storage.cend(),
                                [id](const auto& pair)
                                {
                                    return pair.first == id;
                                }

                                )
                       == storage.cend()
                   && "A given numbering subset should appear only once!");
        }
#endif // NDEBUG


        template<class StorageT>
        typename StorageT::const_iterator Iterator(const StorageT& storage, const NumberingSubset& numbering_subset)
        {
            assert(!storage.empty()
                   && "Should have been addressed in calling site (FindInStorage anonymous function).");
            const auto id = numbering_subset.GetUniqueId();

            return std::find_if(storage.cbegin(),
                                storage.cend(),
                                [id](const auto& pair)
                                {
                                    return pair.first == id;
                                });
        }


        template<class StorageT>
        typename std::tuple_element<1, typename StorageT::value_type>::type
        FindInStorage(const StorageT& storage, const NumberingSubset& numbering_subset, [[maybe_unused]] const Dof& dof)
        {
#ifndef NDEBUG
            // NOLINTBEGIN(bugprone-casting-through-void)
            if (storage.empty())
            {
                std::ostringstream oconv;

                oconv << "Internal error in the library - please issue a ticket!" << '\n';

                const auto node_ptr = dof.GetNodeFromWeakPtr();

                const auto node_bearer_ptr = node_ptr->GetNodeBearerFromWeakPtr();

                // Ugly and not necessarily future proof method to obtain current rank
                // - but for debug purpose it is good enough.
                PetscInt rank = 0;
                MPI_Comm_rank(MPI_COMM_WORLD, &rank);

                oconv << '[' << rank << "] Dof which internal processor-wise or ghost index is "
                      << dof.GetInternalProcessorWiseOrGhostIndex() << " inside NodeBearer which program-wise index is "
                      << node_bearer_ptr->GetProgramWiseIndex() << " and that is manager primarily by rank "
                      << node_bearer_ptr->GetProcessor() << " was queried for numbering subset "
                      << numbering_subset.GetUniqueId() << " but the appropriate data "
                      << " was not found." << '\n';

                if (std::is_same<std::decay_t<StorageT>, Dof::program_wise_index_per_numbering_subset_type>())
                {
                    oconv << "As a program-wise index per numbering subset was requested, it's likely "
                             "FillGhostDofProgramWiseIndexes() anonymous function in GodOfDof__Init() didn't do its "
                             "job properly.";
                }

                throw Exception(oconv.str());
            }
            // NOLINTEND(bugprone-casting-through-void)
#endif // NDEBUG
            auto it = Iterator(storage, numbering_subset);
            assert(it != storage.cend());
            return it->second;
        }


    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
