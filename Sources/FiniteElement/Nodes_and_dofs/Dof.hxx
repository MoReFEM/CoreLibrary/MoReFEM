// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_DOF_DOT_HXX_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_DOF_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/Nodes_and_dofs/Dof.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Numeric/Numeric.hpp"

#include "FiniteElement/Nodes_and_dofs/Internal/DofIndexesTypes.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Dof; }
namespace MoReFEM { class Node; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline auto Dof::GetInternalProcessorWiseOrGhostIndex() const noexcept
        -> Internal::DofNS::internal_processor_wise_or_ghost_index
    {
        assert(internal_processor_wise_or_ghost_index_
               != NumericNS::UninitializedIndex<Internal::DofNS::internal_processor_wise_or_ghost_index>());
        return internal_processor_wise_or_ghost_index_;
    }


    inline bool operator<(const Dof& lhs, const Dof& rhs)
    {
        return (lhs.GetInternalProcessorWiseOrGhostIndex() < rhs.GetInternalProcessorWiseOrGhostIndex());
    }


    inline bool operator==(const Dof& lhs, const Dof& rhs)
    {
        return (lhs.GetInternalProcessorWiseOrGhostIndex() == rhs.GetInternalProcessorWiseOrGhostIndex());
    }


    inline auto Dof::GetProcessorWiseOrGhostIndexPerNumberingSubset() const
        -> const processor_wise_or_ghost_index_per_numbering_subset_type&
    {
        return processor_wise_or_ghost_index_per_numbering_subset_;
    }


    inline auto Dof::GetProgramWiseIndexPerNumberingSubset() const
        -> const program_wise_index_per_numbering_subset_type&
    {
        return program_wise_index_per_numbering_subset_;
    }


    inline std::shared_ptr<const Node> Dof::GetNodeFromWeakPtr() const
    {
        assert(!node_.expired());
        return node_.lock();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_DOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
