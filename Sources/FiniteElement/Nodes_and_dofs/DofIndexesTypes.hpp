// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_DOFINDEXESTYPES_DOT_HPP_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_DOFINDEXESTYPES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/StrongType.hpp"

// IWYU pragma: begin_exports
#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::DofNS
{


    /*!
     * \brief Strong type to represent a program-wise \a Dof index
     *
     * \attention \a MoReFEM::vector_program_wise_index_type, \a MoReFEM::col_program_wise_index_type
     * and \a col_program_wise_index_type are almost the same as this strong type - they just add an additional
     * semantic information in the use within global linear algebra. Don't be astonished if there are at some point
     * conversion between these types and current one.
     */
    // clang-format off
    using program_wise_index =
    StrongType
    <
        std::size_t,
        struct ProgramWiseIndexTag,
        StrongTypeNS::Comparable,
        StrongTypeNS::Hashable,
        StrongTypeNS::DefaultConstructible,
        StrongTypeNS::Printable,
        StrongTypeNS::AsMpiDatatype,
        StrongTypeNS::Addable,
        StrongTypeNS::Incrementable
    >;
    // clang-format on


    /*!
     * \brief Strong type to represent a processor-wise \a Dof index
     *
     * \attention \a MoReFEM::vector_processor_wise_index_type, \a MoReFEM::col_processor_wise_index_type
     * and \a col_processor_wise_index_type are almost the same as this strong type - they just add an additional
     * semantic information in the use within global linear algebra. Don't be astonished if there are at some point
     * conversion between these types and current one.
     */
    // clang-format off
    using processor_wise_or_ghost_index =
    StrongType
    <
        std::size_t,
        struct ProcessorWiseOrGhostIndexTag,
        StrongTypeNS::Comparable,
        StrongTypeNS::Hashable,
        StrongTypeNS::DefaultConstructible,
        StrongTypeNS::Printable,
        StrongTypeNS::AsMpiDatatype,
        StrongTypeNS::Addable,
        StrongTypeNS::Incrementable
    >;
    // clang-format on


    /*!
     * \brief Provide conversion from a processor-wise or ghost index into a processor-wise  index used in a \a GlobalVector.
     *
     * Both yield the same `std::size_t` underlying value, but strong type around them is not the same.
     *
     *  \param[in] index Dof processor-wise or ghost index to be transformed into a vector index.
     *
     * \return vector_processor_wise_index_type that may be used for instance with `AccessVectorContent` facility.
     */
    vector_processor_wise_index_type ToVectorIndex(processor_wise_or_ghost_index index);


} // namespace MoReFEM::DofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

#include "FiniteElement/Nodes_and_dofs/DofIndexesTypes.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_DOFINDEXESTYPES_DOT_HPP_
// *** MoReFEM end header guards *** < //
