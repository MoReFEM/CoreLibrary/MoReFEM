// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <map>
#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/Nodes_and_dofs/Exceptions/Dof.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"


namespace // anonymous
{


    std::string InvalidNumberOfUnknownMsg(std::size_t Nin_file, std::size_t Nexpected);

    std::string DuplicatedUnknownInInputFileMsg(const std::string& duplicated_unknown);

    std::string InconsistentUnknownListMsg(const std::map<std::string, std::size_t>& input_file_unknown_list,
                                           const std::vector<std::string>& tuple_unknown_list);


} // namespace


namespace MoReFEM::ExceptionNS::DofNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    InvalidNumberOfUnknown::~InvalidNumberOfUnknown() = default;


    InvalidNumberOfUnknown::InvalidNumberOfUnknown(std::size_t Nin_file,
                                                   std::size_t Nexpected,
                                                   const std::source_location location)
    : Exception(InvalidNumberOfUnknownMsg(Nin_file, Nexpected), location)
    { }


    DuplicatedUnknownInInputFile::~DuplicatedUnknownInInputFile() = default;


    DuplicatedUnknownInInputFile::DuplicatedUnknownInInputFile(const std::string& duplicated_unknown,
                                                               const std::source_location location)
    : Exception(DuplicatedUnknownInInputFileMsg(duplicated_unknown), location)
    { }


    InconsistentUnknownList::~InconsistentUnknownList() = default;


    InconsistentUnknownList::InconsistentUnknownList(const std::map<std::string, std::size_t>& input_file_unknown_list,
                                                     const std::vector<std::string>& tuple_unknown_list,
                                                     const std::source_location location)
    : Exception(InconsistentUnknownListMsg(input_file_unknown_list, tuple_unknown_list), location)
    { }


} // namespace MoReFEM::ExceptionNS::DofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string InvalidNumberOfUnknownMsg(std::size_t Nin_file, std::size_t Nexpected)
    {
        std::ostringstream oconv;
        oconv << Nexpected << " unknowns were expected in the input data list, but " << Nin_file
              << " were actually read in the file.";

        return oconv.str();
    }


    std::string DuplicatedUnknownInInputFileMsg(const std::string& duplicated_unknown)
    {
        std::ostringstream oconv;
        oconv << "Unknown '" << duplicated_unknown << "' was present at least twice in the input data file.";
        return oconv.str();
    }


    std::string InconsistentUnknownListMsg(const std::map<std::string, std::size_t>& input_file_unknown_list,
                                           const std::vector<std::string>& tuple_unknown_list)
    {
        std::ostringstream oconv;
        oconv << "The unknown found int the input file are not the ones expected by the problem: \n";
        MoReFEM::Utilities::PrintContainer<>::Do(tuple_unknown_list, oconv);
        oconv << " were expected but those that were in the input file were: \n";
        MoReFEM::Utilities::PrintContainer<MoReFEM::Utilities::PrintPolicyNS::Key>::Do(input_file_unknown_list, oconv);
        return oconv.str();
    }


} // namespace
