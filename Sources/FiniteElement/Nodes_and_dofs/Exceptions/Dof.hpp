// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_EXCEPTIONS_DOF_DOT_HPP_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_EXCEPTIONS_DOF_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <map>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::ExceptionNS::DofNS
{


    //! Generic class
    struct Exception : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! When the number of unknowns read in the input file doesn't match the expected one.
    struct InvalidNumberOfUnknown : public Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] Nin_file Number of unknowns read in the input data file.
         * \param[in] Nexpected Expected number of unknowns (cdetermined by the size of the tuple.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidNumberOfUnknown(std::size_t Nin_file,
                                        std::size_t Nexpected,
                                        const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        InvalidNumberOfUnknown(const InvalidNumberOfUnknown& rhs) = default;

        //! Destructor
        virtual ~InvalidNumberOfUnknown() override;
    };


    //! When the same unknown is present at lest twice in the list in the input data file.
    struct DuplicatedUnknownInInputFile final : public Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] duplicated_unknown Unknown that is present twice.
         * \copydoc doxygen_hide_source_location
         */
        explicit DuplicatedUnknownInInputFile(const std::string& duplicated_unknown,
                                              const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~DuplicatedUnknownInInputFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        DuplicatedUnknownInInputFile(const DuplicatedUnknownInInputFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        DuplicatedUnknownInInputFile(DuplicatedUnknownInInputFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DuplicatedUnknownInInputFile& operator=(const DuplicatedUnknownInInputFile& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        DuplicatedUnknownInInputFile& operator=(DuplicatedUnknownInInputFile&& rhs) = default;
    };


    //! When the unknowns in input file don't match the ones expected in the tuple.
    struct InconsistentUnknownList final : public Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] input_file_unknown_list Unknowns read in the input file. Dismiss the value part:
         * map is given there only because it is the container including the data when the exception was found.
         * \param[in] tuple_unknown_list Unknowns expected in the tuple for the problem.
         * \copydoc doxygen_hide_source_location
         */
        explicit InconsistentUnknownList(const std::map<std::string, std::size_t>& input_file_unknown_list,
                                         const std::vector<std::string>& tuple_unknown_list,
                                         const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentUnknownList() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentUnknownList(const InconsistentUnknownList& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentUnknownList(InconsistentUnknownList&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentUnknownList& operator=(const InconsistentUnknownList& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentUnknownList& operator=(InconsistentUnknownList&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::DofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_EXCEPTIONS_DOF_DOT_HPP_
// *** MoReFEM end header guards *** < //
