// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <functional>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"

#include "Utilities/Containers/UnorderedMap.hpp" // IWYU pragma: keep

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/Interface.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::NodeBearerNS
{


    node_bearer_per_coords_index_list_type
    GenerateNodeBearerPerCoordsList(const NodeBearer::vector_shared_ptr& node_bearer_list)
    {
        node_bearer_per_coords_index_list_type ret;

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));

            decltype(auto) interface = node_bearer_ptr->GetInterface();

            decltype(auto) coords_list =
                interface.ComputeCoordsIndexList<::MoReFEM::CoordsNS::index_enum::from_mesh_file>();

            Internal::InterfaceNS::OrderCoordsList(coords_list, std::less<>());

            [[maybe_unused]] auto [it, is_inserted] = ret.insert({ coords_list, node_bearer_ptr });
            assert(is_inserted);
        }

        return ret;
    }


} // namespace MoReFEM::Internal::NodeBearerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
