// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_GENERATENODEBEARERPERCOORDSLIST_DOT_HPP_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_GENERATENODEBEARERPERCOORDSLIST_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "Geometry/Coords/StrongType.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::NodeBearerNS
{


    // clang-format off
    //! Convenient alias.
    using node_bearer_per_coords_index_list_type = std::unordered_map
    <
        std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>,
        NodeBearer::shared_ptr,
        Utilities::ContainerHash
    >;
    // clang-format on


    /*!
     * \brief iterates over a list of \a NodeBearer and sort them by the \a Coords list related to its underlying \a Interface.
     *
     * \internal This is in Internal namespace for a good reason: it is only required as an helper in some initialization stuff, such as for \a CoordsMatching
     * operator.
     *
     * \param[in] node_bearer_list The \a NodeBearer list that will populate the return value.
     *
     * \return Key is the list of \a Coords indexes that delimits the \a Interface, value is the \a NodeBearer. The chosen index is the one read from
     * the mesh file - this choice stems from the use case of \a CoordsMatching.
     */
    node_bearer_per_coords_index_list_type
    GenerateNodeBearerPerCoordsList(const NodeBearer::vector_shared_ptr& node_bearer_list);


} // namespace MoReFEM::Internal::NodeBearerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_GENERATENODEBEARERPERCOORDSLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
