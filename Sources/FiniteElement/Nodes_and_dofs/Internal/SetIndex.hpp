// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_SETINDEX_DOT_HPP_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_SETINDEX_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "SetIndex.hpp"


#include "FiniteElement/Nodes_and_dofs/DofIndexesTypes.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/DofIndexesTypes.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Dof; }
namespace MoReFEM { class NumberingSubset; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::DofNS
{


    /*!
     * \brief This struct provides a way to allow some anonymous functions to set \a Dof indexes.
     *
     * The alternative would be to make the relevant methods  public in \a Dof, which would have been misleading; now
     * the access is still possible but the call sequence makes it obvious it is not intended to be used
     * by a standard user of the library.
     */
    struct SetIndex
    {

        /*!
         * \brief Set program-wise index for \a dof.
         *
         * \param[in] numbering_subset \a NumberingSubset for which index is set.
         * \param[in] index Index to be set.
         * \param[in,out] dof The \a Dof which program-wise index will be set.
         */
        static void
        ProgramWiseIndex(const NumberingSubset& numbering_subset, ::MoReFEM::DofNS::program_wise_index index, Dof& dof);


        /*!
         * \brief Set processor-wise or ghost index for \a dof.
         *
         * \param[in] numbering_subset \a NumberingSubset for which index is set.
         * \param[in] index Index to be set.
         * \param[in,out] dof The \a Dof which processor-wise or ghost index will be set.
         */
        static void ProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset,
                                              ::MoReFEM::DofNS::processor_wise_or_ghost_index index,
                                              Dof& dof);


        /*!
         * \brief Set internal processor-wise or ghost index for \a dof (not dependent upon a \a NumberingSubset).
         *
         * \param[in] index Index to be set.
         * \param[in,out] dof The \a Dof which internal processor-wise or ghost index will be set.
         */
        static void
        InternalProcessorWiseOrGhostIndex(::MoReFEM::Internal::DofNS::internal_processor_wise_or_ghost_index index,
                                          Dof& dof);
    };


} // namespace MoReFEM::Internal::DofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_SETINDEX_DOT_HPP_
// *** MoReFEM end header guards *** < //
