// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Core/NumberingSubset/NumberingSubset.hpp" // IWYU pragma: keep

#include "Geometry/StrongType.hpp" // IWYU pragma: export

#include "FiniteElement/Nodes_and_dofs/Dof.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Unknown; }
namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM { class NodeBearer; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief A Node is an ensemble of Dofs located at the same place and addressing the same unknown and shape function
     * label.
     *
     * \internal A node doesn't store directly an \a ExtendedUnknown: if the \a Unknown and the \a ShapeFunctionLabel
     * are obvious, a same \a Node might be registered in several \a FEltSpace for different \a NumberingSubset.
     * \endinternal
     */
    class Node final : public std::enable_shared_from_this<Node>
    {
      public:
        //! Shared pointer.
        using shared_ptr = std::shared_ptr<Node>;

        //! Vector of shared pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Friendship to \a NodeBearer (the only class allowed to create \a Node object).
        friend class NodeBearer;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \attention I had to make it public as std::make_shared doesn't propagate friendship and weak_ptr construction
         * requires to build it directly as a smart pointer (rather than converting after the fact a raw one) but for
         * all intents and purposes this constructor should be viewed as private: creating \a Node objects is something
         * only \a NodeBearer class is allowed to do.
         *
         * \param[in] extended_unknown Unknown addressed by all the dofs in the Node
         * (e.g. 'displacement', 'pressure'). It is here an extended unknown because when it is created it is
         * within a given numbering subset, however it's important to remember a given Node might be related to many
         * numbering subsets in the end (some may be added through \a RegisterNumberingSubset()).
         * \param[in] node_bearer_ptr Pointer to the node bearer onto which the \a Node is created.
         */
        explicit Node(const std::shared_ptr<const NodeBearer>& node_bearer_ptr,
                      const ExtendedUnknown& extended_unknown);

        //! Destructor.
        ~Node() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Node(const Node& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Node(Node&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Node& operator=(const Node& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Node& operator=(Node&& rhs) = delete;


        ///@}

        /*!
         * \brief This method must be called to fulfill construction of the object.
         *
         * \param[in] Ndof Number of dofs to create.
         *
         * \internal The \a Dof are built there as they need weak pointer to current object - which the constructor
         * can't give directly.
         */
        void Init(GeometryNS::dimension_type Ndof);

      public:
        //! Get the unknown.
        const Unknown& GetUnknown() const noexcept;

        //! Get the shape function label.
        const std::string& GetShapeFunctionLabel() const noexcept;

        //! Get the list of dofs.
        const Dof::vector_shared_ptr& GetDofList() const noexcept;

        //! Returns the \a i -th dof of the list.
        //! \param[in] i Position of the \a Dof in the vector.
        const Dof& GetDof(std::size_t i) const;

        //! Returns the \a i -th dof of the list as a smart pointer.
        //! \param[in] i Position of the \a Dof in the vector.
        const Dof::shared_ptr& GetDofPtr(std::size_t i) const;

        //! Returns the number of dofs.
        std::size_t Ndof() const noexcept;

        /*!
         * \brief Register a numbering subset for the node if it does not exist yet; do nothing otherwise.
         *
         * \param[in] numbering_subset \a NumberingSubset to be registered.
         */
        void RegisterNumberingSubset(NumberingSubset::const_shared_ptr numbering_subset);

        //! Returns whether the node belongs to the \a numbering_subset.
        //! \param[in] numbering_subset \a NumberingSubset used as filter.
        bool IsInNumberingSubset(const NumberingSubset& numbering_subset) const;

        //! Return the list of numbering subset.
        const NumberingSubset::vector_const_shared_ptr& GetNumberingSubsetList() const noexcept;

        /*! \brief Return a pointer to the \a NodeBearer to which the current \a Node belongs to.
         *
         * \internal <b><tt>[internal]</tt></b> No reference on purpose here: node_bearer_ is stored as a weak_ptr not
         * to introduce circular dependency.
         * \endinternal
         *
         * \return Shared pointer to the enclosing \a NodeBearer.
         */
        std::shared_ptr<const NodeBearer> GetNodeBearerFromWeakPtr() const;

      private:
        //! Unknown.
        const Unknown& unknown_;

        //! Shape function.
        const std::string& shape_function_label_;

        //! List of numbering subsets to which the node belongs.
        NumberingSubset::vector_const_shared_ptr numbering_subset_list_;

        //! List of dofs.
        Dof::vector_shared_ptr dof_list_;

        //! Weak pointer to NodeBearer.
        std::weak_ptr<const NodeBearer> node_bearer_;

#ifndef NDEBUG
        //! This variable is initialized to true once \a Init() method has beenc called.
        bool init_called_ = false;
#endif // NDEBUG
    };


    //! \copydoc doxygen_hide_operator_less
    //! Criteria are unknown first and shape_function_label in case of same unknown.
    bool operator<(const Node& lhs, const Node& rhs);

    //! \copydoc doxygen_hide_operator_equal
    //! Criteria is both same unknown and same shape_function_label.
    bool operator==(const Node& lhs, const Node& rhs);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/Nodes_and_dofs/Node.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODE_DOT_HPP_
// *** MoReFEM end header guards *** < //
