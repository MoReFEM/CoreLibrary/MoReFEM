// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/Nodes_and_dofs/Node.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NodeBearer; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const Unknown& Node::GetUnknown() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return unknown_;
    }


    inline const Dof::vector_shared_ptr& Node::GetDofList() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return dof_list_;
    }


    inline const Dof::shared_ptr& Node::GetDofPtr(std::size_t i) const
    {
        assert(init_called_ && "Init() should be called!");
        const auto& dof_list = GetDofList();
        assert(i < dof_list.size());
        assert(!(!dof_list[i]));
        return dof_list[i];
    }


    inline const Dof& Node::GetDof(std::size_t i) const
    {
        assert(init_called_ && "Init() should be called!");
        return *GetDofPtr(i);
    }


    inline const std::string& Node::GetShapeFunctionLabel() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return shape_function_label_;
    }


    inline std::size_t Node::Ndof() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return dof_list_.size();
    }


    inline const NumberingSubset::vector_const_shared_ptr& Node::GetNumberingSubsetList() const noexcept
    {
        assert(init_called_ && "Init() should be called!");
        return numbering_subset_list_;
    }


    inline std::shared_ptr<const NodeBearer> Node::GetNodeBearerFromWeakPtr() const
    {
        assert(init_called_ && "Init() should be called!");
        assert(!node_bearer_.expired());
        return node_bearer_.lock();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODE_DOT_HXX_
// *** MoReFEM end header guards *** < //
