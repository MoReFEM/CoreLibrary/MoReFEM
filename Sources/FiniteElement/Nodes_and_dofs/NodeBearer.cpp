// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <set>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>
// IWYU pragma: no_include <__tree>

#include "Core/NumberingSubset/NumberingSubset.hpp" // for NumberingSubset

#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/Unknown/Unknown.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    NodeBearer::NodeBearer(Interface::shared_ptr interface) : interface_(std::move(interface))
    {
        assert(!(!interface_));
    }


    Node::vector_shared_ptr
    NodeBearer ::GetNodeList(const Unknown& unknown,
                             [[maybe_unused]] const std::string& shape_function_label) const // #1146 temporary!
    {
        const auto& node_list = GetNodeList();

        Node::vector_shared_ptr ret;

        std::ranges::copy_if(node_list,

                             std::back_inserter(ret),
                             [&unknown /*, &shape_function_label*/](const auto& node_ptr)
                             {
                                 assert(!(!node_ptr));
                                 return node_ptr->GetUnknown() == unknown;
                                 // #1146 Temporary deactivated.
                                 // && node_ptr->GetShapeFunctionLabel() == shape_function_label;
                             });

        return ret;
    }


    Node::shared_ptr NodeBearer::AddNode(const ExtendedUnknown& extended_unknown,
                                         const ::MoReFEM::GeometryNS::dimension_type Ndof)
    {
        assert(Ndof.Get() > 0);

        auto node_ptr = std::make_shared<Node>(shared_from_this(), extended_unknown);
        node_ptr->Init(Ndof);

        node_list_.push_back(node_ptr);

        return node_ptr;
    }


    void NodeBearer::SetProcessor(rank_type processor)
    {
        processor_ = processor;
    }


    bool NodeBearer::IsUnknown(const Unknown& unknown) const
    {
        const auto& node_list = GetNodeList();

        const auto end = node_list.cend();
        const auto it = std::find_if(node_list.cbegin(),
                                     end,
                                     [&unknown](const auto& node_ptr)
                                     {
                                         assert(!(!node_ptr));
                                         return node_ptr->GetUnknown() == unknown;
                                     });

        return it != end;
    }


    std::size_t NodeBearer::Ndof() const
    {
        const auto& node_list = GetNodeList();

        std::size_t ret = 0UL;

        // NOLINTNEXTLINE(readability-use-anyofallof)
        for (const auto& node_ptr : node_list)
        {
            assert(!(!node_ptr));
            ret += node_ptr->Ndof();
        }

        return ret;
    }


    std::size_t NodeBearer::Ndof(const NumberingSubset& numbering_subset) const
    {
        const auto& node_list = GetNodeList();

        std::size_t ret = 0UL;

        for (const auto& node_ptr : node_list)
        {
            assert(!(!node_ptr));

            if (node_ptr->IsInNumberingSubset(numbering_subset))
                ret += node_ptr->Ndof();
        }

        return ret;
    }


    void NodeBearer::SetProgramWiseIndex(NodeBearerNS::program_wise_index_type index)
    {
        program_wise_index_ = index;
    }


    std::size_t NodeBearer::Nnode(const Unknown& unknown, const std::string& shape_function_label) const
    {
        const auto& node_list = GetNodeList();

        return static_cast<std::size_t>(std::count_if(node_list.cbegin(),
                                                      node_list.cend(),
                                                      [&unknown, &shape_function_label](const auto& node_ptr)
                                                      {
                                                          assert(!(!node_ptr));
                                                          const auto& node = *node_ptr;
                                                          return node.GetUnknown() == unknown
                                                                 && node.GetShapeFunctionLabel()
                                                                        == shape_function_label;
                                                      }));
    }


    bool NodeBearer::IsInNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) node_list = GetNodeList();

        assert(!node_list.empty()
               && "Nodes are created (shortly) after NodeBearer; but current method "
                  "relies upon them to tell whether a NodeBearer is in a NumberingSubset. It is likely "
                  "you attempted to call this method in the middle of the initialization phase, when "
                  "node list aren't defined yet; in this case the result will always yield false.");

        return std::ranges::any_of(node_list,
                                   [&numbering_subset](const auto& node_ptr)
                                   {
                                       assert(!(!node_ptr));
                                       return node_ptr->IsInNumberingSubset(numbering_subset);
                                   });
    }


    void NodeBearer::SetGhost(const NumberingSubset& numbering_subset, rank_type processor)
    {
        [[maybe_unused]] auto [it, is_new_element] =
            ghost_processor_list_per_numbering_subset_.insert({ numbering_subset.GetUniqueId(), {} });

        auto& second_member = it->second;

        second_member.insert(processor);
    }


    const std::set<rank_type>& NodeBearer::GetGhostProcessorList(const NumberingSubset& numbering_subset) const noexcept
    {
        decltype(auto) ghost_processor_list_per_numbering_subset = GetGhostProcessorListPerNumberingSubset();

        auto it = ghost_processor_list_per_numbering_subset.find(numbering_subset.GetUniqueId());

        if (it == ghost_processor_list_per_numbering_subset.cend())
        {
            static const std::set<rank_type> empty;
            return empty;
        }

        return it->second;
    }


    std::set<rank_type> NodeBearer::ComputeGhostProcessorList() const
    {
        std::set<rank_type> ret;

        decltype(auto) ghost_processor_list_per_numbering_subset = GetGhostProcessorListPerNumberingSubset();

        for (const auto& [numbering_subset_id, processor_list] : ghost_processor_list_per_numbering_subset)
            std::ranges::copy(processor_list, std::inserter(ret, ret.begin()));

        return ret;
    }


    bool NodeBearer::IsGhosted(const NumberingSubset& numbering_subset) const noexcept
    {
        auto it = ghost_processor_list_per_numbering_subset_.find(numbering_subset.GetUniqueId());

        if (it == ghost_processor_list_per_numbering_subset_.cend())
            return false;

        if (it->second.empty())
            return false;

        return true;
    }


    Node::vector_shared_ptr NodeBearer::GetNodeList(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) full_node_list = GetNodeList();

        Node::vector_shared_ptr ret;
        ret.reserve(full_node_list.size());

        std::ranges::copy_if(full_node_list,

                             std::back_inserter(ret),
                             [&numbering_subset](const auto& node_ptr)
                             {
                                 assert(!(!node_ptr));
                                 return node_ptr->IsInNumberingSubset(numbering_subset);
                             });

        return ret;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
