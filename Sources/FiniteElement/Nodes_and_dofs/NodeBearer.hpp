// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODEBEARER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODEBEARER_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <set>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

// IWYU pragma: begin_exports
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
// IWYU pragma: end_exports

#include "Geometry/Interfaces/Interface.hpp"

// IWYU pragma: begin_exports
#include "FiniteElement/Nodes_and_dofs/Node.hpp"
#include "FiniteElement/Nodes_and_dofs/StrongType.hpp"
// IWYU pragma: end_exports


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class Unknown; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatchInterfaceNodeBearer; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatrixPattern; }
namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    //! Enum to list all the possible choices available to number the dofs.
    enum class DofNumberingScheme {

        contiguous_per_node //! < Means that all the dofs born by a same node share contiguous indexes.
                            //        contiguous_per_component
    };


    /*!
     *
     * \brief Returns the way dofs are numbered.
     *
     * \return the way dofs are numbered.
     *
     * \todo #256 Put that in the input data file!
     */
    DofNumberingScheme CurrentDofNumberingScheme();


    /*!
     * \brief A NodeBearer is created whenever some dofs are located on a given geometric Interface.
     *
     * So for instance if in a mesh there are triangles P1:
     * - If we consider P1 finite elements, dofs are all located on vertices. So NodeBearer objects will be created
     * for each Vertex object that bears a dof.
     * - If we consider P2 finite elements, there are also dofs on edges. Therefore additional NodeBearer objects are
     * created for each Edge object that bears a dof.
     *
     * Of course, the same is true for Face or Volume interfaces.
     *
     * There is only one NodeBearer for a given interface; there is further classification within NodeBearer objects
     * (Dofs are grouped per Node).
     *
     */
    class NodeBearer final : public std::enable_shared_from_this<NodeBearer>
    {
      public:
        //! Shared smart pointer.
        using shared_ptr = std::shared_ptr<NodeBearer>;

        //! Vector of shared smart pointers.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Shared smart pointer to a const value. This is used for instance when a \a Node creates such a pointer from
        //! a weak_ptr.
        using const_shared_ptr = std::shared_ptr<const NodeBearer>;

        //! Vector of shared smart pointers to a const value. This is used for instance when a \a Node creates such a
        //! pointer from a weak_ptr.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Friendship to give access to internal class.
        friend class Internal::FEltSpaceNS::MatrixPattern;

        //! Friendship to give access to internal class.
        friend class Internal::FEltSpaceNS::MatchInterfaceNodeBearer;

        //! Friendship to \a GodOfDof.
        friend class GodOfDof;

      private:
        //! Convenient alias
        using ghost_processor_list_per_numbering_subset_type =
            std::unordered_map<NumberingSubsetNS::unique_id, std::set<rank_type>>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] interface Interface onto which the node is built.
         *
         * \internal <b><tt>[internal]</tt></b> index is given as a std::size_t as in practice it will be the size of
         * the list that will be passed to this constructor:
         * \code
         * node_list_.push_back(std::make_shared<Node>(interface, node_list_.size()));
         * \endcode
         * \endinternal
         *
         */
        explicit NodeBearer(Interface::shared_ptr interface);

        //! Destructor.
        ~NodeBearer() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NodeBearer(const NodeBearer& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NodeBearer(NodeBearer&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NodeBearer& operator=(const NodeBearer& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NodeBearer& operator=(NodeBearer&& rhs) = delete;

        ///@}

        //! Get index.
        ::MoReFEM::NodeBearerNS::program_wise_index_type GetProgramWiseIndex() const noexcept;

        /*!
         * \brief Returns the list of \a Node matching the given \a unknown and \a shape_function_label.
         *
         * \internal <b><tt>[internal]</tt></b> This method is expected to be used only in init phase, hence the brutal
         * return by value of a vector.
         * No restriction upon \a NumberingSubset in purpose; that's the reason \a ExtendedUnknown is not used.
         * A same \a Node might be defined upon several \a NumberingSubset.
         * \endinternal
         *
         * \param[in] unknown \a Unknown for which the list of \a Node is sought.
         * \param[in] shape_function_label Shape function label for which the list of \a Node is sought.
         *
         * \return List of \a Node related to \a unknown.
         *
         * \internal This method is only called in the initialization phase, and it should remain that way: it involves
         * allocating and fillind a std::vector on the fly.
         * \endinternal
         */
        Node::vector_shared_ptr GetNodeList(const Unknown& unknown, const std::string& shape_function_label) const;

        //! Set the processor to which the node bearer belongs to.
        //! \param[in] processor Processor that handles the current object.
        void SetProcessor(rank_type processor);

        /*!
         * \brief Specify the \a NodeBearer is a ghost on \a processor.
         *
         * \param[in] processor The processor on which \a NodeBearer exists as a ghost.
         * \param[in] numbering_subset \a NumberingSubset for which current rank is following
         * \a NodeBearer (there might be several)
         */
        void SetGhost(const NumberingSubset& numbering_subset, rank_type processor);

        //! Get the processor to which the node bearer belongs to.
        rank_type GetProcessor() const noexcept;

        //! Returns the nature of the node.
        InterfaceNS::Nature GetNature() const noexcept;

        //! Returns the number of dofs born by the node.
        std::size_t Ndof() const;

        //! Returns the number of dofs born by the node inside the \a numbering_subset.
        //! \param[in] numbering_subset \a NumberingSubset used as filter.
        std::size_t Ndof(const NumberingSubset& numbering_subset) const;

        //! Whether an unknown is present or not.
        //! \param[in] unknown \a Unknown under test.
        bool IsUnknown(const Unknown& unknown) const;

        //! Returns the interface on which the node is located.
        const Interface& GetInterface() const noexcept;

        //! Set the index.
        //! \param[in] index Index of the \a NodeBearer, computed outside the class.
        void SetProgramWiseIndex(NodeBearerNS::program_wise_index_type index);

        //! Number of Nodes for a couple Unknown/Shape function label.
        //! \param[in] unknown \a Unknown used as filter.
        //! \param[in] shape_function_label String representing the shape function used as filter.
        std::size_t Nnode(const Unknown& unknown, const std::string& shape_function_label) const;

        //! Whether there are nodes in the node bearer.
        bool IsEmpty() const noexcept;

        //! Return the list of nodes.
        const Node::vector_shared_ptr& GetNodeList() const noexcept;

        /*!
         * \brief Return the list of nodes that match the given \a NumberingSubset.
         *
         * \param[in] numbering_subset Only consider \a Node that are in this \a NumberingSubset.
         *
         * \return The list of \a Node after filtering.
         *
         * \attention This method is not very efficient as it entails a memory allocation - should be performed only during initialization phase.
         */
        Node::vector_shared_ptr GetNodeList(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Whether the \a NodeBearer is a ghost or not.
         *
         * \attention This method is intended to be called ONLY once the partitioning is fully done (but if you're not a
         * developer of the library you shouldn't bother as the partitioning occurs during \a Model::Initialize()  or \a
         * Model::Run() calls which should be one of your very first line of code in the main).
         *
         * This is actually a simple test upon the processor: if processor is same as rank it is not a ghost.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \return True if the \a NodeBearer is a ghost of a \a NodeBearer handled by another mpi rank.
         */
        bool IsGhost(const Wrappers::Mpi& mpi) const noexcept;

        /*!
         * \brief Returns whether the \a NodeBearer is in the \a numbering_subset.
         *
         * \param[in] numbering_subset \a NumberingSubset used as filter.
         *
         * \return True if one of its \a Node is in \a NumberingSubset.
         */
        bool IsInNumberingSubset(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Returns the list of processors that store the \a NodeBearer as a ghost.
         *
         * \param[in] numbering_subset \a NumberingSubset for which we want the list of processors that follow
         * current node bearer.
         *
         * \return The list of processors.
         */
        const std::set<rank_type>& GetGhostProcessorList(const NumberingSubset& numbering_subset) const noexcept;

        /*!
         * \brief Compute the list of all processors that track current \a NodeBearer.
         *
         * \return List of processors.
         */
        std::set<rank_type> ComputeGhostProcessorList() const;

        /*!
         * \brief Whether there are other processors that ghost the \a NodeBearer.
         *
         * \param[in] numbering_subset \a NumberingSubset for which we want the information.
         *
         * \return True if there is at least one.
         */
        bool IsGhosted(const NumberingSubset& numbering_subset) const noexcept;

      private:
        //! Processors that include as ghost the current \a NodeBearer for each \a NumberingSubset.
        const ghost_processor_list_per_numbering_subset_type& GetGhostProcessorListPerNumberingSubset() const noexcept;

        //! Processors that include as ghost the current \a NodeBearer for each \a NumberingSubset.
        ghost_processor_list_per_numbering_subset_type& GetNonCstGhostProcessorListPerNumberingSubset() noexcept;


      private:
        /*!
         * \brief Add a new node.
         *
         * \internal <b><tt>[internal]</tt></b> It is assumed here the node does not exist yet; such liabilities are
         * handled by the class in charge of actually calling such methods (\a MatchInterfaceNodeBearer).
         * \endinternal
         *
         * \param[in] extended_unknown Couple \a Unknown / \a Numbering subset for which the node is/are created.
         * \param[in] Ndof Number of dofs to create on the node.
         *
         * \return Node newly created.
         */
        Node::shared_ptr AddNode(const ExtendedUnknown& extended_unknown, ::MoReFEM::GeometryNS::dimension_type Ndof);


      private:
        //! An internal index that is useful during partitioning steps.
        ::MoReFEM::NodeBearerNS::program_wise_index_type program_wise_index_ =
            NumericNS::UninitializedIndex<NodeBearerNS::program_wise_index_type>();

        /*!
         * \brief Processor that holds the node.
         *
         * Once data have been reduced to processor wise, this data might still be useful: ghost nodes
         * keep existing!
         */
        rank_type processor_ = rank_type{ NumericNS::UninitializedIndex<std::size_t>() };

        /*!
         * \brief Processors that include as ghost the current \a NodeBearer for each \a NumberingSubset.
         *
         */
        ghost_processor_list_per_numbering_subset_type ghost_processor_list_per_numbering_subset_;

        //! List of nodes.
        Node::vector_shared_ptr node_list_;

        //! Interface that bears the node.
        Interface::shared_ptr interface_;
    };


    //! \copydoc doxygen_hide_operator_less
    //! Criterion: the underlying index (returned by GetProgramWiseIndex()).
    bool operator<(const NodeBearer& lhs, const NodeBearer& rhs);

    //! \copydoc doxygen_hide_operator_equal
    //! Criterion: the underlying index (returned by GetProgramWiseIndex()).
    bool operator==(const NodeBearer& lhs, const NodeBearer& rhs);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

namespace std
{


    //! Provide hash function for NodeBearer::shared_ptr.
    template<>
    struct hash<MoReFEM::NodeBearer::shared_ptr>
    {
      public:
        //! Overload of std::has for NodeBearer::shared_ptr
        //! \param[in] ptr Shared pointer to a \a NodeBearer object.
        std::size_t operator()(const MoReFEM::NodeBearer::shared_ptr& ptr) const
        {
            assert(!(!ptr));
            return std::hash<MoReFEM::NodeBearerNS::program_wise_index_type>()(ptr->GetProgramWiseIndex());
        }
    };


} // namespace std


#include "FiniteElement/Nodes_and_dofs/NodeBearer.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODEBEARER_DOT_HPP_
// *** MoReFEM end header guards *** < //
