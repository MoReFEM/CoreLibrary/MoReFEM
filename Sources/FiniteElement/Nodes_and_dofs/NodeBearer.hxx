// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODEBEARER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODEBEARER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }
namespace MoReFEM { class Interface; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline ::MoReFEM::NodeBearerNS::program_wise_index_type NodeBearer::GetProgramWiseIndex() const noexcept
    {
        assert(program_wise_index_ != NumericNS::UninitializedIndex<NodeBearerNS::program_wise_index_type>());
        return program_wise_index_;
    }


    inline auto NodeBearer::GetProcessor() const noexcept -> rank_type
    {
        assert(processor_ != rank_type{ NumericNS::UninitializedIndex<std::size_t>() });
        return processor_;
    }


    inline bool operator<(const NodeBearer& lhs, const NodeBearer& rhs)
    {
        return lhs.GetProgramWiseIndex() < rhs.GetProgramWiseIndex();
    }


    inline bool operator==(const NodeBearer& lhs, const NodeBearer& rhs)
    {
        return lhs.GetProgramWiseIndex() == rhs.GetProgramWiseIndex();
    }


    inline InterfaceNS::Nature NodeBearer::GetNature() const noexcept
    {
        assert(!(!interface_));
        return interface_->GetNature();
    }


    inline const Interface& NodeBearer::GetInterface() const noexcept
    {
        assert(!(!interface_));
        return *interface_;
    }


    inline const Node::vector_shared_ptr& NodeBearer::GetNodeList() const noexcept
    {
        return node_list_;
    }


    inline DofNumberingScheme CurrentDofNumberingScheme()
    {
        return DofNumberingScheme::contiguous_per_node;
    }


    inline bool NodeBearer::IsEmpty() const noexcept
    {
        return node_list_.empty();
    }


    inline bool NodeBearer::IsGhost(const Wrappers::Mpi& mpi) const noexcept
    {
        return GetProcessor() != mpi.GetRank();
    }


    inline auto NodeBearer::GetGhostProcessorListPerNumberingSubset() const noexcept
        -> const ghost_processor_list_per_numbering_subset_type&
    {
        return ghost_processor_list_per_numbering_subset_;
    }


    inline auto NodeBearer::GetNonCstGhostProcessorListPerNumberingSubset() noexcept
        -> ghost_processor_list_per_numbering_subset_type&
    {
        return const_cast<ghost_processor_list_per_numbering_subset_type&>(GetGhostProcessorListPerNumberingSubset());
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_NODEBEARER_DOT_HXX_
// *** MoReFEM end header guards *** < //
