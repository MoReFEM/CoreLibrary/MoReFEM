### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Dof.cpp
		${CMAKE_CURRENT_LIST_DIR}/Node.cpp
		${CMAKE_CURRENT_LIST_DIR}/NodeBearer.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Dof.hpp
		${CMAKE_CURRENT_LIST_DIR}/Dof.hxx
		${CMAKE_CURRENT_LIST_DIR}/DofIndexesTypes.hpp
		${CMAKE_CURRENT_LIST_DIR}/DofIndexesTypes.hxx
		${CMAKE_CURRENT_LIST_DIR}/Node.hpp
		${CMAKE_CURRENT_LIST_DIR}/Node.hxx
		${CMAKE_CURRENT_LIST_DIR}/NodeBearer.hpp
		${CMAKE_CURRENT_LIST_DIR}/NodeBearer.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/StrongType.hpp
		${CMAKE_CURRENT_LIST_DIR}/UniqueId.hpp
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
