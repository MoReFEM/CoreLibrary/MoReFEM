// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERDEGREEOFEXACTNESS_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERDEGREEOFEXACTNESS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM::Advanced::QuadratureRuleNS
{


    /*!
     * \brief Intended to be used as a CRTP for quadrature rules for which selection is done by giving a degree
     * of exactness.
     *
     * The rule with the least points that fulfilled the required exactness is chosen.
     */
    template<class DerivedT>
    struct PerDegreeOfExactness
    {

        //! Number of rules.
        static std::size_t Nrule();

        //! Maximum degree of exactness.
        static std::size_t MaximumDegreeOfExactness();

        //! Return the cheapest quadrature rule ensuring that polynoms of degree \c degree are computed exactly.
        //! \param[in] degree Degree pf exactness used to choose the \a QuadratureRule.
        static const QuadratureRule::const_shared_ptr& GetRuleFromDegreeOfExactness(std::size_t degree);
    };


} // namespace MoReFEM::Advanced::QuadratureRuleNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerDegreeOfExactness.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERDEGREEOFEXACTNESS_DOT_HPP_
// *** MoReFEM end header guards *** < //
