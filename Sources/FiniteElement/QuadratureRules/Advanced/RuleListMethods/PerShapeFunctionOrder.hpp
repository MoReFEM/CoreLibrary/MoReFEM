// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERSHAPEFUNCTIONORDER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERSHAPEFUNCTIONORDER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM::Advanced::QuadratureRuleNS
{


    /*!
     * \brief Intended to be used as a CRTP for quadrature rules for which selection is done by giving the
     * order of  the shape function.
     *
     * This is intended to be used for Spectral finite elements.
     */
    template<class DerivedT>
    struct PerShapeFunctionOrder
    {

        //! Constructor.
        PerShapeFunctionOrder();

        //! Return the quadrature rule adapted for the \a order given, e.g. 3 for Q3.
        //! \param[in] order Order used as filter.
        static QuadratureRule::const_shared_ptr GetRuleFromShapeFunctionOrder(Eigen::Index order);


      private:
        /*!
         * \brief List of quadrature rules stored so far.
         *
         * When a new rule is queried, there is first a check: if it already exists the existing law is
         * returned, otherwise the new one is generated, stored and then returned.
         *
         * \return Key is the order considered, value the related quadrature rule.
         */
        static std::unordered_map<Eigen::Index, QuadratureRule::const_shared_ptr>&
        GetNonCstQuadratureRulePerOrderList();
    };


} // namespace MoReFEM::Advanced::QuadratureRuleNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods//PerShapeFunctionOrder.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERSHAPEFUNCTIONORDER_DOT_HPP_
// *** MoReFEM end header guards *** < //
