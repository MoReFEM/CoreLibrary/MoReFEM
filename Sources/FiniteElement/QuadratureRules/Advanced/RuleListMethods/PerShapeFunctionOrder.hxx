// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERSHAPEFUNCTIONORDER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERSHAPEFUNCTIONORDER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerShapeFunctionOrder.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::QuadratureRuleNS
{


    template<class DerivedT>
    QuadratureRule::const_shared_ptr PerShapeFunctionOrder<DerivedT>::GetRuleFromShapeFunctionOrder(Eigen::Index order)
    {
        // \todo #910 Fix more properly that!
        if (order == 0)
            ++order;

        auto& quadrature_rule_per_order_list = GetNonCstQuadratureRulePerOrderList();

        const auto it = quadrature_rule_per_order_list.find(order);

        if (it != quadrature_rule_per_order_list.cend())
            return it->second;

        auto rule = DerivedT::GetShapeFunctionOrder(order);

        quadrature_rule_per_order_list.insert({ order, rule });
        return rule;
    }


    template<class DerivedT>
    std::unordered_map<Eigen::Index, QuadratureRule::const_shared_ptr>&
    PerShapeFunctionOrder<DerivedT>::GetNonCstQuadratureRulePerOrderList()
    {
        static std::unordered_map<Eigen::Index, QuadratureRule::const_shared_ptr> ret;

        static bool first = true;

        if (first)
        {
            first = false;
            ret.max_load_factor(Utilities::DefaultMaxLoadFactor());
        }

        return ret;
    }


} // namespace MoReFEM::Advanced::QuadratureRuleNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERSHAPEFUNCTIONORDER_DOT_HXX_
// *** MoReFEM end header guards *** < //
