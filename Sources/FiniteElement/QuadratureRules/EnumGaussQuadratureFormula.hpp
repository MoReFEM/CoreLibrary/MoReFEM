// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_ENUMGAUSSQUADRATUREFORMULA_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_ENUMGAUSSQUADRATUREFORMULA_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::QuadratureNS
{


    /*!
     * \brief Variants of Gauss quadrature formula available.
     */
    enum class GaussFormula { Gauss, Gauss_Lobatto };


} // namespace MoReFEM::QuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_ENUMGAUSSQUADRATUREFORMULA_DOT_HPP_
// *** MoReFEM end header guards *** < //
