// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <source_location>
#include <sstream>
#include <string>

#include "FiniteElement/QuadratureRules/Exceptions/QuadratureRuleList.hpp"


namespace // anonymous
{
    // Forward declarations here; definitions are at the end of the file

    std::string InvalidDegreeMsg(std::size_t degree, std::size_t max_degree);


} // namespace


namespace MoReFEM::ExceptionNS::QuadratureRuleListNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    InvalidDegree::~InvalidDegree() = default;


    InvalidDegree::InvalidDegree(std::size_t degree, std::size_t max_degree, const std::source_location location)
    : Exception(InvalidDegreeMsg(degree, max_degree), location)
    { }


} // namespace MoReFEM::ExceptionNS::QuadratureRuleListNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file
    std::string InvalidDegreeMsg(std::size_t degree, std::size_t max_degree)
    {
        std::ostringstream oconv;
        oconv << "A quadrature rule for degree " << degree
              << " has been required but the maximum degree of exactness "
                 "available for an element of this type is "
              << max_degree << '.';

        return oconv.str();
    }


} // namespace
