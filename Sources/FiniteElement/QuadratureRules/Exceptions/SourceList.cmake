### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/QuadratureRuleList.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/QuadratureRuleList.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

