// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_GAUSSQUADRATUREFORMULA_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_GAUSSQUADRATUREFORMULA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <numeric>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp"
#include "FiniteElement/QuadratureRules/Internal/GaussQuadratureFormula.hpp"


namespace MoReFEM::Internal::QuadratureNS
{


    /*!
     * \brief Computation of Gauss quadrature formula.
     *
     * \tparam QuadratureFormulaT Variant of Gauss quadrature formula to use.
     *
     * \param[in] Npoints_gauss Number of Gauss points to consider.
     * \param[out] points Values for each of the quadrature point.
     * \param[out] weights Weight for each of the quadrature point.
     *
     * For details about that function, take a look at
     *    P. N. swarztrauber, Computing the points and weights for
     *    Gauss-Legendre quadrature, SIAM J. Sci. Comput.,
     *    24(2002) pp. 945-954.
     *
     * This code is adapted from Ondomatic, which itself was taken from elsewhere.
     */
    template<::MoReFEM::QuadratureNS::GaussFormula QuadratureFormulaT>
    void ComputeGaussFormulas(Eigen::Index Npoints_gauss, Eigen::VectorXd& points, Eigen::VectorXd& weights);


} // namespace MoReFEM::Internal::QuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_GAUSSQUADRATUREFORMULA_DOT_HPP_
// *** MoReFEM end header guards *** < //
