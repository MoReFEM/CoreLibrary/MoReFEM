// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_GAUSSQUADRATUREFORMULA_DOT_HXX_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_GAUSSQUADRATUREFORMULA_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <numbers>


namespace MoReFEM::Internal::QuadratureNS
{


    template<::MoReFEM::QuadratureNS::GaussFormula QuadratureFormulaT>
    void ComputeGaussFormulas(const Eigen::Index Ngauss_points, Eigen::VectorXd& points, Eigen::VectorXd& weights)
    {
        // For details about that function, take a look at
        //
        //     P. N. swarztrauber, Computing the points and weights for
        //     Gauss-Legendre quadrature, SIAM J. Sci. Comput.,
        //     24(2002) pp. 945-954.
        //

        assert(Ngauss_points > 0 && "Added in MoReFEM; case 0 caused crashes...");

        const double eps = std::numeric_limits<double>::epsilon();

        constexpr double pi = std::numbers::pi;
        constexpr double half_pi = 0.5 * std::numbers::pi;

        // For Gauss-Lobatto, we compute internal nodes.
        assert(!(QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto && Ngauss_points < 2));

        const auto Nquadrature_point =
            (QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto ? Ngauss_points - 1
                                                                                        : Ngauss_points);

        // points(i) will be equal to cos(theta(i))
        Eigen::VectorXd theta(Ngauss_points);
        points.resize(Ngauss_points);
        weights.resize(Ngauss_points);

        // coefficient of Fourier transform of Legendre polynom and derivative
        // we need the second derivative for Gauss-Lobatto points
        Eigen::VectorXd coef(Nquadrature_point);
        coef.setZero();
        Eigen::VectorXd deriv_coef{ coef };
        Eigen::VectorXd second_deriv_coef{ coef };

        // exact expressions are used for order 1, 2, 3
        switch (Nquadrature_point)
        {
        case 1:
            Internal::GaussQuadratureNS::ComputeExactFormula<QuadratureFormulaT, 1UL>::Perform(points, weights);
            return;
        case 2:
            Internal::GaussQuadratureNS::ComputeExactFormula<QuadratureFormulaT, 2UL>::Perform(points, weights);
            return;
        case 3:
            Internal::GaussQuadratureNS::ComputeExactFormula<QuadratureFormulaT, 3UL>::Perform(points, weights);
            return;
        default:
            break;
        }

        // for order greater than 3, numerical computation
        const auto Nparity_points = Nquadrature_point % 2;
        const auto Nhalf_points = Ngauss_points / 2;
        const auto Nhalf = (Nquadrature_point + 1) / 2;

        double zero = 0.0;
        double cz = zero;

        Internal::GaussQuadratureNS::ComputeFourierCoef(cz, coef, deriv_coef, second_deriv_coef);

        double dtheta = zero;
        if (QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto && (Nparity_points == 1))
            dtheta = half_pi / static_cast<double>(Nhalf - 1);
        else
            dtheta = half_pi / static_cast<double>(Nhalf);

        const double dthalf = 0.5 * dtheta;
        const double cmax = 0.2 * dtheta;

        // the zeros of Legendre polynom
        double zprev = zero, zlast = zero;
        double pb = zero, dpb = zero, ddpb = zero, dcor = zero;
        Eigen::Index nix{};

        //
        //     estimate first point next to theta = pi/2
        //
        if (Nparity_points != 0)
        {
            // Nquadrature_point = 2 Nhalf-1
            // if odd the first zero is at the middle pi/2
            // and the following pi/2-pi/(2*Nhalf)
            if (QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto)
            {
                zero = half_pi - dthalf;
                zprev = half_pi + dthalf;
                nix = Nhalf - 1;
            } else
            {
                zero = half_pi - dtheta;
                zprev = half_pi;
                nix = Nhalf - 1; // index of the point
            }
        } else
        {
            // if even, no zero on the middle, the first zero is on pi/2-pi/(4*Nhalf)
            if (QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto)
            {
                zero = half_pi - dtheta;
                zprev = half_pi;
                nix = Nhalf - 1; // index of the point
            } else
            {
                zero = half_pi - dthalf;
                zprev = half_pi + dthalf;
                nix = Nhalf;
            }
        }

        bool each_point_not_computed = true;

        while (each_point_not_computed)
        {
            int Niteration = 0;
            bool test_convergence_newton = true;
            double residu(1.0);

            while (test_convergence_newton && Niteration < 100)
            {
                ++Niteration;
                zlast = zero;

                //
                //     newton iterations
                //
                Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                    Nquadrature_point, zero, cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);

                if (QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto)
                    dcor = dpb / ddpb;
                else
                    dcor = pb / dpb;

                const double sgnd = NumericNS::IsZero(dcor) ? 1.0 : dcor / std::fabs(dcor);

                // we don't move the point further than 0.2*delta_theta
                double tmp = std::fabs(dcor);
                dcor = sgnd * std::min(tmp, cmax);
                zero = zero - dcor;
                double residu_prec = residu;
                residu = std::fabs(zero - zlast);
                // we check if the stopping criteria are reached
                if ((std::fabs(zero - zlast) <= eps * std::fabs(zero)) || ((Niteration > 5) && (residu_prec < residu)))
                    test_convergence_newton = false;
            }

            assert(nix - 1 < theta.size());
            theta(nix - 1) = zero;
            double zhold = zero;
            //      weights(nix) = (Nquadrature_point+Nquadrature_point+1)/(dpb*dpb)
            //
            //     yakimiw's formula permits using old pb and dpb
            //
            if (QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto)
            {
                double tmp = NumericNS::Square(pb - dcor * dpb + 0.5 * dcor * dcor * ddpb);
                assert(!NumericNS::IsZero(tmp));
                assert(nix - 1 < weights.size());
                weights(nix - 1) = 1.0 / tmp;
            } else
            {
                assert(!NumericNS::IsZero(std::sin(zlast)));
                double tmp = NumericNS::Square(dpb + pb * std::cos(zlast) / std::sin(zlast));
                assert(!NumericNS::IsZero(tmp));
                assert(nix - 1 < weights.size());
                weights(nix - 1) = static_cast<double>(2 * Nquadrature_point + 1) / tmp;
            }

            --nix;

            if (nix == 0)
                each_point_not_computed = false;
            else if (nix <= (Nhalf - 1))
                zero += zero - zprev;

            zprev = zhold;
        }

        //
        //     extend points and weights via symmetries
        //
        if ((QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss) && (Nparity_points != 0))
        {
            assert(Nhalf - 1 < theta.size());
            theta(Nhalf - 1) = half_pi;
            Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                Nquadrature_point, half_pi, cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);

            assert(!NumericNS::IsZero(dpb));
            assert(Nhalf - 1 < weights.size());
            weights(Nhalf - 1) = static_cast<double>(2 * Nquadrature_point + 1) / NumericNS::Square(dpb);
        }

        if ((QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto) && (Nparity_points == 0))
        {
            assert(Nhalf - 1 < theta.size());
            theta(Nhalf - 1) = half_pi;
            Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                Nquadrature_point, half_pi, cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);
            assert(!NumericNS::IsZero(pb));
            assert(Nhalf - 1 < weights.size());
            weights(Nhalf - 1) = 1. / NumericNS::Square(pb);
        }

        // DISP(Nhalf); DISP(theta);DISP(weights);
        if (QuadratureFormulaT == ::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto)
        {
            assert(theta.size() >= Nhalf);
            assert(weights.size() >= Nhalf);

            for (auto i = Nhalf - 1; i >= 0; --i)
            {
                theta(i + 1) = theta(i);
                weights(i + 1) = weights(i);
            }


            theta(0) = 0.0;
            Internal::GaussQuadratureNS::ComputeLegendrePolAndDerivative(
                Nquadrature_point, theta(0), cz, coef, deriv_coef, second_deriv_coef, pb, dpb, ddpb);

            // DISP(cz); DISP(pb);
            weights(0) = 1.0 / (NumericNS::Square(pb));
        }

        // DISP(theta);
        for (auto i = Eigen::Index{}; i < Nhalf_points; i++)
        {
            assert(Ngauss_points - i - 1 < weights.size());
            assert(Ngauss_points - i - 1 < theta.size());
            weights(Ngauss_points - i - 1) = weights(i);
            theta(Ngauss_points - i - 1) = pi - theta(i);
        }

        // DISP(theta); DISP(weights);
        double sum = 0.;

        assert(Ngauss_points <= weights.size());
        assert(Ngauss_points <= theta.size());
        for (auto i = Eigen::Index{}; i < Ngauss_points; i++)
            sum += weights(i);

        assert(!NumericNS::IsZero(sum));

        for (auto i = Eigen::Index{}; i < Ngauss_points; i++)
        {
            weights(i) = 2. * weights(i) / sum;
            points(i) = std::cos(theta(i));
        }

        // reverse the arrays
        const auto Nweight = weights.size();

        for (auto i = Eigen::Index{}; i < Nweight / 2; i++)
        {
            double tmp = weights(i);
            assert(weights.size() >= i + 1);
            auto index = weights.size() - i - 1;

            weights(i) = weights[index];
            weights[index] = tmp;
        }

        const auto Npoints = points.size();

        for (auto i = Eigen::Index{}; i < Npoints / 2; i++)
        {
            double tmp = points(i);
            assert(points.size() >= i + 1);
            auto index = Nweight - i - 1;

            points(i) = points[index];
            points[index] = tmp;
        }
    }


} // namespace MoReFEM::Internal::QuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_GAUSSQUADRATUREFORMULA_DOT_HXX_
// *** MoReFEM end header guards *** < //
