// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_INSTANTIATION_FWDFORCPP_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_INSTANTIATION_FWDFORCPP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>       // IWYU pragma: export
#include <cassert>     // IWYU pragma: export
#include <cmath>       // IWYU pragma: export
#include <iosfwd>      // IWYU pragma: export
#include <memory>      // IWYU pragma: export
#include <string>      // IWYU pragma: export
#include <type_traits> // IWYU pragma: keep // IWYU pragma: export
#include <vector>      // IWYU pragma: export

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: export

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp" // IWYU pragma: export
#include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"     // IWYU pragma: export
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"             // IWYU pragma: export

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_INSTANTIATION_FWDFORCPP_DOT_HPP_
// *** MoReFEM end header guards *** < //
