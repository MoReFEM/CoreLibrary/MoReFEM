// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_INSTANTIATION_TETRAHEDRON_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_INSTANTIATION_TETRAHEDRON_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <array>

#include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerDegreeOfExactness.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM::QuadratureNS
{


    //! CRTP-defined class for the quadrature rules that concern tetrahedrons.
    struct Tetrahedron : public Advanced::QuadratureRuleNS::PerDegreeOfExactness<Tetrahedron>
    {

        //! \copydoc doxygen_hide_quadrature_rule_degree_of_exactness_list_method
        static const std::array<QuadratureRule::const_shared_ptr, 5>& GetPerDegreeOfExactnessList();
    };


} // namespace MoReFEM::QuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_INSTANTIATION_TETRAHEDRON_DOT_HPP_
// *** MoReFEM end header guards *** < //
