// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "FiniteElement/QuadratureRules/Instantiation/Triangle.hpp"

#include "Utilities/Warnings/Pragma.hpp"

#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::QuadratureNS
{


    namespace // anonymous
    {


        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/return-std-move-in-c++11.hpp" // IWYU pragma: keep
        std::array<QuadratureRule::const_shared_ptr, 5> CreateQuadratureRuleListPerDegreeOfExactness()
        {


            auto one_point_ptr = std::make_shared<QuadratureRule>("triangle_1_point", TopologyNS::Type::triangle, 1);

            {
                const double one_third = 1. / 3.;

                one_point_ptr->AddQuadraturePoint(LocalCoords({ one_third, one_third }), .5);
            }


            auto three_points_ptr =
                std::make_shared<QuadratureRule>("triangle_3_points", TopologyNS::Type::triangle, 2);

            {
                auto& three_points = *three_points_ptr;
                const double one_sixth = 1. / 6.;

                three_points.AddQuadraturePoint(LocalCoords({ 0.5, 0. }), one_sixth);
                three_points.AddQuadraturePoint(LocalCoords({ 0., 0.5 }), one_sixth);
                three_points.AddQuadraturePoint(LocalCoords({ 0.5, 0.5 }), one_sixth);
                assert(three_points.NquadraturePoint() == 3);
            }


            // 4 points Integration rule for triangle (Ref. e.g. Comincioli pag. 234)
            auto four_points_ptr = std::make_shared<QuadratureRule>("triangle_4_points", TopologyNS::Type::triangle, 3);

            {
                auto& four_points = *four_points_ptr;
                const double three_fifth = 3. / 5.;
                const double one_fifth = 1. / 5.;
                const double weight1 = 25. / 96.;
                const double weight2 = -9. / 32.;
                const double one_third = 1. / 3.;

                four_points.AddQuadraturePoint(LocalCoords({ three_fifth, one_fifth }), weight1);
                four_points.AddQuadraturePoint(LocalCoords({ one_fifth, three_fifth }), weight1);
                four_points.AddQuadraturePoint(LocalCoords({ one_fifth, one_fifth }), weight1);
                four_points.AddQuadraturePoint(LocalCoords({ one_third, one_third }), weight2);
            }


            // 6 points Integration rule for triangle
            // Ref: G.R. Cowper,  Gaussian quadrature formulas for triangles,
            //      Internat. J. Numer. Methods Engrg.  7 (1973), 405--408.
            auto six_points_ptr = std::make_shared<QuadratureRule>("triangle_6_points", TopologyNS::Type::triangle, 4);

            {
                auto& six_points = *six_points_ptr;

                const double alpha = 0.091576213509770743;
                const double beta = 0.44594849091596488;
                const double gamma = 1. - 2. * alpha;
                const double delta = 1. - 2. * beta;
                const double weight1 = 0.054975871827660933;
                const double weight2 = 0.11169079483900573;

                six_points.AddQuadraturePoint(LocalCoords({ alpha, alpha }), weight1);
                six_points.AddQuadraturePoint(LocalCoords({ alpha, gamma }), weight1);
                six_points.AddQuadraturePoint(LocalCoords({ gamma, alpha }), weight1);
                six_points.AddQuadraturePoint(LocalCoords({ beta, beta }), weight1);
                six_points.AddQuadraturePoint(LocalCoords({ beta, delta }), weight2);
                six_points.AddQuadraturePoint(LocalCoords({ delta, beta }), weight2);
            }


            // 7 points Integration rule for triangle (Ref. Stroud) D of Ex = 5
            auto seven_points_ptr =
                std::make_shared<QuadratureRule>("triangle_7_points", TopologyNS::Type::triangle, 5);

            {
                auto& seven_points = *seven_points_ptr;

                const double one_third = 1. / 3.;
                const double alpha = 0.10128650732345633;
                const double beta = 0.47014206410511508;
                const double gamma = 1. - 2. * alpha;
                const double delta = 1. - 2. * beta;

                const double weight1 = 0.1125;
                const double weight2 = 0.062969590272413576;
                const double weight3 = 0.066197076394253090;

                seven_points.AddQuadraturePoint(LocalCoords({ one_third, one_third }), weight1);
                seven_points.AddQuadraturePoint(LocalCoords({ alpha, alpha }), weight2);
                seven_points.AddQuadraturePoint(LocalCoords({ alpha, gamma }), weight2);
                seven_points.AddQuadraturePoint(LocalCoords({ gamma, alpha }), weight2);
                seven_points.AddQuadraturePoint(LocalCoords({ beta, beta }), weight3);
                seven_points.AddQuadraturePoint(LocalCoords({ beta, delta }), weight3);
                seven_points.AddQuadraturePoint(LocalCoords({ delta, beta }), weight3);
            }


            assert(one_point_ptr->NquadraturePoint() == 1);
            assert(three_points_ptr->NquadraturePoint() == 3);
            assert(four_points_ptr->NquadraturePoint() == 4);
            assert(six_points_ptr->NquadraturePoint() == 6);
            assert(seven_points_ptr->NquadraturePoint() == 7);


            return { { one_point_ptr, three_points_ptr, four_points_ptr, six_points_ptr, seven_points_ptr } };
        }


    } // namespace


    const std::array<QuadratureRule::const_shared_ptr, 5>& Triangle::GetPerDegreeOfExactnessList()
    {
        static const std::array<QuadratureRule::const_shared_ptr, 5> ret =
            CreateQuadratureRuleListPerDegreeOfExactness();

        return ret;
    }
    PRAGMA_DIAGNOSTIC(pop)


} // namespace MoReFEM::QuadratureNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
