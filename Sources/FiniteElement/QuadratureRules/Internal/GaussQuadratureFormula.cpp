// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <numbers>
// IWYU pragma: no_include <type_traits>

#include "Utilities/Numeric/Numeric.hpp"

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp"
#include "FiniteElement/QuadratureRules/Internal/GaussQuadratureFormula.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Internal::GaussQuadratureNS
{

    using GaussFormula = QuadratureNS::GaussFormula;

    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    void
    ComputeExactFormula<::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto, 1UL>::Perform(Eigen::VectorXd& points,
                                                                                            Eigen::VectorXd& weights)
    {
        // [1-,1] with weights 1, 1
        points(0) = -1.;
        points(1) = 1.;

        weights(0) = 1.;
        weights(1) = 1.;
    }


    void
    ComputeExactFormula<::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto, 2UL>::Perform(Eigen::VectorXd& points,
                                                                                            Eigen::VectorXd& weights)
    {
        // [-1, 0, 1] with weights 1/3, 4/3, 1/3
        points(0) = -1.;
        points(1) = 0.;
        points(2) = 1.;

        weights(0) = 1. / 3.;
        weights(1) = 4. / 3.;
        weights(2) = weights(0);
    }


    void
    ComputeExactFormula<::MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto, 3UL>::Perform(Eigen::VectorXd& points,
                                                                                            Eigen::VectorXd& weights)
    {
        points(0) = -1.;
        points(1) = ((5. - std::sqrt(5.)) * .2) - 1.;
        points(2) = -points(1);
        points[3] = 1.;

        weights(0) = 1. / 6.;
        weights(1) = 10. * weights(0);
        weights(2) = weights(1);
        weights[3] = weights(0);
    }

    void ComputeExactFormula<GaussFormula::Gauss, 1UL>::Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights)
    {
        points(0) = 0.;
        weights(0) = 2.;
    }


    void ComputeExactFormula<GaussFormula::Gauss, 2UL>::Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights)
    {
        points(0) = (3. - std::numbers::sqrt3) / 3. - 1.;
        points(1) = -points(0);

        weights(0) = 1.;
        weights(1) = 1.;
    }


    void ComputeExactFormula<GaussFormula::Gauss, 3UL>::Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights)
    {
        points(0) = -sqrt(15.) / 5.; // Check formula! See #240...
        points(1) = 0.;
        points(2) = -points(0);

        weights(0) = 5. / 9.;
        weights(1) = 8. / 9.;
        weights(2) = weights(0);
    }


    void ComputeFourierCoef(double& cz, Eigen::VectorXd& cp, Eigen::VectorXd& dcp, Eigen::VectorXd& ddcp)
    {
        const auto n = cp.size();

        const auto ncp = (n + 1) / 2;
        double t1 = -1.;
        double t2 = static_cast<double>(n) + 1.;
        double t3 = 0.;
        double t4 = 2. * static_cast<double>(n) + 1.;

        assert(dcp.size() == cp.size()); // Number of quadrature points.
        assert(ddcp.size() == cp.size());

        if (n % 2 == 0)
        {
            assert(ncp < cp.size());
            cp[ncp] = 1.;
            for (Eigen::Index j = ncp; j >= 2; --j)
            {
                t1 += 2.;
                t2 += -1.;
                t3 += 1.;
                t4 += -2.;
                assert(!NumericNS::IsZero(t3));
                assert(!NumericNS::IsZero(t4));
                cp[j - 1] = (t1 * t2) / (t3 * t4) * cp[j];
            }

            t1 = t1 + 2.;
            t2 = t2 - 1.;
            t3 = t3 + 1.;
            t4 = t4 - 2.;

            assert(!NumericNS::IsZero(t3));
            assert(!NumericNS::IsZero(t4));
            cz = (t1 * t2) / (t3 * t4) * cp(1);

            for (Eigen::Index j = 1; j <= ncp; j++)
            {
                dcp[j] = (2 * static_cast<double>(j)) * cp[j];
                ddcp[j] = (2 * static_cast<double>(j)) * dcp[j];
            }
        } else
        {
            cp[ncp] = 1.;
            for (Eigen::Index j = ncp - 1; j >= 1; j--)
            {
                t1 = t1 + 2.;
                t2 = t2 - 1.;
                t3 = t3 + 1.;
                t4 = t4 - 2.;

                assert(!NumericNS::IsZero(t3));
                assert(!NumericNS::IsZero(t4));
                cp[j] = (t1 * t2) / (t3 * t4) * cp[j + 1];
            }
            for (Eigen::Index j = 1; j <= ncp; j++)
            {
                dcp[j] = static_cast<double>(2 * j - 1) * cp[j];
                ddcp[j] = static_cast<double>(2 * j - 1) * dcp[j];
            }
        }
    }


    //! intermediary function used for the computation of Gauss points
    void ComputeLegendrePolAndDerivative(Eigen::Index n,
                                         double theta,
                                         double cz,
                                         const Eigen::VectorXd& cp,
                                         const Eigen::VectorXd& dcp,
                                         const Eigen::VectorXd& ddcp,
                                         double& pb,
                                         double& dpb,
                                         double& ddpb)
    {
        const double cdt = std::cos(2 * theta);
        const double sdt = std::sin(2 * theta);

        assert(cp.size() == dcp.size());
        assert(cp.size() == ddcp.size());

        if (n % 2 == 0)
        {
            // n even
            const Eigen::Index kdo = n / 2;

            assert(kdo < cp.size());

            pb = cz / 2;
            dpb = 0.;
            ddpb = 0.;
            if (n > 0)
            {
                double cth = cdt;
                double sth = sdt;
                for (Eigen::Index k = 1; k <= kdo; ++k)
                {
                    //      pb = pb+cp(k)*std::cos(2*k*theta)
                    pb += cp[k] * cth;
                    //      dpb = dpb-(k+k)*cp(k)*std::sin(2*k*theta)
                    dpb -= dcp[k] * sth;
                    ddpb -= ddcp[k] * cth;
                    const double chh = cdt * cth - sdt * sth;
                    sth = sdt * cth + cdt * sth;
                    cth = chh;
                }
            }
        } else
        {

            //  n odd
            const Eigen::Index kdo = (n + 1) / 2;

            assert(kdo < cp.size());

            pb = 0.;
            dpb = 0.;
            ddpb = 0.;
            double cth = std::cos(theta);
            double sth = std::sin(theta);

            for (Eigen::Index k = 1; k <= kdo; k++)
            {
                //  pb = pb+cp(k)*std::cos((2*k-1)*theta)
                pb += cp[k] * cth;
                // dpb = dpb-(k+k-1)*cp(k)*std::sin((2*k-1)*theta)
                dpb -= dcp[k] * sth;
                ddpb -= ddcp[k] * cth;
                const double chh = cdt * cth - sdt * sth;
                sth = sdt * cth + cdt * sth;
                cth = chh;
            }
        }
    }
    // NOLINTEND(bugprone-easily-swappable-parameters)

} // namespace MoReFEM::Internal::GaussQuadratureNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
