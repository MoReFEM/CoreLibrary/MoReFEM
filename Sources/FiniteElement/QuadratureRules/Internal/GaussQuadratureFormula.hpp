// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_INTERNAL_GAUSSQUADRATUREFORMULA_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_INTERNAL_GAUSSQUADRATUREFORMULA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp"


namespace MoReFEM::Internal::GaussQuadratureNS
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Ignored as I do not have a clue about most of the function arguments...
    // ============================

    /*!
     * \brief Intermediary function used for the computation of Gauss points
     *
     *     computes pn(theta) and its derivative dpb(theta) with
     *     respect to theta
     *
     *     computes the fourier coefficients of the legendre
     *     polynomial p_n0 and its derivative.
     *     n is the degree and n/2 or (n+1)/2
     *     coefficients are returned in cp depending on whether
     *     n is even or odd. The same number of coefficients
     *     are returned in dcp. For n even the constant
     *     coefficient is returned in cz.
     *
     * \param[out] cp This container is already allocated in input; itss size is the number of quadrature
     * points. \param[out] dcp This container is already allocated in input; itss size is the number of
     * quadrature points. \param[out] ddcp This container is already allocated in input; itss size is the number
     * of quadrature points.
     *
     * n in the text above is the size of either of the container involved.
     */
    void ComputeFourierCoef(double& cz, Eigen::VectorXd& cp, Eigen::VectorXd& dcp, Eigen::VectorXd& ddcp);


    /*!
     * \brief Intermediary function used for the computation of Gauss points
     *
     *  Computes pn(theta) and its derivative dpb(theta) with respect to theta.
     */
    void ComputeLegendrePolAndDerivative(Eigen::Index n,
                                         double theta,
                                         double cz,
                                         const Eigen::VectorXd& cp,
                                         const Eigen::VectorXd& dcp,
                                         const Eigen::VectorXd& ddcp,
                                         double& pb,
                                         double& dpb,
                                         double& ddpb);


    /*!
     * \brief Compute exact formula.
     *
     * \tparam QuadratureFormulaT Whether Gauss or Gauss-Lobatto is considered/
     * \warning Only Gauss-Lobatto one has been checked; other one was taken from Ondomatic
     * but not used at all.
     * \tparam I Used to determine the number of quadrature points to use.
     * In Gauss-Lobatto, I + 1 7quadrature points are used.
     * In Gauss, I quadrature points are used.
     *
     */
    template<QuadratureNS::GaussFormula QuadratureFormulaT, std::size_t I>
    struct ComputeExactFormula;


    template<>
    struct ComputeExactFormula<MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto, 1UL>
    {

        static void Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights);
    };


    template<>
    struct ComputeExactFormula<MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto, 2UL>
    {

        static void Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights);
    };


    template<>
    struct ComputeExactFormula<MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto, 3UL>
    {

        static void Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights);
    };


    template<>
    struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss, 1UL>
    {

        static void Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights);
    };


    template<>
    struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss, 2UL>
    {

        static void Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights);
    };


    template<>
    struct ComputeExactFormula<QuadratureNS::GaussFormula::Gauss, 3UL>
    {

        static void Perform(Eigen::VectorXd& points, Eigen::VectorXd& weights);
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::GaussQuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_INTERNAL_GAUSSQUADRATUREFORMULA_DOT_HPP_
// *** MoReFEM end header guards *** < //
