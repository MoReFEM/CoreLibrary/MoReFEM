// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATUREPOINT_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATUREPOINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp"


namespace MoReFEM
{


    /*!
     * \brief Quadrature point used to perform discrete integration.
     *
     */
    class QuadraturePoint final : public LocalCoords
    {
      public:
        //! Alias to shared_ptr to constant object.
        using const_shared_ptr = std::shared_ptr<const QuadraturePoint>;

        //! Alias to a vector of const_shared_ptr.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;


      public:
        /// \name Constructors and destructor.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] local_coords Coordinates of the quadrature point.
         * \param[in] weight Weight of the point in the integration.
         * \param[in] rule_name Name of the quadrature rule to which the point belongs.
         * \param[in] index Index within the quadrature rule.
         */
        explicit QuadraturePoint(LocalCoords&& local_coords,
                                 double weight,
                                 const std::string& rule_name,
                                 std::size_t index);

        //! Destructor.
        virtual ~QuadraturePoint() override;

        //! \copydoc doxygen_hide_copy_constructor
        QuadraturePoint(const QuadraturePoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        QuadraturePoint(QuadraturePoint&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        QuadraturePoint& operator=(const QuadraturePoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        QuadraturePoint& operator=(QuadraturePoint&& rhs) = delete;

        ///@}

        //! Get the weight.
        double GetWeight() const noexcept;

        //! Print function (used also for operator<< overload).
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;

        //! Get the name of the quadrature rule to which the point belongs.
        const std::string& GetRuleName() const;

        //! Get the index of the quadrature point within the quadrature rule.
        std::size_t GetIndex() const noexcept;


      private:
        //! Weight of the point.
        const double weight_;

        //! Reference to the name of the quadrature rule to which the point belongs.
        const std::string& rule_name_;

        //! Index within the quadrature rule (from 0 to Nquadrature_point - 1).
        const std::size_t index_;
    };


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const QuadraturePoint& rhs);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/QuadratureRules/QuadraturePoint.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATUREPOINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
