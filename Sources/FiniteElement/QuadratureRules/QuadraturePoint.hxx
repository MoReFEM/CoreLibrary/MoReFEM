// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATUREPOINT_DOT_HXX_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATUREPOINT_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <string>

namespace MoReFEM
{


    inline double QuadraturePoint::GetWeight() const noexcept
    {
        return weight_;
    }


    inline const std::string& QuadraturePoint::GetRuleName() const
    {
        return rule_name_;
    }


    inline std::size_t QuadraturePoint::GetIndex() const noexcept
    {
        return index_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATUREPOINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
