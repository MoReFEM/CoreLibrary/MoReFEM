// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"

#include "Geometry/Coords/LocalCoords.hpp"


namespace MoReFEM
{


    QuadratureRule::QuadratureRule(std::string&& name,
                                   QuadraturePoint::vector_const_shared_ptr&& point_list,
                                   TopologyNS::Type topology_id,
                                   std::size_t degree_of_exactness)
    : name_(std::move(name)), point_list_(std::move(point_list)), topology_id_(topology_id),
      degree_of_exactness_(degree_of_exactness)
    { }


    QuadratureRule::QuadratureRule(std::string&& name, TopologyNS::Type topology_id, std::size_t degree_of_exactness)
    : name_(std::move(name)), topology_id_(topology_id), degree_of_exactness_(degree_of_exactness)
    { }


    QuadratureRule::QuadratureRule(TopologyNS::Type topology_id, std::size_t degree_of_exactness)
    : topology_id_(topology_id), degree_of_exactness_(degree_of_exactness)
    { }


    void QuadratureRule::AddQuadraturePoint(LocalCoords&& local_coords, double weight)
    {
        auto&& quad_pt =
            std::make_shared<const QuadraturePoint>(std::move(local_coords), weight, GetName(), point_list_.size());

        point_list_.emplace_back(std::move(quad_pt));
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
