// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/EnumTopology.hpp" // IWYU pragma: export

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{

    /*!
     * \brief Defines a quadrature rule.
     */
    class QuadratureRule
    {

      public:
        //! Alias to shared_ptr to constant object.
        using const_shared_ptr = std::shared_ptr<const QuadratureRule>;

        //! Alias to unique_ptr to constant object.
        using const_unique_ptr = std::unique_ptr<const QuadratureRule>;

        //! Alias to a vector of const_shared_ptr.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;


      public:
        /*!
         * \class doxygen_hide_quadrature_rule_constructor_args
         *
         * \param[in] topology_id Topology identifier.
         * \param[in] degree_of_exactness Degree of exactness of the rule. Choose NumericNS::UninitiazedIndex() for
         * rules for which it is pointless.
         *
         * \todo This should be handled more properly with inheritance, so that rules for which it is pointless do not
         * even have such an attribute, but convention above will do for the time being.
         */

        /*!
         * \class doxygen_hide_quadrature_rule_constructor_name_arg
         *
         * \param[in] name Name of the quadrature rule.
         */


        /// \name Constructors and destructor.
        ///@{

        /*!
         * Constructor.
         *
         * \copydetails doxygen_hide_quadrature_rule_constructor_args
         * \copydetails doxygen_hide_quadrature_rule_constructor_name_arg
         * \param[in] point_list List of quadrature points.
         */
        explicit QuadratureRule(std::string&& name,
                                QuadraturePoint::vector_const_shared_ptr&& point_list,
                                TopologyNS::Type topology_id,
                                std::size_t degree_of_exactness);

        /*!
         * Constructor.
         *
         * In this overload, quadrature points aren't yet defined and must be added afterwards through
         * \a AddQuadraturePoint() method.
         *
         * \copydetails doxygen_hide_quadrature_rule_constructor_args
         * \copydetails doxygen_hide_quadrature_rule_constructor_name_arg
         */
        explicit QuadratureRule(std::string&& name,
                                TopologyNS::Type topology_id,
                                std::size_t degree_of_exactness = NumericNS::UninitializedIndex<std::size_t>());

        /*!
         * \brief Dummy constructor with only degree of exactness defined.
         *
         * Should be used solely to ease definition of sorting functions.
         *
         * \copydetails doxygen_hide_quadrature_rule_constructor_args
         *
         * \code
         *
         * auto sorting_rule = [](const GetQuadratureRule& rule1, const GetQuadratureRule& rule2)
         * {
         *      return rule1.DegreeOfExactness() < rule2.DegreeOfExactness();
         * };
         *
         * std::vector<QuadratureRule> list;
         * ... defines your list ...
         *
         * std::lower_bound(list.cbegin(), list.cend(), GetQuadratureRule(5), sorting_rule);
         *
         * \endcode
         */
        explicit QuadratureRule(TopologyNS::Type topology_id, std::size_t degree_of_exactness);

        //! Destructor.
        ~QuadratureRule() = default;

        //! \copydoc doxygen_hide_copy_constructor
        QuadratureRule(const QuadratureRule& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        QuadratureRule(QuadratureRule&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        QuadratureRule& operator=(const QuadratureRule& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        QuadratureRule& operator=(QuadratureRule&& rhs) = delete;


        ///@}


        /*!
         * \brief Add a new quadrature point.
         *
         * The quadrature point is created inside the function from \a local_coords and \a weight.
         *
         * \param[in] local_coords Local coordinates of the quadrature point to create.
         * \param[in] weight Weight of the quadrature point to create.
         */
        void AddQuadraturePoint(LocalCoords&& local_coords, double weight);

        //! Number of quadrature points.
        Eigen::Index NquadraturePoint() const noexcept;

        //! Access to one quadrature point.
        //! \param[in] index Position in the vector of the sought \a QuadraturePoint.
        const QuadraturePoint& Point(Eigen::Index index) const noexcept;

        //! Return the degree of exactness if relevant (and will assert in debug if not).
        std::size_t DegreeOfExactness() const noexcept;

        //! Identifier of the topology upon which the rule is defined.
        TopologyNS::Type GetTopologyIdentifier() const noexcept;

        //! Returns the name of the quadrature rule.
        const std::string& GetName() const noexcept;

        //! Returns the list of quadrature points.
        const QuadraturePoint::vector_const_shared_ptr& GetQuadraturePointList() const noexcept;


      private:
        //! Name of the quadrature rule.
        std::string name_;

        //! List of quadrature points.
        QuadraturePoint::vector_const_shared_ptr point_list_;

        //! Identifier of the geometric element upon which the rule is defined.
        const TopologyNS::Type topology_id_;

        /*!
         * \brief Degree of exactness.
         *
         * Equal to NumericNS::UninitiazedIndex() for rules for which it is pointless.
         *
         * \todo This should be handled more properly with inheritance, so that rules for which it is pointless do not
         * even have such an attribute, but convention above will do for the time being.
         */
        const std::size_t degree_of_exactness_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/QuadratureRules/QuadratureRule.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HPP_
// *** MoReFEM end header guards *** < //
