// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>


namespace MoReFEM
{


    inline auto QuadratureRule::NquadraturePoint() const noexcept -> Eigen::Index
    {
        assert(!point_list_.empty());
        return static_cast<Eigen::Index>(point_list_.size());
    }


    inline const QuadraturePoint& QuadratureRule::Point(Eigen::Index index) const noexcept
    {
        const auto size_t_index = static_cast<std::size_t>(index);
        assert(!point_list_.empty());
        assert(size_t_index < point_list_.size());
        assert(!(!point_list_[size_t_index]));
        return *(point_list_[size_t_index]);
    }


    inline std::size_t QuadratureRule::DegreeOfExactness() const noexcept
    {
        assert(degree_of_exactness_ != NumericNS::UninitializedIndex<std::size_t>()
               && "Should be called only if it is relevant for the law!");
        return degree_of_exactness_;
    }


    inline TopologyNS::Type QuadratureRule::GetTopologyIdentifier() const noexcept
    {
        return topology_id_;
    }


    inline const std::string& QuadratureRule::GetName() const noexcept
    {
        assert(!name_.empty());
        return name_;
    }


    inline const QuadraturePoint::vector_const_shared_ptr& QuadratureRule::GetQuadraturePointList() const noexcept
    {
        assert(!point_list_.empty());
        return point_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HXX_
// *** MoReFEM end header guards *** < //
