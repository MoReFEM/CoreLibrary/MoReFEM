// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <ostream>
#include <sstream>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

#include "FiniteElement/QuadratureRules/Instantiation/Hexahedron.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Point.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Quadrangle.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Segment.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Tetrahedron.hpp"
#include "FiniteElement/QuadratureRules/Instantiation/Triangle.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        QuadratureRulePerTopology::storage_type DetermineDefaultQuadratureRule(std::size_t degree_of_exactness,
                                                                               Eigen::Index max_shape_function_order);


    } // namespace


    // clang-format off
    QuadratureRulePerTopology::QuadratureRulePerTopology(storage_type&& quadrature_rule_per_topology)
    : quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {
#ifndef NDEBUG
            decltype(auto) list = GetRulePerTopology();
            for (const auto& pair : list)
                assert(pair.second != nullptr);
#endif // NDEBUG
    }
    // clang-format on


    QuadratureRulePerTopology::QuadratureRulePerTopology(std::size_t degree_of_exactness,
                                                         Eigen::Index shape_function_order)
    : self(DetermineDefaultQuadratureRule(degree_of_exactness, shape_function_order))
    { }


    void QuadratureRulePerTopology::Print(std::ostream& stream) const noexcept
    {
        const auto& quadrature_rule_per_topology = GetRulePerTopology();

        stream << "List of registered quadrature rules:" << '\n';

        for (const auto& topology_content : quadrature_rule_per_topology)
        {
            const auto& quadrature_rule_ptr = topology_content.second;
            assert(!(!quadrature_rule_ptr));

            stream << "\t - " << quadrature_rule_ptr->GetName() << '\n';
        }
    }


    const QuadratureRule& QuadratureRulePerTopology::GetRule(TopologyNS::Type topology) const
    {
        decltype(auto) rule_list = GetRulePerTopology();
        const auto it = rule_list.find(topology);

        if (it == rule_list.cend())
        {
            std::ostringstream oconv;

            oconv << "You require a quadrature rule for topology " << topology << "; no rule was given for it.";
            throw Exception(oconv.str());
        }

        assert(!(!it->second));
        return *(it->second);
    }


    namespace // anonymous
    {


        QuadratureRulePerTopology::storage_type DetermineDefaultQuadratureRule(std::size_t degree_of_exactness,
                                                                               Eigen::Index max_shape_function_order)
        {
            return { { { TopologyNS::Type::point,
                         QuadratureNS::Point::GetRuleFromDegreeOfExactness(degree_of_exactness) },
                       { TopologyNS::Type::segment,
                         QuadratureNS::Segment::GetRuleFromShapeFunctionOrder(max_shape_function_order) },
                       { TopologyNS::Type::triangle,
                         QuadratureNS::Triangle::GetRuleFromDegreeOfExactness(degree_of_exactness) },
                       { TopologyNS::Type::quadrangle,
                         QuadratureNS::Quadrangle::GetRuleFromShapeFunctionOrder(max_shape_function_order) },
                       { TopologyNS::Type::tetrahedron,
                         QuadratureNS::Tetrahedron::GetRuleFromDegreeOfExactness(degree_of_exactness) },
                       { TopologyNS::Type::hexahedron,
                         QuadratureNS::Hexahedron::GetRuleFromShapeFunctionOrder(max_shape_function_order) } } };
        }


    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
