// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULEPERTOPOLOGY_DOT_HPP_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULEPERTOPOLOGY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <memory>

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    /*!
     * \brief This class list the quadrature rule to use for each topology.
     *
     * It is intended to be used either in FEltSpace level or in GlobalVariationalOperator level (the latter supersedes
     * the former if specified; if not the operator takes the set of rules defines within its FEltSpace).
     */
    class QuadratureRulePerTopology
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = QuadratureRulePerTopology;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Convenient alias.
        using storage_type = std::map<TopologyNS::Type, QuadratureRule::const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] quadrature_rule_per_topology Quadrature rule to use for each topology.
         */
        explicit QuadratureRulePerTopology(storage_type&& quadrature_rule_per_topology);


        /*!
         * \brief Constructor to produce default choices for each topology.
         *
         * \param[in] degree_of_exactness Parameter used to determine rule to use for topologies that are built upon
         * PerDegreeOfExactness process.
         * \param[in] shape_function_order Parameter used to determine rule to use for topologies that are built upon
         * PerShapeFunctionOrder process.
         */
        explicit QuadratureRulePerTopology(std::size_t degree_of_exactness, Eigen::Index shape_function_order);


        //! Destructor.
        ~QuadratureRulePerTopology() = default;

        //! \copydoc doxygen_hide_copy_constructor
        QuadratureRulePerTopology(const QuadratureRulePerTopology& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        QuadratureRulePerTopology(QuadratureRulePerTopology&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        QuadratureRulePerTopology& operator=(const QuadratureRulePerTopology& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        QuadratureRulePerTopology& operator=(QuadratureRulePerTopology&& rhs) = delete;

        ///@}


        /*!
         * \brief Print the list of quadrature rules in storage.
         *
         * This method is mostly there for dev purposes.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Print(std::ostream& stream) const noexcept;


        //! Access to the list of quadrature rule to use for each topology.
        const storage_type& GetRulePerTopology() const noexcept;

        //! Return the rule to use with a given topology.
        //! \param[in] topology Topology for which the \a QuadratureRule is sought.
        const QuadratureRule& GetRule(TopologyNS::Type topology) const;


      private:
        /*!
         * \brief Quadrature rule to use for each topology.
         */
        const storage_type quadrature_rule_per_topology_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULEPERTOPOLOGY_DOT_HPP_
// *** MoReFEM end header guards *** < //
