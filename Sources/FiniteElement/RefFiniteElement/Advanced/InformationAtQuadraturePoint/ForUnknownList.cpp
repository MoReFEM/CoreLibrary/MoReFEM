// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <numeric>
#include <variant>

#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"
#include "Geometry/StrongType.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM::Advanced::InfosAtQuadPointNS
{


    namespace // anonymous
    {


        void FillPhiAndDerivates(const Advanced::RefFEltInLocalOperator& ref_felt,
                                 const QuadraturePoint& quadrature_point,
                                 Eigen::VectorXd& phi,
                                 ForUnknownList::matrix_type& dphi);


        auto InitComputeHelper(ForUnknownList* for_unknown_list_ptr,
                               const RefGeomElt& ref_geom_elt,
                               AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
            -> ::MoReFEM::Internal::InfosAtQuadPointNS::compute_local_felt_space_data_variant_type
        {
            Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

            assert(!(!for_unknown_list_ptr));
            const auto& for_unknown_list = *for_unknown_list_ptr;

            decltype(auto) mesh_dimension = for_unknown_list.GetMeshDimension();

            if (mesh_dimension == ref_geom_elt.GetDimension())
            {
                if (do_allocate_gradient_felt_phi == AllocateGradientFEltPhi::yes)
                    return ::MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::
                        SameDimensionWithGradient(mesh_dimension);

                return ::MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::SameDimensionNoGradient(
                    mesh_dimension);
            }

            switch (ref_geom_elt.GetDimension().Get())
            {
            case 2:
                return ::MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::LowerDimensionDimension2(
                    mesh_dimension);
            case 1:
                return ::MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::LowerDimensionDimension1(
                    mesh_dimension);
            case 0:
                return ::MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::LowerDimensionDimension0(
                    mesh_dimension);
            }

            assert(false);
            exit(EXIT_FAILURE);
        }

    } // namespace


    ForUnknownList ::ForUnknownList(const QuadraturePoint& quadrature_point,
                                    const RefGeomElt& ref_geom_elt,
                                    const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                                    const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                    AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
    : quadrature_point_{ quadrature_point }, mesh_dimension_{ static_cast<Eigen::Index>(mesh_dimension) },
      compute_data_helper_{ InitComputeHelper(this, ref_geom_elt, do_allocate_gradient_felt_phi) }
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

        InitRefGeometricShapeFunction(ref_geom_elt);
        InitRefFEltShapeFunction(ref_geom_elt, ref_felt_list);

        const auto& deriv_ref_phi_felt = GetGradientRefFEltPhi();

        decltype(auto) compute_data_helper = GetNonCstComputeHelper();

        if (std::holds_alternative<
                Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::SameDimensionWithGradient>(
                compute_data_helper))
        {
            std::get<Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::SameDimensionWithGradient>(
                compute_data_helper)
                .Resize(deriv_ref_phi_felt.rows(), deriv_ref_phi_felt.cols());
        }

        inverse_jacobian_ = ::MoReFEM::Wrappers::EigenNS::InitDimensionMatrixVariant(mesh_dimension.Get());

        Ncomponent_sequence_.resize(static_cast<std::size_t>(ref_geom_elt.GetDimension().Get()));
        std::iota(Ncomponent_sequence_.begin(), Ncomponent_sequence_.end(), 0);
    }


    void ForUnknownList::InitRefGeometricShapeFunction(const RefGeomElt& ref_geom_elt)
    {
        const auto dimension =
            ::MoReFEM::GeometryNS::dimension_type{ static_cast<Eigen::Index>(ref_geom_elt.GetDimension()) };
        const auto& quad_pt = GetQuadraturePoint();

        auto& phi_ref_geo = GetNonCstRefGeometricPhi();
        auto& deriv_phi_ref_geo = GetNonCstGradientRefGeometricPhi();

        const LocalNodeNS::index_type Nnode{ static_cast<LocalNodeNS::index_type::underlying_type>(
            ref_geom_elt.Ncoords()) };

        phi_ref_geo.resize(Nnode.Get());
        deriv_phi_ref_geo.resize(Nnode.Get(), dimension.Get());

        for (LocalNodeNS::index_type local_node_index{}; local_node_index < Nnode; ++local_node_index)
        {
            phi_ref_geo(local_node_index.Get()) = ref_geom_elt.ShapeFunction(local_node_index, quad_pt);

            for (::MoReFEM::GeometryNS::dimension_type component{}; component < dimension; ++component)
            {
                deriv_phi_ref_geo(local_node_index.Get(), component.Get()) =
                    ref_geom_elt.FirstDerivateShapeFunction(local_node_index, component, quad_pt);
            }
        }
    }


    void ForUnknownList ::InitRefFEltShapeFunction(
        const RefGeomElt& ref_geom_elt,
        const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list)
    {
        const auto Nnode = std::accumulate(
            ref_felt_list.cbegin(),
            ref_felt_list.cend(),
            LocalNodeNS::index_type{},
            [](LocalNodeNS::index_type sum, const Advanced::RefFEltInLocalOperator::const_unique_ptr& ref_felt_ptr)
            {
                if (!(!ref_felt_ptr))
                    return sum + ref_felt_ptr->Nnode();
                else
                    return sum;
            });

        assert(Nnode > LocalNodeNS::index_type{ 0 });

        const auto dimension = static_cast<Eigen::Index>(ref_geom_elt.GetDimension());

        auto& phi_ref_felt = GetNonCstRefFEltPhi();
        auto& deriv_phi_ref_felt = GetNonCstGradientRefFEltPhi();

        phi_ref_felt.resize(Nnode.Get());
        deriv_phi_ref_felt.resize(Nnode.Get(), dimension);

        const auto& quad_pt = GetQuadraturePoint();

        for (const auto& ref_felt_ptr : ref_felt_list)
        {
            assert(!(!ref_felt_ptr));
            FillPhiAndDerivates(*ref_felt_ptr, quad_pt, phi_ref_felt, deriv_phi_ref_felt);
        }
    }


    void ForUnknownList::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
    {


        GetNonCstDeterminant() = std::visit(
            [this, &local_felt_space](auto& variant)
            {
                return variant.Compute(local_felt_space, *this);
            },
            GetNonCstComputeHelper());
    }


    namespace // anonymous
    {


        void FillPhiAndDerivates(const Advanced::RefFEltInLocalOperator& ref_felt,
                                 const QuadraturePoint& quadrature_point,
                                 Eigen::VectorXd& phi,
                                 ForUnknownList::matrix_type& dphi)
        {
            const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();

            const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ basic_ref_felt.GetTopologyDimension() };
            const auto Nnode = LocalNodeNS::index_type{ ref_felt.Nnode() };

            // It's this line that make the difference between the two reference finite elements (if there
            // are 2 of course...)
            decltype(auto) node_position_list_in_matrix = ref_felt.GetLocalNodeIndexList();

            assert(!node_position_list_in_matrix.empty());
            assert(static_cast<Eigen::Index>(node_position_list_in_matrix.size()) == Nnode.Get());

            for (LocalNodeNS::index_type local_node_index{}; local_node_index < Nnode; ++local_node_index)
            {
                const auto node_position_in_matrix =
                    node_position_list_in_matrix[static_cast<std::size_t>(local_node_index.Get())];

                phi(node_position_in_matrix) = basic_ref_felt.ShapeFunction(local_node_index, quadrature_point);

                for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent; ++component)
                {
                    dphi(node_position_in_matrix, component.Get()) =
                        basic_ref_felt.FirstDerivateShapeFunction(local_node_index, component, quadrature_point);
                }
            }
        }


    } // namespace


} // namespace MoReFEM::Advanced::InfosAtQuadPointNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
