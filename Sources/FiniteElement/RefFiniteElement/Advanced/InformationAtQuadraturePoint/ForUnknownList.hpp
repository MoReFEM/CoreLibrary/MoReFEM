// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <limits>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
#include "Utilities/LinearAlgebra/StrongType.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


// IWYU pragma: begin_exports
#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/Enum.hpp"
#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/ComputeLocalFEltSpaceDataVariant.hpp"
// IWYU pragma: end_exports

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS { class Generic; }
namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS { class SameDimensionWithGradient; }
namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS { class LowerDimensionDimension2; }
namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS { class LowerDimensionDimension1; }
namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS { class LowerDimensionDimension0; }
namespace MoReFEM::TestNS { struct AccessForUnknownList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::InfosAtQuadPointNS
{


    /*!
     * \brief Stores data related to a given quadrature point, such as the geometric and finite element shape
     * function values.
     *
     */
    class ForUnknownList final
    : public ::MoReFEM::Crtp::LocalMatrixStorage<ForUnknownList, 2UL, Advanced::RefFEltNS::gradient_matrix_type>,
      public ::MoReFEM::Crtp::LocalVectorStorage<ForUnknownList, 2UL, Eigen::VectorXd>
    {

      public:
        //! Alias.
        using self = ForUnknownList;

        //! Alias.
        using unique_ptr = std::unique_ptr<self>;

        //! Friendship to hekper class.
        friend class MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::SameDimensionWithGradient;

        //! Friendship to hekper class.
        friend class MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::LowerDimensionDimension2;

        //! Friendship to hekper class.
        friend class MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::LowerDimensionDimension1;

        //! Friendship to hekper class.
        friend class MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::LowerDimensionDimension0;

        //! Convenient alias.
        using matrix_type = Advanced::RefFEltNS::gradient_matrix_type;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = ::MoReFEM::Crtp::LocalMatrixStorage<self, 2UL, matrix_type>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = ::MoReFEM::Crtp::LocalVectorStorage<self, 2UL, Eigen::VectorXd>;

        static_assert(std::is_convertible<self*, vector_parent*>());

        //! Friendship to use only for tests.
        friend MoReFEM::TestNS::AccessForUnknownList;


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         *
         * \param[in] quadrature_point \a QuadraturePoint for which the helper data are computed.
         * \param[in] ref_geom_elt \a RefGeomElt considered.
         * \param[in] ref_felt_list List of reference finite elements (The brand adapted for usage in
         * \a LocalVariationalOperator).
         * \param[in] mesh_dimension Dimension of the mesh (so might be higher than dimension of \a
         * ref_geom_elt.
         */
        explicit ForUnknownList(const QuadraturePoint& quadrature_point,
                                const RefGeomElt& ref_geom_elt,
                                const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                                ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

        //! Destructor.
        ~ForUnknownList() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ForUnknownList(const ForUnknownList& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ForUnknownList(ForUnknownList&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ForUnknownList& operator=(const ForUnknownList& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ForUnknownList& operator=(ForUnknownList&& rhs) = delete;

        ///@}


      public:
        //! Geometric shape function value at each local node of the reference geometric element.
        const Eigen::VectorXd& GetRefGeometricPhi() const noexcept;

        //! Geometric shape function value at each local node of the reference geometric element.
        //! \param[in] local_node_index Position of the local node in the local storage.
        double GetRefGeometricPhi(Eigen::Index local_node_index) const;

        /*!
         * \brief First derivative of the geometric shape function value at each local node of the
         * reference geometric element.
         *
         * By convention rows stand for the local_node_index considered, and columns for the component against
         * which derivation is performed.
         *
         * \return First derivative of the geometric shape function value at each local node of the
         * reference geometric element.
         */
        const matrix_type& GetGradientRefGeometricPhi() const noexcept;

        //! Finite element shape function value at each local node of the reference finite element.
        const Eigen::VectorXd& GetRefFEltPhi() const noexcept;

        /*!
         * \brief Finite element shape function value at each local node of the reference finite element.
         *
         * This is in fact an alias of GetRefFEltPhi().
         *
         * \return Finite element shape function value at each local node of the reference finite element.
         */
        const Eigen::VectorXd& GetFEltPhi() const noexcept;

        //! Finite element shape function value at each local node of the reference finite element.
        //! \param[in] local_node_index Position of the local node in the local storage.
        double GetRefFEltPhi(Eigen::Index local_node_index) const;

        /*!
         * \brief First derivative of the finite element shape function value at each local node of the
         * reference finite element.
         *
         * By convention rows stand for the local_node_index considered, and columns for the component against
         * which derivation is performed.
         *
         * \return First derivative of the finite element shape function value at each local node of the
         * reference finite element.
         *
         * \attention This method should be called only for the variant \a SameDimensionWithGradient
         * - so far when \a GeomElt and \a Mesh don't share the same dimension we never need it. If you
         * require it at some point, feel free to [open an
         * issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues) or contact us at morefem-maint@inria.fr.
         *
         */
        const matrix_type& GetGradientRefFEltPhi() const noexcept;


        /*!
         * \brief Compute the attributes that depends on the \a local_felt_space.
         *
         * \param[in] local_felt_space \a LocalFEltSpace for which data are computed.
         */
        void ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space);


        /*!
         * \brief Get the first derivative of the finite element shape function value at each node of the
         * finite element (not the reference one).
         *
         * The value returns is the one matching the last LocalFEltSpace associated to the object
         * through ComputeLocalFEltSpaceData() method.
         *
         * By convention rows stand for the local_node_index considered, and columns for the component against
         * which derivation is performed.
         *
         * \return First derivative of the finite element shape function value at each node of the
         * finite element (not the reference one).
         */
        const matrix_type& GetGradientFEltPhi() const noexcept;


        /*!
         * \brief Get the determinant of the jacobian.
         *
         * The value returns is the one matching the last LocalFEltSpace associated to the object
         * through ComputeLocalFEltSpaceData() method.
         *
         * \return Determinant of the jacobian.
         */
        double GetJacobianDeterminant() const noexcept;


        /*!
         * \brief Get the absolute value of the determinant of the jacobian.
         *
         * The value returns is the one matching the last LocalFEltSpace associated to the object
         * through ComputeLocalFEltSpaceData() method.
         *
         * \return Absolute value of the determinant of the jacobian.
         *
         */
        double GetAbsoluteValueJacobianDeterminant() const;


        //! Returns the number of nodes.
        Eigen::Index Nnode() const noexcept;


        //! Returns the dimension of the mesh.
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;

        //! Get quadrature point information.
        const QuadraturePoint& GetQuadraturePoint() const noexcept;

        //! Whether the gradient of finite element is required or not.
        bool DoAllocateGradientFEltPhi() const;


      private:
        //! Init reference geometric shape functions.
        //! \param[in] ref_geom_elt \a RefGeomElt for which geometric shape function is computed.
        void InitRefGeometricShapeFunction(const RefGeomElt& ref_geom_elt);

        //! Init reference finite element shape functions.
        //! \param[in] ref_geom_elt \a RefGeomElt for which finite element shape function is computed.
        //! \param[in] ref_felt_list List of reference finite elements for which the shape functions are
        //! computed.
        void InitRefFEltShapeFunction(const RefGeomElt& ref_geom_elt,
                                      const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list);

        //! Non constant accessor to determinant.
        double& GetNonCstDeterminant() noexcept;

        //! Return the sequence [0, Ncomponent - 1].
        const std::vector<int>& GetComponentSequence() const noexcept;


      private:
        //! Non constant version of \a GetRefGeometricPhi().
        Eigen::VectorXd& GetNonCstRefGeometricPhi() noexcept;

        //! Non constant version of \a GetGradientRefGeometricPhi().
        matrix_type& GetNonCstGradientRefGeometricPhi() noexcept;

        //! Non constant version of \a GetRefFEltPhi().
        Eigen::VectorXd& GetNonCstRefFEltPhi() noexcept;

        //! Non constant version of \a GetGradientRefFEltPhi().
        matrix_type& GetNonCstGradientRefFEltPhi() noexcept;

        /*!
         * \brief Non constant access to the inverse of the jacobian matrix.
         *
         * \warning This one should not be used outside of the place the calculation occurs; all accessors
         * should hence be private.
         *
         * \return Inverse of the jacobian matrix.
         */
        ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant& GetNonCstInverseJacobian() noexcept;

        //! Non constant accessor to the helper class in cjhrge of computing  local data for a given \a LocalFEltSpace.
        ::MoReFEM::Internal::InfosAtQuadPointNS::compute_local_felt_space_data_variant_type&
        GetNonCstComputeHelper() noexcept;

        //! Accessor to the helper class in cjhrge of computing  local data for a given \a LocalFEltSpace.
        const ::MoReFEM::Internal::InfosAtQuadPointNS::compute_local_felt_space_data_variant_type&
        GetComputeHelper() const noexcept;

      private:
        /// \name Reference quantities
        ///@{

        //! Quadrature point considered in the class.
        const QuadraturePoint& quadrature_point_;

        //! Dimension of the mesh (might be higher than the one of finite elements here).
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;


        ///@}


      private:
        /// \name Quantities dependent of the actual GeometricElt considered.
        ///@{

        //! Determinant of the jacobian matrix.
        double determinant_ = std::numeric_limits<double>::max();


        ///@}

        /*!
         * \brief Helper class used to compute local data for a given \a LocalFEltSpace.
         *
         * The point of this indirection is to leverage a visitor over the different configurations (is the geometric
         * element of the same dimension as the mesh? Do we need to compute local gradients?)
         */
        ::MoReFEM::Internal::InfosAtQuadPointNS::compute_local_felt_space_data_variant_type compute_data_helper_;


        /*!
         * \brief Return a sequence from 0 to Ncomponent - 1.
         *
         * \internal <b><tt>[internal]</tt></b> Stored here to keep generating it at each call of dPhi().
         * \endinternal
         */
        std::vector<int> Ncomponent_sequence_;


        /*!
         * \brief Inverse of the jacobian matrix.
         *
         * \internal There is a data attribute for this as we strive to avoid at all cost memory allocation during the crushing of numbers...
         */
        ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant inverse_jacobian_;


      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{


        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t { deriv_phi_ref_geo = 0, deriv_phi_ref_felt };


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t { phi_ref_geo = 0, phi_ref_felt = 1 };

        ///@}
    };


} // namespace MoReFEM::Advanced::InfosAtQuadPointNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
