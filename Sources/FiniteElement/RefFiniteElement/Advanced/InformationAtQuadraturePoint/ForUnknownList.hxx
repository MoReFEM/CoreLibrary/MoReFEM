// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Utilities/Containers/EnumClass.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM::Advanced::GeomEltNS { class ComputeJacobian; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::InfosAtQuadPointNS
{


    inline const QuadraturePoint& ForUnknownList::GetQuadraturePoint() const noexcept
    {
        return quadrature_point_;
    }


    inline auto ForUnknownList::GetRefGeometricPhi() const noexcept -> const Eigen::VectorXd&
    {
        return GetLocalVector<EnumUnderlyingType(LocalVectorIndex::phi_ref_geo)>();
    }


    inline auto ForUnknownList::GetNonCstRefGeometricPhi() noexcept -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetRefGeometricPhi());
    }


    inline double ForUnknownList::GetRefGeometricPhi(Eigen::Index local_node_index) const
    {
        assert(local_node_index < GetRefGeometricPhi().size());
        return GetRefGeometricPhi()(local_node_index);
    }


    inline auto ForUnknownList::GetGradientRefGeometricPhi() const noexcept -> const matrix_type&
    {
        return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_ref_geo)>();
    }


    inline auto ForUnknownList::GetNonCstGradientRefGeometricPhi() noexcept -> matrix_type&
    {
        return const_cast<matrix_type&>(GetGradientRefGeometricPhi());
    }


    inline auto ForUnknownList::GetRefFEltPhi() const noexcept -> const Eigen::VectorXd&
    {
        return GetLocalVector<EnumUnderlyingType(LocalVectorIndex::phi_ref_felt)>();
    }


    inline auto ForUnknownList::GetFEltPhi() const noexcept -> const Eigen::VectorXd&
    {
        return GetRefFEltPhi();
    }


    inline auto ForUnknownList::GetNonCstRefFEltPhi() noexcept -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetRefFEltPhi());
    }


    inline double ForUnknownList::GetRefFEltPhi(Eigen::Index local_node_index) const
    {
        assert(local_node_index < GetRefFEltPhi().size());
        return GetRefFEltPhi()(local_node_index);
    }


    inline auto ForUnknownList::GetGradientRefFEltPhi() const noexcept -> const matrix_type&
    {
        return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_ref_felt)>();
    }


    inline auto ForUnknownList::GetNonCstGradientRefFEltPhi() noexcept -> matrix_type&
    {
        return const_cast<matrix_type&>(GetGradientRefFEltPhi());
    }


    inline double& ForUnknownList::GetNonCstDeterminant() noexcept
    {
        return determinant_;
    }


    inline auto ForUnknownList ::GetGradientFEltPhi() const noexcept -> const matrix_type&
    {
        decltype(auto) compute_data_helper = GetComputeHelper();
        using same = ::MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::SameDimensionWithGradient;
        assert("Make sure AllocateGradientFEltPhi::yes was given to the local operator constructor!"
               "If that was the case, check if mesh dimension and geometric element dimension are the same."
               "If not, it is not yet implemented - feel free to contact us at morefem-maint@inria.fr."
               && std::holds_alternative<same>(compute_data_helper));

        return std::get<same>(compute_data_helper).GetGradientFEltPhi();
    }


    inline const std::vector<int>& ForUnknownList::GetComponentSequence() const noexcept
    {
        return Ncomponent_sequence_;
    }


    inline double ForUnknownList::GetJacobianDeterminant() const noexcept
    {
        assert(!NumericNS::AreEqual(std::numeric_limits<double>::max(), determinant_));
        return determinant_;
    }


    inline double ForUnknownList::GetAbsoluteValueJacobianDeterminant() const
    {
        return std::fabs(GetJacobianDeterminant());
    }


    inline auto ForUnknownList::GetMeshDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return mesh_dimension_;
    }


    inline auto ForUnknownList::GetNonCstInverseJacobian() noexcept
        -> ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant&
    {
        return inverse_jacobian_;
    }


    inline Eigen::Index ForUnknownList::Nnode() const noexcept
    {
        return GetFEltPhi().size();
    }


    inline auto ForUnknownList::GetComputeHelper() const noexcept
        -> const ::MoReFEM::Internal::InfosAtQuadPointNS::compute_local_felt_space_data_variant_type&
    {
        return compute_data_helper_;
    }

    inline auto ForUnknownList::GetNonCstComputeHelper() noexcept
        -> ::MoReFEM::Internal::InfosAtQuadPointNS::compute_local_felt_space_data_variant_type&
    {
        return const_cast<::MoReFEM::Internal::InfosAtQuadPointNS::compute_local_felt_space_data_variant_type&>(
            GetComputeHelper());
    }


} // namespace MoReFEM::Advanced::InfosAtQuadPointNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
