// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <numeric>
#include <vector>

#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"

#include "Utilities/Containers/EnumClass.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/RefGeometricElt/Advanced/LocalNode/Alias.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp"


namespace MoReFEM::Advanced
{


    RefFEltInLocalOperator::RefFEltInLocalOperator(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                                   const LocalNodeNS::index_type index_first_node_in_elementary_data,
                                                   const Eigen::Index index_first_dof_in_elementary_data)
    : ref_felt_(ref_felt), index_first_dof_(index_first_dof_in_elementary_data),
      Ndof_per_component_(ref_felt.Ndof() / ref_felt.Ncomponent().Get())
    {
        const auto Ndof = ref_felt.Ndof();
        const auto Ncomponent = ref_felt.Ncomponent();

        assert(Ndof % Ncomponent.Get() == 0);
        const auto Ndof_per_component = NdofPerComponent();

        const auto Nnode = ref_felt.Nnode();

        local_node_index_list_.resize(static_cast<std::size_t>(Nnode.Get()));
        std::iota(
            local_node_index_list_.begin(), local_node_index_list_.end(), index_first_node_in_elementary_data.Get());

        local_dof_index_list_.resize(static_cast<std::size_t>(Ndof));
        std::iota(local_dof_index_list_.begin(), local_dof_index_list_.end(), index_first_dof_in_elementary_data);

        std::vector<Eigen::Index> buf(static_cast<std::size_t>(Ndof_per_component));

        for (::MoReFEM::GeometryNS::dimension_type i{ 0 }; i < Ncomponent; ++i)
        {
            std::iota(buf.begin(), buf.end(), index_first_dof_in_elementary_data + i.Get() * Ndof_per_component);
            local_dof_index_list_per_component_.push_back(buf);
        }

        const auto& ref_felt_space_dim = ref_felt.GetFEltSpaceDimension();

        matrix_parent::InitLocalMatrixStorage({ { { Nnode.Get(), ref_felt_space_dim.Get() } } });

        vector_parent::InitLocalVectorStorage({ { Nnode.Get(), // to store the vector given in return type.
                                                  Nnode.Get(),
                                                  Nnode.Get() } });
    }


    auto RefFEltInLocalOperator::ExtractSubMatrix(const Advanced::RefFEltNS::gradient_matrix_type& full_matrix) const
        -> const Advanced::RefFEltNS::gradient_matrix_type&
    {
        auto& ret = matrix_parent::GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::extract_sub_matrix_helper)>(); // mutable...

        assert(full_matrix.rows() >= ret.rows());
        assert(full_matrix.cols() >= ret.cols());
        assert(GetFEltSpaceDimension().Get() == ret.cols());
        decltype(auto) row_range = GetLocalNodeIndexList();
        assert(Nnode().Get() == static_cast<Eigen::Index>(row_range.size()));

        ret.noalias() = full_matrix(row_range, Eigen::all);
        return ret;
    }


    auto RefFEltInLocalOperator::ExtractSubVector(const Eigen::VectorXd& full_vector) const -> const Eigen::VectorXd&
    {
        auto& ret = vector_parent::GetLocalVector<EnumUnderlyingType(
            LocalVectorIndex::extract_sub_vector_helper)>(); // mutable...

        assert(full_vector.rows() >= ret.rows());

        const auto& selection = GetLocalNodeIndexList();
        assert(static_cast<Eigen::Index>(selection.size()) == ret.size());
        ret.noalias() = full_vector(selection);

        return ret;
    }


    auto RefFEltInLocalOperator::ComputeGradientDisplacementMatrix(
        const Advanced::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
        const Eigen::VectorXd& local_displacement,
        ::MoReFEM::GeometryNS::dimension_type mesh_dimension) const -> ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>
    {
        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> ret;
        ret.resize(mesh_dimension.Get(), mesh_dimension.Get());

        const auto& dphi = quad_pt_unknown_data.GetGradientFEltPhi();
        const auto& dphi_of_unknown = ExtractSubMatrix(dphi);
        const auto Nnode = this->Nnode();

        assert(Nnode == LocalNodeNS::index_type{ dphi_of_unknown.rows() });

        // These two are mutable - data attributes that only exist to avoid reallocating pointlessly memory.
        // Values should not be used anywhere - they are only helpers for a given computation.
        decltype(auto) displacement_derivate_component =
            vector_parent::GetLocalVector<EnumUnderlyingType(LocalVectorIndex::helper_1)>();
        decltype(auto) component_dphi_column =
            vector_parent::GetLocalVector<EnumUnderlyingType(LocalVectorIndex::helper_2)>();

        for (auto derivation_component = ::MoReFEM::GeometryNS::dimension_type{}; derivation_component < mesh_dimension;
             ++derivation_component)
        {
            displacement_derivate_component.noalias() =
                local_displacement(Eigen::seqN(derivation_component.Get() * Nnode.Get(), Nnode.Get()));

            for (auto component = ::MoReFEM::GeometryNS::dimension_type{}; component < mesh_dimension; ++component)
            {
                component_dphi_column.noalias() = dphi_of_unknown(Eigen::all, Eigen::seqN(component.Get(), 1));

                // NOLINTBEGIN(bugprone-easily-swappable-parameters,readability-suspicious-call-argument)
                ret(derivation_component.Get(), component.Get()) =
                    component_dphi_column.transpose() * displacement_derivate_component;
                // NOLINTEND(bugprone-easily-swappable-parameters,readability-suspicious-call-argument)
            }
        }

        return ret;
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
