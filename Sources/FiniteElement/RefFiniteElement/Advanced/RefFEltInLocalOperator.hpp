// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_REFFELTINLOCALOPERATOR_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_REFFELTINLOCALOPERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits>
#include <vector>

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/Alias.hpp"              // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }
namespace MoReFEM::Internal::LocalVariationalOperatorNS { class ElementaryDataImpl; }
namespace MoReFEM::Advanced::InfosAtQuadPointNS { class ForUnknownList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    /*!
     * \brief Reference finite element a standard developer should use.
     *
     * \internal <b><tt>[internal]</tt></b> There are lower-level classes in Internal::RefFEltNS namespace that
     * should not be used directly; current class is circumscribed to one operator and should provide all relevant
     * information a developer might need when implementing a new operator. The only exception is if a new type of
     * reference finite element must be defined; in this case a child class of Internal::RefEltNS::BasicRefFElt
     * should be created (see this class for much more details about how to do it).
     * \endinternal
     *
     */
    class RefFEltInLocalOperator final
    : public ::MoReFEM::Crtp::
          LocalMatrixStorage<RefFEltInLocalOperator, 1UL, Advanced::RefFEltNS::gradient_matrix_type>,
      public ::MoReFEM::Crtp::LocalVectorStorage<RefFEltInLocalOperator, 3UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = RefFEltInLocalOperator;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to vector of unique pointers.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;

        //! Friendship to ElementaryDataImpl, which should be the only class able to instantiate
        //! RefFEltInLocalOperator.
        friend class Internal::LocalVariationalOperatorNS::ElementaryDataImpl;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = ::MoReFEM::Crtp::LocalMatrixStorage<self, 1UL, Advanced::RefFEltNS::gradient_matrix_type>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = ::MoReFEM::Crtp::LocalVectorStorage<self, 3UL>;

        static_assert(std::is_convertible<self*, vector_parent*>());

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] ref_felt The RefFEltInFEltSpace object issued by the FEltSpace which will be embedded
         * in current class.
         * \param[in] index_first_node_in_elementary_data Index of the first node related to the finite
         * element type in the elementary matrices.
         * \param[in] index_first_dof_in_elementary_data Same as \a index_first_node_in_elementary_data for the dof
         * indexes.
         *
         * \internal <b><tt>[internal]</tt></b> Usually I am weary of such low-level constructor arguments, but in
         * this case it is fine: the user should never have to call this constructor... All the required instances
         * of present class are built in ElementaryDataImpl constructor, which is itself in Internal namespace.
         * \endinternal
         */
        explicit RefFEltInLocalOperator(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt,
                                        LocalNodeNS::index_type index_first_node_in_elementary_data,
                                        Eigen::Index index_first_dof_in_elementary_data);

        //! Destructor.
        ~RefFEltInLocalOperator() = default;

        //! \copydoc doxygen_hide_copy_constructor
        RefFEltInLocalOperator(const RefFEltInLocalOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        RefFEltInLocalOperator(RefFEltInLocalOperator&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        RefFEltInLocalOperator& operator=(const RefFEltInLocalOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RefFEltInLocalOperator& operator=(RefFEltInLocalOperator&& rhs) = delete;


        ///@}

        //! Number of nodes.
        LocalNodeNS::index_type Nnode() const noexcept;

        //! Number of dofs.
        Eigen::Index Ndof() const noexcept;

        //! Return the related reference finite element.
        const Internal::RefFEltNS::BasicRefFElt& GetBasicRefFElt() const noexcept;

        //! Return the related unknown/numbering subset.
        const ExtendedUnknown& GetExtendedUnknown() const noexcept;


      public:
        /*!
         * \brief Extract the part of \a full_matrix that is related to \a ref_felt.
         *
         * \internal <b><tt>[internal]</tt></b> The reference points to a mutable data attribute of
         * RefFEltInLocalOperator.
         * \endinternal
         *
         * \param[in] full_matrix The matrix from which a block is extracted.
         *
         * \return Reference to the extracted sub-matrix.
         */
        const Advanced::RefFEltNS::gradient_matrix_type&
        ExtractSubMatrix(const Advanced::RefFEltNS::gradient_matrix_type& full_matrix) const;

        /*!
         * \brief Extract the part of \a full_vector that is related to \a ref_felt.
         *
         * \internal <b><tt>[internal]</tt></b> The reference points to a mutable data attribute of
         * RefFEltInLocalOperator.
         * \endinternal
         *
         * \param[in] full_vector The vector from which a block is extracted.
         *
         * \return Reference to the extracted sub-vector.
         */
        const Eigen::VectorXd& ExtractSubVector(const Eigen::VectorXd& full_vector) const;


        /*!
         * \brief Compute the gradient matrix related to a given local displacement.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         * \attention This method clearly expects to work within an operator which acts only upon a solid
         * displacement unknown. If your operator actually consider others unknown, you should rather create another
         * operator (typically a \a GlobalParameterOperator) which acts only on solid displacement and then call the
         * resulting value in your own operator (see \a UpdateCauchyGreenTensor use for an illustration).
         *
         * \param[in] local_displacement Displacement at the dofs of the finite
         * element under consideration.
         * \param[in] mesh_dimension Dimension of the mesh.
         *
         * \return gradient_matrix A square matrix which size is dimension of the mesh. It is allocated on the stack.
         */
        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> ComputeGradientDisplacementMatrix(
            const ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
            const Eigen::VectorXd& local_displacement,
            ::MoReFEM::GeometryNS::dimension_type mesh_dimension) const;

      public:
        //! Returns the number of components.
        ::MoReFEM::GeometryNS::dimension_type Ncomponent() const noexcept;

        //! Access to the dimension of the mesg in which current finite element is built.
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;

        //! Access to the dimension of the finite element space for which current reference finite element is built.
        ::MoReFEM::GeometryNS::dimension_type GetFEltSpaceDimension() const noexcept;


      public:
        /*!
         * \brief Return the (contiguous) list of node indexes related to \a RefFEltInFEltSpace in \a ElementaryData
         * matrices or vectors.
         *
         * Please treat this method as private and not use it: no class should required this save \a ElementaryDataImpl
         * and the few free-functions that are used to build it.
         *
         * \return Reference to the list of local node indexes.
         */
        const std::vector<Eigen::Index>& GetLocalNodeIndexList() const noexcept;

        //! Returns the index of the first dof of current ref elt for \a component in elementary data.
        //! \param[in] component Component used as filter.
        Eigen::Index GetIndexFirstDofInElementaryData(::MoReFEM::GeometryNS::dimension_type component) const noexcept;

      private:
        /*!
         * \brief Return the (contiguous) list of dof indexes related to \a RefFEltInFEltSpace in \a ElementaryData
         * matrices or vectors; this method returns only a subset related to a given component of the unknown.
         *
         * Please treat this method as private and not use it: no class should required this save ElementaryDataImpl
         * and the few free-functions that are used to build it.
         *
         * \param[in] component_index Index of the component for which the list is sought.
         *
         * \return Reference to the list of local node indexes associated to \a component_index -th component.
         */
        const std::vector<Eigen::Index>&
        GetLocalDofIndexList(::MoReFEM::GeometryNS::dimension_type component_index) const noexcept;


        /*!
         * \brief Return the (contiguous) list of dof indexes related to RefFEltInFEltSpace in ElementaryData
         * matrices or vectors.
         *
         * Please treat this method as private and not use it: no class should required this save ElementaryDataImpl
         * and the few free-functions that are used to build it.
         *
         * \return Reference to the list of local dof indexes.
         */
        const std::vector<Eigen::Index>& GetLocalDofIndexList() const noexcept;

        //! Returns the index of the first dof of current ref elt in elementary data.
        Eigen::Index GetIndexFirstDofInElementaryData() const noexcept;


      private:
        //! Return the underlying RefFEltInFEltSpace, as it is stored in the FEltSpace (see class banner for more
        //! details).
        const Internal::RefFEltNS::RefFEltInFEltSpace& GetUnderlyingRefFElt() const noexcept;

        //! Returns the number of dofs per component.
        Eigen::Index NdofPerComponent() const noexcept;


      private:
        //! Underlying RefFEltInFEltSpace, as it is stored in the FEltSpace (see class banner for more details).
        const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt_;

        //! Index of the first dof of the ref finite element in elementary data.
        const Eigen::Index index_first_dof_;

        //! Number of dofs per component.
        const Eigen::Index Ndof_per_component_;

        /*!
         * \brief (Contiguous) list of node indexes related to RefFEltInFEltSpace in ElementaryData matrices
         * or vectors; this method returns only a subset related to a given component of the unknown.
         */
        std::vector<std::vector<Eigen::Index>> local_dof_index_list_per_component_;

        //! (Contiguous) list of dof indexes related to RefFEltInFEltSpace in ElementaryData matrices or vectors.
        std::vector<Eigen::Index> local_dof_index_list_;

        /*!
         * \brief Return the (contiguous) list of node indexes related to RefFEltInFEltSpace in ElementaryData
         * matrices or vectors; this method returns only a subset related to a given component of the unknown.
         *
         * \internal Morally the indexes are `LocalNodeNS::index_type`, but as we pass this data attribute
         * to Eigen::Matrix operator() we're better of keeping `int` (it is very low level and is not really used
         * besides the sub linear algebra extraction done in `ExtractSubMatrix()` and `ExtractSubVector()`
         */
        std::vector<Eigen::Index> local_node_index_list_;

        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{


        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t {
            extract_sub_matrix_helper =
                0, // to store the matrix given in return type. Size is Nnode x ref_felt_space_dim.
        };


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t {
            extract_sub_vector_helper = 0, // to store the vector given in return type. Size is Nnode.
            helper_1,                      // helper vector which size is Nnode. Don't use across methods.
            helper_2                       // helper vector which size is Nnode. Don't use across methods.
        };
        ///@}
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_REFFELTINLOCALOPERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
