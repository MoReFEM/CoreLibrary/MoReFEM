// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_REFFELTINLOCALOPERATOR_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_REFFELTINLOCALOPERATOR_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/StrongType.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    inline auto RefFEltInLocalOperator::Nnode() const noexcept -> LocalNodeNS::index_type
    {
        return GetUnderlyingRefFElt().Nnode();
    }


    inline Eigen::Index RefFEltInLocalOperator::Ndof() const noexcept
    {
        return GetUnderlyingRefFElt().Ndof();
    }


    inline const Internal::RefFEltNS::BasicRefFElt& RefFEltInLocalOperator::GetBasicRefFElt() const noexcept
    {
        return GetUnderlyingRefFElt().GetBasicRefFElt();
    }


    inline auto RefFEltInLocalOperator::GetLocalNodeIndexList() const noexcept -> const std::vector<Eigen::Index>&
    {
        return local_node_index_list_;
    }


    inline auto RefFEltInLocalOperator::GetLocalDofIndexList() const noexcept -> const std::vector<Eigen::Index>&
    {
        return local_dof_index_list_;
    }


    inline auto
    RefFEltInLocalOperator::GetLocalDofIndexList(::MoReFEM::GeometryNS::dimension_type component_index) const noexcept
        -> const std::vector<Eigen::Index>&
    {
        assert(static_cast<std::size_t>(component_index.Get()) < local_dof_index_list_per_component_.size());
        return local_dof_index_list_per_component_[static_cast<std::size_t>(component_index.Get())];
    }


    inline auto RefFEltInLocalOperator::GetUnderlyingRefFElt() const noexcept
        -> const Internal::RefFEltNS::RefFEltInFEltSpace&
    {
        return ref_felt_;
    }


    inline const ExtendedUnknown& RefFEltInLocalOperator::GetExtendedUnknown() const noexcept
    {
        return GetUnderlyingRefFElt().GetExtendedUnknown();
    }


    inline ::MoReFEM::GeometryNS::dimension_type RefFEltInLocalOperator::Ncomponent() const noexcept
    {
        return GetUnderlyingRefFElt().Ncomponent();
    }


    inline Eigen::Index RefFEltInLocalOperator::GetIndexFirstDofInElementaryData() const noexcept
    {
        assert(index_first_dof_ == GetLocalDofIndexList()[0]);
        return index_first_dof_;
    }


    inline Eigen::Index RefFEltInLocalOperator::GetIndexFirstDofInElementaryData(
        ::MoReFEM::GeometryNS::dimension_type component) const noexcept
    {
        assert(component < GetUnderlyingRefFElt().Ncomponent());
        return GetIndexFirstDofInElementaryData() + NdofPerComponent() * component.Get();
    }


    inline Eigen::Index RefFEltInLocalOperator::NdofPerComponent() const noexcept
    {
        return Ndof_per_component_;
    }


    inline auto RefFEltInLocalOperator::GetMeshDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return GetUnderlyingRefFElt().GetMeshDimension();
    }


    inline ::MoReFEM::GeometryNS::dimension_type RefFEltInLocalOperator::GetFEltSpaceDimension() const noexcept
    {
        return GetUnderlyingRefFElt().GetFEltSpaceDimension();
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_ADVANCED_REFFELTINLOCALOPERATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
