// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_HEXAHEDRONQ0_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_HEXAHEDRONQ0_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp" // IWYU pragma: keep (IWYU indecisive here)

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp"


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for HexahedronQ0.
     *
     * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
     * Advanced::RefGeomEltNS::TopologyNS::Hexahedron class.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */
    class HexahedronQ0
    : public Internal::RefFEltNS::GeometryBasedBasicRefFElt<Advanced::RefGeomEltNS::TopologyNS::Hexahedron,
                                                            Internal::ShapeFunctionNS::Order0,
                                                            InterfaceNS::Nature::none,
                                                            1UL>
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit HexahedronQ0() = default;

        //! Destructor.
        ~HexahedronQ0() override;

        //! \copydoc doxygen_hide_copy_constructor
        HexahedronQ0(const HexahedronQ0& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        HexahedronQ0(HexahedronQ0&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        HexahedronQ0& operator=(const HexahedronQ0& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        HexahedronQ0& operator=(HexahedronQ0&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_HEXAHEDRONQ0_DOT_HPP_
// *** MoReFEM end header guards *** < //
