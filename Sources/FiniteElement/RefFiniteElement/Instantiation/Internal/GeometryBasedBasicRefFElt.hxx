// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_GEOMETRYBASEDBASICREFFELT_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_GEOMETRYBASEDBASICREFFELT_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/Impl/ComputeCenterOfGravity.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        class ShapeFunctionT,
        ::MoReFEM::InterfaceNS::Nature HigherInterfaceConnectedT,
        std::size_t NdiscontinuousLocalNodeT
    >
    // clang-format on
    GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>::
        GeometryBasedBasicRefFElt()
    {
        static_assert(HigherInterfaceConnectedT != ::MoReFEM::InterfaceNS::Nature::undefined,
                      "HigherInterfaceConnectedT must be vertex, edge, face or volume.");

        assert(static_cast<int>(TopologyT::GetInteriorInterface()) > static_cast<int>(HigherInterfaceConnectedT)
               && "Interior must not be connected to neighbors!");

        Init<TopologyT>();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        class ShapeFunctionT,
        ::MoReFEM::InterfaceNS::Nature HigherInterfaceConnectedT,
        std::size_t NdiscontinuousLocalNodeT
    >
    // clang-format on
    Advanced::LocalNode::vector_const_shared_ptr
    GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>::
        ComputeLocalNodeList()
    {
        Advanced::LocalNode::vector_const_shared_ptr local_node_list;

        const auto& local_coords = TopologyT::GetQ1LocalCoordsList();
        assert(local_coords.size() == TopologyT::Nvertex);
        const std::size_t Nvertex = TopologyT::Nvertex;

        LocalNodeNS::index_type current_local_node_index{};

        using Topology = Advanced::InterfaceNS::LocalData<TopologyT>;

        if (static_cast<int>(HigherInterfaceConnectedT) >= static_cast<int>(::MoReFEM::InterfaceNS::Nature::vertex))
        {

            for (std::size_t i = 0UL; i < Nvertex; ++i)
            {
                auto&& local_interface = Topology::ComputeLocalVertexInterface(i);

                auto&& local_node_ptr = std::make_shared<Advanced::LocalNode>(
                    std::move(local_interface), current_local_node_index, local_coords[i]);

                ++current_local_node_index;

                local_node_list.emplace_back(std::move(local_node_ptr));
            }

            assert(current_local_node_index.Get() == Nvertex);

            if constexpr (::MoReFEM::Advanced::Concept::TopologyNS::with_edge<TopologyT>)
            {
                if (static_cast<int>(HigherInterfaceConnectedT)
                    >= static_cast<int>(::MoReFEM::InterfaceNS::Nature::edge))
                {
                    const std::size_t Nedge = TopologyT::Nedge;

                    for (std::size_t i = 0UL; i < Nedge; ++i)
                    {
                        // Compute here the coordinates of the middle of an edge.
                        const auto& vertex_on_edge_index_list = Topology::GetEdge(i);
                        assert(Topology::NverticeInEdge(i) == 2UL);

                        auto center_of_gravity =
                            Impl::ComputeCenterOfGravity<TopologyT>(local_coords, vertex_on_edge_index_list);

                        auto&& local_interface = Topology::ComputeLocalEdgeInterface(i);

                        auto&& local_node_ptr = std::make_shared<Advanced::LocalNode>(
                            std::move(local_interface), current_local_node_index++, center_of_gravity);

                        local_node_list.emplace_back(std::move(local_node_ptr));
                    }


                    assert(current_local_node_index.Get() == Nvertex + Nedge);

                    if constexpr (::MoReFEM::Advanced::Concept::TopologyNS::with_face<TopologyT>)
                    {
                        if (static_cast<int>(HigherInterfaceConnectedT)
                            >= static_cast<int>(::MoReFEM::InterfaceNS::Nature::face))
                        {
                            const std::size_t Nface = TopologyT::Nface;

                            for (std::size_t i = 0UL; i < Nface; ++i)
                            {
                                // Compute here the coordinates of the center of gravity of a face.
                                const auto& vertex_on_face_index_list = Topology::GetFace(i);

                                auto center_of_gravity =
                                    Impl::ComputeCenterOfGravity<TopologyT>(local_coords, vertex_on_face_index_list);

                                auto&& local_interface = Topology::ComputeLocalFaceInterface(i);


                                auto&& local_node_ptr = std::make_shared<Advanced::LocalNode>(
                                    std::move(local_interface), current_local_node_index++, center_of_gravity);

                                local_node_list.emplace_back(std::move(local_node_ptr));
                            }

                            assert(current_local_node_index.Get() == Nvertex + Nedge + Nface);
                        }
                    }
                }
            }
        }


        // Now add the discontinuous local nodes (if any).
        {
            if constexpr (NdiscontinuousLocalNodeT > 0)
            {
                const auto center_of_gravity = ComputeCenterOfGravity(local_coords);

                for (std::size_t i = 0; i < NdiscontinuousLocalNodeT; ++i)
                {
                    auto&& local_interface = Topology::ComputeLocalInteriorInterface();

                    auto&& local_node_ptr = std::make_shared<Advanced::LocalNode>(
                        std::move(local_interface), current_local_node_index++, center_of_gravity);

                    local_node_list.emplace_back(std::move(local_node_ptr));
                }
            }
        }


        return local_node_list;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        class ShapeFunctionT,
        ::MoReFEM::InterfaceNS::Nature HigherInterfaceConnectedT,
        std::size_t NdiscontinuousLocalNodeT
    >
    // clang-format on
    inline double
    GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>::
        ShapeFunction(LocalNodeNS::index_type local_node_index, const LocalCoords& local_coords) const
    {
        return ShapeFunctionT::ShapeFunction(local_node_index, local_coords);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        class ShapeFunctionT,
        ::MoReFEM::InterfaceNS::Nature HigherInterfaceConnectedT,
        std::size_t NdiscontinuousLocalNodeT
    >
    // clang-format on
    inline double
    GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>::
        FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                   ::MoReFEM::GeometryNS::dimension_type component,
                                   const LocalCoords& local_coords) const
    {
        return ShapeFunctionT::FirstDerivateShapeFunction(local_node_index, component, local_coords);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        class ShapeFunctionT,
        ::MoReFEM::InterfaceNS::Nature HigherInterfaceConnectedT,
        std::size_t NdiscontinuousLocalNodeT
    >
    // clang-format on
    inline Eigen::Index
    GeometryBasedBasicRefFElt<TopologyT, ShapeFunctionT, HigherInterfaceConnectedT, NdiscontinuousLocalNodeT>::
        GetOrder() const noexcept
    {
        return ShapeFunctionT::Order;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_GEOMETRYBASEDBASICREFFELT_DOT_HXX_
// *** MoReFEM end header guards *** < //
