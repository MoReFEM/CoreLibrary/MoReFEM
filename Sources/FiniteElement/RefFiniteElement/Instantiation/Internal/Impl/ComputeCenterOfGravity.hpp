// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_IMPL_COMPUTECENTEROFGRAVITY_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_IMPL_COMPUTECENTEROFGRAVITY_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::RefFEltNS::Impl
{


    /*!
     * \brief Compute the center of gravity for a given geometric element from its vertices.
     *
     * This is used to give an approximate spatial position for a \a LocalNode.
     *
     * \param[in] vertex_list List of vertex
     * \param[in] vertex_on_interface_index_list List given by methods such as \a TopologyT::GetEdge(index) or
     * \a TopologyT::GetFace(index)
     *
     * \return Approximate position of the center of gravity of the geometric element considered.
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT, class InterfaceContentT>
    LocalCoords ComputeCenterOfGravity(const std::vector<LocalCoords>& vertex_list,
                                       const InterfaceContentT& vertex_on_interface_index_list);


} // namespace MoReFEM::Internal::RefFEltNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/Impl/ComputeCenterOfGravity.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_IMPL_COMPUTECENTEROFGRAVITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
