// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_IMPL_COMPUTECENTEROFGRAVITY_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_IMPL_COMPUTECENTEROFGRAVITY_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Internal/Impl/ComputeCenterOfGravity.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::RefFEltNS::Impl
{


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT, class InterfaceContentT>
    LocalCoords ComputeCenterOfGravity(const std::vector<LocalCoords>& vertex_list,
                                       const InterfaceContentT& vertex_on_interface_index_list)
    {
        if constexpr (std::is_same_v<InterfaceContentT, std::false_type>)
        {
            assert(false
                   && "Should never be called in runtime in this case; just there to prevent compilation failure!");
            exit(EXIT_FAILURE);
        } else
        {
#ifndef NDEBUG
            const std::size_t Nvertex = TopologyT::Nvertex;

            assert(vertex_list.size() == Nvertex);
            assert(std::all_of(vertex_on_interface_index_list.cbegin(),
                               vertex_on_interface_index_list.cend(),
                               [](std::size_t index)
                               {
                                   return index < Nvertex;
                               }));
#endif // NDEBUG

            std::vector<LocalCoords> vertex_on_interface;
            vertex_on_interface.reserve(vertex_on_interface_index_list.size());

            for (std::size_t vertex_in_interface_index : vertex_on_interface_index_list)
                vertex_on_interface.push_back(vertex_list[vertex_in_interface_index]);

            return ComputeCenterOfGravity(vertex_on_interface);
        }
    }


} // namespace MoReFEM::Internal::RefFEltNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_IMPL_COMPUTECENTEROFGRAVITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
