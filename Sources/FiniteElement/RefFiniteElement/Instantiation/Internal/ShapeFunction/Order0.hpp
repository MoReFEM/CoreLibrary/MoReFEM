// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_ORDER0_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_ORDER0_DOT_HPP_
// *** MoReFEM header guards *** < //

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::ShapeFunctionNS
{


    /*!
     * \brief Define the shape function and its derivate for order 0 (P0, Q0).
     */
    struct Order0
    {


        //! Return value of the order.
        enum { Order = 0 };


        /*!
         *
         * \copydoc doxygen_hide_shape_function
         *
         * For this specific case, method always returns 1. regardless of arguments values.
         */
        static double ShapeFunction(LocalNodeNS::index_type local_node_index, const LocalCoords& local_coords);


        /*!
         * \copydoc doxygen_hide_first_derivate_shape_function
         *
         * For this specific case, method always returns 0. regardless of arguments values.
         */
        static double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                 ::MoReFEM::GeometryNS::dimension_type component,
                                                 const LocalCoords& local_coords);
    };


} // namespace MoReFEM::Internal::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_ORDER0_DOT_HPP_
// *** MoReFEM end header guards *** < //
