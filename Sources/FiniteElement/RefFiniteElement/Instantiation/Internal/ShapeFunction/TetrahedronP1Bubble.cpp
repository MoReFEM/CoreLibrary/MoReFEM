// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TetrahedronP1Bubble.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Internal::ShapeFunctionNS
{


    namespace // anonymous
    {


        std::array<TetrahedronP1Bubble::shape_function_type, 5> ComputeShapeFunctionList();


        std::array<TetrahedronP1Bubble::shape_function_type, 15> ComputeGradShapeFunctionList();


    } // namespace


    const std::array<TetrahedronP1Bubble::shape_function_type, 5>& TetrahedronP1Bubble::ShapeFunctionList()
    {
        static auto ret = ComputeShapeFunctionList();

        return ret;
    };


    const std::array<TetrahedronP1Bubble::shape_function_type, 15>&
    TetrahedronP1Bubble ::FirstDerivateShapeFunctionList()
    {
        static auto ret = ComputeGradShapeFunctionList();

        return ret;
    }


    namespace // anonymous
    {


        std::array<TetrahedronP1Bubble::shape_function_type, 5> ComputeShapeFunctionList()
        {
            return { { [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT
                           const auto buf = (1. - r - s - t);

                           return buf - 64. * r * s * t * buf;
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return r - 64. * r * s * t * (1. - r - s - t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT
                           return s - 64. * r * s * t * (1. - r - s - t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT
                           return t - 64. * r * s * t * (1. - r - s - t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT
                           return 256. * r * s * t * (1. - r - s - t);
                       } } };
        };


        std::array<TetrahedronP1Bubble::shape_function_type, 15> ComputeGradShapeFunctionList()
        {
            return { { [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -1. - 64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -1. - 64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -1. - 64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return 1. - 64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return 1. - 64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -64. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return -64. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT

                           return 1. - 64. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT
                           return 256. * (s * t - 2. * r * s * t - s * s * t - s * t * t);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT
                           return 256. * (t * r - 2. * s * t * r - t * t * r - t * r * r);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           const auto t = local_coords.t(); // NOLINT
                           return 256. * (r * s - 2. * t * r * s - r * r * s - r * s * s);
                       } } };
        }


    } // namespace


} // namespace MoReFEM::Internal::ShapeFunctionNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
