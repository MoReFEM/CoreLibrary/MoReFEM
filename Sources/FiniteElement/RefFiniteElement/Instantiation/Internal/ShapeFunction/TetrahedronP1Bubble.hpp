// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_TETRAHEDRONP1BUBBLE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_TETRAHEDRONP1BUBBLE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>

#include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/StrongType.hpp" // IWYU pragma: export

namespace MoReFEM::Internal::ShapeFunctionNS
{


    /*!
     * \brief Define shape functions of triangle with P1 bubble (additional dof in the interior).
     */
    struct TetrahedronP1Bubble
    : public Advanced::RefGeomEltNS::ShapeFunctionNS::Crtp::AccessShapeFunction<TetrahedronP1Bubble>
    {

        //! Number of component against which derivation occurs.
        static inline constexpr ::MoReFEM::GeometryNS::dimension_type Nderivate_component_{ 3 };

        //! Aliases to avoid magic numbers.
        enum { Nphi_ = 5, Order = 1 };

        //! Alias to a function that takes a Coords and returns a double.
        using shape_function_type = MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionType;

        //! Shape functions.
        static const std::array<shape_function_type, Nphi_>& ShapeFunctionList();

        /*!
         * \brief First derivative of the shape functions.
         *
         * Ordering:
         *   \li d(phi[0], r), d(phi[0], s), d(phi[0], t)
         *   \li d(phi[1], r), d(phi[1], s), d(phi[0], t)
         *   etc...
         *
         * \return The derivatives as an array of functions (ordering defined just above)
         */
        static const std::array<shape_function_type, Nphi_ * Nderivate_component_.Get()>&
        FirstDerivateShapeFunctionList();
    };


} // namespace MoReFEM::Internal::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TetrahedronP1Bubble.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_TETRAHEDRONP1BUBBLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
