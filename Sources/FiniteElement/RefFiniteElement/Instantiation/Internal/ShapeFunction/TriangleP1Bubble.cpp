// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TriangleP1Bubble.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Internal::ShapeFunctionNS
{


    namespace // anonymous
    {


        std::array<TriangleP1Bubble::shape_function_type, 4> ComputeShapeFunctionList();


        std::array<TriangleP1Bubble::shape_function_type, 8> ComputeGradShapeFunctionList();


    } // namespace


    const std::array<TriangleP1Bubble::shape_function_type, 4>& TriangleP1Bubble::ShapeFunctionList()
    {
        static auto ret = ComputeShapeFunctionList();

        return ret;
    };


    const std::array<TriangleP1Bubble::shape_function_type, 8>& TriangleP1Bubble ::FirstDerivateShapeFunctionList()
    {
        static auto ret = ComputeGradShapeFunctionList();

        return ret;
    }


    namespace // anonymous
    {


        std::array<TriangleP1Bubble::shape_function_type, 4> ComputeShapeFunctionList()
        {
            return { { [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return 1. - r - s + 9. * r * s * (r + s - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return r + 9. * r * s * (r + s - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return s + 9. * r * s * (r + s - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return 27. * r * s * (1. - r - s);
                       } } };
        };


        std::array<TriangleP1Bubble::shape_function_type, 8> ComputeGradShapeFunctionList()
        {
            return { { [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return -1. + 9. * s * (2. * r + s - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return -1. + 9. * r * (2. * s + r - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return 1. + 9. * s * (2. * r + s - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return 9. * r * (2. * s + r - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return 9. * s * (2. * r + s - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           const auto s = local_coords.s(); // NOLINT
                           return 1. + 9. * r * (2. * s + r - 1.);
                       },
                       [](const auto& local_coords)
                       {
                           const auto s = local_coords.s(); // NOLINT
                           return 27. * s * (1. - 2. * local_coords.r() - s);
                       },
                       [](const auto& local_coords)
                       {
                           const auto r = local_coords.r(); // NOLINT
                           return 27. * r * (1. - 2. * local_coords.s() - r);
                       } } };
        }


    } // namespace


} // namespace MoReFEM::Internal::ShapeFunctionNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
