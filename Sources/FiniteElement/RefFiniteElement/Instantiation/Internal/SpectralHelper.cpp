// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    double Interpolation(const Eigen::VectorXd& pos, double p, LocalNodeNS::index_type local_node_index)
    {
        const LocalNodeNS::index_type n{ static_cast<LocalNodeNS::index_type::underlying_type>(pos.size()) };
        double s = 1.;

        assert(local_node_index.Get() < pos.size());
        const double pos_phi = pos[local_node_index.Get()];

        for (LocalNodeNS::index_type r{}; r < n; ++r)
        {
            if (r != local_node_index)
            {
                const auto r_value = r.Get();
                s *= (p - pos[r_value]) / (pos_phi - pos[r_value]);
            }
        }

        return s;
    }


    double DerivativeInterpolation(const Eigen::VectorXd& pos, double p, LocalNodeNS::index_type local_node_index)
    {
        double s = 0.;
        double d = 1.;
        const LocalNodeNS::index_type n{ static_cast<Eigen::Index>(pos.size()) };

        for (LocalNodeNS::index_type r{}; r < n; ++r)
        {
            if (r != local_node_index)
                d *= (pos[local_node_index.Get()] - pos[r.Get()]);
        }

        for (LocalNodeNS::index_type r1{}; r1 < n; ++r1)
        {
            if (r1 != local_node_index)
            {
                double c = 1.;

                for (LocalNodeNS::index_type r2{}; r2 < n; ++r2)
                {
                    if (r2 != r1 && r2 != local_node_index)
                        c *= (p - pos[r2.Get()]);
                }

                s += c;
            }
        }
        return s / d;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
