// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SPECTRALHELPER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SPECTRALHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <string>
#include <vector>

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    /*!
     *
     * \class doxygen_hide_spectral_helper_args
     *
     * \param[in] pos The lagrangian basis are built upon the points \a pos. This list gets the
     * same size as the number of local nodes.
     * \param[in] local_node_index Local node for which interpolation is computed.
     * \param[in] p Position for which the interpolation is computed.
     *
     * \internal <b><tt>[internal]</tt></b> Implementation is directly taken from Ondomatic, with
     * just a bit of reformatting.
     * \endinternal
     */


    /*!
     *
     * \class doxygen_hide_spectral_helper_NI_NJ_NK
     *
     * \tparam NI number of local nodes to consider along x axis.
     * \tparam NJ number of local nodes to consider along y axis.
     * \tparam NK number of local nodes to consider along z axis.
     */


    /*!
     * \brief Generates the I, J and K position of a node given its local node index.
     *
     * \copydoc doxygen_hide_spectral_helper_NI_NJ_NK
     *
     * \param[in] local_node_index Index of the local node for which computation occurs.
     *
     * \return (I, J, K) position of a node given its \a local_node_index.
     */
    template<Eigen::Index NI, Eigen::Index NJ, Eigen::Index NK>
    Eigen::Matrix<LocalNodeNS::index_type, 3, 1, Eigen::ColMajor>
    ComputeIntegerCoordinates(LocalNodeNS::index_type local_node_index);


    /*!
     * \brief 1D Interpolation at \a p of the shape function \a phi.
     *
     * \copydoc doxygen_hide_spectral_helper_args
     *
     * \return Interpolated value.
     */
    double Interpolation(const Eigen::VectorXd& pos, double p, LocalNodeNS::index_type local_node_index);


    /*!
     * \brief 1D Interpolation at 'p' of the derivative of a shape function 'phi'.
     *
     * \copydoc doxygen_hide_spectral_helper_args
     *
     * \return Interpolated value.
     */
    double DerivativeInterpolation(const Eigen::VectorXd& pos, double p, LocalNodeNS::index_type local_node_index);


    /*!
     * \brief Generate the most adequate shape function label.
     *
     * \tparam TopologyT Topology considered.
     * \copydoc doxygen_hide_spectral_helper_NI_NJ_NK
     *
     * \return Shape function label.
     *
     * The convention is:
     * - A 'P' or a 'Q'
     * - Followed by one figure if isotropy, e.g. 'P4' or 'Q6'.
     * - Followed by figures separated by a '-'if anisotropy, e.g. 'P2-3' or 'Q5-4-7'.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    std::string GenerateShapeFunctionLabel();


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SPECTRALHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
