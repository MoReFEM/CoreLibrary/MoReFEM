// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SPECTRALHELPER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SPECTRALHELPER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <sstream>
#include <string>
#include <type_traits>

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"
// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS { class Segment; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    template<Eigen::Index NI, Eigen::Index NJ, Eigen::Index NK>
    auto ComputeIntegerCoordinates(LocalNodeNS::index_type local_node_index)
        -> Eigen::Matrix<LocalNodeNS::index_type, 3, 1, Eigen::ColMajor>
    {
        assert(local_node_index.Get() < (NI + 1) * (NJ + 1) * (NK + 1));
        const auto m = local_node_index % LocalNodeNS::index_type{ (NI + 1) * (NJ + 1) };

        // clang-format off
        Eigen::Matrix<LocalNodeNS::index_type, 3, 1, Eigen::ColMajor> ret
        {
            {
                m % LocalNodeNS::index_type { NI + 1 },
                m / LocalNodeNS::index_type { NJ + 1 },
                local_node_index / LocalNodeNS::index_type { (NI + 1) * (NJ + 1) }
            }
        };
        // clang-format on

        return ret;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    std::string GenerateShapeFunctionLabel()
    {
        std::ostringstream oconv;
        const char letter =
            std::is_same<TopologyT, ::MoReFEM::Advanced::RefGeomEltNS::TopologyNS::Segment>() ? 'P' : 'Q';

        constexpr ::MoReFEM::GeometryNS::dimension_type dimension = TopologyT::dimension;

        oconv << letter << NI;

        switch (dimension.Get())
        {
        case 1:
            // Nothing to add.
            break;
        case 2:
            // Check for anisotropy.
            if (NI != NJ)
                oconv << '-' << letter << NJ;
            break;
        case 3:
            // Check for anisotropy.
            if (NI != NJ || NI != NK)
                oconv << '-' << letter << NJ << '-' << letter << NK;
            break;
        default:
            assert(false && "Should not happen!");
            exit(EXIT_FAILURE);
        }

        const std::string ret(oconv.str());
        return ret;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SPECTRALHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
