// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_POINT_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_POINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Instances/Point/Topology/Point.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp"                     // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp" // IWYU pragma: export


namespace MoReFEM::RefFEltNS
{


    //! Enum class to "distinguish" P0 and P1 (not meaningful, but waiting for #1146 to be addressed)
    enum class PointShapeEnum { P0, P1, P2 };


    /*!
     * \brief Reference finite element for Point.
     *
     * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
     * Advanced::RefGeomEltNS::TopologyNS::Point class.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     *
     * \tparam PointShapeEnumT Enum value to "distinguish" P0 and P1. The behaviour of the generated class is
     * exactly the same; the reason to this parameter is that we want a \a Point object to be generated for
     * both "P0" and "P1" shape function labels given in the input file.
     */
    template<PointShapeEnum PointShapeEnumT>
    class Point
    // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            Advanced::RefGeomEltNS::TopologyNS::Point,
            Internal::ShapeFunctionNS::Order0,
            InterfaceNS::Nature::none,
            1UL
        >
    // clang-format on
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Point() = default;

        //! Destructor.
        ~Point() override;

        //! \copydoc doxygen_hide_copy_constructor
        Point(const Point& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Point(Point&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Point& operator=(const Point& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Point& operator=(Point&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_POINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
