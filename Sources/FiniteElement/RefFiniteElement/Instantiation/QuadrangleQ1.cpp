// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "FiniteElement/RefFiniteElement/Instantiation/QuadrangleQ1.hpp"


namespace MoReFEM::RefFEltNS
{


    namespace // anonymous
    {

        // #896 obsolete with the correction of the spectral elements.
        //[[maybe_unused]] const bool registered =  //NOLINT
        //    Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
        //    __LINE__).Register<QuadrangleQ1>();


    } // namespace


    const std::string& QuadrangleQ1::ShapeFunctionLabel()
    {
        static const std::string ret("Q1");
        return ret;
    }


    QuadrangleQ1::~QuadrangleQ1() = default;


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
