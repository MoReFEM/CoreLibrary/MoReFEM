// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_QUADRANGLEQ2C_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_QUADRANGLEQ2C_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle8.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for QuadrangleQ2c.
     *
     * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
     * the one on edges.
     * Local nodes on vertices are numbered exactly as they were on the Advanced::RefGeomEltNS::TopologyNS::Quadrangle
     * class. Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in
     * Advanced::RefGeomEltNS::TopologyNS::Quadrangle traits class is now 'Nvertex + i'.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */
    // clang-format off
        class QuadrangleQ2c
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            Advanced::RefGeomEltNS::TopologyNS::Quadrangle,
            Advanced::RefGeomEltNS::ShapeFunctionNS::Quadrangle8,
            InterfaceNS::Nature::edge,
            0UL
        >
    // clang-format on
    {


      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit QuadrangleQ2c() = default;

        //! Destructor.
        virtual ~QuadrangleQ2c() override;

        //! \copydoc doxygen_hide_copy_constructor
        QuadrangleQ2c(const QuadrangleQ2c& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        QuadrangleQ2c(QuadrangleQ2c&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        QuadrangleQ2c& operator=(const QuadrangleQ2c& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        QuadrangleQ2c& operator=(QuadrangleQ2c&& rhs) = default;


        ///@}
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_QUADRANGLEQ2C_DOT_HPP_
// *** MoReFEM end header guards *** < //
