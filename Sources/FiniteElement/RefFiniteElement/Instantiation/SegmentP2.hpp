// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SEGMENTP2_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SEGMENTP2_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string> // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>

#include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment3.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for SegmentP2.
     *
     * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
     * Advanced::RefGeomEltNS::TopologyNS::Segment class.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */
    // clang-format off
        class SegmentP2 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            Advanced::RefGeomEltNS::TopologyNS::Segment,
            Advanced::RefGeomEltNS::ShapeFunctionNS::Segment3,
            InterfaceNS::Nature::vertex,
            1UL
        >
    // clang-format on
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit SegmentP2() = default;

        //! Destructor.
        ~SegmentP2() override;

        //! \copydoc doxygen_hide_copy_constructor
        SegmentP2(const SegmentP2& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        SegmentP2(SegmentP2&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        SegmentP2& operator=(const SegmentP2& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        SegmentP2& operator=(SegmentP2&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SEGMENTP2_DOT_HPP_
// *** MoReFEM end header guards *** < //
