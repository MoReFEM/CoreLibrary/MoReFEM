// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SPECTRAL_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SPECTRAL_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Spectral.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>

#include "Geometry/Interfaces/Advanced/LocalData.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"

#include "FiniteElement/QuadratureRules/EnumGaussQuadratureFormula.hpp"
#include "FiniteElement/QuadratureRules/GaussQuadratureFormula.hpp"
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"


namespace MoReFEM::RefFEltNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    const std::string& Spectral<TopologyT, NI, NJ, NK>::ShapeFunctionLabel()
    {
        static const std::string ret = Internal::RefFEltNS::GenerateShapeFunctionLabel<TopologyT, NI, NJ, NK>();
        return ret;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    Spectral<TopologyT, NI, NJ, NK>::Spectral()
    {
        this->template Init<TopologyT>();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    double Spectral<TopologyT, NI, NJ, NK>::ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                          const LocalCoords& local_coords) const
    {
        const auto shape_function_index_list =
            Internal::RefFEltNS::ComputeIntegerCoordinates<NI, NJ, NK>(local_node_index);

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ local_coords.GetDimension() };

        double ret = 1.;

        for (auto index = ::MoReFEM::GeometryNS::dimension_type{}; index < Ncomponent; ++index)
            ret *= Internal::RefFEltNS::Interpolation(points_per_ijk_[static_cast<std::size_t>(index.Get())],
                                                      local_coords[index],
                                                      shape_function_index_list(index.Get()));


        return ret;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    double Spectral<TopologyT, NI, NJ, NK>::FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                       ::MoReFEM::GeometryNS::dimension_type icoor,
                                                                       const LocalCoords& local_coords) const
    {
        const auto shape_function_index_list =
            Internal::RefFEltNS::ComputeIntegerCoordinates<NI, NJ, NK>(local_node_index);

        double ret = 1.;

        const auto Ncomponent =
            ::MoReFEM::GeometryNS::dimension_type{ static_cast<Eigen::Index>(local_coords.GetDimension()) };

        for (::MoReFEM::GeometryNS::dimension_type index{}; index < Ncomponent; ++index)
        {
            if (icoor == index)
                ret *=
                    Internal::RefFEltNS::DerivativeInterpolation(points_per_ijk_[static_cast<std::size_t>(index.Get())],
                                                                 local_coords[index],
                                                                 shape_function_index_list[index.Get()]);
            else
                ret *= Internal::RefFEltNS::Interpolation(points_per_ijk_[static_cast<std::size_t>(index.Get())],
                                                          local_coords[index],
                                                          shape_function_index_list[index.Get()]);
        }

        return ret;
    }


    /////////////////////
    // PRIVATE METHODS //
    /////////////////////

    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    Advanced::LocalNode::vector_const_shared_ptr Spectral<TopologyT, NI, NJ, NK>::ComputeLocalNodeList()
    {
        //            static_assert(std::is_same<TopologyT, Advanced::RefGeomEltNS::TopologyNS::Hexahedron>() ||
        //                          std::is_same<TopologyT, Advanced::RefGeomEltNS::TopologyNS::Quadrangle>() ||
        //                          std::is_same<TopologyT, Advanced::RefGeomEltNS::TopologyNS::Segment>(),
        //                          "Spectral finite element doesn't make sense for other topologies.");
        //
        //            static_assert(!(std::is_same<TopologyT, Advanced::RefGeomEltNS::TopologyNS::Segment>() && (NJ !=
        //            0 || NK != 0)),
        //                          "Spectral upon segment can't be defined with NJ or NK > 0!");
        //
        //            static_assert(!(std::is_same<TopologyT, Advanced::RefGeomEltNS::TopologyNS::Quadrangle>() && NK !=
        //            0),
        //                          "Spectral upon quadrangle can't be defined with NK > 0!");
        //

        Advanced::LocalNode::vector_const_shared_ptr local_node_list;

        Eigen::Index Ncomponent{};

        assert(std::all_of(points_per_ijk_.cbegin(),
                           points_per_ijk_.cend(),
                           [](const auto& vector)
                           {
                               return vector.size() == 0;
                           })
               && "This container is initialized in the present method which should be called only once!");

        // The local coords of the local dofs are computed with the Gauss-Lobatto formula.
        Eigen::Vector3i Ngauss{ { NI, NJ, NK } };
        std::array<Eigen::VectorXd, 3> weights; // I don't care about weight in present function;
        // only there for Gauss-Lobatto function prototype.


        for (auto i = Eigen::Index{}; i < 3; ++i)
        {
            const auto as_size_t = static_cast<std::size_t>(i);

            if (Ngauss[i] > 0)
            {
                Internal::QuadratureNS::ComputeGaussFormulas<MoReFEM::QuadratureNS::GaussFormula::Gauss_Lobatto>(
                    Ngauss[i] + 1, points_per_ijk_[as_size_t], weights[as_size_t]);
                Ncomponent = i + 1;
            } else
                points_per_ijk_[as_size_t] = Eigen::VectorXd{ { 0. } };
        }


        // Create here all the local nodes.
        for (Eigen::Index k = 0; k <= NK; ++k)
        {
            const Eigen::Index is_k_border = (k == 0 || k == NK ? 1 : 0);

            for (Eigen::Index j = 0; j <= NJ; ++j)
            {
                const Eigen::Index is_j_border = (j == 0 || j == NJ ? 1 : 0);

                for (Eigen::Index i = 0; i <= NI; ++i)
                {
                    const LocalNodeNS::index_type index{ static_cast<Eigen::Index>(k * (NJ + 1) * (NI + 1)
                                                                                   + j * (NI + 1) + i) };
                    const auto Nborder = is_k_border + is_j_border + (i == 0 || i == NI ? 1 : 0);

                    InterfaceNS::Nature nature = InterfaceNS::Nature::none;

                    LocalCoords::unique_ptr local_coords_ptr(nullptr);

                    switch (Ncomponent)
                    {
                    case 1:
                    {
                        LocalCoords::underlying_type buf{ { points_per_ijk_[0][i] } };
                        local_coords_ptr = std::make_unique<LocalCoords>(buf);
                        break;
                    }
                    case 2:
                    {
                        LocalCoords::underlying_type buf{ { points_per_ijk_[0][i], points_per_ijk_[1][j] } };
                        local_coords_ptr = std::make_unique<LocalCoords>(buf);
                        break;
                    }
                    case 3:
                    {
                        LocalCoords::underlying_type buf{
                            { points_per_ijk_[0][i], points_per_ijk_[1][j], points_per_ijk_[2][k] }
                        };
                        local_coords_ptr = std::make_unique<LocalCoords>(buf);
                        break;
                    }
                    default:
                    {
                        assert(false);
                        break;
                    }
                    }

                    assert(!(!local_coords_ptr));
                    const auto& local_coords = *local_coords_ptr;


                    switch (Nborder)
                    {
                    case 0:
                        nature = InterfaceNS::Nature::volume;
                        break;
                    case 1:
                        nature = InterfaceNS::Nature::face;
                        break;
                    case 2:
                        nature = InterfaceNS::Nature::edge;
                        break;
                    case 3:
                        nature = InterfaceNS::Nature::vertex;
                        break;
                    default:
                        assert(false);
                    }

                    assert(nature != InterfaceNS::Nature::none);

                    auto&& local_interface =
                        Advanced::InterfaceNS::LocalData<TopologyT>::FindLocalInterface(nature, local_coords);


                    auto local_node_ptr =
                        std::make_shared<const Advanced::LocalNode>(std::move(local_interface), index, local_coords);

                    local_node_list.push_back(local_node_ptr);
                }
            }
        }

        assert(local_node_list.size() == (NI + 1) * (NJ + 1) * (NK + 1));

        return local_node_list;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT,
        Eigen::Index NI,
        Eigen::Index NJ,
        Eigen::Index NK
    >
    // clang-format on
    Eigen::Index Spectral<TopologyT, NI, NJ, NK>::GetOrder() const noexcept
    {
        return std::max({ NI, NJ, NK });
    }


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SPECTRAL_DOT_HXX_
// *** MoReFEM end header guards *** < //
