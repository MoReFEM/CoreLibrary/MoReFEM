// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TRIANGLEP0_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TRIANGLEP0_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp"                     // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp" // IWYU pragma: export


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for TriangleP0.
     *
     * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
     * Advanced::RefGeomEltNS::TopologyNS::Triangle class.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */
    class TriangleP0
    // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            Advanced::RefGeomEltNS::TopologyNS::Triangle,
            Internal::ShapeFunctionNS::Order0,
            InterfaceNS::Nature::none,
            1UL
        >
    // clang-format on
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit TriangleP0() = default;

        //! Destructor.
        ~TriangleP0() override;

        //! \copydoc doxygen_hide_copy_constructor
        TriangleP0(const TriangleP0& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        TriangleP0(TriangleP0&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TriangleP0& operator=(const TriangleP0& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        TriangleP0& operator=(TriangleP0&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TRIANGLEP0_DOT_HPP_
// *** MoReFEM end header guards *** < //
