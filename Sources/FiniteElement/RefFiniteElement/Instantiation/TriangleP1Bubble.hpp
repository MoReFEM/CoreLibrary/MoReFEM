// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TRIANGLEP1BUBBLE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TRIANGLEP1BUBBLE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TriangleP1Bubble.hpp" // IWYU pragma: export


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for TriangleP1 with a bubble.
     *
     * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
     * the one on edges.
     * Local nodes on vertices are numbered exactly as they were on the Advanced::RefGeomEltNS::TopologyNS::Triangle
     * class. Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in
     * Advanced::RefGeomEltNS::TopologyNS::Triangle traits class is now 'Nvertex + i'.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */

    class TriangleP1Bubble
    // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            Advanced::RefGeomEltNS::TopologyNS::Triangle,
            Internal::ShapeFunctionNS::TriangleP1Bubble,
            InterfaceNS::Nature::vertex,
            1UL
        >
    // clang-format on
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = TriangleP1Bubble;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit TriangleP1Bubble() = default;

        //! Destructor.
        virtual ~TriangleP1Bubble() override;

        //! \copydoc doxygen_hide_copy_constructor
        TriangleP1Bubble(const TriangleP1Bubble& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        TriangleP1Bubble(TriangleP1Bubble&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TriangleP1Bubble& operator=(const TriangleP1Bubble& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        TriangleP1Bubble& operator=(TriangleP1Bubble&& rhs) = default;

        ///@}

      private:
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TRIANGLEP1BUBBLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
