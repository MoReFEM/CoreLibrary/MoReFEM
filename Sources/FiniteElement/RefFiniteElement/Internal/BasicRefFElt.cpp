// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <string_view>
#include <vector>

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "Utilities/Containers/Print.hpp"

#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/Alias.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    BasicRefFElt::BasicRefFElt() : topology_dimension_(NumericNS::UninitializedIndex<decltype(topology_dimension_)>())
    { }


    BasicRefFElt::~BasicRefFElt() = default;


    void BasicRefFElt::Print(std::ostream& stream) const
    {
        const auto Nvertex = this->NnodeOnVertex();
        const auto Nedge = this->NnodeOnEdge();
        const auto Nface = this->NnodeOnFace();


        for (auto i = Eigen::Index{}; i < Nvertex; ++i)
        {
            auto vertex_dof_ptr = GetLocalNodeOnVertexPtr(i);

            if (!vertex_dof_ptr)
                continue;

            stream << "Vertex " << i << ": (" << vertex_dof_ptr->GetIndex() << ')' << '\n';
        }


        if (AreNodesOnEdges())
        {
            for (auto i = Eigen::Index{}; i < Nedge; ++i)
            {
                for (auto orientation = Advanced::InterfaceNS::orientation_type{ 0UL };
                     orientation < Advanced::InterfaceNS::orientation_type{ 2UL };
                     ++orientation)
                {
                    auto dof_on_current_edge_list = GetLocalNodeOnEdgeList(i, orientation);

                    if (dof_on_current_edge_list.empty())
                        continue;

                    std::vector<LocalNodeNS::index_type> indexes;

                    for (const auto& local_node_ptr : dof_on_current_edge_list)
                    {
                        assert(!(!local_node_ptr));
                        indexes.push_back(local_node_ptr->GetIndex());
                    }

                    stream << "Edge " << i << ", Orientation " << orientation << ": ";
                    Utilities::PrintContainer<>::Do(indexes,
                                                    stream,
                                                    ::MoReFEM::PrintNS::Delimiter::separator(","),
                                                    ::MoReFEM::PrintNS::Delimiter::opener("("),
                                                    ::MoReFEM::PrintNS::Delimiter::closer(")\n"));
                }
            }
        }


        if (AreNodesOnFaces())
        {
            constexpr auto Norientation = Advanced::InterfaceNS::orientation_type{ 8UL };

            for (auto i = Eigen::Index{}; i < Nface; ++i)
            {
                for (auto orientation = Advanced::InterfaceNS::orientation_type{ 0UL }; orientation < Norientation;
                     ++orientation)
                {
                    auto dof_on_current_face_list = GetLocalNodeOnFaceList(i, orientation);

                    if (dof_on_current_face_list.empty())
                        continue;

                    std::vector<LocalNodeNS::index_type> indexes;

                    for (const auto& local_node_ptr : dof_on_current_face_list)
                    {
                        assert(!(!local_node_ptr));
                        indexes.push_back(local_node_ptr->GetIndex());
                    }

                    stream << "Face " << i << ", Orientation " << orientation << ": ";
                    Utilities::PrintContainer<>::Do(indexes,
                                                    stream,
                                                    ::MoReFEM::PrintNS::Delimiter::separator(","),
                                                    ::MoReFEM::PrintNS::Delimiter::opener("("),
                                                    ::MoReFEM::PrintNS::Delimiter::closer(")\n"));
                }
            }
        }


        if (AreNodesOnVolume())
        {
            const auto& local_node_list_on_current_volume = GetLocalNodeOnVolumeList();

            std::vector<LocalNodeNS::index_type> indexes;

            for (const auto& local_node_ptr : local_node_list_on_current_volume)
            {
                assert(!(!local_node_ptr));
                indexes.push_back(local_node_ptr->GetIndex());
            }

            stream << "Volume: ";
            Utilities::PrintContainer<>::Do(indexes,
                                            stream,
                                            ::MoReFEM::PrintNS::Delimiter::separator(","),
                                            ::MoReFEM::PrintNS::Delimiter::opener("("),
                                            ::MoReFEM::PrintNS::Delimiter::closer(")\n"));
        }
    }


#ifndef NDEBUG


    //! Local function to check whether the list are well-formed.
    void CheckLocalNodeListConsistency(const Advanced::LocalNode::vector_const_shared_ptr& local_node_list,
                                       ::MoReFEM::InterfaceNS::Nature nature)
    {
        for (const auto& node_ptr : local_node_list)
        {
            assert(!(!node_ptr));
            assert(node_ptr->GetLocalInterface().GetNature() == nature);
        }
    }

#endif // NDEBUG


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
