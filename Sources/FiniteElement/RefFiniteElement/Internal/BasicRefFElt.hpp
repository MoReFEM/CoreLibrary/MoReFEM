// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELT_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Miscellaneous.hpp"   // IWYU pragma: export
#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"
#include "Geometry/StrongType.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


#ifndef NDEBUG

    /*!
     * \brief Debug function to check whether the list are well-formed.
     *
     * If not, an assert will occur.
     *
     * \param[in] local_node_list List of local nodes being scrutinized.
     * \param[in] nature All nodes in the list are supposed to be located on \a Interface of
     * this type.
     */
    void CheckLocalNodeListConsistency(const Advanced::LocalNode::vector_const_shared_ptr& local_node_list,
                                       ::MoReFEM::InterfaceNS::Nature nature);
#endif // NDEBUG


    /*!
     * \brief Abstract base class used to define a reference finite element.
     *
     * If a new type of finite element is to be added, it should inherit from present class. So far there
     * are two 'families' of finite elements:
     * - Those that uses up the geometric shape functions.
     * - Spectral ones.
     *
     * All the types of finite elements made accessible to an advanced user must be registered properly
     * in the BasicRefFEltFactory, in which the ShapeFunctionLabel() value acts as a key. See for instance
     * FiniteElement/RefFiniteElement/Instantiation/Spectral.cpp for an example of such a registration.
     *
     * The inherited class are in charge of five operations:
     * - Implementing the shape function.
     * - Implementing the first derivate of the shape function.
     * - Compute the local node list (pure virtual method to override).
     * - Call Init() method at the end of its constructor.
     * - Defining a static method ShapeFunctionLabel which returns a string that describes the shape function.
     *
     * \attention This class is really low-level and an advanced user or a developer shouldn't have to deal
     * directly with BasicRefFElt (except to define a new reference finite element).
     *
     * What a developer should use when defining its own operator is a \a RefFEltInLocalOperator, which is
     * an object built on top of a RefFEltInFEltSpace which is itself built upon a BasicRefFElt.
     *
     */
    class BasicRefFElt
    {

      public:
        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const BasicRefFElt>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        BasicRefFElt();

        //! Destructor.
        virtual ~BasicRefFElt();

        //! \copydoc doxygen_hide_copy_constructor
        BasicRefFElt(const BasicRefFElt& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        BasicRefFElt(BasicRefFElt&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        BasicRefFElt& operator=(const BasicRefFElt& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        BasicRefFElt& operator=(BasicRefFElt&& rhs) = default;

        ///@}

      protected:
        /*!
         * \brief The method that does the actual initialisation of the object.
         *
         * \attention This method must be called in the constructor of the derived object!
         */
        template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
        void Init();


      public:
        /// \name Accessors to the nodes regardless of the underlying interface.
        ///@{

        //! Return the number of local nodes.
        LocalNodeNS::index_type NlocalNode() const noexcept;

        //! Access to the \a i -th local node, whatever its nature.
        //! \param[in] i Index of the sought \a LocalNode in the internal local node list.
        const Advanced::LocalNode& GetLocalNode(LocalNodeNS::index_type i) const noexcept;

        //! Access to the complete list of local nodes, whatever their nature.
        const Advanced::LocalNode::vector_const_shared_ptr& GetLocalNodeList() const noexcept;

        ///@}


        /// \name Accessors to the nodes on vertices.
        ///@{


        //! Whether there are nodes on vertices.
        bool AreNodesOnVertices() const noexcept;


        /*!
         * \class doxygen_hide_local_node_on_vertex
         *
         * \brief Return the LocalNode that matches the given \a topologic_vertex_index of the vertex
         *
         * \param[in] topologic_vertex_index Index defined in RefGeomEltNS::TopologyNS. For instance for
         Spectral
         * finite element associated to a quadrangle:

         *
         *     3-----------2
         *     |           |
         *     | 	       |
         *     |           |
         *     | 	       |
         *     | 	       |
         *     0-----------1
         *
         * \return The LocalNode object associated to this vertex (its internal index is the one computed
         * within current class). Might be nullptr if none.
         */

        //! \copydoc doxygen_hide_local_node_on_vertex
        const Advanced::LocalNode::const_shared_ptr&
        GetLocalNodeOnVertexPtr(Eigen::Index topologic_vertex_index) const noexcept;

        //! \copydoc doxygen_hide_local_node_on_vertex
        const Advanced::LocalNode& GetLocalNodeOnVertex(Eigen::Index topologic_vertex_index) const noexcept;

        //! Access to the list of nodes on vertex.
        const Advanced::LocalNode::vector_const_shared_ptr& GetLocalNodeOnVertexList() const noexcept;

        ///@}


        /// \name Accessors to the nodes on edges.
        ///@{

        //! Whether there are nodes on edges.
        bool AreNodesOnEdges() const noexcept;


        /*!
         * \class doxygen_hide_local_node_on_edge
         *
         * \brief Return all the local nodes on the given geometric edge with the proper orientation.
         *
         * \param[in] topologic_edge_index Index defined in RefGeomEltNS::TopologyNS. For instance for Spectral
         * finite element associated to a quadrangle:

         \verbatim

              3-----------2
              |           |
              |           |            Edges:   { { 0, 1 } },
              |           |                     { { 1, 2 } },
              |           |                     { { 3, 2 } },
              | 	      |                     { { 0, 3 } }
              0-----------1
         \endverbatim
         *
         * \return The LocalNode objects associated to this edge (its internal index is the one computed
         * within current class).
         */

        //! \copydoc doxygen_hide_local_node_on_edge
        //! \param[in] orientation Index which gives away the orientation chosen. See \a OrientedEdge for more
        //! details.
        const Advanced::LocalNode::vector_const_shared_ptr&
        GetLocalNodeOnEdgeList(Eigen::Index topologic_edge_index,
                               Advanced::InterfaceNS::orientation_type orientation) const noexcept;

        //! \copydoc doxygen_hide_local_node_on_edge
        //! Orientation is not considered here.
        const std::vector<Advanced::LocalNode::vector_const_shared_ptr>&
        GetLocalNodeOnEdgeList(Eigen::Index topologic_edge_index) const noexcept;

        ///@}


        /// \name Accessors to the nodes on faces.
        ///@{

        //! Whether there are nodes on faces.
        bool AreNodesOnFaces() const noexcept;

        /*!
         * \brief Same for faces - see \a GetLocalNodeOnEdgeList() for more detailed explanation.
         *
         * \param[in] topologic_face_index Index defined in RefGeomEltNS::TopologyNS for faces.
         * \param[in] orientation Index which gives away the orientation chosen. See \a OrientedFace for more
         * details.
         */
        const Advanced::LocalNode::vector_const_shared_ptr&
        GetLocalNodeOnFaceList(Eigen::Index topologic_face_index,
                               Advanced::InterfaceNS::orientation_type orientation) const noexcept;

        //! Return all the local nodes on the given geometric face regardless of the orientation.
        //! \param[in] topologic_face_index Index defined in RefGeomEltNS::TopologyNS for faces.
        const std::vector<Advanced::LocalNode::vector_const_shared_ptr>&
        GetLocalNodeOnFaceList(Eigen::Index topologic_face_index) const noexcept;

        ///@}


        /// \name Accessors to the nodes on volume.
        ///@{

        //! Whether there are nodes on volume.
        bool AreNodesOnVolume() const noexcept;

        //! Same for volume.
        const Advanced::LocalNode::vector_const_shared_ptr& GetLocalNodeOnVolumeList() const noexcept;

        ///@}


        /*!
         * \brief Print the content of the ref finite element.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Print(std::ostream& stream) const;

        //! Dimension of the underlying topology.
        ::MoReFEM::GeometryNS::dimension_type GetTopologyDimension() const noexcept;

        /*!
         * \copydoc doxygen_hide_shape_function
         */
        virtual double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                     const LocalCoords& local_coords) const = 0;


        /*!
         * \copydoc doxygen_hide_first_derivate_shape_function
         */
        virtual double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                  ::MoReFEM::GeometryNS::dimension_type component,
                                                  const LocalCoords& local_coords) const = 0;


        //! \copydoc doxygen_hide_shape_function_order_method
        virtual Eigen::Index GetOrder() const noexcept = 0;

      private:
        /*!
         * \brief Compute the local nodes.
         *
         * This method should not be called outside of Init() method.
         *
         * \return List of local nodes.
         */
        virtual Advanced::LocalNode::vector_const_shared_ptr ComputeLocalNodeList() = 0;

        //! Sort local_node_on_vertex_list_ so that its ordering match the one in TopologyT.
        template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
        void SortLocalNodeOnVertexList();

        /*!
         * \brief Set the list of local nodes on edges within the dedicated container.
         *
         * \param[in] local_node_on_edge_list List of local nodes on edge.
         */
        template<::MoReFEM::Advanced::Concept::TopologyNS::with_edge TopologyT>
        void SetEdgeLocalNodeList(const Advanced::LocalNode::vector_const_shared_ptr& local_node_on_edge_list);

        //! Set the list of local nodes on faces within the dedicated container.
        //! \param[in] local_node_on_face_list List of local nodes on face.
        template<::MoReFEM::Advanced::Concept::TopologyNS::with_face TopologyT>
        void SetFaceLocalNodeList(const Advanced::LocalNode::vector_const_shared_ptr& local_node_on_face_list);

        //! Return the number of nodes on vertices.
        Eigen::Index NnodeOnVertex() const noexcept;

        //! Return the number of nodes on edges.
        Eigen::Index NnodeOnEdge() const noexcept;

        //! Return the number of nodes on faces.
        Eigen::Index NnodeOnFace() const noexcept;


      private:
        //! List of local nodes, whatever their nature.
        Advanced::LocalNode::vector_const_shared_ptr local_node_list_;

        //! Subset of local_node_list_ that includes only nodes on vertices.
        Advanced::LocalNode::vector_const_shared_ptr local_node_on_vertex_list_;

        /*!
         * \brief Local nodes on edges stored with their topologic index as a key.
         *
         * For each geometric key there is an array of two elements: one for each orientation.
         * For each geometric key/orientation, there is the list of all nodes present.
         */
        std::vector<std::vector<Advanced::LocalNode::vector_const_shared_ptr>> local_node_on_edge_storage_;

        /*!
         * \brief Local nodes on faces stored with their topologic index as a key.
         *
         * For each geometric key there is an array of several elements: one for each orientation.
         * For each geometric key/orientation, there is the list of all nodes present.
         */
        std::vector<std::vector<Advanced::LocalNode::vector_const_shared_ptr>> local_node_on_face_storage_;

        //! Subset of local_node_list_ that includes only nodes on volume.
        Advanced::LocalNode::vector_const_shared_ptr local_node_on_volume_list_;

        //! Dimension of the underlying topology.
        ::MoReFEM::GeometryNS::dimension_type topology_dimension_ =
            NumericNS::UninitializedIndex<::MoReFEM::GeometryNS::dimension_type>();
    };


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELT_DOT_HPP_
// *** MoReFEM end header guards *** < //
