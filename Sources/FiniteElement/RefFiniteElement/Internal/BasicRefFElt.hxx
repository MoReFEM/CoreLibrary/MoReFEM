// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELT_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELT_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Miscellaneous.hpp"

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    /////////////////
    // CONSTRUCTOR //
    /////////////////

    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    void BasicRefFElt::Init()
    {
        local_node_list_ = ComputeLocalNodeList();

        topology_dimension_ = TopologyT::dimension;

        Advanced::LocalNode::vector_const_shared_ptr local_node_on_edge_list;
        Advanced::LocalNode::vector_const_shared_ptr local_node_on_face_list;

        {
            const auto& local_node_list = GetLocalNodeList();

            for (const auto& local_node_ptr : local_node_list)
            {
                assert(!(!local_node_ptr));
                const auto interface_nature = local_node_ptr->GetLocalInterface().GetNature();

                switch (interface_nature)
                {
                case ::MoReFEM::InterfaceNS::Nature::vertex:
                    local_node_on_vertex_list_.push_back(local_node_ptr);
                    break;
                case ::MoReFEM::InterfaceNS::Nature::edge:
                    local_node_on_edge_list.push_back(local_node_ptr);
                    break;
                case ::MoReFEM::InterfaceNS::Nature::face:
                    local_node_on_face_list.push_back(local_node_ptr);
                    break;
                case ::MoReFEM::InterfaceNS::Nature::volume:
                    local_node_on_volume_list_.push_back(local_node_ptr);
                    break;
                case ::MoReFEM::InterfaceNS::Nature::none:
                case ::MoReFEM::InterfaceNS::Nature::undefined:
                    assert(false);
                    exit(EXIT_FAILURE);
                }
            }
        }

        if (!local_node_on_vertex_list_.empty())
            SortLocalNodeOnVertexList<TopologyT>();

        if constexpr (Advanced::Concept::TopologyNS::with_edge<TopologyT>)
            SetEdgeLocalNodeList<TopologyT>(local_node_on_edge_list);

        if constexpr (Advanced::Concept::TopologyNS::with_face<TopologyT>)
            SetFaceLocalNodeList<TopologyT>(local_node_on_face_list);
    }


    ////////////////////////////////////////////////
    // ACCESSORS TO NODES REGARDLESS OF INTERFACE //
    ////////////////////////////////////////////////

    inline auto BasicRefFElt::NlocalNode() const noexcept -> LocalNodeNS::index_type
    {
        return LocalNodeNS::index_type{ static_cast<Eigen::Index>(local_node_list_.size()) };
    }


    inline const Advanced::LocalNode::vector_const_shared_ptr& BasicRefFElt::GetLocalNodeList() const noexcept
    {
        return local_node_list_;
    }


    inline const Advanced::LocalNode& BasicRefFElt::GetLocalNode(LocalNodeNS::index_type index) const noexcept
    {
        const auto& list = GetLocalNodeList();
        const auto size_t_index = static_cast<std::size_t>(index.Get());

        assert(size_t_index < list.size());
        assert(!(!(list[size_t_index])));
        return *list[size_t_index];
    }


    ////////////////////////////////////
    // ACCESSORS TO NODES ON VERTICES //
    ////////////////////////////////////


    inline bool BasicRefFElt::AreNodesOnVertices() const noexcept
    {
        return NnodeOnVertex() > Eigen::Index{};
    }


    inline const Advanced::LocalNode::const_shared_ptr&
    BasicRefFElt ::GetLocalNodeOnVertexPtr(Eigen::Index topologic_vertex_index) const noexcept
    {
        const auto& list = GetLocalNodeOnVertexList();
        const std::size_t index = static_cast<std::size_t>(topologic_vertex_index);
        assert(index < list.size());
        assert(!(!(list[index])));
        assert("Make sure the index given in input is relevant."
               && list[index]->GetLocalInterface().GetNature() == ::MoReFEM::InterfaceNS::Nature::vertex);
        return list[index];
    }


    inline const Advanced::LocalNode&
    BasicRefFElt::GetLocalNodeOnVertex(Eigen::Index topologic_vertex_index) const noexcept
    {
        return *GetLocalNodeOnVertexPtr(topologic_vertex_index);
    }


    inline const Advanced::LocalNode::vector_const_shared_ptr& BasicRefFElt::GetLocalNodeOnVertexList() const noexcept
    {
#ifndef NDEBUG
        CheckLocalNodeListConsistency(local_node_on_vertex_list_, ::MoReFEM::InterfaceNS::Nature::vertex);
#endif // NDEBUG
        return local_node_on_vertex_list_;
    }


    /////////////////////////////////
    // ACCESSORS TO NODES ON EDGES //
    /////////////////////////////////


    inline bool BasicRefFElt::AreNodesOnEdges() const noexcept
    {
        return NnodeOnEdge() > Eigen::Index{};
    }


    inline const std::vector<Advanced::LocalNode::vector_const_shared_ptr>&
    BasicRefFElt ::GetLocalNodeOnEdgeList(Eigen::Index topologic_edge_index) const noexcept
    {
        const std::size_t index = static_cast<std::size_t>(topologic_edge_index);
        assert(index < local_node_on_edge_storage_.size());
        return local_node_on_edge_storage_[index];
    }


    inline const Advanced::LocalNode::vector_const_shared_ptr&
    BasicRefFElt ::GetLocalNodeOnEdgeList(Eigen::Index topologic_edge_index,
                                          Advanced::InterfaceNS::orientation_type orientation) const noexcept
    {
        const auto& edge_content = GetLocalNodeOnEdgeList(topologic_edge_index);

        assert(edge_content.size() > orientation.Get());
        assert(!edge_content[orientation.Get()].empty());

        return edge_content[orientation.Get()];
    }


    /////////////////////////////////
    // ACCESSORS TO NODES ON FACES //
    /////////////////////////////////

    inline bool BasicRefFElt::AreNodesOnFaces() const noexcept
    {
        return NnodeOnFace() > Eigen::Index{};
    }


    inline const std::vector<Advanced::LocalNode::vector_const_shared_ptr>&
    BasicRefFElt ::GetLocalNodeOnFaceList(Eigen::Index topologic_face_index) const noexcept
    {
        const std::size_t index = static_cast<std::size_t>(topologic_face_index);
        assert(index < local_node_on_face_storage_.size());
        return local_node_on_face_storage_[index];
    }


    inline const Advanced::LocalNode::vector_const_shared_ptr&
    BasicRefFElt ::GetLocalNodeOnFaceList(Eigen::Index topologic_face_index,
                                          Advanced::InterfaceNS::orientation_type orientation) const noexcept
    {
        const auto& face_content = GetLocalNodeOnFaceList(topologic_face_index);
        assert(face_content.size() > orientation.Get());
        assert(!face_content[orientation.Get()].empty());

        return face_content[orientation.Get()];
    }


    //////////////////////////////////
    // ACCESSORS TO NODES ON VOLUME //
    //////////////////////////////////

    inline bool BasicRefFElt::AreNodesOnVolume() const noexcept
    {
        return !local_node_on_volume_list_.empty();
    }


    inline const Advanced::LocalNode::vector_const_shared_ptr& BasicRefFElt::GetLocalNodeOnVolumeList() const noexcept
    {
#ifndef NDEBUG
        CheckLocalNodeListConsistency(local_node_on_volume_list_, ::MoReFEM::InterfaceNS::Nature::volume);
#endif // NDEBUG

        return local_node_on_volume_list_;
    }


    inline auto BasicRefFElt::GetTopologyDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return topology_dimension_;
    }


    /////////////////////
    // PRIVATE METHODS //
    /////////////////////


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    void BasicRefFElt::SortLocalNodeOnVertexList()
    {
        Advanced::LocalNode::vector_const_shared_ptr sorted_node_on_vertex_list(TopologyT::Nvertex, nullptr);

        const auto& unsorted_node_on_vertex_list = GetLocalNodeOnVertexList();

        for (const auto& local_node_ptr : unsorted_node_on_vertex_list)
        {
            for (std::size_t topologic_vertex_index = 0UL; topologic_vertex_index < TopologyT::Nvertex;
                 ++topologic_vertex_index)
            {
                assert(!(!local_node_ptr));
                if (TopologyT::IsOnVertex(topologic_vertex_index, local_node_ptr->GetLocalCoords()))
                {
                    assert(sorted_node_on_vertex_list[topologic_vertex_index] == nullptr);
                    sorted_node_on_vertex_list[topologic_vertex_index] = local_node_ptr;
                }
            }
        }

        assert(std::none_of(sorted_node_on_vertex_list.cbegin(),
                            sorted_node_on_vertex_list.cend(),
                            Utilities::IsNullptr<Advanced::LocalNode::const_shared_ptr>));

        std::swap(sorted_node_on_vertex_list, local_node_on_vertex_list_);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::with_edge TopologyT>
    void BasicRefFElt::SetEdgeLocalNodeList(const Advanced::LocalNode::vector_const_shared_ptr& local_node_on_edge_list)
    {
        // If no local dofs on edge, nothing to do.
        if (local_node_on_edge_list.empty())
            return;

        local_node_on_edge_storage_.resize(TopologyT::Nedge);

        Advanced::LocalNode::vector_const_shared_ptr ret;

        constexpr std::size_t Nedge_orientation = 2UL;

        for (std::size_t topologic_edge_index = 0UL; topologic_edge_index < TopologyT::Nedge; ++topologic_edge_index)
        {
            auto& edge_content = local_node_on_edge_storage_[topologic_edge_index];
            edge_content.resize(Nedge_orientation);

            for (auto orientation = 0UL; orientation < Nedge_orientation; ++orientation)
            {
                auto& local_dof_on_current_edge_list = edge_content[orientation];

                for (const auto& local_node_ptr : local_node_on_edge_list)
                {
                    assert(!(!local_node_ptr));

                    if (TopologyT::IsOnEdge(topologic_edge_index, local_node_ptr->GetLocalCoords()))
                        local_dof_on_current_edge_list.push_back(local_node_ptr);
                }

                assert(!local_dof_on_current_edge_list.empty());

                if (orientation == 1UL)
                    std::reverse(local_dof_on_current_edge_list.begin(), local_dof_on_current_edge_list.end());
            }

            assert(edge_content.size() == Nedge_orientation);
        }
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::with_face TopologyT>
    void BasicRefFElt::SetFaceLocalNodeList(const Advanced::LocalNode::vector_const_shared_ptr& local_node_on_face_list)
    {
        // If no local dofs on faces, nothing to do.
        if (local_node_on_face_list.empty())
            return;

        local_node_on_face_storage_.resize(TopologyT::Nface);


        Advanced::LocalNode::vector_const_shared_ptr ret;

        // \todo #250 This won't work for elements with different type of faces... (pyramids for instance)
        constexpr auto Nface_orientation =
            Advanced::InterfaceNS::orientation_type{ 2UL * TopologyT::FaceTopology::Nvertex };

        for (std::size_t topologic_face_index = 0UL; topologic_face_index < TopologyT::Nface; ++topologic_face_index)
        {
            Advanced::LocalNode::vector_const_shared_ptr local_node_on_current_face_without_orientation;

            for (const auto& local_node_ptr : local_node_on_face_list)
            {
                assert(!(!local_node_ptr));

                if (TopologyT::IsOnFace(topologic_face_index, local_node_ptr->GetLocalCoords()))
                    local_node_on_current_face_without_orientation.push_back(local_node_ptr);
            }

            assert(!local_node_on_current_face_without_orientation.empty());

            assert(topologic_face_index < local_node_on_face_storage_.size());

            auto& face_content = local_node_on_face_storage_[topologic_face_index];
            face_content.resize(Nface_orientation.Get());

            assert(face_content.size() == Nface_orientation.Get());

            for (auto orientation = Advanced::InterfaceNS::orientation_type{ 0UL }; orientation < Nface_orientation;
                 ++orientation)
            {
                auto& local_node_on_current_face = face_content[orientation.Get()];

                for (const auto& local_node_ptr : local_node_on_current_face_without_orientation)
                {
                    auto&& new_coords = TopologyT::TransformFacePoint(
                        local_node_ptr->GetLocalCoords(), topologic_face_index, orientation);

                    auto it = std::find_if(local_node_on_current_face_without_orientation.cbegin(),
                                           local_node_on_current_face_without_orientation.cend(),
                                           [&new_coords](const auto& local_node_in_list_ptr)
                                           {
                                               assert(!(!local_node_in_list_ptr));
                                               return local_node_in_list_ptr->GetLocalCoords() == new_coords;
                                           });

                    assert(it != local_node_on_current_face_without_orientation.cend());
                    local_node_on_current_face.push_back(*it);
                }
                assert(!local_node_on_current_face.empty());
            }
        }
    }


    inline Eigen::Index BasicRefFElt::NnodeOnVertex() const noexcept
    {
        return static_cast<Eigen::Index>(local_node_on_vertex_list_.size());
    }


    inline Eigen::Index BasicRefFElt::NnodeOnEdge() const noexcept
    {
        return static_cast<Eigen::Index>(local_node_on_edge_storage_.size());
    }


    inline Eigen::Index BasicRefFElt::NnodeOnFace() const noexcept
    {
        return static_cast<Eigen::Index>(local_node_on_face_storage_.size());
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELT_DOT_HXX_
// *** MoReFEM end header guards *** < //
