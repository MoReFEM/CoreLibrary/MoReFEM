// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <memory>
#include <sstream>
#include <string>

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"

#include "FiniteElement/RefFiniteElement/Internal/Exceptions/BasicRefFEltFactory.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    BasicRefFEltFactory::~BasicRefFEltFactory() = default;


    const std::string& BasicRefFEltFactory::ClassName()
    {
        static const std::string ret("BasicRefFEltFactory");
        return ret;
    }


    BasicRefFEltFactory::BasicRefFEltFactory()
    {
        basic_ref_felt_storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    const BasicRefFElt& BasicRefFEltFactory::FetchBasicRefFElt(const std::string& topology_name,
                                                               const std::string& shape_function_label) const
    {
        auto&& key = GenerateKey(topology_name, shape_function_label);

        auto it = basic_ref_felt_storage_.find(key);

        if (it == basic_ref_felt_storage_.cend())
            throw BasicRefFEltFactoryNS::ExceptionNS::UnregisteredObject(
                topology_name, shape_function_label, known_pair_);
        const auto& holder_ptr = it->second;
        assert(!(!holder_ptr));
        return holder_ptr->GetBasicRefFElt();
    }


    std::string BasicRefFEltFactory ::GenerateKey(const std::string& topology_name,
                                                  const std::string& shape_function_label)
    {
        std::ostringstream oconv;
        oconv << "topology=" << topology_name << "_shape_function=" << shape_function_label;

        std::string key(oconv.str());

        return key;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
