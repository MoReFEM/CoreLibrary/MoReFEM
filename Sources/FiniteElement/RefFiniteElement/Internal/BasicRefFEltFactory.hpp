// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELTFACTORY_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELTFACTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "FiniteElement/RefFiniteElement/Internal/Impl/BasicRefFEltHolder.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    /*!
     * \brief This object knows all the reference finite elements available to the program, and can be
     * used to retrieve them.
     *
     * Before the program starts, all the reference finite elements that can be of use must be properly
     * registered to the factory (through method Register() - see its dedicated comment for more details_).
     *
     * Then when we need to build a specific reference finite element (typically after reading the input
     * file in which user gave his specifications), they can be retrieved with method FetchBasicRefFElt().
     *
     * The elegancy of a factory is that it's easy to add a new kind of reference finite element: if you
     * need one not defined in the core of MoReFEM, you can register it from any cpp file without
     * having to modify in any way the library. If for instance you need anisotropic QuadrangleQ3-Q2, you can
     * add in your main.cpp:
     *
     * \code
     * namespace // anonymous
     * {
     * using QuadrangleQ3Q2 = RefFEltNS::Spectral<Advanced::RefGeomEltNS::TopologyNS::Quadrangle, 3, 2, 0>;
     *
     *    const bool register = Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
     * __LINE__).Register<QuadrangleQ3Q2>();
     * }
     * \endcode
     *
     * and you'll be able to summon it with FetchBasicRefFElt():
     *
     * \code
     * auto basic_ref_felt_ptr =
     * Internal::RefFEltNS::BasicRefFEltFactory::Instance().FetchBasicRefFElt("Quadrangle", "Q3-Q2"); // ok!
     * \endcode
     *
     * My example calls an existing reference finite element template Spectral, but it would be true with
     * a completely new class that the user would define by himself.
     *
     * \internal <b><tt>[internal]</tt></b> This class is a Singleton: it is important for its current use that
     * there is one and only one instance of it.
     * \endinternal
     *
     */
    class BasicRefFEltFactory final : public Utilities::Singleton<BasicRefFEltFactory>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();


      public:
        /*!
         * \brief Fetch the reference finite element that matches both the topology and the shape function
         * label.
         *
         * If none found, an exception is thrown.
         *
         * \param[in] topology_name Name of the topology, as given by the static method Name() in the
         * traits class RefGeomEltNS::TopologyNS with which the requested element was registered (see Register()
         * method for more details). \param[in] shape_function_label Label that characterizes the shape
         * function, e.g. 'P0', 'Q1', 'P1b', etc...
         *
         * \return Reference to the relevant \a BasicRefFElt.
         */
        const BasicRefFElt& FetchBasicRefFElt(const std::string& topology_name,
                                              const std::string& shape_function_label) const;


      public:
        /*!
         * \brief Register a reference finite element.
         *
         * \tparam BasicRefFEltT The BasicRefFElt instantiation to register. This class must hold two data:
         * - A static method named ShapeFunctionLabel().
         * - An alias named Topology that points to the adequate object defined in RefGeomEltNS::TopologyNS
        namespace.
         *
         * This function is intended to be called before the program starts, to do so write a code such as:
         *
         * \code
         * namespace // anonymous
         * {
         *
         *      [[maybe_unused]] const bool registered =  //NOLINT
        Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
        __LINE__).Register<TetrahedronP2>();
         *
         *
         * } // namespace // anonymous
         * \endcode
         *
         * in a cpp file.
         *
         * \return Always true: it is a conveniency to be able to use it to actually register a reference finite
         * element in an anonymous namespace.
         */
        template<class BasicRefFEltT>
        bool Register();


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        BasicRefFEltFactory();

        //! Destructor.
        virtual ~BasicRefFEltFactory() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<BasicRefFEltFactory>;
        ///@}


      private:
        /*!
         * \brief Generate a key from the topology name and the shape function label.
         *
         * \param[in] topology_name Name of the topology, as given by the static method Name() in the
         * traits class RefGeomEltNS::TopologyNS with which the requested element was registered.
         * \param[in] shape_function_label String that describes the shape function used.
         */
        static std::string GenerateKey(const std::string& topology_name, const std::string& shape_function_label);

      private:
        /*!
         * \brief Container in charge of storing the known BasicRefFElt.
         *
         * Key is amalgation of topology and shape function label.
         * Value is an object able to build the BasicRefFElt on demand.
         */
        std::unordered_map<std::string, Impl::AbstractBasicRefFEltHolder::unique_ptr> basic_ref_felt_storage_;

        //! User-friendly container that lists all the topology/shape function label registered.
        std::vector<std::pair<std::string, std::string>> known_pair_;
    };


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELTFACTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
