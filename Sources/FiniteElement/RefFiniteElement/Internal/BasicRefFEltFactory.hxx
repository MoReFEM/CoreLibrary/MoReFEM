// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELTFACTORY_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELTFACTORY_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"
// *** MoReFEM header guards *** < //


#include <memory>

#include "Utilities/Exceptions/Factory.hpp"

#include "FiniteElement/RefFiniteElement/Internal/Impl/BasicRefFEltHolder.hpp" // IWYU pragma: keep


namespace MoReFEM::Internal::RefFEltNS
{


    template<class BasicRefFEltT>
    bool BasicRefFEltFactory::Register()
    {
        using Topology = typename BasicRefFEltT::topology;

        auto&& key = GenerateKey(Topology::ClassName(), BasicRefFEltT::ShapeFunctionLabel());

        auto holder_ptr = std::make_unique<Impl::BasicRefFEltHolder<BasicRefFEltT>>();

        if (!basic_ref_felt_storage_.insert(make_pair(key, std::move(holder_ptr))).second)
        {
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(key, "topology name and shape function label"));
        }

        known_pair_.push_back({ Topology::ClassName(), BasicRefFEltT::ShapeFunctionLabel() });

        return true;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_BASICREFFELTFACTORY_DOT_HXX_
// *** MoReFEM end header guards *** < //
