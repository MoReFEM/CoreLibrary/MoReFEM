// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "FiniteElement/RefFiniteElement/Internal/Exceptions/BasicRefFEltFactory.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string UnregisteredObjectMsg(const std::string& topology_name,
                                      const std::string& shape_function_label,
                                      const std::vector<std::pair<std::string, std::string>>& known_pair_list);


} // namespace


namespace MoReFEM::Internal::RefFEltNS::BasicRefFEltFactoryNS::ExceptionNS
{

    Exception::Exception(const std::string& msg, const std::source_location location)
    : ::MoReFEM::ExceptionNS::Factory::Exception(msg, location)
    { }


    Exception::~Exception() = default;


    UnregisteredObject::~UnregisteredObject() = default;


    UnregisteredObject::UnregisteredObject(const std::string& topology_name,
                                           const std::string& shape_function_label,
                                           const std::vector<std::pair<std::string, std::string>>& known_pair_list,
                                           const std::source_location location)
    : Exception(UnregisteredObjectMsg(topology_name, shape_function_label, known_pair_list), location)
    { }


} // namespace MoReFEM::Internal::RefFEltNS::BasicRefFEltFactoryNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string UnregisteredObjectMsg(const std::string& topology_name,
                                      const std::string& shape_function_label,
                                      const std::vector<std::pair<std::string, std::string>>& known_pair_list)

    {
        std::ostringstream oconv;

        oconv << "The combination '" << topology_name << "' / '" << shape_function_label
              << "' was not found in the "
                 "factory. All the known combinations are: \n";

        for (const auto& pair : known_pair_list)
            oconv << "\t- '" << pair.first << "' / '" << pair.second << '\'' << '\n';


        return oconv.str();
    }


} // namespace


#include "Utilities/Exceptions/Factory.hpp"
