// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_EXCEPTIONS_BASICREFFELTFACTORY_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_EXCEPTIONS_BASICREFFELTFACTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <utility>
#include <vector>

#include "Utilities/Exceptions/Factory.hpp"


namespace MoReFEM::Internal::RefFEltNS::BasicRefFEltFactoryNS::ExceptionNS
{


    //! Generic exception for factory.
    class Exception : public ::MoReFEM::ExceptionNS::Factory::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message.
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Thrown when trying to create an object which key is not registered.
    class UnregisteredObject final : public Exception
    {
      public:
        /*!
         * \brief Thrown when trying to create an object which keu is not registered.
         *
         * \param[in] topology_name Name of the topology for which the problem arose.
         * For instance 'triangle'.
         * \param[in] shape_function_label Shape function label involved.
         * \param[in] known_pair_list List of all combination known by the factory.
         * \copydoc doxygen_hide_source_location

         */
        explicit UnregisteredObject(const std::string& topology_name,
                                    const std::string& shape_function_label,
                                    const std::vector<std::pair<std::string, std::string>>& known_pair_list,
                                    const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~UnregisteredObject() override;

        //! \copydoc doxygen_hide_copy_constructor
        UnregisteredObject(const UnregisteredObject& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnregisteredObject(UnregisteredObject&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnregisteredObject& operator=(const UnregisteredObject& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnregisteredObject& operator=(UnregisteredObject&& rhs) = default;
    };


} // namespace MoReFEM::Internal::RefFEltNS::BasicRefFEltFactoryNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_EXCEPTIONS_BASICREFFELTFACTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
