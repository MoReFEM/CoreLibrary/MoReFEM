// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_IMPL_BASICREFFELTHOLDER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_IMPL_BASICREFFELTHOLDER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM::Internal::RefFEltNS::Impl
{


    /*!
     * \brief A class which sole purpose is to hold the creation of a BasicRefFElt until it is required.
     *
     * All the reference finite elements that are available must be stored within the BasicRefFEltFactory.
     * The issue is that a BasicRefFElt can be quite roomy in memory (peculiarly for high order cases)
     * whereas in fact only a handful of them will be used for a given problem.
     *
     * The solution is provided here with the proverbial extra level of indirection: what the factory
     * actually stores is an object which includes only a single pointer. The first time the
     * underlying BasicRefFElt is required, the class is in charge of building it. If already built,
     * the existing object is fetched.
     *
     * This way, we combine the following advantages:
     * - Almost no extra memory used to store BasicRefFElt unused in the problem (more exactly the cost
     * of about one pointer per BasicRefFElt available).
     * - No duplication of a same BasicRefFElt: if two unknowns or two finite element spaces use the same
     * BasicRefFElt the same underlying object is used.
     *
     * \internal <b><tt>[internal]</tt></b> This class is an abstract class there to provide a same
     * base-class for all BasicRefFElt; the actual work is in fact done in what should be its sole child:
     * BasicRefFEltHolder.
     */
    class AbstractBasicRefFEltHolder
    {
      public:
        //! Alias over unique_ptr.
        using unique_ptr = std::unique_ptr<AbstractBasicRefFEltHolder>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        explicit AbstractBasicRefFEltHolder() = default;

        //! Destructor.
        virtual ~AbstractBasicRefFEltHolder();

        //! \copydoc doxygen_hide_copy_constructor
        AbstractBasicRefFEltHolder(const AbstractBasicRefFEltHolder& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        AbstractBasicRefFEltHolder(AbstractBasicRefFEltHolder&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        AbstractBasicRefFEltHolder& operator=(const AbstractBasicRefFEltHolder& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AbstractBasicRefFEltHolder& operator=(AbstractBasicRefFEltHolder&& rhs) = delete;


        ///@}


      public:
        /*!
         * \brief The first time this method is called, the underlying BasicRefFElt is actually generated.
         */
        virtual const BasicRefFElt& GetBasicRefFElt() = 0;
    };


    /*!
     * \brief The class that instantiates a concrete BasicRefFEltHolder.
     *
     * See AbstractBasicRefFEltHolder to get the point of this class.
     */
    template<class BasicRefFEltT>
    class BasicRefFEltHolder final : public AbstractBasicRefFEltHolder
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        explicit BasicRefFEltHolder();

        //! Destructor.
        virtual ~BasicRefFEltHolder() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        BasicRefFEltHolder(const BasicRefFEltHolder& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        BasicRefFEltHolder(BasicRefFEltHolder&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        BasicRefFEltHolder& operator=(const BasicRefFEltHolder& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        BasicRefFEltHolder& operator=(BasicRefFEltHolder&& rhs) = delete;


        ///@}


      public:
        /*!
         * \brief The first time this method is called, the underlying BasicRefFElt is actually generated.
         */
        virtual const BasicRefFElt& GetBasicRefFElt() override final;


      private:
        /*!
         * \brief The underlying BasicRefFElt. Nullptr as long as the program didn't need an actual
         * instantiation.
         */
        BasicRefFElt::const_unique_ptr basic_ref_felt_ = nullptr;
    };


} // namespace MoReFEM::Internal::RefFEltNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Internal/Impl/BasicRefFEltHolder.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_IMPL_BASICREFFELTHOLDER_DOT_HPP_
// *** MoReFEM end header guards *** < //
