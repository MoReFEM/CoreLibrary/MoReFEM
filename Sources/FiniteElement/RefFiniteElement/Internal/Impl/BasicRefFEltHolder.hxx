// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_IMPL_BASICREFFELTHOLDER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_IMPL_BASICREFFELTHOLDER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/Impl/BasicRefFEltHolder.hpp"
// *** MoReFEM header guards *** < //


#include <memory>

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM::Internal::RefFEltNS::Impl
{


    template<class BasicRefFEltT>
    BasicRefFEltHolder<BasicRefFEltT>::BasicRefFEltHolder() : basic_ref_felt_(nullptr)
    { }


    template<class BasicRefFEltT>
    const BasicRefFElt& BasicRefFEltHolder<BasicRefFEltT>::GetBasicRefFElt()
    {
        // If not already existing, built it now!
        if (basic_ref_felt_ == nullptr)
            basic_ref_felt_ = std::make_unique<BasicRefFEltT>();

        return *basic_ref_felt_;
    }


} // namespace MoReFEM::Internal::RefFEltNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_IMPL_BASICREFFELTHOLDER_DOT_HXX_
// *** MoReFEM end header guards *** < //
