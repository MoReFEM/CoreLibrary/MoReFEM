// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_CRTP_CASE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_CRTP_CASE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
#include "Geometry/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM::Advanced::InfosAtQuadPointNS { class ForUnknownList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::Crtp
{


    /*!
     * \brief CRTP that provides the common interface of all the cases  required to compute the
     * \a LocalFEltSpace data for a given (\a GeometricElt, \a QuadraturePoint) pair.
     */
    template<class DerivedT>
    class Case
    {

      public:
        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_compute_felt_space_data_variant_constructor
        explicit Case(GeometryNS::dimension_type mesh_dimension);

        //! Destructor.
        ~Case() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Case(const Case& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Case(Case&& rhs) noexcept = default;

        //! \copydoc doxygen_hide_copy_affectation
        Case& operator=(const Case& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Case& operator=(Case&& rhs) noexcept = delete;

        ///@}

      protected:
        //! Constant accessor to the helper class to compute jacobian.
        const Advanced::GeomEltNS::ComputeJacobian& GetComputeJacobianHelper() const noexcept;

        //! Non constant accessor to the helper class to compute jacobian.
        Advanced::GeomEltNS::ComputeJacobian& GetNonCstComputeJacobianHelper() noexcept;

#ifndef NDEBUG
        //! Returns the dimension of the mesh. For sanity checks only!
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;
#endif // NDEBUG

      private:
        //! Helper class to compute jacobian.
        Advanced::GeomEltNS::ComputeJacobian::unique_ptr compute_jacobian_helper_{ nullptr };

#ifndef NDEBUG
        //! For sanity check!
        const GeometryNS::dimension_type mesh_dimension_;
#endif // NDEBUG
    };


} // namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/Crtp/Case.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_CRTP_CASE_DOT_HPP_
// *** MoReFEM end header guards *** < //
