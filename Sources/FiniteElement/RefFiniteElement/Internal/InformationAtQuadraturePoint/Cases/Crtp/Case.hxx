// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_CRTP_CASE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_CRTP_CASE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/Crtp/Case.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::Crtp
{


    template<class DerivedT>
    Case<DerivedT>::Case(GeometryNS::dimension_type mesh_dimension)
#ifndef NDEBUG
    : mesh_dimension_{ mesh_dimension }
#endif // NDEBUG
    {
        compute_jacobian_helper_ = std::make_unique<Advanced::GeomEltNS::ComputeJacobian>(mesh_dimension);
    }


    template<class DerivedT>
    inline auto Case<DerivedT>::GetComputeJacobianHelper() const noexcept -> const Advanced::GeomEltNS::ComputeJacobian&
    {
        assert(!(!compute_jacobian_helper_));
        return *compute_jacobian_helper_;
    }


    template<class DerivedT>
    inline auto Case<DerivedT>::GetNonCstComputeJacobianHelper() noexcept -> Advanced::GeomEltNS::ComputeJacobian&
    {
        return const_cast<Advanced::GeomEltNS::ComputeJacobian&>(GetComputeJacobianHelper());
    }

#ifndef NDEBUG
    template<class DerivedT>
    inline auto Case<DerivedT>::GetMeshDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return mesh_dimension_;
    }
#endif // NDEBUG


} // namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS::Crtp

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_CRTP_CASE_DOT_HXX_
// *** MoReFEM end header guards *** < //
