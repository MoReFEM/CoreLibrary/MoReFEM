// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_LOWERDIMENSIONDIMENSION0_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_LOWERDIMENSIONDIMENSION0_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/Enum.hpp"
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/Crtp/Case.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced::InfosAtQuadPointNS { class ForUnknownList; }
namespace MoReFEM::TestNS { struct AccessForUnknownList; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS
{

    /*!
     * \copydoc doxygen_hide_compute_felt_space_data_variant_common_description
     *
     * Current class covers the case where \a RefGeomElt is a \a Point.
     */

    class LowerDimensionDimension0 : public Crtp::Case<LowerDimensionDimension0>
    {
      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LowerDimensionDimension0;

        //! Friendship to CRTP parent.
        using parent = Crtp::Case<LowerDimensionDimension0>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the main class for which current one is devised.
        friend class MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList;

        //! Friendship to use only for tests.
        friend MoReFEM::TestNS::AccessForUnknownList;

      public:
        /// \name Special members.
        ///@{
        //! \copydoc doxygen_hide_compute_felt_space_data_variant_constructor
        explicit LowerDimensionDimension0(GeometryNS::dimension_type mesh_dimension);

        //! Destructor.
        ~LowerDimensionDimension0() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LowerDimensionDimension0(const LowerDimensionDimension0& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LowerDimensionDimension0(LowerDimensionDimension0&& rhs) noexcept = default;

        //! \copydoc doxygen_hide_copy_affectation
        LowerDimensionDimension0& operator=(const LowerDimensionDimension0& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LowerDimensionDimension0& operator=(LowerDimensionDimension0&& rhs) noexcept = delete;

        ///@}

      public:
        //! \copydoc doxygen_hide_compute_felt_space_data_variant_compute
        double Compute(const LocalFEltSpace& local_felt_space,
                       Advanced::InfosAtQuadPointNS::ForUnknownList& for_unknown_list);
    };


} // namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_LOWERDIMENSIONDIMENSION0_DOT_HPP_
// *** MoReFEM end header guards *** < //
