// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <variant>

#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/LowerDimensionDimension1.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Geometry/StrongType.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: keep
#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp"


namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS
{


    LowerDimensionDimension1::LowerDimensionDimension1(GeometryNS::dimension_type mesh_dimension)
    : parent(mesh_dimension)
    { }


    double LowerDimensionDimension1::Compute(const LocalFEltSpace& local_felt_space,
                                             Advanced::InfosAtQuadPointNS::ForUnknownList& for_unknown_list)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        assert(for_unknown_list.GetMeshDimension() == GetMeshDimension());

        decltype(auto) geom_elt = local_felt_space.GetGeometricElt();
        assert(geom_elt.GetDimension() < GetMeshDimension());

        decltype(auto) quad_pt = for_unknown_list.GetQuadraturePoint();
        decltype(auto) jacobian = GetNonCstComputeJacobianHelper().Compute(geom_elt, quad_pt);

        return std::visit(
            [](auto& variant) -> double
            {
                return variant(Eigen::seq(0, 1), 0).norm();
            },
            jacobian);
    }


} // namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
