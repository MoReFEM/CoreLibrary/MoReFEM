// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <variant>

#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/SameDimensionWithGradient.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: keep
#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp"


namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS
{


    namespace // anonymous
    {

        //! Convenient alias.
        using matrix_type = Advanced::RefFEltNS::gradient_matrix_type;

        /*!
         * \brief Compute gradient matrix for current finite element.
         *
         * \param[in] gradient_ref_felt_phi Gradient **reference** finite element matrix.
         * \param[in] jacobian Jacobian matrix.
         * \param[in,out] out The resulted matrix. It is expected the matrix has already been properly allocated.
         *
         * \tparam EigenMatrixT The version of `DimensionMatrixVariant` to consider, depending on mesh dimension.
         */
        void ComputeGradFeltPhi(const matrix_type& grad_ref_felt_phi,
                                const ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant& jacobian,
                                matrix_type& out);


    } // namespace


    SameDimensionWithGradient::SameDimensionWithGradient(GeometryNS::dimension_type mesh_dimension)
    : parent(mesh_dimension)
    { }


    double SameDimensionWithGradient::Compute(const LocalFEltSpace& local_felt_space,
                                              Advanced::InfosAtQuadPointNS::ForUnknownList& for_unknown_list)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        assert(for_unknown_list.GetMeshDimension() == GetMeshDimension());

        decltype(auto) geom_elt = local_felt_space.GetGeometricElt();
        decltype(auto) quad_pt = for_unknown_list.GetQuadraturePoint();
        decltype(auto) jacobian = GetNonCstComputeJacobianHelper().Compute(geom_elt, quad_pt);


        decltype(auto) grad_ref_felt_phi = for_unknown_list.GetGradientRefFEltPhi();
        decltype(auto) grad_felt_phi = GetNonCstGradientFEltPhi();

        ComputeGradFeltPhi(grad_ref_felt_phi, jacobian, grad_felt_phi);

        return ::MoReFEM::Wrappers::EigenNS::ComputeDeterminant(jacobian);
    }


    // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
    void SameDimensionWithGradient::Resize(Eigen::Index Nrow, Eigen::Index Ncol)
    {
        gradient_felt_phi_.resize(Nrow, Ncol);
    }


    namespace // anonymous
    {


        void ComputeGradFeltPhi(const matrix_type& grad_ref_felt_phi,
                                const ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant& jacobian,
                                matrix_type& out)
        {
            std::visit(
                [&out, &grad_ref_felt_phi](const auto& jacobian_variant)
                {
                    out.noalias() = grad_ref_felt_phi * jacobian_variant.inverse();
                },
                jacobian);
        }


    } // namespace


} // namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
