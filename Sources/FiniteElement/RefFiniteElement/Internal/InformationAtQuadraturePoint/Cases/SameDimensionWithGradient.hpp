// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_SAMEDIMENSIONWITHGRADIENT_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_SAMEDIMENSIONWITHGRADIENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/Alias.hpp"
#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/Enum.hpp"
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/Crtp/Case.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced::InfosAtQuadPointNS { class ForUnknownList; }
namespace MoReFEM::TestNS { struct AccessForUnknownList; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS
{


    /*!
     * \copydoc doxygen_hide_compute_felt_space_data_variant_common_description
     *
     * Current class covers the case where \a RefGeomElt is of the same dimension as the highest present in the mesh
     * and local gradient matrix is required.
     */
    class SameDimensionWithGradient : public Crtp::Case<SameDimensionWithGradient>
    {
      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SameDimensionWithGradient;

        //! Friendship to CRTP parent.
        using parent = Crtp::Case<SameDimensionWithGradient>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Convenient alias.
        using matrix_type = Advanced::RefFEltNS::gradient_matrix_type;

        //! Friendship to the main class for which current one is devised.
        friend class MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList;

        //! Friendship to use only for tests.
        friend MoReFEM::TestNS::AccessForUnknownList;

      public:
        /// \name Special members.
        ///@{
        //! \copydoc doxygen_hide_compute_felt_space_data_variant_constructor
        explicit SameDimensionWithGradient(GeometryNS::dimension_type mesh_dimension);

        //! Destructor.
        ~SameDimensionWithGradient() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SameDimensionWithGradient(const SameDimensionWithGradient& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SameDimensionWithGradient(SameDimensionWithGradient&& rhs) noexcept = default;

        //! \copydoc doxygen_hide_copy_affectation
        SameDimensionWithGradient& operator=(const SameDimensionWithGradient& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SameDimensionWithGradient& operator=(SameDimensionWithGradient&& rhs) noexcept = delete;

        ///@}

      public:
        //! \copydoc doxygen_hide_compute_felt_space_data_variant_compute
        double Compute(const LocalFEltSpace& local_felt_space,
                       Advanced::InfosAtQuadPointNS::ForUnknownList& for_unknown_list);

        /*!
         * \brief Get the first derivative of the finite element shape function value at each node of the
         * finite element (not the reference one).
         *
         * The value returns is the one matching the last \a LocalFEltSpace associated to the object
         * through ComputeLocalFEltSpaceData() method.
         *
         * By convention rows stand for the local_node_index considered, and columns for the component against
         * which derivation is performed.
         *
         * \return First derivative of the finite element shape function value at each node of the
         * finite element (not the reference one).
         */
        const matrix_type& GetGradientFEltPhi() const noexcept;


      private:
        /*!
         * \brief Allocate the memory for the matrix.
         *
         * This method should be called automatically in \a ForUnknownList.
         *
         * \param[in] Nrow Number of rows.
         * \param[in] Ncol Number of columns.
         */
        void Resize(Eigen::Index Nrow, Eigen::Index Ncol);

        /*!
         * \brief Non constant accessor to gradient matrix for current \a FElt.
         *
         * \return Gradient matrix for the current \a FElt.
         */
        matrix_type& GetNonCstGradientFEltPhi() noexcept;


      private:
        /*!
         * \brief Gradient matrix for the current \a FElt.
         */
        matrix_type gradient_felt_phi_;
    };


} // namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/SameDimensionWithGradient.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_SAMEDIMENSIONWITHGRADIENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
