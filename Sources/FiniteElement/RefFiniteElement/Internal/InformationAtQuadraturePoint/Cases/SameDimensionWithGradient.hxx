// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_SAMEDIMENSIONWITHGRADIENT_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_SAMEDIMENSIONWITHGRADIENT_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/SameDimensionWithGradient.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS
{


    inline auto SameDimensionWithGradient ::GetGradientFEltPhi() const noexcept -> const matrix_type&
    {
        assert(gradient_felt_phi_.rows() > 0 && "If not, `Resize()` has not been called properly!");
        return gradient_felt_phi_;
    }


    inline auto SameDimensionWithGradient::GetNonCstGradientFEltPhi() noexcept -> matrix_type&
    {
        return const_cast<matrix_type&>(GetGradientFEltPhi());
    }


} // namespace MoReFEM::Internal::InfosAtQuadPointNS::ComputeLocalFEltSpaceDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_CASES_SAMEDIMENSIONWITHGRADIENT_DOT_HXX_
// *** MoReFEM end header guards *** < //
