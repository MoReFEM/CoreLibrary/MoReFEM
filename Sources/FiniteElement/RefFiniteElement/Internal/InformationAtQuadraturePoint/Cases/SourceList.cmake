### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/LowerDimensionDimension0.cpp
		${CMAKE_CURRENT_LIST_DIR}/LowerDimensionDimension1.cpp
		${CMAKE_CURRENT_LIST_DIR}/LowerDimensionDimension2.cpp
		${CMAKE_CURRENT_LIST_DIR}/SameDimensionNoGradient.cpp
		${CMAKE_CURRENT_LIST_DIR}/SameDimensionWithGradient.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/LowerDimensionDimension0.hpp
		${CMAKE_CURRENT_LIST_DIR}/LowerDimensionDimension1.hpp
		${CMAKE_CURRENT_LIST_DIR}/LowerDimensionDimension2.hpp
		${CMAKE_CURRENT_LIST_DIR}/SameDimensionNoGradient.hpp
		${CMAKE_CURRENT_LIST_DIR}/SameDimensionWithGradient.hpp
		${CMAKE_CURRENT_LIST_DIR}/SameDimensionWithGradient.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Crtp/SourceList.cmake)
