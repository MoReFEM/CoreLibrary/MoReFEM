// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_COMPUTELOCALFELTSPACEDATAVARIANT_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_COMPUTELOCALFELTSPACEDATAVARIANT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <variant>

// IWYU pragma: begin_exports
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/LowerDimensionDimension0.hpp"
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/LowerDimensionDimension1.hpp"
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/LowerDimensionDimension2.hpp"
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/SameDimensionNoGradient.hpp"
#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/Cases/SameDimensionWithGradient.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::Internal::InfosAtQuadPointNS
{

    //! Variant over the different cases we might have to cover while computing local data for a given (\a
    //! LocalFEltSpace, \a QuadraturePoint).
    // clang-format off
    using compute_local_felt_space_data_variant_type =
    std::variant
    <        
        ComputeLocalFEltSpaceDataNS::SameDimensionNoGradient,
        ComputeLocalFEltSpaceDataNS::SameDimensionWithGradient,
        ComputeLocalFEltSpaceDataNS::LowerDimensionDimension2,
        ComputeLocalFEltSpaceDataNS::LowerDimensionDimension1,
        ComputeLocalFEltSpaceDataNS::LowerDimensionDimension0
    >;
    // clang-format on

} // namespace MoReFEM::Internal::InfosAtQuadPointNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Internal/InformationAtQuadraturePoint/ComputeLocalFEltSpaceDataVariant.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_INFORMATIONATQUADRATUREPOINT_COMPUTELOCALFELTSPACEDATAVARIANT_DOT_HPP_
// *** MoReFEM end header guards *** < //
