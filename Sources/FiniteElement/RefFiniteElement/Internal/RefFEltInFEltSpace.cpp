// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"

#include "Geometry/StrongType.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    RefFEltInFEltSpace::RefFEltInFEltSpace(const BasicRefFElt& basic_ref_felt,
                                           const ExtendedUnknown& extended_unknown,
                                           const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                           const ::MoReFEM::GeometryNS::dimension_type felt_space_dimension)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : basic_ref_felt_(basic_ref_felt), extended_unknown_(extended_unknown), mesh_dimension_(mesh_dimension),
      felt_space_dimension_(felt_space_dimension)
    { }


    Eigen::Index RefFEltInFEltSpace::Ndof() const noexcept
    {
        const auto& unknown = GetExtendedUnknown().GetUnknown();
        const auto Nlocal_node = static_cast<Eigen::Index>(GetBasicRefFElt().NlocalNode());

        switch (unknown.GetNature())
        {
        case UnknownNS::Nature::scalar:
            return Nlocal_node;
        case UnknownNS::Nature::vectorial:
            return Nlocal_node * GetMeshDimension().Get();
        }

        assert(false);
        return NumericNS::UninitializedIndex<Eigen::Index>();
    }


    ::MoReFEM::GeometryNS::dimension_type RefFEltInFEltSpace::Ncomponent() const
    {
        const auto unknown_nature = GetExtendedUnknown().GetUnknown().GetNature();

        switch (unknown_nature)
        {
        case UnknownNS::Nature::scalar:
            return ::MoReFEM::GeometryNS::dimension_type{ 1UL };
        case UnknownNS::Nature::vectorial:
            return ::MoReFEM::GeometryNS::dimension_type{ GetMeshDimension() };
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
