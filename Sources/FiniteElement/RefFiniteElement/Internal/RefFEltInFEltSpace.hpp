// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFFELTINFELTSPACE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFFELTINFELTSPACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/RefGeometricElt/Advanced/LocalNode/Alias.hpp"
#include "Geometry/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    /*!
     * \brief RefFEltInFEltSpace is the description of a finite element related to a specific RefGeomElt,
     * a specific Unknown and a numbering subset.
     *
     * For instance, RefFEltInFEltSpace could describe the tuple ('Triangle3', 'Scalar unknown',
     * 'numbering subset 1').
     *
     * \internal <b><tt>[internal]</tt></b> This class is intended to work within the FEltSpace and should be
     * manipulated by a library developer; it basically enrich the BasicRefFElt with some data related to a
     * \a FEltSpace. What a developer should rather use is a RefFEltInLocalOperator, which is built upon
     * current class and adds several data related to the operator in which it is intended to be used.
     * \endinternal
     *
     */
    class RefFEltInFEltSpace final
    {

      public:
        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const RefFEltInFEltSpace>;

        //! Alias to vector of shared_pointer.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] basic_ref_felt Description of the reference finite element.
         * \param[in] extended_unknown Unknown and numbering subset related to the finite element.
         * \param[in] mesh_dimension Highest dimension of the mesh onto which the finite element space is
         * defined. May be higher than \a felt_space_dimension. \param[in] felt_space_dimension Dimension of the
         * finite element space in which the new object is built.
         */
        explicit RefFEltInFEltSpace(const BasicRefFElt& basic_ref_felt,
                                    const ExtendedUnknown& extended_unknown,
                                    ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                    ::MoReFEM::GeometryNS::dimension_type felt_space_dimension);


        //! Destructor.
        ~RefFEltInFEltSpace() = default;

        //! \copydoc doxygen_hide_copy_constructor
        RefFEltInFEltSpace(const RefFEltInFEltSpace& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        RefFEltInFEltSpace(RefFEltInFEltSpace&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        RefFEltInFEltSpace& operator=(const RefFEltInFEltSpace& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RefFEltInFEltSpace& operator=(RefFEltInFEltSpace&& rhs) = delete;

        ///@}

      public:
        //! Access to the related BasicRefFElt.
        const BasicRefFElt& GetBasicRefFElt() const noexcept;

        //! Returns the number of nodes.
        LocalNodeNS::index_type Nnode() const noexcept;

        //! Returns the number of dofs.
        Eigen::Index Ndof() const noexcept;

        //! Access related Unknown/numbering subset pair.
        const ExtendedUnknown& GetExtendedUnknown() const noexcept;

        //! Access to the dimension of the mesg in which current finite element is built.
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;

        //! Access to the dimension of the finite element space for which current reference finite element is
        //! built.
        ::MoReFEM::GeometryNS::dimension_type GetFEltSpaceDimension() const noexcept;

        //! Returns the number of components: 1 if the unknown is scalar, dimension_ otherwise.
        ::MoReFEM::GeometryNS::dimension_type Ncomponent() const;


      private:
        /*!
         * \brief Access to the reference finite element.
         */
        const BasicRefFElt& basic_ref_felt_;

        //! Unknown/numbering subset pair.
        const ExtendedUnknown& extended_unknown_;

        /*!
         * \brief Highest dimension of the mesh onto which the finite element space is defined.
         *
         * May be higher than \a felt_space_dimension.
         */
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;

        //! Dimension of the finite element space for which current reference finite element is built.
        const ::MoReFEM::GeometryNS::dimension_type felt_space_dimension_;
    };


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFFELTINFELTSPACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
