// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFFELTINFELTSPACE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFFELTINFELTSPACE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    inline const BasicRefFElt& RefFEltInFEltSpace::GetBasicRefFElt() const noexcept
    {
        return basic_ref_felt_;
    }


    inline auto RefFEltInFEltSpace::Nnode() const noexcept -> LocalNodeNS::index_type
    {
        return GetBasicRefFElt().NlocalNode();
    }


    inline const ExtendedUnknown& RefFEltInFEltSpace::GetExtendedUnknown() const noexcept
    {
        return extended_unknown_;
    }


    inline auto RefFEltInFEltSpace::GetMeshDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return mesh_dimension_;
    }


    inline auto RefFEltInFEltSpace::GetFEltSpaceDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return felt_space_dimension_;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFFELTINFELTSPACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
