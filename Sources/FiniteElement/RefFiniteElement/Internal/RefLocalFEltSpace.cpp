// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"

namespace MoReFEM::Internal::RefFEltNS
{


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    RefLocalFEltSpace::RefLocalFEltSpace(const RefGeomElt::shared_ptr& ref_geom_elt,
                                         const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                         const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                         const ::MoReFEM::GeometryNS::dimension_type felt_space_dimension)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : ref_geom_elt_(ref_geom_elt), Ndof_{}
    {
        assert(!(!ref_geom_elt));
        assert(felt_space_dimension <= mesh_dimension && "To check order has not been inverted!");

        const auto& factory = Internal::RefFEltNS::BasicRefFEltFactory::GetInstance();

        for (const auto& extended_unknown_ptr : unknown_list)
        {
            assert(!(!extended_unknown_ptr));
            const auto& extended_unknown = *extended_unknown_ptr;

            const auto& basic_ref_felt =
                factory.FetchBasicRefFElt(ref_geom_elt->GetTopologyName(), extended_unknown.GetShapeFunctionLabel());

            auto&& fe_type_unknown = std::make_shared<RefFEltInFEltSpace>(
                basic_ref_felt, extended_unknown, mesh_dimension, felt_space_dimension);

            Ndof_ += fe_type_unknown->Ndof();
            Nnode_ += fe_type_unknown->Nnode();

            ref_felt_list_.emplace_back(std::move(fe_type_unknown));
        }
    }


    const RefFEltInFEltSpace& RefLocalFEltSpace::GetRefFElt(const FEltSpace& felt_space, const Unknown& unknown) const
    {
        const auto& extended_unknown = felt_space.GetExtendedUnknown(unknown);
        return GetRefFElt(extended_unknown);
    }


    bool RefLocalFEltSpace::IsInNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) list = GetRefFEltList();

        assert(!list.empty());

        const auto end = list.cend();

        const auto it =
            std::find_if(list.cbegin(),
                         end,
                         [&numbering_subset](const auto& ref_felt_ptr)
                         {
                             assert(!(!ref_felt_ptr));
                             return ref_felt_ptr->GetExtendedUnknown().GetNumberingSubset() == numbering_subset;
                         });

        return it != end;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
