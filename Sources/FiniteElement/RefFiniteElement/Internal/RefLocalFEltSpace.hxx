// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFLOCALFELTSPACE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFLOCALFELTSPACE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <numeric>
#include <type_traits> // IWYU pragma: keep

#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    inline const RefGeomElt& RefLocalFEltSpace::GetRefGeomElt() const noexcept
    {
        assert(!(!ref_geom_elt_));
        return *ref_geom_elt_;
    }


    inline auto RefLocalFEltSpace::Nnode() const noexcept -> LocalNodeNS::index_type
    {
        assert("Check the cached value reflects the actual content!"
               && Nnode_
                      == std::accumulate(ref_felt_list_.cbegin(),
                                         ref_felt_list_.cend(),
                                         LocalNodeNS::index_type{},
                                         [](LocalNodeNS::index_type sum, RefFEltInFEltSpace::const_shared_ptr ref_felt)
                                         {
                                             assert(!(!ref_felt));
                                             return sum + ref_felt->Nnode();
                                         }));

        return Nnode_;
    }


    inline Eigen::Index RefLocalFEltSpace::Ndof() const noexcept
    {
        assert("Check the cached value reflects the actual content!"
               && Ndof_
                      == std::accumulate(ref_felt_list_.cbegin(),
                                         ref_felt_list_.cend(),
                                         Eigen::Index{},
                                         [](Eigen::Index sum, RefFEltInFEltSpace::const_shared_ptr ref_felt)
                                         {
                                             assert(!(!ref_felt));
                                             return sum + ref_felt->Ndof();
                                         }));

        return Ndof_;
    }


    inline const RefFEltInFEltSpace& RefLocalFEltSpace ::GetRefFElt(const ExtendedUnknown& extended_unknown) const
    {
        auto it = std::find_if(ref_felt_list_.cbegin(),
                               ref_felt_list_.cend(),
                               [&extended_unknown](const RefFEltInFEltSpace::const_shared_ptr& ref_felt_ptr)
                               {
                                   assert(!(!ref_felt_ptr));
                                   const auto& ref_felt = *ref_felt_ptr;

                                   return ref_felt.GetExtendedUnknown() == extended_unknown;
                               });

        assert(it != ref_felt_list_.cend());
        assert(!(!*it));
        return *(*it);
    }


    inline const RefFEltInFEltSpace::vector_const_shared_ptr& RefLocalFEltSpace::GetRefFEltList() const noexcept
    {
        return ref_felt_list_;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFLOCALFELTSPACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
