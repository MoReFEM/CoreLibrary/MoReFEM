### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/BasicRefFElt.cpp
		${CMAKE_CURRENT_LIST_DIR}/BasicRefFEltFactory.cpp
		${CMAKE_CURRENT_LIST_DIR}/RefFEltInFEltSpace.cpp
		${CMAKE_CURRENT_LIST_DIR}/RefLocalFEltSpace.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/BasicRefFElt.hpp
		${CMAKE_CURRENT_LIST_DIR}/BasicRefFElt.hxx
		${CMAKE_CURRENT_LIST_DIR}/BasicRefFEltFactory.hpp
		${CMAKE_CURRENT_LIST_DIR}/BasicRefFEltFactory.hxx
		${CMAKE_CURRENT_LIST_DIR}/RefFEltInFEltSpace.hpp
		${CMAKE_CURRENT_LIST_DIR}/RefFEltInFEltSpace.hxx
		${CMAKE_CURRENT_LIST_DIR}/RefLocalFEltSpace.hpp
		${CMAKE_CURRENT_LIST_DIR}/RefLocalFEltSpace.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint/SourceList.cmake)
