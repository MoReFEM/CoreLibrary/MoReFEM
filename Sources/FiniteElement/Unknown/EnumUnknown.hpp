// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_UNKNOWN_ENUMUNKNOWN_DOT_HPP_
#define MOREFEM_FINITEELEMENT_UNKNOWN_ENUMUNKNOWN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>


namespace MoReFEM::UnknownNS
{

    /*!
     * \brief Possible values for the nature of a given Unknown.
     */
    enum class Nature { scalar = 0UL, vectorial = 1UL };


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const Nature rhs);


} // namespace MoReFEM::UnknownNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_UNKNOWN_ENUMUNKNOWN_DOT_HPP_
// *** MoReFEM end header guards *** < //
