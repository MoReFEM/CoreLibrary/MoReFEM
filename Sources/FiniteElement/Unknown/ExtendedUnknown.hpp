// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_UNKNOWN_EXTENDEDUNKNOWN_DOT_HPP_
#define MOREFEM_FINITEELEMENT_UNKNOWN_EXTENDEDUNKNOWN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Core/NumberingSubset/NumberingSubset.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/Unknown.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief This class encapsulates an unknown in a given finite element space.
     *
     * In one finite element space unknown is related to exactly one numbering subset and exactly shape function label.
     */
    class ExtendedUnknown final
    {

      public:
        //! Alias to shared pointer to const object.
        using const_shared_ptr = std::shared_ptr<const ExtendedUnknown>;

        //! Alias to vector of shared pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown Pointer to the \a Unknown for which \a ExtendedUnknown is created.
         * \param[in] numbering_subset Pointer to the \a NumberingSubset for which \a ExtendedUnknown is created.
         * \param[in] shape_function_label String that acts as shape_function_label for which \a ExtendedUnknown is
         * created.
         */
        explicit ExtendedUnknown(Unknown::const_shared_ptr unknown,
                                 NumberingSubset::const_shared_ptr numbering_subset,
                                 const std::string& shape_function_label);

        //! Destructor.
        ~ExtendedUnknown() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ExtendedUnknown(const ExtendedUnknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ExtendedUnknown(ExtendedUnknown&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ExtendedUnknown& operator=(const ExtendedUnknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ExtendedUnknown& operator=(ExtendedUnknown&& rhs) = delete;

        ///@}

        //! Get the underlying unknown.
        const Unknown& GetUnknown() const noexcept;

        //! Get the underlying unknown.
        const Unknown::const_shared_ptr& GetUnknownPtr() const noexcept;

        //! Get the underlying numbering subset.
        const NumberingSubset& GetNumberingSubset() const noexcept;

        //! Get the underlying numbering subset as a smart pointer.
        const NumberingSubset::const_shared_ptr& GetNumberingSubsetPtr() const noexcept;

        //! Return the shape function label ('P1', 'Q3', etc...)
        const std::string& GetShapeFunctionLabel() const noexcept;

        //! Nature of the unknown.
        UnknownNS::Nature GetNature() const noexcept;

      private:
        //! Unknown considered.
        Unknown::const_shared_ptr unknown_;

        //! Numbering subset considered.
        NumberingSubset::const_shared_ptr numbering_subset_;

        //! Shape function label.
        const std::string& shape_function_label_;
    };


    //! \copydoc doxygen_hide_operator_equal
    //! Criteria: Two such objects are equal if both unknown and numbering subset are the same.
    bool operator==(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs);


    //! \copydoc doxygen_hide_operator_less
    //! Criteria: first unknown are compared, then numbering subset and finally shape function label.
    bool operator<(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/Unknown/ExtendedUnknown.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_UNKNOWN_EXTENDEDUNKNOWN_DOT_HPP_
// *** MoReFEM end header guards *** < //
