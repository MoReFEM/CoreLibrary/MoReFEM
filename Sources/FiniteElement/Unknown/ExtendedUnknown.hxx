// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_UNKNOWN_EXTENDEDUNKNOWN_DOT_HXX_
#define MOREFEM_FINITEELEMENT_UNKNOWN_EXTENDEDUNKNOWN_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/Unknown/ExtendedUnknown.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <compare>
#include <string>
// IWYU pragma: no_include <__compare/ordering.h>

// IWYU pragma: no_forward_declare MoReFEM::ExtendedUnknown

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    inline const Unknown& ExtendedUnknown::GetUnknown() const noexcept
    {
        assert(!(!unknown_));
        return *unknown_;
    }


    inline const Unknown::const_shared_ptr& ExtendedUnknown::GetUnknownPtr() const noexcept
    {
        assert(!(!unknown_));
        return unknown_;
    }


    inline const NumberingSubset& ExtendedUnknown::GetNumberingSubset() const noexcept
    {
        assert(!(!numbering_subset_));
        return *numbering_subset_;
    }


    inline bool operator==(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs)
    {
        return lhs.GetUnknown() == rhs.GetUnknown() && lhs.GetNumberingSubset() == rhs.GetNumberingSubset()
               && lhs.GetShapeFunctionLabel() == rhs.GetShapeFunctionLabel();
    }


    inline bool operator<(const ExtendedUnknown& lhs, const ExtendedUnknown& rhs)
    {
        if (lhs.GetUnknown() != rhs.GetUnknown())
            return lhs.GetUnknown() < rhs.GetUnknown();

        if (lhs.GetNumberingSubset() != rhs.GetNumberingSubset())
            return lhs.GetNumberingSubset() < rhs.GetNumberingSubset();

        return lhs.GetShapeFunctionLabel() < rhs.GetShapeFunctionLabel();
    }


    inline const NumberingSubset::const_shared_ptr& ExtendedUnknown::GetNumberingSubsetPtr() const noexcept
    {
        assert(!(!numbering_subset_));
        return numbering_subset_;
    }


    inline const std::string& ExtendedUnknown::GetShapeFunctionLabel() const noexcept
    {
        return shape_function_label_;
    }


    inline UnknownNS::Nature ExtendedUnknown::GetNature() const noexcept
    {
        return GetUnknown().GetNature();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_UNKNOWN_EXTENDEDUNKNOWN_DOT_HXX_
// *** MoReFEM end header guards *** < //
