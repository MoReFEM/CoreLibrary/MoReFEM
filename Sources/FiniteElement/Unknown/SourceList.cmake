### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/EnumUnknown.cpp
		${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknown.cpp
		${CMAKE_CURRENT_LIST_DIR}/Unknown.cpp
		${CMAKE_CURRENT_LIST_DIR}/UnknownManager.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/EnumUnknown.hpp
		${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknown.hpp
		${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknown.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/UniqueId.hpp
		${CMAKE_CURRENT_LIST_DIR}/Unknown.doxygen
		${CMAKE_CURRENT_LIST_DIR}/Unknown.hpp
		${CMAKE_CURRENT_LIST_DIR}/Unknown.hxx
		${CMAKE_CURRENT_LIST_DIR}/UnknownManager.hpp
		${CMAKE_CURRENT_LIST_DIR}/UnknownManager.hxx
)

