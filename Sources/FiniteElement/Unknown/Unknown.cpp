// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <string>
#include <utility>

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    const std::string& Unknown::ClassName()
    {
        static const std::string ret("Unknown");
        return ret;
    }


    Unknown::Unknown(std::string name, const UnknownNS::unique_id unique_id, const UnknownNS::Nature nature)
    : unique_id_parent(unique_id), name_(std::move(name)), nature_(nature)
    { }


    bool operator<(const Unknown& lhs, const Unknown& rhs)
    {
        return lhs.GetUniqueId() < rhs.GetUniqueId();
    }


    bool operator==(const Unknown& lhs, const Unknown& rhs)
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


    auto Ncomponent(const Unknown& unknown, const ::MoReFEM::GeometryNS::dimension_type mesh_dimension) -> Eigen::Index
    {

        switch (unknown.GetNature())
        {
        case UnknownNS::Nature::scalar:
            return 1;
        case UnknownNS::Nature::vectorial:
            return mesh_dimension.Get();
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
