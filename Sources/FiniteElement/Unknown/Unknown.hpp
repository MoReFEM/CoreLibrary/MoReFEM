// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWN_DOT_HPP_
#define MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWN_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "Geometry/StrongType.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/EnumUnknown.hpp" // IWYU pragma: export
#include "FiniteElement/Unknown/UniqueId.hpp"    // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Very generic information about a given unknown.
     *
     *
     */
    class Unknown : public Crtp::UniqueId<Unknown, UnknownNS::unique_id, UniqueIdNS::AssignationMode::manual>
    {

      public:
        //! Name of the class.
        static const std::string& ClassName();

        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const Unknown>;

        //! Alias to vector of shared pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Alias to the unique id parent.
        using unique_id_parent = Crtp::UniqueId<Unknown, UnknownNS::unique_id, UniqueIdNS::AssignationMode::manual>;

        //! Friendship.
        friend class UnknownManager;


        /// \name Special members.
        ///@{

      private:
        /*!
         * \brief Constructor.
         *
         * Private as unknown should be created by UnknownManager.
         *
         * \copydetails doxygen_hide_unknown_constructor_args
         */
        explicit Unknown(std::string name, UnknownNS::unique_id unique_id, const UnknownNS::Nature nature);

      public:
        //! Destructor.
        ~Unknown() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Unknown(const Unknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Unknown(Unknown&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Unknown& operator=(const Unknown& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Unknown& operator=(Unknown&& rhs) = delete;


        ///@}


      public:
        //! Nature of the unknown.
        UnknownNS::Nature GetNature() const noexcept;

        //! Get the name of the variable.
        const std::string& GetName() const noexcept;

      private:
        //! Name of the variable.
        const std::string name_;

        //! Nature of the unknown.
        const UnknownNS::Nature nature_;
    };


    //! \copydoc doxygen_hide_operator_less
    //! Criterion used here is the underlying unique id.
    bool operator<(const Unknown& lhs, const Unknown& rhs);

    //! \copydoc doxygen_hide_operator_equal
    //! Criterion used here is the underlying unique id.
    bool operator==(const Unknown& lhs, const Unknown& rhs);

    //! \copydoc doxygen_hide_operator_not_equal
    //! Criterion used here is the underlying unique id.
    bool operator!=(const Unknown& lhs, const Unknown& rhs);

    //! Number of components to consider for the unknown.
    //! \param[in] unknown \a Unknown for which the information is requested.
    //! \param[in] mesh_dimension Dimension of the mesh.
    Eigen::Index Ncomponent(const Unknown& unknown, const ::MoReFEM::GeometryNS::dimension_type mesh_dimension);


    /*!
     * \brief Convert into the \a UnknownNS::unique_id strong type the enum value defined in the input data file.
     *
     * In a typical Model, indexes related to fields (e.g. Unknown) are defined in an enum class named typically \a
     * UnknownIndex. This facility is a shortcut to convert this enum into a proper \a UnknownNS::unique_id that is used
     * in the library to identify \a Unknown.
     *
     * \param[in] enum_value The index related to the \a Unknown, stored in an enum class. See a typical 'InputData.hpp' file
     * to understand it better.
     *
     * \return The identifier for a \a Unknown as used in the library.
     */
    template<class EnumT>
    constexpr UnknownNS::unique_id AsUnknownId(EnumT enum_value);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/Unknown/Unknown.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWN_DOT_HPP_
// *** MoReFEM end header guards *** < //
