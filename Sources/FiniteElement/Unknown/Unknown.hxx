// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWN_DOT_HXX_
#define MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWN_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/Unknown/Unknown.hpp"
// *** MoReFEM header guards *** < //


#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include <type_traits> // IWYU pragma: keep

#include "FiniteElement/Unknown/UniqueId.hpp"
// IWYU pragma: no_forward_declare MoReFEM::Unknown

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::UnknownNS { enum class Nature; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    inline const std::string& Unknown::GetName() const noexcept
    {
        return name_;
    }


    inline bool operator!=(const Unknown& unknown1, const Unknown& unknown2)
    {
        return !(operator==(unknown1, unknown2));
    }


    inline UnknownNS::Nature Unknown::GetNature() const noexcept
    {
        return nature_;
    }


    template<class EnumT>
    constexpr UnknownNS::unique_id AsUnknownId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return UnknownNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWN_DOT_HXX_
// *** MoReFEM end header guards *** < //
