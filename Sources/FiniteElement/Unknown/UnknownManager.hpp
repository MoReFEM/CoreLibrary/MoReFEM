// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWNMANAGER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWNMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "FiniteElement/Unknown/Unknown.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS { template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> struct ClearSingletons; } // IWYU pragma: keep
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Singleton that is aware of all considered unknown, regardless of GodOfDof.
     *
     * Each of the unknowns must be defined in the input data file.
     */
    class UnknownManager final : public Utilities::Singleton<UnknownManager>
    {

      public:
        //! Return the name of the class.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::UnknownNS::Tag;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        friend struct MoReFEM::TestNS::ClearSingletons;

      public:
        /// \name Special members.
        ///@{


        //! \copydoc doxygen_hide_copy_constructor
        UnknownManager(const UnknownManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UnknownManager(UnknownManager&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UnknownManager& operator=(const UnknownManager& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UnknownManager& operator=(UnknownManager&& rhs) = delete;

        ///@}


        /*!
         * \brief Create a \a Unknown object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);


        //! Returns the number of unknowns.
        std::size_t Nunknown() const noexcept;

        //! Get the unknown associated with \a unique_id.
        //! \unique_id_param_in_accessor{Unknown}
        const Unknown& GetUnknown(UnknownNS::unique_id unique_id) const noexcept;

        //! Get the unknown associated with \a unique_id as a smart pointer.
        //! \unique_id_param_in_accessor{Unknown}
        Unknown::const_shared_ptr GetUnknownPtr(UnknownNS::unique_id unique_id) const;

        /*!
         * \brief Get the unknown which name is given as argument.
         *
         * \name_param_in_accessor{Unknown}
         */
        const Unknown& GetUnknown(const std::string& name) const;

        /*!
         * \brief Get the unknown which name is given as argument.
         *
         * \name_param_in_accessor{Unknown}
         */
        Unknown::const_shared_ptr GetUnknownPtr(const std::string& name) const;

        //! Access to the list of unknowns.
        const Unknown::vector_const_shared_ptr& GetList() const noexcept;


      private:
        /*!
         * \brief Create a new Unknown, which is a unique id, a name and a nature (scalar or vectorial).
         *
         * \copydetails doxygen_hide_unknown_constructor_args
         */
        void Create(UnknownNS::unique_id unique_id, const std::string& name, const std::string& nature);

        //! \copydoc doxygen_hide_manager_clear
        void Clear();


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        UnknownManager() = default;

        //! Destructor.
        virtual ~UnknownManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<UnknownManager>;
        ///@}


      private:
        //! Register properly unknown in use.
        //! \param[in] unknown New \a Unknown to register.
        void RegisterUnknown(const Unknown::const_shared_ptr& unknown);


      private:
        //! List of unknowns.
        Unknown::vector_const_shared_ptr unknown_list_;
    };


    /*!
     * \brief Write in output directory a file that lists all the unknowns.
     *
     * Should be called only on root processor.
     *
     * \param[in] output_directory Output directory in which a file named 'unknowns.hhdata'
     * will be written.
     */
    void WriteUnknownList(const FilesystemNS::Directory& output_directory);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/Unknown/UnknownManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWNMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
