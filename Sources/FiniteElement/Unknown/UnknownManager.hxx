// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWNMANAGER_DOT_HXX_
#define MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWNMANAGER_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/Unknown/UnknownManager.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    template<class IndexedSectionDescriptionT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
             ::MoReFEM::Concept::InputDataType InputDataT>
    void UnknownManager::Create(const IndexedSectionDescriptionT&,
                                const ModelSettingsT& model_settings,
                                const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) name =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Name>(model_settings, input_data);

        decltype(auto) nature =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Nature>(model_settings, input_data);

        Create(::MoReFEM::UnknownNS::unique_id{ section_type::GetUniqueId() }, name, nature);
    }


    inline std::size_t UnknownManager::Nunknown() const noexcept
    {
        return unknown_list_.size();
    }


    inline const Unknown& UnknownManager::GetUnknown(UnknownNS::unique_id i) const noexcept
    {
        return *GetUnknownPtr(i);
    }


    inline const Unknown& UnknownManager::GetUnknown(const std::string& unknown_name) const
    {
        return *GetUnknownPtr(unknown_name);
    }


    inline const Unknown::vector_const_shared_ptr& UnknownManager::GetList() const noexcept
    {
        return unknown_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_UNKNOWN_UNKNOWNMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
