// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_ADVANCED_RESTART_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_ADVANCED_RESTART_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string_view>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GlobalMatrix; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::RestartNS
{


    /*!
     * \class doxygen_hide_restart_functions_most_common_param
     *
     * \param[in] time_manager  \a TimeManager is used to give the main base directory in which restart data are stored and
     * to provide which time iteration is sought.
     * \param[in] god_of_dof The \a GodOfDof considered - it will be used to identify the folder in which is the
     * sought data (used in a call to \a GetDataDirectory()).
     *
     * Restart mode is explained in more details on [this wiki note](../Wiki/Restart.md).
     */

    /*!
     * \class doxygen_hide_restart_tag_arg
     *
     * \param[in] tag The name of the quantity (e.g. unknown) that is covered by the sought data; this name had been used
     * to save the restart data in the first place and it appears in the resulting filename, which is in the format:
     * restart_**tag**_**time id**.binarydata where 'time_id' is given by \a time_manager.
     */

    /*!
     * \class doxygen_hide_restart_serialization_arguments
     *
     * \copydoc doxygen_hide_restart_functions_most_common_param
     *
     * \copydoc doxygen_hide_restart_tag_arg
     */

    /*!
     * \brief Write content of \a vector into a dedicated directory that might be used in a later restart run.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     *
     * \param[in] vector The vector which data is written in disk.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void WriteDataForRestart(const TimeManagerT& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalVector& vector);


    /*!
     * \brief Write content of \a matrix into a dedicated directory that might be used in a later restart run.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     * \param[in] matrix The matrix which data is written in disk.
     *
     * \note This function is not used currently but was tested and work (the model didn't need it).
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void WriteDataForRestart(const TimeManagerT& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalMatrix& matrix);


    /*!
     * \brief Load \a vector from the correct restart data.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     *
     * \param[in,out] vector The vector into which the sought data is loaded. It is expected this vector is already properly
     * initialized, with its layout already computed.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LoadGlobalVectorFromData(const TimeManagerT& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalVector& vector);

    /*!
     * \brief Load \a matrix from the correct restart data.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     *
     * \param[in,out] matrix The matrix into which the sought data is loaded. It is expected this matrix is already properly
     * initialized, with its layout already computed.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LoadGlobalMatrixFromData(const TimeManagerT& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalMatrix& matrix);


    /*!
     * \brief Copy the solution files that are prior to the restart time index.
     *
     * \copydoc doxygen_hide_restart_functions_most_common_param
     *
     * \param[in] numbering_subset The \a NumberingSubset of the solutions to be copied.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void CopySolutionFilesFromOriginalRun(const TimeManagerT& time_manager,
                                          const GodOfDof& god_of_dof,
                                          const NumberingSubset& numbering_subset);


} // namespace MoReFEM::Advanced::RestartNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //

#include "FormulationSolver/Advanced/Restart.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_ADVANCED_RESTART_DOT_HPP_
// *** MoReFEM end header guards *** < //
