// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_ADVANCED_RESTART_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_ADVANCED_RESTART_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Advanced/Restart.hpp"
// *** MoReFEM header guards *** < //


#include <string_view>

#include "Utilities/AsciiOrBinary/Advanced/DatafileExtension.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "FormulationSolver/Exceptions/Restart.hpp"
#include "FormulationSolver/Internal/Restart/Restart.hpp"


namespace MoReFEM::Advanced::RestartNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LoadGlobalVectorFromData(const TimeManagerT& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalVector& vector)
    {
        FilesystemNS::File input_file;

        decltype(auto) mpi = god_of_dof.GetMpi();
        decltype(auto) numbering_subset = vector.GetNumberingSubset();

        if (mpi.IsRootProcessor())
        {
            auto filename = ::MoReFEM::Internal::RestartNS::ComputeFilename(time_manager, tag);
            auto directory =
                ::MoReFEM::Internal::RestartNS::GetDataDirectory(time_manager, god_of_dof, numbering_subset);

            input_file = directory.AddFile(filename);

            if (mpi.IsRootProcessor() && !input_file.DoExist())
                throw ExceptionNS::FormulationSolverNS::RestartNS::InexistantFile(input_file);
        }

        // Temporary file used here as a safety: we already know the expected structure of
        // the vector, and the Init...() function below doesn't know it. Using an intermediate
        // vector enables safety check.
        GlobalVector buf(numbering_subset, "Temporary vector in LoadGlobalVectorFromData().");

        buf.InitFromProgramWiseBinaryFile(mpi,
                                          vector.GetProcessorWiseSize(),
                                          vector.GetProgramWiseSize(),
                                          (mpi.IsSequential() ? std::vector<PetscInt>{} : vector.GetGhostPadding()),
                                          (mpi.IsRootProcessor() ? input_file : FilesystemNS::File{}));

        vector.Copy(buf);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void WriteDataForRestart(const TimeManagerT& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalVector& vector)
    {
        decltype(auto) mpi = god_of_dof.GetMpi();

        auto filename = ::MoReFEM::Internal::RestartNS::ComputeFilename(time_manager, tag);
        auto directory = god_of_dof.GetOutputDirectoryForNumberingSubset(vector.GetNumberingSubset());
        auto output_file = directory.AddFile(filename);

        vector.template Print<MpiScale::program_wise>(mpi, output_file, binary_or_ascii::binary);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void WriteDataForRestart(const TimeManagerT& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalMatrix& matrix)
    {
        decltype(auto) mpi = god_of_dof.GetMpi();

        auto filename = ::MoReFEM::Internal::RestartNS::ComputeFilename(time_manager, tag);
        auto directory = god_of_dof.GetOutputDirectoryForNumberingSubset(matrix.GetRowNumberingSubset());
        auto output_file = directory.AddFile(filename);

        matrix.ViewBinary(mpi, output_file);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LoadGlobalMatrixFromData(const TimeManagerT& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalMatrix& matrix)
    {
        FilesystemNS::File input_file;

        decltype(auto) mpi = god_of_dof.GetMpi();
        decltype(auto) numbering_subset = matrix.GetRowNumberingSubset();

        if (mpi.IsRootProcessor())
        {
            auto filename = ::MoReFEM::Internal::RestartNS::ComputeFilename(time_manager, tag);
            auto directory = GetDataDirectory(time_manager, god_of_dof, numbering_subset);

            input_file = directory.AddFile(filename);

            if (mpi.IsRootProcessor() && !input_file.DoExist())
                throw ExceptionNS::FormulationSolverNS::RestartNS::InexistantFile(input_file);
        }

        matrix.LoadBinary(mpi, input_file);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void CopySolutionFilesFromOriginalRun(const TimeManagerT& time_manager,
                                          const GodOfDof& god_of_dof,
                                          const NumberingSubset& numbering_subset)
    {
        auto original_run_directory =
            ::MoReFEM::Internal::RestartNS::GetDataDirectory(time_manager, god_of_dof, numbering_subset);
        auto output_directory = god_of_dof.GetOutputDirectoryForNumberingSubset(numbering_subset);

        InterpretOutputFilesNS::TimeIterationFile time_iteration_data_from_original_run(
            time_manager.GetRestartTimeIterationFile());

        const auto restart_time_index = time_manager.GetRestartTimeIndex();

        decltype(auto) mpi = god_of_dof.GetMpi();

        for (auto time_iteration_file_index = 0UL; time_iteration_file_index < restart_time_index;
             ++time_iteration_file_index)
        {
            decltype(auto) time_iteration_data_for_index =
                time_iteration_data_from_original_run.GetTimeIteration(time_iteration_file_index);

            decltype(auto) source = time_iteration_data_for_index.GetSolutionFilename(mpi);
            std::filesystem::path filename = source.GetDirectoryEntry().path().filename();
            const auto target = output_directory.AddFile(static_cast<std::string>(filename));

            FilesystemNS::Copy(source, target, FilesystemNS::fail_if_already_exist::yes, FilesystemNS::autocopy::no);

            // Complete the time_iteration file as well
            {
                decltype(auto) file = time_manager.GetTimeIterationFile();
                std::ofstream inout{ file.Append() };
                inout << time_iteration_data_for_index << std::endl;
            }
        }
    }


} // namespace MoReFEM::Advanced::RestartNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_ADVANCED_RESTART_DOT_HXX_
// *** MoReFEM end header guards *** < //
