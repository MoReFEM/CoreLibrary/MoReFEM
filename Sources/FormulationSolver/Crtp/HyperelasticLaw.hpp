// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_CRTP_HYPERELASTICLAW_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_CRTP_HYPERELASTICLAW_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>


namespace MoReFEM::FormulationSolverNS
{


    /*!
     * \brief Crtp to add in a VariationalFormulation an hyperelastic_law_ data attribute and its accessors.
     *
     * \tparam DerivedT Class upon which the CRTP is applied.
     * \tparam HyperelasticLawT Type of the hyperelastic law (e.g. HyperelasticLawNS::CiarletGeymonat).
     *
     * \attention Create() must be explicitly called before the law is used.
     */
    template<class DerivedT, class HyperelasticLawT>
    class HyperelasticLaw
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = HyperelasticLaw<DerivedT, HyperelasticLawT>;

        //! Alias to hyperelastic law type.
        using hyperelastic_law_type = HyperelasticLawT;


      protected:
        /// \name Special members.
        ///@{

        //! Constructor.

        explicit HyperelasticLaw() = default;

        //! Destructor.
        ~HyperelasticLaw() = default;

        //! \copydoc doxygen_hide_copy_constructor
        HyperelasticLaw(const HyperelasticLaw& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        HyperelasticLaw(HyperelasticLaw&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        HyperelasticLaw& operator=(const HyperelasticLaw& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        HyperelasticLaw& operator=(HyperelasticLaw&& rhs) = delete;

        ///@}

        /*!
         * \brief Instantiate the hyperelastic law.
         *
         * \param[in] args hyperelastic_law_type's constructor arguments.
         */
        template<typename... Args>
        void Create(Args&&... args);

      public:
        //! Get the underlying hyperelastic law.
        const hyperelastic_law_type& GetHyperelasticLaw() const noexcept;

        //! Get the underlying hyperelastic law as a raw pointer.
        const hyperelastic_law_type* GetHyperelasticLawPtr() const noexcept;


      private:
        //! Hyperelastic law used in stiffness operator.
        typename hyperelastic_law_type::const_unique_ptr hyperelastic_law_ = nullptr;
    };


} // namespace MoReFEM::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Crtp/HyperelasticLaw.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_CRTP_HYPERELASTICLAW_DOT_HPP_
// *** MoReFEM end header guards *** < //
