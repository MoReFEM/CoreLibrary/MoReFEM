// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_CRTP_PENALIZATION_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_CRTP_PENALIZATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>


namespace MoReFEM::FormulationSolverNS
{


    /*!
     * \brief Crtp to add in a VariationalFormulation an penalization_ data attribute and its accessors.
     *
     * \tparam DerivedT Class upon which the CRTP is applied.
     * \tparam PenalizationLawT Type of the penalization (e.g. PenalizationNS::LogI3Penalization).
     *
     * \attention Create() must be explicitly called before the law is used.
     */
    template<class DerivedT, class PenalizationLawT>
    class Penalization
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Penalization<DerivedT, PenalizationLawT>;

        //! Alias to penalization type.
        using penalization_law_type = PenalizationLawT;


      protected:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Penalization() = default;

        //! Destructor.
        ~Penalization() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Penalization(const Penalization& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Penalization(Penalization&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Penalization& operator=(const Penalization& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Penalization& operator=(Penalization&& rhs) = delete;

        ///@}

        /*!
         * \brief Instantiate the penalization.
         *
         * \param[in] args penalization_law_type's constructor arguments.
         */
        template<typename... Args>
        void Create(Args&&... args);

        //! Get the underlying penalization.
        const penalization_law_type& GetPenalizationLaw() const noexcept;

        //! Get the underlying penalization as a raw pointer.
        const penalization_law_type* GetPenalizationLawPtr() const noexcept;


      private:
        //! Penalization used in stiffness operator.
        typename penalization_law_type::const_unique_ptr penalization_ = nullptr;
    };


} // namespace MoReFEM::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Crtp/Penalization.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_CRTP_PENALIZATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
