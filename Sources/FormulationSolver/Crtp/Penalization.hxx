// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_CRTP_PENALIZATION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_CRTP_PENALIZATION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Crtp/Penalization.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::FormulationSolverNS
{


    template<class DerivedT, class PenalizationLawT>
    template<typename... Args>
    void Penalization<DerivedT, PenalizationLawT>::Create(Args&&... args)
    {
        assert(penalization_ == nullptr && "Should be called only once!");

        penalization_ = std::make_unique<PenalizationLawT>(std::forward<decltype(args)>(args)...);
    }


    template<class DerivedT, class PenalizationLawT>
    const PenalizationLawT& Penalization<DerivedT, PenalizationLawT>::GetPenalizationLaw() const noexcept
    {
        assert(!(!penalization_));
        return *penalization_;
    }


    template<class DerivedT, class PenalizationLawT>
    const PenalizationLawT* Penalization<DerivedT, PenalizationLawT>::GetPenalizationLawPtr() const noexcept
    {
        assert(!(!penalization_));
        return penalization_.get();
    }


} // namespace MoReFEM::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_CRTP_PENALIZATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
