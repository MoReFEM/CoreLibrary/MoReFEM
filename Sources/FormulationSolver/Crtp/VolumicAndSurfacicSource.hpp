// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_CRTP_VOLUMICANDSURFACICSOURCE_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_CRTP_VOLUMICANDSURFACICSOURCE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "FormulationSolver/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Crtp
{


    /*!
     * \brief Crtp for VariationalFormulation that needs to address a volumic and/or a surfacic force.
     *
     * \tparam VolumicIndexT Index of the volumic force in the input data tuple of the model considered.
     * (i.e. the index N called in  InputDataNS::VectorialTransientSource<N> template instantiation).
     * \tparam SurfacicIndexT Same as VolumicIndexT for the surfacic force.
     *
     */
    // clang-format on
    template<class DerivedT,
             ParameterNS::Type TypeT,
             std::size_t VolumicIndexT,
             std::size_t SurfacicIndexT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    // clang-format off
    class VolumicAndSurfacicSource
    {


      public:
        //! Alias to the type of parameter used to store the sources.
        using parameter_type = Parameter<TypeT, LocalCoords, TimeManagerT, TimeDependencyT>; // #1369 really LocalCoords?

        /// \name Special members.
        ///@{

        //! Constructor.
        explicit VolumicAndSurfacicSource() = default;

        //! Destructor.
        ~VolumicAndSurfacicSource() = default;

        //! \copydoc doxygen_hide_copy_constructor
        VolumicAndSurfacicSource(const VolumicAndSurfacicSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VolumicAndSurfacicSource(VolumicAndSurfacicSource&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        VolumicAndSurfacicSource& operator=(const VolumicAndSurfacicSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VolumicAndSurfacicSource& operator=(VolumicAndSurfacicSource&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Set one of the operator if it is tagged as activated in the input data file.
         *
         * \param[in] name Name of the parameter (for outputs).
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         * \param[in] felt_space Finite element space upon which the operator is defined.
         * \param[in] unknown_ptr Unknown considered for this operator (might be scalar or vectorial).
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        template<SourceType SourceTypeT, class T, ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        void SetIfTaggedAsActivated(T&& name,
                                    const MoReFEMDataT& morefem_data,
                                    const FEltSpace& felt_space,
                                    const Unknown::const_shared_ptr& unknown_ptr,
                                    const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);


        /*!
         * \brief Whether the force operator labelled by \a SourceTypeT is activated in the current model.
         *
         * This is decided within the input data file with parameter is_activated related to the requested force.
         *
         * \return True if the operator tagged by \a SourceType is considered or not in the current Model/
         */
        template<SourceType SourceTypeT>
        bool IsOperatorActivated() const noexcept;


        /*!
         * \brief Get one of the force operators.
         *
         * It is assumed this operator is activated; this can be checked beforehand by IsOperatorActivated() method.
         *
         * \return The operator related to source \a SourceTypeT. This operator must be marked as activated.
         */
        template<SourceType SourceTypeT>
        const GlobalVariationalOperatorNS::TransientSource<TypeT, TimeManagerT, TimeDependencyT>& GetForceOperator() const noexcept;

        //! Access to the volumic force parameter (if relevant).
        const parameter_type& GetVolumicForceParameter() const;


        //! Access to the surfacic force parameter (if relevant).
        const parameter_type& GetSurfacicForceParameter() const;


      private:
        //! Volumic force operator. Might stay nullptr if is_nature is 'ignore' in the input data file.
        typename GlobalVariationalOperatorNS::TransientSource<TypeT, TimeManagerT, TimeDependencyT>::const_unique_ptr
            volumic_force_operator_ = nullptr;

        //! Surfacic force operator. Might stay nullptr if nature is 'ignore' in the input data file.
        typename GlobalVariationalOperatorNS::TransientSource<TypeT, TimeManagerT, TimeDependencyT>::const_unique_ptr
            surfacic_force_operator_ = nullptr;

        //! Volumic force operator. Might stay nullptr if nature is 'ignore' in the input data file.
        typename parameter_type::unique_ptr volumic_force_parameter_ = nullptr;

        //! Surfacic force operator. Might stay nullptr if nature is 'ignore' in the input data file.
        typename parameter_type::unique_ptr surfacic_force_parameter_ = nullptr;
    };


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //




#include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_CRTP_VOLUMICANDSURFACICSOURCE_DOT_HPP_
// *** MoReFEM end header guards *** < //

