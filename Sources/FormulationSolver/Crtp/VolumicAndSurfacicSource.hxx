// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_CRTP_VOLUMICANDSURFACICSOURCE_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_CRTP_VOLUMICANDSURFACICSOURCE_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Crtp
{


    // clang-format on
    template<class DerivedT,
             ParameterNS::Type TypeT,
             std::size_t VolumicIndexT,
             std::size_t SurfacicIndexT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    // clang-format off
    template<SourceType SourceTypeT, class T, ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void
    VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeManagerT, TimeDependencyT>::SetIfTaggedAsActivated(
        T&& name,
        const MoReFEMDataT& morefem_data,
        const FEltSpace& felt_space,
        const Unknown::const_shared_ptr& unknown_ptr,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    {
        static_assert(std::is_same<TimeManagerT, typename MoReFEMDataT::time_manager_type>());
        
        namespace GVO = GlobalVariationalOperatorNS;

        typename GVO::TransientSource<TypeT, TimeManagerT, TimeDependencyT>::const_unique_ptr global_operator = nullptr;
        typename parameter_type::unique_ptr parameter_ptr = nullptr;

        // If tagged as activated, compute here the global variational operator.
        {
            constexpr auto index = SourceTypeT == SourceType::volumic ? VolumicIndexT : SurfacicIndexT;

            using local_parameter_type = InputDataNS::VectorialTransientSource<index>;

            parameter_ptr = Init3DCompoundParameterFromInputData<local_parameter_type, TimeDependencyT>(
                std::forward<T>(name), felt_space.GetDomain(), morefem_data);

            if (parameter_ptr == nullptr)
                return;

            global_operator = std::make_unique<GVO::TransientSource<TypeT, typename MoReFEMDataT::time_manager_type, TimeDependencyT>>(
                felt_space, unknown_ptr, *parameter_ptr, quadrature_rule_per_topology);
        }

        // Assign properly the created global variational operator.
        switch (SourceTypeT)
        {
        case SourceType::volumic:
            volumic_force_operator_ = std::move(global_operator);
            volumic_force_parameter_ = std::move(parameter_ptr);
            break;
        case SourceType::surfacic:
            surfacic_force_operator_ = std::move(global_operator);
            surfacic_force_parameter_ = std::move(parameter_ptr);
            break;
        }
    }


    // clang-format on
    template<class DerivedT,
             ParameterNS::Type TypeT,
             std::size_t VolumicIndexT,
             std::size_t SurfacicIndexT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    // clang-format off
    template<SourceType SourceTypeT>
    bool
    VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeManagerT, TimeDependencyT>::IsOperatorActivated()
        const noexcept
    {
        switch (SourceTypeT)
        {
        case SourceType::volumic:
            return volumic_force_operator_ != nullptr;
        case SourceType::surfacic:
            return surfacic_force_operator_ != nullptr;
        }
        assert(false && "All cases should be handled in the switch!");
        return false; // to avoid warning in release mode.
    }


    // clang-format on
    template<class DerivedT,
             ParameterNS::Type TypeT,
             std::size_t VolumicIndexT,
             std::size_t SurfacicIndexT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    // clang-format off
    template<SourceType SourceTypeT>
    inline const GlobalVariationalOperatorNS::TransientSource<TypeT, TimeManagerT, TimeDependencyT>&
    VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeManagerT, TimeDependencyT>::GetForceOperator()
        const noexcept
    {
        assert(IsOperatorActivated<SourceTypeT>());

        switch (SourceTypeT)
        {
        case SourceType::volumic:
        {
            assert(!(!volumic_force_operator_));
            return *volumic_force_operator_;
        }
        case SourceType::surfacic:
        {
            assert(!(!surfacic_force_operator_));
            return *surfacic_force_operator_;
        }
        }

        assert(false && "All cases should be handled in the switch!");
        return *volumic_force_operator_; // to avoid warning in release mode.
    }


    // clang-format on
    template<class DerivedT,
             ParameterNS::Type TypeT,
             std::size_t VolumicIndexT,
             std::size_t SurfacicIndexT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    // clang-format off
    auto VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeManagerT, TimeDependencyT>::
            GetVolumicForceParameter() const
    -> const parameter_type&
    {
        assert(!(!volumic_force_parameter_));
        return *volumic_force_parameter_;
    }


    // clang-format on
    template<class DerivedT,
             ParameterNS::Type TypeT,
             std::size_t VolumicIndexT,
             std::size_t SurfacicIndexT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    // clang-format off
    auto VolumicAndSurfacicSource<DerivedT, TypeT, VolumicIndexT, SurfacicIndexT, TimeManagerT, TimeDependencyT>::
            GetSurfacicForceParameter() const
    -> const parameter_type&
    {
        assert(!(!surfacic_force_parameter_));
        return *surfacic_force_parameter_;
    }


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //




// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_CRTP_VOLUMICANDSURFACICSOURCE_DOT_HXX_
// *** MoReFEM end header guards *** < //

