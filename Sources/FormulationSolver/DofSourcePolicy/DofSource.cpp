// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "FormulationSolver/DofSourcePolicy/DofSource.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"


namespace MoReFEM::VariationalFormulationNS::DofSourcePolicyNS
{


    DofSource::DofSource(double factor, const GlobalVector& dof_source) : dof_source_(dof_source), factor_(factor)
    { }


    void DofSource::AddToRhs(GlobalVector& rhs)
    {
        Wrappers::Petsc::AXPY(factor_, dof_source_, rhs);
    }


} // namespace MoReFEM::VariationalFormulationNS::DofSourcePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //
