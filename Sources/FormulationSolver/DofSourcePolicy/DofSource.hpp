// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_DOFSOURCEPOLICY_DOFSOURCE_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_DOFSOURCEPOLICY_DOFSOURCE_DOT_HPP_
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::VariationalFormulationNS::DofSourcePolicyNS
{


    /*!
     * \brief Policy to use when there is a source expressed at the dofs.
     *
     * It is the case for instance in FSI model, where the source is actually the residual obtained from
     * a previous variational formulation.
     */
    class DofSource
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = DofSource;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] dof_source Vector containing the source values at the dofs. It is expected that this
         * vector keeps existing (it should therefore be a data attribute of the calling Model) and that its
         * dimension fits the expected one (namely the same as the rhs and solution of the variational
         * formulation that uses up this policy).
         * \param[in] factor Factor by which the stored vector is multiplied when it is added to the rhs.
         */
        explicit DofSource(double factor, const GlobalVector& dof_source);

      protected:
        //! Destructor.
        ~DofSource() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DofSource(const DofSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        DofSource(DofSource&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DofSource& operator=(const DofSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DofSource& operator=(DofSource&& rhs) = delete;

        ///@}

        //! Add the contribution of dof_source_ to the rhs.
        //! \param[in,out] rhs The rhs that will be incremented.
        void AddToRhs(GlobalVector& rhs);

      private:
        /*!
         * \brief Source expressed at the dofs.
         *
         */
        const GlobalVector& dof_source_;

        //! Factor by which the stored vector is multiplied when it is added to the rhs.
        const double factor_;
    };


} // namespace MoReFEM::VariationalFormulationNS::DofSourcePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_DOFSOURCEPOLICY_DOFSOURCE_DOT_HPP_
// *** MoReFEM end header guards *** < //
