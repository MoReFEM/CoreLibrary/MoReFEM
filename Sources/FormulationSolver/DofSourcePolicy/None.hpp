// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_DOFSOURCEPOLICY_NONE_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_DOFSOURCEPOLICY_NONE_DOT_HPP_
// *** MoReFEM header guards *** < //

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::VariationalFormulationNS::DofSourcePolicyNS
{


    /*!
     * \brief Default policy of hyperelastic model, when there are no source given at the dofs.
     *
     * For instance in hyperelastic model sources are given by an linear operator which in fact encapsulates
     * a Parameter, which values are expressed at the quadrature points rather than at the dofs.
     *
     */
    class None
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = None;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit None() = default;

      protected:
        //! Destructor.
        ~None() = default;

        //! \copydoc doxygen_hide_copy_constructor
        None(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        None(None&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        None& operator=(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        None& operator=(None&& rhs) = delete;

        ///@}

        //! Actually do notthing here!
        static void AddToRhs(GlobalVector&);


      private:
    };


} // namespace MoReFEM::VariationalFormulationNS::DofSourcePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_DOFSOURCEPOLICY_NONE_DOT_HPP_
// *** MoReFEM end header guards *** < //
