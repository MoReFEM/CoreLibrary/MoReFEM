// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "FormulationSolver/Exceptions/Restart.hpp"

#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string InexistantFileMsg(const MoReFEM::FilesystemNS::File& file);

} // namespace


namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS
{


    InexistantFile::~InexistantFile() = default;


    InexistantFile::InexistantFile(const FilesystemNS::File& file, const std::source_location location)
    : MoReFEM::Exception(InexistantFileMsg(file), location)
    { }


} // namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file
    std::string InexistantFileMsg(const MoReFEM::FilesystemNS::File& file)
    {
        std::ostringstream oconv;
        oconv << "File " << file << " expected to hold restart data wasn't found on the filesystem.";

        return oconv.str();
    }


} // namespace
