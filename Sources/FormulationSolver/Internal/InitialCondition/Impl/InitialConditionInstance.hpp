// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_IMPL_INITIALCONDITIONINSTANCE_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_IMPL_INITIALCONDITIONINSTANCE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"


namespace MoReFEM::Internal::FormulationSolverNS::Impl
{


    /*!
     * \brief Template class that provides actual instantiation of a parameter.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     *
     * \tparam NaturePolicyT Policy that determines how to handle the parameter. Policies are enclosed in
     * ParameterNS::Policy namespace. Policies might be for instance Constant (same value everywhere),
     * LuaFunction (value is provided by a function defined in the input data file; additional arguments are
     * chosen here with the variadic template argument \a Args.).
     *
     */
    // clang-format off
        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type, typename... Args> class NaturePolicyT,
            typename... Args
        >
    // clang-format on
    class InitialConditionInstance final : public InitialCondition<TypeT>, public NaturePolicyT<TypeT, Args...>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = InitialConditionInstance<TypeT, NaturePolicyT, Args...>;

        //! Alias to base class.
        using parent = InitialCondition<TypeT>;

        //! Alias to return type.
        using return_type = typename parent::return_type;

        //! Alias to traits of parent class.
        using traits = typename parent::traits;

        //! Alias to nature policy (constant, per quadrature point, Lua function, etc...).
        using nature_policy = NaturePolicyT<TypeT, Args...>;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<self>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh \a Mesh for which the initial condition is defined.
         * \param[in] arguments Variadic arguments for initial conditions that need extra arguments in
         * constructor.
         */
        template<typename... ConstructorArgs>
        explicit InitialConditionInstance(const Mesh& mesh, ConstructorArgs&&... arguments);

        //! Destructor.
        ~InitialConditionInstance() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        InitialConditionInstance(const InitialConditionInstance& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InitialConditionInstance(InitialConditionInstance&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InitialConditionInstance& operator=(const InitialConditionInstance& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InitialConditionInstance& operator=(InitialConditionInstance&& rhs) = delete;

        ///@}

        //! Get the value of the initial condition at \a coords.
        //! \param[in] coords Spatial position at which the initial condition is evaluated.
        return_type SupplGetValue(const SpatialPoint& coords) const override;

        //! Get the value of the initial condition when it's a constant. Disabled if it's not...
        return_type SupplGetConstantValue() const override;

        /*
         * \brief Whether the parameter varies spatially or not.
         */
        bool IsConstant() const override;
    };


} // namespace MoReFEM::Internal::FormulationSolverNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_IMPL_INITIALCONDITIONINSTANCE_DOT_HPP_
// *** MoReFEM end header guards *** < //
