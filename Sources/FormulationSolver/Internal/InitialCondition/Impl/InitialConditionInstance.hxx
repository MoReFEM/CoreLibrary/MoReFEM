// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_IMPL_INITIALCONDITIONINSTANCE_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_IMPL_INITIALCONDITIONINSTANCE_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::FormulationSolverNS::Impl
{


    // clang-format off
            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                typename... Args
            >
    // clang-format on
    template<typename... ConstructorArgs>
    InitialConditionInstance<TypeT, NaturePolicyT, Args...>::InitialConditionInstance(const Mesh& mesh,
                                                                                      ConstructorArgs&&... arguments)
    : parent(mesh), nature_policy(std::forward<ConstructorArgs>(arguments)...)
    {
        static_assert(std::is_convertible<self*, parent*>());
    }


    // clang-format off
            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                typename... Args
            >
    // clang-format on
    inline typename InitialConditionInstance<TypeT, NaturePolicyT, Args...>::return_type
    InitialConditionInstance<TypeT, NaturePolicyT, Args...>::SupplGetValue(const SpatialPoint& coords) const
    {
        return nature_policy::GetValueFromPolicy(coords);
    }


    // clang-format off
            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                typename... Args
            >
    // clang-format on
    inline typename InitialConditionInstance<TypeT, NaturePolicyT, Args...>::return_type
    InitialConditionInstance<TypeT, NaturePolicyT, Args...>::SupplGetConstantValue() const
    {
        assert(nature_policy::IsConstant());
        return nature_policy::GetConstantValueFromPolicy();
    }


    // clang-format off
            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type, typename... Args> class NaturePolicyT,
                typename... Args
            >
    // clang-format on
    inline bool InitialConditionInstance<TypeT, NaturePolicyT, Args...>::IsConstant() const
    {
        return nature_policy::IsConstant();
    }


} // namespace MoReFEM::Internal::FormulationSolverNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_IMPL_INITIALCONDITIONINSTANCE_DOT_HXX_
// *** MoReFEM end header guards *** < //
