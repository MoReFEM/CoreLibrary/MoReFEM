// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITSCALARINITIALCONDITION_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITSCALARINITIALCONDITION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "FormulationSolver/Internal/InitialCondition/Impl/InitialConditionInstance.hpp"
#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"
#include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hpp"
#include "FormulationSolver/Internal/InitialCondition/Policy/LuaFunction.hpp"


namespace MoReFEM::Internal::FormulationSolverNS
{


    /*!
     * \brief Init a \a InitialCondition from the content of the input data file.
     *
     * \param[in] mesh Mesh upon which the InitialCondition is applied. Initial condition
     * might be requested at each of each \a Coords.
     * \param[in] nature One of the possible nature specified in the comment of the input data file. Currently
     * only 'constant' and 'lua_function'.
     * \param[in] value The std::variant object read from the input data file, which nature is determined by
     * \a nature.
     *
     * \return The \a InitialCondition object properly initialized.
     */
    template<class VariantT>
    InitialCondition<ParameterNS::Type::scalar>::unique_ptr
    InitScalarInitialCondition(const Mesh& mesh, const std::string& nature, const VariantT& value);


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITSCALARINITIALCONDITION_DOT_HPP_
// *** MoReFEM end header guards *** < //
