// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITSCALARINITIALCONDITION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITSCALARINITIALCONDITION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::FormulationSolverNS
{


    template<class VariantT>
    InitialCondition<ParameterNS::Type::scalar>::unique_ptr
    InitScalarInitialCondition(const Mesh& mesh, const std::string& nature, const VariantT& value)
    {
        if (nature == "constant")
        {
            using initial_condition_type = Impl::InitialConditionInstance<ParameterNS::Type::scalar, Policy::Constant>;

            return std::make_unique<initial_condition_type>(mesh, std::get<double>(value));
        } else if (nature == "lua_function")
        {
            using initial_condition_type =
                Impl::InitialConditionInstance<ParameterNS::Type::scalar, Policy::LuaFunction>;

            using value_type = ::MoReFEM::Wrappers::Lua::spatial_function;

            return std::make_unique<initial_condition_type>(mesh, std::get<value_type>(value));
        } else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      " OptionFile Constraints.");
        }

        return nullptr;
    }


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITSCALARINITIALCONDITION_DOT_HXX_
// *** MoReFEM end header guards *** < //
