// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITTHREEDIMENSIONALINITIALCONDITION_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITTHREEDIMENSIONALINITIALCONDITION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitScalarInitialCondition.hpp"
#include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hpp"


namespace MoReFEM::Internal::FormulationSolverNS
{


    /*!
     * \brief Init a \a InitialCondition from the content of the input data file.
     *
     * \tparam LuaFieldT Lua field considered in the input file.
     *
     * \param[in] mesh Mesh upon which the InitialCondition is applied. Initial condition
     * might be requested at each of each \a Coords.
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * Condition is actually split in three \a ScalarInitialCondition.
     *
     * \return The \a InitialCondition object properly initialized.
     */
    // clang-format off
    template
    <
        class LuaFieldT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    InitialCondition<ParameterNS::Type::vector, Eigen::Vector3d>::unique_ptr
    InitThreeDimensionalInitialCondition(const Mesh& mesh, const MoReFEMDataT& morefem_data);


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITTHREEDIMENSIONALINITIALCONDITION_DOT_HPP_
// *** MoReFEM end header guards *** < //
