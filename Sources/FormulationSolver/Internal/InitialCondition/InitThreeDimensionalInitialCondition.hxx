// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITTHREEDIMENSIONALINITIALCONDITION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITTHREEDIMENSIONALINITIALCONDITION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::FormulationSolverNS
{


    // clang-format off
    template
    <
        class InitialConditionT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    auto InitThreeDimensionalInitialCondition(const Mesh& mesh, const MoReFEMDataT& morefem_data)
        -> InitialCondition<ParameterNS::Type::vector, Eigen::Vector3d>::unique_ptr
    {
        decltype(auto) nature_vector =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename InitialConditionT::Nature>(morefem_data);

        assert(nature_vector.size() == 3UL);
        const auto& nature_x = nature_vector[0];
        const auto& nature_y = nature_vector[1];
        const auto& nature_z = nature_vector[2];

        if ((nature_x == "ignore" || nature_y == "ignore" || nature_z == "ignore")
            && (nature_x != nature_y || nature_x != nature_z))
            throw Exception("Error for " + InitialConditionT::GetName()
                            + ": if "
                              "one of the item is 'ignore' the other ones should be 'ignore' as well.");

        if (nature_x == "ignore")
            return nullptr;

        decltype(auto) value_per_component =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename InitialConditionT::Value>(morefem_data);

        using value_per_component_type = std::decay_t<decltype(value_per_component)>;

        static_assert(Utilities::IsSpecializationOf<std::vector, value_per_component_type>());
        assert(value_per_component.size() == 3UL);

        auto&& component_x = InitScalarInitialCondition(mesh, nature_x, value_per_component[0]);
        auto&& component_y = InitScalarInitialCondition(mesh, nature_y, value_per_component[1]);
        auto&& component_z = InitScalarInitialCondition(mesh, nature_z, value_per_component[2]);

        return std::make_unique<ThreeDimensionalInitialCondition>(
            std::move(component_x), std::move(component_y), std::move(component_z));
    }


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITTHREEDIMENSIONALINITIALCONDITION_DOT_HXX_
// *** MoReFEM end header guards *** < //
