// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITIALCONDITION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITIALCONDITION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::FormulationSolverNS
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    InitialCondition<TypeT, StorageT>::InitialCondition(const Mesh& mesh) : mesh_(mesh)
    { }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline auto InitialCondition<TypeT, StorageT>::GetConstantValue() const -> return_type
    {
        assert(IsConstant() && "This method is relevant only for spatially constant parameters.");

        return SupplGetConstantValue();
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline auto InitialCondition<TypeT, StorageT>::GetValue(const SpatialPoint& coords) const -> return_type
    {
        if (IsConstant())
            return GetConstantValue();

        return SupplGetValue(coords);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline auto InitialCondition<TypeT, StorageT>::GetMesh() const noexcept -> const Mesh&
    {
        return mesh_;
    }


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITIALCONDITION_DOT_HXX_
// *** MoReFEM end header guards *** < //
