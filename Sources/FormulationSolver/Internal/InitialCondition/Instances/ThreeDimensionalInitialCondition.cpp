// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hpp"

#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::FormulationSolverNS
{


    ThreeDimensionalInitialCondition::ThreeDimensionalInitialCondition(scalar_initial_condition_ptr&& x_component,
                                                                       scalar_initial_condition_ptr&& y_component,
                                                                       scalar_initial_condition_ptr&& z_component)
    : parent(x_component->GetMesh()), scalar_initial_condition_x_(std::move(x_component)),
      scalar_initial_condition_y_(std::move(y_component)), scalar_initial_condition_z_(std::move(z_component))
    {
        assert(GetMesh() == GetScalarInitialConditionY().GetMesh());
        assert(GetMesh() == GetScalarInitialConditionZ().GetMesh());

        content_.resize(3);

        if (IsConstant())
        {
            content_(0) = GetScalarInitialConditionX().GetConstantValue();
            content_(1) = GetScalarInitialConditionY().GetConstantValue();
            content_(2) = GetScalarInitialConditionZ().GetConstantValue();
        }
    }


    ThreeDimensionalInitialCondition::~ThreeDimensionalInitialCondition() = default;


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //
