// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INSTANCES_THREEDIMENSIONALINITIALCONDITION_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INSTANCES_THREEDIMENSIONALINITIALCONDITION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class SpatialPoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FormulationSolverNS
{


    /*!
     * \brief Class to handle a 3D parameter (for instance a force).
     *
     * Such objects should in most if all cases be initialized with InitThreeDimensionalInitialCondition() free
     * function.
     */
    class ThreeDimensionalInitialCondition final : public InitialCondition<ParameterNS::Type::vector, Eigen::Vector3d>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = ThreeDimensionalInitialCondition;

        //! Alias to base class.
        using parent = InitialCondition<ParameterNS::Type::vector, Eigen::Vector3d>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to return type.
        using return_type = typename parent::return_type;

        //! Alias to traits of parent class.
        using traits = typename parent::traits;

        //! Alias to scalar parameter.
        using scalar_initial_condition = InitialCondition<ParameterNS::Type::scalar>;

        //! Alias to scalar parameter unique_ptr.
        using scalar_initial_condition_ptr = scalar_initial_condition::unique_ptr;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] x_component The scalar \a InitialCondition that represents the x component.
         * \param[in] y_component The scalar \a InitialCondition that represents the y component.
         * \param[in] z_component The scalar \a InitialCondition that represents the z component.
         */
        explicit ThreeDimensionalInitialCondition(scalar_initial_condition_ptr&& x_component,
                                                  scalar_initial_condition_ptr&& y_component,
                                                  scalar_initial_condition_ptr&& z_component);

        //! Destructor.
        ~ThreeDimensionalInitialCondition() override;

        //! \copydoc doxygen_hide_copy_constructor
        ThreeDimensionalInitialCondition(const ThreeDimensionalInitialCondition& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ThreeDimensionalInitialCondition(ThreeDimensionalInitialCondition&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ThreeDimensionalInitialCondition& operator=(const ThreeDimensionalInitialCondition& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ThreeDimensionalInitialCondition& operator=(ThreeDimensionalInitialCondition&& rhs) = delete;

        ///@}


      private:
        /*!
         * \brief Returns the constant value (if the parameters is constant).
         *
         * \internal <b><tt>[internal]</tt></b> This method is called by GetConstantValue() once the fact
         * the parameter is spatially constant has been asserted.
         * \endinternal
         *
         * \return Spatially constant value.
         */
        return_type SupplGetConstantValue() const override;

        /*!
         * \brief Get the value of the parameter.
         *
         * \param[in] coords \a Coords at which the initial condition must be evaluated.
         *
         * \return Value of the initial condition.
         */
        return_type SupplGetValue(const SpatialPoint& coords) const override;

        //! Whether the parameter varies spatially or not.
        bool IsConstant() const override;


      private:
        //! Access to contribution of component x.
        scalar_initial_condition& GetScalarInitialConditionX() const noexcept;

        //! Access to contribution of component y.
        scalar_initial_condition& GetScalarInitialConditionY() const noexcept;

        //! Access to contribution of component z.
        scalar_initial_condition& GetScalarInitialConditionZ() const noexcept;


      private:
        //! Contribution of the x component to the vectorial parameter.
        scalar_initial_condition_ptr scalar_initial_condition_x_;

        //! Contribution of the y component to the vectorial parameter.
        scalar_initial_condition_ptr scalar_initial_condition_y_;

        //! Contribution of the z component to the vectorial parameter.
        scalar_initial_condition_ptr scalar_initial_condition_z_;

        //! Content of the parameter.
        mutable traits::value_type content_;
    };


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INSTANCES_THREEDIMENSIONALINITIALCONDITION_DOT_HPP_
// *** MoReFEM end header guards *** < //
