// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_CONSTANT_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_CONSTANT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::FormulationSolverNS::Policy
{


    /*!
     * \brief Parameter policy when the parameter gets the same value everywhere.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    template<ParameterNS::Type TypeT>
    class Constant
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Constant<TypeT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

      private:
        //! Alias to traits class related to TypeT.
        using traits = ::MoReFEM::Internal::ParameterNS::Traits<TypeT>;

      public:
        //! Alias to the return type.
        using return_type = typename traits::return_type;

        //! Alias to the type of the value actually stored.
        using storage_type = std::decay_t<return_type>;

        //! Alias.
        using value_type = typename traits::value_type;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] value Value of the constant initial condition.
        explicit Constant(storage_type value);

        //! Destructor.
        ~Constant() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Constant(const Constant& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Constant(Constant&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Constant& operator=(const Constant& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Constant& operator=(Constant&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Enables to modify the constant value of a parameter.
         *
         * \param[in] value Constant value.
         */
        void SetConstantValue(value_type value) noexcept;

      protected:
        //! Get the value.
        return_type GetConstantValueFromPolicy() const;

        //! Provided here to make the code compile, but should never be called.
        [[noreturn]] return_type GetValueFromPolicy(const SpatialPoint&) const;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        return_type GetAnyValueFromPolicy() const;

      protected:
        //! Whether the parameter varies spatially or not.
        constexpr bool IsConstant() const noexcept;


        //! Write the content of the parameter for which policy is used in a stream.
        //! \copydoc doxygen_hide_stream_inout
        void WriteFromPolicy(std::ostream& stream) const;

      private:
        //! Get the value of the parameter.
        storage_type value_;
    };


} // namespace MoReFEM::Internal::FormulationSolverNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_CONSTANT_DOT_HPP_
// *** MoReFEM end header guards *** < //
