// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_CONSTANT_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_CONSTANT_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/Policy/Constant.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::FormulationSolverNS::Policy
{


    template<ParameterNS::Type TypeT>
    Constant<TypeT>::Constant(typename Constant<TypeT>::storage_type value) : value_(value)
    { }


    template<ParameterNS::Type TypeT>
    [[noreturn]] typename Constant<TypeT>::return_type
    Constant<TypeT>::GetValueFromPolicy([[maybe_unused]] const SpatialPoint& coords) const
    {
        assert(false && "Parameter class should have guided toward GetConstantValue()!");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT>
    inline typename Constant<TypeT>::return_type Constant<TypeT>::GetConstantValueFromPolicy() const
    {
        return value_;
    }


    template<ParameterNS::Type TypeT>
    inline typename Constant<TypeT>::return_type Constant<TypeT>::GetAnyValueFromPolicy() const
    {
        return GetConstantValueFromPolicy();
    }


    template<ParameterNS::Type TypeT>
    inline constexpr bool Constant<TypeT>::IsConstant() const noexcept
    {
        return true;
    }


    template<ParameterNS::Type TypeT>
    void Constant<TypeT>::WriteFromPolicy(std::ostream& stream) const
    {
        stream << "# Homogeneous value:" << std::endl;
        stream << GetConstantValueFromPolicy() << std::endl;
    }


    template<ParameterNS::Type TypeT>
    inline void Constant<TypeT>::SetConstantValue(value_type value) noexcept
    {
        value_ = value;
    }


} // namespace MoReFEM::Internal::FormulationSolverNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_CONSTANT_DOT_HXX_
// *** MoReFEM end header guards *** < //
