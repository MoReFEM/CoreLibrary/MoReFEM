// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_LUAFUNCTION_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_LUAFUNCTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"


namespace MoReFEM::Internal::FormulationSolverNS::Policy
{


    /*!
     * \brief Parameter policy when the parameter gets a value given by an analytic function provided in the
     * input data file.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    template<ParameterNS::Type TypeT>
    class LuaFunction
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LuaFunction<TypeT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

      private:
        //! Alias to traits class related to TypeT.
        using traits = ::MoReFEM::Internal::ParameterNS::Traits<TypeT>;

      public:
        //! Alias to return type.
        using return_type = typename traits::return_type;

        //! Alias to the type of the Lua function.
        using lua_function_type = ::MoReFEM::Wrappers::Lua::spatial_function;

        //! Alias to the storage of the Lua function.
        using storage_type = const lua_function_type&;

        //! Alias
        using value_type = typename traits::value_type;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] lua_function Lua function as read in the input data file.
        explicit LuaFunction(storage_type lua_function);

        //! Destructor.
        ~LuaFunction() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LuaFunction(const LuaFunction& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LuaFunction(LuaFunction&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LuaFunction& operator=(const LuaFunction& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LuaFunction& operator=(LuaFunction&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Enables to modify the constant value of a parameter. Disabled for this Policy.
         */
        void SetConstantValue(value_type);

      protected:
        /*!
         * \brief Provided here to make the code compile, but should never be called
         *
         * (Constant value should not be given through a function: it is less efficient and the constness
         * is not appearant in the code...).
         *
         * \return Irrelevant here.
         */
        [[noreturn]] return_type GetConstantValueFromPolicy() const;

        //! Return the value for \a quadrature_point and \a geom_elt.
        //! \param[in] coords Spatial position at which the initial condition is evaluated.
        return_type GetValueFromPolicy(const SpatialPoint& coords) const;


        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        return_type GetAnyValueFromPolicy() const;

      protected:
        //! Whether the parameter varies spatially or not.
        bool IsConstant() const;

        //! Write the content of the parameter for which policy is used in a stream.
        //! \copydoc doxygen_hide_stream_inout
        void WriteFromPolicy(std::ostream& stream) const;

      private:
        /*!
         * \brief Non constant accessor to the helper Coords object, useful when Lua function acts upon global
         * elements.
         *
         * \internal <b><tt>[internal]</tt></b> This is const because the helper coords is mutable to be
         * used in const methods.
         * \endinternal
         *
         * \return Reference to the helper Coords object.
         */
        SpatialPoint& GetNonCstWorkCoords() const noexcept;

      private:
        //! Store a reference to the Lua function (which is owned by the InputData class).
        storage_type lua_function_;

        //! Helper Coords object, useful when Lua function acts upon global elements.
        mutable SpatialPoint work_coords_;
    };


} // namespace MoReFEM::Internal::FormulationSolverNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/InitialCondition/Policy/LuaFunction.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_LUAFUNCTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
