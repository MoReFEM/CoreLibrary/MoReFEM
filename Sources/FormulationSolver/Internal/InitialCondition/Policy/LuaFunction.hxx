// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_LUAFUNCTION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_LUAFUNCTION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/Policy/LuaFunction.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::FormulationSolverNS::Policy
{


    template<ParameterNS::Type TypeT>
    LuaFunction<TypeT>::LuaFunction(storage_type lua_function) : lua_function_(lua_function)
    { }


    template<ParameterNS::Type TypeT>
    auto LuaFunction<TypeT>::GetValueFromPolicy(const SpatialPoint& coords) const -> return_type
    {
        return lua_function_(coords[::MoReFEM::GeometryNS::dimension_type{ 0 }],
                             coords[::MoReFEM::GeometryNS::dimension_type{ 1 }],
                             coords[::MoReFEM::GeometryNS::dimension_type{ 2 }]);
    }


    template<ParameterNS::Type TypeT>
    auto LuaFunction<TypeT>::GetAnyValueFromPolicy() const -> return_type
    {
        return lua_function_(0., 0., 0.);
    }


    template<ParameterNS::Type TypeT>
    [[noreturn]] typename LuaFunction<TypeT>::return_type LuaFunction<TypeT>::GetConstantValueFromPolicy() const
    {
        assert(false && "A Lua function should yield IsConstant() = false!");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT>
    bool LuaFunction<TypeT>::IsConstant() const
    {
        return false;
    }

    template<ParameterNS::Type TypeT>
    void LuaFunction<TypeT>::WriteFromPolicy(std::ostream& stream) const
    {
        stream << "# Given by the Lua function given in the input data file." << std::endl;
    }


    template<ParameterNS::Type TypeT>
    inline SpatialPoint& LuaFunction<TypeT>::GetNonCstWorkCoords() const noexcept
    {
        return work_coords_;
    }


    template<ParameterNS::Type TypeT>
    inline void LuaFunction<TypeT>::SetConstantValue([[maybe_unused]] value_type value)
    {
        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::FormulationSolverNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_POLICY_LUAFUNCTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
