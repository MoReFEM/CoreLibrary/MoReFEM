### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FORMULATION_SOLVER}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Constant.hpp
		${CMAKE_CURRENT_LIST_DIR}/Constant.hxx
		${CMAKE_CURRENT_LIST_DIR}/LuaFunction.hpp
		${CMAKE_CURRENT_LIST_DIR}/LuaFunction.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

