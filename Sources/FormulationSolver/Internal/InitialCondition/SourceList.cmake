### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FORMULATION_SOLVER}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/InitScalarInitialCondition.hpp
		${CMAKE_CURRENT_LIST_DIR}/InitScalarInitialCondition.hxx
		${CMAKE_CURRENT_LIST_DIR}/InitThreeDimensionalInitialCondition.hpp
		${CMAKE_CURRENT_LIST_DIR}/InitThreeDimensionalInitialCondition.hxx
		${CMAKE_CURRENT_LIST_DIR}/InitialCondition.hpp
		${CMAKE_CURRENT_LIST_DIR}/InitialCondition.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Impl/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instances/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
