// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_RESTART_RESTART_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_RESTART_RESTART_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <string_view>

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM::Internal::RestartNS
{

    /*!
     * \brief Compute the filename that which hold the \a vector data for the
     * proper time iteration (without its encompassing directory)
     *
     * \copydoc doxygen_hide_restart_tag_arg
     *
     * \param[in] time_manager The \a TimeManager provides the base directory in which all restart data may be found,
     * regardless of \a GodOfDof and \a NumberingSubset.
     *
     * \return Filename as a string.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    std::string ComputeFilename(const TimeManagerT& time_manager, std::string_view tag);


    /*!
     * \brief Returns the directory in which restart data may be found.
     *
     * \param[in] time_manager The \a TimeManager provides the base directory in which all restart data may be found,
     * regardless of \a GodOfDof and \a NumberingSubset.
     * \param[in] god_of_dof The \a GodOfDof considered - in practice used to get a step further and find the
     * subdirectory dealing with it.
     * \param[in] numbering_subset The \a NumberingSubset considered - in practice used to get a step further and find the
     * subdirectory dealing with it.
     *
     * \return The full directory in which the restart files may be found.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    ::MoReFEM::FilesystemNS::Directory GetDataDirectory(const TimeManagerT& time_manager,
                                                        const GodOfDof& god_of_dof,
                                                        const NumberingSubset& numbering_subset);


} // namespace MoReFEM::Internal::RestartNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/Restart/Restart.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_RESTART_RESTART_DOT_HPP_
// *** MoReFEM end header guards *** < //
