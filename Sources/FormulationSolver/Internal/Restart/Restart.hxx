// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_RESTART_RESTART_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_RESTART_RESTART_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/Restart/Restart.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::RestartNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    std::string ComputeFilename(const TimeManagerT& time_manager, std::string_view tag)
    {
        std::ostringstream oconv;
        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        oconv << "restart_" << tag << '_' << std::setfill('0') << std::setw(5) << time_manager.NtimeModified()
              << ::MoReFEM::Advanced::AsciiOrBinaryNS::DatafileExtension(binary_or_ascii::binary);
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        std::string filename = oconv.str();
        return filename;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    auto GetDataDirectory(const TimeManagerT& time_manager,
                          const GodOfDof& god_of_dof,
                          const NumberingSubset& numbering_subset) -> ::MoReFEM::FilesystemNS::Directory
    {
        auto ret = time_manager.GetRestartDataDirectory();
        ret.AddSubdirectory("Mesh_" + std::to_string(god_of_dof.GetUniqueId().Get()));
        ret.AddSubdirectory("NumberingSubset_" + std::to_string(numbering_subset.GetUniqueId().Get()));

        return ret;
    }


} // namespace MoReFEM::Internal::RestartNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_RESTART_RESTART_DOT_HXX_
// *** MoReFEM end header guards *** < //
