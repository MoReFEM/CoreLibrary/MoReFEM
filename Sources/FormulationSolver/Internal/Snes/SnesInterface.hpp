// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SNESINTERFACE_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SNESINTERFACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Petsc/Print.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/SnesMacro.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: export

#include "Core/LinearAlgebra/GlobalVector.hpp"


namespace MoReFEM::Internal::SolverNS
{


    /*!
     * \brief Class that includes the most possible generic functions required by SNES.
     *
     * \tparam VariationalFormulationT Your \a VariationalFormulation in which a SolveNonLinear() call is issued.
     *
     * \attention A friendship should be written in \a VariationalFormulationT toward current class:
     * \code
     * friend struct Internal::SolverNS::SnesInterface<self>;
     * \endcode
     * where self is the internal alias toward the \a VariationalFormulationT.
     *
     * The prerequisites of a valid \a VariationalFormulationT are explained in the header of \a VariationalFormulation
     * base class.
     */
    template<class VariationalFormulationT>
    struct SnesInterface
    {

      public:
        //! Constructor.
        SnesInterface() = default;

        //! Destructor.
        ~SnesInterface() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SnesInterface(const SnesInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor - deactivated.
        SnesInterface(SnesInterface&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SnesInterface& operator=(const SnesInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SnesInterface& operator=(SnesInterface&& rhs) = delete;


      public:
        /*!
         * \brief This function is given to SNES to compute the function.
         *
         * \copydoc doxygen_hide_snes_interface_common_arg
         * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
         * \param[in,out] residual Petsc vector that include residual. It is not used per se in MoReFEM; there is
         * however a check in debug that VariationalFormulationT::GetSystemRhs() contains the same data as this
         * parameter.
         *
         *
         */
        static PetscErrorCode Function(SNES snes, Vec evaluation_state, Vec residual, void* context_as_void);


        /*!
         * \brief This function is supposed to be given to SNES to compute the jacobian.
         *
         * \copydoc doxygen_hide_snes_interface_common_arg
         * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
         * \param[in,out] jacobian Jacobian matrix. Actually unused in our wrapper.
         * \param[in,out] preconditioner Preconditioner matrix. Actually unused in our wrapper.
         */
        static PetscErrorCode
        Jacobian(SNES snes, Vec evaluation_state, Mat jacobian, Mat preconditioner, void* context_as_void);


        /*!
         * \brief This function is used to monitor evolution of the convergence.
         *
         * Is is intended to be passed to SNESMonitorSet()
         *
         * \copydoc doxygen_hide_snes_interface_common_arg
         * \param[in] its Index of iteration.
         * \param[in] norm Current L^2 norm.
         */
        static PetscErrorCode Viewer(SNES snes, PetscInt its, PetscReal norm, void* context_as_void);
    };


} // namespace MoReFEM::Internal::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/Snes/SnesInterface.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SNESINTERFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
