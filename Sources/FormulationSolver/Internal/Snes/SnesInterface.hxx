// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SNESINTERFACE_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SNESINTERFACE_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/Snes/SnesInterface.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/Wrappers/Petsc/SnesMacro.hpp" // IWYU pragma: export

#include "Core/LinearAlgebra/GlobalMatrix.hpp" // IWYU pragma: export
#include "Core/LinearAlgebra/GlobalVector.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::SolverNS
{


    template<class VariationalFormulationT>
    PetscErrorCode SnesInterface<VariationalFormulationT>::Function([[maybe_unused]] SNES snes,
                                                                    Vec petsc_evaluation_state,
                                                                    Vec petsc_residual,
                                                                    void* context_as_void)
    {
        MOREFEM_TRAP_SNES_EXCEPTION_TRY
        {
            // context_as_void is in fact the VariationalFormulation object.
            assert(!(!context_as_void));
            VariationalFormulationT* variational_formulation_ptr =
                static_cast<VariationalFormulationT*>(context_as_void);
            auto& variational_formulation = *variational_formulation_ptr;

            decltype(auto) row_numbering_subset = variational_formulation.GetRowNumberingSubset();
            decltype(auto) col_numbering_subset = variational_formulation.GetColumnNumberingSubset();

            GlobalVector residual(row_numbering_subset, Internal::WrapPetscVec(petsc_residual));
            residual.ZeroEntries();

            GlobalVector evaluation_state(col_numbering_subset, Internal::WrapPetscVec(petsc_evaluation_state));

            variational_formulation.ComputeResidual(evaluation_state, residual);
        }
        MOREFEM_TRAP_SNES_EXCEPTION_CATCH(SnesFunction)

        return 0;
    }


    template<class VariationalFormulationT>
    PetscErrorCode SnesInterface<VariationalFormulationT>::Jacobian([[maybe_unused]] SNES snes,
                                                                    Vec petsc_evaluation_state,
                                                                    Mat petsc_jacobian,
                                                                    Mat petsc_preconditioner,
                                                                    void* context_as_void)
    {
        MOREFEM_TRAP_SNES_EXCEPTION_TRY
        {
            // context_as_void is in fact the VariationalFormulation object.
            assert(context_as_void);
            VariationalFormulationT* formulation_ptr = static_cast<VariationalFormulationT*>(context_as_void);
            auto& variational_formulation = *formulation_ptr;

            decltype(auto) row_numbering_subset = variational_formulation.GetRowNumberingSubset();
            decltype(auto) col_numbering_subset = variational_formulation.GetColumnNumberingSubset();
            decltype(auto) mpi = variational_formulation.GetMpi();

            GlobalVector evaluation_state(col_numbering_subset, Internal::WrapPetscVec(petsc_evaluation_state));

            GlobalMatrix tangent(
                mpi, row_numbering_subset, col_numbering_subset, Internal::WrapPetscMat(petsc_jacobian));
            tangent.ZeroEntries();

            GlobalMatrix preconditioner(
                mpi, row_numbering_subset, col_numbering_subset, Internal::WrapPetscMat(petsc_preconditioner));
            preconditioner.ZeroEntries();

            variational_formulation.ComputeTangent(evaluation_state, tangent, preconditioner);
        }
        MOREFEM_TRAP_SNES_EXCEPTION_CATCH(SnesJacobian)

        return 0;
    }


    template<class VariationalFormulationT>
    PetscErrorCode SnesInterface<VariationalFormulationT>::Viewer([[maybe_unused]] SNES snes,
                                                                  PetscInt its,
                                                                  PetscReal norm,
                                                                  void* context_as_void)
    {
        MOREFEM_TRAP_SNES_EXCEPTION_TRY
        {
            assert(context_as_void);

            const VariationalFormulationT* variational_formulation_ptr =
                static_cast<const VariationalFormulationT*>(context_as_void);

            assert(!(!variational_formulation_ptr));

            const auto& variational_formulation = *variational_formulation_ptr;

            variational_formulation.SnesMonitorFunction(its, norm);
        }
        MOREFEM_TRAP_SNES_EXCEPTION_CATCH(SnesViewer)

        return 0;
    }


} // namespace MoReFEM::Internal::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SNESINTERFACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
