// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SOLVER_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SOLVER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/InputData/InputData.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"           // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Solver/Petsc.hpp"


namespace MoReFEM::Internal::SolverNS
{


    /*!
     * \brief Init Petsc solver.
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * \copydoc doxygen_hide_snes_functions_args
     *
     * \return \a Wrappers::Petsc::Snes correctly initialized.
     */
    // clang-format off
    template
    <
        std::size_t SolverIndexT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    ::MoReFEM::Wrappers::Petsc::Snes::unique_ptr
    InitSolver(const MoReFEMDataT& morefem_data,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESFunction snes_function = nullptr,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESJacobian snes_jacobian = nullptr,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESViewer snes_viewer = nullptr,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function = nullptr);


} // namespace MoReFEM::Internal::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/Snes/Solver.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SOLVER_DOT_HPP_
// *** MoReFEM end header guards *** < //
