### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FORMULATION_SOLVER}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/InitialCondition/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Restart/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Snes/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Storage/SourceList.cmake)
