// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <set>
#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/Internal/FindFunctor.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::VarfNS
{


    const GlobalMatrix& GlobalMatrixStorage::GetMatrix(const NumberingSubset& row_numbering_subset,
                                                       const NumberingSubset& col_numbering_subset,
                                                       [[maybe_unused]] const std::source_location location) const

    {
        const auto& storage = GetStorage();

        assert(!storage.empty()
               && "You probably forgot to call Init() method just after the constructor of "
                  "your variational formulation!");

        const auto it = std::ranges::find_if(storage,

                                             NumberingSubsetNS::FindIfConditionForPair<GlobalMatrix::unique_ptr>(
                                                 row_numbering_subset, col_numbering_subset));

#ifndef NDEBUG
        {
            if (it == storage.cend())
            {
                std::ostringstream oconv;

                oconv << "In GlobalMatrixStorage, matrix with numbering subsets (" << row_numbering_subset.GetUniqueId()
                      << ", " << col_numbering_subset.GetUniqueId()
                      << ") was requested but was not properly allocated beforehand with `AllocateGlobalMatrix()`"
                         " or `VariationalFormulation::AllocateSystemMatrix()` calls.";
                throw Exception(oconv.str(), location);
            }
        }
#endif // NDEBUG

        assert(!(!*it));
        return *(*it);
    }


    GlobalMatrix& GlobalMatrixStorage::NewMatrix(const NumberingSubset& row_numbering_subset,
                                                 const NumberingSubset& col_numbering_subset,
                                                 std::string&& name)
    {
        auto&& new_item = std::make_unique<GlobalMatrix>(row_numbering_subset, col_numbering_subset, std::move(name));

        auto& storage = GetNonCstStorage();
        storage.emplace_back(std::move(new_item));

#ifndef NDEBUG
        AssertNoDuplicate();
#endif // NDEBUG

        return *(storage.back());
    }


#ifndef NDEBUG
    void GlobalMatrixStorage::AssertNoDuplicate() const
    {
        const auto& storage = GetStorage();

        std::set<std::pair<::MoReFEM::NumberingSubsetNS::unique_id, ::MoReFEM::NumberingSubsetNS::unique_id>> id_list;
        for (const auto& ptr : storage)
        {
            assert(!(!ptr));
            auto check = id_list.insert(
                { ptr->GetRowNumberingSubset().GetUniqueId(), ptr->GetColNumberingSubset().GetUniqueId() });

            assert(check.second && "A given unique Id should be present only once!");
        }
    }
#endif // NDEBUG


} // namespace MoReFEM::Internal::VarfNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //
