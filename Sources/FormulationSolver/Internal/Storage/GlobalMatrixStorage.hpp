// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALMATRIXSTORAGE_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALMATRIXSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::VarfNS
{


    /*!
     * \brief Class in charge of storing global matrices.
     *
     * \internal <b><tt>[internal]</tt></b> Actual storage use a 'flat-map': it is a STL vector upon which find
     * algorithm is used.
     * The reason for this is that we do not expect many global matrices stored, so flat map is likely to
     * be more efficient that std::map or even std::unordered_map.
     * The key for the search is the pair of numbering subsets that identify which dofs are used respectively
     * for rows and columns.
     * \endinternal
     */
    class GlobalMatrixStorage final
    {

      public:
        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const GlobalMatrixStorage>;

        //! Alias to vector of unique pointers.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit GlobalMatrixStorage() = default;

        //! Destructor.
        ~GlobalMatrixStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalMatrixStorage(const GlobalMatrixStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalMatrixStorage(GlobalMatrixStorage&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalMatrixStorage& operator=(const GlobalMatrixStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalMatrixStorage& operator=(GlobalMatrixStorage&& rhs) = delete;

        ///@}


        /*!
         * \brief Add the matrix described by the given numbering subsets.
         *
         * There can be only one for a given pair of numbering subset; however ordering matters (so
         * { 1, 2 } and { 2, 1 } can both exist, when 1 and 2 are numbering subset unique ids).
         *
         * \copydetails doxygen_hide_matrix_numbering_subset_arg
         *
         * \copydetails doxygen_hide_petsc_vector_name_arg
         *
         * \return A reference to the newly created matrix.
         */
        GlobalMatrix& NewMatrix(const NumberingSubset& row_numbering_subset,
                                const NumberingSubset& col_numbering_subset,
                                std::string&& name);


        //! Access to the global matrix matching the given pair of numbering subset.
        //! \copydetails doxygen_hide_matrix_numbering_subset_arg
        //!
        //! \copydoc doxygen_hide_source_location
        const GlobalMatrix& GetMatrix(const NumberingSubset& row_numbering_subset,
                                      const NumberingSubset& col_numbering_subset,
                                      std::source_location location = std::source_location::current()) const;

        //! Non constant access to the global matrix matching the given pair of numbering subset.
        //! \copydetails doxygen_hide_matrix_numbering_subset_arg
        //!
        //! \copydoc doxygen_hide_source_location
        GlobalMatrix& GetNonCstMatrix(const NumberingSubset& row_numbering_subset,
                                      const NumberingSubset& col_numbering_subset,
                                      std::source_location location = std::source_location::current());


      private:
        //! Access to the internal storage.
        const GlobalMatrix::vector_unique_ptr& GetStorage() const;

        //! Non constant access to the internal storage.
        GlobalMatrix::vector_unique_ptr& GetNonCstStorage();

#ifndef NDEBUG
        //! Check \a NewMatrix() is called at most once for each numbering subset.
        void AssertNoDuplicate() const;
#endif // NDEBUG


      private:
        //! List of all global matrices with the numbering subsets used to find them.
        GlobalMatrix::vector_unique_ptr storage_;
    };


} // namespace MoReFEM::Internal::VarfNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALMATRIXSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
