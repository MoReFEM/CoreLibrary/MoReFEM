// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <set>
#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FormulationSolver/Internal/Storage/GlobalVectorStorage.hpp"

#include "Core/NumberingSubset/Internal/FindFunctor.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM::Internal::VarfNS
{


    const GlobalVector& GlobalVectorStorage::GetVector(const NumberingSubset& numbering_subset,
                                                       [[maybe_unused]] const std::source_location location) const
    {
        const auto& storage = GetStorage();

        const auto it =
            std::ranges::find_if(storage,

                                 NumberingSubsetNS::FindIfCondition<GlobalVector::unique_ptr>(numbering_subset));

#ifndef NDEBUG
        {
            if (it == storage.cend())
            {
                std::ostringstream oconv;

                oconv << "In GlobalVectorStorage, vector with numbering subset " << numbering_subset.GetUniqueId()
                      << " was requested but was not properly allocated beforehand with `AllocateGlobalVector()`"
                         " or `VariationalFormulation::AllocateSystemVector()` calls.";
                throw Exception(oconv.str(), location);
            }
        }
#endif // NDEBUG

        assert(!(!*it));
        return *(*it);
    }


    GlobalVector& GlobalVectorStorage::NewVector(const GodOfDof& god_of_dof,
                                                 const NumberingSubset& numbering_subset,
                                                 std::string&& name)
    {
        auto ptr = std::make_unique<GlobalVector>(numbering_subset, std::move(name));
        AllocateGlobalVector(god_of_dof, *ptr);

        auto& storage = GetNonCstStorage();
        storage.emplace_back(std::move(ptr));

#ifndef NDEBUG
        AssertNoDuplicate();
#endif // NDEBUG

        return *(storage.back());
    }


#ifndef NDEBUG
    void GlobalVectorStorage::AssertNoDuplicate() const
    {
        const auto& storage = GetStorage();

        std::set<::MoReFEM::NumberingSubsetNS::unique_id> id_list;
        for (const auto& ptr : storage)
        {
            assert(!(!ptr));
            auto check = id_list.insert(ptr->GetNumberingSubset().GetUniqueId());

            assert(check.second && "A given unique Id should be present only once!");
        }
    }
#endif // NDEBUG


} // namespace MoReFEM::Internal::VarfNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //
