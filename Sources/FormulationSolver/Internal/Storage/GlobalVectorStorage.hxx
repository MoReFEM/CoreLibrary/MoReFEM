// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALVECTORSTORAGE_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALVECTORSTORAGE_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/Storage/GlobalVectorStorage.hpp"
// *** MoReFEM header guards *** < //


#include "Core/LinearAlgebra/GlobalVector.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::VarfNS
{


    inline GlobalVector& GlobalVectorStorage::GetNonCstVector(const NumberingSubset& numbering_subset,
                                                              const std::source_location location)
    {
        return const_cast<GlobalVector&>(GetVector(numbering_subset, location));
    }


    inline const GlobalVector::vector_unique_ptr& GlobalVectorStorage ::GetStorage() const
    {
        return storage_;
    }


    inline GlobalVector::vector_unique_ptr& GlobalVectorStorage ::GetNonCstStorage()
    {
        return storage_;
    }


} // namespace MoReFEM::Internal::VarfNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALVECTORSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
