/*!
 * \defgroup FormulationSolverGroup Variational formulation
 * 
 * \brief Module dedicated to the definition of a variational formulation.
 *
 */


/// \addtogroup FormulationSolverGroup
///@{

/// \namespace MoReFEM::Internal::VarfNS
/// \brief Namespace that enclose internals related to variational formulation.


/// \namespace MoReFEM::VariationalFormulationNS::DofSourcePolicyNS
/// \brief Namespace that enclose policies to represent a potential source given at dofs. None
/// is by far the most common case.


///@}


/*!
 * \class doxygen_hide_varf_constructor
 *
 * \copydoc doxygen_hide_morefem_data_arg_inout
 * \param[in] god_of_dof \a GodOfDof upon which the variational formulation is defined.
 * \param[in] boundary_condition_list List of Dirichlet boundary conditions to consider (please notice it is also
 * possible to handle them directly; it is for conveniency it might be given here).
 */


/*!
 * \class doxygen_hide_varf_suppl_init
 *
 * \brief Specific initialisation for derived class attributes.
 *
 * \internal <b><tt>[internal]</tt></b> This method is called by base class method VariationalFormulation::Init().
 * \endinternal
 *
 * \copydoc doxygen_hide_morefem_data_arg_in
 */


/*!
 * \class doxygen_hide_snes_interface_common_arg
 *
 * \note The prototype is completely imposed by Petsc; some of the arguments might not be actually used
 * at all.
 *
 * \param[in,out] snes SNES object.
 * \param[in,out] context_as_void In Petsc, optional user-defined function context. In our case it is always
 * a pointer to the \a VariationalFormulationT.
 *
 * \return Petsc error code: 0 if all is right (always returned in current implementation).
 */


/*!
 * \class doxygen_hide_compute_residual
 *
 * \brief Compute the residual.
 *
 * This method is called automatically by \a SolveNonLinear() calls and isn't expected to be used outside this context. If you nonetheless want to use it, make
 * sure the layouts of the parameters are correct and that residual is initially filled with 0..
 *
 * \param[in] evaluation_state MoReFEM counterpart of \a x in [PETSc documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESFunction.html#SNESFunction)
 * \param[out] residual MoReFEM counterpart of \a r in [PETSc documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESFunction.html#SNESFunction)
 */


/*!
 * \class doxygen_hide_compute_tangent
 *
 * \brief Compute the tangent and the preconditioner.
 *
 *  This method is called automatically by \a SolveNonLinear() calls and isn't expected to be used outside this context. If you nonetheless want to use it, make
 * sure the layouts of the parameters are correct and that matrices are initially zeroed.

 * \param[in] evaluation_state MoReFEM counterpart of \a x in [PETSc documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESJacobianFunction.html#SNESJacobianFunction)
 * \param[out]  tangent MoReFEM counterpart of \a Amat in aforementioned PETSc documentation.
 * \param[out]  preconditioner MoReFEM counterpart of \a Pmat in aforementioned PETSc documentation.
 */



/*!
 * \class doxygen_hide_evaluation_state_arg
 *
 * \param[in] evaluation_state The vector with the current state in the non linear solve (named \a x in
 * [PETSc documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESFunction.html#SNESFunction)).
 *
 * The fact this argument is there underlines this method should be used within \a ComputeResidual(...) or \a ComputeTangent() methods
 * (of a \a VariationalFormulation derived class).
 */



/*!
 * \class doxygen_hide_out_tangent_arg
 *
 * \param[out] tangent The matrix that encmpasses PETSc tangent in SNESSolve (\a Amat in
 * [PETSc documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESJacobianFunction.html#SNESJacobianFunction)).
 *
 * The fact this argument is there underlines this method should be used within \a ComputeTangent() method
 * (of a \a VariationalFormulation derived class).
 */


/*!
 * \class doxygen_hide_out_residual_arg
 *
 * \param[out] residual The vector that encmpasses PETSc residual in SNESSolve (\a f in
 * [PETSc documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESFunction.html#SNESFunction)).
 * The fact this argument is there underlines this method should be used within \a ComputeResidual() method
 * (of a \a VariationalFormulation derived class).
*/
