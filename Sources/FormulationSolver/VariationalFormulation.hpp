// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Print.hpp"                   // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"             // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export // IWYU pragma: keep
#include "Core/Solver/Solver.hpp"
#include "Core/TimeManager/Concept.hpp"     // IWYU pragma: export
#include "Core/TimeManager/TimeManager.hpp" // IWYU pragma: export

#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Operators/Enum.hpp"

#include "Parameters/Parameter.hpp"

#include "FormulationSolver/Enum.hpp" // IWYU pragma: export
#include "FormulationSolver/Internal/InitialCondition/InitThreeDimensionalInitialCondition.hpp"
#include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"
#include "FormulationSolver/Internal/Snes/SnesInterface.hpp"
#include "FormulationSolver/Internal/Snes/Solver.hpp"
#include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hpp" // IWYU pragma: export
#include "FormulationSolver/Internal/Storage/GlobalVectorStorage.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief CRTP base for VariationalFormulation.
     *
     * This class is in charge of the bulk of the calculation: storage of global matrices and vectors, assembling,
     * call to the solver.
     *
     * \tparam SolverIndexT Index in the input data file of the solver to use.
     *
     * \tparam DerivedT Class which will inherit from this one through CRTP.
     * DerivedT is expected to define the following methods (otherwise the compilation will fail):
     * - void SupplInit(const MoReFEMData& morefem_data) // where MoReFEMData is defined within
     * the model instance.
     * - void AllocateMatricesAndVectors()
     *
     * Additionally, if NonLinearSolverT is yes the following methods are expected:
     *
     * - void ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
     * where
     *      * evaluation_state is an <i>input</i> data, which is the MoReFEM counterpart of \a x in
     *      [PETSc
     * documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESFunction.html#SNESFunction)
     *      * residual is the <i>output</i> residual computed (\a f in aforementioned PETSc documentation). This
     * residual is a vector with the correct layout and filled with 0. at the beginning of \a ComputeResidual().
     * - void ComputeTangent(const GlobalVector& petsc_evaluation_state, GlobalMatrix& tangent, GlobalMatrix&
     * preconditioner); where
     *      * evaluation_state is an <i>input</i> data, which is the MoReFEM counterpart of \a x in
     *      [PETSc
     * documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESJacobianFunction.html#SNESJacobianFunction)
     *      * tangent is the <i>output</i> tangent computed (\a Amat in aforementioned PETSc documentation).
     *      * preconditioner is the <i>output</i> preconditioner computed (\a Pmat in aforementioned PETSc
     * documentation).
     *
     * \attention \a ComputeResidual() and \a ComputeTangent() are expected to be solely internal functions for SNESSolve(); you may use them
     *  directly if you wish but in this case, you must ensure yourself that input and output arguments have the correct
     * layout and that output values are properly zeroed.
     * \attention This class is specifically designed to be derived <b>once</b> (or at least that the mandatory methods are defined directly in \a DerivedT).
     *
     */
    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT = enable_non_linear_solver::no
    >
    // clang-format on
    class VariationalFormulation
    : public Crtp::CrtpMpi<VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>>
    {
      private:
        //! Alias to current class.
        using self = VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>;

        //! \cond IGNORE_BLOCK_IN_DOXYGEN

        //! Friendship to the struct that defines the functions used in non linear solver.
        //! \a DerivedT is not a mistake...
        friend Internal::SolverNS::SnesInterface<DerivedT>;

        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

      public:
        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

      protected:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * The constructor by itself doesn't do much: most of the initialisation process occurs in the
         * subsequent call to Init().
         *
         * \internal <b><tt>[internal]</tt></b> The reason of this two-step approach is a constraint from C++ language:
         * initialisation calls methods defined in DerivedT through CRTP pattern, and this is illegal to
         * do so in a constructor.
         * This isn't much of an issue: VariationalFormulation is expected to be build solely within base Model
         * class, and this class knows this call must occur (it is already coded once and for all in its internals).
         * \endinternal
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         *
         * \param[in] god_of_dof God of dof into which the formulation works.
         * \param[in] boundary_condition_list List of Dirichlet boundary conditions to take into account in the
         * variational formulation.
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        VariationalFormulation(const GodOfDof& god_of_dof,
                               DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                               MoReFEMDataT& morefem_data);

        //! Destructor.
        virtual ~VariationalFormulation() = default;

        //! \copydoc doxygen_hide_copy_constructor
        VariationalFormulation(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor - deactivated.
        VariationalFormulation(VariationalFormulation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;


        ///@}

      public:
        /*!
         * \brief Performs the complete build of the object.
         *
         * Must be called immediately after the constructor (but it is already taken care of - see constructor
         * help for more details).
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        void Init(const MoReFEMDataT& morefem_data);


      public:
        /*!
         * \class doxygen_hide_varf_solve_linear
         *
         * \brief Perform a linear solve, using Petsc's KSP algorithm.
         *
         * This method does only the resolution itself; assembling or applying boundary conditions must be
         * done beforehand.
         *
         * \tparam IsFactorizedT If the same matrix has already been set up previously you should rather
         * set this parameter to yes to avoid a call to Petsc's KSPSetOperators() and to reuse the already
         * computed factorized matrix. However, in doubt (acceptable during development process...), the safe
         * option is 'no'.
         */

        /*!
         *  \class doxygen_hide_varf_solve_numbering_subset_arg
         *
         * \param[in] row_numbering_subset Solver will be applied on the system matrix which row is given by this
         * numbering subset.
         * \param[in] col_numbering_subset Solver will be applied on the system matrix which column is given by this
         * numbering subset, and with the RHS pointed out by this very same numbering subset.
         */


        /*!
         * \copydoc doxygen_hide_varf_solve_linear
         *
         * \copydetails doxygen_hide_varf_solve_numbering_subset_arg
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] do_print_solver_infos Whether the Petsc solver should print information about the solve in
         * progress on standard output.
         */
        template<IsFactorized IsFactorizedT>
        void SolveLinear(
            const NumberingSubset& row_numbering_subset,
            const NumberingSubset& col_numbering_subset,
            Wrappers::Petsc::print_solver_infos do_print_solver_infos = Wrappers::Petsc::print_solver_infos::yes,
            const std::source_location location = std::source_location::current());


        /*!
         * \copydoc doxygen_hide_varf_solve_linear
         *
         * This version is broader that the overload above, as you can give your own matrix and vector rather than
         * using the so-called 'system' ones.
         *
         * \param[in] matrix Matrix part of the equation to solve (e.g. A in 'A X = R').
         * \param[in] rhs Rhs part of the equation to solve (e.g. R in 'A X = R').
         * \param[out] out Solution part of the equation to solve (e.g. X in 'A X = R'). It must have been properly
         * allocated; only the content is filled.
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] do_print_solver_infos Whether the Petsc solver should print information about the solve in
         * progress on standard output.
         *
         */
        template<IsFactorized IsFactorizedT>
        void SolveLinear(
            const GlobalMatrix& matrix,
            const GlobalVector& rhs,
            GlobalVector& out,
            Wrappers::Petsc::print_solver_infos do_print_solver_infos = Wrappers::Petsc::print_solver_infos::yes,
            const std::source_location location = std::source_location::current());


        /*!
         * \brief Perform a non-linear solve, using Petsc's Newton SNES algorithm.
         *
         * \copydetails doxygen_hide_varf_solve_numbering_subset_arg
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] do_check_convergence Check the convergence if needed. If it does not converge throw an error.
         * \param[in] line_search_type The line search strategy to use. Up to PETSc 3.12, I didn't interact at all with
         * the underlying line search method, but this version introduced a change that made the hyperelastic model
         * converge with difficulty for the static case  - the reason was that for some reason the backtrace strategy
         * was used in place of the basic one. Default is set to the basic method; the available methods are on PETSc
         * online documentation:
         * https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESLineSearchSetType.html
         *
         */
        template<enable_non_linear_solver NonLinT = NonLinearSolverT>
        std::enable_if_t<NonLinT == enable_non_linear_solver::yes, void> SolveNonLinear(
            const NumberingSubset& row_numbering_subset,
            const NumberingSubset& col_numbering_subset,
            Wrappers::Petsc::check_convergence do_check_convergence = Wrappers::Petsc::check_convergence::yes,
            SNESLineSearchType line_search_type = SNESLINESEARCHBASIC,
            const std::source_location location = std::source_location::current());

      public:
        /*!
         * \brief Apply essential boundary counditions.
         *
         * \internal <b><tt>[internal]</tt></b> Natural boundary conditions are managed by dedicated operators (their
         * 'naturalness' is fully used!)
         * \endinternal
         *
         * \tparam AppliedOnT Whether the essential boundary conditions are applied on matrix, vector or both.
         * \tparam BoundaryConditionMethod Choice of the method used to apply Dirichlet boundary conditions.
         *
         * \copydetails doxygen_hide_varf_solve_numbering_subset_arg
         */
        template<VariationalFormulationNS::On AppliedOnT,
                 BoundaryConditionMethod BoundaryConditionMethodT = BoundaryConditionMethod::pseudo_elimination>
        void ApplyEssentialBoundaryCondition(const NumberingSubset& row_numbering_subset,
                                             const NumberingSubset& col_numbering_subset);


        /*!
         * \brief Apply essential boundary counditions on a vector or a matrix which is not a system one.
         *
         *
         * \internal <b><tt>[internal]</tt></b> Natural boundary conditions are managed by dedicated operators (their
         * 'naturalness' is fully used!)
         * \endinternal
         *
         * \tparam BoundaryConditionMethod Choice of the method used to apply Dirichlet boundary conditions.
         * \tparam LinearAlgebraT Either \a GlobalMatrix or \a GlobalVector.
         *
         * \param[in] linear_algebra Vector or matrix to apply the boundary conditions on.
         *
         */
        // clang-format off
        template
        <
            class LinearAlgebraT,
            BoundaryConditionMethod BoundaryConditionMethodT = BoundaryConditionMethod::pseudo_elimination
        >
        // clang-format on
        void ApplyEssentialBoundaryCondition(LinearAlgebraT& linear_algebra);


        /*!
         * \brief Write the solution in the output directory given in the input data file.
         *
         * \copydetails doxygen_hide_time_manager_arg
         * \param[in] numbering_subset Numbering subset onto which solution is described.
         */
        void WriteSolution(const TimeManagerT& time_manager, const NumberingSubset& numbering_subset) const;


      public:
        /*!
         * \class doxygen_hide_varf_matrix_rowcol_numbering_subset
         *
         * \param[in] row_numbering_subset Matrix required is the one which row is described by this numbering subset.
         * \param[in] col_numbering_subset Matrix required is the one which column is described by this numbering
         * subset.
         *
         */

        /*!
         * \class doxygen_hide_varf_matrix_rowcol_numbering_subset_and_return
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset
         *
         * \return Reference to the adequate system matrix.
         */


        /*!
         * \brief Access to the system matrix (A in A X = R).
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset_and_return
         */
        const GlobalMatrix& GetSystemMatrix(const NumberingSubset& row_numbering_subset,
                                            const NumberingSubset& col_numbering_subset) const;

        /*!
         * \brief Non constant access to the system matrix (A in A X = R).
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset_and_return
         */
        GlobalMatrix& GetNonCstSystemMatrix(const NumberingSubset& row_numbering_subset,
                                            const NumberingSubset& col_numbering_subset);


        /*!
         * \class doxygen_hide_varf_vector_numbering_subset
         *
         * \param[in] numbering_subset Vector required is the one described by this numbering subset.
         *
         */

        /*!
         * \class doxygen_hide_varf_vector_numbering_subset_and_return
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset
         *
         * \return Reference to the required vector.
         */


        /*!
         * \brief Access to the system rhs (R in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        const GlobalVector& GetSystemRhs(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Non constant access to the system rhs (R in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        GlobalVector& GetNonCstSystemRhs(const NumberingSubset& numbering_subset);

        /*!
         * \brief Access to the system solution (X in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        const GlobalVector& GetSystemSolution(const NumberingSubset& numbering_subset) const;

        /*!
         * \brief Non constant access to the system solution (X in A X = R).
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset_and_return
         */
        GlobalVector& GetNonCstSystemSolution(const NumberingSubset& numbering_subset);

        //! \copydoc doxygen_hide_time_manager_accessor
        const TimeManagerT& GetTimeManager() const;


      public:
        /*!
         * \brief Get the directory into which all outputs are written.
         *
         * It is assumed to already exist when this call is made.
         *
         * \return Directory into which all outputs are written.
         */
        const FilesystemNS::Directory& GetResultDirectory() const noexcept;

        /*!
         * \brief Returns the output directory into which solutions for \a numbering_subset are written.
         *
         * \param[in] numbering_subset \a NumberingSubset of the solution for which the output directory is requested.
         *
         * \return Path to the output directory into which solutions related to \a numbering_subset are written.
         */
        const FilesystemNS::Directory& GetOutputDirectory(const NumberingSubset& numbering_subset) const;


      protected:
        /*!
         * \brief Allocate the global matrix circonscribed by the two given numbering subsets.
         *
         * This method is merely a wrapper over MoReFEM::AllocateGlobalMatrix() specialized for the system matrices
         * within the \a VariationalFormulation.
         *
         * \copydetails doxygen_hide_varf_matrix_rowcol_numbering_subset
         *
         * This is done once and for all during the initialisation phase of MoReFEM; no others should be
         * allocated in the core of the calculation.
         *
         * The data have already been reduced to processor-wise when this operation is performed (or for that
         * matter when VariationalFormulation are expected to be built, i.e. typically in
         * the method \a SupplInitialize() to define in each model instance).
         *
         * The pattern of the matrix is expected to have been computed in the GodOfDof before the reduction of data.
         *
         */
        void AllocateSystemMatrix(const NumberingSubset& row_numbering_subset,
                                  const NumberingSubset& col_numbering_subset);


        /*!
         * \brief Allocate the global vector circonscribed by the given numbering subset.
         *
         * This method is merely a wrapper over MoReFEM::AllocateGlobalVector() specialized for the system vectors
         * within the \a VariationalFormulation.
         *
         * \copydetails doxygen_hide_varf_vector_numbering_subset
         *
         * This is done once and for all during the initialisation phase of MoReFEM; no others should be
         * allocated in the core of the calculation.
         *
         * The data have already been reduced to processor-wise when this operation is performed (or for that
         * matter when VariationalFormulation are expected to be built, i.e. typically in method SupplInitialize()
         * of the model).
         *
         * \internal <b><tt>[internal]</tt></b> Currently both rhs and solutions are allocated by this call.
         * \endinternal
         *
         */
        void AllocateSystemVector(const NumberingSubset& numbering_subset);

        /*!
         * \class doxygen_hide_init_vector_system_solution
         *
         *
         * \todo #1728 Couldn't this functionality be fulfilled with a source operator?
         *
         * \tparam InitialConditionIndexT Index used to specify which inittial condition to consider from the input data file.
         * \tparam InputDataT Type of the structure that holds the model
         * settings.
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         * \param[in] numbering_subset \a NumberingSubset related to the \a unknown in the \a felt_space.
         * \param[in] felt_space \a FEltSpace onto which the vector is to be  initialized. \param[in] unknown \a Unknown considered in the vector.
         *
         */

        /*!
         * \brief Initialize the vector system solution with an initial condition from the input file with respect to
         * the unknown considered.
         *
         * Boundary conditions related to \a numbering_subset are also applied by this method.
         *
         * \copydoc doxygen_hide_init_vector_system_solution
         */
        template<std::size_t InitialConditionIndexT, ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        void SetInitialSystemSolution(const MoReFEMDataT& morefem_data,
                                      const NumberingSubset& numbering_subset,
                                      const Unknown& unknown,
                                      const FEltSpace& felt_space);


        /*!
         * \copydoc doxygen_hide_init_vector_system_solution
         *
         * \brief Fill the content of \a vector with data from an \a InitialCondition object.
         *
         * \param[in,out] vector Vector which value must be initialized. This vector must already been properly
         * allocated, and itss associated numbering subset must match \a numbering_subset.
         *
         *
         */
        template<std::size_t InitialConditionIndexT, ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        void ApplyInitialConditionToVector(const MoReFEMDataT& morefem_data,
                                           const NumberingSubset& numbering_subset,
                                           const Unknown& unknown,
                                           const FEltSpace& felt_space,
                                           GlobalVector& vector);

      public:
        //! List of essential boundary condition to consider.
        const DirichletBoundaryCondition::vector_shared_ptr& GetEssentialBoundaryConditionList() const noexcept;


      public:
        //! Get god of dof.
        const GodOfDof& GetGodOfDof() const noexcept;

      protected:
        //! Access to the Snes underlying object.
        const Wrappers::Petsc::Snes& GetSnes() const noexcept;

        //! Non constant access to the Snes underlying object.
        Wrappers::Petsc::Snes& GetNonCstSnes() noexcept;

      private:
        /*!
         * \brief Access to the class in charge of storing the global matrices for each relevant
         * combination of numbering subsets.
         */
        const Internal::VarfNS::GlobalMatrixStorage& GetGlobalMatrixStorage() const;

        /*!
         * \brief Non constant access to the class in charge of storing the global matrices for each relevant
         * combination of numbering subsets.
         */
        Internal::VarfNS::GlobalMatrixStorage& GetNonCstGlobalMatrixStorage();

        /*!
         * \brief Access to the class in charge of storing the rhs vectors for each relevant numbering subset.
         */
        const Internal::VarfNS::GlobalVectorStorage& GetRhsVectorStorage() const;


        /*!
         * \brief Access to the class in charge of storing the rhs vectors for each relevant numbering subset.
         */
        Internal::VarfNS::GlobalVectorStorage& GetNonCstRhsVectorStorage();


        /*!
         * \brief Access to the class in charge of storing the solution vectors for each relevant numbering subset.
         */
        const Internal::VarfNS::GlobalVectorStorage& GetSolutionVectorStorage() const;


        /*!
         * \brief Access to the class in charge of storing the solution vectors for each relevant numbering subset.
         */
        Internal::VarfNS::GlobalVectorStorage& GetNonCstSolutionVectorStorage();

      protected:
        //! Files will be written every \a GetDisplayValue() time iterations.
        std::size_t GetDisplayValue() const;

      protected:
        //! \copydoc doxygen_hide_time_manager_accessor
        TimeManagerT& GetNonCstTimeManager();

        //! Get the \a NumberingSubset for the rows, used in \a SolveNonLinear() call.
        template<enable_non_linear_solver NonLinT = NonLinearSolverT>
        std::enable_if_t<NonLinT == enable_non_linear_solver::yes, const NumberingSubset&>
        GetRowNumberingSubset() const noexcept;

        //! Get the \a NumberingSubset for the columns, used in \a SolveNonLinear() call.
        template<enable_non_linear_solver NonLinT = NonLinearSolverT>
        std::enable_if_t<NonLinT == enable_non_linear_solver::yes, const NumberingSubset&>
        GetColumnNumberingSubset() const noexcept;

      private:
        /*!
         * \brief Set both \a NumberingSubset for the non linear system to solve.
         *
         * \param[in] row The \a NumberingSubset used to index the rows of the system matrix.
         * \param[in] col The \a NumberingSubset used to index the columns of the system matrix.
         */
        template<enable_non_linear_solver NonLinT = NonLinearSolverT>
        std::enable_if_t<NonLinT == enable_non_linear_solver::yes, void>
        SetNonLinearNumberingSubset(const NumberingSubset* row, const NumberingSubset* col);

      private:
        /*!
         * \brief An internal function called  in SnesInterface<VariationalFormulationT>::Viewer().
         *
         * This \a Viewer function is what is provided to
         * [SNESMonitorSet]( https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESMonitorSet.html).
         *
         * Current function describes the required text to be printed at each Newton iteration. It is virtual as a model
         develop may want to substitute its own
         * text to the one provided here by default; if so he must in its \a VariationalFormulation class which inherits
         from this one declare:
         * \code
         virtual void SnesMonitorFunction(PetscInt iteration_index, PetscReal norm) const override;
         \endcode
         *
         * \param[in] iteration_index Index of the Newton iteration; this is the quantity provided directly by PETSc
         (hence the type).
         * \param[in] norm Norm of the current residual; this is the quantity provided directly by PETSc (hence the
         type).
         *
         * \internal As this function is virtual I can't restrict it to the case on linear solver is enabled at compile
         time; however an assert and/or an exit
         * is emitted at runtime if it is nonetheless used.
         */
        virtual void SnesMonitorFunction(PetscInt iteration_index, PetscReal norm) const;


      private:
        /// \name Global matrices and vectors.


        ///@{

        /*!
         * \brief List of system matrices.
         *
         */
        Internal::VarfNS::GlobalMatrixStorage global_matrix_storage_;

        //! System rhs.
        Internal::VarfNS::GlobalVectorStorage global_rhs_storage_;

        //! System solution.
        Internal::VarfNS::GlobalVectorStorage global_solution_storage_;


        ///@}

        //! Directory into which all outputs are written.
        const FilesystemNS::Directory& result_directory_;

        //! Transient parameters.
        TimeManagerT& time_manager_;

        /*!
         * \brief Wrapper over Petsc solver utilities.
         *
         * \internal <b><tt>[internal]</tt></b> For conveniency same class is used for all cases (including mere KSP
         * solve). \endinternal
         */
        Wrappers::Petsc::Snes::unique_ptr snes_ = nullptr;

        //! God of dof.
        const GodOfDof& god_of_dof_;

        //! List of essential boundary condition to consider.
        const DirichletBoundaryCondition::vector_shared_ptr boundary_condition_list_;

        //! Files will be written every \a display_value_ time iterations.
        std::size_t display_value_;

        //! The \a NumberingSubset for the rows, used in \a SolveNonLinear() call. Not used if non linear solver is not
        //! enabled.
        const NumberingSubset* row_numbering_subset_ = nullptr;

        //! The \a NumberingSubset for the columns, used in \a SolveNonLinear() call.  Not used if non linear solver is
        //! not enabled.
        const NumberingSubset* column_numbering_subset_ = nullptr;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FormulationSolver/VariationalFormulation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
