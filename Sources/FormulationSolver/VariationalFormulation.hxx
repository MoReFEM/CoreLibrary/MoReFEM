// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Result.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "FormulationSolver/Advanced/Restart.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::VariationalFormulation(
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        MoReFEMDataT& morefem_data)
    : Crtp::CrtpMpi<VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>>(
          morefem_data.GetMpi()),
      result_directory_(morefem_data.GetResultDirectory()), time_manager_(morefem_data.GetNonCstTimeManager()),
      snes_(nullptr), god_of_dof_(god_of_dof), boundary_condition_list_(std::move(boundary_condition_list))
    { }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GodOfDof&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetGodOfDof() const noexcept
    {
        return god_of_dof_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::Init(
        const MoReFEMDataT& morefem_data)
    {
        auto& derived = static_cast<DerivedT&>(*this);

        if constexpr (NonLinearSolverT == enable_non_linear_solver::yes)
        {
            snes_ = Internal::SolverNS::InitSolver<SolverIndexT>(morefem_data,
                                                                 Internal::SolverNS::SnesInterface<DerivedT>::Function,
                                                                 Internal::SolverNS::SnesInterface<DerivedT>::Jacobian,
                                                                 Internal::SolverNS::SnesInterface<DerivedT>::Viewer,
                                                                 derived.ImplementSnesConvergenceTestFunction());
        } else
        {
            snes_ = Internal::SolverNS::InitSolver<SolverIndexT>(morefem_data);
        }

        display_value_ = ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Result::DisplayValue>(morefem_data);
        derived.AllocateMatricesAndVectors();

        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);
        derived.SupplInit(morefem_data);
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<IsFactorized IsFactorizedT>
    inline void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::SolveLinear(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset,
        Wrappers::Petsc::print_solver_infos do_print_solver_infos,
        const std::source_location location)
    {
        return SolveLinear<IsFactorizedT>(GetSystemMatrix(row_numbering_subset, col_numbering_subset),
                                          GetSystemRhs(col_numbering_subset),
                                          GetNonCstSystemSolution(col_numbering_subset),
                                          do_print_solver_infos,
                                          location);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<IsFactorized IsFactorizedT>
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::SolveLinear(
        const GlobalMatrix& matrix,
        const GlobalVector& rhs,
        GlobalVector& out,
        Wrappers::Petsc::print_solver_infos do_print_solver_infos_arg,
        const std::source_location location)
    {
        assert(out.GetNumberingSubset() == rhs.GetNumberingSubset());

        auto do_print_solver_info = (this->GetTimeManager().NtimeModified() % GetDisplayValue()) == 0
                                        ? Wrappers::Petsc::print_solver_infos::yes
                                        : Wrappers::Petsc::print_solver_infos::no;

        if (do_print_solver_infos_arg == Wrappers::Petsc::print_solver_infos::no)
        {
            do_print_solver_info = Wrappers::Petsc::print_solver_infos::no;
        }

        switch (IsFactorizedT)
        {
        case IsFactorized::no:
        {
            GetNonCstSnes().SolveLinear(matrix, matrix, rhs, out, do_print_solver_info, location);
            break;
        }
        case IsFactorized::yes:
        {
            GetNonCstSnes().SolveLinear(rhs, out, do_print_solver_info, location);

            break;
        }
        }

        out.UpdateGhosts();
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, void>
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::SolveNonLinear(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset,
        Wrappers::Petsc::check_convergence do_check_convergence,
        SNESLineSearchType line_search_type,
        const std::source_location location)
    {
        const auto& system_matrix = this->GetSystemMatrix(row_numbering_subset, col_numbering_subset);
        const auto& system_rhs = this->GetSystemRhs(col_numbering_subset);
        auto& system_solution = this->GetNonCstSystemSolution(col_numbering_subset);

        auto& snes = GetNonCstSnes();

        this->SetNonLinearNumberingSubset(&row_numbering_subset, &col_numbering_subset);

        snes.SetLineSearchType(line_search_type);

        snes.SolveNonLinear(static_cast<void*>(this),
                            system_rhs,
                            system_matrix,
                            system_matrix,
                            system_solution,
                            do_check_convergence,
                            location);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<VariationalFormulationNS::On AppliedOnT, BoundaryConditionMethod BoundaryConditionMethodT>
    void
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::ApplyEssentialBoundaryCondition(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset)
    {
        if (GetEssentialBoundaryConditionList().empty())
            return;

        auto& system_matrix = GetNonCstSystemMatrix(row_numbering_subset, col_numbering_subset);
        auto& system_rhs = GetNonCstSystemRhs(col_numbering_subset);

        const auto& god_of_dof = GetGodOfDof();

        const auto& dirichlet_bc_list = GetEssentialBoundaryConditionList();

        for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
        {
            assert(!(!dirichlet_bc_ptr));

            const auto& dirichlet_bc = *dirichlet_bc_ptr;

            if (!dirichlet_bc.IsNumberingSubset(row_numbering_subset))
                continue;

            switch (AppliedOnT)
            {
            case VariationalFormulationNS::On::system_matrix:
            case VariationalFormulationNS::On::system_matrix_and_rhs:
                god_of_dof.template ApplyBoundaryCondition<BoundaryConditionMethodT>(dirichlet_bc, system_matrix);
                break;
            case VariationalFormulationNS::On::system_rhs:
                break;
            }


            switch (AppliedOnT)
            {
            case VariationalFormulationNS::On::system_rhs:
            case VariationalFormulationNS::On::system_matrix_and_rhs:
                god_of_dof.template ApplyBoundaryCondition<BoundaryConditionMethodT>(dirichlet_bc, system_rhs);
                break;
            case VariationalFormulationNS::On::system_matrix:
                break;
            }
        }

        // Call Petsc Assembly() properly on modified linear algebra.
        switch (AppliedOnT)
        {
        case VariationalFormulationNS::On::system_matrix:
        case VariationalFormulationNS::On::system_matrix_and_rhs:
            system_matrix.Assembly();
            break;
        case VariationalFormulationNS::On::system_rhs:
            break;
        }


        switch (AppliedOnT)
        {
        case VariationalFormulationNS::On::system_rhs:
        case VariationalFormulationNS::On::system_matrix_and_rhs:
            system_rhs.Assembly();
            break;
        case VariationalFormulationNS::On::system_matrix:
            break;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<class LinearAlgebraT, BoundaryConditionMethod BoundaryConditionMethodT>
    void
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::ApplyEssentialBoundaryCondition(
        LinearAlgebraT& linear_algebra)
    {
        static_assert(std::is_same<LinearAlgebraT, GlobalMatrix>() || std::is_same<LinearAlgebraT, GlobalVector>(),
                      "Can't be another type (as GodOfDof::ApplyBoundaryCondition() should have an overload for "
                      "LinearAlgebraT.");

        if (GetEssentialBoundaryConditionList().empty())
            return;

        const auto& god_of_dof = GetGodOfDof();

        const auto& dirichlet_bc_list = GetEssentialBoundaryConditionList();

        // Trick to mimic a static ternary operator.
        auto fetch_relevant_numbering_subset = [&linear_algebra]() -> const NumberingSubset&
        {
            if constexpr (std::is_same<LinearAlgebraT, GlobalMatrix>())
                return linear_algebra.GetRowNumberingSubset();
            else
                return linear_algebra.GetNumberingSubset();
        };

        const auto& numbering_subset = fetch_relevant_numbering_subset();

        for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
        {
            assert(!(!dirichlet_bc_ptr));

            const auto& dirichlet_bc = *dirichlet_bc_ptr;

            if (!dirichlet_bc.IsNumberingSubset(numbering_subset))
                continue;

            god_of_dof.template ApplyBoundaryCondition<BoundaryConditionMethodT>(dirichlet_bc, linear_algebra);
        }

        // Call Petsc Assembly() properly on modified linear algebra.
        linear_algebra.Assembly();
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::WriteSolution(
        const TimeManagerT& time_manager,
        const NumberingSubset& numbering_subset) const
    {
        FilesystemNS::File output_file;

        decltype(auto) mpi = this->GetMpi();
        decltype(auto) output_directory = GetOutputDirectory(numbering_subset);
        std::ostringstream oconv;

        oconv
            << "/solution_time_" << std::setfill('0') << std::setw(5)
            << time_manager.NtimeModified(); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

        std::string only_filename = oconv.str();
        oconv.str("");

        oconv << output_directory << only_filename << Utilities::AsciiOrBinary::GetInstance().DatafileExtension();
        output_file = FilesystemNS::File{ oconv.str() };

        GetSystemSolution(numbering_subset).template Print<MpiScale::processor_wise>(mpi, output_file);

        decltype(auto) file = GetTimeManager().GetTimeIterationFile();

        std::ofstream inout{ file.Append() };

        oconv.str("");
        oconv << GetGodOfDof()
                     .template GetOutputDirectoryForNumberingSubset<Internal::GodOfDofNS::wildcard_for_rank::yes>(
                         numbering_subset)
              << only_filename << Utilities::AsciiOrBinary::GetInstance().DatafileExtension();

        inout << time_manager.NtimeModified() << ';' << time_manager.GetTime() << ';' << numbering_subset.GetUniqueId()
              << ';' << oconv.str() << std::endl;
    }


    //////////////////////////
    // PROTECTED ACCESSORS  //
    //////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GlobalMatrix&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetSystemMatrix(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset) const
    {
        return GetGlobalMatrixStorage().GetMatrix(row_numbering_subset, col_numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline GlobalMatrix&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstSystemMatrix(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset)
    {
        return GetNonCstGlobalMatrixStorage().GetNonCstMatrix(row_numbering_subset, col_numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GlobalVector&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetSystemRhs(
        const NumberingSubset& numbering_subset) const
    {
        return GetRhsVectorStorage().GetVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline GlobalVector&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstSystemRhs(
        const NumberingSubset& numbering_subset)
    {
        return GetNonCstRhsVectorStorage().GetNonCstVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const GlobalVector&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetSystemSolution(
        const NumberingSubset& numbering_subset) const
    {
        return GetSolutionVectorStorage().GetVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline GlobalVector&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstSystemSolution(
        const NumberingSubset& numbering_subset)
    {
        return GetNonCstSolutionVectorStorage().GetNonCstVector(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline const TimeManagerT&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetTimeManager() const
    {
        return time_manager_;
    }


    //////////////////////////
    // PRIVATE METHODS      //
    //////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::AllocateSystemMatrix(
        const NumberingSubset& row_numbering_subset,
        const NumberingSubset& col_numbering_subset)
    {
        auto& global_matrix_storage = GetNonCstGlobalMatrixStorage();

        // #1617 Use std::format when available!
        std::string name("SystemMatrix_ns_");
        name += std::to_string(row_numbering_subset.GetUniqueId().Get()) + std::string("_")
                + std::to_string(col_numbering_subset.GetUniqueId().Get());

        auto& matrix = global_matrix_storage.NewMatrix(row_numbering_subset, col_numbering_subset, std::move(name));

        AllocateGlobalMatrix(this->GetGodOfDof(), matrix);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::AllocateSystemVector(
        const NumberingSubset& numbering_subset)
    {
        // #1617 Use std::format when available!
        std::string ns_suffix("_ns_");
        ns_suffix += std::to_string(numbering_subset.GetUniqueId().Get());

        {
            auto& rhs_storage = GetNonCstRhsVectorStorage();
            rhs_storage.NewVector(this->GetGodOfDof(), numbering_subset, std::string("SystemRhs") + ns_suffix);
        }

        {
            auto& solution_storage = GetNonCstSolutionVectorStorage();
            solution_storage.NewVector(
                this->GetGodOfDof(), numbering_subset, std::string("SystemSolution") + ns_suffix);
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<std::size_t IndexT, ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::SetInitialSystemSolution(
        const MoReFEMDataT& morefem_data,
        const NumberingSubset& numbering_subset,
        const Unknown& unknown,
        const FEltSpace& felt_space)
    {
        const auto& god_of_dof = this->GetGodOfDof();

        GlobalVector& system_solution = VariationalFormulation::GetNonCstSystemSolution(numbering_subset);

        ApplyInitialConditionToVector<IndexT>(morefem_data, numbering_subset, unknown, felt_space, system_solution);

        const auto& dirichlet_bc_list = this->GetEssentialBoundaryConditionList();

        for (const auto& dirichlet_bc_ptr : dirichlet_bc_list)
        {
            assert(!(!dirichlet_bc_ptr));
            const auto& dirichlet_bc = *dirichlet_bc_ptr;

            if (!dirichlet_bc.IsNumberingSubset(numbering_subset))
                continue;

            god_of_dof.ApplyPseudoElimination(dirichlet_bc, system_solution);
        }

        system_solution.Assembly();
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<std::size_t IndexT, ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::ApplyInitialConditionToVector(
        const MoReFEMDataT& morefem_data,
        const NumberingSubset& numbering_subset,
        const Unknown& unknown,
        const FEltSpace& felt_space,
        GlobalVector& vector)
    {
        const auto& god_of_dof = this->GetGodOfDof();
        const auto& mesh = god_of_dof.GetMesh();

        using initial_condition_type = InputDataNS::InitialCondition<IndexT>;

        auto initial_condition_ptr =
            Internal::FormulationSolverNS::InitThreeDimensionalInitialCondition<initial_condition_type>(mesh,
                                                                                                        morefem_data);

        // Nullptr sign the case initial condition should be ignored.
        if (!initial_condition_ptr)
            return;

        const auto& initial_condition = *initial_condition_ptr;

        const auto& extended_unknown = felt_space.GetExtendedUnknown(unknown);
        const auto& extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> local_array(vector);

        const auto& felt_list_per_type =
            felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        const auto Nprocessor_wise_dof = static_cast<PetscInt>(god_of_dof.NprocessorWiseDof(numbering_subset));

        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension = felt_space.GetMeshDimension();

        const auto Ncomponent_local = Ncomponent(unknown, mesh_dimension);

        SpatialPoint node_coords;

        for (const auto& pair : felt_list_per_type)
        {
            const auto& ref_felt_space_list_ptr = pair.first;

            const auto& local_felt_space_list = pair.second;

            const auto& ref_node_list =
                ref_felt_space_list_ptr->GetRefFElt(extended_unknown).GetBasicRefFElt().GetLocalNodeList();

            for (const auto& local_felt_space_pair : local_felt_space_list)
            {
                const auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));

                auto& local_felt_space = *local_felt_space_ptr;

                ExtendedUnknown::vector_const_shared_ptr buf{ extended_unknown_ptr };

                local_felt_space.ComputeLocal2Global(buf, DoComputeProcessorWiseLocal2Global::yes);

                const auto& local_2_global =
                    local_felt_space.GetLocal2Global<MoReFEM::MpiScale::processor_wise>(extended_unknown);

                const auto Nlocal_node = ref_node_list.size();

                for (std::size_t i_node = 0; i_node < Nlocal_node; ++i_node)
                {
                    const auto& geometric_element = local_felt_space_ptr->GetGeometricElt();

                    const auto& local_coords = ref_node_list[i_node]->GetLocalCoords();

                    Advanced::GeomEltNS::Local2Global(geometric_element, local_coords, node_coords);

                    const auto& initial_condition_value = initial_condition.GetValue(node_coords);

                    std::size_t dof_index = i_node;

                    assert(initial_condition_value.size() >= Ncomponent_local);

                    if (local_2_global[i_node] < Nprocessor_wise_dof)
                    {
                        for (auto component = Eigen::Index{}; component < Ncomponent_local;
                             ++component, dof_index += Nlocal_node)
                        {
                            const auto dof_index_int = static_cast<PetscInt>(local_2_global[dof_index]);
                            local_array[vector_processor_wise_index_type{ dof_index_int }] =
                                initial_condition_value(component);
                        }
                    }
                }
            }
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Wrappers::Petsc::Snes&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetSnes() const noexcept
    {
        assert(!(!snes_));
        return *snes_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Wrappers::Petsc::Snes&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstSnes() noexcept
    {
        return const_cast<Wrappers::Petsc::Snes&>(GetSnes());
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const FilesystemNS::Directory&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetOutputDirectory(
        const NumberingSubset& numbering_subset) const
    {
        return GetGodOfDof().GetOutputDirectoryForNumberingSubset(numbering_subset);
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const FilesystemNS::Directory&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetResultDirectory() const noexcept
    {
        return result_directory_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Internal::VarfNS::GlobalMatrixStorage&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstGlobalMatrixStorage()
    {
        return global_matrix_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Internal::VarfNS::GlobalMatrixStorage&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetGlobalMatrixStorage() const
    {
        return global_matrix_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetRhsVectorStorage() const
    {
        return global_rhs_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstRhsVectorStorage()
    {
        return global_rhs_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetSolutionVectorStorage() const
    {
        return global_solution_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    Internal::VarfNS::GlobalVectorStorage&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstSolutionVectorStorage()
    {
        return global_solution_storage_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    const DirichletBoundaryCondition::vector_shared_ptr&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetEssentialBoundaryConditionList()
        const noexcept
    {
        return boundary_condition_list_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    std::size_t VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetDisplayValue() const
    {
        return display_value_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    inline TimeManagerT&
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetNonCstTimeManager()
    {
        return time_manager_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    void VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::SnesMonitorFunction(
        [[maybe_unused]] PetscInt iteration_index,
        [[maybe_unused]] PetscReal norm) const
    {
        if constexpr (NonLinearSolverT == enable_non_linear_solver::yes)
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\tAt Newton iteration %d norm is %14.12e\n",
                                                          this->GetMpi(),
                                                          std::source_location::current(),
                                                          iteration_index,
                                                          norm);
        } else
        {
            // I would have liked to use enable_if on this method, but as I also want it virtual it wasn't possible
            // hence this crude exit here.
            assert(false && "Should not be called if non linear solver is not enabled.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, const NumberingSubset&>
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetRowNumberingSubset()
        const noexcept
    {
        assert(!(!row_numbering_subset_));
        return *row_numbering_subset_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, const NumberingSubset&>
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::GetColumnNumberingSubset()
        const noexcept
    {
        assert(!(!column_numbering_subset_));
        return *column_numbering_subset_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        std::size_t SolverIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        enable_non_linear_solver NonLinearSolverT
    >
    // clang-format on
    template<enable_non_linear_solver NonLinT>
    std::enable_if_t<NonLinT == enable_non_linear_solver::yes, void>
    VariationalFormulation<DerivedT, SolverIndexT, TimeManagerT, NonLinearSolverT>::SetNonLinearNumberingSubset(
        const NumberingSubset* row,
        const NumberingSubset* col)
    {
        row_numbering_subset_ = row;
        column_numbering_subset_ = col;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
