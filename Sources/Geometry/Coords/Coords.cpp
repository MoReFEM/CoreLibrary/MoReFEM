// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <istream>
#include <ostream>
#include <set>
#include <sstream>
#include <type_traits> // IWYU pragma: keep
// IWYU pragma: no_include <__tree>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Geometry/Coords/Coords.hpp" // IWYU pragma: associated
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    Coords::~Coords()
    {
#ifndef NDEBUG
        GetNonCstNobjects()--;
#endif // NDEBUG
    }


    Coords::Coords()
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    Coords::Coords(double x, double y, double z, const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    : parent(x, y, z, space_unit)
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    Coords::Coords(GeometryNS::dimension_type Ncoor,
                   std::istream& stream,
                   const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    : parent(Ncoor, stream, space_unit)
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    void Coords::ExtendedPrint(std::ostream& stream) const
    {
        parent::Print(stream);
        stream << " [" << GetProgramWisePosition() << ']';
    }


    void WriteVTK_PolygonalDataFormat(const Coords& point, std::ostream& stream)
    {
        stream << point.x() << ' ';
        stream << point.y() << ' ';
        stream << point.z() << ' ';
        stream << '\n';
    }


    bool Coords::IsInDomain(DomainNS::unique_id domain_unique_id) const
    {
        assert(GetNonCstCreateDomainListForCoords() == create_domain_list_for_coords::yes
               && "GetNonCstCreateDomainListForCoords() should be set to yes. You can do it at the creation of "
                  "your model. See the TestDomainListInCoords model for example.");

        decltype(auto) domain_list = GetDomainList();
        auto begin = domain_list.cbegin();
        auto end = domain_list.cend();

        auto it = std::find(begin, end, domain_unique_id);

        return it != end;
    }


    bool Coords::IsInDomain(const Domain& domain) const
    {
        const auto domain_id = domain.GetUniqueId();
        return IsInDomain(domain_id);
    }


    create_domain_list_for_coords& Coords::GetNonCstCreateDomainListForCoords()
    {
        // Static is initialized only during first call.
        static create_domain_list_for_coords create_domain_list_for_coords_ = create_domain_list_for_coords::no;
        return create_domain_list_for_coords_;
    }


    create_domain_list_for_coords Coords::GetCreateDomainListForCoords()
    {
        return GetNonCstCreateDomainListForCoords();
    }


    void Coords::SetCreateDomainListForCoords()
    {
        GetNonCstCreateDomainListForCoords() = create_domain_list_for_coords::yes;
    }


#ifndef NDEBUG
    std::size_t& Coords::GetNonCstNobjects()
    {
        static std::size_t ret = 0UL;
        return ret;
    }


    std::size_t Coords::Nobjects()
    {
        return GetNonCstNobjects();
    }


#endif // NDEBUG


    void Coords::SetIsLowestProcessor(bool value)
    {
        is_lowest_processor_ = value;
    }


    bool Coords::IsLowestProcessorRank() const noexcept
    {
        return is_lowest_processor_;
    }


    std::ostream& operator<<(std::ostream& stream, const Coords& rhs)
    {
        rhs.Print(stream);
        return stream;
    }


#ifndef NDEBUG
    void AssertNCoordsConsistency(const Wrappers::Mpi& mpi)
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();

        decltype(auto) mesh_storage = mesh_manager.GetStorage();

        auto Ncoords_in_meshes = 0UL;

        for ([[maybe_unused]] const auto& [mesh_id, mesh_ptr] : mesh_storage)
        {
            assert(!(!mesh_ptr));
            const auto& mesh = *mesh_ptr;

            Ncoords_in_meshes += mesh.NprocessorWiseCoord();
            Ncoords_in_meshes += mesh.NghostCoord();
        }


        if (Ncoords_in_meshes != Coords::Nobjects())
        {
            std::ostringstream oconv;
            oconv << "Inconsistent number of Coords on processor " << mpi.GetRank<int>()
                  << ": all meshes "
                     "encompass "
                  << Ncoords_in_meshes << " whereas there are " << Coords::Nobjects()
                  << " Coords "
                     "object currently alive on the processor.";

            throw Exception(oconv.str());
        }
    }

#endif // NDEBUG


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
