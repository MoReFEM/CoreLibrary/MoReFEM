// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_COORDS_DOT_HXX_
#define MOREFEM_GEOMETRY_COORDS_COORDS_DOT_HXX_
// IWYU pragma: private, include "Geometry/Coords/Coords.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iomanip>
#include <memory>
#include <ostream>
#include <set>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/StrongType.hpp"


namespace MoReFEM
{


    template<class T>
    Coords::Coords(T&& array, const ::MoReFEM::CoordsNS::space_unit_type space_unit) : parent(array, space_unit)
    {
#ifndef NDEBUG
        GetNonCstNobjects()++;
#endif // NDEBUG
    }


    inline CoordsNS::index_from_mesh_file Coords::GetIndexFromMeshFile() const noexcept
    {
        return GetCoordIndexes().GetFileIndex();
    }


    inline void Coords::SetIndexFromMeshFile(::MoReFEM::CoordsNS::index_from_mesh_file index,
                                             bool do_allow_second_call) noexcept
    {
        GetNonCstCoordIndexes().SetFileIndex(index, do_allow_second_call);
    }


    inline void Coords::SetMeshLabel(const MeshLabel::const_shared_ptr& mesh_label)
    {
        mesh_label_ = mesh_label;
    }


    inline MeshLabel::const_shared_ptr Coords::GetMeshLabelPtr() const
    {
        return mesh_label_;
    }


    inline bool operator<(const Coords& lhs, const Coords& rhs)
    {
        return lhs.GetProcessorWisePosition() < rhs.GetProcessorWisePosition();
    }


    inline bool operator==(const Coords& lhs, const Coords& rhs)
    {
#ifndef NDEBUG
        if (lhs.GetProcessorWisePosition() == rhs.GetProcessorWisePosition())
        {
            assert("Coordinates should also be the same, even if for efficiency reason I stick to index comparison!"
                   && Distance(lhs, rhs) < 1.e-12);
        }
#endif // NDEBUG

        return lhs.GetProcessorWisePosition() == rhs.GetProcessorWisePosition();
    }


    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    template<bool DoPrintIndexT>
    void WriteEnsightFormat(const Coords& point, std::ostream& stream)
    {
        if (DoPrintIndexT)
        {

            stream << std::setw(8) << point.GetIndexFromMeshFile();
        }

        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.x();
        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.y();
        stream << std::setw(12) << std::scientific << std::setprecision(5) << point.z();
        stream << std::endl;
    }
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

    template<class FloatT>
    void WriteMeditFormat(const ::MoReFEM::GeometryNS::dimension_type dimension,
                          const Coords& point,
                          libmeshb_int lm_mesh_index)
    {

        int label_index(0);

        auto label_ptr = point.GetMeshLabelPtr();
        if (label_ptr)
            label_index = static_cast<int>(label_ptr->GetIndex().Get());

        switch (dimension.Get())
        {
        case 1:
            GmfSetLin(lm_mesh_index, GmfVertices, point.x(), 0., label_index);
            break;
        case 2:
            GmfSetLin(lm_mesh_index, GmfVertices, point.x(), point.y(), label_index);
            break;
        case 3:
            GmfSetLin(lm_mesh_index, GmfVertices, point.x(), point.y(), point.z(), label_index);
            break;
        default:
            assert(false && "Dimension should be 1, 2, or 3!");
            exit(EXIT_FAILURE);
            break;
        }
    }


    inline const Internal::CoordsNS::CoordIndexes& Coords::GetCoordIndexes() const noexcept
    {
        return coord_indexes_;
    }


    inline Internal::CoordsNS::CoordIndexes& Coords::GetNonCstCoordIndexes() noexcept
    {
        return const_cast<Internal::CoordsNS::CoordIndexes&>(GetCoordIndexes());
    }


    inline CoordsNS::program_wise_position Coords::GetProgramWisePosition() const noexcept
    {
        return GetCoordIndexes().GetProgramWisePosition();
    }


    inline CoordsNS::processor_wise_position Coords::GetProcessorWisePosition() const noexcept
    {
        return GetCoordIndexes().GetProcessorWisePosition();
    }


    template<Internal::CoordsNS::DoCheckFirstCall DoCheckFirstCallT>
    void Coords::SetProgramWisePosition(CoordsNS::program_wise_position position) noexcept
    {
        GetNonCstCoordIndexes().SetProgramWisePosition<DoCheckFirstCallT>(position);
    }


    template<Internal::CoordsNS::DoCheckFirstCall DoCheckFirstCallT>
    void Coords::SetProcessorWisePosition(CoordsNS::processor_wise_position position) noexcept
    {
        GetNonCstCoordIndexes().SetProcessorWisePosition<DoCheckFirstCallT>(position);
    }


    inline void Coords::AddDomainContainingCoords(DomainNS::unique_id domain_unique_id)
    {
        assert(GetNonCstCreateDomainListForCoords() == create_domain_list_for_coords::yes
               && "GetNonCstCreateDomainListForCoords() should be set to yes. You can do it at the creation of your "
                  "model. See the TestDomainListInCoords model for example.");
        domain_list_.insert(domain_unique_id);
    }


    template<CoordsNS::index_enum TypeT>
    CoordsNS::index_type<TypeT> Coords::GetIndex() const noexcept
    {
        if constexpr (TypeT == CoordsNS::index_enum::program_wise_position)
            return GetProgramWisePosition();
        else if constexpr (TypeT == CoordsNS::index_enum::processor_wise_position)
            return GetProcessorWisePosition();
        else if constexpr (TypeT == CoordsNS::index_enum::from_mesh_file)
            return GetIndexFromMeshFile();
        else
        {
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    inline const std::set<DomainNS::unique_id>& Coords::GetDomainList() const noexcept
    {
        return domain_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_COORDS_DOT_HXX_
// *** MoReFEM end header guards *** < //
