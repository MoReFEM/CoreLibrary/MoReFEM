// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <optional>

#include "Geometry/Coords/Internal/CoordIndexes.hpp"

#include "Geometry/Coords/StrongType.hpp"


namespace MoReFEM::Internal::CoordsNS
{


    void CoordIndexes::SetFileIndex(::MoReFEM::CoordsNS::index_from_mesh_file value,
                                    [[maybe_unused]] bool do_allow_second_call) noexcept
    {
#ifndef NDEBUG
        {
            if (!do_allow_second_call)
                assert(!file_index_.has_value() && "Should be assigned only once!");
        }
#endif // NDEBUG

        file_index_ = value;
    }


} // namespace MoReFEM::Internal::CoordsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
