// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <istream>

#include "Geometry/Coords/Internal/Factory.hpp"

#include "Utilities/SmartPointers/Internal/Wrap.hpp"


namespace MoReFEM::Internal::CoordsNS
{


    Coords::shared_ptr Factory::Origin()
    {
        return Internal::WrapShared(new Coords);
    }


    Coords::shared_ptr
    Factory::FromComponents(double x, double y, double z, const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    {
        return Internal::WrapShared(new Coords(x, y, z, space_unit));
    }


    Coords::shared_ptr Factory::FromStream(::MoReFEM::GeometryNS::dimension_type Ncoor,
                                           std::istream& stream,
                                           const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    {
        return Internal::WrapShared(new Coords(Ncoor, stream, space_unit));
    }


} // namespace MoReFEM::Internal::CoordsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
