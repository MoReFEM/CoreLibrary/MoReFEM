// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_INTERNAL_FACTORY_DOT_HPP_
#define MOREFEM_GEOMETRY_COORDS_INTERNAL_FACTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Geometry/Coords/Coords.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::CoordsNS
{


    /*!
     * \brief A factory class to create new \a Coords objects.
     *
     * This factory, which is a friend of \a Coords, is there merely to make \a Coords constructors private
     * and therefore make it clear such objects should not be created by a model user (\a SpatialPoint are
     * an entirely different story and are probably what a model writer might be seeking).
     */
    struct Factory
    {


        /*!
         * \brief Create a \a Coords objects with its three components set to 0.
         *
         * \return Pointer to newly created \a Coords.
         */
        static Coords::shared_ptr Origin();

        /*!
         * \brief Create a \a Coords objects from its three components.
         *
         * \copydoc doxygen_hide_coords_from_components_params
         *
         * \return Pointer to newly created \a Coords.
         */
        static Coords::shared_ptr
        FromComponents(double x, double y, double z, const ::MoReFEM::CoordsNS::space_unit_type space_unit);

        /*!
         * \brief Create a \a Coords objects from its three components.
         *
         * \copydoc doxygen_hide_coords_from_stream_params
         *
         * \return Pointer to newly created \a Coords.
         */
        static Coords::shared_ptr FromStream(::MoReFEM::GeometryNS::dimension_type Ncoor,
                                             std::istream& stream,
                                             const ::MoReFEM::CoordsNS::space_unit_type space_unit);

        /*!
         * \brief Create a \a Coords objects from a 3D array.
         *
         * \copydoc doxygen_hide_coords_from_array_params
         *
         * \return Pointer to newly created \a Coords.
         */
        template<typename T>
        static Coords::shared_ptr FromArray(T&& value, const ::MoReFEM::CoordsNS::space_unit_type space_unit);
    };


} // namespace MoReFEM::Internal::CoordsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Coords/Internal/Factory.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_INTERNAL_FACTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
