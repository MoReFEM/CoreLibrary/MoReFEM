// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_INTERNAL_FACTORY_DOT_HXX_
#define MOREFEM_GEOMETRY_COORDS_INTERNAL_FACTORY_DOT_HXX_
// IWYU pragma: private, include "Geometry/Coords/Internal/Factory.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM::Internal::CoordsNS
{


    template<typename T>
    Coords::shared_ptr Factory::FromArray(T&& value, const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    {
        return Internal::WrapShared(new Coords(value, space_unit));
    }


} // namespace MoReFEM::Internal::CoordsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_INTERNAL_FACTORY_DOT_HXX_
// *** MoReFEM end header guards *** < //
