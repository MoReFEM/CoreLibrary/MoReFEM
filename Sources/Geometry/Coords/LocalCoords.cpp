// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <initializer_list>
#include <ostream>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Geometry/Coords/LocalCoords.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/StrongType.hpp"


namespace MoReFEM
{


    LocalCoords::~LocalCoords() = default;


    LocalCoords::LocalCoords(std::initializer_list<double>&& coor) : coordinate_list_{ coor }
    { }


    void LocalCoords::Print(std::ostream& stream) const
    {
        Utilities::PrintContainer<>::Do(coordinate_list_.reshaped(),
                                        stream,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("["),
                                        PrintNS::Delimiter::closer("]"));
    }


    std::vector<::MoReFEM::GeometryNS::dimension_type> ExtractMismatchedComponentIndexes(const LocalCoords& lhs,
                                                                                         const LocalCoords& rhs)
    {
        const auto& component_coordinates1 = lhs.GetCoordinates();
        const auto& component_coordinates2 = rhs.GetCoordinates();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ component_coordinates1.size() };
        assert(Ncomponent.Get() == component_coordinates2.size());

        std::vector<::MoReFEM::GeometryNS::dimension_type> ret;

        for (::MoReFEM::GeometryNS::dimension_type i{ 0 }; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(component_coordinates1(i.Get()), component_coordinates2(i.Get())))
                ret.push_back(i);
        }

        return ret;
    }


    ::MoReFEM::GeometryNS::dimension_type ExtractMismatchedComponentIndex(const LocalCoords& lhs,
                                                                          const LocalCoords& rhs)
    {
        const auto& component_coordinates1 = lhs.GetCoordinates();
        const auto& component_coordinates2 = rhs.GetCoordinates();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ component_coordinates1.size() };
        assert(Ncomponent.Get() == component_coordinates2.size());

        for (::MoReFEM::GeometryNS::dimension_type i{ 0 }; i < Ncomponent; ++i)
        {
            if (!NumericNS::AreEqual(component_coordinates1(i.Get()), component_coordinates2(i.Get())))
            {
#ifndef NDEBUG // \todo #244 This test is really heavy and could be avoided in 'simple' debug run.
                auto&& output_more_generic_function = ExtractMismatchedComponentIndexes(lhs, rhs);
                assert("When the current function is called it is implicitly expected to be for an edge of a "
                       "reference topology element that is essentially quadrangular in nature."
                       && output_more_generic_function.size() == 1UL);
                assert(output_more_generic_function.back() == i);
#endif // NDEBUG

                return i;
            }
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    std::vector<::MoReFEM::GeometryNS::dimension_type>
    ExtractIdenticalComponentIndexes(const std::vector<LocalCoords>& coords)
    {
        assert(coords.size() > 1UL);
        std::vector<LocalCoords::underlying_type> coords_component_list;

        coords_component_list.reserve(coords.size());

        for (const auto& coord : coords)
            coords_component_list.push_back(coord.GetCoordinates());

        const auto& last_coord = coords_component_list.back();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ coords_component_list.back().size() };

#ifndef NDEBUG
        for (const auto& coords_component : coords_component_list)
            assert(coords_component.size() == Ncomponent.Get());
#endif // NDEBUG

        std::vector<::MoReFEM::GeometryNS::dimension_type> ret;

        for (::MoReFEM::GeometryNS::dimension_type i{ 0 }; i < Ncomponent; ++i)
        {
            bool still_identical = true;

            for (std::size_t j = 0; still_identical && j < coords_component_list.size(); ++j)
            {
                if (!NumericNS::AreEqual(coords_component_list[j](i.Get()), last_coord(i.Get())))
                    still_identical = false;
            }

            if (still_identical)
                ret.push_back(i);
        }

        return ret;
    }


    std::pair<::MoReFEM::GeometryNS::dimension_type, double>
    ExtractIdenticalComponentIndex(const std::vector<LocalCoords>& coords)
    {
        assert(coords.size() > 1UL);
        std::vector<LocalCoords::underlying_type> coords_component_list;
        coords_component_list.reserve(coords.size());

        for (const auto& coord : coords)
            coords_component_list.push_back(coord.GetCoordinates());

        const auto& last_coord = coords_component_list.back();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ coords_component_list.back().size() };

#ifndef NDEBUG
        for (const auto& coords_component : coords_component_list)
            assert(coords_component.size() == Ncomponent.Get());
#endif // NDEBUG

        for (::MoReFEM::GeometryNS::dimension_type i{ 0 }; i < Ncomponent; ++i)
        {
            bool still_identical = true;

            for (std::size_t j = 0; still_identical && j < coords_component_list.size(); ++j)
            {
                if (!NumericNS::AreEqual(coords_component_list[j](i.Get()), last_coord(i.Get())))
                    still_identical = false;
            }

            if (still_identical)
            {
#ifndef NDEBUG // \todo #244 This test is really heavy and could be avoided in 'simple' debug run.
                auto&& output_more_generic_function = ExtractIdenticalComponentIndexes(coords);
                assert("When the current function is called it is implicitly expected to be for a face of a "
                       "reference topology element that is essentially quadrangular in nature."
                       && output_more_generic_function.size() == 1UL);
                assert(output_more_generic_function.back() == i);
#endif // NDEBUG
                return std::make_pair(i, last_coord(i.Get()));
            }
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    bool operator==(const LocalCoords& lhs, const LocalCoords& rhs)
    {
        decltype(auto) lhs_vector = lhs.GetCoordinates();
        decltype(auto) rhs_vector = rhs.GetCoordinates();

        // isApprox() would return false unless both are exactly zero; see
        // https://eigen.tuxfamily.org/dox/classEigen_1_1DenseBase.html#ae8443357b808cd393be1b51974213f9c
        if (lhs_vector.isZero() && rhs_vector.isZero())
            return true;

        return lhs_vector.isApprox(rhs_vector);
    }


    std::ostream& operator<<(std::ostream& stream, const LocalCoords& rhs)
    {
        rhs.Print(stream);
        return stream;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
