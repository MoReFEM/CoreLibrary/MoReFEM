// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_LOCALCOORDS_DOT_HPP_
#define MOREFEM_GEOMETRY_COORDS_LOCALCOORDS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <initializer_list>
#include <iosfwd>
#include <memory>
#include <utility>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief LocalCoords used in the local reference element.
     *
     * \note You shouldn't have to handle this directly at all, even if you're writing your own operators. The only
     * reason it is in Advanced rather than in Internal namespace is that you might need it should you define your own
     * \a QuadratureRule.
     *
     * \internal \a LocalCoords doesn't inherit \a SpatialPoint on purpose: I do not want a
     * IS-A relationship induced by a public inheritance. The reason is that I want the strong typing to help keep thex
     * code clean and clear: there is hence no risk on giving a \a SpatialPoint when a \a LocalCoords is expected, or
     * vice-versa. Another reason is that for performance reasons I do not want virtual methods in \a SpatialPoint
     * classes, and I would have to put the destructor virtual as \a SpatialPoint are an obvious candidate to be used
     * polymorphically.
     * \endinternal
     */
    class LocalCoords
    {
      public:
        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<LocalCoords>;


      public:
        //! Alias to underlying type,
        using underlying_type = Wrappers::EigenNS::VectorMaxNd<3>;

      public:
        /// \name Constructors and destructor.
        ///@{


        /*!
         * \brief Constructor from a vector.
         *
         * T is expected to be std::vector<double>.
         * \param[in] coordinates_as_double Value for each component of the object to build. Contrary to \a Coords,
         * a \a LocalCoords may encompass 1, 2 or 3 components.
         */
        template<class T>
        explicit LocalCoords(T&& coordinates_as_double);

        //! Constructor from an initializer list.
        //! \param[in] coor Initializer list which gives the (x, y, z) values.
        LocalCoords(std::initializer_list<double>&& coor);


        //! \copydoc doxygen_hide_copy_constructor
        LocalCoords(const LocalCoords& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        LocalCoords(LocalCoords&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        LocalCoords& operator=(const LocalCoords& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        LocalCoords& operator=(LocalCoords&& rhs) = default;

        //! Destructor.
        virtual ~LocalCoords();

        ///@}


        //! Access coordinate r of the point.
        double r() const noexcept;

        //! Access coordinate s of the point.
        double s() const noexcept;

        //! Access coordinate t of the point.
        double t() const noexcept;

        /*!
         * \brief Subscript operator, const version.
         *
         * \param[in] index Component index. x, y and z coordinates may be accessed respectively with [0], [1], [2].
         *
         * \return Coordinate at the index-th component.
         */
        double operator[](::MoReFEM::GeometryNS::dimension_type index) const noexcept;

        /*!
         * \brief Subscript operator, non-const version.
         *
         * \param[in] index Component index. x, y and z coordinates may be accessed respectively with [0], [1], [2].
         *
         * \return Coordinate at the index-th component.
         */
        double& GetNonCstValue(::MoReFEM::GeometryNS::dimension_type index) noexcept;

        //! Get the dimension of the LocalCoords.
        GeometryNS::dimension_type GetDimension() const noexcept;

        //! Print function (used also for operator<< overload).
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;

        //! Return the coordinates of the LocalCoords.
        const underlying_type& GetCoordinates() const noexcept;

        //! Returns the value of the \a index -th coordinates, or 0 if there are none.
        //! \param[in] index Index of the component requested: 0 for x, 1 for y, 2 for z.
        double GetValueOrZero(::MoReFEM::GeometryNS::dimension_type index) const noexcept;


      private:
        //! List of coordinates. Might be 1, 2 or 3 depending on the dimension of the element for which the coords are
        //! defined.
        underlying_type coordinate_list_;
    };


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * Two LocalCoords are consider equal when all their components are.
     *
     * It is assumed here (and asserted in debug) that both arguments are the same dimension.
     */
    bool operator==(const LocalCoords& lhs, const LocalCoords& rhs);


    /*!
     * \brief Return the center of gravity of several \a LocalCoords.
     *
     * It is assumed here (and asserted in debug) that all LocalCoords are the same dimension.
     *
     * \param[in] coords_list List of \a Coords for which center of gravity is sought.
     *
     * \tparam ContainerT A contained to enclose several LocalCoords objects. Typically a
     * std::vector<LocalCoords> or std::array<LocalCoords, ...>.
     *
     * \return Computed center of gravity.
     */
    template<class ContainerT>
    LocalCoords ComputeCenterOfGravity(const ContainerT& coords_list);

    /*!
     * \brief Find how many components differ between two LocalCoords, and returns their index.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return Indexes of the mismatched components. For instance if (0., 1., 0.) and (0., 4., 0.) are considered in
     * \a coords_list, return {1}.
     */
    std::vector<::MoReFEM::GeometryNS::dimension_type> ExtractMismatchedComponentIndexes(const LocalCoords& lhs,
                                                                                         const LocalCoords& rhs);


    /*!
     * \brief A more limited version of ExtractMismatchedComponentIndexes in the case exactly one value is expected.
     *
     * This is the one actually required: this function is called when determining which is the component that
     * vary inside an edge of a reference topology element that is quadrangular of hexahedronal.
     *
     * There is an assert here that requires only one value fits the bill.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return Position of the mismatched component (unique by design of this function; if not the call was improper).
     */
    ::MoReFEM::GeometryNS::dimension_type ExtractMismatchedComponentIndex(const LocalCoords& lhs,
                                                                          const LocalCoords& rhs);


    /*!
     * \brief Find how many components are identical for each of the input LocalCoords, and returns their index.
     *
     * \param[in] coords_list List of \a Coords under investigation.
     *
     * \return Indexes of the components that are equal. For instance if (0, 1, 0) and (0, 4, 0) are considered in
     * \a coords_list, return (0, 2).
     */
    std::vector<::MoReFEM::GeometryNS::dimension_type>
    ExtractIdenticalComponentIndexes(const std::vector<LocalCoords>& coords_list);


    /*!
     * \brief A more limited version of ExtractIdenticalComponentIndexes in the case exactly one value is expected.
     *
     *
     * There is an assert here that requires only one value fits the bill.
     * \param[in] coords_list List of \a Coords under investigation.
     * \return Key is the position of the identical component (assumed to be unique) and the value is its value.
     */
    std::pair<::MoReFEM::GeometryNS::dimension_type, double>
    ExtractIdenticalComponentIndex(const std::vector<LocalCoords>& coords_list);


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const LocalCoords& rhs);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Coords/LocalCoords.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_LOCALCOORDS_DOT_HPP_
// *** MoReFEM end header guards *** < //
