// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_LOCALCOORDS_DOT_HXX_
#define MOREFEM_GEOMETRY_COORDS_LOCALCOORDS_DOT_HXX_
// IWYU pragma: private, include "Geometry/Coords/LocalCoords.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <numeric>
#include <type_traits> // IWYU pragma: keep
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class T>
    LocalCoords::LocalCoords(T&& coor)
    {
        static_assert(std::is_same<std::remove_reference_t<T>, underlying_type>::value,
                      "Forwarding reference template argument.");

        coordinate_list_ = std::move(coor);
    }


    inline double LocalCoords::r() const noexcept
    {
        return operator[](::MoReFEM::GeometryNS::dimension_type{ Eigen::Index{} });
    }


    inline double LocalCoords::s() const noexcept
    {
        return operator[](::MoReFEM::GeometryNS::dimension_type{ 1 });
    }


    inline double LocalCoords::t() const noexcept
    {
        return operator[](::MoReFEM::GeometryNS::dimension_type{ 2 });
    }


    inline auto LocalCoords::GetDimension() const noexcept -> GeometryNS::dimension_type
    {
        return GeometryNS::dimension_type{ coordinate_list_.size() };
    }


    inline double LocalCoords::operator[](::MoReFEM::GeometryNS::dimension_type index) const noexcept
    {
        assert(index < GetDimension());
        return coordinate_list_(index.Get());
    }


    inline double& LocalCoords::GetNonCstValue(::MoReFEM::GeometryNS::dimension_type index) noexcept
    {
        assert(index < GetDimension());
        return coordinate_list_(index.Get());
    }


    inline double LocalCoords::GetValueOrZero(::MoReFEM::GeometryNS::dimension_type index) const noexcept
    {
        assert(index.Get() < 3);

        if (index >= GetDimension())
            return 0.;

        return coordinate_list_(index.Get());
    }


    template<class ContainerT>
    LocalCoords ComputeCenterOfGravity(const ContainerT& coords_list)
    {
        static_assert(std::is_same<typename ContainerT::value_type, LocalCoords>(),
                      "ContainerT must be a container of LocalCoords objects.");

        assert(!coords_list.empty());
        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ coords_list.back().GetDimension() };

        auto begin = coords_list.cbegin();
        auto end = coords_list.cend();

        assert(std::all_of(begin,
                           end,
                           [&Ncomponent](const LocalCoords& local_coord)
                           {
                               return local_coord.GetDimension() == Ncomponent;
                           }));

        LocalCoords::underlying_type ret_values(Ncomponent.Get());

        const double inv = 1. / static_cast<double>(coords_list.size());

        for (auto i = ::MoReFEM::GeometryNS::dimension_type{ 0 }; i < Ncomponent; ++i)
        {
            ret_values(i.Get()) = inv
                                  * std::accumulate(begin,
                                                    end,
                                                    0.,
                                                    [i](double sum, const LocalCoords& local_coord)
                                                    {
                                                        return sum + local_coord[i];
                                                    });
        }


        return LocalCoords(ret_values);
    }


    inline auto LocalCoords::GetCoordinates() const noexcept -> const underlying_type&
    {
        return coordinate_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_LOCALCOORDS_DOT_HXX_
// *** MoReFEM end header guards *** < //
