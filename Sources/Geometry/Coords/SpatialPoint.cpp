// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <string_view>
#include <utility>

#include "Geometry/Coords/SpatialPoint.hpp"

#include "Utilities/Containers/Print.hpp"


namespace MoReFEM
{


    SpatialPoint::SpatialPoint()
    {
        Reset();
    }


    SpatialPoint::SpatialPoint(Eigen::Vector3d&& vector) : underlying_vector_{ std::move(vector) }
    { }


    SpatialPoint::SpatialPoint(double x, double y, double z, const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    : underlying_vector_(space_unit.Get() * Eigen::Vector3d{ x, y, z })
    { }


    SpatialPoint::SpatialPoint(GeometryNS::dimension_type Ncoor,
                               std::istream& stream,
                               const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    {
        assert(Ncoor <= GeometryNS::dimension_type{ 3 });

        Eigen::Vector3d buf_coordinates;
        buf_coordinates.setZero();

        auto initialPosition = stream.tellg();

        for (auto i = GeometryNS::dimension_type{}; i < Ncoor; ++i)
        {
            stream >> buf_coordinates(i.Get());
            buf_coordinates(i.Get()) *= space_unit.Get();
        }

        if (stream)
        {
            // Modify point only if failbit not set.
            underlying_vector_ = buf_coordinates;
        } else
        {
            // In case of failure, put back the cursor at the point it was before trying unsuccessfully to read a point
            auto state = stream.rdstate();
            stream.clear();
            stream.seekg(initialPosition);
            stream.setstate(state);
        }
    }


    void SpatialPoint::Print(std::ostream& stream) const
    {
        Utilities::PrintContainer<>::Do(underlying_vector_,
                                        stream,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("("),
                                        PrintNS::Delimiter::closer(")"));
    }


    std::ostream& operator<<(std::ostream& stream, const SpatialPoint& rhs)
    {
        rhs.Print(stream);
        return stream;
    }


    void SpatialPoint::SetUnderlyingVector(Eigen::Vector3d&& vector)
    {
        underlying_vector_ = std::move(vector);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
