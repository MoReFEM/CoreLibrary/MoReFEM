// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HPP_
#define MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Geometry/Coords/StrongType.hpp" // IWYU pragma: export
#include "Geometry/StrongType.hpp"        // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Define a spatial three-dimensional point.
     *
     * If the point is actually part of a mesh, you should look for derived class \a Coords.
     *
     * \todo #887 Should probably be advanced, but currently FindSpatialPointOfGlobalVector requires public and casual
     * access to it. However this class was avoided in Poromechanics; I'll have to check whether the new mechanism would
     * work in CardiacMechanics where FindSpatialPointOfGlobalVector is applied (the priority of this task is not very
     * high, and I have to retrieve what I did in Poromechanics.
     */
    class SpatialPoint
    {
      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SpatialPoint;

        //! Convenient smart pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Vector of unique_ptr.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special member functions.
        ///@{


        /*!
         * \brief Default constructor; all coordinates are set to 0.
         *
         */
        explicit SpatialPoint();


        /*!
         * \brief Constructor from Eigen vector.
         *
         * \param[in] vector Eigen vector with values to be stored.
         */
        explicit SpatialPoint(Eigen::Vector3d&& vector);

        /*!
         * \brief Constructor from three scalars.
         *
         * \param[in] x Value for first component.
         * \param[in] y Value for second component.
         * \param[in] z Value for third component.
         * \copydoc doxygen_hide_space_unit_arg
         */
        explicit SpatialPoint(double x, double y, double z, ::MoReFEM::CoordsNS::space_unit_type space_unit);

        /*!
         * \brief Constructor from an array.
         *
         * \tparam T Must be std::array<Floating-point type, 3>.
         *
         * \param[in] value Value of the array to set.
         * \copydoc doxygen_hide_space_unit_arg
         */
        template<typename T>
        explicit SpatialPoint(T&& value, const ::MoReFEM::CoordsNS::space_unit_type space_unit);


        /*!
         * \brief Constructor from a input stream.
         *
         * \param[in] Ncoor Number of coordinates to be read. Expected to be at most 3.
         * \param[in,out] stream Stream from which the point is read. Coordinates are expected to be separated by tabs
         * or spaces.
         * Stream is read until failbit is met; then it is put back at the position just before that failure.
         * \copydoc doxygen_hide_space_unit_arg
         */
        explicit SpatialPoint(GeometryNS::dimension_type Ncoor,
                              std::istream& stream,
                              const ::MoReFEM::CoordsNS::space_unit_type space_unit);

        //! \copydoc doxygen_hide_copy_constructor
        SpatialPoint(const SpatialPoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SpatialPoint(SpatialPoint&& rhs) = delete;

        /*!
         * \brief Destructor.
         *
         * \internal Virtual status is avoided as \a Coords objects are not supposed to be used through a
         * \a SpatialPoint pointer, and at the time being no other child class is expected.
         * \endinternal
         */
        ~SpatialPoint() = default;

        //! \copydoc doxygen_hide_copy_affectation
        SpatialPoint& operator=(const SpatialPoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SpatialPoint& operator=(SpatialPoint&& rhs) = delete;


        ///@}


      public:
        //! Return the first component of the point.
        double x() const noexcept;

        //! Return the second component of the point (if relevant; if not an exception is thrown).
        double y() const noexcept;

        //! Return the third component of the point (if relevant; if not an exception is thrown).
        double z() const noexcept;

        /*!
         * \copydoc doxygen_hide_const_subscript_operator
         *
         * \a i might be 0 (for 'x' component), 1 (for 'y') or 2 (for 'z').
         */
        double operator[](::MoReFEM::GeometryNS::dimension_type index) const noexcept;

        /*!
         * \brief Non constant access to the value for \a component.
         *
         * \param[in] index Index of the sought component: 0 for x, 1 for y and 2 for z.
         */
        double& GetNonCstValue(::MoReFEM::GeometryNS::dimension_type index) noexcept;

        //! Print function: display the coordinates.
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;

        //! Get the underlying Eigen Vector.
        const Eigen::Vector3d& GetUnderlyingVector() const noexcept;

        //! Reset the coords to 0.
        void Reset() noexcept;

        /*!
         * \brief Set the underying vector.
         *
         * \param[in] vector New value for the vector.
         */
        void SetUnderlyingVector(Eigen::Vector3d&& vector);


      private:
        /*!
         * \brief Underlying Eigen vector.
         *
         * It is a 3D vector - if you consider an object with a lesser dimension the unused coordinates are just zeroed.
         */
        Eigen::Vector3d underlying_vector_{};

        //! Friendship.
        friend double Distance(const SpatialPoint& lhs, const SpatialPoint& rhs);
    };


    /*!
     * \brief Calculates the distance between two points, using a L2 norm.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return Distance between two points following L2 norm.
     */
    double Distance(const SpatialPoint& lhs, const SpatialPoint& rhs);

    /*!
     * \brief Calculates the square of the distance between two points, using a L2 norm.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return Square of the distance between two points following L2 norm.
     *
     * If what you want is just to compare distances agains one another, you're better off using this rather than
     * `Distance()` as you avoid computing the costly square root.
     */
    double SquaredDistance(const SpatialPoint& lhs, const SpatialPoint& rhs);


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const SpatialPoint& rhs);

    /*!
     * \brief Overload for operator+ between two `SpatialPoint` objects.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return The sum of both objects.
     */
    SpatialPoint operator+(const SpatialPoint& lhs, const SpatialPoint& rhs);

    /*!
     * \brief Overload for operator- between two `SpatialPoint` objects.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return The differnce of both objects (`lhs` - `rhs`).
     */
    SpatialPoint operator-(const SpatialPoint& lhs, const SpatialPoint& rhs);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Coords/SpatialPoint.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
