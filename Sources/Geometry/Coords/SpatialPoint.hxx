// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HXX_
#define MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HXX_
// IWYU pragma: private, include "Geometry/Coords/SpatialPoint.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <tuple>
#include <type_traits> // IWYU pragma: keep


namespace MoReFEM
{


    template<class T>
    SpatialPoint::SpatialPoint(T&& array, const ::MoReFEM::CoordsNS::space_unit_type space_unit)
    {
        using T_without_reference = typename std::remove_reference_t<T>;

        static_assert(std::is_floating_point<typename T_without_reference::value_type>::value,
                      "T must be an array of floating-point type!");

        static_assert(std::tuple_size<T_without_reference>::value == 3UL,
                      "T must be a std::array of size 3. The condition is necessary but not "
                      "enough to ensure this, but it's a good start (the existence of iterators also helps below).");

        std::transform(array.cbegin(),
                       array.cend(),
                       underlying_vector_.begin(),
                       [space_unit](auto value)
                       {
                           return static_cast<double>(value) * space_unit.Get();
                       });
    }


    inline double SpatialPoint::x() const noexcept
    {
        return underlying_vector_(0);
    }


    inline double SpatialPoint::y() const noexcept
    {
        return underlying_vector_(1);
    }


    inline double SpatialPoint::z() const noexcept
    {
        return underlying_vector_(2);
    }


    inline double SpatialPoint::operator[](::MoReFEM::GeometryNS::dimension_type index) const noexcept
    {
        assert(index.Get() < underlying_vector_.size());
        return underlying_vector_(index.Get());
    }


    inline double& SpatialPoint::GetNonCstValue(::MoReFEM::GeometryNS::dimension_type index) noexcept
    {
        assert(index.Get() < underlying_vector_.size());
        return underlying_vector_(index.Get());
    }


    inline const Eigen::Vector3d& SpatialPoint::GetUnderlyingVector() const noexcept
    {
        return underlying_vector_;
    }


    inline void SpatialPoint::Reset() noexcept
    {
        underlying_vector_.setZero();
    }


    inline auto Distance(const SpatialPoint& lhs, const SpatialPoint& rhs) -> double
    {
        Eigen::Vector3d diff = lhs.GetUnderlyingVector() - rhs.GetUnderlyingVector();
        return diff.norm();
    }


    inline auto SquaredDistance(const SpatialPoint& lhs, const SpatialPoint& rhs) -> double
    {
        Eigen::Vector3d diff = lhs.GetUnderlyingVector() - rhs.GetUnderlyingVector();
        return diff.squaredNorm();
    }


    inline auto operator+(const SpatialPoint& lhs, const SpatialPoint& rhs) -> SpatialPoint
    {
        Eigen::Vector3d vector = lhs.GetUnderlyingVector() + rhs.GetUnderlyingVector();
        return SpatialPoint(std::move(vector));
    }


    inline auto operator-(const SpatialPoint& lhs, const SpatialPoint& rhs) -> SpatialPoint
    {
        Eigen::Vector3d vector = lhs.GetUnderlyingVector() - rhs.GetUnderlyingVector();
        return SpatialPoint(std::move(vector));
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
