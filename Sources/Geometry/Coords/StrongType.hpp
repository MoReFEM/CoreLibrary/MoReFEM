// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_STRONGTYPE_DOT_HPP_
#define MOREFEM_GEOMETRY_COORDS_STRONGTYPE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::CoordsNS
{


    /*!
     * \brief Enum class to choose which kind of \a Coords index is to be used.
     */
    enum class index_enum { program_wise_position, processor_wise_position, from_mesh_file };


    /*!
     * \brief Strong type for index that gives the program-wise position.
     *
     * \copydetails doxygen_hide_strong_type_quick_explanation
     */
    using program_wise_position = StrongType<std::size_t,
                                             struct ProgramWisePositionTag,
                                             StrongTypeNS::Comparable,
                                             StrongTypeNS::Hashable,
                                             StrongTypeNS::DefaultConstructible,
                                             StrongTypeNS::Printable,
                                             StrongTypeNS::AsMpiDatatype,
                                             StrongTypeNS::Incrementable>;


    /*!
     * \brief Strong type for index that gives the processor-wise position.
     *
     * \copydetails doxygen_hide_strong_type_quick_explanation
     */
    using processor_wise_position = StrongType<std::size_t,
                                               struct ProcessorWisePositionTag,
                                               StrongTypeNS::Comparable,
                                               StrongTypeNS::Hashable,
                                               StrongTypeNS::DefaultConstructible,
                                               StrongTypeNS::Printable,
                                               StrongTypeNS::AsMpiDatatype,
                                               StrongTypeNS::Incrementable>;


    /*!
     * \brief Strong type for the \a Coords index that was read in the input mesh file (and is format-dependent).
     *
     * \copydetails doxygen_hide_strong_type_quick_explanation
     */
    using index_from_mesh_file = StrongType<std::size_t,
                                            struct IndexFromMeshFileTag,
                                            StrongTypeNS::Comparable,
                                            StrongTypeNS::Hashable,
                                            StrongTypeNS::DefaultConstructible,
                                            StrongTypeNS::Printable,
                                            StrongTypeNS::AsMpiDatatype,
                                            StrongTypeNS::Incrementable>;


    // clang-format off
    //! Convenient alias to find which \a StrongType to use depending on the enum  value template parameter.
    //! \tparam TypeT Tells which kind of \a Coords index is actually requested.
    template<index_enum TypeT>
    using index_type =
    std::conditional_t
    <
        TypeT == index_enum::program_wise_position,
        program_wise_position,
        std::conditional_t
        <
            TypeT == index_enum::processor_wise_position,
            processor_wise_position,
            index_from_mesh_file
        >
    >;
    // clang-format on

    static_assert(std::is_same<index_type<index_enum::program_wise_position>, program_wise_position>());

    static_assert(std::is_same<index_type<index_enum::processor_wise_position>, processor_wise_position>());

    static_assert(std::is_same<index_type<index_enum::from_mesh_file>, index_from_mesh_file>());

    /*!
     * \brief Strong type for index that gives the program-wise position.
     *
     * \copydetails doxygen_hide_strong_type_quick_explanation
     */
    // clang-format off
    using space_unit_type = 
        StrongType
        <
            double,
            struct SpaceUnitTag,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;
    // clang-format on

} // namespace MoReFEM::CoordsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_STRONGTYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
