// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_ADVANCED_CRITERION_DOT_HPP_
#define MOREFEM_GEOMETRY_DOMAIN_ADVANCED_CRITERION_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Advanced::DomainNS
{


    /*!
     * \brief List of possible criteriathat might be used to describe a \a Domain.
     */
    enum class Criterion { Begin = 0, mesh = Begin, dimension, geometric_elt_type, label, End };


} // namespace MoReFEM::Advanced::DomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_ADVANCED_CRITERION_DOT_HPP_
// *** MoReFEM end header guards *** < //
