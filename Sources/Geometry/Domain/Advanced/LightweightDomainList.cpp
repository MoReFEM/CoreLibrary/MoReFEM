// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <numeric>
#include <sstream>
#include <string>
#include <vector>

#include "Geometry/Domain/Advanced/LightweightDomainList.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM::Advanced
{


    const std::string& LightweightDomainList::ClassName()
    {
        static const std::string ret("LightweightDomainList");
        return ret;
    }


    LightweightDomainList::LightweightDomainList(std::size_t unique_id,
                                                 ::MoReFEM::MeshNS::unique_id mesh_index,
                                                 const std::vector<::MoReFEM::DomainNS::unique_id>& domain_index_list,
                                                 const std::vector<MeshLabelNS::index_type>& mesh_label_list,
                                                 const std::vector<std::size_t>& number_in_domain_list)
    : unique_id_parent(unique_id), mesh_(Internal::MeshNS::MeshManager::GetInstance().GetMesh(mesh_index))
    {
        // Check consistency of both vectors.
        const auto Nlabel = std::accumulate(number_in_domain_list.cbegin(), number_in_domain_list.cend(), 0UL);

        if (mesh_label_list.size() != Nlabel)
        {
            std::ostringstream oconv;
            oconv << "Inconsistency in the creation of LightweightDomainList" << unique_id << ": " << Nlabel
                  << " were expected according to number_in_domain_list field but " << mesh_label_list.size()
                  << " were actually provided in mesh_label_list.";

            throw Exception(oconv.str());
        }

        if (domain_index_list.size() != number_in_domain_list.size())
        {
            std::ostringstream oconv;
            oconv << "Inconsistency in the creation of LightweightDomainList" << unique_id
                  << ": domain_index_list "
                     "and number_in_domain_list should be the same size.";

            throw Exception(oconv.str());
        }


        auto it_label = mesh_label_list.cbegin();

        std::size_t current_index(0UL);

        decltype(auto) domain_manager = DomainManager::GetInstance();

        for (const auto Nlabel_in_domain : number_in_domain_list)
        {
            std::vector<MeshLabelNS::index_type> domain_label_list;

            for (std::size_t i = 0UL; i < Nlabel_in_domain; ++i)
            {
                assert(it_label != mesh_label_list.cend() && "Should have been covered by the above exception.");
                domain_label_list.push_back(*it_label);
                ++it_label;
            }

            assert(current_index < domain_index_list.size());

            const auto domain_unique_id = domain_index_list[current_index++];

            domain_manager.CreateLightweightDomain(domain_unique_id, mesh_index, domain_label_list);

            domain_storage_.push_back(&domain_manager.GetDomain(domain_unique_id));
        }

        assert(it_label == mesh_label_list.cend());
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
