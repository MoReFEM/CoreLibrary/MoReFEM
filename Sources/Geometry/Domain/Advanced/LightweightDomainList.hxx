// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLIST_DOT_HXX_
#define MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLIST_DOT_HXX_
// IWYU pragma: private, include "Geometry/Domain/Advanced/LightweightDomainList.hpp"
// *** MoReFEM header guards *** < //


#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    inline const std::vector<const Domain*>& LightweightDomainList::GetList() const noexcept
    {
        return domain_storage_;
    }


    inline const Mesh& LightweightDomainList::GetMesh() const noexcept
    {
        return mesh_;
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
