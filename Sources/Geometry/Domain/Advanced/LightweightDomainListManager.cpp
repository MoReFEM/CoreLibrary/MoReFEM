// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
// IWYU pragma: no_include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainList.hpp"
#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/StrongType.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM::Advanced
{


    LightweightDomainListManager::~LightweightDomainListManager() = default;


    const std::string& LightweightDomainListManager::ClassName()
    {
        static const std::string ret("LightweightDomainListManager");
        return ret;
    }


    LightweightDomainListManager::LightweightDomainListManager() = default;


    void LightweightDomainListManager::Create(std::size_t unique_id,
                                              ::MoReFEM::MeshNS::unique_id mesh_index,
                                              const std::vector<::MoReFEM::DomainNS::unique_id>& domain_index_list,
                                              const std::vector<MeshLabelNS::index_type>& mesh_label_list,
                                              const std::vector<std::size_t>& number_in_domain_list)
    {
        auto&& ptr = Internal::WrapUniqueToConst(new LightweightDomainList(
            unique_id, mesh_index, domain_index_list, mesh_label_list, number_in_domain_list));

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        auto insert_return_value = lightweight_domain_list_storage_.insert(std::move(pair));

        if (!insert_return_value.second)
            throw Exception("Two LightweightDomainList objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id) + ").");
    }


    const LightweightDomainList& LightweightDomainListManager::GetLightweightDomainList(std::size_t unique_id) const
    {
        auto it = lightweight_domain_list_storage_.find(unique_id);

        assert(it != lightweight_domain_list_storage_.cend());
        assert(!(!(it->second)));

        return *(it->second);
    }


    void LightweightDomainListManager::Clear()
    {
        decltype(auto) storage = GetNonCstLightweightDomainListStorage();
        storage.clear();
        LightweightDomainList::ClearUniqueIdList();
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
