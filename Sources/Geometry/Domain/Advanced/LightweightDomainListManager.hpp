// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLISTMANAGER_DOT_HPP_
#define MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLISTMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <unordered_map>
#include <vector>

#include "Utilities/InputData/Concept.hpp"   // IWYU pragma: export
#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Internal/LightweightDomainList.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainList.hpp"
#include "Geometry/Mesh/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS { template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> struct ClearSingletons; } // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    /*!
     * \brief Manager that is aware of all \a LightweightDomainListManager.
     *
     * Please notice that all \a Domain defined therein are known (and actually managed by the \a DomainManager).
     */
    class LightweightDomainListManager : public Utilities::Singleton<LightweightDomainListManager>
    {

      private:
        //! Convenient alias to avoid repeating the type.
        using storage_type = std::unordered_map<std::size_t, LightweightDomainList::const_unique_ptr>;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        friend struct MoReFEM::TestNS::ClearSingletons;

      public:
        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::LightweightDomainListNS::Tag;

        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        /*!
         * \brief Create a \a LightweightDomainList object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);


        //! Fetch the domain object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{LightweightDomainList}
        const LightweightDomainList& GetLightweightDomainList(std::size_t unique_id) const;

        //! Fetch the domain region object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{LightweightDomainList}
        LightweightDomainList& GetNonCstLightweightDomainList(std::size_t unique_id);

        //! Constant accessor to the complete lightweight domain list list.
        const storage_type& GetLightweightDomainListStorage() const noexcept;

        //! Non constant accessor to the complete lightweight domain list list.
        storage_type& GetNonCstLightweightDomainListStorage() noexcept;

      private:
        /*!
         * \brief Method in charge of adding a new \a LightweightDomainList to the constructor.
         *
         * \copydetails doxygen_hide_lightweight_domain_list_constructor
         */
        void Create(std::size_t unique_id,
                    ::MoReFEM::MeshNS::unique_id mesh_index,
                    const std::vector<::MoReFEM::DomainNS::unique_id>& domain_index_list,
                    const std::vector<MeshLabelNS::index_type>& mesh_label_list,
                    const std::vector<std::size_t>& number_in_domain_list);

      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        LightweightDomainListManager();

        //! Destructor.
        virtual ~LightweightDomainListManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<LightweightDomainListManager>;
        ///@}

        ///  //! \copydoc doxygen_hide_manager_clear
        void Clear();


      private:
        //! Store the variable \a LightweightDomainList objects by their unique identifier.
        storage_type lightweight_domain_list_storage_;
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Domain/Advanced/LightweightDomainListManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLISTMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
