// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <bitset>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/Domain/Domain.hpp"

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/Advanced/Criterion.hpp"
#include "Geometry/Domain/Internal/DomainHelper.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    const std::string& Domain::ClassName()
    {
        static const std::string ret("Domain");
        return ret;
    }


    Domain::Domain() : unique_id_parent(nullptr)
    { }


    Domain::Domain(const DomainNS::unique_id unique_id,
                   const std::vector<::MoReFEM::MeshNS::unique_id>& mesh_index,
                   const std::vector<GeometryNS::dimension_type>& dimension_list,
                   const std::vector<MeshLabelNS::index_type>& mesh_label_index_list,
                   const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list)
    : unique_id_parent(unique_id)
    {
        if (!mesh_index.empty())
        {
            SetConditionType(Advanced::DomainNS::Criterion::mesh);
            mesh_identifier_ = mesh_index.back();
        }

        if (!dimension_list.empty())
            SetDimensionList(dimension_list);

        if (!mesh_label_index_list.empty())
            SetLabelList(mesh_label_index_list);

        if (!geometric_type_list.empty())
            SetRefGeometricEltIdList(geometric_type_list);
    }


    Domain::Domain(DomainNS::unique_id unique_id,
                   const ::MoReFEM::MeshNS::unique_id mesh_index,
                   const std::vector<MeshLabelNS::index_type>& mesh_label_index_list)
    : unique_id_parent(unique_id), mesh_identifier_(mesh_index)
    {
        SetConditionType(Advanced::DomainNS::Criterion::mesh);


        SetLabelList(mesh_label_index_list);
    }


    bool Domain::IsGeometricEltInside(const GeometricElt& geometric_element) const
    {
        if (!CheckConstraintIfRelevant<Advanced::DomainNS::Criterion::mesh>(geometric_element))
            return false;

        if (!CheckConstraintIfRelevant<Advanced::DomainNS::Criterion::dimension>(geometric_element))
            return false;

        if (!CheckConstraintIfRelevant<Advanced::DomainNS::Criterion::label>(geometric_element))
            return false;

        if (!CheckConstraintIfRelevant<Advanced::DomainNS::Criterion::geometric_elt_type>(geometric_element))
            return false;

        return true;
    }


    bool Domain::DoRefGeomEltMatchCriteria(const RefGeomElt& ref_geom_element) const
    {
        if (!CheckConstraintIfRelevant<Advanced::DomainNS::Criterion::dimension>(ref_geom_element))
            return false;

        if (!CheckConstraintIfRelevant<Advanced::DomainNS::Criterion::geometric_elt_type>(ref_geom_element))
            return false;

        return true;
    }


    void Domain::SetDimensionList(const std::vector<GeometryNS::dimension_type>& dimension_list)
    {
        dimension_list_ = dimension_list;
        std::ranges::sort(dimension_list_);
        SetConditionType(Advanced::DomainNS::Criterion::dimension);
    }


    void Domain::SetRefGeometricEltIdList(const std::vector<Advanced::GeomEltNS::GenericName>& name_list)
    {
        assert(!name_list.empty() && "Shouldn't have been called!");

        {
            geometric_type_list_.reserve(name_list.size());

            const auto& factory = Advanced::GeometricEltFactory::GetInstance();

            for (const auto& name : name_list)
                geometric_type_list_.push_back(factory.GetIdentifier(name));

            std::ranges::sort(geometric_type_list_);
        }

        SetConditionType(Advanced::DomainNS::Criterion::geometric_elt_type);
    }


    void Domain::SetMesh(const std::vector<MeshNS::unique_id>& mesh_index_list)
    {
        assert(!mesh_index_list.empty() && "Shouldn't have been called!");

        if (mesh_index_list.size() > 2UL)
            throw Exception("A domain was defined with two or more meshes; it is not currently foreseen! "
                            "(currently either only one or no mesh restriction at all are accepted).");

        assert(mesh_identifier_ == NumericNS::UninitializedIndex<MeshNS::unique_id>()
               && "This private method should be called only once in the constructor!");
        SetConditionType(Advanced::DomainNS::Criterion::mesh);
        mesh_identifier_ = mesh_index_list.back();
    }


    void Domain::SetLabelList(const std::vector<MeshLabelNS::index_type>& label_index_list)
    {
        assert(!label_index_list.empty() && "Shouldn't have been called!");

        if (!IsConstraintOn<Advanced::DomainNS::Criterion::mesh>())
            throw Exception("As mesh labels are very closely related to a mesh, a mesh must be defined!");

        const auto& mesh = GetMesh();

        const auto& label_list = mesh.GetLabelList();

        const auto begin = label_list.cbegin();
        const auto end = label_list.cend();

        for (auto mesh_label_index_to_keep : label_index_list)
        {
            auto it = std::find_if(begin,
                                   end,
                                   [mesh_label_index_to_keep](const MeshLabel::const_shared_ptr& label_ptr)
                                   {
                                       assert(!(!label_ptr));
                                       return label_ptr->GetIndex() == mesh_label_index_to_keep;
                                   });

            if (it != end)
                mesh_label_list_.push_back(*it);
            else
                std::cout << "[WARNING] Mesh label " << mesh_label_index_to_keep
                          << " isn't actually present in the "
                             "mesh "
                          << mesh.GetUniqueId()
                          << "! (if run in parallel and the label is found on at least "
                             "one other rank you may dismiss this warning)."
                          << '\n';
        }

        std::ranges::sort(mesh_label_list_,

                          Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>());

        SetConditionType(Advanced::DomainNS::Criterion::label);
    }


    void Domain::SetConditionType(Advanced::DomainNS::Criterion constraint_type)
    {
        are_constraints_on_.set(static_cast<std::size_t>(constraint_type));
    }


    const Mesh& Domain::GetMesh() const
    {
        return Internal::MeshNS::MeshManager::GetInstance().GetMesh(GetMeshIdentifier());
    }


    namespace // anonymous
    {


        /*!
         * \brief Compute the number of processor-wise \a Coords in a \a Domain.
         *
         * \param[in] domain Domain considered.
         * \param[in] mesh Mesh in which the domain is enclosed.
         * \param[in] only_lowest_rank If true, a \a Coords is counted only if it is the lowest-rank processor on which
         * it appears as processor-wise.
         *
         * \return Number of Coords in the domain.
         */
        std::size_t NprocWiseCoordsInDomain(const Domain& domain, const Mesh& mesh, const bool only_lowest_rank)
        {
            const auto& processor_wise_coords_list = mesh.GetProcessorWiseCoordsList();
            const auto domain_unique_id = domain.GetUniqueId();

            std::size_t Ncoords_in_domain = 0UL;

            for (const auto& coords_ptr : processor_wise_coords_list)
            {
                assert(!(!coords_ptr));
                const auto& coords = *coords_ptr;

                if (coords.IsInDomain(domain_unique_id))
                {
                    if (!only_lowest_rank || coords.IsLowestProcessorRank())
                        ++Ncoords_in_domain;
                }
            }

            return Ncoords_in_domain;
        }


    } // namespace


    template<>
    std::size_t NcoordsInDomain<MpiScale::processor_wise>([[maybe_unused]] const ::MoReFEM::Wrappers::Mpi& mpi,
                                                          const Domain& domain,
                                                          const Mesh& mesh)
    {
        return NprocWiseCoordsInDomain(domain, mesh, false);
    }


    template<>
    std::size_t
    NcoordsInDomain<MpiScale::program_wise>(const ::MoReFEM::Wrappers::Mpi& mpi, const Domain& domain, const Mesh& mesh)
    {
        const auto local_Ncoords_in_domain = NprocWiseCoordsInDomain(domain, mesh, true);

        return mpi.AllReduce(local_Ncoords_in_domain, Wrappers::MpiNS::Op::Sum);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
