// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_DOMAIN_DOT_HXX_
#define MOREFEM_GEOMETRY_DOMAIN_DOMAIN_DOT_HXX_
// IWYU pragma: private, include "Geometry/Domain/Domain.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <bitset>
#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/Domain/Advanced/Criterion.hpp"
#include "Geometry/Domain/Internal/DomainHelper.hpp" // IWYU pragma: keep
#include "Geometry/Mesh/Mesh.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM::Advanced { enum class GeometricEltEnum : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const std::vector<GeometryNS::dimension_type>& Domain::GetDimensionList() const noexcept
    {
        assert(IsConstraintOn<Advanced::DomainNS::Criterion::dimension>());
        return dimension_list_;
    }


    inline const MeshLabel::vector_const_shared_ptr& Domain::GetMeshLabelList() const noexcept
    {
        assert(IsConstraintOn<Advanced::DomainNS::Criterion::label>());
        return mesh_label_list_;
    }


    inline const std::vector<Advanced::GeometricEltEnum>& Domain::GetRefGeometricEltIdList() const noexcept
    {
        assert(IsConstraintOn<Advanced::DomainNS::Criterion::geometric_elt_type>());
        return geometric_type_list_;
    }


    inline MeshNS::unique_id Domain::GetMeshIdentifier() const noexcept
    {
        assert(IsConstraintOn<Advanced::DomainNS::Criterion::mesh>());
        assert(mesh_identifier_.Get() != NumericNS::UninitializedIndex<std::size_t>());
        return mesh_identifier_;
    }


    template<Advanced::DomainNS::Criterion CriterionT>
    inline bool Domain::IsConstraintOn() const noexcept
    {
        constexpr std::size_t index = static_cast<std::size_t>(CriterionT);
        static_assert(index < static_cast<std::size_t>(Advanced::DomainNS::Criterion::End),
                      "Index must belong to the enum class scope!");

        return are_constraints_on_[index];
    }


    template<Advanced::DomainNS::Criterion CriterionT, class GeometricObjectT>
    bool Domain::IsConstraintFulfilled(const GeometricObjectT& object) const
    {
        assert(IsConstraintOn<CriterionT>());
        static_assert(!std::is_pointer<GeometricObjectT>::value, "Should be a bare object!");


        switch (CriterionT)
        {
        case Advanced::DomainNS::Criterion::mesh:
        {
            return Internal::DomainNS::IsObjectInMesh(object, GetMeshIdentifier());
        }
        case Advanced::DomainNS::Criterion::dimension:
        {
            const auto& dimension_list = GetDimensionList();

            return std::binary_search(dimension_list.cbegin(), dimension_list.cend(), object.GetDimension());
        }
        case Advanced::DomainNS::Criterion::label:
        {
            return Internal::DomainNS::IsMeshLabelInList(object, GetMeshLabelList());
        }
        case Advanced::DomainNS::Criterion::geometric_elt_type:
        {
            const auto& type_list = GetRefGeometricEltIdList();

            return std::binary_search(type_list.cbegin(), type_list.cend(), object.GetIdentifier());
        }
        }

        assert(false && "One of the criterion should have been chosen!");
        return false; // to avoid compilation warning.
    }


    template<Advanced::DomainNS::Criterion CriterionT, class GeometricObjectT>
    bool Domain::CheckConstraintIfRelevant(const GeometricObjectT& object) const
    {
        if (!IsConstraintOn<CriterionT>())
            return true;

        return IsConstraintFulfilled<CriterionT>(object);
    }


    inline bool operator==(const Domain& lhs, const Domain& rhs) noexcept
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_DOMAIN_DOT_HXX_
// *** MoReFEM end header guards *** < //
