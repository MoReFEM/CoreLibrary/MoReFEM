// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <source_location>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
// IWYU pragma: no_include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM
{


    DomainManager::~DomainManager() = default;


    const std::string& DomainManager::ClassName()
    {
        static const std::string ret("DomainManager");
        return ret;
    }


    DomainManager::DomainManager() = default;


    void DomainManager::Create(DomainNS::unique_id unique_id,
                               const std::vector<::MoReFEM::MeshNS::unique_id>& mesh_index,
                               const std::vector<GeometryNS::dimension_type>& dimension_list,
                               const std::vector<MeshLabelNS::index_type>& mesh_label_list,
                               const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list)
    {
        auto&& ptr = Internal::WrapUniqueToConst(
            new Domain(unique_id, mesh_index, dimension_list, mesh_label_list, geometric_type_list));

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        decltype(auto) storage = GetNonCstStorage();

        auto [it, was_properly_inserted] = storage.insert(std::move(pair));

        if (!was_properly_inserted)
            throw Exception("Two Domain objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id.Get()) + ").");
    }


    const Domain& DomainManager::GetDomain(DomainNS::unique_id unique_id, const std::source_location location) const
    {
        decltype(auto) storage = GetStorage();
        auto it = storage.find(unique_id);

        if (it == storage.cend())
            throw Exception("Domain " + std::to_string(unique_id.Get()) + " is not defined!", location);

        assert(!(!(it->second)));

        return *(it->second);
    }


    void DomainManager::Clear()
    {
        decltype(auto) storage = GetNonCstStorage();
        storage.clear();
        Domain::ClearUniqueIdList();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
