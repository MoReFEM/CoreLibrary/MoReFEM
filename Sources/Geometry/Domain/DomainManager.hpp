// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_DOMAINMANAGER_DOT_HPP_
#define MOREFEM_GEOMETRY_DOMAIN_DOMAINMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Internal/Domain.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/StrongType.hpp"                      // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/Mesh/UniqueId.hpp"


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// clang-format off
// ============================

namespace MoReFEM::TestNS { template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> struct ClearSingletons; } // IWYU pragma: keep
namespace MoReFEM::Advanced { class LightweightDomainList; }


// ============================
// clang-format on
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


namespace MoReFEM
{


    /*!
     * \brief This class is used to create and retrieve Domain objects.
     *
     * Domain objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * Domain object given its unique id (which is the one that appears in the input data file).
     *
     */
    class DomainManager : public Utilities::Singleton<DomainManager>
    {

      private:
        //! Convenient alias to avoid repeating the type.
        using storage_type = std::unordered_map<DomainNS::unique_id, Domain::const_unique_ptr>;

        /*!
         * \brief Friendship for tests purposes.
         *
         * In tests, we might want to create objects without a full-fledged model:
         *
         \code
         namespace MoReFEM
         {

             struct TestHelper
             {
                TestHelper()
                {
                    const DomainNS::unique_id id { 0UL };
                    const auto mesh_index = 1UL;
                    const auto dimension = 2UL;

                    DomainManager::CreateOrGetInstance().Create(id,
                                                                                  { mesh_index },
                                                                                  { dimension },
                                                                                  { }, { });
                }

             };

         } // namespace MoReFEM
         \endcode
         *
         * for instance creates directly a domain labelled 0 for the mesh 1 with geometric elements of dimension 2.
         *
         * \attention This trick should be used only while writing lightweight tests; do not use this in a real
         * model!
         */
        friend struct TestHelper;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        friend struct MoReFEM::TestNS::ClearSingletons;


      public:
        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::DomainNS::Tag;

        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        /*!
         * \brief Create a \a NumberingSubset object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);

        /*!
         * \brief Fetch the domain object associated with \a unique_id unique identifier.
         *
         * If the unique_id is invalid, an exception is thrown.
         *
         * \unique_id_param_in_accessor{Domain}
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Domain which GetUniqueId() method yield \a unique_id.
         */
        const Domain& GetDomain(DomainNS::unique_id unique_id,
                                const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Fetch the domain object associated with \a unique_id unique identifier.
         *
         * If the unique_id is invalid, an exception is thrown.
         *
         * \unique_id_param_in_accessor{Domain}
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Non constant reference to the domain which GetUniqueId() method yield \a unique_id.
         */
        Domain& GetNonCstDomain(DomainNS::unique_id unique_id,
                                const std::source_location location = std::source_location::current());

        //! Friendship to a class that needs access to \a CreateLightweightDomain method.
        friend class Advanced::LightweightDomainList;

        //! Constant accessor to the domain list.
        const storage_type& GetStorage() const noexcept;

      private:
        /*!
         * \brief Create a brand new Domain.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique. It is in the input data file
         * the figure that is in the block name, e.g. 1 for Domain1 = { .... }.
         * \param[in] mesh_index_list There might be here one index, that indicates in which mesh the domain is defined.
         * If the domain is not limited to one mesh, leave it empty.
         * \param[in] dimension_list List of dimensions to consider. If empty, no restriction on dimension.
         * \param[in] mesh_label_list List of mesh labels to consider. If empty, no restriction on it. This argument
         * must mandatorily be empty if \a mesh_index is empty: a mesh label is closely related to one given mesh.
         * \param[in] geometric_type_list List of geometric element types to consider in the domain. List of elements
         * available is given by Advanced::GeometricEltFactory::GetNameList(); most if not all of them should been
         * displayed in the comment in the input data file.
         *
         */
        void Create(DomainNS::unique_id unique_id,
                    const std::vector<MeshNS::unique_id>& mesh_index_list,
                    const std::vector<GeometryNS::dimension_type>& dimension_list,
                    const std::vector<MeshLabelNS::index_type>& mesh_label_list,
                    const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list);


        /*!
         * \brief Create a brand new lightweight \a Domain.
         *
         * Such a \a Domain, more restricted than a full-fledged one, should be created only by a \a
         * LightweightDomainList object.
         *
         * \param[in] unique_id Identifier of the domain, that must be unique.
         * \param[in] mesh_index Index of the mesh onto which \a Domain must be created.
         * \param[in] mesh_label_list List of mesh labels to consider.
         *
         */
        void CreateLightweightDomain(DomainNS::unique_id unique_id,
                                     MeshNS::unique_id mesh_index,
                                     const std::vector<MeshLabelNS::index_type>& mesh_label_list);

        //! \copydoc doxygen_hide_manager_clear
        void Clear();

        //! Non constant accessor to the domain list.
        storage_type& GetNonCstStorage() noexcept;


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        DomainManager();

        //! Destructor.
        virtual ~DomainManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<DomainManager>;
        ///@}


      private:
        //! Store the domain objects by their unique identifier.
        storage_type storage_;
    };


    /*!
     * \brief Convert into the \a DomainNS::unique_id strong type the enum value defined in the input data file.
     *
     * In a typical Model, indexes related to fields (e.g. Domain) are defined in an enum class named typically \a
     * DomainIndex. This facility is a shortcut to convert this enum into a proper \a DomainNS::unique_id that is used
     * in the library to identify \a Domain.
     *
     * \param[in] enum_value The index related to the \a Domain, stored in an enum class. See a typical 'InputData.hpp' file
     * to understand it better.
     *
     * \return The identifier for a \a Domain as used in the library.
     */
    template<class EnumT>
    constexpr DomainNS::unique_id AsDomainId(EnumT enum_value);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Domain/DomainManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_DOMAINMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
