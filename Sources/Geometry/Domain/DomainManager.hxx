// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_DOMAINMANAGER_DOT_HXX_
#define MOREFEM_GEOMETRY_DOMAIN_DOMAINMANAGER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Domain/DomainManager.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"
#include "Utilities/Type/StrongType/Internal/Convert.hpp"

#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/Mesh/UniqueId.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void DomainManager::Create(const IndexedSectionDescriptionT&,
                               const ModelSettingsT& model_settings,
                               const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) mesh_index_list_as_int =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::MeshIndexList>(model_settings, input_data);
        decltype(auto) dimension_list_as_int =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::DimensionList>(model_settings, input_data);
        decltype(auto) mesh_label_list_as_int =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::MeshLabelList>(model_settings, input_data);
        decltype(auto) geometric_type_list =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::GeomEltTypeList>(model_settings, input_data);

        std::vector<Advanced::GeomEltNS::GenericName> geom_list(geometric_type_list.size());

        std::transform(geometric_type_list.cbegin(),
                       geometric_type_list.cend(),
                       geom_list.begin(),
                       [](const auto& str)
                       {
                           return Advanced::GeomEltNS::GenericName(str);
                       });

        auto mesh_index_list = Internal::StrongTypeNS::Convert<::MoReFEM::MeshNS::unique_id>(mesh_index_list_as_int);
        auto mesh_label_list =
            Internal::StrongTypeNS::Convert<::MoReFEM::MeshLabelNS::index_type>(mesh_label_list_as_int);

        auto dimension_list =
            Internal::StrongTypeNS::Convert<::MoReFEM::GeometryNS::dimension_type>(dimension_list_as_int);

        Create(DomainNS::unique_id{ section_type::GetUniqueId() },
               mesh_index_list,
               dimension_list,
               mesh_label_list,
               geom_list);
    }


    inline Domain& DomainManager::GetNonCstDomain(DomainNS::unique_id unique_id, const std::source_location location)
    {
        return const_cast<Domain&>(GetDomain(unique_id, location));
    }


    inline void DomainManager::CreateLightweightDomain(DomainNS::unique_id unique_id,
                                                       MeshNS::unique_id mesh_index,
                                                       const std::vector<MeshLabelNS::index_type>& mesh_label_list)
    {
        Create(unique_id, { mesh_index }, {}, mesh_label_list, {});
    }


    inline auto DomainManager::GetStorage() const noexcept -> const storage_type&
    {
        return storage_;
    }


    inline auto DomainManager::GetNonCstStorage() noexcept -> storage_type&
    {
        return const_cast<storage_type&>(GetStorage());
    }


    template<class EnumT>
    constexpr DomainNS::unique_id AsDomainId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return DomainNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_DOMAINMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
