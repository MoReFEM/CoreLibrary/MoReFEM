// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Geometry/Domain/Internal/DomainHelper.hpp"

#include "Geometry/Domain/MeshLabel.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::DomainNS
{


    [[noreturn]] bool IsObjectInMesh([[maybe_unused]] const RefGeomElt& ref_geom_elt,
                                     [[maybe_unused]] ::MoReFEM::MeshNS::unique_id id)
    {
        assert("Should never be called in runtime!" && false);
        throw; // to avoid compilation warning.
    }


    [[noreturn]] bool IsMeshLabelInList([[maybe_unused]] const RefGeomElt& ref_geom_elt,
                                        [[maybe_unused]] const MeshLabel::vector_const_shared_ptr& vector)
    {
        assert("Should never be called in runtime!" && false);
        throw; // to avoid compilation warning.
    }


} // namespace MoReFEM::Internal::DomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
