// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_INTERNAL_DOMAINHELPER_DOT_HXX_
#define MOREFEM_GEOMETRY_DOMAIN_INTERNAL_DOMAINHELPER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Domain/Internal/DomainHelper.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM::Internal::DomainNS
{


    inline bool IsObjectInMesh(const GeometricElt& geometric_elt, ::MoReFEM::MeshNS::unique_id mesh_identifier)
    {
        return geometric_elt.GetMeshIdentifier() == mesh_identifier;
    }


    inline bool IsMeshLabelInList(const GeometricElt& geometric_elt,
                                  const MeshLabel::vector_const_shared_ptr& mesh_label_list)
    {
        return std::binary_search(mesh_label_list.cbegin(),
                                  mesh_label_list.cend(),
                                  geometric_elt.GetMeshLabelPtr(),
                                  Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>());
    }


} // namespace MoReFEM::Internal::DomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_INTERNAL_DOMAINHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
