// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>      // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep

#include "Geometry/Domain/MeshLabel.hpp"

#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        std::string DescriptionHelper(const std::string& description, MeshLabelNS::index_type index)
        {
            if (description.empty())
            {
                std::ostringstream oconv;
                oconv << "MeshLabel " << index;
                return oconv.str();
            }

            return description;
        }


    } // namespace


    MeshLabel::MeshLabel(const MeshNS::unique_id mesh_id,
                         const MeshLabelNS::index_type index,
                         const std::string& description)
    : mesh_identifier_(mesh_id), index_(index), description_(DescriptionHelper(description, index))
    { }


    MeshLabel::~MeshLabel() = default;


    const std::string& MeshLabel::ClassName()
    {
        static const std::string ret("MeshLabel");
        return ret;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
