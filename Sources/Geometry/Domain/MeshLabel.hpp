// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_MESHLABEL_DOT_HPP_
#define MOREFEM_GEOMETRY_DOMAIN_MESHLABEL_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

#include "Geometry/Domain/StrongType.hpp" // IWYU pragma: export
#include "Geometry/Mesh/UniqueId.hpp"     // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Depiction of a given label.
     *
     * This is mostly just a couple unique_id/description; the expected modus operandi is that a (smart) pointer to
     * the MeshLabel object will be passed to each element that belongs to it (geometric elements, edges, etc...)
     *
     * So MeshLabels objects should always be created through dynamic allocation:
     * \code
     * auto my_surface = std::make_shared<MeshLabel>(mesh, 42, "Left ventricle");
     * \endcode
     *
     * \attention If for some reason we need to know quickly and often all the geometric elements in a label,
     * this data should be stored as a vector of WEAK pointers, to avoid circular definitions.
     */
    class MeshLabel final
    {
      public:
        //! Typical smart pointer associated to the object.
        using const_shared_ptr = std::shared_ptr<const MeshLabel>;

        //! Vector of smart pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

        //! Name of the class (required by unique_id Crtp).
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor
         *
         * \param[in] mesh_id Identifier of the \a Mesh to which the label belongs to.
         * \param[in] index Identifier related to the label. This identifier should be unique for a given
         * \a Mesh (i.e. that a given mesh should get at most one MeshLabel object with a given
         * unique id).
         * \internal <b><tt>[internal]</tt></b> Crtp UniqueId can't be used used as same integer might be used in
         * different meshes.
         * \endinternal
         * \param[in] description Description of the label. Might be empty.
         *
         */
        explicit MeshLabel(MeshNS::unique_id mesh_id, MeshLabelNS::index_type index, const std::string& description);

        //! Destructor.
        ~MeshLabel();

        //! \copydoc doxygen_hide_copy_constructor
        MeshLabel(const MeshLabel& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MeshLabel(MeshLabel&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MeshLabel& operator=(const MeshLabel& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MeshLabel& operator=(MeshLabel&& rhs) = delete;


        ///@}

        //! Get the description
        const std::string& GetDescription() const noexcept;

        //! Get the mesh identifier.
        MeshNS::unique_id GetMeshIdentifier() const noexcept;

        //! Get the index.
        MeshLabelNS::index_type GetIndex() const noexcept;


      private:
        //! Identifier of the mesh for which the label is defined.
        const MeshNS::unique_id mesh_identifier_;

        /*!
         * \brief Identifier related to the label.
         *
         * This identifier should be unique for a given
         * \a Mesh (i.e. that a given mesh should get at most one MeshLabel object with a given
         * unique id).
         * \internal <b><tt>[internal]</tt></b> Crtp UniqueId can't be used used as same integer might be used in
         * different meshes.
         * \endinternal
         */
        const MeshLabelNS::index_type index_;

        //! Description
        std::string description_;
    };


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * Criterion is whether their unique identifiers (returned by GetUniqueId()) are the same.
     */
    inline bool operator==(const MeshLabel& lhs, const MeshLabel& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * Criterion relies upon their unique identifiers (returned by GetUniqueId()).
     */
    bool operator<(const MeshLabel& lhs, const MeshLabel& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_not_equal
     *
     * Criterion is whether their unique identifiers (returned by GetUniqueId()) are not the same.
     */
    inline bool operator!=(const MeshLabel& lhs, const MeshLabel& rhs) noexcept;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Domain/MeshLabel.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_MESHLABEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
