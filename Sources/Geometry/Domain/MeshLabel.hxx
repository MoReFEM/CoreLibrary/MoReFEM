// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_MESHLABEL_DOT_HXX_
#define MOREFEM_GEOMETRY_DOMAIN_MESHLABEL_DOT_HXX_
// IWYU pragma: private, include "Geometry/Domain/MeshLabel.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Geometry/Mesh/UniqueId.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class MeshLabel; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline const std::string& MeshLabel::GetDescription() const noexcept
    {
        return description_;
    }


    inline MeshNS::unique_id MeshLabel::GetMeshIdentifier() const noexcept
    {
        return mesh_identifier_;
    }


    inline auto MeshLabel::GetIndex() const noexcept -> MeshLabelNS::index_type
    {
        return index_;
    }


    inline bool operator<(const MeshLabel& lhs, const MeshLabel& rhs) noexcept
    {
        assert("One should compare only labels that refer to the same mesh."
               && lhs.GetMeshIdentifier() == rhs.GetMeshIdentifier());

        return lhs.GetIndex() < rhs.GetIndex();
    }


    inline bool operator==(const MeshLabel& lhs, const MeshLabel& rhs) noexcept
    {
        assert("One should compare only labels that refer to the same mesh."
               && lhs.GetMeshIdentifier() == rhs.GetMeshIdentifier());

#ifndef NDEBUG
        // In release mode, indexes are compared
        return lhs.GetIndex() == rhs.GetIndex();
#else  // NDEBUG
       // In debug mode, we check the descriptions matches. If not, it highlight a bug in the attribution of
       // the indexes
        bool ret = lhs.GetIndex() == rhs.GetIndex();

        if (ret)
            assert(lhs.GetDescription() == rhs.GetDescription());

        return ret;
#endif // NDEBUG
    }


    inline bool operator!=(const MeshLabel& lhs, const MeshLabel& rhs) noexcept
    {
        return !operator==(lhs, rhs);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_MESHLABEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
