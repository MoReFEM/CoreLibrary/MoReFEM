### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Domain.cpp
		${CMAKE_CURRENT_LIST_DIR}/DomainManager.cpp
		${CMAKE_CURRENT_LIST_DIR}/MeshLabel.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Domain.hpp
		${CMAKE_CURRENT_LIST_DIR}/Domain.hxx
		${CMAKE_CURRENT_LIST_DIR}/DomainManager.hpp
		${CMAKE_CURRENT_LIST_DIR}/DomainManager.hxx
		${CMAKE_CURRENT_LIST_DIR}/MeshLabel.hpp
		${CMAKE_CURRENT_LIST_DIR}/MeshLabel.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/StrongType.hpp
		${CMAKE_CURRENT_LIST_DIR}/UniqueId.hpp
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
