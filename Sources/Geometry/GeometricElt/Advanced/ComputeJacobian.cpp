// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <variant>
// IWYU pragma: no_include <type_traits>

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/StrongType.hpp"


namespace MoReFEM::Advanced::GeomEltNS
{


    namespace // anonymous
    {

        //! Helper object to compute relevant value for a given \a LocalNode.
        struct Helper
        {

          public:
            /*!
             * \brief Constructor.
             */
            Helper(const GeometricElt& geometric_elt,
                   const LocalCoords& local_coords,
                   Eigen::VectorXd& first_derivate_shape_function_for_local_node);

            //! Compute the relevant quantity from first derivate shape function for each \a LocalNode.
            const Eigen::VectorXd& ComputeForLocalNode(LocalNodeNS::index_type local_node_index);

          private:
            const GeometricElt& geom_elt_;

            const LocalCoords& local_coords_;

            Eigen::VectorXd& first_derivate_shape_function_for_local_node_;
        };


    } // namespace


    ComputeJacobian::ComputeJacobian(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension)
    {
        jacobian_ = ::MoReFEM::Wrappers::EigenNS::InitDimensionMatrixVariant(mesh_dimension.Get());
        work_first_derivate_shape_fct_for_local_node_.resize(mesh_dimension.Get());
    }


    ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant ComputeJacobian::Compute(const GeometricElt& geometric_elt,
                                                                                  const LocalCoords& local_coords)
    {

        const LocalNodeNS::index_type Ncoords{ static_cast<Eigen::Index>(geometric_elt.Ncoords()) };

        Helper first_derivate_contribution_helper(
            geometric_elt, local_coords, GetNonCstWorkFirstDerivateShapeFctForLocalNode());


        return std::visit(
            [&geometric_elt, Ncoords, &first_derivate_contribution_helper](
                auto& jacobian) -> ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant
            {
                const auto Nshape_function = ::MoReFEM::GeometryNS::dimension_type{ jacobian.cols() };

                assert(geometric_elt.GetDimension() <= Nshape_function);
                jacobian.setZero();

                for (LocalNodeNS::index_type local_node_index{}; local_node_index < Ncoords; ++local_node_index)
                {
                    decltype(auto) first_derivate_shape_function_for_local_node =
                        first_derivate_contribution_helper.ComputeForLocalNode(local_node_index);

                    const auto& coords_in_geom_elt = geometric_elt.GetCoord(local_node_index.Get());

                    // If LocalCoords is less than geometric element dimension, columns above its dimension are filled
                    // with zeros.
                    for (::MoReFEM::GeometryNS::dimension_type component{ 0 }; component < Nshape_function;
                         ++component) // \todo 1640 Investigate here!
                    {
                        for (auto shape_fct_index = Eigen::Index{}; shape_fct_index < Nshape_function.Get();
                             ++shape_fct_index)
                        {
                            jacobian(component.Get(), shape_fct_index) +=
                                coords_in_geom_elt[component]
                                * first_derivate_shape_function_for_local_node[shape_fct_index];
                        }
                    }

                    // \todo #446
                    if (geometric_elt.GetIdentifier() == Advanced::GeometricEltEnum::Point1)
                    {
                        jacobian(0, 0) = 1;
                    }
                }

                return jacobian;
            },
            GetNonCstJacobian());
    }


    namespace // anonymous
    {


        Helper::Helper(const GeometricElt& geometric_elt,
                       const LocalCoords& local_coords,
                       Eigen::VectorXd& first_derivate_shape_function_for_local_node)
        : geom_elt_(geometric_elt), local_coords_(local_coords),
          first_derivate_shape_function_for_local_node_(first_derivate_shape_function_for_local_node)
        { }


        const Eigen::VectorXd& Helper::ComputeForLocalNode(const LocalNodeNS::index_type local_node_index)
        {
            const auto Ncomponent =
                ::MoReFEM::GeometryNS::dimension_type{ static_cast<Eigen::Index>(geom_elt_.GetDimension()) };

            auto& ret = first_derivate_shape_function_for_local_node_;

            std::ranges::fill(ret, 0.);

            for (::MoReFEM::GeometryNS::dimension_type component{ 0 }; component < Ncomponent; ++component)
                ret(component.Get()) = geom_elt_.FirstDerivateShapeFunction(local_node_index, component, local_coords_);

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Advanced::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
