// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_COMPUTEJACOBIAN_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_COMPUTEJACOBIAN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::GeomEltNS
{


    /*!
     * \brief Helper class to compute Jacobian for a given \a GeometricElt at a given \a LocalCoords (and thus
     * by extension at a given \a QuadraturePoint).
     *
     * The point of this class is to avoid reallocating needlessly memory for the matrix and the list of first
     * derivates values; one such object is intended to compute jacobians for each \a GeometricElt that share
     * the same underlying \a RefGeomElt.
     */
    class ComputeJacobian
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = ComputeJacobian;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension in the mesh which \a GeomElt will use current facility. This is
         * also by construct the number of rows and columns of the jacobian matrix.
         */
        explicit ComputeJacobian(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension);

        //! Destructor.
        ~ComputeJacobian() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ComputeJacobian(const ComputeJacobian& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ComputeJacobian(ComputeJacobian&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ComputeJacobian& operator=(const ComputeJacobian& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ComputeJacobian& operator=(ComputeJacobian&& rhs) = delete;

        ///@}


        /*!
         * \brief Compute the value of the jacobian at \a geometric_elt and \a local_coords.
         *
         * \param[in] geometric_elt \a GeometricElt considered.
         * \param[in] local_coords \a LocalCoords for which the computation is performed.
         *
         * \return Jacobian matrix.
         */
        ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant Compute(const GeometricElt& geometric_elt,
                                                                     const LocalCoords& local_coords);

      private:
        /*!
         * \brief Non constant accessor to the matrix that holds the result of the computation; it should not
         * be accessible publicly.
         */
        ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant& GetNonCstJacobian() noexcept;

        /*!
         * \brief Non constant accessor to the a work variable.
         *
         * It is not intended to be accessed publicly or even to be used consistently from one method to
         * another.
         *
         * \return List of first derivate values computed for a given \a LocalNode in \a Compute() method.
         */
        Eigen::VectorXd& GetNonCstWorkFirstDerivateShapeFctForLocalNode() noexcept;


      private:
        //! Matrix that holds the result of the computation; it should not be accessible publicly except through
        //! \a Compute() result.
        ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant jacobian_;

        /*!
         * \brief A work variable to store first derivate computation results for a given \a LocalNode without reallocating memory each time.
         *
         * It is not intended to be accessed publicly or even to be used consistently from one method to
         * another.
         */
        Eigen::VectorXd work_first_derivate_shape_fct_for_local_node_;
    };


} // namespace MoReFEM::Advanced::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_COMPUTEJACOBIAN_DOT_HPP_
// *** MoReFEM end header guards *** < //
