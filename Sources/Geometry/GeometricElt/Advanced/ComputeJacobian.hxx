// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_COMPUTEJACOBIAN_DOT_HXX_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_COMPUTEJACOBIAN_DOT_HXX_
// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
// *** MoReFEM header guards *** < //

#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::Advanced::GeomEltNS
{


    inline auto ComputeJacobian::GetNonCstJacobian() noexcept -> ::MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant&
    {
        return jacobian_;
    }


    inline auto ComputeJacobian::GetNonCstWorkFirstDerivateShapeFctForLocalNode() noexcept -> Eigen::VectorXd&
    {
        return work_first_derivate_shape_fct_for_local_node_;
    }


} // namespace MoReFEM::Advanced::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_COMPUTEJACOBIAN_DOT_HXX_
// *** MoReFEM end header guards *** < //
