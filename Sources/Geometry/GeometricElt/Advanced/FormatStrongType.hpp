// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_FORMATSTRONGTYPE_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_FORMATSTRONGTYPE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>

#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::Advanced::GeomEltNS
{


    /*!
     * \brief Strong type for the generic name of a \a GeometricElt (e.g. 'Triangle3').
     *
     * This name is not format-dependent.
     *
     * \copydetails doxygen_hide_strong_type_quick_explanation
     */
    using GenericName = StrongType<std::string,
                                   struct GenericNameTag,
                                   StrongTypeNS::Printable,
                                   StrongTypeNS::Hashable,
                                   StrongTypeNS::Comparable,
                                   StrongTypeNS::DefaultConstructible>;


    /*!
     * \brief Strong type for the Ensight name (if relevant) of a \a GeometricElt .
     *
     * \copydetails doxygen_hide_strong_type_quick_explanation
     */
    using EnsightName = StrongType<std::string,
                                   struct EnsightNameTag,
                                   StrongTypeNS::Printable,
                                   StrongTypeNS::Hashable,
                                   StrongTypeNS::Comparable>;


} // namespace MoReFEM::Advanced::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_FORMATSTRONGTYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
