// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Advanced::GeomEltNS
{


    void ComputeBarycenter(const GeometricElt& geometric_elt, SpatialPoint& barycenter)
    {
        barycenter.Reset();

        const auto& coords_list = geometric_elt.GetCoordsList();
        const auto dimension = geometric_elt.GetDimension();

        const double inv_Ncoords = 1. / static_cast<double>(coords_list.size());

        for (auto i = ::MoReFEM::GeometryNS::dimension_type{}; i < dimension; ++i)
        {
            double& current_barycenter_coord = barycenter.GetNonCstValue(i);

            for (const auto& coords_ptr : coords_list)
            {
                assert(!(!coords_ptr));
                current_barycenter_coord += inv_Ncoords * coords_ptr->operator[](i);
            }
        }
    }


    void Local2Global(const GeometricElt& geometric_elt, const LocalCoords& local_coords, SpatialPoint& out)
    {
#ifndef NDEBUG
        const auto Ncomponent = local_coords.GetDimension();
        assert(geometric_elt.GetDimension() == Ncomponent);
        assert(Ncomponent.Get() <= 3);
#endif // NDEBUG

        out.Reset();

        const auto& local_coords_list = geometric_elt.GetCoordsList();
        const LocalNodeNS::index_type Ncoords{ static_cast<Eigen::Index>(geometric_elt.Ncoords()) };
        assert(Ncoords.Get() == static_cast<Eigen::Index>(local_coords_list.size()));

        for (LocalNodeNS::index_type local_node_index{}; local_node_index < Ncoords; ++local_node_index)
        {
            const auto& local_coords_in_geom_elt_ptr =
                local_coords_list[static_cast<std::size_t>(local_node_index.Get())];
            assert(!(!local_coords_in_geom_elt_ptr));
            const auto& local_coords_in_geom_elt = *local_coords_in_geom_elt_ptr;

            const double shape_fct = geometric_elt.ShapeFunction(local_node_index, local_coords);

            for (auto component = ::MoReFEM::GeometryNS::dimension_type{};
                 component < ::MoReFEM::GeometryNS::dimension_type{ 3 };
                 ++component)
                out.GetNonCstValue(component) += shape_fct * local_coords_in_geom_elt[component];
        }
    }

} // namespace MoReFEM::Advanced::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
