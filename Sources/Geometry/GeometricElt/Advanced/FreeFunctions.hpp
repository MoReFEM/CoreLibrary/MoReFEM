// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_FREEFUNCTIONS_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_FREEFUNCTIONS_DOT_HPP_
// *** MoReFEM header guards *** < //

// #include "Geometry/Coords/Coords.hpp" // if Global2local is reactivated

#include <cstddef> // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class SpatialPoint; }
namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::GeomEltNS
{

    /*!
     * \brief Computes the barycenter of a geometric element.
     *
     * \param[in] geometric_elt \a GeometricElt which barycenter is computed.
     * \param[out] barycenter Computed barycenter. Whatever value might have been in input is overwritten.
     */
    void ComputeBarycenter(const GeometricElt& geometric_elt, SpatialPoint& barycenter);


    /*!
     * \brief Find the global coordinates that match the local ones.
     *
     * \param[in] geometric_elt Geometric element considered.
     * \param[in] local_coords Local coordinates to transform into global ones. The dimension of this one must
     * be the same as the dimension of \a geometric_elt. \param[out] out The image of \a local_coords in the
     * global mesh.
     */
    void Local2Global(const GeometricElt& geometric_elt, const LocalCoords& local_coords, SpatialPoint& out);


    /*!
     * \brief Find the local coordinates that match the global ones.
     *
     * Works only if the dimension of \a geometric_elt and \a coords is the same.
     *
     * \note This code stems from Ondomatic and has not been used so far.
     *
     * \param[in] geometric_elt Geometric element considered.
     * \param[in] coords The Coords object to be converted into \a LocalCoords.
     * \param[in] mesh_dimension Dimension of the mesh.
     *
     * \return LocalCoords object matching \a coords.
     */
    //            LocalCoords
    //            Global2local(const GeometricElt& geometric_elt, const Coords& coords, std::size_t
    //            mesh_dimension);


} // namespace MoReFEM::Advanced::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/GeometricElt/Advanced/FreeFunctions.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_FREEFUNCTIONS_DOT_HPP_
// *** MoReFEM end header guards *** < //
