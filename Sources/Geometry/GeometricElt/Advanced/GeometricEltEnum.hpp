// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_GEOMETRICELTENUM_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_GEOMETRICELTENUM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <functional>


namespace MoReFEM::Advanced
{


    /*!
     * This enum class defines an alias for each geometric element.
     *
     * Doing so breaks some of the beauty of the GeometricElt factory: each GeometricElt has now to create a value
     * in this enum here, which will trigger more compilation.
     * However, adding a new geometric element should be quite rare, contrary to element lookup, and using
     * the name of the class as the identifier would hamper the performances.
     *
     * The following enum class should only be used internally.
     *
     */
    enum class GeometricEltEnum : std::size_t {
        Begin = 0,
        Point1 = Begin,
        Segment2,
        Segment3,
        Triangle3,
        Triangle6,
        Quadrangle4,
        Quadrangle8,
        Quadrangle9,
        Tetrahedron4,
        Tetrahedron10,
        Hexahedron8,
        Hexahedron20,
        Hexahedron27,

        // Pyram5, Pyram13, // \todo currently not defined
        // Prism6, Prism15 // \todo currently not defined

        Nref_geom_elt,
        End = Nref_geom_elt, // when iterating, we do not want to consider the placeholder Undefined.
        Undefined
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


namespace std
{


    //! Provide hash function for the enum class
    template<>
    struct hash<MoReFEM::Advanced::GeometricEltEnum>
    {
      public:
        //! Overload.
        //! \param[in] value Item of the \a GeometricEltEnum.
        //! \return Hash value.
        std::size_t operator()(MoReFEM::Advanced::GeometricEltEnum value) const
        {
            return std::hash<int>()(static_cast<int>(value));
        }
    };


} // namespace std


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_GEOMETRICELTENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
