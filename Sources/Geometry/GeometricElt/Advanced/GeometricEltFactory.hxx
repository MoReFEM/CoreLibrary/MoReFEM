// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_GEOMETRICELTFACTORY_DOT_HXX_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_GEOMETRICELTFACTORY_DOT_HXX_
// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
// *** MoReFEM header guards *** < //


// IWYU pragma: no_include "Utilities/InputData/Extract.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "Utilities/Exceptions/Factory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/GeometricElt/Internal/Register.hpp"         // IWYU pragma: keep
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { enum class GeometricEltEnum : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    inline const std::unordered_set<Advanced::GeomEltNS::GenericName>& GeometricEltFactory::GetNameList() const
    {
        return geometric_elt_name_list_;
    }


    template<class RefGeomEltT>
    bool GeometricEltFactory::RegisterGeometricElt(CreateGeometricEltCallBack default_create,
                                                   CreateGeometricEltCallBackIstream ensight_create)
    {

        if (!geometric_elt_name_list_.insert(RefGeomEltT::traits::ClassName()).second)
        {
            auto&& exception =
                ::MoReFEM::ExceptionNS::Factory::UnableToRegister(RefGeomEltT::traits::ClassName().Get(), "name");
            ThrowBeforeMain(std::move(exception));
        }


        if (!callbacks_.insert(make_pair(RefGeomEltT::traits::Identifier(), default_create)).second)
        {
            auto&& exception =
                ::MoReFEM::ExceptionNS::Factory::UnableToRegister(RefGeomEltT::traits::ClassName().Get(), "identifier");
            ThrowBeforeMain(std::move(exception));
        }

        // Fill geom_ref_elt_type_list_.
        {
            auto geom_ref_elt_type = std::make_shared<RefGeomEltT>();

            if (!geom_ref_elt_type_list_.insert(make_pair(RefGeomEltT::traits::Identifier(), geom_ref_elt_type)).second)
            {
                auto&& exception = ::MoReFEM::ExceptionNS::Factory::UnableToRegister(
                    RefGeomEltT::traits::ClassName().Get(), "identifier");
                ThrowBeforeMain(std::move(exception));
            }
        }

        // Register as Ensight geometric element if relevant.
        using support_ensight_type = typename Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,
                                                                                  RefGeomEltT::traits::Identifier()>;

        constexpr const bool do_register_ensight =
            typename Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,
                                                         RefGeomEltT::traits::Identifier()>();

        if constexpr (support_ensight_type())
        {
            if (do_register_ensight)
                Internal::RegisterEnsight<RefGeomEltT>(ensight_create, callbacks_ensight_, ensight_name_matching_);
        }


        // Register as Medit geometric element if relevant.
        using support_medit_type = typename Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit,
                                                                                RefGeomEltT::traits::Identifier()>;


        constexpr const bool do_register_medit =
            typename Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit,
                                                         RefGeomEltT::traits::Identifier()>();

        if constexpr (support_medit_type())
        {
            if (do_register_medit)
                Internal::RegisterMedit<RefGeomEltT>(default_create, callbacks_medit_, Ncoord_medit_);
        }

        static_assert(do_register_ensight || do_register_medit,
                      "At least one format must be associated to a geometric element!"
                      "Specifically, there should be a template specialization of class Internal::MeshNS::Support "
                      "for at least one of the supported format that inherits from std::true_type; this overload "
                      "must be accessible in the RefGeomElt instantiation.");

        // Register the matching between RefGeomElt name and the GeometricEnum.
        if (!match_name_enum_.insert(make_pair(RefGeomEltT::traits::ClassName(), RefGeomEltT::traits::Identifier()))
                 .second)
        {
            auto&& exception =
                ::MoReFEM::ExceptionNS::Factory::UnableToRegister(RefGeomEltT::traits::ClassName().Get(), "name");
            ThrowBeforeMain(std::move(exception));
        }


        return true;
    }


    inline GeometricEltFactory::CallBack::size_type GeometricEltFactory::NgeometricElt() const
    {
        return callbacks_.size();
    }


    inline bool GeometricEltFactory::IsEnsightName(const Advanced::GeomEltNS::EnsightName& name) const
    {
        return callbacks_ensight_.find(name) != callbacks_ensight_.cend();
    }


    inline const GeometricEltFactory::CallBackMedit& GeometricEltFactory::MeditRefGeomEltList() const
    {
        return callbacks_medit_;
    }


    inline auto GeometricEltFactory::NumberOfCoordsInGeometricElt(GmfKwdCod code) const
        -> ::MoReFEM::GeomEltNS::Nlocal_coords_type
    {
        auto it = Ncoord_medit_.find(code);
        assert("If raised something went wrong in geometric elementregistration" && it != Ncoord_medit_.cend());
        return it->second;
    }


    inline const RefGeomElt& GeometricEltFactory ::GetRefGeomElt(Advanced::GeometricEltEnum identifier) const
    {
        auto pointer = GetRefGeomEltPtr(identifier);
        assert(!(!pointer));
        return *pointer;
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_GEOMETRICELTFACTORY_DOT_HXX_
// *** MoReFEM end header guards *** < //
