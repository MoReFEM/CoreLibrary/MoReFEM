// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_SORTINGCRITERION_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_SORTINGCRITERION_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion
{


    //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
    template<class StrictOrderingOperatorT = Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>>
    struct SurfaceRef
    {
        //! Call the appropriate method of \a GeometricElt that returns the label.
        //! \param[in] ptr \a GeometricElt from which dimension (by \a GeometricElt::GetMeshLabelPtr()) will
        //! be extracted.
        inline static MeshLabel::const_shared_ptr Value(const GeometricElt::shared_ptr& ptr);

        //! Operator used for the sorting criterion.
        using StrictOrderingOperator = StrictOrderingOperatorT;
    };


    //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
    template<class StrictOrderingOperatorT = std::less<::MoReFEM::GeometryNS::dimension_type>>
    struct Dimension
    {

        //! Call the appropriate method of \a GeometricElt that returns the dimension.
        //! \param[in] ptr \a GeometricElt from which dimension (by \a GeometricElt::GetDimension()) will
        //! be extracted.
        inline static ::MoReFEM::GeometryNS::dimension_type Value(const GeometricElt::shared_ptr& ptr);

        //! Operator used for the sorting criterion.
        using StrictOrderingOperator = StrictOrderingOperatorT;
    };


    //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
    template<class StrictOrderingOperatorT = std::less<Advanced::GeometricEltEnum>>
    struct Type
    {
        //! Call the appropriate method of GeometricElt that returns the type of geometric element.
        //! \param[in] ptr \a GeometricElt from which type (by \a GeometricElt::GetIdentifier()) will
        //! be extracted.
        inline static Advanced::GeometricEltEnum Value(const GeometricElt::shared_ptr& ptr);

        //! Operator used for the sorting criterion.
        using StrictOrderingOperator = StrictOrderingOperatorT;
    };


} // namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/GeometricElt/Advanced/SortingCriterion.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_SORTINGCRITERION_DOT_HPP_
// *** MoReFEM end header guards *** < //
