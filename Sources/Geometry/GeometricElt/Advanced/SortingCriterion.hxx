// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_SORTINGCRITERION_DOT_HXX_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_SORTINGCRITERION_DOT_HXX_
// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/SortingCriterion.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion
{

    template<class StrictOrderingOperatorT>
    inline MeshLabel::const_shared_ptr SurfaceRef<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
    {
        return ptr->GetMeshLabelPtr();
    }


    template<class StrictOrderingOperatorT>
    inline auto Dimension<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
        -> ::MoReFEM::GeometryNS::dimension_type
    {
        return ptr->GetDimension();
    };


    template<class StrictOrderingOperatorT>
    inline Advanced::GeometricEltEnum Type<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
    {
        return ptr->GetIdentifier();
    }


} // namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_SORTINGCRITERION_DOT_HXX_
// *** MoReFEM end header guards *** < //
