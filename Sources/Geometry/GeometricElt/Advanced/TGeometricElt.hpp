// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_TGEOMETRICELT_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_TGEOMETRICELT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iomanip>

#include "Core/Enum.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"
#include "Geometry/Interfaces/Internal/BuildInterfaceListHelper.hpp"
#include "Geometry/Interfaces/Internal/Orientation/ComputeOrientation.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GeomEltNS
{


    /*!
     * \brief Derived from 'GeometricElt', it defines many virtual methods that depends one way or another from
     * template parameters
     *
     * 'TGeometricElt' stands for templatized geometric element.
     *
     * \tparam TraitsRefGeomEltT A trait class that defines GeoRefElt behaviour.
     */
    template<class TraitsRefGeomEltT>
    class TGeometricElt : public GeometricElt
    {
      public:
        //! Convenient alias.
        using self = TGeometricElt<TraitsRefGeomEltT>;

        /*!
         * \brief Object that store the information about Medit support.
         *
         * This object inherits from std::true_type if Medit is supported, std::false_type otherwise.
         */
        using MeditSupport =
            Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit, TraitsRefGeomEltT::Identifier()>;

        /*!
         * \brief Object that store the information about Ensight support.
         *
         * This object inherits from std::true_type if Ensight is supported, std::false_type otherwise.
         */
        using EnsightSupport =
            Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight, TraitsRefGeomEltT::Identifier()>;


        /// \name Constructors & destructor
        ///@{

        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit TGeometricElt(::MoReFEM::MeshNS::unique_id mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit TGeometricElt(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               std::istream& stream);


        /*!
         * \brief Constructor from vector
         *
         * \param[in] index Index of the geometric element
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         * \copydoc doxygen_hide_geom_elt_minimal_constructor
         * Vector should be the NcoordsT-long
         */
        explicit TGeometricElt(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               ::MoReFEM::GeomEltNS::index_type index,
                               std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_indexes);

        /*!
         * \brief Constructor from vector; index left undefined
         *
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         * \copydoc doxygen_hide_geom_elt_minimal_constructor
         *
         * Index should be specified through dedicated method.
         */
        explicit TGeometricElt(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_indexes);


        //! Destructor
        virtual ~TGeometricElt() override = 0;

        //! \copydoc doxygen_hide_copy_constructor
        TGeometricElt(const TGeometricElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TGeometricElt(TGeometricElt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TGeometricElt& operator=(const TGeometricElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TGeometricElt& operator=(TGeometricElt&& rhs) = delete;


        ///@}


        //! Returns the identifier of the geometric reference element (for instance 'Tria3')
        static constexpr Advanced::GeometricEltEnum Identifier()
        {
            return TraitsRefGeomEltT::Identifier();
        }

        //! Get the identifier of the geometric element.
        virtual Advanced::GeometricEltEnum GetIdentifier() const override final;

        //! Get a reference to the reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override = 0;

        //! Get the name of the geometric element.
        virtual const Advanced::GeomEltNS::GenericName& GetName() const override final;

        //! \copydoc doxygen_hide_geometric_elt_Ncoords_method
        virtual ::MoReFEM::GeomEltNS::Nlocal_coords_type Ncoords() const override final;

        //! Get the number of summits.
        virtual std::size_t Nvertex() const override final;

        //! Get the number of faces.
        virtual std::size_t Nface() const override final;

        //! Get the number of edges.
        virtual std::size_t Nedge() const override final;

        //! Get the dimension of the geometric element.
        virtual ::MoReFEM::GeometryNS::dimension_type GetDimension() const override final;


        //! \name Shape function methods.
        //@{

        //! \copydoc doxygen_hide_shape_function
        virtual double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                     const LocalCoords& local_coords) const override final;

        //! \copydoc doxygen_hide_first_derivate_shape_function
        virtual double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                  ::MoReFEM::GeometryNS::dimension_type component,
                                                  const LocalCoords& local_coords) const override final;

        //! \copydoc doxygen_hide_second_derivate_shape_function
        virtual double SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                   ::MoReFEM::GeometryNS::dimension_type component1,
                                                   ::MoReFEM::GeometryNS::dimension_type component2,
                                                   const LocalCoords& local_coords) const override final;

        //@}


      private:
        /*!
         * \brief Build the list of all vertices that belongs to the geometric element.
         *
         * \param[in,out] existing_list List of all vertice that have been built so far for the enclosing
         * geometric mesh. If a vertex already exists, point to the already existing object instead of
         * recreating one. If not, create a new object from scratch and add it in the existing_list.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         */
        virtual void BuildVertexList(const GeometricElt* geom_elt_ptr,
                                     Vertex::InterfaceMap& existing_list) override final;


        /*!
         * \brief Build the list of all edges that belongs to the geometric element.
         *
         * \param[in,out] existing_list List of all edges that have been built so far for the enclosing
         * geometric mesh. If an edge already exists, point to the already existing object instead of recreating
         * one. If not, create a new object from scratch and add it in the existing_list.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         *
         */
        virtual void BuildEdgeList(const GeometricElt* geom_elt_ptr, Edge::InterfaceMap& existing_list) override final;

        /*!
         * \brief Build the list of all faces that belongs to the geometric element.
         *
         * \param[in,out] existing_list List of all faces that have been built so far for the enclosing
         * geometric mesh. If a face already exists, point to the already existing object instead of recreating
         * one. If not, create a new object from scratch and add it in the existing_list.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         *
         */
        virtual void BuildFaceList(const GeometricElt* geom_elt_ptr, Face::InterfaceMap& existing_list) override final;


        /*!
         * \brief Build the pointer to the volume if there is one to consider as interface.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] existing_list This list is mostly there for genericity, so that same C++ interface
         * might be used for all 4 kind of interfaces. It is not used however as a searching way to see if the
         * interface already exist or not: by nature a Volume interface can appear only once for its related
         * GeometricElt.
         */
        virtual void BuildVolumeList(const GeometricElt* geom_elt_ptr,
                                     Volume::InterfaceMap& existing_list) override final;


      public:
        //! \name Ensight-related methods.
        //@{

        /*!
         * \brief Write the geometric element in Ensight format.
         *
         * \copydoc doxygen_hide_stream_inout
         *
         * \param do_print_index True if the geometric elementis preceded by its internal index.
         */
        virtual void WriteEnsightFormat(std::ostream& stream, bool do_print_index) const override final;
        //@}

      public:
        //! \name Medit-related methods.
        //@{

        /*!
         * \brief Write the geometric element in Medit format.
         *
         * \param[in] mesh_index Index that was returned by Medit when the output file was opened.
         * \param[in] processor_wise_reindexing Medit requires the indexes run from 1 to Ncoord on each
         * processor; this container maps the Coords index to the processor-wise one to write in the Medit file.
         *
         * The method is indeed an underlying call to the Medit API.
         */
        // clang-format off
        virtual void WriteMeditFormat(libmeshb_int mesh_index,
                                      const std::unordered_map
                                      <
                                        ::MoReFEM::CoordsNS::index_from_mesh_file
                                        , int
                                      >& processor_wise_reindexing) const override final;
        // clang-format on


        //! \copydoc doxygen_hide_geometric_elt_read_medit_method
        virtual void ReadMeditFormat(const ::MoReFEM::FilesystemNS::File& medit_file,
                                     const Coords::vector_shared_ptr& mesh_coords_list,
                                     libmeshb_int libmesh_mesh_index,
                                     std::size_t Ncoord_in_mesh,
                                     int& reference_index) override final;

      private:
        /*!
         * \brief Assert at compile time that the number of coordinates in shape functions is consistent
         * with the topological dimension.
         *
         */
        void StaticAssertBuiltInConsistency();
    };


} // namespace MoReFEM::Internal::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/GeometricElt/Advanced/TGeometricElt.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_TGEOMETRICELT_DOT_HPP_
// *** MoReFEM end header guards *** < //
