// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_TGEOMETRICELT_DOT_HXX_
#define MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_TGEOMETRICELT_DOT_HXX_
// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/Wrappers/Libmesh/Libmesh.hpp"


namespace MoReFEM::Internal::GeomEltNS
{


    /// Deactivate noreturn here: it would trigger false positives. )
    PRAGMA_DIAGNOSTIC(push)
    PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")

    template<class TraitsRefGeomEltT>
    TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(::MoReFEM::MeshNS::unique_id mesh_unique_id)
    : GeometricElt(mesh_unique_id)
    {
        StaticAssertBuiltInConsistency();
    }


    template<class TraitsRefGeomEltT>
    TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                                                    const Coords::vector_shared_ptr& mesh_coords_list,
                                                    ::MoReFEM::GeomEltNS::index_type index,
                                                    std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords)
    : GeometricElt(mesh_unique_id, mesh_coords_list, index, std::move(coords))
    {
        StaticAssertBuiltInConsistency();
    }


    template<class TraitsRefGeomEltT>
    TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(
        ::MoReFEM::MeshNS::unique_id mesh_unique_id,
        const Coords::vector_shared_ptr& mesh_coords_list,
        std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list)
    : GeometricElt(mesh_unique_id, mesh_coords_list, std::move(coords_index_list))
    {
        StaticAssertBuiltInConsistency();
    }


    template<class TraitsRefGeomEltT>
    TGeometricElt<TraitsRefGeomEltT>::TGeometricElt(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                                                    const Coords::vector_shared_ptr& mesh_coords_list,
                                                    std::istream& stream)
    : GeometricElt(mesh_unique_id)
    {
        StaticAssertBuiltInConsistency();

        constexpr auto Nlocal_coords_as_size_t = static_cast<std::size_t>(TraitsRefGeomEltT::Ncoords.Get());

        std::vector<::MoReFEM::GeomEltNS::Nlocal_coords_type> coords_list;
        coords_list.reserve(Nlocal_coords_as_size_t);

        for (auto i = 0UL; i < Nlocal_coords_as_size_t; ++i)
        {
            Eigen::Index value;
            stream >> value;
            coords_list.push_back(::MoReFEM::GeomEltNS::Nlocal_coords_type{ value });
        }

        assert(coords_list.size() == Nlocal_coords_as_size_t);

        if (stream)
        {
            std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> coords_index_list(coords_list.size());

            std::transform(coords_list.cbegin(),
                           coords_list.cend(),
                           coords_index_list.begin(),
                           [](auto index)
                           {
                               return ::MoReFEM::CoordsNS::index_from_mesh_file(static_cast<std::size_t>(index.Get()));
                           });

            // Modify geometric element only if failbit not set.
            SetCoordsList(mesh_coords_list, std::move(coords_index_list));
        } else
            throw Exception("Failure to construct a GeometricElt from input stream.");
    }


    template<class TraitsRefGeomEltT>
    TGeometricElt<TraitsRefGeomEltT>::~TGeometricElt() = default;


    template<class TraitsRefGeomEltT>
    Advanced::GeometricEltEnum TGeometricElt<TraitsRefGeomEltT>::GetIdentifier() const
    {
        return TraitsRefGeomEltT::Identifier();
    }


    template<class TraitsRefGeomEltT>
    const Advanced::GeomEltNS::GenericName& TGeometricElt<TraitsRefGeomEltT>::GetName() const
    {
        return TraitsRefGeomEltT::ClassName();
    }


    template<class TraitsRefGeomEltT>
    auto TGeometricElt<TraitsRefGeomEltT>::Ncoords() const -> ::MoReFEM::GeomEltNS::Nlocal_coords_type
    {
        return TraitsRefGeomEltT::Ncoords;
    }


    template<class TraitsRefGeomEltT>
    std::size_t TGeometricElt<TraitsRefGeomEltT>::Nvertex() const
    {
        return TraitsRefGeomEltT::topology::Nvertex;
    }


    template<class TraitsRefGeomEltT>
    std::size_t TGeometricElt<TraitsRefGeomEltT>::Nedge() const
    {
        return TraitsRefGeomEltT::topology::Nedge;
    }


    template<class TraitsRefGeomEltT>
    std::size_t TGeometricElt<TraitsRefGeomEltT>::Nface() const
    {
        return TraitsRefGeomEltT::topology::Nface;
    }


    template<class TraitsRefGeomEltT>
    auto TGeometricElt<TraitsRefGeomEltT>::GetDimension() const -> ::MoReFEM::GeometryNS::dimension_type
    {
        return TraitsRefGeomEltT::topology::dimension;
    }


    template<class TraitsRefGeomEltT>
    inline void TGeometricElt<TraitsRefGeomEltT>::WriteEnsightFormat([[maybe_unused]] std::ostream& stream,
                                                                     [[maybe_unused]] bool do_print_index) const
    {
        if constexpr (!EnsightSupport())
        {
            throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(TraitsRefGeomEltT::ClassName().Get(),
                                                                         "Ensight");
        } else
        {
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            if (do_print_index)
                stream << std::setw(8) << GetIndex();

            decltype(auto) coords_list = GetCoordsList();

            for (const auto& coord_ptr : coords_list)
            {
                assert(!(!coord_ptr));
                stream << std::setw(8) << coord_ptr->GetIndexFromMeshFile();
            }
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            stream << '\n';
        }
    }


    template<class TraitsRefGeomEltT>
    void TGeometricElt<TraitsRefGeomEltT>::WriteMeditFormat(
        [[maybe_unused]] const libmeshb_int mesh_index,
        [[maybe_unused]] const std::unordered_map<::MoReFEM::CoordsNS::index_from_mesh_file, int>&
            processor_wise_reindexing) const
    {
        if constexpr (!MeditSupport())
        {
            throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(TraitsRefGeomEltT::ClassName().Get(), "Medit");
        } else
        {
            auto label_ptr = GetMeshLabelPtr();
            int label_index = (label_ptr ? static_cast<int>(label_ptr->GetIndex().Get()) : 0);

            const auto& coords_list = GetCoordsList();

            // Medit assumes indexes between 1 and Ncoord, whereas MoReFEM starts at 0.
            std::vector<int> coord_index_list;
            coord_index_list.reserve(coords_list.size());

            for (const auto& coord_ptr : coords_list)
            {
                assert(!(!coord_ptr));
                auto it = processor_wise_reindexing.find(coord_ptr->GetIndexFromMeshFile());

                assert(it != processor_wise_reindexing.cend()
                       && "Otherwise processor_wise_reindexing is poorly built.");

                coord_index_list.push_back(it->second);
            }

            ::MoReFEM::Wrappers::Libmesh ::MeditSetLin<TraitsRefGeomEltT::Ncoords.Get()>(
                mesh_index, GetMeditIdentifier(), coord_index_list, label_index);
        }
    }


    template<class TraitsRefGeomEltT>
    void TGeometricElt<TraitsRefGeomEltT>::ReadMeditFormat(
        [[maybe_unused]] const ::MoReFEM::FilesystemNS::File& medit_file,
        [[maybe_unused]] const Coords::vector_shared_ptr& mesh_coords_list,
        [[maybe_unused]] libmeshb_int libmesh_mesh_index,
        [[maybe_unused]] std::size_t Ncoord_in_mesh,
        [[maybe_unused]] int& label_index)
    {
        if constexpr (!MeditSupport())
        {
            throw MoReFEM::ExceptionNS::GeometricElt::FormatNotSupported(TraitsRefGeomEltT::ClassName().Get(), "Medit");
        } else
        {
            std::vector<std::size_t> coords(TraitsRefGeomEltT::Ncoords.Get());

            ::MoReFEM::Wrappers::Libmesh ::MeditGetLin<TraitsRefGeomEltT::Ncoords.Get()>(
                libmesh_mesh_index, GetMeditIdentifier(), coords, label_index);

            // Check here the indexes of the coords are between 1 and Ncoord:
            for (auto coord_index : coords)
            {
                if (coord_index == 0 || coord_index > Ncoord_in_mesh)
                    throw MoReFEM::ExceptionNS::Format::Medit::InvalidCoordIndex(
                        medit_file, coord_index, Ncoord_in_mesh);
            }

            std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> coord_index_list(TraitsRefGeomEltT::Ncoords.Get());

            std::transform(coords.cbegin(),
                           coords.cend(),
                           coord_index_list.begin(),
                           [](const auto index)
                           {
                               return ::MoReFEM::CoordsNS::index_from_mesh_file(index);
                           });

            SetCoordsList(mesh_coords_list, std::move(coord_index_list));
        }
    }


    template<class TraitsRefGeomEltT>
    inline double TGeometricElt<TraitsRefGeomEltT>::ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                  const LocalCoords& local_coords) const
    {
        return TraitsRefGeomEltT::ShapeFunction(local_node_index, local_coords);
    }


    template<class TraitsRefGeomEltT>
    inline double
    TGeometricElt<TraitsRefGeomEltT>::FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                 ::MoReFEM::GeometryNS::dimension_type icoor,
                                                                 const LocalCoords& local_coords) const
    {
        return TraitsRefGeomEltT::FirstDerivateShapeFunction(local_node_index, icoor, local_coords);
    }


    template<class TraitsRefGeomEltT>
    inline double
    TGeometricElt<TraitsRefGeomEltT>::SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                  ::MoReFEM::GeometryNS::dimension_type icoor,
                                                                  ::MoReFEM::GeometryNS::dimension_type jcoor,
                                                                  const LocalCoords& local_coords) const
    {
        return TraitsRefGeomEltT::SecondDerivateShapeFunction(local_node_index, icoor, jcoor, local_coords);
    }


    template<class TraitsRefGeomEltT>
    void TGeometricElt<TraitsRefGeomEltT>::BuildVertexList(const GeometricElt* geom_elt_ptr,
                                                           Vertex::InterfaceMap& existing_list)
    {
        const auto& coords_list = GetCoordsList();

        using Topology = typename TraitsRefGeomEltT::topology;

        auto&& vertex_list =
            Internal::InterfaceNS::Build<Vertex, Topology>::Perform(geom_elt_ptr, coords_list, existing_list);

        SetVertexList(std::move(vertex_list));
    }


    template<class TraitsRefGeomEltT>
    void TGeometricElt<TraitsRefGeomEltT>::BuildEdgeList(const GeometricElt* geom_elt_ptr,
                                                         Edge::InterfaceMap& existing_list)
    {
        using Topology = typename TraitsRefGeomEltT::topology;

        auto&& oriented_edge_list =
            Internal::InterfaceNS::ComputeEdgeList<Topology>(geom_elt_ptr, GetCoordsList(), existing_list);

        SetOrientedEdgeList(std::move(oriented_edge_list));
    }


    template<class TraitsRefGeomEltT>
    void TGeometricElt<TraitsRefGeomEltT>::BuildFaceList(const GeometricElt* geom_elt_ptr,
                                                         Face::InterfaceMap& existing_list)
    {
        using Topology = typename TraitsRefGeomEltT::topology;

        auto&& oriented_face_list =
            Internal::InterfaceNS::ComputeFaceList<Topology>(geom_elt_ptr, GetCoordsList(), existing_list);

        SetOrientedFaceList(std::move(oriented_face_list));
    }


    template<class TraitsRefGeomEltT>
    void TGeometricElt<TraitsRefGeomEltT>::BuildVolumeList(const GeometricElt* geom_elt,
                                                           Volume::InterfaceMap& existing_list)
    {
        Volume::shared_ptr volume_ptr =
            (TraitsRefGeomEltT::topology::Nvolume > 0UL ? std::make_shared<Volume>(shared_from_this()) : nullptr);
        SetVolume(volume_ptr);

        if (!(!volume_ptr))
        {
            assert("One Volume shouldn't be entered twice in the list: it is proper to each GeometricElt."
                   && existing_list.find(volume_ptr) == existing_list.cend());

            std::vector<const GeometricElt*> vector_raw;
            vector_raw.push_back(geom_elt);

            existing_list.insert({ volume_ptr, vector_raw });
        }
    }


    template<class TraitsRefGeomEltT>
    void TGeometricElt<TraitsRefGeomEltT>::StaticAssertBuiltInConsistency()
    {
        static_assert(TraitsRefGeomEltT::Nderivate_component_ == TraitsRefGeomEltT::topology::dimension,
                      "Shape function should be consistent with geometric element!");
    }


    // Reactivate the warning disabled at the beginning of this file.
    PRAGMA_DIAGNOSTIC(pop)


} // namespace MoReFEM::Internal::GeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_ADVANCED_TGEOMETRICELT_DOT_HXX_
// *** MoReFEM end header guards *** < //
