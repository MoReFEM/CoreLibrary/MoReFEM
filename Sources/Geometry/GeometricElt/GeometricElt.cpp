// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM
{


    GeometricElt::GeometricElt(const MeshNS::unique_id mesh_unique_id) : mesh_identifier_(mesh_unique_id)
    { }


    GeometricElt::~GeometricElt() = default;


    // NOLINTBEGIN(cppcoreguidelines-prefer-member-initializer) - false positive when run in Docker?
    // (related to delegated constructor not properly seen by clang-tidy)
    GeometricElt::GeometricElt(const MeshNS::unique_id mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               GeomEltNS::index_type index,
                               std::vector<CoordsNS::index_from_mesh_file>&& coords)
    : GeometricElt(mesh_unique_id)
    {
        index_ = index;
        SetCoordsList(mesh_coords_list, std::move(coords));
    }
    // NOLINTEND(cppcoreguidelines-prefer-member-initializer)


    GeometricElt::GeometricElt(const MeshNS::unique_id mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               std::vector<CoordsNS::index_from_mesh_file>&& coords)
    : GeometricElt(mesh_unique_id,
                   mesh_coords_list,
                   NumericNS::UninitializedIndex<decltype(index_)>(),
                   std::move(coords))
    { }


    void GeometricElt::SetCoordsList(const Coords::vector_shared_ptr& mesh_coords_list,
                                     std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_indexes)
    {
        assert(!mesh_coords_list.empty());
        using difference_type = Coords::vector_shared_ptr::difference_type;

        const auto begin = mesh_coords_list.cbegin();
        const auto end = mesh_coords_list.cend();

        for (auto coord_index : coords_indexes)
        {
            auto match_function = [coord_index](const auto& coord_ptr)
            {
                assert(!(!coord_ptr));
                return coord_ptr->GetIndexFromMeshFile() == coord_index;
            };

            // If we're lucky, the index read from the mesh file is rather sensical and index 'i' is rather close
            // to the 'i'-th position, which shorten considerably the search. Of course, it is absolutely not a given,
            // and we might be prepared for all of them.
            // (for instance currently if we run Ensight format from prepartitioned data we do not have this luck...)
            auto it_begin_guess = begin + static_cast<difference_type>(coord_index.Get()) - 1;
            if (it_begin_guess < begin || it_begin_guess >= end)
                it_begin_guess = begin;

            auto it_end_guess = begin + static_cast<difference_type>(coord_index.Get()) + 2;
            if (it_end_guess > end)
                it_end_guess = end;

            assert(it_begin_guess <= it_end_guess);

            auto it = std::find_if(it_begin_guess, it_end_guess, match_function);

            // If not in the narrow guess range, look for it in the entire container.
            if (it == it_end_guess)
                it = std::find_if(begin, end, match_function);

            assert(it != end);
            assert(*it);
            coords_list_.push_back(*it);
        }
    }


    template<>
    void GeometricElt::BuildInterface<Vertex>(const GeometricElt* geom_elt_ptr, Vertex::InterfaceMap& interface_list)
    {
        BuildVertexList(geom_elt_ptr, interface_list);
    }


    template<>
    void GeometricElt::BuildInterface<Edge>(const GeometricElt* geom_elt_ptr, Edge::InterfaceMap& interface_list)
    {
        BuildEdgeList(geom_elt_ptr, interface_list);
    }


    template<>
    void GeometricElt::BuildInterface<Face>(const GeometricElt* geom_elt_ptr, Face::InterfaceMap& interface_list)
    {
        BuildFaceList(geom_elt_ptr, interface_list);
    }


    template<>
    void GeometricElt::BuildInterface<Volume>(const GeometricElt* geom_elt_ptr, Volume::InterfaceMap& interface_list)
    {
        BuildVolumeList(geom_elt_ptr, interface_list);
    }


    const Advanced::GeomEltNS::EnsightName& GeometricElt::GetEnsightName() const
    {
        return GetRefGeomElt().GetEnsightName();
    }


    GmfKwdCod GeometricElt::GetMeditIdentifier() const
    {
        return GetRefGeomElt().GetMeditIdentifier();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
