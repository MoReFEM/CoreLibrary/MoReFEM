// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_GEOMETRICELT_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_GEOMETRICELT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <concepts>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iosfwd>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Numeric/Numeric.hpp"      // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"       // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"       // IWYU pragma: keep
#include "Geometry/GeometricElt/Index.hpp"                           // IWYU pragma: export
#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"            // IWYU pragma: export
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"            // IWYU pragma: export
#include "Geometry/Interfaces/Instances/Vertex.hpp"                  // IWYU pragma: export
#include "Geometry/Interfaces/Instances/Volume.hpp"                  // IWYU pragma: export
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/StrongType.hpp"                   // IWYU pragma: export
#include "Geometry/StrongType.hpp"
#include "Geometry/StrongType.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::PostProcessingNS { class OutputDeformedMesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{

    /*!
     * \class doxygen_hide_geom_elt_get_index
     *
     * This index is assumed to be unique **for a given Mesh**: no other geometric element should bear
     * the same index (Mesh enforces that when the mesh is read).
     *
     * This uniqueness is important: indexes are used to compare elements, and also to define a hash
     * function when GeometricElt are put in an unordered_map.
     *
     * However, a check upon the mesh is also required: same index might be used by two different meshes for
     * two unrelated geometric elements.
     */


    /*!
     * \brief Generic class handling geometric element.
     *
     * \attention By design a GeometricElt is automatically related to a Mesh object.
     *
     * This abstract class is meant to be used polymorphically: all geometrical geometric elements should derive from
     * this one. As a matter of fact, the dependency is not direct: a class in Internal::MeshNS namespace named
     * TGeometricElt defines all the pure virtual here (TGeometricElt stands for "templatized geometric element").
     *
     * GeometricElt objects are expected to be defined within a mesh context: for instance their index_ is the
     * index of the current GeometricElt is used to label the geometric element within the mesh.
     *
     * If you need to use a type rather than a specific geometric element (for instance in the mesh you have to
     * store in a list the different types of geometric element involved) use rather RefGeomElt.
     */
    class GeometricElt : public std::enable_shared_from_this<GeometricElt>
    {
      public:
        //! Alias for unique pointer.
        using unique_ptr = std::unique_ptr<GeometricElt>;

        //! Alias for shared pointer.
        using shared_ptr = std::shared_ptr<GeometricElt>;

        //! Alias for vector of pointers
        using vector_shared_ptr = std::vector<shared_ptr>;


        //! Friendship to allow call to SetCoordsList().
        friend class PostProcessingNS::OutputDeformedMesh;


        /// \name Special members.
        ///@{

        /*!
         * \class doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         *
         * \param[in] coords_indexes Indexes of the \a Coords to pick from \a mesh_coords_list.
         * \copydoc doxygen_hide_mesh_coords_list_arg
         */


        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit GeometricElt(MeshNS::unique_id mesh_unique_id);

        /*!
         * \brief Constructor from vector.
         *
         * \copydoc doxygen_hide_geom_elt_minimal_constructor
         *
         * \param[in] index Index associated to the geometric element in the mesh.
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         */
        explicit GeometricElt(MeshNS::unique_id mesh_unique_id,
                              const Coords::vector_shared_ptr& mesh_coords_list,
                              GeomEltNS::index_type index,
                              std::vector<CoordsNS::index_from_mesh_file>&& coords_indexes);

        /*!
         * \brief Constructor from vector; index is left undefined.
         *
         * \copydoc doxygen_hide_geom_elt_minimal_constructor
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         *
         * Index should be specified through dedicated method #SetIndex
         */
        explicit GeometricElt(MeshNS::unique_id mesh_unique_id,
                              const Coords::vector_shared_ptr& mesh_coords_list,
                              std::vector<CoordsNS::index_from_mesh_file>&& coords_indexes);

        //! Destructor
        virtual ~GeometricElt();

        //! \copydoc doxygen_hide_copy_constructor
        explicit GeometricElt(const GeometricElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        explicit GeometricElt(GeometricElt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GeometricElt& operator=(const GeometricElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GeometricElt& operator=(GeometricElt&& rhs) = delete;


        ///@}


        /*!
         * \class doxygen_hide_geometric_elt_Ncoords_method
         *
         * \brief Get the number of \a Coords object required to characterize completely a GeometricElt of this type.
         *
         * For instance 27 for an Hexahedron27.
         *
         * \return Number of \a Coords object required to characterize completely a GeometricElt of this type.
         */

        //! \copydoc doxygen_hide_geometric_elt_Ncoords_method
        virtual ::MoReFEM::GeomEltNS::Nlocal_coords_type Ncoords() const = 0;

        //! Get the number of vertice.
        virtual std::size_t Nvertex() const = 0;

        //! Get the number of edges.
        virtual std::size_t Nedge() const = 0;

        //! Get the number of faces.
        virtual std::size_t Nface() const = 0;

        //! Get the dimension of the geometric element.
        virtual GeometryNS::dimension_type GetDimension() const = 0;

        //! Get the identifier of the geometric element.
        virtual Advanced::GeometricEltEnum GetIdentifier() const = 0;

        //! Get a reference to the reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const = 0;

        //! Get the string name of the geometric element (e.g. 'Triangle3', 'Segment2', etc...).
        virtual const Advanced::GeomEltNS::GenericName& GetName() const = 0;

        //! Mutator to the GeometricElt index.
        //!
        //! \param[in] index New index to set.
        void SetIndex(GeomEltNS::index_type index) noexcept;

        //! Accessor to the GeometricElt index.
        //!
        //! \copydetails doxygen_hide_geom_elt_get_index
        inline GeomEltNS::index_type GetIndex() const;

        /*!
         * \brief Get the identifier of the mesh.
         *
         * You shouldn't have to use this: it is used mostly to implement the comparison operators.
         *
         * \return Index that tags the mesh.
         */
        MeshNS::unique_id GetMeshIdentifier() const noexcept;

        //! Set the label.
        //! \param[in] mesh_label \a MeshLabel associated to the current \a GeometricElt.
        void SetMeshLabel(const MeshLabel::const_shared_ptr& mesh_label) noexcept;

        //! Get the label.
        MeshLabel::const_shared_ptr GetMeshLabelPtr() const noexcept;

        //! Get the coords as a vector.
        const Coords::vector_shared_ptr& GetCoordsList() const noexcept;

        //! Get the ith coords index.
        //! \param i Index of the sought \a Coords object used to define the geometric element.
        template<std::integral T>
        const Coords& GetCoord(T i) const;

        /*!
         * \brief Return the list of vertice.
         *
         * \return The list of vertice (as smart pointers).
         */
        const Vertex::vector_shared_ptr& GetVertexList() const noexcept;

        /*!
         * \brief Return the list of edges.
         *
         * This method assumes the edges were already built by the mesh; if not an exception is thrown.
         *
         * \return The list of edges (as smart pointers).
         */
        const OrientedEdge::vector_shared_ptr& GetOrientedEdgeList() const;

        /*!
         * \brief Return the list of faces.
         *
         * This method assumes the faces were already built by the mesh; if not an exception is thrown.
         *
         * \return The list of faces (as smart pointers).
         */
        const OrientedFace::vector_shared_ptr& GetOrientedFaceList() const;

        /*!
         * \brief Return the volume (as an interface).
         *
         * This method assumes the volume were already built by the mesh; if not an exception is thrown.
         *
         * \return The volume interface. Might be nullptr!
         */
        Volume::shared_ptr GetVolumePtr() const;

        /*!
         * \brief Set the list of vertice.
         *
         * \param[in] vertex_list List of \a Vertex related to the geometric element.
         */
        void SetVertexList(Vertex::vector_shared_ptr&& vertex_list);

        /*!
         * \brief Set the list of edges (with their orientation).
         *
         * \param[in] edge_list List of \a OrientedEdge related to the geometric element.
         */
        void SetOrientedEdgeList(OrientedEdge::vector_shared_ptr&& edge_list);

        /*!
         * \brief Set the list of faces (with their orientation).
         *
         * \param[in] face_list List of \a OrientedFace related to the geometric element.
         */
        void SetOrientedFaceList(OrientedFace::vector_shared_ptr&& face_list);

        /*!
         * \brief Set the volume interface.
         *
         * \param[in] volume New value to set.
         *
         */
        void SetVolume(const Volume::shared_ptr& volume);


        /*!
         * \class doxygen_hide_geom_elt_build_interface_ptr
         *
         * \param[in] geom_elt_ptr Pointer to current \a GeometricElt; must point to same object as this. This is
         * rather clumsy, but I lack the time currently to fix something that is clumsy but ultimately not broken.
         */


        /*!
         * \brief Build a new interface, regardless of the orientation.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] interface_list List of \a InterfaceT under construction when current method is called.
         * If interface is not yet there it is added at the end of the method call.
         */
        template<class InterfaceT>
        void BuildInterface(const GeometricElt* geom_elt_ptr, typename InterfaceT::InterfaceMap& interface_list);


      public:
        //! \name Shape function methods.
        //@{

        //! \copydoc doxygen_hide_shape_function
        virtual double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                     const LocalCoords& local_coords) const = 0;

        //! \copydoc doxygen_hide_first_derivate_shape_function
        virtual double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                  ::MoReFEM::GeometryNS::dimension_type component,
                                                  const LocalCoords& local_coords) const = 0;

        //! \copydoc doxygen_hide_second_derivate_shape_function
        virtual double SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                   ::MoReFEM::GeometryNS::dimension_type component1,
                                                   ::MoReFEM::GeometryNS::dimension_type component2,
                                                   const LocalCoords& local_coords) const = 0;

        //@}


      private:
        /*!
         * \brief Build the list of all vertices that belongs to the geometric element.
         *
         * \param[in,out] existing_list List of all vertice that have been built so far for the enclosing geometric
         * mesh. If a vertex already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         *
         */
        virtual void BuildVertexList(const GeometricElt* geom_elt_ptr, Vertex::InterfaceMap& existing_list) = 0;


        /*!
         * \brief Build the list of all edges that belongs to the geometric element.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] existing_list List of all edges that have been built so far for the enclosing geometric
         * mesh. If an edge already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         *
         */
        virtual void BuildEdgeList(const GeometricElt* geom_elt_ptr, Edge::InterfaceMap& existing_list) = 0;


        /*!
         * \brief Build the list of all faces that belongs to the geometric element.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] existing_list List of all faces that have been built so far for the enclosing geometric
         * mesh. If a face already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         *
         */
        virtual void BuildFaceList(const GeometricElt* geom_elt_ptr, Face::InterfaceMap& existing_list) = 0;


        /*!
         * \brief Build the list of all faces that belongs to the geometric element.
         *
         * \copydoc doxygen_hide_geom_elt_build_interface_ptr
         * \param[in,out] existing_list List of all faces that have been built so far for the enclosing geometric
         * mesh. If a face already exists, point to the already existing object instead of recreating one.
         * If not, create a new object from scratch and add it in the existing_list.
         *
         * \internal <b><tt>[internal]</tt></b> As there is at most one volume by GeometricElt, and this volume can't
         * therefore be shared with others geometric elements, we could have handled it differently from other
         * interfaces. But the little efficiency we could have gained would have to be paid with maintenance cost;
         * hence the choice to use a same mechanism for all the interfaces.
         * \endinternal
         */
        virtual void BuildVolumeList(const GeometricElt* geom_elt_ptr, Volume::InterfaceMap& existing_list) = 0;


      public:
        //! Ensight-related methods.
        //@{

        /*!
         * \brief Get the ensight name of the geometric element (e.g. 'tria3', 'bar2', etc...)
         *
         * An exception is thrown if Medit format is not supported.
         *
         * \return Ensight name (acts as an identifier for Ensight).
         */
        const Advanced::GeomEltNS::EnsightName& GetEnsightName() const;


        /*!
         * \brief Write the geometric element in Ensight format.
         *
         * \copydoc doxygen_hide_stream_inout
         * \param[in] do_print_index True if the geometric element is preceded by its internal index.
         */
        virtual void WriteEnsightFormat(std::ostream& stream, bool do_print_index) const = 0;

        //@}


      public:
        //! Medit-related methods.
        //@{

        /*!
         * \brief Get the identifier Medit use to tag the geometric element.
         *
         * An exception is thrown if Medit format is not supported.
         *
         * \return Identifier.
         */
        GmfKwdCod GetMeditIdentifier() const;


        /*!
         * \brief Write the geometric elementin Medit format.
         *
         * \param[in] mesh_index Index that was returned by Medit when the output file was opened.
         * \param[in] processor_wise_reindexing Medit requires the indexes run from 1 to Ncoord on each processor;
         * this container maps the Coords index to the processor-wise one to write in the Medit file.
         *
         * The method is indeed an underlying call to the Medit API.
         */
        // clang-format off
        virtual void WriteMeditFormat(libmeshb_int mesh_index,
                                      const std::unordered_map
                                      <
                                        ::MoReFEM::CoordsNS::index_from_mesh_file
                                        , int
                                      >& processor_wise_reindexing) const = 0;
        // clang-format on


        /*!
         * \class doxygen_hide_geometric_elt_read_medit_method
         *
         * \brief Read the geometric element in Medit format.
         *
         * \param[in] libmesh_mesh_index Index that was returned by Medit when the input file was opened.
         * \param[in] Ncoord_in_mesh Total number of coord in the mesh. Used for a check upon file validity.
         * \param[out] reference_index Reference of the label, as libmesh int index. The calling function has the
         * responsibility to use (or not) this value and Set the label pointer in GeometricElt object.
         * \copydoc doxygen_hide_mesh_coords_list_arg
         * \param[in] medit_file File being read.
         *
         * The method is indeed an underlying call to the Medit API.
         */

        //! \copydoc doxygen_hide_geometric_elt_read_medit_method

        virtual void ReadMeditFormat(const FilesystemNS::File& medit_file,
                                     const Coords::vector_shared_ptr& mesh_coords_list,
                                     libmeshb_int libmesh_mesh_index,
                                     std::size_t Ncoord_in_mesh,
                                     int& reference_index) = 0;

        //@}

      protected:
        /*!
         * \brief Set the coord list from the vector given in input.
         *
         * \copydoc doxygen_hide_geom_elt_mesh_coords_list_and_coord_indexes_constructor_arg
         *
         * Beware: an error will be raised if the input vector size is not what is expected !
         */
        void SetCoordsList(const Coords::vector_shared_ptr& mesh_coords_list,
                           std::vector<CoordsNS::index_from_mesh_file>&& coords_indexes);


        /*!
         * \brief Tells the object edges or faces have been built.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         */
        template<InterfaceNS::Nature InterfaceT>
        void SetInterfaceListBuilt();

      private:
        /*!
         * \brief Identifier of the mesh.
         */
        const MeshNS::unique_id mesh_identifier_;

        /*!
         * \brief Index associated to the current geometric element.
         *
         * \copydetails doxygen_hide_geom_elt_get_index
         */
        GeomEltNS::index_type index_ = NumericNS::UninitializedIndex<decltype(index_)>();

        //! Surface reference to which the geometric element belongs to.
        MeshLabel::const_shared_ptr mesh_label_ = nullptr;

        //! Coords that belong to the geometric element.
        Coords::vector_shared_ptr coords_list_;

        //! List of vertice.
        Vertex::vector_shared_ptr vertex_list_;

        //! List of edges.
        OrientedEdge::vector_shared_ptr oriented_edge_list_;

        //! List of faces.
        OrientedFace::vector_shared_ptr oriented_face_list_;

        //! Volume (as an interface).
        Volume::shared_ptr volume_ = nullptr;

        /*!
         * \brief Knows whether a given type of interface has been built or not.
         */
        InterfaceNS::Nature higher_interface_type_built_ = InterfaceNS::Nature::undefined;
    };


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * The underlying index is the criterion used here: two geometric elements are equal if their index is the same.
     */
    bool operator==(const GeometricElt& lhs, const GeometricElt& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * As for operator==, underlying index is used to perform the comparison.
     */
    bool operator<(const GeometricElt& lhs, const GeometricElt& rhs) noexcept;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

namespace std
{


    //! Provide hash function for GeometricElt::shared_ptr.
    template<>
    struct hash<MoReFEM::GeometricElt::shared_ptr>
    {
      public:
        //! Overload.
        //! \param[in] ptr Pointer to a \a GeometricElt.
        //! \return Hash value.
        std::size_t operator()(const MoReFEM::GeometricElt::shared_ptr& ptr) const
        {
            assert(!(!ptr));
            return std::hash<std::size_t>()(ptr->GetIndex().Get());
        }
    };


} // namespace std


#include "Geometry/GeometricElt/GeometricElt.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_GEOMETRICELT_DOT_HPP_
// *** MoReFEM end header guards *** < //
