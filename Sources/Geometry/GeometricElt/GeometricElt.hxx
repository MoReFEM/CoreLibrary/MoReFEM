// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_GEOMETRICELT_DOT_HXX_
#define MOREFEM_GEOMETRY_GEOMETRICELT_GEOMETRICELT_DOT_HXX_
// IWYU pragma: private, include "Geometry/GeometricElt/GeometricElt.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"


namespace MoReFEM
{


    inline const Coords::vector_shared_ptr& GeometricElt::GetCoordsList() const noexcept
    {
        return coords_list_;
    }


    inline GeomEltNS::index_type GeometricElt::GetIndex() const
    {
        if (index_ == NumericNS::UninitializedIndex<decltype(index_)>())
            throw Exception("GeometricElt not properly initialized: index got default value");

        return index_;
    }


    inline MeshNS::unique_id GeometricElt::GetMeshIdentifier() const noexcept
    {
        assert(mesh_identifier_ != NumericNS::UninitializedIndex<decltype(mesh_identifier_)>());
        return mesh_identifier_;
    }


    inline const Vertex::vector_shared_ptr& GeometricElt::GetVertexList() const noexcept
    {
        return vertex_list_;
    }


    inline const OrientedEdge::vector_shared_ptr& GeometricElt::GetOrientedEdgeList() const
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::edge)
            throw ExceptionNS::GeometricElt::InterfaceTypeNotBuilt("edge");

        return oriented_edge_list_;
    }


    inline const OrientedFace::vector_shared_ptr& GeometricElt::GetOrientedFaceList() const
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::face)
            throw ExceptionNS::GeometricElt::InterfaceTypeNotBuilt("face");

        return oriented_face_list_;
    }


    inline Volume::shared_ptr GeometricElt::GetVolumePtr() const
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::volume)
            throw ExceptionNS::GeometricElt::InterfaceTypeNotBuilt("volume");

        if (GetDimension() < GeometryNS::dimension_type{ 3 })
            return nullptr;

        assert(!(!volume_));

        return volume_;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::vertex>()
    {
        higher_interface_type_built_ = InterfaceNS::Nature::vertex;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::edge>()
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::vertex)
            throw ExceptionNS::GeometricElt::InvalidInterfaceBuildOrder("edge", "vertex");

        higher_interface_type_built_ = InterfaceNS::Nature::edge;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::face>()
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::edge)
            throw ExceptionNS::GeometricElt::InvalidInterfaceBuildOrder("face", "edge");

        higher_interface_type_built_ = InterfaceNS::Nature::face;
    }


    template<>
    inline void GeometricElt::SetInterfaceListBuilt<InterfaceNS::Nature::volume>()
    {
        if (higher_interface_type_built_ < InterfaceNS::Nature::face)
            throw ExceptionNS::GeometricElt::InvalidInterfaceBuildOrder("volume", "face");

        higher_interface_type_built_ = InterfaceNS::Nature::volume;
    }


    inline void GeometricElt::SetVertexList(Vertex::vector_shared_ptr&& vertex_list)
    {
        vertex_list_ = std::move(vertex_list);
        SetInterfaceListBuilt<InterfaceNS::Nature::vertex>();
    }


    inline void GeometricElt::SetOrientedEdgeList(OrientedEdge::vector_shared_ptr&& oriented_edge_list)
    {
        oriented_edge_list_ = std::move(oriented_edge_list);
        SetInterfaceListBuilt<InterfaceNS::Nature::edge>();
    }


    inline void GeometricElt::SetOrientedFaceList(OrientedFace::vector_shared_ptr&& face_list)
    {
        oriented_face_list_ = std::move(face_list);
        SetInterfaceListBuilt<InterfaceNS::Nature::face>();
    }


    inline void GeometricElt::SetVolume(const Volume::shared_ptr& volume_ptr)
    {
        volume_ = volume_ptr; // might be nullptr
        SetInterfaceListBuilt<InterfaceNS::Nature::volume>();
    }


    inline void GeometricElt::SetMeshLabel(const MeshLabel::const_shared_ptr& label) noexcept
    {
        mesh_label_ = label;
    }


    inline MeshLabel::const_shared_ptr GeometricElt::GetMeshLabelPtr() const noexcept
    {
        return mesh_label_;
    }


    inline void GeometricElt::SetIndex(GeomEltNS::index_type index) noexcept
    {
        index_ = index;
    }


    inline bool operator<(const GeometricElt& element1, const GeometricElt& element2) noexcept
    {
        assert(element1.GetMeshIdentifier() == element2.GetMeshIdentifier()
               && "At the moment I consider it doesn't make sense to compare GeometricElt from two "
                  "different Mesh objects.");

        return element1.GetIndex() < element2.GetIndex();
    }


    inline bool operator==(const GeometricElt& element1, const GeometricElt& element2) noexcept
    {
        assert(element1.GetMeshIdentifier() == element2.GetMeshIdentifier()
               && "At the moment I consider it doesn't make sense to compare GeometricElt from two "
                  "different Mesh objects.");

        return element1.GetIndex() == element2.GetIndex();
    }


    template<std::integral T>
    inline const Coords& GeometricElt::GetCoord(T i) const
    {
        assert(static_cast<std::size_t>(i) < coords_list_.size());
        assert(!(!coords_list_[static_cast<std::size_t>(i)]));
        return *coords_list_[static_cast<std::size_t>(i)];
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_GEOMETRICELT_DOT_HXX_
// *** MoReFEM end header guards *** < //
