// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_FWDFORHPP_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_FWDFORHPP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>  // IWYU pragma: export
#include <iosfwd> // IWYU pragma: export
#include <map>    // IWYU pragma: export
#include <vector> // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"                          // IWYU pragma: export
#include "Geometry/Coords/StrongType.hpp"                      // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export                     // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"                     // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"                        // IWYU pragma: export
#include "Geometry/Interfaces/Advanced/LocalData.hpp"                              // IWYU pragma: export
#include "Geometry/Interfaces/EnumInterface.hpp"                                   // IWYU pragma: export
#include "Geometry/Interfaces/Instances/Edge.hpp"                                  // IWYU pragma: export
#include "Geometry/Interfaces/Instances/Face.hpp"                                  // IWYU pragma: export
#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"                          // IWYU pragma: export
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"                          // IWYU pragma: export
#include "Geometry/Interfaces/Interface.hpp"                                       // IWYU pragma: export
#include "Geometry/Interfaces/Internal/TInterface.hpp"                             // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"         // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp" // IWYU pragma: export

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_FWDFORHPP_DOT_HPP_
// *** MoReFEM end header guards *** < //
