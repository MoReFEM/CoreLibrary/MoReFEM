// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_HEXAHEDRON_HEXAHEDRON20_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_HEXAHEDRON_HEXAHEDRON20_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"
#include "Geometry/RefGeometricElt/Instances/Hexahedron/Traits/Hexahedron20.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced::RefGeomEltNS { class Hexahedron20; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    /*!
     * \brief A Hexahedron20 geometric element read in a mesh.
     *
     * We are not considering here a generic Hexahedron20 object (that's the role of
     * MoReFEM::Advanced::RefGeomEltNS::Hexahedron20), but rather a specific geometric element of the mesh, with for
     * instance the coordinates of its coord, the list of its interfaces and so forth.
     */
    class Hexahedron20 final : public Internal::GeomEltNS::TGeometricElt<Advanced::RefGeomEltNS::Traits::Hexahedron20>
    {
      public:
        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Hexahedron20(MeshNS::unique_id mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Hexahedron20(MeshNS::unique_id mesh_unique_id,
                              const Coords::vector_shared_ptr& mesh_coords_list,
                              std::istream& stream);

        //! \copydoc doxygen_hide_geom_elt_vector_coords_constructor
        explicit Hexahedron20(MeshNS::unique_id mesh_unique_id,
                              const Coords::vector_shared_ptr& mesh_coords_list,
                              std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list);

        //! Destructor.
        ~Hexahedron20() override;

        //! \copydoc doxygen_hide_copy_constructor
        Hexahedron20(const Hexahedron20& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Hexahedron20(Hexahedron20&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Hexahedron20& operator=(const Hexahedron20& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Hexahedron20& operator=(Hexahedron20&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

      private:
        //! Reference geometric element.
        static const Advanced::RefGeomEltNS::Hexahedron20& StaticRefGeomElt();
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_HEXAHEDRON_HEXAHEDRON20_DOT_HPP_
// *** MoReFEM end header guards *** < //
