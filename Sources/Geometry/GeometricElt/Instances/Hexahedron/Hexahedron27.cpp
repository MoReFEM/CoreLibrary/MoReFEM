// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
#include <iostream>
#include <utility>

#include "Geometry/GeometricElt/Instances/Hexahedron/Hexahedron27.hpp"

#include "Utilities/Warnings/Pragma.hpp"

#include "Geometry/GeometricElt/Instances/FwdForCpp.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/Instances/Hexahedron/Format/Hexahedron27.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Hexahedron/Hexahedron27.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // The anonymous namespace is extremely important here: function is invisible from other modules
    namespace // anonymous
    {


        auto CreateMinimal(MeshNS::unique_id mesh_unique_id)
        {
            return std::make_unique<Hexahedron27>(mesh_unique_id);
        }


        // Register the geometric element in the 'GeometricEltFactory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        // NOLINTBEGIN(cert-err58-cpp)
        // We accept here exception might occur and not be caught - it is both very unlikely
        // and would happen anyway before we enter the main() program if it happens nonetheless.
        [[maybe_unused]] const bool registered =
            Advanced::GeometricEltFactory::CreateOrGetInstance()
                .RegisterGeometricElt<Advanced::RefGeomEltNS::Hexahedron27>(CreateMinimal, nullptr);
        // NOLINTEND(cert-err58-cpp)


    } // anonymous namespace


    PRAGMA_DIAGNOSTIC(push)
    PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")

    Hexahedron27::Hexahedron27(MeshNS::unique_id mesh_unique_id)
    : Internal::GeomEltNS::TGeometricElt<Advanced::RefGeomEltNS::Traits::Hexahedron27>(mesh_unique_id)
    {
        static const bool is_first_call = true;

        if (is_first_call)
        {
            const ExceptionNS::GeometricElt::UnsupportedP2Q2 exception;
            std::cerr << "[WARNING] " << exception.GetRawMessage() << '\n';
        }
    }


    Hexahedron27::Hexahedron27(MeshNS::unique_id mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               std::istream& stream)
    : Internal::GeomEltNS::TGeometricElt<Advanced::RefGeomEltNS::Traits::Hexahedron27>(mesh_unique_id,
                                                                                       mesh_coords_list,
                                                                                       stream)
    {
        static const bool is_first_call = true;

        if (is_first_call)
        {
            const ExceptionNS::GeometricElt::UnsupportedP2Q2 exception;
            std::cerr << "[WARNING] " << exception.GetRawMessage() << '\n';
        }
    }


    Hexahedron27::Hexahedron27(MeshNS::unique_id mesh_unique_id,
                               const Coords::vector_shared_ptr& mesh_coords_list,
                               std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list)
    : Internal::GeomEltNS::TGeometricElt<Advanced::RefGeomEltNS::Traits::Hexahedron27>(mesh_unique_id,
                                                                                       mesh_coords_list,
                                                                                       std::move(coords_index_list))
    {
        static const bool is_first_call = true;

        if (is_first_call)
        {
            const ExceptionNS::GeometricElt::UnsupportedP2Q2 exception;
            std::cerr << "[WARNING] " << exception.GetRawMessage() << '\n';
        }
    }

    PRAGMA_DIAGNOSTIC(pop)


    Hexahedron27::~Hexahedron27() = default;


    const RefGeomElt& Hexahedron27::GetRefGeomElt() const
    {
        return StaticRefGeomElt();
    }


    const Advanced::RefGeomEltNS::Hexahedron27& Hexahedron27::StaticRefGeomElt()
    {
        static const Advanced::RefGeomEltNS::Hexahedron27 ret;
        return ret;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
