// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <utility>

#include "Geometry/GeometricElt/Instances/Triangle/Triangle3.hpp"

#include "Geometry/GeometricElt/Instances/FwdForCpp.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Format/Triangle3.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Triangle/Traits/Triangle3.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Triangle3.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // The anonymous namespace is extremely important here: function is invisible from other modules
    namespace // anonymous
    {


        auto CreateEnsight(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                           const Coords::vector_shared_ptr& mesh_coords_list,
                           std::istream& in)
        {
            return std::make_unique<Triangle3>(mesh_unique_id, mesh_coords_list, in);
        }


        auto CreateMinimal(::MoReFEM::MeshNS::unique_id mesh_unique_id)
        {
            return std::make_unique<Triangle3>(mesh_unique_id);
        }


        // Register the geometric element in the 'GeometricEltFactory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        // NOLINTBEGIN(cert-err58-cpp)
        // We accept here exception might occur and not be caught - it is both very unlikely
        // and would happen anyway before we enter the main() program if it happens nonetheless.
        [[maybe_unused]] const bool registered =
            Advanced::GeometricEltFactory::CreateOrGetInstance()
                .RegisterGeometricElt<Advanced::RefGeomEltNS::Triangle3>(CreateMinimal, CreateEnsight);
        // NOLINTEND(cert-err58-cpp)


    } // namespace


    Triangle3::Triangle3(MeshNS::unique_id mesh_unique_id)
    : Internal::GeomEltNS::TGeometricElt<Advanced::RefGeomEltNS::Traits::Triangle3>(mesh_unique_id)
    { }


    Triangle3::Triangle3(MeshNS::unique_id mesh_unique_id,
                         const Coords::vector_shared_ptr& mesh_coords_list,
                         std::istream& stream)
    : TGeometricElt<Advanced::RefGeomEltNS::Traits::Triangle3>(mesh_unique_id, mesh_coords_list, stream)
    { }


    Triangle3::Triangle3(MeshNS::unique_id mesh_unique_id,
                         const Coords::vector_shared_ptr& mesh_coords_list,
                         std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list)
    : TGeometricElt<Advanced::RefGeomEltNS::Traits::Triangle3>(mesh_unique_id,
                                                               mesh_coords_list,
                                                               std::move(coords_index_list))
    { }


    Triangle3::~Triangle3() = default;


    const RefGeomElt& Triangle3::GetRefGeomElt() const
    {
        return StaticRefGeomElt();
    }


    const Advanced::RefGeomEltNS::Triangle3& Triangle3::StaticRefGeomElt()
    {
        static const Advanced::RefGeomEltNS::Triangle3 ret;
        return ret;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
