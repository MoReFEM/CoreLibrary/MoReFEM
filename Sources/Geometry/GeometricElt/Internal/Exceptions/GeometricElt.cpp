// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cctype>
#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"


namespace // anonymous
{


    std::string FormatNotSupportedMsg(const std::string& geometric_elt_identifier, const std::string& format);

    std::string InterfaceTypeNotBuiltMsg(const std::string& edge_or_face);

    std::string InvalidInterfaceBuildOrderMsg(std::string&& attempted_type, std::string&& missing_type);

    std::string Global2LocalNoConvergenceMsg();

    std::string UnsupportedP2Q2Msg();


} // namespace


namespace MoReFEM::ExceptionNS::GeometricElt
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    FormatNotSupported::~FormatNotSupported() = default;


    FormatNotSupported::FormatNotSupported(const std::string& geometric_elt_identifier,
                                           const std::string& format,
                                           const std::source_location location)
    : Exception(FormatNotSupportedMsg(geometric_elt_identifier, format), location)
    { }


    InterfaceTypeNotBuilt::~InterfaceTypeNotBuilt() = default;


    InterfaceTypeNotBuilt::InterfaceTypeNotBuilt(const std::string& edge_or_face, const std::source_location location)
    : Exception(InterfaceTypeNotBuiltMsg(edge_or_face), location)
    { }


    InvalidInterfaceBuildOrder::~InvalidInterfaceBuildOrder() = default;


    InvalidInterfaceBuildOrder::InvalidInterfaceBuildOrder(std::string&& attempted_type,
                                                           std::string&& missing_type,
                                                           const std::source_location location)
    : Exception(InvalidInterfaceBuildOrderMsg(std::move(attempted_type), std::move(missing_type)), location)
    { }


    Global2LocalNoConvergence::~Global2LocalNoConvergence() = default;


    Global2LocalNoConvergence::Global2LocalNoConvergence(const std::source_location location)
    : Exception(Global2LocalNoConvergenceMsg(), location)
    { }


    UnsupportedP2Q2::~UnsupportedP2Q2() = default;


    UnsupportedP2Q2::UnsupportedP2Q2(const std::source_location location) : Exception(UnsupportedP2Q2Msg(), location)
    { }


} // namespace MoReFEM::ExceptionNS::GeometricElt


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file

    std::string FormatNotSupportedMsg(const std::string& geometric_elt_identifier, const std::string& format)
    {
        std::ostringstream oconv;
        oconv << "GeometricElt " << geometric_elt_identifier << " is not supported by " << format << " format";

        return oconv.str();
    }


    std::string InterfaceTypeNotBuiltMsg(const std::string& edge_or_face)
    {
        std::ostringstream oconv;

        assert(!edge_or_face.empty());
        std::string with_upper_case(edge_or_face);
        with_upper_case[0] = static_cast<char>(toupper(with_upper_case[0]));

        oconv << with_upper_case << "s not properly built; a call to Mesh::Build" << with_upper_case
              << "List() "
                 "is probably missing.";

        return oconv.str();
    }


    std::string InvalidInterfaceBuildOrderMsg(std::string&& attempted_type, std::string&& missing_type)
    {
        std::ostringstream oconv;
        oconv << "You attempted to build the list of " << attempted_type
              << " while the "
                 "list of "
              << missing_type << " was not built.";

        return oconv.str();
    }


    std::string Global2LocalNoConvergenceMsg()
    {
        return "No convergence was reached in the Newton algorithm used to find out local coordinates of a mesh point.";
    }


    std::string UnsupportedP2Q2Msg()
    {
        return "Currently, P2/Q2 geometry is not yet supported. Should be done in 2023! "
               "(followed in https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/248).";
    }


} // namespace
