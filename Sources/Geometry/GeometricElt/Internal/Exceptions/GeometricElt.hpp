// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_EXCEPTIONS_GEOMETRICELT_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_EXCEPTIONS_GEOMETRICELT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::ExceptionNS::GeometricElt
{


    //! Generic class for GeometricElt exceptions.
    class Exception : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    /*!
     * \brief Thrown when a geometric element is not supported by the chosen format.
     *
     * For instance some geometric elements are recognized by Ensight and not by Medit (and vice-versa).
     * So for instance if you read an Ensight mesh with a Quadrangle8 and then attempts to write the mesh
     * as a Medit one, this exception will be thrown as Medit can't cope with it.
     */
    class FormatNotSupported final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] geometric_elt_identifier String that identifies the kind of geometric elementconsidered
         (eg 'Triangle3')
         * \param[in] format Name of the format considered  ('Ensight' or 'Medit' currently)
         * \copydoc doxygen_hide_source_location

         */
        explicit FormatNotSupported(const std::string& geometric_elt_identifier,
                                    const std::string& format,
                                    const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~FormatNotSupported() override;

        //! \copydoc doxygen_hide_copy_constructor
        FormatNotSupported(const FormatNotSupported& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        FormatNotSupported(FormatNotSupported&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        FormatNotSupported& operator=(const FormatNotSupported& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        FormatNotSupported& operator=(FormatNotSupported&& rhs) = default;
    };


    /*!
     * \brief Thrown when edges or faces are requested whereas they weren't properly built.
     *
     * They are not built automatically, so user must have called Mesh::BuildEdgeList() or
     * Mesh::BuildFaceList() beforehand.
     */
    class InterfaceTypeNotBuilt final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] edge_or_face "edge" or "face"
         * \copydoc doxygen_hide_source_location

         */
        explicit InterfaceTypeNotBuilt(const std::string& edge_or_face,
                                       const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InterfaceTypeNotBuilt() override;

        //! \copydoc doxygen_hide_copy_constructor
        InterfaceTypeNotBuilt(const InterfaceTypeNotBuilt& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InterfaceTypeNotBuilt(InterfaceTypeNotBuilt&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InterfaceTypeNotBuilt& operator=(const InterfaceTypeNotBuilt& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InterfaceTypeNotBuilt& operator=(InterfaceTypeNotBuilt&& rhs) = default;
    };


    /*!
     * \brief Thrown when you attempt to build an interface type while not having built first the type just
     * below.
     *
     * For instance in order to build faces you must have already built edges.
     */
    class InvalidInterfaceBuildOrder final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] attempted_type Type which build was required by the developer.
         * \param[in] missing_type Type that ought to be already built to build \a attempted_type.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidInterfaceBuildOrder(std::string&& attempted_type,
                                            std::string&& missing_type,
                                            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidInterfaceBuildOrder() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidInterfaceBuildOrder(const InvalidInterfaceBuildOrder& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidInterfaceBuildOrder(InvalidInterfaceBuildOrder&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidInterfaceBuildOrder& operator=(const InvalidInterfaceBuildOrder& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidInterfaceBuildOrder& operator=(InvalidInterfaceBuildOrder&& rhs) = default;
    };


    /*!
     * \brief Thrown when no convergence was reached in Global2Local.
     *
     */
    class Global2LocalNoConvergence : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         *\copydoc doxygen_hide_source_location
         *
         */
        explicit Global2LocalNoConvergence(const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Global2LocalNoConvergence() override;

        //! \copydoc doxygen_hide_copy_constructor
        Global2LocalNoConvergence(const Global2LocalNoConvergence& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Global2LocalNoConvergence(Global2LocalNoConvergence&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Global2LocalNoConvergence& operator=(const Global2LocalNoConvergence& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Global2LocalNoConvergence& operator=(Global2LocalNoConvergence&& rhs) = default;
    };


    /*!
     * \brief Temporary exception to handle the fact P2/Q2 geometry is not supported yet.
     *
     */
    class UnsupportedP2Q2 : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         *\copydoc doxygen_hide_source_location
         *
         */
        explicit UnsupportedP2Q2(const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~UnsupportedP2Q2() override;

        //! \copydoc doxygen_hide_copy_constructor
        UnsupportedP2Q2(const UnsupportedP2Q2& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnsupportedP2Q2(UnsupportedP2Q2&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnsupportedP2Q2& operator=(const UnsupportedP2Q2& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnsupportedP2Q2& operator=(UnsupportedP2Q2&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::GeometricElt


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_EXCEPTIONS_GEOMETRICELT_DOT_HPP_
// *** MoReFEM end header guards *** < //
