// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <source_location>
#include <sstream>
#include <string>

#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"

#include "Utilities/Exceptions/Factory.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricEltFactory.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string InvalidEnsightGeometricEltNameMsg(const MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name);

    std::string InvalidGeometricEltIdMsg(MoReFEM::Advanced::GeometricEltEnum identifier);

    std::string InvalidGeometricEltNameMsg(const MoReFEM::Advanced::GeomEltNS::GenericName& name);


} // namespace


namespace MoReFEM::ExceptionNS::Factory::GeometricElt
{

    Exception::Exception(const std::string& msg, const std::source_location location)
    : ::MoReFEM::ExceptionNS::Factory::Exception(msg, location)
    { }


    Exception::~Exception() = default;


    InvalidEnsightGeometricEltName ::InvalidEnsightGeometricEltName(
        const Advanced::GeomEltNS::EnsightName& ensight_name,
        const std::source_location location)
    : Exception(InvalidEnsightGeometricEltNameMsg(ensight_name), location)
    { }


    InvalidEnsightGeometricEltName::~InvalidEnsightGeometricEltName() = default;


    InvalidGeometricEltId::InvalidGeometricEltId(Advanced::GeometricEltEnum identifier,
                                                 const std::source_location location)
    : Exception(InvalidGeometricEltIdMsg(identifier), location)
    { }

    InvalidGeometricEltId::~InvalidGeometricEltId() = default;


    InvalidGeometricEltName::InvalidGeometricEltName(const Advanced::GeomEltNS::GenericName& name,
                                                     const std::source_location location)
    : Exception(InvalidGeometricEltNameMsg(name), location)
    { }


    InvalidGeometricEltName::~InvalidGeometricEltName() = default;


} // namespace MoReFEM::ExceptionNS::Factory::GeometricElt


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file


    std::string InvalidEnsightGeometricEltNameMsg(const MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name)
    {
        std::ostringstream oconv;
        oconv << "Unavailable Ensight geometric element name (\"";
        oconv << geometric_elt_name << "\"); possible choices are: ";
        oconv << MoReFEM::Advanced::GeometricEltFactory::GetInstance().EnsightGeometricEltNames();
        return oconv.str();
    }


    std::string InvalidGeometricEltIdMsg(MoReFEM::Advanced::GeometricEltEnum identifier)
    {
        std::ostringstream oconv;
        oconv << "Unavailable geometric element identifier (\"";
        oconv << static_cast<std::size_t>(identifier) << "\")";
        return oconv.str();
    }


    std::string InvalidGeometricEltNameMsg(const MoReFEM::Advanced::GeomEltNS::GenericName& name)
    {
        std::ostringstream oconv;
        oconv << "Unavailable geometric element name (\"";
        oconv << name << "\")";
        return oconv.str();
    }


} // namespace
