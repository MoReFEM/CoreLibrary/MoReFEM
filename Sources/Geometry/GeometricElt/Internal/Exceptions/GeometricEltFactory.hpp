// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_EXCEPTIONS_GEOMETRICELTFACTORY_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_EXCEPTIONS_GEOMETRICELTFACTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Factory.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced { enum class GeometricEltEnum : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::ExceptionNS::Factory::GeometricElt
{


    // Use some of the Factory Exception classes already defined in Utilities.
    using ::MoReFEM::ExceptionNS::Factory::UnableToRegister;


    //! Generic exception for GeometricEltFactory.
    class Exception : public ::MoReFEM::ExceptionNS::Factory::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location

         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Thrown when the user requires from the factory an object with an unknown Ensight name (which acts as
    //! an identifier).
    class InvalidEnsightGeometricEltName final : public Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] ensight_name Attempted Ensight name of a \a RefGeomElt from which a \a GeometricElt
         * should have been built.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidEnsightGeometricEltName(const Advanced::GeomEltNS::EnsightName& ensight_name,
                                                const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~InvalidEnsightGeometricEltName() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidEnsightGeometricEltName(const InvalidEnsightGeometricEltName& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidEnsightGeometricEltName(InvalidEnsightGeometricEltName&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidEnsightGeometricEltName& operator=(const InvalidEnsightGeometricEltName& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidEnsightGeometricEltName& operator=(InvalidEnsightGeometricEltName&& rhs) = default;
    };


    /*!
     * \brief Thrown when the user requires from the factory an object with an unknown identifier.
     *
     * Said identifier is one given by the class itself and is not related to a specific IO format.
     */
    class InvalidGeometricEltId final : public Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] identifier Attempted identifier of a \a RefGeomElt from which a \a GeometricElt
         * should have been built.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidGeometricEltId(Advanced::GeometricEltEnum identifier,
                                       const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~InvalidGeometricEltId() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidGeometricEltId(const InvalidGeometricEltId& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidGeometricEltId(InvalidGeometricEltId&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidGeometricEltId& operator=(const InvalidGeometricEltId& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidGeometricEltId& operator=(InvalidGeometricEltId&& rhs) = default;
    };


    /*!
     * \brief Thrown when the user requires from the factory an object with an unknown name.
     *
     * Said identifier is one given by the class itself and is not related to a specific IO format.
     */
    class InvalidGeometricEltName final : public Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] ref_geom_elt_name Attempted name of a \a RefGeomElt from which a \a GeometricElt
         * should have been built.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidGeometricEltName(const Advanced::GeomEltNS::GenericName& ref_geom_elt_name,
                                         const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~InvalidGeometricEltName() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidGeometricEltName(const InvalidGeometricEltName& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidGeometricEltName(InvalidGeometricEltName&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidGeometricEltName& operator=(const InvalidGeometricEltName& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidGeometricEltName& operator=(InvalidGeometricEltName&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::Factory::GeometricElt


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_EXCEPTIONS_GEOMETRICELTFACTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
