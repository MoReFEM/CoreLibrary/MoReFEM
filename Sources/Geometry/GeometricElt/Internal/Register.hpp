// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_REGISTER_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_REGISTER_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "Utilities/Exceptions/Factory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { enum class GeometricEltEnum : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    /*!
     * \brief Function called to register a \a RefGeometricElt which supports Ensight format.
     *
     * It should not be called directly and is intended to be used only in \a
     * GeometricEltFactory::RegisterGeometricElt() method.
     *
     * \tparam RefGeomEltT Type of geometric element we want to add to the \a GeometricEltFactory.
     *
     * \param[in] ensight_create Functor which creates a \a GeometricElt from data read in an input stream.
     * \param[in,out] call_back_ensight Associative container to choose the right function given a Ensight geometric element name.
     * Key is the Ensight name for the geometric element. For instance "tria3" for 'Triangle3'.
     * Value is the function used to create an object of the correct type, eg 'Triangle3' in our example.
     * \param[in,out] ensight_name_matching A match between Ensight name and \a GeometricEltEnum.
     */
    template<class RefGeomElt>
    void RegisterEnsight(
        Advanced::GeometricEltFactory::CreateGeometricEltCallBackIstream ensight_create,
        Advanced::GeometricEltFactory::CallBackEnsight& call_back_ensight,
        std::unordered_map<Advanced::GeomEltNS::EnsightName, Advanced::GeometricEltEnum>& ensight_name_matching);


    /*!
     * \brief Function called to register a \a RefGeometricElt which supports Medit format.
     *
     * It should not be called directly and is intended to be used only in \a
     * GeometricEltFactory::RegisterGeometricElt() method.
     *
     * \tparam RefGeomEltT Type of geometric element we want to add to the \a GeometricEltFactory.
     *
     * \param[in] default_create Functor which creates a \a GeometricElt from a unique id given as input argument.
     * \param[in,out] call_back_medit Associative container to choose the right function given a Medit geometric element name.
     * Key is Medit identifier of a geometric element.
     * Value is a pointer to a function that will create the chosen geometric element (by calling constructor
     * without argument).
     * \param[in] Nnode_medit The \a Ncoords value stored in the traits class of a \a RefGeomElt
     * (e.g. in Geometry/RefGeometricElt/Instances/Hexahedron/Traits/Hexahedron27.hpp)
     */
    template<class RefGeomEltT>
    void RegisterMedit(Advanced::GeometricEltFactory::CreateGeometricEltCallBack default_create,
                       Advanced::GeometricEltFactory::CallBackMedit& call_back_medit,
                       std::map<GmfKwdCod, ::MoReFEM::GeomEltNS::Nlocal_coords_type>& Nnode_medit);


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/GeometricElt/Internal/Register.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_REGISTER_DOT_HPP_
// *** MoReFEM end header guards *** < //
