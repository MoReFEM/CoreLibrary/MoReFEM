// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_REGISTER_DOT_HXX_
#define MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_REGISTER_DOT_HXX_
// IWYU pragma: private, include "Geometry/GeometricElt/Internal/Register.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "Utilities/Exceptions/Factory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { enum class GeometricEltEnum : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    template<class GeometricEltT>
    void RegisterEnsight(
        Advanced::GeometricEltFactory::CreateGeometricEltCallBackIstream ensight_create,
        Advanced::GeometricEltFactory::CallBackEnsight& call_back_ensight,
        std::unordered_map<Advanced::GeomEltNS::EnsightName, Advanced::GeometricEltEnum>& ensight_name_matching)

    {
        decltype(auto) ensight_name =
            Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,
                                                GeometricEltT::traits::Identifier()>::EnsightName();


        if (!ensight_create || !call_back_ensight.insert(std::make_pair(ensight_name, ensight_create)).second)
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(ensight_name.Get(), "ensight"));

        [[maybe_unused]] auto [it, inserted] =
            ensight_name_matching.insert(std::pair(ensight_name, GeometricEltT::traits::Identifier()));

        assert(inserted);
    }


    template<class GeometricEltT>
    void RegisterMedit(Advanced::GeometricEltFactory::CreateGeometricEltCallBack default_create,
                       Advanced::GeometricEltFactory::CallBackMedit& call_back_medit,
                       std::map<GmfKwdCod, ::MoReFEM::GeomEltNS::Nlocal_coords_type>& Nnode_medit)
    {
        auto medit_id = Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit,
                                                            GeometricEltT::traits::Identifier()>::MeditId();

        if (!call_back_medit.insert(std::make_pair(medit_id, default_create)).second)
            ThrowBeforeMain(
                ExceptionNS::Factory::UnableToRegister(GeometricEltT::traits::ClassName().Get(), "medit_id"));

        const auto Ncoords = GeometricEltT::traits::Ncoords;

        if (!Nnode_medit.insert(std::make_pair(medit_id, Ncoords)).second)
            ThrowBeforeMain(
                ExceptionNS::Factory::UnableToRegister(GeometricEltT::traits::ClassName().Get(), "medit_id"));
    }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_INTERNAL_REGISTER_DOT_HXX_
// *** MoReFEM end header guards *** < //
