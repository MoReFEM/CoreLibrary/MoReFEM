// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_ADVANCED_LOCALDATA_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_ADVANCED_LOCALDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <sstream>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/LocalInterface/Advanced/LocalInterface.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::Advanced::InterfaceNS
{

    /*!
     * \brief This helper class is used to access the local edges, faces and also the vertice coordinates.
     *
     * It must be used as follow:
     * \code
     * const auto& topology_face = Internal::InterfaceNS::LocalData<TopologyT>::GetFace(local_face_index);
     * \endcode
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    struct LocalData
    {

        /*!
         * brief Yields a local interface object related to the given \a local_vertex_index vertex of \a
         * TopologyT.
         *
         * \tparam IntegerT Type of the index.
         *
         * \param[in] local_vertex_index Local index of the vertex considered.
         *
         * \return Local interface object.
         */
        template<class IntegerT>
        static LocalInterface ComputeLocalVertexInterface(IntegerT local_vertex_index);


        /*!
         * \brief Provide access to the \a local_edge_index edge of \a TopologyT.
         *
         * \tparam IntegerT Type of the index.
         * \param[in] local_edge_index Local index of the edge considered.
         *
         * \return Edge content of the edge, i.e. the vertices that defines it.
         */
        template<class IntegerT>
            requires ::MoReFEM::Advanced::Concept::TopologyNS::with_edge<TopologyT>
        static auto GetEdge(IntegerT local_edge_index) -> const typename TopologyT::EdgeContent&;

        /*!
         * brief Yields a local interface object related to the given \a local_edge_index edge of \a TopologyT.
         *
         * \param[in] local_edge_index Local index of the edge considered.
         * \tparam IntegerT Type of the index.
         *
         * \return Local interface object.
         */
        template<class IntegerT>
            requires ::MoReFEM::Advanced::Concept::TopologyNS::with_edge<TopologyT>
        static LocalInterface ComputeLocalEdgeInterface(IntegerT local_edge_index);

        /*!
         * \brief Returns the number of vertices in the edge.
         *
         * \param[in] local_edge_index Local index of the edge considered.
         *
         * We can't simply take the size() of \a GetEdge(local_edge_index) as the latter might be
         * std::false_type.
         *
         * \return Number of vertices in the edge.
         */
        template<class IntegerT>
            requires ::MoReFEM::Advanced::Concept::TopologyNS::with_edge<TopologyT>
        static std::size_t NverticeInEdge(IntegerT local_edge_index);


        /*!
         * \brief Provide access to the \a local_face_index face of \a TopologyT.
         *
         * \param[in] local_face_index Local index of the face considered.
         *
         * \return Face content of the face, i.e. the vertices that defines it.
         */
        template<class IntegerT>
            requires ::MoReFEM::Advanced::Concept::TopologyNS::with_face<TopologyT>
        static auto GetFace(IntegerT local_face_index) -> const typename TopologyT::FaceContent&;


        /*!
         * brief Yields a local interface object related to the given \a local_face_index edge of \a TopologyT.
         *
         * \tparam IntegerT Type of the index.
         *
         * \param[in] local_face_index Local index of the face considered.
         *
         * \return Local interface object.
         */
        template<class IntegerT>
            requires ::MoReFEM::Advanced::Concept::TopologyNS::with_face<TopologyT>
        static LocalInterface ComputeLocalFaceInterface(IntegerT local_face_index);


        /*!
         * \brief Provide access to the LocalCoords of the vertices of \a TopologyT.
         *
         * \param[in] local_vertex_index Local index of the vertex considered.
         *
         * \return \a LocalCoords matching the vertex pointed by \a local_vertex_index
         */
        template<class IntegerT>
        static const LocalCoords& GetVertexCoord(IntegerT local_vertex_index);


        /*!
         * \brief Yields a local interface object related to the interior of \a TopologyT
         *
         * \return Local interface object.
         */
        static LocalInterface ComputeLocalInteriorInterface();

        /*!
         * \brief Yields the number of interface of a given \a nature.
         *
         * For instance Nelement(Nature::vertex) yields 4 for a quadrangle.
         *
         * \param[in] nature Nature of the interface considered.
         *
         * \return Number of interfaces of type \a nature.
         */
        static std::size_t Nelement(::MoReFEM::InterfaceNS::Nature nature) noexcept;


        /*!
         * \brief Returns a local interface of type \a nature upon which the \a local_coords is located.
         *
         * Throw an exception if none matches.
         *
         * \param[in] nature Nature of the interface considered.
         * \param[in] local_coords LocalCoords for which a match is sought.
         *
         * \return Local interface of type \a nature onto which \a local_coords may be found.
         */
        static LocalInterface FindLocalInterface(::MoReFEM::InterfaceNS::Nature nature,
                                                 const LocalCoords& local_coords);
    };


} // namespace MoReFEM::Advanced::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Advanced/LocalData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_ADVANCED_LOCALDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
