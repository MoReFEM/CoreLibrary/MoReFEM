// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_ADVANCED_LOCALDATA_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_ADVANCED_LOCALDATA_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Advanced/LocalData.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::Advanced::InterfaceNS
{


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    template<class IntegerT>
    LocalInterface LocalData<TopologyT>::ComputeLocalVertexInterface(IntegerT local_vertex_index)
    {
        static_assert(std::is_integral<IntegerT>::value, "Template argument is expected to be an integral type!");

        assert(local_vertex_index < TopologyT::Nvertex);

        std::array<std::size_t, 1> interface_content{ { local_vertex_index } };

        return LocalInterface(interface_content, ::MoReFEM::InterfaceNS::Nature::vertex, IsInterior::no);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    template<class IntegerT>
        requires ::MoReFEM::Advanced::Concept::TopologyNS::with_edge<TopologyT>
    inline auto LocalData<TopologyT>::GetEdge(IntegerT local_edge_index) -> const typename TopologyT::EdgeContent&
    {
        static_assert(std::is_integral<IntegerT>::value, "Template argument is expected to be an integral type!");

        const auto& local_edge_list = TopologyT::GetEdgeList();

        const auto size_t_index = local_edge_index;
        assert(size_t_index < local_edge_list.size());

        return local_edge_list[size_t_index];
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    template<class IntegerT>
        requires ::MoReFEM::Advanced::Concept::TopologyNS::with_edge<TopologyT>
    LocalInterface LocalData<TopologyT>::ComputeLocalEdgeInterface(IntegerT local_edge_index)
    {
        static_assert(std::is_integral<IntegerT>::value, "Template argument is expected to be an integral type!");

        return LocalInterface(GetEdge(local_edge_index), ::MoReFEM::InterfaceNS::Nature::edge, IsInterior::no);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    template<class IntegerT>
        requires ::MoReFEM::Advanced::Concept::TopologyNS::with_edge<TopologyT>
    std::size_t LocalData<TopologyT>::NverticeInEdge(IntegerT local_edge_index)
    {
        static_assert(std::is_integral<IntegerT>::value, "Template argument is expected to be an integral type!");

        return GetEdge(local_edge_index).size();
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    template<class IntegerT>
        requires ::MoReFEM::Advanced::Concept::TopologyNS::with_face<TopologyT>
    inline auto LocalData<TopologyT>::GetFace(IntegerT local_face_index) -> const typename TopologyT::FaceContent&
    {
        static_assert(std::is_integral<IntegerT>::value, "Template argument is expected to be an integral type!");

        const auto& local_face_list = TopologyT::GetFaceList();
        const auto size_t_index = local_face_index;
        assert(size_t_index < local_face_list.size());

        return local_face_list[size_t_index];
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    template<class IntegerT>
        requires ::MoReFEM::Advanced::Concept::TopologyNS::with_face<TopologyT>
    LocalInterface LocalData<TopologyT>::ComputeLocalFaceInterface(IntegerT local_face_index)
    {
        static_assert(std::is_integral<IntegerT>::value, "Template argument is expected to be an integral type!");

        return LocalInterface(GetFace(local_face_index), ::MoReFEM::InterfaceNS::Nature::face, IsInterior::no);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    template<class IntegerT>
    const LocalCoords& LocalData<TopologyT>::GetVertexCoord(IntegerT local_vertex_index)
    {
        static_assert(std::is_integral<IntegerT>::value, "Template argument is expected to be an integral type!");

        const auto& local_vertex_coord_list = TopologyT::GetQ1LocalCoordsList();

        const auto size_t_index = local_vertex_index;
        assert(size_t_index < local_vertex_coord_list.size());

        return local_vertex_coord_list[size_t_index];
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    LocalInterface LocalData<TopologyT>::ComputeLocalInteriorInterface()
    {
        std::array<std::size_t, 0> empty;

        return LocalInterface(empty, TopologyT::GetInteriorInterface(), IsInterior::yes);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    std::size_t LocalData<TopologyT>::Nelement(::MoReFEM::InterfaceNS::Nature nature) noexcept
    {
        switch (nature)
        {
        case ::MoReFEM::InterfaceNS::Nature::vertex:
            return TopologyT::Nvertex;
        case ::MoReFEM::InterfaceNS::Nature::edge:
            return TopologyT::Nedge;
        case ::MoReFEM::InterfaceNS::Nature::face:
            return TopologyT::Nface;
        case ::MoReFEM::InterfaceNS::Nature::volume:
            return TopologyT::Nvolume;
        case ::MoReFEM::InterfaceNS::Nature::none:
        case ::MoReFEM::InterfaceNS::Nature::undefined:
            assert(false);
            break;
        }

        assert(false);
        return NumericNS::UninitializedIndex<std::size_t>();
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    LocalInterface LocalData<TopologyT>::FindLocalInterface(::MoReFEM::InterfaceNS::Nature nature,
                                                            const LocalCoords& local_coords)
    {
        switch (nature)
        {
        case ::MoReFEM::InterfaceNS::Nature::vertex:
        {
            for (std::size_t i = 0UL; i < TopologyT::Nvertex; ++i)
            {
                if (TopologyT::IsOnVertex(i, local_coords))
                    return ComputeLocalVertexInterface(i);
            }
            break;
        }
        case ::MoReFEM::InterfaceNS::Nature::edge:
        {
            if constexpr (Advanced::Concept::TopologyNS::with_edge<TopologyT>)
            {
                for (std::size_t i = 0UL; i < TopologyT::Nedge; ++i)
                {
                    if (TopologyT::IsOnEdge(i, local_coords))
                        return ComputeLocalEdgeInterface(i);
                }
            } else
            {
                assert(false && "Edge nature shouldn't be reached for topology without edge");
                exit(EXIT_FAILURE);
            }
            break;
        }
        case ::MoReFEM::InterfaceNS::Nature::face:
        {
            if constexpr (Advanced::Concept::TopologyNS::with_face<TopologyT>)
            {
                for (std::size_t i = 0UL; i < TopologyT::Nface; ++i)
                {
                    if (TopologyT::IsOnFace(i, local_coords))
                        return ComputeLocalFaceInterface(i);
                }
            } else
            {
                assert(false && "Edge nature shouldn't be reached for topology without face");
                exit(EXIT_FAILURE);
            }

            break;
        }
        case ::MoReFEM::InterfaceNS::Nature::volume:
        {
            if (TopologyT::IsInside(local_coords))
                return ComputeLocalInteriorInterface();
            break;
        }
        case ::MoReFEM::InterfaceNS::Nature::none:
        case ::MoReFEM::InterfaceNS::Nature::undefined:
            break;
        }

        std::ostringstream oconv;
        oconv << "Local coords " << local_coords << " couldn't be associated to any interface of type " << nature
              << '.';

        throw Exception(oconv.str());
    }


} // namespace MoReFEM::Advanced::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_ADVANCED_LOCALDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
