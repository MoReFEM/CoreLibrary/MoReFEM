// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_ADVANCED_ORIENTATION_STRONGTYPE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_ADVANCED_ORIENTATION_STRONGTYPE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

// IWYU pragma: begin_exports
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::Advanced::InterfaceNS
{


    //! Strong type for orientation.
    //! \copydetails doxygen_hide_strong_type_quick_explanation
    // clang-format off
    using orientation_type =
        StrongType
        <
            std::size_t,
            struct orientation_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::Advanced::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_ADVANCED_ORIENTATION_STRONGTYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
