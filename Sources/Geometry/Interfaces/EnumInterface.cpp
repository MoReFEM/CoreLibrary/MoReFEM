// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <iostream>
#include <map>
#include <string>
#include <utility>

#include "Geometry/Interfaces/EnumInterface.hpp"


namespace // anonymous
{


    // Match a string name to each interface nature.
    const std::map<MoReFEM::InterfaceNS::Nature, std::string>& InterfaceNameMap()
    {
        using namespace MoReFEM::InterfaceNS;

        static const std::map<Nature, std::string> ret{ {
            { Nature::vertex, "Vertex" },
            { Nature::edge, "Edge" },
            { Nature::face, "Face" },
            { Nature::volume, "Volume" },
            // undefined intentionally left out!
        } };

        return ret;
    }


} // namespace


namespace MoReFEM::InterfaceNS
{


    Nature GetNature(const std::string& interface_name)
    {
        const auto& interface_name_map = InterfaceNameMap();

        for (const auto& [nature, name] : interface_name_map)
        {
            if (name == interface_name)
                return nature;
        }

        assert(false && "Was called for an interface_name that didn't match an actual interface!");
        return Nature::undefined;
    }


    std::ostream& operator<<(std::ostream& stream, Nature rhs)
    {
        switch (rhs)
        {
        case MoReFEM::InterfaceNS::Nature::vertex:
        case MoReFEM::InterfaceNS::Nature::edge:
        case MoReFEM::InterfaceNS::Nature::face:
        case MoReFEM::InterfaceNS::Nature::volume:
        {
            const auto& interface_name_map = InterfaceNameMap();

            auto it = interface_name_map.find(rhs);
            assert(it != interface_name_map.cend());
            stream << it->second;
            break;
        }
        case MoReFEM::InterfaceNS::Nature::none:
        case MoReFEM::InterfaceNS::Nature::undefined:
            assert(false);
            break;
        }

        return stream;
    }

} // namespace MoReFEM::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
