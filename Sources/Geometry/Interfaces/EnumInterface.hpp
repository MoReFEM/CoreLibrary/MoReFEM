// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_ENUMINTERFACE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_ENUMINTERFACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::InterfaceNS
{


    /*!
     * \brief Nature of the interface that might be considered.
     */
    enum class Nature : std::size_t { none = 0, vertex, edge, face, volume, undefined };


    /*!
     * \brief Helper class to return at compile time the number of interfaces of \a NatureT for a given \a
     * TopologyT.
     *
     * \tparam TopologyT Topology for which the number of interfaces is requested.
     * \tparam NatureT Nature of the interface.
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT, Nature NatureT>
    struct Ninterface
    {

        //! Number of intefaces of \a NatureT for \a TopologyT.
        static constexpr std::size_t Value();
    };


    /*!
     * \brief Returns the nature of an interface from its name.
     *
     * If the name doesn't match any interface, Nature::undefined is returned (and an assert is raised in debug).
     * \param[in] interface_name Name of the interface.
     *
     * \return Nature of the interface.
     */
    Nature GetNature(const std::string& interface_name);


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     */
    std::ostream& operator<<(std::ostream& stream, Nature rhs);


} // namespace MoReFEM::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/EnumInterface.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_ENUMINTERFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
