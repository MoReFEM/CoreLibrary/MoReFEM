// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_ENUMINTERFACE_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_ENUMINTERFACE_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/EnumInterface.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterfaceNS
{


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT, Nature NatureT>
    constexpr std::size_t Ninterface<TopologyT, NatureT>::Value()
    {
        return NatureT == Nature::vertex
                   ? TopologyT::Nvertex
                   : (NatureT == Nature::edge ? TopologyT::Nedge
                                              : (NatureT == Nature::face ? TopologyT::Nface : TopologyT::Nvolume));

        // Note: I personnally like better the more verbosy form below, but gcc can't cope with it (whereas
        // to my knowledge it's perfectly acceptable C++ 14).
        //            switch (NatureT)
        //            {
        //                case Nature::vertex:
        //                    return TopologyT::Nvertex;
        //                case Nature::edge:
        //                    return TopologyT::Nedge;
        //                case Nature::face:
        //                    return TopologyT::Nface;
        //                case Nature::volume:
        //                    return TopologyT::Nvolume;
        //            }
        //
        //            assert(false);
        //            return NumericNS::UninitializedIndex<std::size_t>();
    }


} // namespace MoReFEM::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_ENUMINTERFACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
