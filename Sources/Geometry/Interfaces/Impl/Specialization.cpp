// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Interface.hpp"

// Keep the space here: don't like it but Interface.hpp needs to be included first,
// and clang-format will shuffle the ordering.
#include "Geometry/Interfaces/Impl/Specialization.hpp"


namespace MoReFEM::InterfaceNS
{


    std::size_t Hash::operator()(const Interface* const interface_ptr) const
    {
        assert(!(!interface_ptr));
        const auto& interface = *interface_ptr;

        std::size_t ret = std::hash<std::size_t>()(EnumUnderlyingType(interface.GetNature()));

        Utilities::HashCombine(ret, interface.GetProgramWiseIndex());

        return ret;
    }


    bool LessByCoords::operator()(const Interface::shared_ptr& lhs, const Interface::shared_ptr& rhs) const
    {
        assert(!(!lhs));
        assert(!(!rhs));
        assert(lhs->GetNature() == rhs->GetNature());

        const auto& lhs_coord = lhs->GetCoordsList();
        const auto& rhs_coord = rhs->GetCoordsList();
        const auto lhs_size = lhs_coord.size();
        const auto rhs_size = rhs_coord.size();

        if (lhs_size != rhs_size)
            return lhs_size < rhs_size;

        for (std::size_t i = 0UL; i < lhs_size; ++i)
        {
            auto lhs_item = lhs_coord[i];
            auto rhs_item = rhs_coord[i];

            if (lhs_item != rhs_item)
                return *lhs_item < *rhs_item;
        }

        // If there they are strictly identical, natural answer is false.
        return false;
    }


} // namespace MoReFEM::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
