// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/Interfaces/Instances/Edge.hpp" // IWYU pragma: associated

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM
{


    const std::string& Edge::ClassName()
    {
        static const std::string ret("Edge");
        return ret;
    }


    Edge::~Edge() = default;


    void Edge::ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list)
    {
        if (!parent::IsPseudoNormalSet())
        {
            const std::size_t geom_elt_list_size = geom_elt_list.size();

            auto& pseudo_normal = parent::SetPseudoNormal();

            std::size_t counter_faces = 0;

            for (std::size_t i = 0; i < geom_elt_list_size; ++i)
            {
                const auto& geom_elt = *geom_elt_list[i];

                if (geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3)
                {
                    const auto& face_list = geom_elt.GetOrientedFaceList();
                    const std::size_t face_list_size = face_list.size();

                    if (face_list_size > 1)
                        throw Exception("A triangle has more than one face. This should not happen during "
                                        "pseudo-normals computation.");

                    assert(face_list.size() == 1);
                    const auto& face = *face_list[0];

                    const auto& pseudo_normal_face_ptr = face.GetUnorientedInterface().GetPseudoNormalPtr();

                    if (pseudo_normal_face_ptr != nullptr)
                    {
                        pseudo_normal += *pseudo_normal_face_ptr;
                        ++counter_faces;
                    }
                }
            }

            const bool check_face =
                false; // #877 Here the mesh Radek gave me does not respect that, ie the surface mesh is not correct
            // but I need to check with him about it, see in matlab the routine to extract the part of the mesh.

            if (counter_faces > 2 && check_face)
            {
                for (std::size_t i = 0; i < geom_elt_list_size; ++i)
                {
                    const auto& geom_elt = *geom_elt_list[i];

                    std::cout << "Label " << geom_elt.GetMeshLabelPtr()->GetIndex() << '\n';

                    std::cout << "Index " << geom_elt.GetIndex() << '\n';

                    const auto& coords_list = geom_elt.GetCoordsList();

                    const std::size_t coords_list_size = coords_list.size();

                    for (std::size_t j = 0; j < coords_list_size; ++j)
                    {
                        const auto& coord = *coords_list[j];

                        coord.ExtendedPrint(std::cout);
                        std::cout << '\n';
                    }
                }
                throw Exception("An edge is shared by more than two faces. This should not happen during "
                                "pseudo-normals computation.\n"
                                "Number of faces "
                                + std::to_string(counter_faces) + ".");
            }

            const double norm = std::sqrt(pseudo_normal(0) * pseudo_normal(0) + pseudo_normal(1) * pseudo_normal(1)
                                          + pseudo_normal(2) * pseudo_normal(2));

            if (NumericNS::IsZero(norm))
                throw Exception(
                    "The norm of a pseudo normal on an edge is zero. This should not happen if the mesh is correct.");

            const double one_over_norm = 1. / norm;

            pseudo_normal *= one_over_norm;
        }
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
