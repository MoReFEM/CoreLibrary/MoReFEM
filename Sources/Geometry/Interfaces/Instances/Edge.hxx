// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INSTANCES_EDGE_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INSTANCES_EDGE_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Instances/Edge.hpp"
// *** MoReFEM header guards *** < //


#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/TInterface.hpp"

// IWYU pragma: no_forward_declare MoReFEM::Edge


namespace MoReFEM
{


    template<class LocalEdgeContentT>
    Edge::Edge(const LocalEdgeContentT& local_edge_content, const Coords::vector_shared_ptr& elt_coords_list)
    : Internal::InterfaceNS::TInterface<Edge, InterfaceNS::Nature::edge>(local_edge_content, elt_coords_list)
    { }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INSTANCES_EDGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
