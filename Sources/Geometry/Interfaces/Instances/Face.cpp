// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/Interfaces/Instances/Face.hpp" // IWYU pragma: associated

#include "Geometry/Coords/SpatialPoint.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM
{


    const std::string& Face::ClassName()
    {
        static const std::string ret("Face");
        return ret;
    }


    Face::~Face() = default;


    void Face::ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list)
    {
        if (!parent::IsPseudoNormalSet())
        {
            auto& pseudo_normal = parent::SetPseudoNormal();

            const std::size_t geom_elt_list_size = geom_elt_list.size();

            int counter_tetra = 0;

            const auto& face_coords_list = GetCoordsList();

            SpatialPoint barycenter_of_face;
            SpatialPoint tetra_coord_last_point;

            const std::size_t face_coords_list_size = face_coords_list.size();

            for (std::size_t i = 0; i < geom_elt_list_size; ++i)
            {
                const auto& geom_elt = *geom_elt_list[i];

                if (geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3)
                {
                    const auto& geom_elt_coords_list = geom_elt.GetCoordsList();

                    // Compute barycenter.
                    const double x_bary =
                        (geom_elt_coords_list[0]->x() + geom_elt_coords_list[1]->x() + geom_elt_coords_list[2]->x())
                        / 3.;
                    const double y_bary =
                        (geom_elt_coords_list[0]->y() + geom_elt_coords_list[1]->y() + geom_elt_coords_list[2]->y())
                        / 3.;
                    const double z_bary =
                        (geom_elt_coords_list[0]->z() + geom_elt_coords_list[1]->z() + geom_elt_coords_list[2]->z())
                        / 3.;

                    barycenter_of_face.GetNonCstValue(GeometryNS::dimension_type{ 0 }) = x_bary;
                    barycenter_of_face.GetNonCstValue(GeometryNS::dimension_type{ 1 }) = y_bary;
                    barycenter_of_face.GetNonCstValue(GeometryNS::dimension_type{ 2 }) = z_bary;

                    // Vector 12
                    const double x = geom_elt_coords_list[1]->x() - geom_elt_coords_list[0]->x();
                    const double y = geom_elt_coords_list[1]->y() - geom_elt_coords_list[0]->y();
                    const double z = geom_elt_coords_list[1]->z() - geom_elt_coords_list[0]->z();

                    // Vector 13
                    const double u = geom_elt_coords_list[2]->x() - geom_elt_coords_list[0]->x();
                    const double v = geom_elt_coords_list[2]->y() - geom_elt_coords_list[0]->y();
                    const double w = geom_elt_coords_list[2]->z() - geom_elt_coords_list[0]->z();

                    // Cross product computation. 12^13
                    pseudo_normal(0) = y * w - z * v;
                    pseudo_normal(1) = z * u - x * w;
                    pseudo_normal(2) = x * v - y * u;
                }

                if (geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Tetrahedron4)
                {
                    const auto& geom_elt_coords_list = geom_elt.GetCoordsList();

                    auto begin = geom_elt_coords_list.cbegin();
                    auto end = geom_elt_coords_list.cend();

                    const auto it =
                        std::find_if_not(begin,
                                         end,
                                         [face_coords_list, face_coords_list_size](const auto& tetra_coords_ptr)
                                         {
                                             int found = 0;
                                             const auto& tetra_coords = *tetra_coords_ptr;

                                             for (std::size_t j = 0UL; j < face_coords_list_size; ++j)
                                             {
                                                 auto& face_item = *face_coords_list[j];

                                                 if (face_item == tetra_coords)
                                                 {
                                                     ++found;
                                                 }
                                             }

                                             return found == 1;
                                         });

                    tetra_coord_last_point.GetNonCstValue(::MoReFEM::GeometryNS::dimension_type{ 0 }) = (*it)->x();
                    tetra_coord_last_point.GetNonCstValue(::MoReFEM::GeometryNS::dimension_type{ 1 }) = (*it)->y();
                    tetra_coord_last_point.GetNonCstValue(::MoReFEM::GeometryNS::dimension_type{ 2 }) = (*it)->z();

                    ++counter_tetra;
                }
            }

            if (counter_tetra > 1)
            {
                throw Exception("A triangle is shared by more than one tetrahedra which means that is it not on "
                                "the boundary of the mesh. This should not happen.");
            }

            const double norm = std::sqrt(pseudo_normal(0) * pseudo_normal(0) + pseudo_normal(1) * pseudo_normal(1)
                                          + pseudo_normal(2) * pseudo_normal(2));

            if (NumericNS::IsZero(norm))
                throw Exception(
                    "The norm of a pseudo normal on a face is zero. This should not happen if the mesh is correct.");

            const double one_over_norm = 1. / norm;

            pseudo_normal *= one_over_norm;

            if (counter_tetra == 1)
            {
                // Vector LastPoint->Barycenter.
                const double r = barycenter_of_face.x() - tetra_coord_last_point.x();
                const double s = barycenter_of_face.y() - tetra_coord_last_point.y();
                const double t = barycenter_of_face.z() - tetra_coord_last_point.z();

                const double verif_orientation = r * pseudo_normal(0) + s * pseudo_normal(1) + t * pseudo_normal(2);

                if (verif_orientation < 0)
                {
                    throw Exception("A triangle is not oriented towards the outside of the tetrahedra. This "
                                    "should not happen and you should verify your mesh before.");
                }
            }
        }
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
