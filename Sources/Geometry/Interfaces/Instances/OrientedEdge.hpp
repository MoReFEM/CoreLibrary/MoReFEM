// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDEDGE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDEDGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Instances/Edge.hpp"                          // IWYU pragma: export
#include "Geometry/Interfaces/Internal/Orientation/ComputeOrientation.hpp" // IWYU pragma: keep
#include "Geometry/Interfaces/Internal/Orientation/Orientation.hpp"        // IWYU pragma: export
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM
{


    /*!
     * \brief Class used to store an edge inside a \a GeometricElt.
     *
     * An \a Edge may be shared among several \a GeometricElt, but their orientation might differ inside each
     * of the \a GeometricElt. The purpose of the current object is therefore to store two information:
     * - A shared pointer to an \a Edge.
     * - An integer that gives the orientation.
     */
    class OrientedEdge final : public Crtp::Orientation<OrientedEdge, Edge>
    {

      public:
        //! Alias to shared pointer.
        using shared_ptr = std::shared_ptr<OrientedEdge>;

        //! Alias to vector of shared_pointer.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Name of the class.
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] edge Edge before any orientation is applied.
         * \param[in] local_edge_index Index of the edge in the local (topology) element; between 0 and Nedge - 1.
         * \param[in] coords_list_in_geom_elt List of \a Coords relared to the considered \a GeometricElt.
         * \copydoc doxygen_hide_topology_token
         */
        template<Advanced::Concept::TopologyNS::IndexedSectionDescriptionType TopologyIndexedSectionDescriptionT>
        explicit OrientedEdge(const Edge::shared_ptr& edge,
                              const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                              std::size_t local_edge_index,
                              TopologyIndexedSectionDescriptionT topology_token);

        //! Destructor.
        ~OrientedEdge() = default;

        //! \copydoc doxygen_hide_copy_constructor
        OrientedEdge(const OrientedEdge& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        OrientedEdge(OrientedEdge&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        OrientedEdge& operator=(const OrientedEdge& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        OrientedEdge& operator=(OrientedEdge&& rhs) = delete;


        ///@}
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Instances/OrientedEdge.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDEDGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
