// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDEDGE_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDEDGE_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/Orientation/ComputeOrientation.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM
{


    template<Advanced::Concept::TopologyNS::IndexedSectionDescriptionType TopologyIndexedSectionDescriptionT>
    OrientedEdge::OrientedEdge(const Edge::shared_ptr& edge_ptr,
                               const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                               std::size_t local_edge_index,
                               [[maybe_unused]] TopologyIndexedSectionDescriptionT topology_token)
    : Crtp::Orientation<OrientedEdge, Edge>(
          edge_ptr,
          Internal::InterfaceNS::ComputeEdgeOrientation<typename TopologyIndexedSectionDescriptionT::type>(
              coords_list_in_geom_elt,
              local_edge_index))
    {
        assert(GetOrientation() < Advanced::InterfaceNS::orientation_type{ 2UL }
               && "2 possible values for edge orientation!");
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDEDGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
