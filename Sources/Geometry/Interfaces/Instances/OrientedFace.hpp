// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDFACE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDFACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Instances/Face.hpp"                          // IWYU pragma: export
#include "Geometry/Interfaces/Internal/Orientation/ComputeOrientation.hpp" // IWYU pragma: keep
#include "Geometry/Interfaces/Internal/Orientation/Orientation.hpp"        // IWYU pragma: export
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM
{


    /*!
     * \brief Class used to store an face inside a GeometricElement.
     *
     * An face may be shared among several GeometricElement, but their orientation might differ inside each
     * of the GeometricElement. The purpose of the current object is therefore to store two information:
     * - A shared pointer to an Face.
     * - An integer that gives the orientation.
     */

    class OrientedFace final : public Crtp::Orientation<OrientedFace, Face>
    {

      public:
        //! Alias to shared pointer.
        using shared_ptr = std::shared_ptr<OrientedFace>;

        //! Alias to vector of shared_pointer.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Name of the class.
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] face Face before any orientation is applied.
         * \param[in] local_face_index Index of the face in the local (topology) element; between 0 and Nface - 1.
         * \param[in] coords_list_in_geom_elt List of \a Coords relared to the considered \a GeometricElt.
         * \copydoc doxygen_hide_topology_token
         */
        template<Advanced::Concept::TopologyNS::IndexedSectionDescriptionType TopologyIndexedSectionDescriptionT>
        explicit OrientedFace(const Face::shared_ptr& face,
                              const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                              std::size_t local_face_index,
                              TopologyIndexedSectionDescriptionT topology_token);


        //! Destructor.
        ~OrientedFace() = default;

        //! \copydoc doxygen_hide_copy_constructor
        OrientedFace(const OrientedFace& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        OrientedFace(OrientedFace&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        OrientedFace& operator=(const OrientedFace& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        OrientedFace& operator=(OrientedFace&& rhs) = delete;


        ///@}
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Instances/OrientedFace.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INSTANCES_ORIENTEDFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
