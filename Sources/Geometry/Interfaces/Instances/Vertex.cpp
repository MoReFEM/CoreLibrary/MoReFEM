// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/Interfaces/Instances/Vertex.hpp" // IWYU pragma: associated

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM
{


    const std::string& Vertex::ClassName()
    {
        static const std::string ret("Vertex");
        return ret;
    }


    Vertex::Vertex(const Coords::shared_ptr& coords)
    : Internal::InterfaceNS::TInterface<Vertex, InterfaceNS::Nature::vertex>(coords)
    {
        assert(!(!coords));
    }


    Vertex::~Vertex() = default;


    void Vertex::ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list)
    {
        if (!parent::IsPseudoNormalSet())
        {
            auto& pseudo_normal = parent::SetPseudoNormal();

            const std::size_t geom_elt_list_size = geom_elt_list.size();

            std::size_t index_p1 = 0;
            std::size_t index_p2 = 0;
            std::size_t index_p3 = 0;

            const auto& coords_list = GetCoordsList();
            assert(coords_list.size() == 1);
            const auto& coords = *coords_list[0];

            for (std::size_t i = 0; i < geom_elt_list_size; ++i)
            {
                assert(!(!geom_elt_list[i]));
                const auto& geom_elt = *geom_elt_list[i];

                if (geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3)
                {
                    const auto& face_list = geom_elt.GetOrientedFaceList();
                    const std::size_t face_list_size = face_list.size();

                    if (face_list_size > 1)
                        throw Exception("A triangle has more than one face. This should not happen.");

                    const auto& face = *face_list[0];
                    const auto& pseudo_normal_ptr = face.GetUnorientedInterface().GetPseudoNormalPtr();

                    if (pseudo_normal_ptr != nullptr)
                    {
                        const auto& interface_coords_list = face.GetUnorientedInterface().GetCoordsList();

                        for (std::size_t j = 0UL; j < 3UL; ++j)
                        {
                            const auto& coords_point = *interface_coords_list[j];

                            if (coords_point == coords)
                            {
                                index_p1 = j;
                            }
                        }

                        index_p2 = (index_p1 + 1) % 3;
                        index_p3 = (index_p1 + 2) % 3;

                        assert(*interface_coords_list[index_p1] == coords);

                        assert(!(!interface_coords_list[index_p1]));
                        assert(!(!interface_coords_list[index_p2]));
                        assert(!(!interface_coords_list[index_p3]));

                        const double p1_x = interface_coords_list[index_p1]->x();
                        const double p1_y = interface_coords_list[index_p1]->y();
                        const double p1_z = interface_coords_list[index_p1]->z();

                        const double x = p1_x - interface_coords_list[index_p3]->x();
                        const double y = p1_y - interface_coords_list[index_p3]->y();
                        const double z = p1_z - interface_coords_list[index_p3]->z();
                        const double norm_31 = std::sqrt(x * x + y * y + z * z);

                        // Vector 21
                        const double u = p1_x - interface_coords_list[index_p2]->x();
                        const double v = p1_y - interface_coords_list[index_p2]->y();
                        const double w = p1_z - interface_coords_list[index_p2]->z();
                        const double norm_21 = std::sqrt(u * u + v * v + w * w);

                        if (NumericNS::IsZero(norm_31 * norm_21))
                            throw Exception(
                                "The norm of a pseudo normal on a face is zero when computing a vertex pseudo-normal."
                                "This should not happen if the mesh is correct.");

                        const double alpha = std::acos((x * u + y * v + z * w) / (norm_31 * norm_21));

                        pseudo_normal += alpha * *pseudo_normal_ptr;
                    }
                }
            }

            const double norm = std::sqrt(pseudo_normal(0) * pseudo_normal(0) + pseudo_normal(1) * pseudo_normal(1)
                                          + pseudo_normal(2) * pseudo_normal(2));

            if (NumericNS::IsZero(norm))
                throw Exception(
                    "The norm of a pseudo normal on a vertex is zero. This should not happen if the mesh is correct.");

            const double one_over_norm = 1. / norm;

            pseudo_normal *= one_over_norm;
        }
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
