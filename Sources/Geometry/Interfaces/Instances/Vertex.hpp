// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INSTANCES_VERTEX_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INSTANCES_VERTEX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/TInterface.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    /*!
     * \brief Class in charge of the Vertex interface.
     *
     */
    class Vertex final : public Internal::InterfaceNS::TInterface<Vertex, InterfaceNS::Nature::vertex>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Vertex;

        //! Alias over shared_ptr.
        using shared_ptr = std::shared_ptr<self>;

        //! Alias over vector of shared_ptr.
        using vector_shared_ptr = std::vector<shared_ptr>;

        //! Alias to parent class.
        using parent = Internal::InterfaceNS::TInterface<Vertex, InterfaceNS::Nature::vertex>;

        static_assert(std::is_convertible<self*, parent*>());

        /*!
         * \brief Alias useful when the vertices are built.
         *
         * When a new \a Vertex is built, index is not yet associated to it; one must determine first whether
         * it has already been built by another \a GeometricElt. The type below is used to list all those
         * already built.
         */
        using InterfaceMap = Utilities::PointerComparison::
            Map<Vertex::shared_ptr, std::vector<const GeometricElt*>, InterfaceNS::LessByCoords>;

        //! Friendship.
        friend Mesh;

        //! Name of the class.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] coords Coords object associated to the Vertex interface.
         */
        explicit Vertex(const Coords::shared_ptr& coords);

        //! Destructor.
        ~Vertex() override;

        //! \copydoc doxygen_hide_copy_constructor
        Vertex(const Vertex& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Vertex(Vertex&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Vertex& operator=(const Vertex& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Vertex& operator=(Vertex&& rhs) = delete;


        ///@}

      private:
        /*!
         * \brief Compute the pseudo-normal of a vertex.
         *
         * \param[in] geom_elt_list List of the geometric elements that share this vertex.
         *
         * \warning Should not be called directly. To compute pseudo-normals use PseudoNormals1 in Lua that will use
         * the dedicated magager to compute them.
         *
         */
        void ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list);
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Instances/Vertex.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INSTANCES_VERTEX_DOT_HPP_
// *** MoReFEM end header guards *** < //
