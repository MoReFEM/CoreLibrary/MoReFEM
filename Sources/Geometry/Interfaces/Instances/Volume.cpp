// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Instances/Volume.hpp" // IWYU pragma: associated
#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM
{


    const std::string& Volume::ClassName()
    {
        static const std::string ret("Volume");
        return ret;
    }


    namespace // anonymous
    {


        /*!
         * \brief Add a new volume to a mesh, and increment accordingly Nvolume_per_mesh.
         *
         * \param[in] mesh_identifier Identifier of the mesh to which the Volume is added.
         * \param[in,out] Nvolume_per_mesh Number of volumes per mesh (represented by its identifier).
         *
         * \return Index of the newly added volume. This index is 0 for first volume, then 1, and so forth...
         */
        ::MoReFEM::InterfaceNS::program_wise_index_type
        AddVolume(MeshNS::unique_id mesh_identifier, std::map<MeshNS::unique_id, std::size_t>& Nvolume_per_mesh);


    } // namespace


    std::map<MeshNS::unique_id, std::size_t>& Volume::NvolumePerMesh()
    {
        static std::map<MeshNS::unique_id, std::size_t> ret;
        return ret;
    }


    Volume::Volume(const GeometricElt::shared_ptr& geometric_elt) : geometric_elt_(geometric_elt)
    {
        assert(!(!geometric_elt));

        decltype(auto) mesh_id = geometric_elt->GetMeshIdentifier();
        SetProgramWiseIndex(AddVolume(mesh_id, NvolumePerMesh()));
    }


    Volume::~Volume() = default;


    const Coords::vector_shared_ptr& Volume::GetCoordsList() const noexcept
    {
        assert(!geometric_elt_.expired()
               && "By design the geometric element should be born by the same processor "
                  "as the volume interface!");
        const GeometricElt::shared_ptr shared_ptr = geometric_elt_.lock();

        return shared_ptr->GetCoordsList();
    }


    namespace // anonymous
    {


        ::MoReFEM::InterfaceNS::program_wise_index_type
        AddVolume(MeshNS::unique_id mesh_identifier, std::map<MeshNS::unique_id, std::size_t>& Nvolume_per_mesh)
        {
            auto it = Nvolume_per_mesh.find(mesh_identifier);

            if (it == Nvolume_per_mesh.cend())
            {
                Nvolume_per_mesh.insert({ mesh_identifier, 1UL });
                return ::MoReFEM::InterfaceNS::program_wise_index_type{ 0UL };
            } else
            {
                ::MoReFEM::InterfaceNS::program_wise_index_type index{ it->second };
                ++it->second;
                return index;
            }
        }


    } // namespace


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
