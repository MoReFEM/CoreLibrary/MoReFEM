// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "Geometry/Interfaces/Interface.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"


namespace MoReFEM
{


    Interface::Interface(const Coords::shared_ptr& coords) : coords_list_({ coords })
    {
        assert(!(!coords));
    }


    Interface::Interface() = default;


    Interface::~Interface() = default;


    void Interface::SetCoordsList(const Coords::vector_shared_ptr& coords_list)
    {
        coords_list_ = coords_list;
        Internal::InterfaceNS::OrderCoordsList(coords_list_);
    }


    std::string ShortHand(const Interface& interface)
    {
        std::ostringstream oconv;

        switch (interface.GetNature())
        {
        case InterfaceNS::Nature::vertex:
            oconv << 'V';
            break;
        case InterfaceNS::Nature::edge:
            oconv << 'E';
            break;
        case InterfaceNS::Nature::face:
            oconv << 'F';
            break;
        case InterfaceNS::Nature::volume:
            oconv << 'G';
            break;
        case InterfaceNS::Nature::none:
        case InterfaceNS::Nature::undefined:
            assert(false && "This method should not be called for such objects.");
            exit(EXIT_FAILURE);
        }

        oconv << interface.GetProgramWiseIndex();

        const auto ret = oconv.str();
        return ret;
    }


    auto Interface::SetPseudoNormal() -> Eigen::Vector3d&
    {
        pseudo_normal_ = std::make_unique<Eigen::Vector3d>();
        pseudo_normal_->setZero();
        return GetNonCstPseudoNormal();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
