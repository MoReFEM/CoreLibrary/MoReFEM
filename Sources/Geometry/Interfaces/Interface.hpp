// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERFACE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INTERFACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/StrongType.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::MeshNS { class ComputeInterfaceListInMesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    //! Convenient enum to lift a check in a very specific case (reloading from pre-partitioned data)
    enum class allow_reset_index { no, yes };


    /*!
     * \brief Polymorphic base of all interface classes.
     *
     * Very little is defined here; definitions really occur in Internal::InterfaceNS::Interface class.
     *
     * \attention It must be noted that \a OrientedEdge and \a OrientedFace are NOT derived from this class: they are
     * separate objects which are built upon (by composition) respectively \a Edge and \a Face which are \a Interface.
     *
     * \internal <b><tt>[internal]</tt></b> This base class is special because we know for sure the number of final
     * classes expected: Vertex, Edge, Face and Volume. That's why for instance there are some constructors here are
     * already "specialized" for one of these cases.
     * \endinternal
     */

    class Interface
    {
      public:
        //! Alias to shared_ptr.
        using shared_ptr = std::shared_ptr<Interface>;

        //! Friendship to the utility class which reorder properly the \a Coords in the \a Interface after the reduction
        //! occurred.
        friend class Internal::MeshNS::ComputeInterfaceListInMesh;


        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor for vertex.
         *
         * \param[in] coords Coords object associated to the Vertex interface.
         */
        explicit Interface(const Coords::shared_ptr& coords);

        /*!
         * \brief Constructor for edges and faces.
         *
         * \tparam LocalContentT Type of the container or structure that represents the interface in the
         * local topology. It is given by TopologyT::EdgeContent for the edges and TopologyT::FaceContent
         * for the faces; where TopologyT is the topology of the GeometricElt for which the Interface is built.
         *
         * \param[in] local_content Integer values that represent the interface within the local topology.
         * Is is one of the element of TopologyT::GetEdgeList() or TopologyT::GetFaceList() (see above for the
         * meaning of TopologyT).
         * \param[in] coords_list_in_geom_elt List of Coords objects within the GeometricElt for which the interface
         * is built.
         */
        template<class LocalContentT>
        explicit Interface(const LocalContentT& local_content,
                           const Coords::vector_shared_ptr& coords_list_in_geom_elt);

        /*!
         * \brief Empty constructor (for volumes).
         *
         * Volume are apart: when they are built the index is already known as we know for sure each Volume is unique
         * for each GeometricElt. Due to this close relationship, no Coords are stored within a Volume, as it would be
         * a waste of space given the Coords can be retrieved through the GeometricElt object that shares the same
         * index. Volume objects include weak pointers to its related GeometricElt so that Coords could
         * be retrieved without duplication of data.
         */
        explicit Interface();

        //! Destructor.
        virtual ~Interface();

        //! \copydoc doxygen_hide_copy_constructor
        Interface(const Interface& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Interface(Interface&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Interface& operator=(const Interface& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Interface& operator=(Interface&& rhs) = delete;

        ///@}

        //! Nature of the Interface.
        virtual InterfaceNS::Nature GetNature() const noexcept = 0;

        /*!
         * \brief Set the identifier associated to this interface.
         *
         * \param[in] id Identifier to be associated to the interface. Identifier is unique for all
         * interfaces of the same nature: there should be for instance only one edge with id = 124.
         */
        template<allow_reset_index AllowResetIndexT = allow_reset_index::no>
        void SetProgramWiseIndex(InterfaceNS::program_wise_index_type id) noexcept;

        //! Get the identifier associated to this interface.
        InterfaceNS::program_wise_index_type GetProgramWiseIndex() const noexcept;

        //! Get the list of \a Coords that belongs to the interface.
        //! \internal This methods is overridden only for \a Volume, as we do not store directly the \a Coords in this
        //! very specific case to avoid redundancy with the \a GeometricElt which already got them.
        virtual const Coords::vector_shared_ptr& GetCoordsList() const noexcept;

        /*!
         * \brief Compute the list of \a Coords indexes that belongs to the interface.
         *
         * \tparam TypeT The type of \a Coords index requested.
         *
         * \return List of \a Coords indexes that belongs to the interface.
         *
         * \internal This method should not be used that much; that is the reason a return by copy has been adopted
         * here.
         */
        template<CoordsNS::index_enum TypeT>
        std::vector<CoordsNS::index_type<TypeT>> ComputeCoordsIndexList() const;

        //! Constant accessor on the pseudo-normal.
        const Eigen::Vector3d& GetPseudoNormal() const noexcept;

        //! Constant accessor on the pointer of the pseudo-normal.
        const std::unique_ptr<Eigen::Vector3d>& GetPseudoNormalPtr() const noexcept;


      private:
        /*!
         * \brief Set coords_list_ and sort it so that the same interface used by another geometric element could
         * match it.
         *
         * The convention is to put first the \a Coords with the lower index, and then choose the rotation that put
         * the lowest possible index in second. The index used to do so is the one used to define \a operator<
         * for \a Coords class (i.e. the processor-wise one).
         *
         * For instance let's consider quadrangle 3 5 1 10.
         *
         * The lower is 1, and there are still two possibilities by rotating it:
         * - 1 10 3 5
         * - 1 5 3 10
         * The latter is chosen because 5 is lower than 10.
         *
         * This method is used only for faces and edges: it is obviously unrequired for vertices, and no Coords
         * is stored for Volumes.
         *
         * \param[in] coords_list List of \a Coords, not yet sort.
         *
         * \internal <b><tt>[internal]</tt></b> It is intended to be called only in Edge and Face constructor.
         * \endinternal
         */
        void SetCoordsList(const Coords::vector_shared_ptr& coords_list);

        /*!
         * \brief Non constant accessor to the underlying list of \a Coords.
         *
         * \return Non constant access to the underlying list of \a Coords.
         */
        Coords::vector_shared_ptr& GetNonCstCoordsList() noexcept;

      protected:
        /*!
         * \brief Whether pseudo-normal has already been set or not.
         *
         * \return True if already set.
         */
        bool IsPseudoNormalSet() const noexcept;

        /*!
         * \brief Set pseudo-normal for the current \a Interface
         *
         * If this method is not called, it is assumed the \a Interface doesn't yield a pseudo-normal; any attempt to
         * use it will produce an `assert` in debug mode.
         *
         * \return Non constant reference to the object that has just been set (its values were zeroed).
         */
        Eigen::Vector3d& SetPseudoNormal();

        //! Non constant accessor on the pseudo-normal.
        Eigen::Vector3d& GetNonCstPseudoNormal() noexcept;

      private:
        /*!
         * \brief Coords that belongs to the edge_or_face.
         *
         * The convention is that the first Coords is the one with the lowest index.
         * The relative ordering is local_coords.
         *
         * For instance, if (89, 42, 15) is read in Init(), (15, 89, 42) will be stored.
         *
         * \internal <b><tt>[internal]</tt></b> This information is really important when the interfaces are built:
         * it is the way we can figure out if a given interface already exist or not.
         * \endinternal
         */
        Coords::vector_shared_ptr coords_list_;

        //! Facility which handles the indexes related to the \a Interface.
        InterfaceNS::program_wise_index_type program_wise_index_ =
            NumericNS::UninitializedIndex<InterfaceNS::program_wise_index_type>();

      private:
        //! Pseudo-normal of the interface.
        std::unique_ptr<Eigen::Vector3d> pseudo_normal_{ nullptr };
    };


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * The convention is that the ordering is done following the coords list in the manner:
     * - Nature are organized in the order: Vertex, Edge, Face, Volume.
     * - If same nature, indexes are compared.
     *
     */
    bool operator<(const Interface& lhs, const Interface& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * Two Interface objects are equal if their nature and their index are the same.
     *
     * Beware: an overload is provided for OrientedEdge and OrientedFace which also takes into account the orientation.
     */
    bool operator==(const Interface& lhs, const Interface& rhs) noexcept;


    /*!
     * \copydoc doxygen_hide_operator_not_equal
     *
     * Defined from operator==.
     */
    bool operator!=(const Interface& lhs, const Interface& rhs) noexcept;


    /*!
     * \brief Gives a short hand to tag the interface, such as 'V15' or 'E105'.
     *
     * The possible letters are:
     * - V (for Vertex)
     * - E (for Edge)
     * - F (for Face)
     * - G (for Volume - V was always taken and G may stand for GeometricElement).
     *
     * The digits are the program-wise index.
     *
     * Such shorthands are used in the context of pre-partitioning: the file with the prepartitioned data uses them up.
     *
     * \param[in] interface \a Interface considered.
     */
    std::string ShortHand(const Interface& interface);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// Ordering is important here!
#include "Geometry/Interfaces/Impl/Specialization.hpp" // IWYU pragma: export
#include "Geometry/Interfaces/Interface.hxx"           // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
