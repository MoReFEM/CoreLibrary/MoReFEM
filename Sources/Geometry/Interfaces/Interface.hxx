// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERFACE_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INTERFACE_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Interface.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <memory>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/StrongType.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Interface; }
namespace MoReFEM { enum class allow_reset_index; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<class LocalContentT>
    Interface::Interface(const LocalContentT& local_content, const Coords::vector_shared_ptr& elt_coords_list)
    {
        Coords::vector_shared_ptr content;

        for (auto vertex_index : local_content)
        {
            assert(vertex_index < elt_coords_list.size());
            content.push_back(elt_coords_list[vertex_index]);
        }

        // Set the coordinates and shuffle the points.
        SetCoordsList(content);
    }


    template<allow_reset_index AllowResetIndexT>
    void Interface::SetProgramWiseIndex(InterfaceNS::program_wise_index_type id) noexcept
    {
        if constexpr (AllowResetIndexT == allow_reset_index::no)
            assert("Should be assigned only once!"
                   && program_wise_index_ == NumericNS::UninitializedIndex<InterfaceNS::program_wise_index_type>());

        program_wise_index_ = id;
    }


    inline InterfaceNS::program_wise_index_type Interface::GetProgramWiseIndex() const noexcept
    {
        assert(NumericNS::UninitializedIndex<InterfaceNS::program_wise_index_type>() != program_wise_index_);
        return program_wise_index_;
    }


    inline const Coords::vector_shared_ptr& Interface::GetCoordsList() const noexcept
    {
#ifndef NDEBUG
        if (GetNature() == InterfaceNS::Nature::volume)
            assert(coords_list_.empty() && "A specialization should be called for Volume!");
#endif // NDEBUG

        return coords_list_;
    }


    inline bool operator==(const Interface& lhs, const Interface& rhs) noexcept
    {
        if (lhs.GetNature() != rhs.GetNature())
            return false;

        return lhs.GetProgramWiseIndex() == rhs.GetProgramWiseIndex();
    }


    inline bool operator<(const Interface& lhs, const Interface& rhs) noexcept
    {
        const auto lhs_nature = lhs.GetNature();
        const auto rhs_nature = rhs.GetNature();

        if (lhs_nature != rhs_nature)
            return lhs_nature < rhs_nature;

        return lhs.GetProgramWiseIndex() < rhs.GetProgramWiseIndex();
    }


    inline bool operator!=(const Interface& lhs, const Interface& rhs) noexcept
    {
        return !(operator==(lhs, rhs));
    }


    inline auto Interface::GetPseudoNormal() const noexcept -> const Eigen::Vector3d&
    {
        assert(!(!pseudo_normal_));
        return *pseudo_normal_;
    }

    inline auto Interface::GetNonCstPseudoNormal() noexcept -> Eigen::Vector3d&
    {
        return const_cast<Eigen::Vector3d&>(GetPseudoNormal());
    }


    inline auto Interface::GetPseudoNormalPtr() const noexcept -> const std::unique_ptr<Eigen::Vector3d>&
    {
        return pseudo_normal_;
    }


    template<CoordsNS::index_enum TypeT>
    std::vector<CoordsNS::index_type<TypeT>> Interface::ComputeCoordsIndexList() const
    {
        using index_type = CoordsNS::index_type<TypeT>;
        decltype(auto) coords_list = GetCoordsList();
        std::vector<index_type> ret(coords_list.size());

        std::transform(coords_list.cbegin(),
                       coords_list.cend(),
                       ret.begin(),
                       [](const auto& coords_ptr)
                       {
                           assert(!(!coords_ptr));
                           return coords_ptr->template GetIndex<TypeT>();
                       });

        return ret;
    }


    inline Coords::vector_shared_ptr& Interface::GetNonCstCoordsList() noexcept
    {
        return const_cast<Coords::vector_shared_ptr&>(GetCoordsList());
    }


    inline bool Interface::IsPseudoNormalSet() const noexcept
    {
        return pseudo_normal_ != nullptr;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERFACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
