// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    template<>
    const Vertex::vector_shared_ptr& GetInterfaceOfGeometricElt<Vertex>(const GeometricElt& geometric_element)
    {
        return geometric_element.GetVertexList();
    }


    template<>
    const OrientedEdge::vector_shared_ptr&
    GetInterfaceOfGeometricElt<OrientedEdge>(const GeometricElt& geometric_element)
    {
        return geometric_element.GetOrientedEdgeList();
    }


    template<>
    const OrientedFace::vector_shared_ptr&
    GetInterfaceOfGeometricElt<OrientedFace>(const GeometricElt& geometric_element)
    {
        return geometric_element.GetOrientedFaceList();
    }


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
