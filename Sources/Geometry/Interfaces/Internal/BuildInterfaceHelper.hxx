// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACEHELPER_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACEHELPER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Internal/Impl/BuildInterfaceHelper.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    template<class InterfaceT, ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    typename InterfaceT::vector_shared_ptr
    Build<InterfaceT, TopologyT>::Perform(const GeometricElt* geom_elt_ptr,
                                          const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                          typename InterfaceT::InterfaceMap& already_existing_interface_list)
    {
        if constexpr (Ninterface() == 0)
            return typename InterfaceT::vector_shared_ptr{};

        typename InterfaceT::vector_shared_ptr ret;
        constexpr std::size_t Ninterface = Build<InterfaceT, TopologyT>::Ninterface();

        for (std::size_t i = 0UL; i < Ninterface; ++i)
        {
            auto item_ptr = Impl::CreateNewInterface<InterfaceT, TopologyT>::Perform(coords_in_geometric_elt, i);

            // Look whether the interface already existed; the criterion is to match or not the coord list
            // (if 2 interfaces get the same list of \a Coords then they are actually the same).
            auto it = already_existing_interface_list.find(item_ptr);

            ::MoReFEM::InterfaceNS::program_wise_index_type new_index{ already_existing_interface_list.size() };

            if (it == already_existing_interface_list.cend())
            {
                // Give a unique index to the new interface: the number inside the container so far.
                item_ptr->SetProgramWiseIndex(new_index++);

                ret.push_back(item_ptr);
                std::vector<const GeometricElt*> vector_raw;
                vector_raw.push_back(geom_elt_ptr);
                already_existing_interface_list.insert({ item_ptr, vector_raw });
            } else
            {
                auto& pair = *it;
                ret.push_back(pair.first); // put in the container the already existing interface.
                pair.second.push_back(geom_elt_ptr);
            }
        }

        return ret;
    }


    template<class InterfaceT, ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    constexpr std::size_t Build<InterfaceT, TopologyT>::Ninterface()
    {
        return MoReFEM::InterfaceNS::Ninterface<TopologyT, InterfaceT::StaticNature()>::Value();
    }


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACEHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
