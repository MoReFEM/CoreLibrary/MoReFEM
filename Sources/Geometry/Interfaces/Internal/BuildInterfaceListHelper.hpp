// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACELISTHELPER_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACELISTHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"
#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"
#include "Geometry/Interfaces/Internal/Orientation/ComputeOrientation.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    /*!
     * \brief Creates the list of \a Edge if it is relevant for \a TopologyT.
     *
     * \tparam TopologyT Topology currently considered.
     *
     * \param[in] geom_elt_ptr Pointer to the \a GeometricElt for which we want to determine edges.
     * \param[in] coords_list_in_geom_elt \a Coords in the \a GeometricElt.
     * \param[in,out] existing_list List of all edges found so far (in the whole mesh, not only for
     * current geometric element).
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    OrientedEdge::vector_shared_ptr ComputeEdgeList(const GeometricElt* geom_elt_ptr,
                                                    const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                                    Edge::InterfaceMap& existing_list);


    /*!
     * \brief Creates the list of \a Face if it is relevant for \a TopologyT.
     *
     * \tparam TopologyT Topology currently considered.
     *
     * \param[in] geom_elt_ptr Pointer to the \a GeometricElt for which we want to determine faces.
     * \param[in] coords_list_in_geom_elt \a Coords in the \a GeometricElt.
     * \param[in,out] existing_list List of all edges found so far (in the whole mesh, not only for
     * current geometric element).
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    OrientedFace::vector_shared_ptr ComputeFaceList(const GeometricElt* geom_elt_ptr,
                                                    const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                                    Face::InterfaceMap& existing_list);


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Internal/BuildInterfaceListHelper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACELISTHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
