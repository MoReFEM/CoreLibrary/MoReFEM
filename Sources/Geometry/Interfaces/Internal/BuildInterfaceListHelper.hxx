// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACELISTHELPER_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACELISTHELPER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Internal/BuildInterfaceListHelper.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::InterfaceNS
{


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    OrientedEdge::vector_shared_ptr
    ComputeEdgeList([[maybe_unused]] const GeometricElt* geom_elt_ptr,
                    [[maybe_unused]] const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                    [[maybe_unused]] Edge::InterfaceMap& existing_list)
    {
        if constexpr (TopologyT::Nedge == 0)
        {
            OrientedEdge::vector_shared_ptr ret;
            return ret;
        } else
        {
            // Create new edges or retrieve them if they already exist. Do not consider orientation at all
            // there.
            auto&& edge_without_orientation_list = Internal::InterfaceNS::Build<Edge, TopologyT>::Perform(
                geom_elt_ptr, coords_list_in_geom_elt, existing_list);

            // Now add the orientation information before storing it into the GeometricElt.
            const std::size_t Nedge = edge_without_orientation_list.size();

            OrientedEdge::vector_shared_ptr oriented_edge_list(Nedge);

            for (std::size_t i = 0UL; i < Nedge; ++i)
            {
                auto& edge_without_orientation_ptr = edge_without_orientation_list[i];

                oriented_edge_list[i] = std::make_shared<OrientedEdge>(
                    edge_without_orientation_ptr, coords_list_in_geom_elt, i, Utilities::Type2Type<TopologyT>());
            }

            return oriented_edge_list;
        }
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    OrientedFace::vector_shared_ptr
    ComputeFaceList([[maybe_unused]] const GeometricElt* geom_elt_ptr,
                    [[maybe_unused]] const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                    [[maybe_unused]] Face::InterfaceMap& existing_list)
    {
        if constexpr (TopologyT::Nface == 0)
        {
            OrientedFace::vector_shared_ptr ret;
            return ret;
        } else
        {
            // Create new faces or retrieve them if they already exist. Do not consider orientation at all
            // there.
            auto&& face_without_orientation_list = Internal::InterfaceNS::Build<Face, TopologyT>::Perform(
                geom_elt_ptr, coords_list_in_geom_elt, existing_list);


            // Now add the orientation information before storing it into the GeometricElt.
            const std::size_t Nface = face_without_orientation_list.size();

            OrientedFace::vector_shared_ptr oriented_face_list(Nface);

            for (std::size_t i = 0UL; i < Nface; ++i)
            {
                auto face_without_orientation_ptr = face_without_orientation_list[i];

                oriented_face_list[i] = std::make_shared<OrientedFace>(
                    face_without_orientation_ptr, coords_list_in_geom_elt, i, Utilities::Type2Type<TopologyT>());
            }

            return oriented_face_list;
        }
    }


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_BUILDINTERFACELISTHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
