// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_IMPL_BUILDINTERFACEHELPER_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_IMPL_BUILDINTERFACEHELPER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Internal/Impl/BuildInterfaceHelper.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Advanced/LocalData.hpp"
#include "Geometry/Interfaces/Interface.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::Internal::InterfaceNS::Impl
{


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    Vertex::shared_ptr
    CreateNewInterface<Vertex, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                   std::size_t local_vertex_index)
    {
        const std::size_t index = local_vertex_index;
        assert(index < coords_in_geometric_elt.size());

        return std::make_shared<Vertex>(coords_in_geometric_elt[index]);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    Edge::shared_ptr
    CreateNewInterface<Edge, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                 std::size_t local_edge_index)
    {
        const auto& local_edge = ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>::GetEdge(local_edge_index);

        return std::make_shared<Edge>(local_edge, coords_in_geometric_elt);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    Face::shared_ptr
    CreateNewInterface<Face, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                 std::size_t local_face_index)
    {
        const auto& local_face = ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>::GetFace(local_face_index);

        return std::make_shared<Face>(local_face, coords_in_geometric_elt);
    }


} // namespace MoReFEM::Internal::InterfaceNS::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_IMPL_BUILDINTERFACEHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
