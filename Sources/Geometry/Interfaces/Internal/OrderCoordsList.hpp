// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORDERCOORDSLIST_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORDERCOORDSLIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    /*!
     * \brief Order the list of \a Coords
     *
     * \param[in,out] coords_list List of \a Coords that delimits an interface. In output, the value are sort according to the algorithm below.
     *
     * If more than 2, we look at its two closest neighbours (with a modulo if at the end of the list) because
     * we also want the second element to be lower than the last.
     * The reason is the following: consider the quadrangle 3 1 2 0.
     * Our rotate would yield: 0 3 1 2.
     * However, if later we meet 0 2 1 3, it is also the same quadrangle, with a different orientation...
     * but orientation is dealt with elsewhere; here we do not want it. Hence the following step
     * to ensure a unique sequence to identify the geometric element.
     *
     * \param[in] functor The functor used to compare items of the \a coords_list. The two usual choices are either the default one if
     * the vector is made of \a Coords::shared_ptr, or std::less<StrongTypeT> where \a StrongTypeT is a \a StrongType
     * related to an index of \a Coords.
     *
     * \internal This was previously a method but I need it elsewhere for \a FromCoordsMatching implementation.
     */
    template<class CoordsVectorT, class FunctorT = Utilities::PointerComparison::Less<Coords::shared_ptr>>
    void OrderCoordsList(CoordsVectorT& coords_list,
                         FunctorT&& functor = Utilities::PointerComparison::Less<Coords::shared_ptr>());


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
#include "Geometry/Interfaces/Internal/OrderCoordsList.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORDERCOORDSLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
