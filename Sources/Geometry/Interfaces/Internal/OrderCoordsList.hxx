// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORDERCOORDSLIST_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORDERCOORDSLIST_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::InterfaceNS
{


    template<class CoordsVectorT, class FunctorT>
    void OrderCoordsList(CoordsVectorT& coords_list, FunctorT&& comparison_op)
    {
        auto begin = coords_list.begin();
        auto end = coords_list.end();

        using difference_type = Coords::vector_shared_ptr::difference_type;

        const difference_type Ncoord_on_interface = static_cast<difference_type>(coords_list.size());
        assert(Ncoord_on_interface > 0l);

        // \todo #205 Not entirely true above: in P2 geometry there could be more than edges...

        // If only one element we're already done.
        if (Ncoord_on_interface > 1)
        {
            // First we determine the minimum element.
            auto it_minimum = std::min_element(begin, end, comparison_op);

            // If only two elements, simply put the minimum in first position.
            if (Ncoord_on_interface == 2)
            {
                std::rotate(begin, it_minimum, end);
            }

            // If more, we look at its two closest neighbours (with a modulo if at the end of the list) because
            // we also want the second element to be lower than the last.
            // The reason is the following: consider the quadrangle 3 1 2 0.
            // Our rotate would yield: 0 3 1 2.
            // However, if later we meet 0 2 1 3, it is also the same quadrangle, with a different orientation...
            // but orientation is dealt with elsewhere; here we do not want it. Hence the following step
            // to ensure a unique sequence to identify the geometric element.
            else
            {
                auto it_after_minimum(it_minimum);
                ++it_after_minimum;

                if (it_after_minimum == end)
                    it_after_minimum = begin;

                auto it_before_minimum = it_minimum == begin ? begin + Ncoord_on_interface - 1 : it_minimum - 1;

                if (comparison_op(*it_after_minimum, *it_before_minimum))
                {
                    std::rotate(begin, it_minimum, end);
                } else
                {
                    std::reverse(begin, end);

                    // begin and end iterators are still valid, but it_minimum is not: its position
                    // has changed. We could avoid the min_element entirely but as we work on tiny vectors
                    // here the gain would be trifling.
                    it_minimum = std::min_element(begin, end, comparison_op);
                    std::rotate(begin, it_minimum, end);
                }
            }
        }
    }


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORDERCOORDSLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
