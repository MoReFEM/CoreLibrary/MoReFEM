// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_COMPUTEORIENTATION_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_COMPUTEORIENTATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Interfaces/Advanced/LocalData.hpp"
#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"
#include "Geometry/Interfaces/Instances/Edge.hpp"
#include "Geometry/Interfaces/Instances/Face.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    /*!
     * \class doxygen_hide_coords_list_in_geom_elt_arg
     *
     * \param[in] coords_list_in_geom_elt List of all \a Coords of a given \a GeometricElt.
     */


    //! Make `orientation_type` enum known to current namespace.
    using ::MoReFEM::Advanced::InterfaceNS::orientation_type;


    /*!
     * \brief Compute the orientation of a given edge.
     *
     * \tparam TopologyT Topology considered.
     *
     * \copydoc doxygen_hide_coords_list_in_geom_elt_arg
     * \param[in] local_edge_index Index of the local edge for which orientation is sought (must be
     * in [0, TopologyT::Nedge[).
     *
     * \return Index that tag the orientation (0 or 1 for an edge).
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    auto ComputeEdgeOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt, std::size_t local_edge_index)
        -> orientation_type;


    /*!
     * \brief Compute the orientation of a given face.
     *
     * \tparam TopologyT Topology considered.
     *
     * \copydoc doxygen_hide_coords_list_in_geom_elt_arg
     * \param[in] local_face_index Index of the local face for which orientation is sought (must be
     * in [0, TopologyT::Nface[).
     *
     * \return Index that tag the orientation.
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    auto ComputeFaceOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt, std::size_t local_face_index)
        -> orientation_type;


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Internal/Orientation/ComputeOrientation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_COMPUTEORIENTATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
