// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_COMPUTEORIENTATION_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_COMPUTEORIENTATION_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Internal/Orientation/ComputeOrientation.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::InterfaceNS
{


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    auto ComputeEdgeOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                const std::size_t local_edge_index) -> orientation_type
    {
        static_assert(!std::is_same<typename TopologyT::EdgeContent, std::false_type>::value,
                      "This free function shouldn't be called when EdgeContent is irrelevant!");

        const auto& topology_edge = ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>::GetEdge(local_edge_index);

        assert(topology_edge.size() == 2UL);

        assert(topology_edge[0] < coords_list_in_geom_elt.size());
        assert(topology_edge[1] < coords_list_in_geom_elt.size());

        const auto& vertex_0 = coords_list_in_geom_elt[topology_edge[0]];
        const auto& vertex_1 = coords_list_in_geom_elt[topology_edge[1]];

        assert(!(!vertex_0));
        assert(!(!vertex_1));

        if (*vertex_1 < *vertex_0)
            return orientation_type{ 1UL };

        return orientation_type{ 0UL };
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    auto ComputeFaceOrientation(const Coords::vector_shared_ptr& coords_list_in_geom_elt,
                                const std::size_t local_face_index) -> orientation_type
    {
        static_assert(!std::is_same<typename TopologyT::FaceContent, std::false_type>::value,
                      "This free function shouldn't be called when FaceContent is irrelevant!");

// Following algorithm is a direct adaptation of Ondomatic's one, with a generalization to avoid
// duplicating it for each topology.
#ifndef NDEBUG
        const std::size_t Nvertex = TopologyT::Nvertex;
#endif // NDEBUG

        const std::size_t Npoint_on_face = TopologyT::FaceTopology::Nvertex;

        const auto& topology_face = ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>::GetFace(local_face_index);

        assert(topology_face.size() == Npoint_on_face);

        orientation_type ret{ 0UL };

        for (std::size_t i = 1UL; i < Npoint_on_face; ++i)
        {
            assert(topology_face[i] < Nvertex);
            assert(topology_face[ret.Get()] < Nvertex);
            assert(!(!coords_list_in_geom_elt[topology_face[i]]));
            assert(!(!coords_list_in_geom_elt[topology_face[ret.Get()]]));

            if (*coords_list_in_geom_elt[topology_face[i]] < *coords_list_in_geom_elt[topology_face[ret.Get()]])
                ret = orientation_type{ i };
        }

        {
            const std::size_t first_index = (ret.Get() + Npoint_on_face - 1UL) % Npoint_on_face;
            const std::size_t second_index = (ret.Get() + 1UL) % Npoint_on_face;

            assert(first_index < Npoint_on_face);
            assert(second_index < Npoint_on_face);

            const std::size_t topology_first_index = topology_face[first_index];
            const std::size_t topology_second_index = topology_face[second_index];

            assert(topology_first_index < Nvertex);
            assert(topology_second_index < Nvertex);
            assert(!(!coords_list_in_geom_elt[topology_first_index]));
            assert(!(!coords_list_in_geom_elt[topology_second_index]));

            if (*coords_list_in_geom_elt[topology_first_index] < *coords_list_in_geom_elt[topology_second_index])
                ret += orientation_type{ Npoint_on_face };
        }

        return ret;
    }


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_COMPUTEORIENTATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
