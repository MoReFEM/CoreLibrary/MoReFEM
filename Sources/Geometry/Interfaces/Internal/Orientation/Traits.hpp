// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_TRAITS_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_TRAITS_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::InterfaceNS::Traits
{


    /*!
     * \brief Traits class for oriented interface.
     */
    template<class InterfaceT>
    struct OrientedInterface;


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    template<>
    struct OrientedInterface<Vertex>
    {
        using type = Vertex;
    };


    template<>
    struct OrientedInterface<Volume>
    {
        using type = Volume;
    };


    template<>
    struct OrientedInterface<Edge>
    {
        using type = OrientedEdge;
    };


    template<>
    struct OrientedInterface<OrientedEdge>
    {
        using type = OrientedEdge;
    };


    template<>
    struct OrientedInterface<Face>
    {
        using type = OrientedFace;
    };


    template<>
    struct OrientedInterface<OrientedFace>
    {
        using type = OrientedFace;
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::InterfaceNS::Traits


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_ORIENTATION_TRAITS_DOT_HPP_
// *** MoReFEM end header guards *** < //
