// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_TINTERFACE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_TINTERFACE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <memory>
#include <sstream>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Pointer.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Interface.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::InterfaceNS
{


    /*!
     * \brief CRTP from which the various interfaces (vertex, edge, face or volume) should inherit.
     *
     * \tparam DerivedT CRTP term.
     * \tparam NatureT Whether we are considering a vertex, an edge, a face or a volume.
     */
    template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
    class TInterface : public Interface
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor for vertex.
         *
         * \param[in] coords Coords object associated to the Vertex interface.
         */
        explicit TInterface(const Coords::shared_ptr& coords);

        /*!
         * \brief Constructor for edges and faces.
         *
         * \tparam LocalContentT Type of the container or structure that represents the interface in the
         * local topology. It is given by TopologyT::EdgeContent for the edges and TopologyT::FaceContent
         * for the faces; where TopologyT is the topology of the GeometricElt for which the Interface is built.
         *
         * \param[in] local_content Integer values that represent the interface within the local topology.
         * It is one of the element of TopologyT::GetEdgeList() or TopologyT::GetFaceList() (see above for the
         * meaning of TopologyT).
         * \param[in] coords_list_in_geom_elt List of Coords objects within the GeometricElt for which the
         * interface is built.
         */
        template<class LocalContentT>
        explicit TInterface(const LocalContentT& local_content,
                            const Coords::vector_shared_ptr& coords_list_in_geom_elt);

        /*!
         * \brief Empty constructor (for volumes).
         *
         * Volume are apart: when they are built the index is already known as we know for sure each Volume is
         * unique for each GeometricElt. Due to this close relationship, no Coords are stored within a Volume,
         * as it would be a waste of space given the Coords can be retrieved through the GeometricElt object
         * that shares the same index. Volume objects include weak pointers to its related GeometricElt so that
         * Coords could be retrieved without duplication of data.
         */
        explicit TInterface() = default;

        //! Destructor.
        ~TInterface() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        TInterface(const TInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TInterface(TInterface&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TInterface& operator=(const TInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TInterface& operator=(TInterface&& rhs) = delete;
        ///@}

        /*!
         * \brief Print the underlying coords list.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Print(std::ostream& stream) const;

        //! Nature of the Interface.
        virtual ::MoReFEM::InterfaceNS::Nature GetNature() const noexcept override final;

        //! Nature of the Interface as a static method.
        constexpr static enum ::MoReFEM::InterfaceNS::Nature StaticNature();
    };


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Internal/TInterface.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_TINTERFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
