// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INTERNAL_TINTERFACE_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_INTERNAL_TINTERFACE_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/Internal/TInterface.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::InterfaceNS
{


    template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
    template<class LocalContentT>
    TInterface<DerivedT, NatureT>::TInterface(const LocalContentT& local_content,
                                              const Coords::vector_shared_ptr& elt_coords_list)
    : Interface(local_content, elt_coords_list)
    { }


    template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
    TInterface<DerivedT, NatureT>::TInterface(const Coords::shared_ptr& coords) : Interface(coords)
    { }


    template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
    void TInterface<DerivedT, NatureT>::Print(std::ostream& stream) const
    {
        std::ostringstream oconv;
        oconv << "Coord in the ";
        oconv << NatureT;
        oconv << " -> [";

        Utilities::PrintContainer<Utilities::PrintPolicyNS::Pointer>::Do(
            GetCoordsList(),
            stream,
            ::MoReFEM::PrintNS::Delimiter::separator(", "),
            ::MoReFEM::PrintNS::Delimiter::opener(oconv.str()),
            ::MoReFEM::PrintNS::Delimiter::closer("]"));

        assert(GetProgramWiseIndex() != NumericNS::UninitializedIndex<decltype(GetProgramWiseIndex())>());

        stream << " and id = " << GetProgramWiseIndex();
    }


    template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
    ::MoReFEM::InterfaceNS::Nature TInterface<DerivedT, NatureT>::GetNature() const noexcept
    {
        return NatureT;
    }


    template<class DerivedT, ::MoReFEM::InterfaceNS::Nature NatureT>
    constexpr ::MoReFEM::InterfaceNS::Nature TInterface<DerivedT, NatureT>::StaticNature()
    {
        return NatureT;
    }


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INTERNAL_TINTERFACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
