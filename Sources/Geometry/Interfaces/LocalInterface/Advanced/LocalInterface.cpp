// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <type_traits> // IWYU pragma: keep

#include "Geometry/Interfaces/LocalInterface/Advanced/LocalInterface.hpp"


namespace MoReFEM::Advanced::InterfaceNS
{


    [[noreturn]] LocalInterface::LocalInterface([[maybe_unused]] std::false_type unused,
                                                ::MoReFEM::InterfaceNS::Nature nature,
                                                IsInterior interior)
    : nature_(nature), is_interior_(interior)
    {
        assert(false && "Here only for compile requirements; should never be actually called!");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Advanced::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
