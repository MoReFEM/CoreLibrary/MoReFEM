// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_ADVANCED_LOCALINTERFACE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_ADVANCED_LOCALINTERFACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

// #include "Geometry/Interfaces/EnumInterface.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::InterfaceNS
{


    //! Whether the interface is an interior interface or not.
    enum IsInterior { no, yes };


    /*!
     * \brief Geometric interface considered at the local level ('local' in the finite element meaning).
     */
    class LocalInterface
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LocalInterface;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] vertex_index_list List of vertices that delimits the interface, given by their index.
         * The index here are the ones used in the description of the topology; so for instance for a triangle
         * it is among {0, 1, 2} where the position of each of those is given in topologic description.
         * \param[in] nature Nature of the interface (vertex, edge, face or volume).
         * \param[in] is_interior Whether the interface is an interior interface or not.
         */
        template<class VertexIndexListTypeT>
        explicit LocalInterface(const VertexIndexListTypeT& vertex_index_list,
                                ::MoReFEM::InterfaceNS::Nature nature,
                                IsInterior is_interior);


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // ============================

        /*!
         * \brief 'Overload' of the constructor when VertexIndexListTypeT is std::false_type, e.g. volume for a
         * segment.
         *
         * This constructor should never be called but is required to make the code compile.
         *
         */
        [[noreturn]] LocalInterface(std::false_type, ::MoReFEM::InterfaceNS::Nature nature, IsInterior is_interior);

        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        //! Destructor.
        ~LocalInterface() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LocalInterface(const LocalInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LocalInterface(LocalInterface&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        LocalInterface& operator=(const LocalInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LocalInterface& operator=(LocalInterface&& rhs) = delete;

        ///@}

        /*!
         * \brief Get the list of vertices that delimits the interface.
         *
         * This list can't be required for an interior interface, which is a very special case.
         *
         * \return List of vertices that delimits the interface.
         */
        const std::vector<std::size_t>& GetVertexIndexList() const noexcept;

        //! Get the nature of the interface.
        const ::MoReFEM::InterfaceNS::Nature& GetNature() const noexcept;


      private:
        //! Constant accessor to the whether the interface is an interior interface or not.
        IsInterior GetIsInterior() const noexcept;

      private:
        /*!
         * \brief List of vertices that delimits the interface, given by their index.
         *
         * The index here are the ones used in the description of the topology; so for instance for a triangle
         * it is among {0, 1, 2} where the position of each of those is given in topologic description.
         */
        std::vector<std::size_t> vertex_index_list_;

        //! Nature of the interface.
        ::MoReFEM::InterfaceNS::Nature nature_;

        //! Whether the interface is an interior interface or not.
        const IsInterior is_interior_;
    };


} // namespace MoReFEM::Advanced::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/LocalInterface/Advanced/LocalInterface.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_ADVANCED_LOCALINTERFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
