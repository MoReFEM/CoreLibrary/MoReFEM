// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_ADVANCED_LOCALINTERFACE_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_ADVANCED_LOCALINTERFACE_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/LocalInterface/Advanced/LocalInterface.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/Interfaces/EnumInterface.hpp"


namespace MoReFEM::Advanced::InterfaceNS
{


    template<class VertexIndexListTypeT>
    LocalInterface::LocalInterface(const VertexIndexListTypeT& vertex_index_list,
                                   ::MoReFEM::InterfaceNS::Nature nature,
                                   const IsInterior is_interior)
    : nature_(nature), is_interior_(is_interior)
    {
        static_assert(!std::is_same<VertexIndexListTypeT, std::false_type>(),
                      "Should be handled by a dedicated constructor.");

#ifndef NDEBUG
        switch (GetNature())
        {
        case ::MoReFEM::InterfaceNS::Nature::vertex:
        case ::MoReFEM::InterfaceNS::Nature::edge:
        case ::MoReFEM::InterfaceNS::Nature::face:
        case ::MoReFEM::InterfaceNS::Nature::volume:
            break;
        case ::MoReFEM::InterfaceNS::Nature::none:
        case ::MoReFEM::InterfaceNS::Nature::undefined:
            assert(false && "Invalid nature for a local interface (these values are used for straight interfaces");
        }
#endif // NDEBUG

        // assert(!vertex_index_list.empty()); No: might be empty in case interior interface is being defined.
        vertex_index_list_.resize(vertex_index_list.size());

        std::copy(vertex_index_list.cbegin(), vertex_index_list.cend(), vertex_index_list_.begin());
    }


    inline const std::vector<std::size_t>& LocalInterface::GetVertexIndexList() const noexcept
    {
#ifndef NDEBUG
        switch (GetNature())
        {
        case ::MoReFEM::InterfaceNS::Nature::vertex:
        case ::MoReFEM::InterfaceNS::Nature::edge:
        case ::MoReFEM::InterfaceNS::Nature::face:
            break;
        case ::MoReFEM::InterfaceNS::Nature::none:
        case ::MoReFEM::InterfaceNS::Nature::undefined:
        case ::MoReFEM::InterfaceNS::Nature::volume:
            assert(false && "Should not be called for either of these natures!");
        }
#endif // NDEBUG

        assert(!GetIsInterior()
               && "Should not be called for an interior interface. For such a case, simply take "
                  "all vertices of the geometric element...");

        return vertex_index_list_;
    }


    inline const ::MoReFEM::InterfaceNS::Nature& LocalInterface::GetNature() const noexcept
    {
        return nature_;
    }


    inline IsInterior LocalInterface::GetIsInterior() const noexcept
    {
        return is_interior_;
    }


} // namespace MoReFEM::Advanced::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_ADVANCED_LOCALINTERFACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
