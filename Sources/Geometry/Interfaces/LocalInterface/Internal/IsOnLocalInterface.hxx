// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_INTERNAL_ISONLOCALINTERFACE_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_INTERNAL_ISONLOCALINTERFACE_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::InterfaceNS
{


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    inline bool IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>::GetVertexCoord(vertex_index) == coords;
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    bool IsOnEdge_Spectral(std::size_t edge_index, const LocalCoords& coords)
    {
        using local_data_type = ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>;

        // Extract the vertices that delimits the edge.
        const auto& vertex_on_edge = local_data_type::GetEdge(edge_index);
        assert(vertex_on_edge.size() == 2UL);

        // Then their coordinates.
        const LocalCoords& coords_vertex_1 = local_data_type::GetVertexCoord(vertex_on_edge[0]);
        const LocalCoords& coords_vertex_2 = local_data_type::GetVertexCoord(vertex_on_edge[1]);

        // See which is the component that vary between both vertices.
        const auto mismatched_component = ExtractMismatchedComponentIndex(coords_vertex_1, coords_vertex_2);

        constexpr auto Ncomponent = TopologyT::dimension;
        assert(Ncomponent == coords.GetDimension());
        assert(Ncomponent == coords_vertex_1.GetDimension());

        // The other components must remains equal.
        for (::MoReFEM::GeometryNS::dimension_type i{ 0 }; i < Ncomponent; ++i)
        {
            if (i == mismatched_component)
            {
                const double min = std::min(coords_vertex_1[i], coords_vertex_2[i]);
                const double max = std::max(coords_vertex_1[i], coords_vertex_2[i]);

                // Check whether the coordinates in between vertices.
                if (coords[i] < min || coords[i] > max)
                    return false;

                continue;
            }

            if (!NumericNS::AreEqual(coords[i], coords_vertex_1[i]))
                return false;
        }

        return true;
    }


} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_INTERNAL_ISONLOCALINTERFACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
