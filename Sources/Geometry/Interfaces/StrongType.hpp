// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_STRONGTYPE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_STRONGTYPE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>                                                   // IWYU pragma: keep
                                                                     // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::InterfaceNS
{

    //! Strong type for displacement vectors.
    //! \copydetails doxygen_hide_strong_type_quick_explanation
    // clang-format off
    using program_wise_index_type =
        StrongType
        <
            std::size_t,
            struct program_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Hashable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::AsMpiDatatype,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_STRONGTYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
