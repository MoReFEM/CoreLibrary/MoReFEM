// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <fstream>
#include <functional>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Geometry/Interpolator/CoordsMatching.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"


namespace MoReFEM::MeshNS::InterpolationNS
{

    namespace // anonymous
    {

        const std::string& MeshesLineMoniker()
        {
            static const std::string ret{ "Meshes:" };
            return ret;
        }


    } // namespace


    CoordsMatching::CoordsMatching(const FilesystemNS::File& interpolation_file)
    {
        Read(interpolation_file);
    }


    const std::string& CoordsMatching::ClassName()
    {
        static const std::string ret("MeshNS::InterpolationNS::CoordsMatching");
        return ret;
    }


    void CoordsMatching::Read(const ::MoReFEM::FilesystemNS::File& file)
    {
        source_to_target_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        target_to_source_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        std::ifstream in{ file.Read() };

        std::string line;

        while (getline(in, line))
        {
            Utilities::String::Strip(line);

            // Ignore empty lines and comment lines.
            if (line.empty())
                continue;

            if (Utilities::String::StartsWith(line, "#"))
                continue;

            if (Utilities::String::StartsWith(line, MeshesLineMoniker()))
            {
                SetMeshes(file, line);
                continue;
            }

            // A valid line should include two integers, that are vertex indexes on each of the mesh.
            std::istringstream iconv(line);

            std::size_t tmp = 0;

            iconv >> tmp;
            const ::MoReFEM::CoordsNS::index_from_mesh_file new_source_index(tmp);

            if (iconv.fail())
            {
                std::ostringstream oconv;
                oconv << "Invalid line in interpolation file " << file << ": \n" << line << '\n';
                throw Exception(oconv.str());
            }

            iconv >> tmp;
            const ::MoReFEM::CoordsNS::index_from_mesh_file new_target_index(tmp);

            if (iconv.fail())
            {
                std::ostringstream oconv;
                oconv << "Invalid line in interpolation file " << file << ": \n" << line << '\n';
                throw Exception(oconv.str());
            }

            if (!iconv.eof())
            {
                std::ostringstream oconv;
                oconv << "Invalid line in interpolation file " << file << ": \n" << line << '\n';
                throw Exception(oconv.str());
            }

            {
                auto [iterator, is_properly_inserted] =
                    source_to_target_.insert({ new_source_index, new_target_index });

                if (!is_properly_inserted)
                {
                    std::ostringstream oconv;
                    oconv << "Invalid interpolation file " << file << ": source value " << new_source_index
                          << " was found twice!";

                    throw Exception(oconv.str());
                }
            }

            {
                auto [iterator, is_properly_inserted] =
                    target_to_source_.insert({ new_target_index, new_source_index });

                if (!is_properly_inserted)
                {
                    std::ostringstream oconv;
                    oconv << "Invalid interpolation file " << file << ": target value " << new_target_index
                          << " was found twice!";

                    throw Exception(oconv.str());
                }
            }
        } // while

        if (std::ranges::any_of(mesh_ids_,

                                [](const auto id)
                                {
                                    return id == NumericNS::UninitializedIndex<decltype(id)>();
                                }))
        {
            std::ostringstream oconv;
            oconv << "Invalid interpolation file " << file
                  << ": the meshes id were not properly set "
                     "(there should be a line '"
                  << MeshesLineMoniker()
                  << "a b' where a and b are the integer unique "
                     "ids of the meshes involved)";

            throw Exception(oconv.str());
        }
    }


    ::MoReFEM::CoordsNS::index_from_mesh_file
    CoordsMatching ::FindSourceIndex(::MoReFEM::CoordsNS::index_from_mesh_file target_index) const
    {
        decltype(auto) target_to_source = GetTargetToSource();
        auto it = target_to_source.find(target_index);

        assert(it != target_to_source.cend()
               && "Check your interpolation file (given in input data file) "
                  "is correct... ");

        return it->second;
    }


    ::MoReFEM::CoordsNS::index_from_mesh_file
    CoordsMatching ::FindTargetIndex(::MoReFEM::CoordsNS::index_from_mesh_file source_index) const
    {
        decltype(auto) source_to_target = GetSourceToTarget();
        auto it = source_to_target.find(source_index);

        assert(it != source_to_target.cend()
               && "Check your interpolation file (given in input data file) "
                  "is correct... ");

        return it->second;
    }


    void CoordsMatching::SetMeshes(const ::MoReFEM::FilesystemNS::File& file, std::string line)
    {
        assert(Utilities::String::StartsWith(line, MeshesLineMoniker())
               && "It's an assert as this condition is the "
                  "only case in which this method should be invoked");

        if (!std::ranges::all_of(mesh_ids_,

                                 [](const auto id)
                                 {
                                     return id == NumericNS::UninitializedIndex<decltype(id)>();
                                 }))
        {
            std::ostringstream oconv;
            oconv << "Invalid interpolation file " << file
                  << ": the meshes id are read at least twice in "
                     "the interpolation file (there should be only one line with the format '"
                  << MeshesLineMoniker() << "a b' where a and b are the integer unique ids of the meshes involved)";

            throw Exception(oconv.str());
        }

        line.erase(0, MeshesLineMoniker().size());

        std::size_t mesh_id_read = 0;

        std::istringstream iconv(line);
        iconv >> mesh_id_read;
        mesh_ids_[0] = ::MoReFEM::MeshNS::unique_id{ mesh_id_read };

        if (iconv.fail())
        {
            std::ostringstream oconv;
            oconv << "Invalid line in interpolation file " << file
                  << ": the line giving the meshes "
                     "involved couldn't be interpreted properly.";
            throw Exception(oconv.str());
        }

        iconv >> mesh_id_read;
        mesh_ids_[1] = ::MoReFEM::MeshNS::unique_id{ mesh_id_read };

        if (iconv.fail())
        {
            std::ostringstream oconv;
            oconv << "Invalid line in interpolation file " << file
                  << ": the line giving the meshes "
                     "involved couldn't be interpreted properly.";
            throw Exception(oconv.str());
        }

        if (mesh_ids_[0] == mesh_ids_[1])
        {
            std::ostringstream oconv;
            oconv << "Invalid line in interpolation file " << file << ": both meshes read were identical.";
            throw Exception(oconv.str());
        }

        if (!iconv.eof())
        {
            std::ostringstream oconv;
            oconv << "Invalid line in interpolation file " << file << ": \n" << line << "\n";
            throw Exception(oconv.str());
        }
    }


    std::vector<CoordsNS::index_from_mesh_file>
    CoordsMatching::FindSourceIndex(const std::vector<CoordsNS::index_from_mesh_file>& target_indexes) const
    {
        std::vector<CoordsNS::index_from_mesh_file> ret(target_indexes.size());

        std::ranges::transform(target_indexes,

                               ret.begin(),
                               [this](CoordsNS::index_from_mesh_file target_index)
                               {
                                   return this->FindSourceIndex(target_index);
                               });

        Internal::InterfaceNS::OrderCoordsList(ret, std::less<>());

        return ret;
    }


    std::vector<CoordsNS::index_from_mesh_file>
    CoordsMatching::FindTargetIndex(const std::vector<CoordsNS::index_from_mesh_file>& source_indexes) const
    {
        std::vector<CoordsNS::index_from_mesh_file> ret(source_indexes.size());

        std::ranges::transform(source_indexes,

                               ret.begin(),
                               [this](CoordsNS::index_from_mesh_file source_index)
                               {
                                   return this->FindTargetIndex(source_index);
                               });

        Internal::InterfaceNS::OrderCoordsList(ret, std::less<>());


        return ret;
    }


    CoordsMatching::const_unique_ptr CoordsMatching::GenerateReverseCoordsMatching() const
    {
        // Not using WrapUnique() here, as:
        // - Return type is a const_unique_ptr
        // - Before wrapping it the object is modified.
        auto* ret = new CoordsMatching(*this);

        auto& object = *ret;
        std::swap(object.source_index_list_, object.target_index_list_);
        std::swap(object.source_to_target_, object.target_to_source_);
        std::swap(object.mesh_ids_[0], object.mesh_ids_[1]);

        return CoordsMatching::const_unique_ptr(ret);
    }


} // namespace MoReFEM::MeshNS::InterpolationNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
