// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERPOLATOR_COORDSMATCHING_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERPOLATOR_COORDSMATCHING_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interpolator/CoordsMatching.hpp"
// *** MoReFEM header guards *** < //


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Geometry/CoordsMatchingFile.hpp" // IWYU pragma: export

#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM::MeshNS::InterpolationNS
{


    inline const CoordsMatching::mapping_type& CoordsMatching::GetSourceToTarget() const noexcept
    {
        assert(source_to_target_.size() == target_to_source_.size());
        return source_to_target_;
    }


    inline const CoordsMatching::mapping_type& CoordsMatching::GetTargetToSource() const noexcept
    {
        assert(source_to_target_.size() == target_to_source_.size());
        return target_to_source_;
    }


    inline MeshNS::unique_id CoordsMatching::GetSourceMeshId() const noexcept
    {
        assert(mesh_ids_[0] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        return mesh_ids_[0];
    }


    inline MeshNS::unique_id CoordsMatching::GetTargetMeshId() const noexcept
    {
        assert(mesh_ids_[1] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        return mesh_ids_[1];
    }


    inline const std::array<MeshNS::unique_id, 2UL>& CoordsMatching::GetMeshIds() const noexcept
    {
        assert(mesh_ids_[0] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        assert(mesh_ids_[1] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        return mesh_ids_;
    }


} // namespace MoReFEM::MeshNS::InterpolationNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERPOLATOR_COORDSMATCHING_DOT_HXX_
// *** MoReFEM end header guards *** < //
