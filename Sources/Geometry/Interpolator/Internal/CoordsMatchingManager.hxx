// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERPOLATOR_INTERNAL_COORDSMATCHINGMANAGER_DOT_HXX_
#define MOREFEM_GEOMETRY_INTERPOLATOR_INTERNAL_COORDSMATCHINGMANAGER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <filesystem>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"


namespace MoReFEM::Internal::MeshNS
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void CoordsMatchingManager ::Create(const IndexedSectionDescriptionT&,
                                        const ModelSettingsT& model_settings,
                                        const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) path = ::MoReFEM::FilesystemNS::File{ std::filesystem::path{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Path>(model_settings, input_data) } };

        Create(
            path,
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::DoComputeReverse>(model_settings, input_data));
    }


    inline auto CoordsMatchingManager::GetList() const -> const managed_type::vector_const_unique_ptr&
    {
        return list_;
    }


    inline auto CoordsMatchingManager::GetNonCstList() -> managed_type::vector_const_unique_ptr&
    {
        return const_cast<managed_type::vector_const_unique_ptr&>(GetList());
    }

} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERPOLATOR_INTERNAL_COORDSMATCHINGMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
