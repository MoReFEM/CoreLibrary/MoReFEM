### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/CoordsMatching.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/CoordsMatching.hpp
		${CMAKE_CURRENT_LIST_DIR}/CoordsMatching.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
