// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_ADVANCED_DISTANCEFROMMESH_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_ADVANCED_DISTANCEFROMMESH_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM
{
    class Mesh;
}

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


namespace MoReFEM::Advanced::MeshNS
{


    /*!
     * \brief Class in charge to compute the distance between a point and a mesh.
     *
     */
    class DistanceFromMesh final
    {

      public:
        //! Alias over shared_ptr.
        using unique_ptr = std::unique_ptr<DistanceFromMesh>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        explicit DistanceFromMesh();

        //! Destructor.
        ~DistanceFromMesh() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DistanceFromMesh(const DistanceFromMesh& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        DistanceFromMesh(DistanceFromMesh&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DistanceFromMesh& operator=(const DistanceFromMesh& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DistanceFromMesh& operator=(DistanceFromMesh&& rhs) = delete;


        ///@}

        /*!
         * \brief Computes the distance between a point and a given mesh.
         *
         * \param[in] point Point to compute the distance.
         * \param[in] mesh Surface definition with Triangle3, Edges and Points.
         * \param[in] point_projection Projection of the point on the mesh.
         * \param[in] normal_on_point_projection Normal to the mesh at the projection point.
         *
         * \return Distance between a point and a given mesh.
         */
        double ComputeDistance(const SpatialPoint& point,
                               const Mesh& mesh,
                               SpatialPoint& point_projection,
                               SpatialPoint& normal_on_point_projection);

        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // ============================

      private:
        /*!
         * \brief Internal functions to determine the region.
         *
         * \param[in] point_on_plane Projectted point ont the triangle plane.
         * \param[in] point1 First point of the triangle.
         * \param[in] point2 Second point of the triangle.
         * \param[in] point3 Third point of the triangle.
         * \param[in] k Case.
         * \param[in] region Region of the triangle (triangle cut in 7 regions).
         * \param[in] point_close Closest point of the triangle of the projection point.
         *
         * \return region of the triangle.
         */
        std::size_t DistanceFromTriangleCaseK(const SpatialPoint& point_on_plane,
                                              const SpatialPoint& point1,
                                              const SpatialPoint& point2,
                                              const SpatialPoint& point3,
                                              const std::size_t k,
                                              std::size_t region,
                                              SpatialPoint& point_close);

      private:
        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPointOnPlane() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPointOnPlane() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPointOnPlanePoint() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPointOnPlanePoint() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPointClose() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPointClose() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPointPoint1() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPointPoint1() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint1Point() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint1Point() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint1Point2() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint1Point2() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint1Point3() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint1Point3() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint1PointOnPlane() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint1PointOnPlane() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint3Proj() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint3Proj() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPointProj() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPointProj() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint3Point3Proj() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint3Point3Proj() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPointPointProj() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPointPointProj() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint1PointProj() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint1PointProj() noexcept;

        //! Constant accessor to the internal coord to help compute the distance.
        const SpatialPoint& GetPoint2PointProj() const noexcept;

        //! Non constant accessor to the internal coord to help compute the distance.
        SpatialPoint& GetNonCstPoint2PointProj() noexcept;

      private:
        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point_on_plane_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point_on_plane_point_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point_close_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point_point1_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point1_point2_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point1_point3_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point1_point_on_plane_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point3_proj_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point_proj_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point3_point3_proj_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point_point_proj_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point1_point_proj_ = nullptr;

        //! Internal coord to help compute the distance.
        SpatialPoint::unique_ptr point2_point_proj_ = nullptr;

        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================
    };


} // namespace MoReFEM::Advanced::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Mesh/Advanced/DistanceFromMesh.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_ADVANCED_DISTANCEFROMMESH_DOT_HPP_
// *** MoReFEM end header guards *** < //
