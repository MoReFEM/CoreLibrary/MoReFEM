// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_ADVANCED_DISTANCEFROMMESH_DOT_HXX_
#define MOREFEM_GEOMETRY_MESH_ADVANCED_DISTANCEFROMMESH_DOT_HXX_
// IWYU pragma: private, include "Geometry/Mesh/Advanced/DistanceFromMesh.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class SpatialPoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::MeshNS
{


    inline const SpatialPoint& DistanceFromMesh::GetPointOnPlane() const noexcept
    {
        assert(!(!point_on_plane_));
        return *point_on_plane_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPointOnPlane() noexcept
    {
        return const_cast<SpatialPoint&>(GetPointOnPlane());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPointOnPlanePoint() const noexcept
    {
        assert(!(!point_on_plane_point_));
        return *point_on_plane_point_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPointOnPlanePoint() noexcept
    {
        return const_cast<SpatialPoint&>(GetPointOnPlanePoint());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPointClose() const noexcept
    {
        assert(!(!point_close_));
        return *point_close_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPointClose() noexcept
    {
        return const_cast<SpatialPoint&>(GetPointClose());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPointPoint1() const noexcept
    {
        assert(!(!point_point1_));
        return *point_point1_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPointPoint1() noexcept
    {
        return const_cast<SpatialPoint&>(GetPointPoint1());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPoint1Point2() const noexcept
    {
        assert(!(!point1_point2_));
        return *point1_point2_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPoint1Point2() noexcept
    {
        return const_cast<SpatialPoint&>(GetPoint1Point2());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPoint1Point3() const noexcept
    {
        assert(!(!point1_point3_));
        return *point1_point3_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPoint1Point3() noexcept
    {
        return const_cast<SpatialPoint&>(GetPoint1Point3());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPoint1PointOnPlane() const noexcept
    {
        assert(!(!point1_point_on_plane_));
        return *point1_point_on_plane_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPoint1PointOnPlane() noexcept
    {
        return const_cast<SpatialPoint&>(GetPoint1PointOnPlane());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPoint3Proj() const noexcept
    {
        assert(!(!point3_proj_));
        return *point3_proj_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPoint3Proj() noexcept
    {
        return const_cast<SpatialPoint&>(GetPoint3Proj());
    }

    inline const SpatialPoint& DistanceFromMesh::GetPointProj() const noexcept
    {
        assert(!(!point_proj_));
        return *point_proj_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPointProj() noexcept
    {
        return const_cast<SpatialPoint&>(GetPointProj());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPoint3Point3Proj() const noexcept
    {
        assert(!(!point3_point3_proj_));
        return *point3_point3_proj_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPoint3Point3Proj() noexcept
    {
        return const_cast<SpatialPoint&>(GetPoint3Point3Proj());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPointPointProj() const noexcept
    {
        assert(!(!point_point_proj_));
        return *point_point_proj_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPointPointProj() noexcept
    {
        return const_cast<SpatialPoint&>(GetPointPointProj());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPoint1PointProj() const noexcept
    {
        assert(!(!point1_point_proj_));
        return *point1_point_proj_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPoint1PointProj() noexcept
    {
        return const_cast<SpatialPoint&>(GetPoint1PointProj());
    }


    inline const SpatialPoint& DistanceFromMesh::GetPoint2PointProj() const noexcept
    {
        assert(!(!point2_point_proj_));
        return *point2_point_proj_;
    }


    inline SpatialPoint& DistanceFromMesh::GetNonCstPoint2PointProj() noexcept
    {
        return const_cast<SpatialPoint&>(GetPoint2PointProj());
    }


} // namespace MoReFEM::Advanced::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_ADVANCED_DISTANCEFROMMESH_DOT_HXX_
// *** MoReFEM end header guards *** < //
