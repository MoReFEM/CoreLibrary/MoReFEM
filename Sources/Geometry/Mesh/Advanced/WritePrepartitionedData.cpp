// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <fstream>
#include <map>
#include <memory>
#include <sstream>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Associative.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Advanced::MeshNS
{


    namespace // anonymous
    {


        template<::MoReFEM::MeshNS::Format FormatT>
        using Information = ::MoReFEM::Internal::MeshNS::FormatNS::Information<FormatT>;


        //! \param[in,out] reduced_mesh_path In input, the path to the reduced mesh file without its extension (the '.'
        //! however is already there. In
        //!  output, the extension is properly added.
        template<::MoReFEM::MeshNS::Format FormatT>
        void WriteFormat(const Mesh& mesh, std::ofstream& out, FilesystemNS::File& reduced_mesh_path);


        template<::MoReFEM::MeshNS::Format FormatT>
        void WriteFormatSpecificInformation(const Mesh& mesh, std::ofstream& out);

    } // namespace


    WritePrepartitionedData::WritePrepartitionedData(const Mesh& mesh,
                                                     const FilesystemNS::Directory& output_directory,
                                                     ::MoReFEM::MeshNS::Format format)
    {


        partition_data_file_ = output_directory.AddFile("mesh_data.lua");

        if (partition_data_file_.DoExist())
        {
            std::ostringstream oconv;
            oconv << "File " << partition_data_file_ << " already exists!";
            throw Exception(oconv.str());
        }

        std::ofstream out{ partition_data_file_.NewContent() };

        std::ostringstream oconv;
        oconv << output_directory << "/mesh" << '.';
        reduced_mesh_file_ = output_directory.AddFile("mesh.");

        using namespace ::MoReFEM::MeshNS;

        // Note: I could have made that more elegant by making the function template, but I didn't deem it that
        // useful.
        switch (format)
        {
        case ::MoReFEM::MeshNS::Format::Medit:
        {
            WriteFormat<::MoReFEM::MeshNS::Format::Medit>(mesh, out, reduced_mesh_file_);
            break;
        }
        case ::MoReFEM::MeshNS::Format::Ensight:
        {
            WriteFormat<::MoReFEM::MeshNS::Format::Ensight>(mesh, out, reduced_mesh_file_);
            break;
        }
        case ::MoReFEM::MeshNS::Format::VTK_PolygonalData:
        case ::MoReFEM::MeshNS::Format::Vizir:
        case ::MoReFEM::MeshNS::Format::End:
        {
            throw Exception("Format not supported for pre-partition.");
        }
        } // switch


        {
            out << "-- Path to the reduced mesh" << '\n';
            out << "mesh_file = \"" << reduced_mesh_file_ << "\"\n\n";

            out << "Nprocessor_wise_coord = " << mesh.NprocessorWiseCoord() << '\n';
            out << "Nghost_coord = " << mesh.NghostCoord() << '\n';
            out << "space_unit = " << mesh.GetSpaceUnit() << '\n';
            out << "Nprocessor_wise_vertex = " << mesh.Nvertex<RoleOnProcessor::processor_wise>() << '\n';
            out << "Nprogram_wise_vertex = " << mesh.NprogramWiseVertex() << '\n';
            out << "Nprocessor_wise_edge = " << mesh.Nedge<RoleOnProcessor::processor_wise>() << '\n';
            out << "Nprocessor_wise_face = " << mesh.Nface<RoleOnProcessor::processor_wise>() << '\n';
            out << "Nprocessor_wise_volume = " << mesh.Nvolume<RoleOnProcessor::processor_wise>() << '\n';
            out << "do_build_edges = " << (mesh.AreEdgesBuilt() ? "true" : "false") << '\n';
            out << "do_build_faces = " << (mesh.AreFacesBuilt() ? "true" : "false") << '\n';
            out << "do_build_volumes = " << (mesh.AreVolumesBuilt() ? "true" : "false") << '\n';


            {
                decltype(auto) list = mesh.ComputeProcessorWiseAndGhostGeometricEltList();

                std::vector<::MoReFEM::GeomEltNS::index_type> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetIndex());
                }

                out << "\n-- List of the indexes of the GeometricElt (both the processor-wise and the ghost ones - the "
                       "processor-wise ones are written first)"
                    << '\n';
                out << "geometric_elt_index_list = ";
                Utilities::PrintContainer<>::Do(index_list,
                                                out,
                                                PrintNS::Delimiter::separator(", "),
                                                PrintNS::Delimiter::opener("{"),
                                                PrintNS::Delimiter::closer("}"));

                out << '\n';
            }

            {
                decltype(auto) list = mesh.GetProcessorWiseCoordsList();

                std::vector<::MoReFEM::CoordsNS::program_wise_position> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetProgramWisePosition());
                }

                out << "\n-- The program wise indexes of the processor-wise Coords" << '\n';
                out << "program_wise_coords_index_list_proc = ";
                Utilities::PrintContainer<>::Do(index_list,
                                                out,
                                                PrintNS::Delimiter::separator(", "),
                                                PrintNS::Delimiter::opener("{"),
                                                PrintNS::Delimiter::closer("}"));

                out << '\n';
            }

            {
                decltype(auto) list = mesh.GetGhostCoordsList();

                std::vector<::MoReFEM::CoordsNS::program_wise_position> index_list;
                index_list.reserve(list.size());

                for (const auto& ptr : list)
                {
                    assert(!(!ptr));
                    index_list.push_back(ptr->GetProgramWisePosition());
                }

                out << "\n-- The program wise indexes of the ghost Coords" << '\n';
                out << "program_wise_coords_index_list_ghost = ";
                Utilities::PrintContainer<>::Do(index_list,
                                                out,
                                                PrintNS::Delimiter::separator(", "),
                                                PrintNS::Delimiter::opener("{"),
                                                PrintNS::Delimiter::closer("}"));

                out << '\n' << '\n';
            }

            {
                const Internal::MeshNS::ComputeInterfaceListInMesh original_interface_list(mesh);

                {
                    decltype(auto) list = original_interface_list.GetVertexList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nvertex (processor-wise + ghost) = " << index_list.size() << '\n';
                    out << "vertex_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << '\n' << '\n';
                }

                {
                    decltype(auto) list = original_interface_list.GetEdgeList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nedge (processor-wise + ghost) = " << index_list.size() << '\n';
                    out << "edge_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << '\n' << '\n';
                }

                {
                    decltype(auto) list = original_interface_list.GetFaceList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nface (processor-wise + ghost) = " << index_list.size() << '\n';
                    out << "face_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << '\n' << '\n';
                }

                {
                    decltype(auto) list = original_interface_list.GetVolumeList();

                    std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> index_list;
                    index_list.reserve(list.size());

                    for (const auto& ptr : list)
                    {
                        assert(!(!ptr));
                        index_list.push_back(ptr->GetProgramWiseIndex());
                    }

                    out << "-- Nvolume (processor-wise + ghost) = " << index_list.size() << '\n';
                    out << "volume_index_list = ";
                    Utilities::PrintContainer<>::Do(index_list,
                                                    out,
                                                    PrintNS::Delimiter::separator(", "),
                                                    PrintNS::Delimiter::opener("{"),
                                                    PrintNS::Delimiter::closer("}"));

                    out << '\n' << '\n';
                }
            }
        }
    }


    namespace // anonymous
    {


        template<::MoReFEM::MeshNS::Format FormatT>
        void WriteFormat(const Mesh& mesh, std::ofstream& out, FilesystemNS::File& reduced_mesh_path)
        {
            WriteFormatSpecificInformation<FormatT>(mesh, out);

            std::filesystem::path path = reduced_mesh_path.GetDirectoryEntry().path();
            path.replace_extension(Information<FormatT>::Extension());

            const FilesystemNS::File with_extension{ std::move(path) };

            mesh.Write<FormatT>(with_extension);

            reduced_mesh_path = with_extension;
        }


        template<>
        void WriteFormatSpecificInformation<::MoReFEM::MeshNS::Format::Medit>(const Mesh& mesh, std::ofstream& out)
        {
            out << "-- Medit needs to know how many geometric elements were processor-wise for each type to be "
                   "able to reconstruct properly from prepartitioned data - as the partial mesh used contains both "
                   "processor-wise and ghost data."
                << '\n';

            out << "Nprocessor_wise_geom_elt_per_type = ";

            using lua_map_policy =
                Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;

            decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();
            std::map<std::size_t, std::size_t> Nprocessor_wise_geom_elt_per_type;

            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                const auto Nelt_of_type = mesh.NgeometricElt<RoleOnProcessor::processor_wise>(ref_geom_elt);

                [[maybe_unused]] auto [it, is_inserted] = Nprocessor_wise_geom_elt_per_type.insert(
                    std::pair(ref_geom_elt.GetMeditIdentifier(), Nelt_of_type));

                assert(is_inserted);
            }

            Utilities::PrintContainer<lua_map_policy>::Do(Nprocessor_wise_geom_elt_per_type,
                                                          out,
                                                          ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                          ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                          ::MoReFEM::PrintNS::Delimiter::closer(" }\n\n"));
        }


        template<>
        void WriteFormatSpecificInformation<::MoReFEM::MeshNS::Format::Ensight>(const Mesh& mesh, std::ofstream& out)
        {
            out << "-- Ensight needs to know how many geometric elements were processor-wise for each combination "
                   "type / mesh label to be able to reconstruct properly from prepartitioned data - as the partial "
                   "mesh "
                   "used contains both processor-wise and ghost data."
                << '\n';

            out << "Nprocessor_wise_geom_elt_per_type_and_label = ";

            using lua_map_policy =
                Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;

            decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

            std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t> Nprocessor_wise_geom_elt_per_type_and_label;

            decltype(auto) mesh_label_list = mesh.GetLabelList();

            for (const auto& mesh_label_ptr : mesh_label_list)
            {
                assert(!(!mesh_label_ptr));
                const auto& mesh_label = *mesh_label_ptr;

                for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
                {
                    assert(!(!ref_geom_elt_ptr));
                    const auto& ref_geom_elt = *ref_geom_elt_ptr;

                    const auto Nelt_of_type =
                        mesh.NgeometricElt<RoleOnProcessor::processor_wise>(ref_geom_elt, mesh_label);

                    if (Nelt_of_type == 0)
                        continue;

                    const Internal::MeshNS::RefGeomEltAndMeshLabel ref_geom_elt_and_mesh_label(ref_geom_elt,
                                                                                               mesh_label);

                    [[maybe_unused]] auto [it, is_inserted] = Nprocessor_wise_geom_elt_per_type_and_label.insert(
                        std::pair(ref_geom_elt_and_mesh_label, Nelt_of_type));

                    assert(is_inserted);
                }
            }

            Utilities::PrintContainer<lua_map_policy>::Do(Nprocessor_wise_geom_elt_per_type_and_label,
                                                          out,
                                                          ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                          ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                          ::MoReFEM::PrintNS::Delimiter::closer(" }\n\n"));
        }


    } // namespace


} // namespace MoReFEM::Advanced::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
