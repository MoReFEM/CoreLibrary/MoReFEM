// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_ADVANCED_WRITEPREPARTITIONEDDATA_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_ADVANCED_WRITEPREPARTITIONEDDATA_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Utilities/Filesystem/File.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::MeshNS { enum class Format; }
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::MeshNS
{


    /*!
     * \brief This lightweight class writes on disk the data that will be required to a further run parallel run
     * from the data computed here.
     *
     * If the parallelism strategy is "Precompute", the computation will stop shortly after this class enters the
     * game. If "Parallel", it will do the parallel computation after these data are written. The class isn't used
     * for the others parallelism strategies.
     *
     * This is mostly a function - I made it a class just to provide user-friendly access to the generated
     * files.
     */
    class WritePrepartitionedData
    {

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh \a Mesh which partitioning will be written on disk.
         * \param[in] output_directory Directory into which the data are written for a given \a Mesh and a given
         * mpi rank. Typically something like /Volumes/Data/${USER}/MoReFEM/Results/MyModel/Rank_0/Mesh_1.
         * \param[in] format Format into which write the reduced mesh. Should be the same as the format used
         * to read the original mesh (this way you're sure the operation is possible - some types of geometric
         * element are not supported by all formats).
         *
         */
        explicit WritePrepartitionedData(const Mesh& mesh,
                                         const FilesystemNS::Directory& output_directory,
                                         ::MoReFEM::MeshNS::Format format);

        //! Destructor.
        ~WritePrepartitionedData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        WritePrepartitionedData(const WritePrepartitionedData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        WritePrepartitionedData(WritePrepartitionedData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        WritePrepartitionedData& operator=(const WritePrepartitionedData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        WritePrepartitionedData& operator=(WritePrepartitionedData&& rhs) = delete;

        ///@}

      public:
        //! Path to the generated mesh for current rank.
        const FilesystemNS::File& GetReducedMeshFile() const noexcept;

        //! Path to the Lua file with the data required to rebuild the partitioned mesh.
        const FilesystemNS::File& GetPartitionDataFile() const noexcept;

      private:
        //! Path to the generated mesh for current rank.
        FilesystemNS::File reduced_mesh_file_;

        //! Path to the Lua file with the data required to rebuild the partitioned mesh.
        FilesystemNS::File partition_data_file_;
    };


} // namespace MoReFEM::Advanced::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_ADVANCED_WRITEPREPARTITIONEDDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
