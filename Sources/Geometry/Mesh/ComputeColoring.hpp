// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_COMPUTECOLORING_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_COMPUTECOLORING_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Geometry/GeometricElt/GeometricElt.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::Utilities::PointerComparison { template <class T> struct Equal; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Computes coloring for a \a mesh.
     *
     * The present algorithm attributes 'colors' to each GeometricElt so that two adjacent GeometricElt never
     * share the same color. As few colors as possible are used here.
     *
     * \param[in] mesh Mesh for which the coloring is computed.
     * \param[in] dimension Only geometric elements of a given dimension are considered here.
     * \param[out] Ngeometric_elt_with_color For each index/color, the number of geometric elements. For instance
     * the third element of the vector is the number of geometric elements with color 2.
     *
     * Beware: this algorithm acts processor-wise: if it is called after the reduction to processor-wise data,
     * it will actupon them alone and not on the whole initial object.
     *
     * \return The coloring attributed for each geometric elements.
     *
     * \internal <b><tt>[internal]</tt></b> Unordered map use the address and not the GeometricElt object in its hash
     * table.
     * \endinternal
     */
    std::unordered_map<GeometricElt::shared_ptr,
                       std::size_t,
                       std::hash<GeometricElt::shared_ptr>,
                       Utilities::PointerComparison::Equal<GeometricElt::shared_ptr>>
    ComputeColoring(const Mesh& mesh,
                    ::MoReFEM::GeometryNS::dimension_type dimension,
                    std::vector<std::size_t>& Ngeometric_elt_with_color);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_COMPUTECOLORING_DOT_HPP_
// *** MoReFEM end header guards *** < //
