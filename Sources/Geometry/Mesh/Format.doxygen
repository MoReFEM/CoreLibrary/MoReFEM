

/*!
 * \class doxygen_hide_geometry_format_read_common_arg
 *
 * \param[in] mesh The \a Mesh being build that called the present function. It is
 * there only because a reference to the mnesh is required to build a \a MeshLabel.
 * \param[in] file Path to the file being read.
 * \copydoc doxygen_hide_space_unit_arg
 * \param[out] dimension Number of components considered in the Coords objects. It is format_dependent: Ensight
 * always returns 3 regardless of rthe mesh considered whereas Medit may return 2 or 3.
 * \param[out] unsort_element_list List of geometric elements read in the file. No specific order is expected here;
 * \a Mesh will anyway reorder them for its own needs.
 * \param[out] coords_list List of \a Coords objects read in the file.
 * \param[out] mesh_label_list List of \a MeshLabels read in the file.
 */


/*!
 * \class doxygen_hide_geometry_format_write_common_arg
 *
 * \param[in] mesh Mesh to be written on the disk.
 * \param[in] mesh_file Path to the file into which the mesh is to be written.
 */



/*!
 * \class doxygen_hide_geometry_format_ensight_support
 *
 * \brief Provides relevant information for Ensight support.
 *
 * \internal <b><tt>[internal]</tt></b> The parent true_type is extremely important: the operator bool()
 * it defines is used within TGeometricElt to check whether the GeometricElt can be manipulated by
 * Ensight or not.
 */


/*!
 * \class doxygen_hide_geometry_format_medit_support
 *
 * \brief Provides relevant information for Medit support.
 *
 * \internal <b><tt>[internal]</tt></b> The parent true_type is extremely important: the operator bool()
 * it defines is used within TGeometricElt to check whether the GeometricElt can be manipulated by
 * Medit or not.
 */
