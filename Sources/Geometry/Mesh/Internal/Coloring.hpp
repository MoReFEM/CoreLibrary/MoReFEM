// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_COLORING_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_COLORING_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ColoringNS
{

    //! Convenient alias.
    using connecticity_helper_type = std::unordered_map<GeometricElt::shared_ptr,
                                                        GeometricElt::vector_shared_ptr,
                                                        std::hash<GeometricElt::shared_ptr>,
                                                        Utilities::PointerComparison::Equal<GeometricElt::shared_ptr>>;


    /*!
     * \brief Compute the connectivity of the geometric elements within the \a mesh.
     *
     * \param[in] mesh The mesh for which the connectivity is computed.
     * \param[in] dimension Consider only the geometric elements of this dimension.
     *
     * \return Key is each geometric element, value the list of contiguous geometric elements. Self connection
     * is rejected here.
     */
    connecticity_helper_type ComputeConnectivity(const Mesh& mesh, ::MoReFEM::GeometryNS::dimension_type dimension);


    //! A function to print the results of the connectivity, very useful in debug mode.
    //! \param[in] connectivity The computed connectivity.
    void PrintConnectivity(const connecticity_helper_type& connectivity);


    /*!
     * \brief Compute for each vertex the list of \a GeometricElt of a given dimension to which it belongs to.
     *
     * \param[in] mesh \a Mesh in which the computation is performed.
     * \param[in] dimension Only geometric elements of this dimension are considered.
     *
     * \return Key is the \a Vertex, value the list of \a GeometricElt to which the \a Vertex belongs to.
     */
    std::unordered_map<Vertex::shared_ptr, GeometricElt::vector_shared_ptr>
    ComputeGeometricElementForEachVertex(const Mesh& mesh, const ::MoReFEM::GeometryNS::dimension_type dimension);


} // namespace MoReFEM::Internal::ColoringNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_COLORING_DOT_HPP_
// *** MoReFEM end header guards *** < //
