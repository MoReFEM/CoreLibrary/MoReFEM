// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <memory>
#include <ostream>
#include <type_traits> // IWYU pragma: keep
#include <unordered_set>
#include <vector>

#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::MeshNS
{


    namespace // anonymous
    {


        template<class InterfaceT>
        void ComputeMeshLevelList(const typename InterfaceT::vector_shared_ptr& geom_elt_interface_list,
                                  std::unordered_set<::MoReFEM::InterfaceNS::program_wise_index_type>& set,
                                  typename InterfaceT::vector_shared_ptr& mesh_list);

        void ComputeMeshLevelListForVolume(const GeometricElt& geom_elt, Volume::vector_shared_ptr& mesh_volume_list);

        template<class InterfaceT>
        void PrintTypeOfInterface(const typename InterfaceT::vector_shared_ptr& interface_list, std::ostream& stream);


        //! Extract the proper index for tagging a \a Coords object.
        ::MoReFEM::CoordsNS::program_wise_position ExtractIndex(const Coords& coords);


        //! Returns a copy of the list with \a Interface sort by increasing program-wise index.
        template<class InterfaceT>
        typename InterfaceT::vector_shared_ptr
        SortPerProgramWiseIndex(typename InterfaceT::vector_shared_ptr interface_list);


    } // namespace


    ComputeInterfaceListInMesh::ComputeInterfaceListInMesh(const Mesh& mesh)
    {
        // The list of interfaces is not stored as such in mesh...
        vertex_list_.reserve(mesh.Nvertex<RoleOnProcessor::processor_wise>() + mesh.Nvertex<RoleOnProcessor::ghost>());
        edge_list_.reserve(mesh.Nedge<RoleOnProcessor::processor_wise>() + mesh.Nedge<RoleOnProcessor::ghost>());
        face_list_.reserve(mesh.Nface<RoleOnProcessor::processor_wise>() + mesh.Nface<RoleOnProcessor::ghost>());
        volume_list_.reserve(mesh.Nvolume<RoleOnProcessor::processor_wise>() + mesh.Nvolume<RoleOnProcessor::ghost>());

        std::unordered_set<std::size_t> mesh_vertex_index_list;
        std::unordered_set<std::size_t> mesh_edge_index_list;
        std::unordered_set<std::size_t> mesh_face_index_list;
        mesh_vertex_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
        mesh_edge_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
        mesh_face_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

        InterfaceIndexListManager index_list;

        ComputeForGeomEltList(mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>(), index_list);

        assert(vertex_list_.size() == mesh.Nvertex<RoleOnProcessor::processor_wise>());
        assert(edge_list_.size() == mesh.Nedge<RoleOnProcessor::processor_wise>());
        assert(face_list_.size() == mesh.Nface<RoleOnProcessor::processor_wise>());
        assert(volume_list_.size() == mesh.Nvolume<RoleOnProcessor::processor_wise>());

        ComputeForGeomEltList(mesh.GetGeometricEltList<RoleOnProcessor::ghost>(), index_list);

        assert(vertex_list_.size()
               == mesh.Nvertex<RoleOnProcessor::processor_wise>() + mesh.Nvertex<RoleOnProcessor::ghost>());

        assert(edge_list_.size()
               == mesh.Nedge<RoleOnProcessor::processor_wise>() + mesh.Nedge<RoleOnProcessor::ghost>());
        assert(face_list_.size()
               == mesh.Nface<RoleOnProcessor::processor_wise>() + mesh.Nface<RoleOnProcessor::ghost>());
        assert(volume_list_.size()
               == mesh.Nvolume<RoleOnProcessor::processor_wise>() + mesh.Nvolume<RoleOnProcessor::ghost>());
    }


    void ComputeInterfaceListInMesh::ComputeForGeomEltList(const GeometricElt::vector_shared_ptr& geom_elt_list,
                                                           InterfaceIndexListManager& interface_index_list_manager)
    {
        auto interface_without_orientation = [](const auto& oriented_interface_ptr)
        {
            assert(!(!oriented_interface_ptr));
            return oriented_interface_ptr->GetUnorientedInterfacePtr();
        };

        // Work variables. Should not lead to too much allocation: std::vector<>::clear() leaves the
        // capacity unchanged.
        Edge::vector_shared_ptr geom_elt_edge_list;
        Face::vector_shared_ptr geom_elt_face_list;


        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));

            const auto& geom_elt = *geom_elt_ptr;
            ComputeMeshLevelList<Vertex>(
                geom_elt.GetVertexList(), interface_index_list_manager.mesh_vertex_index_list, vertex_list_);

            geom_elt_edge_list.clear();
            decltype(auto) oriented_edge_list = geom_elt.GetOrientedEdgeList();

            std::ranges::transform(oriented_edge_list,

                                   std::back_inserter(geom_elt_edge_list),
                                   interface_without_orientation);

            ComputeMeshLevelList<Edge>(
                geom_elt_edge_list, interface_index_list_manager.mesh_edge_index_list, edge_list_);

            geom_elt_face_list.clear();
            decltype(auto) oriented_face_list = geom_elt.GetOrientedFaceList();

            std::ranges::transform(oriented_face_list,

                                   std::back_inserter(geom_elt_face_list),
                                   interface_without_orientation);

            ComputeMeshLevelList<Face>(
                geom_elt_face_list, interface_index_list_manager.mesh_face_index_list, face_list_);

            ComputeMeshLevelListForVolume(geom_elt, volume_list_);
        }
    }


    void ComputeInterfaceListInMesh::Print(std::ostream& stream) const
    {
        stream << "# For each geometric interface, give the list of the Coords that delimits it.\n"
                  "# The index associated to the interface (e.g. 4 in 'Vertex 4') is the program-wise index "
                  "associated to "
                  "the interface.\n"
                  "# The indexes in the [] are Coords program-wise indexes, which are the position of the Coords "
                  "in the "
                  " Mesh object\n# regardless of the convention used in the original mesh format."
               << '\n';

        {
            // The copy is not a mistake: for convenience we want to write the index by increasing order.
            auto vertex_list = SortPerProgramWiseIndex<Vertex>(GetVertexList());

            for (const auto& vertex_ptr : vertex_list)
            {
                assert(!(!vertex_ptr));
                stream << vertex_ptr->GetNature() << ' ' << vertex_ptr->GetProgramWiseIndex() << ";";

                // Put it into a vector solely to ensure same print format as for the other interfaces.
                const auto& coords_list = vertex_ptr->GetCoordsList();
                assert(coords_list.size() == 1UL);
                assert(!(!coords_list.back()));

                const std::vector<::MoReFEM::CoordsNS::program_wise_position> index{ ExtractIndex(
                    *(coords_list.back())) };

                Utilities::PrintContainer<>::Do(index, stream);
            }
        }

        {
            PrintTypeOfInterface<Edge>(GetEdgeList(), stream);
            PrintTypeOfInterface<Face>(GetFaceList(), stream);
            PrintTypeOfInterface<Volume>(GetVolumeList(), stream);
        }
    }


    InterfaceIndexListManager::InterfaceIndexListManager()
    {
        mesh_vertex_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
        mesh_edge_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
        mesh_face_index_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    void ComputeInterfaceListInMesh::ReorderCoordsInEdgesAndFaces()
    {
        decltype(auto) edge_list = GetNonCstEdgeList();

        for (const auto& edge_ptr : edge_list)
        {
            assert(!(!edge_ptr));
            Internal::InterfaceNS::OrderCoordsList(edge_ptr->GetNonCstCoordsList());
        }

        decltype(auto) face_list = GetNonCstFaceList();

        for (const auto& face_ptr : face_list)
        {
            assert(!(!face_ptr));
            Internal::InterfaceNS::OrderCoordsList(face_ptr->GetNonCstCoordsList());
        }
    }


    namespace // anonymous
    {


        template<class InterfaceT>
        void ComputeMeshLevelList(const typename InterfaceT::vector_shared_ptr& geom_elt_interface_list,
                                  std::unordered_set<::MoReFEM::InterfaceNS::program_wise_index_type>& set,
                                  typename InterfaceT::vector_shared_ptr& mesh_list)

        {
            for (const typename InterfaceT::shared_ptr& interface_ptr : geom_elt_interface_list)
            {
                assert(!(!interface_ptr));
                const auto index = interface_ptr->GetProgramWiseIndex();

                const auto it = set.find(index);

                if (it == set.cend())
                {
                    mesh_list.push_back(interface_ptr);
                    set.insert(index);
                }
            }
        }


        void ComputeMeshLevelListForVolume(const GeometricElt& geom_elt, Volume::vector_shared_ptr& mesh_volume_list)
        {
            auto volume_ptr = geom_elt.GetVolumePtr();

            if (!(!volume_ptr))
                mesh_volume_list.push_back(volume_ptr);
        }


        template<class InterfaceT>
        void PrintTypeOfInterface(const typename InterfaceT::vector_shared_ptr& interface_list, std::ostream& stream)
        {
            auto sorted_interface_list = SortPerProgramWiseIndex<InterfaceT>(interface_list);

            for (const auto& interface_ptr : sorted_interface_list)
            {
                assert(!(!interface_ptr));
                const auto& interface = *interface_ptr;

                stream << interface.StaticNature() << ' ' << interface.GetProgramWiseIndex() << ";";

                const auto& coords_list = interface.GetCoordsList();
                std::vector<::MoReFEM::CoordsNS::program_wise_position> index_list(coords_list.size());

                std::transform(coords_list.cbegin(),
                               coords_list.cend(),
                               index_list.begin(),
                               [](const auto& coords_ptr)
                               {
                                   assert(!(!coords_ptr));
                                   return ExtractIndex(*coords_ptr);
                               });

                std::ranges::sort(index_list);

                Utilities::PrintContainer<>::Do(index_list, stream);
            }
        }


        ::MoReFEM::CoordsNS::program_wise_position ExtractIndex(const Coords& coords)
        {
            return coords.GetProgramWisePosition();
        }


        template<class InterfaceT>
        typename InterfaceT::vector_shared_ptr
        SortPerProgramWiseIndex(typename InterfaceT::vector_shared_ptr interface_list)
        {
            auto program_wise_comp = [](const auto& lhs, const auto& rhs)
            {
                assert(!(!lhs));
                assert(!(!rhs));

                return lhs->GetProgramWiseIndex() < rhs->GetProgramWiseIndex();
            };

            std::sort(interface_list.begin(), interface_list.end(), program_wise_comp);

            return interface_list;
        }


    } // namespace


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
