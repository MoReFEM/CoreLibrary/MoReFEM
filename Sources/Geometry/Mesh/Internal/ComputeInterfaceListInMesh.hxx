// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_COMPUTEINTERFACELISTINMESH_DOT_HXX_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_COMPUTEINTERFACELISTINMESH_DOT_HXX_
// IWYU pragma: private, include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"
// *** MoReFEM header guards *** < //


#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Internal::MeshNS
{


    inline const Vertex::vector_shared_ptr& ComputeInterfaceListInMesh::GetVertexList() const noexcept
    {
        return vertex_list_;
    }


    inline const Edge::vector_shared_ptr& ComputeInterfaceListInMesh::GetEdgeList() const noexcept
    {
        return edge_list_;
    }


    inline const Face::vector_shared_ptr& ComputeInterfaceListInMesh::GetFaceList() const noexcept
    {
        return face_list_;
    }


    inline const Volume::vector_shared_ptr& ComputeInterfaceListInMesh::GetVolumeList() const noexcept
    {
        return volume_list_;
    }


    inline Edge::vector_shared_ptr& ComputeInterfaceListInMesh::GetNonCstEdgeList() noexcept
    {
        return const_cast<Edge::vector_shared_ptr&>(GetEdgeList());
    }


    inline Face::vector_shared_ptr& ComputeInterfaceListInMesh::GetNonCstFaceList() noexcept
    {
        return const_cast<Face::vector_shared_ptr&>(GetFaceList());
    }


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_COMPUTEINTERFACELISTINMESH_DOT_HXX_
// *** MoReFEM end header guards *** < //
