// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/Mesh/Internal/CreateMeshDataDirectory.hpp"

#include "Utilities/Filesystem/Directory.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM::Internal::MeshNS
{


    std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>
    CreateMeshDataDirectory(const ::MoReFEM::FilesystemNS::Directory& output_directory)
    {
        std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr> ret;

        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
        decltype(auto) mesh_id_list = mesh_manager.GetStorage();

        for (const auto& [mesh_id, mesh_ptr] : mesh_id_list)
        {
            auto new_subdir = std::make_unique<::MoReFEM::FilesystemNS::Directory>(
                output_directory, std::string("Mesh_") + std::to_string(mesh_id.Get()));

            new_subdir->ActOnFilesystem();

            ret.insert({ mesh_id, std::move(new_subdir) });
        }

        return ret;
    }


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
