// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_CREATEMESHDATADIRECTORY_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_CREATEMESHDATADIRECTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>

#include "Utilities/Filesystem/Directory.hpp"

#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM::Internal::MeshNS
{


    /*!
     * \brief Create on disk an output directory for each mesh.
     *
     * \param[in] output_directory Output directory in which all the model outputs are written (it is the root directory
     * of all outputs).
     *
     * \return Key is the unique id of the mesh, value the path to it.
     */
    std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>
    CreateMeshDataDirectory(const ::MoReFEM::FilesystemNS::Directory& output_directory);


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_CREATEMESHDATADIRECTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
