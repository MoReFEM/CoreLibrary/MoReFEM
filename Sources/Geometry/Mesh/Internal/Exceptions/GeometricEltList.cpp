// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <source_location>
#include <sstream>
#include <string>

#include "Geometry/Mesh/Internal/Exceptions/GeometricEltList.hpp"

#include "Geometry/Domain/StrongType.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp" // IWYU pragma: keep


namespace // anonymous
{


    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type, MoReFEM::MeshLabelNS::index_type label);

    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type);

    std::string InvalidRequestMsg(::MoReFEM::GeometryNS::dimension_type dimension);

    std::string DuplicateInGeometricEltIndexMsg(std::size_t Ngeometric_element, std::size_t Nunique_indexes);


} // namespace


namespace MoReFEM::ExceptionNS::GeometricEltList
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    InitNotCleared::InitNotCleared(const std::source_location location)
    : Exception("GeometricEltList::Init() can only be called upon an empty object", location)
    { }


    InitNotCleared::~InitNotCleared() = default;


    InvalidRequest::InvalidRequest(const RefGeomElt& geometric_type,
                                   MeshLabelNS::index_type label,
                                   const std::source_location location)
    : Exception(InvalidRequestMsg(geometric_type, label), location)
    { }


    InvalidRequest::InvalidRequest(::MoReFEM::GeometryNS::dimension_type dimension, const std::source_location location)
    : Exception(InvalidRequestMsg(dimension), location)
    { }


    InvalidRequest::InvalidRequest(const RefGeomElt& geometric_type, const std::source_location location)
    : Exception(InvalidRequestMsg(geometric_type), location)
    { }


    InvalidRequest::~InvalidRequest() = default;


    DuplicateInGeometricEltIndex::~DuplicateInGeometricEltIndex() = default;


    DuplicateInGeometricEltIndex::DuplicateInGeometricEltIndex(std::size_t Ngeometric_element,
                                                               std::size_t Nunique_indexes,
                                                               const std::source_location location)
    : Exception(DuplicateInGeometricEltIndexMsg(Ngeometric_element, Nunique_indexes), location)
    { }


} // namespace MoReFEM::ExceptionNS::GeometricEltList


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type, MoReFEM::MeshLabelNS::index_type label)
    {
        std::ostringstream oconv;

        oconv << "Position and number of geometric elements with type " << geometric_type.GetName() << " and label "
              << label << " was requested, but no such element exist (the first index is the position "
              << "of the type in enumeration provided in Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp).";

        return oconv.str();
    }


    std::string InvalidRequestMsg(const MoReFEM::RefGeomElt& geometric_type)
    {
        std::ostringstream oconv;

        oconv << "Position and number of geometric elements with type " << geometric_type.GetName()
              << " was requested, but no such element exist (the first index is the position "
              << "of the type in enumeration provided in Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp).";

        return oconv.str();
    }


    std::string InvalidRequestMsg(::MoReFEM::GeometryNS::dimension_type dimension)
    {
        std::ostringstream oconv;

        oconv << "Position and number of geometric elements with dimension " << dimension
              << " was requested, but no such element exist.";

        return oconv.str();
    }


    std::string DuplicateInGeometricEltIndexMsg(std::size_t Ngeometric_element, std::size_t Nunique_indexes)
    {
        std::ostringstream oconv;

        oconv << "GeometricElt indexes are expected to be unique, and that is not the case here: " << Nunique_indexes
              << " unique indexes were found for " << Ngeometric_element << " geometric elements.";

        return oconv.str();
    }


} // namespace
