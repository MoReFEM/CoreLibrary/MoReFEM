// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_EXCEPTIONS_GEOMETRICELTLIST_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_EXCEPTIONS_GEOMETRICELTLIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "Geometry/Domain/StrongType.hpp"
#include "Geometry/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::GeometricEltList
{

    //! Generic exception thrown by a GeometricEltList object.
    class Exception : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Thrown when GeometricEltList::Init() is called upon an object already initialized.
    class InitNotCleared final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         *\copydoc doxygen_hide_source_location

         */
        explicit InitNotCleared(const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InitNotCleared() override;

        //! \copydoc doxygen_hide_copy_constructor
        InitNotCleared(const InitNotCleared& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InitNotCleared(InitNotCleared&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InitNotCleared& operator=(const InitNotCleared& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InitNotCleared& operator=(InitNotCleared&& rhs) = default;
    };


    /*!
     * \brief Thrown when a request was done for a type not found in the GeometricEltList.
     *
     * For instance if you request a TriangleP1 and none is found, this exception is thrown.
     * The reason for this is that the user should have kept track of the type actually present
     * (GeometricEltList provides this information if needed!)
     */
    class InvalidRequest final : public Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] geometric_type Geometric element type which was requested.
         * \param[in] label Index of the label requested.
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidRequest(const RefGeomElt& geometric_type,
                                MeshLabelNS::index_type label,
                                const std::source_location location = std::source_location::current());

        /*!
         * \brief Constructor.
         *
         * \param[in] geometric_type Geometric element type which was requested.
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidRequest(const RefGeomElt& geometric_type,
                                const std::source_location location = std::source_location::current());


        /*!
         * \brief Constructor.
         *
         * \param[in] dimension Dimension of the geometric element requested.
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidRequest(::MoReFEM::GeometryNS::dimension_type dimension,
                                const std::source_location location = std::source_location::current());


        //! Destructor
        virtual ~InvalidRequest() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidRequest(const InvalidRequest& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidRequest(InvalidRequest&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidRequest& operator=(const InvalidRequest& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidRequest& operator=(InvalidRequest&& rhs) = default;
    };


    //! Thrown when indexes of the geometric elements read are not unique.
    class DuplicateInGeometricEltIndex final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] Ngeometric_element Number of \a GeometricElt.
         * \param[in] Nunique_indexes Number of unique indexes allotted after building of all \a GeometricElt.
         * It should have been equal to \a Ngeometric_element.
         * \copydoc doxygen_hide_source_location
         *
         */
        explicit DuplicateInGeometricEltIndex(std::size_t Ngeometric_element,
                                              std::size_t Nunique_indexes,
                                              const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~DuplicateInGeometricEltIndex() override;

        //! \copydoc doxygen_hide_copy_constructor
        DuplicateInGeometricEltIndex(const DuplicateInGeometricEltIndex& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        DuplicateInGeometricEltIndex(DuplicateInGeometricEltIndex&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DuplicateInGeometricEltIndex& operator=(const DuplicateInGeometricEltIndex& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        DuplicateInGeometricEltIndex& operator=(DuplicateInGeometricEltIndex&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::GeometricEltList


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_EXCEPTIONS_GEOMETRICELTLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
