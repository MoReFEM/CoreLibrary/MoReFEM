// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdint>
#include <fstream> // IWYU pragma: keep
#include <iomanip>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Geometry/Mesh/Internal/Format/Exceptions/Ensight.hpp"

#include "Utilities/Containers/Sort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "Geometry/Coords/Internal/Factory.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/Advanced/SortingCriterion.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"
#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS::Ensight
{


    // Declarations here; definitions (and comments) in a namespace at the end of this file
    namespace // anonymous
    {

        namespace FormatExceptionNS = ::MoReFEM::ExceptionNS::Format;

        namespace EnsightExceptionNS = FormatExceptionNS::Ensight;


        enum class CoordOrElt : std::uint8_t { coord, element };


        enum class CoordOrEltStatus : std::uint8_t {
            offOrAssign, // at the moment, no reason to make a difference between both
            given,
            ignore
        };


        CoordOrEltStatus DetermineNodeOrEltStatus(std::ifstream& file_in,
                                                  CoordOrElt coord_or_element,
                                                  const ::MoReFEM::FilesystemNS::File& mesh_file);


        std::size_t ReadNumberCoords(std::ifstream& file_in);

        template<CoordOrEltStatus CoordOrEltStatusT>
        void ReadCoordsHelper(std::istream& file_in,
                              ::MoReFEM::CoordsNS::space_unit_type space_unit,
                              Coords::vector_shared_ptr& coord_list);

        void ReadCoords(std::ifstream& file_in,
                        const ::MoReFEM::FilesystemNS::File& mesh_file,
                        ::MoReFEM::CoordsNS::space_unit_type space_unit,
                        CoordOrEltStatus coord_or_elt_status,
                        Coords::vector_shared_ptr& coord_list);


        // maybe_unused is here to please clang: it is actually used!
        template<CoordOrEltStatus CoordOrEltStatusT>
        [[maybe_unused]] void ReadBlockHelper(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                                              const Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                              const Coords::vector_shared_ptr& coords_list,
                                              const MeshLabel::const_shared_ptr& label,
                                              std::size_t Nprocessor_wise_elt,
                                              GeometricElt::vector_shared_ptr& processor_wise_geometric_elt_list,
                                              GeometricElt::vector_shared_ptr& ghost_geometric_elt_list,
                                              std::istream& file_in);

        MeshLabel::const_shared_ptr
        EnsightReadLabel(::MoReFEM::MeshNS::unique_id mesh_id,
                         const std::optional<std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t>>&
                             Nprocessor_wise_geom_elt_per_type_and_label,
                         const Coords::vector_shared_ptr& coords_list,
                         const ::MoReFEM::FilesystemNS::File& mesh_file,
                         CoordOrEltStatus element_status,
                         std::istream& stream,
                         GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                         GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list);

        void FlushBlock(
            std::ostream& file_out,
            const std::map<::MoReFEM::Advanced::GeomEltNS::EnsightName, GeometricElt::vector_shared_ptr>& current_block,
            const MeshLabel::const_shared_ptr& current_label,
            bool do_print_index,
            std::size_t& part_index);

        void WriteLabels(std::ostream& file_out,
                         const GeometricElt::vector_shared_ptr& geometric_elt_list,
                         bool do_print_index);


    } // anonymous namespace


    void ReadFile(const ::MoReFEM::MeshNS::unique_id mesh_id,
                  const ::MoReFEM::FilesystemNS::File& mesh_file,
                  ::MoReFEM::CoordsNS::space_unit_type space_unit,
                  const std::optional<std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t>>&
                      Nprocessor_wise_geom_elt_per_type_and_label,
                  ::MoReFEM::GeometryNS::dimension_type& dimension,
                  GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                  GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list,
                  Coords::vector_shared_ptr& coords_list,
                  MeshLabel::vector_const_shared_ptr& mesh_label_list)
    {
        dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 }; // Ensight read all its coords as dimension 3.

        std::ifstream file_in{ mesh_file.Read() };

        if (!file_in)
            throw FormatExceptionNS::UnableToOpenFile(mesh_file);

        // Skip the first two lines
        const std::streamsize max_stream_size = std::numeric_limits<std::streamsize>::max();

        file_in.ignore(max_stream_size, '\n');
        file_in.ignore(max_stream_size, '\n');

        // Determine coord and element status with next two lines
        const CoordOrEltStatus coord_or_elt_status = DetermineNodeOrEltStatus(file_in, CoordOrElt::coord, mesh_file);
        const CoordOrEltStatus element_status = DetermineNodeOrEltStatus(file_in, CoordOrElt::element, mesh_file);

        // Skip next line (which is "coordinates")
        file_in.ignore(max_stream_size, '\n');

        // Read all the coords
        // Check whether the mesh is actually 2D or 3D (Ensight format is always three-dimensional,
        // with third component being always 0 for 2D problems)
        ReadCoords(file_in, mesh_file, space_unit, coord_or_elt_status, coords_list);

        // Now read all the geometric elements blocks
        while (file_in)
            mesh_label_list.push_back(EnsightReadLabel(mesh_id,
                                                       Nprocessor_wise_geom_elt_per_type_and_label,
                                                       coords_list,
                                                       mesh_file,
                                                       element_status,
                                                       file_in,
                                                       unsort_processor_wise_geom_elt_list,
                                                       unsort_ghost_geom_elt_list));

        assert(file_in.eof());
    }


    void WriteFile(const Mesh& mesh, const ::MoReFEM::FilesystemNS::File& mesh_file)
    {
        std::ofstream file_out{ mesh_file.NewContent() };

        if (!file_out)
            throw FormatExceptionNS::UnableToOpenFile(mesh_file);

        std::cout << "Writing to file " << mesh_file << '\n';

        // Write the first two lines
        for (std::size_t i = 0UL; i < 2UL; ++i)
            file_out << "Geometry file\n";

        // Write coords and element related lines
        file_out << "node id given\n";
        file_out << "element id assign\n";

        // Fifth line is "coordinates"
        file_out << "coordinates\n";

        // Sixth one is the number of coords; respect the Ensight convention of width of 8
        decltype(auto) proc_wise_coords_list = mesh.GetProcessorWiseCoordsList();
        decltype(auto) ghost_coords_list = mesh.GetGhostCoordsList();
        const auto Ncoords = proc_wise_coords_list.size() + ghost_coords_list.size();

        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        file_out << std::setw(8) << Ncoords << '\n';

        // Now write all coords
        for (const auto& coords_ptr : proc_wise_coords_list)
            WriteEnsightFormat<true>(*coords_ptr, file_out);

        for (const auto& coords_ptr : ghost_coords_list)
            WriteEnsightFormat<true>(*coords_ptr, file_out);

        // Finally write the labels. Ensure first the geometric elements are sort the right way!
        {
            auto all_geom_elt_list = mesh.ComputeProcessorWiseAndGhostGeometricEltList();

            namespace sc = Advanced::GeometricEltNS::SortingCriterion;
            std::ranges::stable_sort(all_geom_elt_list,

                                     Utilities::Sort<GeometricElt::shared_ptr, sc::SurfaceRef<>>);

            WriteLabels(file_out, all_geom_elt_list, false);
        }
    }


    // Definitions
    namespace // anonymous
    {


        /*
         * \brief Utility to read third and fourth lines of the mesh file.
         *
         * The expected format for them is:
         * node id *status*
         * element id *status*
         * where status is among the following: off/given/assign/ignore.
         *
         * \param[in] line Line considered.
         * \param[in] CoordOrElt Whether we are considering the line concerning the coord or the element.
         * \param[in] mesh_file Name of the mesh file (only for logging purposes).
         */
        CoordOrEltStatus DetermineNodeOrEltStatus(std::ifstream& file_in,
                                                  CoordOrElt coord_or_element,
                                                  const ::MoReFEM::FilesystemNS::File& mesh_file)
        {
            std::string buf;
            std::string expected;
            std::size_t line_index = 0;

            // First string should be either "node" or "element"
            getline(file_in, buf, ' ');

            switch (coord_or_element)
            {
            case CoordOrElt::coord:
                expected = "node";
                line_index = 3UL;
                break;
            case CoordOrElt::element:
                expected = "element";
                line_index = 4UL;
                break;
            }

            // Ignore GCC warning about uninitialized dimension_read (should another format be added to the code
            // that another warning would say the switch doesn't cover all cases).
            PRAGMA_DIAGNOSTIC(push)
#ifdef MOREFEM_GCC
            PRAGMA_DIAGNOSTIC(ignored "-Wmaybe-uninitialized")
#endif // MOREFEM_GCC

            if (buf != expected)
                throw EnsightExceptionNS::InvalidThirdOrFourthLine(mesh_file, line_index);

            // Second string should be id
            getline(file_in, buf, ' ');
            expected = "id";

            if (buf != expected)
                throw EnsightExceptionNS::InvalidThirdOrFourthLine(mesh_file, line_index);

            // Third string should be among <off/given/assign/ignore>
            getline(file_in, buf);

            if (buf == "off" || buf == "assign")
                return CoordOrEltStatus::offOrAssign;

            if (buf == "given")
                return CoordOrEltStatus::given;

            if (buf == "ignore")
                return CoordOrEltStatus::ignore;

            throw EnsightExceptionNS::InvalidThirdOrFourthLine(mesh_file, line_index);

            PRAGMA_DIAGNOSTIC(pop)
        }


        // Get the number of coords (sixth line)
        std::size_t ReadNumberCoords(std::ifstream& file_in)
        {
            std::string line;
            getline(file_in, line);
            return std::stoul(line);
        }


        /*
         * \brief Read an ensight line that describes a coord and store it
         *
         * \tparam CoordOrEltStatusT Status of the nodes read, read earlier in Ensight file
         *
         * For coord lines, the format is:
         *     - *ID* *X* *Y* *Z* for status = ignore or given
         *     - *X* *Y* *Z* for status = off or assign
         *
         * \param[in,out] file_in Stream being read; function stops when its status is fail (ie last
         * correct line read)
         * \param[out] coord_list List of all coords read
         */
        template<CoordOrEltStatus CoordOrEltStatusT>
        void ReadCoordsHelper(std::istream& file_in,
                              const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                              Coords::vector_shared_ptr& coord_list)
        {
            // NOLINTBEGIN(misc-const-correctness)
            ::MoReFEM::CoordsNS::index_from_mesh_file index{ 1UL }; // Ensight begins its numeration at 1
            [[maybe_unused]] std::size_t ensight_index = 0;         // Ensight index, which will be handled differently
                                                                    // depending on template parameter
            // NOLINTEND(misc-const-correctness)

            while (true)
            {
                if constexpr (CoordOrEltStatusT == CoordOrEltStatus::ignore
                              || CoordOrEltStatusT == CoordOrEltStatus::given)
                    file_in >> ensight_index;

                auto&& point_ptr = Internal::CoordsNS::Factory::FromStream(
                    ::MoReFEM::GeometryNS::dimension_type{ 3 }, file_in, space_unit);
                auto& point = *point_ptr;

                if (!file_in)
                    return;

                if constexpr (CoordOrEltStatusT == CoordOrEltStatus::given)
                    point.SetIndexFromMeshFile(::MoReFEM::CoordsNS::index_from_mesh_file(ensight_index));
                else
                    point.SetIndexFromMeshFile(index++);

                coord_list.emplace_back(std::move(point_ptr));
            }
        }


        /*
         * \brief Read the coords
         *
         * \param[in,out] fileStream Stream that is reading the input file. Reading goes on until the item read
         * is not a coord; the status of the stream is cleant so that reading can continue (file doesn't end
         * with coords...)
         * \param[in] mesh_file Name of the mesh file (only for logging purposes)
         * \param{in] coord_or_elt_status Status of the coord. For 'ignore' and 'given' an additional column
         * (the id) has to be read
         * \param[out] coord_list List of all coords read
         *
         *
         */
        void ReadCoords(std::ifstream& file_in,
                        const ::MoReFEM::FilesystemNS::File& mesh_file,
                        const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                        CoordOrEltStatus coord_or_elt_status,
                        Coords::vector_shared_ptr& coord_list)
        {
            assert(coord_list.empty());
            auto Ncoords = ReadNumberCoords(file_in);
            coord_list.reserve(Ncoords);

            switch (coord_or_elt_status)
            {
            case CoordOrEltStatus::offOrAssign:
                ReadCoordsHelper<CoordOrEltStatus::offOrAssign>(file_in, space_unit, coord_list);
                break;
            case CoordOrEltStatus::ignore:
                ReadCoordsHelper<CoordOrEltStatus::ignore>(file_in, space_unit, coord_list);
                break;
            case CoordOrEltStatus::given:
                ReadCoordsHelper<CoordOrEltStatus::given>(file_in, space_unit, coord_list);
                break;
            }

            if (Ncoords != coord_list.size())
                throw EnsightExceptionNS::InvalidCoords(mesh_file, Ncoords, coord_list.size());

            file_in.clear(); // reset the state, which was by construction failbit

            std::cout << Ncoords << " coords read in " << mesh_file << '\n';
        }


        /*
         * \brief Read an ensight line that describes an element of a block and store it
         *
         * \tparam CoordOrEltStatusT Status of the elements read, read earlier in Ensight file
         *
         * For element, it is:
         *     - *ID* *coord* *coord* ... for status = ignore or given
         *     - *coord* *coord* ... for status = off or assign
         *
         * \param[in,out] file_in Stream being read; function stops when its status is fail (ie last correct
         * line read)
         * \param[in] geometric_elt_name Name of the geometric element being read.
         * \param[out] geometric_elt_list Key is the dimension, value is the list of elements.
         * \param[in] Label::shared_ptr label Label to which the geometric elements in the block belong to.
         *
         */
        // NOLINTBEGIN(bugprone-easily-swappable-parameters)
        template<CoordOrEltStatus CoordOrEltStatusT>
        void ReadBlockHelper(const ::MoReFEM::MeshNS::unique_id mesh_unique_id,
                             const Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                             const Coords::vector_shared_ptr& coords_list,
                             const MeshLabel::const_shared_ptr& label,
                             std::size_t Nprocessor_wise_elt,
                             GeometricElt::vector_shared_ptr& processor_wise_geometric_elt_list,
                             GeometricElt::vector_shared_ptr& ghost_geometric_elt_list,
                             std::istream& file_in)
        // NOLINTEND(bugprone-easily-swappable-parameters)
        {
            // First one is used in the cases the file doesn't give indexes, and second one if on the contrary
            // it does, in which case we keep Ensight internal convention.
            static GeomEltNS::index_type assigned_ensight_index{ 1UL };

            // NOLINTNEXTLINE(misc-const-correctness)
            [[maybe_unused]] std::size_t read_ensight_index{ 0UL };

            const auto& geometric_elt_factory = ::MoReFEM::Advanced::GeometricEltFactory::CreateOrGetInstance();

            auto count = 0UL;

            while (true)
            {
                if constexpr (CoordOrEltStatusT == CoordOrEltStatus::ignore
                              || CoordOrEltStatusT == CoordOrEltStatus::given)
                    file_in >> read_ensight_index;

                GeometricElt::shared_ptr geometric_element = nullptr;
                try
                {
                    geometric_element = geometric_elt_factory.CreateFromEnsightName(
                        mesh_unique_id, coords_list, geometric_elt_name, file_in);
                }
                catch (const Exception&)
                {
                    // By design the \a GeometricElement constructor fails if the stream read is
                    // invalid; here we expect this exception to end the reading of a block.
                    // There will be a check later upon the number of \a GeometricElt built, which
                    // must match the forecast given in the geo file.
                    if (!file_in)
                        return;

                    // If another exception occurred, throw it back.
                    throw;
                }

                assert("If stream is correct geometric elementshould be well defined" && geometric_element);
                geometric_element->SetMeshLabel(label);

                switch (CoordOrEltStatusT) // choice at compile time
                {
                case CoordOrEltStatus::given:
                    geometric_element->SetIndex(GeomEltNS::index_type{ read_ensight_index });
                    break;
                case CoordOrEltStatus::offOrAssign:
                case CoordOrEltStatus::ignore:
                    geometric_element->SetIndex(assigned_ensight_index++);
                    break;
                }

                ++count;

                if (count <= Nprocessor_wise_elt)
                    processor_wise_geometric_elt_list.push_back(geometric_element);
                else
                    ghost_geometric_elt_list.push_back(geometric_element);
            }
        }


        // clang-format off
        std::size_t ExtractNprocessorWiseGeomElt(const std::map
                                                 <
                                                    Internal::MeshNS::RefGeomEltAndMeshLabel,
                                                    std::size_t
                                                 >& Nprocessor_wise_geom_elt_per_type_and_label,
                                                 const Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                                 const MeshLabel& mesh_label)
        // clang-format on
        {
            decltype(auto) geometric_elt_factory = ::MoReFEM::Advanced::GeometricEltFactory::CreateOrGetInstance();
            decltype(auto) ref_geom_elt = geometric_elt_factory.GetRefGeomElt(geometric_elt_name);

            const Internal::MeshNS::RefGeomEltAndMeshLabel ref_geom_elt_and_mesh_label(ref_geom_elt, mesh_label);

            auto it = Nprocessor_wise_geom_elt_per_type_and_label.find(ref_geom_elt_and_mesh_label);

            if (it == Nprocessor_wise_geom_elt_per_type_and_label.cend())
                return 0UL;

            return it->second;
        }


        /*
         * \brief Read a block that defines a label.
         *
         * \param[in,out] geometric_elt_list The list of all geometric elements read in Ensight file
         * so far, in input. Files related to label block being read are added in output.
         * \param[in,out] stream Input stream from which the block is read.
         * \param[in] mesh_file Ensight file being read (used only in case of error).
         * \param[in] element_status How the indexes are handled in Ensight format (read earlier in the
         * Ensight file).
         *
         * \return Pointer to a newly created object; nullptr if the block couldn't be read properly.
         *
         * An exception is thrown if any reading from the stream fails, except in the case the end of
         * file was reached.
         */
        MeshLabel::const_shared_ptr
        EnsightReadLabel(const ::MoReFEM::MeshNS::unique_id mesh_id,
                         const std::optional<std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t>>&
                             Nprocessor_wise_geom_elt_per_type_and_label,
                         const Coords::vector_shared_ptr& coords_list,
                         const ::MoReFEM::FilesystemNS::File& mesh_file,
                         CoordOrEltStatus element_status,
                         std::istream& stream,
                         GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                         GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list)
        {
            auto index = MeshLabelNS::index_type{ NumericNS::UninitializedIndex<std::size_t>() };
            std::string description;

            {
                // First line should be "part " # where # is an index
                {
                    std::string buf;
                    stream >> buf;
                    if (buf != "part")
                    {
                        // If the end of Ensight file was reached, no problem
                        if (stream.eof())
                            return nullptr;

                        throw EnsightExceptionNS::InvalidLabelBlock(mesh_file, buf);
                    }
                }

                // Read index from "part ... " line.
                // This is NOT the value we want to use if the Ensight file was written by MoReFEM
                // (this is wto work around the Ensight convention which expects consecutive
                // labels from 1 to N, where N is the number of mesh labels).
                {
                    std::size_t buf{};
                    stream >> buf;
                    assert(stream.good());
                    index = MeshLabelNS::index_type{ buf };
                }
            }

            {
                // Second one should be a description string. Beware: finishing '\n' still in the buffer!
                stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                getline(stream, description);

                // In the case description begins with "MeshLabel", extract the index that follows and
                // use this in place of the index read on the line above.
                // Such description lines are used when the Ensight file was generated from a
                // MoReFEM mesh; in this case it might proves useful to use the same indexing
                // convention as in this earlier mesh.
                const std::string mesh_label_name = "MeshLabel_";

                if (Utilities::String::StartsWith(description, mesh_label_name))
                {
                    auto copy = description.substr(mesh_label_name.size());
                    index = MeshLabelNS::index_type{ static_cast<std::size_t>(stoi(copy)) };
                } else
                {
                    std::cout << "[WARNING] In the mesh file " << mesh_file
                              << ", the block "
                                 "part "
                              << index
                              << " didn't give in the next descriptive line the MeshLabel "
                                 "information with the expected format. If you're using a native Ensight file, it's "
                                 "not an issue. However, if you're using an Ensight file that was generated by MoReFEM "
                                 "(for instance for a run from prepartitioned data), it is really dubious and might "
                                 "highlight a bug."
                                 "(the index is run from the next line because in MoReFEM we allow not consecutive "
                                 "labels, which is not supported natively by Ensight).\n";
                }
            }

            auto current_label = std::make_shared<const MeshLabel>(mesh_id, index, description);

            const auto& geometric_elt_factory = ::MoReFEM::Advanced::GeometricEltFactory::CreateOrGetInstance();

            // In the following, we should get one block per geometric geometric element present in the label
            // Format is:
            // - one line for the name of the geometric element considered.
            // - one line to tell how many geometric elements are expected.
            // - and then a line per geometric element of this kind in the label.
            while (stream)
            {
                // Description of the geometric geometric elementthat will be read in Ensight format
                // (e.g. "tria3"), and the number of objects considered
                auto begin_block_position = stream.tellg();

                std::string read_name;
                getline(stream, read_name);

                Advanced::GeomEltNS::EnsightName ensight_geom_elt_name(read_name);

                // Exit the loop if end of file is reached
                if (stream.eof())
                {
                    return current_label;
                }

                if (ensight_geom_elt_name.Get().substr(0, 4) == "part")
                {
                    // Means we are beginning to scrutinize a new label
                    stream.seekg(begin_block_position);

                    return current_label;
                } else if (!geometric_elt_factory.IsEnsightName(ensight_geom_elt_name))
                    throw ::MoReFEM::ExceptionNS::Factory::GeometricElt ::InvalidEnsightGeometricEltName(
                        ensight_geom_elt_name);
                else
                {
                    // Fourth line should be the number of geometric geometric elements described in the block
                    std::size_t Ngeometric_elt_in_current_mesh_label = 0;
                    stream >> Ngeometric_elt_in_current_mesh_label;

                    std::size_t Nproc_wise_geom_elt = Ngeometric_elt_in_current_mesh_label;

                    if (Nprocessor_wise_geom_elt_per_type_and_label)
                    {
                        Nproc_wise_geom_elt = ExtractNprocessorWiseGeomElt(
                            Nprocessor_wise_geom_elt_per_type_and_label.value(), ensight_geom_elt_name, *current_label);

                        if (Nproc_wise_geom_elt > Ngeometric_elt_in_current_mesh_label)
                        {
                            std::ostringstream oconv;
                            oconv << "Inconsistent pre-partitioned data: " << Nproc_wise_geom_elt
                                  << " processor-wise geometric element were foreseen for label "
                                  << current_label->GetIndex() << " and " << ensight_geom_elt_name << "; "
                                  << "but only" << Ngeometric_elt_in_current_mesh_label
                                  << " were present in the mesh file";
                            throw Exception(oconv.str());
                        }
                    }

                    if (!stream)
                        throw EnsightExceptionNS::InvalidNumberOfGeometricElts(mesh_file);

                    // Next read the geometric geometric elements until the stream fails to do so
                    const std::size_t initial_size =
                        unsort_processor_wise_geom_elt_list.size() + unsort_ghost_geom_elt_list.size();

                    switch (element_status)
                    {
                    case CoordOrEltStatus::offOrAssign:
                        ReadBlockHelper<CoordOrEltStatus::offOrAssign>(mesh_id,
                                                                       ensight_geom_elt_name,
                                                                       coords_list,
                                                                       current_label,
                                                                       Nproc_wise_geom_elt,
                                                                       unsort_processor_wise_geom_elt_list,
                                                                       unsort_ghost_geom_elt_list,
                                                                       stream);
                        break;
                    case CoordOrEltStatus::ignore:
                        ReadBlockHelper<CoordOrEltStatus::ignore>(mesh_id,
                                                                  ensight_geom_elt_name,
                                                                  coords_list,
                                                                  current_label,
                                                                  Nproc_wise_geom_elt,
                                                                  unsort_processor_wise_geom_elt_list,
                                                                  unsort_ghost_geom_elt_list,
                                                                  stream);
                        break;
                    case CoordOrEltStatus::given:
                        ReadBlockHelper<CoordOrEltStatus::given>(mesh_id,
                                                                 ensight_geom_elt_name,
                                                                 coords_list,
                                                                 current_label,
                                                                 Nproc_wise_geom_elt,
                                                                 unsort_processor_wise_geom_elt_list,
                                                                 unsort_ghost_geom_elt_list,
                                                                 stream);
                        break;
                    }

                    const auto current_Ngeom_elt =
                        unsort_processor_wise_geom_elt_list.size() + unsort_ghost_geom_elt_list.size();

                    if (initial_size + Ngeometric_elt_in_current_mesh_label != current_Ngeom_elt)
                        throw EnsightExceptionNS::BadNumberOfEltsInLabel(mesh_file,
                                                                         index,
                                                                         description,
                                                                         ensight_geom_elt_name,
                                                                         Ngeometric_elt_in_current_mesh_label,
                                                                         current_Ngeom_elt - initial_size);

                    stream.clear(); // reset the state, which was by construction failbit at this point
                }
            }

            return current_label;
        }


        /*!
         * \brief Flush to output file the previous block read.
         *
         * This is intended to be an helper function of #WriteLabels defined below.
         *
         * \param[in,out] part_index Ensight writes parts numbered from 1 to Npart (DO NOT PUT 0: it leads to
         * a segmentation faults when loading Ensight..). So this is an increased index, as we can't rely upon mesh
         * label for this (Medit meshes labels may start at 0).
         */
        void FlushBlock(
            std::ostream& file_out,
            const std::map<::MoReFEM::Advanced::GeomEltNS::EnsightName, GeometricElt::vector_shared_ptr>& current_block,
            const MeshLabel::const_shared_ptr& current_label,
            bool do_print_index,
            std::size_t& part_index)
        {
            if (!current_block.empty())
            {
                assert(current_label);
                // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
                file_out << "part" << std::setw(8) << part_index++ << '\n';

                auto description = current_label->GetDescription();
                Utilities::String::Replace(" ", "_", description);
                file_out << description << '\n';

                for (const auto& pair : current_block)
                {
                    file_out << pair.first << '\n';
                    const auto& local_geometric_elt_list = pair.second;
                    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
                    file_out << std::setw(8) << local_geometric_elt_list.size() << '\n';

                    for (const auto& block_geom_elt : local_geometric_elt_list) // all geometric elements of the same
                                                                                // geometric_type and same label
                        block_geom_elt->WriteEnsightFormat(file_out, do_print_index);
                }
            }
        }


        /*
         * /brief Write the part of Ensight file dedicated to labels
         *
         * \param[in] file_out The output stream to which the data are written
         * \param[in] geometric_elt_list List of all geometric elements in the mesh. These geometric elements
         * will be sort by label and types before being written (Ensight format is a list of blocks, with
         * geometrical geometric elements put together in a block.
         * So for instance in a block we'll have first all triangles3, then all tetra4, etc...
         * \param[in] do_print_index True if the geometric elementis preceded by its internal index
         */
        void WriteLabels(std::ostream& file_out,
                         const GeometricElt::vector_shared_ptr& geometric_elt_list,
                         bool do_print_index)
        {
            MeshLabel::const_shared_ptr current_label(nullptr);
            std::map<::MoReFEM::Advanced::GeomEltNS::EnsightName, GeometricElt::vector_shared_ptr> current_block;

            std::size_t part_index = 1UL;

            for (const auto& geometric_elt_ptr : geometric_elt_list)
            {
                assert(!(!geometric_elt_ptr));
                const auto& geometric_element = *geometric_elt_ptr;

                if (geometric_element.GetMeshLabelPtr() != current_label)
                {
                    // Write to file the previous label block (if any)
                    FlushBlock(file_out, current_block, current_label, do_print_index, part_index);

                    // Prepare next block
                    current_block.clear();

                    // Beginning a new label block
                    current_label = geometric_element.GetMeshLabelPtr();
                }

                current_block[geometric_element.GetEnsightName()].push_back(geometric_elt_ptr);
            }

            // Flush the last block in memory once the loop is done
            FlushBlock(file_out, current_block, current_label, do_print_index, part_index);
        }

    } // anonymous namespace


} // namespace MoReFEM::Internal::MeshNS::FormatNS::Ensight


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
