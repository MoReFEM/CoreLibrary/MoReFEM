// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_ENSIGHT_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_ENSIGHT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <map>
#include <optional>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Internal::MeshNS { class RefGeomEltAndMeshLabel; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS::FormatNS::Ensight
{

    /*!
     * \brief Read an Ensight 6 geometric file.
     *
     * It reads Ensight 6 format file; there are absolutely no guarantee it is able to read any valid
     * Ensight file (on the contrary, I'm positive it does not). However it is able to read the Ensight
     * files likely to be given as input in MoReFEM.
     *
     * An Ensight case file is also required to read it with Ensight client.
     *
     *
     * \copydoc doxygen_hide_mesh_constructor_3_bis
     * \copydoc doxygen_hide_mesh_constructor_4
     * \copydoc doxygen_hide_space_unit_arg
     * \param[out] dimension Dimension of the mesh, as read in the input file.
     * \param[in] mesh_id Unique identifier of the \a Mesh for the construction of which
     * present function is called.
     * \param[in] Nprocessor_wise_geom_elt_per_type_and_label Number of processor-wise \a GeometricElt
     * sort per \a RefGeomElt and \a MeshLabel. This optional parameter is required only when we rebuild
     * from prepartitioned data; it is used to tag whether a given \a GeometricElt is processor-wise or
     * ghost.
     *
     */
    void ReadFile(const ::MoReFEM::MeshNS::unique_id mesh_id,
                  const ::MoReFEM::FilesystemNS::File& mesh_file,
                  ::MoReFEM::CoordsNS::space_unit_type space_unit,
                  const std::optional<std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t>>&
                      Nprocessor_wise_geom_elt_per_type_and_label,
                  ::MoReFEM::GeometryNS::dimension_type& dimension,
                  GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                  GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list,
                  Coords::vector_shared_ptr& coords_list,
                  MeshLabel::vector_const_shared_ptr& mesh_label_list);


    /*!
     * \brief Write a mesh in Ensight format.
     *
     * \copydoc doxygen_hide_geometry_format_write_common_arg
     */
    void WriteFile(const Mesh& mesh, const ::MoReFEM::FilesystemNS::File& mesh_file);


} // namespace MoReFEM::Internal::MeshNS::FormatNS::Ensight


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_ENSIGHT_DOT_HPP_
// *** MoReFEM end header guards *** < //
