// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <source_location>
#include <sstream>
#include <string>

#include "Utilities/Filesystem/File.hpp"

#include "Geometry/Domain/StrongType.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"

// IWYU pragma: no_include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"

#include "Geometry/Mesh/Internal/Format/Exceptions/Ensight.hpp" // IWYU pragma: associated


namespace // anonymous
{

    // Declaration here; definitions at the end of the file
    std::string InvalidCoordsMsg(const MoReFEM::FilesystemNS::File& ensight_file,
                                 std::size_t expected_number,
                                 std::size_t number_found);
    std::string InvalidThirdOrFourthLineMsg(const MoReFEM::FilesystemNS::File& ensight_file, std::size_t line_number);
    std::string BadNumberOfEltsInLabelMsg(const MoReFEM::FilesystemNS::File& ensight_file,
                                          MoReFEM::MeshLabelNS::index_type index,
                                          const std::string& description,
                                          const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                          std::size_t expected_number,
                                          std::size_t number_found);
    std::string InvalidLabelBlockMsg(const MoReFEM::FilesystemNS::File& ensight_file, const std::string& string_read);
    std::string InvalidNumberOfGeometricEltsMsg(const MoReFEM::FilesystemNS::File& ensight_file);

} // namespace


namespace MoReFEM::ExceptionNS::Format::Ensight
{


    InvalidThirdOrFourthLine::~InvalidThirdOrFourthLine() = default;


    InvalidThirdOrFourthLine::InvalidThirdOrFourthLine(const FilesystemNS::File& ensight_file,
                                                       std::size_t line_number,
                                                       const std::source_location location)
    : Exception(InvalidThirdOrFourthLineMsg(ensight_file, line_number), location)
    { }


    InvalidCoords::~InvalidCoords() = default;


    InvalidCoords::InvalidCoords(const FilesystemNS::File& ensight_file,
                                 std::size_t expected_number,
                                 std::size_t number_found,
                                 const std::source_location location)
    : Exception(InvalidCoordsMsg(ensight_file, expected_number, number_found), location)
    { }


    BadNumberOfEltsInLabel::~BadNumberOfEltsInLabel() = default;


    BadNumberOfEltsInLabel::BadNumberOfEltsInLabel(
        const FilesystemNS::File& ensight_file,
        MeshLabelNS::index_type index,
        const std::string& description,
        const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
        std::size_t expected_number,
        std::size_t number_found,
        const std::source_location location)
    : Exception(BadNumberOfEltsInLabelMsg(ensight_file,
                                          index,
                                          description,
                                          geometric_elt_name,
                                          expected_number,
                                          number_found),
                location)
    { }


    InvalidLabelBlock::~InvalidLabelBlock() = default;


    InvalidLabelBlock::InvalidLabelBlock(const FilesystemNS::File& ensight_file,
                                         const std::string& string_read,
                                         const std::source_location location)
    : Exception(InvalidLabelBlockMsg(ensight_file, string_read), location)
    { }


    InvalidNumberOfGeometricElts::~InvalidNumberOfGeometricElts() = default;


    InvalidNumberOfGeometricElts::InvalidNumberOfGeometricElts(const FilesystemNS::File& ensight_file,
                                                               const std::source_location location)
    : Exception(InvalidNumberOfGeometricEltsMsg(ensight_file), location)
    { }


} // namespace MoReFEM::ExceptionNS::Format::Ensight


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string FileInformation(const MoReFEM::FilesystemNS::File& ensight_file)
    {
        std::ostringstream oconv;
        oconv << "Error in Ensight file ";
        oconv << ensight_file << ": ";
        return oconv.str();
    }


    std::string InvalidCoordsMsg(const MoReFEM::FilesystemNS::File& ensight_file,
                                 std::size_t expected_number,
                                 std::size_t number_found)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);
        oconv << "there were " << expected_number << " coords announced in the Ensight file but " << number_found
              << " were actually found.";
        return oconv.str();
    }


    std::string InvalidThirdOrFourthLineMsg(const MoReFEM::FilesystemNS::File& ensight_file, std::size_t line_number)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);

        switch (line_number)
        {
        case 3:
            oconv << "invalid third line: expected format was 'node id *status*'";
            break;
        case 4:
            oconv << "invalid fourth line: expected format was 'element id *status*'";
            break;
        default:
            // Should never occur: if so this is clearly a bug
            std::cerr << "Line number should be 3 or 4, not " << line_number << '\n';
            assert(false);
        }

        oconv << " where status is among 'off', 'given', 'assign' or 'ignore'";
        return oconv.str();
    }


    std::string BadNumberOfEltsInLabelMsg(const MoReFEM::FilesystemNS::File& ensight_file,
                                          MoReFEM::MeshLabelNS::index_type index,
                                          const std::string& description,
                                          const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                          std::size_t expected_number,
                                          std::size_t number_found)
    {
        std::ostringstream oconv;
        oconv << FileInformation((ensight_file));
        oconv << "there were " << expected_number << ' ' << geometric_elt_name
              << " announced in the Ensight file for label " << description << " (index = " << index << ") but "
              << number_found << " were actually found.";
        return oconv.str();
    }


    std::string InvalidLabelBlockMsg(const MoReFEM::FilesystemNS::File& ensight_file, const std::string& string_read)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);
        oconv << "Label block is expected to begin with a line 'part ###'";
        oconv << "; instead the string '" << string_read << " 'was read";
        return oconv.str();
    }


    std::string InvalidNumberOfGeometricEltsMsg(const MoReFEM::FilesystemNS::File& ensight_file)
    {
        std::ostringstream oconv;
        oconv << FileInformation(ensight_file);
        oconv << "Number of geometric elements expected in current label couldn't be read properly";
        return oconv.str();
    }


} // namespace
