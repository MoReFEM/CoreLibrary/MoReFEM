// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// IWYU pragma: private,  include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_ENSIGHT_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_ENSIGHT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>


#include "Geometry/Domain/StrongType.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::Format::Ensight
{


    /*!
     * \brief Thrown if third or fourth line is not what is expected
     *
     * Expected format is "XXX id YYY" where XXX is 'node' or 'element' and YYY is among
     * <off/given/assign/ignore>
     */
    class InvalidThirdOrFourthLine final : public Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] line_number 3 or 4
         * \param[in] ensight_file Ensight file being read
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidThirdOrFourthLine(const FilesystemNS::File& ensight_file,
                                          std::size_t line_number,
                                          const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidThirdOrFourthLine() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidThirdOrFourthLine(const InvalidThirdOrFourthLine& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidThirdOrFourthLine(InvalidThirdOrFourthLine&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidThirdOrFourthLine& operator=(const InvalidThirdOrFourthLine& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidThirdOrFourthLine& operator=(InvalidThirdOrFourthLine&& rhs) = default;
    };


    /*!
     * \brief Thrown when the announced number of coords has not been retrieved.
     */
    class InvalidCoords final : public Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] expected_number Number of coords plainly written in the Ensight file
         * \param[in] number_found Size of the list of coords actually read in thesame Ensight file
         * \param[in] ensight_file Ensight file being read
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidCoords(const FilesystemNS::File& ensight_file,
                               std::size_t expected_number,
                               std::size_t number_found,
                               const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidCoords() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidCoords(const InvalidCoords& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidCoords(InvalidCoords&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidCoords& operator=(const InvalidCoords& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidCoords& operator=(InvalidCoords&& rhs) = default;
    };

    /*!
     * \brief Thrown when the announced number of geometric elements has not been retrieved.
     */
    class BadNumberOfEltsInLabel final : public Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] index Index of the block considered (the 'part ###' bit in the Ensign=ht file)
         * \param[in] description Name associated to the label considered.
         * \param[in] expected_number Number of coords plainly written in the Ensight file
         * \param[in] geometric_elt_name Name of the geometric element being currently read.
         * \param[in] number_found Size of the list of coords actually read in thesame Ensight file
         * \param[in] ensight_file Ensight file being read
         * \copydoc doxygen_hide_source_location

         */
        explicit BadNumberOfEltsInLabel(const FilesystemNS::File& ensight_file,
                                        MeshLabelNS::index_type index,
                                        const std::string& description,
                                        const ::MoReFEM::Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                        std::size_t expected_number,
                                        std::size_t number_found,
                                        const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~BadNumberOfEltsInLabel() override;

        //! \copydoc doxygen_hide_copy_constructor
        BadNumberOfEltsInLabel(const BadNumberOfEltsInLabel& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        BadNumberOfEltsInLabel(BadNumberOfEltsInLabel&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        BadNumberOfEltsInLabel& operator=(const BadNumberOfEltsInLabel& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        BadNumberOfEltsInLabel& operator=(BadNumberOfEltsInLabel&& rhs) = default;
    };


    /*!
     * \brief Thrown when a block doesn't begin with the expected string
     */
    class InvalidLabelBlock final : public Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] ensight_file Ensight file being read\
         * \param[in] string_read String read where "part" was expected
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidLabelBlock(const FilesystemNS::File& ensight_file,
                                   const std::string& string_read,
                                   const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidLabelBlock() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidLabelBlock(const InvalidLabelBlock& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidLabelBlock(InvalidLabelBlock&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidLabelBlock& operator=(const InvalidLabelBlock& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidLabelBlock& operator=(InvalidLabelBlock&& rhs) = default;
    };


    /*!
     * \brief Thrown when number of geometric elements can't be read properly
     */
    class InvalidNumberOfGeometricElts final : public Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] ensight_file Ensight file being read
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidNumberOfGeometricElts(const FilesystemNS::File& ensight_file,
                                              const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidNumberOfGeometricElts() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidNumberOfGeometricElts(const InvalidNumberOfGeometricElts& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidNumberOfGeometricElts(InvalidNumberOfGeometricElts&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidNumberOfGeometricElts& operator=(const InvalidNumberOfGeometricElts& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidNumberOfGeometricElts& operator=(InvalidNumberOfGeometricElts&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::Format::Ensight


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_ENSIGHT_DOT_HPP_
// *** MoReFEM end header guards *** < //
