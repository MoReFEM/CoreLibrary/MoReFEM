// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <sstream>
#include <string>

#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp" // IWYU pragma: associated

#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{
    // Only declarations are provided here; definitions are at the end of this file

    std::string FileInformation(const MoReFEM::FilesystemNS::File& input_file);
    std::string UnableToOpenFileMsg(const MoReFEM::FilesystemNS::File& input_file);
    std::string UnsupportedGeometricEltMsg(const std::string& geometric_elt_identifier, const std::string& format);


} // namespace


namespace MoReFEM::ExceptionNS::Format
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    UnableToOpenFile::~UnableToOpenFile() = default;


    UnableToOpenFile::UnableToOpenFile(const FilesystemNS::File& input_file, const std::source_location location)
    : Exception(UnableToOpenFileMsg(input_file), location)
    { }


    UnsupportedGeometricElt::~UnsupportedGeometricElt() = default;


    UnsupportedGeometricElt::UnsupportedGeometricElt(const std::string& geometric_elt_identifier,
                                                     const std::string& format,
                                                     const std::source_location location)
    : Exception(UnsupportedGeometricEltMsg(geometric_elt_identifier, format), location)
    { }


} // namespace MoReFEM::ExceptionNS::Format


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

// Definitions are provided here; declarations were provided at the beginning of this file
namespace // anonymous
{


    std::string FileInformation(const MoReFEM::FilesystemNS::File& input_file)
    {
        std::ostringstream oconv;
        oconv << "Error in Format file ";
        oconv << input_file << ": ";
        return oconv.str();
    }


    std::string UnableToOpenFileMsg(const MoReFEM::FilesystemNS::File& input_file)
    {
        std::ostringstream oconv;
        oconv << FileInformation(input_file);
        oconv << "Unable to open file";

        return oconv.str();
    }


    std::string UnsupportedGeometricEltMsg(const std::string& geometric_elt_identifier, const std::string& format)
    {
        std::ostringstream oconv;
        oconv << "Can't write in " << format << " format: geometric element '" << geometric_elt_identifier
              << "' is not supported";
        return oconv.str();
    }


} // namespace
