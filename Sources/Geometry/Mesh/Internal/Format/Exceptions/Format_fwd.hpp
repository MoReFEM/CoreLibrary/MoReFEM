// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_FORMAT_FWD_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_FORMAT_FWD_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Geometry/Mesh/Internal/Format/Exceptions/Ensight.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Medit.hpp"

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_FORMAT_FWD_DOT_HPP_
// *** MoReFEM end header guards *** < //
