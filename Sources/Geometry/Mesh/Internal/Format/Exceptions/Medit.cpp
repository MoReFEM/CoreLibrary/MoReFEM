// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// IWYU pragma: private,  include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <ostream>
#include <source_location>
#include <sstream>
#include <string>

#include "Geometry/Mesh/Internal/Format/Exceptions/Medit.hpp" // IWYU pragma: associated

#include "Utilities/Filesystem/File.hpp"

#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"

// IWYU pragma: no_include "Geometry/Mesh/Internal/Format/Exceptions/Format_fwd.hpp"


namespace // anonymous
{


    // Only declarations are provided here; definitions are at the end of this file

    std::string FileInformation(const MoReFEM::FilesystemNS::File& medit_filename);
    std::string
    UnableToOpenMsg(const MoReFEM::FilesystemNS::File& medit_filename, int mesh_version, const std::string& action);
    std::string InvalidExtensionMsg(const MoReFEM::FilesystemNS::File& medit_filename, const std::string& action);
    std::string InvalidDimensionMsg(const MoReFEM::FilesystemNS::File& medit_filename, int dimension);
    std::string
    InvalidCoordIndexMsg(const MoReFEM::FilesystemNS::File& medit_filename, std::size_t index, std::size_t Ncoord);
    std::string InvalidPathMsg(const MoReFEM::FilesystemNS::File& medit_filename, const std::string& action);


    constexpr auto arch64{ 8 };

} // namespace


namespace MoReFEM::ExceptionNS::Format::Medit
{


    UnableToOpen::~UnableToOpen() = default;


    UnableToOpen::UnableToOpen(const FilesystemNS::File& medit_filename,
                               int mesh_version,
                               const std::string& action,
                               const std::source_location location)
    : Exception(UnableToOpenMsg(medit_filename, mesh_version, action), location)
    { }


    InvalidExtension::~InvalidExtension() = default;


    InvalidExtension::InvalidExtension(const FilesystemNS::File& medit_filename,
                                       const std::string& action,
                                       const std::source_location location)
    : Exception(InvalidExtensionMsg(medit_filename, action), location)
    { }


    InvalidPath::~InvalidPath() = default;


    InvalidPath::InvalidPath(const FilesystemNS::File& medit_filename,
                             const std::string& action,
                             const std::source_location location)
    : Exception(InvalidPathMsg(medit_filename, action), location)
    { }


    InvalidDimension::~InvalidDimension() = default;


    InvalidDimension::InvalidDimension(const FilesystemNS::File& medit_filename,
                                       int dimension,
                                       const std::source_location location)
    : Exception(InvalidDimensionMsg(medit_filename, dimension), location)
    { }


    InvalidCoordIndex::~InvalidCoordIndex() = default;


    InvalidCoordIndex::InvalidCoordIndex(const MoReFEM::FilesystemNS::File& medit_filename,
                                         std::size_t index,
                                         std::size_t Ncoord,
                                         const std::source_location location)
    : Exception(InvalidCoordIndexMsg(medit_filename, index, Ncoord), location)
    { }


} // namespace MoReFEM::ExceptionNS::Format::Medit


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

// Definitions are provided here; declarations were provided at the beginning of this file
namespace // anonymous
{


    std::string FileInformation(const MoReFEM::FilesystemNS::File& medit_filename)
    {
        std::ostringstream oconv;
        oconv << "Error in Medit file ";
        oconv << medit_filename << ": ";
        return oconv.str();
    }


    /*!
     * \brief This function writes on \a stream a warning when mesh version is 3 and architecture is not 64 bits
     *
     * \tparam SizeOfPointerT Must be sizeof(void*)
     */
    template<int SizeOfPointerT>
    void WarnInvalidArchitectureHelper(std::ostream& stream)
    {
        constexpr auto byte_size{ 8 };
        assert(SizeOfPointerT == sizeof(void*));
        stream << "Mesh version 3 expects 64 bits architecture, whereas size of a pointer on your system is "
               << SizeOfPointerT << ", which matches a " << SizeOfPointerT * byte_size << " bits architecture.";

        if (SizeOfPointerT > arch64)
            stream << "\nPlease provide a specialization for your type of architecture for "
                      "warnInvalidArchitectureHelper() template function";
    }


    template<>
    void WarnInvalidArchitectureHelper<arch64>([[maybe_unused]] std::ostream& stream)
    {

        static_assert(arch64 == sizeof(void*));
    }


    void WarnInvalidArchitecture(std::ostream& stream)
    {
        WarnInvalidArchitectureHelper<sizeof(void*)>(stream);
    }


    std::string
    UnableToOpenMsg(const MoReFEM::FilesystemNS::File& medit_filename, int mesh_version, const std::string& action)
    {
        std::ostringstream oconv;
        oconv << FileInformation(medit_filename);
        oconv << "Unable to " << action
              << " the file. Medit API is unfortunately not very specific, however "
                 "invalid file extension and invalid path should have already been ruled out.";

        if (mesh_version == 3)
            WarnInvalidArchitecture(oconv);

        return oconv.str();
    }


    std::string InvalidExtensionMsg(const MoReFEM::FilesystemNS::File& medit_filename, const std::string& action)
    {
        std::ostringstream oconv;
        oconv << FileInformation(medit_filename);
        oconv << "Unable to " << action
              << " the file: file extension is not valid (mesh or meshb extension are expected).";

        return oconv.str();
    }


    std::string InvalidPathMsg(const MoReFEM::FilesystemNS::File& medit_filename, const std::string& action)
    {
        std::ostringstream oconv;
        oconv << FileInformation(medit_filename);
        oconv << "Unable to " << action << " the file: path is invalid.";

        return oconv.str();
    }


    std::string InvalidDimensionMsg(const MoReFEM::FilesystemNS::File& medit_filename, int dimension)
    {
        std::ostringstream oconv;
        oconv << FileInformation(medit_filename);
        oconv << "Dimension read is incorrect: 2D or 3D mesh was expected and dimension read is " << dimension;

        return oconv.str();
    }


    std::string
    InvalidCoordIndexMsg(const MoReFEM::FilesystemNS::File& medit_filename, std::size_t index, std::size_t Ncoord)
    {
        std::ostringstream oconv;
        oconv << FileInformation(medit_filename);
        oconv << "One of the indexes read for a Coords is " << index
              << " whereas the expected values are "
                 "between 1 and the total number of coords in the mesh ("
              << Ncoord << " here).";

        return oconv.str();
    }


} // namespace
