// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_MEDIT_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_MEDIT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>


#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::Format::Medit
{


    //! Thrown when the file was found but couldn't be interpreted as a Medit file.
    class UnableToOpen final : public Exception
    {

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] medit_filename Medit file being read
         * \param[in] mesh_version Libmesh5 format.
         * Libmesh5 handles 3 different formats:
         *    - '1' supports only floats
         *    - '2' throws in doubles
         *    - '3' removes the 2Go cap for a given file
         * The version has been read by dedicated libmesh function (or not if the file doesn't exist...).
         * It is used because the version may explain some reading failure
         * \param[in] action Either "read" or "write" expected.
         * \copydoc doxygen_hide_source_location
         */
        explicit UnableToOpen(const FilesystemNS::File& medit_filename,
                              int mesh_version,
                              const std::string& action,
                              const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~UnableToOpen() override;

        //! \copydoc doxygen_hide_copy_constructor
        UnableToOpen(const UnableToOpen& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnableToOpen(UnableToOpen&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnableToOpen& operator=(const UnableToOpen& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnableToOpen& operator=(UnableToOpen&& rhs) = default;
    };


    //! Thrown when file extension is invalid.
    class InvalidExtension final : public Exception
    {

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] medit_filename Medit file being read
         * \param[in] action Either "read" or "write" expected.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidExtension(const FilesystemNS::File& medit_filename,
                                  const std::string& action,
                                  const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidExtension() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidExtension(const InvalidExtension& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidExtension(InvalidExtension&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidExtension& operator=(const InvalidExtension& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidExtension& operator=(InvalidExtension&& rhs) = default;
    };


    //! Thrown when the path doesn't exist.
    class InvalidPath final : public Exception
    {

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] medit_filename Medit file being read
         * \param[in] action Either "read" or "write" expected.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidPath(const FilesystemNS::File& medit_filename,
                             const std::string& action,
                             const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidPath() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidPath(const InvalidPath& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidPath(InvalidPath&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidPath& operator=(const InvalidPath& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidPath& operator=(InvalidPath&& rhs) = default;
    };


    //! Thrown when dimension is incorrect.
    class InvalidDimension final : public Exception
    {

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] medit_filename Medit file being read
         * \param[in] dimension Dimension read in the file
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidDimension(const FilesystemNS::File& medit_filename,
                                  int dimension,
                                  const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidDimension() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidDimension(const InvalidDimension& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidDimension(InvalidDimension&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidDimension& operator=(const InvalidDimension& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidDimension& operator=(InvalidDimension&& rhs) = default;
    };


    /*!
     * \brief Thrown when the coord index read are not correct.
     */
    class InvalidCoordIndex final : public Exception
    {

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] medit_filename Medit file being read.
         * \param[in] index Index read.
         * \param[in] Ncoord Total number of coord in the mesh.
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidCoordIndex(const MoReFEM::FilesystemNS::File& medit_filename,
                                   std::size_t index,
                                   std::size_t Ncoord,
                                   const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidCoordIndex() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidCoordIndex(const InvalidCoordIndex& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidCoordIndex(InvalidCoordIndex&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidCoordIndex& operator=(const InvalidCoordIndex& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidCoordIndex& operator=(InvalidCoordIndex&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::Format::Medit


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_MEDIT_DOT_HPP_
// *** MoReFEM end header guards *** < //
