// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
// IWYU pragma: no_include <__tree>
#include <map>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>

#include "Utilities/Containers/Internal/EnumClassIterator.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    namespace // anonymous
    {


        /*!
         * \brief Recursive function to generate the mapping between format names and the enum class.
         */
        template<::MoReFEM::MeshNS::Format CurrentType>
        void GenerateMappingImpl(std::map<std::string, ::MoReFEM::MeshNS::Format>& name_mapping)
        {
            using TypeIterator = Internal::EnumClassNS::Iterator<::MoReFEM::MeshNS::Format, CurrentType>;
            [[maybe_unused]] auto ret = name_mapping.insert({ Information<CurrentType>::Name(), CurrentType });

            assert(ret.second && "No name should be present twice!");

            GenerateMappingImpl<TypeIterator::Increment()>(name_mapping);
        }


        //! End the recursion.
        template<>
        void GenerateMappingImpl<::MoReFEM::MeshNS::Format::End>(
            [[maybe_unused]] std::map<std::string, ::MoReFEM::MeshNS::Format>& name_mapping)
        { }


        /*!
         * \brief Generate the mapping between format names and the enum class.
         */
        std::map<std::string, ::MoReFEM::MeshNS::Format> GenerateMapping()
        {
            std::map<std::string, ::MoReFEM::MeshNS::Format> ret;
            GenerateMappingImpl<::MoReFEM::MeshNS::Format::Begin>(ret);
            return ret;
        }


    } // namespace


    ::MoReFEM::MeshNS::Format GetType(const std::string& format_name)
    {
        static auto mapping = GenerateMapping();

        auto it = mapping.find(format_name);

        if (it == mapping.cend())
        {
            std::ostringstream oconv;

            oconv << "No type matching format name " << format_name << " were found. The possible values are: " << '\n';
            Utilities::PrintContainer<Utilities::PrintPolicyNS::Key>::Do(
                mapping,
                oconv,
                ::MoReFEM::PrintNS::Delimiter::separator("\n\t- "),
                ::MoReFEM::PrintNS::Delimiter::opener("\t- "),
                ::MoReFEM::PrintNS::Delimiter::closer("\n"));

            throw MoReFEM::Exception(oconv.str());
        }

        return it->second;
    }


} // namespace MoReFEM::Internal::MeshNS::FormatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
