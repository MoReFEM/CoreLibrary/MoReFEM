// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_FORMAT_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_FORMAT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <type_traits> // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced { enum class GeometricEltEnum : std::size_t; }
namespace MoReFEM::MeshNS { enum class Format; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS::FormatNS
{

    /*!
     * \brief Helper class to provide the information related to a known format.
     *
     */
    template<::MoReFEM::MeshNS::Format TypeT>
    struct Information
    { };


    /*!
     * \brief Get the format associated to \a format_name.
     *
     * \param[in] format_name Name of the format (e.g. 'Medit'). This name must be provided by a method
     * Name() of a specialization of \a Information template class.
     *
     * \return Enum value associated to the format.
     */
    ::MoReFEM::MeshNS::Format GetType(const std::string& format_name);


    /*!
     * \brief Default behaviour for TypeT support: no support.
     *
     * By default TypeT is not supported by a GeometricElt; you must specialize this class to
     * indicate TypeT is appropriate for a GeometricElt. The specialization must inherit from
     * std::true_type, and the body gives the identifier in TypeT (Medit in the following example):
     *
     * \code
     * template<>
     * struct Medit<Advanced::GeometricEltEnum::Segment2> : public std::true_type
     * {
     * //! Medit code for this object
     * static constexpr GmfKwdCod MeditId() { return GmfEdges; }
     * };
     *
     * \endcode
     *
     */
    template<::MoReFEM::MeshNS::Format TypeT, Advanced::GeometricEltEnum NatureT>
    struct Support : public std::false_type
    { };

} // namespace MoReFEM::Internal::MeshNS::FormatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Mesh/Internal/Format/Ensight.hpp"
#include "Geometry/Mesh/Internal/Format/Information.hpp"
#include "Geometry/Mesh/Internal/Format/Medit.hpp"
#include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hpp"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_FORMAT_DOT_HPP_
// *** MoReFEM end header guards *** < //
