// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Geometry/Mesh/Internal/Format/Information.hpp"

#include "Geometry/Mesh/Internal/Format/Format.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    const std::string& Information<::MoReFEM::MeshNS::Format::Ensight>::Name()
    {
        static const std::string ret("Ensight");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Ensight>::Extension()
    {
        static const std::string ret("geo");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Medit>::Name()
    {
        static const std::string ret("Medit");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Medit>::Extension()
    {
        static const std::string ret("mesh");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Vizir>::Name()
    {
        static const std::string ret("Vizir");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Vizir>::Extension()
    {
        static const std::string ret("mesh");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::VTK_PolygonalData>::Name()
    {
        static const std::string ret("VTK_PolygonalData");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::VTK_PolygonalData>::Extension()
    {
        static const std::string ret("vtk");
        return ret;
    }


} // namespace MoReFEM::Internal::MeshNS::FormatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
