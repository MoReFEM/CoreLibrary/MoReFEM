// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_INFORMATION_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_INFORMATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/Mesh/Format.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::MeshNS::FormatNS { template <::MoReFEM::MeshNS::Format TypeT> struct Information; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    /*!
     * \brief Specialization of Information struct that provides generic information about Ensight format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::Ensight>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the Ensight files.
        static const std::string& Extension();
    };


    /*!
     * \brief Specialization of Information struct that provides generic information about Medit format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::Medit>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the Medit files.
        static const std::string& Extension();
    };


    /*!
     * \brief Specialization of Information struct that provides generic information about
     * VTK_PolygonalData format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::VTK_PolygonalData>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the VTK_PolygonalData files.
        static const std::string& Extension();
    };


    /*!
     * \brief Specialization of Information struct that provides generic information about Medit format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::Vizir>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the Medit files.
        static const std::string& Extension();
    };


} // namespace MoReFEM::Internal::MeshNS::FormatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_INFORMATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
