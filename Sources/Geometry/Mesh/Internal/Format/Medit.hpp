// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_MEDIT_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_MEDIT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <map>
#include <optional>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS::FormatNS::Medit
{

    /*!
     * \brief Read a mesh in Medit format.
     *
     * \copydoc doxygen_hide_mesh_constructor_1_bis
     *
     * \copydoc doxygen_hide_mesh_constructor_3_bis
     *
     * \copydoc doxygen_hide_mesh_constructor_4
     *
     * \param[in] Nprocessor_wise_geom_elt_per_type Number of processor-wise \a GeometricElt sort per
     * \a RefGeomElt. This optional parameter is required only when we rebuild from prepartitioned data;
     it is used
     * to tag whether a given \a GeometricElt is processor-wise or ghost.

     */
    void ReadFile(const ::MoReFEM::MeshNS::unique_id unique_id,
                  const ::MoReFEM::FilesystemNS::File& mesh_file,
                  ::MoReFEM::CoordsNS::space_unit_type space_unit,
                  std::optional<std::map<std::size_t, std::size_t>> Nprocessor_wise_geom_elt_per_type,
                  ::MoReFEM::GeometryNS::dimension_type& dimension,
                  GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                  GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list,
                  Coords::vector_shared_ptr& coords_list,
                  MeshLabel::vector_const_shared_ptr& mesh_label_list);

    /*!
     * \brief Write a mesh in Medit format.
     *
     * \copydoc doxygen_hide_geometry_format_write_common_arg
     * \param[in] version Version of Medit to use (usually choose '2'; '1' is for float precision).
     */
    void WriteFile(const Mesh& mesh, const ::MoReFEM::FilesystemNS::File& mesh_file, int version = 2);


} // namespace MoReFEM::Internal::MeshNS::FormatNS::Medit


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_MEDIT_DOT_HPP_
// *** MoReFEM end header guards *** < //
