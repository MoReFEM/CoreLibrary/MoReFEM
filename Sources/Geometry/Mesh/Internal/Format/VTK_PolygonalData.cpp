// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <fstream> // IWYU pragma: keep
#include <iostream>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hpp" // IWYU pragma: associated

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Mesh.hpp"

namespace MoReFEM::Internal::MeshNS::FormatNS::VTK_PolygonalData
{


    [[noreturn]] void ReadFile([[maybe_unused]] const ::MoReFEM::MeshNS::unique_id mesh_id,
                               [[maybe_unused]] const ::MoReFEM::FilesystemNS::File& mesh_file,
                               [[maybe_unused]] ::MoReFEM::CoordsNS::space_unit_type space_unit,
                               [[maybe_unused]] ::MoReFEM::GeometryNS::dimension_type& dimension,
                               [[maybe_unused]] GeometricElt::vector_shared_ptr& unsort_geom_element_list,
                               [[maybe_unused]] Coords::vector_shared_ptr& coords_list,
                               [[maybe_unused]] MeshLabel::vector_const_shared_ptr& mesh_label_list)
    {
        assert(false && "Read file not implemented yet for VTK_PolygonalData.");
        exit(EXIT_FAILURE);
    }


    void WriteFile(const Mesh& mesh, const ::MoReFEM::FilesystemNS::File& mesh_file)
    {
        std::ofstream file_out{ mesh_file.NewContent() };

        if (!file_out)
            throw ExceptionNS::Format::UnableToOpenFile(mesh_file);

        std::cout << "Writing to file " << mesh_file << '\n';

        file_out << "# vtk DataFile Version 1.0\n";
        file_out << "VTK Mesh from MoReFEM\n";
        file_out << "ASCII\n";
        file_out << "DATASET POLYDATA\n";

        // Write points.
        file_out << "POINTS ";

        decltype(auto) processor_wise_coords_list = mesh.GetProcessorWiseCoordsList();
        decltype(auto) ghost_coords_list = mesh.GetGhostCoordsList();

        auto Ncoords = processor_wise_coords_list.size() + ghost_coords_list.size();
        file_out << Ncoords << " double\n";

        // Now write all coords
        for (const auto& coords_ptr : processor_wise_coords_list)
            WriteVTK_PolygonalDataFormat(*coords_ptr, file_out);

        for (const auto& coords_ptr : ghost_coords_list)
            WriteVTK_PolygonalDataFormat(*coords_ptr, file_out);

        // VERTICES LINES  POLYGONS TRIANGLE_STRIPS
        file_out << "POLYGONS ";

        std::cout << "[WARNING] The VTK format will only write the triangles of the mesh considered for now: "
                  << mesh_file << '\n';

        decltype(auto) geometric_type =
            Advanced::GeometricEltFactory::GetInstance().GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);

        auto subset = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(geometric_type);

        std::size_t number_of_elements = 0;

        for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
        {
            ++number_of_elements;
        }

        file_out << number_of_elements << ' ' << 4 * number_of_elements << '\n';

        for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
        {
            const auto& geom_elt_ptr = *it_geom_elemt;
            assert(!(!geom_elt_ptr));

            const auto& geom_elt_coords_list = geom_elt_ptr->GetCoordsList();

            assert(geom_elt_coords_list.size() == 3);

            file_out << "3" << '\t' << geom_elt_coords_list[0]->GetIndexFromMeshFile() << '\t'
                     << geom_elt_coords_list[1]->GetIndexFromMeshFile() << '\t'
                     << geom_elt_coords_list[2]->GetIndexFromMeshFile() << '\n';
        }
    }


} // namespace MoReFEM::Internal::MeshNS::FormatNS::VTK_PolygonalData


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
