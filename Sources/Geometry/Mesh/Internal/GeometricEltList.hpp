// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_GEOMETRICELTLIST_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_GEOMETRICELTLIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <unordered_map>
#include <utility>

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM::Internal::MeshNS
{


    /*!
     * \brief Type useful to access quickly elements in the GeometricEltList class.
     *
     * Key of the map is index of the label.
     * Value of the map is a pair which elements are:
     *  - first: starting index to the elements having this label.
     *  - second: the number of elements having this label.
     */
    using geom_elt_per_label_id_type = std::unordered_map<MeshLabelNS::index_type, std::pair<std::size_t, std::size_t>>;


    /*!
     * \brief Convenient alias over the underlying storage of the GeometricElt in GeometricEltList.
     *
     * See geometric_elt_lookup_helper_ of GeometricEltList for an explanation of the content of this
     * ugly container.
     *
     */
    using geom_elt_lookup_type = std::unordered_map<MoReFEM::Advanced::GeometricEltEnum, geom_elt_per_label_id_type>;


    /*!
     * \brief Helper class designed to be used within Mesh to store all the elements.
     *
     * GeometricElts need to be stored efficiently, but we also need to be able to access them quickly.
     *
     * The current class intends to be in charge of all these operations.
     *
     * The storage of GeometricElt (in data_) is done the following way:
     * . Criterion 1 is the dimension.
     * . Criterion 2 is the element type ("Triangle3", "Hexa6", etc...)
     * . Criterion 3 is the label to which the element belongs. So all triangles for a label which
     * index is *i* will be packed together.
     */
    class GeometricEltList final
    {


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit GeometricEltList() = default;

        //! Destructor.
        ~GeometricEltList() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GeometricEltList(const GeometricEltList& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GeometricEltList(GeometricEltList&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GeometricEltList& operator=(const GeometricEltList& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GeometricEltList& operator=(GeometricEltList&& rhs) = delete;


        ///@}

        //! Clear the object.
        void Clear();

        //! Initialize from a list of geometric elements (a priori not yet sort).
        //! \param[in] unsort_list The list of \a GeometricElt, typically read from a mesh file. They
        //! still need to be sort properly in MoReFEM internal storage for maximum efficiency.
        //! \param[in] do_print If true, print onto std::cout information about the \a GeometricElt read.
        void Init(const GeometricElt::vector_shared_ptr& unsort_list, bool do_print);

        //! Number of geometric elements.
        std::size_t NgeometricElt() const;

        //! Number of geometric elements of a given dimension.
        //! \param[in] dimension Dimension used as filter.
        //! \return Number of \a GeometricElt with the chosen \a dimension.
        std::size_t NgeometricElt(::MoReFEM::GeometryNS::dimension_type dimension) const;

        //! Number of geometric elements of a given \a RefGeomElt and with a given \a MeshLabel.
        //! \param[in] ref_geom_elt \a RefGeomElt we want to tally.
        //! \param[in] mesh_label \a MeshLabel we want to tally.
        //! \return Number of \a GeometricElt with the chosen \a ref_geom_elt and \a mesh_label.
        std::size_t NgeometricElt(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label) const;


        /*!
         * \brief Access to the list of all geometric elements.
         *
         * \return All geometric elements as a vector of smart pointers.
         */
        const GeometricElt::vector_shared_ptr& All() const noexcept;

        /*!
         * \brief Obtains the position in the internal storage of all GeometricElt that shares the same
         * dimension.
         *
         * \param[in] dimension Dimension of the sought \a GeometricElt.
         *
         * \return Pair <first index, number of geometric elements that share the same dimension>.
         */
        const std::pair<std::size_t, std::size_t>&
        GetLocationInEltList(::MoReFEM::GeometryNS::dimension_type dimension) const;


        /*!
         * \brief Obtains the position in the internal storage of all GeometricElt that shares the type
         * of RefGeomElt.
         *
         * \param[in] ref_geom_elt Type of the geometric element requested.
         *
         * \return Pair <first index, number of geometric elements that share the same type, regardless
         * of the label type>
         *
         * The elements themselves can be retrieved afterwards by the [] operator:
         * \code
         * GeometricEltList list;
         * ... definition ...
         * RefGeomElt geo_type(new Triangle3);
         *
         * auto all_elements_of_given_type = list.GetLocationInEltList(geo_type);
         * std::size_t first_element = all_elements_of_given_type.first;
         * std::size_t last_element = first_element + all_elements_of_given_type.second - 1UL ;
         * for (std::size_t i = first_element; i <= last_element; ++i)
         *   ... access to each element through list[i] ...
         * \endcode
         *
         *
         */
        std::pair<std::size_t, std::size_t> GetLocationInEltList(const RefGeomElt& ref_geom_elt) const;


        /*!
         * \brief Same as the namesake method except we consider only elements that belongs to a given label.
         *
         * \param[in] ref_geom_elt Type of the geometric element requested.
         * \param[in] label_index Index of the label considered.
         * \param[out] out Pair <first index, number of geometric elements that share the same type and label
         *
         * \return True if at least one element match the request, false if the label doesn't exist for the
         * given geometric element type. In case the geometric type doesn't exist, an exception is thrown
         * (because it highlights a discrepancy somewhere in the code; for the label it is different as we
         * didn't enforce in detail how they are partitioned between all geometric element types).
         *
         */
        bool GetLocationInEltList(const RefGeomElt& ref_geom_elt,
                                  MeshLabelNS::index_type label_index,
                                  std::pair<std::size_t, std::size_t>& out) const;


        /*!
         * \brief Returns the pair (first index)in each label for a given type of geometric element.
         *
         * \param[in] ref_geom_elt Instance of the type of geometric element considered (the default one is
         * fine: only identified is important here).
         *
         * \return Key of the unordered map is the index of the label. Value is a pair: first value of which
         * is the first index in the list and second value is the number of elements that share both the element
         * type and the label.
         */
        const geom_elt_per_label_id_type& GetLocationInEltListPerLabel(const RefGeomElt& ref_geom_elt) const;


        /*!
         * \brief Returns all the types of geometric elements in the object.
         *
         * \return Vector which include all RefGeomElts found in the object.
         */
        RefGeomElt::vector_shared_ptr BagOfEltType() const;

        /*!
         * \brief Same as namesake methods except we limit ourselves to a given dimension.
         *
         * \param[in] dimension Only objects of this dimension will be considered.
         *
         * \return Vector which include all RefGeomElts of a given dimension found in the object.
         */
        RefGeomElt::vector_shared_ptr BagOfEltType(::MoReFEM::GeometryNS::dimension_type dimension) const;


        /*!
         * \brief Returns a list of all elements that belongs to the same label.
         *
         * \param[in] label Smart pointer standing for the label.
         *
         * \return The list as a vector of smart pointers.
         */
        GeometricElt::vector_shared_ptr GeometricEltListInLabel(const MeshLabel::const_shared_ptr& label) const;


        //! \copydoc doxygen_hide_mesh_get_geom_elt_from_index_1
        const GeometricElt& GetGeometricEltFromIndex(::MoReFEM::GeomEltNS::index_type index) const;


        //! \copydoc doxygen_hide_mesh_get_geom_elt_from_index_2
        const GeometricElt& GetGeometricEltFromIndex(::MoReFEM::GeomEltNS::index_type index,
                                                     const RefGeomElt& ref_geom_elt) const;


      private:
        /*!
         * \brief The actual underlying data.
         *
         * Detail of the layout is given in the class header documentation.
         */
        GeometricElt::vector_shared_ptr data_;


        /*!
         * \brief A minimalist container to be able to fetch quickly all the geometric elements of a given kind.
         *
         * Key of the first map: identifier of the GeometricElt. (for instance 'Triangle3').
         * Value of the first map is a second map.
         * Key of the second map is index of the label.
         * Value of the second map is a pair which elements are:
         *  - first: starting index to the elements having this label
         *  - second: the number of elements having this label.
         *
         * This quite ugly container is supposed to be purely internal; it shouldn't be left accessible to
         * the public interface!
         *
         */
        geom_elt_lookup_type geometric_elt_lookup_helper_;


        //! Key is the dimension, value is the pair <First index, number of elements>.
        std::map<::MoReFEM::GeometryNS::dimension_type, std::pair<std::size_t, std::size_t>> dimension_accessor_;
    };

    //! Convenient alias.
    using iterator_geometric_element = GeometricElt::vector_shared_ptr::const_iterator;

    //! Alias for subset.
    using subset_range = std::pair<iterator_geometric_element, iterator_geometric_element>;


    /*!
     * \brief Helper function to create a vector of geometric elements given the position of the
     * first element in it and the number of elements considered.
     *
     * \param[in] pair Pair <first_element, Nelt>
     * \param[in] geometric_elt_list List of all geometric elements that belongs to the mesh.
     * Mesh::geometric_elt_list_ is expected here.
     *
     * \return Pair <begin, end> that returns all the geometric elements meeting the requirements.
     */
    subset_range SubsetRange(const std::pair<std::size_t, std::size_t>& pair,
                             const Internal::MeshNS::GeometricEltList& geometric_elt_list);


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Mesh/Internal/GeometricEltList.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_GEOMETRICELTLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
