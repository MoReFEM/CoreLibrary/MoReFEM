// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_GEOMETRICELTLIST_DOT_HXX_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_GEOMETRICELTLIST_DOT_HXX_
// IWYU pragma: private, include "Geometry/Mesh/Internal/GeometricEltList.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Internal::MeshNS
{


    inline std::size_t GeometricEltList::NgeometricElt() const
    {
        return data_.size();
    }


    inline const GeometricElt::vector_shared_ptr& GeometricEltList::All() const noexcept
    {
        return data_;
    }


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_GEOMETRICELTLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
