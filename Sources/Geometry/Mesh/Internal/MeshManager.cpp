// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::MeshNS
{


    MeshManager::~MeshManager() = default;


    const std::string& MeshManager::ClassName()
    {
        static const std::string ret("MeshManager");
        return ret;
    }


    MeshManager::MeshManager()
    {
        storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    void
    MeshManager::Create(const ::MoReFEM::MeshNS::unique_id unique_id,
                        const ::MoReFEM::FilesystemNS::File& mesh_file,
                        ::MoReFEM::GeometryNS::dimension_type dimension,
                        ::MoReFEM::MeshNS::Format format,
                        const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                        Mesh::BuildEdge do_build_edge,
                        Mesh::BuildFace do_build_face,
                        Mesh::BuildVolume do_build_volume,
                        Mesh::BuildPseudoNormals do_build_pseudo_normals,
                        std::optional<std::reference_wrapper<::MoReFEM::Wrappers::Lua::OptionFile>> prepartitioned_data)
    {
        InsertMesh(Internal::WrapUniqueToConst(new Mesh(unique_id,
                                                        mesh_file,
                                                        dimension,
                                                        format,
                                                        space_unit,
                                                        prepartitioned_data,
                                                        do_build_edge,
                                                        do_build_face,
                                                        do_build_volume,
                                                        do_build_pseudo_normals)));
    }


    void MeshManager ::Create(::MoReFEM::GeometryNS::dimension_type dimension,
                              const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                              GeometricElt::vector_shared_ptr&& unsort_processor_wise_geom_elt_list,
                              GeometricElt::vector_shared_ptr&& unsort_ghost_geom_elt_list,
                              Coords::vector_shared_ptr&& coords_list,
                              MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                              Mesh::BuildEdge do_build_edge,
                              Mesh::BuildFace do_build_face,
                              Mesh::BuildVolume do_build_volume,
                              Mesh::BuildPseudoNormals do_build_pseudo_normals)
    {
        if (unsort_processor_wise_geom_elt_list.empty())
            throw Exception("A mesh must contain at least one GeometricElement");

        const auto& any_elt_ptr = unsort_processor_wise_geom_elt_list.back();
        assert(!(!any_elt_ptr));
        const auto unique_id = any_elt_ptr->GetMeshIdentifier();

        assert(std::ranges::all_of(unsort_processor_wise_geom_elt_list,

                                   [unique_id](const auto& elt_ptr)
                                   {
                                       assert(!(!elt_ptr));
                                       return elt_ptr->GetMeshIdentifier() == unique_id;
                                   }));

        assert(std::ranges::all_of(unsort_ghost_geom_elt_list,

                                   [unique_id](const auto& elt_ptr)
                                   {
                                       assert(!(!elt_ptr));
                                       return elt_ptr->GetMeshIdentifier() == unique_id;
                                   }));

        InsertMesh(Internal::WrapUniqueToConst(new Mesh(unique_id,
                                                        dimension,
                                                        space_unit,
                                                        std::move(unsort_processor_wise_geom_elt_list),
                                                        std::move(unsort_ghost_geom_elt_list),
                                                        std::move(coords_list),
                                                        std::move(mesh_label_list),
                                                        do_build_edge,
                                                        do_build_face,
                                                        do_build_volume,
                                                        do_build_pseudo_normals)));
    }


    void MeshManager::LoadFromPrepartitionedData(const ::MoReFEM::Wrappers::Mpi& mpi,
                                                 ::MoReFEM::MeshNS::unique_id unique_id,
                                                 ::MoReFEM::Wrappers::Lua::OptionFile& prepartitioned_data,
                                                 ::MoReFEM::GeometryNS::dimension_type dimension,
                                                 ::MoReFEM::MeshNS::Format format)
    {
        ::MoReFEM::CoordsNS::space_unit_type space_unit{};
        bool do_build_edges{};
        bool do_build_faces{};
        bool do_build_volumes{};
        std::filesystem::path mesh_file{};

        prepartitioned_data.Read("mesh_file", "", mesh_file);
        prepartitioned_data.Read("space_unit", "", space_unit);
        prepartitioned_data.Read("do_build_edges", "", do_build_edges);
        prepartitioned_data.Read("do_build_faces", "", do_build_faces);
        prepartitioned_data.Read("do_build_volumes", "", do_build_volumes);

        InsertMesh(
            Internal::WrapUniqueToConst(new Mesh(mpi,
                                                 unique_id,
                                                 ::MoReFEM::FilesystemNS::File{ std::move(mesh_file) },
                                                 prepartitioned_data,
                                                 dimension,
                                                 format,
                                                 space_unit,
                                                 do_build_edges ? Mesh::BuildEdge::yes : Mesh::BuildEdge::no,
                                                 do_build_faces ? Mesh::BuildFace::yes : Mesh::BuildFace::no,
                                                 do_build_volumes ? Mesh::BuildVolume::yes : Mesh::BuildVolume::no)));
    }


    const Mesh& MeshManager::GetMesh(::MoReFEM::MeshNS::unique_id unique_id) const
    {
        decltype(auto) storage = GetStorage();
        auto it = storage.find(unique_id);

        assert(it != storage.cend());
        assert(!(!(it->second)));

        return *(it->second);
    }


    void WriteInterfaceListForEachMesh(
        const std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&
            mesh_output_directory_storage,
        std::string_view filename)
    {
        for (const auto& pair : mesh_output_directory_storage)
            WriteInterfaceList(pair, filename);
    }


    void MeshManager::InsertMesh(Mesh::const_unique_ptr&& mesh_ptr)
    {
        assert(!(!mesh_ptr));
        const auto unique_id = mesh_ptr->GetUniqueId();

        auto&& pair = std::make_pair(unique_id, std::move(mesh_ptr));

        auto& storage = GetNonCstStorage();

        auto [it, was_properly_inserted] = storage.insert(std::move(pair));

        if (!was_properly_inserted)
            throw Exception("Two mesh objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id.Get()) + ").");
    }


    ::MoReFEM::MeshNS::unique_id MeshManager::GenerateUniqueId()
    {
        return Mesh::GenerateNewEligibleId();
    }


    void MeshManager::Clear()
    {
        decltype(auto) storage = GetNonCstStorage();
        storage.clear();
        Mesh::ClearUniqueIdList();
    }


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
