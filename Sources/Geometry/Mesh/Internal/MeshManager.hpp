// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_MESHMANAGER_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_MESHMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <map>
#include <optional>
#include <unordered_map>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Internal/Mesh.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "Geometry/Mesh/Mesh.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Lua { class OptionFile; }
namespace MoReFEM::TestNS { template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> struct ClearSingletons; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS
{


    /*!
     * \brief This class is used to create and retrieve Mesh objects.
     *
     * Mesh objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * Mesh object given its unique id (which is the one that appears in the input data file).
     *
     */
    class MeshManager : public Utilities::Singleton<MeshManager>
    {

      public:
        //! Alias to storage.
        using storage_type = std::unordered_map<::MoReFEM::MeshNS::unique_id, Mesh::const_unique_ptr>;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        friend struct MoReFEM::TestNS::ClearSingletons;


        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::MeshNS::Tag;

        /*!
         * \brief Create a \a Mesh object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);

        /*!
         * \brief Load the mesh related data from pre-partitioned data.
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
        >
        // clang-format on
        void LoadFromPrepartitionedData(const IndexedSectionDescriptionT& indexed_section_description,
                                        const MoReFEMDataT& morefem_data);

        //! Fetch the mesh object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{Mesh}
        const Mesh& GetMesh(::MoReFEM::MeshNS::unique_id unique_id) const;

        //! Fetch the mesh object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{Mesh}
        Mesh& GetNonCstMesh(::MoReFEM::MeshNS::unique_id unique_id);


        /*!
         * \brief Create a brand new Mesh and returns a reference to it.
         *
         * \copydoc doxygen_hide_mesh_constructor_5
         *
         * \copydoc doxygen_hide_mesh_constructor_1
         *
         * \copydoc doxygen_hide_mesh_constructor_2
         *
         * \copydoc doxygen_hide_mesh_optional_prepartitioned_data_arg
         *
         * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test
         * executables; however in full-fledged model instances you should not use this constructor at all:
         * the other one above calls it with the correct data from the input data file.
         * \endinternal
         */
        void Create(const ::MoReFEM::MeshNS::unique_id unique_id,
                    const ::MoReFEM::FilesystemNS::File& mesh_file,
                    ::MoReFEM::GeometryNS::dimension_type dimension,
                    ::MoReFEM::MeshNS::Format format,
                    const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                    Mesh::BuildEdge do_build_edge = Mesh::BuildEdge::no,
                    Mesh::BuildFace do_build_face = Mesh::BuildFace::no,
                    Mesh::BuildVolume do_build_volume = Mesh::BuildVolume::no,
                    Mesh::BuildPseudoNormals do_build_pseudo_normals = Mesh::BuildPseudoNormals::no,
                    std::optional<std::reference_wrapper<::MoReFEM::Wrappers::Lua::OptionFile>> prepartitioned_data =
                        std::nullopt);


        /*!
         * \brief Create a brand new Mesh and returns a reference to it.
         *
         * \copydoc doxygen_hide_space_unit_arg
         * \param[in] dimension Dimension of the mesh.
         * \copydoc doxygen_hide_mesh_constructor_2
         * \copydoc doxygen_hide_mesh_constructor_3
         *
         * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test
         * executables; however in full-fledged model instances you should not use this constructor at all:
         * the other one above calls it with the correct data from the input data file.
         * \endinternal
         */
        void Create(::MoReFEM::GeometryNS::dimension_type dimension,
                    ::MoReFEM::CoordsNS::space_unit_type space_unit,
                    GeometricElt::vector_shared_ptr&& unsort_processor_wise_geom_elt_list,
                    GeometricElt::vector_shared_ptr&& unsort_ghost_geom_elt_list,
                    Coords::vector_shared_ptr&& coords_list,
                    MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                    Mesh::BuildEdge do_build_edge,
                    Mesh::BuildFace do_build_face,
                    Mesh::BuildVolume do_build_volume,
                    Mesh::BuildPseudoNormals do_build_pseudo_normals = Mesh::BuildPseudoNormals::no);


        /*!
         * \brief Create a new Mesh from data that were computed in a previous run.
         *
         * \copydoc doxygen_hide_mpi_param
         *
         * \param[in] format Format of the input file.
         * \param[in] unique_id Unique identifier of the \a Mesh to be created.
         * \param[in] prepartitioned_data Lua file which gives the data needed to reconstruct the data
         * from pre-computed partitioned data. Note: it is not const as such objects relies on a Lua stack
         * which is modified for virtually every operation.
         * \param[in,out] dimension Dimension of the mesh read in the input file.
         *
         */
        void LoadFromPrepartitionedData(const ::MoReFEM::Wrappers::Mpi& mpi,
                                        ::MoReFEM::MeshNS::unique_id unique_id,
                                        ::MoReFEM::Wrappers::Lua::OptionFile& prepartitioned_data,
                                        ::MoReFEM::GeometryNS::dimension_type dimension,
                                        ::MoReFEM::MeshNS::Format format);


        /*!
         * \brief Constant accessor to the store the mesh objects.
         *
         * Key is the unique id of each mesh.
         * Value is the actual mesh.
         *
         * \return All meshes in the form of pairs (unique_id, pointer to mesh object).
         */
        const storage_type& GetStorage() const noexcept;

        /*!
         * \brief Yields a unique id currently not used.
         *
         * This method is used in very specific instances and  should not be used lightly; think twice before
         * using it!
         *
         * The generated value is guaranteed not to be in the container of Mesh unique ids; it is <b>NOT</b>
         * added inside.
         *
         * \return Unique id not currently used.
         */
        static ::MoReFEM::MeshNS::unique_id GenerateUniqueId();


      private:
        //! \copydoc doxygen_hide_manager_clear
        void Clear();

        /*!
         * \brief Non constant accessor to the store the mesh objects.
         *
         * Key is the unique id of each mesh.
         * Value is the actual mesh.
         *
         * \return All meshes in the form of pairs (unique_id, pointer to mesh object).
         */
        storage_type& GetNonCstStorage() noexcept;


        /*!
         * \brief Helper method to Create() in charge of adding new mesh to storage.
         *
         * \param[in] mesh_ptr Pointer to the mesh to be introduced.
         */
        void InsertMesh(Mesh::const_unique_ptr&& mesh_ptr);


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        MeshManager();

        //! Destructor.
        virtual ~MeshManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<MeshManager>;
        ///@}


      private:
        /*!
         * \brief Store the mesh objects.
         *
         * Key is the unique id of each mesh.
         * Value is the actual mesh.
         */
        storage_type storage_;
    };


    /*!
     * \brief Write the interface list for each mesh.
     *
     * Should be called only on root processor.
     * \param[in] mesh_output_directory_storage Key is the unique id of the meshes, value the path to the
     * associated output directory, which should already exist.
     *  \param[in] filename Name if the file to be written in output directory; default value is what is
     * expected by facilities such as tests or post-processing.(the functionality to modify it has been
     * introduced mostly for dev purposes; tests and post-processing expect the default value).
     */
    // clang-format off
    void WriteInterfaceListForEachMesh(const std::map
                                       <
                                            ::MoReFEM::MeshNS::unique_id,
                                            ::MoReFEM::FilesystemNS::Directory::const_unique_ptr
                                       >& mesh_output_directory_storage,
                                       std::string_view filename = "interfaces.hhdata");
    // clang-format on


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Mesh/Internal/MeshManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_MESHMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
