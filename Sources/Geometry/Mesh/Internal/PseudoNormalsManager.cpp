// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <string>
#include <vector>

#include "Geometry/Mesh/Internal/PseudoNormalsManager.hpp"

#include "Utilities/Miscellaneous.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal
{


    PseudoNormalsManager::~PseudoNormalsManager() = default;


    const std::string& PseudoNormalsManager::ClassName()
    {
        static const std::string ret("PseudoNormalsManager");
        return ret;
    }


    PseudoNormalsManager::PseudoNormalsManager() = default;


    // NOLINTBEGIN(readability-convert-member-functions-to-static) - in fact the whole class should be dropped - see
    // #1912
    void PseudoNormalsManager::Create(const std::vector<::MoReFEM::DomainNS::unique_id>& domain_index_list, Mesh& mesh)
    {
        std::cout << "===========================================" << '\n';
        std::cout << "[WARNING] Pseudo-normals computation." << '\n';
        std::cout << "For now the computation is limited to Triangle3 and the corresponding Edges and Vertices."
                  << '\n';
        std::cout << "In the case of a volumic mesh the orientation of the triangles with respect to the volume is "
                     "tested and in this case all the triangles must be defined with an outside oriented normal."
                  << '\n';

        const auto& domain_manager = DomainManager::GetInstance();

        std::vector<MeshLabelNS::index_type> label_list_index;

        // If domain_index_list is empty no restriction applied on domain.
        if (domain_index_list.empty())
        {
            const auto& mesh_label_list = mesh.GetLabelList();

            const std::size_t mesh_label_list_size = mesh_label_list.size();

            assert(std::ranges::none_of(mesh_label_list, Utilities::IsNullptr<MeshLabel::const_shared_ptr>));

            for (std::size_t i = 0UL; i < mesh_label_list_size; ++i)
            {
                const auto& mesh_label = *mesh_label_list[i];
                label_list_index.push_back(mesh_label.GetIndex());
            }
        } else
        {
            for (const auto domain_index : domain_index_list)
            {
                const auto& mesh_label_list = domain_manager.GetDomain(domain_index).GetMeshLabelList();

                assert(std::ranges::none_of(mesh_label_list,

                                            Utilities::IsNullptr<MeshLabel::const_shared_ptr>));

                const std::size_t mesh_label_list_size = mesh_label_list.size();

                for (std::size_t j = 0UL; j < mesh_label_list_size; ++j)
                {
                    const auto& mesh_label = *mesh_label_list[j];
                    label_list_index.push_back(mesh_label.GetIndex());
                }
            }
        }

        // mesh.ComputePseudoNormals(label_list_index); //#938 deactivated for the moment.

        std::cout << "===========================================" << '\n';
    }
    // NOLINTEND(readability-convert-member-functions-to-static)

    void PseudoNormalsManager::Clear()
    { }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
