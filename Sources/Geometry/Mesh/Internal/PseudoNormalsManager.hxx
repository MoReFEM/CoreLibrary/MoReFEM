// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_PSEUDONORMALSMANAGER_DOT_HXX_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_PSEUDONORMALSMANAGER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Mesh/Internal/PseudoNormalsManager.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/InputData/Extract.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"


namespace MoReFEM::Internal
{


    template<class PseudoNormalsSectionT>
    void PseudoNormalsManager::Create(const PseudoNormalsSectionT& section)
    {


        decltype(auto) mesh_index =
            ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename PseudoNormalsSectionT::MeshIndex>(
                section);
        decltype(auto) domain_index_list =
            ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename PseudoNormalsSectionT::DomainIndexList>(
                section);

        auto& mesh = Internal::MeshNS::MeshManager::GetInstance().GetNonCstMesh(mesh_index);

        Create(domain_index_list, mesh);
    }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_PSEUDONORMALSMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
