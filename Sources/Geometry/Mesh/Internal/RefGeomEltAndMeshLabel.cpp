// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <ostream>
#include <sstream>
#include <string>
#include <string_view>

#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

// IWYU pragma: no_include <__compare/ordering.h>


namespace MoReFEM::Internal::MeshNS
{


    namespace // anonymous
    {


        std::string GenerateIdentifier(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label)
        {
            std::ostringstream oconv;
            oconv << ref_geom_elt.GetName() << "_mesh_label_" << mesh_label.GetIndex();

            return oconv.str();
        }


    } // namespace


    RefGeomEltAndMeshLabel::RefGeomEltAndMeshLabel(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label)
    : identifier_(GenerateIdentifier(ref_geom_elt, mesh_label))
    { }


    RefGeomEltAndMeshLabel::RefGeomEltAndMeshLabel(std::string_view identifier) : identifier_(identifier)
    { }


    std::ostream& operator<<(std::ostream& stream, const RefGeomEltAndMeshLabel& rhs)
    {
        stream << rhs.GetIdentifier();
        return stream;
    }


    bool operator<(const RefGeomEltAndMeshLabel& lhs, const RefGeomEltAndMeshLabel& rhs)
    {
        return lhs.GetIdentifier() < rhs.GetIdentifier();
    }


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
