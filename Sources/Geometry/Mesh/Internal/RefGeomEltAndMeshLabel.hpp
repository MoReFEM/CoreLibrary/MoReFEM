// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_REFGEOMELTANDMESHLABEL_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_REFGEOMELTANDMESHLABEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iostream>
#include <string>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class MeshLabel; }
namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS
{


    /*!
     * \brief An ad hoc object to serve as key in some containers.
     *
     * This is useful to amalgamate \a RefGeomElt and \a MeshLabel into a single quantity that can be used as key in
     * an ordered container (this is needed for instance for Ensight format, for which we want to store for
     * prepartitioned data how many processor-wise \a GeometricElt are associated with a given \a RefGeomElt and \a
     * MeshLabel).
     */
    class RefGeomEltAndMeshLabel
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = RefGeomEltAndMeshLabel;

        //! With this declaration, the class yields true regarding IsString<RefGeomEltAndMeshLabel>::value.
        static constexpr bool identified_as_string = true;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] ref_geom_elt The \a RefGeomElt we want to refer with the created object.
         * \param[in] mesh_label The \a MeshLabel we want to refer with the created object.
         */
        explicit RefGeomEltAndMeshLabel(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label);

        /*!
         * \brief Constructor for deserialization.
         *
         * \param[in] identifier Identifier from which an object is to be created (typically this identifier should
         * have been generated through GetIdentifier() in an earlier run of MoReFEM).
         */
        explicit RefGeomEltAndMeshLabel(std::string_view identifier);

        //! Destructor.
        ~RefGeomEltAndMeshLabel() = default;

        //! \copydoc doxygen_hide_copy_constructor
        RefGeomEltAndMeshLabel(const RefGeomEltAndMeshLabel& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        RefGeomEltAndMeshLabel(RefGeomEltAndMeshLabel&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        RefGeomEltAndMeshLabel& operator=(const RefGeomEltAndMeshLabel& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RefGeomEltAndMeshLabel& operator=(RefGeomEltAndMeshLabel&& rhs) = delete;

        ///@}

        //! Accessor to the generated identifier.
        const std::string& GetIdentifier() const noexcept;

      private:
        //! Identifier to use in the map.
        const std::string identifier_;
    };


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     */
    std::ostream& operator<<(std::ostream& stream, const RefGeomEltAndMeshLabel& rhs);


    //! \copydoc doxygen_hide_operator_less
    bool operator<(const RefGeomEltAndMeshLabel& lhs, const RefGeomEltAndMeshLabel& rhs);


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_REFGEOMELTANDMESHLABEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
