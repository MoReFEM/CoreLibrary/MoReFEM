// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <fstream>

#include "Geometry/Mesh/Internal/Format/Ensight.hpp"
#include "Geometry/Mesh/Internal/Format/Medit.hpp"
#include "Geometry/Mesh/Internal/Format/VTK_PolygonalData.hpp"

// IWYU pragma: no_include <__tree>

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iterator>
#include <map>
#include <memory>
#include <optional>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Sort.hpp"
#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/Advanced/SortingCriterion.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Internal/ComputeInterfaceListInMesh.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Internal/GeometricEltList.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Internal/RefGeomEltAndMeshLabel.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{


    void Mesh::ReduceLabelList()
    {
        auto& label_list = GetNonCstLabelList();
        label_list.clear();

        Utilities::PointerComparison::Set<MeshLabel::const_shared_ptr> label_to_keep;

        const auto& geometric_elt_list = GetGeometricEltList<RoleOnProcessor::processor_wise>();

        for (const auto& geometric_elt_ptr : geometric_elt_list)
        {
            assert(!(!geometric_elt_ptr));
            assert(geometric_elt_ptr->GetMeshLabelPtr() != nullptr);

            label_to_keep.insert(geometric_elt_ptr->GetMeshLabelPtr());

            const auto& coords_list_of_geometric_element = geometric_elt_ptr->GetCoordsList();

            for (const auto& coord_ptr : coords_list_of_geometric_element)
            {
                assert(!(!coord_ptr));

                // Not a given: for instance Ensight format do not provide it to its \a Coords.
                if (coord_ptr->GetMeshLabelPtr())
                    label_to_keep.insert(coord_ptr->GetMeshLabelPtr());
            }
        }

        assert(std::ranges::none_of(label_to_keep, Utilities::IsNullptr<MeshLabel::const_shared_ptr>));

        label_list.resize(label_to_keep.size());
        std::ranges::move(label_to_keep, label_list.begin());

        assert(std::ranges::none_of(label_list, Utilities::IsNullptr<MeshLabel::const_shared_ptr>));
    }


    namespace // anonymous
    {


        /*!
         * \brief Determine the \a Coords that are already present in lower ranks (only counting the processor-wise ones
         * - ghost are discarded here).
         *
         * (so the list is empty for rank 0...)
         */
        std::vector<CoordsNS::index_from_mesh_file>
        ComputeAlreadyFoundOnLowerRanks(const Wrappers::Mpi& mpi,
                                        std::size_t Nprogram_wise_coords,
                                        const Coords::vector_shared_ptr& a_processor_wise_coords_list)
        {
            assert(mpi.Nprocessor<int>() > 1);
            const auto rank = mpi.GetRank();
            const auto Nproc = mpi.Nprocessor();

            std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> processor_wise_coords_id_list(
                a_processor_wise_coords_list.size());

            std::ranges::transform(a_processor_wise_coords_list,

                                   processor_wise_coords_id_list.begin(),
                                   [](const auto& coords_ptr)
                                   {
                                       assert(!(!coords_ptr));
                                       return coords_ptr->GetIndexFromMeshFile();
                                   });

            std::vector<CoordsNS::index_from_mesh_file> ret;

            if (rank == rank_type{ 0UL })
            {
                mpi.SendContainer(rank_type{ 1UL }, processor_wise_coords_id_list);
            } else if (rank < Nproc - rank_type{ 1UL })
            {
                ret = mpi.Receive<CoordsNS::index_from_mesh_file>(rank - rank_type{ 1UL }, Nprogram_wise_coords);

                auto to_send = ret;

                std::ranges::copy(processor_wise_coords_id_list,

                                  std::back_inserter(to_send));

                Utilities::EliminateDuplicate(to_send);

                mpi.SendContainer(rank + rank_type{ 1UL }, to_send);
            } else
                ret = mpi.Receive<CoordsNS::index_from_mesh_file>(rank - rank_type{ 1UL }, Nprogram_wise_coords);

            assert(std::ranges::is_sorted(ret));

            return ret;
        }


    } // namespace


    void Mesh::SetIsLowestProcessor(const Wrappers::Mpi& mpi,
                                    std::size_t Nprogram_wise_coords,
                                    const Coords::vector_shared_ptr& a_processor_wise_coords_list)
    {

        const auto already_on_lower_ranks =
            ComputeAlreadyFoundOnLowerRanks(mpi, Nprogram_wise_coords, a_processor_wise_coords_list);

        const auto begin_already_on_lower_ranks = already_on_lower_ranks.cbegin();
        const auto end_already_on_lower_ranks = already_on_lower_ranks.cend();

        for (const auto& coords_ptr : a_processor_wise_coords_list)
        {
            assert(!(!coords_ptr));

            const auto id = coords_ptr->GetIndexFromMeshFile();

            const bool already_known = std::binary_search(begin_already_on_lower_ranks, end_already_on_lower_ranks, id);

            coords_ptr->SetIsLowestProcessor(!already_known);
        }
    }


    void Mesh::SetReducedCoordsList(const Wrappers::Mpi& mpi,
                                    Coords::vector_shared_ptr&& a_processor_wise_coords_list,
                                    Coords::vector_shared_ptr&& a_ghost_coords_list)
    {
#ifndef NDEBUG
        {
            Coords::vector_shared_ptr intersection;
            std::ranges::set_intersection(a_processor_wise_coords_list,

                                          a_ghost_coords_list,

                                          std::back_inserter(intersection),
                                          [](const auto& lhs, const auto& rhs)
                                          {
                                              assert(!(!lhs));
                                              assert(!(!rhs));
                                              return lhs->GetProgramWisePosition() < rhs->GetProgramWisePosition();
                                          });

            assert(intersection.empty()
                   && "Ghost coords list is assumed not to encompass Coords already handled "
                      "by processor-wise list.");
        }
#endif // NDEBUG

        const auto Nprogram_wise_coords = GetProcessorWiseCoordsList().size();
        // < At this stage GetProcessorWiseCoordsList() still encompasses all program-wise Coords
        // (this will be wrong at the very next line...)

        SetProcessorWiseCoordsList(std::move(a_processor_wise_coords_list));
        SetGhostCoordsList(std::move(a_ghost_coords_list));

        decltype(auto) processor_wise_coords_list = GetProcessorWiseCoordsList();
        decltype(auto) ghost_coords_list = GetGhostCoordsList();

        // Set whether it is the lowest processor the \a Coords is given in processor-wise list.
        // As default is false, no operation needed for ghost \a Coords.
        {
            SetIsLowestProcessor(mpi, Nprogram_wise_coords, processor_wise_coords_list);

            // For safety mostly - the field shouldn't be called for for a ghost \a Coords.
            // (it is required to compute the number of \a Coords in a given \a Domain, which is done only on processor-
            // wise ones).
            for (const auto& coords_ptr : ghost_coords_list)
            {
                assert(!(!coords_ptr));
                coords_ptr->SetIsLowestProcessor(false);
            }
        }

        // Finally set the index which gives the position in the \a Mesh.
        {
            CoordsNS::processor_wise_position index{ 0UL };

            for (const auto& coords_ptr : processor_wise_coords_list)
            {
                assert(!(!coords_ptr));
                coords_ptr->SetProcessorWisePosition(index++);
            }

            // By convention, ghost coords indexing follow the same indexing (i.e. first ghost Coords gets
            // Nprocessor_wise_coords index).
            for (const auto& coords_ptr : ghost_coords_list)
            {
                assert(!(!coords_ptr));
                coords_ptr->SetProcessorWisePosition(index++);
            }
        }

        // As the \a Coords indexes have been recomputed, update accordingly the \a Coords list for \a Edge and \a Face
        // (they rely upon a specific ordering which depends on these indexes).
        {
            Internal::MeshNS::ComputeInterfaceListInMesh helper(*this);
            helper.ReorderCoordsInEdgesAndFaces();
        }
    }


    void Mesh::ShrinkToProcessorWise(const Wrappers::Mpi& mpi,
                                     const GeometricElt::vector_shared_ptr& processor_wise_geom_elt_list,
                                     const GeometricElt::vector_shared_ptr& ghost_geom_elt_list,
                                     Coords::vector_shared_ptr&& processor_wise_coords_list,
                                     Coords::vector_shared_ptr&& ghost_coords_list)
    {
        processor_wise_geometric_elt_list_.Clear();
        processor_wise_geometric_elt_list_.Init(processor_wise_geom_elt_list, false);

        ghost_geometric_elt_list_.Init(ghost_geom_elt_list, false);

        Nvertex_on_rank_ = DetermineNInterface<Vertex>();
        Nedge_ = DetermineNInterface<Edge>();
        Nface_ = DetermineNInterface<Face>();
        Nvolume_ = DetermineNInterface<Volume>();

        SetReducedCoordsList(mpi, std::move(processor_wise_coords_list), std::move(ghost_coords_list));

        ReduceLabelList();
    }


    namespace // anonymous
    {


        /*!
         * \brief If optional field prepartitioned data is defined, extract the field Nprocessor_wise_geom_elt_per_type
         * from it.
         *
         * This is useful for Medit mesh.
         */
        std::optional<std::map<std::size_t, std::size_t>> NprocessorWiseGeomEltPerType(
            const std::optional<std::reference_wrapper<Wrappers::Lua::OptionFile>>& prepartitioned_data)
        {
            if (!prepartitioned_data)
                return std::nullopt;

            std::map<std::size_t, std::size_t> ret;

            prepartitioned_data.value().get().Read("Nprocessor_wise_geom_elt_per_type", "", ret);

            return std::make_optional(ret);
        }


        /*!
         * \brief If optional field prepartitioned data is defined, extract the field
         * Nprocessor_wise_geom_elt_per_type_and_label from it.
         *
         * This is useful for Ensight mesh.
         */
        std::optional<std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t>>
        NprocessorWiseGeomEltPerTypeAndLabel(
            const std::optional<std::reference_wrapper<Wrappers::Lua::OptionFile>>& prepartitioned_data)
        {
            if (!prepartitioned_data)
                return std::nullopt;

            std::map<std::string, std::size_t> with_string_keys;

            prepartitioned_data.value().get().Read("Nprocessor_wise_geom_elt_per_type_and_label", "", with_string_keys);

            std::map<Internal::MeshNS::RefGeomEltAndMeshLabel, std::size_t> ret;

            for (const auto& [string_key, Nprocessor_wise_geom_elt] : with_string_keys)
            {
                Internal::MeshNS::RefGeomEltAndMeshLabel key(string_key);

                [[maybe_unused]] auto [it, is_inserted] =
                    ret.emplace(std::pair(std::move(key), Nprocessor_wise_geom_elt));
                assert(is_inserted);
            }

            return std::make_optional(ret);
        }


    } // namespace


    void Mesh::Read(const FilesystemNS::File& mesh_file,
                    MeshNS::Format format,
                    const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                    std::optional<std::reference_wrapper<Wrappers::Lua::OptionFile>> prepartitioned_data,
                    GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                    GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list,
                    Coords::vector_shared_ptr& coords_list,
                    MeshLabel::vector_const_shared_ptr& mesh_label_list)
    {
        {
            decltype(auto) factory = Advanced::GeometricEltFactory::CreateOrGetInstance();
            factory.CheckNotEmpty();
        }

        auto dimension_read = NumericNS::UninitializedIndex<::MoReFEM::GeometryNS::dimension_type>();

        switch (format)
        {
        case MeshNS::Format::Medit:
        {
            auto Nprocessor_wise_geom_elt_per_type = NprocessorWiseGeomEltPerType(prepartitioned_data);

            Internal::MeshNS::FormatNS::Medit::ReadFile(GetUniqueId(),
                                                        mesh_file,
                                                        space_unit,
                                                        Nprocessor_wise_geom_elt_per_type,
                                                        dimension_read,
                                                        unsort_processor_wise_geom_elt_list,
                                                        unsort_ghost_geom_elt_list,
                                                        coords_list,
                                                        mesh_label_list);

            break;
        }
        case MeshNS::Format::Ensight:
        {
            auto Nprocessor_wise_geom_elt_per_type_and_label =
                NprocessorWiseGeomEltPerTypeAndLabel(prepartitioned_data);

            Internal::MeshNS::FormatNS::Ensight::ReadFile(GetUniqueId(),
                                                          mesh_file,
                                                          space_unit,
                                                          Nprocessor_wise_geom_elt_per_type_and_label,
                                                          dimension_read,
                                                          unsort_processor_wise_geom_elt_list,
                                                          unsort_ghost_geom_elt_list,
                                                          coords_list,
                                                          mesh_label_list);

            break;
        }
        case MeshNS::Format::VTK_PolygonalData:
        case MeshNS::Format::Vizir:
        case MeshNS::Format::End:
            assert(false);
            break;
        }

        if (dimension_read < GetDimension())
        {
            std::ostringstream oconv;
            oconv << "Dimension given in the input file is higher than the one read in the mesh file " << mesh_file;

            throw Exception(oconv.str());
        }
    }


#if !defined(NDEBUG) && !defined(SONARQUBE_4_MOREFEM)
    void Mesh::CheckGeometricEltOrdering() const
    {
        auto copy_geometric_elt_list = GetGeometricEltList<RoleOnProcessor::processor_wise>();

        namespace sc = Advanced::GeometricEltNS::SortingCriterion;

        std::ranges::stable_sort(
            copy_geometric_elt_list,

            Utilities::Sort<GeometricElt::shared_ptr, sc::Dimension<>, sc::Type<>, sc::SurfaceRef<>>);

        assert(copy_geometric_elt_list == GetGeometricEltList<RoleOnProcessor::processor_wise>());
    }
#endif // NDEBUG & SONARQUBE_4_MOREFEM


    template<>
    std::array<std::size_t, 2UL> Mesh::DetermineNInterface<Volume>() const
    {
        const auto& processor_wise_geometric_elt_list = GetGeometricEltList<RoleOnProcessor::processor_wise>();
        const auto& ghost_geometric_elt_list = GetGeometricEltList<RoleOnProcessor::ghost>();

        auto is_non_null_volume = [](const auto& geom_elt_ptr)
        {
            assert(!(!geom_elt_ptr));
            return geom_elt_ptr->GetVolumePtr() != nullptr;
        };

        const auto Nprocessor = std::count_if(
            processor_wise_geometric_elt_list.cbegin(), processor_wise_geometric_elt_list.cend(), is_non_null_volume);

        const auto Nghost =
            std::count_if(ghost_geometric_elt_list.cbegin(), ghost_geometric_elt_list.cend(), is_non_null_volume);

        return { { static_cast<std::size_t>(Nprocessor), static_cast<std::size_t>(Nghost) } };
    }


    void
    WriteInterfaceList(const std::pair<const MeshNS::unique_id, FilesystemNS::Directory::const_unique_ptr>& mesh_infos,
                       std::string_view filename)
    {

        const auto& mesh_output_directory_ptr = mesh_infos.second;

        assert(!(!mesh_output_directory_ptr));

        const auto& mesh_output_directory = *mesh_output_directory_ptr;

        auto path = mesh_output_directory.AddFile(filename);

        decltype(auto) mesh = Internal::MeshNS::MeshManager::GetInstance().GetMesh(mesh_infos.first);

        std::ofstream out{ path.NewContent() };

        const Internal::MeshNS::ComputeInterfaceListInMesh mesh_interface_list(mesh);
        mesh_interface_list.Print(out);
    }


    template<>
    void Mesh::Write<MeshNS::Format::Ensight>(const FilesystemNS::File& mesh_file) const
    {
        Internal::MeshNS::FormatNS::Ensight::WriteFile(*this, mesh_file);
    }


    template<>
    void Mesh::Write<MeshNS::Format::Medit>(const FilesystemNS::File& mesh_file) const
    {
        constexpr int version = 2; // namely double precision - no weird stuff to handle very big files not handled
                                   // afterwards by Ensight.

        Internal::MeshNS::FormatNS::Medit::WriteFile(*this, mesh_file, version);
    }


    template<>
    void Mesh::Write<MeshNS::Format::VTK_PolygonalData>(const FilesystemNS::File& mesh_file) const
    {
        Internal::MeshNS::FormatNS::VTK_PolygonalData::WriteFile(*this, mesh_file);
    }


    void Mesh::ComputePseudoNormals(const std::vector<MeshLabelNS::index_type>& label_list_index,
                                    const Vertex::InterfaceMap& vertex_interface_list,
                                    const Edge::InterfaceMap& edge_interface_list,
                                    const Face::InterfaceMap& face_interface_list) const
    {
        const auto label_index_list_size = MeshLabelNS::index_type{ label_list_index.size() };

        for (MeshLabelNS::index_type label_index{ 0UL }; label_index < label_index_list_size; ++label_index)
        {
            decltype(auto) geometric_type =
                Advanced::GeometricEltFactory::GetInstance().GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);

            auto subset = GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(
                geometric_type, label_list_index[label_index.Get()]);

            for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
            {
                const auto& geom_elt_ptr = *it_geom_elemt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;
                assert(geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3);

                auto begin = face_interface_list.cbegin();
                auto end = face_interface_list.cend();

                const auto it = std::find_if(begin,
                                             end,
                                             [&geom_elt](const auto& face)
                                             {
                                                 assert(!(!face.first));

                                                 const auto& face_coord = face.first->GetCoordsList();
                                                 const auto& geom_elt_coord = geom_elt.GetCoordsList();
                                                 const auto face_size = face_coord.size();
                                                 const auto geom_elt_size = geom_elt_coord.size();

                                                 assert(geom_elt_size == face_size);

                                                 int found = 0;

                                                 for (std::size_t j = 0UL; j < face_size; ++j)
                                                 {
                                                     auto& face_item = *face_coord[j];

                                                     for (std::size_t k = 0UL; k < geom_elt_size; ++k)
                                                     {
                                                         auto& geom_elt_item = *geom_elt_coord[k];

                                                         if (face_item == geom_elt_item)
                                                         {
                                                             ++found;
                                                         }
                                                     }
                                                 }

                                                 return found == 3;
                                             });

                if (!(it == end))
                    it->first->ComputePseudoNormal(it->second);
                else
                    throw Exception("A face was not found in the geometric elements. This should not happen.");
            }
        }

        // std::cout << Face::NumberOfFacesPseudoNormalComputed() << '\t' << "pseudo-normals for Faces." << std::endl;

        for (MeshLabelNS::index_type label_index{ 0UL }; label_index < label_index_list_size; ++label_index)
        {
            decltype(auto) geometric_type =
                Advanced::GeometricEltFactory::GetInstance().GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);

            auto subset = GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(
                geometric_type, label_list_index[label_index.Get()]);

            for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
            {
                const auto& geom_elt_ptr = *it_geom_elemt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;
                assert(geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3);

                const auto& edge_list = geom_elt.GetOrientedEdgeList();

                const auto edge_list_size = edge_list.size();

                for (std::size_t j = 0; j < edge_list_size; ++j)
                {
                    auto& edge = *edge_list[j];
                    auto it = edge_interface_list.find(edge_list[j]->GetUnorientedInterfacePtr());
                    assert(it != edge_interface_list.cend());
                    const auto& geom_elemts = it->second;
                    edge.GetNonCstUnorientedInterface().ComputePseudoNormal(geom_elemts);
                }
            }
        }

        // std::cout << Edge::NumberOfEdgesPseudoNormalComputed() << '\t' << "pseudo-normals for Edges." << std::endl;

        for (MeshLabelNS::index_type label_index{ 0UL }; label_index < label_index_list_size; ++label_index)
        {
            decltype(auto) geometric_type =
                Advanced::GeometricEltFactory::GetInstance().GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);

            auto subset = GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(
                geometric_type, label_list_index[label_index.Get()]);

            for (auto it_geom_elemt = subset.first; it_geom_elemt != subset.second; ++it_geom_elemt)
            {
                const auto& geom_elt_ptr = *it_geom_elemt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;
                assert(geom_elt.GetIdentifier() == Advanced::GeometricEltEnum::Triangle3);

                const auto& vertex_list = geom_elt.GetVertexList();

                const auto vertex_list_size = vertex_list.size();

                for (std::size_t j = 0; j < vertex_list_size; ++j)
                {
                    auto& vertex = *vertex_list[j];
                    auto it = vertex_interface_list.find(vertex_list[j]);
                    assert(it != vertex_interface_list.cend());
                    const auto& geom_elemts = it->second;
                    vertex.ComputePseudoNormal(geom_elemts);
                }
            }
        }
    }


    void Mesh::SetGhostCoordsList(Coords::vector_shared_ptr&& list)
    {
        ghost_coords_list_ = std::move(list);
    }


    void Mesh::SetProcessorWiseCoordsList(Coords::vector_shared_ptr&& list)
    {
        processor_wise_coords_list_ = std::move(list);
    }


    void Mesh::SortPrepartitionedCoords(std::size_t Nprocessor_wise)
    {
        auto& full_coords_list = GetNonCstProcessorWiseCoordsList();
        assert(Nprocessor_wise <= full_coords_list.size());

        Coords::vector_shared_ptr ghost_list(full_coords_list.size() - Nprocessor_wise);

        auto it_separator =
            full_coords_list.begin() + static_cast<Coords::vector_shared_ptr::difference_type>(Nprocessor_wise);
        auto end = full_coords_list.end();

        std::move(it_separator, end, ghost_list.begin());

        full_coords_list.erase(it_separator, end);

        SetGhostCoordsList(std::move(ghost_list));
    }


    GeometricElt::vector_shared_ptr Mesh::ComputeProcessorWiseAndGhostGeometricEltList() const
    {
        decltype(auto) proc_wise_geom_elt_list = GetGeometricEltList<RoleOnProcessor::processor_wise>();
        decltype(auto) ghost_geom_elt_list = GetGeometricEltList<RoleOnProcessor::ghost>();

        GeometricElt::vector_shared_ptr ret(proc_wise_geom_elt_list.size() + ghost_geom_elt_list.size());

        auto it = std::ranges::copy(proc_wise_geom_elt_list, ret.begin());

        [[maybe_unused]] auto [_, end] = std::ranges::copy(ghost_geom_elt_list, it.out);

        assert(end == ret.end());
        return ret;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
