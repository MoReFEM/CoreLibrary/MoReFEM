// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_MESH_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_MESH_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <array>
#include <cstddef>
#include <functional>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

#include "Utilities/Containers/Array.hpp"
#include "Utilities/Containers/Sort.hpp" // IWYU pragma: keep
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Numeric/Numeric.hpp"   // IWYU pragma: export
#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"                            // IWYU pragma: export
#include "Geometry/Domain/MeshLabel.hpp"                         // IWYU pragma: export
#include "Geometry/GeometricElt/GeometricElt.hpp"                // IWYU pragma: export
#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp" // IWYU pragma: export
#include "Geometry/Mesh/Format.hpp"                              // IWYU pragma: export
#include "Geometry/Mesh/Internal/GeometricEltList.hpp"           // IWYU pragma: export
#include "Geometry/Mesh/UniqueId.hpp"                            // IWYU pragma: export
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"               // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Lua { class OptionFile; }
namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Internal { class PseudoNormalsManager; }
namespace MoReFEM::Internal::FEltSpaceNS { struct ReduceMesh; }
namespace MoReFEM::Internal::MeshNS { class MeshManager; }
namespace MoReFEM { enum class RoleOnProcessor; }
namespace MoReFEM { enum class RoleOnProcessorPlusBoth; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Class responsible for a mesh and its content.
     *
     * Objects of this class can only be created through the MeshManager.
     */
    class Mesh final : public Crtp::UniqueId<Mesh, MeshNS::unique_id, UniqueIdNS::AssignationMode::manual>
    {

      public:
        /*!
         * \class doxygen_hide_mesh_enum
         * \name Convenient enum classes.
         *
         * Their purpose is to make Init() reading more palatable:
         * \code
         * Mesh mesh;
         * mesh.Init(mesh_file, format, BuildEdge::yes, BuildFace::no);
         * \endcode
         * instead of:
         * \code
         * Mesh mesh;
         * mesh.Init(mesh_file, format, true, false);
         * \endcode
         * As a bonus, there is a type checking involved that prevent messing up the order.
         */

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildEdge { no, yes };

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildFace { no, yes };

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildVolume { no, yes };

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildPseudoNormals { no, yes };

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Mesh;

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Convenient alias to parent.
        using unique_id_parent = Crtp::UniqueId<self, MeshNS::unique_id, UniqueIdNS::AssignationMode::manual>;

        //! Alias to vector of unique_ptr.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;

        //! Convenient alias.
        using iterator_geometric_element = Internal::MeshNS::iterator_geometric_element;

        //! Alias for subset.
        using subset_range = Internal::MeshNS::subset_range;

        //! Returns the name of the class.
        static const std::string& ClassName();


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // \todo #9 Because it creates a spurious warning; I would like though the friendship to ultimately appear.
        // ============================

        //! Friendship.
        friend Internal::MeshNS::MeshManager;

        //! Friendship.
        friend Internal::FEltSpaceNS::ReduceMesh;

        //! Friendship.
        friend Internal::PseudoNormalsManager;

        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


      private:
        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor from a file.
         *
         * The constructor is private on purpose: Mesh objects are intended to be created
         * through MeshManager singleton class, which performs additional bookkeeping on them.
         *
         * \copydoc doxygen_hide_mesh_constructor_1
         *
         * \copydoc doxygen_hide_mesh_constructor_2
         *
         * \copydoc doxygen_hide_mesh_constructor_5
         *
         * \copydoc doxygen_hide_mesh_optional_prepartitioned_data_arg
         */
        explicit Mesh(
            MeshNS::unique_id unique_id,
            const FilesystemNS::File& mesh_file,
            ::MoReFEM::GeometryNS::dimension_type dimension,
            MeshNS::Format format,
            const ::MoReFEM::CoordsNS::space_unit_type space_unit,
            std::optional<std::reference_wrapper<Wrappers::Lua::OptionFile>> prepartitioned_data = std::nullopt,
            BuildEdge do_build_edge = BuildEdge::no,
            BuildFace do_build_face = BuildFace::no,
            BuildVolume do_build_volume = BuildVolume::no,
            BuildPseudoNormals do_build_pseudo_normals = BuildPseudoNormals::no);


        /*!
         * \brief Construct a Mesh given the list of \a Coords, \a GeometricElt and \a MeshLabel.
         *
         * The constructor is private on purpose: Mesh objects are intended to be created
         * through MeshManager singleton class, which performs additional bookkeeping on them.
         *
         * \copydoc doxygen_hide_mesh_constructor_1
         *
         * \copydoc doxygen_hide_mesh_constructor_2
         *
         * \copydoc doxygen_hide_mesh_constructor_3
         */
        explicit Mesh(MeshNS::unique_id unique_id,
                      ::MoReFEM::GeometryNS::dimension_type dimension,
                      ::MoReFEM::CoordsNS::space_unit_type space_unit,
                      GeometricElt::vector_shared_ptr&& unsort_processor_wise_geom_elt_list,
                      GeometricElt::vector_shared_ptr&& unsort_ghost_geom_elt_list,
                      Coords::vector_shared_ptr&& coords_list,
                      MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                      BuildEdge do_build_edge = BuildEdge::no,
                      BuildFace do_build_face = BuildFace::no,
                      BuildVolume do_build_volume = BuildVolume::no,
                      BuildPseudoNormals do_build_pseudo_normals = BuildPseudoNormals::no);


        /*!
         * \brief Constructor from pre-partitioned data.
         *
         * \copydoc doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_mesh_constructor_5
         *
         * \param[in] unique_id Unique identifier of the \a Mesh to be created.
         * \param[in] prepartitioned_data Lua file which gives the data needed to reconstruct the data
         * from pre-computed partitioned data. Note: it is not const as such objects relies on a Lua stack
         * which is modified for virtually each operation.
         * \param[in,out] dimension Dimension of the mesh read in the input file.
         * \copydoc doxygen_hide_mesh_constructor_2
         * \copydoc doxygen_hide_space_unit_arg
         */
        explicit Mesh(const Wrappers::Mpi& mpi,
                      MeshNS::unique_id unique_id,
                      const FilesystemNS::File& mesh_file,
                      Wrappers::Lua::OptionFile& prepartitioned_data,
                      ::MoReFEM::GeometryNS::dimension_type dimension,
                      MeshNS::Format format,
                      const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                      BuildEdge do_build_edge = BuildEdge::no,
                      BuildFace do_build_face = BuildFace::no,
                      BuildVolume do_build_volume = BuildVolume::no,
                      BuildPseudoNormals do_build_pseudo_normals = BuildPseudoNormals::no);


      public:
        //! Destructor.
        ~Mesh();

        //! \copydoc doxygen_hide_copy_constructor - disabled.
        Mesh(const Mesh& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Mesh(Mesh&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Mesh& operator=(const Mesh& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Mesh& operator=(Mesh&& rhs) = delete;

        ///@}


        //! Get the dimension.
        GeometryNS::dimension_type GetDimension() const noexcept;

        //! List of all geometric elements that belong to the same label.
        //! \param[in] label \a MeshLabel used as filter.
        template<RoleOnProcessor RoleOnProcessorT>
        GeometricElt::vector_shared_ptr GeometricEltListInLabel(const MeshLabel::const_shared_ptr& label) const;

        //! Print the number of geometric elements for each label (for debug purposes).
        //! \copydoc doxygen_hide_stream_inout
        template<RoleOnProcessor RoleOnProcessorT>
        void PrintGeometricEltsInLabel(std::ostream& stream) const;

        //! Number of processor-wise \a Coords.
        std::size_t NprocessorWiseCoord() const noexcept;

        /*!
         * \brief Number of \a Coords that are exclusively associated to ghost \a NodeBearer.
         *
         * Should not be useful to develop a model; it's there for two usages only:
         * - Movemesh implementation within the library.
         * - Some integration tests.
         *
         * \return Number of \a Coords that are exclusively associated to ghost \a NodeBearer.
         */
        std::size_t NghostCoord() const noexcept;

        /*!
         * \brief A vector that contains a pointer to a default geometric element for each type encountered in the mesh.
         *
         * For instance the vector would return a pointer to default Triangle3 and Tetraedre4 geometric elements
         * if the mesh is constituted solely of these two types of elements.
         * \copydoc doxygen_hide_tparam_role_on_processor_plus_both
         *
         * \return One RefGeomElt object if GeometricElt of the same type were found.
         */
        template<RoleOnProcessorPlusBoth RoleOnProcessorPlusBothT>
        RefGeomElt::vector_shared_ptr BagOfEltType() const;

        /*!
         * \brief Same as namesake methods except we limit ourselves to a given dimension.
         *
         * \param[in] dimension Only objects of this dimension will be considered.
         * \copydoc doxygen_hide_tparam_role_on_processor_plus_both
         * \return One RefGeomElt object if GeometricElt of the same type were found.
         */
        template<RoleOnProcessorPlusBoth RoleOnProcessorPlusBothT>
        RefGeomElt::vector_shared_ptr BagOfEltType(::MoReFEM::GeometryNS::dimension_type dimension) const;

        /*!
         * \brief Obtains all the geometric elements of a given type.
         *
         * \param[in] geometric_type Type of the geometric element requested.
         *
         * \copydoc doxygen_hide_tparam_role_on_processor
         *
         * \return Pair of iterators that delimits the required subset.
         *
         * \code
         * auto subset = GetSubsetGeometricEltList(geometric_type); // geometric_type is assumed to be properly
         * initialized. for (auto it = subset.first; it != subset.second; ++it)
         * {
         *      ... loop over all elements that share the given type ...
         * }
         *
         * \endcode
         *
         * \internal <b><tt>[internal]</tt></b> The internal structure of the geometric elements storage shouldn't
         * vary much (if at all...) during the program, so you can be fairly assured the iterators won't be invalidated.
         * Nonetheless, do not store them for instance in the class: generate them when they are required.
         * \endinternal
         *
         */
        template<RoleOnProcessor RoleOnProcessorT>
        subset_range GetSubsetGeometricEltList(const RefGeomElt& geometric_type) const;

        /*!
         * \brief Obtains all the geometric elements of a given type belonging to a given label.
         *
         * \param[in] geometric_type Type of the geometric element requested.
         * \param[in] label_index Index of the label considered
         *
         * \return Pair of iterators that delimits the required subset.
         *
         * See namesake method to see how it should be used.
         */
        template<RoleOnProcessor RoleOnProcessorT>
        subset_range GetSubsetGeometricEltList(const RefGeomElt& geometric_type,
                                               MeshLabelNS::index_type label_index) const;

        /*!
         * \brief Return the list of geometric elements of the requested \a dimension.
         *
         * This method does not allocate new dynamic array.
         * \copydoc doxygen_hide_tparam_role_on_processor
         *
         * \param[in] dimension Dimension used as filter.
         *
         * \return Pair of iterators that delimit all the appropriate  \a GeometricElt.
         */
        template<RoleOnProcessor RoleOnProcessorT>
        subset_range GetSubsetGeometricEltList(::MoReFEM::GeometryNS::dimension_type dimension) const;

#ifndef NDEBUG
        /*!
         * \brief Check the underlying ordering is what is expected within #Mesh.
         *
         * This is obviously a debug method...
         */
        void CheckGeometricEltOrdering() const;

#endif // NDEBUG

        //! Get the number of processor-wise vertex.
        //! \copydoc doxygen_hide_tparam_role_on_processor
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t Nvertex() const;

        //! Get the number of program-wise vertex.
        std::size_t NprogramWiseVertex() const noexcept;

        /*!
         * \class doxygen_hide_mesh_get_geom_elt_from_index_1
         *
         * \brief Return the \a GeometricElt which index (given by GetIndex()) is \a index.
         *
         * \note This method is frankly not efficient at all; it is introduced solely for some PostProcessing need.
         *
         * \param[in] index Index which associated \a GeometricElt is sought.
         *
         * \return \a GeometricElt which index (given by GetIndex()) is \a index.
         */

        //! \copydoc doxygen_hide_mesh_get_geom_elt_from_index_1
        template<RoleOnProcessor RoleOnProcessorT>
        const GeometricElt& GetGeometricEltFromIndex(::MoReFEM::GeomEltNS::index_type index) const;

        /*!
         * \class doxygen_hide_mesh_get_geom_elt_from_index_2
         *
         * \brief Return the \a GeometricElt which index (given by GetIndex()) is \a index and which type is a
         * \a ref_geom_elt.
         *
         * \note This method is slightly better than its counterpart without \a ref_geom_elt but the comment
         * about its lack of efficiency still stands.
         *
         * \param[in] index Index which associated \a GeometricElt is sought.
         * \param[in] ref_geom_elt \a RefGeomElt of the sought \a GeometricElt.
         *
         * \return \a GeometricElt which index (given by GetIndex()) is \a index.
         */

        //! \copydoc doxygen_hide_mesh_get_geom_elt_from_index_2
        template<RoleOnProcessor RoleOnProcessorT>
        const GeometricElt& GetGeometricEltFromIndex(::MoReFEM::GeomEltNS::index_type index,
                                                     const RefGeomElt& ref_geom_elt) const;

      public:
        //! Returns the format in which the mesh was originally read. May be 'None' if undisclosed.
        MeshNS::Format GetInitialFormat() const noexcept;

      public:
        /// \name Edge-related methods.
        ///@{


        //! Tells whether the edges have been built.
        bool AreEdgesBuilt() const;

        //! Get the number of edges. The edges must have been built prior to the call.
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t Nedge() const;
        ///@}


      public:
        /// \name Face-related methods.
        ///@{

        //! Tells whether the faces have been built.
        bool AreFacesBuilt() const;

        //! Get the number of faces. The faces must have been built prior to the call.
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t Nface() const;
        ///@}


      public:
        /// \name Volume-related methods.
        ///@{

        //! Tells whether the volumes have been built.
        bool AreVolumesBuilt() const;

        //! Get the number of volumes. The volumes must have been built prior to the call.
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t Nvolume() const;
        ///@}


        //! Number of geometric elements.
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t NgeometricElt() const;

        //! Number of geometric elements of a given dimension.
        //! \copydoc doxygen_hide_tparam_role_on_processor
        //! \param[in] dimension Dimension used as filter.
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t NgeometricElt(::MoReFEM::GeometryNS::dimension_type dimension) const;

        //! Number of geometric elements of a given type
        //! \copydoc doxygen_hide_tparam_role_on_processor
        //! \param[in] ref_geom_elt Type of the \a RefGeomElt.
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t NgeometricElt(const RefGeomElt& ref_geom_elt) const;

        //! Number of geometric elements of a given type within a given label.
        //! \copydoc doxygen_hide_tparam_role_on_processor
        //! \param[in] mesh_label \a MeshLabel for which the tallying is done.
        //! \param[in] ref_geom_elt Type of the \a RefGeomElt.
        template<RoleOnProcessor RoleOnProcessorT>
        std::size_t NgeometricElt(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label) const;

        //! Get the \a index -th coord on the processor.
        //! \param[in] index Position of the sought \a Coords in the processor-wise \a Coords list. This index is
        //! the output of Coords::GetPositionInCoordsListInMesh(), NOT Coords::GetIndex() which might not be contiguous.
        const Coords& GetCoord(std::size_t index) const;

        /*!
         * \brief Return the list of processor-wise geometric elements.
         */
        template<RoleOnProcessor RoleOnProcessorT>
        const GeometricElt::vector_shared_ptr& GetGeometricEltList() const noexcept;

        /*!
         * \class doxygen_hide_processor_wise_and_ghost_coords
         *
         * What is the meaning of  "processor_wise" and "ghost" for \a Coords?
         *
         * For a \a Dof, the "ghost" concept (relevant only for parallel runs) is clear cut:
         * - If a \a Dof is handled primarily by the current processor, we call it "processor_wise".
         * - If it is primarily handled by another processor BUT its value is nonetheless required on the current
         * processor, we call it a "ghost". PETSc gets a whole mechanism to update these values (handled mostly
         * automatically in \a GlobalVector in MoReFEM).
         * - If a \a Dof is not relevant in any way on the current processor.... it is simply not there at all.
         *
         * Regarding \a Coords, the definition as such isn't so clear, but is nonetheless useful:
         * - A "processor_wise" \a Coords is a \a Coords directly related to a \a NodeBearer which bears a
         * processor-wise \a Dof.
         * - A "ghost" \a Coords is.... every odd \a Coords we need on the processor somehow but aren't related to a
         * processor-wise \a Dof. For instance they might be: . A \a Coords related to a ghost \a Dof. . A \a Coords
         * that forms a \a GeometricElt which is used in a \a DirichletBoundaryCondition (by convention all the \a
         * Coords related to a boundary condition are kept on all processors - we would need this to be able to
         * reconstruct properly the pattern of the matrices.
         */

        /*!
         * \brief Get the list of all \a Coords on the processor.
         *
         * \copydoc doxygen_hide_processor_wise_and_ghost_coords
         */
        const Coords::vector_shared_ptr& GetProcessorWiseCoordsList() const noexcept;


        /*!
         * \brief Get the list of the ghost \a Coords.
         *
         * \copydoc doxygen_hide_processor_wise_and_ghost_coords
         *
         * So to put in a nutshell: unless you are a core library develop that needs to tinker with library aspects, you
         * probably don't need this method!
         *
         * \return List of \a Coords.
         */
        const Coords::vector_shared_ptr& GetGhostCoordsList() const noexcept;

        /*!
         * \brief Get the list of the ghost \a Coords.
         *
         * \copydoc doxygen_hide_processor_wise_and_ghost_coords
         *
         * So to put in a nutshell: unless you are a core library develop that needs to tinker with library aspects, you
         * probably don't need this method! (even more so than for \a GetGhostCoordsList()).
         *
         * \return List of \a Coords.
         */
        Coords::vector_shared_ptr& GetNonCstGhostCoordsList() noexcept;

        //! Get the list of labels.
        const MeshLabel::vector_const_shared_ptr& GetLabelList() const noexcept;

        //! Get the space unit.
        CoordsNS::space_unit_type GetSpaceUnit() const noexcept;

        /*!
         * \brief Write the mesh into a file in the given format.
         *
         * Several notes and caveat:
         * - Only processor-wise data are written on disk; i.e. there is one mesh per processor.
         * - So you need to use mpi rank in the mesh file name, to avoid overwriting the work of another processor.
         * - Some elements of the original mesh (before data reduction) might be 'lost': for instance if there are
         * no boundary condition on an edge and therefore no specific finite element treatment, it is dropped in the
         * reduction.
         * - The operation will fail if you write in a different format than the initial one and one of the original
         * geometric element is not supported by chosen output format.
         *
         * \param[in] mesh_file Path to the file into which the mesh will be written.
         * \tparam FormatT Format of the file to be written. Read the documentation above about the possible
         * discrepancies of formats.
         */
        template<::MoReFEM::MeshNS::Format FormatT>
        void Write(const FilesystemNS::File& mesh_file) const;

      private:
        /*!
         * \brief Compute the relevant pseudo-normals on the mesh.
         *
         * \param[in] label_list_index List of the label on which compute pseudo normals.
         * \param[in,out] vertex_interface_list List of \a Vertex.
         * \param[in,out] edge_interface_list List of \a Edge.
         * \param[in,out] face_interface_list List of \a Face. Eadh
         * \warning Should not be called directly. To compute pseudo-normals use PseudoNormals1 in Lua that will use
         * the dedicated magager to compute them.
         *
         * \attention Interface might suggest nothing is modified at all by this method call.
         * That is absolutely not the case: `Mesh` object and the list of interfaces are indeed left unchanged,
         * but many interface elements are modified as pseudo-normal has been computed for them.
         */
        void ComputePseudoNormals(const std::vector<MeshLabelNS::index_type>& label_list_index,
                                  const Vertex::InterfaceMap& vertex_interface_list,
                                  const Edge::InterfaceMap& edge_interface_list,
                                  const Face::InterfaceMap& face_interface_list) const;

      private:
        /*!
         * \class doxygen_hide_reduced_coords_list_param
         *
         * \param[in] processor_wise_coords_list List of processor-wise coords list, devised from the processor-wise
         * list of \a NodeBearer.
         * \param[in] ghost_coords_list List of ghost coords list, i.e. \a Coords that are only involved
         * with ghost \a NodeBearer. If a \a Coords is related to both a processor-wise and a ghost \a NodeBearer,
         * it won't appear in this list (put differently, intersection of processor_wise_coords_list and
         * ghost_coords_list is the empty set).
         *
         * The internal index and the lowest processor for each \a Coords aren't yet computed; they will be only after
         * the call to \a SetReducedCoordsList.
         */

        /*!
         * \brief Init a local mesh from a global one.
         *
         * \copydoc doxygen_hide_mpi_param
         * \param[in] processor_wise_geom_elt_list List of all geometric elements that are handled by the local
         * processor. This list must be a subset of the current list of geometric elements. \param[in]
         * ghost_geom_elt_list List of ghost \a GeometricElt. \copydoc doxygen_hide_reduced_coords_list_param
         */
        void ShrinkToProcessorWise(const Wrappers::Mpi& mpi,
                                   const GeometricElt::vector_shared_ptr& processor_wise_geom_elt_list,
                                   const GeometricElt::vector_shared_ptr& ghost_geom_elt_list,
                                   Coords::vector_shared_ptr&& processor_wise_coords_list,
                                   Coords::vector_shared_ptr&& ghost_coords_list);

        /*!
         * \brief Set the new coords after reduction to processor-wise.
         *
         * This method is supposed to be called only in ShrinkToProcessorWise().
         *
         * \copydoc doxygen_hide_mpi_param
         * \copydoc doxygen_hide_reduced_coords_list_param
         */
        void SetReducedCoordsList(const Wrappers::Mpi& mpi,
                                  Coords::vector_shared_ptr&& processor_wise_coords_list,
                                  Coords::vector_shared_ptr&& ghost_coords_list);


        /*!
         * \brief For all processor-wise \a Coords, determine which is the lowest rank in which they appear (not
         * counting ghosts).
         *
         *
         * The point is to be able to count without duplicating the number of \a Coords inside a given \a Domain.
         *
         * \copydoc doxygen_hide_mpi_param
         * \param[in] Nprogram_wise_coords Number of program-wise coords. This is required for internal Mpi
         * computations, \param[in,out] a_processor_wise_coords_list The processor-wise list of \a Coords. The list
         * itself is not modified, but the point of the method is to set properly the internal field \a
         * is_lowest_processor_, hence the 'out' status.
         *
         * \internal This is an internal method that is meant to be called by SetReducedCoordsList() only.
         */
        static void SetIsLowestProcessor(const Wrappers::Mpi& mpi,
                                         std::size_t Nprogram_wise_coords,
                                         const Coords::vector_shared_ptr& a_processor_wise_coords_list);


      private:
        /*!
         * \brief Constructor helper method.
         *
         * This method should be called by a any constructor, as it builds and sorts the expected content of the class.
         *
         * \copydoc doxygen_hide_mesh_constructor_3
         * \copydoc doxygen_hide_mesh_constructor_2
         */
        void Construct(GeometricElt::vector_shared_ptr&& unsort_processor_wise_geom_elt_list,
                       GeometricElt::vector_shared_ptr&& unsort_ghost_geom_elt_list,
                       Coords::vector_shared_ptr&& coords_list,
                       MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                       BuildEdge do_build_edge,
                       BuildFace do_build_face,
                       BuildVolume do_build_volume,
                       BuildPseudoNormals do_build_pseudo_normals);


        /*!
         * \brief Read a mesh from a file.
         *
         * \param[in] mesh_file File from which the data will be loaded.
         * \param[in] format Format of the input file.

         * \copydoc doxygen_hide_space_unit_arg
         * \copydoc doxygen_hide_mesh_constructor_3
         * \copydoc doxygen_hide_mesh_optional_prepartitioned_data_arg
         *
         * (for instance Ensight will always yield 3 here by construct).
         */
        void Read(const FilesystemNS::File& mesh_file,
                  ::MoReFEM::MeshNS::Format format,
                  const ::MoReFEM::CoordsNS::space_unit_type space_unit,
                  std::optional<std::reference_wrapper<Wrappers::Lua::OptionFile>> prepartitioned_data,
                  GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                  GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list,
                  Coords::vector_shared_ptr& coords_list,
                  MeshLabel::vector_const_shared_ptr& mesh_label_list);


        //! \name Helper methods for edges and faces.
        //@{

        /*!
         * \brief Actual implementation of \a BuildEdgeList or \a BuildFaceList.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         *
         * \internal <b><tt>[internal]</tt></b> A specialization for Vertex is declared in namespace scope at the end of
         * this file.
         * \endinternal
         *
         * This method builds both processor-wise and ghost interface.
         *
         * \param[in,out] interface_list List of \a InterfaceT under construct.
         */
        template<class InterfaceT>
        void BuildInterface(typename InterfaceT::InterfaceMap& interface_list);


        /*!
         * \brief Helper of \a BuildInterface.
         *
         * \copydoc doxygen_hide_tparam_role_on_processor
         *
         * This method builds the interface either for processor-wise or for ghost.
         *
         * \param[in,out] interface_list List of \a InterfaceT under construct.
         */
        template<class InterfaceT, RoleOnProcessor RoleOnProcessorT>
        void BuildInterfaceHelper(typename InterfaceT::InterfaceMap& interface_list);


        /*!
         * \brief Set the number of interfaces of \a InterfaceT type.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         * \param[in] Nprocessor_wise Number of processsor-wise edges or faces fed to the object .
         * \param[in] Nghost Number of ghost edges or faces fed to the object .
         */
        template<class InterfaceT>
        void SetNinterface(std::size_t Nprocessor_wise, std::size_t Nghost);


        /*!
         * \brief Determine the number of interfaces of \a InterfaceT type.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         *
         * \return Number of interfaces of type \a InterfaceT; first index is for processor-wise count, second one for
         * ghosts.
         */
        template<class InterfaceT>
        std::array<std::size_t, 2UL> DetermineNInterface() const;


        //@}


        /*!
         * \brief Reconstruct the labels after reduction to processor-wise data.
         *
         * Only labels involved with \a GeometricElt kept on the local processors are kept.
         */
        void ReduceLabelList();

      private:
        //! Non constant access to the list of all \a Coords on the processor.
        Coords::vector_shared_ptr& GetNonCstProcessorWiseCoordsList() noexcept;

        //! Mutator the list of the \a Coords related to processor-wise \a NodeBearer.
        //! \param[in] list The list of ghost \a Coords computed outside the \a Mesh.
        void SetProcessorWiseCoordsList(Coords::vector_shared_ptr&& list);


        //! Mutator the list of the \a Coords related ONLY to ghost \a NodeBearer.
        //! \param[in] list The list of ghost \a Coords computed outside the \a Mesh.
        void SetGhostCoordsList(Coords::vector_shared_ptr&& list);

        //! Non constant access to the list of labels.
        MeshLabel::vector_const_shared_ptr& GetNonCstLabelList() noexcept;

        /*!
         * \brief For constructor for prepartitioned data: split the \a processor_wise_coords_list_ in processor-wise
         * and ghost \a Coords.
         *
         * In this constructor, we want to rebuild the model from data that were partitioned beforehand. So we read a
         * mesh file which is a subset of the full mesh submitted initially to the partition; when it is read all \a
         * Coords are tagged as processor-wise. That is not actually the reality: some of them are in fact mainly born
         * by another processor and it is a ghost for current rank. The role of this method is to sort this and split
         * between \a processor_wise_coords_list_ and \a ghost_coords_list_; it's rather trivial as by construct the \a
         * Nprocessor_wise first elements are processor-wise.
         *
         * \param[in] Nprocessor_wise Number of \a Coords to keep in processor-wise containers.
         *
         * \internal Should not be used outside of the constructor for prepartitioned data.
         *
         */
        void SortPrepartitionedCoords(std::size_t Nprocessor_wise);


      public:
        /*!
         * \brief Compute a list of \a GeometricElt which covers both the processor-wise and ghost.
         *
         * \attention This operation is costly (a new container is allocated) and is intended to be used only when
         * writing a mesh into a file and in preparation of prepartifioned data.
         *
         * \return The list of all \a GeometricElt of interest for the current processor.
         */
        GeometricElt::vector_shared_ptr ComputeProcessorWiseAndGhostGeometricEltList() const;


      private:
        /*!
         * \brief Internal method that dispatch the correct \a GeometricElt list depending on \a RoleOnProcessorT value.
         *
         * \copydoc doxygen_hide_tparam_role_on_processor
         */
        template<RoleOnProcessor RoleOnProcessorT>
        const Internal::MeshNS::GeometricEltList& GetInternalGeomEltList() const noexcept;


      private:
        //! List of all \a Coords on the processor.
        //! \copydoc doxygen_hide_processor_wise_and_ghost_coords
        Coords::vector_shared_ptr processor_wise_coords_list_;

        /*!
         * \brief List of ghost \a Coords.
         *
         * \copydoc doxygen_hide_processor_wise_and_ghost_coords
         */
        Coords::vector_shared_ptr ghost_coords_list_;

        /*!
         * \brief List of all the geometric elements of the mesh, sort by dimension.
         *
         * Key is the dimension, and value is the list of all geometric elements of that dimension in the mesh.
         *
         * Underlying order is expected to be by geometric element nature (all triangles3, then all triangles6, etc...).
         */
        Internal::MeshNS::GeometricEltList processor_wise_geometric_elt_list_;


        /*!
         * \brief List of all ghost \a GeometricElt.
         *
         * These \a GeometricElt aren't directly handled by the current rank, but they are related for few \a
         * LocalFEltSpace that gets some \a NodeBearer that are managed by the current rank, We shouldn't have to use
         * them except for writing data on disk; the point of this list is mostly to keep track of them.
         */
        Internal::MeshNS::GeometricEltList ghost_geometric_elt_list_;


        //! Dimension of the mesh.
        const ::MoReFEM::GeometryNS::dimension_type dimension_;

        //! All processor-wise labels.
        MeshLabel::vector_const_shared_ptr label_list_;

        /*!
         * \brief Number of built vertex (processor-wise).
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         *
         * First index is the processor-wise count, second the ghost one.
         */
        std::array<std::size_t, 2UL> Nvertex_on_rank_ = Utilities::FilledWithUninitializedIndex<std::size_t, 2UL>();

        /*!
         * \brief Number of built vertex (program-wise).
         */
        std::size_t Nprogram_wise_vertex_ = NumericNS::UninitializedIndex<std::size_t>();

        /*!
         * \brief Number of built edges that are related to a \a NodeBearer.
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         *
         * If BuildEdgeList() has not been called, this quantity is an UninitializedIndex; fetching this quantity
         * should lead to an assert.
         *
         * First index is the processor-wise count, second the ghost one.
         */
        std::array<std::size_t, 2UL> Nedge_ = Utilities::FilledWithUninitializedIndex<std::size_t, 2UL>();

        /*!
         * \brief Number of built faces.
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         *
         * If BuildFaceList() has not been called, this quantity is an UninitializedIndex; fetching this quantity
         * should lead to an assert.
         * First index is the processor-wise count, second the ghost one.
         */
        std::array<std::size_t, 2UL> Nface_ = Utilities::FilledWithUninitializedIndex<std::size_t, 2UL>();

        /*!
         * \brief Number of built volumes.
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         *
         * If BuildFaceList() has not been called, this quantity is an UninitializedIndex; fetching this quantity
         * should lead to an assert.
         *
         * First index is the processor-wise count, second the ghost one.
         */
        std::array<std::size_t, 2UL> Nvolume_ = Utilities::FilledWithUninitializedIndex<std::size_t, 2UL>();

        //! Space unit of the mesh.
        const ::MoReFEM::CoordsNS::space_unit_type space_unit_;

        //! Format in which the mesh was first read. May be 'None' if undisclosed or not kept.
        MeshNS::Format initial_format_ = MeshNS::Format::None;


    }; // class Mesh


    //! Specialization for Volume.
    template<>
    std::array<std::size_t, 2UL> Mesh::DetermineNInterface<Volume>() const;


    /*!
     * \brief Write in the given output directory a file in which all the interfaces are written.
     *
     * This function should be called only on root processor.
     *
     * \param[in] mesh Key is the unique id of the \a Mesh which interface list is to be written; value
     * the path of its associated output directory (which should have already been created).
     * \param[in] filename Name if the file to be written in output directory; default value is what is expected by
     * facilities such as tests or post-processing. (the functionality to modify it has been introduced mostly for dev
     * purposes; tests and post-processing expect the default value).
     *
     * A 'interfaces.hhdata' file will be written inside this output directory.
     */
    void WriteInterfaceList(
        const std::pair<const ::MoReFEM::MeshNS::unique_id, FilesystemNS::Directory::const_unique_ptr>& mesh,
        std::string_view filename = "interfaces.hhdata");


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * The comparison is performed using the underlying unique id.
     */
    bool operator==(const Mesh& lhs, const Mesh& rhs);


    /*!
     * \brief Convert into the \a MeshNS::unique_id strong type the enum value defined in the input data file.
     *
     * In a typical Model, indexes related to fields (e.g. Mesh) are defined in an enum class named
     * typically \a MeshIndex. This facility is a shortcut to convert this enum into a proper \a
     * MeshNS::unique_id that is used in the library to identify a \a Mesh.
     *
     * \param[in] enum_value The index related to the \a Mesh, stored in an enum class. See a typical 'InputData.hpp' file
     * to understand it better.
     *
     * \return The identifier for a \a Mesh as used in the library.
     */
    template<class EnumT>
    constexpr MeshNS::unique_id AsMeshId(EnumT enum_value);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Mesh/Mesh.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_MESH_DOT_HPP_
// *** MoReFEM end header guards *** < //
