// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_MESH_DOT_HXX_
#define MOREFEM_GEOMETRY_MESH_MESH_DOT_HXX_
// IWYU pragma: private, include "Geometry/Mesh/Mesh.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iterator>
#include <memory>
#include <ostream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Containers/Array.hpp"
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: keep
#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Sort.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/SortingCriterion.hpp"
#include "Geometry/Interfaces/Internal/Orientation/Traits.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    inline auto Mesh::GetDimension() const noexcept -> GeometryNS::dimension_type
    {
        return dimension_;
    }


    inline std::size_t Mesh::NprocessorWiseCoord() const noexcept
    {
        return processor_wise_coords_list_.size();
    }


    inline std::size_t Mesh::NghostCoord() const noexcept
    {
        return ghost_coords_list_.size();
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline const GeometricElt::vector_shared_ptr& Mesh::GetGeometricEltList() const noexcept
    {
        return this->template GetInternalGeomEltList<RoleOnProcessorT>().All();
    }


    template<RoleOnProcessorPlusBoth RoleOnProcessorPlusBothT>
    RefGeomElt::vector_shared_ptr Mesh::BagOfEltType() const
    {
        switch (RoleOnProcessorPlusBothT)
        {
        case RoleOnProcessorPlusBoth::processor_wise:
            return GetInternalGeomEltList<RoleOnProcessor::processor_wise>().BagOfEltType();
        case RoleOnProcessorPlusBoth::ghost:
            return GetInternalGeomEltList<RoleOnProcessor::ghost>().BagOfEltType();
        case RoleOnProcessorPlusBoth::both:
        {
            auto ret = GetInternalGeomEltList<RoleOnProcessor::processor_wise>().BagOfEltType();
            auto ghost_list = GetInternalGeomEltList<RoleOnProcessor::ghost>().BagOfEltType();

            std::copy(ghost_list.cbegin(), ghost_list.cend(), std::back_inserter(ret));

            Utilities::EliminateDuplicate(ret,
                                          Utilities::PointerComparison::Less<RefGeomElt::shared_ptr>(),
                                          Utilities::PointerComparison::Equal<RefGeomElt::shared_ptr>());

            return ret;
        }
        }
    }


    template<RoleOnProcessorPlusBoth RoleOnProcessorPlusBothT>
    RefGeomElt::vector_shared_ptr Mesh::BagOfEltType(::MoReFEM::GeometryNS::dimension_type dimension) const
    {
        switch (RoleOnProcessorPlusBothT)
        {
        case RoleOnProcessorPlusBoth::processor_wise:
            return GetInternalGeomEltList<RoleOnProcessor::processor_wise>().BagOfEltType(dimension);
        case RoleOnProcessorPlusBoth::ghost:
            return GetInternalGeomEltList<RoleOnProcessor::ghost>().BagOfEltType(dimension);
        case RoleOnProcessorPlusBoth::both:
        {
            auto ret = GetInternalGeomEltList<RoleOnProcessor::processor_wise>().BagOfEltType(dimension);
            auto ghost_list = GetInternalGeomEltList<RoleOnProcessor::ghost>().BagOfEltType(dimension);

            std::copy(ghost_list.cbegin(), ghost_list.cend(), std::back_inserter(ret));

            Utilities::EliminateDuplicate(ret,
                                          Utilities::PointerComparison::Less<RefGeomElt::shared_ptr>(),
                                          Utilities::PointerComparison::Equal<RefGeomElt::shared_ptr>());

            return ret;
        }
        }
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline GeometricElt::vector_shared_ptr
    Mesh ::GeometricEltListInLabel(const MeshLabel::const_shared_ptr& label) const
    {
        return GetInternalGeomEltList<RoleOnProcessorT>().GeometricEltListInLabel(label);
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::size_t Mesh::NgeometricElt() const
    {
        return GetInternalGeomEltList<RoleOnProcessorT>().NgeometricElt();
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::size_t Mesh::NgeometricElt(::MoReFEM::GeometryNS::dimension_type dimension) const
    {
        return GetInternalGeomEltList<RoleOnProcessorT>().NgeometricElt(dimension);
    }


    inline bool Mesh::AreEdgesBuilt() const
    {
        return (Nedge_[0] != NumericNS::UninitializedIndex<std::size_t>());
    }


    inline bool Mesh::AreFacesBuilt() const
    {
        return (Nface_[0] != NumericNS::UninitializedIndex<std::size_t>());
    }


    inline bool Mesh::AreVolumesBuilt() const
    {
        return (Nvolume_[0] != NumericNS::UninitializedIndex<std::size_t>());
    }


    inline const Coords& Mesh::GetCoord(std::size_t index) const
    {
        assert(index < processor_wise_coords_list_.size()
               && "Make sure the index is a result of Coords::GetPositionInCoordsListInMesh() and not "
                  "Coords::GetIndex().");
        assert(!(!processor_wise_coords_list_[index]));
        return *processor_wise_coords_list_[index];
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::size_t Mesh::Nvertex() const
    {
        assert("The vertices must have been built before their number is requested!"
               && Nvertex_on_rank_[0] != NumericNS::UninitializedIndex<std::size_t>());

        constexpr auto index = static_cast<std::size_t>(RoleOnProcessorT);
        static_assert(index < Utilities::ArraySize<decltype(Nvertex_on_rank_)>::GetValue());

        return Nvertex_on_rank_[index];
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::size_t Mesh::Nedge() const
    {
        assert("The edges must have been built before their number is requested!"
               && Nedge_[0] != NumericNS::UninitializedIndex<std::size_t>());

        constexpr auto index = static_cast<std::size_t>(RoleOnProcessorT);
        static_assert(index < Utilities::ArraySize<decltype(Nedge_)>::GetValue());

        return Nedge_[index];
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::size_t Mesh::Nface() const
    {
        assert("The faces must have been built before their number is requested!"
               && Nface_[0] != NumericNS::UninitializedIndex<std::size_t>());

        constexpr auto index = static_cast<std::size_t>(RoleOnProcessorT);
        static_assert(index < Utilities::ArraySize<decltype(Nface_)>::GetValue());

        return Nface_[index];
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::size_t Mesh::Nvolume() const
    {
        assert("The volume must have been built before their number is requested!"
               && Nvolume_[0] != NumericNS::UninitializedIndex<std::size_t>());

        constexpr auto index = static_cast<std::size_t>(RoleOnProcessorT);
        static_assert(index < Utilities::ArraySize<decltype(Nvolume_)>::GetValue());

        return Nvolume_[index];
    }


    inline auto Mesh::GetSpaceUnit() const noexcept -> CoordsNS::space_unit_type
    {
        return space_unit_;
    }


    template<>
    inline void Mesh::SetNinterface<Vertex>(std::size_t Nprocessor_wise, std::size_t Nghost)
    {
        Nvertex_on_rank_[0] = Nprocessor_wise;
        Nvertex_on_rank_[1] = Nghost;
    }


    template<>
    inline void Mesh::SetNinterface<Edge>(std::size_t Nprocessor_wise, std::size_t Nghost)
    {
        Nedge_[0] = Nprocessor_wise;
        Nedge_[1] = Nghost;
    }


    template<>
    inline void Mesh::SetNinterface<Face>(std::size_t Nprocessor_wise, std::size_t Nghost)
    {
        Nface_[0] = Nprocessor_wise;
        Nface_[1] = Nghost;
    }


    template<>
    inline void Mesh::SetNinterface<Volume>(std::size_t Nprocessor_wise, std::size_t Nghost)
    {
        Nvolume_[0] = Nprocessor_wise;
        Nvolume_[1] = Nghost;
    }


    template<class OrientedType>
    void DetermineNinterfaceHelper(const GeometricElt::vector_shared_ptr geom_elt_list,
                                   typename OrientedType::vector_shared_ptr& interface_list)
    {
        for (const auto& geometric_elt_ptr : geom_elt_list)
        {
            assert(!(!geometric_elt_ptr));

            const auto& geometric_elt_interface_list =
                Internal::InterfaceNS::GetInterfaceOfGeometricElt<OrientedType>(*geometric_elt_ptr);

            for (const auto& interface_ptr : geometric_elt_interface_list)
                interface_list.push_back(interface_ptr);
        }
    }


    template<class InterfaceT>
    std::array<std::size_t, 2UL> Mesh::DetermineNInterface() const
    {
        decltype(auto) processor_wise_geometric_elt_list = GetGeometricEltList<RoleOnProcessor::processor_wise>();
        decltype(auto) ghost_geometric_elt_list = GetGeometricEltList<RoleOnProcessor::ghost>();

        using OrientedType = typename Internal::InterfaceNS::Traits::OrientedInterface<InterfaceT>::type;

        typename OrientedType::vector_shared_ptr processor_wise_interface_list;
        typename OrientedType::vector_shared_ptr ghost_interface_list_helper, ghost_interface_list;

        DetermineNinterfaceHelper<OrientedType>(processor_wise_geometric_elt_list, processor_wise_interface_list);
        DetermineNinterfaceHelper<OrientedType>(ghost_geometric_elt_list, ghost_interface_list_helper);

        // We do want know to reduce the list of interfaces so that one interface appears only once there,
        // regardless of its orientation.
        // Using Utilities::PointerComparison::Less<typename OrientedType::shared_ptr>() as pointer criterion is
        // not enough: operator== for OrientedEdge and OrientedFace also checks for orientation.
        // Using the underlying pointer wouldn't work: Vertex doesn't define this method.
        // That's why the comparison is upon indexes: these indexes are the ones attributes before orientation is
        // considered, and they are present in Vertex, OrientedEdge and OrientedFace objects.
        auto equal_unoriented =
            [](const typename OrientedType::shared_ptr& lhs, const typename OrientedType::shared_ptr& rhs)
        {
            assert(!(!lhs));
            assert(!(!rhs));
            return lhs->GetProgramWiseIndex() == rhs->GetProgramWiseIndex();
        };

        auto less_unoriented =
            [](const typename OrientedType::shared_ptr& lhs, const typename OrientedType::shared_ptr& rhs)
        {
            assert(!(!lhs));
            assert(!(!rhs));
            return lhs->GetProgramWiseIndex() < rhs->GetProgramWiseIndex();
        };


        Utilities::EliminateDuplicate(processor_wise_interface_list, less_unoriented, equal_unoriented);

        Utilities::EliminateDuplicate(ghost_interface_list_helper, less_unoriented, equal_unoriented);

        // Here remove the few ghost that might be also present in processor-wise.
        ghost_interface_list.reserve(ghost_interface_list_helper.size());

        std::set_difference(ghost_interface_list_helper.cbegin(),
                            ghost_interface_list_helper.cend(),
                            processor_wise_interface_list.cbegin(),
                            processor_wise_interface_list.cend(),
                            std::back_inserter(ghost_interface_list),
                            less_unoriented);

        const auto Nprocessor_wise = processor_wise_interface_list.size();

        // We count as ghost only the interfaces that are tied to a ghost \a GeometricElt but aren't yet in the
        // processor_wise interface list.

        const auto Nghost = ghost_interface_list.size();

        std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> proc_index_list, ghost_index_list;
        ghost_index_list.reserve(Nghost);
        proc_index_list.reserve(Nprocessor_wise);

        for (const auto& proc_interface_ptr : processor_wise_interface_list)
        {
            proc_index_list.push_back(proc_interface_ptr->GetProgramWiseIndex());
        }


        for (const auto& ghost_interface_ptr : ghost_interface_list)
        {
            if (std::binary_search(processor_wise_interface_list.cbegin(),
                                   processor_wise_interface_list.cend(),
                                   ghost_interface_ptr,
                                   Utilities::PointerComparison::Less<typename OrientedType::shared_ptr>()))
                continue;

            ghost_index_list.push_back(ghost_interface_ptr->GetProgramWiseIndex());
        }

        assert(proc_index_list.size() == Nprocessor_wise);
        assert(ghost_index_list.size() == Nghost);

        return { { Nprocessor_wise, Nghost } };
    }


    template<class InterfaceT, RoleOnProcessor RoleOnProcessorT>
    void Mesh::BuildInterfaceHelper(typename InterfaceT::InterfaceMap& interface_list)
    {
        decltype(auto) geometric_elt_list = GetGeometricEltList<RoleOnProcessorT>();

        for (const auto& geometric_elt_ptr : geometric_elt_list)
        {
            assert(!(!geometric_elt_ptr));
            geometric_elt_ptr->template BuildInterface<InterfaceT>(geometric_elt_ptr.get(), interface_list);
        }
    }


    template<class InterfaceT>
    void Mesh::BuildInterface(typename InterfaceT::InterfaceMap& interface_list)
    {
        assert(interface_list.empty());

        BuildInterfaceHelper<InterfaceT, RoleOnProcessor::processor_wise>(interface_list);
        const auto Nprocessor_wise = interface_list.size();

        std::vector<::MoReFEM::InterfaceNS::program_wise_index_type> proc_index_list, ghost_index_list,
            ghost_helper_list;

        for (const auto& pair : interface_list)
            proc_index_list.push_back(pair.first->GetProgramWiseIndex());

        Utilities::EliminateDuplicate(proc_index_list);

        BuildInterfaceHelper<InterfaceT, RoleOnProcessor::ghost>(interface_list);

        for (const auto& pair : interface_list)
            ghost_helper_list.push_back(pair.first->GetProgramWiseIndex());

        Utilities::EliminateDuplicate(ghost_helper_list);

        std::set_difference(ghost_helper_list.cbegin(),
                            ghost_helper_list.cend(),
                            proc_index_list.cbegin(),
                            proc_index_list.cend(),
                            std::back_inserter(ghost_index_list));

        assert(interface_list.size() >= Nprocessor_wise);
        const auto Nghost = interface_list.size() - Nprocessor_wise;

        SetNinterface<InterfaceT>(Nprocessor_wise, Nghost);

#ifndef NDEBUG
        {
            auto [Nproc_wise_check, Nghost_check] = DetermineNInterface<InterfaceT>();

            assert(Nprocessor_wise == Nproc_wise_check);
        }
#endif // NDEBUG
    }


    inline const Coords::vector_shared_ptr& Mesh::GetProcessorWiseCoordsList() const noexcept
    {
        return processor_wise_coords_list_;
    }


    inline const Coords::vector_shared_ptr& Mesh::GetGhostCoordsList() const noexcept
    {
        return ghost_coords_list_;
    }


    inline Coords::vector_shared_ptr& Mesh::GetNonCstGhostCoordsList() noexcept
    {
        return const_cast<Coords::vector_shared_ptr&>(GetGhostCoordsList());
    }


    inline const MeshLabel::vector_const_shared_ptr& Mesh::GetLabelList() const noexcept
    {
        assert(
            std::none_of(label_list_.cbegin(), label_list_.cend(), Utilities::IsNullptr<MeshLabel::const_shared_ptr>));

        assert(std::is_sorted(label_list_.cbegin(),
                              label_list_.cend(),
                              Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>()));
        return label_list_;
    }


    inline Coords::vector_shared_ptr& Mesh::GetNonCstProcessorWiseCoordsList() noexcept
    {
        return const_cast<Coords::vector_shared_ptr&>(GetProcessorWiseCoordsList());
    }


    inline MeshLabel::vector_const_shared_ptr& Mesh::GetNonCstLabelList() noexcept
    {
        return const_cast<MeshLabel::vector_const_shared_ptr&>(GetLabelList());
    }


    inline bool operator==(const Mesh& lhs, const Mesh& rhs)
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


    inline std::size_t Mesh::NprogramWiseVertex() const noexcept
    {
        assert(Nprogram_wise_vertex_ != NumericNS::UninitializedIndex<std::size_t>());
        return Nprogram_wise_vertex_;
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline const GeometricElt& Mesh::GetGeometricEltFromIndex(::MoReFEM::GeomEltNS::index_type index) const
    {
        return GetInternalGeomEltList<RoleOnProcessorT>().GetGeometricEltFromIndex(index);
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline const GeometricElt& Mesh::GetGeometricEltFromIndex(::MoReFEM::GeomEltNS::index_type index,
                                                              const RefGeomElt& ref_geom_elt) const
    {
        return GetInternalGeomEltList<RoleOnProcessorT>().GetGeometricEltFromIndex(index, ref_geom_elt);
    }


    inline MeshNS::Format Mesh::GetInitialFormat() const noexcept
    {
        return initial_format_;
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline const Internal::MeshNS::GeometricEltList& Mesh::GetInternalGeomEltList() const noexcept
    {
        if constexpr (RoleOnProcessorT == RoleOnProcessor::processor_wise)
            return processor_wise_geometric_elt_list_;

        if constexpr (RoleOnProcessorT == RoleOnProcessor::ghost)
            return ghost_geometric_elt_list_;

        assert(false && "Should not happen!");
        exit(EXIT_FAILURE);
    }


    template<RoleOnProcessor RoleOnProcessorT>
    void Mesh::PrintGeometricEltsInLabel(std::ostream& stream) const
    {
        const auto& label_list = GetLabelList();

        for (const auto& surf : label_list)
        {
            stream << "Surface " << surf->GetIndex() << " -> " << std::endl;
            auto geo_elements_in_surf = GeometricEltListInLabel<RoleOnProcessorT>(surf);

            {
                namespace sc = Advanced::GeometricEltNS::SortingCriterion;
                std::stable_sort(geo_elements_in_surf.begin(),
                                 geo_elements_in_surf.end(),
                                 Utilities::Sort<GeometricElt::shared_ptr, sc::Type<>>);
            }

            std::size_t Nsame_geo_element = 0UL;
            Advanced::GeometricEltEnum id(Advanced::GeometricEltEnum::Undefined);

            std::string name;

            for (auto geo_element : geo_elements_in_surf)
            {
                if (geo_element->GetIdentifier() == id)
                    ++Nsame_geo_element;
                else
                {
                    if (id != Advanced::GeometricEltEnum::Undefined)
                        stream << '\t' << Nsame_geo_element << ' ' << name << std::endl;

                    name = geo_element->GetName();

                    Nsame_geo_element = 1UL;
                    id = geo_element->GetIdentifier();
                }
            }

            if (Nsame_geo_element)
                stream << '\t' << Nsame_geo_element << ' ' << name << std::endl;
        }
    }


    template<RoleOnProcessor RoleOnProcessorT>
    std::size_t Mesh::NgeometricElt(const RefGeomElt& ref_geom_elt) const
    {
        auto [begin, end] = GetSubsetGeometricEltList<RoleOnProcessorT>(ref_geom_elt);
        assert(end >= begin);
        return static_cast<std::size_t>(end - begin);
    }


    template<RoleOnProcessor RoleOnProcessorT>
    inline std::size_t Mesh::NgeometricElt(const RefGeomElt& ref_geom_elt, const MeshLabel& mesh_label) const
    {
        return GetInternalGeomEltList<RoleOnProcessorT>().NgeometricElt(ref_geom_elt, mesh_label);
    }


    template<RoleOnProcessor RoleOnProcessorT>
    Mesh::subset_range Mesh::GetSubsetGeometricEltList(::MoReFEM::GeometryNS::dimension_type dimension) const
    {
        auto pair = GetInternalGeomEltList<RoleOnProcessorT>().GetLocationInEltList(dimension);
        return Internal::MeshNS::SubsetRange(pair, GetInternalGeomEltList<RoleOnProcessorT>());
    }


    template<RoleOnProcessor RoleOnProcessorT>
    Mesh::subset_range Mesh::GetSubsetGeometricEltList(const RefGeomElt& ref_geom_elt) const
    {
        decltype(auto) internal = GetInternalGeomEltList<RoleOnProcessorT>();
        auto pair = internal.GetLocationInEltList(ref_geom_elt);
        return Internal::MeshNS::SubsetRange(pair, internal);
    }


    template<RoleOnProcessor RoleOnProcessorT>
    Mesh::subset_range Mesh::GetSubsetGeometricEltList(const RefGeomElt& geometric_type,
                                                       MeshLabelNS::index_type label_index) const
    {
        decltype(auto) internal = GetInternalGeomEltList<RoleOnProcessorT>();
        std::pair<std::size_t, std::size_t> position_of_subset;
        if (internal.GetLocationInEltList(geometric_type, label_index, position_of_subset))
            return Internal::MeshNS::SubsetRange(position_of_subset, internal);

        // If none, return a pair of end iterators.
        auto end = internal.All().cend();

        return { end, end };
    }


    template<class EnumT>
    constexpr MeshNS::unique_id AsMeshId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return MeshNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_MESH_DOT_HXX_
// *** MoReFEM end header guards *** < //
