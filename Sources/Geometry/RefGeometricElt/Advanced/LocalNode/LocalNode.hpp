// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_LOCALNODE_LOCALNODE_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_LOCALNODE_LOCALNODE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <vector>

// IWYU pragma: begin_exports
#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/LocalInterface/Advanced/LocalInterface.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/Alias.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::Advanced
{


    /*!
     * \brief A \a LocalNode is mostly an extension of a \a LocalCoords, which additionally is attributed an index and keeps track on the \a LocalInterface
     * on which it is located.
     *
     * So there might be several \a LocalNode on a given \a LocalInterface (there are no local equivalent on \a
     * NodeBearer though).
     *
     */
    class LocalNode final
    {

      public:
        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const LocalNode>;

        //! Alias to vector of shared_pointer.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] local_interface Local interface upon which the LocalNode is built.
         * \param[in] index Incremental index of the local nodes in the BasicRefFElt. All local nodes on vertices
         * are numbered first; then all edges, and so on.
         * \param[in] local_coords Approximate position of the local node.
         */
        LocalNode(Advanced::InterfaceNS::LocalInterface&& local_interface,
                  LocalNodeNS::index_type index,
                  LocalCoords local_coords);

        //! Destructor.
        ~LocalNode() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LocalNode(const LocalNode& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LocalNode(LocalNode&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LocalNode& operator=(const LocalNode& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LocalNode& operator=(LocalNode&& rhs) = delete;

        ///@}


        //! Get the local interface.
        const Advanced::InterfaceNS::LocalInterface& GetLocalInterface() const noexcept;

        //! Get the local index of the dof.
        LocalNodeNS::index_type GetIndex() const noexcept;

        //! Get the local coordinates of the dof.
        const LocalCoords& GetLocalCoords() const noexcept;

        //! Print the information of the local dof.
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;


      private:
        //! Local interface.
        const Advanced::InterfaceNS::LocalInterface local_interface_;

        //! Local index.
        const LocalNodeNS::index_type index_;

        //! Local coordinates.
        LocalCoords local_coords_;
    };


    //! \copydoc doxygen_hide_operator_less
    //! The criterion used is the index as returned by \a LocalNode::GetIndex().
    bool operator<(const LocalNode& lhs, const LocalNode& rhs);


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_LOCALNODE_LOCALNODE_DOT_HPP_
// *** MoReFEM end header guards *** < //
