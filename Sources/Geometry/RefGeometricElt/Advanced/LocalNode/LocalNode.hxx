// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_LOCALNODE_LOCALNODE_DOT_HXX_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_LOCALNODE_LOCALNODE_DOT_HXX_
// IWYU pragma: private, include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Type/StrongType/Skills/Comparable.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Advanced/LocalNode/Alias.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }
namespace MoReFEM::Advanced { class LocalNode; }
namespace MoReFEM::Advanced::InterfaceNS { class LocalInterface; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    inline const Advanced::InterfaceNS::LocalInterface& LocalNode::GetLocalInterface() const noexcept
    {
        return local_interface_;
    }


    inline LocalNodeNS::index_type LocalNode::GetIndex() const noexcept
    {
        return index_;
    }


    inline const LocalCoords& LocalNode::GetLocalCoords() const noexcept
    {
        return local_coords_;
    }


    inline bool operator<(const LocalNode& lhs, const LocalNode& rhs)
    {
        return lhs.GetIndex() < rhs.GetIndex();
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_LOCALNODE_LOCALNODE_DOT_HXX_
// *** MoReFEM end header guards *** < //
