// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_TOPOLOGY_CONCEPT_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_TOPOLOGY_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Miscellaneous.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::Concept::TopologyNS
{


    /*!
     * \brief Defines a concept to identify topology class
     *
     * Such as MoReFEM::Advanced::RefGeomEltNS::TopologyNS::Triangle class.
     *
     * \internal I didn't introduce a specific variable to check exactly this is a topology class, but chose
     * to use instead several traits that are expected in such a class.
     */
    template<typename T>
    concept TraitsClass = requires {
        typename T::EdgeTopology;
        typename T::EdgeContent;
        typename T::FaceTopology;
        typename T::FaceContent;
    };


    /*!
     * \brief Defines a concept to identify topologies with well-define edge.
     *
     */
    template<typename T>
    concept with_edge = TraitsClass<T> && !std::is_same<typename T::EdgeTopology, std::false_type>();


    /*!
     * \brief Defines a concept to identify topologies with well-define face.
     *
     */
    template<typename T>
    concept with_face = TraitsClass<T> && !std::is_same<typename T::FaceTopology, std::false_type>();


    /*!
     * \brief Helper type used in defining classes \a OrientedEdge and \a OrientedFace
     *
     * Both this classes are non-template classes... but needs in the constructor data from a template
     * holding topology data (which must respect the concept \a Concept::TopologyNS::TraitsClass.
     *
     * A common trick to do so is to use an indirection: an additional argument is passed that
     * gives the information indirectly. This way, only the constructor is templated, not the whole
     * class (see \a Utilities::Type2Type for a much more detailed explanation).
     *
     * Concept defined here ensure the token passed to \a OrientedEdge and \a OrientedFace constructor
     * is valid (this check is performed at compile time).
     *
     * \tparam Type2TypeT A Type2Type class which template parameter is a class which satisfies
     * the \a Concept::TopologyNS::TraitsClass. For instance
     * Utilities::Type2Type<Advanced::RefGeomEltNS::TopologyNS::Triangle>.
     */
    template<typename Type2TypeT>
    concept IndexedSectionDescriptionType = requires {
        { Concept::TopologyNS::TraitsClass<typename Type2TypeT::type> };
        { Utilities::IsSpecializationOf<Utilities::Type2Type, Type2TypeT>() };
    };


} // namespace MoReFEM::Advanced::Concept::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_TOPOLOGY_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
