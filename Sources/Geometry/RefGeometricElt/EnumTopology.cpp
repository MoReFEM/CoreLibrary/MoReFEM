// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <iostream>

#include "Geometry/RefGeometricElt/EnumTopology.hpp"


namespace MoReFEM::TopologyNS
{


    std::ostream& operator<<(std::ostream& stream, const Type rhs)
    {
        using type = MoReFEM::TopologyNS::Type;

        switch (rhs)
        {
        case type::point:
            stream << "point";
            break;
        case type::segment:
            stream << "segment";
            break;
        case type::triangle:
            stream << "triangle";
            break;
        case type::tetrahedron:
            stream << "tetrahedron";
            break;
        case type::quadrangle:
            stream << "quadrangle";
            break;
        case type::hexahedron:
            stream << "hexahedron";
            break;

        } // switch

        return stream;
    }


} // namespace MoReFEM::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
