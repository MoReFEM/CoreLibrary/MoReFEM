// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>
#include <cassert>
#include <cstdlib>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
{


    namespace // anonymous
    {

        std::pair<::MoReFEM::GeometryNS::dimension_type, double> AnalyseFace(std::size_t face_index);

        LocalCoords Extract2DCoordinates(::MoReFEM::GeometryNS::dimension_type face_constant_component,
                                         const LocalCoords& coords);


    } // namespace


    const std::string& Hexahedron::ClassName()
    {
        static const std::string ret("Hexahedron");
        return ret;
    }


    /************************************************************************
     *
     *         7--------6
     *        /.       /|
     *       / .      / |
     *      4________5  |
     *      |  .     |  |
     *      |  3.....|..2
     *      | .      | /
     *      |.       |/
     *      0________1
     *
     *
     *************************************************************************/


    const std::array<Hexahedron::EdgeContent, Hexahedron::Nedge>& Hexahedron::GetEdgeList()
    {
        static const std::array<Hexahedron::EdgeContent, Hexahedron::Nedge> ret{ { { { 0UL, 1UL } },
                                                                                   { { 1UL, 2UL } },
                                                                                   { { 3UL, 2UL } },
                                                                                   { { 0UL, 3UL } },
                                                                                   { { 0UL, 4UL } },
                                                                                   { { 1UL, 5UL } },
                                                                                   { { 2UL, 6UL } },
                                                                                   { { 3UL, 7UL } },
                                                                                   { { 4UL, 5UL } },
                                                                                   { { 5UL, 6UL } },
                                                                                   { { 7UL, 6UL } },
                                                                                   { { 4UL, 7UL } }

        } };

        return ret;
    }


    const std::array<Hexahedron::FaceContent, Hexahedron::Nface>& Hexahedron::GetFaceList()
    {
        static const std::array<Hexahedron::FaceContent, Hexahedron::Nface> ret{ { { { 0UL, 1UL, 2UL, 3UL } },
                                                                                   { { 0UL, 1UL, 5UL, 4UL } },
                                                                                   { { 0UL, 3UL, 7UL, 4UL } },
                                                                                   { { 1UL, 2UL, 6UL, 5UL } },
                                                                                   { { 3UL, 2UL, 6UL, 7UL } },
                                                                                   { { 4UL, 5UL, 6UL, 7UL } } } };

        return ret;
    }


    const std::vector<LocalCoords>& Hexahedron::GetQ1LocalCoordsList()
    {

        static const std::vector<LocalCoords> ret{ { -1., -1., -1. }, { 1., -1., -1. }, { 1., 1., -1. },
                                                   { -1., 1., -1. },  { -1., -1., 1. }, { 1., -1., 1. },
                                                   { 1., 1., 1. },    { -1., 1., 1. }

        };

        return ret;
    }


    const std::vector<LocalCoords>& Hexahedron::GetQ2cLocalCoordsList()
    {
        // Ordered by increasing topology (vertex, edge, face then volume).
        static const std::vector<LocalCoords> ret{
            { -1., -1., -1. }, { 1., -1., -1. }, { 1., 1., -1. },  { -1., 1., -1. }, { -1., -1., 1. }, { 1., -1., 1. },
            { 1., 1., 1. },    { -1., 1., 1. },  { 0., -1., -1. }, { 1., 0., -1. },  { 0., 1., -1. },  { -1., 0., -1. },
            { -1., -1., 0. },  { 1., -1., 0. },  { 1., 1., 0. },   { -1., 1., 0. },  { 0., -1., 1. },  { 1., 0., 1. },
            { 0., 1., 1. },    { -1., 0., 1. },  { 0., 0., -1. },  { -1., 0., 0. },  { 0., -1., 0. },  { 0., 0., 1. },
            { 1., 0., 0. },    { 0., 1., 0. },   { 0., 0., 0. }

        };

        return ret;
    }


    const std::vector<LocalCoords>& Hexahedron::GetQ2LocalCoordsList()
    {
        // Ordered by increasing topology (vertex, edge, face then volume).
        static const std::vector<LocalCoords> ret{
            { -1., -1., -1. }, { 1., -1., -1. }, { 1., 1., -1. },  { -1., 1., -1. }, { -1., -1., 1. },
            { 1., -1., 1. },   { 1., 1., 1. },   { -1., 1., 1. },  { 0., -1., -1. }, { 1., 0., -1. },
            { 0., 1., -1. },   { -1., 0., -1. }, { -1., -1., 0. }, { 1., -1., 0. },  { 1., 1., 0. },
            { -1., 1., 0. },   { 0., -1., 1. },  { 1., 0., 1. },   { 0., 1., 1. },   { -1., 0., 1. }

        };

        return ret;
    }


    bool Hexahedron::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Hexahedron>(vertex_index, coords);
    }


    bool Hexahedron::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnEdge_Spectral<Hexahedron>(edge_index, coords);
    }


    bool Hexahedron::IsOnFace(std::size_t face_index, const LocalCoords& coords)
    {
        auto [face_constant_component, constant_component_value] = AnalyseFace(face_index);

        assert(dimension == coords.GetDimension());

        if (!NumericNS::AreEqual(coords[face_constant_component], constant_component_value))
            return false;

        // At this stage we know coords is on the right plane but not whether it is inside the quadrangle.
        auto&& coords2d = Extract2DCoordinates(face_constant_component, coords);

        return Quadrangle::IsOnFace(0UL, coords2d);
    }


    LocalCoords Hexahedron::TransformFacePoint(const LocalCoords& coords,
                                               std::size_t face_index,
                                               Advanced::InterfaceNS::orientation_type orientation)
    {
        assert(IsOnFace(face_index, coords));
        assert(orientation < Advanced::InterfaceNS::orientation_type{ 8UL });

        auto [face_constant_component, constant_component_value] = AnalyseFace(face_index);

        // Reduce coords to 2d on the face plane, and use Quadrangle nameskake function for the calculation.
        auto&& coords2d = Extract2DCoordinates(face_constant_component, coords);

        auto&& coords2d_after_transformation = Quadrangle::TransformFacePoint(coords2d, 0UL, orientation);

        // Now grow back the third coordinate!
        static_assert(dimension == ::MoReFEM::GeometryNS::dimension_type{ 3 });
        LocalCoords::underlying_type buf(dimension.Get());

        ::MoReFEM::GeometryNS::dimension_type buf_index{};
        ::MoReFEM::GeometryNS::dimension_type index_2d{};

        for (::MoReFEM::GeometryNS::dimension_type i{}, size = ::MoReFEM::GeometryNS::dimension_type{ dimension };
             i < size;
             ++i)
        {
            assert(buf_index < size);

            if (i == face_constant_component)
                buf(buf_index.Get()) = constant_component_value;
            else
                buf(buf_index.Get()) = coords2d_after_transformation[index_2d++];

            ++buf_index;
        }

        LocalCoords ret(buf);
        return ret;
    }


    ::MoReFEM::InterfaceNS::Nature Hexahedron::GetInteriorInterface()
    {
        return ::MoReFEM::InterfaceNS::Nature::volume;
    }


    ::MoReFEM::TopologyNS::Type Hexahedron::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::hexahedron;
    }


    bool Hexahedron::IsInside(const LocalCoords& coords)
    {
        const auto& coords_list = GetQ1LocalCoordsList();
        assert(coords_list.size() == 8UL);

        const auto r = coords.r();
        const auto s = coords.s();
        const auto t = coords.t();

        {
            const auto& bottom_left_front = coords_list[0];

            if (r < bottom_left_front.r() || s < bottom_left_front.s() || t < bottom_left_front.t())
                return false;
        }

        {
            constexpr auto top_right_back_index{ 6 };
            const auto& top_right_back = coords_list[top_right_back_index];

            if (r > top_right_back.r() || s > top_right_back.s() || t > top_right_back.t())
                return false;
        }

        return true;
    }


    namespace // anonymous
    {


        /*!
         * \brief Extract for an hexahedron face which of its component is constant and what is its value.
         */
        std::pair<::MoReFEM::GeometryNS::dimension_type, double> AnalyseFace(std::size_t face_index)
        {
            // Extract the vertices that delimits the face.
            const auto& vertex_on_face = Advanced::InterfaceNS::LocalData<Hexahedron>::GetFace(face_index);
            assert(vertex_on_face.size() == 4UL);

            // Then their coordinates.
            std::vector<LocalCoords> vertex_local_coords_list;

            for (auto vertex_index : vertex_on_face)
                vertex_local_coords_list.push_back(
                    Advanced::InterfaceNS::LocalData<Hexahedron>::GetVertexCoord(vertex_index));

            auto pair = ExtractIdenticalComponentIndex(vertex_local_coords_list);

            assert(pair.first < Hexahedron::dimension);

            return pair;
        }


        /*!
         * \brief 'Transform' a 3d coords in the hexahedron from into a 2d one.
         *
         * The dropped dimension is the one constant for the given face.
         */
        LocalCoords Extract2DCoordinates(::MoReFEM::GeometryNS::dimension_type face_constant_component,
                                         const LocalCoords& coords)
        {
            const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ coords.GetDimension() };
            assert(Ncomponent.Get() == 3UL);

            constexpr auto dim2 = 2;
            LocalCoords::underlying_type coords2d(dim2);

            Eigen::Index current{};

            for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent; ++component)
            {
                if (component == face_constant_component)
                    continue;

                assert(current < dim2);

                coords2d(current++) = coords[component];
            }

            assert(coords2d.size() == static_cast<std::size_t>(dim2));
            LocalCoords ret(coords2d);

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
