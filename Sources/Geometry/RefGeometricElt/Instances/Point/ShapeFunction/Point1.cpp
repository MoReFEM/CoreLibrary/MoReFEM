// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>

#include "Geometry/RefGeometricElt/Instances/Point/ShapeFunction/Point1.hpp"

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
{


    namespace // anonymous
    {


        const std::array<ShapeFunctionType, 0>& EmptyArray()
        {
            // NOLINTNEXTLINE(misc-const-correctness)
            static std::array<ShapeFunctionType, 0> ret;
            return ret;
        };


    } // namespace


    const std::array<ShapeFunctionType, 1>& Point1::ShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 1> ret{ { Constant<1>() } };

        return ret;
    };


    const std::array<ShapeFunctionType, 0>& Point1::FirstDerivateShapeFunctionList()
    {
        return EmptyArray();
    };


    const std::array<ShapeFunctionType, 0>& Point1::SecondDerivateShapeFunctionList()
    {
        return EmptyArray();
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
