// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/RefGeometricElt/Instances/Point/Topology/Point.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
{


    const std::string& Point::ClassName()
    {
        static const std::string ret("Point");
        return ret;
    }


    bool Point::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Point>(vertex_index, coords);
    }


    const std::vector<LocalCoords>& Point::GetQ1LocalCoordsList()
    {
        static const std::vector<LocalCoords> ret{ { 0 } };

        return ret;
    }


    ::MoReFEM::InterfaceNS::Nature Point::GetInteriorInterface()
    {
        return ::MoReFEM::InterfaceNS::Nature::vertex;
    }


    ::MoReFEM::TopologyNS::Type Point::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::point;
    }


    bool Point::IsInside(const LocalCoords& coords)
    {
        assert(GetQ1LocalCoordsList().size() == 1);
        return coords == GetQ1LocalCoordsList().back();
    }


} // namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
