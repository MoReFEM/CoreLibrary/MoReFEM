// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>

#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle9.hpp"

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
{


    const std::array<ShapeFunctionType, 9>& Quadrangle9::ShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 9> ret{
            { [](const auto& local)
              {
                  return 0.25 * (1. - local.r()) * (1. - local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. + local.r()) * (1. - local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.r()) * (1. + local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - local.r()) * (1. + local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r() * local.r()) * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * (1. - local.s() * local.s()) * local.r();
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r() * local.r()) * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * (1. - local.s() * local.s()) * local.r();
              },
              [](const auto& local)
              {
                  return (1. - local.r()) * (1. + local.r()) * (1. - local.s()) * (1. + local.s());
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 18>& Quadrangle9::FirstDerivateShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 18> ret{
            { [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.r() * local.s()
                         + 0.25 * (1. - local.r()) * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - local.r()) * local.r() * local.s()
                         + 0.25 * (1. - local.r()) * (1. - local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.r() * local.s()
                         - 0.25 * (1. + local.r()) * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.r()) * local.r() * local.s()
                         - 0.25 * (1. + local.r()) * (1. - local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.r() * local.s()
                         + 0.25 * (1. + local.r()) * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.r()) * local.r() * local.s()
                         + 0.25 * (1. + local.r()) * (1. + local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.r() * local.s()
                         - 0.25 * (1. - local.r()) * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - local.r()) * local.r() * local.s()
                         - 0.25 * (1. - local.r()) * (1. + local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return local.r() * local.s() * (1. - local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r() * local.r()) * (local.s() - (1. - local.s()));
              },

              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * local.s() * (1. + local.r());
              },

              [](const auto& local)
              {
                  return -local.r() * local.s() * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r() * local.r()) * (local.s() + (1. + local.s()));
              },

              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return local.r() * local.s() * (1. - local.r());
              },

              [](const auto& local)
              {
                  return -2. * local.r() * (1. - local.s()) * (1. + local.s());
              },
              [](const auto& local)
              {
                  return -2. * local.s() * (1. - local.r()) * (1. + local.r());
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 36>& Quadrangle9::SecondDerivateShapeFunctionList()
    {

        static const std::array<ShapeFunctionType, 36> ret{
            { [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.s() - 0.25 * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * local.r() * (-2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - 2. * local.r()) * (local.s() - (1. - local.s()));
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.s() - 0.25 * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * local.r() * (-2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + 2. * local.r()) * (local.s() - (1. - local.s()));
              },
              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.s() + 0.25 * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * local.r() * (2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + 2. * local.r()) * (local.s() + (1. + local.s()));
              },
              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.s() + 0.25 * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * local.r() * (2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - 2. * local.r()) * (local.s() + (1. + local.s()));
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return local.s() * (1. - local.s());
              },
              [](const auto& local)
              {
                  return local.r() * (1. - 2. * local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * (local.s() - (1. - local.s()));
              },
              [](const auto& local)
              {
                  return 1. - local.r() * local.r();
              },

              [](const auto& local)
              {
                  return 0.5 * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return -local.s() * (1. + local.r());
              },
              [](const auto& local)
              {
                  return -local.s() * (1. + 2. * local.r());
              },
              [](const auto& local)
              {
                  return -local.r() * (1. + local.r());
              },

              [](const auto& local)
              {
                  return -local.s() * (1. + local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * (1. + 2. * local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * (local.s() + (1. + local.s()));
              },
              [](const auto& local)
              {
                  return 1. - local.r() * local.r();
              },

              [](const auto& local)
              {
                  return 0.5 * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return local.s() * (1. - local.r());
              },
              [](const auto& local)
              {
                  return local.s() * (1. - 2. * local.r());
              },
              [](const auto& local)
              {
                  return local.r() * (1. - local.r());
              },

              [](const auto& local)
              {
                  return -2. * (1. - local.s()) * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 4. * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return 4. * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -2. * (1. - local.r()) * (1. + local.r());
              } }
        };

        return ret;
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
