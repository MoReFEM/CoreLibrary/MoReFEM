// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <limits>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
{


    const std::string& Quadrangle::ClassName()
    {
        static const std::string ret("Quadrangle");
        return ret;
    }


    /************************************************************************
     *
     *     3-----------2
     *     |           |
     *     |           |
     *     |           |
     *     |           |
     *     |           |
     *     0-----------1
     *
     *************************************************************************/


    const std::array<Quadrangle::EdgeContent, Quadrangle::Nedge>& Quadrangle::GetEdgeList()
    {
        static const std::array<Quadrangle::EdgeContent, Quadrangle::Nedge> ret{
            { { { 0UL, 1UL } }, { { 1UL, 2UL } }, { { 3UL, 2UL } }, { { 0UL, 3UL } } }
        };

        return ret;
    }


    const std::array<Quadrangle::FaceContent, Quadrangle::Nface>& Quadrangle::GetFaceList()
    {
        static const std::array<Quadrangle::FaceContent, Quadrangle::Nface> ret{ { { { 0UL, 1UL, 2UL, 3UL } } } };

        return ret;
    }


    const std::vector<LocalCoords>& Quadrangle::GetQ1LocalCoordsList()
    {
        static const std::vector<LocalCoords> ret{ { -1., -1. }, { 1., -1. }, { 1., 1. }, { -1., 1. } };

        return ret;
    }


    bool Quadrangle::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Quadrangle>(vertex_index, coords);
    }


    bool Quadrangle::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnEdge_Spectral<Quadrangle>(edge_index, coords);
    }


    bool Quadrangle::IsOnFace([[maybe_unused]] std::size_t face_index, const LocalCoords& coords)
    {
        assert(face_index < Nface);
        assert(face_index == 0UL);

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ dimension };

        const auto& local_coords_list = GetQ1LocalCoordsList();

        for (auto component = ::MoReFEM::GeometryNS::dimension_type{}; component < Ncomponent; ++component)
        {
            // Take two diagonally opposed vertices to check whether the point is inside or not.
            const auto min = std::min(local_coords_list[0][component], local_coords_list[2][component]);
            const auto max = std::max(local_coords_list[0][component], local_coords_list[2][component]);

            // Check whether the coordinates in between vertices.
            if (coords[component] < min || coords[component] > max)
                return false;
        }


        return true;
    }


    LocalCoords Quadrangle::TransformFacePoint(const LocalCoords& coords,
                                               [[maybe_unused]] std::size_t face_index,
                                               Advanced::InterfaceNS::orientation_type orientation)
    {
        assert(IsOnFace(face_index, coords));
        assert(orientation < Advanced::InterfaceNS::orientation_type{ 8UL });
        assert(face_index == 0UL && "Only choice for a 2D quadrangle...");

        double x_before = coords[::MoReFEM::GeometryNS::dimension_type{}];
        double y_before = coords[::MoReFEM::GeometryNS::dimension_type{ 1 }];

        assert(!NumericNS::AreEqual(x_before, std::numeric_limits<double>::lowest()));
        assert(!NumericNS::AreEqual(y_before, std::numeric_limits<double>::lowest()));

        double x_after = std::numeric_limits<double>::lowest();
        double y_after = std::numeric_limits<double>::lowest();


        // Switch to the reference quadrangle [0,1]^2 (due to the history of the code ie Ondomatic...). #896.
        x_before = 0.5 * (x_before + 1.);
        y_before = 0.5 * (y_before + 1.);

        switch (orientation.Get())
        {
        case 0UL:
        {
            x_after = x_before;
            y_after = y_before;
            break;
        }
        case 1UL:
        {
            x_after = 1. - y_before;
            y_after = x_before;
            break;
        }
        case 2UL:
        {
            x_after = 1. - x_before;
            y_after = 1. - y_before;
            break;
        }
        case 3UL:
        {
            x_after = y_before;
            y_after = 1. - x_before;
            break;
        }
        case 4UL:
        {
            x_after = y_before;
            y_after = x_before;
            break;
        }
        case 5UL:
        {
            x_after = 1. - x_before;
            y_after = y_before;
            break;
        }
        case 6UL:
        {
            x_after = 1. - y_before;
            y_after = 1. - x_before;

            break;
        }
        case 7UL:
        {
            x_after = x_before;
            y_after = 1. - y_before;
            break;
        }
        }

        // Switch back to [-1,1]^2. #896.
        x_after = 2. * x_after - 1.;
        y_after = 2. * y_after - 1.;

        assert(!NumericNS::AreEqual(x_after, std::numeric_limits<double>::lowest()));
        assert(!NumericNS::AreEqual(y_after, std::numeric_limits<double>::lowest()));

        LocalCoords ret({ x_after, y_after });
        return ret;
    }


    ::MoReFEM::InterfaceNS::Nature Quadrangle::GetInteriorInterface()
    {
        return ::MoReFEM::InterfaceNS::Nature::face;
    }


    ::MoReFEM::TopologyNS::Type Quadrangle::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::quadrangle;
    }


    bool Quadrangle::IsInside(const LocalCoords& coords)
    {
        const auto& coords_list = GetQ1LocalCoordsList();
        assert(coords_list.size() == 4UL);

        const auto r = coords.r();
        const auto s = coords.s();

        {
            const auto& bottom_left = coords_list[0];

            if (r < bottom_left.r() || s < bottom_left.s())
                return false;
        }

        {
            const auto& top_right = coords_list[2];

            if (r > top_right.r() || s > top_right.s())
                return false;
        }

        return true;
    }


} // namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
