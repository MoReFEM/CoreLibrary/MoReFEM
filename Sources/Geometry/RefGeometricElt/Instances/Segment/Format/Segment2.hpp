// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_FORMAT_SEGMENT2_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_FORMAT_SEGMENT2_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Format.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::MeshNS::FormatNS
{

    template <::MoReFEM::MeshNS::Format TypeT, Advanced::GeometricEltEnum NatureT> struct Support;

}

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::MeshNS::FormatNS
{


    //! \copydoc doxygen_hide_geometry_format_ensight_support
    template<>
    struct Support<::MoReFEM::MeshNS::Format::Ensight, Advanced::GeometricEltEnum::Segment2> : public std::true_type
    {


        //! Name of such an object in Ensight 6 files.
        static const Advanced::GeomEltNS::EnsightName& EnsightName();
    };


    //! \copydoc doxygen_hide_geometry_format_medit_support
    template<>
    struct Support<::MoReFEM::MeshNS::Format::Medit, Advanced::GeometricEltEnum::Segment2> : public std::true_type
    {


        //! Medit code for this object.
        static constexpr GmfKwdCod MeditId();
    };


} // namespace MoReFEM::Internal::MeshNS::FormatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/RefGeometricElt/Instances/Segment/Format/Segment2.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_FORMAT_SEGMENT2_DOT_HPP_
// *** MoReFEM end header guards *** < //
