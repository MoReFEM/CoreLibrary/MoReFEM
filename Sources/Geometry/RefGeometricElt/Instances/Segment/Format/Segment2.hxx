// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_FORMAT_SEGMENT2_DOT_HXX_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_FORMAT_SEGMENT2_DOT_HXX_
// IWYU pragma: private, include "Geometry/RefGeometricElt/Instances/Segment/Format/Segment2.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Format.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    inline constexpr GmfKwdCod
    Support<::MoReFEM::MeshNS::Format::Medit, Advanced::GeometricEltEnum::Segment2>::MeditId()
    {
        return GmfEdges;
    }


} // namespace MoReFEM::Internal::MeshNS::FormatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_FORMAT_SEGMENT2_DOT_HXX_
// *** MoReFEM end header guards *** < //
