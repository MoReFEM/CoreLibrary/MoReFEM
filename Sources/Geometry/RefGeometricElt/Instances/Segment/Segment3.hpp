// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_SEGMENT3_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_SEGMENT3_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Geometry/RefGeometricElt/Instances/Segment/Traits/Segment3.hpp"
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS
{


    /*!
     * \brief Acts as a strawman class for MoReFEM::Advanced::RefGeomEltNS::Traits::Segment3.
     *
     * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
     * store in one dynamic container all the kinds of GeometricElt present in a mesh.
     *
     * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
     * can be included in:
     *
     * \code
     * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
     * \endcode
     *
     */
    class Segment3 final : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Segment3>
    {
      public:
        //! Constructor.
        Segment3() = default;

        //! Destructor.
        virtual ~Segment3() override;

        //! \copydoc doxygen_hide_copy_constructor
        Segment3(const Segment3& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Segment3(Segment3&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Segment3& operator=(const Segment3& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Segment3& operator=(Segment3&& rhs) = delete;


      private:
        // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_SEGMENT3_DOT_HPP_
// *** MoReFEM end header guards *** < //
