// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
{


    const std::string& Segment::ClassName()
    {
        static const std::string ret("Segment");
        return ret;
    }


    /************************************************************************
     *   0-----------1         *
     *************************************************************************/


    const std::array<Segment::EdgeContent, Segment::Nedge>& Segment::GetEdgeList()
    {
        static const std::array<Segment::EdgeContent, Segment::Nedge> ret{ { { { 0UL, 1UL } } } };

        return ret;
    }


    const std::vector<LocalCoords>& Segment::GetQ1LocalCoordsList()
    {
        static const std::vector<LocalCoords> ret{ { -1. }, { 1. } };

        return ret;
    }


    bool Segment::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Segment>(vertex_index, coords);
    }


    bool Segment::IsOnEdge([[maybe_unused]] std::size_t edge_index, const LocalCoords& coords)
    {
        assert(edge_index == 0UL); // the only relevant value for a \a Segment...

        static_assert(dimension == ::MoReFEM::GeometryNS::dimension_type{ 1 });

        assert(coords.GetDimension() == dimension);
        const auto& local_coords_list = GetQ1LocalCoordsList();
        assert(local_coords_list.size() == 2UL);

        constexpr ::MoReFEM::GeometryNS::dimension_type comp0{};

        const auto min = std::min(local_coords_list[0][comp0], local_coords_list[1][comp0]);
        const auto max = std::max(local_coords_list[0][comp0], local_coords_list[1][comp0]);

        // Check whether the coordinates in between vertices.
        return (coords[comp0] >= min && coords[comp0] <= max);
    }


    ::MoReFEM::InterfaceNS::Nature Segment::GetInteriorInterface()
    {
        return ::MoReFEM::InterfaceNS::Nature::edge;
    }


    ::MoReFEM::TopologyNS::Type Segment::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::segment;
    }


    bool Segment::IsInside(const LocalCoords& coords)
    {
        const auto& coords_list = GetQ1LocalCoordsList();
        assert(coords_list.size() == 2UL);

        return coords.r() >= coords_list[0].r() && coords.r() <= coords_list[1].r();
    }


} // namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
