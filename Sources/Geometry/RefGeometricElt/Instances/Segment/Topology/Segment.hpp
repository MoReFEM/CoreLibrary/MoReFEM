// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_TOPOLOGY_SEGMENT_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_TOPOLOGY_SEGMENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>     // IWYU pragma: keep
#include <iosfwd>      // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
// IWYU pragma: no_include <string>

// IWYU pragma: begin_exports
#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp"
// IWYU pragma: end_exports

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::InterfaceNS { template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT> struct LocalData; } // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
{


    /*!
     * \brief Topology of a segment.
     */
    class Segment final
    {

      public:
        //! Data.
        enum : Eigen::Index {
            Nvertex = 2,
            Nedge = 1,
            Nface = 0,
            Nvolume = 0,
        };

        //! Dimension.
        static inline constexpr ::MoReFEM::GeometryNS::dimension_type dimension{ 1 };

        //! Topology of an edge.
        using EdgeTopology = Segment;

        //! Topology of a face (irrelevant here...).
        using FaceTopology = std::false_type;

        //! Container used to store all the points of an edge. The index of the points are actually stored.
        using EdgeContent = std::array<std::size_t, EdgeTopology::Nvertex>;

        //! Container used to store all the points of a face. The index of the points are actually stored.
        using FaceContent = std::false_type;

        //! Container used to store the local coordinates of the vertices.
        using LocalCoordListType = std::vector<LocalCoords>;

        /*!
         * \brief Interface in which interior dofs (i.e. those without continuity with neighbor elements) are
         * stored.
         */
        static ::MoReFEM::InterfaceNS::Nature GetInteriorInterface();

        //! Returns the enum that tags the topology.
        static ::MoReFEM::TopologyNS::Type GetType() noexcept;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Segment() = default;

      protected:
        //! Destructor.
        ~Segment() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Segment(const Segment& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Segment(Segment&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Segment& operator=(const Segment& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Segment& operator=(Segment&& rhs) = delete;

        ///@}

      public:
        //! Return the name of the topology.
        static const std::string& ClassName();

        /*!
         * \copydoc doxygen_hide_is_on_local_vertex
         */
        static bool IsOnVertex(std::size_t local_vertex_index, const LocalCoords& local_coords);

        /*!
         * \copydoc doxygen_hide_is_on_local_edge
         */
        static bool IsOnEdge(std::size_t local_edge_index, const LocalCoords& local_coords);

        //! Return the local coordinates of the vertices.
        static const LocalCoordListType& GetQ1LocalCoordsList();


      private:
        //! Whether a \a local_coords is inside the reference segment or not.
        //! \param[in] local_coords \a LocalCoords which status related to the \a RefGeomElt is sought.
        static bool IsInside(const LocalCoords& local_coords);

        //! Return the list of edges.
        static const std::array<EdgeContent, Nedge>& GetEdgeList();


        /*!
         * \brief Friendship to LocalData.
         *
         * As you can see above, list of local edges, faces and list of vertex coordinates are private
         * members; to access them you must use the LocalData class.
         *
         * For instance:
         *
         * \code
         * const auto& topology_face =
         * Advanced::RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index); \endcode
         *
         */
        template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
        friend struct Advanced::InterfaceNS::LocalData;
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_TOPOLOGY_SEGMENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
