// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_TRAITS_SEGMENT3_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_TRAITS_SEGMENT3_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Format/Segment3.hpp"        // IWYU pragma: export
#include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment3.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"       // IWYU pragma: export
// < absolutely required to let MoReFEM know which format are actually supported!

#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"
#include "Geometry/RefGeometricElt/StrongType.hpp"

namespace MoReFEM::Advanced::RefGeomEltNS::Traits
{


    /*!
     * \brief Traits class that holds the static functions related to shape functions, interface and topology.
     *
     * It can't be instantiated directly: its purpose is either to provide directly a data through
     * static function:
     *
     * \code
     * constexpr auto Nshape_function = Segment3::NshapeFunction();
     * \endcode
     *
     * or to be a template parameter to MoReFEM::Advanced::RefGeomEltNS::Segment3 class (current class is in an
     * additional layer of namespace):
     *
     * \code
     * MoReFEM::Advanced::RefGeomEltNS::Traits::Segment3
     * \endcode
     *
     */
    class Segment3 final
    : public ::MoReFEM::Internal::RefGeomEltNS::RefGeomEltImpl<Segment3, ShapeFunctionNS::Segment3, TopologyNS::Segment>
    {


      protected:
        //! Convenient alias.
        using self = Segment3;


        /// \name Special members: prevent direct instantiation of RefGeomEltImpl objects.
        ///@{

        //! Constructor.
        Segment3() = delete;

        //! Destructor.
        ~Segment3() = delete;

        //! \copydoc doxygen_hide_copy_constructor
        Segment3(const Segment3& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Segment3(Segment3&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Segment3& operator=(const Segment3& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Name associated to the RefGeomElt.
         *
         * \return Name that is guaranteed to be unique (through the GeometricEltFactory) and can
         * therefore also act as an identifier.
         */
        static const Advanced::GeomEltNS::GenericName& ClassName();

        //! Number of Coords required to describe fully a GeometricElt of this type.
        static inline constexpr ::MoReFEM::GeomEltNS::Nlocal_coords_type Ncoords{ 3 };

        /*!
         * \brief Enum associated to the RefGeomElt.
         *
         * \return Enum value guaranteed to be unique (through the GeometricEltFactory); its
         * raison d'être is that many operations are much faster on an enumeration than on a string.
         */
        static constexpr MoReFEM::Advanced::GeometricEltEnum Identifier()
        {
            return MoReFEM::Advanced::GeometricEltEnum::Segment3;
        }


      private:
        // THIS IS A TRAIT CLASS, NO MEMBERS ALLOWED HERE!
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS::Traits


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_SEGMENT_TRAITS_SEGMENT3_DOT_HPP_
// *** MoReFEM end header guards *** < //
