// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <iosfwd> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Format/Tetrahedron10.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Format.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    const Advanced::GeomEltNS::EnsightName&
    Support<::MoReFEM::MeshNS::Format::Ensight, Advanced::GeometricEltEnum::Tetrahedron10>::EnsightName()
    {
        static const Advanced::GeomEltNS::EnsightName ret("tetra10");
        return ret;
    };


} // namespace MoReFEM::Internal::MeshNS::FormatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
