// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron10.hpp"

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
{

    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
    const std::array<ShapeFunctionType, 10>& Tetrahedron10::ShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 10> ret{
            { [](const auto& local_coords)
              {
                  return -(1. - local_coords.r() - local_coords.s() - local_coords.t())
                         * (1. - 2. * (1. - local_coords.r() - local_coords.s() - local_coords.t()));
              },
              [](const auto& local_coords)
              {
                  return -local_coords.r() * (1. - 2. * local_coords.r());
              },
              [](const auto& local_coords)
              {
                  return -local_coords.s() * (1. - 2. * local_coords.s());
              },
              [](const auto& local_coords)
              {
                  return -local_coords.t() * (1. - 2. * local_coords.t());
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.r() * (1. - local_coords.r() - local_coords.s() - local_coords.t());
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.r() * local_coords.s();
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.s() * (1. - local_coords.r() - local_coords.s() - local_coords.t());
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.t() * (1. - local_coords.r() - local_coords.s() - local_coords.t());
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.r() * local_coords.t();
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.s() * local_coords.t();
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 30>& Tetrahedron10::FirstDerivateShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 30> ret{ {
            [](const auto& local_coords)
            {
                return -3. + 4. * local_coords.r() + 4. * local_coords.s() + 4. * local_coords.t();
            },
            [](const auto& local_coords)
            {
                return -3. + 4. * local_coords.r() + 4. * local_coords.s() + 4. * local_coords.t();
            },
            [](const auto& local_coords)
            {
                return -3. + 4. * local_coords.r() + 4. * local_coords.s() + 4. * local_coords.t();
            },

            [](const auto& local_coords)
            {
                return -1. + 4. * local_coords.r();
            },
            Constant<0>(),
            Constant<0>(),

            Constant<0>(),
            [](const auto& local_coords)
            {
                return -1. + 4. * local_coords.s();
            },
            Constant<0>(),

            Constant<0>(),
            Constant<0>(),
            [](const auto& local_coords)
            {
                return -1. + 4. * local_coords.t();
            },

            [](const auto& local_coords)
            {
                return 4. - 8. * local_coords.r() - 4. * local_coords.s() - 4. * local_coords.t();
            },
            [](const auto& local_coords)
            {
                return -4. * local_coords.r();
            },
            [](const auto& local_coords)
            {
                return -4. * local_coords.r();
            },

            [](const auto& local_coords)
            {
                return 4. * local_coords.s();
            },
            [](const auto& local_coords)
            {
                return 4. * local_coords.r();
            },
            Constant<0>(),

            [](const auto& local_coords)
            {
                return -4. * local_coords.s();
            },
            [](const auto& local_coords)
            {
                return 4. - 4. * local_coords.r() - 8. * local_coords.s() - 4. * local_coords.t();
            },
            [](const auto& local_coords)
            {
                return -4. * local_coords.s();
            },

            [](const auto& local_coords)
            {
                return -4. * local_coords.t();
            },
            [](const auto& local_coords)
            {
                return -4. * local_coords.t();
            },
            [](const auto& local_coords)
            {
                return 4. - 4. * local_coords.r() - 4. * local_coords.s() - 8. * local_coords.t();
            },

            [](const auto& local_coords)
            {
                return 4. * local_coords.t();
            },
            Constant<0>(),
            [](const auto& local_coords)
            {
                return 4. * local_coords.r();
            },

            Constant<0>(),
            [](const auto& local_coords)
            {
                return 4. * local_coords.t();
            },
            [](const auto& local_coords)
            {
                return 4. * local_coords.s();
            },
        } };

        return ret;
    };


    const std::array<ShapeFunctionType, 90>& Tetrahedron10::SecondDerivateShapeFunctionList()
    {

        static const std::array<ShapeFunctionType, 90> ret{ {
            Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),
            Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<0>(),  Constant<0>(),
            Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
            Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),
            Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
            Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),
            Constant<-8>(), Constant<-4>(), Constant<-4>(), Constant<-4>(), Constant<0>(),  Constant<0>(),
            Constant<-4>(), Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),
            Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
            Constant<0>(),  Constant<-4>(), Constant<0>(),  Constant<-4>(), Constant<-8>(), Constant<-4>(),
            Constant<0>(),  Constant<-4>(), Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<-4>(),
            Constant<0>(),  Constant<0>(),  Constant<-4>(), Constant<-4>(), Constant<-4>(), Constant<-8>(),
            Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
            Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),
            Constant<0>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),  Constant<4>(),  Constant<0>(),
        } };

        return ret;
    };
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)

} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
