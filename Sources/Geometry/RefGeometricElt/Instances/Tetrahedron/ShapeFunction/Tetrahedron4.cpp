// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron4.hpp"

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
{

    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
    const std::array<ShapeFunctionType, 4>& Tetrahedron4::ShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 4> ret{ { [](const auto& local_coords)
                                                             {
                                                                 return 1. - local_coords.r() - local_coords.s()
                                                                        - local_coords.t();
                                                             },
                                                             [](const auto& local_coords)
                                                             {
                                                                 return local_coords.r();
                                                             },
                                                             [](const auto& local_coords)
                                                             {
                                                                 return local_coords.s();
                                                             },
                                                             [](const auto& local_coords)
                                                             {
                                                                 return local_coords.t();
                                                             } } };

        return ret;
    };


    const std::array<ShapeFunctionType, 12>& Tetrahedron4::FirstDerivateShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 12> ret{ { Constant<-1>(),
                                                              Constant<-1>(),
                                                              Constant<-1>(),
                                                              Constant<1>(),
                                                              Constant<0>(),
                                                              Constant<0>(),
                                                              Constant<0>(),
                                                              Constant<1>(),
                                                              Constant<0>(),
                                                              Constant<0>(),
                                                              Constant<0>(),
                                                              Constant<1>() } };

        return ret;
    };


    const std::array<ShapeFunctionType, 36>& Tetrahedron4::SecondDerivateShapeFunctionList()
    {

        static const std::array<ShapeFunctionType, 36> ret{
            { Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
              Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
              Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
              Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
              Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(),
              Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>(), Constant<0>() }
        };

        return ret;
    };
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)

} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
