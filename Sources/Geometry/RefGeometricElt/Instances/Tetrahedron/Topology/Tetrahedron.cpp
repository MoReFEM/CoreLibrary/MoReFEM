// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>
#include <cassert>
#include <cstdlib>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
{


    const std::string& Tetrahedron::ClassName()
    {
        static const std::string ret("Tetrahedron");
        return ret;
    }


    /************************************************************************
     *
     *           3
     *          /.\
     *         / . \
     *        /  2  \
     *       / .  .  \
     *      /.      . \
     *     0-----------1
     *
     *************************************************************************/


    const std::array<Tetrahedron::EdgeContent, Tetrahedron::Nedge>& Tetrahedron::GetEdgeList()
    {
        static const std::array<Tetrahedron::EdgeContent, Tetrahedron::Nedge> ret{ { { { 0UL, 1UL } },
                                                                                     { { 1UL, 2UL } },
                                                                                     { { 0UL, 2UL } },
                                                                                     { { 0UL, 3UL } },
                                                                                     { { 1UL, 3UL } },
                                                                                     { { 2UL, 3UL } } } };

        return ret;
    }


    const std::array<Tetrahedron::FaceContent, Tetrahedron::Nface>& Tetrahedron::GetFaceList()
    {
        static const std::array<Tetrahedron::FaceContent, Tetrahedron::Nface> ret{
            { { { 0, 1, 2 } }, { { 0, 1, 3 } }, { { 0, 2, 3 } }, { { 1, 2, 3 } } }
        };

        return ret;
    }


    bool Tetrahedron::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Tetrahedron>(vertex_index, coords);
    }


    bool Tetrahedron::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
    {
        if (!IsInside(coords))
            return false;

        assert(edge_index < 6UL);
        assert(coords.GetDimension() == ::MoReFEM::GeometryNS::dimension_type{ 3 });
        const double r = coords.r();
        const double s = coords.s();
        const double t = coords.t();

        switch (edge_index)
        {
        case 0UL:
            return NumericNS::IsZero(s) && NumericNS::IsZero(t);
        case 1UL:
            return NumericNS::AreEqual(1., r + s) && NumericNS::IsZero(t);
        case 2UL:
            return NumericNS::IsZero(r) && NumericNS::IsZero(t);
        case 3UL:
            return NumericNS::IsZero(r) && NumericNS::IsZero(s);
        case 4UL:
            return NumericNS::AreEqual(1., r + t) && NumericNS::IsZero(s);
        case 5UL:
            return NumericNS::AreEqual(1., s + t) && NumericNS::IsZero(r);
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        assert(false);
        return false;
    }


    bool Tetrahedron::IsOnFace(std::size_t face_index, const LocalCoords& coords)
    {
        if (!IsInside(coords))
            return false;

        assert(face_index < 4UL);
        assert(coords.GetDimension() == ::MoReFEM::GeometryNS::dimension_type{ 3 });

        const double r = coords.r();
        const double s = coords.s();
        const double t = coords.t();

        switch (face_index)
        {
        case 0UL:
            return NumericNS::IsZero(t);
        case 1UL:
            return NumericNS::IsZero(s);
        case 2UL:
            return NumericNS::IsZero(r);
        case 3UL:
            return NumericNS::AreEqual(1., r + s + t);
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        assert(false);
        return false;
    }


    LocalCoords Tetrahedron::TransformFacePoint(const LocalCoords& coords,
                                                std::size_t face_index,
                                                Advanced::InterfaceNS::orientation_type orientation)
    {
        assert(face_index < 4UL);
        assert(IsOnFace(face_index, coords));
        assert(orientation < Advanced::InterfaceNS::orientation_type{ 6UL });

        const double r = coords.r();
        const double s = coords.s();
        const double t = coords.t();


        // Create a dummy 2D LocalCoords that acts upon the face-triangle.
        constexpr auto dim2 = 2;

        LocalCoords::underlying_type coords_2d_buf(dim2);

        switch (face_index)
        {
        case 0UL:
            coords_2d_buf(0) = r;
            coords_2d_buf(1) = s;
            break;
        case 1UL:
            coords_2d_buf(0) = r;
            coords_2d_buf(1) = t;
            break;
        case 2UL:
        case 3UL:
            coords_2d_buf(0) = s;
            coords_2d_buf(1) = t;
            break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        const LocalCoords coords_2d_before_transformation(coords_2d_buf);

        // Use these 2D coords with the namesake method in Triangle.
        auto coords_2d_after_transformation =
            Triangle::TransformFacePoint(coords_2d_before_transformation, 0UL, orientation);

        // Then report the 2D result into a 3D LocalCoords.
        const double r_2d = coords_2d_after_transformation[::MoReFEM::GeometryNS::dimension_type{}];
        const double s_2d = coords_2d_after_transformation[::MoReFEM::GeometryNS::dimension_type{ 1 }];

        switch (face_index)
        {
        case 0UL:
            return LocalCoords({ r_2d, s_2d, 0. });
        case 1UL:
            return LocalCoords({ r_2d, 0., s_2d });
        case 2UL:
            return LocalCoords({ 0., s_2d, r_2d });
        case 3UL:
            return LocalCoords({ 1. - r_2d - s_2d, r_2d, s_2d });
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        assert(false);
        return coords_2d_after_transformation; // dummy return to avoid compilation error.
    }


    const std::vector<LocalCoords>& Tetrahedron::GetQ1LocalCoordsList()
    {
        static const std::vector<LocalCoords> ret{ { 0., 0., 0. }, { 1., 0., 0. }, { 0., 1., 0. }, { 0., 0., 1. } };

        return ret;
    }


    bool Tetrahedron::IsInside(const LocalCoords& coords)
    {
        assert(coords.GetDimension() == ::MoReFEM::GeometryNS::dimension_type{ 3 });

        const double r = coords.r();
        const double s = coords.s();
        const double t = coords.t();

        if (r < 0. || s < 0. || t < 0.)
            return false;

        if (r + s + t <= 1.)
            return true;

        return false;
    }


    ::MoReFEM::InterfaceNS::Nature Tetrahedron::GetInteriorInterface()
    {
        return ::MoReFEM::InterfaceNS::Nature::volume;
    }


    ::MoReFEM::TopologyNS::Type Tetrahedron::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::tetrahedron;
    }


} // namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
