// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_SHAPEFUNCTION_TRIANGLE3_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_SHAPEFUNCTION_TRIANGLE3_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <array>

#include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/StrongType.hpp" // IWYU pragma: export

namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
{


    /*!
     * \brief Define Triangle3 shape functions and its derivative.
     */
    struct Triangle3 : public Crtp::AccessShapeFunction<Triangle3>
    {

        //! Number of component against which derivation occurs.
        static inline constexpr ::MoReFEM::GeometryNS::dimension_type Nderivate_component_{ 2 };

        //! \copydoc doxygen_hide_shape_function_instance_enum
        enum { Nphi_ = 3, Order = 1 };


        //! Shape functions.
        static const std::array<ShapeFunctionType, Nphi_>& ShapeFunctionList();

        /*!
         * \brief First derivative of the shape functions.
         *
         * Ordering:
         *   \li d(phi[0], r), d(phi[0], s)
         *   \li d(phi[1], r), d(phi[1], s)
         *   etc...
         *
         * \return The derivatives as an array of functions (ordering defined just above)
         */
        static const std::array<ShapeFunctionType, Nphi_ * Nderivate_component_.Get()>&
        FirstDerivateShapeFunctionList();

        /*!
         * \brief Second derivative of the shape functions.
         *
         * Ordering:
         *   \li d2(phi[0], r, r), d2(phi[0], r, s)
         *   \li d2(phi[0], s, r), d2(phi[0], s, s)
         *   \li d2(phi[1], r, r), d2(phi[1], r, s)
         *   etc...
         *
         * \return The derivatives as an array of functions (ordering defined just above)
         */

        static const std::array<ShapeFunctionType, Nphi_ * Nderivate_component_.Get() * Nderivate_component_.Get()>&
        SecondDerivateShapeFunctionList();
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_SHAPEFUNCTION_TRIANGLE3_DOT_HPP_
// *** MoReFEM end header guards *** < //
