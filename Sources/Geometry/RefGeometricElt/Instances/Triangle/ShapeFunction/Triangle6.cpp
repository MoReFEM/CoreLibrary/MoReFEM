// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>

#include "Geometry/RefGeometricElt/Instances/Triangle/ShapeFunction/Triangle6.hpp"

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)

namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
{

    const std::array<ShapeFunctionType, 6>& Triangle6::ShapeFunctionList()
    {
        static const std::array<ShapeFunctionType, 6> ret{
            { [](const auto& local_coords)
              {
                  return (1. - local_coords.r() - local_coords.s())
                         * (1. - local_coords.r() - local_coords.r() - local_coords.s() - local_coords.s());
              },
              [](const auto& local_coords)
              {
                  return -local_coords.r() * (1. - local_coords.r() - local_coords.r());
              },
              [](const auto& local_coords)
              {
                  return -local_coords.s() * (1. - local_coords.s() - local_coords.s());
              },
              [](const auto& local_coords)
              {
                  return (4. * local_coords.r() * (1. - local_coords.r() - local_coords.s()));
              },
              [](const auto& local_coords)
              {
                  return (4. * local_coords.r() * local_coords.s());
              },
              [](const auto& local_coords)
              {
                  return (4. * local_coords.s() * (1. - local_coords.r() - local_coords.s()));
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 12>& Triangle6::FirstDerivateShapeFunctionList()
    {

        static const std::array<ShapeFunctionType, 12> ret{
            { [](const auto& local_coords)
              {
                  return 4. * (local_coords.r() + local_coords.s()) - 3.;
              },
              [](const auto& local_coords)
              {
                  return 4. * (local_coords.r() + local_coords.s()) - 3.;
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.r() - 1.;
              },
              Constant<0>(),
              Constant<0>(),
              [](const auto& local_coords)
              {
                  return 4. * local_coords.s() - 1.;
              },
              [](const auto& local_coords)
              {
                  return 4. * (1. - local_coords.r() - local_coords.r() - local_coords.s());
              },
              [](const auto& local_coords)
              {
                  return -4. * local_coords.r();
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.s();
              },
              [](const auto& local_coords)
              {
                  return 4. * local_coords.r();
              },
              [](const auto& local_coords)
              {
                  return -4. * local_coords.s();
              },
              [](const auto& local_coords)
              {
                  return 4. * (1. - local_coords.r() - local_coords.s() - local_coords.s());
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 24>& Triangle6::SecondDerivateShapeFunctionList()
    {

        static const std::array<ShapeFunctionType, 24> ret{
            { Constant<0>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<4>(),  Constant<0>(),
              Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<0>(),  Constant<4>(),
              Constant<-8>(), Constant<-4>(), Constant<-4>(), Constant<0>(),  Constant<0>(),  Constant<4>(),
              Constant<4>(),  Constant<0>(),  Constant<0>(),  Constant<-4>(), Constant<-4>(), Constant<-8>() }
        };

        return ret;
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
