// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <cassert>
#include <cstdlib>
#include <limits>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/Advanced/Orientation/StrongType.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
{


    const std::string& Triangle::ClassName()
    {
        static const std::string ret("Triangle");
        return ret;
    }


    /************************************************************************
     *
     *     2
     *     | \
     *     |   \
     *     |     \
     *     |       \
     *     |         \
     *     0-----------1
     *
     *************************************************************************/


    const std::array<Triangle::EdgeContent, Triangle::Nedge>& Triangle::GetEdgeList()
    {
        static const std::array<Triangle::EdgeContent, Triangle::Nedge> ret{
            { { { 0UL, 1UL } }, { { 1UL, 2UL } }, { { 0UL, 2UL } } }
        };

        return ret;
    }


    const std::array<Triangle::FaceContent, Triangle::Nface>& Triangle::GetFaceList()
    {
        static const std::array<Triangle::FaceContent, Triangle::Nface> ret{ { { { 0UL, 1UL, 2UL } } } };

        return ret;
    }


    bool Triangle::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Triangle>(vertex_index, coords);
    }


    bool Triangle::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
    {
        if (!IsInside(coords))
            return false;

        assert(edge_index < 3UL);
        assert(coords.GetDimension() == ::MoReFEM::GeometryNS::dimension_type{ 2 });
        const double r = coords.r();
        const double s = coords.s();

        switch (edge_index)
        {
        case 0UL:
            return NumericNS::IsZero(s);
        case 1UL:
            return NumericNS::AreEqual(1., r + s);
        case 2UL:
            return NumericNS::IsZero(r);
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        assert(false);
        return false;
    }


    bool Triangle::IsOnFace([[maybe_unused]] std::size_t face_index, const LocalCoords& coords)
    {
        assert(face_index == 0UL);
        return IsInside(coords);
    }


    LocalCoords Triangle::TransformFacePoint(const LocalCoords& coords,
                                             [[maybe_unused]] std::size_t face_index,
                                             Advanced::InterfaceNS::orientation_type orientation)
    {
        assert(face_index == 0UL && "Only choice for a 2D triangle...");
        assert(IsOnFace(face_index, coords));
        assert(orientation < Advanced::InterfaceNS::orientation_type{ 6UL });

        const double x_before = coords[::MoReFEM::GeometryNS::dimension_type{}];
        const double y_before = coords[::MoReFEM::GeometryNS::dimension_type{ 1 }];

        assert(!NumericNS::AreEqual(x_before, std::numeric_limits<double>::lowest()));
        assert(!NumericNS::AreEqual(y_before, std::numeric_limits<double>::lowest()));

        double x_after = std::numeric_limits<double>::lowest();
        double y_after = std::numeric_limits<double>::lowest();

        switch (orientation.Get())
        {
        case 0UL:
        {
            x_after = x_before;
            y_after = y_before;
            break;
        }
        case 1UL:
        {
            x_after = 1. - x_before - y_before;
            y_after = x_before;
            break;
        }
        case 2UL:
        {
            x_after = y_before;
            y_after = 1. - x_before - y_before;
            break;
        }
        case 3UL:
        {
            x_after = y_before;
            y_after = x_before;
            break;
        }
        case 4UL:
        {
            x_after = 1. - x_before - y_before;
            y_after = y_before;
            break;
        }
        case 5UL:
        {
            x_after = x_before;
            y_after = 1. - x_before - y_before;
            break;
        }
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        assert(!NumericNS::AreEqual(x_after, std::numeric_limits<double>::lowest()));
        assert(!NumericNS::AreEqual(y_after, std::numeric_limits<double>::lowest()));

        LocalCoords ret({ x_after, y_after });
        return ret;
    }


    const std::vector<LocalCoords>& Triangle::GetQ1LocalCoordsList()
    {
        static const std::vector<LocalCoords> ret{ { 0., 0. }, { 1., 0. }, { 0., 1. } };

        return ret;
    }


    bool Triangle::IsInside(const LocalCoords& coords)
    {
        assert(coords.GetDimension() == ::MoReFEM::GeometryNS::dimension_type{ 2 });

        const double r = coords.r();
        const double s = coords.s();

        if (r < 0. || s < 0.)
            return false;

        if (r + s <= 1.)
            return true;

        return false;
    }


    ::MoReFEM::InterfaceNS::Nature Triangle::GetInteriorInterface()
    {
        return ::MoReFEM::InterfaceNS::Nature::face;
    }


    ::MoReFEM::TopologyNS::Type Triangle::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::triangle;
    }


} // namespace MoReFEM::Advanced::RefGeomEltNS::TopologyNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
