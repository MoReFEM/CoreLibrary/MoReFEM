// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_TRIANGLE6_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_TRIANGLE6_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Geometry/RefGeometricElt/Instances/Triangle/Traits/Triangle6.hpp"
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS
{

    /*!
     * \brief Acts as a strawman class for MoReFEM::Advanced::RefGeomEltNS::Traits::Triangle6.
     *
     * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
     * store in one dynamic container all the kinds of GeometricElt present in a mesh.
     *
     * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
     * can be included in:
     *
     * \code
     * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
     * \endcode
     *
     */
    class Triangle6 final : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Triangle6>
    {
      public:
        //! Constructor.
        Triangle6() = default;

        //! Destructor.
        virtual ~Triangle6() override;

        //! \copydoc doxygen_hide_copy_constructor
        Triangle6(const Triangle6& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Triangle6(Triangle6&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Triangle6& operator=(const Triangle6& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Triangle6& operator=(Triangle6&& rhs) = delete;


      private:
        // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
    };


} // namespace MoReFEM::Advanced::RefGeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_TRIANGLE6_DOT_HPP_
// *** MoReFEM end header guards *** < //
