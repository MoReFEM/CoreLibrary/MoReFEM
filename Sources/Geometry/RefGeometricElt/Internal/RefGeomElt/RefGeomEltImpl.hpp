// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"


namespace MoReFEM::Internal::RefGeomEltNS
{


    /*!
     * \brief Traits class which unifies traits from Interface and from shape functions.
     *
     * \tparam DerivedT RefGeomEltImpl is also to be used as a CRTP.
     * \tparam ShapeFunctionTraitsT Trait class which define the shape functions and their derivatives.
     * These structures are in namespace Advanced::RefGeomEltNS::ShapeFunctionNS and bear the same names as the final
     * class in MoReFEM namespace.
     * \tparam TopologyT Topology considered (one of the class defined within TopologyNS namespace).
     */
    template<class DerivedT,
             class ShapeFunctionTraitsT,
             ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    class RefGeomEltImpl : public ShapeFunctionTraitsT
    {
      public:
        //! Alias over Topology.
        using topology = TopologyT;

        //! Number of shape functions considered.
        static constexpr Eigen::Index NshapeFunction();

        //! Number of coordinates considered (derivates will be performed against each of them).
        static constexpr Eigen::Index Ncoordinates();

        //! \copydoc doxygen_hide_second_derivate_shape_function
        static double SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                  ::MoReFEM::GeometryNS::dimension_type component1,
                                                  ::MoReFEM::GeometryNS::dimension_type component2,
                                                  const LocalCoords& local_coords);

        //! Return the barycenter.
        static const LocalCoords& GetBarycenter();


      private:
        /// \name Special members.
        ///@{

        //! Constructor (protected due to the traits nature of the class).
        RefGeomEltImpl() = default;

        //! Destructor (protected due to the traits nature of the class).
        ~RefGeomEltImpl() = default;

        //! \copydoc doxygen_hide_copy_constructor
        RefGeomEltImpl(const RefGeomEltImpl& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        RefGeomEltImpl(RefGeomEltImpl&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        RefGeomEltImpl& operator=(const RefGeomEltImpl& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RefGeomEltImpl& operator=(RefGeomEltImpl&& rhs) = delete;

        ///@}
    };


    /*!
     * \brief Computes the barycenter of a reference element of \a TopologyT.
     *
     * \return Computed barycenter.
     */
    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    LocalCoords ComputeBarycenter();


} // namespace MoReFEM::Internal::RefGeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HPP_
// *** MoReFEM end header guards *** < //
