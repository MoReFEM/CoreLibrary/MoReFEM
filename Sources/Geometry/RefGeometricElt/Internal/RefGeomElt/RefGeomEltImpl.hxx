// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HXX_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HXX_
// IWYU pragma: private, include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::RefGeomEltNS
{


    template<class DerivedT,
             class ShapeFunctionTraitsT,
             ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    constexpr auto RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::NshapeFunction() -> Eigen::Index
    {
        return static_cast<Eigen::Index>(ShapeFunctionTraitsT::ShapeFunctionList().size());
    }


    template<class DerivedT,
             class ShapeFunctionTraitsT,
             ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    constexpr auto RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::Ncoordinates() -> Eigen::Index
    {
        return static_cast<Eigen::Index>(TopologyT::Nderivate_component_);
    }


    template<::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    LocalCoords ComputeBarycenter()
    {
        const auto& coords_list = TopologyT::GetQ1LocalCoordsList();
        const auto dimension = TopologyT::dimension;

        const double inv_Ncoords = 1. / static_cast<double>(coords_list.size());

        LocalCoords::underlying_type barycenter_coords(dimension.Get());
        barycenter_coords.setZero();

        for (auto i = ::MoReFEM::GeometryNS::dimension_type{ 0 }; i < dimension; ++i)
        {
            double& current_barycenter_coord = barycenter_coords(i.Get());

            for (const auto& coords : coords_list)
                current_barycenter_coord += inv_Ncoords * coords[i];
        }

        return LocalCoords(barycenter_coords);
    }


    template<class DerivedT,
             class ShapeFunctionTraitsT,
             ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    const LocalCoords& RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::GetBarycenter()
    {
        static LocalCoords barycenter = ComputeBarycenter<TopologyT>();
        return barycenter;
    }


    template<class DerivedT,
             class ShapeFunctionTraitsT,
             ::MoReFEM::Advanced::Concept::TopologyNS::TraitsClass TopologyT>
    double RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::SecondDerivateShapeFunction(
        LocalNodeNS::index_type i,
        ::MoReFEM::GeometryNS::dimension_type icoor,
        ::MoReFEM::GeometryNS::dimension_type jcoor,
        const LocalCoords& local_coords)
    {
        auto Ncoor = ShapeFunctionTraitsT::Nderivate_component_;
        assert(i.Get() < NshapeFunction() && icoor < Ncoor && jcoor < Ncoor);

        const auto index = static_cast<std::size_t>(i.Get() * Ncoor.Get() + icoor.Get() * Ncoor.Get() + jcoor.Get());


        return ShapeFunctionTraitsT::SecondDerivateShapeFunctionList()[index](local_coords);
    }


} // namespace MoReFEM::Internal::RefGeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HXX_
// *** MoReFEM end header guards *** < //
