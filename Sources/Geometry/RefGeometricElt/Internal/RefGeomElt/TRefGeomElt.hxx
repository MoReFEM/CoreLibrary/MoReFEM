// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_TREFGEOMELT_DOT_HXX_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_TREFGEOMELT_DOT_HXX_
// IWYU pragma: private, include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM::Internal::RefGeomEltNS
{


    template<class TraitsRefGeomEltT>
    inline Advanced::GeometricEltEnum TRefGeomElt<TraitsRefGeomEltT>::GetIdentifier() const
    {
        return TraitsRefGeomEltT::Identifier();
    }


    template<class TraitsRefGeomEltT>
    inline auto TRefGeomElt<TraitsRefGeomEltT>::GetDimension() const -> ::MoReFEM::GeometryNS::dimension_type
    {
        return TraitsRefGeomEltT::topology::dimension;
    }


    template<class TraitsRefGeomEltT>
    inline const Advanced::GeomEltNS::GenericName& TRefGeomElt<TraitsRefGeomEltT>::GetName() const
    {
        return TraitsRefGeomEltT::ClassName();
    }


    template<class TraitsRefGeomEltT>
    inline const std::string& TRefGeomElt<TraitsRefGeomEltT>::GetTopologyName() const
    {
        return TraitsRefGeomEltT::topology::ClassName();
    }


    template<class TraitsRefGeomEltT>
    inline ::MoReFEM::TopologyNS::Type TRefGeomElt<TraitsRefGeomEltT>::GetTopologyIdentifier() const
    {
        return TraitsRefGeomEltT::topology::GetType();
    }


    template<class TraitsRefGeomEltT>
    inline auto TRefGeomElt<TraitsRefGeomEltT>::Ncoords() const -> ::MoReFEM::GeomEltNS::Nlocal_coords_type
    {
        return TraitsRefGeomEltT::Ncoords;
    }


    template<class TraitsRefGeomEltT>
    const LocalCoords& TRefGeomElt<TraitsRefGeomEltT>::GetBarycenter() const
    {
        return TraitsRefGeomEltT::GetBarycenter();
    }


    template<class TraitsRefGeomEltT>
    const std::vector<LocalCoords>& TRefGeomElt<TraitsRefGeomEltT>::GetQ1LocalCoordsList() const
    {
        return TraitsRefGeomEltT::topology::GetQ1LocalCoordsList();
    }


    template<class TraitsRefGeomEltT>
    inline double TRefGeomElt<TraitsRefGeomEltT>::ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                const LocalCoords& local_coords) const
    {
        return TraitsRefGeomEltT::ShapeFunction(local_node_index, local_coords);
    }


    template<class TraitsRefGeomEltT>
    inline double
    TRefGeomElt<TraitsRefGeomEltT>::FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                               ::MoReFEM::GeometryNS::dimension_type icoor,
                                                               const LocalCoords& local_coords) const
    {
        return TraitsRefGeomEltT::FirstDerivateShapeFunction(local_node_index, icoor, local_coords);
    }


    template<class TraitsRefGeomEltT>
    inline double
    TRefGeomElt<TraitsRefGeomEltT>::SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                ::MoReFEM::GeometryNS::dimension_type icoor,
                                                                ::MoReFEM::GeometryNS::dimension_type jcoor,
                                                                const LocalCoords& local_coords) const
    {
        return TraitsRefGeomEltT::SecondDerivateShapeFunction(local_node_index, icoor, jcoor, local_coords);
    }


    template<class TraitsRefGeomEltT>
    std::size_t TRefGeomElt<TraitsRefGeomEltT>::Nvertex() const noexcept
    {
        return TraitsRefGeomEltT::topology::Nvertex;
    }


    template<class TraitsRefGeomEltT>
    std::size_t TRefGeomElt<TraitsRefGeomEltT>::Nedge() const noexcept
    {
        return TraitsRefGeomEltT::topology::Nedge;
    }


    template<class TraitsRefGeomEltT>
    std::size_t TRefGeomElt<TraitsRefGeomEltT>::Nface() const noexcept
    {
        return TraitsRefGeomEltT::topology::Nface;
    }


    template<class TraitsRefGeomEltT>
    ::MoReFEM::InterfaceNS::Nature TRefGeomElt<TraitsRefGeomEltT>::GetInteriorInterfaceNature() const noexcept
    {
        return TraitsRefGeomEltT::topology::GetInteriorInterface();
    }


    template<class TraitsRefGeomEltT>
    GmfKwdCod TRefGeomElt<TraitsRefGeomEltT>::GetMeditIdentifier() const
    {
        if constexpr (!MeditSupport())
        {
            throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(TraitsRefGeomEltT::ClassName().Get(), "Medit");
        } else
        {
            return Internal::MeshNS::FormatNS ::Support<::MoReFEM::MeshNS::Format::Medit,
                                                        TraitsRefGeomEltT::Identifier()>::MeditId();
        }
    }


    template<class TraitsRefGeomEltT>
    inline const Advanced::GeomEltNS::EnsightName& TRefGeomElt<TraitsRefGeomEltT>::GetEnsightName() const
    {
        if constexpr (!EnsightSupport())
        {
            throw MoReFEM::ExceptionNS::Format::UnsupportedGeometricElt(TraitsRefGeomEltT::ClassName().Get(),
                                                                        "Ensight");
        } else
        {
            static auto ret = Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,
                                                                  TraitsRefGeomEltT::Identifier()>::EnsightName();

            return ret;
        }
    }


} // namespace MoReFEM::Internal::RefGeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_TREFGEOMELT_DOT_HXX_
// *** MoReFEM end header guards *** < //
