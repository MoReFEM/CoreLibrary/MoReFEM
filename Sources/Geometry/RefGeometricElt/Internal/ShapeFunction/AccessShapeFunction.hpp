// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_ACCESSSHAPEFUNCTION_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_ACCESSSHAPEFUNCTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/StrongType.hpp"


namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS::Crtp
{


    /*!
     * \brief Curiously recurrent template pattern (CRTP) that provides two accessors to the value
     * of a shape function and its derivates.
     *
     * \tparam DerivedT Any element of Advanced::RefGeomEltNS::ShapeFunctionNS namespace.
     */
    template<class DerivedT>
    struct AccessShapeFunction
    {

        /*!
         * \copydoc doxygen_hide_shape_function
         */
        static double ShapeFunction(LocalNodeNS::index_type local_node_index, const LocalCoords& local_coords);


        //! \copydoc doxygen_hide_first_derivate_shape_function
        static double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                 ::MoReFEM::GeometryNS::dimension_type component,
                                                 const LocalCoords& local_coords);
    };

} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_ACCESSSHAPEFUNCTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
