// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_ACCESSSHAPEFUNCTION_DOT_HXX_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_ACCESSSHAPEFUNCTION_DOT_HXX_
// IWYU pragma: private, include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS::Crtp
{

    template<class DerivedT>
    double AccessShapeFunction<DerivedT>::ShapeFunction(LocalNodeNS::index_type local_node_index,
                                                        const LocalCoords& local_coords)
    {
        const auto& shape_function_list = DerivedT::ShapeFunctionList();

        const auto index = static_cast<std::size_t>(local_node_index.Get());
        assert(index < shape_function_list.size());

        return shape_function_list[index](local_coords);
    }


    template<class DerivedT>
    double AccessShapeFunction<DerivedT>::FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                                     ::MoReFEM::GeometryNS::dimension_type component,
                                                                     const LocalCoords& local_coords)
    {
        const auto& gradient_shape_function_list = DerivedT::FirstDerivateShapeFunctionList();
        auto Ncoor = DerivedT::Nderivate_component_;

        const auto index = static_cast<std::size_t>(local_node_index.Get() * Ncoor.Get() + component.Get());
        assert(index < gradient_shape_function_list.size());
        return gradient_shape_function_list[index](local_coords);
    }


} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_ACCESSSHAPEFUNCTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
