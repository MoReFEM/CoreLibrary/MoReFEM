// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_CONSTANTSHAPEFUNCTION_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_CONSTANTSHAPEFUNCTION_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <functional>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS
{


    /*!
     * \brief This function is used to define a ShapeFunctionType that returns a (rational) constant.
     *
     * It doesn't work with irrational values due to the template nature of the instantiation.
     *
     * \return Function that returns the same integer or rational value for all spatial positions.
     */
    template<int NumT, int DenomT = 1>
    static const ShapeFunctionType Constant()
    {

        return [](const LocalCoords&) -> double
        {
            return static_cast<double>(NumT) / static_cast<double>(DenomT);
        };
    }


} // namespace MoReFEM::Advanced::RefGeomEltNS::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_SHAPEFUNCTION_CONSTANTSHAPEFUNCTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
