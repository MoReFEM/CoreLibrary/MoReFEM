// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_REFGEOMELT_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_REFGEOMELT_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/LocalCoords.hpp"                     // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp" // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/EnumTopology.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/StrongType.hpp"   // IWYU pragma: export
#include "Geometry/StrongType.hpp"
#include "Geometry/StrongType.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Polymorphic class which can access static functions related to shape functions, interface and topology.
     *
     * This class is used to hold polymorphically the generic information related to a geometric element (shape
     * functions, interface and topology).
     *
     * It can be used for instance to store in a small vector the list of all the kinds of GeometricElt
     * met in the mesh.
     *
     * The equivalent that also holds specific data (Coords involved for instance) is GeometricElt.
     *
     * \internal <b><tt>[internal]</tt></b> RefGeomElt pure virtual methods are all defined in derived class
     * TRefGeomElt. Currently only the static functions required by the current state of the code are implemented, but
     * many others could have been and aren't. It is not very difficult to do it when necessary: the principle is always
     * to call the traits class which defines the static function (take any existing one to understand how it works).
     * \endinternal
     *
     */
    class RefGeomElt
    {
      public:
        //! Alias for shared pointer.
        using shared_ptr = std::shared_ptr<const RefGeomElt>;

        //! Alias for unique pointer.
        using const_unique_ptr = std::unique_ptr<const RefGeomElt>;

        //! Alias for vector of pointers
        using vector_shared_ptr = std::vector<shared_ptr>;


        /// \name Special members.
        ///@{

      protected:
        //! Default constructor.
        RefGeomElt() = default;

        //! Destructor.
        virtual ~RefGeomElt();

        //! \copydoc doxygen_hide_copy_constructor
        RefGeomElt(const RefGeomElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        RefGeomElt(RefGeomElt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        RefGeomElt& operator=(const RefGeomElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RefGeomElt& operator=(RefGeomElt&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Get the identifier of the geometric element.
         *
         * \return The identifier of a GeometricElt as defined within MoReFEM (independent of IO format).
         */
        virtual Advanced::GeometricEltEnum GetIdentifier() const = 0;

        /*!
         * \brief Get the number of Coords object required to characterize completely a GeometricElt of this type.
         *
         * For instance 27 for an Hexahedron27.
         *
         * \return Number of Coords object required to characterize completely a GeometricElt of this type.
         */
        virtual GeomEltNS::Nlocal_coords_type Ncoords() const = 0;

        //! Get the dimension of the geometric element.
        virtual GeometryNS::dimension_type GetDimension() const = 0;

        /*!
         * \brief Get the name associated to the element (e.g. 'Triangle3').
         *
         * This name is guaranteed to be unique
         * \return Name associated to the element (e.g. 'Triangle3').
         */
        virtual const Advanced::GeomEltNS::GenericName& GetName() const = 0;

        //! Get the name associated to the Topology (e.g. 'Triangle').
        virtual const std::string& GetTopologyName() const = 0;

        //! Get the enum value associated to the Topology (e.g. 'TopologyNS::Type::tetrahedron').
        virtual TopologyNS::Type GetTopologyIdentifier() const = 0;

        //! Get the local coordinates of the barycenter.
        virtual const LocalCoords& GetBarycenter() const = 0;

        //! Get the list of local coordinates of the vertices.
        virtual const std::vector<LocalCoords>& GetQ1LocalCoordsList() const = 0;

        //! Return the number of vertices.
        virtual std::size_t Nvertex() const noexcept = 0;

        //! Return the number of edges.
        virtual std::size_t Nedge() const noexcept = 0;

        //! Return the number of faces.
        virtual std::size_t Nface() const noexcept = 0;

        //! Returns the nature of the interior interface.
        virtual ::MoReFEM::InterfaceNS::Nature GetInteriorInterfaceNature() const noexcept = 0;

        /*!
         * \brief Get the identifier Medit use to tag the geometric element.
         *
         * An exception is thrown if Medit format is not supported.
         *
         * \return Identifier.
         */
        virtual GmfKwdCod GetMeditIdentifier() const = 0;

        //! Get the Ensight name. If Ensight doesn't support the type empty string is returned.
        virtual const Advanced::GeomEltNS::EnsightName& GetEnsightName() const = 0;


      public:
        /// \name Shape function methods.
        ///@{

        //! \copydoc doxygen_hide_shape_function
        virtual double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                     const LocalCoords& local_coords) const = 0;

        //! \copydoc doxygen_hide_first_derivate_shape_function
        virtual double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                  ::MoReFEM::GeometryNS::dimension_type component,
                                                  const LocalCoords& local_coords) const = 0;

        //! \copydoc doxygen_hide_second_derivate_shape_function
        virtual double SecondDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                   ::MoReFEM::GeometryNS::dimension_type component1,
                                                   ::MoReFEM::GeometryNS::dimension_type component2,
                                                   const LocalCoords& local_coords) const = 0;

        ///@}


      private:
        // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
    };


    /*!
     *
     * \copydoc doxygen_hide_operator_equal
     * The comparison is performed with underlying GeometricEltEnum.
     */
    bool operator==(const RefGeomElt& lhs, const RefGeomElt& rhs);


    /*!
     *
     * \copydoc doxygen_hide_operator_not_equal
     * The comparison is performed with underlying GeometricEltEnum.
     */
    bool operator!=(const RefGeomElt& lhs, const RefGeomElt& rhs);


    /*!
     * \copydoc doxygen_hide_operator_less
     *
     * The comparison is performed with underlying GeometricEltEnum.
     *
     */
    bool operator<(const RefGeomElt& lhs, const RefGeomElt& rhs);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/RefGeometricElt/RefGeomElt.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_REFGEOMELT_DOT_HPP_
// *** MoReFEM end header guards *** < //
