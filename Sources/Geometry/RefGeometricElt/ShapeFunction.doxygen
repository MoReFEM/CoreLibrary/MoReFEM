/*!
 *
 * \class doxygen_hide_shape_function
 *
 * \brief Shape function call.
 *
 * \param[in] local_node_index Index of the local node considered.
 * \param[in] local_coords Local coordinates at which the shape function is applied.
 *
 * \return Shape function value at the local node considered.
 */


/*!
 *
 * \class doxygen_hide_first_derivate_shape_function
 *
 * \brief First derivate of the shape function call.
 *
 * \param[in] local_node_index Index of the local node considered.
 * \param[in] local_coords Local coordinates at which the shape function is applied.
 * \param[in] component Component of the derivate considered.
 *
 * \return Derivate of the shape function wrt to \a component at the local node considered.
 */


/*!
 *
 * \class doxygen_hide_second_derivate_shape_function
 *
 * \brief Second derivate of the shape function call.
 *
 * \param[in] local_node_index Index of the local node considered.
 * \param[in] local_coords Local coordinates at which the shape function is applied.
 * \param[in] component1 Component of the first derivate considered.
 * \param[in] component2 Component of the second derivate considered.
 *
 * \return Derivate of the shape function wrt to \a component1 and \a component2 at the local node considered.
 */



/*!
 * \class doxygen_hide_shape_function_order_method 
 *
 * \brief Constant accessor to the order of the finite element. e.g. 1 for P1 or 2 for Q2c.
 *
 * \return Order of the finite element. e.g. 1 for P1 or 2 for Q2c.
 */

/*!
 * \class doxygen_hide_shape_function_instance_enum
 *
 * \brief Convenient enum which encompass magic numbers for the type of geometric element implemented.
 */ 