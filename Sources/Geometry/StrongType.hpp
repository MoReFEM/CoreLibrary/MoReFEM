// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_STRONGTYPE_DOT_HPP_
#define MOREFEM_GEOMETRY_STRONGTYPE_DOT_HPP_
// *** MoReFEM header guards *** < //


// IWYU pragma: begin_exports
#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::GeometryNS
{


    /*!
     * \brief Strong type that gives the dimension of a \a GeometricElt.
     *
     * \copydetails doxygen_hide_strong_type_quick_explanation
     */
    // clang-format off
    using dimension_type =
        StrongType
        <
            Eigen::Index,
            struct dimension_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Addable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible,
            StrongTypeNS::Printable
        >;
    // clang-format on


} // namespace MoReFEM::GeometryNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_STRONGTYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
