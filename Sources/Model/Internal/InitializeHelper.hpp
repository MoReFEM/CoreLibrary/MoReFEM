// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_INTERNAL_INITIALIZEHELPER_DOT_HPP_
#define MOREFEM_MODEL_INTERNAL_INITIALIZEHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <optional>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp" // IWYU pragma: keep
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"


namespace MoReFEM::Internal::ModelNS
{


    /*!
     * \brief Init most of the singleton managers, using the data from the input data file,
     *
     * \internal This is expected to be used only  for some tests; for \a Model all the managers are actually dealt with in the  `Initialize()` method.
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void InitMostSingletonManager(const MoReFEMDataT& morefem_data);


} // namespace MoReFEM::Internal::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Model/Internal/InitializeHelper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_INTERNAL_INITIALIZEHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
