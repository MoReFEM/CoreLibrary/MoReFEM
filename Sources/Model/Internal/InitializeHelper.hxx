// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_INTERNAL_INITIALIZEHELPER_DOT_HXX_
#define MOREFEM_MODEL_INTERNAL_INITIALIZEHELPER_DOT_HXX_
// IWYU pragma: private, include "Model/Internal/InitializeHelper.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <optional>

#include "Utilities/InputData/Internal/EmptyInputData.hpp"
#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"


namespace MoReFEM::Internal::ModelNS
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void InitMostSingletonManager(const MoReFEMDataT& morefem_data)
    {
        decltype(auto) input_data = morefem_data.GetInputData();
        decltype(auto) mpi = morefem_data.GetMpi();
        decltype(auto) model_settings = morefem_data.GetModelSettings();

        {
            auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = UnknownManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = DomainManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = Advanced::LightweightDomainListManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = DirichletBoundaryConditionManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = Internal::MeshNS::CoordsMatchingManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = GodOfDofManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager, mpi);
        }
    }


} // namespace MoReFEM::Internal::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_INTERNAL_INITIALIZEHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
