// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_MAIN_INTERNAL_COMMANDLINEARGUMENTSFORUPDATELUAFILE_DOT_HPP_
#define MOREFEM_MODEL_MAIN_INTERNAL_COMMANDLINEARGUMENTSFORUPDATELUAFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::InputDataNS { enum class verbosity; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::ModelNS
{


    /*!
     * \brief Supplementary command line options to use in the \a MoReFEMData policy for executables that update
     * Lua files.
     */
    struct UpdateLuaFileCLI
    {

        /*!
         * \brief Required method for the policy to add the new command line options.
         *
         * \param[in,out] command TCLAP object in charge of the command line options.
         */
        void Add(TCLAP::CmdLine& command);

        /*!
         * \brief Returns true if the Lua file should be rewritten in a more compact way,
         * with no comments, description, etc...
         *
         * \return True if comments are not to be written when updating the Lua file.
         */
        InputDataNS::verbosity DoSkipComments() const;

      private:
        //! TCLAP object which hold data about the flag skip-comment.
        std::unique_ptr<TCLAP::SwitchArg> skip_comment_arg_{ nullptr };
    };


} // namespace MoReFEM::Internal::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_MAIN_INTERNAL_COMMANDLINEARGUMENTSFORUPDATELUAFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
