// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_MAIN_MAIN_DOT_HPP_
#define MOREFEM_MODEL_MAIN_MAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstdlib>

#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export
#include "Core/Parameter/TypeEnum.hpp"      // IWYU pragma: export


namespace MoReFEM::ModelNS
{


    /*!
     * \brief The typical main for a model; its main() function should look like:
     *
     \code
     int main(int argc, char** argv)
     {
         return ModelNS::Main<**model class**>(argc, argv);
     }
     \endcode
     *
     * \tparam ModelT The \a Model class considered.
     *
     * \param[in] argc The first argument from main() function.
     * \param[in] argv The second argument from main() function.
     *
     * \return The error code for the main.
     */
    // clang-format off
    template
    <
        class ModelT
    >
    // clang-format on
    int Main(int argc, char** argv);


} // namespace MoReFEM::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Model/Main/Main.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_MAIN_MAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
