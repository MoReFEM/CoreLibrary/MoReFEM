// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_MAIN_MAIN_DOT_HXX_
#define MOREFEM_MODEL_MAIN_MAIN_DOT_HXX_
// IWYU pragma: private, include "Model/Main/Main.hpp"
// *** MoReFEM header guards *** < //


#include <map>           // IWYU pragma: export
#include <unordered_map> // IWYU pragma: export
#include <vector>        // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/InputData.hpp"
#include "Utilities/InputData/Internal/Write/Write.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::ModelNS
{


    template<class ModelT>
    int Main(int argc, char** argv)
    {
        try
        {
            typename ModelT::morefem_data_type morefem_data(argc, argv);

            const auto& input_data = morefem_data.GetInputData();
            const auto& mpi = morefem_data.GetMpi();

            try
            {
                ModelT model(morefem_data);
                model.Run();

                input_data.PrintUnusedLeafs(std::cout, mpi);
            }
            catch (const InputDataNS::ExceptionNS::MissingIndexedSectionDescriptionInModelSettingsTuple& e)
            {
                std::ostringstream oconv;
                oconv << "Exception caught from MoReFEMData: " << e.what() << std::endl;

                std::cout << oconv.str();
                return EXIT_FAILURE;
            }
            catch (const std::exception& e)
            {
                ExceptionNS::PrintAndAbort(mpi, e.what());
            }
        }
        catch (const ::MoReFEM::ExceptionNS::MoReFEMDataNS::NonExistingLuaFile& e)
        {
            typename ModelT::morefem_data_type::model_settings_type model_settings;
            model_settings.Init();
            model_settings.CheckTupleCompletelyFilled();

            // clang-format off
            Internal::InputDataNS::CreateDefaultInputFile
            <
            typename ModelT::morefem_data_type::input_data_type
            >(e.GetInputDataFile(), model_settings);
            // clang-format on

            std::cout << "Exception caught from MoReFEMData: " << e.what() << std::endl;
            std::cout << e.GetInputDataFile()
                      << " wasn't existing and has just been created on root processor; "
                         "please edit it and then copy it onto all machines intended to run the code in parallel."
                      << std::endl;

            return EXIT_SUCCESS;
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught from MoReFEMData: " << e.what() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData - status " << e.getExitStatus() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
#ifndef NDEBUG
        catch (const Advanced::Assertion& e)
        {
            std::ostringstream oconv;
            oconv << "MoReFEM Assertion caught: " << e.what() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
#endif // NDEBUG
        catch (...)
        {
            assert(false
                   && "If this happens please open an issue: all possible exceptions"
                      "should be caught above with possibility to access `what()` method.");
            exit(EXIT_FAILURE);
        }

        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_MAIN_MAIN_DOT_HXX_
// *** MoReFEM end header guards *** < //
