// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_MAIN_MAINUPDATELUAFILE_DOT_HXX_
#define MOREFEM_MODEL_MAIN_MAINUPDATELUAFILE_DOT_HXX_
// IWYU pragma: private, include "Model/Main/MainUpdateLuaFile.hpp"
// *** MoReFEM header guards *** < //


// IWYU pragma: begin_exports
#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Model/Main/Internal/CommandLineArgumentsForUpdateLuaFile.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::ModelNS
{


    template<class ModelT>
    int MainUpdateLuaFile(int argc, char** argv)
    {
        try
        {
            // We're not using the model input_data_type directly as we want to tolerate missing fields in the Lua file
            // (e.g. if a new one was introduced in the tuple after the first version of the Lua file was generated).
            using tuple = typename ModelT::morefem_data_type::input_data_type::underlying_tuple_type;
            using input_data_type = MoReFEM::InputData<tuple, ::MoReFEM::InputDataNS::do_update_lua_file::yes>;

            // clang-format off
            using morefem_data_type =
                MoReFEMData
                <
                    typename  ModelT::morefem_data_type::model_settings_type,
                    input_data_type,
                    typename  ModelT::morefem_data_type::time_manager_type,
                    program_type::update_lua_file,
                    InputDataNS::DoTrackUnusedFields::no,
                    Internal::ModelNS::UpdateLuaFileCLI
                >;
            // clang-format on

            morefem_data_type morefem_data(argc, argv);

            Internal::InputDataNS::RewriteInputDataFile(
                morefem_data.GetModelSettings(), morefem_data.GetInputData(), morefem_data.DoSkipComments());
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught: " << e.what() << std::endl;

            std::cerr << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData - status " << e.getExitStatus() << std::endl;

            std::cerr << oconv.str();
            return EXIT_FAILURE;
        }
#ifndef NDEBUG
        catch (const Advanced::Assertion& e)
        {
            std::ostringstream oconv;
            oconv << "MoReFEM Assertion caught: " << e.what() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
#endif // NDEBUG
        catch (...)
        {
            assert(false
                   && "If this happens please open an issue: all possible exceptions"
                      "should be caught above with possibility to access `what()` method.");
            exit(EXIT_FAILURE);
        }

        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_MAIN_MAINUPDATELUAFILE_DOT_HXX_
// *** MoReFEM end header guards *** < //
