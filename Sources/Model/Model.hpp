// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_MODEL_DOT_HPP_
#define MOREFEM_MODEL_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <map>
#include <optional>
#include <string>
#include <vector>

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/Wrappers/Petsc/Print.hpp"                    // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/InputData/Instances/TimeManager/Restart.hpp" // IWYU pragma: export
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Extract.hpp"     // IWYU pragma: export
#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"               // IWYU pragma: export
#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp" // IWYU pragma: export
#include "Core/TimeManager/Policy/Evolution/VariableTimeStep.hpp" // IWYU pragma: export
#include "Core/TimeManager/TimeManager.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/DomainManager.hpp" // IWYU pragma: export
#include "Geometry/Mesh/Internal/CreateMeshDataDirectory.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Internal/PseudoNormalsManager.hpp"
#include "Geometry/Mesh/Mesh.hpp" // IWYU pragma: export

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: export
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/Unknown/Unknown.hpp" // IWYU pragma: export

#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "FormulationSolver/VariationalFormulation.hpp" // IWYU pragma: export

#include "Model/Internal/InitializeHelper.hpp"


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_do_create_output_dir_arg
     *
     * \param[in] do_create_output_dir If yes, create the new output directory (and remove any preexisting one).
     * This is the expected setting for a Model. 'no' is mostly useful for PostProcessing purposes: Model classes
     * defined in PostProcessing usually use the same input data file as the Model they cover but we do not
     * want to erase the results of the computation...
     */


    //! Enum class to say whether banners are printed or not.
    enum class print_banner { yes, no };


    /*!
     * \brief Class that drives the finite element resolution.
     *
     * A main can consist only on the instantiation of a Model and a call to its Run() method.
     *
     * \tparam DerivedT Model is used as a CRTP class.
     * \tparam DoConsiderProcessorWiseLocal2GlobalT Whether the local-2-global for processor-wise data might be required
     * by at least one of the operator. In some models processor-wise is never considered (most linear models); in that
     * case it's irrelevant to use up loads of memory to store them so 'no'should be given.
     * \tparam TimeManagerPolicyT The time manager policy to use. Currently three options are open:
     * - TimeManagerNS::Policy::ConstantTimeStep
     * - TimeManagerNS::Policy::VariableTimeStep
     * - TimeManagerNS::Policy::None when no time variation occurs (rather useful for tests)
     *
     * DerivedT is expected to define the following methods (otherwise the compilation will fail):
     * - void InitializeStep()
     * - void Forward();
     * - void SupplFinalizeStep()
     * - void SupplFinalize()
     * - void SupplInitialize();
     * - bool SupplHasFinishedConditions() const;
     *
     * In the case TimeManagerNS::Policy::None policy is chosen, only SupplInitialize() and SupplFinalize() are
     * mandatory.
     *
     */
    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    class Model : public Crtp::CrtpMpi<Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>;

        //! Alias to MoReFEMDataT.
        using morefem_data_type = MoReFEMDataT;

        //! Alias to the object in charge of time management.
        using time_manager_type = typename morefem_data_type::time_manager_type;


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * Model drives most of MoReFEM; so its constructor is responsible for a lot the initialisation
         * steps (for instance VariationalFormulation or dof numbering are fully built there).
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         *
         * \param[in] a_create_domain_list_for_coords Whether the model will compute the list of domains a coord is in
         * or not.
         *
         * \param[in] do_print_banner If True, print text at the beginning and the end of the program. False has
         * been introduced only for the integration tests.
         */
        explicit Model(
            morefem_data_type& morefem_data,
            create_domain_list_for_coords a_create_domain_list_for_coords = create_domain_list_for_coords::no,
            print_banner do_print_banner = print_banner::yes);


        //! Destructor.
        ~Model();

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor - deactivated.
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /*!
         * \brief Whether the model has finished all its dynamic iterations.
         *
         * \internal <b><tt>[internal]</tt></b> Part of its implementation is the responsibility of DerivedT: the base
         * class just checks whether maximum time has been hit, but it is possible for some model to add additional
         * criteria through SupplHasFinishedConditions().
         * \endinternal
         *
         * \return True if all time iterations are done.
         */
        bool HasFinished();


        /*!
         * \brief Initialize the model.
         *
         * Base class only creates the GodOfDof and finite element spaces; additional functionalities
         * must be implemented in DerivedT::SupplInitialize() (this method must be defined even
         * in cases its content is null).
         *
         */
        void Initialize();


        /*!
         * \brief Initialize the dynamic step.
         *
         * Base class only print information about time; additional functionalities
         * must be implemented in DerivedT::SupplInitializeStep() (this method must be defined even
         * in cases its content is null).
         *
         */
        void InitializeStep();


        /*!
         * \brief Finalize the dynamic step.
         *
         * Base class only asks the TimeManager object to update the time; additional functionalities
         * must be implemented in DerivedT::SupplFinalizeStep() (this method must be defined even
         * in cases its content is null).
         *
         */
        void FinalizeStep();


        /*!
         * \brief Finalize the model once the time loop is done.
         *
         *
         */
        void Finalize();


        /*!
         * \brief A convenient wrapper over the usual way a Model is used.
         *
         * A main could be just the instantiation of a Model and subsequently the call to this method.
         *
         * This method is equivalent to what is usually in a main for Verdandi:
         *
         * \code

         * model.Initialize();
         *
         * while (!HasFinished())
         * {
         *     model.InitializeStep();
         *     model.Forward();
         *     model.SupplFinalizeStep();
         * }
         *
         * model.Finalize();
         *
         * \endcode
         *
         */
        void Run();

        //! Access to the object in charge of transient data.
        const time_manager_type& GetTimeManager() const noexcept;

        /*!
         * \brief Access to output directory.
         *
         */
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

        //! Setter on do_print_new_time_iteration_banner_.
        //! \param[in] do_print True if you want to print the banner.
        void SetDoPrintNewTimeIterationBanner(bool do_print) noexcept;

        //! Set do_clear_god_of_dof_temporary_data_after_initialize_ to false.
        void SetClearGodOfDofTemporaryDataToFalse();

        //! Access a GodOfDof object using its unique id.
        //! \unique_id_param_in_accessor{GodOfDof}
        const GodOfDof& GetGodOfDof(::MoReFEM::MeshNS::unique_id unique_id) const;


        /*!
         * \brief Accessor to the underlying \a MoReFEMData object.
         *
         * \return Constant reference to the \a MoReFEMData object.
         */
        const morefem_data_type& GetMoReFEMData() const noexcept;


        //! \non_cst_accessor{Mesh which unique identifier is \a unique_id}
        //! \unique_id_param_in_accessor{Mesh}.
        const Mesh& GetMesh(::MoReFEM::MeshNS::unique_id unique_id) const;


        /*!
         * \copydetails doxygen_hide_do_write_restart_data
         *
         * \return True if such restart data should be written.
         *
         * This is for an \a Advanced user; you may have a look at Elastic model in the example to see
         * how it is used.
         */
        bool DoWriteRestartData() const noexcept;


      protected:
        //! \accessor{Mesh which unique identifier is \a unique_id}
        //! \unique_id_param_in_accessor{Mesh}.
        Mesh& GetNonCstMesh(::MoReFEM::MeshNS::unique_id unique_id) const;


        //! Files will be written every \a GetDisplayValue() time iterations.
        std::size_t GetDisplayValue() const;

        //! Non constant access to the object in charge of transient data.
        time_manager_type& GetNonCstTimeManager() noexcept;

        /*!
         * \brief Accessor to the underlying \a MoReFEMData object.
         *
         * \return Constant reference to the \a MoReFEMData object.
         */
        morefem_data_type& GetNonCstMoReFEMData() noexcept;


      private:
        /*!
         * \brief Go the the next iteration step.
         *
         */
        void UpdateTime();

        //! Print a banner with text, the time and the iteration number
        void PrintNewTimeIterationBanner() const;

        //! Constant accessor to create_domain_list_for_coords_.
        create_domain_list_for_coords GetCreateDomainListForCoords() const noexcept;

        //! Coords will get the information of which domain they are in.
        void CreateDomainListForCoords();

        /*!
         * \brief Whether banners should be printed or not.
         *
         * \return Boolean...
         */
        bool DoPrintBanner() const noexcept;


      private:
        //! Reference to the \a MoReFEMData object.
        morefem_data_type& morefem_data_;

        /*!
         * \brief Files will be written every \a display_value_ time iteration. Choose 1 to write all of them.
         *
         * May be `std::nullopt` for 'models' related to some of the tests.
         */
        std::optional<std::size_t> display_value_{ std::nullopt };

        /*!
         * \brief Enables to postpone the ClearGodOfDofTemporaryData to be able to create operators after the
         * initialize.
         *
         * \attention If set to false you will need to call ClearGodOfDofTemporaryData manually once you have initialize
         * all your operators. When writing a simple MoReFEM model one should not set this attribute to false it is
         * designed to be used when interfacong with another code.
         */
        bool do_clear_god_of_dof_temporary_data_after_initialize_ = true;

        /*!
         * \brief If false, the banner for each new time iteration won't be printed.
         *
         * Even if true it is second to \a do_print_banner_.
         */
        bool do_print_new_time_iteration_banner_ = true;

        //! If false, no banner at all will be printed. False is useful for test framework.
        print_banner do_print_banner_ = print_banner::yes;

#ifndef NDEBUG
        //! In debug mode, data attribute to check \a Initialize is not called more than one for a given \a Model
        //! object.
        bool was_initialize_already_called_{ false };
#endif // NDEBUG

        //! \copydetails doxygen_hide_do_write_restart_data
        bool do_write_restart_data_{ false };
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Model/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
