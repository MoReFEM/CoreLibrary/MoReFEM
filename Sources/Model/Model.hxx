// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_MODEL_DOT_HXX_
#define MOREFEM_MODEL_MODEL_DOT_HXX_
// IWYU pragma: private, include "Model/Model.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <ctime>
#include <filesystem>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Extract.hpp"
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Instances.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"
#include "Geometry/Mesh/Internal/CreateMeshDataDirectory.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "Model/Internal/InitializeHelper.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM
{

    namespace FilesystemNS { class Directory; }
    namespace InputDataNS { struct TimeManager; }

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //
// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::Model(
        morefem_data_type& morefem_data,
        create_domain_list_for_coords a_create_domain_list_for_coords,
        print_banner do_print_banner)
    : Crtp::CrtpMpi<Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>>(morefem_data.GetMpi()),
      morefem_data_(morefem_data), do_print_banner_(do_print_banner)
    {
        if (a_create_domain_list_for_coords == create_domain_list_for_coords::yes)
            Coords::SetCreateDomainListForCoords();

        // current date and time system
        time_t now = std::time(nullptr);
        char* date_time = ctime(&now);

        if constexpr (!std::is_same<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>())
        {
            using Result = InputDataNS::Result;

            if constexpr (!std::is_same<time_manager_type, TimeManagerNS::Instance::Static>())
            {
                static_assert(MoReFEMDataT::input_data_type::template Find<InputDataNS::TimeManager>()
                                  || MoReFEMDataT::model_settings_type::template Find<InputDataNS::TimeManager>(),
                              "A model assumes a field TimeManager is present in InputData or ModelSettings tuple.");
            }

            if constexpr (MoReFEMDataT::time_manager_type::do_support_restart_mode
                          == TimeManagerNS::support_restart_mode::yes)
                GetNonCstTimeManager().SetRestartIfRelevant();

            if constexpr (std::is_same<time_manager_type, TimeManagerNS::Instance::Static>())
                display_value_ = 0.;
            else
                display_value_ = ::MoReFEM::InputDataNS::ExtractLeaf<Result::DisplayValue>(morefem_data);

        } else
        {
            static_assert(!MoReFEMDataT::input_data_type::template Find<InputDataNS::TimeManager>(),
                          "If TimeManager is in the input data tuple you can't choose 'TimeManagerNS::Policy::None'"
                          "policy.");
        }

        decltype(auto) mpi = this->GetMpi();

        // For most tests written after July 2023, there are no `InputData` to consider and a more
        // limited `MoReFEMData` object is used instead.
        if constexpr (MoReFEMDataT::ConceptIsMoReFEMData == is_morefem_data::yes)
        {
            decltype(auto) input_data = morefem_data.GetInputData();

            if (mpi.IsRootProcessor())
            {
                decltype(auto) output_directory = GetOutputDirectory();

                if constexpr (!std::is_same_v<typename MoReFEMDataT::input_data_type,
                                              MoReFEM::Internal::InputDataNS::EmptyInputData>)
                {
                    auto target_file = output_directory.AddFile("input_data.lua");

                    FilesystemNS::File input_file{ input_data.GetInputFile() };

                    FilesystemNS::Copy(
                        input_file, target_file, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::yes);
                }

                // Specify in output directory how many processors are involved.

                {
                    auto target_file = output_directory.AddFile("mpi.hhdata");

                    std::ofstream out{ target_file.NewContent() };

                    out << "Nprocessor: " << mpi.template Nprocessor<int>() << std::endl;
                }

                // Specify the name of the model. It might be useful as same Lua file might be used with different
                // models (e.g. different time scheme or hyperelastic law for hyperelastic model).
                {
                    auto target_file = output_directory.AddFile("model_name.hhdata");

                    std::ofstream out{ target_file.NewContent() };

                    out << DerivedT::ClassName() << std::endl;
                }
            }
        }

        if (DoPrintBanner())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n================================================================\n", mpi);

            std::ostringstream oconv;
            oconv << "MoReFEM ";
            oconv << 8 * sizeof(void*);
            oconv << " bits ";
            Wrappers::Petsc::PrintMessageOnFirstProcessor(oconv.str().c_str(), mpi);

            if (mpi.template Nprocessor<int>() == 1)
                Wrappers::Petsc::PrintMessageOnFirstProcessor("on 1 processor \n", mpi);
            else
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "on %d processors \n", mpi, std::source_location::current(), mpi.template Nprocessor<int>());

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "%s=================================================================\n",
                mpi,
                std::source_location::current(),
                date_time);
        }

        if constexpr (InputDataNS::Find<InputDataNS::Restart::DoWriteRestartData, MoReFEMDataT>())
        {
            do_write_restart_data_ =
                ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Restart::DoWriteRestartData>(morefem_data);
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::~Model()
    {
        const auto& mpi = this->GetMpi();

        if (DoPrintBanner())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\nIf no exception, all results have been printed in %s.\n",
                                                          mpi,
                                                          std::source_location::current(),
                                                          GetOutputDirectory().GetPath().c_str());

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n==============================================================\n", mpi);
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "MoReFEM %s ended (if exception thrown it will appear afterwards).\n",
                mpi,
                std::source_location::current(),
                DerivedT::ClassName().c_str());

            // current date and time system
            time_t now = time(nullptr);
            char* date_time = ctime(&now);

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "%s==============================================================\n",
                mpi,
                std::source_location::current(),
                date_time);
        }


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
        if (mpi.template Nprocessor<int>() > 1 && mpi.IsRootProcessor())
            ::MoReFEM::Internal::Wrappers::Petsc::CheckUpdateGhostManager::GetInstance().Print();
#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    bool Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::HasFinished()
    {
        auto& time_manager = GetNonCstTimeManager();

        if (time_manager.HasFinished())
            return true;

        if (static_cast<const DerivedT&>(*this).SupplHasFinishedConditions())
            return true;

        return false;
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::Initialize()
    {
#ifndef NDEBUG
        {
            assert(was_initialize_already_called_ == false && "Model::Initialize() must be called only once!");
            was_initialize_already_called_ = true;
        }
#endif // NDEBUG

        const auto& mpi = this->GetMpi();

        decltype(auto) morefem_data = this->GetMoReFEMData();
        decltype(auto) input_data = morefem_data.GetInputData();

        decltype(auto) model_settings = morefem_data.GetModelSettings();

        const bool is_root_processor = mpi.IsRootProcessor();

        const auto parallelism = morefem_data.GetParallelismPtr();

        // Init all the relevant objects found in the input data file.
        // Ordering is important here: do not modify it lightly!

        {
            auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();

            if (!(!parallelism)
                && parallelism->GetParallelismStrategy() == Advanced::parallelism_strategy::run_from_preprocessed)
                Advanced::SetFromPreprocessedData<>(morefem_data, manager);
            else
                Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        auto mesh_directory_storage = Internal::MeshNS::CreateMeshDataDirectory(GetOutputDirectory());
        mpi.Barrier();

        {
            auto& manager = DomainManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = Internal::MeshNS::CoordsMatchingManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }


        {
            auto& manager = Advanced::LightweightDomainListManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        if (Coords::GetCreateDomainListForCoords() == create_domain_list_for_coords::yes)
            CreateDomainListForCoords();

        // Advanced::SetFromInputData<Internal::PseudoNormalsManager>(input_data); #938 not activated for the moment.
        {
            auto& manager = UnknownManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = DirichletBoundaryConditionManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }


        // Write the list of unknowns.
        // #289 We assume here there is one model, as the target file gets a hardcoded name...
        if (is_root_processor)
            WriteUnknownList(GetOutputDirectory());

        {
            auto& manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = GodOfDofManager::CreateOrGetInstance();
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager, mpi);
        }

        Internal::GodOfDofNS::InitAllGodOfDof(
            morefem_data, model_settings, DoConsiderProcessorWiseLocal2GlobalT, std::move(mesh_directory_storage));

#ifndef NDEBUG
        {
            AssertNCoordsConsistency(mpi);
        }
#endif // NDEBUG

        Internal::MeshNS::WriteInterfaceListForEachMesh(mesh_directory_storage);

        if constexpr (!std::is_same<time_manager_type, TimeManagerNS::Policy::None>())
        {
            // As FiberListManager gets a constructor with arguments, call it explicitly there.
            decltype(auto) time_manager = morefem_data.GetTimeManager();

            {
                auto& manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                              ParameterNS::Type::scalar,
                                              time_manager_type>::CreateOrGetInstance(std::source_location::current(),
                                                                                      time_manager);
                Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
            }

            {
                auto& manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                              ParameterNS::Type::scalar,
                                              time_manager_type>::CreateOrGetInstance(std::source_location::current(),
                                                                                      time_manager);
                Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
            }

            {
                auto& manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                              ParameterNS::Type::vector,
                                              time_manager_type>::CreateOrGetInstance(std::source_location::current(),
                                                                                      time_manager);
                Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
            }

            {
                auto& manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                              ParameterNS::Type::vector,
                                              time_manager_type>::CreateOrGetInstance(std::source_location::current(),
                                                                                      time_manager);
                Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
            }
        }

        // If the point was just to write preprocessed data; stop the program here.
        // I put this before the eventual additional steps required by the Model: all the partition is complete
        // at this point so there should be no need to run SupplInitialize() first.
        // In the case I'm wrong, the call may be put later but in this case in derived model one should be put inside
        // SupplInitialize(): we do not want for instance initialization step such as the computing of the static
        // case to occur.
        PrecomputeExit(morefem_data);

        static_cast<DerivedT&>(*this).SupplInitialize();

        if (do_clear_god_of_dof_temporary_data_after_initialize_)
            ClearGodOfDofTemporaryData();

        ClearAllBoundaryConditionInitialValueList();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::InitializeStep()
    {
        static_assert(!std::is_same<time_manager_type, TimeManagerNS::Policy::None>(),
                      "This method shouldn't be called with the no time manager policy");

        decltype(auto) time_manager = this->GetTimeManager();

        if constexpr (MoReFEMDataT::time_manager_type::do_support_restart_mode
                      == TimeManagerNS::support_restart_mode::yes)
        {
            // Update time for current time step.
            if (time_manager.IsInRestartMode())
            {
                // In the very first time step, don't update time as it was already properly
                // when loading time data.
                static bool do_call_update_time = false;

                if (do_call_update_time)
                    UpdateTime();

                do_call_update_time = true;
            } else
                UpdateTime();
        } else
            UpdateTime();

        // Print time information
        if (DoPrintBanner() && do_print_new_time_iteration_banner_)
        {
            if ((time_manager.NtimeModified() % GetDisplayValue()) == 0)
            {
                PrintNewTimeIterationBanner();
            }
        }

        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplInitializeStep();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::FinalizeStep()
    {
        static_assert(!std::is_same<time_manager_type, TimeManagerNS::Policy::None>(),
                      "This method shouldn't be called with the no time manager policy");

        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplFinalizeStep();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::Finalize()
    {
        const auto& mpi = this->GetMpi();

        decltype(auto) time_keep = TimeKeep::GetInstance();

        std::string time_end = time_keep.TimeElapsedSinceBeginning();

        if (DoPrintBanner())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "Time of execution : %s.\n", mpi, std::source_location::current(), time_end.c_str());
            Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);
        }

        time_keep.PrintEndProgram();

        // Additional operations required by DerivedT.
        static_cast<DerivedT&>(*this).SupplFinalize();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::Run()
    {
        auto& crtp_helper = static_cast<DerivedT&>(*this);

        crtp_helper.Initialize();

        if constexpr ((!std::is_same<time_manager_type, TimeManagerNS::Instance::Static>())
                      && (!std::is_same<time_manager_type, TimeManagerNS::Instance::None>()))
        {
            while (!crtp_helper.HasFinished())
            {
                crtp_helper.InitializeStep();
                crtp_helper.Forward();
                crtp_helper.FinalizeStep();
            }
        }

        crtp_helper.Finalize();
    }


    ////////////////////////
    // ACCESSORS          //
    ////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline const Mesh& Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetMesh(
        ::MoReFEM::MeshNS::unique_id mesh_index) const
    {
        return Internal::MeshNS::MeshManager::GetInstance().GetMesh(mesh_index);
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline Mesh& Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetNonCstMesh(
        ::MoReFEM::MeshNS::unique_id mesh_index) const
    {
        return Internal::MeshNS::MeshManager::GetInstance().GetNonCstMesh(mesh_index);
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    const GodOfDof& Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetGodOfDof(
        ::MoReFEM::MeshNS::unique_id unique_id) const
    {
        return GodOfDofManager::GetInstance().GetGodOfDof(unique_id);
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline auto Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetNonCstTimeManager() noexcept
        -> time_manager_type&
    {
        return GetNonCstMoReFEMData().GetNonCstTimeManager();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline auto Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetTimeManager() const noexcept
        -> const time_manager_type&
    {
        static_assert(!std::is_same<time_manager_type, TimeManagerNS::Policy::None>(),
                      "This method shouldn't be called with the no time manager policy");

        return GetMoReFEMData().GetTimeManager();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    const FilesystemNS::Directory&
    Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetOutputDirectory() const noexcept
    {
        return GetMoReFEMData().GetResultDirectory();
    }


    ////////////////////////
    // PRIVATE METHODS    //
    ////////////////////////


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::UpdateTime()
    {
        static_assert(!std::is_same<time_manager_type, TimeManagerNS::Policy::None>(),
                      "This method shouldn't be called with the no time manager policy");

        GetNonCstTimeManager().IncrementTime();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::PrintNewTimeIterationBanner() const
    {
        const auto& mpi = this->GetMpi();

        static_assert(!std::is_same<time_manager_type, TimeManagerNS::Policy::None>(),
                      "This method shouldn't be called with the no time manager policy");

        decltype(auto) time_manager = GetTimeManager();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n---------------------------------------------------\n", mpi);
        Wrappers::Petsc::PrintMessageOnFirstProcessor("%s Iteration: %d, Time: %f -> %f \n",
                                                      mpi,
                                                      std::source_location::current(),
                                                      DerivedT::ClassName().c_str(),
                                                      time_manager.NtimeModified(),
                                                      time_manager.GetTime() - time_manager.GetTimeStep(),
                                                      time_manager.GetTime());
        Wrappers::Petsc::PrintMessageOnFirstProcessor("---------------------------------------------------\n", mpi);
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    std::size_t Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetDisplayValue() const
    {
        static_assert(!std::is_same<time_manager_type, TimeManagerNS::Policy::None>(),
                      "This method shouldn't be called with the no time manager policy");

        assert(display_value_ != std::nullopt
               && "This attribute may be kept nullopt for few of the tests; if you get this in a full fledged model "
                  "please report it in an issue.");

        return display_value_.value();
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::SetClearGodOfDofTemporaryDataToFalse()
    {
        do_clear_god_of_dof_temporary_data_after_initialize_ = false;
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::SetDoPrintNewTimeIterationBanner(
        bool do_print) noexcept
    {
        do_print_new_time_iteration_banner_ = do_print;
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline void Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::CreateDomainListForCoords()
    {
        auto& domain_manager = DomainManager::CreateOrGetInstance();

        const auto& domain_list = domain_manager.GetStorage();

        for (const auto& [domain_unique_id, domain_ptr] : domain_list)
        {
            assert(!(!domain_ptr));

            const auto& domain = *domain_ptr;

            const auto& mesh = domain.GetMesh();

            const auto& geometric_elt_list = mesh.template GetGeometricEltList<RoleOnProcessor::processor_wise>();

            for (const auto& geometric_elt_ptr : geometric_elt_list)
            {
                assert(!(!geometric_elt_ptr));

                const auto& geom_elt = *geometric_elt_ptr;

                if (domain.IsGeometricEltInside(geom_elt))
                {
                    auto& coords_list_elem = geom_elt.GetCoordsList();

                    for (auto& coords_ptr : coords_list_elem)
                    {
                        assert(!(!coords_ptr));

                        auto& coords = *coords_ptr;

                        coords.AddDomainContainingCoords(domain_unique_id);
                    }
                }
            }
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    bool Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::DoPrintBanner() const noexcept
    {
        switch (do_print_banner_)
        {
        case print_banner::yes:
            return true;
        case print_banner::no:
            return false;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline auto Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetMoReFEMData() const noexcept
        -> const morefem_data_type&
    // clang-format on
    {
        return morefem_data_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline auto Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::GetNonCstMoReFEMData() noexcept
        -> morefem_data_type&
    // clang-format on
    {
        return const_cast<morefem_data_type&>(GetMoReFEMData());
    }


    // clang-format off
    template
    <
        class DerivedT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    bool Model<DerivedT, MoReFEMDataT, DoConsiderProcessorWiseLocal2GlobalT>::DoWriteRestartData() const noexcept
    {
        return do_write_restart_data_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
