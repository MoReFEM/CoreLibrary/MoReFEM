##########################################
# Library to compile once files that will be used in several executables.
# There are also non source files (README, lua files, etc...) so that
# they appear correctly when CMake generates project for an IDE.
##########################################

add_library(MoReFEM4Elasticity_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4Elasticity_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/demo_2d.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_2d_binary.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_2d_binary_from_prepartitioned_data.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_2d_from_prepartitioned_data.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_3d.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_3d_binary.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_3d_binary_from_prepartitioned_data.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_3d_from_prepartitioned_data.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_3d_restart.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_3d_binary_restart.lua
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp 
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hxx		
 		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx
)

target_link_libraries(MoReFEM4Elasticity_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      )

morefem_organize_IDE(MoReFEM4Elasticity_lib ModelInstances/Elasticity ModelInstances/Elasticity)

morefem_install(MoReFEM4Elasticity_lib)

##########################################
# Executable to run the model
##########################################

add_executable(MoReFEM4Elasticity ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4Elasticity
                      MoReFEM4Elasticity_lib)

morefem_organize_IDE(MoReFEM4Elasticity ModelInstances/Elasticity ModelInstances/Elasticity)                      

apply_lto_if_supported(MoReFEM4Elasticity)

morefem_install(MoReFEM4Elasticity)


##########################################
# Executable to update a input Lua file
##########################################

add_executable(MoReFEM4ElasticityUpdateLuaFile
               ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)
               
target_link_libraries(MoReFEM4ElasticityUpdateLuaFile
                      ${MOREFEM_POST_PROCESSING}
                      MoReFEM4Elasticity_lib)

morefem_organize_IDE(MoReFEM4ElasticityUpdateLuaFile ModelInstances/Elasticity ModelInstances/Elasticity)                      

morefem_install(MoReFEM4ElasticityUpdateLuaFile)

##########################################
# Executable to produce outputs in Ensight format
##########################################

add_executable(MoReFEM4ElasticityEnsightOutput
               ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)
               
target_link_libraries(MoReFEM4ElasticityEnsightOutput
                      ${MOREFEM_POST_PROCESSING}
                      MoReFEM4Elasticity_lib)

morefem_organize_IDE(MoReFEM4ElasticityEnsightOutput ModelInstances/Elasticity ModelInstances/Elasticity)                      

morefem_install(MoReFEM4ElasticityEnsightOutput)


##########################################
# Tests
##########################################


morefem_test_run_model_in_both_modes(NAME ElasticityModel3D
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)

morefem_test_run_model_in_both_modes(NAME ElasticityModel3D
                                     FROM_PREPARTITIONED_DATA
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_from_prepartitioned_data.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)    

morefem_test_run_model_in_both_modes(NAME ElasticityModel3D
                                     BINARY
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_binary.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)

morefem_test_run_model_in_both_modes(NAME ElasticityModel3D
                                     BINARY
                                     FROM_PREPARTITIONED_DATA
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_binary_from_prepartitioned_data.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)  
                                     
                                                                                                                    
morefem_test_run_model_in_both_modes(NAME ElasticityModel3D
                                     RESTART
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_restart.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 20)

morefem_test_run_model_in_both_modes(NAME ElasticityModel3D
                                     BINARY RESTART
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_3d_binary_restart.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)                                     


morefem_test_run_model_in_both_modes(NAME ElasticityModel2D
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)
                          
morefem_test_run_model_in_both_modes(NAME ElasticityModel2D
                                     FROM_PREPARTITIONED_DATA
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d_from_prepartitioned_data.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)
                                         
morefem_test_run_model_in_both_modes(NAME ElasticityModel2D
                                     BINARY
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d_binary.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)                                            


morefem_test_run_model_in_both_modes(NAME ElasticityModel2D
                                     BINARY
                                     FROM_PREPARTITIONED_DATA
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d_binary_from_prepartitioned_data.lua
                                     EXE MoReFEM4Elasticity
                                     TIMEOUT 50)   


##########################################
# Executable and test which check reference results are properly found.
##########################################

add_executable(MoReFEM4ElasticityCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
target_link_libraries(MoReFEM4ElasticityCheckResults
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS}
                      MoReFEM4Elasticity_lib)

morefem_organize_IDE(MoReFEM4ElasticityCheckResults ModelInstances/Elasticity ModelInstances/Elasticity)                        

morefem_boost_test_check_results(NAME Elasticity
                                 EXE MoReFEM4ElasticityCheckResults                                 
                                 TIMEOUT 20)
