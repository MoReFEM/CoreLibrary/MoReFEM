// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "ModelInstances/Elasticity/Model.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM::ElasticityNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data), variational_formulation_(nullptr)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();
        decltype(auto) numbering_subset_ptr =
            god_of_dof.GetNumberingSubsetPtr(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));
        const auto& numbering_subset = *numbering_subset_ptr;

        {
            decltype(auto) bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::sole)) };

            decltype(auto) main_felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
            decltype(auto) neumannn_felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::neumann));
            decltype(auto) unknown_manager = UnknownManager::GetInstance();
            decltype(auto) unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::solid_displacement));

            variational_formulation_ = std::make_unique<variational_formulation_type>(
                main_felt_space, neumannn_felt_space, unknown, numbering_subset, god_of_dof, bc_list, morefem_data);
        }

        // Initialization here is also in charge of running the static case, and in restart mode
        // to load the data from the previous run.
        GetNonCstVariationalFormulation().Init(morefem_data);
    }


    void Model::Forward()
    {
        auto& formulation = this->GetNonCstVariationalFormulation();

        // Only Rhs is modified at each time iteration; compute it and solve the system.
        formulation.ComputeDynamicSystemRhs();
        const auto& numbering_subset = formulation.GetNumberingSubset();

        static bool is_first_call = true;

        if (is_first_call)
        {
            formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
                numbering_subset, numbering_subset);
            formulation.SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset);
            is_first_call = false;
        } else
        {
            formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset,
                                                                                                  numbering_subset);

            formulation.SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset);
        }
    }


    void Model::SupplFinalizeStep()
    {
        // Update quantities for next iteration.
        auto& formulation = this->GetNonCstVariationalFormulation();
        const auto& numbering_subset = formulation.GetNumberingSubset();

        formulation.WriteSolution(this->GetTimeManager(), numbering_subset);
        formulation.UpdateDisplacementAndVelocity();
    }


    void Model::SupplFinalize() const
    { }


    void Model::SupplInitializeStep() const
    {
        if (parent::DoWriteRestartData())
            GetVariationalFormulation().WriteRestartData();
    }


} // namespace MoReFEM::ElasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
