// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_ELASTICITY_MODEL_DOT_HXX_
#define MOREFEM_MODELINSTANCES_ELASTICITY_MODEL_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/Elasticity/Model.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <string>


namespace MoReFEM::ElasticityNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Elasticity");
        return name;
    }


    inline const Model::variational_formulation_type& Model::GetVariationalFormulation() const noexcept
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline Model::variational_formulation_type& Model::GetNonCstVariationalFormulation() noexcept
    {
        return const_cast<variational_formulation_type&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


} // namespace MoReFEM::ElasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_ELASTICITY_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
