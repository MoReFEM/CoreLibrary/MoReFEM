// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Utilities/InputData/ModelSettings.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "ModelInstances/Elasticity/InputData.hpp"


namespace MoReFEM::ElasticityNS
{


    void ModelSettings::Init()
    {
        // ****** Numbering subset ******
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>>(
                "Monolithic");
        }

        // ****** Unknown ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::solid_displacement)>>(
                { "Solid displacement" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::solid_displacement)>::Name>("solid_displacement");
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::solid_displacement)>::Nature>("vectorial");
        }

        // ****** Domain ******
        {
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
                { "Highest dimension geometric elements" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>>(
                { "Domain upon which Neumann boundary condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>>(
                { "Domain upon which Dirichlet boundary condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ "Full mesh" });

            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
        }

        // ****** Finite element space ******
        {
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
                "Finite element space for highest geometric dimension");
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>>(
                { "Finite element space for Neumann boundary condition" });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::highest_dimension));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::UnknownList>(
                { "solid_displacement" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::neumann));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::UnknownList>(
                { "solid_displacement" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });
        }

        // ****** Dirichlet boundary condition ******
        {
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>>(
                { "Sole Dirichlet boundary condition" });

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::UnknownName>(
                "solid_displacement");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::dirichlet));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::IsMutable>(
                false);
        }


        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Sole mesh" });


        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::volumic)>>(
            { "Volumic transient source" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::surfacic)>>(
            { "Surfacic transient source" });

        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Linear solver" });

        // ****** Volumic mass ******
        Add<InputDataNS::Solid::VolumicMass::Nature>("constant");
    }


} // namespace MoReFEM::ElasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
