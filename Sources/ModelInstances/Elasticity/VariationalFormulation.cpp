// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
// IWYU pragma: no_include "Core/MoReFEMData/Internal/AbstractClass.hpp"

#include <cassert>
#include <cstddef>
#include <functional>
#include <memory>
#include <string>
#include <tuple>
#include <utility>

#include "ModelInstances/Elasticity/VariationalFormulation.hpp"

#include "Utilities/Miscellaneous.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/LinearAlgebra/Operations.hpp"
#include "Core/TimeManager/StaticOrDynamic.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"

#include "FormulationSolver/Advanced/Restart.hpp"
#include "FormulationSolver/Enum.hpp"

#include "ModelInstances/Elasticity/InputData.hpp"


namespace MoReFEM::ElasticityNS
{


    VariationalFormulation::~VariationalFormulation() = default;


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        volumic_mass_ =
            InitScalarParameterFromInputData<InputDataNS::Solid::VolumicMass>("Volumic mass", domain, morefem_data);

        if (!GetVolumicMass().IsConstant())
            throw Exception("Current elastic model is restricted to a constant volumic mass!");

        young_modulus_ =
            InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>("Young modulus", domain, morefem_data);

        poisson_ratio_ =
            InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>("Poisson ratio", domain, morefem_data);

        DefineOperators(morefem_data);

        {
            // Assemble the stiffness matrix.
            GlobalMatrixWithCoefficient matrix(GetNonCstStiffness(), 1.);
            GetStiffnessOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }

        decltype(auto) time_manager = parent::GetTimeManager();

        if (!time_manager.IsInRestartMode())
        {
            decltype(auto) mpi = parent::GetMpi();

            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);

            RunStaticCase();
            WriteSolution(time_manager, GetNumberingSubset());
            UpdateDisplacement();
        } else
        {
            CopySolutionFilesFromOriginalRun();
            LoadRestartData();
        }

        ComputeDynamicMatrices();
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    VariationalFormulation ::VariationalFormulation(
        const FEltSpace& main_felt_space,
        const FEltSpace& neumann_felt_space,
        const Unknown& solid_displacement,
        const NumberingSubset& numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data), main_felt_space_(main_felt_space),
      neumann_felt_space_(neumann_felt_space), solid_displacement_(solid_displacement),
      numbering_subset_(numbering_subset)
    {
        assert(parent::GetTimeManager().IsTimeStepConstant()
               && "Model's current implementation relies on this assumption.");
    }


    void VariationalFormulation::RunStaticCase()
    {
        AddSourcesToRhs();

        const auto& numbering_subset = GetNumberingSubset();
        parent::GetNonCstSystemMatrix(numbering_subset, numbering_subset).Copy(GetStiffness());

        parent::template ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
            numbering_subset, numbering_subset);

        parent::template SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset);
    }


    void VariationalFormulation::ComputeDynamicSystemRhs()
    {
        auto& rhs = this->GetNonCstSystemRhs(GetNumberingSubset());

        // Compute the system RHS. The rhs is effectively zeroed through the first MatMult call.
        const auto& current_displacement_matrix = GetMatrixCurrentDisplacement();
        const auto& current_velocity_matrix = GetMatrixCurrentVelocity();

        const auto& current_displacement_vector = GetVectorCurrentDisplacement();
        const auto& current_velocity_vector = GetVectorCurrentVelocity();

        GlobalLinearAlgebraNS::MatMult(current_displacement_matrix, current_displacement_vector, rhs);
        GlobalLinearAlgebraNS::MatMultAdd(current_velocity_matrix, current_velocity_vector, rhs, rhs);
    }


    void VariationalFormulation::AddSourcesToRhs()
    {
        const auto& numbering_subset = GetNumberingSubset();
        auto& rhs = parent::GetNonCstSystemRhs(numbering_subset);
        const auto time = parent::GetTimeManager().GetTime();

        if (this->template IsOperatorActivated<SourceType::volumic>())
        {
            GlobalVectorWithCoefficient force_vector(rhs, 1.);
            this->template GetForceOperator<SourceType::volumic>().Assemble(std::make_tuple(std::ref(force_vector)),
                                                                            time);
        }

        if (this->template IsOperatorActivated<SourceType::surfacic>())
        {
            GlobalVectorWithCoefficient force_vector(rhs, 1.);
            this->template GetForceOperator<SourceType::surfacic>().Assemble(std::make_tuple(std::ref(force_vector)),
                                                                             time);
        }
    }


    void VariationalFormulation ::AllocateMatricesAndVectors()
    {
        const auto& numbering_subset = GetNumberingSubset();

        parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
        parent::AllocateSystemVector(numbering_subset);

        const auto& system_matrix = parent::GetSystemMatrix(numbering_subset, numbering_subset);
        const auto& system_rhs = parent::GetSystemRhs(numbering_subset);

        mass_ = std::make_unique<GlobalMatrix>(system_matrix);
        stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
        matrix_current_displacement_ = std::make_unique<GlobalMatrix>(system_matrix);
        matrix_current_velocity_ = std::make_unique<GlobalMatrix>(system_matrix);

        vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs, "Current velocity");
        vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs, "Current displacement");
    }


    void VariationalFormulation::ComputeDynamicMatrices()
    {
        const auto& numbering_subset = GetNumberingSubset();
        auto& system_matrix = this->GetNonCstSystemMatrix(numbering_subset, numbering_subset);
        const auto& stiffness = GetStiffness();

        {
            GlobalMatrixWithCoefficient mass(GetNonCstMass(), 1.);
            GetMassOperator().Assemble(std::make_tuple(std::ref(mass)));
        }

        const auto& mass = GetMass();

        {
            // Compute the system matrix, which won't change afterwards!
            system_matrix.Copy(stiffness);
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            system_matrix.Scale(0.5);

            const auto coefficient =
                2. * GetVolumicMass().GetConstantValue() / NumericNS::Square(parent::GetTimeManager().GetTimeStep());
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
#ifndef NDEBUG
            AssertSameNumberingSubset(mass, system_matrix);
#endif // NDEBUG

            GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(coefficient, mass, system_matrix);
        }

        {
            // Displacement matrix.
            auto& current_displacement_matrix = GetNonCstMatrixCurrentDisplacement();
            current_displacement_matrix.Copy(mass);

            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            const auto coefficient =
                2. * GetVolumicMass().GetConstantValue() / NumericNS::Square(parent::GetTimeManager().GetTimeStep());
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

            current_displacement_matrix.Scale(coefficient);

#ifndef NDEBUG
            AssertSameNumberingSubset(stiffness, current_displacement_matrix);
#endif // NDEBUG

            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(-.5, stiffness, current_displacement_matrix);
        }

        {
            // Velocity matrix.
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            auto& current_velocity_matrix = GetNonCstMatrixCurrentVelocity();
            current_velocity_matrix.Copy(mass);
            current_velocity_matrix.Scale(2. * GetVolumicMass().GetConstantValue()
                                          / parent::GetTimeManager().GetTimeStep());
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        }
    }


    void VariationalFormulation::UpdateDisplacement()
    {
        GetNonCstVectorCurrentDisplacement().Copy(parent::GetSystemSolution(GetNumberingSubset()));
    }


    void VariationalFormulation::UpdateVelocity()
    {
        const auto& current_displacement_vector = GetVectorCurrentDisplacement();
        const auto& system_solution = parent::GetSystemSolution(GetNumberingSubset());
        auto& current_velocity_vector = GetNonCstVectorCurrentVelocity();

        assert(parent::GetTimeManager().GetStaticOrDynamic() == StaticOrDynamic::dynamic_);

        {
            // Update first the velocity.
            const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> solution(system_solution);
            const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> displacement_prev(
                current_displacement_vector);
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> velocity(current_velocity_vector);

            const auto size = velocity.GetSize();
            assert(size == solution.GetSize());
            assert(size == displacement_prev.GetSize());

            const double factor = 2. / parent::GetTimeManager().GetTimeStep();

            for (auto i = vector_processor_wise_index_type{ 0UL }; i < size; ++i)
            {
                velocity[i] *= -1.;
                velocity[i] += factor * (solution.GetValue(i) - displacement_prev.GetValue(i));
            }
        }
    }


    void VariationalFormulation::DefineOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = this->GetGodOfDof();
        const auto& felt_space_highest_dimension = GetMainFEltSpace();
        const auto& felt_space_neumann = GetNeumannFEltSpace();

        const auto mesh_dimension = god_of_dof.GetMesh().GetDimension();

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::solid_displacement));

        this->template SetIfTaggedAsActivated<SourceType::volumic>(
            "Volumic force", morefem_data, felt_space_highest_dimension, displacement_ptr);

        this->template SetIfTaggedAsActivated<SourceType::surfacic>(
            "Surfacic force", morefem_data, felt_space_neumann, displacement_ptr);

        namespace GVO = GlobalVariationalOperatorNS;

        stiffness_operator_ = std::make_unique<GVO::GradOnGradientBasedElasticityTensor<time_manager_type>>(
            felt_space_highest_dimension,
            displacement_ptr,
            displacement_ptr,
            GetYoungModulus(),
            GetPoissonRatio(),
            ParameterNS::ReadGradientBasedElasticityTensorConfigurationFromFile(mesh_dimension, morefem_data));

        mass_operator_ = std::make_unique<GVO::Mass>(felt_space_highest_dimension, displacement_ptr, displacement_ptr);
    }


    void VariationalFormulation::WriteRestartData() const
    {
        decltype(auto) time_manager = parent::GetTimeManager();
        decltype(auto) god_of_dof = parent::GetGodOfDof();

        Advanced::RestartNS::WriteDataForRestart(time_manager, god_of_dof, "velocity", GetVectorCurrentVelocity());
        Advanced::RestartNS::WriteDataForRestart(
            time_manager, god_of_dof, "displacement", GetVectorCurrentDisplacement());
    }


    void VariationalFormulation::LoadRestartData()
    {
        decltype(auto) time_manager = parent::GetTimeManager();

        decltype(auto) god_of_dof = parent::GetGodOfDof();

        Advanced::RestartNS::LoadGlobalVectorFromData(
            time_manager, god_of_dof, "velocity", GetNonCstVectorCurrentVelocity());
        Advanced::RestartNS::LoadGlobalVectorFromData(
            time_manager, god_of_dof, "displacement", GetNonCstVectorCurrentDisplacement());
    }


    void VariationalFormulation::CopySolutionFilesFromOriginalRun()
    {
        Advanced::RestartNS::CopySolutionFilesFromOriginalRun(
            parent::GetTimeManager(), parent::GetGodOfDof(), GetNumberingSubset());
    }


} // namespace MoReFEM::ElasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
