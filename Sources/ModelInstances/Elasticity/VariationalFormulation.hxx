// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_ELASTICITY_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_MODELINSTANCES_ELASTICITY_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/Elasticity/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "ModelInstances/Elasticity/InputData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class Unknown; }
namespace MoReFEM::GlobalVariationalOperatorNS { class Mass; }
namespace MoReFEM::GlobalVariationalOperatorNS { template <TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> class GradOnGradientBasedElasticityTensor; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ElasticityNS
{


    inline void VariationalFormulation ::UpdateDisplacementAndVelocity()
    {
        // Ordering of both calls is capital here!
        UpdateVelocity();
        UpdateDisplacement();
    }


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const noexcept
    {
        return nullptr;
    }


    inline const GlobalVector& VariationalFormulation ::GetVectorCurrentDisplacement() const noexcept
    {
        assert(!(!vector_current_displacement_));
        return *vector_current_displacement_;
    }


    inline GlobalVector& VariationalFormulation ::GetNonCstVectorCurrentDisplacement() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
    }


    inline const GlobalVector& VariationalFormulation ::GetVectorCurrentVelocity() const noexcept
    {
        assert(!(!vector_current_velocity_));
        return *vector_current_velocity_;
    }


    inline GlobalVector& VariationalFormulation ::GetNonCstVectorCurrentVelocity() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetMatrixCurrentDisplacement() const noexcept
    {
        assert(!(!matrix_current_displacement_));
        return *matrix_current_displacement_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstMatrixCurrentDisplacement() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixCurrentDisplacement());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetMatrixCurrentVelocity() const noexcept
    {
        assert(!(!matrix_current_velocity_));
        return *matrix_current_velocity_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstMatrixCurrentVelocity() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixCurrentVelocity());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetMass() const noexcept
    {
        assert(!(!mass_));
        return *mass_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstMass() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMass());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetStiffness() const noexcept
    {
        assert(!(!stiffness_));
        return *stiffness_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstStiffness() noexcept
    {
        return const_cast<GlobalMatrix&>(GetStiffness());
    }


    inline const GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor<time_manager_type>&
    VariationalFormulation::GetStiffnessOperator() const noexcept
    {
        assert(!(!stiffness_operator_));
        return *stiffness_operator_;
    }


    inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation ::GetMassOperator() const noexcept
    {
        assert(!(!mass_operator_));
        return *mass_operator_;
    }


    inline const NumberingSubset& VariationalFormulation ::GetNumberingSubset() const noexcept
    {
        return numbering_subset_;
    }


    inline auto VariationalFormulation ::GetVolumicMass() const noexcept -> const solid_scalar_parameter_type&
    {
        assert(!(!volumic_mass_));
        return *volumic_mass_;
    }


    inline auto VariationalFormulation ::GetYoungModulus() const noexcept -> const solid_scalar_parameter_type&
    {
        assert(!(!young_modulus_));
        return *young_modulus_;
    }


    inline auto VariationalFormulation ::GetPoissonRatio() const noexcept -> const solid_scalar_parameter_type&
    {
        assert(!(!poisson_ratio_));
        return *poisson_ratio_;
    }


    inline const Unknown& VariationalFormulation ::GetSolidDisplacement() const noexcept
    {
        return solid_displacement_;
    }


    inline const FEltSpace& VariationalFormulation ::GetMainFEltSpace() const noexcept
    {
        return main_felt_space_;
    }


    inline const FEltSpace& VariationalFormulation ::GetNeumannFEltSpace() const noexcept
    {
        return neumann_felt_space_;
    }


} // namespace MoReFEM::ElasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_ELASTICITY_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
