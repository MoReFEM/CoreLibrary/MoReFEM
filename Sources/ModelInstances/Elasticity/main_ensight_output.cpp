// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep

#include "Model/Main/MainEnsightOutput.hpp"

#include "ModelInstances/Elasticity/Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::ElasticityNS;

// NOLINTBEGIN(bugprone-exception-escape)
int main(int argc, char** argv)
{
    const std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId(
        NumberingSubsetIndex::monolithic) };

    const std::vector<std::string> unknown_list{ "solid_displacement" };

    // NOLINTNEXTLINE(clang-analyzer-optin.cplusplus.VirtualCall)  due to TCLAP bug
    return ModelNS::MainEnsightOutput<ElasticityNS::Model>(
        argc, argv, AsMeshId(MeshIndex::mesh), numbering_subset_id_list, unknown_list);
}
// NOLINTEND(bugprone-exception-escape)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
