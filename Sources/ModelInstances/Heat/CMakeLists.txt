##########################################
# Library to compile once files that will be used in several executables.
# There are also non source files (README, lua files, etc...) so that
# they appear correctly when CMake generates project for an IDE.
##########################################

add_library(MoReFEM4Heat_lib ${LIBRARY_TYPE} )

target_sources(MoReFEM4Heat_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/demo_1d.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_2d.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_restart_1d.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_restart_2d.lua
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx

)

target_link_libraries(MoReFEM4Heat_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)

morefem_organize_IDE(MoReFEM4Heat_lib ModelInstances/Heat ModelInstances/Heat)                         

##########################################
# Executable to run the model
##########################################

add_executable(MoReFEM4Heat ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4Heat
                      MoReFEM4Heat_lib)
apply_lto_if_supported(MoReFEM4Heat)

morefem_organize_IDE(MoReFEM4Heat ModelInstances/Heat ModelInstances/Heat)

morefem_install(MoReFEM4Heat MoReFEM4Heat_lib)

##########################################
# Executable to produce outputs in Ensight format
##########################################

add_executable(MoReFEM4HeatEnsightOutput
               ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)


target_link_libraries(MoReFEM4HeatEnsightOutput
                      MoReFEM4Heat_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(MoReFEM4HeatEnsightOutput ModelInstances/Heat ModelInstances/Heat)

morefem_install(MoReFEM4HeatEnsightOutput)

##########################################
# Executable to update a input Lua file
##########################################

add_executable(MoReFEM4HeatUpdateLuaFile
               ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)

target_link_libraries(MoReFEM4HeatUpdateLuaFile
                      MoReFEM4Heat_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(MoReFEM4HeatUpdateLuaFile ModelInstances/Heat ModelInstances/Heat)

morefem_install(MoReFEM4HeatUpdateLuaFile)



##########################################
# Tests
##########################################


morefem_test_run_model_in_both_modes(NAME HeatModel2D
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/demo_2d.lua
                                     EXE MoReFEM4Heat
                                     TIMEOUT 20)
                                     
morefem_test_run_model_in_both_modes(NAME HeatModel1D
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/demo_1d.lua
                                     EXE MoReFEM4Heat
                                     TIMEOUT 20)
                                     
morefem_test_run_model_in_both_modes(NAME HeatModel2D
                                     RESTART
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/demo_restart_2d.lua
                                     EXE MoReFEM4Heat
                                     TIMEOUT 20)
                                     
morefem_test_run_model_in_both_modes(NAME HeatModel1D
                                     RESTART
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/demo_restart_1d.lua
                                     EXE MoReFEM4Heat
                                     TIMEOUT 20)


##########################################
# Executable and test which check reference results are properly found.
##########################################

add_executable(MoReFEM4HeatCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
target_link_libraries(MoReFEM4HeatCheckResults
                      MoReFEM4Heat_lib
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(MoReFEM4HeatCheckResults ModelInstances/Heat ModelInstances/Heat)


morefem_boost_test_check_results(NAME Heat
                                   EXE MoReFEM4HeatCheckResults                                   
                                   TIMEOUT 20)

