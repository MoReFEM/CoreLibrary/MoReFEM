// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_HEAT_INPUTDATA_DOT_HPP_
#define MOREFEM_MODELINSTANCES_HEAT_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp"
#include "Core/TimeManager/TimeManager.hpp"
// IWYU pragma: end_exports

#include "Geometry/Domain/UniqueId.hpp"


namespace MoReFEM::HeatNS
{


    //! \copydoc doxygen_hide_source_enum
    enum class ForceIndexList : std::size_t {
        volumic_source = 1,
        neumann_boundary_condition = 2,
        robin_boundary_condition = 3
    };


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t {
        mesh = 1 // only one mesh considered in current model!
    };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        highest_dimension = 1,
        neumann = 2,
        robin = 3,
        dirichlet_1 = 4,
        dirichlet_2 = 5,
        full_mesh = 6
    };


    //! \copydoc doxygen_hide_boundary_condition_enum
    enum class BoundaryConditionIndex : std::size_t { first = 1, second = 2 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { highest_dimension = 1, neumann = 2, robin = 3 };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { temperature = 1 };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex { solver = 1 };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { monolithic = 1 };


    //! Index to tag the tensors involved.
    enum class TensorIndex { diffusion_tensor = 1 };


    //! \copydoc doxygen_hide_initial_condition_enum
    enum class InitialConditionIndex { temperature = 1 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        InputDataNS::Restart,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::neumann),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::robin),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet_1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet_2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::first),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::second),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::neumann),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::robin),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver),

        InputDataNS::Diffusion::Density,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>,
        InputDataNS::Diffusion::TransfertCoefficient,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::volumic_source),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::neumann_boundary_condition),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::robin_boundary_condition),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::temperature),

        InputDataNS::Parallelism,
        InputDataNS::Result

    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::neumann),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::robin),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::monolithic),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::temperature),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::neumann),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::robin),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet_1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet_2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::first),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::second),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver),

        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>::IndexedSectionDescription,

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::volumic_source),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::neumann_boundary_condition),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_SCALAR_TRANSIENT_SOURCE(ForceIndexList::robin_boundary_condition),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::temperature)
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type =
        ::MoReFEM::TimeManagerNS::Instance::ConstantTimeStep<TimeManagerNS::support_restart_mode::yes>;


    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::model>;

} // namespace MoReFEM::HeatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_HEAT_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
