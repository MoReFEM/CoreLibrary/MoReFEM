// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <memory>

#include "ModelInstances/Heat/Model.hpp"

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/Extract.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"


namespace MoReFEM::HeatNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const GodOfDof& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();
        const NumberingSubset& numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        {
            const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = {
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::first)),
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::second))
            };

            variational_formulation_ =
                std::make_unique<VariationalFormulation>(numbering_subset, god_of_dof, bc_list, morefem_data);
        }

        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();

        using TimeManager = InputDataNS::TimeManager;

        const double time_max = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeMax>(morefem_data);

        if (NumericNS::IsZero(time_max))
            variational_formulation.SetStaticOrDynamic(VariationalFormulation::StaticOrDynamic::static_case);
        else
            variational_formulation.SetStaticOrDynamic(VariationalFormulation::StaticOrDynamic::dynamic_case);

        variational_formulation.Init(morefem_data);
        variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
    }


    void Model::Forward()
    {
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();

        // Only Rhs is modified at each time iteration; compute it and solve the system.
        variational_formulation.ComputeDynamicSystemRhs();
        variational_formulation.CallSolver();
    }


    void Model::SupplFinalizeStep()
    {
        // Update quantities for next iteration.
        const VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& numbering_subset = variational_formulation.GetNumberingSubset();

        variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
    }


    void Model::SupplFinalize() const
    { }


    void Model::SupplInitializeStep()
    {
        if (parent::DoWriteRestartData())
            GetVariationalFormulation().WriteRestartData();
    }


} // namespace MoReFEM::HeatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
