// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_HEAT_MODEL_DOT_HXX_
#define MOREFEM_MODELINSTANCES_HEAT_MODEL_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/Heat/Model.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <string>

#include "ModelInstances/Heat/VariationalFormulation.hpp"


namespace MoReFEM::HeatNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Heat model");
        return name;
    }


    inline const VariationalFormulation& Model::GetVariationalFormulation() const
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
    {
        return const_cast<VariationalFormulation&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        const VariationalFormulation& formulation = GetVariationalFormulation();

        // In the static case no loop time so HasFinished is set at true to avoid the while loop
        if (formulation.GetStaticOrDynamic() == VariationalFormulation::StaticOrDynamic::static_case)
            return true;
        else
            return false; // ie no additional condition
    }


} // namespace MoReFEM::HeatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_HEAT_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
