// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
// IWYU pragma: no_include "Core/MoReFEMData/Internal/AbstractClass.hpp"

#include <functional>
#include <memory>
#include <tuple>
#include <utility>

#include "ModelInstances/Heat/VariationalFormulation.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"

#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/LinearAlgebra/Operations.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "FormulationSolver/Advanced/Restart.hpp"
#include "FormulationSolver/Enum.hpp"

#include "ModelInstances/Heat/InputData.hpp"


namespace MoReFEM::HeatNS
{


    VariationalFormulation::~VariationalFormulation() = default;


    VariationalFormulation::VariationalFormulation(
        const NumberingSubset& numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data), numbering_subset_(numbering_subset)
    { }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        using Diffusion = InputDataNS::Diffusion;

        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        diffusion_tensor_ =
            InitScalarParameterFromInputData<Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>>(
                "Diffusion tensor", domain, morefem_data);
        transfert_coefficient_ = InitScalarParameterFromInputData<Diffusion::TransfertCoefficient>(
            "Transfert coefficient", domain, morefem_data);

        density_ = InitScalarParameterFromInputData<Diffusion::Density>("Density", domain, morefem_data);

        if (!GetDiffusionTensor().IsConstant())
            throw Exception("Current heat model is restricted to a constant diffusion tensor.");

        if (!GetTransfertCoefficient().IsConstant())
            throw Exception("Current heat model is restricted to a constant transfert coefficient.");

        if (!GetDiffusionDensity().IsConstant())
            throw Exception("Current heat model is restricted to a constant diffusion density.");

        DefineOperators(morefem_data);

        decltype(auto) time_manager = parent::GetTimeManager();

        if (time_manager.IsInRestartMode())
        {
            CopySolutionFilesFromOriginalRun();
            LoadRestartData();
            AssembleTransientOperators();
            PrepareDynamicRuns(morefem_data);
            run_case_ = StaticOrDynamic::dynamic_case;
        } else
        {
            if (GetStaticOrDynamic() == StaticOrDynamic::static_case)
                RunStaticCase();
            else
                PrepareDynamicRuns(morefem_data);
        }
    }


    void VariationalFormulation::RunStaticCase()
    {
        AssembleStaticOperators();
        AssembleTransientOperators();
        ComputeRhs();
        CallSolver();
    }


    void VariationalFormulation::CallSolver()
    {
        static bool is_first_call{ true };
        decltype(auto) numbering_subset = GetNumberingSubset();

        if (is_first_call)
        {
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(numbering_subset,
                                                                                                 numbering_subset);
            SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset);
            is_first_call = false;
        } else
        {
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset,
                                                                                      numbering_subset);
            SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset);
        }
    }


    void VariationalFormulation::DefineOperators(const morefem_data_type& morefem_data)
    {
        const GodOfDof& god_of_dof = GetGodOfDof();
        const FEltSpace& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
        const FEltSpace& felt_space_dim_N_minus_1_Neumann =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::neumann));
        const FEltSpace& felt_space_dim_N_minus_1_Robin = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::robin));

        const auto& temperature_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::temperature));

        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        namespace GVO = GlobalVariationalOperatorNS;

        {
            using parameter_type =
                InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::volumic_source)>;

            volumic_source_parameter_ =
                InitScalarParameterFromInputData<parameter_type>("Volumic source", domain, morefem_data);

            if (volumic_source_parameter_ != nullptr)
            {
                volumic_source_operator_ = std::make_unique<source_operator_type>(
                    felt_space_highest_dimension, temperature_ptr, *volumic_source_parameter_);
            }
        } // namespace InputDataNS;

        conductivity_operator_ =
            std::make_unique<GVO::GradPhiGradPhi>(felt_space_highest_dimension, temperature_ptr, temperature_ptr);

        if (run_case_ == StaticOrDynamic::dynamic_case)
            capacity_operator_ =
                std::make_unique<GVO::Mass>(felt_space_highest_dimension, temperature_ptr, temperature_ptr);


        {
            using parameter_type =
                InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::neumann_boundary_condition)>;

            neumann_parameter_ =
                InitScalarParameterFromInputData<parameter_type>("Neumann boundary condition", domain, morefem_data);

            if (neumann_parameter_ != nullptr)
            {
                neumann_operator_ = std::make_unique<source_operator_type>(
                    felt_space_dim_N_minus_1_Neumann, temperature_ptr, *neumann_parameter_);
            }
        }

        robin_bilinear_part_operator_ =
            std::make_unique<GVO::Mass>(felt_space_dim_N_minus_1_Robin, temperature_ptr, temperature_ptr);

        {
            using parameter_type =
                InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::robin_boundary_condition)>;

            robin_parameter_ =
                InitScalarParameterFromInputData<parameter_type>("Robin boundary condition", domain, morefem_data);

            if (robin_parameter_ != nullptr)
            {
                robin_linear_part_operator_ = std::make_unique<source_operator_type>(
                    felt_space_dim_N_minus_1_Robin, temperature_ptr, *robin_parameter_);
            }
        }
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();

        parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
        parent::AllocateSystemVector(numbering_subset);

        const GlobalMatrix& system_matrix = GetSystemMatrix(numbering_subset, numbering_subset);
        const GlobalVector& system_rhs = GetSystemRhs(numbering_subset);

        vector_current_volumic_source_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_neumann_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_robin_ = std::make_unique<GlobalVector>(system_rhs);

        matrix_conductivity_ = std::make_unique<GlobalMatrix>(system_matrix);

        if (run_case_ == StaticOrDynamic::dynamic_case)
            matrix_capacity_ = std::make_unique<GlobalMatrix>(system_matrix);

        matrix_robin_ = std::make_unique<GlobalMatrix>(system_matrix);
    }


    void VariationalFormulation::AssembleStaticOperators()
    {
        const auto& numbering_subset = GetNumberingSubset();

        auto& system_matrix = GetNonCstSystemMatrix(numbering_subset, numbering_subset);

        {
            const double transfer_coefficient = GetTransfertCoefficient().GetConstantValue();
            GlobalMatrixWithCoefficient matrix(GetNonCstMatrixRobin(), transfer_coefficient);
            GetRobinBilinearPartOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }

        {
            const double diffusion_tensor = GetDiffusionTensor().GetConstantValue();
            GlobalMatrixWithCoefficient matrix(system_matrix, diffusion_tensor);
            GetConductivityOperator().Assemble(std::make_tuple(std::ref(matrix)));
        }

#ifndef NDEBUG
        AssertSameNumberingSubset(GetMatrixRobin(), system_matrix);
#endif // NDEBUG
        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., GetMatrixRobin(), system_matrix);
    }


    void VariationalFormulation::AssembleTransientOperators()
    {
        const double transfert_coefficient = GetTransfertCoefficient().GetConstantValue();
        const double time = GetTimeManager().GetTime();

        // Put all the vectors to zero
        GetNonCstVectorCurrentVolumicSource().ZeroEntries();
        GetNonCstVectorCurrentNeumann().ZeroEntries();
        GetNonCstVectorCurrentRobin().ZeroEntries();

        {
            GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentNeumann(), 1.);
            GetNeumannOperator().Assemble(std::make_tuple(std::ref(vector)), time);
        }

        {
            GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentRobin(), transfert_coefficient);
            GetRobinLinearPartOperator().Assemble(std::make_tuple(std::ref(vector)), time);
        }

        {
            GlobalVectorWithCoefficient vector(GetNonCstVectorCurrentVolumicSource(), 1.);
            GetVolumicSourceOperator().Assemble(std::make_tuple(std::ref(vector)), time);
        }
    }


    void VariationalFormulation::PrepareDynamicRuns(const morefem_data_type& morefem_data)
    {
        // Assemble once and for all the system matrix in dynamic case; intermediate matrices used
        // to compute rhs at each time iteration are also computed there.
        AssembleStaticOperators();
        AssembleCapacityOperator();
        ComputeDynamicMatrices();

        const GodOfDof& god_of_dof = GetGodOfDof();

        {
            const NumberingSubset& numbering_subset = GetNumberingSubset();
            const FEltSpace& felt_space_highest_dimension =
                god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
            const Unknown& temperature =
                UnknownManager::GetInstance().GetUnknown(AsUnknownId(UnknownIndex::temperature));

            constexpr auto index = EnumUnderlyingType(InitialConditionIndex::temperature);

            if (!GetTimeManager().IsInRestartMode())
                parent::SetInitialSystemSolution<index>(
                    morefem_data, numbering_subset, temperature, felt_space_highest_dimension);
        }
    }


    void VariationalFormulation::AssembleCapacityOperator()
    {
        const double density = GetDiffusionDensity().GetConstantValue();

        const double capacity_coefficient = density / GetTimeManager().GetTimeStep();

        GlobalMatrixWithCoefficient matrix(GetNonCstMatrixCapacityPerTimeStep(), capacity_coefficient);


        GetCapacityOperator().Assemble(std::make_tuple(std::ref(matrix)));
    }


    void VariationalFormulation::ComputeDynamicMatrices()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();
        auto& system_matrix = GetNonCstSystemMatrix(numbering_subset, numbering_subset);
        const GlobalMatrix& capacity_matrix = GetMatrixCapacityPerTimeStep();

        {
#ifndef NDEBUG
            AssertSameNumberingSubset(capacity_matrix, system_matrix);
#endif // NDEBUG

            GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., capacity_matrix, system_matrix);
        }
    }


    void VariationalFormulation::ComputeRhs()
    {
        decltype(auto) numbering_subset = GetNumberingSubset();
        GlobalVector& rhs = GetNonCstSystemRhs(numbering_subset);

        rhs.Copy(GetVectorCurrentVolumicSource());
        GlobalLinearAlgebraNS::AXPY(1., GetVectorCurrentNeumann(), rhs);
        GlobalLinearAlgebraNS::AXPY(1., GetVectorCurrentRobin(), rhs);

        if (GetStaticOrDynamic() == StaticOrDynamic::dynamic_case)
        {
            GlobalLinearAlgebraNS::MatMultAdd(
                GetMatrixCapacityPerTimeStep(), GetSystemSolution(numbering_subset), rhs, rhs);
        }
    }


    void VariationalFormulation::ComputeDynamicSystemRhs()
    {
        const NumberingSubset& numbering_subset = GetNumberingSubset();

        GetNonCstSystemSolution(numbering_subset).UpdateGhosts();

        AssembleTransientOperators();
        ComputeRhs();
    }


    void VariationalFormulation::SetStaticOrDynamic(StaticOrDynamic value)
    {
        run_case_ = value;
    }


    void VariationalFormulation::LoadRestartData()
    {
        decltype(auto) time_manager = parent::GetTimeManager();

        decltype(auto) god_of_dof = parent::GetGodOfDof();

        Advanced::RestartNS::LoadGlobalVectorFromData(
            time_manager, god_of_dof, "temperature", GetNonCstSystemSolution(GetNumberingSubset()));
        Advanced::RestartNS::LoadGlobalVectorFromData(
            time_manager, god_of_dof, "rhs", GetNonCstSystemRhs(GetNumberingSubset()));
    }


    void VariationalFormulation::CopySolutionFilesFromOriginalRun()
    {
        Advanced::RestartNS::CopySolutionFilesFromOriginalRun(
            parent::GetTimeManager(), parent::GetGodOfDof(), GetNumberingSubset());
    }


    void VariationalFormulation::WriteRestartData() const
    {
        decltype(auto) time_manager = parent::GetTimeManager();
        decltype(auto) god_of_dof = parent::GetGodOfDof();
        decltype(auto) numbering_subset = GetNumberingSubset();

        Advanced::RestartNS::WriteDataForRestart(
            time_manager, god_of_dof, "temperature", GetSystemSolution(numbering_subset));
        Advanced::RestartNS::WriteDataForRestart(time_manager, god_of_dof, "rhs", GetSystemRhs(numbering_subset));
    }


} // namespace MoReFEM::HeatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
