// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_HEAT_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM_MODELINSTANCES_HEAT_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"

#include <map>
#include <ostream>
#include <type_traits>
#include <utility>

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "FormulationSolver/Internal/Snes/SnesInterface.hpp"
#include "FormulationSolver/VariationalFormulation.hpp"

#include "ModelInstances/Heat/InputData.hpp"


namespace MoReFEM::HeatNS
{


    //! Variational formulaton to solve for a heat problem with Dirichlet, Neumann and Robin BC.
    class VariationalFormulation final
    : public ::MoReFEM::
          VariationalFormulation<VariationalFormulation, EnumUnderlyingType(SolverIndex::solver), time_manager_type>
    {
      private:
        //! \copydoc doxygen_hide_alias_self
        using self = VariationalFormulation;

        //! Alias to the parent class.
        using parent =
            ::MoReFEM::VariationalFormulation<self, EnumUnderlyingType(SolverIndex::solver), time_manager_type>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<time_manager_type>;


      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to source operator type.
        using source_operator_type =
            GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::scalar, time_manager_type>;

        //! Alias to source parameter type.
        using source_parameter_type = ScalarParameter<time_manager_type>;

        //! Alias on a pair of Unknown.
        using UnknownPair = std::pair<const Unknown&, const Unknown&>;

      public:
        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_varf_constructor
        //!
        //! \param[in] numbering_subset The only \a NumberingSubset used in this variational formulation.
        explicit VariationalFormulation(const NumberingSubset& numbering_subset,
                                        const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type& morefem_data);

        //! Destructor.
        ~VariationalFormulation() override;

        //! \copydoc doxygen_hide_copy_constructor
        VariationalFormulation(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VariationalFormulation(VariationalFormulation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

        ///@}


        //! At each time iteration, compute the system Rhs.
        void ComputeDynamicSystemRhs();

        //! Accessor to the capacity matrix per time step.
        const GlobalMatrix& GetMatrixCapacityPerTimeStep() const;

        /*!
         * \brief Call the solver
         *
         * In this model matrix may be factorized so the very first call differs from the following ones which use
         * up the factorization to speed up the computation.
         */
        void CallSolver();


      private:
        //! Run the static case.
        void RunStaticCase();

        /*!
         * \brief Compute Rhs related to \a numbering_subset_
         *
         * Computation is not exactly the same in static or dynamic mode.
         */
        void ComputeRhs();


        /*!
         * \brief Prepare dynamic runs.
         *
         * For instance for dynamic iterations the system matrix is always the same; compute it once and for all
         * here.
         *
         * StaticOrDynamic rhs is what changes between two time iterations, but to compute it the same matrices are
         * used at each time iteration; they are also computed there
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        void PrepareDynamicRuns(const morefem_data_type& morefem_data);


        /*!
         * \brief Assemble method for all the static operators except the capacity.
         */
        void AssembleStaticOperators();

        /*!
         * \brief Assemble method for all the transient operators (Source, Neumann and Robin).
         */
        void AssembleTransientOperators();

        /*!
         * \brief Assemble method for the capacity operator.
         */
        void AssembleCapacityOperator();


        //! Compute all the matrices required for dynamic calculation.
        void ComputeDynamicMatrices();

      public:
        /*!
         * \brief Write data that would be required to run later in restart mode.
         */
        void WriteRestartData() const;

      private:
        /*!
         * \brief Load restart data from a previous run.
         */
        void LoadRestartData();


        /*!
         * \brief In restart mode, copy the data that were computed in the original run and won't be recomputed.
         */
        void CopySolutionFilesFromOriginalRun();


      public:
        //! Enum class to decide to run either the static or dynamic problem.
        enum class StaticOrDynamic { static_case, dynamic_case };


        //! Getter for the run case.
        StaticOrDynamic GetStaticOrDynamic() const;


        //! Setter for the run case.
        //! \param[in] value Value to be set.
        void SetStaticOrDynamic(StaticOrDynamic value);

        /*!
         * \brief Get the only numbering subset relevant for this VariationalFormulation.
         *
         * There is a more generic accessor in the base class but is use is more unwieldy.
         *
         * \return The only relevant \a NumberingSubset.
         */
        const NumberingSubset& GetNumberingSubset() const;


      private:
        /// \name CRTP-required methods.
        ///@{

        //! \copydoc doxygen_hide_varf_suppl_init
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();


        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

        ///@}


      private:
        /*!
         * \brief Define the properties of all the global variational operators involved.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        void DefineOperators(const morefem_data_type& morefem_data);

        //! Get the volumic source operator.
        const source_operator_type& GetVolumicSourceOperator() const noexcept;

        //! Get the conductivity operator.
        const GlobalVariationalOperatorNS::GradPhiGradPhi& GetConductivityOperator() const noexcept;

        //! Get the capacity operator.
        const GlobalVariationalOperatorNS::Mass& GetCapacityOperator() const noexcept;

        //! Get the Neumann Operator
        const source_operator_type& GetNeumannOperator() const noexcept;

        //! Get the Robin bilinear part operator.
        const GlobalVariationalOperatorNS::Mass& GetRobinBilinearPartOperator() const noexcept;

        //! Get the Robin linear part operator.
        const source_operator_type& GetRobinLinearPartOperator() const noexcept;

      private:
        /// \name Accessors to vectors and matrices specific to the heat problem.
        ///@{

        //! Accessor.
        const GlobalVector& GetVectorCurrentVolumicSource() const;

        //! Accessor.
        GlobalVector& GetNonCstVectorCurrentVolumicSource();

        //! Accessor.
        const GlobalVector& GetVectorCurrentNeumann() const;

        //! Accessor.
        GlobalVector& GetNonCstVectorCurrentNeumann();

        //! Accessor.
        const GlobalVector& GetVectorCurrentRobin() const;

        //! Accessor.
        GlobalVector& GetNonCstVectorCurrentRobin();

        //! Accessor.
        const GlobalMatrix& GetMatrixConductivity() const;

        //! Accessor.
        GlobalMatrix& GetNonCstMatrixConductivity();

        //! Accessor.
        GlobalMatrix& GetNonCstMatrixCapacityPerTimeStep();

        //! Accessor.
        const GlobalMatrix& GetMatrixRobin() const;

        //! Accessor.
        GlobalMatrix& GetNonCstMatrixRobin();


        ///@}

        /// \name Material parameters.
        ///@{

        //! Diffusion density,
        const scalar_parameter_type& GetDiffusionDensity() const;

        //! Diffusion tranfert coefficient.
        const scalar_parameter_type& GetTransfertCoefficient() const;

        //! Diffusion tensor.
        const scalar_parameter_type& GetDiffusionTensor() const;

        ///@}

      private:
        //! Accessor on the temperature temperature pair.
        const UnknownPair& GetTemperatureTemperaturePair() const noexcept;

      private:
        /// \name Global variational operators.
        ///@{


        //! Conductivity operator.
        GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr conductivity_operator_ = nullptr;

        //! Capacity operator
        GlobalVariationalOperatorNS::Mass::const_unique_ptr capacity_operator_ = nullptr;

        //! Volumic source operator.
        source_operator_type::const_unique_ptr volumic_source_operator_ = nullptr;

        //! Neumann operator.
        source_operator_type::const_unique_ptr neumann_operator_ = nullptr;

        //! Robin bilinear part operator (int(Gamma)(h*u*v)).
        GlobalVariationalOperatorNS::Mass::const_unique_ptr robin_bilinear_part_operator_ = nullptr;

        //! Robin linear part operator (int(Gamma)(h*uc*v)).
        source_operator_type::const_unique_ptr robin_linear_part_operator_ = nullptr;

        ///@}


        /// \name Parameters used to define TransientSource operators.
        ///@{

        //! Volumic source parameter.
        source_parameter_type::unique_ptr volumic_source_parameter_ = nullptr;

        //! Neumann parameter.
        source_parameter_type::unique_ptr neumann_parameter_ = nullptr;

        //! Robin parameter.
        source_parameter_type::unique_ptr robin_parameter_ = nullptr;

        ///@}


      private:
        /// \name Global vectors and matrices specific to the heat problem.
        ///@{

        //! current volumic source vector.
        GlobalVector::unique_ptr vector_current_volumic_source_ = nullptr;

        //! Neumann BC vector.
        GlobalVector::unique_ptr vector_current_neumann_ = nullptr;

        //! Robin BC vector.
        GlobalVector::unique_ptr vector_current_robin_ = nullptr;

        //! Matrix current conductivity.
        GlobalMatrix::unique_ptr matrix_conductivity_ = nullptr;

        //! Current capacity per time matrix.
        GlobalMatrix::unique_ptr matrix_capacity_ = nullptr;

        //! Current Robin matrix.
        GlobalMatrix::unique_ptr matrix_robin_ = nullptr;


        ///@}


      private:
        //! \name Material parameters.
        ///@{

        //! Diffusion tensor.
        typename scalar_parameter_type::unique_ptr diffusion_tensor_ = nullptr;

        //! Transfer coefficient .
        typename scalar_parameter_type::unique_ptr transfert_coefficient_ = nullptr;

        //! Diffusion Density.
        typename scalar_parameter_type::unique_ptr density_ = nullptr;

        ///@}


      private:
        //! Type of case to run read from the input data file.
        StaticOrDynamic run_case_ = StaticOrDynamic::static_case;

        //! Numbering subset covering the displacement in the main finite element space.
        const NumberingSubset& numbering_subset_;
    };


} // namespace MoReFEM::HeatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ModelInstances/Heat/VariationalFormulation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_HEAT_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
