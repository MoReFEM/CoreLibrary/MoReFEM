-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,

	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 2

} -- transient



-- Sole mesh
Mesh1 = {

	-- Path of the mesh file to use. 
	-- IMPORTANT: if run from preprocessed data, this path won't be used (the already cut mesh will be used 
	-- instead). 
	-- Expected format: "VALUE"
	mesh = "${MOREFEM_ROOT}/Data/Mesh/heat_bar_1D.geo",

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = 'Ensight',

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 1,

	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1

} -- Mesh1

-- Highest dimension geometric elements
-- mesh_index: { 1 }
Domain1 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 1 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain1

-- Domain upon which Neumann boundary condition is applied
-- mesh_index: { 1 }
Domain2 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 0 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 1 },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain2

-- Domain upon which Robin boundary condition is applied
-- mesh_index: { 1 }
Domain3 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 0 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 2 },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain3

-- Domain upon which first Dirichlet boundary condition is applied
-- mesh_index: { 1 }
Domain4 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = {  },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 1 },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain4

-- Domain upon which second Dirichlet boundary condition is applied
-- mesh_index: { 1 }
Domain5 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = {  },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 2 },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain5

-- Covers the while mesh
-- mesh_index: { 1 }
Domain6 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = {  },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain6

-- First Dirichlet boundary condition
-- unknown: 'temperature'
-- domain_index: 4
-- is_mutable: false
EssentialBoundaryCondition1 = {

	-- Comp1, Comp2 or Comp3
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
	component = 'Comp1',

	-- Values at each of the relevant component.
	-- Expected format: { VALUE1, VALUE2, ...}
	value = { 0 }

} -- EssentialBoundaryCondition1

-- Second Dirichlet boundary condition
-- unknown: 'temperature'
-- domain_index: 5
-- is_mutable: false
EssentialBoundaryCondition2 = {

	-- Comp1, Comp2 or Comp3
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
	component = 'Comp1',

	-- Values at each of the relevant component.
	-- Expected format: { VALUE1, VALUE2, ...}
	value = { 0 }

} -- EssentialBoundaryCondition2

-- Finite element space for highest geometric dimension
-- unknown_list: { 'temperature' }
FiniteElementSpace1 = {

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' }

} -- FiniteElementSpace1

-- Finite element space for Neumann boundary condition
-- unknown_list: { 'temperature' }
FiniteElementSpace2 = {

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' }

} -- FiniteElementSpace2

-- Finite element space for Robin boundary condition
-- unknown_list: { 'temperature' }
FiniteElementSpace3 = {

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { 'P1' }

} -- FiniteElementSpace3

-- Linear solver
Petsc1 = {

	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-50,

	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,

	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,

	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'lu', 'none'})
	preconditioner = 'lu',

	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-09,

	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-08,

	-- Solver to use. The list includes all the solvers which have been explicitly activated in MoReFEM. 
	-- However, it does not mean all are actually available in your local environment: some require that some 
	-- external dependencies were added along with PETSc. The ThirdPartyCompilationFactory facility set them up 
	-- by default. 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Gmres', 'Mumps', 'Petsc', 'SuperLU_dist', 'Umfpack' })
	solver = 'SuperLU_dist'

} -- Petsc1

Diffusion = {

	Density = {
		-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
		-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',

		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 1

	}, -- Density

	Tensor1 = {
		-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
		-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',

		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 1

	}, -- Tensor1

	TransfertCoefficient = {
		-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
		-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = 'constant',

		-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
		--  
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
		-- [[ 
		-- function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- ]] 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 0

	}, -- TransfertCoefficient

} -- Diffusion

-- Volumic source
ScalarTransientSource1 = {

	-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
	-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = 'constant',

	-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
	--  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
	-- Expected format: see the variant description...
	value = 10

} -- ScalarTransientSource1

-- Neumann boundary condition
ScalarTransientSource2 = {

	-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
	-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = 'constant',

	-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
	--  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
	-- Expected format: see the variant description...
	value = 100

} -- ScalarTransientSource2

-- Robin boundary condition
ScalarTransientSource3 = {

	-- How is given each component of the parameter (as a constant, as a Lua function, per quadrature point, 
	-- etc...). Choose "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = 'constant',

	-- The value for the parameter, which type depends directly on the nature chosen in the namesake field: 
	--  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
	-- Expected format: see the variant description...
	value = 100

} -- ScalarTransientSource3

-- Initial condition for temperature
InitialCondition1 = {

	-- How is given the parameter (as a constant, per quadrature point, etc...). Choose "ignore" if you do not 
	-- want this parameter (in this case it will stay at nullptr) - in this case all components must be 
	-- 'ignore'. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
	nature = { 'lua_function', 'constant', 'constant' },

	-- For each 'VALUE*n* (see 'Expected format' below), the format may be:  
	--  If nature is 'constant', expected format is VALUE 
	--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
	--  If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block: 
	-- [[ 
	-- function(x, y, z)  
	-- return x + y - z 
	-- end 
	-- ]] 
	-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
	-- Expected format: { VALUE1, VALUE2, ...}
	value = { 
[[
                    function(x, y, z)
                        return 10. * math.exp(-(x-0.5-10/2)^2);
                    end
                ]], 0, 0 }

} -- InitialCondition1

Parallelism = {

	-- What should be done for a parallel run. There are 4 possibilities: 
	--  'Precompute': Precompute the data for a later parallel run and stop once it's done. 
	--  'ParallelNoWrite': Run the code in parallel without using any pre-processed data and do not write down 
	-- the processed data. 
	--  'Parallel': Run the code in parallel without using any pre-processed data and write down the processed 
	-- data. 
	--  'RunFromPreprocessed': Run the code in parallel using pre-processed data. 
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Precompute', 'ParallelNoWrite', 'Parallel', 'RunFromPreprocessed'})
	policy = 'Parallel',

	-- Directory in which parallelism data will be written or read (depending on the policy).
	-- Expected format: "VALUE"
	directory = '${MOREFEM_PREPARTITIONED_DATA_DIR}/Heat/1D'

} -- Parallelism

Restart = {

    -- Time iteration from which we should restart the model (should be present in the first column of the
    -- 'time_iteration.hhdata' file of the previous run. Put 0 if no restart intended. This value makes only
    -- sense in 'RunFromPreprocessed' mode.
    -- Expected format: VALUE
    time_iteration = 0,

    -- Result directory of the previous run (as it was written in Result::OutputDirectory field - so without
    -- the 'Rank_*'subfolder that is automatically added). Leave empty if no restart intended.
    -- Expected format: "VALUE"
    data_directory = '',

    -- Whether restart data are written or not. Such data are written only on root processor and only in binary
    -- format.
    -- Expected format: 'true' or 'false' (without the quote)
    do_write_restart_data = true

} -- Restart

Result = {

	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. You may also use 
	-- ${MOREFEM_START_TIME} in the value which will be replaced by a time under format 
	-- YYYY_MM_DD_HH_MM_SS.Please do not read the value directly from this Lua file: whenever you need the path 
	-- to the result directory, use instead MoReFEMData::GetResultDirectory(). 
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_RESULT_DIR}/Heat/1D",

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,

	-- Defines the solutions output format. Set to false for ascii or true for binary.
	-- Expected format: 'true' or 'false' (without the quote)
	binary_output = false

} -- Result

