// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE model_heat

#include <filesystem>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(const std::string& seq_or_par, const std::string& dimension);


} // namespace

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "1D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "1D");
}


BOOST_FIXTURE_TEST_CASE(sequential_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "2D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "2D");
}


BOOST_FIXTURE_TEST_CASE(sequential_restart_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart", "1D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_restart_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart", "1D");
}


BOOST_FIXTURE_TEST_CASE(sequential_restart_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart", "2D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_restart_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart", "2D");
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    void CommonTestCase(const std::string& seq_or_par, const std::string& dimension)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        std::string root_dir_path;
        std::string output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path = environment.GetEnvironmentVariable("MOREFEM_ROOT"));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR"));

        const FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        const FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir, std::vector<std::string>{ "Sources", "ModelInstances", "Heat", "ExpectedResults", dimension });

        FilesystemNS::Directory obtained_dir(
            output_dir, std::vector<std::string>{ seq_or_par, "Ascii", "Heat", dimension, "Rank_0" });

        BOOST_CHECK(output_dir.DoExist() == true);

        if (!Utilities::String::EndsWith(seq_or_par, "_Restart"))
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata");

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata");

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case");

        std::ostringstream oconv;

        for (auto i = 0UL; i <= 20; ++i)
        {
            oconv.str("");
            oconv << "temperature." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, oconv.str());
        }
    }


} // namespace
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
