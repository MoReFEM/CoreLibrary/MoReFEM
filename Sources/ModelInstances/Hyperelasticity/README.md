A dynamic hyperelastic model with:

- Midpoint time scheme.
- CiarletGeymonat hyperelastic law.

It must be noted a more generic implementation of hyperelastic model do exist in an independent model, in which you may choose half sum/midpoint and any hyperelastic law. It is however more challenging code to read, with arguably some not very user-friendly classes that hinder the understanding, hence the decision to let here as a simple example the current implementation.
