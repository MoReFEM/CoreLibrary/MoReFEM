// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
// IWYU pragma: no_include "Core/MoReFEMData/Internal/AbstractClass.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <tuple>
#include <utility>

#include "ModelInstances/Hyperelasticity/VariationalFormulation.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"

#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/LinearAlgebra/Operations.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "FormulationSolver/Advanced/Restart.hpp"

#include "ModelInstances/Hyperelasticity/InputData.hpp"


namespace MoReFEM::MidpointHyperelasticityNS
{


    VariationalFormulation::~VariationalFormulation() = default;


    VariationalFormulation::VariationalFormulation(
        const NumberingSubset& displacement_numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data),
      displacement_numbering_subset_(displacement_numbering_subset)
    {
        assert(parent::GetTimeManager().IsTimeStepConstant() && "Current instantiation relies on this assumption!");
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();

        parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        parent::AllocateSystemVector(displacement_numbering_subset);

        const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
        matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);

        const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);
        vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_ = std::make_unique<GlobalVector>(system_rhs);
        vector_displacement_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);
        vector_velocity_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
        vector_midpoint_position_ = std::make_unique<GlobalVector>(system_rhs);
        vector_midpoint_velocity_ = std::make_unique<GlobalVector>(system_rhs);
        vector_diff_displacement_ = std::make_unique<GlobalVector>(system_rhs);
    }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        decltype(auto) domain_full_mesh = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));


        solid_ = std::make_unique<Solid<time_manager_type>>(
            morefem_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

        DefineStaticOperators(morefem_data);

        if (parent::GetTimeManager().IsInRestartMode())
        {
            CopySolutionFilesFromOriginalRun();
            LoadRestartData();
        }
    }


    void VariationalFormulation::DefineStaticOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));
        const auto& felt_space_force = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::force));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));


        hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), GetSolid());

        stiffness_operator_ = std::make_unique<StiffnessOperatorType>(felt_space_volume,
                                                                      displacement_ptr,
                                                                      displacement_ptr,
                                                                      GetSolid(),
                                                                      GetTimeManager(),
                                                                      hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                                      nullptr,
                                                                      nullptr);

        decltype(auto) domain_force = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::force));

        using parameter_type = InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>;

        force_parameter_ =
            Init3DCompoundParameterFromInputData<parameter_type>("Surfacic force", domain_force, morefem_data);

        if (force_parameter_ != nullptr)
        {
            surfacic_force_operator_ = std::make_unique<
                GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector,
                                                             typename morefem_data_type::time_manager_type>>(
                felt_space_force, displacement_ptr, *force_parameter_);
        }
    }


    void VariationalFormulation::UpdateVectorsAndMatrices(const GlobalVector& evaluation_state)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            break;
        case StaticOrDynamic::dynamic_:
            UpdateDynamicVectorsAndMatrices(evaluation_state);
            break;
        }

        AssembleOperators(evaluation_state);
    }


    void VariationalFormulation::UpdateDynamicVectorsAndMatrices(const GlobalVector& evaluation_state)
    {
        //    UpdateDisplacementAtNewtonIteration();

        UpdateVelocityAtNewtonIteration(evaluation_state);

        auto& midpoint_position = GetNonCstVectorMidpointPosition();

        midpoint_position.Copy(GetVectorCurrentDisplacement());
        GlobalLinearAlgebraNS::AXPY(1., evaluation_state, midpoint_position);

        midpoint_position.Scale(0.5); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

        midpoint_position.UpdateGhosts();

        auto& midpoint_velocity = GetNonCstVectorMidpointVelocity();

        midpoint_velocity.Copy(evaluation_state);

        GlobalLinearAlgebraNS::AXPY(-1., GetVectorCurrentDisplacement(), midpoint_velocity);

        midpoint_velocity.Scale(1. / GetTimeManager().GetTimeStep());

        midpoint_velocity.UpdateGhosts();

        GetNonCstVectorCurrentDisplacement().UpdateGhosts();
    }


    void VariationalFormulation::UpdateVelocityAtNewtonIteration(const GlobalVector& evaluation_state)
    {
        auto& velocity_at_newton_iteration = GetNonCstVectorVelocityAtNewtonIteration();
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

        if (GetSnes().GetSnesIteration() == 0)
        {
            velocity_at_newton_iteration.Copy(GetVectorCurrentVelocity());
        } else
        {
            const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();

            auto& diff_displacement = GetNonCstVectorDiffDisplacement();

            diff_displacement.Copy(evaluation_state);
            GlobalLinearAlgebraNS::AXPY(-1., displacement_previous_time_iteration, diff_displacement);

            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            diff_displacement.Scale(2. / GetTimeManager().GetTimeStep());
            velocity_at_newton_iteration.Copy(diff_displacement);
            GlobalLinearAlgebraNS::AXPY(-1., velocity_previous_time_iteration, velocity_at_newton_iteration);
        }

        velocity_at_newton_iteration.UpdateGhosts();
    }


    void VariationalFormulation::AssembleOperators(const GlobalVector& evaluation_state)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            AssembleNewtonStaticOperators(evaluation_state);
            break;
        case StaticOrDynamic::dynamic_:
            AssembleNewtonDynamicOperators();
            break;
        }
    }


    void VariationalFormulation::AssembleNewtonStaticOperators(const GlobalVector& evaluation_state)
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            DisplacementGlobalVector(evaluation_state));
        }

        const std::size_t newton_iteration = GetSnes().GetSnesIteration();

        if (newton_iteration == 0UL)
        {
            auto& vector_surfacic_force = GetNonCstVectorSurfacicForce();

            vector_surfacic_force.ZeroEntries();

            GlobalVectorWithCoefficient vec(vector_surfacic_force, 1.);

            const double time = parent::GetTimeManager().GetTime();

            GetSurfacicForceOperator().Assemble(std::make_tuple(std::ref(vec)), time);
        }
    }


    void VariationalFormulation::AssembleNewtonDynamicOperators()
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            const GlobalVector& displacement_vector = GetVectorMidpointPosition();

            GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            DisplacementGlobalVector(displacement_vector));
        }
    }


    void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {
        UpdateVectorsAndMatrices(evaluation_state);

        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticResidual(residual);
            break;
        case StaticOrDynamic::dynamic_:
            ComputeDynamicResidual(evaluation_state, residual);
            break;
        }

        ApplyEssentialBoundaryCondition(residual);
    }


    void VariationalFormulation::ComputeStaticResidual(GlobalVector& residual)
    {
        GlobalLinearAlgebraNS::AXPY(1., GetVectorStiffnessResidual(), residual);

        GlobalLinearAlgebraNS::AXPY(-1., GetVectorSurfacicForce(), residual);
    }


    void VariationalFormulation::ComputeDynamicResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {
        GlobalLinearAlgebraNS::AXPY(1., GetVectorStiffnessResidual(), residual);

        const auto& time_manager = GetTimeManager();
        const double time_step = time_manager.GetTimeStep();
        const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
        auto& diff_displacement = GetNonCstVectorDiffDisplacement();
        diff_displacement.Copy(evaluation_state);

        GlobalLinearAlgebraNS::AXPY(-1., displacement_previous_time_iteration, diff_displacement);

        GlobalLinearAlgebraNS::AXPY(-time_step, velocity_previous_time_iteration, diff_displacement);

        GlobalLinearAlgebraNS::MatMultAdd(GetMatrixMassPerSquareTimeStep(), diff_displacement, residual, residual);
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters) - true but strong type hindered by not movable object
    void VariationalFormulation::ComputeTangent([[maybe_unused]] const GlobalVector& evaluation_state,
                                                GlobalMatrix& tangent,
                                                [[maybe_unused]] GlobalMatrix& preconditioner)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticTangent(tangent);
            break;
        case StaticOrDynamic::dynamic_:
            ComputeDynamicTangent(tangent);
            break;
        }

        ApplyEssentialBoundaryCondition(tangent);
    }


    void VariationalFormulation::ComputeStaticTangent(GlobalMatrix& tangent)
    {
        tangent.ZeroEntries();

        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent);
    }


    void VariationalFormulation::ComputeDynamicTangent(GlobalMatrix& tangent)
    {
        tangent.ZeroEntries();

#ifndef NDEBUG
        AssertSameNumberingSubset(GetMatrixTangentStiffness(), tangent);
        AssertSameNumberingSubset(GetMatrixMassPerSquareTimeStep(), tangent);
#endif // NDEBUG

        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., GetMatrixMassPerSquareTimeStep(), tangent);

        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(0.5, GetMatrixTangentStiffness(), tangent);
    }


    void VariationalFormulation::PrepareDynamicRuns()
    {
        DefineDynamicOperators();

        AssembleDynamicOperators();

        if (!parent::GetTimeManager().IsInRestartMode())
            UpdateForNextTimeStep();
    }


    void VariationalFormulation::DefineDynamicOperators()
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));

        namespace GVO = GlobalVariationalOperatorNS;

        mass_operator_ = std::make_unique<GVO::Mass>(felt_space_volume, displacement_ptr, displacement_ptr);
    }


    void VariationalFormulation::AssembleDynamicOperators()
    {
        const double volumic_mass = GetSolid().GetVolumicMass().GetConstantValue();

        const double time_step = GetTimeManager().GetTimeStep();

        const double mass_coefficient = 2. * volumic_mass / (time_step * time_step);

        GlobalMatrixWithCoefficient matrix(GetNonCstMatrixMassPerSquareTimeStep(), mass_coefficient);

        GetMassOperator().Assemble(std::make_tuple(std::ref(matrix)));
    }


    void VariationalFormulation::UpdateForNextTimeStep()
    {
        UpdateDisplacementBetweenTimeStep();

        UpdateVelocityBetweenTimeStep();

        // ComputeGuessForNextTimeStep();
    }


    void VariationalFormulation::UpdateDisplacementBetweenTimeStep()
    {
        GetNonCstVectorCurrentDisplacement().Copy(GetSystemSolution(GetDisplacementNumberingSubset()));

        GetNonCstVectorCurrentDisplacement().UpdateGhosts();
    }


    void VariationalFormulation::UpdateVelocityBetweenTimeStep()
    {
        GetNonCstVectorCurrentVelocity().Copy(GetVectorVelocityAtNewtonIteration());

        GetNonCstVectorCurrentVelocity().UpdateGhosts();
    }


    void VariationalFormulation::ComputeGuessForNextTimeStep()
    {
        const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
        auto& system_solution = GetNonCstSystemSolution(displacement_numbering_subset);
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

        GlobalLinearAlgebraNS::AXPY(GetTimeManager().GetTimeStep(), velocity_previous_time_iteration, system_solution);

        system_solution.UpdateGhosts();
    }


    void VariationalFormulation::WriteRestartData() const
    {
        decltype(auto) time_manager = parent::GetTimeManager();
        decltype(auto) god_of_dof = parent::GetGodOfDof();

        // Note: strictly speaking, we could avoid this one - solution is already written outside of
        // restart data. It is easier to do so here but if at some point I/O is a real bottleneck
        // we could restart from the solution already written. The function to reload it however
        // would be a tiny bit more complex - as said solution may be written in ascii or binary
        // whereas here we do it in binary mode in any case.
        Advanced::RestartNS::WriteDataForRestart(
            time_manager, god_of_dof, "displacement", GetVectorCurrentDisplacement());
        Advanced::RestartNS::WriteDataForRestart(time_manager, god_of_dof, "velocity", GetVectorCurrentVelocity());
    }


    void VariationalFormulation::LoadRestartData()
    {
        decltype(auto) time_manager = parent::GetTimeManager();

        decltype(auto) god_of_dof = parent::GetGodOfDof();

        Advanced::RestartNS::LoadGlobalVectorFromData(
            time_manager, god_of_dof, "displacement", GetNonCstVectorCurrentDisplacement());
        Advanced::RestartNS::LoadGlobalVectorFromData(
            time_manager, god_of_dof, "velocity", GetNonCstVectorCurrentVelocity());

        this->GetNonCstSystemSolution(GetDisplacementNumberingSubset()).Copy(GetVectorCurrentDisplacement());
    }


    void VariationalFormulation::CopySolutionFilesFromOriginalRun()
    {
        decltype(auto) numbering_subset =
            GetGodOfDof().GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));

        Advanced::RestartNS::CopySolutionFilesFromOriginalRun(
            parent::GetTimeManager(), parent::GetGodOfDof(), numbering_subset);
    }


} // namespace MoReFEM::MidpointHyperelasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
