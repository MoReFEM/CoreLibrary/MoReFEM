// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_HYPERELASTICITY_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM_MODELINSTANCES_HYPERELASTICITY_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <ostream>
#include <type_traits>
#include <utility>

#include "Utilities/Containers/Print.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/Solver/Solver.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hpp"
#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
#include "FormulationSolver/VariationalFormulation.hpp"

#include "ModelInstances/Hyperelasticity/InputData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::GlobalVariationalOperatorNS { struct DisplacementTag; }
namespace MoReFEM::Internal::SolverNS { template <class VariationalFormulationT> struct SnesInterface; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::MidpointHyperelasticityNS
{


    //! \copydoc doxygen_hide_simple_varf
    class VariationalFormulation final
    : public MoReFEM::VariationalFormulation<VariationalFormulation,
                                             EnumUnderlyingType(SolverIndex::solver),
                                             time_manager_type,
                                             enable_non_linear_solver::yes>,
      public FormulationSolverNS::HyperelasticLaw<VariationalFormulation,
                                                  HyperelasticLawNS::CiarletGeymonat<time_manager_type>>
    {
      private:
        //! \copydoc doxygen_hide_alias_self
        using self = VariationalFormulation;

        //! Alias to the parent class.
        using parent = MoReFEM::VariationalFormulation<VariationalFormulation,
                                                       EnumUnderlyingType(SolverIndex::solver),
                                                       time_manager_type,
                                                       enable_non_linear_solver::yes>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;

        //! Alias to hyperlastic law parent,
        using hyperelastic_law_parent =
            FormulationSolverNS::HyperelasticLaw<VariationalFormulation,
                                                 HyperelasticLawNS::CiarletGeymonat<time_manager_type>>;

        //! Alias to the viscoelasticity policy used.
        using ViscoelasticityPolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<
                time_manager_type>;

        //! Alias to the active stress policy used.
        using InternalVariablePolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<
                time_manager_type>;

        //! Alias to the hyperelasticity policy used.
        using hyperelasticity_policy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS ::Hyperelasticity<
                typename hyperelastic_law_parent::hyperelastic_law_type>;

        //! Alias to the type of the source parameter.
        using force_parameter_type =
            Parameter<ParameterNS::Type::vector, LocalCoords, time_manager_type, ParameterNS::TimeDependencyNS::None>;

        //! Alias on a pair of Unknown.
        using UnknownPair = std::pair<const Unknown&, const Unknown&>;

        //! Strong type for displacement global vectors.
        using DisplacementGlobalVector =
            StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

        //! Friendship to the class which implements the prototyped functions required by Petsc Snes algorithm.
        friend struct Internal::SolverNS::SnesInterface<self>;

      public:
        //! Alias to the stiffness operator type used.
        using StiffnessOperatorType =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor<hyperelasticity_policy,
                                                                          ViscoelasticityPolicy,
                                                                          InternalVariablePolicy,
                                                                          time_manager_type>;

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      public:
        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_varf_constructor
        //! \param[in] displacement_numbering_subset The \a NumberingSubset used in this variational formulation
        //! to keep track of displacement.
        explicit VariationalFormulation(const NumberingSubset& displacement_numbering_subset,
                                        const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type& morefem_data);

        //! Destructor.
        ~VariationalFormulation() override;

        //! \copydoc doxygen_hide_copy_constructor
        VariationalFormulation(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VariationalFormulation(VariationalFormulation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

        ///@}

        /*!
         * \brief Get the displacement numbering subset relevant for this VariationalFormulation.
         *
         * There is a more generic accessor in the base class but its use is more unwieldy.
         *
         * \return \a NumberingSubset related to displacement.
         */
        const NumberingSubset& GetDisplacementNumberingSubset() const noexcept;

        //! Update for next time step. (not called after each dynamic iteration).
        void UpdateForNextTimeStep();

        //! Prepare dynamic runs.
        void PrepareDynamicRuns();

      public:
        /*!
         * \brief Write data that would be required to run later in restart mode.
         */
        void WriteRestartData() const;

      private:
        /*!
         * \brief Load restart data from a previous run.
         */
        void LoadRestartData();

        /*!
         * \brief In restart mode, copy the data that were computed in the original run and won't be recomputed.
         */
        void CopySolutionFilesFromOriginalRun();


      private:
        /// \name CRTP-required methods.
        ///@{

        //! \copydoc doxygen_hide_varf_suppl_init
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();

        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

        ///@}


      private:
        //! \copydoc doxygen_hide_compute_residual
        void ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual);

        //! \copydoc doxygen_hide_compute_tangent
        void ComputeTangent(const GlobalVector& evaluation_state, GlobalMatrix& tangent, GlobalMatrix& preconditioner);

        /*!
         * \brief Assemble method for the mass operator.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleStaticOperators(const GlobalVector& evaluation_state);


        /*!
         * \brief Assemble method for the mass operator.
         */
        void AssembleDynamicOperators();

        /*!
         * \brief Assemble method for all the dynamic operators.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Assemble method for all the static operators.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleNewtonStaticOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Assemble method for all the dynamic operators.
         */
        void AssembleNewtonDynamicOperators();

        /*!
         * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
         * and the residual.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void UpdateVectorsAndMatrices(const GlobalVector& evaluation_state);

        /*!
         * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
         * and the residual.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void UpdateDynamicVectorsAndMatrices(const GlobalVector& evaluation_state);

        /*!
         * \brief Compute the tangent for a static case.
         *
         * \copydoc doxygen_hide_out_tangent_arg
         */
        void ComputeStaticTangent(GlobalMatrix& tangent);

        /*!
         * \brief Compute the residual for a static case.
         *
         * \copydoc doxygen_hide_out_residual_arg
         */
        void ComputeStaticResidual(GlobalVector& residual);

        /*!
         * \brief Compute the tangent for a dynamic case.
         *
         * \copydoc doxygen_hide_out_tangent_arg
         */
        void ComputeDynamicTangent(GlobalMatrix& tangent);

        /*!
         * \brief Compute the residual for a dynamic case.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         * \copydoc doxygen_hide_out_residual_arg
         */
        void ComputeDynamicResidual(const GlobalVector& evaluation_state, GlobalVector& residual);

        //! Update current displacement. Already called in UpdateForNextTimeStep().
        void UpdateDisplacementBetweenTimeStep();

        //! Update current displacement. Already called in UpdateForNextTimeStep().
        void UpdateVelocityBetweenTimeStep();

        //! Compute the guess for next time step with the new velocity.
        void ComputeGuessForNextTimeStep();

      private:
        /*!
         * \brief Define the properties of all the static global variational operators involved.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        void DefineStaticOperators(const morefem_data_type& morefem_data);

        /*!
         * \brief Define the properties of all the dynamic global variational operators involved.
         */
        void DefineDynamicOperators();

        //! Get the mass per square time step operator.
        const GlobalVariationalOperatorNS::Mass& GetMassOperator() const noexcept;

        //! Get the hyperelastic stiffness operator.
        const StiffnessOperatorType& GetStiffnessOperator() const noexcept;

        //! Accessor to the surfacic source operator.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperator() const noexcept;

      private:
        /// \name Global variational operators.
        ///@{

        //! Mass operator.
        GlobalVariationalOperatorNS::Mass::const_unique_ptr mass_operator_ = nullptr;

        //! Stiffness operator.
        StiffnessOperatorType::const_unique_ptr stiffness_operator_ = nullptr;

        //! Volumic source operator.
        GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>::const_unique_ptr
            surfacic_force_operator_ = nullptr;

        ///@}

      private:
        /// \name Accessors to the global vectors and matrices managed by the class.
        ///@{

        //! Accessor.
        const GlobalMatrix& GetMatrixMassPerSquareTimeStep() const noexcept;

        //! Accessor.
        GlobalMatrix& GetNonCstMatrixMassPerSquareTimeStep() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorStiffnessResidual() const noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorStiffnessResidual() noexcept;

        //! Accessor.
        const GlobalMatrix& GetMatrixTangentStiffness() const noexcept;

        //! Accessor.
        GlobalMatrix& GetNonCstMatrixTangentStiffness() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForce() const noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorSurfacicForce() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorCurrentDisplacement() const noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorCurrentDisplacement() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorCurrentVelocity() const noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorCurrentVelocity() noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorVelocityAtNewtonIteration() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorVelocityAtNewtonIteration() const noexcept;

        //! Accessor.
        const GlobalVector& GetVectorMidpointPosition() const noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorMidpointPosition() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorMidpointVelocity() const noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorMidpointVelocity() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorDiffDisplacement() const noexcept;

        //! Accessor.
        GlobalVector& GetNonCstVectorDiffDisplacement() noexcept;

        ///@}

        //! Access to the solid.
        const Solid<time_manager_type>& GetSolid() const noexcept;

      private:
        /*!
         * \brief Update the vector that contains the values seeked at the moment the residual is evaluated.
         *
         */
        void UpdateDisplacementAtNewtonIteration();

        /*!
         * \brief Update the vector that contains the values seeked at the moment the residual is evaluated.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void UpdateVelocityAtNewtonIteration(const GlobalVector& evaluation_state);

      private:
        /// \name Global vectors and matrices specific to the problem.
        ///@{

        //! Stiffness residual vector.
        GlobalVector::unique_ptr vector_stiffness_residual_ = nullptr;

        //! Evaluation state of the residual of the problem (only useful in  SNES method)
        GlobalVector::unique_ptr vector_surfacic_force_ = nullptr;

        //! Mass matrix.
        GlobalMatrix::unique_ptr matrix_mass_per_square_time_step_ = nullptr;

        //! Matrix stiffness tangent.
        GlobalMatrix::unique_ptr matrix_tangent_stiffness_ = nullptr;

        //! Evaluation state of the residual of the problem (only useful in SNES method)
        GlobalVector::unique_ptr vector_displacement_at_newton_iteration_ = nullptr;

        //! Evaluation state of the residual of the problem (only useful in SNES method)
        GlobalVector::unique_ptr vector_velocity_at_newton_iteration_ = nullptr;

        //! Velocity from previous time iteration.
        GlobalVector::unique_ptr vector_current_velocity_ = nullptr;

        //! Displacement from previous time iteration.
        GlobalVector::unique_ptr vector_current_displacement_ = nullptr;

        //! Midpoint position.
        GlobalVector::unique_ptr vector_midpoint_position_ = nullptr;

        //! Difference displacement Yn+1 - Yn. Here just to avoid allocate it every time step.
        GlobalVector::unique_ptr vector_diff_displacement_ = nullptr;

        //! Midpoint velocity.
        GlobalVector::unique_ptr vector_midpoint_velocity_ = nullptr;

        ///@}

      private:
        /// \name Numbering subsets used in the formulation.
        ///@{

        //! The only \a NumberingSubset used in the formulation.
        const NumberingSubset& displacement_numbering_subset_;

        ///@}

      private:
        //! Material parameters of the solid.
        typename Solid<time_manager_type>::const_unique_ptr solid_ = nullptr;

        //! Force parameter for the static force.
        force_parameter_type::unique_ptr force_parameter_ = nullptr;
    };


} // namespace MoReFEM::MidpointHyperelasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ModelInstances/Hyperelasticity/VariationalFormulation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_HYPERELASTICITY_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
