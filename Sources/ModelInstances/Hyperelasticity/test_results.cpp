// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE model_hyperelasticity

#include <filesystem>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& solver_name = "");


} // namespace

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq");
}


BOOST_FIXTURE_TEST_CASE(mpi4, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_restart, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_restart, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart");
}


BOOST_FIXTURE_TEST_CASE(mpi4_from_partitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData");
}


#ifdef MOREFEM_WITH_MUMPS
BOOST_FIXTURE_TEST_CASE(sequential_mumps, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Mumps");
}


#if not defined(__APPLE__)
// Mumps no longer correctly supported on macOS in parallel; see #1705
BOOST_FIXTURE_TEST_CASE(mpi4_mumps, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "Mumps");
}
#endif
#endif // PetscDefined(HAVE_MUMPS)


BOOST_FIXTURE_TEST_CASE(sequential_gmres, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Gmres");
}

// \todo 1783 Investigate to make it work!
// BOOST_FIXTURE_TEST_CASE(parallel_gmres, TestNS::FixtureNS::TestEnvironment)
//{
//    CommonTestCase("Mpi4", "Gmres");
//}

#ifdef MOREFEM_WITH_MUMPS_UMFPACK
BOOST_FIXTURE_TEST_CASE(sequential_umfpack, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Umfpack");
}
#endif // PetscDefined(HAVE_UMFPACK)


BOOST_FIXTURE_TEST_CASE(sequential_petsc_solver, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Petsc");
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& solver_name)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        std::string root_dir_path;
        std::string output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path = environment.GetEnvironmentVariable("MOREFEM_ROOT"));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR"));

        const FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        const FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);
        BOOST_CHECK(output_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir, std::vector<std::string>{ "Sources", "ModelInstances", "Hyperelasticity", "ExpectedResults" });

        std::string model_name = "MidpointHyperelasticity";

        if (!solver_name.empty())
            model_name += "_with_" + solver_name;

        FilesystemNS::Directory obtained_dir(output_dir,
                                             std::vector<std::string>{ seq_or_par, "Ascii", model_name, "Rank_0" });

        // No symbolic link to the special Lua file used as input in ExpectedResults (would need dedicated function
        // as produced one would still call it 'input_data.lua', but frankly it's probably not worth the time to write
        // it...
        if (solver_name.empty()
            && (!Utilities::String::EndsWith(seq_or_par, "_FromPrepartitionedData")
                && (!Utilities::String::EndsWith(seq_or_par, "_Restart"))))
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata");

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata");

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case");

        std::ostringstream oconv;

        for (auto i = 0; i <= 5; ++i)
        {
            oconv.str("");
            oconv << "displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, oconv.str());
        }
    }


} // namespace
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
