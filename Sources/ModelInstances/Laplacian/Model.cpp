// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "ModelInstances/Laplacian/Model.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"


namespace MoReFEM::LaplacianNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const GodOfDof& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();
        const NumberingSubset& numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        {
            const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::first)) };

            variational_formulation_ = std::make_unique<VariationalFormulation>(god_of_dof, bc_list, morefem_data);
        }

        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);
        variational_formulation.WriteSolution(parent::GetTimeManager(), numbering_subset);
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize() const
    { }


} // namespace MoReFEM::LaplacianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
