// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_LAPLACIAN_MODEL_DOT_HXX_
#define MOREFEM_MODELINSTANCES_LAPLACIAN_MODEL_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/Laplacian/Model.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <string>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::LaplacianNS { class VariationalFormulation; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::LaplacianNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Laplacian");
        return name;
    }


    inline const VariationalFormulation& Model::GetVariationalFormulation() const
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
    {
        return const_cast<VariationalFormulation&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false;
    }


    inline void Model::SupplInitializeStep()
    { }


} // namespace MoReFEM::LaplacianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_LAPLACIAN_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
