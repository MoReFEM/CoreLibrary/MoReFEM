// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
// IWYU pragma: no_include "Core/MoReFEMData/Internal/AbstractClass.hpp"

#include <cstddef>
#include <functional>
#include <iostream>
#include <memory>
#include <tuple>
#include <utility>

#include "ModelInstances/Laplacian/VariationalFormulation.hpp"

#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "FormulationSolver/Enum.hpp"

#include "ModelInstances/Laplacian/InputData.hpp"


namespace MoReFEM::LaplacianNS
{


    VariationalFormulation::~VariationalFormulation() = default;


    VariationalFormulation ::VariationalFormulation(
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data)
    { }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& monolithic_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));
        const auto& pressure = UnknownManager::GetInstance().GetUnknown(AsUnknownId(UnknownIndex::pressure));

        constexpr auto initial_condition_index = EnumUnderlyingType(InitialConditionIndex::initial_condition);

        parent::SetInitialSystemSolution<initial_condition_index>(
            morefem_data, monolithic_numbering_subset, pressure, felt_space_volume);

        DefineOperators(morefem_data);
        AssembleStaticOperators();

        ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
            monolithic_numbering_subset, monolithic_numbering_subset);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
        auto& time_keeper = TimeKeep::GetInstance();
        time_keeper.PrintTimeElapsed("Before solving with factorization.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

        if (GetMpi().IsRootProcessor())
            std::cout << "Before solving with factorization." << '\n';

        SolveLinear<IsFactorized::no>(monolithic_numbering_subset, monolithic_numbering_subset);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
        time_keeper.PrintTimeElapsed("After solving with factorization.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

        if (GetMpi().IsRootProcessor())
            std::cout << "After solving with factorization." << '\n';

        const std::size_t Nsolve = 20;

        for (std::size_t i = 0; i < Nsolve; ++i)
        {
            SolveLinear<IsFactorized::yes>(monolithic_numbering_subset, monolithic_numbering_subset);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
            time_keeper.PrintTimeElapsed("After solving without factorization.");
#endif // MOREFEM_EXTENDED_TIME_KEEP
        }
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& monolithic_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        parent::AllocateSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);
        parent::AllocateSystemVector(monolithic_numbering_subset);
    }


    void VariationalFormulation::DefineOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        decltype(auto) volume_domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::volume));

        const auto& pressure = UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::pressure));

        namespace GVO = GlobalVariationalOperatorNS;

        grad_grad_operator_ = std::make_unique<GVO::GradPhiGradPhi>(felt_space_volume, pressure, pressure);

        {
            using parameter_type =
                InputDataNS::ScalarTransientSource<EnumUnderlyingType(SourceIndexList::volumic_source)>;

            volumic_source_parameter_ =
                InitScalarParameterFromInputData<parameter_type>("Volumic source", volume_domain, morefem_data);

            if (volumic_source_parameter_ != nullptr)
            {
                volumic_source_operator_ =
                    std::make_unique<source_operator_type>(felt_space_volume, pressure, *volumic_source_parameter_);
            }
        } // namespace InputDataNS;
    }


    void VariationalFormulation::AssembleStaticOperators()
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& monolithic_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        auto& system_matrix = GetNonCstSystemMatrix(monolithic_numbering_subset, monolithic_numbering_subset);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
        auto& time_keeper = TimeKeep::GetInstance();
#endif // MOREFEM_EXTENDED_TIME_KEEP

        {
            GlobalMatrixWithCoefficient matrix(system_matrix, 1.);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
            time_keeper.PrintTimeElapsed("Before assembling laplacian operator.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

            if (GetMpi().IsRootProcessor())
                std::cout << "Before assembling laplacian operator." << '\n';

            GetGradGradOperator().Assemble(std::make_tuple(std::ref(matrix)));
#ifdef MOREFEM_EXTENDED_TIME_KEEP
            time_keeper.PrintTimeElapsed("After assembling laplacian operator.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

            if (GetMpi().IsRootProcessor())
                std::cout << "After assembling laplacian operator." << '\n';
        }

        if (volumic_source_parameter_ != nullptr)
        {
            auto& rhs = GetNonCstSystemRhs(monolithic_numbering_subset);

            {
                GlobalVectorWithCoefficient vector(rhs, 1.);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("Before assembling rhs.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

                if (GetMpi().IsRootProcessor())
                    std::cout << "Before assembling rhs." << '\n';

                GetVolumicSourceOperator().Assemble(std::make_tuple(std::ref(vector)), 0.);

#ifdef MOREFEM_EXTENDED_TIME_KEEP
                time_keeper.PrintTimeElapsed("After assembling rhs.");
#endif // MOREFEM_EXTENDED_TIME_KEEP

                if (GetMpi().IsRootProcessor())
                    std::cout << "After assembling rhs." << '\n';
            }
        }
    }


} // namespace MoReFEM::LaplacianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
