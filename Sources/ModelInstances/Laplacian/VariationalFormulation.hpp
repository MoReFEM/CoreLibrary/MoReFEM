// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_LAPLACIAN_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM_MODELINSTANCES_LAPLACIAN_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"

#include <map>
#include <ostream>
#include <type_traits>

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "FormulationSolver/VariationalFormulation.hpp"

#include "ModelInstances/Laplacian/InputData.hpp"


namespace MoReFEM::LaplacianNS
{


    //! Variational formulaton to solve for a laplacian problem with Dirichlet BC.
    class VariationalFormulation final : public MoReFEM::VariationalFormulation<VariationalFormulation,
                                                                                EnumUnderlyingType(SolverIndex::solver),
                                                                                time_manager_type>
    {
      private:
        //! \copydoc doxygen_hide_alias_self
        using self = VariationalFormulation;

        //! Alias to the parent class.
        using parent =
            MoReFEM::VariationalFormulation<self, EnumUnderlyingType(SolverIndex::solver), time_manager_type>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;


      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to source operator type.
        using source_operator_type =
            GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::scalar, time_manager_type>;

        //! Alias to source parameter type.
        using source_parameter_type = ScalarParameter<time_manager_type>;

      public:
        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_varf_constructor
        explicit VariationalFormulation(const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type& morefem_data);

        //! Destructor.
        ~VariationalFormulation() override;

        //! \copydoc doxygen_hide_copy_constructor
        VariationalFormulation(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VariationalFormulation(VariationalFormulation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

        ///@}

      private:
        /*!
         * \brief Assemble method for all the static operators except the capacity.
         */
        void AssembleStaticOperators();

      private:
        /// \name CRTP-required methods.
        ///@{

        //! \copydoc doxygen_hide_varf_suppl_init
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();


        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

        ///@}


      private:
        /*!
         * \brief Define the properties of all the global variational operators involved.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        void DefineOperators(const morefem_data_type& morefem_data);

        //! Get the conductivity operator.
        const GlobalVariationalOperatorNS::GradPhiGradPhi& GetGradGradOperator() const noexcept;

        //! Get the volumic source operator.
        const source_operator_type& GetVolumicSourceOperator() const noexcept;

      private:
        /// \name Global variational operators.
        ///@{


        //! Conductivity operator.
        GlobalVariationalOperatorNS::GradPhiGradPhi::const_unique_ptr grad_grad_operator_ = nullptr;

        //! Volumic source operator.
        source_operator_type::const_unique_ptr volumic_source_operator_ = nullptr;

        ///@}


        /// \name Parameters used to define TransientSource operators.
        ///@{

        //! Volumic source parameter.
        source_parameter_type::unique_ptr volumic_source_parameter_ = nullptr;

        ///@}
    };


} // namespace MoReFEM::LaplacianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ModelInstances/Laplacian/VariationalFormulation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_LAPLACIAN_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
