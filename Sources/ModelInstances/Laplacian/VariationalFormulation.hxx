// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_LAPLACIAN_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_MODELINSTANCES_LAPLACIAN_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/Laplacian/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::GlobalVariationalOperatorNS { class GradPhiGradPhi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::LaplacianNS
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
    VariationalFormulation::GetGradGradOperator() const noexcept
    {
        assert(!(!grad_grad_operator_));
        return *grad_grad_operator_;
    }


    inline const VariationalFormulation::source_operator_type&
    VariationalFormulation::GetVolumicSourceOperator() const noexcept
    {
        assert(!(!volumic_source_operator_));
        return *volumic_source_operator_;
    }


} // namespace MoReFEM::LaplacianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_LAPLACIAN_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
