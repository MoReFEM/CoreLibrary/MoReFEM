##########################################
# Library to compile once files that will be used in several executables.
# There are also non source files (README, lua files, etc...) so that
# they appear correctly when CMake generates project for an IDE.
##########################################

add_library(MoReFEM4NonLinearShell_lib ${LIBRARY_TYPE} )

target_sources(MoReFEM4NonLinearShell_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua   
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hxx
 		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx
)

target_link_libraries(MoReFEM4NonLinearShell_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)

morefem_organize_IDE(MoReFEM4NonLinearShell_lib ModelInstances/NonLinearShell ModelInstances/NonLinearShell)                       

morefem_install(MoReFEM4NonLinearShell_lib)

##########################################
# Executable to run the model
##########################################

add_executable(MoReFEM4NonLinearShell ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4NonLinearShell
                      MoReFEM4NonLinearShell_lib)
apply_lto_if_supported(MoReFEM4NonLinearShell)

morefem_organize_IDE(MoReFEM4NonLinearShell ModelInstances/NonLinearShell ModelInstances/NonLinearShell)   

morefem_install(MoReFEM4NonLinearShell)


##########################################
# Executable to produce outputs in Ensight format
##########################################

add_executable(MoReFEM4NonLinearShellEnsightOutput
               ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)

target_link_libraries(MoReFEM4NonLinearShellEnsightOutput
                      MoReFEM4NonLinearShell_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(MoReFEM4NonLinearShellEnsightOutput ModelInstances/NonLinearShell ModelInstances/NonLinearShell)                         

morefem_install(MoReFEM4NonLinearShellEnsightOutput)


##########################################
# Executable to update a input Lua file
##########################################

add_executable(MoReFEM4NonLinearShellUpdateLuaFile
               ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)

               
target_link_libraries(MoReFEM4NonLinearShellUpdateLuaFile
                      MoReFEM4NonLinearShell_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(MoReFEM4NonLinearShellUpdateLuaFile ModelInstances/NonLinearShell ModelInstances/NonLinearShell)   

morefem_install(MoReFEM4NonLinearShellUpdateLuaFile)


##########################################
# Tests
##########################################

# #1570 Parallel mode not yet supported.
morefem_test_run_model_in_sequential_mode(NAME NonLinearShell
                                          NO_ENSIGHT_OUTPUT
                                          LUA  ${MOREFEM_ROOT}/Sources/ModelInstances/NonLinearShell/demo.lua
                                          EXE MoReFEM4NonLinearShell
                                          TIMEOUT 2000)

##########################################
# Executable and test which check reference results are properly found.
##########################################

# Note: ViZiR is used by this executable to compare data; it is slightly different to what is done
# for the other models embedded in the library.

add_executable(MoReFEM4NonLinearShellModelCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
target_link_libraries(MoReFEM4NonLinearShellModelCheckResults
                      MoReFEM4NonLinearShell_lib
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(MoReFEM4NonLinearShellModelCheckResults ModelInstances/NonLinearShell ModelInstances/NonLinearShell)

morefem_boost_test_check_results(NAME NonLinearShellModel
                                 EXE MoReFEM4NonLinearShellModelCheckResults
                                 TIMEOUT 20)
