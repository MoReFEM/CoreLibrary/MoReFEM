// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_NONLINEARSHELL_INPUTDATA_DOT_HPP_
#define MOREFEM_MODELINSTANCES_NONLINEARSHELL_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Geometry/PseudoNormals.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
// IWYU pragma: end_exports

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp"
#include "Core/TimeManager/TimeManager.hpp"


namespace MoReFEM::MidpointNonLinearShellNS
{


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { displacement = 1 };

    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { displacement = 1 };

    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };

    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        full_mesh = 1,
        volume = 2,
        force = 3,
        dirichlet = 4,
    };

    //! \copydoc doxygen_hide_boundary_condition_enum
    enum class BoundaryConditionIndex : std::size_t { clamped = 1 };

    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { volume = 1, force = 2 };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex { solver = 1 };

    //! \copydoc doxygen_hide_source_enum
    enum class ForceIndexList { surfacic = 1 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::volume),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::force),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::clamped),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::volume),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::force),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(ForceIndexList::surfacic),

        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
        InputDataNS::Solid::CheckInvertedElements,

        InputDataNS::Parallelism,
        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::displacement),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::volume),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::force),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::displacement),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::volume),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::force),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::clamped),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(ForceIndexList::surfacic)
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<TimeManagerNS::Policy::ConstantTimeStep<>>;


    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::model>;


} // namespace MoReFEM::MidpointNonLinearShellNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_NONLINEARSHELL_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
