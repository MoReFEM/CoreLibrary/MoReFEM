// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Utilities/InputData/ModelSettings.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "ModelInstances/NonLinearShell/InputData.hpp"

namespace MoReFEM::MidpointNonLinearShellNS
{


    void ModelSettings::Init()
    {
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>>(
                "Numbering subset for displacement");
        }

        // ****** Unknown ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>({ "Displacement" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Name>("displacement");
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Nature>("vectorial");
        }

        // ****** Domain ******
        {
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ "Full mesh" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>(
                { "Domain upon which volumic force is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::force)>>(
                { "Domain upon which surfacic force is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>>(
                { "Domain upon which Dirichlet boundary conditions are applied" });

            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::force)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
        }


        // ****** Finite element space ******
        {
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>>(
                "Finite element space for volume");
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>>(
                { "Finite element space for force" });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::volume));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::UnknownList>({ "displacement" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::displacement) });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::force));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>::UnknownList>({ "displacement" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::displacement) });
        }

        // ****** Dirichlet boundary condition ******
        {
            SetDescription<
                InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::clamped)>>(
                { "Sole boundary condition" });


            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::clamped)>::UnknownName>("displacement");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::clamped)>::DomainIndex>(EnumUnderlyingType(DomainIndex::dirichlet));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::clamped)>::IsMutable>(false);
        }


        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Sole mesh" });


        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });

        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>>(
            { "Surfacic force" });
    }


} // namespace MoReFEM::MidpointNonLinearShellNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
