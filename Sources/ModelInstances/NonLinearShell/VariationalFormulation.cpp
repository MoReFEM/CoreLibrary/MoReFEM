// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
// IWYU pragma: no_include "Core/MoReFEMData/Internal/AbstractClass.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <tuple>
#include <utility>
#include <vector>

#include "ModelInstances/NonLinearShell/VariationalFormulation.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/LinearAlgebra/Operations.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/StaticOrDynamic.hpp"

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Coords/SpatialPoint.hpp"
#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "ModelInstances/NonLinearShell/InputData.hpp"

namespace MoReFEM::MidpointNonLinearShellNS
{


    VariationalFormulation::~VariationalFormulation() = default;


    VariationalFormulation::VariationalFormulation(
        const NumberingSubset& displacement_numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data),
      displacement_numbering_subset_(displacement_numbering_subset)
    {
        assert(parent::GetTimeManager().IsTimeStepConstant() && "Current instantiation relies on this assumption!");
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();

        parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        parent::AllocateSystemVector(displacement_numbering_subset);

        const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);

        vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_ = std::make_unique<GlobalVector>(system_rhs);
        vector_displacement_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);
        vector_velocity_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);

        matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);

        vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
        vector_midpoint_position_ = std::make_unique<GlobalVector>(system_rhs);
        vector_midpoint_velocity_ = std::make_unique<GlobalVector>(system_rhs);
        vector_diff_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        vector_last_converged_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);
    }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        decltype(auto) domain_full_mesh = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));


        solid_ = std::make_unique<Solid<time_manager_type>>(
            morefem_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

        constexpr auto degree_of_exactness{ 3 };
        constexpr auto shape_function_order{ 2 };
        quadrature_rule_per_topology_for_operators_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

#if PETSC_VERSION_GE(3, 22, 0)
        GetNonCstSnes().SetDivergenceTolerance(PETSC_UNLIMITED);
#else
        GetNonCstSnes().SetDivergenceTolerance(-1);
#endif

        DefineStaticOperators(morefem_data);
    }


    void VariationalFormulation::DefineStaticOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));
        const auto& felt_space_force = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::force));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));


        hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), GetSolid());

        shell_operator_ = std::make_unique<NonLinearShellType>(felt_space_volume,
                                                               displacement_ptr,
                                                               displacement_ptr,
                                                               hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                               quadrature_rule_per_topology_for_operators_.get());

        decltype(auto) domain_force = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::force));

        using parameter_type = InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>;

        force_parameter_ =
            Init3DCompoundParameterFromInputData<parameter_type>("volumic force", domain_force, morefem_data);

        if (force_parameter_ != nullptr)
        {
            surfacic_force_operator_ = std::make_unique<
                GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector,
                                                             typename morefem_data_type::time_manager_type>>(
                felt_space_force, displacement_ptr, *force_parameter_);
        }
    }


    void VariationalFormulation::UpdateVectorsAndMatrices(const GlobalVector& evaluation_state)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            break;
        case StaticOrDynamic::dynamic_:
            UpdateDynamicVectorsAndMatrices(evaluation_state);
            break;
        }

        AssembleOperators(evaluation_state);
    }


    void VariationalFormulation::UpdateDynamicVectorsAndMatrices(const GlobalVector& evaluation_state)
    {
        UpdateVelocityAtNewtonIteration(evaluation_state);

        auto& midpoint_position = GetNonCstVectorMidpointPosition();

        midpoint_position.Copy(GetVectorCurrentDisplacement());
        GlobalLinearAlgebraNS::AXPY(1., evaluation_state, midpoint_position);

        midpoint_position.Scale(0.5); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

        midpoint_position.UpdateGhosts();

        auto& midpoint_velocity = GetNonCstVectorMidpointVelocity();

        midpoint_velocity.Copy(evaluation_state);

        GlobalLinearAlgebraNS::AXPY(-1., GetVectorCurrentDisplacement(), midpoint_velocity);

        midpoint_velocity.Scale(1. / GetTimeManager().GetTimeStep());

        midpoint_velocity.UpdateGhosts();

        GetNonCstVectorCurrentDisplacement().UpdateGhosts();
    }


    void VariationalFormulation::UpdateVelocityAtNewtonIteration(const GlobalVector& evaluation_state)
    {
        auto& velocity_at_newton_iteration = GetNonCstVectorVelocityAtNewtonIteration();
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

        if (GetSnes().GetSnesIteration() == 0)
        {
            velocity_at_newton_iteration.Copy(GetVectorCurrentVelocity());
        } else
        {
            const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();

            auto& diff_displacement = GetNonCstVectorDiffDisplacement();

            diff_displacement.Copy(evaluation_state);
            GlobalLinearAlgebraNS::AXPY(-1., displacement_previous_time_iteration, diff_displacement);

            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            diff_displacement.Scale(2. / GetTimeManager().GetTimeStep());
            velocity_at_newton_iteration.Copy(diff_displacement);
            GlobalLinearAlgebraNS::AXPY(-1., velocity_previous_time_iteration, velocity_at_newton_iteration);
        }

        velocity_at_newton_iteration.UpdateGhosts();
    }


    void VariationalFormulation::AssembleOperators(const GlobalVector& evaluation_state)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            AssembleNewtonStaticOperators(evaluation_state);
            break;
        case StaticOrDynamic::dynamic_:
            AssembleNewtonDynamicOperators();
            break;
        }
    }


    void VariationalFormulation::AssembleNewtonStaticOperators(const GlobalVector& evaluation_state)
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            shell_operator_->Assemble(std::make_tuple(std::ref(mat), std::ref(vec)), evaluation_state);
        }

        const auto newton_iteration = GetSnes().GetSnesIteration();

        if (newton_iteration == 0UL)
        {
            auto& vector_surfacic_force = GetNonCstVectorSurfacicForce();

            vector_surfacic_force.ZeroEntries();

            GlobalVectorWithCoefficient vec(vector_surfacic_force, 1.);

            const double time = parent::GetTimeManager().GetTime();

            GetSurfacicForceOperator().Assemble(std::make_tuple(std::ref(vec)), time);
        }
    }


    void VariationalFormulation::AssembleNewtonDynamicOperators()
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            const GlobalVector& displacement_vector = GetVectorMidpointPosition();

            shell_operator_->Assemble(std::make_tuple(std::ref(mat), std::ref(vec)), displacement_vector);
        }
    }


    void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {

        UpdateVectorsAndMatrices(evaluation_state);

        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticResidual(residual);
            break;
        case StaticOrDynamic::dynamic_:
            ComputeDynamicResidual(evaluation_state, residual);
            break;
        }

        ApplyEssentialBoundaryCondition(residual);
    }


    void VariationalFormulation::ComputeStaticResidual(GlobalVector& residual)
    {
        GlobalLinearAlgebraNS::AXPY(1., GetVectorStiffnessResidual(), residual);

        GlobalLinearAlgebraNS::AXPY(-1. * continuation_test_, GetVectorSurfacicForce(), residual);
    }


    void VariationalFormulation::ComputeDynamicResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {
        GlobalLinearAlgebraNS::AXPY(1., GetVectorStiffnessResidual(), residual);

        const auto& time_manager = GetTimeManager();
        const double time_step = time_manager.GetTimeStep();
        const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
        auto& diff_displacement = GetNonCstVectorDiffDisplacement();
        diff_displacement.Copy(evaluation_state);

        GlobalLinearAlgebraNS::AXPY(-1., displacement_previous_time_iteration, diff_displacement);

        GlobalLinearAlgebraNS::AXPY(-time_step, velocity_previous_time_iteration, diff_displacement);

        GlobalLinearAlgebraNS::MatMultAdd(GetMatrixMassPerSquareTimeStep(), diff_displacement, residual, residual);
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters) - true but strong type hindered by not movable object
    void VariationalFormulation::ComputeTangent([[maybe_unused]] const GlobalVector& evaluation_state,
                                                GlobalMatrix& tangent,
                                                [[maybe_unused]] GlobalMatrix& preconditioner)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticTangent(tangent);
            break;
        case StaticOrDynamic::dynamic_:
            ComputeDynamicTangent(tangent);
            break;
        }

        ApplyEssentialBoundaryCondition(tangent);
    }


    void VariationalFormulation::ComputeStaticTangent(GlobalMatrix& tangent)
    {
        tangent.ZeroEntries();

        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent);
    }


    void VariationalFormulation::ComputeDynamicTangent(GlobalMatrix& tangent)
    {
        tangent.ZeroEntries();

#ifndef NDEBUG
        AssertSameNumberingSubset(GetMatrixTangentStiffness(), tangent);
        AssertSameNumberingSubset(GetMatrixMassPerSquareTimeStep(), tangent);
#endif // NDEBUG

        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., GetMatrixMassPerSquareTimeStep(), tangent);

        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(0.5, GetMatrixTangentStiffness(), tangent);
    }


    void VariationalFormulation::PrepareDynamicRuns()
    {

        DefineDynamicOperators();

        AssembleDynamicOperators();

        UpdateForNextTimeStep();
    }


    void VariationalFormulation::DefineDynamicOperators()
    {

        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));

        namespace GVO = GlobalVariationalOperatorNS;

        mass_operator_ = std::make_unique<GVO::Mass>(felt_space_volume, displacement_ptr, displacement_ptr);
    }


    void VariationalFormulation::AssembleDynamicOperators()
    {

        const double volumic_mass = GetSolid().GetVolumicMass().GetConstantValue();

        const double time_step = GetTimeManager().GetTimeStep();

        const double mass_coefficient = 2. * volumic_mass / (time_step * time_step);

        GlobalMatrixWithCoefficient matrix(GetNonCstMatrixMassPerSquareTimeStep(), mass_coefficient);

        GetMassOperator().Assemble(std::make_tuple(std::ref(matrix)));
    }


    void VariationalFormulation::UpdateForNextTimeStep()
    {

        UpdateDisplacementBetweenTimeStep();

        UpdateVelocityBetweenTimeStep();

        // ComputeGuessForNextTimeStep();
    }


    void VariationalFormulation::UpdateDisplacementBetweenTimeStep()
    {

        GetNonCstVectorCurrentDisplacement().Copy(GetSystemSolution(GetDisplacementNumberingSubset()));

        GetNonCstVectorCurrentDisplacement().UpdateGhosts();
    }


    void VariationalFormulation::UpdateVelocityBetweenTimeStep()
    {

        GetNonCstVectorCurrentVelocity().Copy(GetVectorVelocityAtNewtonIteration());

        GetNonCstVectorCurrentVelocity().UpdateGhosts();
    }


    void VariationalFormulation::ComputeGuessForNextTimeStep()
    {

        const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
        auto& system_solution = GetNonCstSystemSolution(displacement_numbering_subset);
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

        GlobalLinearAlgebraNS::AXPY(GetTimeManager().GetTimeStep(), velocity_previous_time_iteration, system_solution);

        system_solution.UpdateGhosts();
    }


    void VariationalFormulation::WriteMeditSolution(const FilesystemNS::File& filename,
                                                    GeometryNS::dimension_type component) const
    {
        const auto& god_of_dof = GetGodOfDof();

        const auto& displacement = UnknownManager::GetInstance().GetUnknown(AsUnknownId(UnknownIndex::displacement));

        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        const auto& displacement_extended_unknown = felt_space_volume.GetExtendedUnknown(displacement);
        decltype(auto) extended_unknown_ptr = felt_space_volume.GetExtendedUnknownPtr(displacement);

        felt_space_volume.ComputeLocal2Global(std::move(extended_unknown_ptr), DoComputeProcessorWiseLocal2Global::yes);


        const auto& list = felt_space_volume.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        assert(list.size() == 1);

        std::vector<LocalCoords> local_coords_list;
        for (const auto& [ref_local_felt_space_ptr, unused] : list)
        {
            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

            const auto& ref_elt = ref_local_felt_space.GetRefFElt(displacement_extended_unknown);

            const auto& node_list = ref_elt.GetBasicRefFElt().GetLocalNodeList();
            // std::cout << "\nFelt nodes coords: \n";
            for (const auto& node : node_list)
            {
                const auto& local_coords = node->GetLocalCoords();
                // local_coords.Print(std::cout);
                local_coords_list.push_back(local_coords);
            }
        }

        decltype(auto) domain_volume = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::volume));

        const auto& mesh = god_of_dof.GetMesh();
        const auto& ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

        const SpatialPoint work_coords;
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);
        Eigen::VectorXd data;
        const auto mesh_dimension = mesh.GetDimension().Get();
        data.resize(static_cast<Eigen::Index>(local_coords_list.size()) * mesh_dimension);
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        const auto Ndof_per_component = data.size() / mesh_dimension;

        // TMP #1970 This line is unacceptable and should be removed; I neutralize clang-tidy warnings
        // to avoid polluting its diagnostic.
        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        std::vector<Eigen::Index> felt_to_geo_index_match{ 0,  2,  8, 6,  18, 20, 26, 24, 1,  5,  7,  3,  19, 23,
                                                           25, 21, 9, 11, 17, 15, 4,  22, 10, 14, 16, 12, 13 };
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        if (filename.DoExist())
            filename.Remove();

        std::ofstream medit_sol_output_stream{ filename.NewContent() };

        medit_sol_output_stream << "MeshVersionFormatted 3\n\n";
        medit_sol_output_stream << "Dimension\n";
        medit_sol_output_stream << mesh_dimension << "\n\n";
        medit_sol_output_stream << "HOSolAtHexahedraQ2\n";

        for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
        {
            assert(!(!ref_geom_elt_ptr));
            const auto& ref_geom_elt = *ref_geom_elt_ptr;

            if (!domain_volume.DoRefGeomEltMatchCriteria(ref_geom_elt))
                continue;

            decltype(auto) iterator_range =
                mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

            const auto Nelements = std::distance(iterator_range.first, iterator_range.second);
            medit_sol_output_stream << Nelements << '\n';
            medit_sol_output_stream << "1 1\n";
            medit_sol_output_stream << "2 " << Ndof_per_component << '\n';

            for (auto it_geom_elt = iterator_range.first; it_geom_elt != iterator_range.second; ++it_geom_elt)
            {
                const auto& geom_elt_ptr = *it_geom_elt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                const auto& local_felt_space = felt_space_volume.GetLocalFEltSpace(geom_elt);

                GlobalVariationalOperatorNS::ExtractLocalDofValues(
                    local_felt_space,
                    displacement_extended_unknown,
                    GetSystemSolution(
                        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement))),
                    data);

                for (auto i = Eigen::Index{}; i < Ndof_per_component; ++i)
                {
                    medit_sol_output_stream.precision(
                        8); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
                    medit_sol_output_stream << data(felt_to_geo_index_match[static_cast<std::size_t>(i)]
                                                    + component.Get() * Ndof_per_component)
                                            << " ";
                }
                medit_sol_output_stream << '\n';
            }
        }

        medit_sol_output_stream << '\n';
        medit_sol_output_stream << "END";
    }

    void VariationalFormulation::SolveStaticContinuation()
    {
        const auto& mpi = GetMpi();

        const auto& god_of_dof = GetGodOfDof();
        const auto& displacement_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));

        continuation_step_counter_ = 0;

        continuation_test_ = 1;
        auto continuation_step = continuation_test_;

        // Boolean to re-enter the loop after a convergence in the continuation.
        bool converged_in_last_iteration = false;

        while (std::fabs(continuation_test_) < 1. || NumericNS::AreEqual(std::fabs(continuation_test_), 1.))
        {
            while (GetSnes().GetNonLinearConvergenceReason() == Wrappers::Petsc::convergence_status::no
                   || GetSnes().GetNonLinearConvergenceReason() == Wrappers::Petsc::convergence_status::pending
                   || converged_in_last_iteration)
            {

                converged_in_last_iteration = false;

                if (mpi.IsRootProcessor())
                {
                    std::cout << '\n' << "----------------------------------------------" << '\n';
                    std::cout << std::defaultfloat << "Static Continuation step : " << continuation_test_ << '\n';
                    std::cout << "----------------------------------------------" << '\n' << '\n';

                    ++continuation_step_counter_;
                }

                mpi.Barrier();

                GetNonCstSystemSolution(displacement_numbering_subset).Copy(*vector_last_converged_displacement_);
                GetNonCstSystemSolution(displacement_numbering_subset).UpdateGhosts(); // #TAG useless?
                GetNonCstSystemRhs(displacement_numbering_subset).ZeroEntries();       // #TAG useless?

                SolveNonLinear(displacement_numbering_subset,
                               displacement_numbering_subset,
                               Wrappers::Petsc::check_convergence::no);

                if (GetSnes().GetNonLinearConvergenceReason() == Wrappers::Petsc::convergence_status::no)
                {
                    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
                    continuation_step = 0.5 * continuation_step;
                    continuation_test_ -= continuation_step;

                } else
                {

                    converged_in_last_iteration = true;

                    if (NumericNS::AreEqual(std::fabs(continuation_test_), 1.))
                        break;

                    continuation_test_ = std::min(continuation_test_ + continuation_step,
                                                  1.); // Possible to do 2. * continuation_step

                    vector_last_converged_displacement_->Copy(GetSystemSolution(displacement_numbering_subset));
                }
            }

            if (GetSnes().GetNonLinearConvergenceReason() == Wrappers::Petsc::convergence_status::yes
                && NumericNS::AreEqual(std::fabs(continuation_test_), 1.))
                break;
        }

        if (mpi.IsRootProcessor())
            std::cout << '\n'
                      << continuation_step_counter_ << " step(s) of static continuation were needed to converge."
                      << '\n';

        mpi.Barrier();
    }


} // namespace MoReFEM::MidpointNonLinearShellNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
