// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_NONLINEARSHELL_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_MODELINSTANCES_NONLINEARSHELL_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/NonLinearShell/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "ModelInstances/NonLinearShell/InputData.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }  // lines 30-30
namespace MoReFEM { class GlobalVector; }  // lines 31-31
namespace MoReFEM { template <TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> class Solid; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::MidpointNonLinearShellNS
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation ::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation::GetMassOperator() const noexcept
    {
        assert(!(!mass_operator_));
        return *mass_operator_;
    }


    inline const VariationalFormulation::StiffnessOperatorType&
    VariationalFormulation::GetStiffnessOperator() const noexcept
    {
        assert(!(!stiffness_operator_));
        return *stiffness_operator_;
    }


    inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
    VariationalFormulation::GetSurfacicForceOperator() const noexcept
    {
        assert(!(!surfacic_force_operator_));
        return *surfacic_force_operator_;
    }


    inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
    {
        assert(!(!vector_stiffness_residual_));
        return *vector_stiffness_residual_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorVelocityAtNewtonIteration() const noexcept
    {
        assert(!(!vector_velocity_at_newton_iteration_));
        return *vector_velocity_at_newton_iteration_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorVelocityAtNewtonIteration() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorVelocityAtNewtonIteration());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixMassPerSquareTimeStep() const noexcept
    {
        assert(!(!matrix_mass_per_square_time_step_));
        return *matrix_mass_per_square_time_step_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixMassPerSquareTimeStep() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixMassPerSquareTimeStep());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
    {
        assert(!(!matrix_tangent_stiffness_));
        return *matrix_tangent_stiffness_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForce() const noexcept
    {
        assert(!(!vector_surfacic_force_));
        return *vector_surfacic_force_;
    }

    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForce() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForce());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentDisplacement() const noexcept
    {
        assert(!(!vector_current_displacement_));
        return *vector_current_displacement_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentDisplacement() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentVelocity() const noexcept
    {
        assert(!(!vector_current_velocity_));
        return *vector_current_velocity_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentVelocity() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorMidpointPosition() const noexcept
    {
        assert(!(!vector_midpoint_position_));
        return *vector_midpoint_position_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointPosition() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorMidpointPosition());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorMidpointVelocity() const noexcept
    {
        assert(!(!vector_midpoint_velocity_));
        return *vector_midpoint_velocity_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointVelocity() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorMidpointVelocity());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorDiffDisplacement() const noexcept
    {
        assert(!(!vector_diff_displacement_));
        return *vector_diff_displacement_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorDiffDisplacement() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorDiffDisplacement());
    }


    inline const Solid<time_manager_type>& VariationalFormulation::GetSolid() const noexcept
    {
        assert(!(!solid_));
        return *solid_;
    }


    inline const NumberingSubset& VariationalFormulation::GetDisplacementNumberingSubset() const noexcept
    {
        return displacement_numbering_subset_;
    }


} // namespace MoReFEM::MidpointNonLinearShellNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_NONLINEARSHELL_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
