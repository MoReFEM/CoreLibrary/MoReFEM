// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Model/Main/Main.hpp"

#include "ModelInstances/NonLinearShell/Model.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(clang-analyzer-optin.cplusplus.VirtualCall, bugprone-exception-escape)
int main(int argc, char** argv)
{
    return ModelNS::Main<MidpointNonLinearShellNS::Model>(argc, argv);
}
// NOLINTEND(clang-analyzer-optin.cplusplus.VirtualCall, bugprone-exception-escape)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
