// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <filesystem>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE model_nonlinear_shell

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq");
}

// To be enabled when ViZiR post processing will be properly handled in #1570.
BOOST_FIXTURE_TEST_CASE(mpi4, TestNS::FixtureNS::TestEnvironment)
{
    // CommonTestCase("Mpi4");
}


BOOST_FIXTURE_TEST_CASE(mpi4_from_partitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    // CommonTestCase("Mpi4_FromPrepartitionedData");
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        std::string root_dir_path;
        std::string output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path = environment.GetEnvironmentVariable("MOREFEM_ROOT"));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR"));

        const FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        const FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);
        BOOST_CHECK(output_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir, std::vector<std::string>{ "Sources", "ModelInstances", "NonLinearShell", "ExpectedResults" });

        FilesystemNS::Directory obtained_dir(
            output_dir, std::vector<std::string>{ seq_or_par, "Ascii", "NonLinearShell", "Rank_0" });

        // No symbolic link to the special Lua file used as input in ExpectedResults (would need dedicated function
        // as produced one would still call it 'input_data.lua', but frankly it's probably not worth the time to write
        // it...
        if (seq_or_par != "Mpi4_FromPrepartitionedData")
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata");

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata");


        ref_dir.AddSubdirectory("NumberingSubset_1");
        obtained_dir.AddSubdirectory("NumberingSubset_1");

        [[maybe_unused]] const TestNS::CompareDataFiles<MeshNS::Format::Vizir> check_x(
            ref_dir, obtained_dir, "shell_x.sol", 1.e-7);
        [[maybe_unused]] const TestNS::CompareDataFiles<MeshNS::Format::Vizir> check_y(
            ref_dir, obtained_dir, "shell_y.sol", 1.e-7);
        [[maybe_unused]] const TestNS::CompareDataFiles<MeshNS::Format::Vizir> check_z(
            ref_dir, obtained_dir, "shell_z.sol", 1.e-7);
    }


} // namespace
