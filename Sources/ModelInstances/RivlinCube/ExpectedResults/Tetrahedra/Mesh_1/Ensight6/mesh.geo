Geometry file
Geometry file
node id given
element id assign
coordinates
       8
       1 0.00000e+00 0.00000e+00 0.00000e+00
       2 1.00000e+00 0.00000e+00 0.00000e+00
       3 1.00000e+00 1.00000e+00 0.00000e+00
       4 0.00000e+00 1.00000e+00 0.00000e+00
       5 0.00000e+00 0.00000e+00 1.00000e+00
       6 1.00000e+00 0.00000e+00 1.00000e+00
       7 1.00000e+00 1.00000e+00 1.00000e+00
       8 0.00000e+00 1.00000e+00 1.00000e+00
part       1
MeshLabel_1
tria3
       2
       1       5       4
       4       5       8
part       2
MeshLabel_2
tria3
       2
       1       2       5
       2       6       5
part       3
MeshLabel_3
tria3
       2
       1       4       2
       4       3       2
part       4
MeshLabel_4
tria3
       2
       2       3       6
       3       7       6
part       5
MeshLabel_5
tria3
       2
       7       3       8
       3       4       8
part       6
MeshLabel_6
tria3
       2
       5       6       8
       8       6       7
part       7
MeshLabel_201
bar2
       1
       1       2
part       8
MeshLabel_202
bar2
       1
       2       3
part       9
MeshLabel_203
bar2
       1
       3       4
part      10
MeshLabel_204
bar2
       1
       4       1
part      11
MeshLabel_205
bar2
       1
       5       6
part      12
MeshLabel_206
bar2
       1
       6       7
part      13
MeshLabel_207
bar2
       1
       7       8
part      14
MeshLabel_208
bar2
       1
       8       5
part      15
MeshLabel_209
bar2
       1
       1       5
part      16
MeshLabel_210
bar2
       1
       2       6
part      17
MeshLabel_211
bar2
       1
       4       8
part      18
MeshLabel_212
bar2
       1
       3       7
part      19
MeshLabel_500
tetra4
       6
       5       1       4       2
       4       5       2       6
       6       2       4       3
       4       5       6       8
       3       4       6       8
       3       6       7       8
