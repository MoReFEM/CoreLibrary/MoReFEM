// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_RIVLINCUBE_MODEL_DOT_HPP_
#define MOREFEM_MODELINSTANCES_RIVLINCUBE_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: export
// IWYU pragma: no_include <string>
#include <memory>      // IWYU pragma: export
#include <type_traits> // IWYU pragma: keep
#include <vector>      // IWYU pragma: export

#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/Mpi/Mpi.hpp"           // IWYU pragma: export
#include "Utilities/TimeKeep/TimeKeep.hpp" // IWYU pragma: export

#include "Core/Enum.hpp" // IWYU pragma: export

#include "Model/Model.hpp" // IWYU pragma: export

#include "ModelInstances/RivlinCube/InputData.hpp"              // IWYU pragma: export
#include "ModelInstances/RivlinCube/VariationalFormulation.hpp" // IWYU pragma: export


namespace MoReFEM::RivlinCubeNS
{


    //! \copydoc doxygen_hide_simple_model
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{

        //! Manage the change of time iteration.
        void Forward();

      private:
        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();

      private:
        //! Non constant access to the underlying VariationalFormulation object.
        VariationalFormulation& GetNonCstVariationalFormulation();

        //! Access to the underlying VariationalFormulation object.
        const VariationalFormulation& GetVariationalFormulation() const;


      private:
        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();


        ///@}

      private:
        //! Underlying variational formulation.
        VariationalFormulation::unique_ptr variational_formulation_;
    };

} // namespace MoReFEM::RivlinCubeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ModelInstances/RivlinCube/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_RIVLINCUBE_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
