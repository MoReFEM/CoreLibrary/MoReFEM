// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Utilities/InputData/ModelSettings.hpp"

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "ModelInstances/RivlinCube/InputData.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::RivlinCubeNS
{


    void ModelSettings::Init()
    {
        {
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face1)>>(
                { "Dirichlet boundary condition applied upon face 1" });
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face2)>>(
                { "Dirichlet boundary condition applied upon face 2" });
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face3)>>(
                { "Dirichlet boundary condition applied upon face 3" });

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::face1)>::UnknownName>("solid_displacement");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::face1)>::DomainIndex>(EnumUnderlyingType(DomainIndex::face1));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face1)>::IsMutable>(
                false);

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::face2)>::UnknownName>("solid_displacement");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::face2)>::DomainIndex>(EnumUnderlyingType(DomainIndex::face2));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face2)>::IsMutable>(
                false);

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::face3)>::UnknownName>("solid_displacement");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::face3)>::DomainIndex>(EnumUnderlyingType(DomainIndex::face3));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::face3)>::IsMutable>(
                false);
        }


        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>>(
            { "Monolithic numbering subset" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>({ "Solid displacement" });

        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Name>({ "solid_displacement" });

        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Nature>({ "vectorial" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Mesh" });
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::Dimension>(3UL);

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
            { "Domain for geometric elements of the highest dimension" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>>({ "Domain for face 1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face2)>>({ "Domain for face 2" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face3)>>({ "Domain for face 3" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face4)>>({ "Domain for face 4" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face5)>>({ "Domain for face 5" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face6)>>({ "Domain for face 6" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face456)>>(
            { "Domain for faces 4, 5 and 6" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>(
            { "Domain that covers the whole mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face2)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face3)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face4)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face5)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face6)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face456)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>({ 1UL });

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
            { "Finite element space with all elements of the highest dimension of the mesh" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::NumberingSubsetList>({ 1 });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::GodOfDofIndex>(1UL);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::DomainIndex>(1UL);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::UnknownList>(
            { "solid_displacement" });

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure)>>(
            { "Finite element space for description of the surface pressure" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure)>::NumberingSubsetList>({ 1 });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure)>::GodOfDofIndex>(1UL);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure)>::DomainIndex>(8UL);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface_pressure)>::UnknownList>(
            { "solid_displacement" });


        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
        SetDescription<
            InputDataNS::InitialCondition<EnumUnderlyingType(InitialConditionIndex::displacement_initial_condition)>>(
            { "Initial condition for displacement" });
    }


} // namespace MoReFEM::RivlinCubeNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
