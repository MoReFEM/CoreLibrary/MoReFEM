// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_RIVLINCUBE_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_MODELINSTANCES_RIVLINCUBE_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/RivlinCube/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "ModelInstances/RivlinCube/InputData.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }  // lines 31-31
namespace MoReFEM { class GlobalVector; }  // lines 32-32
namespace MoReFEM { class NumberingSubset; }  // lines 33-33
namespace MoReFEM::FilesystemNS { class File; }  // lines 34-34

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::RivlinCubeNS
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation ::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    inline const VariationalFormulation::StiffnessOperatorType&
    VariationalFormulation::GetStiffnessOperator() const noexcept
    {
        assert(!(!stiffness_operator_));
        return *stiffness_operator_;
    }


    inline auto VariationalFormulation::GetFollowingPressureOperator() const noexcept
        -> const following_pressure_op_type&
    {
        assert(!(!following_pressure_operator_));
        return *following_pressure_operator_;
    }


    inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
    {
        assert(!(!vector_stiffness_residual_));
        return *vector_stiffness_residual_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual()
    {
        return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
    {
        assert(!(!matrix_tangent_stiffness_));
        return *matrix_tangent_stiffness_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness()
    {
        return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorFollowingPressureResidual() const noexcept
    {
        assert(!(!vector_following_pressure_residual_));
        return *vector_following_pressure_residual_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorFollowingPressureResidual()
    {
        return const_cast<GlobalVector&>(GetVectorFollowingPressureResidual());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentFollowingPressure() const noexcept
    {
        assert(!(!matrix_tangent_following_pressure_));
        return *matrix_tangent_following_pressure_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentFollowingPressure()
    {
        return const_cast<GlobalMatrix&>(GetMatrixTangentFollowingPressure());
    }


    inline const ScalarParameter<time_manager_type>& VariationalFormulation::GetStaticPressure() const noexcept
    {
        assert(!(!static_pressure_));
        return *static_pressure_;
    }


    inline const NumberingSubset& VariationalFormulation::GetNumberingSubset() const
    {
        return numbering_subset_;
    }


    inline const Solid<time_manager_type>& VariationalFormulation::GetSolid() const noexcept
    {
        assert(!(!solid_));
        return *solid_;
    }


    inline const FilesystemNS::File& VariationalFormulation::GetTangentQuadraticVerificationFile() const noexcept
    {
        return tangent_quadratic_verification_file_;
    }


} // namespace MoReFEM::RivlinCubeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_RIVLINCUBE_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
