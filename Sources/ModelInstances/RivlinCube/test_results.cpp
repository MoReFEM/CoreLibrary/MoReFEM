// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE model_rivlin_cube

#include <filesystem>
#include <string>
#include <vector>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& geometry);


} // namespace


PRAGMA_DIAGNOSTIC(push)


BOOST_FIXTURE_TEST_CASE(sequential_hexahedra, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Hexahedra");
}


BOOST_FIXTURE_TEST_CASE(mpi4_hexahedra, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "Hexahedra");
}


BOOST_FIXTURE_TEST_CASE(sequential_tetrahedra, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Tetrahedra");
}


BOOST_FIXTURE_TEST_CASE(mpi4_tetrahedra, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "Tetrahedra");
}

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& geometry)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        std::string root_dir_path;
        std::string output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path = environment.GetEnvironmentVariable("MOREFEM_ROOT"));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR"));

        const FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        const FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);
        BOOST_CHECK(output_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir,
            std::vector<std::string>{ "Sources", "ModelInstances", "RivlinCube", "ExpectedResults", geometry });

        FilesystemNS::Directory obtained_dir(
            output_dir, std::vector<std::string>{ seq_or_par, "Ascii", "RivlinCube", geometry, "Rank_0" });

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata");

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata");

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case");

        TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, "solid_displacement.00000.scl");
    }


} // namespace
