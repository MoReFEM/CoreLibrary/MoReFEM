// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_STOKES_ENVIRONMENT_DOT_HPP_
#define MOREFEM_MODELINSTANCES_STOKES_ENVIRONMENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Environment/Environment.hpp"


namespace MoReFEM::StokesNS
{

    /*!
     * \brief Set an environment variable to enable both configurations: with Stokes operator or with two separate ones.
     */
    inline void SetStokesConfiguration()
    {

        auto& environment = Utilities::Environment::CreateOrGetInstance();

#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
        environment.SetEnvironmentVariable(std::make_pair("MOREFEM_STOKES_N_OP", "TwoOperators"));
#else
        environment.SetEnvironmentVariable(std::make_pair("MOREFEM_STOKES_N_OP", "OneOperator"));
#endif
    }

} // namespace MoReFEM::StokesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_STOKES_ENVIRONMENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
