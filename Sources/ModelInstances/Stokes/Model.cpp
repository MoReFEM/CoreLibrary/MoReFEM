// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>
#include <memory>

#include "Model/Model.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ModelInstances/Stokes/Model.hpp"
#include "ModelInstances/Stokes/VariationalFormulation.hpp"


namespace MoReFEM::StokesNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = this->GetGodOfDof(AsMeshId(MeshIndex::mesh));

        const auto& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        auto&& numbering_subset_list = felt_space_highest_dimension.GetNumberingSubsetList();

        assert((numbering_subset_list.size() == 1 || numbering_subset_list.size() == 2)
               && "1 or 2 numbering subsets expected here!");

        is_monolithic_ = numbering_subset_list.size() == 1;

        const auto& unknown_manager = UnknownManager::GetInstance();

        const auto& velocity = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::velocity));
        const auto& pressure = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::pressure));

        // This form takes care of both monolithic and non monolithic case (in the former front == back...).
        constexpr const double uzawa_coupling_term = 15.;

        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();

        {
            const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::sole)) };

            variational_formulation_ =
                std::make_unique<VariationalFormulation>(felt_space_highest_dimension.GetNumberingSubset(velocity),
                                                         felt_space_highest_dimension.GetNumberingSubset(pressure),
                                                         god_of_dof,
                                                         bc_list,
                                                         uzawa_coupling_term,
                                                         morefem_data);
        }


        auto& formulation = this->GetNonCstVariationalFormulation();


        const auto& mpi = this->GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);

        formulation.Init(morefem_data);


        formulation.RunStaticCase();
    }


    void Model::Forward()
    {
        assert(false && "Only static case at the moment!");
        exit(EXIT_FAILURE);
    }


    void Model::SupplFinalizeStep()
    {
        assert(false && "Only static case at the moment!");
        exit(EXIT_FAILURE);
    }


    void Model::SupplFinalize() const
    { }


} // namespace MoReFEM::StokesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
