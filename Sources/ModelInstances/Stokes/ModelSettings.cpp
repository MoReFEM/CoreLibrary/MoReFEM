// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Utilities/Containers/EnumClass.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "ModelInstances/Stokes/InputData.hpp"


namespace MoReFEM::StokesNS
{


    void ModelSettings::Init()
    {
        // ****** Numbering subset ******
        {
            SetDescription<
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity_or_monolithic)>>(
                "Numbering subset for either velocity (when non monolithic approac) or monolitic");
            SetDescription<
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure_or_irrelevant)>>(
                "Numbering subset for pressure in non monolithic approach; not used in monolithic one");
        }

        // ****** Unknown ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>>({ "Velocity" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>>({ "Pressure" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>::Name>("velocity");
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>::Name>("pressure");

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>::Nature>("vectorial");
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>::Nature>("scalar");
        }

        // ****** Domain ******
        {
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
                { "Encompass of geometric element of the highest available dimension" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>>(
                { "Domain upon which Neumann condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>>(
                { "Domain upon which Dirichlet condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>(
                { "Domain that covers the whole mesh" });

            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
        }

        // ****** Finite element space ******
        {
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
                { "Finite element space for highest geometric dimension.\nConcerning the numbering subset, choose "
                  "{1, 1} if you want to consider a monolithic resolution and {1, 2} if you aim instead a Uzawa "
                  "resolution (which is much slower and requires a fine-tuned value currently hardcoded)." });
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>>(
                { "Finite element space for Neumann boundary condition" });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::highest_dimension));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::UnknownList>(
                { "velocity", "pressure" });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::neumann));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::UnknownList>({ "velocity" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::velocity_or_monolithic) });
        }

        // ****** Dirichlet boundary condition ******
        {
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>>(
                { "Sole boundary condition" });

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::UnknownName>(
                "velocity");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::dirichlet));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::IsMutable>(
                false);
        }

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Sole mesh" });


        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Linear solver" });

        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::volumic)>>(
            { "Volumic source" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::surfacic)>>(
            { "Surfacic source" });
    }


} // namespace MoReFEM::StokesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
