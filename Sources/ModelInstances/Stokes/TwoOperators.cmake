##########################################
# Library to compile once files that will be used in several executables.
# There are also non source files (README, lua files, etc...) so that
# they appear correctly when CMake generates project for an IDE.
##########################################

add_library(MoReFEM4Stokes_2_operators_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4Stokes_2_operators_lib
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Environment.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
	PUBLIC        
		${CMAKE_CURRENT_LIST_DIR}/Model.hpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hxx
		${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
 		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx
        ${CMAKE_CURRENT_LIST_DIR}/TwoOperators.cmake
)

target_compile_definitions(MoReFEM4Stokes_2_operators_lib PUBLIC MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS)

target_link_libraries(MoReFEM4Stokes_2_operators_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)

morefem_organize_IDE(MoReFEM4Stokes_2_operators_lib ModelInstances/Stokes/TwoOperators ModelInstances/Stokes)

morefem_install(MoReFEM4Stokes_2_operators_lib)

##########################################
# Executable to run the model
##########################################

add_executable(MoReFEM4Stokes_2_operators ${CMAKE_CURRENT_LIST_DIR}/main.cpp)

target_compile_definitions(MoReFEM4Stokes_2_operators PUBLIC MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS)

target_link_libraries(MoReFEM4Stokes_2_operators
                      MoReFEM4Stokes_2_operators_lib)
apply_lto_if_supported(MoReFEM4Stokes_2_operators)

morefem_organize_IDE(MoReFEM4Stokes_2_operators ModelInstances/Stokes/TwoOperators ModelInstances/Stokes)

morefem_install(MoReFEM4Stokes_2_operators)

##########################################
# Executable to produce outputs in Ensight format
##########################################

add_executable(MoReFEM4Stokes_2_operatorsEnsightOutput
               ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output_monolithic.cpp)

target_compile_definitions(MoReFEM4Stokes_2_operatorsEnsightOutput PUBLIC MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS)

target_link_libraries(MoReFEM4Stokes_2_operatorsEnsightOutput
                      MoReFEM4Stokes_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(MoReFEM4Stokes_2_operatorsEnsightOutput ModelInstances/Stokes/TwoOperators ModelInstances/Stokes)                      

morefem_install(MoReFEM4Stokes_2_operatorsEnsightOutput)


##########################################
# Tests
##########################################

morefem_test_run_model_in_both_modes(NAME StokesModel_2_operators
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Stokes/demo.lua
                                     EXE MoReFEM4Stokes_2_operators
                                     TIMEOUT 500)

morefem_test_run_model_in_both_modes(NAME StokesModel_2_operators
                                     FROM_PREPARTITIONED_DATA
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Stokes/demo_from_prepartitioned_data.lua
                                     EXE MoReFEM4Stokes_2_operators
                                     TIMEOUT 500)

##########################################
# Executable and test which check reference results are properly found.
##########################################

add_executable(MoReFEM4Stokes_2_operatorsCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)

target_compile_definitions(MoReFEM4Stokes_2_operatorsCheckResults PUBLIC MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS)

target_link_libraries(MoReFEM4Stokes_2_operatorsCheckResults
                      MoReFEM4Stokes_lib
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(MoReFEM4Stokes_2_operatorsCheckResults ModelInstances/Stokes/TwoOperators ModelInstances/Stokes)

morefem_boost_test_check_results(NAME Stokes_2_operators
                                   EXE MoReFEM4Stokes_2_operatorsCheckResults
                                   TIMEOUT 20)
