// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include "Core/MoReFEMData/Internal/AbstractClass.hpp"

#include <array>
#include <cassert>
#include <cstddef>
#include <functional>
#include <iostream>
#include <memory>
#include <tuple>
#include <utility>

#include "ModelInstances/Stokes/VariationalFormulation.hpp"

#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/LinearAlgebra/Operations.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"


namespace MoReFEM::StokesNS
{


    VariationalFormulation::~VariationalFormulation() = default;


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    VariationalFormulation::VariationalFormulation(
        const NumberingSubset& velocity_numbering_subset,
        const NumberingSubset& pressure_numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        const double uzawa_coupling_term,
        morefem_data_type& morefem_data)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data),
      velocity_numbering_subset_(velocity_numbering_subset), pressure_numbering_subset_(pressure_numbering_subset),
      uzawa_coupling_term_(uzawa_coupling_term)
    { }


    void VariationalFormulation::RunStaticCase()
    {
        if (IsMonolithic())
            MonolithicSolve();
        else
            UzawaSolve();
    }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        DefineOperators(morefem_data);
        AssembleStaticCase();
    }


    void VariationalFormulation::MonolithicSolve()
    {
        assert(IsMonolithic());

        const auto& velocity_numbering_subset = GetVelocityNumberingSubset();

        assert(velocity_numbering_subset == GetPressureNumberingSubset());

        ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(velocity_numbering_subset,
                                                                                             velocity_numbering_subset);

        SolveLinear<IsFactorized::no>(velocity_numbering_subset, velocity_numbering_subset);
        WriteSolution(this->GetTimeManager(), velocity_numbering_subset);
    }


    void VariationalFormulation::UzawaSolve()
    {
        assert(!IsMonolithic());

        const bool is_root_processor = GetMpi().IsRootProcessor();

        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        for (std::size_t i = 0; i < 1500; ++i) // #494 Not to bother with stop condition right now...
        {
            if (is_root_processor)
                std::cout << "Iteration " << i << " -- velocity " << '\n';

            ComputeNewVelocity();

            if (is_root_processor)
                std::cout << "\nIteration " << i << " -- pressure " << '\n';

            ComputeNewPressure();
        }
    }


    void VariationalFormulation::AssembleStaticCase()
    {
        const auto& velocity_numbering_subset = GetVelocityNumberingSubset();

        {
            auto& rhs = GetNonCstSystemRhs(velocity_numbering_subset);
            const auto time = GetTimeManager().GetTime();

            auto& assemble_into = IsMonolithic() ? rhs : GetNonCstForceVector();

            GlobalVectorWithCoefficient force_vector(assemble_into, 1.);
            auto force_tuple = std::make_tuple(std::ref(force_vector));

            if (IsOperatorActivated<SourceType::volumic>())
                GetForceOperator<SourceType::volumic>().Assemble(force_tuple, time);

            if (IsOperatorActivated<SourceType::surfacic>())
                GetForceOperator<SourceType::surfacic>().Assemble(force_tuple, time);
        }

#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
        {
            auto& system_matrix = GetNonCstSystemMatrix(velocity_numbering_subset, velocity_numbering_subset);

            if (!GetFluidViscosity().IsConstant())
                throw Exception("Current structure of Stokes model with 2 operators assumes the viscosity is a "
                                "constant. However, other Stokes implementation with 1 operator is able to "
                                "deal with variable viscosity (2 operators model could also be adapted to do "
                                "so but it isn't worth it at the moment).");


            GlobalMatrixWithCoefficient matrix_with_coeff(system_matrix, GetFluidViscosity().GetConstantValue());
            GetVelocityStiffnessOperator().Assemble(std::make_tuple(std::ref(matrix_with_coeff)));
        }

        {
            const auto& pressure_numbering_subset = GetPressureNumberingSubset();
            auto& system_matrix = GetNonCstSystemMatrix(velocity_numbering_subset, pressure_numbering_subset);

            GlobalMatrixWithCoefficient matrix_with_coeff(system_matrix, -1.);

            GetScalarDivVectorialOperator().Assemble(std::make_tuple(std::ref(matrix_with_coeff)));
        }

#else  // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

        // Here I must distinguish monolithic and non monolithic: if I tried to use the non monolithic
        // implementation for both, I would get twice the expected values as I would assemble twice in the same
        // matrix...
        if (IsMonolithic())
        {
            auto& system_matrix = GetNonCstSystemMatrix(velocity_numbering_subset, velocity_numbering_subset);

            GlobalMatrixWithCoefficient matrix_with_coeff(system_matrix, 1.);
            GetStokesOperator().Assemble(std::make_tuple(std::ref(matrix_with_coeff)));
        } else
        {
            const auto& pressure_numbering_subset = GetPressureNumberingSubset();
            auto& block_velocity_pressure = GetNonCstSystemMatrix(velocity_numbering_subset, pressure_numbering_subset);
            auto& block_velocity_velocity = GetNonCstSystemMatrix(velocity_numbering_subset, velocity_numbering_subset);

            GlobalMatrixWithCoefficient mat1(block_velocity_pressure, 1.);
            GlobalMatrixWithCoefficient mat2(block_velocity_velocity, 1.);

            GetStokesOperator().Assemble(std::make_tuple(std::ref(mat1), std::ref(mat2)));
        }
#endif // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
    }


    void VariationalFormulation::DefineOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = this->GetGodOfDof();

        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        fluid_viscosity_ =
            InitScalarParameterFromInputData<InputDataNS::Fluid::Viscosity>("Viscosity", domain, morefem_data);

        const auto& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
        const auto& felt_space_neumann = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::neumann));

        const auto& unknown_manager = UnknownManager::GetInstance();

        const auto& velocity_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::velocity));
        const auto& pressure_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::pressure));

        const std::array<Unknown::const_shared_ptr, 2> velocity_pressure{ { velocity_ptr, pressure_ptr } };

        SetIfTaggedAsActivated<SourceType::volumic>(
            "Volumic force", morefem_data, felt_space_highest_dimension, velocity_ptr);

        SetIfTaggedAsActivated<SourceType::surfacic>("Surfacic force", morefem_data, felt_space_neumann, velocity_ptr);


#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

        namespace GVO = GlobalVariationalOperatorNS;

        scalar_div_vectorial_operator_ = std::make_unique<const GVO::ScalarDivVectorial>(
            felt_space_highest_dimension, velocity_pressure, velocity_pressure, 1., 1.);

        velocity_stiffness_operator_ =
            std::make_unique<const GVO::GradPhiGradPhi>(felt_space_highest_dimension, velocity_ptr, velocity_ptr);
#else // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

        stokes_operator_ = std::make_unique<stokes_op_type>(
            felt_space_highest_dimension, velocity_pressure, velocity_pressure, GetFluidViscosity());

#endif // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const auto& velocity_numbering_subset = GetVelocityNumberingSubset();
        const auto& pressure_numbering_subset = GetPressureNumberingSubset();


        if (IsMonolithic())
        {
            parent::AllocateSystemMatrix(velocity_numbering_subset, velocity_numbering_subset);
            parent::AllocateSystemVector(velocity_numbering_subset);
        } else
        {
            // We do need only the upper blocks: bottom left is transposed of top right and bottom right is 0.
            parent::AllocateSystemMatrix(velocity_numbering_subset, velocity_numbering_subset);
            parent::AllocateSystemMatrix(velocity_numbering_subset, pressure_numbering_subset);

            parent::AllocateSystemVector(velocity_numbering_subset);
            parent::AllocateSystemVector(pressure_numbering_subset);

            const auto& velocity_solution = GetSystemSolution(velocity_numbering_subset);

            force_vector_ = std::make_unique<GlobalVector>(velocity_solution);
            pressure_term_in_velocity_computation_ = std::make_unique<GlobalVector>(velocity_solution);
            pressure_increment_ = std::make_unique<GlobalVector>(GetSystemSolution(pressure_numbering_subset));
        }
    }


    void VariationalFormulation::ComputeNewVelocity()
    {
        assert(!IsMonolithic() && "Required only for Uzawa computation.");

        const auto& velocity_numbering_subset = GetVelocityNumberingSubset();

        auto& rhs = GetNonCstSystemRhs(velocity_numbering_subset);
        rhs.Copy(GetForceVector());

        auto& pressure_driven_term = GetNonCstPressureTermInVelocityComputation();

        {
            const auto& pressure_numbering_subset = GetPressureNumberingSubset();
            const auto& top_right_matrix_block = GetSystemMatrix(velocity_numbering_subset, pressure_numbering_subset);
            GlobalLinearAlgebraNS::MatMult(
                top_right_matrix_block, GetSystemSolution(pressure_numbering_subset), pressure_driven_term);
        }

        GlobalLinearAlgebraNS::AXPY(-1., pressure_driven_term, rhs);

        static bool first_call = true;

        if (first_call)
        {
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
                velocity_numbering_subset, velocity_numbering_subset);
            SolveLinear<IsFactorized::no>(velocity_numbering_subset, velocity_numbering_subset);
            first_call = false;
        } else
        {
            ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(velocity_numbering_subset,
                                                                                      velocity_numbering_subset);
            SolveLinear<IsFactorized::yes>(velocity_numbering_subset, velocity_numbering_subset);
        }

        WriteSolution(this->GetTimeManager(), velocity_numbering_subset);
    }


    void VariationalFormulation::ComputeNewPressure()
    {
        assert(!IsMonolithic() && "Required only for Uzawa computation.");

        const auto& velocity_numbering_subset = GetVelocityNumberingSubset();
        const auto& pressure_numbering_subset = GetPressureNumberingSubset();

        auto& solution = GetNonCstSystemSolution(pressure_numbering_subset);

        const auto& top_right_matrix_block = GetSystemMatrix(velocity_numbering_subset, pressure_numbering_subset);

        auto& pressure_increment = GetNonCstPressureIncrement();

        GlobalLinearAlgebraNS::MatMultTranspose(
            top_right_matrix_block, GetSystemSolution(velocity_numbering_subset), pressure_increment);

        GlobalLinearAlgebraNS::AXPY(GetCouplingTerm(), pressure_increment, solution);
    }


} // namespace MoReFEM::StokesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
