// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_STOKES_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_MODELINSTANCES_STOKES_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/Stokes/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"     // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/BilinearForm/ScalarDivVectorial.hpp" // IWYU pragma: export
#else                                                                    // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
#include "OperatorInstances/VariationalOperator/BilinearForm/Stokes.hpp" // IWYU pragma: export
#endif                                                                   // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

#include "FormulationSolver/Internal/Snes/SnesInterface.hpp" // IWYU pragma: export


namespace MoReFEM::StokesNS
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

    inline const GlobalVariationalOperatorNS::ScalarDivVectorial&
    VariationalFormulation ::GetScalarDivVectorialOperator() const noexcept
    {
        assert(!(!scalar_div_vectorial_operator_));
        return *scalar_div_vectorial_operator_;
    }


    inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
    VariationalFormulation::GetVelocityStiffnessOperator() const noexcept
    {
        assert(!(!velocity_stiffness_operator_));
        return *velocity_stiffness_operator_;
    }


#else // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

    inline auto VariationalFormulation ::GetStokesOperator() const noexcept -> const stokes_op_type&
    {
        assert(!(!stokes_operator_));
        return *stokes_operator_;
    }

#endif // MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS

    inline auto VariationalFormulation::GetFluidViscosity() const -> const scalar_parameter_type&
    {
        assert(!(!fluid_viscosity_));
        return *fluid_viscosity_;
    }


    inline const NumberingSubset& VariationalFormulation::GetVelocityNumberingSubset() const
    {
        return velocity_numbering_subset_;
    }


    inline const NumberingSubset& VariationalFormulation::GetPressureNumberingSubset() const
    {
        return pressure_numbering_subset_;
    }


    inline double VariationalFormulation::GetCouplingTerm() const
    {
        return uzawa_coupling_term_;
    }


    inline const GlobalVector& VariationalFormulation::GetForceVector() const
    {
        assert(!(!force_vector_));
        return *force_vector_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstForceVector()
    {
        return const_cast<GlobalVector&>(GetForceVector());
    }


    inline const GlobalVector& VariationalFormulation::GetPressureTermInVelocityComputation() const
    {
        assert(!(!pressure_term_in_velocity_computation_));
        return *pressure_term_in_velocity_computation_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstPressureTermInVelocityComputation()
    {
        return const_cast<GlobalVector&>(GetPressureTermInVelocityComputation());
    }


    inline GlobalVector& VariationalFormulation::GetNonCstPressureIncrement()
    {
        assert(!(!pressure_increment_));
        return *pressure_increment_;
    }


    inline bool VariationalFormulation::IsMonolithic() const
    {
        return GetVelocityNumberingSubset() == GetPressureNumberingSubset();
    }


} // namespace MoReFEM::StokesNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_STOKES_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
