// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <filesystem>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE model_stokes

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ModelInstances/Stokes/Environment.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq");
}


BOOST_FIXTURE_TEST_CASE(mpi4, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4");
}


BOOST_FIXTURE_TEST_CASE(mpi4_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData");
}

PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        std::string root_dir_path;
        std::string output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path = environment.GetEnvironmentVariable("MOREFEM_ROOT"));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR"));

        const FilesystemNS::Directory root_dir(std::filesystem::path{ root_dir_path }, FilesystemNS::behaviour::read);

        const FilesystemNS::Directory output_dir(std::filesystem::path{ output_dir_path },
                                                 FilesystemNS::behaviour::read);

        BOOST_REQUIRE_NO_THROW(root_dir.ActOnFilesystem());
        BOOST_REQUIRE_NO_THROW(output_dir.ActOnFilesystem());

        FilesystemNS::Directory ref_dir(
            root_dir, std::vector<std::string>{ "Sources", "ModelInstances", "Stokes", "ExpectedResults" });

        static bool first_call = true;

        if (first_call)
        {
            StokesNS::SetStokesConfiguration();
            first_call = false;
        }

        FilesystemNS::Directory obtained_dir(
            output_dir,
            std::vector<std::string>{
                seq_or_par, "Ascii", "Stokes", environment.GetEnvironmentVariable("MOREFEM_STOKES_N_OP"), "Rank_0" });

        if (seq_or_par != "Mpi4_FromPrepartitionedData")
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata");

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata");

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo");
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case");

        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        [[maybe_unused]] auto pressure_test =
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, "pressure.00000.scl", 1.e-12);
        [[maybe_unused]] auto velocity_test =
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, "velocity.00000.scl", 1.e-8);
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace
