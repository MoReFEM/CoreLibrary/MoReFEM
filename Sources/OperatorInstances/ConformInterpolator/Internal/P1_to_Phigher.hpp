// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_INTERNAL_P1_TO_PHIGHER_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_INTERNAL_P1_TO_PHIGHER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Operators/ConformInterpolator/Lagrangian/LagrangianInterpolator.hpp"


namespace MoReFEM::Internal::ConformInterpolatorNS
{


    //! Convenient alias to pairing.
    using pairing_type = ::MoReFEM::ConformInterpolatorNS::pairing_type;


    /*!
     * \brief Family of interpolators that extend a P1 finite element to higher order: P1b, P2, etc...
     *
     * \tparam LocalInterpolatorT The local interpolator that describes exactly how the interpolation matrix
     * is written locally. It's there for instance that is defined the higher order that is to be attained.
     *
     */
    template<class LocalInterpolatorT>
    // clang-format off
            class P1_to_Phigher
            : public ::MoReFEM::ConformInterpolatorNS::LagrangianNS::LagrangianInterpolator
            <
                P1_to_Phigher<LocalInterpolatorT>,
                LocalInterpolatorT
            >
    // clang-format on
    {
      private:
        //! \copydoc doxygen_hide_alias_self
        using self = P1_to_Phigher<LocalInterpolatorT>;

        //! Convenient alias.
        // clang-format off
                using parent = ::MoReFEM::ConformInterpolatorNS::LagrangianNS::LagrangianInterpolator
                <
                    self,
                    LocalInterpolatorT
                >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<self>;

        //! Class name.
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \attention Constructor call must be followed by Init() to fully initialize the interpolator.
         *
         * \param[in] source_felt_space Finite element space of the source.
         * \param[in] source_numbering_subset Numbering subset of the source. First arguments of \a pairing
         * pairs must belong to it, and \a source_felt_space must encompass it.
         * \param[in] target_felt_space Finite element space of the target.
         * \param[in] target_numbering_subset Numbering subset of the target. Second arguments of \a pairing
         * pairs must belong to it, and \a target_felt_space must encompass it.
         *
         * \param[in] pairing A vector of pair in which each pair is an association between an unknown of the
         * source and one from the target. For instance if we consider a fluid with (vf, pf) and a solid with
         * (vs, ds) and want to interpolate from the former to the latter, (vf, vs) must be specified to
         * indicate ds and pf are ignored and vf is associated with vs.
         *
         */
        explicit P1_to_Phigher(const FEltSpace& source_felt_space,
                               const NumberingSubset& source_numbering_subset,
                               const FEltSpace& target_felt_space,
                               const NumberingSubset& target_numbering_subset,
                               pairing_type&& pairing);

        //! Destructor.
        ~P1_to_Phigher() = default;

        //! \copydoc doxygen_hide_copy_constructor
        P1_to_Phigher(const P1_to_Phigher& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        P1_to_Phigher(P1_to_Phigher&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        P1_to_Phigher& operator=(const P1_to_Phigher& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        P1_to_Phigher& operator=(P1_to_Phigher&& rhs) = delete;

        ///@}

      private:
    };


} // namespace MoReFEM::Internal::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ConformInterpolator/Internal/P1_to_Phigher.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_INTERNAL_P1_TO_PHIGHER_DOT_HPP_
// *** MoReFEM end header guards *** < //
