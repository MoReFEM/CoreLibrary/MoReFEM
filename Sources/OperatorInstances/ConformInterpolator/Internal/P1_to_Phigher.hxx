// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_INTERNAL_P1_TO_PHIGHER_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_INTERNAL_P1_TO_PHIGHER_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ConformInterpolator/Internal/P1_to_Phigher.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::ConformInterpolatorNS
{


    template<class LocalInterpolatorT>
    const std::string& P1_to_Phigher<LocalInterpolatorT>::ClassName()
    {
        static const std::string ret = "P1_to_" + LocalInterpolatorT::GetTargetShapeFunctionLabel();
        return ret;
    }


    template<class LocalInterpolatorT>
    P1_to_Phigher<LocalInterpolatorT>::P1_to_Phigher(const FEltSpace& source_felt_space,
                                                     const NumberingSubset& source_numbering_subset,
                                                     const FEltSpace& target_felt_space,
                                                     const NumberingSubset& target_numbering_subset,
                                                     pairing_type&& pairing)
    : parent(source_felt_space, source_numbering_subset, target_felt_space, target_numbering_subset, std::move(pairing))
    {
#ifndef NDEBUG

        const auto& interpolation_data = parent::GetInterpolationData();
        const auto& source_unknown_storage = interpolation_data.GetSourceData().GetExtendedUnknownList();

        assert(std::all_of(source_unknown_storage.cbegin(),
                           source_unknown_storage.cend(),
                           [](const auto& extended_unknown_ptr)
                           {
                               assert(!(!extended_unknown_ptr));
                               return extended_unknown_ptr->GetShapeFunctionLabel() == "P1";
                           }));

        const auto& target_unknown_storage = interpolation_data.GetTargetData().GetExtendedUnknownList();

        assert(std::all_of(target_unknown_storage.cbegin(),
                           target_unknown_storage.cend(),
                           [](const auto& extended_unknown_ptr)
                           {
                               if (!extended_unknown_ptr)
                                   return true; // some unknowns may be dropped.

                               return extended_unknown_ptr->GetShapeFunctionLabel()
                                      == LocalInterpolatorT::GetTargetShapeFunctionLabel();
                           }));
#endif // NDEBUG
    }


} // namespace MoReFEM::Internal::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_INTERNAL_P1_TO_PHIGHER_DOT_HXX_
// *** MoReFEM end header guards *** < //
