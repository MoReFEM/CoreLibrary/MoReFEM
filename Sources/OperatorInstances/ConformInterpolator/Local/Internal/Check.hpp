// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_INTERNAL_CHECK_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_INTERNAL_CHECK_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM::Internal::ConformInterpolatorNS::Local::Impl
{


#ifndef NDEBUG
    /*!
     * \brief Assert the local node lists are consistent, i.e. that broader one is an extension of p1
     * one.
     *
     * \param[in] p1_local_node_list List of P1 local nodes.
     * \param[in] broader_local_node_list List of all local nodes.
     */
    void AssertLocalNodeConsistency(const Advanced::LocalNode::vector_const_shared_ptr& p1_local_node_list,
                                    const Advanced::LocalNode::vector_const_shared_ptr& broader_local_node_list);
#endif // NDEBUG


} // namespace MoReFEM::Internal::ConformInterpolatorNS::Local::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_INTERNAL_CHECK_DOT_HPP_
// *** MoReFEM end header guards *** < //
