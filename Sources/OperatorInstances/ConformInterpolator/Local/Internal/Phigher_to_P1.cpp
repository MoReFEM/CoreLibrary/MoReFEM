// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <utility>

#include "OperatorInstances/ConformInterpolator/Local/Internal/Phigher_to_P1.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"

#include "OperatorInstances/ConformInterpolator/Local/Internal/Check.hpp"


namespace MoReFEM::Internal::ConformInterpolatorNS::Local
{


    Phigher_to_P1::~Phigher_to_P1() = default;


    Phigher_to_P1::Phigher_to_P1(const FEltSpace& p_higher_felt_space,
                                 const Internal::RefFEltNS::RefLocalFEltSpace& p1_ref_local_felt_space,
                                 const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
    : parent(p1_ref_local_felt_space.GetRefGeomElt(), interpolation_data)
    {
        const auto& ref_geom_elt = GetRefGeomElt();

        const auto& p_higher_ref_local_felt_space = parent::GetRefLocalFEltSpace(p_higher_felt_space, ref_geom_elt);

        auto& local_projection_matrix = GetNonCstProjectionMatrix();

        const auto& source_data = interpolation_data.GetSourceData();
        const auto& target_data = interpolation_data.GetTargetData();

        const auto& p1_ref_felt = target_data.GetCommonBasicRefFElt(p1_ref_local_felt_space);

        const auto& p_higher_ref_felt = source_data.GetCommonBasicRefFElt(p_higher_ref_local_felt_space);

        const auto& p_higher_local_node_list = p_higher_ref_felt.GetLocalNodeList();
        const auto& p1_local_node_list = p1_ref_felt.GetLocalNodeList();

#ifndef NDEBUG
        Impl::AssertLocalNodeConsistency(p1_local_node_list, p_higher_local_node_list);
#endif // NDEBUG

        local_projection_matrix.resize(
            static_cast<Eigen::Index>(p1_local_node_list.size()) * target_data.NunknownComponent(),
            static_cast<Eigen::Index>(p_higher_local_node_list.size()) * source_data.NunknownComponent());
        local_projection_matrix.setZero();

        const auto Nnode_in_col = static_cast<Eigen::Index>(p_higher_local_node_list.size());
        const auto Nnode_in_row = static_cast<Eigen::Index>(p1_local_node_list.size());
        assert(Nnode_in_col > Nnode_in_row);

        // Prepare the content of the block that will be repeated for each unknown component.
        Eigen::MatrixXd block(Nnode_in_row, Nnode_in_col);
        block.setZero();

        for (auto i = Eigen::Index{}; i < Nnode_in_row; ++i)
            block(i, i) = 1.;

        // Now repeat the block for the others unknowns or components.
        FillMatrixFromNodeBlock(std::move(block));
    }


} // namespace MoReFEM::Internal::ConformInterpolatorNS::Local


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
