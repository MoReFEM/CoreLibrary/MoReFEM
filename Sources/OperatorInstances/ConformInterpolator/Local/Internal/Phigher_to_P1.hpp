// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_INTERNAL_PHIGHER_TO_P1_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_INTERNAL_PHIGHER_TO_P1_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <type_traits>

#include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"

#include "OperatorInstances/ConformInterpolator/Local/FwdForHpp.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

// Ideally should be in FwdForHpp.hpp, but not yet supported:
// https://github.com/include-what-you-use/include-what-you-use/issues/608
namespace MoReFEM { class FEltSpace; }  // IWYU pragma: export
namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }  // IWYU pragma: export
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }  // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ConformInterpolatorNS::Local
{


    /*!
     * \brief Local interpolator from  P{n} to P1 when N > 1.
     */
    class Phigher_to_P1 : public ::MoReFEM::ConformInterpolatorNS::LagrangianNS::LocalLagrangianInterpolator
    {

      private:
        //! Alias to parent.
        using parent = ::MoReFEM::ConformInterpolatorNS::LagrangianNS::LocalLagrangianInterpolator;

        static_assert(std::is_convertible<self*, parent*>());


      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Phigher_to_P1;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] p_higher_felt_space \a FEltSpace of the source.
         * \param[in] p1_ref_local_felt_space \a RefLocalFEltSpace considered for the target.
         * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
         */
        explicit Phigher_to_P1(const FEltSpace& p_higher_felt_space,
                               const Internal::RefFEltNS::RefLocalFEltSpace& p1_ref_local_felt_space,
                               const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);

        //! Destructor.
        virtual ~Phigher_to_P1() override;

        //! \copydoc doxygen_hide_copy_constructor
        Phigher_to_P1(const Phigher_to_P1& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Phigher_to_P1(Phigher_to_P1&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Phigher_to_P1& operator=(const Phigher_to_P1& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Phigher_to_P1& operator=(Phigher_to_P1&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::Internal::ConformInterpolatorNS::Local


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_INTERNAL_PHIGHER_TO_P1_DOT_HPP_
// *** MoReFEM end header guards *** < //
