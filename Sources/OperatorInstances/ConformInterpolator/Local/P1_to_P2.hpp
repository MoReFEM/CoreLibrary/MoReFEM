// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_P1_TO_P2_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_P1_TO_P2_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <type_traits>

#include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"

#include "OperatorInstances/ConformInterpolator/Local/FwdForHpp.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

// Ideally should be in FwdForHpp.hpp, but not yet supported:
// https://github.com/include-what-you-use/include-what-you-use/issues/608
namespace MoReFEM { class FEltSpace; }  // IWYU pragma: export
namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }  // IWYU pragma: export
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }  // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ConformInterpolatorNS::Local
{


    /*!
     * \brief Local operator to interpolate from P1 to P2.
     */
    class P1_to_P2 : public LagrangianNS::LocalLagrangianInterpolator
    {
      private:
        //! Alias to parent.
        using parent = LagrangianNS::LocalLagrangianInterpolator;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = P1_to_P2;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Label of the target shape function label.
        static const std::string& GetTargetShapeFunctionLabel();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] p1_felt_space \a FEltSpace for the P1 dofs,
         * \param[in] p2_ref_local_felt_space \a RefLocalFEltSpace for the target (with the P2 dofs).
         * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
         *
         */
        explicit P1_to_P2(const FEltSpace& p1_felt_space,
                          const Internal::RefFEltNS::RefLocalFEltSpace& p2_ref_local_felt_space,
                          const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);

        //! Destructor.
        virtual ~P1_to_P2() override;

        //! \copydoc doxygen_hide_copy_constructor
        P1_to_P2(const P1_to_P2& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        P1_to_P2(P1_to_P2&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        P1_to_P2& operator=(const P1_to_P2& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        P1_to_P2& operator=(P1_to_P2&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::ConformInterpolatorNS::Local


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ConformInterpolator/Local/P1_to_P2.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_P1_TO_P2_DOT_HPP_
// *** MoReFEM end header guards *** < //
