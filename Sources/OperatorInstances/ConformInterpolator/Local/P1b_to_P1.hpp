// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_P1B_TO_P1_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_P1B_TO_P1_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <type_traits> // IWYU pragma: keep

#include "OperatorInstances/ConformInterpolator/Local/FwdForHpp.hpp"
#include "OperatorInstances/ConformInterpolator/Local/Internal/Phigher_to_P1.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

// Ideally should be in FwdForHpp.hpp, but not yet supported:
// https://github.com/include-what-you-use/include-what-you-use/issues/608
namespace MoReFEM { class FEltSpace; }  // IWYU pragma: export
namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }  // IWYU pragma: export
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }  // IWYU pragma: export

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ConformInterpolatorNS::Local
{


    /*!
     * \brief Local operator that defines P1 -> P1b interpolation.
     */
    class P1b_to_P1 : public ::MoReFEM::Internal::ConformInterpolatorNS::Local::Phigher_to_P1
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = P1b_to_P1;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Class name.
        static const std::string& ClassName();

        //! Label of the source shape function label.
        static const std::string& GetSourceShapeFunctionLabel();

        //! Parent.
        using parent = ::MoReFEM::Internal::ConformInterpolatorNS::Local::Phigher_to_P1;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] source_felt_space \a FEltSpace with the P1b unknown.
         * \param[in] target_ref_local_felt_space \a RefLocalFEltSpace for the P1 unknown.
         * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
         *
         */
        explicit P1b_to_P1(const FEltSpace& source_felt_space,
                           const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                           const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);

        //! Destructor.
        virtual ~P1b_to_P1() override;

        //! \copydoc doxygen_hide_copy_constructor
        P1b_to_P1(const P1b_to_P1& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        P1b_to_P1(P1b_to_P1&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        P1b_to_P1& operator=(const P1b_to_P1& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        P1b_to_P1& operator=(P1b_to_P1&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::ConformInterpolatorNS::Local


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_LOCAL_P1B_TO_P1_DOT_HPP_
// *** MoReFEM end header guards *** < //
