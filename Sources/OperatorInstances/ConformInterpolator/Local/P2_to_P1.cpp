// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "OperatorInstances/ConformInterpolator/Local/P2_to_P1.hpp"


namespace MoReFEM::ConformInterpolatorNS::Local
{


    const std::string& P2_to_P1::ClassName()
    {
        static const std::string ret = GetSourceShapeFunctionLabel() + "_to_P1";
        return ret;
    }


    const std::string& P2_to_P1::GetSourceShapeFunctionLabel()
    {
        static const std::string ret("P2");
        return ret;
    }


    P2_to_P1::P2_to_P1(const FEltSpace& source_felt_space,
                       const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                       const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
    : parent(source_felt_space, target_ref_local_felt_space, interpolation_data)
    { }


    P2_to_P1::~P2_to_P1() = default;


} // namespace MoReFEM::ConformInterpolatorNS::Local


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
