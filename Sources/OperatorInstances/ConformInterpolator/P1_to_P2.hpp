// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_P1_TO_P2_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_P1_TO_P2_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "OperatorInstances/ConformInterpolator/Internal/P1_to_Phigher.hpp"
#include "OperatorInstances/ConformInterpolator/Local/P1_to_P2.hpp"


namespace MoReFEM::ConformInterpolatorNS
{


    //! Alias that defines P1 -> P2 interpolator.
    using P1_to_P2 = Internal::ConformInterpolatorNS::P1_to_Phigher<Local::P1_to_P2>;


} // namespace MoReFEM::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_P1_TO_P2_DOT_HPP_
// *** MoReFEM end header guards *** < //
