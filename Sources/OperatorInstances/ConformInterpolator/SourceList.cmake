### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/SubsetOrSuperset.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/P1_to_P1b.hpp
		${CMAKE_CURRENT_LIST_DIR}/P1_to_P2.hpp
		${CMAKE_CURRENT_LIST_DIR}/P1b_to_P1.hpp
		${CMAKE_CURRENT_LIST_DIR}/P2_to_P1.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/SubsetOrSuperset.hpp
		${CMAKE_CURRENT_LIST_DIR}/SubsetOrSuperset.hxx
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Local/SourceList.cmake)
