// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <numeric>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "OperatorInstances/ConformInterpolator/SubsetOrSuperset.hpp"

#include "Utilities/Containers/PointerComparison.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


namespace MoReFEM::ConformInterpolatorNS
{


    SubsetOrSuperset::SubsetOrSuperset(const FEltSpace& source_felt_space,
                                       const NumberingSubset& source_numbering_subset,
                                       const FEltSpace& target_felt_space,
                                       const NumberingSubset& target_numbering_subset)
    : parent(source_felt_space, source_numbering_subset, target_felt_space, target_numbering_subset, pairing_type())
    { }


    namespace // anonymous
    {


        using dof_iterator = Dof::vector_shared_ptr::const_iterator;


        /*!
         * \brief Create the shell of the projection matrix.
         *
         * This matrix is diagonal per nature; its diagonal terms are not yet filled at this point.
         */
        void CreateProjectionMatrix(const Dof::vector_shared_ptr& source_processor_wise_dof_list,
                                    const Dof::vector_shared_ptr& target_processor_wise_dof_list,
                                    const Wrappers::Mpi& mpi,
                                    GlobalMatrix& projection_matrix,
                                    std::vector<std::vector<PetscInt>>& diagonal_structure);


        /*!
         * \brief Set the correct values in the \a projection_matrix.
         *
         * This matrix is diagonal per nature; its diagonal terms are not yet filled at this point.
         */
        void SetValuesInProjectionMatrix(const FEltSpace& source,
                                         const Dof::vector_shared_ptr& target_processor_wise_dof_list,
                                         const std::vector<std::vector<PetscInt>>& diagonal_structure,
                                         GlobalMatrix& projection_matrix);

#ifndef NDEBUG
        /*!
         * \bried Check one of the dof list is a subset of the other.
         */
        void AssertValidity(const Dof::vector_shared_ptr& source_dof_list,
                            const Dof::vector_shared_ptr& target_dof_list);
#endif // NDEBUG

    } // namespace


    void SubsetOrSuperset::ComputeInterpolationMatrix()
    {
        const auto& interpolation_data = parent::GetInterpolationData();
        const auto& source_data = interpolation_data.GetSourceData();
        const auto& target_data = interpolation_data.GetTargetData();

        const auto& source_felt_space = source_data.GetFEltSpace();
        const auto& target_felt_space = target_data.GetFEltSpace();

        auto god_of_dof_ptr = source_felt_space.GetGodOfDofFromWeakPtr(); // not a reference on purpose!
        assert(!(!god_of_dof_ptr));
        const auto& god_of_dof = *god_of_dof_ptr;
        const auto& mpi = god_of_dof.GetMpi();
        assert(god_of_dof_ptr->GetUniqueId() == target_felt_space.GetGodOfDofFromWeakPtr()->GetUniqueId()
               && "Both finite element space must belong to the same GodOfDof!");

        const auto& complete_source_dof_list =
            source_felt_space.GetProcessorWiseDofList(source_data.GetNumberingSubset());
        const auto& complete_target_dof_list =
            target_felt_space.GetProcessorWiseDofList(target_data.GetNumberingSubset());

        const auto& source_numbering_subset = source_data.GetNumberingSubset();
        const auto& target_numbering_subset = target_data.GetNumberingSubset();

        Dof::vector_shared_ptr source_dof_list;
        Dof::vector_shared_ptr target_dof_list;

        std::ranges::copy_if(complete_source_dof_list,

                             std::back_inserter(source_dof_list),
                             [&source_numbering_subset](const auto& dof_ptr)
                             {
                                 assert(!(!dof_ptr));
                                 return dof_ptr->IsInNumberingSubset(source_numbering_subset);
                             });

        std::ranges::copy_if(complete_target_dof_list,

                             std::back_inserter(target_dof_list),
                             [&target_numbering_subset](const auto& dof_ptr)
                             {
                                 assert(!(!dof_ptr));
                                 return dof_ptr->IsInNumberingSubset(target_numbering_subset);
                             });

#ifndef NDEBUG
        AssertValidity(source_dof_list, target_dof_list);
#endif // NDEBUG

        auto& projection_matrix = GetNonCstInterpolationMatrix();

        std::vector<std::vector<PetscInt>> diagonal_structure;

        CreateProjectionMatrix(source_dof_list, target_dof_list, mpi, projection_matrix, diagonal_structure);

        SetValuesInProjectionMatrix(source_felt_space, target_dof_list, diagonal_structure, projection_matrix);
    }


    namespace // anonymous
    {


        void CreateProjectionMatrix(const Dof::vector_shared_ptr& source_processor_wise_dof_list,
                                    const Dof::vector_shared_ptr& target_processor_wise_dof_list,
                                    const Wrappers::Mpi& mpi,
                                    GlobalMatrix& projection_matrix,
                                    std::vector<std::vector<PetscInt>>& diagonal_structure)
        {
            const auto Nprocessor_wise_dof_in_source = source_processor_wise_dof_list.size();

            const auto Nprocessor_wise_dof_in_target = target_processor_wise_dof_list.size();

            diagonal_structure.reserve(Nprocessor_wise_dof_in_target);

            const auto begin_source = source_processor_wise_dof_list.cbegin();
            const auto end_source = source_processor_wise_dof_list.cend();

            {
                // Matrix pattern uses at index the processor-wise dof index and in its values the program-wise
                // one...
                for (const auto& dof_ptr : target_processor_wise_dof_list)
                {
                    assert(!(!dof_ptr));

                    if (std::find(begin_source, end_source, dof_ptr) != end_source)
                    {
                        std::vector<PetscInt>&& value{ static_cast<PetscInt>(
                            dof_ptr->GetProgramWiseIndex(projection_matrix.GetColNumberingSubset()).Get()) };
                        diagonal_structure.emplace_back(value);
                    } else
                        diagonal_structure.emplace_back();
                }

                assert(diagonal_structure.size() == Nprocessor_wise_dof_in_target);

                const Wrappers::Petsc::MatrixPattern matrix_pattern(diagonal_structure);

                if (mpi.IsSequential())
                {
                    projection_matrix.InitSequentialMatrix(
                        row_processor_wise_index_type{ static_cast<PetscInt>(Nprocessor_wise_dof_in_target) },
                        col_processor_wise_index_type{ static_cast<PetscInt>(Nprocessor_wise_dof_in_source) },
                        matrix_pattern,
                        mpi);
                } else
                {
                    const auto Nprogram_wise_dof_in_source =
                        mpi.AllReduce(Nprocessor_wise_dof_in_source, Wrappers::MpiNS::Op::Sum);

                    const auto Nprogram_wise_dof_in_target =
                        mpi.AllReduce(Nprocessor_wise_dof_in_target, Wrappers::MpiNS::Op::Sum);

                    projection_matrix.InitParallelMatrix(
                        row_processor_wise_index_type{ static_cast<PetscInt>(Nprocessor_wise_dof_in_target) },
                        col_processor_wise_index_type{ static_cast<PetscInt>(Nprocessor_wise_dof_in_source) },
                        row_program_wise_index_type{ static_cast<PetscInt>(Nprogram_wise_dof_in_target) },
                        col_program_wise_index_type{ static_cast<PetscInt>(Nprogram_wise_dof_in_source) },
                        matrix_pattern,
                        mpi);
                }
            }
        }


        void SetValuesInProjectionMatrix(const FEltSpace& source,
                                         const Dof::vector_shared_ptr& target_processor_wise_dof_list,
                                         const std::vector<std::vector<PetscInt>>& diagonal_structure,
                                         GlobalMatrix& projection_matrix)
        {
            // Zero all the terms in the matrix (a safety; probably not required).
            projection_matrix.ZeroEntries();

            const auto Ntarget_processor_wise_dof = static_cast<PetscInt>(target_processor_wise_dof_list.size());

            const auto& mpi = source.GetMpi();

            auto Nproc_dof_per_proc = mpi.CollectFromEachProcessor(Ntarget_processor_wise_dof);

            using difference_type = decltype(Nproc_dof_per_proc)::difference_type;

            const auto rank = mpi.GetRank<difference_type>();

            auto first_row_index = std::accumulate(Nproc_dof_per_proc.cbegin(), Nproc_dof_per_proc.cbegin() + rank, 0);

            // Add the relevant 1: 1 per line at program-wise dof column.
            PetscInt row = first_row_index;

            std::size_t current_index = 0UL;

            for (const auto& dof_ptr : target_processor_wise_dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;

                assert(current_index < diagonal_structure.size());
                if (!diagonal_structure[current_index].empty())
                {
                    projection_matrix.SetValue(
                        row,
                        static_cast<PetscInt>(dof.GetProgramWiseIndex(projection_matrix.GetColNumberingSubset()).Get()),
                        static_cast<PetscScalar>(1.),
                        INSERT_VALUES);
                }

                ++row;
                ++current_index;
            }

            assert(current_index == target_processor_wise_dof_list.size());

            projection_matrix.Assembly();
        }


#ifndef NDEBUG
        /*!
         * \bried Check one of the dof list is a subset of the other.
         */
        void AssertValidity(const Dof::vector_shared_ptr& source_dof_list,
                            const Dof::vector_shared_ptr& target_dof_list)
        {
            assert(std::ranges::is_sorted(source_dof_list,

                                          Utilities::PointerComparison::Less<Dof::shared_ptr>()));
            assert(std::ranges::is_sorted(target_dof_list,

                                          Utilities::PointerComparison::Less<Dof::shared_ptr>()));

            Dof::vector_shared_ptr intersection_list;

            std::ranges::set_intersection(source_dof_list,

                                          target_dof_list,

                                          std::back_inserter(intersection_list),
                                          Utilities::PointerComparison::Less<Dof::shared_ptr>());

            assert(intersection_list.size() == std::min(source_dof_list.size(), target_dof_list.size())
                   && "This interpolator should only be used to determine either subset or superset of the source "
                      "dof list");
        }
#endif // NDEBUG


    } // namespace


} // namespace MoReFEM::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
