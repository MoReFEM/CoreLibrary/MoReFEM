// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_SUBSETORSUPERSET_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_SUBSETORSUPERSET_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "Operators/ConformInterpolator/Crtp/LagrangianInterpolator.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ConformInterpolatorNS
{


    /*!
     * \brief Interpolate from a (\a FEltSpace, \a NumberingSubset) to either a larger or smaller target.
     *
     * Target must be either a subset or a superset of source.
     */
    class SubsetOrSuperset final : public Crtp::LagrangianInterpolator<SubsetOrSuperset>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SubsetOrSuperset;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


      private:
        //! Alias to parent.
        using parent = Crtp::LagrangianInterpolator<SubsetOrSuperset>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Frienship to parent.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] source_felt_space \a FEltSpace of the source.
         * \param[in] source_numbering_subset \a NumberingSubset of the source.
         * \param[in] target_felt_space \a FEltSpace of the target.
         * \param[in] target_numbering_subset \a NumberingSubset of the target.
         */
        explicit SubsetOrSuperset(const FEltSpace& source_felt_space,
                                  const NumberingSubset& source_numbering_subset,
                                  const FEltSpace& target_felt_space,
                                  const NumberingSubset& target_numbering_subset);

        //! Destructor.
        ~SubsetOrSuperset() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SubsetOrSuperset(const SubsetOrSuperset& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SubsetOrSuperset(SubsetOrSuperset&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SubsetOrSuperset& operator=(const SubsetOrSuperset& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SubsetOrSuperset& operator=(SubsetOrSuperset&& rhs) = delete;

        ///@}

      private:
        /*!
         * \brief Init interpolation matrix.
         *
         * \internal <b><tt>[internal]</tt></b> This method is expected by the parent/Crtp class.
         * \endinternal
         */
        void ComputeInterpolationMatrix();
    };


} // namespace MoReFEM::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ConformInterpolator/SubsetOrSuperset.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_SUBSETORSUPERSET_DOT_HPP_
// *** MoReFEM end header guards *** < //
