// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_SUBSETORSUPERSET_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_SUBSETORSUPERSET_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ConformInterpolator/SubsetOrSuperset.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ConformInterpolatorNS
{


} // namespace MoReFEM::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_CONFORMINTERPOLATOR_SUBSETORSUPERSET_DOT_HXX_
// *** MoReFEM end header guards *** < //
