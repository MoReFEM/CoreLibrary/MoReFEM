// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ADVANCED_INVARIANTHOLDER_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ADVANCED_INVARIANTHOLDER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/TupleIteration.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


namespace MoReFEM::Advanced
{

    /*!
     * \brief Class in charge of storing the relevant invariants and provide derivates with respect to
     * Cauchy-Green tensor.
     *
     * Hyperelastic laws are expected to derive from this class and use the invariants/
     *
     * \tparam InvariantsT The invariants to consider for the hyperelastic law.
     *
     * There is typically an extra level of indirection  in the laws, e.g.
     *
     \code
      template
     <
         ::MoReFEM::InvariantNS::coords CoordsEnumT = ::MoReFEM::InvariantNS::coords::cartesian
     >
     class CiarletGeymonat
     : public Advanced::InvariantHolder
     <
         typename InvariantNS::Invariant1<CoordsEnumT>::type,
         typename InvariantNS::Invariant2<CoordsEnumT>::type,
         typename InvariantNS::Invariant3<CoordsEnumT>::type
     >
     \endcode
     *
     * which is there just to handle both cartesian and generalized coordinates.
     *
     */
    // clang-format off
    template
    <
        class... InvariantsT
    >
    // clang-format on
    class InvariantHolder : public InvariantsT...
    {
      public:
        //! Number of invariants.
        static constexpr auto Ninvariant = sizeof...(InvariantsT);

      private:
        //! Convenient alias
        using tuple_type = std::tuple<InvariantsT...>;

        //! Convenient alias over a facility to "iterate" through all invariants stored in the tuple.
        using tuple_iteration = Internal::InvariantNS::TupleIteration<tuple_type, 0UL>;

        /*!
         * \brief Find the position in \a InvariantsT variadic argument of the one which index is \a EnumT.
         *
         * \return Position of the invariants which index is \a EnumT.
         */
        template<InvariantNS::index EnumT>
        static constexpr auto FindIndex();


      public:
        //! Alias to self.
        using self = InvariantHolder<InvariantsT...>;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<self>;

        /*!
         * \brief Tells whether the invariant which matches index \a EnumT is present or not.
         *
         * \return True if one of tue invariants in the template parameters matches \a EnumT.
         */
        template<InvariantNS::index EnumT>
        static constexpr bool IsPresent();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the \a Mesh considered.
         * \param[in] content Tells whether first and second derivates need to be stored.
         * \param[in] invariants The variadic constructor arguments for each invariant.
         * For instance the initialization for `ExponentialJ1J4` looks like:
         \code
         invariant_holder_parent(mesh_dimension, ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv,
         { mesh_dimension }, { mesh_dimension }, { mesh_dimension }, { mesh_dimension, fibers})
         \endcode
         */
        explicit InvariantHolder(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                 InvariantNS::Content content,
                                 InvariantsT&&... invariants);

        //! Destructor.
        virtual ~InvariantHolder() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InvariantHolder(const InvariantHolder& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvariantHolder(InvariantHolder&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvariantHolder& operator=(const InvariantHolder& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvariantHolder& operator=(InvariantHolder&& rhs) = default;

        ///@}


        /*!
         * \brief Get one of the invariants.
         *
         * \tparam  IndexT The index of the invariant to get (e.g. invariants_index::I1).
         *
         * \return The value of the invariant.
         */
        template<InvariantNS::index IndexT>
        double GetInvariant() const noexcept;

        /*!
         * \brief Update the invariants in the case of cartesian coordinates.
         *
         * \copydoc doxygen_hide_invariant_cartesian_update_common
         */
        void UpdateInvariants(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
                              const ::MoReFEM::QuadraturePoint& quad_pt,
                              const ::MoReFEM::GeometricElt& geom_elt);
        /*!
         * \brief Update the invariants in the case of generalized coordinates.
         *
         * \copydoc doxygen_hide_invariant_generalized_update_common
         *
         */
        void UpdateInvariants(const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
                              const ::MoReFEM::QuadraturePoint& quad_pt,
                              const ::MoReFEM::GeometricElt& geom_elt,
                              const InvariantNS::GeneralizedNS::Input& generalized_input,
                              const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis);

        //! Dimension of the mesh.
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;


        //! Get what is actually stored and computed within the class (see \a InvariantNS::Content for more
        //! details).
        InvariantNS::Content GetContent() const noexcept;

        /*!
         * \class doxygen_hide_cauchy_green_tensor_as_vector_arg
         *
         * \param[in] cauchy_green_tensor The vector which holds the values of CauchyGreen tensor; expected content is:
         * - (xx, yy, xy) for dimension 2
         * - (xx, yy, zz, xy, yz, xz) for dimension 3
         * It should have been computed by \a UpdateCauchyGreenTensor operator.
         */


        /*!
         * \brief Get one of the first derivative of the invariants against Cauchy-Green tensor.
         *
         * \return The value of the invariant derivative.
         */
        template<InvariantNS::index IndexT>
        const ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>& GetFirstDerivativeWrtCauchyGreen() const noexcept;

        /*!
         * \brief Get one of the first derivative of the invariants against Cauchy-Green tensor.
         *
         * \return The value of the invariant second derivative.
         */
        template<InvariantNS::index IndexT>
        const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>& GetSecondDerivativeWrtCauchyGreen() const noexcept;


      private:
        //! What is actually stored and computed within the class (see \a InvariantNS::Content for more details).
        const InvariantNS::Content content_;

        //! Dimension of the mesh.
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Advanced/InvariantHolder.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ADVANCED_INVARIANTHOLDER_DOT_HPP_
// *** MoReFEM end header guards *** < //
