// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ADVANCED_INVARIANTHOLDER_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ADVANCED_INVARIANTHOLDER_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Advanced/InvariantHolder.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"


namespace MoReFEM::Advanced
{


    // clang-format off
    template
    <
        class... InvariantsT
    >
    InvariantHolder<InvariantsT...>
    ::InvariantHolder(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                      InvariantNS::Content content,
                      InvariantsT&&... invariants)
    // clang-format on
    : InvariantsT(invariants)..., content_(content), mesh_dimension_(mesh_dimension)
    { }


    // clang-format off
    template
    <
        class... InvariantsT
    >
    template<InvariantNS::index IndexT>
    // clang-format on
    constexpr auto InvariantHolder<InvariantsT...>::FindIndex()
    {
        return tuple_iteration::template FindIndex<IndexT>();
    }


    // clang-format off
    template
    <
        class... InvariantsT
    >
    template<InvariantNS::index IndexT>
    // clang-format on
    double InvariantHolder<InvariantsT...>::GetInvariant() const noexcept
    {
        static_assert(FindIndex<IndexT>() < Ninvariant);

        using parent_to_use = std::tuple_element_t<FindIndex<IndexT>(), tuple_type>;

        return parent_to_use::GetValue();
    }


    // clang-format off
    template
    <
            class... InvariantsT
    >
    template<InvariantNS::index IndexT>
    // clang-format on
    auto InvariantHolder<InvariantsT...>::GetFirstDerivativeWrtCauchyGreen() const noexcept
        -> const ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>&
    {
        static_assert(FindIndex<IndexT>() < Ninvariant);

        using parent_to_use = std::tuple_element_t<FindIndex<IndexT>(), tuple_type>;

        return parent_to_use::GetFirstDerivates();
    }


    // clang-format off
    template
    <
            class... InvariantsT
    >
    template<InvariantNS::index IndexT>
    // clang-format on
    auto InvariantHolder<InvariantsT...>::GetSecondDerivativeWrtCauchyGreen() const noexcept
        -> const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>&
    {
        static_assert(FindIndex<IndexT>() < Ninvariant);

        using parent_to_use = std::tuple_element_t<FindIndex<IndexT>(), tuple_type>;

        return parent_to_use::GetSecondDerivates();
    }


    // clang-format off
    template
    <
        class... InvariantsT
    >
    // clang-format on
    inline auto InvariantHolder<InvariantsT...>::GetMeshDimension() const noexcept
        -> ::MoReFEM::GeometryNS::dimension_type
    {
        return mesh_dimension_;
    }


    // clang-format off
    template
    <
        class... InvariantsT
    >
    // clang-format on
    inline InvariantNS::Content InvariantHolder<InvariantsT...>::GetContent() const noexcept
    {
        return content_;
    }


    // clang-format off
        template
        <
            class... InvariantsT
        >
    // clang-format on
    void
    InvariantHolder<InvariantsT...>::UpdateInvariants(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
                                                      const ::MoReFEM::QuadraturePoint& quad_pt,
                                                      const ::MoReFEM::GeometricElt& geom_elt)
    {

        tuple_iteration::Update(cauchy_green_tensor, quad_pt, geom_elt, *this);
    }


    // clang-format off
    template
    <
        class... InvariantsT
    >
    // clang-format on
    void InvariantHolder<InvariantsT...>::UpdateInvariants(
        const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        const ::MoReFEM::QuadraturePoint& quad_pt,
        const ::MoReFEM::GeometricElt& geom_elt,
        const InvariantNS::GeneralizedNS::Input& generalized_input,
        const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {

        tuple_iteration::Update(
            matricial_cauchy_green_tensor, quad_pt, geom_elt, generalized_input, contravariant_basis, *this);
    }


    // clang-format off
    template
    <
        class... InvariantsT
    >
    // clang-format on
    template<InvariantNS::index EnumT>
    constexpr bool InvariantHolder<InvariantsT...>::IsPresent()
    {
        return tuple_iteration::template IsPresent<EnumT>();
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ADVANCED_INVARIANTHOLDER_DOT_HXX_
// *** MoReFEM end header guards *** < //
