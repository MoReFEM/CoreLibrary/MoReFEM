// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ENUM_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::InvariantNS
{


    /*!
     * \brief Content of InvariantHolder.
     *
     * - invariants mean that only the invariants themselves are stored and computed.
     * - invariants_and_first_deriv mean all first derivates are also stored.
     * - invariants_and_first_and_second_deriv adds second derivates to prior content.
     */
    enum class Content {
        invariants = 0,
        invariants_and_first_deriv,
        invariants_and_first_and_second_deriv,
    };


    /*!
     * \brief Enum class to define invariants numbering.
     *
     */
    enum class index : std::size_t { I1 = 0, I2, I3, I4, I6 };


} // namespace MoReFEM::InvariantNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
