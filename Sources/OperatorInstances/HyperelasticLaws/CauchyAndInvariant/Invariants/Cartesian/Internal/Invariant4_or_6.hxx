// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Internal/Invariant4_or_6.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"


namespace MoReFEM::Internal::InvariantNS::CartesianNS
{

    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::Invariant4_or_6(
        ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        const fiber_type& fiber)
    : parent(mesh_dimension), fiber_(fiber)
    {
        if (mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 1 })
        {
            decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();
            assert(first_deriv.size() == 1);
            first_deriv(0) = 1.;
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::Update(
        const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
        const ::MoReFEM::QuadraturePoint& quad_pt,
        const ::MoReFEM::GeometricElt& geom_elt)
    {
        const auto mesh_dimension = parent::GetMeshDimension();

        const auto& tau_interpolate_at_quad_point = GetFiber().GetValue(quad_pt, geom_elt);

        switch (mesh_dimension.Get())
        {
        case 1:
        {
            assert(cauchy_green_tensor.size() == 1);

            const double xx = cauchy_green_tensor(0);
            const double nx = tau_interpolate_at_quad_point(0);
            const double norm = nx * nx;

            double value = 0.;

            if (!(NumericNS::IsZero(norm)))
            {
                value = nx * xx * nx;
                value /= norm;
            }

            parent::SetValue(value);
        }
        break;
        case 2:
        {
            assert(cauchy_green_tensor.size() == 3);
            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double xy = cauchy_green_tensor(2);

            const double nx = tau_interpolate_at_quad_point(0);
            const double ny = tau_interpolate_at_quad_point(1);

            const double norm = nx * nx + ny * ny;

            double value = 0.;

            if (!(NumericNS::IsZero(norm)))
            {
                value = nx * (nx * xx + ny * xy) + ny * (nx * xy + ny * yy);
                value /= norm;
            }

            parent::SetValue(value);
        }
        break;
        case 3:
        {
            assert(cauchy_green_tensor.size() == 6);

            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double zz = cauchy_green_tensor(2);
            const double xy = cauchy_green_tensor(3);
            const double yz = cauchy_green_tensor(4);
            const double xz = cauchy_green_tensor(5);

            const double nx = tau_interpolate_at_quad_point(0);
            const double ny = tau_interpolate_at_quad_point(1);
            const double nz = tau_interpolate_at_quad_point(2);

            const double norm = nx * nx + ny * ny + nz * nz;

            double value = 0.;

            if (!(NumericNS::IsZero(norm)))
            {
                value = nx * (nx * xx + ny * xy + nz * xz) + ny * (nx * xy + ny * yy + nz * yz)
                        + nz * (nx * xz + ny * yz + nz * zz);
                value /= norm;
            }

            parent::SetValue(value);
        }
        break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::UpdateFirstDerivates(
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
        [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
        [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt)
    {
        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        const auto mesh_dimension = parent::GetMeshDimension();

        const auto& tau_interpolate_at_quad_point = GetFiber().GetValue(quad_pt, geom_elt);

        switch (mesh_dimension.Get())
        {
        case 1:
            // Constant and already set in constructor.
            assert(first_deriv.size() == 1);
            break;
        case 2:
        {
            assert(first_deriv.size() == 3);

            const double nx = tau_interpolate_at_quad_point(0);
            const double ny = tau_interpolate_at_quad_point(1);

            first_deriv.setZero();

            const double norm = nx * nx + ny * ny;

            if (!(NumericNS::IsZero(norm)))
            {
                const double inv_norm = 1. / norm;
                first_deriv(0) = (nx * nx) * inv_norm;
                first_deriv(1) = (ny * ny) * inv_norm;
                first_deriv(2) = (nx * ny) * inv_norm;
            }
        }
        break;
        case 3:
        {
            assert(first_deriv.size() == 6);
            assert(first_deriv.size() == 6);

            const double nx = tau_interpolate_at_quad_point(0);
            const double ny = tau_interpolate_at_quad_point(1);
            const double nz = tau_interpolate_at_quad_point(2);

            first_deriv.setZero();

            const double norm = nx * nx + ny * ny + nz * nz;

            if (!(NumericNS::IsZero(norm)))
            {
                const double inv_norm = 1. / norm;
                first_deriv(0) = (nx * nx) * inv_norm;
                first_deriv(1) = (ny * ny) * inv_norm;
                first_deriv(2) = (nz * nz) * inv_norm;
                first_deriv(3) = (nx * ny) * inv_norm;
                first_deriv(4) = (ny * nz) * inv_norm;
                first_deriv(5) = (nx * nz) * inv_norm;
            }
        }
        break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    auto Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::GetFiber() const noexcept -> const fiber_type&
    {
        return fiber_;
    }


} // namespace MoReFEM::Internal::InvariantNS::CartesianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
// *** MoReFEM end header guards *** < //
