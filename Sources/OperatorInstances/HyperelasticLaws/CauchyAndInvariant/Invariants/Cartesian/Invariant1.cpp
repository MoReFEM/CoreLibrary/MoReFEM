// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>
#include <cstdlib>

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant1.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::InvariantNS::CartesianNS
{


    namespace // anonymous
    {


        void SetConstantFirstDerivates(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                       ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>& first_deriv);

    } // namespace


    Invariant1::Invariant1(::MoReFEM::GeometryNS::dimension_type mesh_dimension) : parent(mesh_dimension)
    {
        SetConstantFirstDerivates(mesh_dimension, parent::GetNonCstFirstDerivates());

        // Second derivates are already filled with zeros
    }


    void Invariant1::Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
                            [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
                            [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt)
    {
        const auto mesh_dimension = GetMeshDimension();

        switch (mesh_dimension.Get())
        {
        case 1:
        {
            assert(cauchy_green_tensor.size() == 1);
            const double value = cauchy_green_tensor(0) + 2.;
            SetValue(value);
        }
        break;
        case 2:
        {
            assert(cauchy_green_tensor.size() == 3);
            const double value = cauchy_green_tensor(0) + cauchy_green_tensor(1) + 1.;
            SetValue(value);
        }
        break;
        case 3:
        {
            assert(cauchy_green_tensor.size() == 6);
            const double value = cauchy_green_tensor(0) + cauchy_green_tensor(1) + cauchy_green_tensor(2);
            SetValue(value);
        }
        break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        void SetConstantFirstDerivates(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                       ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>& first_deriv)
        {
            switch (mesh_dimension.Get())
            {
            case 1:
            {
                assert(first_deriv.size() == 1);
                first_deriv(0) = 1.;
            }
            break;
            case 2:
            {
                assert(first_deriv.size() == 3);
                first_deriv(0) = 1.;
                first_deriv(1) = 1.;
                first_deriv(2) = 0.;
            }
            break;
            case 3:
            {
                assert(first_deriv.size() == 6);
                first_deriv.setZero();
                first_deriv(0) = 1.;
                first_deriv(1) = 1.;
                first_deriv(2) = 1.;
            }
            break;
            default:
                assert(false);
                exit(EXIT_FAILURE);
            }
        }


    } // namespace


} // namespace MoReFEM::InvariantNS::CartesianNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
