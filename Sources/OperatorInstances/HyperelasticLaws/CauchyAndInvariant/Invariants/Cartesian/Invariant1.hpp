// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INVARIANT1_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INVARIANT1_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <type_traits>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InvariantNS::CartesianNS
{


    /*!
     * \brief Invariant I1 for cartesian coordinates
     *
     * \internal For this invariant first and second derivates are constant so there are no Update methods for them.
     */
    class Invariant1 : public ::MoReFEM::Internal::InvariantNS::Base<index::I1>
    {
      public:
        //! Alias to self.
        using self = Invariant1;

        //! Parent class
        using parent = ::MoReFEM::Internal::InvariantNS::Base<index::I1>;

        static_assert(std::is_convertible<self*, parent*>());


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] mesh_dimension Dimension of the mesh.
        Invariant1(::MoReFEM::GeometryNS::dimension_type mesh_dimension);

        //! Destructor.
        ~Invariant1() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Invariant1(const Invariant1& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Invariant1(Invariant1&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Invariant1& operator=(const Invariant1& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Invariant1& operator=(Invariant1&& rhs) = delete;

        ///@}

      public:
        //! \copydoc doxygen_hide_invariant_cartesian_update
        void Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
                    const ::MoReFEM::QuadraturePoint& quad_pt,
                    const ::MoReFEM::GeometricElt& geom_elt);
    };


} // namespace MoReFEM::InvariantNS::CartesianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INVARIANT1_DOT_HPP_
// *** MoReFEM end header guards *** < //
