// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>
#include <cstdlib>

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant2.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::InvariantNS::CartesianNS
{


    namespace // anonymous
    {


        void SetConstantSecondDerivates(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>& second_deriv);

    } // namespace


    Invariant2::Invariant2(::MoReFEM::GeometryNS::dimension_type mesh_dimension) : parent(mesh_dimension)
    {
        // Peculiar case as first derivate is constant.
        if (mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 1 })
        {
            decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();
            assert(first_deriv.size() == 1);
            first_deriv(0) = 2.;
        }


        SetConstantSecondDerivates(mesh_dimension, parent::GetNonCstSecondDerivates());
    }


    void Invariant2::Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
                            [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
                            [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt)
    {
        const auto mesh_dimension = GetMeshDimension();

        switch (mesh_dimension.Get())
        {
        case 1:
        {
            assert(cauchy_green_tensor.size() == 1);
            const double xx = cauchy_green_tensor(0);
            const double value = 2. * xx + 1.;
            SetValue(value);
        }
        break;
        case 2:
        {
            assert(cauchy_green_tensor.size() == 3);
            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double xy = cauchy_green_tensor(2);

            const double value = xx * yy + xx + yy - NumericNS::Square(xy);
            SetValue(value);
        }
        break;
        case 3:
        {
            assert(cauchy_green_tensor.size() == 6);
            using NumericNS::Square;

            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double zz = cauchy_green_tensor(2);
            const double xy = cauchy_green_tensor(3);
            const double yz = cauchy_green_tensor(4);
            const double xz = cauchy_green_tensor(5);

            const double value = xx * yy + yy * zz + xx * zz - Square(xy) - Square(xz) - Square(yz);
            SetValue(value);
        }
        break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    void Invariant2::UpdateFirstDerivates(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
                                          [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
                                          [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt)
    {
        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        const auto mesh_dimension = parent::GetMeshDimension();

        switch (mesh_dimension.Get())
        {
        case 1:
            // Constant and already set in constructor.
            break;
        case 2:
        {
            assert(first_deriv.size() == 3);
            assert(cauchy_green_tensor.size() == 3);
            first_deriv(0) = 1. + cauchy_green_tensor(1);
            first_deriv(1) = 1. + cauchy_green_tensor(0);
            first_deriv(2) = -cauchy_green_tensor(2);
        }
        break;
        case 3:
        {
            assert(first_deriv.size() == 6);
            assert(first_deriv.size() == 6);
            assert(cauchy_green_tensor.size() == 6);
            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double zz = cauchy_green_tensor(2);
            const double xy = cauchy_green_tensor(3);
            const double yz = cauchy_green_tensor(4);
            const double xz = cauchy_green_tensor(5);

            first_deriv(0) = yy + zz;
            first_deriv(1) = xx + zz;
            first_deriv(2) = xx + yy;
            first_deriv(3) = -xy;
            first_deriv(4) = -yz;
            first_deriv(5) = -xz;
        }
        break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        void SetConstantSecondDerivates(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>& second_deriv)
        {
            switch (mesh_dimension.Get())
            {
            case 1:
            {
                assert(second_deriv.rows() == 1);
                assert(second_deriv.cols() == 1);
                second_deriv.setZero();
            }
            break;
            case 2:
            {
                assert(second_deriv.rows() == 3);
                assert(second_deriv.cols() == 3);
                second_deriv.setZero();

                // [0, 1, 0]
                // [1, 0, 0]
                // [0, 0, -0.5]

                second_deriv(2, 2) = -0.5;
                second_deriv(0, 1) = second_deriv(1, 0) = 1.;
            }
            break;
            case 3:
            {
                assert(second_deriv.rows() == 6);
                assert(second_deriv.cols() == 6);
                second_deriv.setZero();

                // [0, 1, 1, 0   , 0   , 0   ],
                // [1, 0, 1, 0   , 0   , 0   ],
                // [1, 1, 0, 0   , 0   , 0   ],
                // [0, 0, 0, -0.5, 0   , 0   ],
                // [0, 0, 0, 0   , -0.5, 0   ],
                // [0, 0, 0, 0   , 0   , -0.5]

                second_deriv(3, 3) = second_deriv(4, 4) = second_deriv(5, 5) = -.5;

                for (auto i = 0; i < 3; ++i)
                    for (auto j = i + 1; j < 3; ++j)
                        second_deriv(i, j) = second_deriv(j, i) = 1.;
            }
            break;
            default:
                assert(false);
                exit(EXIT_FAILURE);
            }
        }


    } // namespace


} // namespace MoReFEM::InvariantNS::CartesianNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
