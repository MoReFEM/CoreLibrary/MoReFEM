// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INVARIANT4_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INVARIANT4_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <type_traits>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/Parameter/FiberEnum.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Internal/Invariant4_or_6.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InvariantNS::CartesianNS
{


    /*!
     * \brief Invariant I4 for cartesian coordinates
     */
    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    using Invariant4 =
        ::MoReFEM::Internal::InvariantNS::CartesianNS::Invariant4_or_6<index::I4, FiberPolicyT, TimeManagerT>;


} // namespace MoReFEM::InvariantNS::CartesianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_CARTESIAN_INVARIANT4_DOT_HPP_
// *** MoReFEM end header guards *** < //
