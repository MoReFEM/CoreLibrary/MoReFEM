// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_GENERALIZEDINPUT_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_GENERALIZEDINPUT_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::InvariantNS::GeneralizedNS
{


    inline auto Input::GetContravariantMetricAsVector() const noexcept -> const vector6d&
    {
        return contravariant_metric_as_vector_;
    }


    inline auto Input::GetNonCstContravariantMetricAsVector() noexcept -> vector6d&
    {
        return const_cast<vector6d&>(GetContravariantMetricAsVector());
    }


    inline auto Input::GetI2HelperMatrix() const noexcept -> const matrix6d&
    {
        assert(is_invariant2_ == is_I2::yes);
        return i2_helper_matrix_;
    }


    inline auto Input::GetNonCstI2HelperMatrix() noexcept -> matrix6d&
    {
        return const_cast<matrix6d&>(GetI2HelperMatrix());
    }


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_GENERALIZEDINPUT_DOT_HXX_
// *** MoReFEM end header guards *** < //
