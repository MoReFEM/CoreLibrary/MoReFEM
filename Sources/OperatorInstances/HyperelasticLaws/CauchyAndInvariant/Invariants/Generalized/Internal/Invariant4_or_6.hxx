// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Internal/Invariant4_or_6.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"


namespace MoReFEM::Internal::InvariantNS::GeneralizedNS
{


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::Invariant4_or_6(
        ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        const fiber_type& fiber)
    : parent(mesh_dimension), fiber_(fiber)
    {
        assert(mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 3 }
               && "Generalized coordinates are used only for 3D-shells so far; "
                  "if you need them for lower dimensions please "
                  "[post an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues)");
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::Update(
        const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        const ::MoReFEM::QuadraturePoint& quad_pt,
        const ::MoReFEM::GeometricElt& geom_elt,
        [[maybe_unused]] const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
        const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        const auto& tau_interpolate_at_quad_point = GetFiber().GetValue(quad_pt, geom_elt);

        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        assert(matricial_cauchy_green_tensor.rows() == 6 && matricial_cauchy_green_tensor.cols() == 1);
        assert(tau_interpolate_at_quad_point.size() == contravariant_basis.rows()
               && tau_interpolate_at_quad_point.size() == contravariant_basis.cols());

        const double Cxx = matricial_cauchy_green_tensor(0, 0);
        const double Cyy = matricial_cauchy_green_tensor(1, 0);
        const double Czz = matricial_cauchy_green_tensor(2, 0);
        const double Cxy = matricial_cauchy_green_tensor(3, 0);
        const double Cyz = matricial_cauchy_green_tensor(4, 0);
        const double Cxz = matricial_cauchy_green_tensor(5, 0);
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

        constexpr Eigen::Index first_component = 0;
        constexpr Eigen::Index second_component = 1;
        constexpr Eigen::Index third_component = 2;

        const double nx = tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, first_component);
        const double ny = tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, second_component);
        const double nz = tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, third_component);

        const double norm = nx * nx + ny * ny + nz * nz;
        double I4{};

        if (!(NumericNS::IsZero(norm)))
        {
            I4 = nx * (nx * Cxx + ny * Cxy + nz * Cxz) + ny * (nx * Cxy + ny * Cyy + nz * Cyz)
                 + nz * (nx * Cxz + ny * Cyz + nz * Czz);
        }

        parent::SetValue(I4);
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::UpdateFirstDerivates(
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
        [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
        [[maybe_unused]] const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        const auto& tau_interpolate_at_quad_point = GetFiber().GetValue(quad_pt, geom_elt);

        assert(tau_interpolate_at_quad_point.size() == contravariant_basis.rows()
               && tau_interpolate_at_quad_point.size() == contravariant_basis.cols());

        constexpr Eigen::Index first_component = 0;
        constexpr Eigen::Index second_component = 1;
        constexpr Eigen::Index third_component = 2;

        const double nx = tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, first_component);
        const double ny = tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, second_component);
        const double nz = tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, third_component);

        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        first_deriv.setZero();

        const double norm = nx * nx + ny * ny + nz * nz;

        if (!(NumericNS::IsZero(norm)))
        {
            first_deriv(0) = (nx * nx);
            first_deriv(1) = (ny * ny);
            first_deriv(2) = (nz * nz);
            first_deriv(3) = (nx * ny);
            first_deriv(4) = (ny * nz);
            first_deriv(5) = (nx * nz);
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    auto Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::GetFiber() const noexcept -> const fiber_type&
    {
        return fiber_;
    }


} // namespace MoReFEM::Internal::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
// *** MoReFEM end header guards *** < //
