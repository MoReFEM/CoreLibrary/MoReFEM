// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Invariant1.hpp"

#include "Utilities/Containers/EnumClass.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InvariantNS::GeneralizedNS
{


    Invariant1::Invariant1(::MoReFEM::GeometryNS::dimension_type mesh_dimension) : parent(mesh_dimension)
    {
        assert(mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 3 }
               && "Generalized coordinates are used only for 3D-shells so far; "
                  "if you need them for lower dimensions please "
                  "[post an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues)");

        matrix_parent::InitLocalMatrixStorage({ {
            { 1UL, 1UL } // scalar_matrix
        } });

        // Second derivates are already filled with zeros
    }


    void Invariant1::Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
                            [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
                            [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
                            const InvariantNS::GeneralizedNS::Input& generalized_input,
                            [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        decltype(auto) scalar_matrix =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::scalar_matrix)>();

        assert(scalar_matrix.rows() == 1);
        assert(scalar_matrix.cols() == 1);

        decltype(auto) contravariant_metric_as_vector = generalized_input.GetContravariantMetricAsVector();

        scalar_matrix.noalias() = contravariant_metric_as_vector.transpose() * matricial_cauchy_green_tensor;

        SetValue(scalar_matrix(0, 0));
    }


    void Invariant1::UpdateFirstDerivates(
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
        [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
        const InvariantNS::GeneralizedNS::Input& generalized_input,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        decltype(auto) contravariant_metric_as_vector = generalized_input.GetContravariantMetricAsVector();

        assert(contravariant_metric_as_vector.rows() == first_deriv.size());

        for (auto i = Eigen::Index{}, size = first_deriv.size(); i < size; ++i)
        {
            first_deriv(i) = contravariant_metric_as_vector(i, 0);

            if (i > 2)
            {
                constexpr auto one_half{ 0.5 };
                first_deriv(i) *= one_half;
            }
        }
    }


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
