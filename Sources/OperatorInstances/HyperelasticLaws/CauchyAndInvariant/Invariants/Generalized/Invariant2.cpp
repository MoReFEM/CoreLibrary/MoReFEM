// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Invariant2.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InvariantNS::GeneralizedNS
{


    Invariant2::Invariant2(::MoReFEM::GeometryNS::dimension_type mesh_dimension) : parent(mesh_dimension)
    {
        assert(mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 3 }
               && "Generalized coordinates are used only for 3D-shells so far; "
                  "if you need them for lower dimensions please "
                  "[post an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues)");

        // Second derivates are already filled with zeros
    }


    void Invariant2::Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
                            [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
                            [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
                            const InvariantNS::GeneralizedNS::Input& generalized_input,
                            [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
        assert(generalized_input.IsI2Supported());

        decltype(auto) I2_matrix_helper = generalized_input.GetI2HelperMatrix();

        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        Eigen::Matrix<double, 1, 1> scalar_matrix;
        Eigen::Matrix<double, 6, 1> intermediate;

        intermediate.noalias() = I2_matrix_helper * matricial_cauchy_green_tensor;
        scalar_matrix.noalias() = 0.5 * matricial_cauchy_green_tensor.transpose() * intermediate;

        assert(scalar_matrix.rows() == 1);
        assert(scalar_matrix.cols() == 1);

        SetValue(scalar_matrix(0, 0));
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


    void Invariant2::UpdateFirstDerivates(
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
        [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
        const InvariantNS::GeneralizedNS::Input& generalized_input,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        assert(generalized_input.IsI2Supported());

        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        decltype(auto) I2_matrix_helper = generalized_input.GetI2HelperMatrix();

        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        {
            Eigen::Matrix<double, 6, 1> buf;

            buf.noalias() = I2_matrix_helper * matricial_cauchy_green_tensor;

            const auto size = first_deriv.size();
            const auto euclidean_dimension = Eigen::Index{ 3 };

            for (auto i = Eigen::Index{}; i < size - euclidean_dimension; ++i)
            {
                first_deriv(i) = buf(i, 0);
            }

            for (auto i = euclidean_dimension; i < size; ++i)
            {
                first_deriv(i) = buf(i, 0);
                first_deriv(i) *= 0.5;
            }
        }
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


    void Invariant2::UpdateSecondDerivates(
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
        [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
        const InvariantNS::GeneralizedNS::Input& generalized_input,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        assert(generalized_input.IsI2Supported());

        decltype(auto) second_deriv = parent::GetNonCstSecondDerivates();

        decltype(auto) I2_matrix_helper = generalized_input.GetI2HelperMatrix();

        const auto size = second_deriv.rows();
        assert(size == second_deriv.cols());

        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        second_deriv = I2_matrix_helper;

        for (auto i = Eigen::Index{ 3 }; i < size; ++i)
            for (auto j = Eigen::Index{}; j < size; ++j)
            {
                second_deriv(i, j) *= 0.5;
                second_deriv(j, i) *= 0.5;
            }

        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
