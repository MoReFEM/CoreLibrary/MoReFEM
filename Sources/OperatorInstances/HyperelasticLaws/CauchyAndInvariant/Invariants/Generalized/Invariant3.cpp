// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Invariant3.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::InvariantNS::GeneralizedNS
{


    Invariant3::Invariant3(::MoReFEM::GeometryNS::dimension_type mesh_dimension) : parent(mesh_dimension)
    {
        assert(mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 3 }
               && "Generalized coordinates are used only for 3D-shells so far; "
                  "if you need them for lower dimensions please "
                  "[post an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues)");
    }


    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    void
    Invariant3::Update([[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
                       [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
                       [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
                       const InvariantNS::GeneralizedNS::Input& generalized_input,
                       [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        using NumericNS::Square;
        assert(matricial_cauchy_green_tensor.rows() == 6 && matricial_cauchy_green_tensor.cols() == 1);

        const double Cxx = matricial_cauchy_green_tensor(0, 0);
        const double Cyy = matricial_cauchy_green_tensor(1, 0);
        const double Czz = matricial_cauchy_green_tensor(2, 0);
        const double Cxy = matricial_cauchy_green_tensor(3, 0);
        const double Cyz = matricial_cauchy_green_tensor(4, 0);
        const double Cxz = matricial_cauchy_green_tensor(5, 0);

        const double cauchy_green_determinant =
            Cxx * Cyy * Czz - Cxx * Square(Cyz) - Cyy * Square(Cxz) - Czz * Square(Cxy) + 2. * Cxy * Cyz * Cxz;

        SetValue(cauchy_green_determinant * generalized_input.GetDeterminant());
    }


    void Invariant3::UpdateFirstDerivates(
        const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
        [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
        const InvariantNS::GeneralizedNS::Input& generalized_input,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        using NumericNS::Square;

        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        assert(first_deriv.size() == 6);
        assert(matricial_cauchy_green_tensor.rows() == 6 && matricial_cauchy_green_tensor.cols() == 1);

        const double Cxx = matricial_cauchy_green_tensor(0, 0);
        const double Cyy = matricial_cauchy_green_tensor(1, 0);
        const double Czz = matricial_cauchy_green_tensor(2, 0);
        const double Cxy = matricial_cauchy_green_tensor(3, 0);
        const double Cyz = matricial_cauchy_green_tensor(4, 0);
        const double Cxz = matricial_cauchy_green_tensor(5, 0);

        first_deriv(0) = Cyy * Czz - Square(Cyz);
        first_deriv(1) = Cxx * Czz - Square(Cxz);
        first_deriv(2) = Cxx * Cyy - Square(Cxy);
        first_deriv(3) = Cyz * Cxz - Czz * Cxy;
        first_deriv(4) = Cxy * Cxz - Cxx * Cyz;
        first_deriv(5) = Cxy * Cyz - Cyy * Cxz;

        first_deriv *= generalized_input.GetDeterminant();
    }


    void Invariant3::UpdateSecondDerivates(
        const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        [[maybe_unused]] const ::MoReFEM::QuadraturePoint& quad_pt,
        [[maybe_unused]] const ::MoReFEM::GeometricElt& geom_elt,
        const InvariantNS::GeneralizedNS::Input& generalized_input,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis)
    {
        // [0    , Czz  , Cyy  , 0        , -Cyz     , 0        ],
        // [Czz  , 0    , Cxx  , 0        , 0        , -Cxz     ],
        // [Cyy  , Cxx  , 0    , -Cxy     , 0        , 0        ],
        // [0    , 0    , -Cxy , -0.5*Czz , 0.5*Cxz  , 0.5*Cyz  ],
        // [-Cyz , 0    , 0    , 0.5*Cxz  , -0.5*Cxx , 0.5*Cxy  ],
        // [0    , -Cxz , 0    , 0.5*Cyz  , 0.5*Cxy  , -0.5*Cyy ]

        decltype(auto) second_deriv = parent::GetNonCstSecondDerivates();

        assert(second_deriv.rows() == 6);
        assert(second_deriv.cols() == 6);
        assert(matricial_cauchy_green_tensor.rows() == 6 && matricial_cauchy_green_tensor.cols() == 1);

        const double Cxx = matricial_cauchy_green_tensor(0, 0);
        const double Cyy = matricial_cauchy_green_tensor(1, 0);
        const double Czz = matricial_cauchy_green_tensor(2, 0);
        const double Cxy = matricial_cauchy_green_tensor(3, 0);
        const double Cyz = matricial_cauchy_green_tensor(4, 0);
        const double Cxz = matricial_cauchy_green_tensor(5, 0);

        second_deriv.setZero();

        second_deriv(3, 3) = -0.5 * Czz;
        second_deriv(4, 4) = -0.5 * Cxx;
        second_deriv(5, 5) = -0.5 * Cyy;

        second_deriv(1, 0) = second_deriv(0, 1) = Czz;
        second_deriv(2, 0) = second_deriv(0, 2) = Cyy;
        second_deriv(1, 2) = second_deriv(2, 1) = Cxx;

        second_deriv(0, 4) = second_deriv(4, 0) = -Cyz;
        second_deriv(1, 5) = second_deriv(5, 1) = -Cxz;
        second_deriv(2, 3) = second_deriv(3, 2) = -Cxy;

        second_deriv(3, 4) = second_deriv(4, 3) = 0.5 * Cxz;
        second_deriv(3, 5) = second_deriv(5, 3) = 0.5 * Cyz;
        second_deriv(5, 4) = second_deriv(4, 5) = 0.5 * Cxy;

        second_deriv *= generalized_input.GetDeterminant();
    }
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

} // namespace MoReFEM::InvariantNS::GeneralizedNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
