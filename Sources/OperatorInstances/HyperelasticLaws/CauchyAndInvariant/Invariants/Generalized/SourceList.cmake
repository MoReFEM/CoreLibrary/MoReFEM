### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/GeneralizedInput.cpp
		${CMAKE_CURRENT_LIST_DIR}/Invariant1.cpp
		${CMAKE_CURRENT_LIST_DIR}/Invariant2.cpp
		${CMAKE_CURRENT_LIST_DIR}/Invariant3.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/GeneralizedInput.hpp
		${CMAKE_CURRENT_LIST_DIR}/GeneralizedInput.hxx
		${CMAKE_CURRENT_LIST_DIR}/Invariant1.hpp
		${CMAKE_CURRENT_LIST_DIR}/Invariant2.hpp
		${CMAKE_CURRENT_LIST_DIR}/Invariant3.hpp
		${CMAKE_CURRENT_LIST_DIR}/Invariant4.hpp
		${CMAKE_CURRENT_LIST_DIR}/Invariant6.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
