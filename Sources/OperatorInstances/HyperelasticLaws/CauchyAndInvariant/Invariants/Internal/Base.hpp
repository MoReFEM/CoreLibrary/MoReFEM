// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp"


namespace MoReFEM::Internal::InvariantNS
{


    /*!
     * \brief Base class for invariants.
     *
     * \tparam EnumT Enumeration which represents the invariant considered. For instance "I3".
     */

    template<::MoReFEM::InvariantNS::index EnumT>
    class Base
    {
      public:
        //! Returns the underlying invariant enum
        static constexpr auto underlying_enum = EnumT;

        //! Convenient alias.
        using first_derivates_type = ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>;

        //! Convenient alias.
        using second_derivates_type = ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] mesh_dimension Dimension of the mesh.
        explicit Base(::MoReFEM::GeometryNS::dimension_type mesh_dimension);

        //! Destructor.
        ~Base() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Base(const Base& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Base(Base&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Base& operator=(const Base& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Base& operator=(Base&& rhs) = delete;

        ///@}

        //! Set the value of the invariant.
        //! \param[in] value Value to set.
        void SetValue(double value) noexcept;

        //! Get the value of the invariant.
        double GetValue() const noexcept;

        //! Return mesh dimension
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;

        //! Returns the first derivates as a vector.
        const first_derivates_type& GetFirstDerivates() const noexcept;

        //! Returns the second derivates as a matrix.
        const second_derivates_type& GetSecondDerivates() const noexcept;

      protected:
        //! Non constant accessor to the first derivates vector.
        first_derivates_type& GetNonCstFirstDerivates() noexcept;

        //! Non constant accessor to the second derivates matrix.
        second_derivates_type& GetNonCstSecondDerivates() noexcept;

      private:
        //! Value of the invariant.
        double value_{};

        //! Mesh dimension.
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;

        //! Vector which stores last first derivates computed.
        first_derivates_type first_derivates_;

        //! Matrix which stores last second derivates computed.
        second_derivates_type second_derivates_;
    };


} // namespace MoReFEM::Internal::InvariantNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HPP_
// *** MoReFEM end header guards *** < //
