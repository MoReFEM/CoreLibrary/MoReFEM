// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::InvariantNS
{


    template<::MoReFEM::InvariantNS::index EnumT>
    Base<EnumT>::Base(::MoReFEM::GeometryNS::dimension_type mesh_dimension) : mesh_dimension_(mesh_dimension)
    {
        assert(mesh_dimension > ::MoReFEM::GeometryNS::dimension_type{ 0 });
        assert(mesh_dimension < ::MoReFEM::GeometryNS::dimension_type{ 4 });

        const auto size =
            static_cast<Eigen::Index>(mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 1 }
                                          ? 1
                                          : (mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 2 } ? 3 : 6));

        first_derivates_.resize(size);
        first_derivates_.setZero();

        second_derivates_.resize(size, size);
        second_derivates_.setZero();
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    inline double Base<EnumT>::GetValue() const noexcept
    {
        return value_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    inline void Base<EnumT>::SetValue(double value) noexcept
    {
        value_ = value;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    auto Base<EnumT>::GetMeshDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return mesh_dimension_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    auto Base<EnumT>::GetFirstDerivates() const noexcept -> const first_derivates_type&
    {
        return first_derivates_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    auto Base<EnumT>::GetNonCstFirstDerivates() noexcept -> first_derivates_type&
    {
        return const_cast<first_derivates_type&>(GetFirstDerivates());
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    auto Base<EnumT>::GetSecondDerivates() const noexcept -> const second_derivates_type&
    {
        return second_derivates_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    auto Base<EnumT>::GetNonCstSecondDerivates() noexcept -> second_derivates_type&
    {
        return const_cast<second_derivates_type&>(GetSecondDerivates());
    }


} // namespace MoReFEM::Internal::InvariantNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HXX_
// *** MoReFEM end header guards *** < //
