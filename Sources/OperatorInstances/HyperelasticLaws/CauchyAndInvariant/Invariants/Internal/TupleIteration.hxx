// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_TUPLEITERATION_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_TUPLEITERATION_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/TupleIteration.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <tuple>

#include "Utilities/Containers/Tuple/Concept.hpp"
#include "Utilities/Miscellaneous.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"


namespace MoReFEM::Internal::InvariantNS
{


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<::MoReFEM::InvariantNS::index EnumT>
    constexpr std::size_t TupleIteration<TupleT, Index>::FindIndex()
    {
        if constexpr (current_type::underlying_enum == EnumT)
            return Index;
        else
            return TupleIteration<TupleT, Index + 1>::template FindIndex<EnumT>();
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<::MoReFEM::InvariantNS::index EnumT>
    constexpr bool TupleIteration<TupleT, Index>::IsPresent()
    {
        if constexpr (current_type::underlying_enum == EnumT)
            return true;
        else
        {
            if constexpr (is_last)
                return false;
            else
                return next_element::template IsPresent<EnumT>();
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<class InvariantHolderT>
    void TupleIteration<TupleT, Index>::Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor,
                                               const ::MoReFEM::QuadraturePoint& quad_pt,
                                               const ::MoReFEM::GeometricElt& geom_elt,
                                               InvariantHolderT& invariant_holder)

    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-local-typedef.hpp"
        using parent_to_use = std::tuple_element_t<Index, TupleT>;
        PRAGMA_DIAGNOSTIC(pop)

        const auto content_type = invariant_holder.GetContent();

        switch (content_type)
        {
        case ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv:
            RUN_IF_EXISTING(
                invariant_holder.parent_to_use::UpdateSecondDerivates(cauchy_green_tensor, quad_pt, geom_elt))

            [[fallthrough]];
        case ::MoReFEM::InvariantNS::Content::invariants_and_first_deriv:
            RUN_IF_EXISTING(
                invariant_holder.parent_to_use::UpdateFirstDerivates(cauchy_green_tensor, quad_pt, geom_elt))

            [[fallthrough]];
        case ::MoReFEM::InvariantNS::Content::invariants:
            invariant_holder.parent_to_use::Update(cauchy_green_tensor, quad_pt, geom_elt);
            break;
        }

        if constexpr (!is_last)
            next_element::Update(cauchy_green_tensor, quad_pt, geom_elt, invariant_holder);
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<class InvariantHolderT>
    void
    TupleIteration<TupleT, Index>::Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
                                          const ::MoReFEM::QuadraturePoint& quad_pt,
                                          const ::MoReFEM::GeometricElt& geom_elt,
                                          const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
                                          const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis,
                                          InvariantHolderT& invariant_holder)

    {

        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-local-typedef.hpp"
        using parent_to_use = std::tuple_element_t<Index, TupleT>;
        PRAGMA_DIAGNOSTIC(pop)

        const auto content_type = invariant_holder.GetContent();

        switch (content_type)
        {
        case ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv:
            RUN_IF_EXISTING(invariant_holder.parent_to_use::UpdateSecondDerivates(
                matricial_cauchy_green_tensor, quad_pt, geom_elt, generalized_input, contravariant_basis))
            [[fallthrough]];
        case ::MoReFEM::InvariantNS::Content::invariants_and_first_deriv:
            RUN_IF_EXISTING(invariant_holder.parent_to_use::UpdateFirstDerivates(
                matricial_cauchy_green_tensor, quad_pt, geom_elt, generalized_input, contravariant_basis))
            [[fallthrough]];
        case ::MoReFEM::InvariantNS::Content::invariants:
            invariant_holder.parent_to_use::Update(
                matricial_cauchy_green_tensor, quad_pt, geom_elt, generalized_input, contravariant_basis);
            break;
        }

        if constexpr (!is_last)
            next_element::Update(matricial_cauchy_green_tensor,
                                 quad_pt,
                                 geom_elt,
                                 generalized_input,
                                 contravariant_basis,
                                 invariant_holder);
    }


} // namespace MoReFEM::Internal::InvariantNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_TUPLEITERATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
