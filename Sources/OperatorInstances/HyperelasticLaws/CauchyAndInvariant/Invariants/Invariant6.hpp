// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INVARIANT6_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INVARIANT6_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant6.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Invariant6.hpp"

namespace MoReFEM::InvariantNS
{


    /*!
     * \brief Traits struct to redirect toward the proper sixth invariant to use.
     */
    template<coords CoordsEnumT, FiberNS::AtNodeOrAtQuadPt FiberPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct Invariant6;


    /*!
     * \brief Specialization to redirect toward cartesian invariant.
     */
    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct Invariant6<coords::cartesian, FiberPolicyT, TimeManagerT>
    {
        //! Traits giving the class to use.
        using type = CartesianNS::Invariant6<FiberPolicyT, TimeManagerT>;
    };


    /*!
     * \brief Specialization to redirect toward generalized invariant.
     */
    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct Invariant6<coords::generalized, FiberPolicyT, TimeManagerT>
    {
        //! Traits giving the class to use.
        using type = GeneralizedNS::Invariant6<FiberPolicyT, TimeManagerT>;
    };


} // namespace MoReFEM::InvariantNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INVARIANT6_DOT_HPP_
// *** MoReFEM end header guards *** < //
