// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CIARLETGEYMONATDEVIATORIC_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CIARLETGEYMONATDEVIATORIC_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string> // IWYU pragma: keep

#include "Core/Parameter/FiberEnum.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Advanced/InvariantHolder.hpp" // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant1.hpp"    // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant2.hpp"    // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant3.hpp"    // IWYU pragma: export

#include "ParameterInstances/Compound/Solid/Solid.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    /*!
     * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT = ::MoReFEM::InvariantNS::coords::cartesian
    >
    class CiarletGeymonatDeviatoric
    : public Advanced::InvariantHolder
    <
        typename InvariantNS::Invariant1<CoordsEnumT>::type,
        typename InvariantNS::Invariant2<CoordsEnumT>::type,
        typename InvariantNS::Invariant3<CoordsEnumT>::type
    >
    // clang-format on
    {
      public:
        //! Return the name of the hyperelastic law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! \copydoc doxygen_hide_alias_self
        using self = CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>;

        //! \copydoc doxygen_hide_alias_const_unique_ptr
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to parent.
        // clang-format off
        using invariant_holder_parent =
        Advanced::InvariantHolder
        <
            typename InvariantNS::Invariant1<CoordsEnumT>::type,
            typename InvariantNS::Invariant2<CoordsEnumT>::type,
            typename InvariantNS::Invariant3<CoordsEnumT>::type
        >;
        // clang-format on


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] solid Object which provides the required material parameters for the solid.
         */
        explicit CiarletGeymonatDeviatoric(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                           const Solid<TimeManagerT>& solid);

        //! Destructor.
        ~CiarletGeymonatDeviatoric() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        CiarletGeymonatDeviatoric(const CiarletGeymonatDeviatoric& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        CiarletGeymonatDeviatoric(CiarletGeymonatDeviatoric&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        CiarletGeymonatDeviatoric& operator=(const CiarletGeymonatDeviatoric& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        CiarletGeymonatDeviatoric& operator=(CiarletGeymonatDeviatoric&& rhs) = delete;

        ///@}

      public:
        //! Function W.
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double W(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of first invariant (dWdI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI1(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of second invariant (dWdI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI2(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of first invariant (d2WdI1dI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI1(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of second invariant (d2WdI2dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI2(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of third invariant (d2WdI3dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of first and second invariant (d2WdI1dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI1dI2(const QuadraturePoint& quadrature_point,
                                          const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of first and third invariant (d2WdI1dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI1dI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of second and third invariant (d2WdI2dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI2dI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;


      private:
        //! Kappa1.
        const scalar_parameter_type& GetKappa1() const noexcept;

        //! Kappa2.
        const scalar_parameter_type& GetKappa2() const noexcept;


      private:
        //! Kappa1.
        const scalar_parameter_type& kappa1_;

        //! Kappa2.
        const scalar_parameter_type& kappa2_;
    };


} // namespace MoReFEM::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CIARLETGEYMONATDEVIATORIC_DOT_HPP_
// *** MoReFEM end header guards *** < //
