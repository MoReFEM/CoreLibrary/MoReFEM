// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_EXPONENTIALJ1J4J6_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_EXPONENTIALJ1J4J6_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <bitset>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <exception>
#include <vector>

#include "Utilities/InputData/InputData.hpp"

#include "Parameters/Parameter.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Advanced/InvariantHolder.hpp" // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant1.hpp"    // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant2.hpp"    // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant3.hpp"    // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant4.hpp"    // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant6.hpp"    // IWYU pragma: export

#include "ParameterInstances/Compound/Solid/Solid.hpp" // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::HyperelasticLawNS
{


    /*!
     * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT = ::MoReFEM::InvariantNS::coords::cartesian,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT = FiberNS::AtNodeOrAtQuadPt::at_node
    >
    class ExponentialJ1J4J6
    : public Advanced::InvariantHolder
    <
        typename InvariantNS::Invariant1<CoordsEnumT>::type,
        typename InvariantNS::Invariant2<CoordsEnumT>::type,
        typename InvariantNS::Invariant3<CoordsEnumT>::type,
        typename InvariantNS::Invariant4<CoordsEnumT, FiberPolicyT, TimeManagerT>::type,
        typename InvariantNS::Invariant6<CoordsEnumT, FiberPolicyT, TimeManagerT>::type
    >
    // clang-format on
    {
      public:
        //! Return the name of the hyperelastic law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! \copydoc doxygen_hide_alias_self
        using self = ExponentialJ1J4J6<TimeManagerT, CoordsEnumT, FiberPolicyT>;

        //! \copydoc doxygen_hide_alias_const_unique_ptr
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to parent.
        // clang-format off
        using invariant_holder_parent =
        Advanced::InvariantHolder
        <
            typename InvariantNS::Invariant1<CoordsEnumT>::type,
            typename InvariantNS::Invariant2<CoordsEnumT>::type,
            typename InvariantNS::Invariant3<CoordsEnumT>::type,
            typename InvariantNS::Invariant4<CoordsEnumT, FiberPolicyT, TimeManagerT>::type,
            typename InvariantNS::Invariant6<CoordsEnumT, FiberPolicyT, TimeManagerT>::type
        >;
        // clang-format on

        //! \copydoc doxygen_hide_fiber_type_alias
        using fiber_type = FiberList<FiberPolicyT, ParameterNS::Type::vector, TimeManagerT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] solid Object which provides the required material parameters for the solid.
         * \param[in] fibersI4 Fibers to define I4 in the law.
         * \param[in] fibersI6 Fibers to define I6 in the law.
         */
        explicit ExponentialJ1J4J6(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                   const Solid<TimeManagerT>& solid,
                                   const fiber_type& fibersI4,
                                   const fiber_type& fibersI6);

        //! Destructor.
        ~ExponentialJ1J4J6() override = default;

        //! Copy constructor.
        ExponentialJ1J4J6(const ExponentialJ1J4J6&) = delete;

        //! Move constructor.
        ExponentialJ1J4J6(ExponentialJ1J4J6&&) = delete;

        //! Copy affectation.
        ExponentialJ1J4J6& operator=(const ExponentialJ1J4J6&) = delete;

        //! Move affectation.
        ExponentialJ1J4J6& operator=(ExponentialJ1J4J6&&) = delete;

        ///@}


      public:
        //! Function W.
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double W(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of first invariant (dWdI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI1(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of second invariant (dWdI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI2(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI4)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI4(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI6)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double dWdI6(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of first invariant (d2WdI1dI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI1(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of second invariant (d2WdI2dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI2(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of third invariant (d2WdI3dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of third invariant (d2WdI4dI4)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI4(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of third invariant (d2WdI6dI6)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI6dI6(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of first and second invariant (d2WdI1dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI1dI2(const QuadraturePoint& quadrature_point,
                                          const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of first and third invariant (d2WdI1dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI1dI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of second and third invariant (d2WdI1dI4)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI1dI4(const QuadraturePoint& quadrature_point,
                                          const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of second and third invariant (d2WdI1dI6)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI1dI6(const QuadraturePoint& quadrature_point,
                                          const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of second and third invariant (d2WdI2dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI2dI3(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of second and third invariant (d2WdI2dI4)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI2dI4(const QuadraturePoint& quadrature_point,
                                          const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of second and third invariant (d2WdI2dI6)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double d2WdI2dI6(const QuadraturePoint& quadrature_point,
                                          const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of second and third invariant (d2WdI3dI4)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI3dI4(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of second and third invariant (d2WdI3dI6)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double d2WdI3dI6(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;


      private:
        //! Mu1.
        const scalar_parameter_type& GetMu1() const noexcept;

        //! Mu2.
        const scalar_parameter_type& GetMu2() const noexcept;

        //! C0.
        const scalar_parameter_type& GetC0() const noexcept;

        //! C1.
        const scalar_parameter_type& GetC1() const noexcept;

        //! C2.
        const scalar_parameter_type& GetC2() const noexcept;

        //! C3.
        const scalar_parameter_type& GetC3() const noexcept;

        //! C4.
        const scalar_parameter_type& GetC4() const noexcept;

        //! C5.
        const scalar_parameter_type& GetC5() const noexcept;

        //! Hyperelastic bulk.
        const scalar_parameter_type& GetBulk() const noexcept;

      private:
        //! Mu1.
        const scalar_parameter_type& mu_1_;

        //! Mu2.
        const scalar_parameter_type& mu_2_;

        //! C0.
        const scalar_parameter_type& c_0_;

        //! C1.
        const scalar_parameter_type& c_1_;

        //! C2.
        const scalar_parameter_type& c_2_;

        //! C3.
        const scalar_parameter_type& c_3_;

        //! C4.
        const scalar_parameter_type& c_4_;

        //! C5.
        const scalar_parameter_type& c_5_;

        //! Bulk.
        const scalar_parameter_type& bulk_;
    };


} // namespace MoReFEM::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4J6.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_EXPONENTIALJ1J4J6_DOT_HPP_
// *** MoReFEM end header guards *** < //
