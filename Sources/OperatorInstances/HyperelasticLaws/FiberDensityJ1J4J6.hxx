// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_FIBERDENSITYJ1J4J6_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_FIBERDENSITYJ1J4J6_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/FiberDensityJ1J4J6.hpp"
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    const std::string& FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::ClassName()
    {
        static const std::string ret("FiberDensityJ1J4J6");
        return ret;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::FiberDensityJ1J4J6(
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        const Solid<TimeManagerT>& solid,
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI4,
        const FiberList<FiberPolicyT, ParameterNS::Type::scalar>* fiber_density_I4,
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI6,
        const FiberList<FiberPolicyT, ParameterNS::Type::scalar>* fiber_density_I6)
    : invariant_holder_parent(mesh_dimension,
                              ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv,
                              { mesh_dimension },
                              { mesh_dimension },
                              { mesh_dimension },
                              { mesh_dimension, fibersI4 },
                              { mesh_dimension, fibersI6 }),
      mu_1_(solid.GetMu1()), mu_2_(solid.GetMu2()), c_0_(solid.GetC0()), c_1_(solid.GetC1()), c_2_(solid.GetC2()),
      c_3_(solid.GetC3()), c_4_(solid.GetC4()), c_5_(solid.GetC5()), bulk_(solid.GetHyperelasticBulk()),
      fiber_density_I4_(fiber_density_I4), fiber_density_I6_(fiber_density_I6)
    { }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::W(const QuadraturePoint& quad_pt,
                                                            const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I2 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I2>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I4 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I4>();
        const double I6 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I6>();
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3.);
        const double sqrt_I3 = std::sqrt(I3);
        const double expI1 =
            std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 =
            std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
        const double expI6 =
            std::exp(GetC5().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);
        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return GetMu1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
               + GetMu2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.)
               + GetC0().GetValue(quad_pt, geom_elt) * expI1
               + fiber_density_I4 * GetC2().GetValue(quad_pt, geom_elt) * expI4
               + fiber_density_I6 * GetC4().GetValue(quad_pt, geom_elt) * expI6
               + GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::dWdI1(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3.);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3.);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return 2. * C0 * C1 * NumericNS::Square(I3_pow_minus_one_third) * (I1 - 3. * I3_pow_one_third) * expI1
               + mu1 * I3_pow_minus_one_third;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::dWdI2(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);

        return mu2 * NumericNS::Pow(I3, -2. / 3.);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::dWdI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I2 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I2>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I4 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I4>();
        const double I6 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I6>();

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double two_third = 2. / 3.;

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3.);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3.);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);
        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);
        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);
        const double kappa = GetBulk().GetValue(quad_pt, geom_elt);

        const double expI1 =
            std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 =
            std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
        const double expI6 =
            std::exp(GetC5().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);
        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return -two_third * C0 * C1 * NumericNS::Pow(I3, 5. * minus_one_third) * I1 * (I1 - 3. * I3_pow_one_third)
                   * expI1
               + fiber_density_I4 * two_third * C2 * C3 * NumericNS::Pow(I3, 5. * minus_one_third) * I4
                     * (I3_pow_one_third - I4) * expI4
               + fiber_density_I6 * two_third * C4 * C5 * NumericNS::Pow(I3, 5. * minus_one_third) * I6
                     * (I3_pow_one_third - I6) * expI6
               + mu1 * minus_one_third * I1 * NumericNS::Pow(I3, 4. * minus_one_third)
               + 2. * mu2 * minus_one_third * I2 * NumericNS::Pow(I3, 5. * minus_one_third)
               + kappa * (0.5 * NumericNS::Pow(I3, -0.5) - 0.5 / I3);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::dWdI4(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I4 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I4>();

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3.);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3.);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);

        return -2. * fiber_density_I4 * C2 * C3 * NumericNS::Square(I3_pow_minus_one_third) * (I3_pow_one_third - I4)
               * expI4;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::dWdI6(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I6 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I6>();

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3.);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3.);

        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return -2. * fiber_density_I6 * C4 * C5 * NumericNS::Square(I3_pow_minus_one_third) * (I3_pow_one_third - I6)
               * expI6;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI1(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3.);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3.);

        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return 2. * C0 * C1 * NumericNS::Pow(I3, -4. / 3.)
               * (2. * C1 * NumericNS::Square(I1 - 3. * I3_pow_one_third) + NumericNS::Square(I3_pow_one_third))
               * expI1;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I2 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I2>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I4 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I4>();
        const double I6 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I6>();

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double two_ninth = 2. / 9.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3.);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);
        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);
        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);
        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return two_ninth * NumericNS::Pow(I3, -10. / 3.)
                   * (C0 * C1 * expI1 * I1
                          * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third)
                             + 5. * I1 * NumericNS::Square(I3_pow_one_third) - 12. * I3)
                      + fiber_density_I4 * C2 * C3 * expI4 * I4
                            * (-4. * I3 + (5. + 2. * C3) * I4 * NumericNS::Square(I3_pow_one_third)
                               - 4. * C3 * I3_pow_one_third * NumericNS::Square(I4) + 2. * C3 * NumericNS::Cube(I4))
                      + fiber_density_I6 * C4 * C5 * expI6 * I6
                            * (-4. * I3 + (5. + 2. * C5) * I6 * NumericNS::Square(I3_pow_one_third)
                               - 4. * C5 * I3_pow_one_third * NumericNS::Square(I6) + 2. * C5 * NumericNS::Cube(I6)))
               + 4. / 9. * mu1 * I1 * NumericNS::Pow(I3, -7. / 3.) + 10. / 9. * mu2 * I2 * NumericNS::Pow(I3, -8. / 3.)
               + GetBulk().GetValue(quad_pt, geom_elt)
                     * (0.5 * NumericNS::Pow(I3, -2.) - 0.25 * NumericNS::Pow(I3, -1.5));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI4(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I4 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I4>();

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);

        return 2. * fiber_density_I4 * C2 * C3 * NumericNS::Pow(I3, 4 * minus_one_third) * expI4
               * ((1. + 2. * C3) * NumericNS::Pow(I3, 2. / 3.) - 4. * C3 * NumericNS::Pow(I3, 1. / 3.) * I4
                  + 2. * C3 * NumericNS::Square(I4));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI6dI6(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I6 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I6>();

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third);

        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return 2. * fiber_density_I6 * C4 * C5 * NumericNS::Pow(I3, 4 * minus_one_third) * expI6
               * ((1. + 2. * C5) * NumericNS::Pow(I3, 2. / 3.) - 4. * C5 * NumericNS::Pow(I3, 1. / 3.) * I6
                  + 2. * C5 * NumericNS::Square(I6));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI1dI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third);

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3.);

        constexpr const double minus_two_third = -2. / 3.;

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return minus_two_third * C0 * C1 * expI1 * NumericNS::Pow(I3, -7. / 3.)
                   * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third)
                      + 2. * I1 * NumericNS::Square(I3_pow_one_third) - 3. * I3)
               - 1. / 3. * mu1 * NumericNS::Pow(I3, 2. * minus_two_third);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI2dI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        constexpr const double minus_one_third = -1. / 3.;
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);

        return mu2 * 2. * minus_one_third * NumericNS::Pow(I3, 5. * minus_one_third);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI3dI4(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I4 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I4>();

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double minus_two_third = 2. * minus_one_third;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);

        return minus_two_third * NumericNS::Pow(I3, -7. / 3.) * fiber_density_I4 * C2 * C3 * expI4
               * (-I3 + 2. * (1. + C3) * NumericNS::Pow(I3, 2. / 3.) * I4
                  - 4. * C3 * NumericNS::Pow(I3, 1. / 3.) * I4 * I4 + 2. * C3 * NumericNS::Cube(I4));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI3dI6(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I6 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I6>();

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double minus_two_third = 2. * minus_one_third;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third);

        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);


        return minus_two_third * NumericNS::Pow(I3, -7. / 3.) * fiber_density_I6 * C4 * C5 * expI6
               * (-I3 + 2. * (1. + C5) * NumericNS::Pow(I3, 2. / 3.) * I6
                  - 4. * C5 * NumericNS::Pow(I3, 1. / 3.) * I6 * I6 + 2. * C5 * NumericNS::Cube(I6));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline constexpr double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI2(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline constexpr double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI1dI2(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline constexpr double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI1dI4(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline constexpr double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI2dI4(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline constexpr double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI1dI6(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline constexpr double FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::d2WdI2dI6(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetMu1() const noexcept -> const scalar_parameter_type&
    {
        return mu_1_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetMu2() const noexcept -> const scalar_parameter_type&
    {
        return mu_2_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetC0() const noexcept -> const scalar_parameter_type&
    {
        return c_0_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetC1() const noexcept -> const scalar_parameter_type&
    {
        return c_1_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetC2() const noexcept -> const scalar_parameter_type&
    {
        return c_2_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetC3() const noexcept -> const scalar_parameter_type&
    {
        return c_3_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetC4() const noexcept -> const scalar_parameter_type&
    {
        return c_4_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetC5() const noexcept -> const scalar_parameter_type&
    {
        return c_5_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline auto FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetBulk() const noexcept -> const scalar_parameter_type&
    {
        return bulk_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline const FiberList<FiberPolicyT, ParameterNS::Type::scalar>*
    FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetFiberDensityI4() const noexcept
    {
        return fiber_density_I4_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT
    >
    // clang-format on
    inline const FiberList<FiberPolicyT, ParameterNS::Type::scalar>*
    FiberDensityJ1J4J6<CoordsEnumT, FiberPolicyT>::GetFiberDensityI6() const noexcept
    {
        return fiber_density_I6_;
    }


} // namespace MoReFEM::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_FIBERDENSITYJ1J4J6_DOT_HXX_
// *** MoReFEM end header guards *** < //
