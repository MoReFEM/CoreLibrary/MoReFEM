// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_LOGI3PENALIZATION_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_LOGI3PENALIZATION_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::HyperelasticLawNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    const std::string& LogI3Penalization<TimeManagerT, CoordsEnumT>::ClassName()
    {
        static const std::string ret("LogI3Penalization");
        return ret;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    LogI3Penalization<TimeManagerT, CoordsEnumT>::LogI3Penalization(
        ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        const Solid<TimeManagerT>& solid)
    : invariant_holder_parent(mesh_dimension,
                              ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv,
                              { mesh_dimension }),
      bulk_(solid.GetHyperelasticBulk())
    { }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double LogI3Penalization<TimeManagerT, CoordsEnumT>::W(const QuadraturePoint& quad_pt,
                                                           const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double sqrt_I3 = std::sqrt(I3);

        return GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double LogI3Penalization<TimeManagerT, CoordsEnumT>::dWdI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();

        return GetBulk().GetValue(quad_pt, geom_elt) * (0.5 * NumericNS::Pow(I3, -0.5) - 0.5 / I3);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double LogI3Penalization<TimeManagerT, CoordsEnumT>::d2WdI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();

        return GetBulk().GetValue(quad_pt, geom_elt)
               * (-0.25 * NumericNS::Pow(I3, -1.5) + 0.5 * NumericNS::Pow(I3, -2.));
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline auto LogI3Penalization<TimeManagerT, CoordsEnumT>::GetBulk() const noexcept -> const scalar_parameter_type&
    {
        return bulk_;
    }

} // namespace MoReFEM::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_LOGI3PENALIZATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
