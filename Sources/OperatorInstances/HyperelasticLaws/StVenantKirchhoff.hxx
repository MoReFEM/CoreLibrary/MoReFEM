// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_STVENANTKIRCHHOFF_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_STVENANTKIRCHHOFF_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"
// *** MoReFEM header guards *** < //


#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    const std::string& StVenantKirchhoff<TimeManagerT, CoordsEnumT>::ClassName()
    {
        static const std::string ret("StVenant-Kirchhoff");
        return ret;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    StVenantKirchhoff<TimeManagerT, CoordsEnumT>::StVenantKirchhoff(
        ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        const Solid<TimeManagerT>& solid)
    : invariant_holder_parent(mesh_dimension,
                              ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv,
                              { mesh_dimension },
                              { mesh_dimension },
                              { mesh_dimension }),
      lame_lambda_(solid.GetLameLambda()), lame_mu_(solid.GetLameMu()), bulk_(solid.GetHyperelasticBulk())
    { }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::W(const QuadraturePoint& quad_pt,
                                                           const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I2 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I2>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();

        const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);
        const double bulk = GetBulk().GetValue(quad_pt, geom_elt);

        return (0.125 * lame_lambda_value + 0.25 * lame_mu_value) * NumericNS::Square(I1)
               - (0.75 * lame_lambda_value + 0.5 * lame_mu_value) * I1 - 0.5 * lame_mu_value * I2
               + 1.125 * lame_lambda_value + 0.75 * lame_mu_value + 0.5 * bulk * NumericNS::Square(I3 - 1.);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::dWdI1(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();

        const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

        return (0.25 * lame_lambda_value + 0.5 * lame_mu_value) * I1 - (0.75 * lame_lambda_value + 0.5 * lame_mu_value);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::dWdI2(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {

        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

        return -0.5 * lame_mu_value;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::dWdI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double bulk = GetBulk().GetValue(quad_pt, geom_elt);
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();

        return bulk * (I3 - 1.);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::d2WdI1(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {

        const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

        return 0.25 * lame_lambda_value + 0.5 * lame_mu_value;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline constexpr double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::d2WdI2(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::d2WdI3(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const noexcept
    {
        return GetBulk().GetValue(quad_pt, geom_elt);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline constexpr double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::d2WdI1dI3(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline constexpr double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::d2WdI2dI3(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline constexpr double StVenantKirchhoff<TimeManagerT, CoordsEnumT>::d2WdI1dI2(

        [[maybe_unused]] const QuadraturePoint& quad_pt,
        [[maybe_unused]] const GeometricElt& geom_elt) noexcept
    {
        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline auto StVenantKirchhoff<TimeManagerT, CoordsEnumT>::GetLameLambda() const noexcept
        -> const scalar_parameter_type&
    {
        return lame_lambda_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline auto StVenantKirchhoff<TimeManagerT, CoordsEnumT>::GetLameMu() const noexcept -> const scalar_parameter_type&
    {
        return lame_mu_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline auto StVenantKirchhoff<TimeManagerT, CoordsEnumT>::GetBulk() const noexcept -> const scalar_parameter_type&
    {
        return bulk_;
    }

} // namespace MoReFEM::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_STVENANTKIRCHHOFF_DOT_HXX_
// *** MoReFEM end header guards *** < //
