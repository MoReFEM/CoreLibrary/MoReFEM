// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iosfwd>
#include <iterator>
#include <memory>
#include <numeric>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

#include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hpp" // IWYU pragma: associated

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/MoReFEMData/Internal/Parallelism.hpp" // IWYU pragma: keep

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"
#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::NonConformInterpolatorNS
{


    namespace // anonymous
    {


        NodeBearer::vector_shared_ptr ComputeNodeBearerListInNumberingSubset(const GodOfDof& god_of_dof,
                                                                             const NumberingSubset& numbering_subset);


        using node_bearer_per_coords_index_list_type = Internal::NodeBearerNS::node_bearer_per_coords_index_list_type;


        void AllocateAndBuildInterpolatorMatrix(
            const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
            const Wrappers::Petsc::MatrixPattern& matrix_pattern,
            GlobalMatrix& interpolation_matrix);


        NodeBearer::vector_const_shared_ptr
        IdentifyTargetNodeBearerToProcess(const Dof::vector_shared_ptr& target_processor_wise_dof_list);


        /*!
         * \brief Compute for each row the position of the non zero element.
         *
         * We know there is exactly one per row due to the nature of the operator.
         *
         * \return The exterior vector iterates over the rows of the interpolation matrix. The interior one gives away the program-wise position of non
         * zero elements.
         */
        std::vector<PetscInt>
        ComputeNonZeroPositionPerRow(const NodeBearer::vector_const_shared_ptr& target_node_bearer_list,
                                     std::size_t Nprocessor_wise_target_dof,
                                     const NumberingSubset& source_numbering_subset,
                                     const NumberingSubset& target_numbering_subset,
                                     const MeshNS::InterpolationNS::CoordsMatching& coords_matching,
                                     const node_bearer_per_coords_index_list_type& source_node_bearer_list_per_coords);


        /*!
         * \brief "Convert" the result of \a ComputeNonZeroPositionPerRow() into the format expected by \a MatrixPattern.
         *
         *
         * \return The exterior vector iterates over the rows of the interpolation matrix. The interior one gives away the program-wise position of non
         * zero elements.
         */
        std::vector<std::vector<PetscInt>>
        ComputeNonZeroPatternForMatrixPattern(const std::vector<PetscInt>& interpolation_pattern);


    } // namespace


    void FromCoordsMatching::PrintIfRelevantPrepartitionedData() const
    {
        if (file_for_prepartitioned_data_.has_value())
        {
            auto file = file_for_prepartitioned_data_.value();
            std::ofstream out{ file.NewContent() };

            Utilities::PrintContainer<>::Do(GetNonZeroPositionPerRow(),
                                            out,
                                            PrintNS::Delimiter::separator(","),
                                            PrintNS::Delimiter::opener("non_zero_position_per_row = {"),
                                            PrintNS::Delimiter::closer("}\n"));
        }
    }


    void FromCoordsMatching::SetFileForPrepartitionedData(const Internal::Parallelism& parallelism)
    {
        switch (parallelism.GetParallelismStrategy())
        {
        case Advanced::parallelism_strategy::none:
        case Advanced::parallelism_strategy::parallel_no_write:
        {
            assert(!file_for_prepartitioned_data_.has_value());
            break;
        }
        case Advanced::parallelism_strategy::precompute:
        case Advanced::parallelism_strategy::parallel:
        case Advanced::parallelism_strategy::run_from_preprocessed:
        {
            decltype(auto) parallelism_dir = parallelism.GetDirectory();

            const FilesystemNS::Directory interpolator_directory(parallelism_dir, "FromCoordsMatchingInterpolator");

            interpolator_directory.ActOnFilesystem();

            file_for_prepartitioned_data_ = interpolator_directory.AddFile("non_zero_position_per_row.lua");
        }
        }
    }


    void FromCoordsMatching::StandardConstruct(
        const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
        store_matrix_pattern do_store_matrix_pattern)
    {

        decltype(auto) source_felt_space = coords_matching_interpolator.GetSourceFEltSpace();
        decltype(auto) target_felt_space = coords_matching_interpolator.GetTargetFEltSpace();
        decltype(auto) source_numbering_subset = coords_matching_interpolator.GetSourceNumberingSubset();
        decltype(auto) target_numbering_subset = coords_matching_interpolator.GetTargetNumberingSubset();

        interpolation_matrix_ =
            std::make_unique<GlobalMatrix>(target_numbering_subset, source_numbering_subset, "interpolation matrix");

        const auto Nprocessor_wise_target_dof = target_felt_space.NprocessorWiseDof(target_numbering_subset);

#ifndef NDEBUG
        {
            const auto Nprogram_wise_source_dof = source_felt_space.NprogramWiseDof(source_numbering_subset);
            const auto Nprogram_wise_target_dof = target_felt_space.NprogramWiseDof(target_numbering_subset);
            assert(Nprogram_wise_source_dof == Nprogram_wise_target_dof);
        }
#endif // NDEBUG

        const auto target_processor_wise_dof_list = target_felt_space.GetProcessorWiseDofList(target_numbering_subset);

        decltype(auto) source_god_of_dof_ptr = source_felt_space.GetGodOfDofFromWeakPtr();
        assert(!(!source_god_of_dof_ptr));
        const auto& source_god_of_dof = *source_god_of_dof_ptr;
        decltype(auto) target_god_of_dof_ptr = target_felt_space.GetGodOfDofFromWeakPtr();
        assert(!(!target_god_of_dof_ptr));
        const auto& target_god_of_dof = *target_god_of_dof_ptr;

        const NodeBearer::vector_shared_ptr source_node_bearer_list =
            ComputeNodeBearerListInNumberingSubset(source_god_of_dof, source_numbering_subset);

        const auto source_node_bearer_list_per_coords =
            Internal::NodeBearerNS::GenerateNodeBearerPerCoordsList(source_node_bearer_list);

        const auto target_node_bearer_list = IdentifyTargetNodeBearerToProcess(target_processor_wise_dof_list);

        decltype(auto) coords_matching_manager = Internal::MeshNS::CoordsMatchingManager::GetInstance();

        decltype(auto) coords_matching =
            coords_matching_manager.GetCoordsMatching(source_god_of_dof.GetUniqueId(), target_god_of_dof.GetUniqueId());

        non_zero_position_per_row_ = ComputeNonZeroPositionPerRow(target_node_bearer_list,
                                                                  Nprocessor_wise_target_dof,
                                                                  source_numbering_subset,
                                                                  target_numbering_subset,
                                                                  coords_matching,
                                                                  source_node_bearer_list_per_coords);

        matrix_pattern_ = std::make_unique<Wrappers::Petsc::MatrixPattern>(
            ComputeNonZeroPatternForMatrixPattern(non_zero_position_per_row_));

        decltype(auto) matrix_pattern = GetMatrixPattern();

        decltype(auto) interpolation_matrix = GetNonCstInterpolationMatrix();

        AllocateAndBuildInterpolatorMatrix(coords_matching_interpolator, matrix_pattern, interpolation_matrix);

        interpolation_matrix.Assembly();

        if (do_store_matrix_pattern == store_matrix_pattern::no)
            matrix_pattern_ = nullptr;

        PrintIfRelevantPrepartitionedData();
    }


    void FromCoordsMatching::ConstructFromPrepartitionedData(const Wrappers::Mpi& mpi,
                                                             const std::size_t coords_matching_interpolator_index)
    {
        decltype(auto) coords_matching_interpolator_manager =
            Internal::FEltSpaceNS::CoordsMatchingInterpolatorManager::GetInstance();

        decltype(auto) coords_matching_interpolator =
            coords_matching_interpolator_manager.GetCoordsMatchingInterpolator(coords_matching_interpolator_index);

        ::MoReFEM::Wrappers::Lua::OptionFile lua_file(GetFileFromPrepartitionedData());

#ifndef NDEBUG
        do_store_matrix_pattern_ = store_matrix_pattern::yes;
#endif // NDEBUG

        decltype(auto) source_felt_space = coords_matching_interpolator.GetSourceFEltSpace();
        decltype(auto) target_felt_space = coords_matching_interpolator.GetTargetFEltSpace();

        lua_file.Read("non_zero_position_per_row", "", non_zero_position_per_row_);

        decltype(auto) row_numbering_subset = coords_matching_interpolator.GetTargetNumberingSubset();

        decltype(auto) col_numbering_subset = coords_matching_interpolator.GetSourceNumberingSubset();

        auto Nrow_processor_wise = row_processor_wise_index_type{ static_cast<PetscInt>(
            target_felt_space.NprocessorWiseDof(row_numbering_subset)) };
        auto Nrow_program_wise = row_program_wise_index_type{ static_cast<PetscInt>(
            target_felt_space.NprogramWiseDof(row_numbering_subset)) };
        auto Ncol_processor_wise = col_processor_wise_index_type{ static_cast<PetscInt>(
            source_felt_space.NprocessorWiseDof(col_numbering_subset)) };
        auto Ncol_program_wise = col_program_wise_index_type{ static_cast<PetscInt>(
            source_felt_space.NprogramWiseDof(col_numbering_subset)) };

        std::vector<PetscInt> iCSR(static_cast<std::size_t>(Nrow_processor_wise.Get()) + 1UL);
        std::iota(iCSR.begin(), iCSR.end(), 0);

        auto jCSR = non_zero_position_per_row_;
        const std::vector<double> interpolation_values(jCSR.size(), 1.);

        matrix_pattern_ = std::make_unique<Wrappers::Petsc::MatrixPattern>(std::move(iCSR), std::move(jCSR));

        interpolation_matrix_ =
            std::make_unique<GlobalMatrix>(row_numbering_subset, col_numbering_subset, "interpolation matrix");

        interpolation_matrix_->InitParallelMatrix(
            Nrow_processor_wise, Ncol_processor_wise, Nrow_program_wise, Ncol_program_wise, *matrix_pattern_, mpi);

        auto& interpolation_matrix = *interpolation_matrix_;
        interpolation_matrix.SetParallelNonZeroPatternWithValues(*matrix_pattern_, interpolation_values);
        interpolation_matrix.Assembly();
    }


    inline const FilesystemNS::File& FromCoordsMatching::GetFileFromPrepartitionedData() const
    {
        if (!file_for_prepartitioned_data_.has_value())
        {
            assert(false && "This method shouldn't be called for an invalid parallelism strategy.");
            exit(EXIT_FAILURE);
        }

        return file_for_prepartitioned_data_.value();
    }


    namespace // anonymous
    {


        NodeBearer::vector_shared_ptr ComputeNodeBearerListInNumberingSubset(const GodOfDof& god_of_dof,
                                                                             const NumberingSubset& numbering_subset)
        {
            NodeBearer::vector_shared_ptr ret;

            decltype(auto) proc_node_bearer_list = god_of_dof.GetProcessorWiseNodeBearerList();
            decltype(auto) ghost_node_bearer_list = god_of_dof.GetGhostNodeBearerList();

            auto is_in_numbering_subset = [&numbering_subset](const NodeBearer::shared_ptr& node_bearer_ptr)
            {
                assert(!(!node_bearer_ptr));
                return node_bearer_ptr->IsInNumberingSubset(numbering_subset);
            };

            std::ranges::copy_if(proc_node_bearer_list,

                                 std::back_inserter(ret),
                                 is_in_numbering_subset);

            std::ranges::copy_if(ghost_node_bearer_list,

                                 std::back_inserter(ret),
                                 is_in_numbering_subset);
            return ret;
        }


        void AllocateAndBuildInterpolatorMatrix(
            const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
            const Wrappers::Petsc::MatrixPattern& matrix_pattern,
            GlobalMatrix& interpolation_matrix)
        {

            decltype(auto) source_felt_space = coords_matching_interpolator.GetSourceFEltSpace();
            decltype(auto) target_felt_space = coords_matching_interpolator.GetTargetFEltSpace();
            decltype(auto) source_numbering_subset = coords_matching_interpolator.GetSourceNumberingSubset();
            decltype(auto) target_numbering_subset = coords_matching_interpolator.GetTargetNumberingSubset();

            decltype(auto) mpi = source_felt_space.GetMpi();

            const auto Nprocessor_wise_source_dof =
                static_cast<PetscInt>(source_felt_space.NprocessorWiseDof(source_numbering_subset));
            const auto Nprocessor_wise_target_dof =
                static_cast<PetscInt>(target_felt_space.NprocessorWiseDof(target_numbering_subset));

            const auto Nprogram_wise_source_dof =
                static_cast<PetscInt>(source_felt_space.NprogramWiseDof(source_numbering_subset));
            const auto Nprogram_wise_target_dof =
                static_cast<PetscInt>(target_felt_space.NprogramWiseDof(target_numbering_subset));

            decltype(auto) jCSR = matrix_pattern.GetJCsr();
            assert(static_cast<PetscInt>(jCSR.size()) == Nprocessor_wise_target_dof);
            const std::vector<double> interpolation_values(jCSR.size(), 1.);

            if (mpi.IsSequential())
            {
                assert(Nprocessor_wise_source_dof == Nprocessor_wise_target_dof);

                interpolation_matrix.InitSequentialMatrix(row_processor_wise_index_type{ Nprocessor_wise_target_dof },
                                                          col_processor_wise_index_type{ Nprocessor_wise_source_dof },
                                                          matrix_pattern,
                                                          mpi);

                interpolation_matrix.SetSequentialNonZeroPatternWithValues(matrix_pattern, interpolation_values);
            } else
            {
                assert(Nprogram_wise_source_dof == Nprogram_wise_target_dof);

                interpolation_matrix.InitParallelMatrix(row_processor_wise_index_type{ Nprocessor_wise_target_dof },
                                                        col_processor_wise_index_type{ Nprocessor_wise_source_dof },
                                                        row_program_wise_index_type{ Nprogram_wise_target_dof },
                                                        col_program_wise_index_type{ Nprogram_wise_source_dof },
                                                        matrix_pattern,
                                                        mpi);

                interpolation_matrix.SetParallelNonZeroPatternWithValues(matrix_pattern, interpolation_values);
            }
        }


        NodeBearer::vector_const_shared_ptr
        IdentifyTargetNodeBearerToProcess(const Dof::vector_shared_ptr& target_processor_wise_dof_list)
        {
            NodeBearer::vector_const_shared_ptr ret;
            ret.reserve(target_processor_wise_dof_list.size());

            for (const auto& target_dof_ptr : target_processor_wise_dof_list)
            {
                assert(!(!target_dof_ptr));
                const auto& target_dof = *target_dof_ptr;

                const auto target_node_ptr = target_dof.GetNodeFromWeakPtr();
                assert(!(!target_node_ptr));

                const auto target_node_bearer_ptr = target_node_ptr->GetNodeBearerFromWeakPtr();
                assert(!(!target_node_bearer_ptr));

                ret.push_back(target_node_bearer_ptr);
            }

            assert(ret.size() == target_processor_wise_dof_list.size());

            // A same \a NodeBearer may have been reported many times (e.g. for a vectorial unknown once per
            // component).
            Utilities::EliminateDuplicate(ret,
                                          Utilities::PointerComparison::Less<NodeBearer::const_shared_ptr>(),
                                          Utilities::PointerComparison::Equal<NodeBearer::const_shared_ptr>());

            return ret;
        }


        /*!
         * \brief Compute for each row the position of the non zero element.
         *
         * We know there is exactly one per row due to the nature of the operator.
         *
         * \return The exterior vector iterates over the rows of the interpolation matrix. The interior one gives away the program-wise position of non
         * zero elements.
         */
        // NOLINTBEGIN(readability-function-cognitive-complexity)
        std::vector<PetscInt>
        ComputeNonZeroPositionPerRow(const NodeBearer::vector_const_shared_ptr& target_node_bearer_list,
                                     const std::size_t Nprocessor_wise_target_dof,
                                     const NumberingSubset& source_numbering_subset,
                                     const NumberingSubset& target_numbering_subset,
                                     const MeshNS::InterpolationNS::CoordsMatching& coords_matching,
                                     const node_bearer_per_coords_index_list_type& source_node_bearer_list_per_coords)
        {
            constexpr PetscInt dumb_value = -99;
            std::vector<PetscInt> ret(Nprocessor_wise_target_dof, dumb_value);

            for (const auto& target_node_bearer_ptr : target_node_bearer_list)
            {
                assert(!(!target_node_bearer_ptr));
                const auto& target_node_bearer = *target_node_bearer_ptr;

                decltype(auto) target_interface = target_node_bearer.GetInterface();

                decltype(auto) target_interface_coords_list =
                    target_interface.ComputeCoordsIndexList<CoordsNS::index_enum::from_mesh_file>();

                const auto source_coords_index = coords_matching.FindSourceIndex(target_interface_coords_list);

                auto it = source_node_bearer_list_per_coords.find(source_coords_index);
                assert(it != source_node_bearer_list_per_coords.cend());

                const auto& source_node_bearer_ptr = it->second;
                assert(!(!source_node_bearer_ptr));
                const auto& source_node_bearer = *source_node_bearer_ptr;

                decltype(auto) source_node_list = source_node_bearer.GetNodeList(source_numbering_subset);

                decltype(auto) target_node_list = target_node_bearer.GetNodeList(target_numbering_subset);

                const auto Nnode = source_node_list.size();
                assert(Nnode == target_node_list.size());

                for (auto node_index = 0UL; node_index < Nnode; ++node_index)
                {

                    const auto& source_node_ptr = source_node_list[node_index];
                    const auto& target_node_ptr = target_node_list[node_index];

                    assert(!(!source_node_ptr));
                    assert(!(!target_node_ptr));

                    decltype(auto) source_dof_list = source_node_ptr->GetDofList();
                    decltype(auto) target_dof_list = target_node_ptr->GetDofList();

                    assert(source_dof_list.size() == target_dof_list.size());

                    const auto Ndof = source_dof_list.size();

                    for (auto dof_index = 0UL; dof_index < Ndof; ++dof_index)
                    {
                        assert(dof_index < target_dof_list.size());
                        const auto& target_dof_ptr = target_dof_list[dof_index];
                        assert(!(!target_dof_ptr));
                        const auto& target_dof = *target_dof_ptr;
                        const auto processor_wise_index =
                            target_dof.GetProcessorWiseOrGhostIndex(target_numbering_subset);
                        assert(processor_wise_index.Get() < ret.size());

                        assert(ret[processor_wise_index.Get()] == dumb_value
                               && "A given row should be handled only once!");
                        assert(!(!source_dof_list[dof_index]));

                        ret[processor_wise_index.Get()] = static_cast<PetscInt>(
                            source_dof_list[dof_index]->GetProgramWiseIndex(source_numbering_subset).Get());
                    }
                }
            }

            assert(std::ranges::none_of(ret,

                                        [](const PetscInt value)
                                        {
                                            return value == dumb_value;
                                        }));

            return ret;
        }
        // NOLINTEND(readability-function-cognitive-complexity)

        std::vector<std::vector<PetscInt>>
        ComputeNonZeroPatternForMatrixPattern(const std::vector<PetscInt>& interpolation_pattern)
        {
            std::vector<std::vector<PetscInt>> ret;
            ret.reserve(interpolation_pattern.size());

            for (auto value : interpolation_pattern)
                ret.push_back({ value });

            assert(ret.size() == interpolation_pattern.size());

            return ret;
        }


    } // namespace


} // namespace MoReFEM::NonConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
