// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_FROMCOORDSMATCHING_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_FROMCOORDSMATCHING_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <optional>
#include <vector>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal { class Parallelism; }
namespace MoReFEM::Internal::FEltSpaceNS { class CoordsMatchingInterpolator; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::NonConformInterpolatorNS
{


    /*!
     * \brief Enum class to say whether matrix pattern should be stored.
     *
     * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
     */
    enum class store_matrix_pattern { yes, no };


    //! Convenient alias to pairing.
    using pairing_type = Advanced::ConformInterpolatorNS::pairing_type;


    /*!
     * \brief Class that matches the dofs on vertices from two different meshes.
     *
     * \attention This class is a tad unwieldy to use, and currently assumes that:
     *
     * . Finite element spaces/numbering subset couples from both source and target feature the same amounts
     * of unknowns sort in the same way. In other words, (velocity_solid, pressure_solid) for source and
     * (pressure_fluid, velocity_fluid) won't be handled for the time being.
     * . Finite elements on interface must be P1.
     *
     * \internal There is room for improvement here should the construction of this interpolator proves to be a
     * bottleneck (I don't think so, but still possible): the reduction to processor-wise dofs intervenes rather
     * lately in the process and so there are some unused computation.
     * \endinternal
     */
    class FromCoordsMatching
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FromCoordsMatching;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to unique pointer to const object.
        using const_unique_ptr = std::unique_ptr<const self>;


      public:
        /// \name Special members.
        ///@{


        /*!
         * \class doxygen_hide_from_coords_matching_do_store_matrix_pattern_arg
         *
         * \param[in] do_store_matrix_pattern Whether matrix pattern should be stored.
         * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
         */


        /*! Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         * \copydoc doxygen_hide_from_coords_matching_do_store_matrix_pattern_arg
         * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
         * \param[in] coords_matching_interpolator_index Index in the input file of the \a CoordsMatching operator.
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit FromCoordsMatching(const MoReFEMDataT& morefem_data,
                                    std::size_t coords_matching_interpolator_index,
                                    store_matrix_pattern do_store_matrix_pattern = store_matrix_pattern::no);

        //! Destructor.
        ~FromCoordsMatching() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FromCoordsMatching(const FromCoordsMatching& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FromCoordsMatching(FromCoordsMatching&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FromCoordsMatching& operator=(const FromCoordsMatching& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FromCoordsMatching& operator=(FromCoordsMatching&& rhs) = delete;

        ///@}

        //! Access to interpolation matrix.
        const GlobalMatrix& GetInterpolationMatrix() const noexcept;

        /*!
         * \brief Pattern of the matrix.
         *
         * \attention Should be called only if the matrix pattern is stored in the class (see constructor dedicated
         * argument).
         *
         * \return Pattern of the matrix.
         */
        const Wrappers::Petsc::MatrixPattern& GetMatrixPattern() const noexcept;

        /*!
         * \brief Keep for each processor-wise index of target the position of the related \a Dof on source.
         *
         * This information is a bit redundant with the interpolation matrix itself but is very helpful for debug
         * purposes.
         *
         * \return There is one item per row (which processor-wise index is represented by the index of the vector); the value inside is the (program-wise)
         * position of the associated source \a Dof.
         *
         */
        const std::vector<PetscInt>& GetNonZeroPositionPerRow() const noexcept;


      private:
        /*!
         * \brief Construct the object in the case of a standard run. Should not be called outside of constructor.
         *
         * \param[in] coords_matching_interpolator Internal object used by the interpolator.
         * \copydoc doxygen_hide_from_coords_matching_do_store_matrix_pattern_arg
         * 'yes' is relevant only for some Test executables; otherwise 'no' is fine and is hence the default choice.
         */
        void StandardConstruct(const Internal::FEltSpaceNS::CoordsMatchingInterpolator& coords_matching_interpolator,
                               store_matrix_pattern do_store_matrix_pattern);

        /*!
         * \brief Construct the object in the case of a run from prepartitioned data.
         *
         *  Should not be called outside of constructor.
         *
         *  \copydetails doxygen_hide_mpi_param
         *  \param[in] coords_matching_interpolator_index Index identifying the interpolator.
         */
        void ConstructFromPrepartitionedData(const Wrappers::Mpi& mpi,
                                             const std::size_t coords_matching_interpolator_index);


        //! Access to interpolation matrix.
        GlobalMatrix& GetNonCstInterpolationMatrix() noexcept;

        /*!
         * \brief Write in the chosen directory date required to rebuild  the interpolator.
         *
         * As the \a Coords::index_from_mesh_file is not the same in the case of a run from prepartitioned data
         * (which uses up a reduced mesh from the start), the \a MeshNS::InterpolationNS::CoordsMatching object
         * can't be used with reconstructed data. The only missing data however is the position for each row of the
         * non zero element, which is read from a file savec in \a directory.
         *
         * \attention This method does nothing if \a file_for_prepartitioned_data_ is empty (should happen only when no parallelism block in the input
         * file.
         */
        void PrintIfRelevantPrepartitionedData() const;


        /*!
         * \brief Set the file which includes the data for a prepartitioned run - if relevant.
         *
         * Nothing is done if the strategy chosen is Advanced::parallelism_strategy::parallel_no_write.
         *
         * \copydoc doxygen_hide_parallelism_param
         */
        void SetFileForPrepartitionedData(const Internal::Parallelism& parallelism);

        /*!
         * \brief Get the path of the file in which prepartitioned data are (or will be depending on the current parallelism strategy) stored.
         *
         * \return Path of the file if relevant. If this is called whereas a non relevant parallelism strategy has been chosen, the program is aborted
         *
         *  \internal This is handled by an error or assert as this Accessor is a purely internal one; it shouldn't
         * be exposed to the user regardless of its choice of strategy in the Lua file.
         */
        const FilesystemNS::File& GetFileFromPrepartitionedData() const;

      private:
        //! Global matrix used for the interpolation.
        GlobalMatrix::unique_ptr interpolation_matrix_ = nullptr;

        /*!
         * \brief Pattern of the matrix.
         *
         * \note It is stored only if constructor requires it explicitly - if not it is reset to nullptr once used.
         */
        Wrappers::Petsc::MatrixPattern::const_unique_ptr matrix_pattern_ = nullptr;

        /*!
         * \brief Keep for each processor-wise index of target the position of the related \a Dof on source.
         *
         * This information is a bit redundant with the interpolation matrix itself but is very helpful for debug
         * purposes.
         */
        std::vector<PetscInt> non_zero_position_per_row_;

        /*!
         * \brief Path to the file in which prepartitioned data will be written.
         *
         * This attribute must be set only by \a SetFileForPrepartitionedData; the directory creation if needed is
         * handled there.
         */
        std::optional<FilesystemNS::File> file_for_prepartitioned_data_;

#ifndef NDEBUG

        //! Whether matrix pattern is kept within the class or not. If 'no', GetMatrixPattern() shouldn't be called.
        store_matrix_pattern do_store_matrix_pattern_ = store_matrix_pattern::no;
#endif // NDEBUG
    };


} // namespace MoReFEM::NonConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_FROMCOORDSMATCHING_DOT_HPP_
// *** MoReFEM end header guards *** < //
