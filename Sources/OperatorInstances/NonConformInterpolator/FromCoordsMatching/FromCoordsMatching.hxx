// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_FROMCOORDSMATCHING_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_FROMCOORDSMATCHING_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM::NonConformInterpolatorNS { enum class store_matrix_pattern; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::NonConformInterpolatorNS
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    FromCoordsMatching::FromCoordsMatching(const MoReFEMDataT& morefem_data,
                                           const std::size_t coords_matching_interpolator_index,
                                           store_matrix_pattern do_store_matrix_pattern)
#ifndef NDEBUG
    : do_store_matrix_pattern_(do_store_matrix_pattern)
#endif // NDEBUG
    {
        decltype(auto) coords_matching_interpolator_manager =
            Internal::FEltSpaceNS::CoordsMatchingInterpolatorManager::GetInstance();

        decltype(auto) coords_matching_interpolator =
            coords_matching_interpolator_manager.GetCoordsMatchingInterpolator(coords_matching_interpolator_index);

        decltype(auto) parallelism_ptr = morefem_data.GetParallelismPtr();

        assert(!(!parallelism_ptr));
        const auto& parallelism = *parallelism_ptr;

        SetFileForPrepartitionedData(parallelism);

        switch (parallelism.GetParallelismStrategy())
        {
        case Advanced::parallelism_strategy::precompute:
        case Advanced::parallelism_strategy::parallel_no_write:
        case Advanced::parallelism_strategy::parallel:
        {
            StandardConstruct(coords_matching_interpolator, do_store_matrix_pattern);
            break;
        }
        case Advanced::parallelism_strategy::run_from_preprocessed:
        {
            ConstructFromPrepartitionedData(morefem_data.GetMpi(), coords_matching_interpolator_index);
            break;
        }
        case Advanced::parallelism_strategy::none:
            assert(false && "Invalid choice for this method!");
            exit(EXIT_FAILURE);
        }
    }


    inline const GlobalMatrix& FromCoordsMatching::GetInterpolationMatrix() const noexcept
    {
        assert(!(!interpolation_matrix_));
        return *interpolation_matrix_;
    }


    inline GlobalMatrix& FromCoordsMatching::GetNonCstInterpolationMatrix() noexcept
    {
        return const_cast<GlobalMatrix&>(GetInterpolationMatrix());
    }


    inline const Wrappers::Petsc::MatrixPattern& FromCoordsMatching::GetMatrixPattern() const noexcept
    {
        assert(do_store_matrix_pattern_ == store_matrix_pattern::yes && "If 'no', present method shouldn't be called.");
        assert(!(!matrix_pattern_));
        return *matrix_pattern_;
    }


    inline const std::vector<PetscInt>& FromCoordsMatching::GetNonZeroPositionPerRow() const noexcept
    {
        return non_zero_position_per_row_;
    }


} // namespace MoReFEM::NonConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_FROMCOORDSMATCHING_DOT_HXX_
// *** MoReFEM end header guards *** < //
