// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_GLOBALCOORDSQUADPOINTS_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_GLOBALCOORDSQUADPOINTS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp" // IWYU pragma: export
#include "Core/TimeManager/Instances.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

#include "OperatorInstances/ParameterOperator/Local/GlobalCoordsQuadPoints.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalParameterOperatorNS
{


    /*!
     * \brief Implementation of global \a GlobalCoordsQuadPoints operator.
     *
     * Given a \a QuadratureRule, this operator computes the global coordinates of the \a QuadraturePoints used by
     * this rule on the relevant finite element space and stores them into a \a ParameterAtQuadraturePoint
     * attribute.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT = TimeManagerNS::Instance::Static>
    class GlobalCoordsQuadPoints final
    : public GlobalParameterOperator<GlobalCoordsQuadPoints<TimeManagerT>,
                                     LocalParameterOperatorNS::GlobalCoordsQuadPoints<TimeManagerT>,
                                     ParameterNS::Type::vector,
                                     TimeManagerT>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GlobalCoordsQuadPoints<TimeManagerT>;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Convenient alias to pinpoint the GlobalParameterOperator parent.
        using parent = GlobalParameterOperator<self,
                                               LocalParameterOperatorNS::GlobalCoordsQuadPoints<TimeManagerT>,
                                               ParameterNS::Type::vector,
                                               TimeManagerT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Alias to the local operator coupled with this global one.
        using local_operator_type = LocalParameterOperatorNS::GlobalCoordsQuadPoints<TimeManagerT>;

        //! Alias 'inherited' from local operator class,
        using param_at_quad_pt_type = typename local_operator_type::param_at_quad_pt_type;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] felt_space Finite element space upon which the operator is defined.
         * \param[in] unknown Unknown considered for this operator (might be vector or vectorial).
         * \param[in] quadrature_rule_per_topology Quadrature rule from which the global coordinates of the
         * quadrature points will be computed. \param[in,out] global_coords_quad_pt \a Parameter to update.
         */
        explicit GlobalCoordsQuadPoints(const FEltSpace& felt_space,
                                        const Unknown& unknown,
                                        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                        param_at_quad_pt_type& global_coords_quad_pt);

        //! Destructor.
        ~GlobalCoordsQuadPoints() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalCoordsQuadPoints(const GlobalCoordsQuadPoints& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalCoordsQuadPoints(GlobalCoordsQuadPoints&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalCoordsQuadPoints& operator=(const GlobalCoordsQuadPoints& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalCoordsQuadPoints& operator=(GlobalCoordsQuadPoints&& rhs) = delete;

        ///@}


        /*!
         * \brief To get the local operator to convert local quadrature point coordinates into global ones.
         */
        void Update() const;


      public:
        /*!
         * \brief Only defined to respect the generic interface expected by the parent, it is not used as there is
         * no extraction of data from a global to a local level needed for this class.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         *
         */
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         local_operator_type& local_operator,
                                         const std::tuple<>&&) const;
    };


} // namespace MoReFEM::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ParameterOperator/GlobalCoordsQuadPoints.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_GLOBALCOORDSQUADPOINTS_DOT_HPP_
// *** MoReFEM end header guards *** < //
