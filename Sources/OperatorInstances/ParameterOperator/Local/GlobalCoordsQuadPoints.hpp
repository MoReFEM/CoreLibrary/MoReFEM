// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_GLOBALCOORDSQUADPOINTS_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_GLOBALCOORDSQUADPOINTS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Instances.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"


namespace MoReFEM::LocalParameterOperatorNS
{


    /*!
     * \brief Implementation of local \a GlobalCoordsQuadPoints operator.
     * This local operator is in charge of computing the global coordinates of each quadrature point.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT = TimeManagerNS::Instance::Static>
    class GlobalCoordsQuadPoints final
    : public Advanced::LocalParameterOperator<ParameterNS::Type::vector, TimeManagerT>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GlobalCoordsQuadPoints<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to parent.
        using parent = Advanced::LocalParameterOperator<ParameterNS::Type::vector, TimeManagerT>;

        //! Alias 'inherited' from parent class,
        using elementary_data_type = typename parent::elementary_data_type;

        // clang-format off
        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using param_at_quad_pt_type = ParameterAtQuadraturePoint
        <
            ParameterNS::Type::vector,
            TimeManagerT,
            ParameterNS::TimeDependencyNS::None
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity.
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in,out] global_coords_quad_pt \a Parameter to update.
         *
         * \internal This constructor must not be called manually: it is involved only in
         * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         */
        explicit GlobalCoordsQuadPoints(const ExtendedUnknown::const_shared_ptr& unknown_list,
                                        elementary_data_type&& elementary_data,
                                        param_at_quad_pt_type& global_coords_quad_pt);


        //! Destructor.
        ~GlobalCoordsQuadPoints() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalCoordsQuadPoints(const GlobalCoordsQuadPoints& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalCoordsQuadPoints(GlobalCoordsQuadPoints&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalCoordsQuadPoints& operator=(const GlobalCoordsQuadPoints& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalCoordsQuadPoints& operator=(GlobalCoordsQuadPoints&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();
    };


} // namespace MoReFEM::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ParameterOperator/Local/GlobalCoordsQuadPoints.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_GLOBALCOORDSQUADPOINTS_DOT_HPP_
// *** MoReFEM end header guards *** < //
