// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_GLOBALCOORDSQUADPOINTS_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_GLOBALCOORDSQUADPOINTS_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/GlobalCoordsQuadPoints.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"


namespace MoReFEM::LocalParameterOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    GlobalCoordsQuadPoints<TimeManagerT>::GlobalCoordsQuadPoints(
        const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
        elementary_data_type&& a_elementary_data,
        // clang-format off
        ParameterAtQuadraturePoint
        <
             ParameterNS::Type::vector,
             TimeManagerT,
             ParameterNS::TimeDependencyNS::None
        >& global_coords_quad_pt)
    // clang-format on
    : parent(a_unknown_storage, std::move(a_elementary_data), global_coords_quad_pt)
    { }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& GlobalCoordsQuadPoints<TimeManagerT>::ClassName()
    {
        static std::string name("GlobalCoordsQuadPoints");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void GlobalCoordsQuadPoints<TimeManagerT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = parent::GetNonCstElementaryData();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        SpatialPoint global_quad_pt_coords_as_spatial_point;

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            auto functor = [&geom_elt, &quad_pt, &global_quad_pt_coords_as_spatial_point](auto& global_quad_pt_coords)
            {
                assert(global_quad_pt_coords.size()
                       == global_quad_pt_coords_as_spatial_point.GetUnderlyingVector().size());

                Advanced::GeomEltNS::Local2Global(geom_elt, quad_pt, global_quad_pt_coords_as_spatial_point);

                global_quad_pt_coords(0) = global_quad_pt_coords_as_spatial_point.x();
                global_quad_pt_coords(1) = global_quad_pt_coords_as_spatial_point.y();
                global_quad_pt_coords(2) = global_quad_pt_coords_as_spatial_point.z();
            };

            parent::GetNonCstParameter().UpdateValue(quad_pt, geom_elt, functor);
        }
    }


} // namespace MoReFEM::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_GLOBALCOORDSQUADPOINTS_DOT_HXX_
// *** MoReFEM end header guards *** < //
