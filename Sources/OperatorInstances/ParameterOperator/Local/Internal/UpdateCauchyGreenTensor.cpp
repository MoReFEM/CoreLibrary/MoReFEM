// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>

#include "OperatorInstances/ParameterOperator/Local/Internal/UpdateCauchyGreenTensor.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Internal::LocalParameterOperatorNS
{


    void Update3D(const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& gradient_component_disp,
                  ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor)
    {
        using NumericNS::Square;

        assert(gradient_component_disp.rows() == 3);
        assert(gradient_component_disp.cols() == 3);

        assert(cauchy_green_tensor.size() == 6);

        // Component Cxx
        cauchy_green_tensor(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0))
                                 + Square(gradient_component_disp(1, 0)) + Square(gradient_component_disp(2, 0));

        // Component Cyy
        cauchy_green_tensor(1) = 1. + 2. * gradient_component_disp(1, 1) + Square(gradient_component_disp(0, 1))
                                 + Square(gradient_component_disp(1, 1)) + Square(gradient_component_disp(2, 1));

        // Component Czz
        cauchy_green_tensor(2) = 1. + 2. * gradient_component_disp(2, 2) + Square(gradient_component_disp(0, 2))
                                 + Square(gradient_component_disp(1, 2)) + Square(gradient_component_disp(2, 2));

        // Component Cxy
        cauchy_green_tensor(3) = gradient_component_disp(0, 1) + gradient_component_disp(1, 0)
                                 + gradient_component_disp(0, 0) * gradient_component_disp(0, 1)
                                 + gradient_component_disp(1, 0) * gradient_component_disp(1, 1)
                                 + gradient_component_disp(2, 0) * gradient_component_disp(2, 1);

        // Component Cyz
        cauchy_green_tensor(4) = gradient_component_disp(1, 2) + gradient_component_disp(2, 1)
                                 + gradient_component_disp(0, 2) * gradient_component_disp(0, 1)
                                 + gradient_component_disp(1, 2) * gradient_component_disp(1, 1)
                                 + gradient_component_disp(2, 2) * gradient_component_disp(2, 1);

        // Component Cxz
        cauchy_green_tensor(5) = gradient_component_disp(2, 0) + gradient_component_disp(0, 2)
                                 + gradient_component_disp(0, 2) * gradient_component_disp(0, 0)
                                 + gradient_component_disp(1, 2) * gradient_component_disp(1, 0)
                                 + gradient_component_disp(2, 2) * gradient_component_disp(2, 0);
    }


    void Update2D(const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& gradient_component_disp,
                  ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor)
    {
        using NumericNS::Square;

        assert(cauchy_green_tensor.size() == 3);
        assert(gradient_component_disp.rows() == 2);
        assert(gradient_component_disp.cols() == 2);

        // Component Cxx
        cauchy_green_tensor(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0))
                                 + Square(gradient_component_disp(1, 0));

        // Component Cyy
        cauchy_green_tensor(1) = 1. + 2. * gradient_component_disp(1, 1) + Square(gradient_component_disp(0, 1))
                                 + Square(gradient_component_disp(1, 1));

        // Component Cxy
        cauchy_green_tensor(2) = gradient_component_disp(0, 1) + gradient_component_disp(1, 0)
                                 + gradient_component_disp(0, 0) * gradient_component_disp(0, 1)
                                 + gradient_component_disp(1, 0) * gradient_component_disp(1, 1);
    }


    void Update1D(const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& gradient_component_disp,
                  ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor)
    {
        using NumericNS::Square;

        assert(gradient_component_disp.rows() == 1);
        assert(gradient_component_disp.cols() == 1);
        assert(cauchy_green_tensor.size() == 1);

        // Component Cxx
        cauchy_green_tensor(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0));
    }


} // namespace MoReFEM::Internal::LocalParameterOperatorNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
