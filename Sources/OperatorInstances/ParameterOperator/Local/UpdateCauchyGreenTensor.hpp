// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp" // IWYU pragma: export

#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Instances.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "OperatorInstances/ParameterOperator/Local/Internal/UpdateCauchyGreenTensor.hpp"


namespace MoReFEM::LocalParameterOperatorNS
{


    /*!
     * \brief Implementation of local \a UpdateCauchyGreenTensor operator.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT = TimeManagerNS::Instance::Static,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Wrappers::EigenNS::dWVector
    >
    // clang-format on
    class UpdateCauchyGreenTensor final
    : public Advanced::LocalParameterOperator<ParameterNS::Type::vector, TimeManagerT, TimeDependencyT, StorageT>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = UpdateCauchyGreenTensor;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to parent.
        using parent =
            Advanced::LocalParameterOperator<ParameterNS::Type::vector, TimeManagerT, TimeDependencyT, StorageT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using param_at_quad_pt_type = typename parent::param_at_quad_pt_type;

        //! Alias 'inherited' from parent class.
        using elementary_data_type = typename parent::elementary_data_type;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one vector).
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in,out] cauchy_green_tensor \a Parameter to update.
         *
         * \internal This constructor must not be called manually: it is involved only in
         * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         */
        explicit UpdateCauchyGreenTensor(const ExtendedUnknown::const_shared_ptr& unknown_list,
                                         elementary_data_type&& elementary_data,
                                         param_at_quad_pt_type& cauchy_green_tensor);

        //! Destructor.
        ~UpdateCauchyGreenTensor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        UpdateCauchyGreenTensor(const UpdateCauchyGreenTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UpdateCauchyGreenTensor(UpdateCauchyGreenTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UpdateCauchyGreenTensor& operator=(const UpdateCauchyGreenTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UpdateCauchyGreenTensor& operator=(UpdateCauchyGreenTensor&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! Constant accessor to the former increment local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetIncrementLocalDisplacement() const noexcept;

        //! Non constant accessor to the former increment local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstIncrementLocalDisplacement() noexcept;

      private:
        /*!
         * \brief Increment Displacement obtained at previous time iteration expressed at the local level.
         *
         * \internal This is a work variable that should be used only within ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd increment_local_displacement_;
    };


} // namespace MoReFEM::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
