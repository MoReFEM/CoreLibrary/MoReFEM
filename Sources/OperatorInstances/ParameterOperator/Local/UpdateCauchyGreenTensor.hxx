// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp"
// *** MoReFEM header guards *** < //


#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::LocalParameterOperatorNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>::UpdateCauchyGreenTensor(
        const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
        elementary_data_type&& a_elementary_data,
        param_at_quad_pt_type& cauchy_green_tensor)
    : parent(a_unknown_storage, std::move(a_elementary_data), cauchy_green_tensor)
    {
        const auto& elementary_data = parent::GetElementaryData();

        increment_local_displacement_.resize(elementary_data.NdofRow());
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    const std::string& UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>::ClassName()
    {
        static std::string name("UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>");
        return name;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    void UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = parent::GetNonCstElementaryData();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto& increment_local_displacement = GetIncrementLocalDisplacement();

        const auto mesh_dimension = elementary_data.GetMeshDimension();

        const auto& ref_felt = elementary_data.GetRefFElt(parent::GetExtendedUnknown());

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();

            assert(increment_local_displacement.size()
                   == quad_pt_unknown_data.Nnode() * infos_at_quad_pt.GetMeshDimension().Get());

            auto gradient_displacement = ref_felt.ComputeGradientDisplacementMatrix(
                quad_pt_unknown_data, increment_local_displacement, mesh_dimension);

            auto functor =
                [&gradient_displacement, mesh_dimension](::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor)
            {
                switch (mesh_dimension.Get())
                {
                case 1:
                    Internal::LocalParameterOperatorNS::Update1D(gradient_displacement, cauchy_green_tensor);
                    break;
                case 2:
                    Internal::LocalParameterOperatorNS::Update2D(gradient_displacement, cauchy_green_tensor);
                    break;
                case 3:
                    Internal::LocalParameterOperatorNS::Update3D(gradient_displacement, cauchy_green_tensor);
                    break;
                default:
                    assert(false);
                    exit(EXIT_FAILURE);
                }
            };

            parent::GetNonCstParameter().UpdateValue(quad_pt, geom_elt, functor);
        }
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline auto
    UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>::GetIncrementLocalDisplacement() const noexcept
        -> const Eigen::VectorXd&
    {
        return increment_local_displacement_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline auto
    UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>::GetNonCstIncrementLocalDisplacement() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetIncrementLocalDisplacement());
    }


} // namespace MoReFEM::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
