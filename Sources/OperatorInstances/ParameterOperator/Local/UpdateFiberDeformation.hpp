// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATEFIBERDEFORMATION_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATEFIBERDEFORMATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"


namespace MoReFEM::LocalParameterOperatorNS
{


    /*!
     * \brief Local operator in charge of updating fiber deformation.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class UpdateFiberDeformation final
    : public Advanced::LocalParameterOperator<ParameterNS::Type::scalar, TimeManagerT>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = UpdateFiberDeformation<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to parent.
        using parent = Advanced::LocalParameterOperator<ParameterNS::Type::scalar, TimeManagerT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using scalar_param_at_quad_pt_type =
            ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using vectorial_param_at_quad_pt_type =
            ParameterAtQuadraturePoint<ParameterNS::Type::vector, TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Alias 'inherited' from parent class,
        using elementary_data_type = typename parent::elementary_data_type;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_ptr Unknown considered by the operators.
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] fiber_deformation Parameter to update.
         * \param[in] contraction_rheology_residual Parameter used to update th deformation fiber.
         * \param[in] schur_complement Parameter used to update th deformation fiber.
         *
         * \internal This constructor must not be called manually: it is involved only in
         * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         *
         * \internal Can't apply coding standard here about ordering of arguments as the first three ones are
         * expected by parent class in that exact order (and the two others are variadic in this parent class)
         */
        explicit UpdateFiberDeformation(const ExtendedUnknown::const_shared_ptr& unknown_ptr,
                                        elementary_data_type&& elementary_data,
                                        scalar_param_at_quad_pt_type& fiber_deformation,
                                        const scalar_param_at_quad_pt_type& contraction_rheology_residual,
                                        const vectorial_param_at_quad_pt_type& schur_complement);

        //! Destructor.
        ~UpdateFiberDeformation() = default;

        //! \copydoc doxygen_hide_copy_constructor
        UpdateFiberDeformation(const UpdateFiberDeformation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UpdateFiberDeformation(UpdateFiberDeformation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UpdateFiberDeformation& operator=(const UpdateFiberDeformation& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UpdateFiberDeformation& operator=(UpdateFiberDeformation&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! Constant accessor to the former increment local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetIncrementLocalDisplacement() const noexcept;

        //! Non constant accessor to the former increment local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstIncrementLocalDisplacement() noexcept;

      private:
        //! Constant access to the contraction rheologu residual.
        const scalar_param_at_quad_pt_type& GetContractionRheologyResidual() const noexcept;

        //! Constant access to the schur complement.
        const vectorial_param_at_quad_pt_type& GetSchurComplement() const noexcept;

      private:
        /*!
         * \brief Increment Displacement obtained at previous time iteration expressed at the local level.
         *
         * \internal This is a work variable that should be used only within ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd increment_local_displacement_;

      private:
        /*!
         * \brief Current residual of the contraction rheology at the newton iteration, will be used to update
         * ec_n+1. G_BS = (tau_c_n+12# + mu*ec_p_n+12)(1+2ec_n+12) - Es*(e1D_n+12# - ec_n+12)(1+2e1D_n+12#) K22 =
         * dG_BS_dec_n+1 residual = K22^(-1)*G_BS
         */
        const scalar_param_at_quad_pt_type& contraction_rheology_residual_;

        /*!
         * \brief Current schur complement at the newton iteration, will be used to update ec_n+1.
         * K21 = dG_dyn_dy_n+1, where G_dyn is the residual of the dynamics.
         * K22 = dG_BS_dec_n+1
         * schur = K22^(-1)*K21
         */
        const vectorial_param_at_quad_pt_type& schur_complement_;
    };


} // namespace MoReFEM::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATEFIBERDEFORMATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
