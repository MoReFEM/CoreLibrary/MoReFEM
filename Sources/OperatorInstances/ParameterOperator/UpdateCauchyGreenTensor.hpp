// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATECAUCHYGREENTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATECAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp" // IWYU pragma: export

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

#include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalParameterOperatorNS
{


    /*!
     * \brief Implementation of global \a UpdateCauchyGreenTensor operator.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Wrappers::EigenNS::dWVector
    >
    // clang-format on
    class UpdateCauchyGreenTensor final
    // clang-format off
    : public GlobalParameterOperator
    <
        UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>,
        LocalParameterOperatorNS::UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>,
        ParameterNS::Type::vector,
        TimeManagerT,
        TimeDependencyT,
        StorageT
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to array of unique pointers.
        template<std::size_t SizeT>
        using array_const_unique_ptr = std::array<const_unique_ptr, SizeT>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Convenient alias to pinpoint the GlobalParameterOperator parent.
        // clang-format off
        using parent =
        GlobalParameterOperator
        <
            UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>,
            LocalParameterOperatorNS::UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>,
            ParameterNS::Type::vector,
            TimeManagerT,
            TimeDependencyT,
            StorageT
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Alias to the type of the coupled local operator.
        using local_operator_type =
            LocalParameterOperatorNS::UpdateCauchyGreenTensor<TimeManagerT, TimeDependencyT, StorageT>;

        //! Alias 'inherited' from local operator class,
        using param_at_quad_pt_type = typename local_operator_type::param_at_quad_pt_type;

        static_assert(std::is_same_v<param_at_quad_pt_type, typename parent::param_at_quad_pt_type>);

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] felt_space Finite element space upon which the operator is defined.
         * \param[in] unknown Unknown considered for this operator (might be vector or vectorial).
         * \param[in,out] cauchy_green_tensor Cauchy Green tensor which is to be updated by present operator.
         * Size of a local vector inside must be 3 for a 2D mesh and 6 for a 3D one.
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit UpdateCauchyGreenTensor(const FEltSpace& felt_space,
                                         const Unknown& unknown,
                                         param_at_quad_pt_type& cauchy_green_tensor,
                                         const QuadratureRulePerTopology* const quadrature_rule_per_topology);

        //! Destructor.
        ~UpdateCauchyGreenTensor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        UpdateCauchyGreenTensor(const UpdateCauchyGreenTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UpdateCauchyGreenTensor(UpdateCauchyGreenTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UpdateCauchyGreenTensor& operator=(const UpdateCauchyGreenTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UpdateCauchyGreenTensor& operator=(UpdateCauchyGreenTensor&& rhs) = delete;

        ///@}


        /*!
         * \brief Assemble into one or several vectors.
         *
         * \param[in] current_solid_displacement The current solid displacement (iteration {n}) used to compute the
         * tensor.
         */
        void Update(const GlobalVector& current_solid_displacement) const;


      public:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * Here there is one:
         * - The current solid displacement (iteration {n}) used to compute the tensor.
         *
         */
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         local_operator_type& local_operator,
                                         const std::tuple<const GlobalVector&>& additional_arguments) const;
    };


} // namespace MoReFEM::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATECAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
