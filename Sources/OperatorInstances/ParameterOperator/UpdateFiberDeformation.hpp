// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATEFIBERDEFORMATION_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATEFIBERDEFORMATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp" // IWYU pragma: export

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

#include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalParameterOperatorNS
{


    /*!
     * \brief Implementation of global UpdateFiberDeformation operator.
     *
     * G_BS = (tau_c_n+12# + mu*ec_p_n+12)(1+2ec_n+12) - Es*(e1D_n+12# - ec_n+12)(1+2e1D_n+12#)
     * K22 = dG_BS_dec_n+1
     * K21 = dG_dyn_dy_n+1, where G_dyn is the residual of the dynamics.
     * ec_n+1(k+1) = ec_n+1_(k) + dec
     * dec = K22^-1(-G_BS - K21*dY)
     * ec = fiber deformation
     * dY = displacement increment
     *
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class UpdateFiberDeformation final
    // clang-format off
    : public GlobalParameterOperator
    <
        UpdateFiberDeformation<TimeManagerT>,
        LocalParameterOperatorNS::UpdateFiberDeformation<TimeManagerT>,
        ParameterNS::Type::scalar,
        TimeManagerT
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = UpdateFiberDeformation<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Convenient alias to pinpoint the GlobalParameterOperator parent.
        using parent = GlobalParameterOperator<self,
                                               LocalParameterOperatorNS::UpdateFiberDeformation<TimeManagerT>,
                                               ParameterNS::Type::scalar,
                                               TimeManagerT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Alias to the coupled local operator.
        using local_operator_type = LocalParameterOperatorNS::UpdateFiberDeformation<TimeManagerT>;

        //! Alias 'inherited' from local operator class,
        using scalar_param_at_quad_pt_type = typename local_operator_type::scalar_param_at_quad_pt_type;

        //! Alias 'inherited' from local operator class,
        using vectorial_param_at_quad_pt_type = typename local_operator_type::vectorial_param_at_quad_pt_type;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] felt_space Finite element space upon which the operator is defined.
         * \param[in] unknown Unknown considered for this operator (might be scalar or vectorial).
         * \param[in,out] fiber_deformation Fiber deformation parameter, which is updated by present class.
         * \param[in] contraction_rheology_residual Contraction rheology residual.
         * \param[in] schur_complement Schur complement.
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit UpdateFiberDeformation(const FEltSpace& felt_space,
                                        const Unknown& unknown,
                                        const scalar_param_at_quad_pt_type& contraction_rheology_residual,
                                        const vectorial_param_at_quad_pt_type& schur_complement,
                                        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                        scalar_param_at_quad_pt_type& fiber_deformation);


        //! Destructor.
        ~UpdateFiberDeformation() = default;

        //! \copydoc doxygen_hide_copy_constructor
        UpdateFiberDeformation(const UpdateFiberDeformation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UpdateFiberDeformation(UpdateFiberDeformation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UpdateFiberDeformation& operator=(const UpdateFiberDeformation& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UpdateFiberDeformation& operator=(UpdateFiberDeformation&& rhs) = delete;

        ///@}


        /*!
         * \brief Assemble into one or several vectors.
         *
         * \param[in] displacement_increment Vector that includes data from the previous iteration.
         */
        void Update(const GlobalVector& displacement_increment) const;


      public:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         local_operator_type& local_operator,
                                         const std::tuple<const GlobalVector&>& additional_arguments) const;
    };


} // namespace MoReFEM::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/ParameterOperator/UpdateFiberDeformation.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATEFIBERDEFORMATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
