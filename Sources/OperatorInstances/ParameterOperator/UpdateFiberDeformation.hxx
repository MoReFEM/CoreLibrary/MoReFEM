// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATEFIBERDEFORMATION_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATEFIBERDEFORMATION_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ParameterOperator/UpdateFiberDeformation.hpp"
// *** MoReFEM header guards *** < //


#include <tuple>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp"

#include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalParameterOperatorNS
{

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void UpdateFiberDeformation<TimeManagerT>::Update(const GlobalVector& displacement_increment) const
    {
        parent::UpdateImpl(displacement_increment);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void UpdateFiberDeformation<TimeManagerT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        local_operator_type& local_operator,
        const std::tuple<const GlobalVector&>& additional_arguments) const
    {
        GlobalVariationalOperatorNS::ExtractLocalDofValues(local_felt_space,
                                                           this->GetExtendedUnknown(),
                                                           std::get<0>(additional_arguments),
                                                           local_operator.GetNonCstIncrementLocalDisplacement());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    UpdateFiberDeformation<TimeManagerT>::UpdateFiberDeformation(
        const FEltSpace& felt_space,
        const Unknown& unknown,
        const scalar_param_at_quad_pt_type& contraction_rheology_residual,
        const vectorial_param_at_quad_pt_type& schur_complement,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
        scalar_param_at_quad_pt_type& fiber_deformation)
    : parent(felt_space,
             unknown,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::no,
             fiber_deformation,
             contraction_rheology_residual,
             schur_complement)
    { }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& UpdateFiberDeformation<TimeManagerT>::ClassName()
    {
        static std::string name("UpdateFiberDeformation");
        return name;
    }


} // namespace MoReFEM::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATEFIBERDEFORMATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
