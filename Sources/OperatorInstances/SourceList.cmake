### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
		${CMAKE_CURRENT_LIST_DIR}/OperatorInstances.doxygen
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/ConformInterpolator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/HyperelasticLaws/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/NonConformInterpolator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/ParameterOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/StateOperator/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/VariationalOperator/SourceList.cmake)
