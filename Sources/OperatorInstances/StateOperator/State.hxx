// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_STATEOPERATOR_STATE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_STATEOPERATOR_STATE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/StateOperator/State.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM::InterpolationOperatorNS
{


    inline const State::DirichletDofListType& State::GetProcessorWiseDirichletDofList() const
    {
        return processor_wise_dirichlet_dof_list_;
    }


    inline bool State::AreProcessorWiseDirichletDof() const
    {
        return !processor_wise_dirichlet_dof_list_.empty();
    }


#ifndef NDEBUG


    inline PetscInt State::NexpectedProgramWiseDofIncludingDirichlet() const noexcept
    {
        assert(Nexpected_program_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<PetscInt>());
        return Nexpected_program_wise_dof_including_dirichlet_;
    }


    inline PetscInt State::NexpectedProgramWiseDofExcludingDirichlet() const noexcept
    {
        assert(Nexpected_program_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<PetscInt>());
        return Nexpected_program_wise_dof_excluding_dirichlet_;
    }


    inline PetscInt State::NexpectedProcessorWiseDofIncludingDirichlet() const noexcept
    {
        assert(Nexpected_processor_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<PetscInt>());
        return Nexpected_processor_wise_dof_including_dirichlet_;
    }


    inline PetscInt State::NexpectedProcessorWiseDofExcludingDirichlet() const noexcept
    {
        assert(Nexpected_processor_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<PetscInt>());
        return Nexpected_processor_wise_dof_excluding_dirichlet_;
    }

#endif // NDEBUG


} // namespace MoReFEM::InterpolationOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_STATEOPERATOR_STATE_DOT_HXX_
// *** MoReFEM end header guards *** < //
