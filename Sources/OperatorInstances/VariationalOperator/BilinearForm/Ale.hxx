// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_ALE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_ALE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Ale.hpp"
// *** MoReFEM header guards *** < //


#include <string>

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp" // IWYU pragma: keep
// IWYU pragma: no_include "OperatorInstances/VariationalOperator/BilinearForm/Ale.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    Ale<TimeManagerT>::Ale(const FEltSpace& felt_space,
                           const Unknown::const_shared_ptr& unknown_ptr,
                           const Unknown::const_shared_ptr& test_unknown_ptr,
                           const scalar_parameter_type& density,
                           const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             density)
    { }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& Ale<TimeManagerT>::ClassName()
    {
        static std::string name("Ale");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LinearAlgebraTupleT>
    inline void Ale<TimeManagerT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                            const GlobalVector& input_vector,
                                            const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LocalOperatorTypeT>
    inline void
    Ale<TimeManagerT>::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                                   LocalOperatorTypeT& local_operator,
                                                   const std::tuple<const GlobalVector&>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              parent::GetNthUnknown(),
                              std::get<0>(additional_arguments),
                              local_operator.GetNonCstFormerLocalVelocity());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_ALE_DOT_HXX_
// *** MoReFEM end header guards *** < //
