// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_BIDOMAIN_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_BIDOMAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
#include <type_traits>
// IWYU pragma: no_include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp"      // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Bidomain.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM {

    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    class FiberList;

} // namespace MoReFEM

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief Operator description.
     *
     * \todo #9 Describe operator!
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    // clang-format on
    class Bidomain final
    // clang-format off
        : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            Bidomain<TimeManagerT>,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::Bidomain<TimeManagerT, TimeDependencyT>
        >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Bidomain<TimeManagerT, TimeDependencyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_operator_type = Advanced::LocalVariationalOperatorNS::Bidomain<TimeManagerT, TimeDependencyT>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Unique ptr.
        using const_unique_ptr = std::unique_ptr<const Bidomain>;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = typename local_operator_type::scalar_parameter_type;

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type = typename local_operator_type::vectorial_fiber_type;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_list Two scalar unknowns considered: first one is transcellular potential,
         * second one is extracellular one.
         * \copydoc doxygen_hide_test_unknown_list_param
         *
         * \param[in] intracellular_trans_diffusion_tensor intracellular_trans_diffusion_tensor
         * \param[in] extracellular_trans_diffusion_tensor extracellular_trans_diffusion_tensor
         * \param[in] intracellular_fiber_diffusion_tensor intracellular_fiber_diffusion_tensor
         * \param[in] extracellular_fiber_diffusion_tensor extracellular_fiber_diffusion_tensor
         * \param[in] fibers Fibers.
         * \copydoc doxygen_hide_gvo_felt_space_arg
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit Bidomain(const FEltSpace& felt_space,
                          const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                          const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                          const scalar_parameter_type& intracellular_trans_diffusion_tensor,
                          const scalar_parameter_type& extracellular_trans_diffusion_tensor,
                          const scalar_parameter_type& intracellular_fiber_diffusion_tensor,
                          const scalar_parameter_type& extracellular_fiber_diffusion_tensor,
                          const vectorial_fiber_type& fibers,
                          const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);


        //! Destructor.
        ~Bidomain() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Bidomain(const Bidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Bidomain(Bidomain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Bidomain& operator=(const Bidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Bidomain& operator=(Bidomain&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Assemble into one or several matrices.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
         *
         * \param[in] global_matrix_with_coeff_list List of global matrices into which the operator is
         * assembled. These matrices are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Bidomain.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_BIDOMAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
