// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_BIDOMAIN_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_BIDOMAIN_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Bidomain.hpp"
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    Bidomain<TimeManagerT, TimeDependencyT>::Bidomain(
        const FEltSpace& felt_space,
        const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
        const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
        const scalar_parameter_type& intracellular_trans_diffusion_tensor,
        const scalar_parameter_type& extracellular_trans_diffusion_tensor,
        const scalar_parameter_type& intracellular_fiber_diffusion_tensor,
        const scalar_parameter_type& extracellular_fiber_diffusion_tensor,
        const vectorial_fiber_type& fibers,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_list,
             test_unknown_list,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::no,
             intracellular_trans_diffusion_tensor,
             extracellular_trans_diffusion_tensor,
             intracellular_fiber_diffusion_tensor,
             extracellular_fiber_diffusion_tensor,
             fibers)
    { }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    const std::string& Bidomain<TimeManagerT, TimeDependencyT>::ClassName()
    {
        static std::string name("Bidomain");
        return name;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    inline void Bidomain<TimeManagerT, TimeDependencyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                                  const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_BIDOMAIN_DOT_HXX_
// *** MoReFEM end header guards *** < //
