// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_FWDFORHPP_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_FWDFORHPP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>  // IWYU pragma: export
#include <iosfwd> // IWYU pragma: export
#include <memory> // IWYU pragma: export
#include <tuple>  // IWYU pragma: export

#include "Core/Parameter/FiberEnum.hpp" // IWYU pragma: export
#include "Core/Parameter/TypeEnum.hpp"  // IWYU pragma: export

#include "Geometry/Domain/Domain.hpp"                          // IWYU pragma: export
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/Unknown.hpp" // IWYU pragma: export

#include "Operators/Enum.hpp"                                                      // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"       // IWYU pragma: export
#include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp" // IWYU pragma: export

// namespace MoReFEM { class GlobalVector; } // export not supported yet by IWYU

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_FWDFORHPP_DOT_HPP_
// *** MoReFEM end header guards *** < //
