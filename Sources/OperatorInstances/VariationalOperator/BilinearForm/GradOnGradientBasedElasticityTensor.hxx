// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hpp"
// *** MoReFEM header guards *** < //


#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp" // IWYU pragma: keep

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    GradOnGradientBasedElasticityTensor<TimeManagerT>::GradOnGradientBasedElasticityTensor(
        const FEltSpace& felt_space,
        const Unknown::const_shared_ptr& unknown_ptr,
        const Unknown::const_shared_ptr& test_unknown_ptr,
        const scalar_parameter_type& young_modulus,
        const scalar_parameter_type& poisson_ratio,
        const ParameterNS::GradientBasedElasticityTensorConfiguration configuration,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::no,
             young_modulus,
             poisson_ratio,
             configuration)
    {
        assert(unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& GradOnGradientBasedElasticityTensor<TimeManagerT>::ClassName()
    {
        static std::string name("GradOnGradientBasedElasticityTensor");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LinearAlgebraTupleT>
    inline void GradOnGradientBasedElasticityTensor<TimeManagerT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                                            const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
    }

} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
