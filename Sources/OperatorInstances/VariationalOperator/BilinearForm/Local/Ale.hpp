// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_ALE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_ALE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
#include <vector>
// IWYU pragma: no_include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    namespace AleNS
    {


    }


    /*!
     * \brief Local operator for \a Ale.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class Ale final : public BilinearLocalVariationalOperator<Eigen::MatrixXd>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Ale<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operator. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] density Density as a \a Parameter.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit Ale(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                     const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                     elementary_data_type&& elementary_data,
                     const scalar_parameter_type& density);

        //! Destructor.
        virtual ~Ale() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        Ale(const Ale& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Ale(Ale&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Ale& operator=(const Ale& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Ale& operator=(Ale&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


        //! Constant accessor to the former local velocity required by ComputeEltArray().
        const std::vector<double>& GetFormerLocalVelocity() const noexcept;

        //! Non constant accessor to the former local velocity required by ComputeEltArray().
        std::vector<double>& GetNonCstFormerLocalVelocity() noexcept;


      private:
        //! Access to the density.
        const scalar_parameter_type& GetDensity() const noexcept;

      private:
        //! Density.
        const scalar_parameter_type& density_;

        /*!
         * \brief Velocity obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd former_local_velocity_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Ale.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_ALE_DOT_HPP_
// *** MoReFEM end header guards *** < //
