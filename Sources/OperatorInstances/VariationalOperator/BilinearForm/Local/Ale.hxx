// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_ALE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_ALE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/Ale.hpp"
// *** MoReFEM header guards *** < //


#include <vector>

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void Ale<TimeManagerT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = GetNonCstElementaryData();
        const auto& former_local_velocity = GetFormerLocalVelocity();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Ncomponent = ref_felt.Ncomponent();

        const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

        assert(Ncomponent == test_ref_felt.Ncomponent());

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.setZero();

        const auto Nnode_for_unknown = ref_felt.Nnode();

        const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

        decltype(auto) geom_elt = elementary_data.GetCurrentGeomElt();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& phi = quad_pt_unknown_data.GetFEltPhi();

            const auto& phi_test = test_quad_pt_unknown_data.GetFEltPhi();
            const auto& grad_phi_test = test_quad_pt_unknown_data.GetGradientFEltPhi();

            decltype(auto) quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double geometric_factor =
                quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            const double factor = geometric_factor * GetDensity().GetValue(quad_pt, geom_elt);

            {
                // ===========================================
                // First add the modified_v * gradV * Vtest contribution.
                // Only the block for the first component is filled here.
                // ===========================================
                for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent; ++component)
                {
                    double modified_velocity_at_quad_pt = 0.;

                    // Compute the \a component for velocity at quadrature point.
                    for (auto node_test_index = 0UL; node_test_index < Nnode_for_test_unknown; ++node_test_index)
                    {
                        assert(node_test_index + component.Get() * Nnode_for_test_unknown
                               < former_local_velocity.size());
                        modified_velocity_at_quad_pt +=
                            former_local_velocity[node_test_index + component.Get() * Nnode_for_test_unknown]
                            * phi_test(node_test_index);
                    }

                    for (auto m = 0UL; m < Nnode_for_test_unknown; ++m)
                    {
                        for (auto n = 0UL; n < Nnode_for_unknown; ++n)
                        {
                            matrix_result(m, n) +=
                                factor * modified_velocity_at_quad_pt * grad_phi_test(m, component.Get()) * phi(n);
                        }
                    }
                }
            }

            {
                // ===========================================
                // Then add the div (modified_v) * (Vx Vtextx + Vy Vtesty) contribution.
                // Likewise, only the block for the first component is filled here.
                // ===========================================
                double div_modified_velocity_at_quad_pt = 0.;

                for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent; ++component)
                {
                    // Compute the \a component for velocity at quadrature point.
                    for (auto node_index = 0UL; node_index < Nnode_for_test_unknown; ++node_index)
                        div_modified_velocity_at_quad_pt +=
                            former_local_velocity[node_index + component.Get() * Nnode_for_test_unknown]
                            * grad_phi_test(node_index, component.Get());
                }

                for (auto m = 0UL; m < Nnode_for_test_unknown; ++m)
                {
                    const double m_value = factor * div_modified_velocity_at_quad_pt * phi_test(m);

                    for (auto n = 0UL; n < Nnode_for_unknown; ++n)
                        matrix_result(m, n) += m_value * phi(n);
                }
            }


            {
                // ===========================================
                // Duplicate the block for each component.
                // ===========================================
                for (auto m = 0UL; m < Nnode_for_test_unknown; ++m)
                {
                    for (auto n = 0UL; n < Nnode_for_unknown; ++n)
                    {
                        for (auto component_block = ::MoReFEM::GeometryNS::dimension_type{ 1UL };
                             component_block < Ncomponent;
                             ++component_block)
                        {
                            const auto shift_row = Nnode_for_test_unknown * component_block.Get();
                            const auto shift_col = Nnode_for_unknown * component_block.Get();
                            matrix_result(m + shift_row, n + shift_col) = matrix_result(m, n);
                        }
                    }
                }
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    Ale<TimeManagerT>::Ale(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                           const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                           elementary_data_type&& a_elementary_data,
                           const scalar_parameter_type& density)
    : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
      density_(density)
    {
        assert(a_unknown_storage.size() == 1 && "Operator currently written to be used with one unknown only!");
        assert(!(!a_unknown_storage.back()));
        assert(a_unknown_storage.back()->GetNature() == UnknownNS::Nature::vectorial);

        assert(a_test_unknown_storage.size() == 1 && "Operator currently written to be used with one unknown only!");
        assert(!(!a_test_unknown_storage.back()));
        assert(a_test_unknown_storage.back()->GetNature() == UnknownNS::Nature::vectorial);

        const auto& elementary_data = parent::GetElementaryData();

        former_local_velocity_.resize(elementary_data.NdofRow());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto Ale<TimeManagerT>::GetDensity() const noexcept -> const scalar_parameter_type&
    {
        return density_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto Ale<TimeManagerT>::GetFormerLocalVelocity() const noexcept -> const Eigen::VectorXd&
    {
        return former_local_velocity_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto Ale<TimeManagerT>::GetNonCstFormerLocalVelocity() noexcept -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalVelocity());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& Ale<TimeManagerT>::ClassName()
    {
        static std::string name("Ale");
        return name;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_ALE_DOT_HXX_
// *** MoReFEM end header guards *** < //
