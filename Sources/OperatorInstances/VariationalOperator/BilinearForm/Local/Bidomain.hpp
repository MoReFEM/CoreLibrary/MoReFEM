// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_BIDOMAIN_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_BIDOMAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export

#include "ParameterInstances/Fiber/FiberList.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local operator for \a Bidomain.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    // clang-format on
    class Bidomain final : public BilinearLocalVariationalOperator<Eigen::MatrixXd>,
                           public Crtp::LocalMatrixStorage<Bidomain<TimeManagerT>, 6UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Bidomain<TimeManagerT, TimeDependencyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, TimeDependencyT>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 6UL>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type = FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                               ParameterNS::Type::vector,
                                               TimeManagerT,
                                               TimeDependencyT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         *
         * \param[in] elementary_data Elementary matrices and
         * vectors that will perform the calculations.
         * \param[in] intracellular_trans_diffusion_tensor
         * Intracellular trans diffusion tensor.
         * \param[in] extracellular_trans_diffusion_tensor Extracellular
         * trans diffusion tensor. \param[in] intracellular_fiber_diffusion_tensor Intracellular fiber diffusion
         * tensor.
         * \param[in] extracellular_fiber_diffusion_tensor Extracellular fiber diffusion tensor.
         * \param[in] fibers Vectorial fibers.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method.
         * \endinternal
         */
        explicit Bidomain(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                          const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                          elementary_data_type&& elementary_data,
                          const scalar_parameter_type& intracellular_trans_diffusion_tensor,
                          const scalar_parameter_type& extracellular_trans_diffusion_tensor,
                          const scalar_parameter_type& intracellular_fiber_diffusion_tensor,
                          const scalar_parameter_type& extracellular_fiber_diffusion_tensor,
                          const vectorial_fiber_type& fibers);

        //! Destructor.
        virtual ~Bidomain() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        Bidomain(const Bidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Bidomain(Bidomain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Bidomain& operator=(const Bidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Bidomain& operator=(Bidomain&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        //! Get the first diffusion tensor.
        const scalar_parameter_type& GetIntracelluarTransDiffusionTensor() const noexcept;

        //! Get the second diffusion tensor.
        const scalar_parameter_type& GetExtracelluarTransDiffusionTensor() const noexcept;

        //! Get the third diffusion tensor.
        const scalar_parameter_type& GetIntracelluarFiberDiffusionTensor() const noexcept;

        //! Get the fourth diffusion tensor.
        const scalar_parameter_type& GetExtracelluarFiberDiffusionTensor() const noexcept;

        //! Get the fiber.
        const vectorial_fiber_type& GetFibers() const noexcept;

      private:
        //! \name Material parameters.
        ///@{

        //! First diffusion tensor = sigma_i_t.
        const scalar_parameter_type& intracellular_trans_diffusion_tensor_;

        //! Second diffusion tensor = sigma_e_t.
        const scalar_parameter_type& extracellular_trans_diffusion_tensor_;

        //! Third diffusion tensor = sigma_i_l.
        const scalar_parameter_type& intracellular_fiber_diffusion_tensor_;

        //! Fourth diffusion tensor = sigma_e_l.
        const scalar_parameter_type& extracellular_fiber_diffusion_tensor_;

        //! Fibers parameter.
        const vectorial_fiber_type& fibers_;

        ///@}

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes of local matrices.
        enum class LocalMatrixIndex : std::size_t {
            block_matrix1 = 0,
            block_matrix2,
            block_matrix3,
            block_matrix4,
            dphi_test_sigma,
            dpsi_test_sigma
        };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Bidomain.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_BIDOMAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
