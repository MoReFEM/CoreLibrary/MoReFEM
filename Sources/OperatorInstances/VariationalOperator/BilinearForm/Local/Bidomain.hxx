// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_BIDOMAIN_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_BIDOMAIN_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/Bidomain.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    Bidomain<TimeManagerT, TimeDependencyT>::Bidomain(
        const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
        elementary_data_type&& a_elementary_data,
        const scalar_parameter_type& intracellular_trans_diffusion_tensor,
        const scalar_parameter_type& extracellular_trans_diffusion_tensor,
        const scalar_parameter_type& intracellular_fiber_diffusion_tensor,
        const scalar_parameter_type& extracellular_fiber_diffusion_tensor,
        const vectorial_fiber_type& fibers)
    : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
      matrix_parent(), intracellular_trans_diffusion_tensor_(intracellular_trans_diffusion_tensor),
      extracellular_trans_diffusion_tensor_(extracellular_trans_diffusion_tensor),
      intracellular_fiber_diffusion_tensor_(intracellular_fiber_diffusion_tensor),
      extracellular_fiber_diffusion_tensor_(extracellular_fiber_diffusion_tensor), fibers_(fibers)
    {
        const auto& elementary_data = GetElementaryData();

        const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode().Get();

        const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
        const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode().Get();

        const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown1 = test_unknown1_ref_felt.Nnode().Get();

        const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
        const auto Nnode_for_test_unknown2 = test_unknown2_ref_felt.Nnode().Get();

        const auto felt_space_dimension = unknown1_ref_felt.GetFEltSpaceDimension().Get();

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_unknown1, Nnode_for_unknown1 },   // block matrix 1 (Vm,Vm)
            { Nnode_for_test_unknown2, Nnode_for_unknown2 },   // block matrix 2 (Ue,Ue)
            { Nnode_for_test_unknown1, Nnode_for_unknown2 },   // block matrix 3 (Vm, Ue)
            { Nnode_for_test_unknown2, Nnode_for_unknown1 },   // block matrix 4 (Ue, Vm)
            { Nnode_for_test_unknown1, felt_space_dimension }, // dPhi_test*sigma
            { Nnode_for_test_unknown2, felt_space_dimension }  // dPsi_test*sigma
        } });
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    const std::string& Bidomain<TimeManagerT, TimeDependencyT>::ClassName()
    {
        static std::string name("Bidomain");
        return name;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    void Bidomain<TimeManagerT, TimeDependencyT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.setZero();

        const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));

        const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));

        // Current operator yields in fact a diagonal per block matrix where each block is the same.
        auto& block_matrix1 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix1)>();
        auto& block_matrix2 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix2)>();
        auto& block_matrix3 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix3)>();
        auto& block_matrix4 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix4)>();
        auto& dphi_test_sigma =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
        auto& dpsi_test_sigma =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_test_sigma)>();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode();
        const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode();

        assert(unknown1_ref_felt.Ncomponent().Get() == 1UL && "Bidomain operator limited to scalar unknowns.");
        assert(unknown2_ref_felt.Ncomponent().Get() == 1UL && "Bidomain operator limited to scalar unknowns.");

        const auto Nnode_for_test_unknown1 = test_unknown1_ref_felt.Nnode();
        const auto Nnode_for_test_unknown2 = test_unknown2_ref_felt.Nnode();

        assert(test_unknown1_ref_felt.Ncomponent().Get() == 1UL && "Bidomain operator limited to scalar unknowns.");
        assert(test_unknown2_ref_felt.Ncomponent().Get() == 1UL && "Bidomain operator limited to scalar unknowns.");

        const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
        const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
        const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
        const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto felt_space_dimension = unknown1_ref_felt.GetFEltSpaceDimension();

        auto& fibers = GetFibers();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            // First compute the content of the block matrix.
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double factor = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            const double factor1 = factor * intracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);
            const double factor2 = factor * extracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);
            const double factor3 = factor * intracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);
            const double factor4 = factor * extracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

            const auto& grad_felt_phi_for_unknown = quad_pt_unknown_data.GetGradientFEltPhi();
            const auto& dphi = unknown1_ref_felt.ExtractSubMatrix(grad_felt_phi_for_unknown);
            const auto& dpsi = unknown2_ref_felt.ExtractSubMatrix(grad_felt_phi_for_unknown);
            const auto& grad_felt_phi_for_test_unknown = test_quad_pt_unknown_data.GetGradientFEltPhi();
            const auto& dphi_test = test_unknown1_ref_felt.ExtractSubMatrix(grad_felt_phi_for_test_unknown);
            const auto& dpsi_test = test_unknown2_ref_felt.ExtractSubMatrix(grad_felt_phi_for_test_unknown);

            assert(dphi.rows() == Nnode_for_unknown1.Get());
            assert(dpsi.rows() == Nnode_for_unknown2.Get());
            assert(dphi_test.rows() == Nnode_for_test_unknown1.Get());
            assert(dpsi_test.rows() == Nnode_for_test_unknown2.Get());

            block_matrix1.noalias() = factor1 * dphi_test * dphi.transpose();
            block_matrix2.noalias() = (factor1 + factor2) * dpsi_test * dpsi.transpose();
            block_matrix3.noalias() = factor1 * dphi_test * dpsi.transpose();
            block_matrix4.noalias() = factor1 * dpsi_test * dphi.transpose();

            const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);

            double norm = 0.;
            for (auto component = Eigen::Index{}; component < felt_space_dimension.Get(); ++component)
                norm += NumericNS::Square(tau_interpolate(component));

            if (!(NumericNS::IsZero(norm)))
            {
                ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> tau_sigma;
                tau_sigma.noalias() = tau_interpolate * tau_interpolate.transpose();
                tau_sigma /= norm; // if put on above line memory allocation incurred...

                dphi_test_sigma.noalias() = dphi_test * tau_sigma;
                dphi_test_sigma *= (factor3 - factor1);
                block_matrix1.noalias() += dphi_test_sigma * dphi.transpose();
                block_matrix3.noalias() += dphi_test_sigma * dpsi.transpose();

                dpsi_test_sigma.noalias() = dpsi_test * tau_sigma;
                dpsi_test_sigma *= (factor3 - factor1);
                block_matrix4.noalias() += dpsi_test_sigma * dphi.transpose();

                dpsi_test_sigma.noalias() = dpsi_test * tau_sigma;
                dpsi_test_sigma *= (factor3 - factor1 + factor4 - factor2);
                block_matrix2.noalias() += dpsi_test_sigma * dpsi.transpose();
            }

            {
                auto row_slicing = Eigen::seqN(0, Nnode_for_test_unknown1.Get());
                auto col_slicing = Eigen::seqN(0, Nnode_for_unknown1.Get());
                matrix_result(row_slicing, col_slicing) += block_matrix1;
            }

            {
                auto row_slicing = Eigen::seqN(Nnode_for_test_unknown1.Get(), Nnode_for_test_unknown2.Get());
                auto col_slicing = Eigen::seqN(Nnode_for_unknown1.Get(), Nnode_for_unknown2.Get());
                matrix_result(row_slicing, col_slicing) += block_matrix2;
            }

            {
                auto row_slicing = Eigen::seqN(Nnode_for_test_unknown1.Get(), Nnode_for_test_unknown2.Get());
                auto col_slicing = Eigen::seqN(0, Nnode_for_unknown1.Get());
                matrix_result(row_slicing, col_slicing) += block_matrix4;
            }

            {
                auto row_slicing = Eigen::seqN(0, Nnode_for_test_unknown1.Get());
                auto col_slicing = Eigen::seqN(Nnode_for_unknown1.Get(), Nnode_for_unknown2.Get());
                matrix_result(row_slicing, col_slicing) += block_matrix3;
            }
        }
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto Bidomain<TimeManagerT, TimeDependencyT>::GetIntracelluarTransDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return intracellular_trans_diffusion_tensor_;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto Bidomain<TimeManagerT, TimeDependencyT>::GetExtracelluarTransDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return extracellular_trans_diffusion_tensor_;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto Bidomain<TimeManagerT, TimeDependencyT>::GetIntracelluarFiberDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return intracellular_fiber_diffusion_tensor_;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto Bidomain<TimeManagerT, TimeDependencyT>::GetExtracelluarFiberDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return extracellular_fiber_diffusion_tensor_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto Bidomain<TimeManagerT, TimeDependencyT>::GetFibers() const noexcept -> const vectorial_fiber_type&
    {
        return fibers_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_BIDOMAIN_DOT_HXX_
// *** MoReFEM end header guards *** < //
