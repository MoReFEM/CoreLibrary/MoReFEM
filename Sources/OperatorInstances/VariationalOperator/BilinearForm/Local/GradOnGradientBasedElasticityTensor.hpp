// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <type_traits>

#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::ParameterNS
{
    enum class GradientBasedElasticityTensorConfiguration;
}


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Implementation of elastic operator.
     *
     * \todo Improve the comment by writing its mathematical definition!
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class GradOnGradientBasedElasticityTensor final
    : public BilinearLocalVariationalOperator<Eigen::MatrixXd>,
      public Crtp::LocalMatrixStorage<GradOnGradientBasedElasticityTensor<TimeManagerT>, 3UL, Eigen::MatrixXd>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GradOnGradientBasedElasticityTensor<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to parent.
        using parent = BilinearLocalVariationalOperator<Eigen::MatrixXd>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Type of matricial parameters (gradient-based elasticity tensor).
        using matrix_parameter_type = Parameter<ParameterNS::Type::matrix,
                                                LocalCoords,
                                                TimeManagerT,
                                                ParameterNS::TimeDependencyNS::None,
                                                ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<9>>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 3UL, Eigen::MatrixXd>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type (STL vector)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * one vectorial unknown.
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         *
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] young_modulus Young modulus of the solid.
         * \param[in] poisson_ratio Poisson coefficient of the solid.
         * \param[in] configuration Whether we consider 2D/plane_strain, 2D/plane_stress operator or 3D
         * operator.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit GradOnGradientBasedElasticityTensor(
            const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
            elementary_data_type&& elementary_data,
            const scalar_parameter_type& young_modulus,
            const scalar_parameter_type& poisson_ratio,
            const ParameterNS::GradientBasedElasticityTensorConfiguration configuration);

        //! Destructor.
        virtual ~GradOnGradientBasedElasticityTensor() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        GradOnGradientBasedElasticityTensor(const GradOnGradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GradOnGradientBasedElasticityTensor(GradOnGradientBasedElasticityTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GradOnGradientBasedElasticityTensor& operator=(const GradOnGradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GradOnGradientBasedElasticityTensor& operator=(GradOnGradientBasedElasticityTensor&& rhs) = delete;

        ///@}


        /*!
         * \brief Compute the elementary \a OperatorNatureT.
         *
         * For current operator, only matrix makes sense; so only this specialization is actually defined.
         *
         */
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        /*!
         * \brief Init the gradient based elasticity tensor depending on the scheme chosen in the input file.
         *
         * \param[in] young_modulus Young's modulus
         * \param[in] poisson_ratio Poisson ratio
         * \param[in] configuration Configuration to use: for instance in 2D whether we're considering
         * plane stress or plane strain.
         */
        void
        InitGradientBasedElasticityTensor(const scalar_parameter_type& young_modulus,
                                          const scalar_parameter_type& poisson_ratio,
                                          const ParameterNS::GradientBasedElasticityTensorConfiguration configuration);

        //! Gradient-based elasticity tensor.
        matrix_parameter_type& GetNonCstGradientBasedElasticityTensor();

      private:
        //! Gradient-based elasticity tensor.
        typename matrix_parameter_type::unique_ptr gradient_based_elasticity_tensor_parameter_ = nullptr;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes of local matrices.
        enum class LocalMatrixIndex : std::size_t { block_contribution = 0, transposed_dphi, three_product_helper };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
