// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Internal/GradOnGradientBasedElasticityTensor.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    GradOnGradientBasedElasticityTensor<TimeManagerT>::GradOnGradientBasedElasticityTensor(
        const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
        elementary_data_type&& a_elementary_data,
        const scalar_parameter_type& young_modulus,
        const scalar_parameter_type& poisson_ratio,
        const ParameterNS::GradientBasedElasticityTensorConfiguration configuration)
    : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent()
    {
        assert(unknown_list.size() == 1);
        assert(test_unknown_list.size() == 1);

        const auto& elementary_data = GetElementaryData();

        gradient_based_elasticity_tensor_parameter_ =
            Internal::LocalVariationalOperatorNS::InitGradientBasedElasticityTensor<TimeManagerT>(
                young_modulus, poisson_ratio, configuration);

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        const auto felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_unknown.Get(), Nnode_for_unknown.Get() },    // block_contribution
            { felt_space_dimension.Get(), Nnode_for_unknown.Get() },      // transposed dphi
            { Nnode_for_test_unknown.Get(), felt_space_dimension.Get() }, // three_product_helper
        } });
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& GradOnGradientBasedElasticityTensor<TimeManagerT>::ClassName()
    {
        static std::string name("GradOnGradientBasedElasticityTensor");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void GradOnGradientBasedElasticityTensor<TimeManagerT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();

        auto& block_contribution =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();
        auto& three_product_helper =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::three_product_helper)>();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        const auto Ncomponent = unknown_ref_felt.Ncomponent();

        assert(Ncomponent == test_unknown_ref_felt.Ncomponent());

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& gradient_based_elasticity_tensor =
                GetNonCstGradientBasedElasticityTensor().GetValue(quad_pt, geom_elt);

            const double factor = quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant() * quad_pt.GetWeight();

            const auto& dphi = quad_pt_unknown_data.GetGradientFEltPhi();

            const auto& dphi_test = test_quad_pt_unknown_data.GetGradientFEltPhi();

            for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
            {
                const auto row_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);
                auto row_slicing = Eigen::seqN(row_component.Get() * Ncomponent.Get(), Ncomponent.Get());

                for (::MoReFEM::GeometryNS::dimension_type col_component{}; col_component < Ncomponent; ++col_component)
                {
                    const auto col_first_index = unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                    auto col_slicing = Eigen::seqN(col_component.Get() * Ncomponent.Get(), Ncomponent.Get());
                    ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> gradient_based_block =
                        gradient_based_elasticity_tensor(row_slicing, col_slicing);

                    three_product_helper.noalias() = dphi_test * gradient_based_block;
                    block_contribution.noalias() = factor * three_product_helper * dphi.transpose();

                    for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_for_test_unknown; ++row_node)
                    {
                        for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode_for_unknown; ++col_node)
                            matrix_result(row_first_index + row_node.Get(), col_first_index + col_node.Get()) +=
                                block_contribution(row_node.Get(), col_node.Get());
                    }
                }
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto GradOnGradientBasedElasticityTensor<TimeManagerT>::GetNonCstGradientBasedElasticityTensor()
        -> matrix_parameter_type&
    {
        assert(!(!gradient_based_elasticity_tensor_parameter_));
        return *gradient_based_elasticity_tensor_parameter_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
