// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <memory> // IWYU pragma: keep
#include <string>
#include <utility>

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiGradPhi.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForCpp.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    GradPhiGradPhi::GradPhiGradPhi(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                   const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                   elementary_data_type&& a_elementary_data)
    : parent(extended_unknown_list, test_extended_unknown_list, std::move(a_elementary_data))
    {
        assert(extended_unknown_list.size() == 1);
        assert(test_extended_unknown_list.size() == 1);

        const auto& elementary_data = parent::GetElementaryData();

        const auto& unknown = parent::GetNthUnknown(0);
        const auto& test_unknown = parent::GetNthTestUnknown(0);

        const auto& ref_felt = elementary_data.GetRefFElt(unknown);
        const auto& test_ref_felt = elementary_data.GetTestRefFElt(test_unknown);

        const auto Nnode_for_unknown = ref_felt.Nnode();
        const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

#ifndef NDEBUG
        {
            const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();
            assert(!infos_at_quad_pt_list.empty());
            const auto& last_dphi = infos_at_quad_pt_list.back().GetUnknownData().GetGradientFEltPhi();
            assert(last_dphi.rows() == Nnode_for_unknown.Get());
        }
#endif // NDEBUG

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_unknown.Get(), Nnode_for_unknown.Get() } // block matrix
        } });
    }


    GradPhiGradPhi::~GradPhiGradPhi() = default;


    const std::string& GradPhiGradPhi::ClassName()
    {
        static const std::string name("GradPhiGradPhi");
        return name;
    }


    void GradPhiGradPhi::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = parent::GetNonCstElementaryData();

        const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

        // Current operator yields in fact a diagonal per block matrix where each block is the same.
        auto& block_matrix =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix)>();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.setZero();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();


        const auto Nnode_for_unknown = ref_felt.Nnode();
        const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

        const auto Ncomponent = ref_felt.Ncomponent();
        assert(Ncomponent == test_ref_felt.Ncomponent());

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            block_matrix.setZero();

            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            // First compute the content of the block matrix.
            const double factor = infos_at_quad_pt.GetQuadraturePoint().GetWeight()
                                  * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            const auto& dphi = ref_felt.ExtractSubMatrix(quad_pt_unknown_data.GetGradientFEltPhi());

            const auto& dphi_test = test_ref_felt.ExtractSubMatrix(test_quad_pt_unknown_data.GetGradientFEltPhi());

            assert(dphi.rows() == Nnode_for_unknown.Get());
            assert(dphi_test.rows() == Nnode_for_test_unknown.Get());

            block_matrix.noalias() = factor * dphi_test * dphi.transpose();

            // Then report it into the elementary matrix.
            for (auto test_node_index = LocalNodeNS::index_type{}; test_node_index < Nnode_for_test_unknown;
                 ++test_node_index)
            {
                for (auto node_index = LocalNodeNS::index_type{}; node_index < Nnode_for_unknown; ++node_index)
                {
                    const auto m = test_node_index.Get();
                    const auto n = node_index.Get();

                    const double value = block_matrix(m, n);

                    for (auto deriv_component = ::MoReFEM::GeometryNS::dimension_type{}; deriv_component < Ncomponent;
                         ++deriv_component)
                    {
                        const auto shift = Nnode_for_test_unknown.Get() * deriv_component.Get();
                        matrix_result(m + shift, n + shift) += value;
                    }
                }
            }
        }
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
