// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_INTERNAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_INTERNAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/LocalCoords.hpp"

#include "Parameters/Internal/Alias.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::ParameterNS { enum class GradientBasedElasticityTensorConfiguration; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    /*!
     * \brief Init the gradient based elasticity tensor depending on the configuration chosen in the input
     * settings.
     *
     * \param[in] young_modulus Young's modulus
     * \param[in] poisson_ratio Poisson ratio
     * \param[in] configuration Configuration to use: for instance in 2D whether we're considering
     * plane stress or plane strain.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class YoungModulusTypeT,
        class PoissonRatioTypeT
    >
    typename Parameter
    <
        ParameterNS::Type::matrix,
        LocalCoords,
        TimeManagerT,
        ::MoReFEM::ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<9>
    >::unique_ptr
    // clang-format on
    InitGradientBasedElasticityTensor(
        const YoungModulusTypeT& young_modulus,
        const PoissonRatioTypeT& poisson_ratio,
        const ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration configuration);


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Internal/GradOnGradientBasedElasticityTensor.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_INTERNAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
