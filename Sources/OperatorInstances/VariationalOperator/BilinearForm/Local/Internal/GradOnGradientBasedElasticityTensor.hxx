// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_INTERNAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_INTERNAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/Internal/GradOnGradientBasedElasticityTensor.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstdlib>
#include <memory>

#include "Geometry/Domain/Domain.hpp"

#include "Parameters/Internal/Alias.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp" // IWYU pragma: keep
#include "ParameterInstances/GradientBasedElasticityTensor/GradientBasedElasticityTensor.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    // clang-format off
    template
    <
       TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
       class YoungModulusTypeT,
       class PoissonRatioTypeT
    >
    typename Parameter
    <
        ParameterNS::Type::matrix,
        LocalCoords,
        TimeManagerT,
        ::MoReFEM::ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<9>
    >::unique_ptr
    // clang-format on
    InitGradientBasedElasticityTensor(
        const YoungModulusTypeT& young_modulus,
        const PoissonRatioTypeT& poisson_ratio,
        const ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration configuration)
    {
        using Configuration = ParameterNS::GradientBasedElasticityTensorConfiguration;

        switch (configuration)
        {
        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim1:
        {
            using parameter_type =
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<TimeManagerT, Configuration::dim1>;
            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
        }
        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain:
        {
            using parameter_type =
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<TimeManagerT, Configuration::dim2_plane_strain>;
            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
        }
        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress:
        {
            using parameter_type =
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<TimeManagerT, Configuration::dim2_plane_stress>;
            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
        }
        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim3:
        {
            using parameter_type =
                ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<TimeManagerT, Configuration::dim3>;
            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
        }
        }

        assert(false && "All configuration cases should have been dealt with above.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_INTERNAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
