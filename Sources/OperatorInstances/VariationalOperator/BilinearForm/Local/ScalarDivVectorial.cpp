// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>
#include <utility>

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/ScalarDivVectorial.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForCpp.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    ScalarDivVectorial::ScalarDivVectorial(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                           const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                           elementary_data_type&& a_elementary_data,
                                           double alpha,
                                           double beta)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : BilinearLocalVariationalOperator(extended_unknown_list, test_extended_unknown_list, std::move(a_elementary_data)),
      alpha_(alpha), beta_(beta)
    {
#ifndef NDEBUG
        {
            const auto& unknown_storage = this->GetExtendedUnknownList();
            assert(unknown_storage.size() == 2UL
                   && "Namesake GlobalVariationalOperator should provide the correct one.");
            assert(!(!unknown_storage.front()));
            assert("Namesake GlobalVariationalOperator should provide two unknowns, the first being vectorial"
                   && unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
            assert(!(!unknown_storage.back()));
            assert("Namesake GlobalVariationalOperator should provide two unknowns, the second being scalar"
                   && unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);

            const auto& test_unknown_storage = this->GetExtendedTestUnknownList();
            assert(test_unknown_storage.size() == 2UL
                   && "Namesake GlobalVariationalOperator should provide the correct one.");
            assert(!(!test_unknown_storage.front()));
            assert("Namesake GlobalVariationalOperator should provide two test unknowns, the first being vectorial"
                   && test_unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
            assert(!(!test_unknown_storage.back()));
            assert("Namesake GlobalVariationalOperator should provide two test unknowns, the second being scalar"
                   && test_unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
        }
#endif // NDEBUG
    }


    ScalarDivVectorial::~ScalarDivVectorial() = default;


    const std::string& ScalarDivVectorial::ClassName()
    {
        static const std::string name("ScalarDivVectorial");
        return name;
    }


    void ScalarDivVectorial::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();

        const auto& scalar_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::scalar)));
        const auto& vectorial_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));

        const auto& scalar_test_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::scalar)));
        const auto& vectorial_test_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));

        const auto Nnode_velocity = vectorial_ref_felt.Nnode();
        const auto Ndof_velocity = vectorial_ref_felt.Ndof();
        const auto Nnode_pressure = scalar_ref_felt.Nnode();

        const auto Nnode_test_velocity = vectorial_test_ref_felt.Nnode();
        const auto Ndof_test_velocity = vectorial_test_ref_felt.Ndof();
        const auto Nnode_test_pressure = scalar_test_ref_felt.Nnode();

        const auto Ncomponent = vectorial_ref_felt.Ncomponent();

        assert(Ncomponent == vectorial_test_ref_felt.Ncomponent());

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const double alpha = GetAlpha();
        const double beta = GetBeta();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& felt_phi = quad_pt_unknown_data.GetFEltPhi();                  // on (v p)
            const auto& gradient_felt_phi = quad_pt_unknown_data.GetGradientFEltPhi(); // on (v p)

            const auto& test_felt_phi = test_quad_pt_unknown_data.GetFEltPhi();                  // on (v* p*)
            const auto& test_gradient_felt_phi = test_quad_pt_unknown_data.GetGradientFEltPhi(); // on (v* p*)

            const auto& pressure_felt_phi = scalar_ref_felt.ExtractSubVector(felt_phi); // only on p

            const auto& test_pressure_felt_phi = scalar_test_ref_felt.ExtractSubVector(test_felt_phi); // only on p*

            const double factor = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            // Part in (v* p)
            for (auto node_test_velocity_index = LocalNodeNS::index_type{};
                 node_test_velocity_index < Nnode_test_velocity;
                 ++node_test_velocity_index)
            {
                for (auto node_pressure_index = LocalNodeNS::index_type{}; node_pressure_index < Nnode_pressure;
                     ++node_pressure_index)
                {
                    const auto dof_pressure = node_pressure_index.Get();

                    // The two terms below give the same result but in a different manner. It is just here to show how
                    // to use them.
                    // const auto test_pressure_term = test_felt_phi(Nnode_test_velocity + node_test_pressure_index);
                    const auto pressure_term = pressure_felt_phi(node_pressure_index.Get());

                    for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent; ++component)
                    {
                        const auto dof_test_velocity_index =
                            node_test_velocity_index.Get() + Nnode_test_velocity.Get() * component.Get();

                        const double product =
                            factor * pressure_term
                            * test_gradient_felt_phi(node_test_velocity_index.Get(), component.Get());

                        assert(dof_test_velocity_index < matrix_result.rows());
                        assert(Ndof_velocity + dof_pressure < matrix_result.cols());
                        matrix_result(dof_test_velocity_index, Ndof_velocity + dof_pressure) += beta * product;
                    }
                }
            }

            // Part in (p* v)
            for (auto node_test_pressure_index = LocalNodeNS::index_type{};
                 node_test_pressure_index < Nnode_test_pressure;
                 ++node_test_pressure_index)
            {
                const auto dof_test_pressure = node_test_pressure_index.Get();

                // The two terms below give the same result but in a different manner. It is just here to show how to
                // use them. const auto pressure_term = felt_phi(Nnode_velocity + node_pressure_index);
                const auto test_pressure_term = test_pressure_felt_phi(node_test_pressure_index.Get());

                for (auto node_velocity_index = LocalNodeNS::index_type{}; node_velocity_index < Nnode_velocity;
                     ++node_velocity_index)
                {
                    auto dof_velocity_index = node_velocity_index.Get();

                    for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent;
                         ++component, dof_velocity_index += Nnode_velocity.Get())
                    {
                        const double product =
                            factor * test_pressure_term * gradient_felt_phi(node_velocity_index.Get(), component.Get());

                        assert(Ndof_test_velocity + dof_test_pressure < matrix_result.rows());
                        assert(dof_velocity_index < matrix_result.cols());

                        matrix_result(Ndof_test_velocity + dof_test_pressure, dof_velocity_index) += alpha * product;
                    }
                }
            }
        }
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
