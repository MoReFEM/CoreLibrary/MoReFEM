// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SCALARDIVVECTORIAL_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SCALARDIVVECTORIAL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <type_traits>

#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{

    /*!
     * \class doxygen_hide_scalar_div_vectorial_formula
     *
     * \f$ \alpha \cdot p' \cdot div \ u + \beta \cdot p \cdot div \ u' \f$
     *
     * where:
     *
     * - \f$p\f$ is the scalar unknown.
     * - \f$p'\f$ is the associated test function.
     * - \f$u\f$ is the vectorial unknown.
     * - \f$u'\f$ is the associated test function.
     * - \f$\alpha\f$ and \f$\beta\f$ are fixed coefficients.
     */


    /*!
     * \brief Implementation at local level of Scalar div(Vectorial) operator.
     *
     * The operator actually computes:
     *
     * \copydetails doxygen_hide_scalar_div_vectorial_formula
     *
     */
    class ScalarDivVectorial final : public BilinearLocalVariationalOperator<Eigen::MatrixXd>
    {

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<ScalarDivVectorial>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to parent.
        using parent = BilinearLocalVariationalOperator<Eigen::MatrixXd>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_scalar_div_vectorial_formula
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and  vectors that will perform the calculations.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         *
         * \param[in] alpha See its meaning in the formula above.
         * \param[in] beta See its meaning in the formula above.
         *
         */
        explicit ScalarDivVectorial(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                    const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                    elementary_data_type&& elementary_data,
                                    double alpha,
                                    double beta);

        //! Destructor.
        virtual ~ScalarDivVectorial() override;

        //! \copydoc doxygen_hide_copy_constructor
        ScalarDivVectorial(const ScalarDivVectorial& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ScalarDivVectorial(ScalarDivVectorial&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ScalarDivVectorial& operator=(const ScalarDivVectorial& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ScalarDivVectorial& operator=(ScalarDivVectorial&& rhs) = delete;


        ///@}


        /*!
         * \brief Compute the elementary matrix.
         *
         * Structure of the elementary matrix (with dimension = 3 and respectively (m, n) dofs for velocity
         * and pressure):
         *
         *    vx1, vx2, ..., vxm, vy1, vy2, ..., vym, vz1, vz2, ..., vzm, p1, p2, ..., pn
         * (                                                                               ) // Same ordering
         * for rows. (                                                                               ) ( ) ( )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         *
         * This operator fills only the mixed terms (v, p) and (p, v); the matrix is symmetric.
         *
         */
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        //! Access to internal attribute \a alpha_.
        double GetAlpha() const noexcept;

        //! Access to internal attribute \a beta_.
        double GetBeta() const noexcept;


      private:
        //! Convenient enum to tag each of the two \a Unknowns considered.
        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex : std::size_t { vectorial = 0, scalar = 1 };

        //! Convenient enum to tag each of the two test \a Unknowns considered.
        enum class TestUnknownIndex { vectorial = 0, scalar = 1 };

        /*!
         * \brief \f$ \alpha \f$ term in the following formula:
         *
         *  \copydetails doxygen_hide_scalar_div_vectorial_formula
         */
        const double alpha_;

        /*!
         * \brief \f$ \beta \f$ term in the following formula:
         *
         *  \copydetails doxygen_hide_scalar_div_vectorial_formula
         */
        const double beta_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/ScalarDivVectorial.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SCALARDIVVECTORIAL_DOT_HPP_
// *** MoReFEM end header guards *** < //
