// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_STOKES_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_STOKES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{

    /*!
     * \brief Local operator for \a Stokes.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class Stokes final : public BilinearLocalVariationalOperator<Eigen::MatrixXd>,
                         public Crtp::LocalMatrixStorage<Stokes<TimeManagerT>, 2UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Stokes<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 2UL>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] fluid_viscosity Viscosity of the fluid.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit Stokes(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                        const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                        elementary_data_type&& elementary_data,
                        const scalar_parameter_type& fluid_viscosity);


        //! Destructor.
        virtual ~Stokes() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        Stokes(const Stokes& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Stokes(Stokes&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Stokes& operator=(const Stokes& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Stokes& operator=(Stokes&& rhs) = delete;

        ///@}


        /*!
         * \brief Compute the elementary matrix.
         *
         * Structure of the elementary matrix (with dimension = 3 and respectively (m, n) dofs for velocity
         * and pressure):
         *
         *    vx1, vx2, ..., vxm, vy1, vy2, ..., vym, vz1, vz2, ..., vzm, p1, p2, ..., pn
         * (                                                                               ) // Same ordering
         * for rows. (                                                                               ) ( ) ( )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         * (                                                                               )
         *
         *
         */
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        //! Get the viscosity of the fluid.
        const scalar_parameter_type& GetFluidViscosity() const;

      private:
        //! \name Material parameters.
        ///@{

        //! Viscosity of the fluid.
        const scalar_parameter_type& fluid_viscosity_;

        ///@}

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes of local matrices.
        enum class LocalMatrixIndex : std::size_t { velocity_block_matrix = 0, transposed_dphi_velocity };

        ///@}


      private:
        //! Convenient enum to tag each of the two \a Unknowns considered.
        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex : std::size_t { vectorial = 0, scalar = 1 };

        //! Convenient enum to tag each of the two test \a Unknowns considered.
        enum class TestUnknownIndex { vectorial = 0, scalar = 1 };
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Stokes.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_STOKES_DOT_HPP_
// *** MoReFEM end header guards *** < //
