// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_STOKES_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_STOKES_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/Stokes.hpp"
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    Stokes<TimeManagerT>::Stokes(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                 const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                 elementary_data_type&& a_elementary_data,
                                 const scalar_parameter_type& fluid_viscosity)
    : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent(),
      fluid_viscosity_(fluid_viscosity)
    {

#ifndef NDEBUG
        {
            const auto& unknown_storage = this->GetExtendedUnknownList();
            assert(unknown_storage.size() == 2UL
                   && "Namesake GlobalVariationalOperator should provide the correct one.");
            assert(!(!unknown_storage.front()));
            assert("Namesake GlobalVariationalOperator should provide two unknowns, the first being vectorial"
                   && unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
            assert(!(!unknown_storage.back()));
            assert("Namesake GlobalVariationalOperator should provide two unknowns, the second being scalar"
                   && unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);

            const auto& test_unknown_storage = this->GetExtendedTestUnknownList();
            assert(test_unknown_storage.size() == 2UL
                   && "Namesake GlobalVariationalOperator should provide the correct one.");
            assert(!(!test_unknown_storage.front()));
            assert("Namesake GlobalVariationalOperator should provide two test unknowns, the first being vectorial"
                   && test_unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
            assert(!(!test_unknown_storage.back()));
            assert("Namesake GlobalVariationalOperator should provide two test unknowns, the second being scalar"
                   && test_unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
        }
#endif // NDEBUG

        const auto& elementary_data = GetElementaryData();
        const auto& velocity_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));
        const auto Nnode_for_velocity = velocity_ref_felt.Nnode().Get();

        const auto& test_velocity_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));
        const auto Nnode_for_test_velocity = test_velocity_ref_felt.Nnode().Get();

        const auto felt_space_dimension = velocity_ref_felt.GetFEltSpaceDimension().Get();

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_velocity, Nnode_for_velocity }, // velocity_block_matrix
            { felt_space_dimension, Nnode_for_velocity }     // transposed_dphi_velocity_block
        } });
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& Stokes<TimeManagerT>::ClassName()
    {
        static std::string name("Stokes");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void Stokes<TimeManagerT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.setZero();

        const auto& scalar_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::scalar)));
        const auto& vectorial_ref_felt =
            elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));

        const auto& scalar_test_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::scalar)));
        const auto& vectorial_test_ref_felt =
            elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));

        const auto Nnode_velocity = vectorial_ref_felt.Nnode();
        const auto Ndof_velocity = vectorial_ref_felt.Ndof();
        const auto Nnode_pressure = scalar_ref_felt.Nnode();

        const auto Nnode_test_velocity = vectorial_test_ref_felt.Nnode();
        const auto Nnode_test_pressure = scalar_test_ref_felt.Nnode();
        const auto Ndof_test_velocity = vectorial_test_ref_felt.Ndof();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        auto& velocity_block_matrix =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::velocity_block_matrix)>();
        // < Used to store factor * dPhi * transp(dPhi_test).

        const auto& fluid_viscosity = GetFluidViscosity();
        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto Ncomponent = vectorial_ref_felt.Ncomponent();
        assert(Ncomponent == vectorial_test_ref_felt.Ncomponent());

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double factor = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant()
                                  * fluid_viscosity.GetValue(quad_pt, geom_elt);


            // First the scalar div vectorial contribution, which should yield a result similar to operator
            // ScalarDivVectorial.
            {
                const auto& felt_phi = quad_pt_unknown_data.GetFEltPhi();
                const auto& test_felt_phi = test_quad_pt_unknown_data.GetFEltPhi();

                const auto& gradient_felt_phi = quad_pt_unknown_data.GetGradientFEltPhi();
                const auto& test_gradient_felt_phi = test_quad_pt_unknown_data.GetGradientFEltPhi();

                const auto& pressure_felt_phi = scalar_ref_felt.ExtractSubVector(felt_phi);                // only on p
                const auto& test_pressure_felt_phi = scalar_test_ref_felt.ExtractSubVector(test_felt_phi); // only on p*

                // Part in (v* p)
                for (auto node_test_velocity_index = LocalNodeNS::index_type{};
                     node_test_velocity_index < Nnode_test_velocity;
                     ++node_test_velocity_index)
                {
                    for (auto node_pressure_index = LocalNodeNS::index_type{}; node_pressure_index < Nnode_pressure;
                         ++node_pressure_index)
                    {
                        const auto dof_pressure = node_pressure_index.Get();

                        // The two terms below give the same result but in a different manner.
                        // It is just here to show how to use them.
                        // const auto test_pressure_term =
                        //   test_felt_phi(Nnode_test_velocity + node_test_pressure_index);
                        const auto pressure_term = pressure_felt_phi(node_pressure_index.Get());

                        for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent; ++component)
                        {
                            auto dof_test_velocity_index =
                                node_test_velocity_index.Get() + Nnode_test_velocity.Get() * component.Get();

                            const double product =
                                factor * pressure_term
                                * test_gradient_felt_phi(node_test_velocity_index.Get(), component.Get());

                            assert(dof_test_velocity_index < matrix_result.rows());
                            assert(Ndof_velocity + dof_pressure < matrix_result.cols());
                            matrix_result(dof_test_velocity_index, Ndof_velocity + dof_pressure) -= product;
                        }
                    }
                }

                // Part in (p* v)
                for (auto node_test_pressure_index = LocalNodeNS::index_type{};
                     node_test_pressure_index < Nnode_test_pressure;
                     ++node_test_pressure_index)
                {
                    const auto dof_test_pressure = node_test_pressure_index.Get();

                    // The two terms below give the same result but in a different manner. It is just here to show
                    // how to use them.
                    // const auto pressure_term = felt_phi(Nnode_velocity + node_pressure_index);
                    const auto test_pressure_term = test_pressure_felt_phi(node_test_pressure_index.Get());

                    for (auto node_velocity_index = LocalNodeNS::index_type{}; node_velocity_index < Nnode_velocity;
                         ++node_velocity_index)
                    {
                        auto dof_velocity_index = node_velocity_index.Get();

                        for (auto component = ::MoReFEM::GeometryNS::dimension_type{}; component < Ncomponent;
                             ++component, dof_velocity_index += Nnode_velocity.Get())
                        {
                            const double product = factor * test_pressure_term
                                                   * gradient_felt_phi(node_velocity_index.Get(), component.Get());

                            assert(Ndof_test_velocity + dof_test_pressure < matrix_result.rows());
                            assert(dof_velocity_index < matrix_result.cols());

                            matrix_result(Ndof_test_velocity + dof_test_pressure, dof_velocity_index) -= product;
                        }
                    }
                }
            }

            // Then here we perform grad-grad operator on the velocity-velocity block.
            {
                Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

                const auto& dphi_velocity =
                    vectorial_ref_felt.ExtractSubMatrix(quad_pt_unknown_data.GetGradientFEltPhi());

                const auto& dphi_test_velocity =
                    vectorial_test_ref_felt.ExtractSubMatrix(test_quad_pt_unknown_data.GetGradientFEltPhi());

                velocity_block_matrix.setZero();

                // First compute the content of the block matrix.
                assert(dphi_velocity.rows() == Nnode_velocity.Get());
                assert(dphi_velocity.cols() == Ncomponent.Get());

                assert(dphi_test_velocity.rows() == Nnode_test_velocity.Get());
                assert(dphi_test_velocity.cols() == Ncomponent.Get());

                velocity_block_matrix.noalias() = factor * dphi_test_velocity * dphi_velocity.transpose();

                // Then report it into the elementary matrix.
                for (auto m = LocalNodeNS::index_type{}; m < Nnode_test_velocity; ++m)
                {
                    for (auto n = LocalNodeNS::index_type{}; n < Nnode_velocity; ++n)
                    {
                        const double value = velocity_block_matrix(m.Get(), n.Get());

                        for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent; ++component)
                        {
                            const auto shift_row = Nnode_test_velocity.Get() * component.Get();
                            const auto shift_col = Nnode_velocity.Get() * component.Get();
                            matrix_result(m.Get() + shift_row, n.Get() + shift_col) += value;
                        }
                    }
                }
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto Stokes<TimeManagerT>::GetFluidViscosity() const -> const scalar_parameter_type&
    {
        return fluid_viscosity_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_STOKES_DOT_HXX_
// *** MoReFEM end header guards *** < //
