// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SURFACICBIDOMAIN_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SURFACICBIDOMAIN_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hpp"
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM {

    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    class FiberList;


} // namespace MoReFEM

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    SurfacicBidomain<TimeManagerT, TimeDependencyT>::SurfacicBidomain(
        const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
        elementary_data_type&& a_elementary_data,
        const scalar_parameter_type& intracellular_trans_diffusion_tensor,
        const scalar_parameter_type& extracellular_trans_diffusion_tensor,
        const scalar_parameter_type& intracellular_fiber_diffusion_tensor,
        const scalar_parameter_type& extracellular_fiber_diffusion_tensor,
        const scalar_parameter_type& heterogeneous_conductivity_coefficient,
        const vectorial_fiber_type& fibers,
        const scalar_fiber_type& angles)
    : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
      matrix_parent(), vector_parent(), intracellular_trans_diffusion_tensor_(intracellular_trans_diffusion_tensor),
      extracellular_trans_diffusion_tensor_(extracellular_trans_diffusion_tensor),
      intracellular_fiber_diffusion_tensor_(intracellular_fiber_diffusion_tensor),
      extracellular_fiber_diffusion_tensor_(extracellular_fiber_diffusion_tensor),
      heterogeneous_conductivity_coefficient_(heterogeneous_conductivity_coefficient), fibers_(fibers), angles_(angles)
    {
        const auto& elementary_data = GetElementaryData();

        const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode().Get();

        const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
        const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode().Get();

        const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown1 = test_unknown1_ref_felt.Nnode().Get();

        const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
        const auto Nnode_for_test_unknown2 = test_unknown2_ref_felt.Nnode().Get();

        const auto ref_felt_space_dimension = unknown1_ref_felt.GetFEltSpaceDimension().Get();

        const auto mesh_dimension = unknown1_ref_felt.GetMeshDimension().Get();

        assert(ref_felt_space_dimension < mesh_dimension && "This operator is a surfacic operator.");
        assert((ref_felt_space_dimension + 1 == mesh_dimension) && "This operator is a surfacic operator.");

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_unknown1, Nnode_for_unknown1 },       // block matrix 1 (Vm,Vm)
            { Nnode_for_test_unknown2, Nnode_for_unknown2 },       // block matrix 2 (Ue,Ue)
            { Nnode_for_test_unknown1, Nnode_for_unknown2 },       // block matrix 3 (Vm, Ue)
            { Nnode_for_test_unknown2, Nnode_for_unknown1 },       // block matrix 4 (Ue, Vm)
            { Nnode_for_test_unknown1, ref_felt_space_dimension }, // dPhi_test*sigma
            { Nnode_for_test_unknown2, ref_felt_space_dimension }, // dPsi_test*sigma
            { Nnode_for_test_unknown1, ref_felt_space_dimension }, // dPhi_test*contravariant_metric_tensor
            { Nnode_for_test_unknown2, ref_felt_space_dimension }, // dPsi_test*contravariant_metric_tensor
        } });


        this->InitLocalVectorStorage({ {
            mesh_dimension,           // tau_interpolate_ortho
            ref_felt_space_dimension, // tau_covariant_basis
            ref_felt_space_dimension  // tau_ortho_covariant_basis
        } });
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    const std::string& SurfacicBidomain<TimeManagerT, TimeDependencyT>::ClassName()
    {
        static std::string name("SurfacicBidomain");
        return name;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetContravariantBasis() const noexcept
        -> const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>&
    {
        return contravariant_basis_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on.
    auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetNonCstContravariantBasis() noexcept
    -> ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>&
    {
        return const_cast<::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>&>(GetContravariantBasis());
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on.
    auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetReducedContravariantMetricTensor() const noexcept
    -> const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<2>&
    {
        return reduced_contravariant_metric_tensor_;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on.
    auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetNonCstReducedContravariantMetricTensor() noexcept
    -> ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<2>&
    {
        return const_cast<::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<2>&>(GetReducedContravariantMetricTensor());
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    void SurfacicBidomain<TimeManagerT, TimeDependencyT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.setZero();

        const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));

        const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));

        const auto mesh_dimension = unknown1_ref_felt.GetMeshDimension().Get();

        auto& block_matrix1 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix1)>();
        auto& block_matrix2 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix2)>();
        auto& block_matrix3 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix3)>();
        auto& block_matrix4 =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix4)>();
        auto& dphi_test_sigma =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
        auto& dpsi_test_sigma =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_test_sigma)>();
        auto& dphi_test_contravariant_metric_tensor = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::dphi_test_contravariant_metric_tensor)>();
        auto& dpsi_test_contravariant_metric_tensor = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::dpsi_test_contravariant_metric_tensor)>();

        auto& tau_interpolate_ortho =
            vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_interpolate_ortho)>();
        auto& tau_covariant_basis =
            vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_covariant_basis)>();
        auto& tau_ortho_covariant_basis =
            vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_ortho_covariant_basis)>();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode();
        const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode();

        assert(unknown1_ref_felt.Ncomponent().Get() == 1UL && "SurfacicBidomain operator limited to scalar unknowns.");
        assert(unknown2_ref_felt.Ncomponent().Get() == 1UL && "SurfacicBidomain operator limited to scalar unknowns.");

        const auto Nnode_for_test_unknown1 = test_unknown1_ref_felt.Nnode();
        const auto Nnode_for_test_unknown2 = test_unknown2_ref_felt.Nnode();

        assert(test_unknown1_ref_felt.Ncomponent().Get() == 1UL
               && "SurfacicBidomain operator limited to scalar unknowns.");
        assert(test_unknown2_ref_felt.Ncomponent().Get() == 1UL
               && "SurfacicBidomain operator limited to scalar unknowns.");

        const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
        const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
        const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
        const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();
        const auto& heterogeneous_conductivity_coefficient = GetHeterogeneousConductivityCoefficient();

        auto& fibers = GetFibers();
        auto& angles = GetAngles();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto felt_space_dimension = unknown1_ref_felt.GetFEltSpaceDimension().Get();


        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            ComputeContravariantBasis(quad_pt_unknown_data);

            const double factor = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            const double factor1 = factor * intracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);

            const double factor2 = factor * extracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);

            const double factor3 = factor * intracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

            const double factor4 = factor * extracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

            const double heterogeneous_conductivity_coefficient_at_quad =
                heterogeneous_conductivity_coefficient.GetValue(quad_pt, geom_elt);

            const auto& grad_felt_phi_for_unknown = quad_pt_unknown_data.GetGradientRefFEltPhi();

            const auto& dphi = unknown1_ref_felt.ExtractSubMatrix(grad_felt_phi_for_unknown);

            const auto& dpsi = unknown2_ref_felt.ExtractSubMatrix(grad_felt_phi_for_unknown);

            const auto& grad_felt_phi_for_test_unknown = test_quad_pt_unknown_data.GetGradientRefFEltPhi();

            const auto& dphi_test = test_unknown1_ref_felt.ExtractSubMatrix(grad_felt_phi_for_test_unknown);

            const auto& dpsi_test = test_unknown2_ref_felt.ExtractSubMatrix(grad_felt_phi_for_test_unknown);

            assert(dphi.rows() == Nnode_for_unknown1.Get());
            assert(dpsi.rows() == Nnode_for_unknown2.Get());
            assert(dphi_test.rows() == Nnode_for_test_unknown1.Get());
            assert(dpsi_test.rows() == Nnode_for_test_unknown2.Get());

            block_matrix1.setZero();
            block_matrix2.setZero();
            block_matrix3.setZero();
            block_matrix4.setZero();

            decltype(auto) reduced_contravariant_metric_tensor = GetReducedContravariantMetricTensor();

            dphi_test_contravariant_metric_tensor.noalias() = dphi_test * reduced_contravariant_metric_tensor;
            dphi_test_contravariant_metric_tensor *= factor1;

            block_matrix1.noalias() = dphi_test_contravariant_metric_tensor * dphi.transpose();


            dpsi_test_contravariant_metric_tensor.noalias() = dpsi_test * reduced_contravariant_metric_tensor;
            dpsi_test_contravariant_metric_tensor *= (factor1 + factor2);

            block_matrix2.noalias() = dpsi_test_contravariant_metric_tensor * dpsi.transpose();

            dphi_test_contravariant_metric_tensor.noalias() = dphi_test * reduced_contravariant_metric_tensor;
            dphi_test_contravariant_metric_tensor *= factor1;

            block_matrix3.noalias() = dphi_test_contravariant_metric_tensor * dpsi.transpose();

            dpsi_test_contravariant_metric_tensor.noalias() = dpsi_test * reduced_contravariant_metric_tensor;
            dpsi_test_contravariant_metric_tensor *= factor1;

            block_matrix4.noalias() = dpsi_test_contravariant_metric_tensor * dphi.transpose();

            // Fiber part of the operator.
            const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);

            double norm = 0.;
            for (int component = 0; component < mesh_dimension; ++component)
                norm += tau_interpolate(component) * tau_interpolate(component);

            ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> tau_sigma;
            ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> tau_ortho_sigma;
            tau_sigma.setZero();
            tau_ortho_sigma.setZero();

            if (!(NumericNS::IsZero(norm)))
            {
                decltype(auto) contravariant_basis = GetContravariantBasis();

                // tau_interpolate_ortho
                tau_interpolate_ortho.setZero();
                tau_interpolate_ortho(0) =
                    tau_interpolate(1) * contravariant_basis(2, 2) - tau_interpolate(2) * contravariant_basis(1, 2);
                tau_interpolate_ortho(1) =
                    tau_interpolate(2) * contravariant_basis(0, 2) - tau_interpolate(0) * contravariant_basis(2, 2);
                tau_interpolate_ortho(2) =
                    tau_interpolate(0) * contravariant_basis(1, 2) - tau_interpolate(1) * contravariant_basis(0, 2);

                tau_covariant_basis.setZero();
                tau_ortho_covariant_basis.setZero();

                // Projection in the covariant basis.
                for (int component = 0; component < mesh_dimension; ++component)
                {
                    for (int cov_component = 0; cov_component < felt_space_dimension; ++cov_component)
                    {
                        tau_covariant_basis(cov_component) +=
                            tau_interpolate(component) * contravariant_basis(component, cov_component);
                        tau_ortho_covariant_basis(cov_component) +=
                            tau_interpolate_ortho(component) * contravariant_basis(component, cov_component);
                    }
                }

                tau_sigma.noalias() = tau_covariant_basis * tau_covariant_basis.transpose();
                tau_ortho_sigma.noalias() = tau_ortho_covariant_basis * tau_ortho_covariant_basis.transpose();

                const auto& theta = angles.GetValue(quad_pt, geom_elt);

                double i_theta = 0.;

                if (std::fabs(theta) < NumericNS::DefaultEpsilon<double>())
                    i_theta = 1.;
                else
                    i_theta = 0.5 + std::sin(2. * theta) / (4. * theta);

                tau_ortho_sigma *= (1. - i_theta);

                tau_sigma *= i_theta / norm;

                tau_sigma += tau_ortho_sigma / norm;

                dphi_test_sigma.noalias() = dphi_test * tau_sigma;
                dphi_test_sigma *= (factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad;

                block_matrix1.noalias() += dphi_test_sigma * dphi.transpose();

                dphi_test_sigma.noalias() = dphi_test * tau_sigma;
                dphi_test_sigma *= heterogeneous_conductivity_coefficient_at_quad * (factor3 - factor1);

                block_matrix3.noalias() += dphi_test_sigma * dpsi.transpose();

                dpsi_test_sigma.noalias() = dpsi_test * tau_sigma;
                dpsi_test_sigma *= (factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad;

                block_matrix4.noalias() += dpsi_test_sigma * dphi.transpose();

                dpsi_test_sigma.noalias() = dpsi_test * tau_sigma;
                dpsi_test_sigma *=
                    (factor3 - factor1 + factor4 - factor2) * heterogeneous_conductivity_coefficient_at_quad;

                block_matrix2.noalias() += dpsi_test_sigma * dpsi.transpose();
            }

            // Then report it into the elementary matrix.
            for (auto node_test_unknown1_index = Eigen::Index{};
                 node_test_unknown1_index < Nnode_for_test_unknown1.Get();
                 ++node_test_unknown1_index)
            {
                for (auto node_unknown1_index = Eigen::Index{}; node_unknown1_index < Nnode_for_unknown1.Get();
                     ++node_unknown1_index)
                {
                    const double value1 = block_matrix1(node_test_unknown1_index, node_unknown1_index);

                    matrix_result(node_test_unknown1_index, node_unknown1_index) += value1;
                }
            }

            for (auto node_test_unknown2_index = Eigen::Index{};
                 node_test_unknown2_index < Nnode_for_test_unknown2.Get();
                 ++node_test_unknown2_index)
            {
                for (auto node_unknown2_index = Eigen::Index{}; node_unknown2_index < Nnode_for_unknown2.Get();
                     ++node_unknown2_index)
                {
                    const double value2 = block_matrix2(node_test_unknown2_index, node_unknown2_index);

                    matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1.Get(),
                                  node_unknown2_index + Nnode_for_unknown1.Get()) += value2;
                }
            }

            for (auto node_test_unknown2_index = Eigen::Index{};
                 node_test_unknown2_index < Nnode_for_test_unknown2.Get();
                 ++node_test_unknown2_index)
            {
                for (auto node_unknown1_index = Eigen::Index{}; node_unknown1_index < Nnode_for_unknown1.Get();
                     ++node_unknown1_index)
                {
                    const double value4 = block_matrix4(node_test_unknown2_index, node_unknown1_index);

                    matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1.Get(), node_unknown1_index) +=
                        value4;
                }
            }

            for (auto node_test_unknown1_index = Eigen::Index{};
                 node_test_unknown1_index < Nnode_for_test_unknown1.Get();
                 ++node_test_unknown1_index)
            {
                for (auto node_unknown2_index = Eigen::Index{}; node_unknown2_index < Nnode_for_unknown2.Get();
                     ++node_unknown2_index)
                {
                    const double value3 = block_matrix3(node_test_unknown1_index, node_unknown2_index);

                    matrix_result(node_test_unknown1_index, node_unknown2_index + Nnode_for_unknown1.Get()) += value3;
                }
            }
        }
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    void SurfacicBidomain<TimeManagerT, TimeDependencyT>::ComputeContravariantBasis(
        const Advanced::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data)
    {
        auto& elementary_data = GetNonCstElementaryData();

        const auto mesh_dimension = quad_pt_unknown_data.GetMeshDimension();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto& dphi_geo = quad_pt_unknown_data.GetGradientRefGeometricPhi();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ geom_elt.GetDimension() };

        const auto Nshape_function = dphi_geo.rows();

        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> covariant_basis(mesh_dimension.Get(), mesh_dimension.Get());

        covariant_basis.setZero();

        for (auto shape_fct_index = Eigen::Index{}; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

            for (::MoReFEM::GeometryNS::dimension_type component_shape_function{};
                 component_shape_function < Ncomponent;
                 ++component_shape_function)
            {
                for (::MoReFEM::GeometryNS::dimension_type coord_index{}; coord_index < mesh_dimension; ++coord_index)
                {
                    covariant_basis(coord_index.Get(), component_shape_function.Get()) +=
                        coords_in_geom_elt[coord_index] * dphi_geo(shape_fct_index, component_shape_function.Get());
                }
            }
        }

        double norm = 0.;

        const auto mesh_dimension_minus_one = mesh_dimension.Get() - 1;


        // Normal computation.
        switch (mesh_dimension.Get())
        {
        case 2:
            covariant_basis(0, mesh_dimension_minus_one) = covariant_basis(1, 0);
            covariant_basis(1, mesh_dimension_minus_one) = -covariant_basis(0, 0);
            norm = std::sqrt(covariant_basis(0, mesh_dimension_minus_one) * covariant_basis(0, mesh_dimension_minus_one)
                             + covariant_basis(1, mesh_dimension_minus_one)
                                   * covariant_basis(1, mesh_dimension_minus_one));
            covariant_basis(0, mesh_dimension_minus_one) /= norm;
            covariant_basis(1, mesh_dimension_minus_one) /= norm;
            break;
        case 3:
            covariant_basis(0, mesh_dimension_minus_one) =
                covariant_basis(1, 0) * covariant_basis(2, 1) - covariant_basis(2, 0) * covariant_basis(1, 1);
            covariant_basis(1, mesh_dimension_minus_one) =
                covariant_basis(2, 0) * covariant_basis(0, 1) - covariant_basis(0, 0) * covariant_basis(2, 1);
            covariant_basis(2, mesh_dimension_minus_one) =
                covariant_basis(0, 0) * covariant_basis(1, 1) - covariant_basis(1, 0) * covariant_basis(0, 1);
            norm = std::sqrt(
                covariant_basis(0, mesh_dimension_minus_one) * covariant_basis(0, mesh_dimension_minus_one)
                + covariant_basis(1, mesh_dimension_minus_one) * covariant_basis(1, mesh_dimension_minus_one)
                + covariant_basis(2, mesh_dimension_minus_one) * covariant_basis(2, mesh_dimension_minus_one));
            covariant_basis(0, mesh_dimension_minus_one) /= norm;
            covariant_basis(1, mesh_dimension_minus_one) /= norm;
            covariant_basis(2, mesh_dimension_minus_one) /= norm;
            break;
        }


        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> covariant_metric_tensor;
        covariant_metric_tensor.noalias() = covariant_basis.transpose() * covariant_basis;

        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> contravariant_metric_tensor;

        contravariant_metric_tensor.noalias() = covariant_metric_tensor.inverse();

        const auto Ncomponent_value = Ncomponent.Get();

        GetNonCstReducedContravariantMetricTensor() =
            contravariant_metric_tensor(Eigen::seqN(0, Ncomponent_value), Eigen::seqN(0, Ncomponent_value));

        decltype(auto) contravariant_basis = GetNonCstContravariantBasis();
        contravariant_basis.noalias() = covariant_basis * contravariant_metric_tensor;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetIntracelluarTransDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return intracellular_trans_diffusion_tensor_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetExtracelluarTransDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return extracellular_trans_diffusion_tensor_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetIntracelluarFiberDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return intracellular_fiber_diffusion_tensor_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetExtracelluarFiberDiffusionTensor() const noexcept
        -> const scalar_parameter_type&
    {
        return extracellular_fiber_diffusion_tensor_;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto
    SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetHeterogeneousConductivityCoefficient() const noexcept
        -> const scalar_parameter_type&
    {
        return heterogeneous_conductivity_coefficient_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetFibers() const noexcept
        -> const vectorial_fiber_type&
    {
        return fibers_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto SurfacicBidomain<TimeManagerT, TimeDependencyT>::GetAngles() const noexcept -> const scalar_fiber_type&
    {
        return angles_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SURFACICBIDOMAIN_DOT_HXX_
// *** MoReFEM end header guards *** < //
