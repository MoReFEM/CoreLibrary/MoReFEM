// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>
#include <type_traits> // IWYU pragma: keep

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    Mass::Mass(const FEltSpace& felt_space,
               const Unknown::const_shared_ptr& unknown_ptr,
               const Unknown::const_shared_ptr& test_unknown_ptr,
               const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::no,
             DoComputeProcessorWiseLocal2Global::no)
    { }


    const std::string& Mass::ClassName()
    {
        static const std::string name("Mass");
        return name;
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
