### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP_INSTANCES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/GradPhiGradPhi.cpp
		${CMAKE_CURRENT_LIST_DIR}/Mass.cpp
		${CMAKE_CURRENT_LIST_DIR}/ScalarDivVectorial.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Ale.hpp
		${CMAKE_CURRENT_LIST_DIR}/Ale.hxx
		${CMAKE_CURRENT_LIST_DIR}/Bidomain.hpp
		${CMAKE_CURRENT_LIST_DIR}/Bidomain.hxx
		${CMAKE_CURRENT_LIST_DIR}/FwdForCpp.hpp
		${CMAKE_CURRENT_LIST_DIR}/FwdForHpp.hpp
		${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.hpp
		${CMAKE_CURRENT_LIST_DIR}/GradOnGradientBasedElasticityTensor.hxx
		${CMAKE_CURRENT_LIST_DIR}/GradPhiGradPhi.hpp
		${CMAKE_CURRENT_LIST_DIR}/GradPhiGradPhi.hxx
		${CMAKE_CURRENT_LIST_DIR}/Mass.hpp
		${CMAKE_CURRENT_LIST_DIR}/Mass.hxx
		${CMAKE_CURRENT_LIST_DIR}/ScalarDivVectorial.hpp
		${CMAKE_CURRENT_LIST_DIR}/ScalarDivVectorial.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/Stokes.hpp
		${CMAKE_CURRENT_LIST_DIR}/Stokes.hxx
		${CMAKE_CURRENT_LIST_DIR}/SurfacicBidomain.hpp
		${CMAKE_CURRENT_LIST_DIR}/SurfacicBidomain.hxx
		${CMAKE_CURRENT_LIST_DIR}/VariableMass.hpp
		${CMAKE_CURRENT_LIST_DIR}/VariableMass.hxx
)

include(${CMAKE_CURRENT_LIST_DIR}/Local/SourceList.cmake)
