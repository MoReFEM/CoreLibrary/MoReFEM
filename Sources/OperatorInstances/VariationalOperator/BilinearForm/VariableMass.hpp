// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_VARIABLEMASS_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_VARIABLEMASS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp"          // IWYU pragma: export
#include "OperatorInstances/VariationalOperator/BilinearForm/Local/VariableMass.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief Implementation of global mass operator for poromechanics.
     *
     * \todo Improve the comment by writing its mathematical definition!
     * \todo #971 Probably merge it with 'usual' mass operator through a policy.
     */
    template<class ParameterT>
    class VariableMass final
    // clang-format off
        : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            VariableMass<ParameterT>,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::VariableMass<ParameterT>
        >
    // clang-format on
    {

      public:
        //! Self.
        using self = VariableMass<ParameterT>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_operator_type = Advanced::LocalVariationalOperatorNS::VariableMass<ParameterT>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
        using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            self,
            Advanced::OperatorNS::Nature::bilinear,
            local_operator_type
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Unique ptr.
        using const_unique_ptr = std::unique_ptr<const self>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_ptr Unknown onto which the operator works.
         * \copydoc doxygen_hide_test_unknown_ptr_param
         *
         * \copydoc doxygen_hide_gvo_felt_space_arg
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         *
         * \param[in] parameter_factor Spatial factor to consider expressed at quadrature points.
         */
        explicit VariableMass(const FEltSpace& felt_space,
                              const Unknown::const_shared_ptr& unknown_ptr,
                              const Unknown::const_shared_ptr& test_unknown_ptr,
                              const ParameterT& parameter_factor,
                              const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Destructor.
        ~VariableMass() = default;

        //! \copydoc doxygen_hide_move_constructor
        VariableMass(VariableMass&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_constructor
        VariableMass(const VariableMass& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariableMass& operator=(const VariableMass& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariableMass& operator=(VariableMass&& rhs) = delete;

        ///@}


        /*!
         * \brief Assemble into one or several matrices.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
         *
         * \param[in] global_matrix_with_coeff_tuple List of global matrices into which the operator is
         * assembled. These matrices are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_tuple, const Domain& domain = Domain()) const;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/VariableMass.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_VARIABLEMASS_DOT_HPP_
// *** MoReFEM end header guards *** < //
