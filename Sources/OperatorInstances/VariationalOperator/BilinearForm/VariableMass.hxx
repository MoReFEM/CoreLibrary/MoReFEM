// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_VARIABLEMASS_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_VARIABLEMASS_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/VariableMass.hpp"
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class ParameterT>
    VariableMass<ParameterT>::VariableMass(const FEltSpace& felt_space,
                                           const Unknown::const_shared_ptr& unknown_ptr,
                                           const Unknown::const_shared_ptr& test_unknown_ptr,
                                           const ParameterT& parameter_factor,
                                           const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::no,
             DoComputeProcessorWiseLocal2Global::no,
             parameter_factor)
    { }


    template<class ParameterT>
    const std::string& VariableMass<ParameterT>::ClassName()
    {
        static std::string name("VariableMass");
        return name;
    }


    template<class ParameterT>
    template<class LinearAlgebraTupleT>
    inline void VariableMass<ParameterT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                   const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_VARIABLEMASS_DOT_HXX_
// *** MoReFEM end header guards *** < //
