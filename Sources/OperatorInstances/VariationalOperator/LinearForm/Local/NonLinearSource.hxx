// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    NonLinearSource<ReactionLawT>::NonLinearSource(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                                   elementary_data_type&& a_elementary_data,
                                                   reaction_law_type& reaction_law)
    : parent(unknown_list, test_unknown_list, std::move(a_elementary_data)), reaction_law_(reaction_law)
    {
        const auto& elementary_data = GetElementaryData();
        former_local_solution_.resize(elementary_data.NdofRow());
        former_local_solution_.setZero();
    }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    NonLinearSource<ReactionLawT>::~NonLinearSource() = default;


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    const std::string& NonLinearSource<ReactionLawT>::ClassName()
    {
        static std::string name("NonLinearSource");
        return name;
    }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    void NonLinearSource<ReactionLawT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = parent::GetNonCstElementaryData();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& local_solution = GetFormerLocalSolution();

        assert(parent::GetNthUnknown().GetNature() == UnknownNS::Nature::scalar
               && "NonLinearSource is limited to a scalar unknown. It could be extended to a vectorial one but "
                  "not in a generic way.");

        auto& vector_result = elementary_data.GetNonCstVectorResult();

        const auto& test_ref_felt = elementary_data.GetTestRefFElt(parent::GetNthTestUnknown());

        const auto Nnode_test = test_ref_felt.Nnode();

        auto& unknown_interpolate_at_quad_point = GetNonCstReactionLaw().GetNonCstLocalPotential();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& phi_test = test_quad_pt_unknown_data.GetRefFEltPhi();
            const auto& phi = quad_pt_unknown_data.GetRefFEltPhi();

            const auto& quad_pt = test_quad_pt_unknown_data.GetQuadraturePoint();

            const double quad_pt_factor =
                quad_pt.GetWeight() * test_quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            unknown_interpolate_at_quad_point = 0.;

            // This loop interpolates the unknown at the current quad point. It is needed to forward gate.
            for (auto node_index = LocalNodeNS::index_type{}; node_index < Nnode_test; ++node_index)
            {
                unknown_interpolate_at_quad_point += phi(node_index.Get()) * local_solution(node_index.Get());
            }

            const double i_ion =
                GetNonCstReactionLaw().ReactionLawFunction(unknown_interpolate_at_quad_point, quad_pt, geom_elt);

            // This loop integrates the source at the quad point.
            for (auto node_index = LocalNodeNS::index_type{}; node_index < Nnode_test; ++node_index)
            {
                vector_result(node_index.Get()) += quad_pt_factor * phi_test(node_index.Get()) * i_ion;
            }
        }
    }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    inline const typename NonLinearSource<ReactionLawT>::reaction_law_type&
    NonLinearSource<ReactionLawT>::GetReactionLaw() const noexcept
    {
        return reaction_law_;
    }

    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    inline typename NonLinearSource<ReactionLawT>::reaction_law_type&
    NonLinearSource<ReactionLawT>::GetNonCstReactionLaw() noexcept
    {
        return reaction_law_;
    }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    inline auto NonLinearSource<ReactionLawT>::GetFormerLocalSolution() const noexcept -> const Eigen::VectorXd&
    {
        return former_local_solution_;
    }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    inline auto NonLinearSource<ReactionLawT>::GetNonCstFormerLocalSolution() noexcept -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalSolution());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_DOT_HXX_
// *** MoReFEM end header guards *** < //
