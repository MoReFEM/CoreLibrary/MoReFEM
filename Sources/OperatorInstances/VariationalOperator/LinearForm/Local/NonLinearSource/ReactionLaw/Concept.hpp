// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_CONCEPT_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::Concept
{


    /*!
     * \brief Defines a concept to identify a reaction law to pass to \a NonLinearSource operator.
     *
     */
    template<typename T>
    concept ReactionLaw = requires(T obj) {
        T::ConceptIsReactionLaw == true;
        typename T::time_manager_type;
        {
            obj.ReactionLawFunction(0., std::declval<QuadraturePoint>(), std::declval<GeometricElt>())
        } -> std::same_as<double>;
    };


    /*!
     * \class doxygen_hide_concept_reaction_law_alias
     *
     * \brief A keyword to indicate this class is an acceptable choice for concept \a ReactionLaw.
     */


    /*!
     * \class doxygen_hide_reaction_law_template_arg
     *
     * \tparam ReactionLawT The type of reaction law to consider.
     */


} // namespace MoReFEM::Advanced::Concept


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
