// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_COURTEMANCHERAMIREZNATTEL_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_COURTEMANCHERAMIREZNATTEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef>
#include <iosfwd>
#include <limits>
#include <memory>

#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }  // lines 45-45
namespace MoReFEM { class GeometricElt; }  // lines 46-46
namespace MoReFEM { class QuadraturePoint; }  // lines 47-47
namespace MoReFEM { class QuadratureRulePerTopology; }  // lines 48-48
namespace MoReFEM::FilesystemNS { class File; }  // lines 50-50

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class QuadratureRulePerTopology; }

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ReactionLawNS
{


    /*!
     * \brief Implementation of the CourtemancheRamirezNattel reaction law. Defines f and g for a
     * ReactionDiffusion model.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class CourtemancheRamirezNattel
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = CourtemancheRamirezNattel<TimeManagerT>;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the reaction law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_concept_reaction_law_alias
        static inline constexpr bool ConceptIsReactionLaw = true;

        //! Alias to a scalar parameter at quadrature point.
        using ScalarParameterAtQuadPt =
            ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT, ParameterNS::TimeDependencyNS::None
                                       // There is a time dependency but it is much more sophisticated
                                       // that what this template parameter is able to cope with,
                                       // namely only something like f(x) * g(t).
                                       >;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
         *
         * \param[in] domain \a Domain upon which the \a Parameters are defined.
         *
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit CourtemancheRamirezNattel(const MoReFEMDataT& morefem_data,
                                           const Domain& domain,
                                           const QuadratureRulePerTopology& quadrature_rule_per_topology)
            requires(TimeManagerT::IsTimeEvolving() == true);

        //! Destructor.
        ~CourtemancheRamirezNattel() = default;

        //! \copydoc doxygen_hide_copy_constructor
        CourtemancheRamirezNattel(const CourtemancheRamirezNattel& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        CourtemancheRamirezNattel(CourtemancheRamirezNattel&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        CourtemancheRamirezNattel& operator=(const CourtemancheRamirezNattel& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        CourtemancheRamirezNattel& operator=(CourtemancheRamirezNattel&& rhs) = delete;

        ///@}

        /*!
         *
         * \brief Defines f(u,w) = See NonLinearSource
         *
         * \todo #9 (Gautier) Improve this comment!
         *
         * \param[in] local_potential Local potential.
         * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
         * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
         *
         * \return Value of the reaction law at the requested location.
         */
        double
        ReactionLawFunction(const double local_potential, const QuadraturePoint& quad_pt, const GeometricElt& geom_elt);

        //! Accessor to the local potential.
        double GetLocalPotential() const noexcept;

        //! Non constant accessor to the local potential.
        double& GetNonCstLocalPotential() noexcept;

        /*!
         * \brief Write gate into a file.
         *
         * \param[in] filename Path of the file into which gate must be written.
         */
        void WriteGate(const FilesystemNS::File& filename) const;

        //! Non constant accessor the the gate \a Parameter. We decided to put the m parameter in it.
        ScalarParameterAtQuadPt& GetNonCstGate() noexcept;

        //! Accessor the the gate \a Parameter.
        const ScalarParameterAtQuadPt& GetGate() const noexcept;

      private:
        /*!
         * \brief Compute the concentrations.
         *
         * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
         * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
         */
        void ComputeConcentrations(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt);

      private:
        //! Accessor to the \a TimeManager of the model.
        const TimeManagerT& GetTimeManager() const noexcept;

      private:
        /*!
         * \brief Convenient enum to pinpoint quantities in the \a parameter_list_ array.
         *
         * \todo #9 Explain meaning of each of those! (Gautier)
         */
        enum class parameter_index : std::size_t {
            m = 0,
            h = 1,
            j = 2,
            ao = 3,
            io = 4,
            ua = 5,
            ui = 6,
            xr = 7,
            xs = 8,
            d = 9,
            f = 10,
            fca = 11,
            urel = 12,
            vrel = 13,
            wrel = 14,
            nai = 15,
            nao = 16,
            cao = 17,
            ki = 18,
            ko = 19,
            cai = 20,
            naiont = 21,
            kiont = 22,
            caiont = 23,
            ileak = 24,
            iup = 25,
            itr = 26,
            irel = 27,
            nsr = 28, /* NSR Ca Concentration (mM) */
            jsr = 29,
        };

      private:
        //! List of all scalar parameters to consider.
        std::array<typename ScalarParameterAtQuadPt::unique_ptr, 30UL> parameter_list_;

        /*!
         * \brief Non constant accessor to the parameter which index is \a index.
         *
         * \param[in] index Index of the sought \a Parameter. To make the code more readable, use the enum
         class:
         * \code
         * decltype(auto) nsr_param = GetNonCstParameter(EnumUnderlyingType(parameter_index::nsr));
         \endcode
         *
         * \return Reference to the required \a Parameter.
         */
        ScalarParameterAtQuadPt& GetNonCstParameter(parameter_index index) noexcept;


        /*!
         * \brief Constant accessor to the parameter which index is \a index.
         *
         * \param[in] index Index of the sought \a Parameter. To make the code more readable, use the enum
         class:
         * \code
         * decltype(auto) nsr_param = GetParameter(EnumUnderlyingType(parameter_index::nsr));
         \endcode
         *
         * \return Constant reference to the required \a Parameter.
         */
        const ScalarParameterAtQuadPt& GetParameter(parameter_index index) const noexcept;

      private:
        //! Time manager.
        const TimeManagerT& time_manager_;

        //! Value of the local potential.
        double local_potential_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::Advanced::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/CourtemancheRamirezNattel.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_COURTEMANCHERAMIREZNATTEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
