// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_FITZHUGHNAGUMO_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_FITZHUGHNAGUMO_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>
#include <limits>
#include <memory>

#include "Core/InputData/Instances/Reaction/FitzHughNagumo.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }  // lines 42-42
namespace MoReFEM { class GeometricElt; }  // lines 43-43
namespace MoReFEM { class QuadraturePoint; }  // lines 44-44
namespace MoReFEM { class QuadratureRulePerTopology; }  // lines 45-45
namespace MoReFEM::FilesystemNS { class File; }  // lines 47-47

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ReactionLawNS
{

    /*!
     * \brief Implementation of the FitzHughNagumo reaction law. Defines f and g for a ReactionDiffusion model.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class FitzHughNagumo
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FitzHughNagumo<TimeManagerT>;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_concept_reaction_law_alias
        static inline constexpr bool ConceptIsReactionLaw = true;

        //! Returns the name of the reaction law.
        static const std::string& ClassName();

      private:
        //! Alias to the matching input datum.
        using input_data_fhn = ::MoReFEM::InputDataNS::ReactionNS::FitzHughNagumo;

        //! Alias to a scalar parameter at quadrature point.
        using ScalarParameterAtQuadPt =
            ParameterAtQuadraturePoint<ParameterNS::Type::scalar,
                                       TimeManagerT,
                                       ParameterNS::TimeDependencyNS::None // There is a time dependency but it
                                                                           // is much more sophisticated
                                       // that what this template parameter is able to cope with,
                                       // namely only something like f(x) * g(t).
                                       >;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
         *
         * \param[in] domain \a Domain upon which the \a Parameters are defined.
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit FitzHughNagumo(const MoReFEMDataT& morefem_data,
                                const Domain& domain,
                                const QuadratureRulePerTopology& quadrature_rule_per_topology)
            requires(TimeManagerT::IsTimeEvolving() == true);

        //! Destructor.
        ~FitzHughNagumo() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FitzHughNagumo(const FitzHughNagumo& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FitzHughNagumo(FitzHughNagumo&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FitzHughNagumo& operator=(const FitzHughNagumo& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FitzHughNagumo& operator=(FitzHughNagumo&& rhs) = delete;

        ///@}

        /*!
         * \brief Defines f(u,w)=u-u^3/3-w. See NonLinearSource.
         *
         * \todo #9 (Gautier) Improve this comment!
         *
         * \param[in] local_potential Local potential.
         * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
         * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
         *
         * \return Value of the reaction law at the requested location.
         */
        double
        ReactionLawFunction(const double local_potential, const QuadraturePoint& quad_pt, const GeometricElt& geom_elt);

        //! Accessor to the local potential.
        double GetLocalPotential() const noexcept;

        //! Non constant accessor to the local potential.
        double& GetNonCstLocalPotential() noexcept;

        /*!
         * \brief Write gate into a file.
         *
         * \param[in] filename Path of the file into which gate must be written.
         */
        void WriteGate(const FilesystemNS::File& filename) const;

      private:
        /*!
         * \class doxygen_hide_reaction_diffusion_gate_law_function_args
         *
         * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
         * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
         * \param[out] gate Computed gate.
         */

        /*!
         * \brief Defines g(u,w)=a(u+b-cw). See NonLinearSource.
         *
         * \copydoc doxygen_hide_reaction_diffusion_gate_law_function_args
         */
        void GateLawFunction(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt, double& gate) const;

        //! Accessor to the \a TimeManager.
        const TimeManagerT& GetTimeManager() const noexcept;

        //! Get A.
        // \todo #9 Improve this comment (Gautier)
        double GetA() const noexcept;

        //! Get B.
        // \todo #9 Improve this comment (Gautier)
        double GetB() const noexcept;

        //! Get C.
        // \todo #9 Improve this comment (Gautier)
        double GetC() const noexcept;

        //! Non constant accessor the the gate \a Parameter.
        ScalarParameterAtQuadPt& GetNonCstGate() noexcept;

        //! Accessor the the gate \a Parameter.
        const ScalarParameterAtQuadPt& GetGate() const noexcept;

      private:
        //! Gate.
        typename ScalarParameterAtQuadPt::unique_ptr gate_ = nullptr;

      private:
        //! \name FitzHughNagumo parameters.
        ///@{

        //! a
        // \todo #9 Improve this comment (Gautier)
        const double a_;

        //! b
        // \todo #9 Improve this comment (Gautier)
        const double b_;

        //! c
        // \todo #9 Improve this comment (Gautier)
        const double c_;

        ///@}

        //! \a TimeManager of the model.
        const TimeManagerT& time_manager_;

        //! Local potential.
        double local_potential_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::Advanced::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/FitzHughNagumo.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_FITZHUGHNAGUMO_DOT_HPP_
// *** MoReFEM end header guards *** < //
