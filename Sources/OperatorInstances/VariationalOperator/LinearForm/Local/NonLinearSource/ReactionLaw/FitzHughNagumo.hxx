// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_FITZHUGHNAGUMO_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_FITZHUGHNAGUMO_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/FitzHughNagumo.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <memory>
#include <string>

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Extract.hpp" // IWYU pragma: export
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ReactionLawNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& FitzHughNagumo<TimeManagerT>::ClassName()
    {
        static std::string name("FitzHughNagumo");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void FitzHughNagumo<TimeManagerT>::GateLawFunction([[maybe_unused]] const QuadraturePoint& quad_pt,
                                                       [[maybe_unused]] const GeometricElt& geom_elt,
                                                       double& gate) const
    {
        const auto& local_potential = GetLocalPotential();
        gate += GetTimeManager().GetTimeStep() * GetA() * (local_potential + GetB() - GetC() * gate);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    double FitzHughNagumo<TimeManagerT>::ReactionLawFunction(const double local_potential,
                                                             const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt)
    {
        auto functor = [&quad_pt, &geom_elt, this](double& gate)
        {
            return GateLawFunction(quad_pt, geom_elt, gate);
        };

        const double new_gate = GetNonCstGate().UpdateAndGetValue(quad_pt, geom_elt, functor);

        return local_potential - NumericNS::Cube(local_potential) / 3. - new_gate;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void FitzHughNagumo<TimeManagerT>::WriteGate(const FilesystemNS::File& filename) const
    {
        GetGate().Write(filename);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    FitzHughNagumo<TimeManagerT>::FitzHughNagumo(const MoReFEMDataT& morefem_data,
                                                 const Domain& domain,
                                                 const QuadratureRulePerTopology& default_quadrature_rule_set)
        requires(TimeManagerT::IsTimeEvolving() == true)
    : a_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_fhn::ACoefficient>(morefem_data)),
      b_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_fhn::BCoefficient>(morefem_data)),
      c_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_fhn::CCoefficient>(morefem_data)),
      time_manager_(morefem_data.GetTimeManager())
    {
        using InitialConditionGate = ::MoReFEM::InputDataNS::InitialConditionGate;

        const double initial_condition_gate =
            ::MoReFEM::InputDataNS::ExtractLeaf<InitialConditionGate::Value>(morefem_data);

        gate_ = std::make_unique<ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT>>(
            "Gate", domain, default_quadrature_rule_set, initial_condition_gate, this->GetTimeManager());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double FitzHughNagumo<TimeManagerT>::GetLocalPotential() const noexcept
    {
        return local_potential_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double& FitzHughNagumo<TimeManagerT>::GetNonCstLocalPotential() noexcept
    {
        return local_potential_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const TimeManagerT& FitzHughNagumo<TimeManagerT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double FitzHughNagumo<TimeManagerT>::GetA() const noexcept
    {
        return a_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double FitzHughNagumo<TimeManagerT>::GetB() const noexcept
    {
        return b_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double FitzHughNagumo<TimeManagerT>::GetC() const noexcept
    {
        return c_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto FitzHughNagumo<TimeManagerT>::GetNonCstGate() noexcept -> ScalarParameterAtQuadPt&
    {
        return const_cast<ScalarParameterAtQuadPt&>(this->GetGate());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto FitzHughNagumo<TimeManagerT>::GetGate() const noexcept -> const ScalarParameterAtQuadPt&
    {
        assert(!(!gate_));
        return *gate_;
    }


} // namespace MoReFEM::Advanced::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_FITZHUGHNAGUMO_DOT_HXX_
// *** MoReFEM end header guards *** < //
