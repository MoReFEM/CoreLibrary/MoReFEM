// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_MITCHELLSCHAEFFER_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_MITCHELLSCHAEFFER_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <iosfwd>
#include <limits>
#include <memory>

#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }  // lines 45-45
namespace MoReFEM { class GeometricElt; }  // lines 46-46
namespace MoReFEM { class QuadraturePoint; }  // lines 47-47
namespace MoReFEM { class QuadratureRulePerTopology; }  // lines 48-48
namespace MoReFEM::FilesystemNS { class File; }  // lines 50-50

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Advanced::ReactionLawNS
{

    /*!
     * \brief Implementation of the MitchellSchaeffer reaction law. Defines f and g for a ReactionDiffusion
     * model.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class MitchellSchaeffer
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MitchellSchaeffer<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;


        //! Returns the name of the reaction law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_concept_reaction_law_alias
        static inline constexpr bool ConceptIsReactionLaw = true;

      private:
        //! Alias to the matching input datum.
        using input_data_ms = ::MoReFEM::InputDataNS::ReactionNS::MitchellSchaeffer;

        //! Alias to a scalar parameter at quadrature point.
        using ScalarParameterAtQuadPt =
            ParameterAtQuadraturePoint<ParameterNS::Type::scalar,
                                       TimeManagerT,
                                       ParameterNS::TimeDependencyNS::None // There is a time dependency but it is much
                                                                           // more sophisticated that what this template
                                                                           // parameter is able to cope with, namely
                                                                           // only something like f(x) * g(t).
                                       >;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
         *
         * \param[in] domain \a Domain upon which the \a Parameters are defined.
         *
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit MitchellSchaeffer(const MoReFEMDataT& morefem_data,
                                   const Domain& domain,
                                   const QuadratureRulePerTopology& quadrature_rule_per_topology)
            requires(TimeManagerT::IsTimeEvolving() == true);

        //! Destructor.
        ~MitchellSchaeffer() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MitchellSchaeffer(const MitchellSchaeffer& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MitchellSchaeffer(MitchellSchaeffer&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MitchellSchaeffer& operator=(const MitchellSchaeffer& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MitchellSchaeffer& operator=(MitchellSchaeffer&& rhs) = delete;

        ///@}

        /*!
         *
         * \brief Defines f(u,w) = w(u-u_min)^2(u_max-u)/(tau_in(u_max-u_min)) - (u-u_min)/(tau_out(u_max -
         * u_min)) See NonLinearSource
         *
         * \todo #9 (Gautier) Improve this comment!
         *
         * \param[in] local_potential Local potential.
         * \param[in] quad_pt \a QuadraturePoint at which the function is evaluated.
         * \param[in] geom_elt \a GeometricElt inside which the function is evaluated.
         *
         * \return Value of the reaction law at the requested location.
         */
        double
        ReactionLawFunction(const double local_potential, const QuadraturePoint& quad_pt, const GeometricElt& geom_elt);

        //! Accessor to the local potential.
        double GetLocalPotential() const noexcept;

        //! Non constant accessor to the local potential.
        double& GetNonCstLocalPotential() noexcept;

        /*!
         * \brief Write gate into a file.
         *
         * \param[in] filename Path of the file into which gate must be written.
         */
        void WriteGate(const FilesystemNS::File& filename) const;

        //! Non constant accessor the the gate \a Parameter.
        ScalarParameterAtQuadPt& GetNonCstGate() noexcept;

        //! Accessor the the gate \a Parameter.
        const ScalarParameterAtQuadPt& GetGate() const noexcept;

      private:
        /*!
         * \brief Defines g = (1/(u_max-u_min)^2 - w)/tau_open if u < u_gate
         *                                       - w/tau_close if u > u_gate
         * See NonLinearSource
         *
         * \copydoc doxygen_hide_reaction_diffusion_gate_law_function_args
         */
        void GateLawFunction(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt, double& gate) const;

        //! Accessor to the \a TimeManager.
        const TimeManagerT& GetTimeManager() const noexcept;

        //! tau_in.
        // .
        // \todo #9 Improve this comment (Gautier)
        double GetTauIn() const noexcept;

        //! Tau out .
        // \todo #9 Improve this comment (Gautier)
        double GetTauOut() const noexcept;

        //! Tau open .
        // \todo #9 Improve this comment (Gautier)
        double GetTauOpen() const noexcept;

        //! Tau close .
        // \todo #9 Improve this comment (Gautier)
        const scalar_parameter_type& GetTauClose() const;

        //! Ugate .
        // \todo #9 Improve this comment (Gautier)
        double GetUGate() const noexcept;

        //! Umin .
        // \todo #9 Improve this comment (Gautier)
        double GetUMin() const noexcept;

        //! Umax .
        // \todo #9 Improve this comment (Gautier)
        double GetUMax() const noexcept;


      private:
        //! Gate.
        typename ScalarParameterAtQuadPt::unique_ptr gate_ = nullptr;

      private:
        //! \name MitchellSchaeffer parameters.
        ///@{

        //! tau_in .
        // \todo #9 Improve this comment (Gautier)
        const double tau_in_;

        //! tau_out_ .
        // \todo #9 Improve this comment (Gautier)
        const double tau_out_;

        //! tau_open_ .
        // \todo #9 Improve this comment (Gautier)
        const double tau_open_;

        //! tau_close_ .
        // \todo #9 Improve this comment (Gautier)
        typename scalar_parameter_type::unique_ptr tau_close_ = nullptr;

        //! u_gate_ .
        // \todo #9 Improve this comment (Gautier)
        const double u_gate_;

        //! u_min_ .
        // \todo #9 Improve this comment (Gautier)
        const double u_min_;

        //! u_max_ .
        // \todo #9 Improve this comment (Gautier)
        const double u_max_;

        ///@}

        //! \a TimeManager of the model.
        const TimeManagerT& time_manager_;

        //! Local potential.
        double local_potential_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::Advanced::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/MitchellSchaeffer.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_MITCHELLSCHAEFFER_DOT_HPP_
// *** MoReFEM end header guards *** < //
