// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_MITCHELLSCHAEFFER_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_MITCHELLSCHAEFFER_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/MitchellSchaeffer.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <memory>
#include <string>

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp" // IWYU pragma: keep
#include "Parameters/ParameterAtQuadraturePoint.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ReactionLawNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& MitchellSchaeffer<TimeManagerT>::ClassName()
    {
        static std::string name("MitchellSchaeffer");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void MitchellSchaeffer<TimeManagerT>::GateLawFunction(const QuadraturePoint& quad_pt,
                                                          const GeometricElt& geom_elt,
                                                          double& gate) const
    {
        const auto& local_potential = GetLocalPotential();

        if (local_potential < GetUGate())
        {
            assert(!NumericNS::IsZero(NumericNS::Square(GetUMax() - GetUMin())));
            assert(!NumericNS::IsZero(GetTauOpen()));

            gate +=
                GetTimeManager().GetTimeStep() * (1. / NumericNS::Square(GetUMax() - GetUMin()) - gate) / GetTauOpen();
        } else
        {
            gate += GetTimeManager().GetTimeStep() * (-gate / GetTauClose().GetValue(quad_pt, geom_elt));
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    double MitchellSchaeffer<TimeManagerT>::ReactionLawFunction(const double local_potential,
                                                                const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt)
    {
        auto functor = [&quad_pt, &geom_elt, this](double& gate)
        {
            return GateLawFunction(quad_pt, geom_elt, gate);
        };

        const double new_gate = GetNonCstGate().UpdateAndGetValue(quad_pt, geom_elt, functor);

        assert(!NumericNS::IsZero(GetTauIn() * (GetUMax() - GetUMin())));
        assert(!NumericNS::IsZero(GetTauOut() * (GetUMax() - GetUMin())));

        return new_gate * NumericNS::Square(local_potential - GetUMin()) * (GetUMax() - local_potential)
                   / (GetTauIn() * (GetUMax() - GetUMin()))
               - (local_potential - GetUMin()) / (GetTauOut() * (GetUMax() - GetUMin()));
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void MitchellSchaeffer<TimeManagerT>::WriteGate(const FilesystemNS::File& filename) const
    {
        GetGate().Write(filename);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    MitchellSchaeffer<TimeManagerT>::MitchellSchaeffer(const MoReFEMDataT& morefem_data,
                                                       const Domain& domain,
                                                       const QuadratureRulePerTopology& default_quadrature_rule_set)
        requires(TimeManagerT::IsTimeEvolving() == true)
    : tau_in_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_ms::TauIn>(morefem_data)),
      tau_out_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_ms::TauOut>(morefem_data)),
      tau_open_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_ms::TauOpen>(morefem_data)),
      u_gate_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_ms::PotentialGate>(morefem_data)),
      u_min_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_ms::PotentialMin>(morefem_data)),
      u_max_(::MoReFEM::InputDataNS::ExtractLeaf<input_data_ms::PotentialMax>(morefem_data)),
      time_manager_(morefem_data.GetTimeManager())
    {
        using InitialConditionGate = ::MoReFEM::InputDataNS::InitialConditionGate;

        const double initial_condition_gate =
            ::MoReFEM::InputDataNS::ExtractLeaf<InitialConditionGate::Value>(morefem_data);

        gate_ = std::make_unique<ScalarParameterAtQuadPt>(
            "Gate", domain, default_quadrature_rule_set, initial_condition_gate, this->GetTimeManager());

        tau_close_ = InitScalarParameterFromInputData<input_data_ms::TauClose>("Tau Close", domain, morefem_data);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double MitchellSchaeffer<TimeManagerT>::GetLocalPotential() const noexcept
    {
        return local_potential_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double& MitchellSchaeffer<TimeManagerT>::GetNonCstLocalPotential() noexcept
    {
        return local_potential_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const TimeManagerT& MitchellSchaeffer<TimeManagerT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double MitchellSchaeffer<TimeManagerT>::GetTauIn() const noexcept
    {
        return tau_in_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double MitchellSchaeffer<TimeManagerT>::GetTauOut() const noexcept
    {
        return tau_out_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double MitchellSchaeffer<TimeManagerT>::GetTauOpen() const noexcept
    {
        return tau_open_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto MitchellSchaeffer<TimeManagerT>::GetTauClose() const -> const scalar_parameter_type&
    {
        assert(!(!tau_close_));
        return *tau_close_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double MitchellSchaeffer<TimeManagerT>::GetUGate() const noexcept
    {
        return u_gate_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double MitchellSchaeffer<TimeManagerT>::GetUMin() const noexcept
    {
        return u_min_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double MitchellSchaeffer<TimeManagerT>::GetUMax() const noexcept
    {
        return u_max_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto MitchellSchaeffer<TimeManagerT>::GetNonCstGate() noexcept -> ScalarParameterAtQuadPt&
    {
        return const_cast<ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT>&>(this->GetGate());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto MitchellSchaeffer<TimeManagerT>::GetGate() const noexcept -> const ScalarParameterAtQuadPt&
    {
        assert(!(!gate_));
        return *gate_;
    }


} // namespace MoReFEM::Advanced::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_REACTIONLAW_MITCHELLSCHAEFFER_DOT_HXX_
// *** MoReFEM end header guards *** < //
