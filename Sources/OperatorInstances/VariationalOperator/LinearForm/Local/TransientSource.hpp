// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_TRANSIENTSOURCE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_TRANSIENTSOURCE_DOT_HPP_
// *** MoReFEM header guards *** < //
//
// #include <memory>
// #include <vector>
//
// #include "Utilities/InputData/InputData.hpp"
//
// #include "Core/Parameter/TypeEnum.hpp"
// #include "Parameters/Parameter.hpp"
//
// #include "Operators/LocalVariationalOperator/LinearLocalVariationalOperator.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Implementation of local TransientSource operator.
     *
     * \todo Improve the comment by writing its mathematical definition!
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    class TransientSource final : public LinearLocalVariationalOperator<Eigen::VectorXd>
    {


      public:
        //! \copydoc doxygen_hide_alias_self
        using self = TransientSource<TypeT, TimeManagerT, TimeDependencyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to parameter type.
        using parameter_type = Parameter<TypeT, LocalCoords, TimeManagerT, TimeDependencyT>;

        //! Alias to parent.
        using parent = LinearLocalVariationalOperator<Eigen::VectorXd>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Returns the name of the operator.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly one vectorial unknown. \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] source Lua function describing the source applied.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit TransientSource(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                 const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                 typename parent::elementary_data_type&& elementary_data,
                                 const parameter_type& source);


        //! Destructor.
        virtual ~TransientSource() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        TransientSource(const TransientSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TransientSource(TransientSource&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TransientSource& operator=(const TransientSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TransientSource& operator=(TransientSource&& rhs) = delete;

        ///@}


        /*!
         * \brief Compute the elementary \a OperatorNatureT.
         *
         * For current operator, only vector makes sense; so only this specialization is actually defined.
         *
         * \internal <b><tt>[internal]</tt></b> This parameter is computed by
         * GlobalVariationalOperatorNS::TransientSource::SetComputeEltArrayArguments() method.
         * \endinternal
         *
         */
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


        //! Set the time.
        //! \param[in] time New time.
        void SetTime(double time) noexcept;


      private:
        /*!
         * \brief Access to the transient source parameter.
         */
        const parameter_type& GetSource() const;


      private:
        //! Source  considered.
        const parameter_type& source_;

        //! Current time in seconds.
        double time_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/LinearForm/Local/TransientSource.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_TRANSIENTSOURCE_DOT_HPP_
// *** MoReFEM end header guards *** < //
