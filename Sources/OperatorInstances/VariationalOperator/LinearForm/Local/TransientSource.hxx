// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_TRANSIENTSOURCE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_TRANSIENTSOURCE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/TransientSource.hpp"
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    TransientSource<TypeT, TimeManagerT, TimeDependencyT>::TransientSource(
        const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
        typename parent::elementary_data_type&& a_elementary_data,
        const parameter_type& source)
    : parent(unknown_list, test_unknown_list, std::move(a_elementary_data)), source_(source)
    {
        assert(unknown_list.size() == 1);
        assert(test_unknown_list.size() == 1);
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    const std::string& TransientSource<TypeT, TimeManagerT, TimeDependencyT>::ClassName()
    {
        static std::string name("TransientSource");
        return name;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    void TransientSource<TypeT, TimeManagerT, TimeDependencyT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = GetNonCstElementaryData();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        auto& vector_result = elementary_data.GetNonCstVectorResult();

        const auto& current_geom_elt = elementary_data.GetCurrentGeomElt();

        const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown());

        const auto Ncomponent = test_ref_felt.Ncomponent();

#ifndef NDEBUG
        {
            if (TypeT == ParameterNS::Type::scalar)
                assert(Ncomponent.Get() == 1);
            else if (TypeT == ParameterNS::Type::vector)
                assert(Ncomponent == elementary_data.GetMeshDimension());
        }
#endif // NDEBUG

        const auto Nnode_test = test_ref_felt.Nnode();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& phi_test = test_quad_pt_unknown_data.GetRefFEltPhi();

            const auto& quad_pt = test_quad_pt_unknown_data.GetQuadraturePoint();

            const double quad_pt_factor =
                quad_pt.GetWeight() * test_quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            const auto& force_value = GetSource().GetValue(quad_pt, current_geom_elt);

            for (auto node_index = LocalNodeNS::index_type{}; node_index < Nnode_test; ++node_index)
            {
                auto dof_index = node_index.Get();

                const auto factor = quad_pt_factor * phi_test(node_index.Get());

                for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent;
                     ++component, dof_index += Nnode_test.Get())
                {
                    if constexpr (TypeT == ParameterNS::Type::scalar)
                        vector_result(dof_index) += factor * force_value;
                    else if constexpr (TypeT == ParameterNS::Type::vector)
                        vector_result(dof_index) += factor * force_value(component.Get());
                }
            }
        }
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    inline const typename TransientSource<TypeT, TimeManagerT, TimeDependencyT>::parameter_type&
    TransientSource<TypeT, TimeManagerT, TimeDependencyT>::GetSource() const
    {
        return source_;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    inline void TransientSource<TypeT, TimeManagerT, TimeDependencyT>::SetTime(double time) noexcept
    {
        time_ = time;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_TRANSIENTSOURCE_DOT_HXX_
// *** MoReFEM end header guards *** < //
