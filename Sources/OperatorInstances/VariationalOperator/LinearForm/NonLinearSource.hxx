// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_NONLINEARSOURCE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_NONLINEARSOURCE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/NonLinearSource.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::GlobalVariationalOperatorNS
{


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    NonLinearSource<ReactionLawT>::NonLinearSource(const FEltSpace& felt_space,
                                                   const Unknown::const_shared_ptr& unknown_ptr,
                                                   reaction_law_type& reaction_law,
                                                   const QuadratureRulePerTopology* const quadrature_rule_per_topology)

    : parent(felt_space,
             unknown_ptr,
             unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::no,
             DoComputeProcessorWiseLocal2Global::yes,
             reaction_law)
    { }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    const std::string& NonLinearSource<ReactionLawT>::ClassName()
    {
        static std::string name("NonLinearSource");
        return name;
    }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    inline void NonLinearSource<ReactionLawT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                        const GlobalVector& input_vector,
                                                        const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
    }


    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    template<class LocalOperatorTypeT>
    inline void NonLinearSource<ReactionLawT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<const GlobalVector&>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments),
                              local_operator.GetNonCstFormerLocalSolution());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_NONLINEARSOURCE_DOT_HXX_
// *** MoReFEM end header guards *** < //
