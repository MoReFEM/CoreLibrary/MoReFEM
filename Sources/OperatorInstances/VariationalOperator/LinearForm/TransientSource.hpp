// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_TRANSIENTSOURCE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_TRANSIENTSOURCE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/TransientSource.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{

    /*!
     * \brief Implementation of global TransientSource operator.
     *
     * \todo Improve the comment by writing its mathematical definition!
     *
     * \tparam TypeT Nature of the source (scalar or vector). It is vector by default only for historic reasons:
     * this operator was introduced to deal with vectorial case.
     * \tparam TimeDependencyT Time dependency of the underlying \a Parameter.
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    class TransientSource final
    : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
    <
        TransientSource<TypeT, TimeManagerT, TimeDependencyT>,
        Advanced::OperatorNS::Nature::linear,
        Advanced::LocalVariationalOperatorNS::TransientSource<TypeT, TimeManagerT, TimeDependencyT>
    >
    // clang-format on
    {

      public:
        static_assert(TypeT != ParameterNS::Type::matrix, "Current implementation of local operator wouldn't comply!");

        //! \copydoc doxygen_hide_alias_self
        using self = TransientSource<TypeT, TimeManagerT, TimeDependencyT>;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_operator_type =
            Advanced::LocalVariationalOperatorNS::TransientSource<TypeT, TimeManagerT, TimeDependencyT>;

        //! Alias to parameter type.
        using parameter_type = typename local_operator_type::parameter_type;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
        using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            self,
            Advanced::OperatorNS::Nature::linear,
            local_operator_type
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] source Parameter describing the force applied.
         * \copydoc doxygen_hide_gvo_felt_space_arg
         * \param[in] unknown_ptr Unknown considered for this operator (might be scalar or vectorial).
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit TransientSource(const FEltSpace& felt_space,
                                 const Unknown::const_shared_ptr& unknown_ptr,
                                 const parameter_type& source,
                                 const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Destructor.
        ~TransientSource() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TransientSource(const TransientSource& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        TransientSource(TransientSource&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TransientSource& operator=(const TransientSource& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        TransientSource& operator=(TransientSource&& rhs) = default;

        ///@}


        /*!
         * \brief Assemble into one or several vectors.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalVectorWithCoefficient objects.
         *
         * \param[in] global_vector_with_coeff_tuple List of global vectors into which the operator is
         * assembled. These vectors are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] time Time in seconds.
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& global_vector_with_coeff_tuple,
                      double time,
                      const Domain& domain = Domain()) const;


      public:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        template<class LocalOperatorTypeT>
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         LocalOperatorTypeT& local_operator,
                                         const std::tuple<double>& additional_arguments) const;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_TRANSIENTSOURCE_DOT_HPP_
// *** MoReFEM end header guards *** < //
