// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_TRANSIENTSOURCE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_TRANSIENTSOURCE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    TransientSource<TypeT, TimeManagerT, TimeDependencyT>::TransientSource(
        const FEltSpace& felt_space,
        const Unknown::const_shared_ptr& unknown_ptr,
        const parameter_type& source,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_ptr,
             unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::no,
             DoComputeProcessorWiseLocal2Global::no,
             source)
    { }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    const std::string& TransientSource<TypeT, TimeManagerT, TimeDependencyT>::ClassName()
    {
        static std::string name("Transient source");
        return name;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    template<class LinearAlgebraTupleT>
    inline void TransientSource<TypeT, TimeManagerT, TimeDependencyT>::Assemble(
        LinearAlgebraTupleT&& global_vector_with_coeff_tuple,
        double time,
        const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(global_vector_with_coeff_tuple), domain, time);
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    template<class LocalOperatorTypeT>
    inline void TransientSource<TypeT, TimeManagerT, TimeDependencyT>::SetComputeEltArrayArguments(
        [[maybe_unused]] const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<double>& additional_arguments) const
    {
        local_operator.SetTime(std::get<0>(additional_arguments));
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_TRANSIENTSOURCE_DOT_HXX_
// *** MoReFEM end header guards *** < //
