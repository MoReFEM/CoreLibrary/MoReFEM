// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_FOLLOWINGPRESSURE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_FOLLOWINGPRESSURE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/FollowingPressure.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief FollowingPressure operator.
     *
     * \tparam TypeT Type of the pressure parameter. Only scalar is possible.
     * \tparam TimeDependencyT Policy of the time dependency of the pressure.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    class FollowingPressure
    final : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
    <
        FollowingPressure<TimeManagerT, TimeDependencyT>,
        Advanced::OperatorNS::Nature::nonlinear,
        Advanced::LocalVariationalOperatorNS::FollowingPressure<TimeManagerT, TimeDependencyT>
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FollowingPressure<TimeManagerT, TimeDependencyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Const Unique ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_operator_type =
            Advanced::LocalVariationalOperatorNS::FollowingPressure<TimeManagerT, TimeDependencyT>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::nonlinear,
                local_operator_type
            >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Alias to the parameter type.
        using parameter_type = Parameter<ParameterNS::Type::scalar, LocalCoords, TimeManagerT, TimeDependencyT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_gvo_felt_space_arg
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         * \param[in] unknown_ptr Vectorial unknown considered.
         * \param[in] test_unknown_ptr Vectorial unknown considered for test function.
         * \param[in] pressure Pressure to apply.
         */
        explicit FollowingPressure(const FEltSpace& felt_space,
                                   const Unknown::const_shared_ptr& unknown_ptr,
                                   const Unknown::const_shared_ptr& test_unknown_ptr,
                                   const parameter_type& pressure,
                                   const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Destructor.
        ~FollowingPressure() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FollowingPressure(const FollowingPressure& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FollowingPressure(FollowingPressure&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FollowingPressure& operator=(const FollowingPressure& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FollowingPressure& operator=(FollowingPressure&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
         * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] previous_iteration_data Value from previous time iteration.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      const GlobalVector& previous_iteration_data,
                      const Domain& domain = Domain()) const;


      public:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        template<class LocalOperatorTypeT>
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         LocalOperatorTypeT& local_operator,
                                         const std::tuple<const GlobalVector&>& additional_arguments) const;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/FollowingPressure.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_FOLLOWINGPRESSURE_DOT_HPP_
// *** MoReFEM end header guards *** < //
