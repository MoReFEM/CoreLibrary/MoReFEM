// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_FOLLOWINGPRESSURE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_FOLLOWINGPRESSURE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/FollowingPressure.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::GlobalVariationalOperatorNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    FollowingPressure<TimeManagerT, TimeDependencyT>::FollowingPressure(
        const FEltSpace& felt_space,
        const Unknown::const_shared_ptr& unknown_ptr,
        const Unknown::const_shared_ptr& test_unknown_ptr,
        const parameter_type& pressure,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             pressure)
    {
        assert(unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_ptr->GetNature() == UnknownNS::Nature::vectorial);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    const std::string& FollowingPressure<TimeManagerT, TimeDependencyT>::ClassName()
    {
        static std::string name("FollowingPressure");
        return name;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    inline void FollowingPressure<TimeManagerT, TimeDependencyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                                           const GlobalVector& input_vector,
                                                                           const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<class LocalOperatorTypeT>
    inline void FollowingPressure<TimeManagerT, TimeDependencyT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<const GlobalVector&>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments),
                              local_operator.GetNonCstFormerLocalDisplacement());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_FOLLOWINGPRESSURE_DOT_HXX_
// *** MoReFEM end header guards *** < //
