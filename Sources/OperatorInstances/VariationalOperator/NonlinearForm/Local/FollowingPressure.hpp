// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FOLLOWINGPRESSURE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FOLLOWINGPRESSURE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

#include "Geometry/Coords/Internal/Factory.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"

#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local FollowingPressure operator.
     *
     * \tparam TypeT Type of the pressure parameter. Only scalar is possible.
     * \tparam TimeDependencyT Policy of the time dependency of the pressure.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    // clang-format on
    class FollowingPressure final
    : public NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>,
      public Crtp::LocalMatrixStorage<FollowingPressure<TimeManagerT, TimeDependencyT>, 2UL>,
      public Crtp::LocalVectorStorage<FollowingPressure<TimeManagerT, TimeDependencyT>, 3UL>
    {

        //            static_assert(TypeT ==  ParameterNS::Type::scalar,
        //                          "Only meaningful for this type.");

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FollowingPressure<TimeManagerT, TimeDependencyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Type of the elementary matrix.
        using matrix_type = Eigen::MatrixXd;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 2UL>;

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 3UL>;

        //! Alias to the parameter type.
        using parameter_type = Parameter<ParameterNS::Type::scalar, LocalCoords, TimeManagerT, TimeDependencyT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and  vectors that will perform the calculations
         * \param[in] pressure \a Parameter which holds static pressure values.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit FollowingPressure(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                   const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                   elementary_data_type&& elementary_data,
                                   const parameter_type& pressure);

        //! Destructor.
        virtual ~FollowingPressure() override;

        //! \copydoc doxygen_hide_copy_constructor
        FollowingPressure(const FollowingPressure& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FollowingPressure(FollowingPressure&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FollowingPressure& operator=(const FollowingPressure& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FollowingPressure& operator=(FollowingPressure&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


        //! Constant accessor to the former local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalDisplacement() const noexcept;

        //! Non constant accessor to the former local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalDisplacement() noexcept;


      private:
        //! Get the applied pressure.
        const parameter_type& GetPressure() const noexcept;

        /*!
         * \brief Non constant accessor to the helper Coords object, useful when Lua function acts upon global
         * elements.
         */
        SpatialPoint& GetNonCstWorkCoords() noexcept;

      private:
        //! \name Parameters.
        ///@{

        //! Scalar parameter pressure.
        const parameter_type& pressure_;

        ///@}


        /*!
         * \brief Displacement obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd former_local_displacement_;

        //! Helper Coords object.
        SpatialPoint work_coords_;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes related to local matrices.
        enum class LocalMatrixIndex : std::size_t { phi_test_dphi_dr = 0, phi_test_dphi_ds };


        //! Indexes related to local vectors.
        enum class LocalVectorIndex : std::size_t { dxdr = 0, dxds, dxdr_cross_dxds };
        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/FollowingPressure.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FOLLOWINGPRESSURE_DOT_HPP_
// *** MoReFEM end header guards *** < //
