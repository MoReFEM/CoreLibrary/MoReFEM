// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FOLLOWINGPRESSURE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FOLLOWINGPRESSURE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/FollowingPressure.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    FollowingPressure<TimeManagerT, TimeDependencyT>::FollowingPressure(
        const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
        elementary_data_type&& a_elementary_data,
        const parameter_type& pressure)
    : NonlinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
      matrix_parent(), vector_parent(), pressure_(pressure)
    {
        const auto& elementary_data = GetElementaryData();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        const auto mesh_dimension = elementary_data.GetMeshDimension();

        assert(mesh_dimension == ::MoReFEM::GeometryNS::dimension_type{ 3 }
               && "Operator restricted to 2D elements in a 3D mesh.");

        assert(unknown_ref_felt.GetFEltSpaceDimension() == ::MoReFEM::GeometryNS::dimension_type{ 2 }
               && "Operator restricted to 2D elements in a 3D mesh.");

        former_local_displacement_.resize(elementary_data.NdofRow());

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_unknown.Get(), Nnode_for_unknown.Get() }, // phi_test_dphi_dr
            { Nnode_for_test_unknown.Get(), Nnode_for_unknown.Get() }  // phi_test_dphi_ds
        } });

        this->vector_parent::InitLocalVectorStorage({ {
            mesh_dimension.Get(), // dxdr
            mesh_dimension.Get(), // dxds
            mesh_dimension.Get()  // dxdr_cross_dxds
        } });
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    FollowingPressure<TimeManagerT, TimeDependencyT>::~FollowingPressure() = default;


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    const std::string& FollowingPressure<TimeManagerT, TimeDependencyT>::ClassName()
    {
        static std::string name("FollowingPressure");
        return name;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    void FollowingPressure<TimeManagerT, TimeDependencyT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        const auto& local_displacement = GetFormerLocalDisplacement();

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        auto& vector_result = elementary_data.GetNonCstVectorResult();
        matrix_result.setZero(); // Is it necessary ?
        vector_result.setZero(); // Is it necessary ?

        auto& phi_test_dphi_dr =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::phi_test_dphi_dr)>();
        auto& phi_test_dphi_ds =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::phi_test_dphi_ds)>();

        auto& dxdr_at_quad_point =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxdr)>();
        auto& dxds_at_quad_point =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxds)>();
        auto& dxdr_cross_dxds_at_quad_point =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dxdr_cross_dxds)>();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

        // Number of component of the unknown.
        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ elementary_data.GetMeshDimension() };

        const auto Nnode_for_unknown = ref_felt.Nnode();
        const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        assert(geom_elt.GetDimension() == ::MoReFEM::GeometryNS::dimension_type{ 2 });

        const auto& pressure = GetPressure();

        auto& coords_in_geom_elt = GetNonCstWorkCoords();


        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& dphi = quad_pt_unknown_data.GetGradientRefFEltPhi();

            const auto& phi_test = test_quad_pt_unknown_data.GetRefFEltPhi();

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double factor = quad_pt.GetWeight() * pressure.GetValue(quad_pt, geom_elt); // Here no jacobian.

            dxdr_at_quad_point.setZero();
            dxds_at_quad_point.setZero();

            // Here the two indices hardcoded 0 and 1 correspond to the two variables of derivation in a 2D element
            // but the vectors are in 3D, hence Ncomponent = 3.
            for (auto node_index = LocalNodeNS::index_type{}; node_index < Nnode_for_unknown; ++node_index)
            {
                auto dof_index = node_index.Get();

                Advanced::GeomEltNS::Local2Global(
                    geom_elt, ref_felt.GetBasicRefFElt().GetLocalNode(node_index).GetLocalCoords(), coords_in_geom_elt);

                for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent;
                     ++component, dof_index += Nnode_for_unknown.Get())
                {
                    const auto local_factor = coords_in_geom_elt[component] + local_displacement(dof_index);

                    dxdr_at_quad_point(component.Get()) += dphi(node_index.Get(), 0) * local_factor;
                    dxds_at_quad_point(component.Get()) += dphi(node_index.Get(), 1) * local_factor;
                }
            }

            // Same comment as previously.
            for (auto test_node_index = LocalNodeNS::index_type{}; test_node_index < Nnode_for_test_unknown;
                 ++test_node_index)
            {
                for (auto node_index = LocalNodeNS::index_type{}; node_index < Nnode_for_unknown; ++node_index)
                {
                    const auto i = test_node_index.Get();
                    const auto j = node_index.Get();

                    phi_test_dphi_dr(i, j) = phi_test(i) * dphi(j, 0);
                    phi_test_dphi_ds(i, j) = phi_test(i) * dphi(j, 1);
                }
            }

            // The matrix is filled by block. Its size is (Nnode*Ncomponent)x(Nnode*Ncomponent).
            // Each block depends on a coordinate of dxdr and dxds.
            for (auto test_node_index = LocalNodeNS::index_type{}; test_node_index < Nnode_for_test_unknown;
                 ++test_node_index)
            {
                for (auto node_index = LocalNodeNS::index_type{}; node_index < Nnode_for_unknown; ++node_index)
                {
                    const auto i = test_node_index.Get();
                    const auto j = node_index.Get();

                    // Block 12
                    matrix_result(i, j + Nnode_for_unknown.Get()) +=
                        factor
                        * (-dxdr_at_quad_point(2) * phi_test_dphi_ds(i, j)
                           - (-dxds_at_quad_point(2)) * phi_test_dphi_dr(i, j));
                    // Block 13
                    matrix_result(i, j + 2 * Nnode_for_unknown.Get()) +=
                        factor
                        * (dxdr_at_quad_point(1) * phi_test_dphi_ds(i, j)
                           - dxds_at_quad_point(1) * phi_test_dphi_dr(i, j));
                    // Block 21
                    matrix_result(i + Nnode_for_test_unknown.Get(), j) +=
                        factor
                        * (dxdr_at_quad_point(2) * phi_test_dphi_ds(i, j)
                           - dxds_at_quad_point(2) * phi_test_dphi_dr(i, j));
                    // Block 23
                    matrix_result(i + Nnode_for_test_unknown.Get(), j + 2 * Nnode_for_unknown.Get()) +=
                        factor
                        * (-dxdr_at_quad_point(0) * phi_test_dphi_ds(i, j)
                           - (-dxds_at_quad_point(0)) * phi_test_dphi_dr(i, j));
                    // Block 31
                    matrix_result(i + 2 * Nnode_for_test_unknown.Get(), j) +=
                        factor
                        * (-dxdr_at_quad_point(1) * phi_test_dphi_ds(i, j)
                           - (-dxds_at_quad_point(1)) * phi_test_dphi_dr(i, j));
                    // Block 32
                    matrix_result(i + 2 * Nnode_for_test_unknown.Get(), j + Nnode_for_unknown.Get()) +=
                        factor
                        * (dxdr_at_quad_point(0) * phi_test_dphi_ds(i, j)
                           - dxds_at_quad_point(0) * phi_test_dphi_dr(i, j));
                }
            }

            // Cross product computation.
            dxdr_cross_dxds_at_quad_point(0) =
                dxdr_at_quad_point(1) * dxds_at_quad_point(2) - dxdr_at_quad_point(2) * dxds_at_quad_point(1);
            dxdr_cross_dxds_at_quad_point(1) =
                dxdr_at_quad_point(2) * dxds_at_quad_point(0) - dxdr_at_quad_point(0) * dxds_at_quad_point(2);
            dxdr_cross_dxds_at_quad_point(2) =
                dxdr_at_quad_point(0) * dxds_at_quad_point(1) - dxdr_at_quad_point(1) * dxds_at_quad_point(0);

            // Integration of the residual.
            for (auto test_node_index = LocalNodeNS::index_type{}; test_node_index < Nnode_for_test_unknown;
                 ++test_node_index)
            {
                auto dof_index = test_node_index.Get();

                const auto value = factor * phi_test(test_node_index.Get());

                for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ncomponent;
                     ++component, dof_index += Nnode_for_test_unknown.Get())
                    vector_result(dof_index) += value * dxdr_cross_dxds_at_quad_point(component.Get());
            }
        }
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const typename FollowingPressure<TimeManagerT, TimeDependencyT>::parameter_type&
    FollowingPressure<TimeManagerT, TimeDependencyT>::GetPressure() const noexcept
    {
        return pressure_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto FollowingPressure<TimeManagerT, TimeDependencyT>::GetFormerLocalDisplacement() const noexcept
        -> const Eigen::VectorXd&
    {
        return former_local_displacement_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto FollowingPressure<TimeManagerT, TimeDependencyT>::GetNonCstFormerLocalDisplacement() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalDisplacement());
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline SpatialPoint& FollowingPressure<TimeManagerT, TimeDependencyT>::GetNonCstWorkCoords() noexcept
    {
        return work_coords_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FOLLOWINGPRESSURE_DOT_HXX_
// *** MoReFEM end header guards *** < //
