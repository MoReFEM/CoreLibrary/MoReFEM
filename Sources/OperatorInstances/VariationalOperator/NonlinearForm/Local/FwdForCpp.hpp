// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FWDFORCPP_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FWDFORCPP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>     // IWYU pragma: export
#include <cmath>       // IWYU pragma: export
#include <type_traits> // IWYU pragma: keep // IWYU pragma: export
#include <vector>      // IWYU pragma: export

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

// IWYU pragma: export
// IWYU pragma: export

#include "Geometry/Mesh/Mesh.hpp" // IWYU pragma: export

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp" // IWYU pragma: export
#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp"     // IWYU pragma: export

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_FWDFORCPP_DOT_HPP_
// *** MoReFEM end header guards *** < //
