// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARMEMBRANE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARMEMBRANE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hpp"
// *** MoReFEM header guards *** < //


#include <vector>



#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    NonlinearMembrane<TimeManagerT>::NonlinearMembrane(
        const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
        elementary_data_type&& a_elementary_data,
        const scalar_parameter_type& youngs_modulus,
        const scalar_parameter_type& poisson_ratio,
        const scalar_parameter_type& thickness,
        const scalar_parameter_type& pretension)
    : NonlinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
      matrix_parent(), vector_parent(), youngs_modulus_(youngs_modulus), poisson_ratio_(poisson_ratio),
      thickness_(thickness), pretension_(pretension)
    {
        const auto& elementary_data = GetElementaryData();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode = unknown_ref_felt.Nnode();


        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_test = test_unknown_ref_felt.Nnode();

#ifndef NDEBUG
        {
            const auto ref_felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

            const auto euclidean_dimension = unknown_ref_felt.GetMeshDimension();

            assert(ref_felt_space_dimension < euclidean_dimension && "This operator is a surfacic operator.");
            assert((ref_felt_space_dimension + ::MoReFEM::GeomEltNS::dimension_type {1}) == euclidean_dimension && "This operator is a surfacic operator.");
        }
#endif // NDEBUG

        // #1840 Use Eigen::VectorXd instead of stD::vector?
        former_local_displacement_.resize(static_cast<std::size_t>(elementary_data.NdofCol()));

        matrix_parent::InitLocalMatrixStorage({ {
            { 3, 3 },              // tangent_tensor
            { 3, 6 },              // De_membrane
            { 6, 3 },              // transposed_De_membrane
            { 6, 3 },              // transposed_De_mult_tangent_tensor
            { 6, 6 },              // tangent_matrix
            { 3, 2 },              // displacement_gradient
            { 3, 2 },              // covariant_basis,
            { 3, 2 },              // contravariant_basis,
            { 2, 3 },              // transposed_covariant_basis,
            { 2, 2 },              // covariant_metric_tensor,
            { 2, 2 },              // contravariant_metric_tensor,
            { 2, 2 },              // gradient_based_block
            { 2, Nnode },          // transposed_dphi
            { Nnode_test, 2 },     // dphi_test_mult_gradient_based_block
            { Nnode_test, Nnode }, // block_contribution
            { 3, 3 },              // invert_generalized_covariant_basis
            { 3, 3 },              // test_pk_in_ref_basis
            { 3, 3 },              // test_gl_in_ref_basis
        } });

        vector_parent::InitLocalVectorStorage({ {
            3, // green-lagrange
            3, // second-PK
            6, // rhs_part
        } });
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& NonlinearMembrane<TimeManagerT>::ClassName()
    {
        static std::string name("NonlinearMembrane");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeEltArray()
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.setZero();

        // Vector related calculation.
        auto& vector_result = elementary_data.GetNonCstVectorResult();
        vector_result.setZero();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto Nnode = unknown_ref_felt.Nnode();

        const auto Nnode_test = test_unknown_ref_felt.Nnode();

        decltype(auto) youngs_modulus = GetYoungsModulus();
        decltype(auto) poisson_ratio = GetPoissonRatio();
        decltype(auto) thickness = GetThickness();
        decltype(auto) pretension = GetPretension();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        constexpr auto Ncomponent = ::MoReFEM::GeomEltNS::dimension_type { 3 };
        constexpr auto Nsurface_comp = Eigen::Index { 2 };

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& dphi = quad_pt_unknown_data.GetGradientRefFEltPhi();

            double determinant;

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double youngs_modulus_value = youngs_modulus.GetValue(quad_pt, geom_elt);
            const double poisson_ratio_value = poisson_ratio.GetValue(quad_pt, geom_elt);
            const double thickness_value = thickness.GetValue(quad_pt, geom_elt);

            const auto pretension_value = pretension.GetValue(quad_pt, geom_elt);

            ComputeContravariantBasis(quad_pt_unknown_data, determinant);
            ComputeDisplacementGradient(quad_pt_unknown_data);
            ComputeGreenLagrange(pretension_value);

            ComputeTangentTensor(youngs_modulus_value, poisson_ratio_value);
            ComputeSecondPiolaKirchhoff();
            ComputeDe();
            ComputeTangentMatrixAndRightHandSide();

            const auto weight_meas = quad_pt.GetWeight() * std::sqrt(determinant) * thickness_value;
            // * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

            const auto& grad_felt_phi_test = test_quad_pt_unknown_data.GetGradientRefFEltPhi();

            const auto& dphi_test = grad_felt_phi_test;

            assert(dphi.rows() == Nnode);
            assert(dphi_test.rows() == Nnode_test);

            if (parent::DoAssembleIntoMatrix())
            {
                auto& tangent_matrix =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();

                auto& gradient_based_block = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                    LocalMatrixIndex::gradient_based_block)>();

                // LocalMatrix dPhi_mult_gradient_based_block(dPhi.rows(), static_cast<int>(Ncomponent));
                // #1840 remove dphi_test_mult_gradient_based_block
                auto& block_contribution =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();

                for (::MoReFEM::GeomEltNS::dimension_type row_component {}; row_component < Ncomponent; ++row_component)
                {
                    const auto row_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);

                    for (::MoReFEM::GeomEltNS::dimension_type col_component {}; col_component < Ncomponent; ++col_component)
                    {
                        const auto col_first_index =
                            test_unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                        Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(
                            tangent_matrix, row_component, col_component, gradient_based_block);

                        block_contribution.noalias() = weight_meas * dphi_test * gradient_based_block * dphi.transpose();

                        for (auto row_node = Eigen::Index {}; row_node < Nnode_test; ++row_node)
                        {
                            assert(row_first_index + row_node < matrix_result.rows());
                            assert(row_node < block_contribution.rows());

                            for (auto col_node = Eigen::Index {}; col_node < Nnode; ++col_node)
                            {
                                assert(col_first_index + col_node < matrix_result.cols());
                                assert(col_node < block_contribution.cols());

                                matrix_result(row_first_index + row_node, col_first_index + col_node) +=
                                    block_contribution(row_node, col_node);
                            }
                        }
                    }
                }
            }

            if (parent::DoAssembleIntoVector())
            {
                auto& rhs_part =
                    vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();

                for (::MoReFEM::GeomEltNS::dimension_type row_component {}; row_component < Ncomponent; ++row_component)
                {
                    const auto dof_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);
                    const auto component_first_index = row_component.Get() * Nsurface_comp;

                    // Compute the new contribution to vector_result here.
                    // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
                    for (auto row_node = Eigen::Index {}; row_node < Nnode_test; ++row_node)
                    {
                        double value = 0.;

                        assert(row_node < dphi.rows());
                        assert(dphi.cols() == Nsurface_comp);

                        for (auto col = Eigen::Index {}; col < Nsurface_comp; ++col)
                        {
                            assert(col + component_first_index < rhs_part.size());
                            value += dphi(row_node, col) * rhs_part(col + component_first_index);
                        }

                        assert(dof_first_index + row_node < vector_result.size());

                        vector_result(dof_first_index + row_node) += value * weight_meas;
                    }
                }
            }

        } // loop over quadrature points
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeContravariantBasis(
        const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
        double& determinant)
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& covariant_basis =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
        covariant_basis.setZero();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();
        const auto& dphi_geo = quad_pt_unknown_data.GetGradientRefGeometricPhi();

        constexpr auto Nsurface_comp = ::MoReFEM::GeomEltNS::dimension_type{ 2 };
        constexpr auto euclidean_dimension = ::MoReFEM::GeomEltNS::dimension_type {3};
        const auto Nshape_function = dphi_geo.rows();

        for (auto shape_fct_index = Eigen::Index {}; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

            for (::MoReFEM::GeomEltNS::dimension_type component_shape_function {}; component_shape_function < Nsurface_comp;
                 ++component_shape_function)
            {
                for (auto coord_index = ::MoReFEM::GeomEltNS::dimension_type {}; coord_index < euclidean_dimension; ++coord_index)
                {
                    covariant_basis(coord_index.Get(), component_shape_function.Get()) +=
                        coords_in_geom_elt[coord_index] * dphi_geo(shape_fct_index, component_shape_function.Get());
                }
            }
        }


        auto& covariant_metric_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
        auto& contravariant_metric_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();

        covariant_metric_tensor.noalias() = covariant_basis.transpose() * covariant_basis;
        
        determinant = covariant_metric_tensor.determinant();
        contravariant_metric_tensor.noalias() = covariant_metric_tensor.inverse();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeDisplacementGradient(
        const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data)
    {
        auto& displacement_gradient =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
        displacement_gradient.setZero();

        auto& local_displacement = GetFormerLocalDisplacement();

        const auto& dphi = quad_pt_unknown_data.GetGradientRefFEltPhi();

        constexpr auto Nsurface_comp = ::MoReFEM::GeomEltNS::dimension_type { 2 };
        constexpr auto euclidean_dimension = ::MoReFEM::GeomEltNS::dimension_type {3};

        const auto Nshape_function = dphi.rows();

        for (auto shape_fct_index = Eigen::Index {}; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            for (::MoReFEM::GeomEltNS::dimension_type component_shape_function {}; component_shape_function < Nsurface_comp;
                 ++component_shape_function)
            {
                for (auto coord_index = ::MoReFEM::GeomEltNS::dimension_type {}; coord_index < euclidean_dimension; ++coord_index)
                {
                    const auto local_displacement_index = Nshape_function * coord_index.Get() + shape_fct_index;
                    assert(static_cast<std::size_t>(local_displacement_index) < local_displacement.size());
                    
                    displacement_gradient(coord_index.Get(), component_shape_function.Get()) +=
                        local_displacement[static_cast<std::size_t>(local_displacement_index)]
                        * dphi(shape_fct_index, component_shape_function.Get());
                }
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeGreenLagrange(const double pretension)
    {
        auto& green_lagrange =
            vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::green_lagrange)>();
        const auto& displacement_gradient =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
        const auto& covariant_basis =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();

        assert(green_lagrange.size() == 3);
        green_lagrange.setZero();

        // Green-Lagrange strain tensor entries
        for (auto i = Eigen::Index {}; i < 3; ++i)
        {
            const auto disp_grad_0 = displacement_gradient(i, 0);
            const auto disp_grad_1 = displacement_gradient(i, 1);
            const auto cov_basis_0 = covariant_basis(i, 0);
            const auto cov_basis_1 = covariant_basis(i, 1);

            green_lagrange(0) += disp_grad_0 * (cov_basis_0 + 0.5 * disp_grad_0);
            green_lagrange(1) += disp_grad_1 * (cov_basis_1 + 0.5 * disp_grad_1);
            green_lagrange(2) += disp_grad_0 * cov_basis_1 + disp_grad_1 * cov_basis_0 + disp_grad_0 * disp_grad_1;
        }

        // PRETENSION

        if (!NumericNS::IsZero(pretension))
        {
            const auto& covariant_metric_tensor =
                matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
            green_lagrange(0) += pretension * covariant_metric_tensor(0, 0);
            green_lagrange(1) += pretension * covariant_metric_tensor(1, 1);
            green_lagrange(2) += 2. * pretension * covariant_metric_tensor(0, 1);
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeTangentTensor(double E, double nu)
    {
        auto& tangent_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();
        const auto& contravariant_metric_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();

        assert(!NumericNS::IsZero(nu));

        const double factor = E / (1. - NumericNS::Square(nu));

        tangent_tensor.setZero();

        const auto contravariant_metric_tensor_0_0 = contravariant_metric_tensor(0, 0);
        const auto contravariant_metric_tensor_0_1 = contravariant_metric_tensor(0, 1);
        const auto contravariant_metric_tensor_1_1 = contravariant_metric_tensor(1, 1);

        // Tangent tensor entries (upper triangular)
        tangent_tensor(0, 0) = factor * NumericNS::Square(contravariant_metric_tensor_0_0);
        tangent_tensor(0, 1) = factor
                               * ((1. - nu) * NumericNS::Square(contravariant_metric_tensor_0_1)
                                  + nu * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_1_1);

        tangent_tensor(0, 2) = factor * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_0_1;
        tangent_tensor(1, 1) = factor * NumericNS::Square(contravariant_metric_tensor_1_1);
        tangent_tensor(1, 2) = factor * contravariant_metric_tensor_1_1 * contravariant_metric_tensor_0_1;
        tangent_tensor(2, 2) = 0.5 * factor
                               * ((1. - nu) * contravariant_metric_tensor_0_0 * contravariant_metric_tensor_1_1
                                  + (1. + nu) * NumericNS::Square(contravariant_metric_tensor_0_1));

        // Symmetry
        tangent_tensor(1, 0) = tangent_tensor(0, 1);
        tangent_tensor(2, 0) = tangent_tensor(0, 2);
        tangent_tensor(2, 1) = tangent_tensor(1, 2);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeSecondPiolaKirchhoff()
    {
        auto& second_PK = vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::second_PK)>();
        const auto& green_lagrange =
            vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::green_lagrange)>();
        const auto& tangent_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();

        second_PK = tangent_tensor * green_lagrange;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeDe()
    {
        auto& De_membrane = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_membrane)>();
        auto& transposed_De_membrane =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_membrane)>();
        const auto& displacement_gradient =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();
        const auto& covariant_basis =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();

        De_membrane.setZero();

        // De/Dgrady matrix entries
        for (auto i = Eigen::Index {}; i < 3; ++i)
        {
            const auto cov_basis_0 = covariant_basis(i, 0);
            const auto cov_basis_1 = covariant_basis(i, 1);
            const auto disp_grad_0 = displacement_gradient(i, 0);
            const auto disp_grad_1 = displacement_gradient(i, 1);

            De_membrane(0, 2 * i) = cov_basis_0 + disp_grad_0;
            De_membrane(1, 2 * i + 1) = cov_basis_1 + disp_grad_1;
            De_membrane(2, 2 * i) = cov_basis_1 + disp_grad_1;
            De_membrane(2, 2 * i + 1) = cov_basis_0 + disp_grad_0;
        }

        // Transposed
        // #1840 Should vanish, but this operator was not written properly (transpose command out of the blue like this
        // should not happen) so I don't modify it for the time being.
        transposed_De_membrane.noalias() = De_membrane.transpose();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void NonlinearMembrane<TimeManagerT>::ComputeTangentMatrixAndRightHandSide()
    {
        auto& tangent_matrix =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
        auto& tangent_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_tensor)>();
        const auto& De_membrane =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_membrane)>();
        const auto& transposed_De_membrane =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_membrane)>();
        auto& transposed_De_mult_tangent_tensor = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::transposed_De_mult_tangent_tensor)>();
        auto& rhs_part = vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();
        const auto& second_PK =
            vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::second_PK)>();

        if (parent::DoAssembleIntoMatrix())
        {
            transposed_De_mult_tangent_tensor.setZero();
            tangent_matrix.setZero();

            // Tangent: Rigidity part
            // #1840 We should get rid of transposed entirely but see earlier comment for current operator
            tangent_matrix.noalias() = transposed_De_membrane * tangent_tensor * De_membrane;

            // Tangent: Geometric part
            for (auto i = Eigen::Index {}; i < 3; ++i)
            {
                const double second_pk_2 = second_PK(2);

                tangent_matrix(2 * i, 2 * i) += second_PK(0);
                tangent_matrix(2 * i + 1, 2 * i + 1) += second_PK(1);
                tangent_matrix(2 * i, 2 * i + 1) += second_pk_2;
                tangent_matrix(2 * i + 1, 2 * i) += second_pk_2;
            }
        }

        if (parent::DoAssembleIntoVector())
        {
            rhs_part.setZero();
            // #1840 Same as above
            rhs_part.noalias() = transposed_De_membrane * second_PK;
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto NonlinearMembrane<TimeManagerT>::GetYoungsModulus() const noexcept -> const scalar_parameter_type&
    {
        return youngs_modulus_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto NonlinearMembrane<TimeManagerT>::GetPoissonRatio() const noexcept -> const scalar_parameter_type&
    {
        return poisson_ratio_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto NonlinearMembrane<TimeManagerT>::GetThickness() const noexcept -> const scalar_parameter_type&
    {
        return thickness_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto NonlinearMembrane<TimeManagerT>::GetPretension() const noexcept -> const scalar_parameter_type&
    {
        return pretension_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const std::vector<double>& NonlinearMembrane<TimeManagerT>::GetFormerLocalDisplacement() const noexcept
    {
        return former_local_displacement_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline std::vector<double>& NonlinearMembrane<TimeManagerT>::GetNonCstFormerLocalDisplacement() noexcept
    {
        return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARMEMBRANE_DOT_HXX_
// *** MoReFEM end header guards *** < //
