// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARMEMBRANE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARMEMBRANE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp" // IWYU pragma: export

// IWYU pragma: export

#include "FiniteElement/Unknown/ExtendedUnknown.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp"           // IWYU pragma: export
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::InfosAtQuadPointNS { class ForUnknownList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local operator for \a NonlinearMembrane.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class NonlinearMembrane final : public NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>,
                                    public Crtp::LocalMatrixStorage<NonlinearMembrane<TimeManagerT>, 15UL>,
                                    public Crtp::LocalVectorStorage<NonlinearMembrane<TimeManagerT>, 3UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = NonlinearMembrane<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 15UL>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 3UL>;

        static_assert(std::is_convertible<self*, vector_parent*>());

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         *
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] youngs_modulus Young's modulus of the solid.
         * \param[in] poisson_ratio Poisson ratio of the solid.
         * \param[in] thickness Thickness of the solid.
         * \param[in] pretension Pretension (related computation are skipped if the underlying value is 0.).
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit NonlinearMembrane(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                   const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                   elementary_data_type&& elementary_data,
                                   const scalar_parameter_type& youngs_modulus,
                                   const scalar_parameter_type& poisson_ratio,
                                   const scalar_parameter_type& thickness,
                                   const scalar_parameter_type& pretension);

        //! Destructor.
        virtual ~NonlinearMembrane() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        NonlinearMembrane(const NonlinearMembrane& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonlinearMembrane(NonlinearMembrane&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonlinearMembrane& operator=(const NonlinearMembrane& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonlinearMembrane& operator=(NonlinearMembrane&& rhs) = delete;

        ///@}

        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }

        /*!
         * \brief Constant accessor to the former local displacement required by ComputeEltArray().
         *
         * \return Reference to the former local displacement.
         */
        const Eigen::VectorXd& GetFormerLocalDisplacement() const noexcept;

        /*!
         * \brief Non constant accessor to the former local displacement required by ComputeEltArray().
         *
         * \return Non constant reference to the former local displacement.
         */
        Eigen::VectorXd& GetNonCstFormerLocalDisplacement() noexcept;

        //! Accessor to covariant metric tensor.
        const Eigen::Matrix2d& GetCovariantMetricTensor() const noexcept;

        //! Non constant accessor to covariant metric tensor.
        Eigen::Matrix2d& GetNonCstCovariantMetricTensor() noexcept;


      private:
        /*!
         * \brief Compute contravariant basis.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         * \param[out] determinant Determinant of the covariant metric tensor.
         *
         */
        void ComputeContravariantBasis(const Advanced::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                                       double& determinant);

        /*!
         * \brief Compute displacement gradient.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         */
        void ComputeDisplacementGradient(const Advanced::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data);

        /*!
         * \brief Compute Green-Lagrange.
         *
         * \param[in] pretension Value of the pretension.
         */
        void ComputeGreenLagrange(const double pretension);

        /*!
         * \brief Compute locally the tangent tensor.
         *
         * \param[in] E Value at the quadrature point of Young's modulus.
         * \param[in] nu Value at the quadrature point of Poisson ratio.
         */
        void ComputeTangentTensor(double E, double nu);

        //! Compute Second Piola Kirchhoff stress tensor.
        void ComputeSecondPiolaKirchhoff();

        //! Compute matrix De.
        void ComputeDe();

        //! Compute the tangent matrix and/or the RHS (depends of the linear elgebra into which operator is
        //! assembled).
        void ComputeTangentMatrixAndRightHandSide();

      private:
        /*!
         * \brief Accessor to Young's modulus parameter.
         *
         * \return Young's modulus.
         */
        const scalar_parameter_type& GetYoungsModulus() const noexcept;

        /*!
         * \brief Accessor to Poisson ratio parameter.
         *
         * \return Poisson ratio.
         */
        const scalar_parameter_type& GetPoissonRatio() const noexcept;

        /*!
         * \brief Accessor to solid thickness parameter.
         *
         * \return Thickness of the solid.
         */
        const scalar_parameter_type& GetThickness() const noexcept;

        /*!
         * \brief Accessor to pretension parameter.
         *
         * \return Pretension.
         */
        const scalar_parameter_type& GetPretension() const noexcept;

      private:
        //! \name Material parameters.
        ///@{

        //! Young's modulus.
        const scalar_parameter_type& youngs_modulus_;

        //! Poisson ratio.
        const scalar_parameter_type& poisson_ratio_;

        //! Thickness.
        const scalar_parameter_type& thickness_;

        //! Pretension.
        const scalar_parameter_type& pretension_;

        ///@}

        //! Local displacement computed at the former time iteration.
        Eigen::VectorXd former_local_displacement_;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t {
            tangent_tensor = 0,
            De_membrane,
            transposed_De_mult_tangent_tensor,
            tangent_matrix,
            displacement_gradient,
            covariant_basis,
            contravariant_basis,
            transposed_covariant_basis,
            contravariant_metric_tensor,
            transposed_dphi,
            block_contribution,
            work_matrix,
            invert_generalized_covariant_basis,
            test_pk_in_ref_basis,
            test_gl_in_ref_basis
        };


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t { green_lagrange = 0, second_PK, rhs_part };

        ///@}

      private:
        //! Covariant metric tensor.
        Eigen::Matrix2d covariant_metric_tensor_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARMEMBRANE_DOT_HPP_
// *** MoReFEM end header guards *** < //
