// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/MITC9/MITC9.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"

namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local operator for \a NonlinearShell. Look for the specific documentation in Documentation/Operators directory for details.
     */
    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    class NonlinearShell final
    : public HyperelasticityPolicyT,
      public TyingPointsPolicyT,
      public NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>,
      public Crtp::LocalMatrixStorage<NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>, 7UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to variational operator parent.
        using parent = NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 7UL>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Convenient alias.
        using tying_pt_interpolation_component =
            Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         *
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] hyperelastic_law The hyperelastic law if there is an hyperelastic part in the operator,
         * or nullptr otherwise. Example of hyperelasticlaw is MoReFEM::HyperelasticLawNS::CiarletGeymonat.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
         * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         */
        explicit NonlinearShell(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                elementary_data_type&& elementary_data,
                                const typename HyperelasticityPolicyT::law_type* hyperelastic_law);

        //! Destructor.
        virtual ~NonlinearShell() override;

        //! \copydoc doxygen_hide_copy_constructor
        NonlinearShell(const NonlinearShell& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonlinearShell(NonlinearShell&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonlinearShell& operator=(const NonlinearShell& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonlinearShell& operator=(NonlinearShell&& rhs) = delete;

        ///@}

        //! Computes the elementary vector and matrix contributions.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation();

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }

        /*!
         * \brief Constant accessor to the former local displacement required by ComputeEltArray().
         *
         * \return Reference to the former local displacement.
         */
        const Eigen::VectorXd& GetFormerLocalDisplacement() const noexcept;

        /*!
         * \brief Non constant accessor to the former local displacement required by ComputeEltArray().
         *
         * \return Non constant reference to the former local displacement.
         */
        Eigen::VectorXd& GetNonCstFormerLocalDisplacement() noexcept;

      private:
        /*!
         * \brief Helper function to compute the value of a component the Cauchy Green tensor at the tying points.
         *
         * \param[in] covariant_basis Covariant basis expressed at the tying points.
         * \param[in] displacement_gradient Displacement gradient expressed at the tying points.
         * \param[in] component_pair Both tensorial components considered
         * \return The value of the \a i \a j Cauchy Green component (which i and j for each of item of \a component_pair).
         */
        double ComputeCauchyGreenAtTyingPointHelper(
            const Eigen::Matrix3d& covariant_basis,
            const Eigen::Matrix3d& displacement_gradient,
            const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& component_pair);

        /*!
         * \brief Computes the metric tensors directly at the quadrature points (used for the Cauchy Green tensor).
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         * \param[out] covariant_metric_tensor Covariant metric tensor.
         *
         *  \return Contravariant  basis as first element, determinant of covariant matrix as second one.
         */
        std::pair<Eigen::Matrix3d, double>
        ComputeMetricTensorsAtQuadPt(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                                     Eigen::Matrix3d& covariant_metric_tensor);


        /*!
         * \brief Computes the contravariant basis at the tying points (used for the Green Lagrange tensor).
         *
         *  \param[in] dphi_at_tying_point Gradient of the geometric shape functions at the tying points.
         *
         * \return Work matrix which conntains the covariant basis.
         *
         */
        [[nodiscard]] Eigen::Matrix3d ComputeCovariantBasisAtTyingPoint(
            const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_at_tying_point);

        /*!
         * \brief Computes displacement gradient at a tying point.
         *
         * \param[in] dphi_at_tying_point Gradient of the finite element shape functions at the tying points.
         *
         * \return Work matrix which conntains the computed gradient.
         */
        [[nodiscard]] Eigen::Matrix3d ComputeDisplacementGradientAtTyingPoint(
            const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_at_tying_point);


        /*!
         * \brief Updates all local matrices/vectors for the current quadrature point from the list of values
         * obtained at the tying points.
         *
         * \param[in] quad_pt Current quadrature point.
         * \param[in] covariant_metric_tensor Covariant metric tensor.
         * \param[in,out] cauchy_green_as_vector Cauchy-Green tensor. Its size is assumed to be already given in input; content is completely
         * computed by current method.
         */
        void PrepareInternalDataFromTyingPtToQuadPt(const QuadraturePoint& quad_pt,
                                                    const Eigen::Matrix3d& covariant_metric_tensor,
                                                    ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_as_vector);

        /*!
         * \brief Computes the Second Piola-Kirchhoff stress tensor and its derivative in generalized coordinates.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] ref_felt Reference finite element considered.
         * \param[in] contravariant_basis Contravariant basis.
         * \param[in] matricial_cauchy_green_tensor Matricial Cauchy-Green tensor.
         * \param[out] dW Computed dW value.
         */
        void ComputeWDerivates(const QuadraturePoint& quad_pt,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt,
                               const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
                               const Eigen::Matrix3d& contravariant_basis,
                               ::MoReFEM::Wrappers::EigenNS::dWVector& dW);


        //! Sets the interpolated product of finite element shape function gradients at each quadrature point.
        void InitGradGradArray();

        /*!
         * \brief Updates the diagonal components of the strain tensors (Cauchy Green and Green Lagrange derivative)
         *  at each quadrature point from the tying point data.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] component Component of the strain tensors for which the update is required.
         *
         * \return The value of the updated Cauchy Green component.
         */
        double UpdateDiagonalStrainsFromTyingPt(const QuadraturePoint& quad_pt,
                                                tying_pt_interpolation_component component);

        /*!
         * \brief Updates the extradiagonal components of the strain tensors (Cauchy Green and Green Lagrange derivative)
         *  at each quadrature point from the tying point data.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] component Component of the strain tensors for which the update is required.
         *
         * \return The value of the updated Cauchy Green component.
         *
         */
        double UpdateExtraDiagonalStrainsFromTyingPt(const QuadraturePoint& quad_pt,
                                                     tying_pt_interpolation_component component);


        /*!
         * \brief Gets the local matrix array containing the product of gradients, interpolated from tying points at
         * the current quadrature point. Array index stands for a component index.
         *
         * \param[in] quad_pt_index Quadrature point index sought.
         * \return The array of matrices containing the product of gradients.
         *
         */
        const std::array<Eigen::MatrixXd, 9>& GetGradGradProductAtQuadPt(std::size_t quad_pt_index) const noexcept;

        //! Accessor to the object storing data required to compute W derivates.
        const InvariantNS::GeneralizedNS::Input& GetGeneralizedInputArguments() const noexcept;

        //! Non constant accessor to the object storing data required to compute W derivates.
        InvariantNS::GeneralizedNS::Input& GetNonCstGeneralizedInputArguments() noexcept;

      private:
        //! Accessor to d2W .
        const ::MoReFEM::Wrappers::EigenNS::d2WMatrix& Getd2W() const noexcept;

        //! Non constant accessor to d2W .
        ::MoReFEM::Wrappers::EigenNS::d2WMatrix& GetNonCstd2W() noexcept;

        /*!
         * \brief Compute extended dW
         *
         * \param[in] dW dW.
         *
         * We want the same order as the (i, j) loop for the grad-grad contribution for extended_dW. Its content is:
         *
         * \code
         // Diagonal terms.
         for (auto i = Eigen::Index {}; i < 3; ++i)
             extended_dW(3 * i + i) = dW(i);

         // Extradiagonal terms.
         extended_dW(1) = extended_dW(3) = dW(3);
         extended_dW(2) = extended_dW(6) = dW(5);
         extended_dW(5) = extended_dW(7) = dW(4);
         \endcode
         *
         * \return Computed vector, allocated on the stack.
         */
        ::MoReFEM::Wrappers::EigenNS::VectorNd<9>
        ComputeExtendedD2W(const ::MoReFEM::Wrappers::EigenNS::dWVector& dW) const noexcept;

      private:
        //! Local displacement computed at the former time iteration.
        Eigen::VectorXd former_local_displacement_;

        //! Contains the product of gradients of the Felt Shape functions at each quadrature point.
        std::vector<std::array<Eigen::MatrixXd, 9>> grad_grad_list_;

        //! Data required to compute W derivates
        InvariantNS::GeneralizedNS::Input generalized_input_;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t {
            cauchy_green_as_vector, // in a (6,1) matrix as we need to transpose and do matrix operations.
            De_dot_test_grad,
            transposed_De_dot_test_grad,
            de_dot_d2W,
            linear_part,
            d2W_dot_De_dot_test_grad
        };

        //! d2W matrix.
        ::MoReFEM::Wrappers::EigenNS::d2WMatrix d2W_;


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t {
            extended_dW // used for the geometric stiffness contribution
        };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearShell.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HPP_
// *** MoReFEM end header guards *** < //
