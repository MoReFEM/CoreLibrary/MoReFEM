// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearShell.hpp"
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::NonlinearShell(
        const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
        elementary_data_type&& a_elementary_data,
        const typename HyperelasticityPolicyT::law_type* hyperelastic_law)
    : HyperelasticityPolicyT(a_elementary_data.GetMeshDimension(), hyperelastic_law), TyingPointsPolicyT(),
      parent(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)), matrix_parent(),
      generalized_input_(InvariantNS::GeneralizedNS::is_I2::yes)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

        const auto& elementary_data = GetElementaryData();
        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_test = test_unknown_ref_felt.Nnode();
        const auto& ref_geom_elt = elementary_data.GetRefGeomElt();

#ifndef NDEBUG
        {
            const auto ref_felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

            const auto euclidean_dimension = unknown_ref_felt.GetMeshDimension();

            assert(ref_felt_space_dimension == euclidean_dimension && "This operator is a volumic operator.");
        }
#endif // NDEBUG

        former_local_displacement_.resize(elementary_data.NdofCol());

        matrix_parent::InitLocalMatrixStorage({ {
            { 6, 1 },                                  // cauchy_green_as_vector
            { 6, 3 * Nnode.Get() },                    // De_dot_test_grad
            { 3 * Nnode_test.Get(), 6 },               // transposed_De_dot_test_grad
            { 3 * Nnode_test.Get(), 6 },               // de_dot_d2W
            { 3 * Nnode_test.Get(), 3 * Nnode.Get() }, // linear_part
            { 6, 3 * Nnode.Get() }                     // d2W_dot_De_dot_test_grad
        } });

        d2W_ = ::MoReFEM::Wrappers::EigenNS::Initd2WMatrix(3);

        const auto& information_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        TyingPointsPolicyT::InitTyingPointData(
            information_at_quad_pt_list, ref_geom_elt, unknown_ref_felt, test_unknown_ref_felt);

        InitGradGradArray();
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::~NonlinearShell() = default;


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    const std::string& NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ClassName()
    {
        static std::string name("NonlinearShell");
        return name;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::InitLocalComputation()
    { }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.setZero();

        auto& linear_part = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part)>();
        linear_part.setZero();

        auto& vector_result = elementary_data.GetNonCstVectorResult();
        vector_result.setZero();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        std::size_t quad_pt_index = 0;
        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            Eigen::Matrix3d covariant_metric_tensor;

            auto [contravariant_basis, determinant] =
                ComputeMetricTensorsAtQuadPt(quad_pt_unknown_data, covariant_metric_tensor);

            // Warning: we can't provide the even neater `VectorMaxNd<6>` as this variable will be passed to
            // hyperelastic policy, which expects a dWVector. Not complying here would induce an unneeded copy - which
            // triggers by design an assert.
            ::MoReFEM::Wrappers::EigenNS::dWVector cauchy_green_tensor(6);

            PrepareInternalDataFromTyingPtToQuadPt(quad_pt, covariant_metric_tensor, cauchy_green_tensor);

            auto dW = ::MoReFEM::Wrappers::EigenNS::InitdWVector(3);

            ComputeWDerivates(quad_pt, geom_elt, unknown_ref_felt, cauchy_green_tensor, contravariant_basis, dW);

            decltype(auto) d2W = Getd2W();

            const auto& De_dot_test_grad =
                matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();

            const auto& transposed_De_dot_test_grad = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                LocalMatrixIndex::transposed_De_dot_test_grad)>();
            auto& d2W_dot_De_dot_test_grad = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                LocalMatrixIndex::d2W_dot_De_dot_test_grad)>();

            const auto geometric_jacobian = std::fabs(determinant);

            const auto weight_meas = quad_pt.GetWeight() * geometric_jacobian;

            // Linear term.
            if (parent::DoAssembleIntoVector())
                vector_result.noalias() += weight_meas * transposed_De_dot_test_grad * dW;

            // Bilinear terms. There are in fact two: one for the linear part and another for non-linear one.
            if (parent::DoAssembleIntoMatrix())
            {
                d2W_dot_De_dot_test_grad.noalias() = d2W * De_dot_test_grad;
                matrix_result.noalias() += weight_meas * transposed_De_dot_test_grad * d2W_dot_De_dot_test_grad;

                const auto extended_dW = ComputeExtendedD2W(dW);

                const auto& grad_grad_ij_at_quad_pt = GetGradGradProductAtQuadPt(quad_pt_index);

                assert(static_cast<Eigen::Index>(grad_grad_ij_at_quad_pt.size()) == extended_dW.size());
                // Non-linear part.
                for (auto i = Eigen::Index(), size = extended_dW.size(); i < size; ++i)
                    matrix_result +=
                        weight_meas * extended_dW(i) * grad_grad_ij_at_quad_pt[static_cast<std::size_t>(i)];
            }

            ++quad_pt_index;
        } // loop over quadrature points

        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeMetricTensorsAtQuadPt(
        const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
        Eigen::Matrix3d& covariant_metric_tensor) -> std::pair<Eigen::Matrix3d, double>
    {
        const auto& elementary_data = GetElementaryData();

        Eigen::Matrix3d covariant_basis;
        covariant_basis.setZero();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto& dphi_geo = quad_pt_unknown_data.GetGradientRefGeometricPhi();
        const auto Nshape_function = dphi_geo.rows();
        constexpr auto euclidean_dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };

        for (auto shape_fct_index = Eigen::Index{}; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

            for (auto component_shape_function = ::MoReFEM::GeometryNS::dimension_type{};
                 component_shape_function < euclidean_dimension;
                 ++component_shape_function)
            {
                for (auto coord_index = ::MoReFEM::GeometryNS::dimension_type{}; coord_index < euclidean_dimension;
                     ++coord_index)
                {
                    covariant_basis(coord_index.Get(), component_shape_function.Get()) +=
                        coords_in_geom_elt[coord_index] * dphi_geo(shape_fct_index, component_shape_function.Get());
                }
            }
        }

        Eigen::Matrix3d contravariant_metric_tensor;

        covariant_metric_tensor.noalias() = covariant_basis.transpose() * covariant_basis;
        contravariant_metric_tensor.noalias() = covariant_metric_tensor.inverse();

        GetNonCstGeneralizedInputArguments().Update(contravariant_metric_tensor);

        Eigen::Matrix3d contravariant_basis;
        contravariant_basis.noalias() = covariant_basis * contravariant_metric_tensor;

        return std::make_pair(contravariant_basis, covariant_basis.determinant());
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeCovariantBasisAtTyingPoint(
        const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_at_tying_point) -> Eigen::Matrix3d
    {
        const auto& elementary_data = GetElementaryData();

        Eigen::Matrix3d covariant_basis_at_tying_pt;
        covariant_basis_at_tying_pt.setZero();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto Nshape_function = dphi_at_tying_point.rows();
        constexpr auto euclidean_dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };

        for (auto shape_fct_index = Eigen::Index{}; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

            for (auto component_shape_function = ::MoReFEM::GeometryNS::dimension_type{};
                 component_shape_function < euclidean_dimension;
                 ++component_shape_function)
            {
                for (auto coord_index = ::MoReFEM::GeometryNS::dimension_type{}; coord_index < euclidean_dimension;
                     ++coord_index)
                {
                    covariant_basis_at_tying_pt(coord_index.Get(), component_shape_function.Get()) +=
                        coords_in_geom_elt[coord_index]
                        * dphi_at_tying_point(shape_fct_index, component_shape_function.Get());
                }
            }
        }

        return covariant_basis_at_tying_pt;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeDisplacementGradientAtTyingPoint(
        const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_at_tying_point) -> Eigen::Matrix3d
    {
        Eigen::Matrix3d ret;
        ret.setZero();

        auto& local_displacement = GetFormerLocalDisplacement();

        constexpr auto euclidean_dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };

        const auto Nshape_function = dphi_at_tying_point.rows();

        for (auto shape_fct_index = Eigen::Index{}; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            for (auto component_shape_function = ::MoReFEM::GeometryNS::dimension_type{};
                 component_shape_function < euclidean_dimension;
                 ++component_shape_function)
            {
                for (auto coord_index = ::MoReFEM::GeometryNS::dimension_type{}; coord_index < euclidean_dimension;
                     ++coord_index)
                {
                    const auto local_displacement_index = Nshape_function * coord_index.Get() + shape_fct_index;
                    assert(local_displacement_index < local_displacement.size());

                    ret(coord_index.Get(), component_shape_function.Get()) +=
                        local_displacement(local_displacement_index)
                        * dphi_at_tying_point(shape_fct_index, component_shape_function.Get());
                }
            }
        }

        return ret;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::PrepareInternalDataFromTyingPtToQuadPt(
        const QuadraturePoint& quad_pt,
        const Eigen::Matrix3d& covariant_metric_tensor,
        ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_as_vector)
    {

        cauchy_green_as_vector.setZero();

        auto& De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();
        auto& transposed_De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_dot_test_grad)>();

        De_dot_test_grad.setZero();
        transposed_De_dot_test_grad.setZero();

        const double C_11 = UpdateDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_rr);

        const double C_22 = UpdateDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_ss);
        const double C_33 = UpdateDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_zz);

        const double C_12 = UpdateExtraDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_rs);

        const double C_23 = UpdateExtraDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_sz);
        const double C_13 = UpdateExtraDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_rz);

        cauchy_green_as_vector(0, 0) = C_11 + covariant_metric_tensor(0, 0);
        cauchy_green_as_vector(1, 0) = C_22 + covariant_metric_tensor(1, 1);
        cauchy_green_as_vector(2, 0) = C_33 + covariant_metric_tensor(2, 2);
        cauchy_green_as_vector(3, 0) = C_12 + covariant_metric_tensor(0, 1);
        cauchy_green_as_vector(4, 0) = C_23 + covariant_metric_tensor(1, 2);
        cauchy_green_as_vector(5, 0) = C_13 + covariant_metric_tensor(0, 2);
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeWDerivates(
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& unknown_ref_felt,
        const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        const Eigen::Matrix3d& contravariant_basis,
        ::MoReFEM::Wrappers::EigenNS::dWVector& dW)
    {

        auto& d2W = GetNonCstd2W();

        decltype(auto) generalized_input_args = GetGeneralizedInputArguments();

        dW.setZero();
        d2W.setZero();

        const auto& hyperelasticity_ptr = static_cast<HyperelasticityPolicyT*>(this);
        auto& hyperelasticity = *hyperelasticity_ptr;

        hyperelasticity.ComputeWDerivates(quad_pt,
                                          geom_elt,
                                          unknown_ref_felt,
                                          matricial_cauchy_green_tensor,
                                          generalized_input_args,
                                          contravariant_basis,
                                          dW,
                                          d2W);
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    double NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeCauchyGreenAtTyingPointHelper(
        const Eigen::Matrix3d& covariant_basis,
        const Eigen::Matrix3d& displacement_gradient,
        const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& component_pair)
    {
        double dot_products = 0.;
        constexpr auto euclidean_dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };

        const auto i = EnumUnderlyingType(component_pair.first);
        const auto j = EnumUnderlyingType(component_pair.second);

        for (auto k = ::MoReFEM::GeometryNS::dimension_type{}; k < euclidean_dimension; ++k)
        {
            const auto k_index = k.Get();

            dot_products += covariant_basis(k_index, i) * displacement_gradient(k_index, j)
                            + covariant_basis(k_index, j) * displacement_gradient(k_index, i)
                            + displacement_gradient(k_index, i) * displacement_gradient(k_index, j);
        }

        return dot_products;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::InitGradGradArray()
    {
        const auto& elementary_data = GetElementaryData();
        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            auto get_grad_grad = [this, &quad_pt](tying_pt_interpolation_component component) -> const Eigen::MatrixXd&
            {
                return this->GetTyingPointDataForComponent(quad_pt, component).GetGradGradProduct();
            };

            grad_grad_list_.push_back({ { get_grad_grad(tying_pt_interpolation_component::e_rr),
                                          get_grad_grad(tying_pt_interpolation_component::e_rs),
                                          get_grad_grad(tying_pt_interpolation_component::e_rz),
                                          get_grad_grad(tying_pt_interpolation_component::e_sr),
                                          get_grad_grad(tying_pt_interpolation_component::e_ss),
                                          get_grad_grad(tying_pt_interpolation_component::e_sz),
                                          get_grad_grad(tying_pt_interpolation_component::e_zr),
                                          get_grad_grad(tying_pt_interpolation_component::e_zs),
                                          get_grad_grad(tying_pt_interpolation_component::e_zz) } });

        } // Loop on quad pts.
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    double NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::UpdateDiagonalStrainsFromTyingPt(
        const QuadraturePoint& quad_pt,
        tying_pt_interpolation_component component)
    {
        double ret = 0.;
        auto& De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();
        auto& transposed_De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_dot_test_grad)>();

        decltype(auto) mitc_data = TyingPointsPolicyT::GetTyingPointDataForComponent(quad_pt, component);

        constexpr auto dimension = Eigen::Index{ 3 };

        const auto tensor_component = EnumUnderlyingType(component);

        const auto component_pair = std::make_pair(component, component);

        decltype(auto) tying_point_list = mitc_data.GetTyingPointList();

        for (const auto& tying_point_ptr : tying_point_list)
        {
            assert(!(!tying_point_ptr));
            const auto& tying_point = *tying_point_ptr;

            decltype(auto) dphi_geo = tying_point.GetGeometricGradient();
            decltype(auto) dphi_felt = tying_point.GetFEltGradient();

            const auto displacement_gradient_at_tying_pt = ComputeDisplacementGradientAtTyingPoint(dphi_felt);

            const auto covariant_basis_at_tying_pt = ComputeCovariantBasisAtTyingPoint(dphi_geo);

            const auto non_geometric_part = ComputeCauchyGreenAtTyingPointHelper(
                covariant_basis_at_tying_pt, displacement_gradient_at_tying_pt, component_pair);

            const auto tying_pt_shape_function_value = tying_point.GetShapeFunctionValue();

            ret += tying_pt_shape_function_value * non_geometric_part;

            decltype(auto) dphi_test_felt = tying_point.GetTestFEltGradient();
            const auto Nnode_test = LocalNodeNS::index_type{ dphi_test_felt.rows() };
            const auto Nnode = LocalNodeNS::index_type{ dphi_felt.rows() };

            for (auto comp = Eigen::Index{}; comp < dimension; ++comp)
            {
                for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode; ++row_node)
                {
                    De_dot_test_grad(tensor_component, row_node.Get() + comp * Nnode.Get()) +=
                        tying_pt_shape_function_value
                        * (displacement_gradient_at_tying_pt(comp, tensor_component)
                           + covariant_basis_at_tying_pt(comp, tensor_component))
                        * dphi_felt(row_node.Get(), tensor_component);
                }

                for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test; ++row_node)
                {
                    transposed_De_dot_test_grad(row_node.Get() + comp * Nnode_test.Get(), tensor_component) +=
                        tying_pt_shape_function_value
                        * (displacement_gradient_at_tying_pt(comp, tensor_component)
                           + covariant_basis_at_tying_pt(comp, tensor_component))
                        * dphi_test_felt(row_node.Get(), tensor_component);
                }
            }
        }

        return ret;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    double NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::UpdateExtraDiagonalStrainsFromTyingPt(
        const QuadraturePoint& quad_pt,
        tying_pt_interpolation_component component)
    {
        decltype(auto) mitc_data = TyingPointsPolicyT::GetTyingPointDataForComponent(quad_pt, component);

        double ret = 0.;
        auto& De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();
        auto& transposed_De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_dot_test_grad)>();

        const auto component_index = EnumUnderlyingType(component);

        const auto tensor_component_pair = ComputePair(component);

        const auto first_component = EnumUnderlyingType(tensor_component_pair.first);
        const auto second_component = EnumUnderlyingType(tensor_component_pair.second);

        constexpr auto dimension = GeometryNS::dimension_type{ 3 };

        decltype(auto) tying_point_list = mitc_data.GetTyingPointList();

        for (const auto& tying_point_ptr : tying_point_list)
        {
            assert(!(!tying_point_ptr));
            const auto& tying_point = *tying_point_ptr;

            decltype(auto) dphi_geo = tying_point.GetGeometricGradient();
            decltype(auto) dphi_felt = tying_point.GetFEltGradient();
            decltype(auto) dphi_test_felt = tying_point.GetTestFEltGradient();

            const auto displacement_gradient_at_tying_pt = ComputeDisplacementGradientAtTyingPoint(dphi_felt);
            const auto covariant_basis_at_tying_pt = ComputeCovariantBasisAtTyingPoint(dphi_geo);

            const auto non_geometric_part = ComputeCauchyGreenAtTyingPointHelper(
                covariant_basis_at_tying_pt, displacement_gradient_at_tying_pt, tensor_component_pair);

            const auto tying_pt_shape_function_value = tying_point.GetShapeFunctionValue();
            ret += tying_pt_shape_function_value * non_geometric_part;
            const auto Nnode_test = LocalNodeNS::index_type{ dphi_test_felt.rows() };
            const auto Nnode = LocalNodeNS::index_type{ dphi_felt.rows() };

            for (auto comp = GeometryNS::dimension_type{}; comp < dimension; ++comp)
            {
                for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode; ++row_node)
                {
                    De_dot_test_grad(component_index, row_node.Get() + comp.Get() * Nnode.Get()) +=
                        tying_pt_shape_function_value
                        * ((displacement_gradient_at_tying_pt(comp.Get(), first_component)
                            + covariant_basis_at_tying_pt(comp.Get(), first_component))
                               * dphi_felt(row_node.Get(), second_component)
                           + (displacement_gradient_at_tying_pt(comp.Get(), second_component)
                              + covariant_basis_at_tying_pt(comp.Get(), second_component))
                                 * dphi_felt(row_node.Get(), first_component));
                }

                for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test; ++row_node)
                {
                    transposed_De_dot_test_grad(row_node.Get() + comp.Get() * Nnode_test.Get(), component_index) +=
                        tying_pt_shape_function_value
                        * ((displacement_gradient_at_tying_pt(comp.Get(), first_component)
                            + covariant_basis_at_tying_pt(comp.Get(), first_component))
                               * dphi_test_felt(row_node.Get(), second_component)
                           + (displacement_gradient_at_tying_pt(comp.Get(), second_component)
                              + covariant_basis_at_tying_pt(comp.Get(), second_component))
                                 * dphi_test_felt(row_node.Get(), first_component));
                }
            }
        }

        return ret;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    inline auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetFormerLocalDisplacement() const noexcept
        -> const Eigen::VectorXd&
    {
        return former_local_displacement_;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    inline auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetNonCstFormerLocalDisplacement() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalDisplacement());
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    inline const std::array<Eigen::MatrixXd, 9>&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetGradGradProductAtQuadPt(
        const std::size_t quad_pt_index) const noexcept
    {
        assert(quad_pt_index < grad_grad_list_.size());
        return grad_grad_list_[quad_pt_index];
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    const InvariantNS::GeneralizedNS::Input&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetGeneralizedInputArguments() const noexcept
    {
        return generalized_input_;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    InvariantNS::GeneralizedNS::Input&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetNonCstGeneralizedInputArguments() noexcept
    {
        return const_cast<InvariantNS::GeneralizedNS::Input&>(GetGeneralizedInputArguments());
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::Getd2W() const noexcept
        -> const ::MoReFEM::Wrappers::EigenNS::d2WMatrix&
    {
        return d2W_;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetNonCstd2W() noexcept
        -> ::MoReFEM::Wrappers::EigenNS::d2WMatrix&

    {
        return const_cast<::MoReFEM::Wrappers::EigenNS::d2WMatrix&>(Getd2W());
    }

    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeExtendedD2W(
        const ::MoReFEM::Wrappers::EigenNS::dWVector& dW) const noexcept -> ::MoReFEM::Wrappers::EigenNS::VectorNd<9>
    {
        ::MoReFEM::Wrappers::EigenNS::VectorNd<9> ret;
        ret.setZero();

        for (auto i = Eigen::Index{}; i < 3; ++i)
            ret(3 * i + i) = dW(i);

        // Extradiagonal terms.
        ret(1) = ret(3) = dW(3);
        ret(2) = ret(6) = dW(5);
        ret(5) = ret(7) = dW(4);

        return ret;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HXX_
// *** MoReFEM end header guards *** < //
