// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"

namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Mixed solid incompressibility.
     *
     * Used as penalization for incompressible hyperelasticity.
     *
     */
    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class DifferentCauchyGreenMixedSolidIncompressibility final
    : public NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>,
      public Crtp::
          LocalMatrixStorage<DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>, 5UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Type of the elementary matrix.
        using matrix_type = Eigen::MatrixXd;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 5UL>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Alias to CauchyGreenTensor.
        using cauchy_green_tensor_type = ParameterAtQuadraturePoint<ParameterNS::Type::vector,
                                                                    TimeManagerT,
                                                                    ParameterNS::TimeDependencyNS::None,
                                                                    ::MoReFEM::Wrappers::EigenNS::dWVector>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         *
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] hydrostatic_law_volumetric_part Volumetric part of the hydrostatic law policy; do not
         * delete this pointer!
         * \param[in] hydrostatic_law_deviatoric_part  Deviatoric part of the hydrostatic law policy; do not
         * delete this pointer!
         * \param[in] cauchy_green_tensor Cauchy-Green tensor of the related second
         * Piola-Kirchhoff stress tensor.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit DifferentCauchyGreenMixedSolidIncompressibility(
            const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
            elementary_data_type&& elementary_data,
            const cauchy_green_tensor_type& cauchy_green_tensor,
            const HydrostaticLawPolicyT* hydrostatic_law_volumetric_part,
            const HydrostaticLawPolicyT* hydrostatic_law_deviatoric_part);

        //! Destructor.
        virtual ~DifferentCauchyGreenMixedSolidIncompressibility() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        DifferentCauchyGreenMixedSolidIncompressibility(const DifferentCauchyGreenMixedSolidIncompressibility& rhs) =
            delete;

        //! \copydoc doxygen_hide_move_constructor
        DifferentCauchyGreenMixedSolidIncompressibility(DifferentCauchyGreenMixedSolidIncompressibility&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DifferentCauchyGreenMixedSolidIncompressibility&
        operator=(const DifferentCauchyGreenMixedSolidIncompressibility& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DifferentCauchyGreenMixedSolidIncompressibility&
        operator=(DifferentCauchyGreenMixedSolidIncompressibility&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }

        //! Set Cauchy-Green tensor \a Parameter. Must be called only once.
        //! \param[in] param The parameter computed outside of the class.
        //! \internal <b><tt>[internal]</tt></b> Same function name as the one used by the
        //! SecondPiolaKirchhoffOperator to use the ApplySetCauchyGreenTensor helper. Here we only need to use
        //! it on the cauchy green tensor needed for the volumetric part of the computation as the deviatoric
        //! part as already been precomputed and is inherited through the \a stiffness_parent when this operator
        //! is constructed by the QuasiIncompressibleSecondPiolaKirchhoffOperator.
        void SetCauchyGreenTensor(const cauchy_green_tensor_type* param);

        //! Constant accessor to the former deviatoric local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalDisplacementDeviatoric() const noexcept;

        //! Non constant accessor to the deviatoric former local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalDisplacementDeviatoric() noexcept;

        //! Constant accessor to the former deviatoric local pressure required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalPressureDeviatoric() const noexcept;

        //! Non constant accessor to the former deviatoric local pressure required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalPressureDeviatoric() noexcept;

        //! Constant accessor to the former volumetric local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalDisplacementVolumetric() const noexcept;

        //! Non constant accessor to the former volumetric local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalDisplacementVolumetric() noexcept;

        //! Constant accessor to the former volumetric local pressure required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalPressureVolumetric() const noexcept;

        //! Non constant accessor to the former volumetric local pressure required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalPressureVolumetric() noexcept;

      private:
        //! Get Cauchy-Green tensor \a Parameter.
        const cauchy_green_tensor_type& GetCauchyGreenTensorDeviatoric() const noexcept;

        //! Get Cauchy-Green tensor \a Parameter.
        const cauchy_green_tensor_type& GetCauchyGreenTensorVolumetric() const noexcept;

        //! Helper class to manage computation of the deviatoric part of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
        GetNonCstDerivativeGreenLagrangeDeviatoric() noexcept;

        //! Helper class to manage computation of the volumetric part of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
        GetNonCstDerivativeGreenLagrangeVolumetric() noexcept;

        /*!
         * \brief Compute internal data such as invariants, De, Cauchy-Green tensor for current quadrature
         * point.
         *
         * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
         * point, such as the geometric and finite element shape function values. Quadrature points related to
         * functions are considered here.
         * \param[in] felt_space_dimension Dimension of the \a FEltSpace.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] local_displacement_deviatoric Local displacement observed within the finite element at
         * previous time iteration. \param[in] local_pressure_deviatoric Local pressure observed within the
         * finite element at previous time iteration. \param[in] local_displacement_volumetric Local
         * displacement observed within the finite element at previous time iteration. \param[in]
         * local_pressure_volumetric Local pressure observed within the finite element at previous time
         * iteration.
         * \param[in] displacement_ref_felt Reference finite element for displacement.
         * \param[in] pressure_ref_felt Reference finite element for pressure.
         * \param[in] test_displacement_ref_felt Test reference finite element for displacement.
         * \param[in] test_pressure_ref_felt Test reference finite element for pressure.
         */
        void PrepareInternalDataForQuadraturePoint(const InformationAtQuadraturePoint& infos_at_quad_pt,
                                                   const GeometricElt& geom_elt,
                                                   const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                                                   const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                                                   const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                                                   const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                                                   const Eigen::VectorXd& local_displacement_deviatoric,
                                                   const Eigen::VectorXd& local_pressure_deviatoric,
                                                   const Eigen::VectorXd& local_displacement_volumetric,
                                                   const Eigen::VectorXd& local_pressure_volumetric,
                                                   const ::MoReFEM::GeometryNS::dimension_type felt_space_dimension);

        /*!
         * \brief Manage the ComputeEltArray part of the calculation that is dependent on the dimension
         * considered.
         *
         *  \tparam MeshDimensionT Dimension of the mesh.
         *
         * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
         * point, such as the geometric and finite element shape function values. Quadrature points related to
         * functions are considered here.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] local_displacement_deviatoric Local displacement observed within the finite element at
         * previous time iteration. \param[in] local_pressure_deviatoric Local pressure observed within the
         * finite element at previous time iteration. \param[in] local_displacement_volumetric Local
         * displacement observed within the finite element at previous time iteration. \param[in]
         * local_pressure_volumetric Local pressure observed within the finite element at previous time
         * iteration.
         * \param[in] displacement_ref_felt Reference finite element for displacement.
         * \param[in] pressure_ref_felt Reference finite element for pressure.
         * \param[in] test_displacement_ref_felt Test reference finite element for displacement.
         * \param[in] test_pressure_ref_felt Test reference finite element for pressure.
         */
        template<std::size_t FeltSpaceDimensionT>
        void PrepareInternalDataForQuadraturePointForDimension(
            const InformationAtQuadraturePoint& infos_at_quad_pt,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
            const Eigen::VectorXd& local_displacement_deviatoric,
            const Eigen::VectorXd& local_pressure_deviatoric,
            const Eigen::VectorXd& local_displacement_volumetric,
            const Eigen::VectorXd& local_pressure_volumetric);


      private:
        //! Underlying hydrostatic law - volumetric part.
        const HydrostaticLawPolicyT& GetHydrostaticLawVolumetricPart() const noexcept;

        //! Underlying hydrostatic law - deviatoric part.
        const HydrostaticLawPolicyT& GetHydrostaticLawDeviatoricPart() const noexcept;

        //! Underlying hydrostatic law - volumetric part.
        HydrostaticLawPolicyT& GetNonCstHydrostaticLawVolumetricPart() noexcept;

        //! Underlying hydrostatic law - deviatoric part.
        HydrostaticLawPolicyT& GetNonCstHydrostaticLawDeviatoricPart() noexcept;


      private:
        //! Helper class to manage computation of the deviatoric part of the derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>::unique_ptr deriv_green_lagrange_deviatoric_ =
            nullptr;

        //! Helper class to manage computation of the volumetric part of the derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>::unique_ptr deriv_green_lagrange_volumetric_ =
            nullptr;

        /*!
         * \brief Reference to the Cauchy-Green tensor \a Parameter inherited through the \a stiffness_parent.
         *
         * Do not release this pointer: it should point to an object which is already handled correctly.
         *
         */
        const cauchy_green_tensor_type& cauchy_green_tensor_deviatoric_;

        /*!
         * \
         * \brief Pointer to the Cauchy-Green tensor \a Parameter.
         *
         * Do not release this pointer: it should point to an object which is already handled correctly.
         *
         * This must be set with SetCauchyGreenTensor().
         */
        const cauchy_green_tensor_type* cauchy_green_tensor_volumetric_ = nullptr;


        /*!
         * \brief Displacement obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray. \endinternal
         */
        Eigen::VectorXd former_local_displacement_deviatoric_;

        /*!
         * \brief Displacement obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray. \endinternal
         */
        Eigen::VectorXd former_local_displacement_volumetric_;

        /*!
         * \brief Pressure obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd former_local_pressure_deviatoric_;

        /*!
         * \brief Pressure obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd former_local_pressure_volumetric_;

      private:
        //! Underlying hydrostatic law - volumetric part.
        const HydrostaticLawPolicyT* const hydrostatic_law_volumetric_part_;

        //! Underlying hydrostatic law - deviatoric part.
        const HydrostaticLawPolicyT* const hydrostatic_law_deviatoric_part_;

      private:
        /*!
         * \brief An internal work matrix.
         *
         * \attention It is a data attribute only to avoid allocating memory while crushing numbers...
         */
        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor, 3, Eigen::Dynamic> work_matrix_1_;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes related to local matrices.
        enum class LocalMatrixIndex : std::size_t {
            block_contribution = 0,
            dphi_test_disp_mult_column_matrix,
            block_contribution_disp_pres,
            row_matrix_mult_transposed_dphi_disp,
            block_contribution_pres_disp
        };


        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
