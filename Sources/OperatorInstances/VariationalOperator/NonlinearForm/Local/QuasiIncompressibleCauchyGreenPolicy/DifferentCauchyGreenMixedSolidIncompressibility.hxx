// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        DifferentCauchyGreenMixedSolidIncompressibility(
            const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
            elementary_data_type&& a_elementary_data,
            const cauchy_green_tensor_type& cauchy_green_tensor,
            const HydrostaticLawPolicyT* hydrostatic_law_volumetric_part,
            const HydrostaticLawPolicyT* hydrostatic_law_deviatoric_part)
    : NonlinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent(),
      cauchy_green_tensor_deviatoric_(cauchy_green_tensor),
      hydrostatic_law_volumetric_part_(hydrostatic_law_volumetric_part),
      hydrostatic_law_deviatoric_part_(hydrostatic_law_deviatoric_part)
    {
        const auto& elementary_data = GetElementaryData();

        const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_displacement = displacement_ref_felt.Nnode();

        const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
        const auto Nnode_pressure = pressure_ref_felt.Nnode();

        const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
        const auto Nnode_test_displacement = test_displacement_ref_felt.Nnode();

        const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));
        const auto Nnode_test_pressure = test_pressure_ref_felt.Nnode();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ elementary_data.GetGeomEltDimension() };

        former_local_displacement_deviatoric_.resize(elementary_data.GetRefFElt(GetNthUnknown(0)).Ndof());

        former_local_displacement_volumetric_.resize(elementary_data.GetRefFElt(GetNthUnknown(0)).Ndof());

        former_local_pressure_deviatoric_.resize(elementary_data.GetRefFElt(GetNthUnknown(1)).Ndof());

        former_local_pressure_volumetric_.resize(elementary_data.GetRefFElt(GetNthUnknown(1)).Ndof());

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_test_displacement.Get(), Nnode_displacement.Get() }, // block_contribution
            { Nnode_test_displacement.Get(), 1 },                        // dphi_test_disp_mult_column_matrix,
            { Nnode_test_displacement.Get(), Nnode_pressure.Get() },     // block_contribution_disp_pres,
            { 1, Nnode_displacement.Get() },                             // row_matrix_mult_transposed_dphi_disp,
            { Nnode_test_pressure.Get(), Nnode_displacement.Get() }      // block_contribution_pres_disp
        } });

        work_matrix_1_.resize(Ncomponent.Get(), Nnode_displacement.Get());

        deriv_green_lagrange_deviatoric_ =
            std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(Ncomponent);

        deriv_green_lagrange_volumetric_ =
            std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(Ncomponent);
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ClassName()
    {
        static std::string name("DifferentCauchyGreenMixedSolidIncompressibility");
        return name;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ComputeEltArray()
    {

        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        const auto& local_displacement_deviatoric = GetFormerLocalDisplacementDeviatoric();
        const auto& local_pressure_deviatoric = GetFormerLocalPressureDeviatoric();
        const auto& local_displacement_volumetric = GetFormerLocalDisplacementVolumetric();
        const auto& local_pressure_volumetric = GetFormerLocalPressureVolumetric();

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        auto& vector_result = elementary_data.GetNonCstVectorResult();
        matrix_result.setZero();
        vector_result.setZero();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));

        const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
        const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto felt_space_dimension = displacement_ref_felt.GetFEltSpaceDimension();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {

            PrepareInternalDataForQuadraturePoint(infos_at_quad_pt,
                                                  geom_elt,
                                                  displacement_ref_felt,
                                                  pressure_ref_felt,
                                                  test_displacement_ref_felt,
                                                  test_pressure_ref_felt,
                                                  local_displacement_deviatoric,
                                                  local_pressure_deviatoric,
                                                  local_displacement_volumetric,
                                                  local_pressure_volumetric,
                                                  felt_space_dimension);
        }
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        PrepareInternalDataForQuadraturePoint(const InformationAtQuadraturePoint& infos_at_quad_pt,
                                              const GeometricElt& geom_elt,
                                              const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                                              const Eigen::VectorXd& local_displacement_deviatoric,
                                              const Eigen::VectorXd& local_pressure_deviatoric,
                                              const Eigen::VectorXd& local_displacement_volumetric,
                                              const Eigen::VectorXd& local_pressure_volumetric,
                                              const ::MoReFEM::GeometryNS::dimension_type felt_space_dimension)
    {
        switch (felt_space_dimension.Get())
        {
        case 2:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<2>(infos_at_quad_pt,
                                                                       geom_elt,
                                                                       displacement_ref_felt,
                                                                       pressure_ref_felt,
                                                                       test_displacement_ref_felt,
                                                                       test_pressure_ref_felt,
                                                                       local_displacement_deviatoric,
                                                                       local_pressure_deviatoric,
                                                                       local_displacement_volumetric,
                                                                       local_pressure_volumetric);
            break;
        }
        case 3:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<3>(infos_at_quad_pt,
                                                                       geom_elt,
                                                                       displacement_ref_felt,
                                                                       pressure_ref_felt,
                                                                       test_displacement_ref_felt,
                                                                       test_pressure_ref_felt,
                                                                       local_displacement_deviatoric,
                                                                       local_pressure_deviatoric,
                                                                       local_displacement_volumetric,
                                                                       local_pressure_volumetric);
            break;
        }
        default:
            assert(false);
        }
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<std::size_t FeltSpaceDimensionT>
    void DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        PrepareInternalDataForQuadraturePointForDimension(
            const InformationAtQuadraturePoint& infos_at_quad_pt,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
            const Eigen::VectorXd& local_displacement_deviatoric,
            const Eigen::VectorXd& local_pressure_deviatoric,
            const Eigen::VectorXd& local_displacement_volumetric,
            const Eigen::VectorXd& local_pressure_volumetric)
    {
        // ===================================================================================
        // Access to work matrices/vectors.
        // ===================================================================================
        auto& block_contribution =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();

        auto& dphi_test_disp_mult_column_matrix = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::dphi_test_disp_mult_column_matrix)>();
        auto& block_contribution_disp_pres = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::block_contribution_disp_pres)>();

        auto& row_matrix_mult_transposed_dphi_disp = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::row_matrix_mult_transposed_dphi_disp)>();
        auto& block_contribution_pres_disp = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::block_contribution_pres_disp)>();

        // ===================================================================================
        // Finite element related computations.
        // ===================================================================================

        const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

        decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
        decltype(auto) quad_pt_test_unknown_list_data = infos_at_quad_pt.GetTestUnknownData();

        const auto weight_meas = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

        const auto& grad_felt_phi = quad_pt_unknown_data.GetGradientFEltPhi();                // on (u p)
        const auto& test_grad_felt_phi = quad_pt_test_unknown_list_data.GetGradientFEltPhi(); // on (u* p*)

        const auto& dphi_displacement = displacement_ref_felt.ExtractSubMatrix(grad_felt_phi);

        const auto& dphi_test_displacement = test_displacement_ref_felt.ExtractSubMatrix(test_grad_felt_phi);

        const auto& phi = quad_pt_unknown_data.GetFEltPhi();                // on (u p)
        const auto& test_phi = quad_pt_test_unknown_list_data.GetFEltPhi(); // on (u* p*)

        const auto& pressure_phi = pressure_ref_felt.ExtractSubVector(phi);
        const auto& test_pressure_phi = test_pressure_ref_felt.ExtractSubVector(test_phi);

        // ===================================================================================
        // Residual terms
        // ===================================================================================

        // Deviatoric part.

        auto displacement_gradient_deviatoric = displacement_ref_felt.ComputeGradientDisplacementMatrix(
            quad_pt_unknown_data,
            local_displacement_deviatoric,
            ::MoReFEM::GeometryNS::dimension_type{ FeltSpaceDimensionT });

        auto& derivative_green_lagrange_deviatoric = GetNonCstDerivativeGreenLagrangeDeviatoric();

        const auto& derivative_green_lagrange_deviatoric_at_quad_point =
            derivative_green_lagrange_deviatoric.Update(displacement_gradient_deviatoric);

        const auto& cauchy_green_tensor_deviatoric = GetCauchyGreenTensorDeviatoric();
        const auto& cauchy_green_tensor_deviatoric_at_quad_point =
            cauchy_green_tensor_deviatoric.GetValue(quad_pt, geom_elt);


        GetNonCstHydrostaticLawDeviatoricPart().UpdateInvariants(
            cauchy_green_tensor_deviatoric_at_quad_point, quad_pt, geom_elt);


        // Volumetric part.
        auto displacement_gradient_volumetric = displacement_ref_felt.ComputeGradientDisplacementMatrix(
            quad_pt_unknown_data,
            local_displacement_volumetric,
            ::MoReFEM::GeometryNS::dimension_type{ FeltSpaceDimensionT });


        auto& derivative_green_lagrange_volumetric = GetNonCstDerivativeGreenLagrangeVolumetric();
        [[maybe_unused]] const auto& derivative_green_lagrange_volumetric_at_quad_point =
            derivative_green_lagrange_volumetric.Update(displacement_gradient_volumetric);

        const auto& cauchy_green_tensor_volumetric = GetCauchyGreenTensorVolumetric();
        const auto& cauchy_green_tensor_volumetric_at_quad_point =
            cauchy_green_tensor_volumetric.GetValue(quad_pt, geom_elt);

        GetNonCstHydrostaticLawVolumetricPart().UpdateInvariants(
            cauchy_green_tensor_volumetric_at_quad_point, quad_pt, geom_elt);


        const auto Nnode_pressure = pressure_ref_felt.Nnode();

        const double pressure_deviatoric_at_quad_point = pressure_phi.transpose() * local_pressure_deviatoric;
        const double pressure_volumetric_at_quad_point = pressure_phi.transpose() * local_pressure_volumetric;

        decltype(auto) hydrostatic_law_deviatoric = GetHydrostaticLawDeviatoricPart();

        const auto& dI3dC =
            hydrostatic_law_deviatoric.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

        const double inv3 = hydrostatic_law_deviatoric.template GetInvariant<::MoReFEM::InvariantNS::index::I3>();
        const double sqrt_inv3 = std::sqrt(inv3);

        ::MoReFEM::Wrappers::EigenNS::dWVector hydrostatic_stress;
        hydrostatic_stress.noalias() = -dI3dC / sqrt_inv3;

        ::MoReFEM::Wrappers::EigenNS::dWVector diff_hydrostatic_stress_wrt_pres_deviatoric;
        diff_hydrostatic_stress_wrt_pres_deviatoric.noalias() = hydrostatic_stress;

        hydrostatic_stress *= pressure_deviatoric_at_quad_point;

        ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9> rhs_disp;
        rhs_disp.noalias() = derivative_green_lagrange_deviatoric_at_quad_point.transpose() * hydrostatic_stress;

        const auto& hydrostatic_law_volumetric_part = GetHydrostaticLawVolumetricPart();

        const auto penalization_gradient = hydrostatic_law_volumetric_part.dWdI3(quad_pt, geom_elt);

        const double bulk = hydrostatic_law_volumetric_part.GetBulk().GetValue(quad_pt, geom_elt);
        const auto rhs_pres = penalization_gradient + pressure_volumetric_at_quad_point / bulk;

        // Fill vector_result.
        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ displacement_ref_felt.GetMeshDimension() };

        const auto Nnode_disp = displacement_ref_felt.Nnode();

        auto& elementary_data = GetNonCstElementaryData();
        auto& vector_result = elementary_data.GetNonCstVectorResult();

        // Residual on disp
        for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
        {
            const auto dof_first_index = test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);
            const auto component_first_index = row_component.Get() * Ncomponent.Get();

            // Compute the new contribution to vector_result here.
            // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
            for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_disp; ++row_node)
            {
                double value = 0.;

                for (::MoReFEM::GeometryNS::dimension_type col{}; col < Ncomponent; ++col)
                    value +=
                        dphi_test_displacement(row_node.Get(), col.Get()) * rhs_disp(col.Get() + component_first_index);

                vector_result(dof_first_index + row_node.Get()) += value * weight_meas;
            }
        }

        // Residual on pres
        const auto dof_first_index_pres = Nnode_disp.Get() * Ncomponent.Get();
        for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_pressure; ++row_node)
        {
            vector_result(dof_first_index_pres + row_node.Get()) +=
                rhs_pres * weight_meas * test_pressure_phi(row_node.Get());
        }

        // ===================================================================================
        // Tangent terms
        // ===================================================================================
        auto& matrix_result = elementary_data.GetNonCstMatrixResult();

        const auto Nnode_test_disp = test_displacement_ref_felt.Nnode();
        const auto Nnode_test_pressure = test_pressure_ref_felt.Nnode();

        // Tangent disp disp (deviatoric).
        const auto& d2I3dCdC =
            hydrostatic_law_deviatoric.template GetSecondDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

        ::MoReFEM::Wrappers::EigenNS::d2WMatrix hydrostatic_tangent = dI3dC * dI3dC.transpose(); // pJC^-1 tangent

        hydrostatic_tangent *= (pressure_deviatoric_at_quad_point * NumericNS::Pow(inv3, -1.5));

        hydrostatic_tangent.noalias() -= 2 * sqrt_inv3 * pressure_deviatoric_at_quad_point * d2I3dCdC;

        auto linear_part = Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeLinearPart(
            derivative_green_lagrange_deviatoric_at_quad_point, hydrostatic_tangent);

        auto tangent_matrix_disp_disp =
            Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeNonLinearPart<
                FeltSpaceDimensionT>(hydrostatic_stress);

        tangent_matrix_disp_disp.noalias() += linear_part;

        // Fill disp disp block of matrix_result.
        for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
        {
            const auto row_first_index = test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);

            auto row_slicing = Eigen::seqN(row_component.Get() * Ncomponent.Get(), Ncomponent.Get());

            for (::MoReFEM::GeometryNS::dimension_type col_component{}; col_component < Ncomponent; ++col_component)
            {
                const auto col_first_index = displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                auto col_slicing = Eigen::seqN(col_component.Get() * Ncomponent.Get(), Ncomponent.Get());
                ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> gradient_based_block =
                    tangent_matrix_disp_disp(row_slicing, col_slicing);


                work_matrix_1_.noalias() = gradient_based_block * dphi_displacement.transpose();

                block_contribution.noalias() = weight_meas * dphi_test_displacement * work_matrix_1_;

                for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test_disp; ++row_node)
                {
                    for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode_disp; ++col_node)
                    {

                        matrix_result(row_first_index + row_node.Get(), col_first_index + col_node.Get()) +=
                            block_contribution(row_node.Get(), col_node.Get());
                    }
                }
            }
        }

        // Tangent disp pres (deviatoric).
        ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9> tangent_vector_disp_pres;
        tangent_vector_disp_pres.noalias() = derivative_green_lagrange_deviatoric_at_quad_point.transpose()
                                             * diff_hydrostatic_stress_wrt_pres_deviatoric;

        const auto col_first_index_pressure = Ncomponent.Get() * Nnode_disp.Get();
        const auto row_first_index_pressure = Ncomponent.Get() * Nnode_test_disp.Get();

        // Fill disp pres block of matrix_result.
        for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
        {
            const auto row_first_index_disp =
                test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);

            ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<3> column_matrix =
                tangent_vector_disp_pres(Eigen::seqN(row_component.Get() * Ncomponent.Get(), Ncomponent.Get()));

            dphi_test_disp_mult_column_matrix.noalias() = weight_meas * dphi_test_displacement * column_matrix;

            for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test_disp; ++row_node)
            {
                for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode_pressure; ++col_node)
                {
                    block_contribution_disp_pres(row_node.Get(), col_node.Get()) =
                        dphi_test_disp_mult_column_matrix(row_node.Get(), 0) * pressure_phi(col_node.Get());
                }
            }

            for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test_disp; ++row_node)
            {
                for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode_pressure; ++col_node)
                    matrix_result(row_first_index_disp + row_node.Get(), col_first_index_pressure + col_node.Get()) +=
                        block_contribution_disp_pres(row_node.Get(), col_node.Get());
            }
        }

        // Tangent pres disp (volumetric).
        const auto& dI3dC_volumetric =
            hydrostatic_law_volumetric_part
                .template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

        const double inv3_volumetric =
            hydrostatic_law_volumetric_part.template GetInvariant<::MoReFEM::InvariantNS::index::I3>();

        const double sqrt_inv3_volumetric = std::sqrt(inv3_volumetric);

        ::MoReFEM::Wrappers::EigenNS::dWVector diff_hydrostatic_stress_wrt_pres_volumetric;
        diff_hydrostatic_stress_wrt_pres_volumetric.noalias() = dI3dC_volumetric / sqrt_inv3_volumetric;

        ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9> tangent_vector_pres_disp;
        tangent_vector_pres_disp.noalias() = derivative_green_lagrange_volumetric_at_quad_point.transpose()
                                             * diff_hydrostatic_stress_wrt_pres_volumetric;

        const auto penalization_scd_deriv = hydrostatic_law_volumetric_part.d2WdI3(quad_pt, geom_elt);

        tangent_vector_pres_disp *= 2. * sqrt_inv3_volumetric * penalization_scd_deriv;

        // Fill pres disp block of matrix_result.
        for (::MoReFEM::GeometryNS::dimension_type col_component{}; col_component < Ncomponent; ++col_component)
        {
            const auto col_first_index_disp = displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component);

            ::MoReFEM::Wrappers::EigenNS::RowVectorMaxNd<3> row_matrix =
                tangent_vector_pres_disp(Eigen::seqN(col_component.Get() * Ncomponent.Get(), Ncomponent.Get()));

            row_matrix_mult_transposed_dphi_disp.noalias() = weight_meas * row_matrix * dphi_displacement.transpose();

            for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test_pressure; ++row_node)
            {
                for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode_disp; ++col_node)
                {
                    block_contribution_pres_disp(row_node.Get(), col_node.Get()) =
                        test_pressure_phi(row_node.Get()) * row_matrix_mult_transposed_dphi_disp(0, col_node.Get());
                }
            }

            for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test_pressure; ++row_node)
            {
                for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode_disp; ++col_node)
                    matrix_result(row_first_index_pressure + row_node.Get(), col_first_index_disp + col_node.Get()) +=
                        block_contribution_pres_disp(row_node.Get(), col_node.Get());
            }
        }

        // Tangent pres pres (volumetric).
        const double tangent_matrix_pres_pres = weight_meas / bulk;

        // Fill pres pres block of matrix_result.
        for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test_pressure; ++row_node)
        {
            for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode_pressure; ++col_node)
                matrix_result(row_first_index_pressure + row_node.Get(), col_first_index_pressure + col_node.Get()) +=
                    tangent_matrix_pres_pres * test_pressure_phi(row_node.Get()) * pressure_phi(col_node.Get());
        }
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const HydrostaticLawPolicyT&
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetHydrostaticLawVolumetricPart() const noexcept
    {
        assert(!(!hydrostatic_law_volumetric_part_)
               && "Please check what was provided in the constructor of the law that used up current policy!");
        return *hydrostatic_law_volumetric_part_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const HydrostaticLawPolicyT&
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetHydrostaticLawDeviatoricPart() const noexcept
    {
        assert(!(!hydrostatic_law_deviatoric_part_)
               && "Please check what was provided in the constructor of the law that used up current policy!");
        return *hydrostatic_law_deviatoric_part_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline HydrostaticLawPolicyT&
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstHydrostaticLawVolumetricPart() noexcept
    {
        return const_cast<HydrostaticLawPolicyT&>(GetHydrostaticLawVolumetricPart());
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline HydrostaticLawPolicyT&
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstHydrostaticLawDeviatoricPart() noexcept
    {
        return const_cast<HydrostaticLawPolicyT&>(GetHydrostaticLawDeviatoricPart());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::SetCauchyGreenTensor(
        const cauchy_green_tensor_type* param)
    {
        assert(cauchy_green_tensor_volumetric_ == nullptr && "Should be called only once.");
        cauchy_green_tensor_volumetric_ = param;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetCauchyGreenTensorVolumetric() const noexcept
        -> const cauchy_green_tensor_type&
    {
        assert(!(!cauchy_green_tensor_volumetric_));
        return *cauchy_green_tensor_volumetric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetFormerLocalDisplacementDeviatoric() const noexcept
        -> const Eigen::VectorXd&
    {
        return former_local_displacement_deviatoric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstFormerLocalDisplacementDeviatoric() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalDisplacementDeviatoric());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetFormerLocalPressureDeviatoric() const noexcept
        -> const Eigen::VectorXd&
    {
        return former_local_pressure_deviatoric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstFormerLocalPressureDeviatoric() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalPressureDeviatoric());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetFormerLocalDisplacementVolumetric() const noexcept
        -> const Eigen::VectorXd&
    {
        return former_local_displacement_volumetric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstFormerLocalDisplacementVolumetric() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalDisplacementVolumetric());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetFormerLocalPressureVolumetric() const noexcept
        -> const Eigen::VectorXd&
    {
        return former_local_pressure_volumetric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstFormerLocalPressureVolumetric() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalPressureVolumetric());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetCauchyGreenTensorDeviatoric() const noexcept
        -> const cauchy_green_tensor_type&
    {
        return cauchy_green_tensor_deviatoric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstDerivativeGreenLagrangeDeviatoric() noexcept
    {
        assert(!(!deriv_green_lagrange_deviatoric_));
        return *deriv_green_lagrange_deviatoric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                    TimeManagerT>::GetNonCstDerivativeGreenLagrangeVolumetric() noexcept
    {
        assert(!(!deriv_green_lagrange_volumetric_));
        return *deriv_green_lagrange_volumetric_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
