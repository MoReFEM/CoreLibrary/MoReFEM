// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"

namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Mixed solid incompressibility.
     *
     * Used as penalization for incompressible hyperelasticity.
     *
     */
    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class SameCauchyGreenMixedSolidIncompressibility final
    : public NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>,
      public Crtp::LocalMatrixStorage<SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>,
                                      4UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Type of the elementary matrix.
        using matrix_type = Eigen::MatrixXd;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 4UL>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Alias to CauchyGreenTensor.
        using cauchy_green_tensor_type = ParameterAtQuadraturePoint<ParameterNS::Type::vector,
                                                                    TimeManagerT,
                                                                    ParameterNS::TimeDependencyNS::None,
                                                                    ::MoReFEM::Wrappers::EigenNS::dWVector>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         *
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] hydrostatic_law Hydrostatic law policy; do not delete this pointer!
         *
         * \param[in] cauchy_green_tensor
         * Cauchy-Green tensor of the related second Piola-Kirchhoff stress tensor.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit SameCauchyGreenMixedSolidIncompressibility(
            const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
            elementary_data_type&& elementary_data,
            const cauchy_green_tensor_type& cauchy_green_tensor,
            const HydrostaticLawPolicyT* hydrostatic_law,
            const HydrostaticLawPolicyT*);

        //! Destructor.
        virtual ~SameCauchyGreenMixedSolidIncompressibility() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        SameCauchyGreenMixedSolidIncompressibility(const SameCauchyGreenMixedSolidIncompressibility& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SameCauchyGreenMixedSolidIncompressibility(SameCauchyGreenMixedSolidIncompressibility&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SameCauchyGreenMixedSolidIncompressibility&
        operator=(const SameCauchyGreenMixedSolidIncompressibility& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SameCauchyGreenMixedSolidIncompressibility&
        operator=(SameCauchyGreenMixedSolidIncompressibility&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }

        //! Constant accessor to the former local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalDisplacement() const noexcept;

        //! Non constant accessor to the former local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalDisplacement() noexcept;

        //! Constant accessor to the former local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalPressure() const noexcept;

        //! Non constant accessor to the former local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalPressure() noexcept;

      private:
        //! Get Cauchy-Green tensor \a Parameter.
        const cauchy_green_tensor_type& GetCauchyGreenTensor() const noexcept;

        //! Helper class to manage computation of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>& GetNonCstDerivativeGreenLagrange() noexcept;

        /*!
         * \brief Compute internal data such as invariants, De, Cauchy-Green tensor for current quadrature
         * point.
         *
         * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
         * point, such as the geometric and finite element shape function values. Quadrature points related to
         * functions are considered here.
         * \param[in] felt_space_dimension Dimension of the \a FEltSpace.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] local_displacement Local displacement observed within the finite element at previous time
         * iteration.
         * \param[in] local_pressure Local pressure observed within the finite element at previous time
         * iteration.
         * \param[in] displacement_ref_felt Reference finite element for displacement.
         * \param[in] pressure_ref_felt Reference finite element for pressure.
         * \param[in] test_displacement_ref_felt Test reference finite element for displacement.
         * \param[in] test_pressure_ref_felt Test reference finite element for pressure.
         */
        void PrepareInternalDataForQuadraturePoint(const InformationAtQuadraturePoint& infos_at_quad_pt,
                                                   const GeometricElt& geom_elt,
                                                   const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                                                   const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                                                   const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                                                   const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                                                   const Eigen::VectorXd& local_displacement,
                                                   const Eigen::VectorXd& local_pressure,
                                                   const ::MoReFEM::GeometryNS::dimension_type felt_space_dimension);

        /*!
         * \brief Manage the ComputeEltArray part of the calculation that is dependent on the dimension
         * considered.
         *
         *  \tparam MeshDimensionT Dimension of the mesh.
         *
         * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
         * point, such as the geometric and finite element shape function values. Quadrature points related to
         * functions are considered here.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] local_displacement Local displacement observed within the finite element at previous time
         * iteration.
         * \param[in] local_pressure Local pressure observed within the finite element at previous time
         * iteration.
         * \param[in] displacement_ref_felt Reference finite element for displacement.
         * \param[in] pressure_ref_felt Reference finite element for pressure.
         * \param[in] test_displacement_ref_felt Test reference finite element for displacement.
         * \param[in] test_pressure_ref_felt Test reference finite element for pressure.
         */
        template<std::size_t FeltSpaceDimensionT>
        void PrepareInternalDataForQuadraturePointForDimension(
            const InformationAtQuadraturePoint& infos_at_quad_pt,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
            const Eigen::VectorXd& local_displacement,
            const Eigen::VectorXd& local_pressure);


      private:
        //! Underlying hydrostatic law.
        const HydrostaticLawPolicyT& GetHydrostaticLaw() const noexcept;

        //! Underlying hydrostatic law.
        HydrostaticLawPolicyT& GetNonCstHydrostaticLaw() noexcept;

      private:
        //! Helper class to manage computation of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>::unique_ptr deriv_green_lagrange_ = nullptr;

        /*!
         * \brief Pointer to the Cauchy-Green tensor \a Parameter.
         *
         * Do not release this pointer: it should point to an object which is already handled correctly.
         *
         * This must be set with SetCauchyGreenTensor().
         */
        const cauchy_green_tensor_type& cauchy_green_tensor_;


        /*!
         * \brief Displacement obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray. \endinternal
         */
        Eigen::VectorXd former_local_displacement_;

        /*!
         * \brief Pressure obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd former_local_pressure_;

      private:
        //! Underlying hydrostatic law.
        const HydrostaticLawPolicyT* const hydrostatic_law_;

        /*!
         * \brief An internal work matrix.
         *
         * \attention It is a data attribute only to avoid allocating memory while crushing numbers...
         *
         * Its dimension is (Ncomponent, Nnode_displacement).
         *
         * **Never** use it to propagate information between two methods: the idea is really to help *within one given
         * method* to perform a specific compulation without allocating memory in the process.
         *
         */
        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor, 3, Eigen::Dynamic> work_matrix_1_;


        /*!
         * \brief An internal work vector.
         *
         * \attention It is a data attribute only to avoid allocating memory while crushing numbers...
         *
         * Its dimension is (Nnode_displacement).
         *
         * **Never** use it to propagate information between two methods: the idea is really to help *within one given
         * method* to perform a specific compulation without allocating memory in the process.
         */
        Eigen::VectorXd work_vector_disp_;

        /*!
         * \brief An internal work vector.
         *
         * \attention It is a data attribute only to avoid allocating memory while crushing numbers...
         *
         * Its dimension is (Nnode_test_displacement).
         *
         * **Never** use it to propagate information between two methods: the idea is really to help *within one given
         * method* to perform a specific compulation without allocating memory in the process.
         */
        Eigen::VectorXd work_vector_test_disp_;


      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes related to local matrices.
        enum class LocalMatrixIndex : std::size_t {
            displacement_displacement_contribution = 0,
            pressure_displacement_contribution,
            pressure_pressure_contribution,
            displacement_pressure_contribution
        };
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
