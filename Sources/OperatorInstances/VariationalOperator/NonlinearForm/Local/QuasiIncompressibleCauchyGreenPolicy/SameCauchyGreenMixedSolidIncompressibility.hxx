// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        SameCauchyGreenMixedSolidIncompressibility(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                                   elementary_data_type&& a_elementary_data,
                                                   const cauchy_green_tensor_type& cauchy_green_tensor,
                                                   const HydrostaticLawPolicyT* hydrostatic_law,
                                                   [[maybe_unused]] const HydrostaticLawPolicyT* should_be_nullptr)
    : NonlinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent(),
      cauchy_green_tensor_(cauchy_green_tensor), hydrostatic_law_(hydrostatic_law)
    {
        assert(should_be_nullptr == nullptr);
        const auto& elementary_data = GetElementaryData();

        const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
        const auto Nnode_displacement = displacement_ref_felt.Nnode();

        const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
        const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));

        const auto Nnode_test_displacement = test_displacement_ref_felt.Nnode();
        const auto Nnode_test_pressure = test_pressure_ref_felt.Nnode();
        const auto Nnode_pressure = pressure_ref_felt.Nnode();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ elementary_data.GetGeomEltDimension() };

        former_local_displacement_.resize(elementary_data.GetRefFElt(GetNthUnknown(0)).Ndof());

        former_local_pressure_.resize(elementary_data.GetRefFElt(GetNthUnknown(1)).Ndof());

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_test_displacement.Get(), Nnode_displacement.Get() }, // displacement_displacement_contribution
            { Nnode_test_pressure.Get(), Nnode_displacement.Get() },     // pressure_displacement_contribution
            { Nnode_test_pressure.Get(), Nnode_pressure.Get() },         // pressure_pressure_contribution
            { Nnode_test_displacement.Get(), Nnode_pressure.Get() },     // pressure_displacement_contribution

        } });

        work_matrix_1_.resize(Ncomponent.Get(), Nnode_displacement.Get());
        work_vector_disp_.resize(Nnode_displacement.Get());
        work_vector_test_disp_.resize(Nnode_test_displacement.Get());

        deriv_green_lagrange_ =
            std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(Ncomponent);
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ClassName()
    {
        static std::string name("SameCauchyGreenMixedSolidIncompressibility");
        return name;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetFormerLocalDisplacement()
        const noexcept -> const Eigen::VectorXd&
    {
        return former_local_displacement_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                           TimeManagerT>::GetNonCstFormerLocalDisplacement() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalDisplacement());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetFormerLocalPressure()
        const noexcept -> const Eigen::VectorXd&
    {
        return former_local_pressure_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                           TimeManagerT>::GetNonCstFormerLocalPressure() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalPressure());
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetCauchyGreenTensor()
        const noexcept -> const cauchy_green_tensor_type&
    {
        return cauchy_green_tensor_;
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                               TimeManagerT>::GetNonCstDerivativeGreenLagrange() noexcept
    {
        assert(!(!deriv_green_lagrange_));
        return *deriv_green_lagrange_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        const auto& local_displacement = GetFormerLocalDisplacement();
        const auto& local_pressure = GetFormerLocalPressure();

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        auto& vector_result = elementary_data.GetNonCstVectorResult();
        matrix_result.setZero();
        vector_result.setZero();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));

        const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
        const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto felt_space_dimension = displacement_ref_felt.GetFEltSpaceDimension();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            PrepareInternalDataForQuadraturePoint(infos_at_quad_pt,
                                                  geom_elt,
                                                  displacement_ref_felt,
                                                  pressure_ref_felt,
                                                  test_displacement_ref_felt,
                                                  test_pressure_ref_felt,
                                                  local_displacement,
                                                  local_pressure,
                                                  felt_space_dimension);
        }
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        PrepareInternalDataForQuadraturePoint(const InformationAtQuadraturePoint& infos_at_quad_pt,
                                              const GeometricElt& geom_elt,
                                              const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                                              const Eigen::VectorXd& local_displacement,
                                              const Eigen::VectorXd& local_pressure,
                                              const ::MoReFEM::GeometryNS::dimension_type felt_space_dimension)
    {
        switch (felt_space_dimension.Get())
        {
        case 2:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<2>(infos_at_quad_pt,
                                                                       geom_elt,
                                                                       displacement_ref_felt,
                                                                       pressure_ref_felt,
                                                                       test_displacement_ref_felt,
                                                                       test_pressure_ref_felt,
                                                                       local_displacement,
                                                                       local_pressure);
            break;
        }
        case 3:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<3>(infos_at_quad_pt,
                                                                       geom_elt,
                                                                       displacement_ref_felt,
                                                                       pressure_ref_felt,
                                                                       test_displacement_ref_felt,
                                                                       test_pressure_ref_felt,
                                                                       local_displacement,
                                                                       local_pressure);
            break;
        }
        default:
            assert(false);
        }
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<std::size_t FeltSpaceDimensionT>
    void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        PrepareInternalDataForQuadraturePointForDimension(
            const InformationAtQuadraturePoint& infos_at_quad_pt,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
            const Eigen::VectorXd& local_displacement,
            const Eigen::VectorXd& local_pressure)
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        auto& vector_result = elementary_data.GetNonCstVectorResult();

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ displacement_ref_felt.GetMeshDimension() };

        const auto Nnode_disp = displacement_ref_felt.Nnode();
        const auto Nnode_pressure = pressure_ref_felt.Nnode();

        const auto Nnode_test_disp = test_displacement_ref_felt.Nnode();
        const auto Nnode_test_pressure = test_pressure_ref_felt.Nnode();

        const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

        decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
        decltype(auto) quad_pt_test_unknown_list_data = infos_at_quad_pt.GetTestUnknownData();

        const auto weight_meas = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

        const auto& grad_felt_phi = quad_pt_unknown_data.GetGradientFEltPhi();                // on (u p)
        const auto& test_grad_felt_phi = quad_pt_test_unknown_list_data.GetGradientFEltPhi(); // on (u* p*)

        const auto& dphi_displacement = displacement_ref_felt.ExtractSubMatrix(grad_felt_phi);

        const auto& dphi_test_displacement = test_displacement_ref_felt.ExtractSubMatrix(test_grad_felt_phi);

        const auto& phi = quad_pt_unknown_data.GetFEltPhi();                // on (u p)
        const auto& test_phi = quad_pt_test_unknown_list_data.GetFEltPhi(); // on (u* p*)

        const auto& pressure_phi = pressure_ref_felt.ExtractSubVector(phi);
        const auto& test_pressure_phi = test_pressure_ref_felt.ExtractSubVector(test_phi);

        auto displacement_gradient = displacement_ref_felt.ComputeGradientDisplacementMatrix(
            quad_pt_unknown_data, local_displacement, ::MoReFEM::GeometryNS::dimension_type{ FeltSpaceDimensionT });

        auto& derivative_green_lagrange = GetNonCstDerivativeGreenLagrange();
        const auto& derivative_green_lagrange_at_quad_point = derivative_green_lagrange.Update(displacement_gradient);
        const auto& cauchy_green_tensor = GetCauchyGreenTensor();
        const auto& cauchy_green_tensor_at_quad_point = cauchy_green_tensor.GetValue(quad_pt, geom_elt);

        GetNonCstHydrostaticLaw().UpdateInvariants(cauchy_green_tensor_at_quad_point, quad_pt, geom_elt);

        const double pressure_at_quad_point = pressure_phi.transpose() * local_pressure;

        decltype(auto) hydrostatic_law = GetHydrostaticLaw();

        const auto& dI3dC =
            hydrostatic_law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

        const double inv3 = hydrostatic_law.template GetInvariant<::MoReFEM::InvariantNS::index::I3>();
        const double sqrt_inv3 = std::sqrt(inv3);

        ::MoReFEM::Wrappers::EigenNS::dWVector hydrostatic_stress;
        hydrostatic_stress = dI3dC;

        ::MoReFEM::Wrappers::EigenNS::dWVector diff_hydrostatic_stress_wrt_pres;
        diff_hydrostatic_stress_wrt_pres.noalias() = hydrostatic_stress / -sqrt_inv3;

        hydrostatic_stress *= -pressure_at_quad_point / sqrt_inv3;

        ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9> rhs_disp;
        rhs_disp.noalias() = derivative_green_lagrange_at_quad_point.transpose() * hydrostatic_stress;

        const auto penalization_gradient = hydrostatic_law.dWdI3(quad_pt, geom_elt);

        const double bulk = hydrostatic_law.GetBulk().GetValue(quad_pt, geom_elt);
        const auto rhs_pres = penalization_gradient + pressure_at_quad_point / bulk;

        // Residual on disp
        for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
        {
            const auto dof_first_index = test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);
            const auto component_first_index = row_component.Get() * Ncomponent.Get();

            // Compute the new contribution to vector_result here.
            // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
            for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_disp; ++row_node)
            {
                const double value = dphi_test_displacement(row_node.Get(), Eigen::all)
                                     * rhs_disp(Eigen::seqN(component_first_index, Ncomponent.Get()));

                vector_result(dof_first_index + row_node.Get()) += value * weight_meas;
            }
        }

        // Residual on pres
        const auto dof_first_index_pres = Nnode_disp.Get() * Ncomponent.Get();
        for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_pressure; ++row_node)
        {
            vector_result(dof_first_index_pres + row_node.Get()) +=
                rhs_pres * weight_meas * test_pressure_phi(row_node.Get());
        }

        // Derivative disp disp
        const auto& d2I3dCdC =
            hydrostatic_law.template GetSecondDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

        ::MoReFEM::Wrappers::EigenNS::d2WMatrix hydrostatic_tangent = dI3dC * dI3dC.transpose(); // pJC^-1 tangent

        hydrostatic_tangent *= (pressure_at_quad_point * NumericNS::Pow(inv3, -1.5));
        hydrostatic_tangent.noalias() -= 2 * sqrt_inv3 * pressure_at_quad_point * d2I3dCdC;

        auto linear_part = Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeLinearPart(
            derivative_green_lagrange_at_quad_point, hydrostatic_tangent);

        auto tangent_matrix_disp_disp =
            Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeNonLinearPart<
                FeltSpaceDimensionT>(hydrostatic_stress);

        tangent_matrix_disp_disp.noalias() += linear_part;

        // Derivative disp pres
        ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9> tangent_vector_disp_pres;
        tangent_vector_disp_pres.noalias() =
            derivative_green_lagrange_at_quad_point.transpose() * diff_hydrostatic_stress_wrt_pres;

        // Derivative pres disp
        const auto penalization_scd_deriv = hydrostatic_law.d2WdI3(quad_pt, geom_elt);

        ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9> tangent_vector_pres_disp;
        tangent_vector_pres_disp.noalias() = tangent_vector_disp_pres;
        tangent_vector_pres_disp *= -2. * sqrt_inv3 * penalization_scd_deriv;

        // Tangent disp disp
        {
            auto& contribution = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                LocalMatrixIndex::displacement_displacement_contribution)>();

            for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
            {
                const auto row_first_index = test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);
                const auto row_slicing = Eigen::seqN(row_first_index, Nnode_test_disp.Get());
                const auto row_component_slicing =
                    Eigen::seqN(row_component.Get() * Ncomponent.Get(), Ncomponent.Get());

                for (::MoReFEM::GeometryNS::dimension_type col_component{}; col_component < Ncomponent; ++col_component)
                {
                    const auto col_first_index = displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component);
                    auto col_slicing = Eigen::seqN(col_first_index, Nnode_disp.Get());

                    const auto col_component_slicing =
                        Eigen::seqN(col_component.Get() * Ncomponent.Get(), Ncomponent.Get());

                    work_matrix_1_.noalias() = tangent_matrix_disp_disp(row_component_slicing, col_component_slicing)
                                               * dphi_displacement.transpose();
                    contribution.noalias() = weight_meas * dphi_test_displacement * work_matrix_1_;

                    matrix_result(row_slicing, col_slicing) += contribution;
                }
            }
        }

        // Tangent disp pres
        {
            const auto col_first_index_pressure = Ncomponent.Get() * Nnode_disp.Get();
            const auto col_slicing = Eigen::seqN(col_first_index_pressure, Nnode_pressure.Get());

            for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
            {
                const auto row_first_index_disp =
                    test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);

                {
                    const auto component_slicing =
                        Eigen::seqN(row_component.Get() * Ncomponent.Get(), Ncomponent.Get());
                    work_vector_test_disp_.noalias() =
                        weight_meas * dphi_test_displacement * tangent_vector_disp_pres(component_slicing);
                }

                auto& contribution = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                    LocalMatrixIndex::displacement_pressure_contribution)>();

                ::MoReFEM::Wrappers::EigenNS::BroadcastTwoVectorsIntoMatrix(
                    work_vector_test_disp_, pressure_phi, contribution);

                const auto row_slicing = Eigen::seqN(row_first_index_disp, Nnode_test_disp.Get());
                matrix_result(row_slicing, col_slicing) += contribution;
            }
        }

        // Tangent pres disp
        {
            const auto row_first_index_pressure = Ncomponent.Get() * Nnode_test_disp.Get();

            auto& pressure_displacement_contribution = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                LocalMatrixIndex::pressure_displacement_contribution)>();

            auto row_slicing = Eigen::seqN(row_first_index_pressure, Nnode_test_pressure.Get());

            for (::MoReFEM::GeometryNS::dimension_type col_component{}; col_component < Ncomponent; ++col_component)
            {
                const auto col_first_index_disp = displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component);
                auto col_slicing = Eigen::seqN(col_first_index_disp, Nnode_disp.Get());

                ::MoReFEM::Wrappers::EigenNS::RowVectorMaxNd<3> row_matrix =
                    tangent_vector_pres_disp(Eigen::seqN(col_component.Get() * Ncomponent.Get(), Ncomponent.Get()));

                work_vector_disp_.noalias() = weight_meas * row_matrix * dphi_displacement.transpose();

                ::MoReFEM::Wrappers::EigenNS::BroadcastTwoVectorsIntoMatrix(
                    test_pressure_phi, work_vector_disp_, pressure_displacement_contribution);
                matrix_result(row_slicing, col_slicing) += pressure_displacement_contribution;
            }
        }

        // Tangent pres pres
        {
            const double tangent_matrix_pres_pres = weight_meas / bulk;
            const auto row_first_index_pressure = Ncomponent.Get() * Nnode_test_disp.Get();

            const auto row_slicing = Eigen::seqN(row_first_index_pressure, Nnode_test_pressure.Get());
            const auto col_slicing = Eigen::seqN(row_first_index_pressure, Nnode_pressure.Get());

            auto& pres_pres_contribution = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                LocalMatrixIndex::pressure_pressure_contribution)>();

            ::MoReFEM::Wrappers::EigenNS::BroadcastTwoVectorsIntoMatrix(
                test_pressure_phi, pressure_phi, pres_pres_contribution);

            matrix_result(row_slicing, col_slicing) += tangent_matrix_pres_pres * pres_pres_contribution;
        }
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const HydrostaticLawPolicyT&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetHydrostaticLaw() const noexcept
    {
        assert(!(!hydrostatic_law_)
               && "Please check what was provided in the constructor of the law that used up current policy!");
        return *hydrostatic_law_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline HydrostaticLawPolicyT&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetNonCstHydrostaticLaw() noexcept
    {
        return const_cast<HydrostaticLawPolicyT&>(GetHydrostaticLaw());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
