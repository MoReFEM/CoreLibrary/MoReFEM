// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"

#include "Operators/Enum.hpp"
#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
#include "Operators/LocalVariationalOperator/ElementaryData.hpp"                    // IWYU pragma: export
#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp" // IWYU pragma: export

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Define the operator used in the hyperelastic problem.
     *
     * \todo Improve the comment by writing its mathematical definition!
     *
     * \tparam HyperelasticityPolicyT Policy that defines if an hyperelastic contribution is present in the
     * tensor. \tparam ViscoelasticityPolicyT Policy that defines if an viscoelastic contribution is present in
     * the tensor. \tparam InternalVariablePolicyT Policy that defines if an active stress contribution is
     * present in the tensor.
     *
     */
    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class SecondPiolaKirchhoffStressTensor final
    : public HyperelasticityPolicyT,
      public ViscoelasticityPolicyT,
      public InternalVariablePolicyT,
      public NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>,
      public Crtp::LocalMatrixStorage<SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                                       ViscoelasticityPolicyT,
                                                                       InternalVariablePolicyT,
                                                                       TimeManagerT>,
                                      2UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                      ViscoelasticityPolicyT,
                                                      InternalVariablePolicyT,
                                                      TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Type of the elementary matrix.
        using matrix_type = Eigen::MatrixXd;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to variational operator parent.
        using parent = NonlinearLocalVariationalOperator<Eigen::MatrixXd, Eigen::VectorXd>;

        //! Alias for tangent matrix type.
        //! Such a matrix with relatively low maximum size is allocated on the stack.
        using tangent_matrix_type = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor, 9, 9>;

        //! Alias for tangent vector type.
        //! Such a vector with relatively low maximum size is allocated on the stack.
        using rhs_type = Eigen::Matrix<double, Eigen::Dynamic, 1, Eigen::ColMajor, 9>;


        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 2UL>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Rejuvenate alias from parent class.
        using elementary_data_type = typename parent::elementary_data_type;

        //! Alias to the type of the input of the active stress policy.
        using input_internal_variable_policy_type =
            typename InternalVariablePolicyT::input_internal_variable_policy_type;

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using cauchy_green_tensor_type = ParameterAtQuadraturePoint<ParameterNS::Type::vector,
                                                                    TimeManagerT,
                                                                    ParameterNS::TimeDependencyNS::None,
                                                                    ::MoReFEM::Wrappers::EigenNS::dWVector>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly one vectorial unknown (as long as #7 is not implemented at the very least...)
         *
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         *
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] solid Object which provides the required material parameters for the solid.
         * \param[in] hyperelastic_law The hyperelastic law if there is an  hyperelastic part in the operator, or nullptr otherwise.
         * Example of hyperelasticlaw is MoReFEM::HyperelasticLawNS::CiarletGeymonat.
         * \copydoc doxygen_hide_time_manager_arg
         * \param[in] input_internal_variable_policy Object involved with the internal variable (such as active stress)
         * policy.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit SecondPiolaKirchhoffStressTensor(
            const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
            elementary_data_type&& elementary_data,
            const Solid<TimeManagerT>& solid,
            const TimeManagerT& time_manager,
            const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
            input_internal_variable_policy_type* input_internal_variable_policy);

        //! Destructor.
        virtual ~SecondPiolaKirchhoffStressTensor() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        SecondPiolaKirchhoffStressTensor(const SecondPiolaKirchhoffStressTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SecondPiolaKirchhoffStressTensor(SecondPiolaKirchhoffStressTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SecondPiolaKirchhoffStressTensor& operator=(const SecondPiolaKirchhoffStressTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SecondPiolaKirchhoffStressTensor& operator=(SecondPiolaKirchhoffStressTensor&& rhs) = delete;


        ///@}


        /*!
         * \brief Compute the elementary matrix and/or vector.
         */
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


        //! Constant accessor to the former local displacement required by ComputeEltArray().
        const Eigen::VectorXd& GetFormerLocalDisplacement() const noexcept;

        //! Non constant accessor to the former local displacement required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstFormerLocalDisplacement() noexcept;

        //! Method to update internal variables of the active stress policy.
        void UpdateInternalVariableInternalVariables();

        //! Set Cauchy-Green tensor \a Parameter. Must be called only once.
        //! \param[in] param The parameter computed outside of the class.
        void SetCauchyGreenTensor(const cauchy_green_tensor_type* param);

        //! Accessor to tangent matrix.
        const tangent_matrix_type& GetTangentMatrix() const noexcept;

        //! Accessor to rhs .
        const rhs_type& GetRhs() const noexcept;


      private:
        //! Convenient alias to drop namespace specifications.
        using InformationAtQuadraturePoint =
            ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint;

        //! Non constant accessor to tangent matrix.
        tangent_matrix_type& GetNonCstTangentMatrix() noexcept;


        //! Non constant accessor to rhs .
        rhs_type& GetNonCstRhs() noexcept;


        /*!
         * \brief Part of ComputeEltArray once all the dimension depending operations have been performed.
         *
         * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature
         * point, such as the geometric and finite element shape function values. Quadrature points related to
         * functions are considered here.
         * \param[in,out] elementary_data Objects which includes relevant elementary data. It might be modified
         in
         * only one way: it contains the local matrices and vectors accessible through GetNonCstMatrixResult()
         and
         * GetNonCstVectorResult() respectively.
         */
        void ComputeAtQuadraturePoint(const InformationAtQuadraturePoint& infos_at_quad_pt,
                                      elementary_data_type& elementary_data) const;


        /*!
         * \class doxygen_hide_dW_d2W_derivates_arg
         *
         * \param[out] dW \a GlobalVector which stores the first derivates of W.
         * \param[out] d2W \a GlobalMatrix which stores the second derivates of W.
         */

        /*!
         * \brief Compute the derivates of W.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         * Test unknowns aren't considered at this point.
         * \param[in] ref_felt Reference finite element considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         *
         * \copydoc doxygen_hide_de_arg
         *
         * \copydoc doxygen_hide_dW_d2W_derivates_arg
         */
        template<Eigen::Index DimensionT>
        void ComputeWDerivates(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt,
                               const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
                               ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                               ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);

        /*!
         * \brief Compute internal data such as invariants, De, Cauchy-Green tensor for current quadrature
         * point.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] local_displacement Local displacement in observed within the finite element at previous
         * time iteration. \param[in] ref_felt Reference finite element considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         */
        void PrepareInternalDataForQuadraturePoint(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                                                   const GeometricElt& geom_elt,
                                                   const Advanced::RefFEltInLocalOperator& ref_felt,
                                                   const Eigen::VectorXd& local_displacement,
                                                   const ::MoReFEM::GeometryNS::dimension_type mesh_dimension);


        //! Helper class to manage computation of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>& GetNonCstDerivativeGreenLagrange() noexcept;


        /*!
         * \brief Manage the ComputeEltArray part of the calculation that is dependent on the dimension
         * considered.
         *
         *  \tparam MeshDimensionT Dimension of the mesh.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         * Test unknowns aren't considered at this point.
         *
         * \param[in] local_displacement Local displacement in observed within the finite element at previous
         * time iteration. \param[in] ref_felt Reference finite element considered. \param[in] geom_elt \a
         * GeometricElt for which the computation takes place.
         */
        template<Eigen::Index MeshDimensionT>
        void PrepareInternalDataForQuadraturePointForDimension(
            const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& ref_felt,
            const Eigen::VectorXd& local_displacement);

        //! Get Cauchy-Green tensor \a Parameter.
        const cauchy_green_tensor_type& GetCauchyGreenTensor() const noexcept;


      private:
        //! Helper class to manage computation of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>::unique_ptr deriv_green_lagrange_ = nullptr;

        /*!
         * \brief Pointer to the Cauchy-Green tensor \a Parameter.
         *
         * Do not release this pointer: it should point to an object which is already handled correctly.
         *
         * This must be set with SetCauchyGreenTensor().
         */
        const cauchy_green_tensor_type* cauchy_green_tensor_ = nullptr;

        /*!
         * \brief Displacement obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray. \endinternal
         */
        Eigen::VectorXd former_local_displacement_;


        //! Tangent matrix.
        tangent_matrix_type tangent_matrix_;

        //! Rhs.
        rhs_type rhs_;

        /*!
         * \brief Helper matrix to compute dphi_test * gradient_based_block * dphi.transpose()
         *
         * Eigen doesn't know how to compute a matrix - matrix - matrix product without allocating some memory; so we
         * compute first `gradient_based_block * dphi.transpose()` and then the second result.
         *
         * Usually, when we know for sure the intermediate matrix is not too big it is allocated directly on the stack.
         *
         * However here `dphi` might (but is probably not) be big enough not to enable this optimization; as its size
         * depends on the shape function label used we can't specify mac xolumn size anyway.
         *
         * So we are using current matrix to allocate just once the required memory.
         *
         * \attention Matrix here is not expected to be used outside of this very computation; there should be no call outside of it.
         *
         * \internal No accessor defined on purpose for this data attribute.
         */
        mutable Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor, 3> helper_three_matrix_product_;


        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Convenient aliases for matrix indexes.
        enum class LocalMatrixIndex : std::size_t { block_contribution = 0, transposed_dphi };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
