// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor.hpp"
// *** MoReFEM header guards *** < //

#include <any>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Concept.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    auto SecondPiolaKirchhoffStressTensor
         <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            InternalVariablePolicyT,
            TimeManagerT
         >::ClassName()
        // clang-format on
        -> const std::string&
    {
        static std::string name("SecondPiolaKirchhoffStressTensor");
        return name;
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::SecondPiolaKirchhoffStressTensor(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                        elementary_data_type&& a_elementary_data,
                                        const Solid<TimeManagerT>& solid,
                                        const TimeManagerT& time_manager,
                                        const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                        input_internal_variable_policy_type* input_internal_variable_policy)
    // clang-format on
    : HyperelasticityPolicyT(a_elementary_data.GetMeshDimension(), hyperelastic_law),
      ViscoelasticityPolicyT(a_elementary_data.GetMeshDimension(),
                             a_elementary_data.NdofCol(),
                             a_elementary_data.GetGeomEltDimension(),
                             solid,
                             time_manager),
      InternalVariablePolicyT(a_elementary_data.GetMeshDimension(),
                              a_elementary_data.NnodeRow(),
                              a_elementary_data.NquadraturePoint(),
                              time_manager,
                              input_internal_variable_policy),
      parent(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent()
    {

        const auto& elementary_data = parent::GetElementaryData();
        const auto mesh_dimension = elementary_data.GetMeshDimension();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        const auto felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

        deriv_green_lagrange_ =
            std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(mesh_dimension);

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_for_test_unknown.Get(), Nnode_for_unknown.Get() }, // block_contribution
            { felt_space_dimension.Get(), Nnode_for_unknown.Get() }    // transposed dPhi
        } });

        helper_three_matrix_product_.resize(mesh_dimension.Get(), Nnode_for_unknown.Get());

        former_local_displacement_.resize(elementary_data.NdofCol());
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                          ViscoelasticityPolicyT,
                                          InternalVariablePolicyT,
                                          TimeManagerT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = parent::GetNonCstElementaryData();

        const auto& local_displacement = GetFormerLocalDisplacement();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto mesh_dimension = elementary_data.GetMeshDimension();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();


        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {

            PrepareInternalDataForQuadraturePoint(infos_at_quad_pt.GetUnknownData(),
                                                  geom_elt,
                                                  elementary_data.GetRefFElt(this->GetNthUnknown(0)),
                                                  local_displacement,
                                                  mesh_dimension);

            ComputeAtQuadraturePoint(infos_at_quad_pt, elementary_data);
        }


        if constexpr (!(requires { self::no_internal_variable; }))
        {
            // clang-format off
            if constexpr (Internal::Concept::SecondPiolaKirchhoffStressTensorNS::With_CorrectRHSWithActiveSchurComplement
                          <
                              InternalVariablePolicyT,
                              decltype(elementary_data.GetNonCstVectorResult())
                          >)
            // clang-format on
            {

                const auto& internal_variable_ptr = static_cast<InternalVariablePolicyT*>(this);
                auto& internal_variable = *internal_variable_ptr;
                internal_variable.CorrectRHSWithActiveSchurComplement(elementary_data.GetNonCstVectorResult());
            }
        }
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    void SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
     >::UpdateInternalVariableInternalVariables()
    // clang-format on
    {

        auto& elementary_data = parent::GetNonCstElementaryData();
        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& internal_variable_ptr = static_cast<InternalVariablePolicyT*>(this);
        auto& internal_variable = *internal_variable_ptr;

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const auto current_deform =
                internal_variable.GetFiberDeformationAtCurrentTimeIteration().GetValue(quad_pt, geom_elt);
            const auto prev_deform =
                internal_variable.GetFiberDeformationAtPreviousTimeIteration().GetValue(quad_pt, geom_elt);

            internal_variable.UpdateInternalVariables(
                quad_pt, geom_elt, infos_at_quad_pt.GetUnknownData().GetRefFEltPhi(), current_deform, prev_deform);
        }
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    void SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::ComputeAtQuadraturePoint(const InformationAtQuadraturePoint& infos_at_quad_pt,
                                elementary_data_type& elementary_data) const
    // clang-format on
    {

        const auto Ncomponent = ::MoReFEM::GeometryNS::dimension_type{ elementary_data.GetGeomEltDimension() };

        decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
        decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

        const auto& dphi = quad_pt_unknown_data.GetGradientFEltPhi();
        const auto& dphi_test = test_quad_pt_unknown_data.GetGradientFEltPhi();

        const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

        const auto weight_meas = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(this->GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(this->GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        decltype(auto) tangent_matrix = GetTangentMatrix();

        if (parent::DoAssembleIntoMatrix())
        {
            // Matrix related calculation.
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();

            auto& block_contribution =
                matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();

            for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
            {
                const auto row_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);
                auto row_slicing = Eigen::seqN(row_component.Get() * Ncomponent.Get(), Ncomponent.Get());

                for (::MoReFEM::GeometryNS::dimension_type col_component{}; col_component < Ncomponent; ++col_component)
                {
                    const auto col_first_index = unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                    auto col_slicing = Eigen::seqN(col_component.Get() * Ncomponent.Get(), Ncomponent.Get());
                    ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> gradient_based_block =
                        tangent_matrix(row_slicing, col_slicing);

                    helper_three_matrix_product_.noalias() = gradient_based_block * dphi.transpose();
                    block_contribution.noalias() = weight_meas * dphi_test * helper_three_matrix_product_;

                    matrix_result(Eigen::seqN(row_first_index, Nnode_for_test_unknown.Get()),
                                  Eigen::seqN(col_first_index, Nnode_for_unknown.Get())) += block_contribution;
                }
            }
        }

        if (parent::DoAssembleIntoVector())
        {
            // Vector related calculation.
            auto& vector_result = elementary_data.GetNonCstVectorResult();

            for (::MoReFEM::GeometryNS::dimension_type row_component{}; row_component < Ncomponent; ++row_component)
            {
                const auto dof_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);
                const auto component_first_index = row_component.Get() * Ncomponent.Get();

                // Compute the new contribution to vector_result here.
                // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs.
                decltype(auto) rhs = GetRhs();

                for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_for_test_unknown; ++row_node)
                {
                    const double value = dphi_test(row_node.Get(), Eigen::all)
                                         * rhs(Eigen::seqN(component_first_index, Ncomponent.Get()));
                    vector_result(dof_first_index + row_node.Get()) += value * weight_meas;
                }
            }
        }
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    template<Eigen::Index DimensionT>
    void SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::ComputeWDerivates(
            const Advanced::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& ref_felt,
            const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
            ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
            ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W)
    // clang-format on
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
        decltype(auto) cauchy_green_tensor = GetCauchyGreenTensor();
        decltype(auto) quad_pt = quad_pt_unknown_data.GetQuadraturePoint();
        decltype(auto) cauchy_green_tensor_value = cauchy_green_tensor.GetValue(quad_pt, geom_elt);

        if constexpr (!(requires { self::no_hyperelasticity; }))
        {
            const auto& hyperelasticity_ptr = static_cast<HyperelasticityPolicyT*>(this);
            auto& hyperelasticity = *hyperelasticity_ptr;
            hyperelasticity.ComputeWDerivates(quad_pt, geom_elt, ref_felt, cauchy_green_tensor_value, dW, d2W);
        }

        if constexpr (!(requires { self::no_internal_variable; }))
        {
            const auto& internal_variable_ptr = static_cast<InternalVariablePolicyT*>(this);
            auto& internal_variable = *internal_variable_ptr;
            internal_variable.template ComputeWDerivates<DimensionT>(
                quad_pt_unknown_data, geom_elt, ref_felt, cauchy_green_tensor_value, De, dW, d2W);
        }

        if constexpr (!(requires { self::no_viscoelasticity; }))
        {
            const auto& viscoelasticity_ptr = static_cast<ViscoelasticityPolicyT*>(this);
            auto& viscoelasticity = *viscoelasticity_ptr;
            viscoelasticity.template ComputeWDerivates<DimensionT>(
                quad_pt_unknown_data, geom_elt, ref_felt, De, dW, d2W);
        }
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    void SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::PrepareInternalDataForQuadraturePoint(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                                             const GeometricElt& geom_elt,
                                             const Advanced::RefFEltInLocalOperator& ref_felt,
                                             const Eigen::VectorXd& local_displacement,
                                             const ::MoReFEM::GeometryNS::dimension_type mesh_dimension)
    // clang-format off
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
        switch (mesh_dimension.Get())
        {
        case 1:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<1>(
                quad_pt_unknown_data, geom_elt, ref_felt, local_displacement);
            break;
        }
        case 2:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<2>(
                quad_pt_unknown_data, geom_elt, ref_felt, local_displacement);
            break;
        }
        case 3:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<3>(
                quad_pt_unknown_data, geom_elt, ref_felt, local_displacement);
            break;
        }
        default:
            assert(false);
        }
        
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    inline auto
    SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetFormerLocalDisplacement() const noexcept
        // clang-format on
        -> const Eigen::VectorXd&
    {
        return former_local_displacement_;
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    auto
    SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetNonCstDerivativeGreenLagrange() noexcept
    -> DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
    // clang-format on
    {
        assert(!(!deriv_green_lagrange_));
        return *deriv_green_lagrange_;
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    inline auto
    SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetNonCstFormerLocalDisplacement() noexcept
    -> Eigen::VectorXd&
    // clang-format on
    {
        return const_cast<Eigen::VectorXd&>(GetFormerLocalDisplacement());
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    template<Eigen::Index DimensionT>
    void 
    SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::PrepareInternalDataForQuadraturePointForDimension(
            const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& ref_felt,
            const Eigen::VectorXd& local_displacement)
    // clang-format on
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
        auto gradient_displacement = ref_felt.ComputeGradientDisplacementMatrix(
            quad_pt_unknown_data, local_displacement, ::MoReFEM::GeometryNS::dimension_type{ DimensionT });

        // ========================================================================================================
        // Compute first the invariants for current quadrature point.
        // For this purpose, Cauchy-Green tensor and De matrix (see P22 on Philippe's note) must be computed first.
        // ========================================================================================================

        auto& derivative_green_lagrange = GetNonCstDerivativeGreenLagrange();
        const auto& De = derivative_green_lagrange.Update(gradient_displacement);

        decltype(auto) check_inverted = MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements::GetInstance();

        switch (check_inverted.DoCheckInvertedElements())
        {
        case check_inverted_elements_policy::do_check:
        {
            auto deformation_gradient = ::MoReFEM::Wrappers::EigenNS::InitDimensionMatrixVariant(DimensionT);

            // Visitor used here as we want the code to just work independently of the dimension of the problem
            // considered (to put in a nutshell: this is a trick to make the code work regardless of the variabt used
            // in DimensionMatrix).
            const auto determinant_deformation_gradient = std::visit(
                [&gradient_displacement](auto& matrix)
                {
                    matrix.noalias() = gradient_displacement + Eigen::MatrixXd::Identity(DimensionT, DimensionT);
                    return matrix.determinant();
                },
                deformation_gradient);

            if (determinant_deformation_gradient <= 0.)
            {
                // For some reason Eigen triggers an assert about memory allocation if exception is thrown.
                // As here exception is meant to lead to an exit of the program, workaround to allow back
                // Eigen memory allocation is acceptable.
                Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

                throw Exception("Some mesh elements have been inverted. "
                                "Consider using an adaptative loading method "
                                "(i.e continuation method for static cases or smaller "
                                "time steps for dynamic ones).");
            }
            break;
        }
        case check_inverted_elements_policy::no_check:
            break; // Maybe free the memory allocated for the local matrix deformation gradient?

        case check_inverted_elements_policy::from_input_data:
        case check_inverted_elements_policy::unspecified:
            assert(false && "Should not happen");
            exit(EXIT_FAILURE);
        } // switch

        // ===================================================================================
        // Then compute the derivates of W, required to build both bilinear and linear terms.
        // ===================================================================================

        auto dW = ::MoReFEM::Wrappers::EigenNS::InitdWVector(DimensionT);

        auto d2W = ::MoReFEM::Wrappers::EigenNS::Initd2WMatrix(DimensionT);

        // Put to zero as in each ComputeWDerivates of the policies once should add the contributions and use Add.
        dW.setZero();
        d2W.setZero();

        ComputeWDerivates<DimensionT>(quad_pt_unknown_data, geom_elt, ref_felt, De, dW, d2W);

        // ===================================================================================
        // Finally build the terms that are actually required.
        // ===================================================================================

        // Linear term.
        if (parent::DoAssembleIntoVector())
        {
            decltype(auto) rhs = GetNonCstRhs();
            rhs.noalias() = De.transpose() * dW;
        }

        // Bilinear terms. There are in fact both: one for linear part and another for non-linear one.
        if (parent::DoAssembleIntoMatrix())
        {
            auto linear_part =
                Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeLinearPart(De, d2W);

            auto non_linear_part =
                Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeNonLinearPart<
                    DimensionT>(dW);

            const auto& viscoelasticity_ptr = static_cast<ViscoelasticityPolicyT*>(this);
            auto& viscoelasticity = *viscoelasticity_ptr;

            auto& tangent_matrix = GetNonCstTangentMatrix();
            tangent_matrix.noalias() = linear_part + non_linear_part;

            if constexpr (!(requires { self::no_viscoelasticity; }))
            {
                decltype(auto) tangent_matrix_visco = viscoelasticity.GetTangentVisco();
                tangent_matrix.noalias() += tangent_matrix_visco;
            }
        }
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    void
    SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::SetCauchyGreenTensor(const cauchy_green_tensor_type* param)
    // clang-format on
    {
        assert(cauchy_green_tensor_ == nullptr && "Should be called only once.");
        cauchy_green_tensor_ = param;
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    inline auto
    SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetCauchyGreenTensor() const noexcept
    -> const cauchy_green_tensor_type&
    // clang-format on
    {
        assert(!(!cauchy_green_tensor_));
        return *cauchy_green_tensor_;
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    
    inline auto SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetTangentMatrix() const noexcept
    -> const tangent_matrix_type&
    // clang-format on
    {
        return tangent_matrix_;
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >

    inline auto SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetNonCstTangentMatrix() noexcept
    -> tangent_matrix_type&
    // clang-format on
    {
        return const_cast<tangent_matrix_type&>(GetTangentMatrix());
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    auto SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetRhs() const noexcept
    -> const rhs_type&
    // clang-format on
    {
        return rhs_;
    }


    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    auto SecondPiolaKirchhoffStressTensor
    <
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        TimeManagerT
    >::GetNonCstRhs() noexcept
    -> rhs_type&
    // clang-format on
    {
        return const_cast<rhs_type&>(GetRhs());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
