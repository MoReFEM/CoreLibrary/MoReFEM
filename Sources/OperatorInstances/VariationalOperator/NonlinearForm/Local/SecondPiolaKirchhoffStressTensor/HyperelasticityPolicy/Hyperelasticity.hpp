// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Advanced/InvariantHolder.hpp" // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
{

    /*!
     * \brief Policy to use when hyperelasticity is involved in \a SecondPiolaKirchhoffStressTensor
     * operator.
     *
     * \tparam HyperelasticLawT Hyperelastic law considered.
     */
    template<class HyperelasticLawT>
    class Hyperelasticity
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Hyperelasticity<HyperelasticLawT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Type of the elementary matrix.
        using matrix_type = Eigen::MatrixXd;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to undelrying law.
        using law_type = HyperelasticLawT;

        //! Alias to Cauchy Green tensor.
        // clang-format off
        using cauchy_green_tensor_type =
            ParameterAtQuadraturePoint
            <
                ParameterNS::Type::vector,
                typename HyperelasticLawT::time_manager_type,
                ParameterNS::TimeDependencyNS::None,
                ::MoReFEM::Wrappers::EigenNS::dWVector
            >;
        // clang-format on

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] hyperelasticity_law Hyperelastic law considered, which must be defined elsewhere.
         */
        explicit Hyperelasticity(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                 const HyperelasticLawT* const hyperelasticity_law);

        //! Destructor.
        ~Hyperelasticity() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Hyperelasticity(const Hyperelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Hyperelasticity(Hyperelasticity&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Hyperelasticity& operator=(const Hyperelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Hyperelasticity& operator=(Hyperelasticity&& rhs) = delete;

        ///@}

        /*!
         * \class doxygen_hide_second_piola_compute_W_deriv_hyperelasticity
         *
         * \brief Compute the hyperelasticity and its derivatives.
         *
         * \copydoc doxygen_hide_dW_d2W_derivates_arg
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] ref_felt Reference finite element considered.
         * \param[in] cauchy_green_tensor_value Value oif the CauchyGreen tensor at the current
         * \a QuadraturePoint.
         */

        //! \copydoc doxygen_hide_second_piola_compute_W_deriv_hyperelasticity
        void ComputeWDerivates(const QuadraturePoint& quad_pt,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt,
                               const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                               ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                               ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);


        /*!
         * \class doxygen_hide_cauchy_green_as_vector_arg
         *
         * \param[in] cauchy_green_as_vector Value of the CauchyGreen tensor at the current
         * \a QuadraturePoint in generalized coordinates stored as a (6, 1) matrix.; expected content is:
         * (11, 22, 33, 12, 23, 13) for the components. It is stored as a matrix as we also need its
         * transposed to do matrix matrix related operations.
         */

        /*!
         * \class doxygen_hide_contravariant_metric_as_vector_arg
         *
         * \param[in] contravariant_metric_as_vector Value of the contravariant metric tensor
         * at the current \a QuadraturePoint in generalized coordinates stored as a (6, 1) matrix.
         * Expected content is (11, 22, 33, 12, 23, 13) for the components. It is stored as a matrix as
         * we need to do matrix matrix related operations.
         */

        /*!
         * \class doxygen_hide_transposed_contravariant_metric_as_vector_arg
         *
         * \param[in] transposed_contravariant_metric_as_vector Value of the transposed contravariant
         * metric tensor at the current \a QuadraturePoint in generalized coordinates stored as a
         * (1, 6) matrix.
         * Expected content is (11, 22, 33, 12, 23, 13) for the components. It is stored as a matrix as
         * we need to do matrix matrix related operations.
         */


        /*!
         *
         * \brief Compute the hyperelasticity and its derivatives.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] ref_felt Reference finite element considered.
         * \copydoc doxygen_hide_cauchy_green_as_vector_arg
         *
         * \copydoc doxygen_hide_generalized_input_arg
         *
         * \param[in] contravariant_basis The contravariant basis used to compute the contravariant fiber vector.
         * \param[in,out] dW Second Piola-Kirchhoff stress tensor in generalized coordinates.
         * \param[in,out] d2W Second Piola-Kirchhoff stress tensor derivative in generalized coordinates.
         */

        void ComputeWDerivates(const QuadraturePoint& quad_pt,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt,
                               const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_as_vector,
                               const InvariantNS::GeneralizedNS::Input& generalized_input,
                               const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis,
                               ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                               ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);


      private:
        //! Helper matrix to store outer product.
        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>& GetNonCstWorkMatrixOuterProduct() noexcept;

        //! Underlying hyperelasticity law.
        const law_type& GetHyperelasticLaw() const noexcept;

        //! Underlying hyperelasticity law.
        law_type& GetNonCstHyperelasticLaw() noexcept;


        /*!
         * \brief Helper function to keep the ComputeWDerivates functions DRY.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in,out] dW Second Piola-Kirchhoff stress tensor in generalized coordinates.
         * \param[in,out] d2W Second Piola-Kirchhoff stress tensor derivative in generalized coordinates.
         */
        void ComputeChainRule(const QuadraturePoint& quad_pt,
                              const GeometricElt& geom_elt,
                              ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                              ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);


      private:
        //! Helper matrix to store outer product.
        ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6> work_matrix_outer_product_;

        //! Underlying hyperelasticity law.
        const HyperelasticLawT* const hyperelastic_law_;
    };


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
