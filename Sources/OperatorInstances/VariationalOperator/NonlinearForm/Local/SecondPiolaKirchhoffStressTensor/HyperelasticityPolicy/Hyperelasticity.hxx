// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
{


    template<class HyperelasticLawT>
    Hyperelasticity<HyperelasticLawT>::Hyperelasticity(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                                       const HyperelasticLawT* const hyperelastic_law)
    : hyperelastic_law_(hyperelastic_law)
    {
        switch (mesh_dimension.Get())
        {
        case 1:
            GetNonCstWorkMatrixOuterProduct().resize(1, 1);
            break;
        case 2:
            GetNonCstWorkMatrixOuterProduct().resize(3, 3);
            break;
        case 3:
            GetNonCstWorkMatrixOuterProduct().resize(6, 6);
            break;
        default:
            assert(false);
            break;
        }
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeWDerivates(
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt,
        [[maybe_unused]] const Advanced::RefFEltInLocalOperator& ref_felt,
        const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
        ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
        ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W)
    {
        GetNonCstHyperelasticLaw().UpdateInvariants(cauchy_green_tensor_value, quad_pt, geom_elt);

        ComputeChainRule(quad_pt, geom_elt, dW, d2W);
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeWDerivates(
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt,
        [[maybe_unused]] const Advanced::RefFEltInLocalOperator& ref_felt,
        const ::MoReFEM::Wrappers::EigenNS::dWVector& matricial_cauchy_green_tensor,
        const InvariantNS::GeneralizedNS::Input& generalized_input,
        const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis,
        ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
        ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W)
    {
        GetNonCstHyperelasticLaw().UpdateInvariants(
            matricial_cauchy_green_tensor, quad_pt, geom_elt, generalized_input, contravariant_basis);

        ComputeChainRule(quad_pt, geom_elt, dW, d2W);
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeChainRule(const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt,
                                                             ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                                                             ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W)
    {
        decltype(auto) law = GetHyperelasticLaw();

        constexpr bool is_I1 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I1>();
        constexpr bool is_I2 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I2>();
        constexpr bool is_I3 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I3>();
        constexpr bool is_I4 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I4>();
        constexpr bool is_I6 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I6>();

        {
            assert(d2W.isZero());
            assert(dW.isZero());

            {
                if constexpr (is_I1)
                {
                    const double dWdI1 = law.dWdI1(quad_pt, geom_elt);
                    const auto& dI1dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I1>();

                    dW.noalias() += dWdI1 * dI1dC;

                    const double d2WdI1dI1 = law.d2WdI1(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI1dI1))
                        d2W.noalias() += d2WdI1dI1 * dI1dC * dI1dC.transpose();

                    if constexpr (is_I2)
                    {
                        const auto& dI2dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I2>();

                        const double d2WdI1dI2 = law.d2WdI1dI2(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI2))
                        {
                            d2W.noalias() += d2WdI1dI2 * (dI1dC * dI2dC.transpose());
                            d2W.noalias() += d2WdI1dI2 * (dI2dC * dI1dC.transpose());
                        }
                    }

                    if constexpr (is_I3)
                    {
                        const auto& dI3dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                        const double d2WdI1dI3 = law.d2WdI1dI3(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI3))
                        {
                            d2W.noalias() += d2WdI1dI3 * (dI1dC * dI3dC.transpose());
                            d2W.noalias() += d2WdI1dI3 * (dI3dC * dI1dC.transpose());
                        }
                    }

                    if constexpr (is_I4)
                    {
                        const auto& dI4dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                        const double d2WdI1dI4 = law.d2WdI1dI4(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI4))
                        {
                            d2W.noalias() += d2WdI1dI4 * (dI1dC * dI4dC.transpose());
                            d2W.noalias() += d2WdI1dI4 * (dI4dC * dI1dC.transpose());
                        }
                    }

                    if constexpr (is_I6)
                    {
                        const auto& dI6dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                        const double d2WdI1dI6 = law.d2WdI1dI6(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI6))
                        {
                            d2W.noalias() += d2WdI1dI6 * (dI1dC * dI6dC.transpose());
                            d2W.noalias() += d2WdI1dI6 * (dI6dC * dI1dC.transpose());
                        }
                    }
                }

                if constexpr (is_I2)
                {
                    const double dWdI2 = law.dWdI2(quad_pt, geom_elt);
                    d2W.noalias() +=
                        dWdI2 * law.template GetSecondDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I2>();

                    const auto& dI2dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I2>();

                    dW.noalias() += dWdI2 * dI2dC;

                    const double d2WdI2dI2 = law.d2WdI2(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI2dI2))
                        d2W.noalias() += d2WdI2dI2 * dI2dC * dI2dC.transpose();

                    if constexpr (is_I3)
                    {
                        const auto& dI3dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                        const double d2WdI2dI3 = law.d2WdI2dI3(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI2dI3))
                        {
                            d2W.noalias() += d2WdI2dI3 * (dI2dC * dI3dC.transpose());
                            d2W.noalias() += d2WdI2dI3 * (dI3dC * dI2dC.transpose());
                        }
                    }

                    if constexpr (is_I4)
                    {
                        const auto& dI4dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                        const double d2WdI2dI4 = law.d2WdI2dI4(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI2dI4))
                        {
                            d2W.noalias() += d2WdI2dI4 * (dI2dC * dI4dC.transpose());
                            d2W.noalias() += d2WdI2dI4 * (dI4dC * dI2dC.transpose());
                        }
                    }

                    if constexpr (is_I6)
                    {
                        const auto& dI6dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                        const double d2WdI2dI6 = law.d2WdI2dI6(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI2dI6))
                        {
                            d2W.noalias() += d2WdI2dI6 * (dI2dC * dI6dC.transpose());
                            d2W.noalias() += d2WdI2dI6 * (dI6dC * dI2dC.transpose());
                        }
                    }
                }

                if constexpr (is_I3)
                {
                    const double dWdI3 = law.dWdI3(quad_pt, geom_elt);
                    d2W.noalias() +=
                        dWdI3 * law.template GetSecondDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                    const auto& dI3dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                    dW.noalias() += dWdI3 * dI3dC;

                    const double d2WdI3dI3 = law.d2WdI3(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI3dI3))
                        d2W.noalias() += d2WdI3dI3 * dI3dC * dI3dC.transpose();


                    if constexpr (is_I4)
                    {
                        const auto& dI4dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                        const double d2WdI3dI4 = law.d2WdI3dI4(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI3dI4))
                        {
                            d2W.noalias() += d2WdI3dI4 * (dI3dC * dI4dC.transpose());
                            d2W.noalias() += d2WdI3dI4 * (dI4dC * dI3dC.transpose());
                        }
                    }

                    if constexpr (is_I6)
                    {
                        const auto& dI6dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                        const double d2WdI3dI6 = law.d2WdI3dI6(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI3dI6))
                        {
                            d2W.noalias() += d2WdI3dI6 * (dI3dC * dI6dC.transpose());
                            d2W.noalias() += d2WdI3dI6 * (dI6dC * dI3dC.transpose());
                        }
                    }
                }


                if constexpr (is_I4)
                {
                    const double dWdI4 = law.dWdI4(quad_pt, geom_elt);
                    const auto& dI4dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                    dW.noalias() += dWdI4 * dI4dC;

                    const double d2WdI4dI4 = law.d2WdI4(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI4dI4))
                        d2W.noalias() += d2WdI4dI4 * (dI4dC * dI4dC.transpose());
                }

                if constexpr (is_I6)
                {
                    const double dWdI6 = law.dWdI6(quad_pt, geom_elt);
                    const auto& dI6dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                    dW.noalias() += dWdI6 * dI6dC;

                    const double d2WdI6dI6 = law.d2WdI6dI6(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI6dI6))
                        d2W.noalias() += d2WdI6dI6 * (dI6dC * dI6dC.transpose());
                }
            }
        }

        d2W *= 4.;
        dW *= 2.;
    }


    template<class HyperelasticLawT>
    inline auto Hyperelasticity<HyperelasticLawT>::GetNonCstWorkMatrixOuterProduct() noexcept
        -> ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>&
    {
        return work_matrix_outer_product_;
    }


    template<class HyperelasticLawT>
    inline auto Hyperelasticity<HyperelasticLawT>::GetHyperelasticLaw() const noexcept -> const law_type&
    {
        assert(!(!hyperelastic_law_));
        return *hyperelastic_law_;
    }


    template<class HyperelasticLawT>
    inline auto Hyperelasticity<HyperelasticLawT>::GetNonCstHyperelasticLaw() noexcept -> law_type&
    {
        return const_cast<law_type&>(GetHyperelasticLaw());
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
