// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
{


    template<>
    auto ComputeNonLinearPart<1>(const ::MoReFEM::Wrappers::EigenNS::dWVector& dW) -> matrix_type
    {
        matrix_type out(1, 1);
        out(0, 0) = dW(0); // Term SIGMA_xx
        return out;
    }


    template<>
    auto ComputeNonLinearPart<2>(const ::MoReFEM::Wrappers::EigenNS::dWVector& dW) -> matrix_type
    {
        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        matrix_type out(4, 4);
        out.setZero();

        for (auto i = Eigen::Index{}; i < 2; ++i)
        {
            auto twice = 2 * i;

            for (auto iComp = Eigen::Index{}; iComp < 2; ++iComp)
                out(twice + iComp, twice + iComp) = dW(iComp); // Term SIGMA_xx, SIGMA_yy

            out(twice, twice + 1) = out(twice + 1, twice) = dW(2); // Term SIGMA_xy
        }

        return out;
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


    template<>
    auto ComputeNonLinearPart<3>(const ::MoReFEM::Wrappers::EigenNS::dWVector& dW) -> matrix_type
    {
        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        matrix_type out(9, 9);
        out.setZero();

        for (auto i = Eigen::Index{}; i < 3; ++i)
        {
            auto thrice = 3 * i;

            for (auto iComp = Eigen::Index{}; iComp < 3; ++iComp)
                out(thrice + iComp, thrice + iComp) = dW(iComp); // Term SIGMA_xx, SIGMA_yy, SIGMA_zz

            out(thrice, thrice + 1) = out(thrice + 1, thrice) = dW(3);         // Term SIGMA_xy
            out(thrice + 1, thrice + 2) = out(thrice + 2, thrice + 1) = dW(4); // Term SIGMA_yz
            out(thrice, thrice + 2) = out(thrice + 2, thrice) = dW(5);         // Term SIGMA_xz
        }

        return out;
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


    auto ComputeLinearPart(const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
                           const ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W) -> matrix_type
    {
        ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix buf;
        buf.noalias() = d2W * De;
        matrix_type ret;
        ret.noalias() = De.transpose() * buf;
        return ret;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
