// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_HELPER_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp" // IWYU pragma: export

#include "Core/Enum.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
{

    //! Matrix used for storage; max size 9 x 9 is for 3D case.
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    using matrix_type = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor, 9, 9>;

    /*!
     * \brief Compute the non linear contribution to the tangent matrix.
     *
     * \tparam MeshDimensionT Dimension of the mesh.
     *
     * \param[in] dW First derivates of W.
     * \return Non inear contribution to the tangent matrix.
     */
    template<Eigen::Index MeshDimensionT>
    matrix_type ComputeNonLinearPart(const ::MoReFEM::Wrappers::EigenNS::dWVector& dW);


    /*!
     * \brief Compute the linear contribution to the tangent matrix.
     *
     * \copydoc doxygen_hide_de_arg
     *
     * \param[in] d2W Second derivates of W.
     * \return Linear contribution to the tangent matrix.
     */
    matrix_type ComputeLinearPart(const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
                                  const ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
