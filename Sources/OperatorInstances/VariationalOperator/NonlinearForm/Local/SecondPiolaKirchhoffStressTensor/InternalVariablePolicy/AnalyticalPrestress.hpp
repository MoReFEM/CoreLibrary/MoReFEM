// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/TimeManager/TimeManager.hpp"

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp" // IWYU pragma: export
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hpp" // IWYU pragma: export

#include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{

    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class AnalyticalPrestress;

} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    /*!
     * \brief Policy to use when \a AnalyticalPrestress is involved in \a
     * SecondPiolaKirchhoffStressTensor.
     *
     * \tparam FiberIndexT Index of the relevant fiber in the \a InputDataFile.
     *
     * \todo #9 (Gautier) Explain difference with InputAnalyticalPrestress.
     */
    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class AnalyticalPrestress : public Crtp::LocalVectorStorage<AnalyticalPrestress<FiberIndexT, TimeManagerT>, 1UL>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = AnalyticalPrestress<FiberIndexT, TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 1UL>;

        //! Friendship to the GlobalVariationalOperator.
        friend ::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::InternalVariablePolicyNS::
            AnalyticalPrestress<FiberIndexT, TimeManagerT>;

        //! Alias to a scalar parameter at quadrature point.
        using ScalarParameterAtQuadPt = ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT>;

        //! Alias to the type of the input of the policy.
        using input_internal_variable_policy_type = ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::
            InternalVariablePolicyNS::InputAnalyticalPrestress<TimeManagerT>;

        //! Friendship to the class that calls SetSigmaC().
        template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t I, std::size_t TupleSizeT>
        friend struct Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::SetSigmaCHelper;

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type =
            FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector, TimeManagerT>;

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using scalar_param_at_quad_pt_type = ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] Nnode_unknown Number of nodes.
         * \param[in] Nquad_point Nunmber of quadrature point.
         * \param[in] time_manager Object that keeps track of the time within the Model.
         * \param[in] input_internal_variable_policy \a InputAnalyticalPrestress object.
         */
        explicit AnalyticalPrestress(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                     const LocalNodeNS::index_type Nnode_unknown,
                                     const Eigen::Index Nquad_point,
                                     const TimeManagerT& time_manager,
                                     input_internal_variable_policy_type* input_internal_variable_policy);

        //! Destructor.
        ~AnalyticalPrestress() = default;

        //! \copydoc doxygen_hide_copy_constructor
        AnalyticalPrestress(const AnalyticalPrestress& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AnalyticalPrestress(AnalyticalPrestress&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        AnalyticalPrestress& operator=(const AnalyticalPrestress& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AnalyticalPrestress& operator=(AnalyticalPrestress&& rhs) = delete;

        ///@}

      public:
        /*!
         * \class doxygen_hide_second_piola_compute_W_derivates_internal_variable
         *
         * \brief Compute the active stress and its derivatives.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         * \param[in] ref_felt Reference finite element considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] cauchy_green_tensor_value Value oif the CauchyGreen tensor at the current
         * \a QuadraturePoint.
         * \copydoc doxygen_hide_de_arg
         */

        /*!
         * \copydoc doxygen_hide_second_piola_compute_W_derivates_internal_variable
         *
         * \copydoc doxygen_hide_dW_d2W_derivates_arg
         */
        template<Eigen::Index DimensionT>
        void ComputeWDerivates(const Advanced::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt,
                               const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                               const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
                               ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                               ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);

      public:
        //! Constant accessor to the local velocity required by ComputeEltArray().
        const Eigen::VectorXd& GetLocalElectricalActivationPreviousTime() const noexcept;

        //! Non constant accessor to the local velocity required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstLocalElectricalActivationPreviousTime() noexcept;

        //! Constant accessor to the local velocity required by ComputeEltArray().
        const Eigen::VectorXd& GetLocalElectricalActivationAtTime() const noexcept;

        //! Non constant accessor to the local velocity required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstLocalElectricalActivationAtTime() noexcept;

        //! Indicates if sigma_c must be advanced in time or not.
        //! \param[in] do_update Value to set.
        void SetDoUpdateSigmaC(const bool do_update);

        //! Constant accessor to sigma_c.
        const scalar_param_at_quad_pt_type& GetSigmaC() const noexcept;

      private:
        //! Constant accessor to \a TimeManager.
        const TimeManagerT& GetTimeManager() const noexcept;

        //! Constant accessor to the fibers.
        const vectorial_fiber_type& GetFibers() const noexcept;

        //! Non constant accessor to sigma_c.
        scalar_param_at_quad_pt_type& GetNonCstSigmaC() noexcept;

        //! Give sigma_c to the local operator.
        //! \param[in] sigma_c New value of sigma_c.
        void SetSigmaC(scalar_param_at_quad_pt_type* sigma_c);

      private:
        //! Constant accessor to the input of the policy.
        const input_internal_variable_policy_type& GetInputActiveStress() const noexcept;

      private:
        //! Fibers parameter.
        const vectorial_fiber_type& fibers_;

        //! Active stress in the fiber direction.
        scalar_param_at_quad_pt_type* sigma_c_;

      private:
        //! Time manager.
        const TimeManagerT& time_manager_;

      private:
        /*!
         * \brief U0 obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd local_electrical_activation_previous_time_;

        /*!
         * \brief U0 obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd local_electrical_activation_at_time_;

      private:
        //! Input of the policy.
        input_internal_variable_policy_type& input_internal_variable_policy_;

      private:
        //! Boolean to know if it is the first newton iteration to advance sigma_c in time.
        bool do_update_sigma_c_;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Convenient enum to alias vectors.
        enum class LocalVectorIndex : std::size_t { tauxtau = 0 };

        ///@}
    };


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM end header guards *** < //
