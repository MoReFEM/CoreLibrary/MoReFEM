// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >

    AnalyticalPrestress<FiberIndexT, TimeManagerT>::AnalyticalPrestress(
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        const LocalNodeNS::index_type Nnode_unknown,
                                                                        [[maybe_unused]] const Eigen::Index Nquad_point,
        const TimeManagerT& time_manager,
        input_internal_variable_policy_type* input_internal_variable_policy)
    : vector_parent(),
    fibers_(::MoReFEM::FiberNS::FiberListManager
            <
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector,
                TimeManagerT
            >::GetInstance().GetFiberList(FiberListNS::unique_id{ FiberIndexT })),
    time_manager_(time_manager),
    input_internal_variable_policy_(*input_internal_variable_policy)
    // clang-format on
    {
        Eigen::Index tauxtau_init{};

        switch (mesh_dimension.Get())
        {
        case 2:
            tauxtau_init = Eigen::Index{ 3 };
            break;
        case 3:
            tauxtau_init = Eigen::Index{ 6 };
            break;
        default:
            assert(false);
            break;
        }

        this->vector_parent::InitLocalVectorStorage({ { tauxtau_init } });

        local_electrical_activation_previous_time_.resize(Nnode_unknown.Get());
        local_electrical_activation_at_time_.resize(Nnode_unknown.Get());
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    template<Eigen::Index DimensionT>
    void AnalyticalPrestress<FiberIndexT, TimeManagerT>::ComputeWDerivates(
        const Advanced::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
        const GeometricElt& geom_elt,
        [[maybe_unused]] const Advanced::RefFEltInLocalOperator& ref_felt,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
        ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
        [[maybe_unused]] ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W)
    {
        const auto run_case = GetTimeManager().GetStaticOrDynamic();

        if (run_case == StaticOrDynamic::dynamic_)
        {
            const auto& quad_pt = quad_pt_unknown_data.GetQuadraturePoint();
            const auto& phi_ref = quad_pt_unknown_data.GetRefFEltPhi();

            auto& fibers = GetFibers();
            auto& tauxtau =
                this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tauxtau)>();
            const double dt = GetTimeManager().GetTimeStep();

            const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);

            const auto dimension = static_cast<Eigen::Index>(DimensionT);

            double norm = 0.;

            for (auto component = Eigen::Index{}; component < dimension; ++component)
                norm += NumericNS::Square(tau_interpolate(component));

            const auto size = dW.rows();

            // Diagonal values sigma_xx, sigma_yy and sigma_zz.
            for (auto i = Eigen::Index{}; i < dimension; ++i)
                tauxtau(i) = NumericNS::Square(tau_interpolate(i));

            // Extra diagonal values sigma_xy, sigma_yz and sigma_xz.
            for (auto i = dimension; i < size; ++i)
                tauxtau(i) = tau_interpolate(i % dimension) * tau_interpolate((i + 1) % dimension);

            if (!(NumericNS::IsZero(norm)))
            {
                tauxtau /= norm;

                double new_sigma_c = 0.;

                if (do_update_sigma_c_)
                {
                    const auto& u0 = GetLocalElectricalActivationPreviousTime();
                    const auto& u1 = GetLocalElectricalActivationAtTime();

                    double u0_at_quad = 0.;
                    double u1_at_quad = 0.;

                    const double sigma_0 = GetInputActiveStress().GetContractility().GetValue(quad_pt, geom_elt);

                    const int Nnode = static_cast<int>(geom_elt.Nvertex());

                    // This loop interpolates u at the current quad point.
                    for (auto node_index = Eigen::Index{}; node_index < Nnode; ++node_index)
                    {
                        u0_at_quad += phi_ref(node_index) * u0(node_index);
                        u1_at_quad += phi_ref(node_index) * u1(node_index);
                    }

                    const double u0_u1_at_quad = u0_at_quad + u1_at_quad;

                    new_sigma_c = GetNonCstSigmaC().UpdateAndGetValue(
                        quad_pt,
                        geom_elt,
                        [u0_u1_at_quad, sigma_0, dt](double& sigma_c)
                        {
                            sigma_c = (0.5 * sigma_0 * NumericNS::AbsPlus(u0_u1_at_quad)
                                       + (1. / dt - 0.5 * std::fabs(u0_u1_at_quad)) * sigma_c)
                                      / (1. / dt + 0.5 * std::fabs(u0_u1_at_quad));
                        });
                } else
                    new_sigma_c = GetNonCstSigmaC().GetValue(quad_pt, geom_elt);

                dW.noalias() += new_sigma_c * tauxtau;
            }
        }
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetFibers() const noexcept
        -> const vectorial_fiber_type&
    {
        return fibers_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const TimeManagerT& AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetNonCstSigmaC() noexcept
        -> scalar_param_at_quad_pt_type&
    {
        return const_cast<scalar_param_at_quad_pt_type&>(GetSigmaC());
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetSigmaC() const noexcept
        -> const scalar_param_at_quad_pt_type&
    {
        assert(!(!sigma_c_));
        return *sigma_c_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline void AnalyticalPrestress<FiberIndexT, TimeManagerT>::SetSigmaC(scalar_param_at_quad_pt_type* sigma_c)
    {
        sigma_c_ = sigma_c;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const Eigen::VectorXd&
    AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetLocalElectricalActivationPreviousTime() const noexcept
    {
        return local_electrical_activation_previous_time_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline Eigen::VectorXd&
    AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetNonCstLocalElectricalActivationPreviousTime() noexcept
    {
        return const_cast<Eigen::VectorXd&>(GetLocalElectricalActivationPreviousTime());
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const Eigen::VectorXd&
    AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetLocalElectricalActivationAtTime() const noexcept
    {
        return local_electrical_activation_at_time_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline Eigen::VectorXd&
    AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetNonCstLocalElectricalActivationAtTime() noexcept
    {
        return const_cast<Eigen::VectorXd&>(GetLocalElectricalActivationAtTime());
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void AnalyticalPrestress<FiberIndexT, TimeManagerT>::SetDoUpdateSigmaC(const bool update)
    {
        do_update_sigma_c_ = update;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const typename AnalyticalPrestress<FiberIndexT, TimeManagerT>::input_internal_variable_policy_type&
    AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetInputActiveStress() const noexcept
    {
        return input_internal_variable_policy_;
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HXX_
// *** MoReFEM end header guards *** < //
