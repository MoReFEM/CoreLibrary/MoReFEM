// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_CONCEPT_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::Concept::SecondPiolaKirchhoffStressTensorNS
{


    /*!
     * \brief Concept to identify when the internal variable policy provides a \a CorrectRHSWithActiveSchurComplement() method.
     *
     * \tparam InternalVariablePolicyT Type of the InternalVariablePolicy considered.
     * \tparam EigenVectorT Type used for the argument of \a CorrectRHSWithActiveSchurComplement() method.
     */
    template<class InternalVariablePolicyT, class EigenVectorT>
    concept With_CorrectRHSWithActiveSchurComplement = (requires(InternalVariablePolicyT obj, EigenVectorT& vector) {
        obj.CorrectRHSWithActiveSchurComplement(vector);
    });


} // namespace MoReFEM::Internal::Concept::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
