// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/Array.hpp"
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/TimeManager/TimeManager.hpp"

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{

    template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class Microsphere;

    class InputMicrosphere;


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    /*!
     * \brief Policy to use when \a Microsphere is involved in \a SecondPiolaKirchhoffStressTensor.
     *
     * \tparam FiberIndexI4T Index of the fiber related to the 4th invariant in the \a InputData file.
     *
     * \tparam FiberIndexI6T Index of the fiber related to the 6th invariant in the \a InputData file.
     *
     */
    template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class Microsphere : public Crtp::LocalMatrixStorage<Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>, 1UL>,
                        public Crtp::LocalVectorStorage<Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>, 3UL>

    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 3UL, vector_type>;

        static_assert(std::is_convertible<self*, vector_parent*>());

        //! Type of the elementary matrix.
        using matrix_type = Eigen::MatrixXd;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 1UL, matrix_type>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Friendship to the GlobalVariationalOperator.
        friend ::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS ::
            Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>;

        //! Alias to a scalar parameter at quadrature point.
        using ScalarParameterAtQuadPt = ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT>;

        //! Alias to the type of the input of the policy.
        using input_internal_variable_policy_type = ::MoReFEM::InputMicrosphere<TimeManagerT>;

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type =
            FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector, TimeManagerT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] Nnode_unknown Number of nodes.
         * \param[in] Nquad_point Nunmber of quadrature point.
         * \param[in] time_manager Object that keeps track of the time within the Model.
         * \param[in] input_microsphere \a InputMicrosphere object.
         */
        explicit Microsphere(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                             const LocalNodeNS::index_type Nnode_unknown,
                             const Eigen::Index Nquad_point,
                             const TimeManagerT& time_manager,
                             input_internal_variable_policy_type* input_microsphere);

        //! Destructor.
        ~Microsphere() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Microsphere(const Microsphere& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Microsphere(Microsphere&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Microsphere& operator=(const Microsphere& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Microsphere& operator=(Microsphere&& rhs) = delete;

        ///@}

      public:
        //! \copydoc doxygen_hide_second_piola_compute_W_derivates_internal_variable
        //!
        //! \copydoc doxygen_hide_dW_d2W_derivates_arg
        template<Eigen::Index DimensionT>
        void ComputeWDerivates(const Advanced::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt,
                               const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                               const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
                               ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                               ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);


      private:
        //! Constant accessor to \a TimeManager.
        const TimeManagerT& GetTimeManager() const noexcept;

        //! Constant accessor to the fibers related to I4.
        const vectorial_fiber_type& GetFibersI4() const noexcept;

        //! Constant accessor to the fibers related to I6.
        const vectorial_fiber_type& GetFibersI6() const noexcept;

        //! Hardcoded quadrature points \a phi_i along the plane.
        const std::array<double, 20UL>& GetQuadraturePointsAlongPlane() const noexcept;

        //! Hardcoded quadrature points \a theta_i outside the plane.
        const std::array<double, 5UL>& GetQuadraturePointsOutsidePlane() const noexcept;

        /*!
         * \brief Computes and return tauI4 (which is the normalized value of fiber I4).
         *
         * \param[in] fiber_I4 Vector to be normalized.
         * \param[in] norm Norm of fiber I4. Could be computed of course from first argument but it happens it was already done so no need to
         * duplicate computation.
         *
         * \return Normalized vector.
         *
         * \internal The whole point of this method is to ensure no memory allocation occurs past initialization phase.
         */
        const Eigen::VectorXd& ComputeTauI4(const Eigen::VectorXd& fiber_I4, double norm) noexcept;

        /*!
         * \brief Computes and return tauI6 (which is the normalized value of fiber I6).
         *
         * \param[in] fiber_I6 Vector to be normalized.
         * \param[in] norm Norm of fiber I6. Could be computed of course from first argument but it happens it was
         already done so no need to
         * duplicate computation.

         *
         * \return Normalized vector.
         *
         * \internal The whole point of this method is to ensure no memory allocation occurs past initialization phase.
         */
        const Eigen::VectorXd& ComputeTauI6(const Eigen::VectorXd& fiber_I6, double norm) noexcept;

      private:
        //! Constant accessor to the input of the policy.
        const input_internal_variable_policy_type& GetInputMicrosphere() const noexcept;

      private:
        //! Time manager.
        const TimeManagerT& time_manager_;

      private:
        //! Fibers parameter for I4.
        const vectorial_fiber_type& fibers_I4_;

        //! Fibers parameter for I6.
        const vectorial_fiber_type& fibers_I6_;

        //! Input of the policy.
        input_internal_variable_policy_type& input_internal_variable_policy_;

        //! Hardcoded quadrature points \a phi_i along the plane.
        std::array<double, 20UL> lbdvp_;

        //! Hardcoded quadrature points \a theta_i outside the plane.
        std::array<double, 5UL> lbdvt_;

        //! Tau I4 - normalized value of fiber I4.
        //! Should not be called outside of `ComputeTauI4()`.
        Eigen::VectorXd tau_I4_;

        //! Tau I6 - normalized value of fiber I6.
        //! Should not be called outside of `ComputeTauI6()`.
        Eigen::VectorXd tau_I6_;


      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Convenient enum to alias vectors.
        enum class LocalVectorIndex : std::size_t { tauxtau = 0, tau_n = 1, tau_global = 2 };

        //! Convenient enum to alias matrices.
        enum class LocalMatrixIndex : std::size_t { tauxtauxtauxtau = 0 };

        ///@}
    };


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HPP_
// *** MoReFEM end header guards *** < //
