// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

//  Modified by Patrick Le Tallec to handle microsphere models
// Add microsphere contribution to free energy derivatives dW and d2W if  the local input fiber length are non zero.
// Only works in 3D.
// Data
// two family of fibers I4 and I6. For each family, need at node level
// Must also provide the integration rule on the sphere (number of points and angles) in radian respectively in angle


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <numbers>

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::Microsphere(
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        [[maybe_unused]] const LocalNodeNS::index_type Nnode_unknown,
        [[maybe_unused]] const Eigen::Index Nquad_point,
        const TimeManagerT& time_manager,
        input_internal_variable_policy_type* input_internal_variable_policy)
    : matrix_parent(), vector_parent(), time_manager_(time_manager),
      fibers_I4_(::MoReFEM::FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                      ParameterNS::Type::vector,
                                                      TimeManagerT>::GetInstance()
                     .GetFiberList(FiberListNS::unique_id{ FiberIndexI4T })),
      fibers_I6_(::MoReFEM::FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                      ParameterNS::Type::vector,
                                                      TimeManagerT>::GetInstance()
                     .GetFiberList(FiberListNS::unique_id{ FiberIndexI6T })),
      input_internal_variable_policy_(*input_internal_variable_policy)
    {
        {
            decltype(auto) fiber_vector = fibers_I4_.GetAnyValue();
            tau_I4_.resize(fiber_vector.size());
        }

        {
            decltype(auto) fiber_vector = fibers_I6_.GetAnyValue();
            tau_I6_.resize(fiber_vector.size());
        }


        Eigen::Index tauxtau_init{};
        Eigen::Index tau_n_init{};
        Eigen::Index tau_global_init{};

        switch (mesh_dimension.Get()) // Only the 3D case is relevant for this operator.
        {
        case 3:
            tauxtau_init = 6;
            tau_n_init = 3;
            tau_global_init = 3;
            break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
            break;
        } // switch

        matrix_parent::InitLocalMatrixStorage({ { { tauxtau_init, tauxtau_init } } });
        vector_parent::InitLocalVectorStorage({ { tauxtau_init, tau_n_init, tau_global_init } });

        // Hardcoded quadrature rule on the unit sphere.
        using std::numbers::pi;
        const auto factor_conv = pi / 180.;

        constexpr auto npquad = Utilities::ArraySize<decltype(lbdvp_)>::GetValue();

        for (auto i = 0UL; i < npquad; ++i)
            lbdvp_[i] = pi * static_cast<double>(i + 1UL) / static_cast<double>(npquad);

        constexpr auto ntquad = Utilities::ArraySize<decltype(lbdvt_)>::GetValue();

        for (auto i = 0UL; i < ntquad; ++i)
            lbdvt_[i] = factor_conv * (70. + 10. * static_cast<double>(i));
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    template<Eigen::Index DimensionT>
    void Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::ComputeWDerivates(
        const Advanced::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
        const GeometricElt& geom_elt,
        [[maybe_unused]] const Advanced::RefFEltInLocalOperator& ref_felt,
        const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
        [[maybe_unused]] const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
        ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
        ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        const auto& quad_pt = quad_pt_unknown_data.GetQuadraturePoint();

        // References to initialized vectors and matrices.
        auto& tauxtau = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tauxtau)>();
        auto& tau_n = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_n)>();
        auto& tau_global =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_global)>();

        // Non linear stiffness contribution (second order derivative of the energetic potential).
        auto& tauxtauxtauxtau =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tauxtauxtauxtau)>();

        // Get local values of fiber vector, fiber density, in plane fiber dispersion and out of plane fiber
        // dispersion for families I4 and I6 from the InputPolicy. The fiber densities need to be prenormalized.
        decltype(auto) input_microsphere = GetInputMicrosphere();

        const auto& fiber_I4 = fibers_I4_.GetValue(quad_pt, geom_elt);
        const auto& density_I4 = input_microsphere.GetFiberStiffnessDensityI4().GetValue(quad_pt, geom_elt);
        const auto& inplane_dispersion_I4 = input_microsphere.GetInPlaneFiberDispersionI4().GetValue(quad_pt, geom_elt);
        const auto& transverse_dispersion_I4 =
            input_microsphere.GetOutOfPlaneFiberDispersionI4().GetValue(quad_pt, geom_elt);

        const auto& fiber_I6 = fibers_I6_.GetValue(quad_pt, geom_elt);
        const auto& density_I6 = input_microsphere.GetFiberStiffnessDensityI6().GetValue(quad_pt, geom_elt);
        const auto& inplane_dispersion_I6 = input_microsphere.GetInPlaneFiberDispersionI6().GetValue(quad_pt, geom_elt);
        const auto& transverse_dispersion_I6 =
            input_microsphere.GetOutOfPlaneFiberDispersionI6().GetValue(quad_pt, geom_elt);

        // normalisation of fiber directions and calculation of the normal to the fiber plane
        // fiber vectors are copied
        const auto norm_I4 = fiber_I4.norm();
        const auto norm_I6 = fiber_I6.norm();

        if (!(NumericNS::IsZero(norm_I4)) and !(NumericNS::IsZero(norm_I6)))
        {
            decltype(auto) tau_I4 = ComputeTauI4(fiber_I4, norm_I4);
            decltype(auto) tau_I6 = ComputeTauI6(fiber_I6, norm_I6);

            // Vectorial product to construct third local basis vector tau_n (normal out of the plane).
            tau_n(0) = tau_I4(1) * tau_I6(2) - tau_I4(2) * tau_I6(1);
            tau_n(1) = tau_I4(2) * tau_I6(0) - tau_I4(0) * tau_I6(2);
            tau_n(2) = tau_I4(0) * tau_I6(1) - tau_I4(1) * tau_I6(0);

            const auto size = dW.rows();

            decltype(auto) quad_pt_list_along_plane = GetQuadraturePointsAlongPlane();
            decltype(auto) quad_pt_list_outside_plane = GetQuadraturePointsOutsidePlane();

            // Loop on spherical quadrature points.
            for (const auto value_quad_pt_p : quad_pt_list_along_plane)
            {
                for (const auto value_quad_pt_t : quad_pt_list_outside_plane)
                {
                    double norm = 0.;

                    // Local coordinates of the integration direction in the fiber frame of reference.
                    const double xloc1 = std::cos(value_quad_pt_p) * std::sin(value_quad_pt_t);
                    const double xloc2 = std::sin(value_quad_pt_p) * std::sin(value_quad_pt_t);
                    const double xloc3 = std::cos(value_quad_pt_t);

                    // Calculation of the local integration directions and diagonal values sigma_xx,
                    // sigma_yy and sigma_zz.
                    for (auto i = Eigen::Index{}; i < DimensionT; ++i)
                    {
                        tau_global(i) = tau_I4(i) * xloc1 + tau_I6(i) * xloc2 + tau_n(i) * xloc3;
                        tauxtau(i) = NumericNS::Square(tau_global(i));
                        norm += NumericNS::Square(tau_global(i));
                    }

                    // Extra diagonal values sigma_xy, sigma_yz and sigma_xz.
                    for (auto i = DimensionT; i < size; ++i)
                        tauxtau(i) = tau_global(i % DimensionT) * tau_global((i + 1) % DimensionT);

                    // normalisation of initial fiberlength
                    tauxtau /= norm;

                    // calculation of the squ are elongation, of the fiber elongation, of the integration
                    // weights, of weighted stiffness
                    double elongation_squared = cauchy_green_tensor_value.dot(tauxtau);

                    for (auto i = DimensionT; i < size; ++i)
                        elongation_squared += cauchy_green_tensor_value(i) * tauxtau(i);

                    const double elongation = std::sqrt(elongation_squared);
                    const double weight_I4 = std::exp(inplane_dispersion_I4 * std::cos(2.0 * value_quad_pt_p)
                                                      - transverse_dispersion_I4 * std::cos(2.0 * value_quad_pt_t));
                    const double weight_I6 = std::exp(-inplane_dispersion_I6 * std::cos(2.0 * value_quad_pt_p)
                                                      - transverse_dispersion_I6 * std::cos(2.0 * value_quad_pt_t));
                    const double local_stiffness = weight_I4 * density_I4 + weight_I6 * density_I6;


                    // Calculation of the fiber traction and of its derivative with respect to the
                    // squared elongation.
                    // Piola = 2 * dW
                    const double dW_contribution = local_stiffness * (elongation - 1.) / elongation;
                    // Tangent = 4 * d2W
                    const double d2W_contribution = local_stiffness / NumericNS::Cube(elongation);

                    // update of gradient and of second derivative with respect to Cauchy Greene
                    dW += dW_contribution * tauxtau;
                    tauxtauxtauxtau.noalias() = tauxtau * tauxtau.transpose();
                    d2W += d2W_contribution * tauxtauxtauxtau;
                }
            }
        }
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetFibersI4() const noexcept
        -> const vectorial_fiber_type&
    {
        return fibers_I4_;
    }

    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetFibersI6() const noexcept
        -> const vectorial_fiber_type&
    {
        return fibers_I6_;
    }

    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const TimeManagerT& Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetInputMicrosphere() const noexcept
        -> const input_internal_variable_policy_type&
    {
        return input_internal_variable_policy_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const std::array<double, 20UL>&
    Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetQuadraturePointsAlongPlane() const noexcept
    {
        return lbdvp_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const std::array<double, 5UL>&
    Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetQuadraturePointsOutsidePlane() const noexcept
    {
        return lbdvt_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::ComputeTauI4(const Eigen::VectorXd& fiber_I4,
                                                                                      double norm) noexcept
        -> const Eigen::VectorXd&
    {
        assert(NumericNS::AreEqual<double>(norm, fiber_I4.norm()));
        tau_I4_.noalias() = fiber_I4 / norm;
        return tau_I4_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::ComputeTauI6(const Eigen::VectorXd& fiber_I6,
                                                                                      double norm) noexcept
        -> const Eigen::VectorXd&
    {
        assert(NumericNS::AreEqual<double>(norm, fiber_I6.norm()));
        tau_I6_.noalias() = fiber_I6 / norm;
        return tau_I6_;
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
// *** MoReFEM end header guards *** < //
