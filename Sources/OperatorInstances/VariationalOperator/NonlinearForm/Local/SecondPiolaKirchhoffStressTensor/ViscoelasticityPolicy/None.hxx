// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_NONE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_NONE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================




// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    None<TimeManagerT>::None([[maybe_unused]] const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                             [[maybe_unused]] const Eigen::Index Ndof,
                             [[maybe_unused]] const ::MoReFEM::GeometryNS::dimension_type Ncomponent,
                             [[maybe_unused]] const Solid<TimeManagerT>& solid,
                             [[maybe_unused]] const TimeManagerT& time_manager)
    { }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_NONE_DOT_HXX_
// *** MoReFEM end header guards *** < //
