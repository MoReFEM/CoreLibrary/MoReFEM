// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/TimeManager/TimeManager.hpp"

#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
#include "Operators/LocalVariationalOperator/ElementaryData.hpp"

#include "Parameters/Parameter.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS
{


    //! Switch between the derivatives to compute in the operator.
    enum class DerivatesWithRespectTo { displacement, velocity, displacement_and_velocity };


    /*!
     * \brief Policy to use when visco-elasticity is involved in \a SecondPiolaKirchhoffStressTensor.
     *
     * \attention This policy intervenes only in dynamic mode (handled by \a TimeManagerT).
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    class Viscoelasticity : public Crtp::LocalMatrixStorage<Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>,
                                                            1UL,
                                                            ::MoReFEM::Wrappers::EigenNS::d2WMatrix>,
                            public Crtp::LocalVectorStorage<Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>,
                                                            1UL,
                                                            ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9>>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;


        //! Type of the elementary matrix.
        using matrix_type = Eigen::MatrixXd;

        //! Type of the elementary vector.
        using vector_type = Eigen::VectorXd;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 1UL, ::MoReFEM::Wrappers::EigenNS::d2WMatrix>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 1UL, ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9>>;

        static_assert(std::is_convertible<self*, vector_parent*>());

        //! Alias to class GreenLagrangeOrEta.
        using GreenLagrangeOrEta = ::MoReFEM::Advanced::LocalVariationalOperatorNS::GreenLagrangeOrEta;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Convenient alias which specifies the maximum size of this matrix.
        using tangent_matrix_type = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor, 9, 9>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] Ndof Number of \a Dof considered.
         * \param[in] Ncomponent Number of components considered.
         * \copydoc doxygen_hide_solid_arg
         * \copydoc doxygen_hide_time_manager_arg
         */
        explicit Viscoelasticity(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                 const Eigen::Index Ndof,
                                 const ::MoReFEM::GeometryNS::dimension_type Ncomponent,
                                 const Solid<TimeManagerT>& solid,
                                 const TimeManagerT& time_manager);

        //! Destructor.
        ~Viscoelasticity() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Viscoelasticity(const Viscoelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Viscoelasticity(Viscoelasticity&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Viscoelasticity& operator=(const Viscoelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Viscoelasticity& operator=(Viscoelasticity&& rhs) = delete;

        ///@}

      public:
        //! Make the type be available without its full namespace dependency.
        using InformationAtQuadraturePoint =
            ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint;


        /*!
         * \class doxygen_hide_de_arg
         *
         * \param[in] De Derivate Green-Lagrange matrix.
         */


        /*!
         * \class doxygen_hide_second_piola_compute_W_derivates_visco_elasticity
         *
         * \brief Compute the contribution of visco-elasticity to the derivates of W.
         *
         * \tparam DimensionT Dimension of the mesh.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         * \param[in] geom_elt \a GeometricElt for which the computation is performed.
         * \param[in] ref_felt Reference finite element considered.
         *
         */


        //! \copydoc doxygen_hide_second_piola_compute_W_derivates_visco_elasticity
        //!
        //! \copydoc doxygen_hide_dW_d2W_derivates_arg
        //!
        //! \copydoc doxygen_hide_de_arg
        template<Eigen::Index DimensionT>
        void ComputeWDerivates(const Advanced::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt,
                               const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
                               ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
                               ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W);

      public:
        //! Get the \a TimeManager.
        const TimeManagerT& GetTimeManager() const noexcept;

      private:
        //! Reference to the \a TimeManager of the model.
        const TimeManagerT& time_manager_;

      public:
        //! Constant accessor to the local velocity required by ComputeEltArray().
        const Eigen::VectorXd& GetLocalVelocity() const noexcept;

        //! Non constant accessor to the local velocity required by ComputeEltArray().
        Eigen::VectorXd& GetNonCstLocalVelocity() noexcept;

      public:
        //! Get the tangent viscosity matrix.
        const tangent_matrix_type& GetTangentVisco() const noexcept;

        //! Non constant accessor to the tangent viscosity matrix.
        tangent_matrix_type& GetNonCstTangentVisco() noexcept;


      private:
        /*!
         * \brief Enables to put the gradient matrix in a vector ordered as
         * (vx,x vx,y vx,z vy,x vy,y vy,z vz,x vz,y vz,z)
         *
         * \param[in] gradient_matrix Matrix which is to be put into a vector.
         * \param[out] vector Vector into which the values are written. This vector is assumed to be
         * already properly allocated.
         */
        void MatrixToVector(const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& gradient_matrix,
                            ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9>& vector);

      private:
        /*!
         * \brief Velocity obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        Eigen::VectorXd local_velocity_;

      private:
        //! Helper class to manage computation of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::eta>& GetNonCstDerivativeEta() noexcept;

        //! Helper class to manage computation of derivative of Green-Lagrange.
        DerivativeGreenLagrange<GreenLagrangeOrEta::eta>::unique_ptr deriv_eta_ = nullptr;

      private:
        //! Viscosity.
        const scalar_parameter_type& GetViscosity() const noexcept;

      private:
        //! Viscosity.
        const scalar_parameter_type& viscosity_;

        //! Tangent of the viscosity matrix.
        Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor, 9, 9> tangent_matrix_visco_;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t { d2W_identity = 0 };


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t { velocity_gradient = 0 };

        ///@}
    };


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
