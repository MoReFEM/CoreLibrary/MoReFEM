// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::Viscoelasticity(
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        const Eigen::Index Ndof_unknown,
        const ::MoReFEM::GeometryNS::dimension_type Ncomponent,
        const Solid<TimeManagerT>& solid,
        const TimeManagerT& time_manager)
    : matrix_parent(), vector_parent(), time_manager_(time_manager), viscosity_(solid.GetViscosity())
    {
        const auto square_Ncomponent = NumericNS::Square(Ncomponent.Get());

        {
            auto& d2W_identity =
                matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_identity)>();
            auto helper = ::MoReFEM::Wrappers::EigenNS::Initd2WMatrix(Ncomponent.Get());
            d2W_identity = d2W_identity.Identity(helper.rows(), helper.cols());
        }

        {
            decltype(auto) tangent_visco = GetNonCstTangentVisco();
            tangent_visco.resize(square_Ncomponent, square_Ncomponent);
            tangent_visco.setZero();
        }

        this->vector_parent::InitLocalVectorStorage({ { mesh_dimension.Get() * mesh_dimension.Get() } });

        local_velocity_.resize(Ndof_unknown);

        deriv_eta_ =
            std::make_unique<Advanced::LocalVariationalOperatorNS ::DerivativeGreenLagrange<GreenLagrangeOrEta::eta>>(
                mesh_dimension);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    void Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::MatrixToVector(
        const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& gradient_matrix,
        ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<9>& vector)
    {
        const auto size = gradient_matrix.rows();
        assert(size == gradient_matrix.cols() && "Implemented for square matrix");
        assert(vector.size() == size * size);

        for (auto i = Eigen::Index{}; i < size; ++i)
        {
            for (auto j = Eigen::Index{}; j < size; ++j)
                vector(i * size + j) = gradient_matrix(i, j);
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    template<Eigen::Index DimensionT>
    void Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::ComputeWDerivates(
        const Advanced::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& De,
        ::MoReFEM::Wrappers::EigenNS::dWVector& dW,
        ::MoReFEM::Wrappers::EigenNS::d2WMatrix& d2W)
    {
        const auto run_case = GetTimeManager().GetStaticOrDynamic();

        if (run_case == StaticOrDynamic::dynamic_)
        {


            if constexpr (DerivatesWithRespectToT == DerivatesWithRespectTo::displacement_and_velocity)
            {

                double eta = GetViscosity().GetValue(quad_pt_unknown_data.GetQuadraturePoint(), geom_elt);
                double dt = GetTimeManager().GetTimeStep();

                const auto& local_velocity = GetLocalVelocity();


                const auto& d2W_identity =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_identity)>();
                auto& vector_gradient_velocity = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
                    LocalVectorIndex::velocity_gradient)>();

                auto matrix_gradient_velocity = ref_felt.ComputeGradientDisplacementMatrix(
                    quad_pt_unknown_data, local_velocity, ::MoReFEM::GeometryNS::dimension_type{ DimensionT });

                MatrixToVector(matrix_gradient_velocity, vector_gradient_velocity);

                auto& derivative_eta = GetNonCstDerivativeEta();
                const auto& Deta = derivative_eta.Update(matrix_gradient_velocity);

                dW.noalias() += eta * De * vector_gradient_velocity;
                d2W.noalias() += 2. * eta / dt * d2W_identity;

                decltype(auto) tangent_matrix_visco = GetNonCstTangentVisco();
                tangent_matrix_visco.noalias() = eta * De.transpose() * Deta;


            } else if constexpr (DerivatesWithRespectToT == DerivatesWithRespectTo::displacement)
            {

                double eta = GetViscosity().GetValue(quad_pt_unknown_data.GetQuadraturePoint(), geom_elt);
                const auto& local_velocity = GetLocalVelocity();

                auto& vector_gradient_velocity = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
                    LocalVectorIndex::velocity_gradient)>();

                auto matrix_gradient_velocity = ref_felt.ComputeGradientDisplacementMatrix(
                    quad_pt_unknown_data, local_velocity, ::MoReFEM::GeometryNS::dimension_type{ DimensionT });

                MatrixToVector(matrix_gradient_velocity, vector_gradient_velocity);

                auto& derivative_eta = GetNonCstDerivativeEta();
                const auto& Deta = derivative_eta.Update(matrix_gradient_velocity);

                dW.noalias() += eta * De * vector_gradient_velocity;

                auto& tangent_matrix_visco = GetNonCstTangentVisco();
                tangent_matrix_visco.noalias() = eta * De.transpose() * Deta;

            } else
            {

                static_assert(DerivatesWithRespectToT == DerivatesWithRespectTo::velocity);

                double eta = GetViscosity().GetValue(quad_pt_unknown_data.GetQuadraturePoint(), geom_elt);

                auto& tangent_matrix_visco = GetNonCstTangentVisco();
                tangent_matrix_visco.setZero();

                const auto& d2W_identity =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_identity)>();

                d2W.noalias() += eta * d2W_identity;
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline const TimeManagerT& Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetLocalVelocity() const noexcept
        -> const Eigen::VectorXd&
    {
        return local_velocity_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetNonCstLocalVelocity() noexcept
        -> Eigen::VectorXd&
    {
        return const_cast<Eigen::VectorXd&>(GetLocalVelocity());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetNonCstDerivativeEta() noexcept
        -> Advanced::LocalVariationalOperatorNS::DerivativeGreenLagrange<
            Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GreenLagrangeOrEta::eta>&
    {
        assert(!(!deriv_eta_));
        return *deriv_eta_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetTangentVisco() const noexcept
        -> const tangent_matrix_type&
    {
        return tangent_matrix_visco_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetNonCstTangentVisco() noexcept
        -> tangent_matrix_type&
    {
        return const_cast<tangent_matrix_type&>(GetTangentVisco());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetViscosity() const noexcept
        -> const scalar_parameter_type&
    {
        return viscosity_;
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
