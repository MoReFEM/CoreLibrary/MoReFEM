// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <utility>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    tying_pt_interpolation_component Symmetric(tying_pt_interpolation_component component)
    {
        switch (component)
        {
        case tying_pt_interpolation_component::e_rr:
        case tying_pt_interpolation_component::e_ss:
        case tying_pt_interpolation_component::e_zz:
            return tying_pt_interpolation_component::none;
        case tying_pt_interpolation_component::Ncomponents:
        case tying_pt_interpolation_component::none:
        {
            assert(false);
            exit(EXIT_FAILURE);
        }
        case tying_pt_interpolation_component::e_rs:
            return tying_pt_interpolation_component::e_sr;
        case tying_pt_interpolation_component::e_sz:
            return tying_pt_interpolation_component::e_zs;
        case tying_pt_interpolation_component::e_rz:
            return tying_pt_interpolation_component::e_zr;
        case tying_pt_interpolation_component::e_sr:
            return tying_pt_interpolation_component::e_rs;
        case tying_pt_interpolation_component::e_zs:
            return tying_pt_interpolation_component::e_sz;
        case tying_pt_interpolation_component::e_zr:
            return tying_pt_interpolation_component::e_rz;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>
    ComputePair(tying_pt_interpolation_component component)
    {
        switch (component)
        {
        case tying_pt_interpolation_component::e_rr:
            return { tying_pt_interpolation_component::e_rr, tying_pt_interpolation_component::e_rr };
        case tying_pt_interpolation_component::e_ss:
            return { tying_pt_interpolation_component::e_ss, tying_pt_interpolation_component::e_ss };
        case tying_pt_interpolation_component::e_zz:
            return { tying_pt_interpolation_component::e_zz, tying_pt_interpolation_component::e_zz };
        case tying_pt_interpolation_component::Ncomponents:
        case tying_pt_interpolation_component::none:
        {
            assert(false);
            exit(EXIT_FAILURE);
        }
        case tying_pt_interpolation_component::e_rs:
            return { tying_pt_interpolation_component::e_rr, tying_pt_interpolation_component::e_ss };
        case tying_pt_interpolation_component::e_sz:
            return { tying_pt_interpolation_component::e_ss, tying_pt_interpolation_component::e_zz };
        case tying_pt_interpolation_component::e_rz:
            return { tying_pt_interpolation_component::e_rr, tying_pt_interpolation_component::e_zz };
        case tying_pt_interpolation_component::e_sr:
            return { tying_pt_interpolation_component::e_ss, tying_pt_interpolation_component::e_rr };
        case tying_pt_interpolation_component::e_zs:
            return { tying_pt_interpolation_component::e_zz, tying_pt_interpolation_component::e_ss };
        case tying_pt_interpolation_component::e_zr:
            return { tying_pt_interpolation_component::e_zz, tying_pt_interpolation_component::e_rr };
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
