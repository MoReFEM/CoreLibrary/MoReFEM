// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_ENUM_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef>
#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief Enum class to fetch data for each component. Note that for interpolation rules that do not use every component,
     * we define their internal data arrays directly at the quadrature points.
     *
     */
    enum class tying_pt_interpolation_component : Eigen::Index {
        e_rr,
        e_ss,
        e_zz,
        e_rs,
        e_sz,
        e_rz,
        e_sr,
        e_zs,
        e_zr,
        Ncomponents,
        none // placeholder for internal functionalities
    };


    /*!
     * \brief Returns the 'symmetric' component of \a component, e.g. \a tying_pt_interpolation_component::e_sz for \a component =
     * \a tying_pt_interpolation_component::e_zs.
     *
     * \param[in] component Component which symmetric is sought.
     *
     * If the component is itself symmetric, by convention this function returns \a
     * tying_pt_interpolation_component::none.
     *
     * \return The symmetric component if \a component is not symmetric already, or \a tying_pt_interpolation_component::none otherwise.
     */
    tying_pt_interpolation_component Symmetric(tying_pt_interpolation_component component);

    /*!
     * \brief Returns the 'pair' of symmetric component forf \a component
     *
     * \param[in] component Component under study.
     *
     * The rule to determine the pair is given e_xy (where \a x and \a y in { 'r', 's', 'z' }:
     *
     *  e_xy -> (e_xx, e_yy).
     *
     * This rule is applied as such when \a x = \a y, so for instance:
     *
     * e_rr -> (e_rr, e_rr)
     *
     * \return The pair of symmetric component with the rule described above.
     */
    std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>
    ComputePair(tying_pt_interpolation_component component);


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
