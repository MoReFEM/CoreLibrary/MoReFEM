// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
#include <algorithm>
#include <cassert>
#include <cstddef>
#include <memory>
#include <utility>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/FillInternalArrayHelper.hpp" // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }  // lines 36-36

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    namespace // anonymous
    {


        /*!
         * \brief Helper function to compute the product of the gradients of the finite element shape functions
         *  at the tying points.
         *
         * \param[in] dphi_at_tying_point Gradient of the finite element shape functions for the unknown at a tying point.
         * \param[in] dphi_test_at_tying_point Gradient of the finite element shape functions for the test function at
         *  a the tying point.
         * \param[in] i First tensorial component considered.
         * \param[in] j Second tensorial component considered.
         * \param[in,out] grad_grad_out Local matrix containing the product of gradients.
         *
         */
        void ComputeGradGrad(
            const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_at_tying_point,
            const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_test_at_tying_point,
            const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& tensor_component_pair,
            Eigen::MatrixXd& grad_grad_out);


    } // namespace


    FillInternalArrayHelper::FillInternalArrayHelper(const RefGeomElt& ref_geom_elt,
                                                     const Advanced::RefFEltInLocalOperator& ref_felt,
                                                     const Advanced::RefFEltInLocalOperator& test_ref_felt)
    : ref_geom_elt_(ref_geom_elt), ref_felt_(ref_felt), test_ref_felt_(test_ref_felt)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

        const auto& basic_test_ref_felt = test_ref_felt.GetBasicRefFElt();
        const auto Nnode_test = basic_test_ref_felt.NlocalNode();

        const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();
        const auto Nnode = basic_ref_felt.NlocalNode();

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_test.Get() * 3, Nnode.Get() * 3 }, // helper_grad_grad
            { Nnode_test.Get() * 3, Nnode.Get() * 3 }  // work_grad_grad
        } });
    }


    void FillInternalArrayHelper::NonInterpolatedComponent(tying_pt_interpolation_component component)
    {
        auto quad_pt_as_sole_tying_pt = std::make_shared<TyingPoint>(
            1., GetCurrentQuadraturePoint(), GetRefGeomElt(), GetRefFElt(), GetTestRefFElt());

        assert(!(!quad_pt_as_sole_tying_pt));
        const auto& tying_point = *quad_pt_as_sole_tying_pt;

        const auto component_pair = ComputePair(component);

        auto& grad_grad_global = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            FillInternalArrayHelper::LocalMatrixIndex::grad_grad_global)>();

        ComputeGradGrad(
            tying_point.GetFEltGradient(), tying_point.GetTestFEltGradient(), component_pair, grad_grad_global);

        data_array_[static_cast<std::size_t>(EnumUnderlyingType(component))] =
            std::make_unique<TyingPointDataForComponent>(quad_pt_as_sole_tying_pt, grad_grad_global);
    }


    void FillInternalArrayHelper ::Reset(
        const Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint& infos_at_quad_pt)
    {
        decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
        const auto& quad_pt = quad_pt_unknown_list_data.GetQuadraturePoint();

        current_quad_pt_ = &quad_pt;

#ifndef NDEBUG
        std::ranges::fill(data_array_, nullptr);
#endif // NDEBUG
    }


    void FillInternalArrayHelper::ComputeInterpolatedComponent(TyingPoint::vector_const_shared_ptr tying_point_list,
                                                               tying_pt_interpolation_component component)
    {
        auto& grad_grad_contribution = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            FillInternalArrayHelper::LocalMatrixIndex::grad_grad_contribution)>();
        auto& grad_grad_global = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            FillInternalArrayHelper::LocalMatrixIndex::grad_grad_global)>();

        grad_grad_contribution.setZero();
        grad_grad_global.setZero();

        for (const auto& tying_point_ptr : tying_point_list)
        {
            assert(!(!tying_point_ptr));
            const auto& tying_point = *tying_point_ptr;

            ComputeGradGrad(tying_point.GetFEltGradient(),
                            tying_point.GetTestFEltGradient(),
                            ComputePair(component),
                            grad_grad_contribution);

            grad_grad_global += tying_point.GetShapeFunctionValue() * grad_grad_contribution;
        }

        const auto comp_as_size_t = static_cast<std::size_t>(component);
        assert(comp_as_size_t < data_array_.size());
        data_array_[comp_as_size_t] =
            std::make_unique<TyingPointDataForComponent>(std::move(tying_point_list), grad_grad_global);
    }


    namespace // anonymous
    {


        void ComputeGradGrad(
            const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_at_tying_point,
            const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type& dphi_test_at_tying_point,
            const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& tensor_component_pair,
            Eigen::MatrixXd& grad_grad_out)
        {
            grad_grad_out.setZero();

            const auto Nnode = LocalNodeNS::index_type{ dphi_at_tying_point.rows() };
            const auto Nnode_test = LocalNodeNS::index_type{ dphi_test_at_tying_point.rows() };
            constexpr auto Ndimension = GeometryNS::dimension_type{ 3 };
            assert(dphi_at_tying_point.cols() == Ndimension.Get());
            assert(grad_grad_out.rows() == 3 * Nnode_test.Get() && grad_grad_out.cols() == 3 * Nnode.Get());

            const auto i = EnumUnderlyingType(tensor_component_pair.first);
            const auto j = EnumUnderlyingType(tensor_component_pair.second);

            for (auto dim = GeometryNS::dimension_type{}; dim < Ndimension; ++dim)
            {
                const auto k = dim.Get();

                for (auto row_node = LocalNodeNS::index_type{}; row_node < Nnode_test; ++row_node)
                {
                    const auto m = row_node.Get();

                    for (auto col_node = LocalNodeNS::index_type{}; col_node < Nnode; ++col_node)
                    {
                        const auto n = col_node.Get();

                        grad_grad_out(m + k * Nnode_test.Get(), n + k * Nnode.Get()) =
                            dphi_test_at_tying_point(m, i) * dphi_at_tying_point(n, j);
                    }
                }
            }
        }
    } // namespace

} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
