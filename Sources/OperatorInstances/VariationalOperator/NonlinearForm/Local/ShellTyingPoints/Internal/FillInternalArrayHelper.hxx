// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_FILLINTERNALARRAYHELPER_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_FILLINTERNALARRAYHELPER_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/FillInternalArrayHelper.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Geometry/Coords/LocalCoords.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    // clang-format off
    template
    <
        class PlaneDataT
    >
    // clang-format on
    void FillInternalArrayHelper ::InterpolatedCase(
        Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component component)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);


        // In the code written by Jerome, there were sometimes an inversion between the 2d components in a given plane
        // and most of the time not.
        // I have observed it was when the place components (e.g. 'RZ') were not aligned with \a component.
        // (it has been of course tested on the TyingPointPolicies tests).
        // I have therefore simplified the interface to hide the switching of local coords here.
        const bool do_invert = (PlaneDataT::related_tying_point_component != component);

        const auto tying_point_list = BuildTyingPointList<PlaneDataT>(do_invert);

        ComputeInterpolatedComponent(tying_point_list, component);

        const auto symmetric_component = Symmetric(component);

        if (symmetric_component
            != Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component::none)
            ComputeInterpolatedComponent(tying_point_list, symmetric_component);
    }


    inline const RefGeomElt& FillInternalArrayHelper::GetRefGeomElt() const noexcept
    {
        return ref_geom_elt_;
    }

    inline const Advanced::RefFEltInLocalOperator& FillInternalArrayHelper::GetRefFElt() const noexcept
    {
        return ref_felt_;
    }

    inline const Advanced::RefFEltInLocalOperator& FillInternalArrayHelper::GetTestRefFElt() const noexcept
    {
        return test_ref_felt_;
    }

    inline const QuadraturePoint& FillInternalArrayHelper::GetCurrentQuadraturePoint() const noexcept
    {
        assert(!(!current_quad_pt_));
        return *current_quad_pt_;
    }


    inline auto FillInternalArrayHelper::Result() noexcept -> data_array_type&&
    {
        assert(std::none_of(
            data_array_.cbegin(), data_array_.cend(), Utilities::IsNullptr<TyingPointDataForComponent::unique_ptr>));
        return std::move(data_array_);
    }


    template<class PlaneDataT>
    TyingPoint::vector_const_shared_ptr FillInternalArrayHelper ::BuildTyingPointList(bool do_invert) const
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

        const auto& tying_points_coords_list = PlaneDataT::GetTyingPointsCoords();

        decltype(auto) quad_pt = GetCurrentQuadraturePoint();
        const auto ret_shape_function = PlaneDataT::ComputeShapeFunction(quad_pt, do_invert);

        TyingPoint::vector_const_shared_ptr ret;
        ret.reserve(tying_points_coords_list.size());

        const auto t_coord_quad_pt = quad_pt.t();

        decltype(auto) ref_geom_elt = GetRefGeomElt();
        decltype(auto) ref_felt = GetRefFElt();
        decltype(auto) test_ref_felt = GetTestRefFElt();
        assert(tying_points_coords_list.size() == ret_shape_function.size());

        auto tying_index = 0UL;

        for (const auto& tying_points_coords_2d : tying_points_coords_list)
        {
            assert(tying_points_coords_2d.size() == 2UL);

            auto tying_pt_local_coords =
                do_invert ? LocalCoords{ tying_points_coords_2d[1], tying_points_coords_2d[0], t_coord_quad_pt }
                          : LocalCoords{ tying_points_coords_2d[0], tying_points_coords_2d[1], t_coord_quad_pt };

            ret.emplace_back(std::make_shared<TyingPoint>(ret_shape_function[tying_index],
                                                          std::move(tying_pt_local_coords),
                                                          ref_geom_elt,
                                                          ref_felt,
                                                          test_ref_felt));

            ++tying_index;
        }

        return ret;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_FILLINTERNALARRAYHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
