// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"
#include "Geometry/StrongType.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/Alias.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    TyingPoint::TyingPoint(double shape_function_value,
                           ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type&& dphi_geo,
                           ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type&& dphi_felt,
                           ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type&& dphi_test_felt)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : shape_function_value_(shape_function_value), dphi_geo_(dphi_geo), dphi_felt_(dphi_felt),
      dphi_test_felt_(dphi_test_felt)
    { }


    auto ComputeGeometricGradientAtLocalCoords(const RefGeomElt& ref_geom_elt, const LocalCoords& local_coords)
        -> ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type
    {
        const auto Ngeometric_coords = LocalNodeNS::index_type{ ref_geom_elt.Ncoords().Get() };
        const auto dimension = ref_geom_elt.GetDimension();

        ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type work_dphi_geo(Ngeometric_coords.Get(), dimension.Get());
        work_dphi_geo.setZero();

        for (LocalNodeNS::index_type local_node_index{}; local_node_index < Ngeometric_coords; ++local_node_index)
        {
            for (::MoReFEM::GeometryNS::dimension_type component{}; component < dimension; ++component)
            {
                work_dphi_geo(local_node_index.Get(), component.Get()) =
                    ref_geom_elt.FirstDerivateShapeFunction(local_node_index, component, local_coords);
            }
        }

        return work_dphi_geo;
    }


    auto ComputeFEltGradientAtLocalCoords(const Internal::RefFEltNS::BasicRefFElt& ref_felt,
                                          const LocalCoords& local_coords)
        -> ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type
    {
        const auto Nnode = ref_felt.NlocalNode();
        const auto Ndimension = ref_felt.GetTopologyDimension();

        ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type work_dphi_felt;
        work_dphi_felt.resize(Nnode.Get(), Ndimension.Get());
        work_dphi_felt.setZero();

        for (LocalNodeNS::index_type local_node_index{}; local_node_index < Nnode; ++local_node_index)
        {
            for (::MoReFEM::GeometryNS::dimension_type component{}; component < Ndimension; ++component)
            {
                work_dphi_felt(local_node_index.Get(), component.Get()) =
                    ref_felt.FirstDerivateShapeFunction(local_node_index, component, local_coords);
            }
        }

        return work_dphi_felt;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
