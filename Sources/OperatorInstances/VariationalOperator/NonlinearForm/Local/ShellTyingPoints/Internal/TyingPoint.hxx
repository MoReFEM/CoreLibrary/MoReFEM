// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINT_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINT_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    template<class LocalCoordsT>
    TyingPoint::TyingPoint(double shape_function_value,
                           LocalCoordsT&& local_coords,
                           const RefGeomElt& ref_geom_elt,
                           const Advanced::RefFEltInLocalOperator& ref_felt,
                           const Advanced::RefFEltInLocalOperator& test_ref_felt)
    : TyingPoint(shape_function_value,
                 ComputeGeometricGradientAtLocalCoords(ref_geom_elt, local_coords),
                 ComputeFEltGradientAtLocalCoords(ref_felt.GetBasicRefFElt(), local_coords),
                 ComputeFEltGradientAtLocalCoords(test_ref_felt.GetBasicRefFElt(), local_coords))
    { }


    inline double TyingPoint::GetShapeFunctionValue() const noexcept
    {
        return shape_function_value_;
    }


    inline auto TyingPoint::GetGeometricGradient() const noexcept
        -> const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type&
    {
        return dphi_geo_;
    }


    inline auto TyingPoint::GetFEltGradient() const noexcept
        -> const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type&
    {
        return dphi_felt_;
    }


    inline auto TyingPoint::GetTestFEltGradient() const noexcept
        -> const ::MoReFEM::Advanced::RefFEltNS::gradient_matrix_type&
    {
        return dphi_test_felt_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
