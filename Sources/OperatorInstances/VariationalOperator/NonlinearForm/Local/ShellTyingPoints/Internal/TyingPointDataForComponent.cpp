// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <utility>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    TyingPointDataForComponent::TyingPointDataForComponent(TyingPoint::vector_const_shared_ptr tying_point_list,
                                                           Eigen::MatrixXd grad_grad_product)
    : tying_point_list_(std::move(tying_point_list)), grad_grad_product_(std::move(grad_grad_product))
    { }


    TyingPointDataForComponent::TyingPointDataForComponent(TyingPoint::const_shared_ptr tying_point_ptr,
                                                           Eigen::MatrixXd grad_grad_product)
    : TyingPointDataForComponent(TyingPoint::vector_const_shared_ptr{ std::move(tying_point_ptr) },
                                 std::move(grad_grad_product))
    { }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
