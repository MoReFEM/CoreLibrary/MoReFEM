// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINTDATAFORCOMPONENT_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINTDATAFORCOMPONENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief Class in charge of storing the relevant data used for the interpolation at tying points.
     *
     * There is one TyingPointDataForComponent object for each component and each quadrature point.
     */
    class TyingPointDataForComponent final // \todo #1664 Rename it with a clearer name!
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = TyingPointDataForComponent;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] tying_point_list List of \a TyingPoint list.
         * \param[in] grad_grad_product Product of the gradients of the test functions and the unknown
         *  shape functions directly interpolated from the tying points to the current quadrature point (hence we only
         *  store one matrix at each quadrature point) - or not interpolated if not required by the policy (\a None and
         * some of \a MITC4 components).
         *
         */
        explicit TyingPointDataForComponent(TyingPoint::vector_const_shared_ptr tying_point_list,
                                            Eigen::MatrixXd grad_grad_product);

        /*!
         * \brief Constructor for non interpolated points.
         *
         *  This one is typically called when there are no interpolation of tying points involved.
         *
         * \param[in] tying_point_ptr Sole tying point considered.
         * \param[in] grad_grad_product Product of the gradients of the test functions and the unknown
         *  shape functions directly interpolated from the tying points to the current quadrature point (hence we only
         *  store one matrix at each quadrature point) - or not interpolated if not required by the policy (\a None and
         * some of \a MITC4 components).
         *
         */
        explicit TyingPointDataForComponent(TyingPoint::const_shared_ptr tying_point_ptr,
                                            Eigen::MatrixXd grad_grad_product);


        //! Destructor.
        ~TyingPointDataForComponent() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TyingPointDataForComponent(const TyingPointDataForComponent& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TyingPointDataForComponent(TyingPointDataForComponent&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TyingPointDataForComponent& operator=(const TyingPointDataForComponent& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TyingPointDataForComponent& operator=(TyingPointDataForComponent&& rhs) = delete;

        ///@}


      public:
        //! Returns the product of the gradients of shape functions of the unknown and the test functions.
        const Eigen::MatrixXd& GetGradGradProduct() const noexcept;

        //! Accessor the the list of \a TyingPoint.
        const TyingPoint::vector_const_shared_ptr& GetTyingPointList() const noexcept;

      private:
        //! List of \a TyingPoint used for the computation.
        TyingPoint::vector_const_shared_ptr tying_point_list_;

        //! Matrix containing the product of the gradients of the test functions and the unknown
        //! shape functions directly interpolated from the tying points to the current quadrature point.
        //! It might be interpolated or not - it is if there is more than 1 item in \a tying_point_list_.
        const Eigen::MatrixXd grad_grad_product_;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINTDATAFORCOMPONENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
