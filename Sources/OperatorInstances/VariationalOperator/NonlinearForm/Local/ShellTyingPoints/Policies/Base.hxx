// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_BASE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_BASE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/Base.hpp"
// *** MoReFEM header guards *** < //


#include <array>
#include <cassert>
#include <cstddef>
#include <memory>

#include "Utilities/Containers/EnumClass.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS { class TyingPointDataForComponent; }
namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS { enum class tying_pt_interpolation_component : Eigen::Index; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    inline const TyingPointDataForComponent& Base::GetTyingPointDataForComponent(
        const QuadraturePoint& quad_pt,
        const Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component tying_pt_component)
        const
    {
        const auto quad_pt_index = static_cast<std::size_t>(quad_pt.GetIndex());
        assert(quad_pt_index < mitc_data_.size());

        decltype(auto) mitc_data_at_quad_pt = mitc_data_[quad_pt_index];
        const auto tying_pt_component_index = static_cast<std::size_t>(EnumUnderlyingType(tying_pt_component));
        assert(tying_pt_component_index < mitc_data_at_quad_pt.size());

        assert(!(!mitc_data_at_quad_pt[tying_pt_component_index]));
        return *mitc_data_at_quad_pt[tying_pt_component_index];
    }


    inline auto Base::GetNonCstMITCData() noexcept -> mitc_data_type&
    {
        return mitc_data_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_BASE_DOT_HXX_
// *** MoReFEM end header guards *** < //
