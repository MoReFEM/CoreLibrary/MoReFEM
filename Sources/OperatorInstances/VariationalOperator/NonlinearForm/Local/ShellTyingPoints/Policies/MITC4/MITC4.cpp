// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <cassert>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/MITC4/MITC4.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/FillInternalArrayHelper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers
namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    namespace // anonymous
    {


        struct PlaneRZ
        {

            static constexpr auto related_tying_point_component = tying_pt_interpolation_component::e_rz;


            /*!
             * \brief Array of the shape functions values for the tying point interpolation in the RZ plane.
             *
             * \param[in] r Local coordinates along r.
             * \param[in] s Local coordinates along s.
             * \return Array containing the shape function values.
             */
            static std::vector<double> ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert);


            static const std::array<std::array<double, 2UL>, 2UL>& GetTyingPointsCoords()
            {
                static const std::array<std::array<double, 2UL>, 2UL> ret{ { { { 0., -1. } }, { { 0., 1. } } } };

                return ret;
            }
        };


        struct PlaneZZ
        {


            static constexpr auto related_tying_point_component = tying_pt_interpolation_component::e_zz;


            /*!
             * \brief Array of the shape functions values for the tying point interpolation in the RZ plane.
             *
             * \param[in] r Local coordinates along r.
             * \param[in] s Local coordinates along s.
             * \return Array containing the shape function values.
             */
            static std::vector<double> ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert);

            static const std::array<std::array<double, 2UL>, 4UL>& GetTyingPointsCoords()
            {
                static const std::array<std::array<double, 2UL>, 4UL> ret{
                    { { { -1., -1. } }, { { 1., -1. } }, { { 1., 1. } }, { { -1., 1. } } }
                };

                return ret;
            }
        };


        using tying_pt_interpolation_component =
            ::MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component;


    } // namespace


    const std::string& MITC4::ClassName()
    {
        static const std::string name("MITC4");
        return name;
    }


    void MITC4::InitTyingPointData(
        const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>& infos_at_quad_pt_list,
        const RefGeomElt& ref_geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const Advanced::RefFEltInLocalOperator& test_ref_felt)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);
#ifndef NDEBUG
        {
            const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();
            const auto Ngeometric_coords = ref_geom_elt.Ncoords();
            const auto ref_felt_space_dimension = basic_ref_felt.GetTopologyDimension();

            constexpr auto euclidean_dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };

            assert(ref_felt_space_dimension == euclidean_dimension
                   && "This interpolation rule requires volumic elements.");

            assert(basic_ref_felt.NlocalNode().Get() == Ngeometric_coords.Get()
                   && "This interpolation rule requires isoparametric elements ");
        }
#endif // NDEBUG

        const auto Nquad_pt = infos_at_quad_pt_list.size();

        auto& mitc_data = GetNonCstMITCData();
        mitc_data.reserve(Nquad_pt);

        FillInternalArrayHelper helper(ref_geom_elt, ref_felt, test_ref_felt);

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            helper.Reset(infos_at_quad_pt);

            helper.InterpolatedCase<PlaneZZ>(tying_pt_interpolation_component::e_zz);
            helper.InterpolatedCase<PlaneRZ>(tying_pt_interpolation_component::e_sz);
            helper.InterpolatedCase<PlaneRZ>(tying_pt_interpolation_component::e_rz);

            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_rr);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_rs);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_sr);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_ss);

            mitc_data.push_back(std::move(helper.Result()));
        }
    }


    namespace // anonymous
    {


        std::vector<double> PlaneRZ::ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert)
        {
            const auto s = do_invert ? quad_pt.r() : quad_pt.s();

            std::vector<double> ret{ (1. - s) * .5, (1. + s) * .5 };
            return ret;
        }


        std::vector<double> PlaneZZ::ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert)
        {
            const auto r = do_invert ? quad_pt.s() : quad_pt.r();
            const auto s = do_invert ? quad_pt.r() : quad_pt.s();

            std::vector<double> ret{ (r - 1.) * (s - 1.) * .25,
                                     -(r + 1.) * (s - 1.) * .25,
                                     (r + 1.) * (s + 1.) * .25,
                                     -(r - 1.) * (s + 1.) * .25 };
            return ret;
        }

    } // namespace


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
