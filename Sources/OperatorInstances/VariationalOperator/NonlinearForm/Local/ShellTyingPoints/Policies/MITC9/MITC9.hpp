// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_MITC9_MITC9_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_MITC9_MITC9_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/Base.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }
namespace MoReFEM::Advanced::LocalVariationalOperatorNS { class InformationAtQuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    //! \copydoc doxygen_hide_mitc9_shell_policy
    class MITC9 : public Base
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MITC9;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        //! Constructor which does not initialize the object completely. A call to InitTyingPointData() is required.
        explicit MITC9() = default;

        //! Destructor.
        ~MITC9() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        MITC9(const MITC9& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MITC9(MITC9&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MITC9& operator=(const MITC9& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MITC9& operator=(MITC9&& rhs) = delete;

        ///@}


      public:
        //! \copydoc doxygen_hide_init_point_data_prototype
        void InitTyingPointData(const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>&
                                    infos_at_quad_pt_list,
                                const RefGeomElt& ref_geom_elt,
                                const Advanced::RefFEltInLocalOperator& ref_felt,
                                const Advanced::RefFEltInLocalOperator& test_ref_felt) override;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_MITC9_MITC9_DOT_HPP_
// *** MoReFEM end header guards *** < //
