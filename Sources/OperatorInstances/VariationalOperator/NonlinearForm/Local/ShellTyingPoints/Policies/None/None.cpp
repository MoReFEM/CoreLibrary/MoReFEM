// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/None/None.hpp" // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/FillInternalArrayHelper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    namespace // anonymous
    {

        using tying_pt_interpolation_component =
            ::MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component;


    } // namespace

    const std::string& None::ClassName()
    {
        static const std::string name("None");
        return name;
    }


    void None::InitTyingPointData(
        const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>& infos_at_quad_pt_list,
        const RefGeomElt& ref_geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const Advanced::RefFEltInLocalOperator& test_ref_felt)
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);
#ifndef NDEBUG
        {
            const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();
            const auto ref_felt_space_dimension = basic_ref_felt.GetTopologyDimension();
            constexpr auto euclidean_dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };
            assert(ref_felt_space_dimension == euclidean_dimension
                   && "This interpolation rule requires volumic elements.");
        }
#endif // NDEBUG

        const auto Nquad_pt = infos_at_quad_pt_list.size();
        auto& mitc_data = GetNonCstMITCData();
        mitc_data.reserve(Nquad_pt);

        Internal::LocalVariationalOperatorNS::TyingPointsNS::FillInternalArrayHelper helper(
            ref_geom_elt, ref_felt, test_ref_felt);

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            helper.Reset(infos_at_quad_pt);

            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_rr);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_rs);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_rz);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_sr);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_ss);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_sz);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_zr);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_zs);
            helper.NonInterpolatedComponent(tying_pt_interpolation_component::e_zz);

            mitc_data.push_back(std::move(helper.Result()));
        }
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
