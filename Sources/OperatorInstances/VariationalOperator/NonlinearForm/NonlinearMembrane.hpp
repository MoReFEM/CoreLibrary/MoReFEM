// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARMEMBRANE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARMEMBRANE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp" // IWYU pragma: keep

#include "Operators/Enum.hpp"                                                // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearMembrane.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{

    /*!
     * \brief Operator description.
     *
     * \todo #9 Describe operator!
     */
    // clang-format off
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class NonlinearMembrane final
    : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
    <
        NonlinearMembrane<TimeManagerT>,
        Advanced::OperatorNS::Nature::nonlinear,
        Advanced::LocalVariationalOperatorNS::NonlinearMembrane<TimeManagerT>
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = NonlinearMembrane<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_operator_type = Advanced::LocalVariationalOperatorNS::NonlinearMembrane<TimeManagerT>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::nonlinear,
                local_operator_type
            >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Unique ptr.
        using const_unique_ptr = std::unique_ptr<const NonlinearMembrane>;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = typename local_operator_type::scalar_parameter_type;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_unknown_list_param
         * \param[in] youngs_modulus Young's modulus of the solid.
         * \param[in] poisson_ratio Poisson ratio of the solid.
         * \param[in] thickness Thickness of the solid.
         * \param[in] pretension Pretension (related computation are skipped if the underlying value is 0.).
         * \copydoc doxygen_hide_gvo_felt_space_arg
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit NonlinearMembrane(const FEltSpace& felt_space,
                                   Unknown::const_shared_ptr unknown_list,
                                   Unknown::const_shared_ptr test_unknown_list,
                                   const scalar_parameter_type& youngs_modulus,
                                   const scalar_parameter_type& poisson_ratio,
                                   const scalar_parameter_type& thickness,
                                   const scalar_parameter_type& pretension,
                                   const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Destructor.
        ~NonlinearMembrane() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NonlinearMembrane(const NonlinearMembrane& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonlinearMembrane(NonlinearMembrane&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonlinearMembrane& operator=(const NonlinearMembrane& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonlinearMembrane& operator=(NonlinearMembrane&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
         * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] previous_iteration_data Value from previous time iteration.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      const GlobalVector& previous_iteration_data,
                      const Domain& domain = Domain()) const;


      public:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        template<class LocalOperatorTypeT>
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         LocalOperatorTypeT& local_operator,
                                         const std::tuple<const GlobalVector&>& additional_arguments) const;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARMEMBRANE_DOT_HPP_
// *** MoReFEM end header guards *** < //
