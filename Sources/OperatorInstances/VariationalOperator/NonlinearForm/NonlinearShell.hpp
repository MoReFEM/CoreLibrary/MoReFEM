// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARSHELL_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARSHELL_DOT_HPP_
// *** MoReFEM header guards *** < //


// # include "Parameters/ParameterType.hpp"

#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearShell.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/ShellTyingPointsPolicy/MITC9.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief NonLinearShell operator.
     *
     * Refer to the documentation Documentation/Operators/3D-shells.pdf for details.
     */

    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    class NonlinearShell final
    : public GlobalVariationalOperatorNS::SameForAllRefGeomElt<
          NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>,
          Advanced::OperatorNS::Nature::nonlinear,
          typename Advanced::LocalVariationalOperatorNS::NonlinearShell<typename HyperelasticityPolicyT::local_policy,
                                                                        typename TyingPointPolicyT::local_policy>>,
      public HyperelasticityPolicyT,
      public TyingPointPolicyT
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_operator_type =
            typename Advanced::LocalVariationalOperatorNS::NonlinearShell<typename HyperelasticityPolicyT::local_policy,
                                                                          typename TyingPointPolicyT::local_policy>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        using parent = GlobalVariationalOperatorNS::
            SameForAllRefGeomElt<self, Advanced::OperatorNS::Nature::nonlinear, local_operator_type>;

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Unique ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * one unknown (the displacement).
         * \copydoc doxygen_hide_test_unknown_list_param
         * \param[in] hyperelastic_law Hyperelastic law considered. Do not delete this pointer!
         * \copydoc doxygen_hide_gvo_felt_space_arg
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit NonlinearShell(const FEltSpace& felt_space,
                                Unknown::const_shared_ptr unknown_list,
                                Unknown::const_shared_ptr test_unknown_list,
                                const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Destructor.
        ~NonlinearShell() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NonlinearShell(const NonlinearShell& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonlinearShell(NonlinearShell&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonlinearShell& operator=(const NonlinearShell& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonlinearShell& operator=(NonlinearShell&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
         * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] previous_iteration_data Value from previous time iteration.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      const GlobalVector& previous_iteration_data,
                      const Domain& domain = Domain()) const;


      public:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        template<class LocalOperatorTypeT>
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         LocalOperatorTypeT& local_operator,
                                         const std::tuple<const GlobalVector&>& additional_arguments) const;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearShell.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARSHELL_DOT_HPP_
// *** MoReFEM end header guards *** < //
