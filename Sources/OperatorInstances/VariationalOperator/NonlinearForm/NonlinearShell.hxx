// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARSHELL_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARSHELL_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearShell.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::NonlinearShell(
        const FEltSpace& felt_space,
        Unknown::const_shared_ptr unknown_list,
        Unknown::const_shared_ptr test_unknown_list,
        const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_list,
             test_unknown_list,
             std::move(quadrature_rule_per_topology),
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             hyperelastic_law)
    {
        assert(unknown_list->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list->GetNature() == UnknownNS::Nature::vectorial);
    }


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    const std::string& NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::ClassName()
    {
        static std::string name("NonlinearShell");
        return name;
    }


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                                        const GlobalVector& input_vector,
                                                                        const Domain& domain) const
    {
        parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
    }


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    template<class LocalOperatorTypeT>
    inline void NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<const GlobalVector&>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments),
                              local_operator.GetNonCstFormerLocalDisplacement());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_NONLINEARSHELL_DOT_HXX_
// *** MoReFEM end header guards *** < //
