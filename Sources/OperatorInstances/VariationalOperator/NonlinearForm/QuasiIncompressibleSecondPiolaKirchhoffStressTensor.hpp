// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLESECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLESECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"

namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief Quasi-incompressible second Piola-Kirchhoff stress tensor operator.
     *
     * This operator is a bit peculiar: it is not directly a child of one of the basic class used to define
     * a \a GlobalVariationalOperator, but merely an ad-hoc object which initializes under the hood 2 such operators
     * and also defines a \a Assemble method which in fact calls assembling for the two internal operators.
     *
     * The rationale for this is that when the incompressible behaviour is expected both operators are actually
     * assembled at the same time, so this simplifies the reading of the code in the model.
     *
     * \internal We could have done as in Stokes operator and define a full-fledged global operator with its associated
     * \a LocalVariationalOperator, but due to the complexity of \a SecondPiolaKirchhoffStressTensor the duplication
     * of the code of the local operator would be far from trivial, contrary to Stokes case.
     * \endinternal
     *
     * \internal We could also have enriched \a SecondPiolaKirchhoffStressTensor operator to deal directly with both
     * cases, but this operator is already by far the most complex operator in MoReFEM and it could have become even
     * more unwieldy.
     * \endinternal
     *
     * \internal Of course, in due time if some additional efficiency is sought writing the unique operator (probably
     * by making incompressibility a template parameter or a policy of SecondPiolaKirchhoffStressTensor) is still
     * an option.
     * \endinternal
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    class QuasiIncompressibleSecondPiolaKirchhoffStressTensor final
    : private SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT, TimeManagerT>,
      private MixedSolidIncompressibilityPolicyT<HydrostaticLawPolicyT>
    // clang-format on
    {

      public:
        // clang-format off
        //! \copydoc doxygen_hide_alias_self
        using self = QuasiIncompressibleSecondPiolaKirchhoffStressTensor
        <
            TimeManagerT,
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            InternalVariablePolicyT,
            HydrostaticLawPolicyT,
            MixedSolidIncompressibilityPolicyT
        >;
        // clang-format on

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias for the stiffnes operator.
        using stiffness_operator_parent = SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                                           ViscoelasticityPolicyT,
                                                                           InternalVariablePolicyT,
                                                                           TimeManagerT>;

        //! Alias for the penalization operator.
        using penalization_operator_parent = MixedSolidIncompressibilityPolicyT<HydrostaticLawPolicyT>;

        //! 'Inherited' alias from stiffness operator.
        using cauchy_green_tensor_type = typename stiffness_operator_parent::cauchy_green_tensor_type;

        //! 'Inherited' alias from stiffness operator.
        using input_internal_variable_policy_type =
            typename stiffness_operator_parent::input_internal_variable_policy_type;

        //! 'Inherited'  strong type for displacement vectors.
        using ConstRefDisplacementGlobalVector = typename stiffness_operator_parent::ConstRefDisplacementGlobalVector;

        //! 'Inherited'  strong type for velocity vectors.
        using ConstRefVelocityGlobalVector = typename stiffness_operator_parent::ConstRefVelocityGlobalVector;

        //! 'Inherited'  strong type for electrical activation vectors.
        using ConstRefPreviousElectricalActivationGlobalVector =
            typename stiffness_operator_parent::ConstRefPreviousElectricalActivationGlobalVector;

        //! 'Inherited'  strong type for electrical activation vectors.
        using ConstRefCurrentElectricalActivationGlobalVector =
            typename stiffness_operator_parent::ConstRefCurrentElectricalActivationGlobalVector;

        //! 'Inherited'  strong type for monolithic deviatoric vectors.
        using ConstRefMonolithicDeviatoricGlobalVector =
            typename penalization_operator_parent::ConstRefMonolithicDeviatoricGlobalVector;

        //! 'Inherited'  strong type for monolithic volumetric vectors.
        using ConstRefMonolithicVolumetricGlobalVector =
            typename penalization_operator_parent::ConstRefMonolithicVolumetricGlobalVector;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_gvo_felt_space_arg
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         *
         * \param[in] unknown_list Container with vectorial then scalar unknown.
         * \copydoc doxygen_hide_test_unknown_list_param
         *
         * \param[in] hydrostatic_law_volumetric_part Volumetric part of the hydrostatic law policy; do not
         * delete this pointer!
         * \param[in] hydrostatic_law_deviatoric_part  Deviatoric part of the hydrostatic law policy; do not
         * delete this pointer!
         * \param[in] solid Object which provides the required material parameters for the solid.
         * \param[in] time_manager \a TimeManager needed for Cauchy Green parameter, Viscoelasticity and Active Stress.
         * \param[in] input_internal_variable_policy  Object required only for internal variable (e.g. if the internal
         * variable is active stress it is useful to compute U0 and U1 locally).
         * \param[in] hyperelastic_law Hyperelastic law considered. Do not delete this pointer!
         */
        explicit QuasiIncompressibleSecondPiolaKirchhoffStressTensor(
            const FEltSpace& felt_space,
            const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
            const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
            const Solid<TimeManagerT>& solid,
            const TimeManagerT& time_manager,
            const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
            const HydrostaticLawPolicyT* hydrostatic_law_volumetric_part,
            const HydrostaticLawPolicyT* hydrostatic_law_deviatoric_part,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology,
            input_internal_variable_policy_type* input_internal_variable_policy);


        //! Destructor.
        ~QuasiIncompressibleSecondPiolaKirchhoffStressTensor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        QuasiIncompressibleSecondPiolaKirchhoffStressTensor(
            const QuasiIncompressibleSecondPiolaKirchhoffStressTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        QuasiIncompressibleSecondPiolaKirchhoffStressTensor(QuasiIncompressibleSecondPiolaKirchhoffStressTensor&& rhs) =
            delete;

        //! \copydoc doxygen_hide_copy_affectation
        QuasiIncompressibleSecondPiolaKirchhoffStressTensor&
        operator=(const QuasiIncompressibleSecondPiolaKirchhoffStressTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        QuasiIncompressibleSecondPiolaKirchhoffStressTensor&
        operator=(QuasiIncompressibleSecondPiolaKirchhoffStressTensor&& rhs) = delete;

        ///@}

      public:
        //! \copydoc doxygen_hide_second_piola_assemble_1
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefMonolithicDeviatoricGlobalVector state_previous_iteration,
                      const Domain& domain = Domain()) const;


        //! \copydoc doxygen_hide_second_piola_assemble_2
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                      const Domain& domain = Domain()) const;


        //! \copydoc doxygen_hide_second_piola_assemble_3
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const bool do_update_sigma_c,
                      const Domain& domain = Domain()) const;


        //! \copydoc doxygen_hide_second_piola_assemble_4
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const Domain& domain = Domain()) const;


        //! \copydoc doxygen_hide_second_piola_assemble_5
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const bool do_update_sigma_c,
                      const Domain& domain = Domain()) const;


        //! \copydoc doxygen_hide_second_piola_assemble_6
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const Domain& domain = Domain()) const;

        /*!
         * \class doxygen_hide_second_piola_assemble_7
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there are neither viscoelasticity nor active policy
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] volumetric_vector Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_7
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefMonolithicDeviatoricGlobalVector state_previous_iteration,
                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                      const Domain& domain = Domain()) const;

        /*!
         * \class doxygen_hide_second_piola_assemble_8
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there are neither viscoelasticity nor active policy
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] volumetric_vector Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_8
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                      const Domain& domain = Domain()) const;

        /*!
         * \class doxygen_hide_second_piola_assemble_9
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there are neither viscoelasticity nor active policy
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] velocity_previous_iteration Vector that includes the velocity from the previous iteration.
         * Used for Viscoelasticity Policy (its nature varies depending on the time scheme used).
         * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from
         * the previous time. Used for InternalVariablePolicy.
         * \param[in] electrical_activation_at_time Vector that includes the electrical activation at the current
         * time. Used for InternalVariablePolicy.
         * \param[in] volumetric_vector Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_9
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                      const Domain& domain = Domain()) const;

        //! Method required by some of the more advanced InternalVariablePolicy (namely those used in CardiacMechanics).
        void RestartInternalVariablesAtPreviousTimeStep();

        //! Method required by some of the more advanced InternalVariablePolicy (namely those used in CardiacMechanics).
        void UpdateInternalVariablesBetweenTimeStep();
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLESECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
