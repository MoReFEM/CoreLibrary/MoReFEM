// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLESECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLESECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::GlobalVariationalOperatorNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    QuasiIncompressibleSecondPiolaKirchhoffStressTensor
    <
        TimeManagerT,
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT
    >::QuasiIncompressibleSecondPiolaKirchhoffStressTensor(
            // clang-format on
            const FEltSpace& felt_space,
            const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
            const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
            const Solid<TimeManagerT>& solid,
            const TimeManagerT& time_manager,
            const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
            const HydrostaticLawPolicyT* hydrostatic_law_volumetric_part,
            const HydrostaticLawPolicyT* hydrostatic_law_deviatoric_part,
            const QuadratureRulePerTopology* const a_quadrature_rule_per_topology,
            input_internal_variable_policy_type* input_internal_variable_policy)
    : stiffness_operator_parent(felt_space,
                                unknown_list[0],
                                test_unknown_list[0],
                                solid,
                                time_manager,
                                hyperelastic_law,
                                a_quadrature_rule_per_topology,
                                input_internal_variable_policy),
      penalization_operator_parent(felt_space,
                                   unknown_list,
                                   test_unknown_list,
                                   stiffness_operator_parent::GetCauchyGreenTensor(),
                                   time_manager,
                                   hydrostatic_law_volumetric_part,
                                   hydrostatic_law_deviatoric_part,
                                   a_quadrature_rule_per_topology)
    {
        assert(!(!unknown_list[0]));
        assert(!(!unknown_list[1]));
        assert(!(!test_unknown_list[0]));
        assert(!(!test_unknown_list[1]));
        assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
        assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        TimeManagerT,
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefMonolithicDeviatoricGlobalVector state_previous_iteration,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefDisplacementGlobalVector(state_previous_iteration.Get()), domain);

        penalization_operator_parent::Assemble(linear_algebra_tuple, state_previous_iteration, domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        TimeManagerT,
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefDisplacementGlobalVector state_previous_iteration,
                                                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(
            linear_algebra_tuple, state_previous_iteration, velocity_previous_iteration, domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<TimeManagerT,
                                                             HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefVelocityGlobalVector velocity_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const bool do_update_sigma_c,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            velocity_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            do_update_sigma_c,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<TimeManagerT,
                                                             HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefVelocityGlobalVector velocity_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            velocity_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<TimeManagerT,
                                                             HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const bool do_update_sigma_c,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            do_update_sigma_c,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<TimeManagerT,
                                                             HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        TimeManagerT,
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefMonolithicDeviatoricGlobalVector state_previous_iteration,
                                                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefDisplacementGlobalVector(state_previous_iteration.Get()), domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, state_previous_iteration, volumetric_vector, domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        TimeManagerT,
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefDisplacementGlobalVector state_previous_iteration,
                                                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple, state_previous_iteration, domain);

        penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()),
                                               volumetric_vector,
                                               domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<TimeManagerT,
                                                             HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefVelocityGlobalVector velocity_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            velocity_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            domain);

        penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()),
                                               volumetric_vector,
                                               domain);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        TimeManagerT,
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::RestartInternalVariablesAtPreviousTimeStep()
    {
        stiffness_operator_parent::RestartInternalVariablesAtPreviousTimeStep();
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        class HydrostaticLawPolicyT,
        template<class>
        class MixedSolidIncompressibilityPolicyT
    >
    // clang-format on
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        TimeManagerT,
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::UpdateInternalVariablesBetweenTimeStep()
    {
        stiffness_operator_parent::UpdateInternalVariablesBetweenTimeStep();
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLESECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
