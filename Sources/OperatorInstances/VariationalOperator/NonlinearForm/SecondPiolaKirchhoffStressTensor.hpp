// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <set>

#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "Core/Parameter/TypeEnum.hpp"

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/Internal/ApplySetCauchyGreenTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief Instantiation of the SecondPiolaKirchhoffStressTensor related to hyperelastic matrix.
     *
     * \tparam HyperelasticityPolicyT Policy that defines if an hyperelastic contribution is present in the tensor.
     * \tparam ViscoelasticityPolicyT Policy that defines if an viscoelastic contribution is present in the tensor.
     * \tparam InternalVariablePolicyT Policy that defines if an internal variable contribution (such as active
     * stress) is present in the tensor.
     */
    // clang-format off
    template
    <
        class HyperelasticityPolicyT,
        class ViscoelasticityPolicyT,
        class InternalVariablePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    class SecondPiolaKirchhoffStressTensor
    // clang-format off
    : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
    <
        SecondPiolaKirchhoffStressTensor
        <
            HyperelasticityPolicyT,
            ViscoelasticityPolicyT,
            InternalVariablePolicyT,
            TimeManagerT
        >,
        Advanced::OperatorNS::Nature::nonlinear,
        typename Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor
        <
            typename HyperelasticityPolicyT::local_policy,
            typename ViscoelasticityPolicyT::local_policy,
            typename InternalVariablePolicyT::local_policy,
            TimeManagerT
        >
    >,
     // clang-format on
     public HyperelasticityPolicyT,
     public ViscoelasticityPolicyT,
     public InternalVariablePolicyT
    {

      public:
        //! Convenient alias.
        using self = SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                      ViscoelasticityPolicyT,
                                                      InternalVariablePolicyT,
                                                      TimeManagerT>;


        //! Alias to local operator.
        // clang-format off
        using local_operator_type =
        typename Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor
        <
            typename HyperelasticityPolicyT::local_policy,
            typename ViscoelasticityPolicyT::local_policy,
            typename InternalVariablePolicyT::local_policy,
            TimeManagerT
        >;
        // clang-format on

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
        using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            self,
            Advanced::OperatorNS::Nature::nonlinear,
            local_operator_type
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_namespace_cluttering
        using input_internal_variable_policy_type =
            typename InternalVariablePolicyT::input_internal_variable_policy_type;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;

        //! Unique ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to CauchyGreenTensor.
        using cauchy_green_tensor_type = typename local_operator_type::cauchy_green_tensor_type;

        //! Alias for Cauchy-Green tensor update operator.
        using update_cauchy_green_tensor_operator_type =
            GlobalParameterOperatorNS::UpdateCauchyGreenTensor<TimeManagerT,
                                                               ::MoReFEM::ParameterNS::TimeDependencyNS::None,
                                                               Wrappers::EigenNS::dWVector>;

        //! Strong type for displacement vectors.
        using ConstRefDisplacementGlobalVector = StrongType<const GlobalVector&, struct DisplacementTag>;

        //! Strong type for velocity vectors.
        using ConstRefVelocityGlobalVector = StrongType<const GlobalVector&, struct VelocityTag>;

        //! Strong type for electrical activation vectors.
        using ConstRefPreviousElectricalActivationGlobalVector =
            StrongType<const GlobalVector&, struct PreviousElectricalActivationTag>;

        //! Strong type for electrical activation vectors.
        using ConstRefCurrentElectricalActivationGlobalVector =
            StrongType<const GlobalVector&, struct CurrentElectricalActivationTag>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_gvo_felt_space_arg
         *
         * \param[in] unknown_ptr Vectorial unknown considered (should be a displacement).
         * \param[in] test_unknown_ptr Vectorial unknown considered for test function.
         * \param[in] solid Object which provides the required material parameters for the solid.
         * \param[in] time_manager Time manager need for Viscoelasticity and Active Stress.
         * \param[in] input_internal_variable_policy Object required only for internal variable (e.g. if the internal
         * variable is active stress it is useful to compute U0 and U1 locally).
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         * \param[in] hyperelastic_law Hyperelastic law considered. Do not delete this pointer!
         */
        explicit SecondPiolaKirchhoffStressTensor(
            const FEltSpace& felt_space,
            const Unknown::const_shared_ptr& unknown_ptr,
            const Unknown::const_shared_ptr& test_unknown_ptr,
            const Solid<TimeManagerT>& solid,
            const TimeManagerT& time_manager,
            const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr,
            input_internal_variable_policy_type* input_internal_variable_policy = nullptr);

        //! Destructor.
        ~SecondPiolaKirchhoffStressTensor() = default;

        //! \copydoc doxygen_hide_move_constructor
        SecondPiolaKirchhoffStressTensor(SecondPiolaKirchhoffStressTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_constructor
        SecondPiolaKirchhoffStressTensor(const SecondPiolaKirchhoffStressTensor& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SecondPiolaKirchhoffStressTensor& operator=(const SecondPiolaKirchhoffStressTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SecondPiolaKirchhoffStressTensor& operator=(SecondPiolaKirchhoffStressTensor&& rhs) = delete;


        ///@}

        //! Share Access to the Cauchy Green Tensor "Global Parameter"
        const cauchy_green_tensor_type& GetCauchyGreenTensor() const noexcept;

        /*!
         * \class doxygen_hide_second_piola_assemble_1
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there are neither viscoelasticity nor active policy
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_1
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      const Domain& domain = Domain()) const;

        /*!
         * \class doxygen_hide_second_piola_assemble_2
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there is viscoelasticity and no active stress
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] velocity_previous_iteration Vector that includes the velocity from the previous iteration. Used
         * for Viscoelasticity Policy (its nature varies depending on the time scheme used).
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_2
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                      const Domain& domain = Domain()) const;


        /*!
         * \class doxygen_hide_second_piola_assemble_3
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there are both active stress and visoelasticity
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] velocity_previous_iteration Vector that includes the velocity from the previous iteration.
         * Used for Viscoelasticity Policy.(its nature
         * varies depending on the time scheme used).
         * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from
         * the previous time. Used for InternalVariablePolicy.
         * \param[in] electrical_activation_at_time Vector that includes the electrical activation at the current
         * time. Used for InternalVariablePolicy.
         * \param[in] do_update_sigma_c Indicates if it is the first Newton loop or not to advance Sigma c in time.
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_3
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const bool do_update_sigma_c,
                      const Domain& domain = Domain()) const;


        /*!
         * \class doxygen_hide_second_piola_assemble_4
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there are both active stress and visoelasticity
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] velocity_previous_iteration Vector that includes the velocity from the previous iteration.
         * Used for Viscoelasticity Policy.(its nature
         * varies depending on the time scheme used).
         * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from the
         * previous time. Used for InternalVariablePolicy.
         * \param[in] electrical_activation_at_time Vector that includes the electrical activation at the current
         * time. Used for InternalVariablePolicy.
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_4
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const Domain& domain = Domain()) const;


        /*!
         * \class doxygen_hide_second_piola_assemble_5
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there is active stress and no visoelasticity
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from the
         * previous time. Used for InternalVariablePolicy. \param[in] electrical_activation_at_time Vector that includes
         * the electrical activation at the current time. Used for InternalVariablePolicy. \param[in] do_update_sigma_c
         * Indicates if it is the first Newton loop or not to advance Sigma c in time.
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_5
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const bool do_update_sigma_c,
                      const Domain& domain = Domain()) const;


        /*!
         * \class doxygen_hide_second_piola_assemble_6
         *
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * This overload is expected to be used when there is active stress and no visoelasticity
         * (hyperelasticity doesn't matter as it requires no additional arguments here).
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixAndCoefficient and/or
         * \a GlobalVectorAndCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] state_previous_iteration Vector that includes data from the previous iteration. (its nature
         * varies depending on the time scheme used).
         * \param[in] electrical_activation_previous_time Vector that includes the electrical activation from the
         * previous time. Used for InternalVariablePolicy. \param[in] electrical_activation_at_time Vector that includes
         * the electrical activation at the current time. Used for InternalVariablePolicy.
         *
         */

        //! \copydoc doxygen_hide_second_piola_assemble_6
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector state_previous_iteration,
                      ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                      ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                      const Domain& domain = Domain()) const;

      public:
        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
         * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
         * and understand, and induces unwanted copies).
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally. Here just one vector for the displacement.
         */
        template<class LocalOperatorTypeT>
        void
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple<ConstRefDisplacementGlobalVector>& additional_arguments) const;

        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
         * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
         * and understand, and induces unwanted copies).
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally. Here two vectors for the displacement and the velocity.
         */
        // clang-format off
        template<class LocalOperatorTypeT>
        void
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple
                                    <
                                        ConstRefDisplacementGlobalVector,
                                        ConstRefVelocityGlobalVector
                                    >& additional_arguments) const;
        // clang-format on


        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
         * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
         * and understand, and induces unwanted copies).
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally. Here four vectors for the displacement, the velocity, the two electrical activations and
         * a boolean to indicate if sigma c must be updated or not.
         */
        // clang-format off
        template<class LocalOperatorTypeT>
        void
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple
                                    <
                                        ConstRefDisplacementGlobalVector,
                                        ConstRefVelocityGlobalVector,
                                        ConstRefPreviousElectricalActivationGlobalVector,
                                        ConstRefCurrentElectricalActivationGlobalVector,
                                        const bool
                                    >& additional_arguments) const;
        // clang-format on

        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
         * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
         * and understand, and induces unwanted copies).
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally. Here four vectors for the displacement, the velocity and the two electrical activations.
         */
        // clang-format off
        template<class LocalOperatorTypeT>
        void
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple
                                    <
                                        ConstRefDisplacementGlobalVector,
                                        ConstRefVelocityGlobalVector,
                                        ConstRefPreviousElectricalActivationGlobalVector,
                                        ConstRefCurrentElectricalActivationGlobalVector
                                    >& additional_arguments) const;
        // clang-format on

        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
         * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
         * and understand, and induces unwanted copies).
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally. Here three vectors for the displacement and the two electrical activations and
         * a boolean to indicate if sigma c must be updated or not.
         */
        // clang-format off
        template<class LocalOperatorTypeT>
        void
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple
                                    <
                                        ConstRefDisplacementGlobalVector,
                                        ConstRefPreviousElectricalActivationGlobalVector,
                                        ConstRefCurrentElectricalActivationGlobalVector,
                                        const bool
                                    >& additional_arguments) const;
        // clang-format on


        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * These arguments are data attributes of the LocalVariationalOperator, for the sake of simplicity (a former
         * scheme where the arguments appeared directly in ComputeEltArray() prototype was much harder to implement
         * and understand, and induces unwanted copies).
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally. Here three vectors for the displacement and the two electrical activations.
         */
        // clang-format off
        template<class LocalOperatorTypeT>
        void
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple
                                    <
                                        ConstRefDisplacementGlobalVector,
                                        ConstRefPreviousElectricalActivationGlobalVector,
                                        ConstRefCurrentElectricalActivationGlobalVector
                                    >& additional_arguments) const;
        // clang-format on

      private:
        //! Accessor to Cauchy Green tensor operator.
        const update_cauchy_green_tensor_operator_type& GetCauchyGreenTensorOperator() const noexcept;


      private:
        //! CauchyGreen tensor.
        typename cauchy_green_tensor_type::unique_ptr cauchy_green_tensor_ = nullptr;

        //! Cauchy Green operator.
        typename update_cauchy_green_tensor_operator_type::const_unique_ptr cauchy_green_tensor_operator_ = nullptr;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
