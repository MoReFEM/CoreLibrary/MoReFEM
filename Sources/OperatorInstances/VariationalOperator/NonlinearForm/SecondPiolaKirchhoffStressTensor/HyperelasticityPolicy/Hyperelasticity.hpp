// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
{


    /*!
     * \brief Policy to use when there is an hyperelastic law considered in the operator.
     *
     * \tparam HyperelasticLawT Hyperelastic law to use; some are defined in Operators/HyperelasticLaw
     * folder (and HyperelasticLawNS namespace). You may use more specific one; see Poromechanics model in
     * which an extended version of StVenantKirchhoff is used.
     *
     * \attention Current class only stores a reference to the hyperelastic law object to use; it is assumed
     * its lifespan exceeds the operator's one.
     */
    template<class HyperelasticLawT>
    class Hyperelasticity
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy = Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
            HyperelasticityPolicyNS::Hyperelasticity<HyperelasticLawT>;

        //! Convenient alias upon template parameter.
        using law_type = HyperelasticLawT;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Hyperelasticity<HyperelasticLawT>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Hyperelasticity() = default;

        //! Destructor.
        ~Hyperelasticity() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Hyperelasticity(const Hyperelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Hyperelasticity(Hyperelasticity&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Hyperelasticity& operator=(const Hyperelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Hyperelasticity& operator=(Hyperelasticity&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
