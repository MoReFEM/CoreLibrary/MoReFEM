// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_APPLYSETCAUCHYGREENTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_APPLYSETCAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{


    /*!
     * \brief Metaprogramming helper to call SetCauchyGreenTensor() for all \a LocalVariationalOperator hidden
     * in \a LocalOperatorTupleT.
     *
     * \tparam doxygen_hide_global_operator_local_operator_tuple_type
     * \tparam Index of the tuple item currently handled in the recursion "loop".
     * \tparam SizeTupleT Must be std::tuple_size<LocalOperatorTupleT>::value; there only to provide the
     * stopping condition of the recursion.
     */
    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t I, std::size_t SizeTupleT>
    struct ApplySetCauchyGreenTensor
    {


        static_assert(std::tuple_size<LocalOperatorTupleT>::value == SizeTupleT, "");


        /*!
         * \brief Static method that does the actual work.
         *
         * \copydoc doxygen_hide_global_operator_local_operator_tuple_in
         * \param[in] cauchy_green_tensor_raw_ptr Raw pointer to a \a CauchyGreenTensor.
         */
        template<class CauchyGreenTypeT>
        static void Perform(const LocalOperatorTupleT& local_operator_tuple,
                            const CauchyGreenTypeT& cauchy_green_tensor_raw_ptr);
    };


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Stopping condition for the recursive call.
    // ============================


    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t SizeTupleT>
    struct ApplySetCauchyGreenTensor<LocalOperatorTupleT, SizeTupleT, SizeTupleT>
    {


        static_assert(std::tuple_size<LocalOperatorTupleT>::value == SizeTupleT, "");


        template<class CauchyGreenTypeT>
        static void Perform(const LocalOperatorTupleT&, const CauchyGreenTypeT&);
    };


    // ============================
    // Stopping condition for the recursive call.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/Internal/ApplySetCauchyGreenTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_APPLYSETCAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
