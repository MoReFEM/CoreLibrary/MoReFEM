// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_APPLYSETCAUCHYGREENTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_APPLYSETCAUCHYGREENTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/Internal/ApplySetCauchyGreenTensor.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{


    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t I, std::size_t SizeTupleT>
    template<class CauchyGreenTypeT>
    void ApplySetCauchyGreenTensor<LocalOperatorTupleT, I, SizeTupleT>::Perform(
        const LocalOperatorTupleT& local_operator_tuple,
        const CauchyGreenTypeT& cauchy_green_tensor_raw_ptr)
    {
        const auto& local_operator_item = std::get<I>(local_operator_tuple);

        if (local_operator_item.IsRelevant())
            local_operator_item.GetNonCstLocalOperator().SetCauchyGreenTensor(cauchy_green_tensor_raw_ptr);

        ApplySetCauchyGreenTensor<LocalOperatorTupleT, I + 1, SizeTupleT>::Perform(local_operator_tuple,
                                                                                   cauchy_green_tensor_raw_ptr);
    }


    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t SizeTupleT>
    template<class CauchyGreenTypeT>
    void ApplySetCauchyGreenTensor<LocalOperatorTupleT, SizeTupleT, SizeTupleT>::Perform(const LocalOperatorTupleT&,
                                                                                         const CauchyGreenTypeT&)
    { }


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_APPLYSETCAUCHYGREENTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
