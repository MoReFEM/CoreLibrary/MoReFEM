// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    //! Implements an active stress Sigma_c*Tau*Tau, with dSigma_c/dt = sigma0 + |u|+ - |u|dSigma_c.
    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class AnalyticalPrestress
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy = ::MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
            InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT, TimeManagerT>;

        //! Alias to the type of the input.
        using input_internal_variable_policy_type = typename local_policy::input_internal_variable_policy_type;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = AnalyticalPrestress<FiberIndexT, TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using param_at_quad_pt_type = ParameterAtQuadraturePoint<ParameterNS::Type::scalar, TimeManagerT>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit AnalyticalPrestress() = default;

        //! Destructor.
        ~AnalyticalPrestress() = default;

        //! \copydoc doxygen_hide_copy_constructor
        AnalyticalPrestress(const AnalyticalPrestress& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AnalyticalPrestress(AnalyticalPrestress&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        AnalyticalPrestress& operator=(const AnalyticalPrestress& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AnalyticalPrestress& operator=(AnalyticalPrestress&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Initialize Sigma_c.
         *
         * \param[in] domain \a Domain.
         * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
         * \param[in] time_manager \a TimeManager of the model.
         * \param[in] local_operator_storage Tuple with all local operators considered.
         * \param[in] input_internal_variable_policy Object with relevant input data.
         */
        template<class LocalOperatorStorageT>
        void InitializeInternalVariablePolicy(const Domain& domain,
                                              const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                              const TimeManagerT& time_manager,
                                              const LocalOperatorStorageT& local_operator_storage,
                                              input_internal_variable_policy_type* input_internal_variable_policy);

      private:
        //! Accessor.
        const input_internal_variable_policy_type& GetInputActiveStress() const noexcept;

        //! Accessor.
        input_internal_variable_policy_type& GetNonCstInputActiveStress() noexcept;

      private:
        //! Object with relevant data for internal variable.
        input_internal_variable_policy_type* input_internal_variable_policy_ = nullptr;

      private:
        //! Sigma_c.
        typename param_at_quad_pt_type::unique_ptr sigma_c_ = nullptr;
    };

} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM end header guards *** < //
