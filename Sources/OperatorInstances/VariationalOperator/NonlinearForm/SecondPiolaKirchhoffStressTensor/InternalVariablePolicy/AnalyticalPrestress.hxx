// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LocalOperatorStorageT>
    void AnalyticalPrestress<FiberIndexT, TimeManagerT>::InitializeInternalVariablePolicy(
        const Domain& domain,
        const QuadratureRulePerTopology& quadrature_rule_per_topology,
        const TimeManagerT& time_manager,
        const LocalOperatorStorageT& local_operator_storage,
        input_internal_variable_policy_type* input_internal_variable_policy)
    {
        assert(!(!input_internal_variable_policy));

        input_internal_variable_policy_ = input_internal_variable_policy;

        sigma_c_ = std::make_unique<param_at_quad_pt_type>("SigmaC",
                                                           domain,
                                                           quadrature_rule_per_topology,
                                                           GetInputActiveStress().GetInitialValueInternalVariable(),
                                                           time_manager);

        param_at_quad_pt_type* raw_sigma_c = sigma_c_.get();


        ::MoReFEM::Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
            SetSigmaCHelper<LocalOperatorStorageT, 0, std::tuple_size<LocalOperatorStorageT>::value>::Perform(
                raw_sigma_c, local_operator_storage);
    }


    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const typename AnalyticalPrestress<FiberIndexT, TimeManagerT>::input_internal_variable_policy_type&
    AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetInputActiveStress() const noexcept
    {
        assert(!(!input_internal_variable_policy_));
        return *input_internal_variable_policy_;
    }


    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline typename AnalyticalPrestress<FiberIndexT, TimeManagerT>::input_internal_variable_policy_type&
    AnalyticalPrestress<FiberIndexT, TimeManagerT>::GetNonCstInputActiveStress() noexcept
    {
        return const_cast<input_internal_variable_policy_type&>(GetInputActiveStress());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_ANALYTICALPRESTRESS_DOT_HXX_
// *** MoReFEM end header guards *** < //
