// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_HELPER_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Parameters/ParameterAtQuadraturePoint.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"


namespace MoReFEM::Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
{


    /*!
     * \class doxygen_hide_internal_variable_helper_init_function
     *
     * \brief Static method that inits properly internal variable (such as active stress).

     * \param[in] domain \a Domain upon which the SecondPiolaKirchhoffStressTensor operator is defined.
     * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
     *
     * \copydetails doxygen_hide_time_manager_arg
     *
     * \param[in] local_operator_storage List of \a LocalVariationalOperator considered in the
     * SecondPiolaKirchhoffStressTensor sort per \a RefGeomElt (container is an associative one).
     * \param[in] input_internal_variable_policy Input internal policy.
     * \param[in,out] internal_variable Internal variable to be initialized.
     */

    /*!
     * \class doxygen_hide_internal_variable_helper_match_dof_function
     *
     * \brief Static method that match dofs between the scalar unknown underneath the internal variable and
     * the vectorial one considered in the operator.
     *
     * \param[in] internal_variable Internal variable for which the matching is performed.
     *
     * \return The object that store the pairings found.
     */


    /*!
     * \brief Helper struct used to call
     * SecondPiolaKirchhoffStressTensor::InitializeInternalVariablePolicy() if the InternalVariablePolicyT
     * is not InternalVariablePolicyNS::None.
     *
     * \tparam InternalVariablePolicyT Internal variable policy.
     */
    template<class InternalVariablePolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct InternalVariableHelper
    {


        //! \copydoc doxygen_hide_internal_variable_helper_init_function
        template<class LocalOperatorStorageT>
        static void InitInternalVariable(
            const Domain& domain,
            const QuadratureRulePerTopology& quadrature_rule_per_topology,
            const TimeManagerT& time_manager,
            const LocalOperatorStorageT& local_operator_storage,
            typename InternalVariablePolicyT::input_internal_variable_policy_type* input_internal_variable_policy,
            InternalVariablePolicyT& internal_variable);
    };


    /*!
     * \brief Helper struct used to call
     * SecondPiolaKirchhoffStressTensor::InitializeInternalVariablePolicy() if the InternalVariablePolicyT
     * is InternalVariablePolicyNS::None.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
     * InternalVariablePolicyNS::None policy in which all the methods would be asked to do nothing.
     * \endinternal
     *
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct InternalVariableHelper<::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                                      InternalVariablePolicyNS::None<TimeManagerT>,
                                  TimeManagerT>
    {

        //! Convenient alias.
        using none_type =
            ::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<
                TimeManagerT>;


        //! \copydoc doxygen_hide_internal_variable_helper_init_function
        template<class LocalOperatorStorageT>
        static void
        InitInternalVariable(const Domain& domain,
                             const QuadratureRulePerTopology& quadrature_rule_per_topology,
                             const TimeManagerT& time_manager,
                             const LocalOperatorStorageT& local_operator_storage,
                             typename none_type::input_internal_variable_policy_type* input_internal_variable_policy,
                             none_type& internal_variable);
    };


    /*!
     * \brief Metaprogramming struct to call SetSigmaC for all elements of a local operator tuple.
     *
     * \copydoc doxygen_hide_global_operator_local_operator_tuple_type
     *
     * \tparam I Position of the element being considered currently in the recursive call.
     * \tparam TupleSizeT Must be std::tuple_size<LocalOperatorTupleT>::value; used for the stopping
     * condition of the recursive call.
     */
    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t I, std::size_t TupleSizeT>
    struct SetSigmaCHelper
    {


        static_assert(TupleSizeT == std::tuple_size<LocalOperatorTupleT>::value, "");

        /*!
         * \brief Static method that does the actual work.
         *
         * \param[in] raw_sigma_c Raw pointer to an adequate \a Parameter for sigma_c value.
         * \copydoc doxygen_hide_global_operator_local_operator_tuple_in
         */
        template<class ParamAtQuadPtT>
        static void Perform(ParamAtQuadPtT* raw_sigma_c, const LocalOperatorTupleT& local_operator_tuple);
    };


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Stopping condition for the recursive call.
    // ============================


    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t TupleSizeT>
    struct SetSigmaCHelper<LocalOperatorTupleT, TupleSizeT, TupleSizeT>
    {

        template<class ParamAtQuadPtT>
        static void Perform(ParamAtQuadPtT*, const LocalOperatorTupleT&);
    };


    // ============================
    // Stopping condition for the recursive call.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
