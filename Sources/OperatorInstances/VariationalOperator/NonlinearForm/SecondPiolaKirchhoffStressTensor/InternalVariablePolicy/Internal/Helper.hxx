// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_HELPER_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_HELPER_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Internal/Helper.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
{


    template<class InternalVariablePolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LocalOperatorStorageT>
    void InternalVariableHelper<InternalVariablePolicyT, TimeManagerT>::InitInternalVariable(
        const Domain& domain,
        const QuadratureRulePerTopology& quadrature_rule_per_topology,
        const TimeManagerT& time_manager,
        const LocalOperatorStorageT& local_operator_storage,
        typename InternalVariablePolicyT::input_internal_variable_policy_type* input_internal_variable_policy,
        InternalVariablePolicyT& internal_variable)
    {
        internal_variable.InitializeInternalVariablePolicy(
            domain, quadrature_rule_per_topology, time_manager, local_operator_storage, input_internal_variable_policy);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LocalOperatorStorageT>
    void InternalVariableHelper<::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                                    InternalVariablePolicyNS::None<TimeManagerT>,
                                TimeManagerT>::
        InitInternalVariable([[maybe_unused]] const Domain& domain,
                             [[maybe_unused]] const QuadratureRulePerTopology& quadrature_rule_per_topology,
                             [[maybe_unused]] const TimeManagerT& time_manager,
                             [[maybe_unused]] const LocalOperatorStorageT& local_operator_storage,
                             [[maybe_unused]]
                             typename none_type::input_internal_variable_policy_type* input_internal_variable_policy,
                             [[maybe_unused]] none_type& internal_variable)
    { }


    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t I, std::size_t TupleSizeT>
    template<class ParamAtQuadPtT>
    void SetSigmaCHelper<LocalOperatorTupleT, I, TupleSizeT>::Perform(ParamAtQuadPtT* raw_sigma_c,
                                                                      const LocalOperatorTupleT& local_operator_tuple)
    {
        const auto& local_operator_item = std::get<I>(local_operator_tuple);

        if (local_operator_item.IsRelevant())
            local_operator_item.GetNonCstLocalOperator().SetSigmaC(raw_sigma_c);

        SetSigmaCHelper<LocalOperatorTupleT, I + 1, TupleSizeT>::Perform(raw_sigma_c, local_operator_tuple);
    }


    template<::MoReFEM::Concept::Tuple LocalOperatorTupleT, std::size_t TupleSizeT>
    template<class ParamAtQuadPtT>
    void SetSigmaCHelper<LocalOperatorTupleT, TupleSizeT, TupleSizeT>::Perform(ParamAtQuadPtT*,
                                                                               const LocalOperatorTupleT&)
    {
        // Do nothing.
    }


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_INTERNAL_HELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
