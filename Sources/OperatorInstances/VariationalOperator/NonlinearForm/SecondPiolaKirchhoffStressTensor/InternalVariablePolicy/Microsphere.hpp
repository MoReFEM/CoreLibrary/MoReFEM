// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    //! Implements a microsphere model for the two families of fibers with in and out plane dispersions.
    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    class Microsphere
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy = ::MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
            InternalVariablePolicyNS::Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>;

        //! Alias to the type of the input.
        using input_internal_variable_policy_type = typename local_policy::input_internal_variable_policy_type;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Microsphere() = default;

        //! Destructor.
        ~Microsphere() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Microsphere(const Microsphere& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Microsphere(Microsphere&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Microsphere& operator=(const Microsphere& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Microsphere& operator=(Microsphere&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Initialize Sigma_c.
         *
         * \param[in] domain \a Domain.
         * \copydoc doxygen_hide_quadrature_rule_per_topology_arg
         * \param[in] time_manager \a TimeManager of the model.
         * \param[in] local_operator_storage Tuple with all local operators considered.
         * \param[in] input_internal_variable_policy Object with relevant input data.
         */
        template<class LocalOperatorStorageT>
        void InitializeInternalVariablePolicy(const Domain& domain,
                                              const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                              const TimeManagerT& time_manager,
                                              const LocalOperatorStorageT& local_operator_storage,
                                              input_internal_variable_policy_type* input_internal_variable_policy);

      private:
        //! Accessor.
        const input_internal_variable_policy_type& GetInputMicrosphere() const noexcept;

        //! Accessor.
        input_internal_variable_policy_type& GetNonCstInputMicrosphere() noexcept;

      private:
        //! Object with relevant data for internal variable.
        input_internal_variable_policy_type* input_internal_variable_policy_ = nullptr;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HPP_
// *** MoReFEM end header guards *** < //
