// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    template<class LocalOperatorStorageT>
    void Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::InitializeInternalVariablePolicy(
        [[maybe_unused]] const Domain& domain,
        [[maybe_unused]] const QuadratureRulePerTopology& quadrature_rule_per_topology,
        [[maybe_unused]] const TimeManagerT& time_manager,
        [[maybe_unused]] const LocalOperatorStorageT& local_operator_storage,
        input_internal_variable_policy_type* input_internal_variable_policy)
    {
        assert(!(!input_internal_variable_policy));

        input_internal_variable_policy_ = input_internal_variable_policy;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const typename Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::input_internal_variable_policy_type&
    Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetInputMicrosphere() const noexcept
    {
        assert(!(!input_internal_variable_policy_));
        return *input_internal_variable_policy_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline typename Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::input_internal_variable_policy_type&
    Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetNonCstInputMicrosphere() noexcept
    {
        return const_cast<input_internal_variable_policy_type&>(GetInputMicrosphere());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
// *** MoReFEM end header guards *** < //
