// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_NONE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_NONE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS
{

    /*!
     * \brief Policy to choose when there is no visco-elasticity.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class None
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy =
            Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<
                TimeManagerT>;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = None<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit None() = default;

        //! Destructor.
        ~None() = default;

        //! \copydoc doxygen_hide_copy_constructor
        None(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        None(None&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        None& operator=(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        None& operator=(None&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_NONE_DOT_HPP_
// *** MoReFEM end header guards *** < //
