// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

// IWYU pragma: begin_exports
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS
{

    /*!
     * \brief Policy to choose when there is visco-elasticity involved in the operator.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::
                 DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    class Viscoelasticity
    {
      public:
        //! Alias to the local policy operator.
        using local_policy = Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::
            ViscoelasticityPolicyNS::Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Viscoelasticity() = default;

        //! Destructor.
        ~Viscoelasticity() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Viscoelasticity(const Viscoelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Viscoelasticity(Viscoelasticity&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Viscoelasticity& operator=(const Viscoelasticity& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Viscoelasticity& operator=(Viscoelasticity&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
