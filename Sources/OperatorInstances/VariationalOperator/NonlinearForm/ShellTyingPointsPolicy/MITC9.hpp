// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SHELLTYINGPOINTSPOLICY_MITC9_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SHELLTYINGPOINTSPOLICY_MITC9_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/MITC9/MITC9.hpp"

namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \class doxygen_hide_mitc9_shell_policy
     *
     * \brief Tying point policy for MITC9 elements.
     *
     */


    //! \copydoc doxygen_hide_mitc9_shell_policy
    class MITC9
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy = MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS::MITC9;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MITC9;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit MITC9() = default;

        //! Destructor.
        ~MITC9() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MITC9(const MITC9& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MITC9(MITC9&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MITC9& operator=(const MITC9& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MITC9& operator=(MITC9&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SHELLTYINGPOINTSPOLICY_MITC9_DOT_HPP_
// *** MoReFEM end header guards *** < //
