// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_INTERPOLATIONDATA_DOT_HPP_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_INTERPOLATIONDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>

#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


namespace MoReFEM::Advanced::ConformInterpolatorNS
{


    /*!
     * \brief A convenient class which holds relevant data to perform a conform interpolation.
     */
    class InterpolationData
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = InterpolationData;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] source_data Data to be projected onto the target.
         * \param[in] target_data Where the data will be projected.
         */
        explicit InterpolationData(SourceOrTargetData::unique_ptr&& source_data,
                                   SourceOrTargetData::unique_ptr&& target_data);

        //! Destructor.
        ~InterpolationData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InterpolationData(const InterpolationData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InterpolationData(InterpolationData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InterpolationData& operator=(const InterpolationData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InterpolationData& operator=(InterpolationData&& rhs) = delete;

        ///@}

      public:
        //! Accessor to source data.
        const SourceOrTargetData& GetSourceData() const noexcept;

        //! Accessor to target data.
        const SourceOrTargetData& GetTargetData() const noexcept;

      private:
        //! Object which hols all relevant information about the source.
        SourceOrTargetData::unique_ptr source_data_ = nullptr;

        //! Object which hols all relevant information about the target.
        SourceOrTargetData::unique_ptr target_data_ = nullptr;
    };


} // namespace MoReFEM::Advanced::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/ConformInterpolator/Advanced/InterpolationData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_INTERPOLATIONDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
