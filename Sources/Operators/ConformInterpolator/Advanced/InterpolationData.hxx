// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_INTERPOLATIONDATA_DOT_HXX_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_INTERPOLATIONDATA_DOT_HXX_
// IWYU pragma: private, include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::ConformInterpolatorNS { class SourceOrTargetData; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ConformInterpolatorNS
{


    inline const SourceOrTargetData& InterpolationData::GetSourceData() const noexcept
    {
        assert(!(!source_data_));
        return *source_data_;
    }


    inline const SourceOrTargetData& InterpolationData::GetTargetData() const noexcept
    {
        assert(!(!target_data_));
        return *target_data_;
    }


} // namespace MoReFEM::Advanced::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_INTERPOLATIONDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
