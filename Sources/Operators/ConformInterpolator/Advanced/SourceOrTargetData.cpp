// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <type_traits> // IWYU pragma: keep

#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"

#include "Utilities/Miscellaneous.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/EnumUnknown.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM::Advanced::ConformInterpolatorNS
{


    SourceOrTargetData::SourceOrTargetData(const FEltSpace& felt_space,
                                           const NumberingSubset& numbering_subset,
                                           const pairing_type& pairing,
                                           SourceOrTargetDataType type)
    : felt_space_(felt_space), numbering_subset_(numbering_subset)
    {
        Unknown::vector_const_shared_ptr unknown_list(pairing.size());

        switch (type)
        {
        case SourceOrTargetDataType::source:
        {
            std::ranges::transform(pairing,

                                   unknown_list.begin(),
                                   [](const auto& pair)
                                   {
                                       return pair.first;
                                   });
            break;
        }
        case SourceOrTargetDataType::target:
        {
            std::ranges::transform(pairing,

                                   unknown_list.begin(),
                                   [](const auto& pair)
                                   {
                                       return pair.second;
                                   });
            break;
        }
        }

        const auto mesh_dimension = felt_space.GetMeshDimension();

        for (const auto& unknown_ptr : unknown_list)
        {
            assert(unknown_ptr != nullptr);

            auto extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(*unknown_ptr);

            extended_unknown_list_.push_back(extended_unknown_ptr);

            Nunknown_component_ += static_cast<Eigen::Index>(unknown_ptr->GetNature() == UnknownNS::Nature::vectorial
                                                                 ? mesh_dimension
                                                                 : ::MoReFEM::GeometryNS::dimension_type{ 1 });
        }
    }


    const FEltSpace& SourceOrTargetData::GetFEltSpace() const noexcept
    {
        return felt_space_;
    }


    const NumberingSubset& SourceOrTargetData::GetNumberingSubset() const noexcept
    {
        return numbering_subset_;
    }


    const ExtendedUnknown::vector_const_shared_ptr& SourceOrTargetData ::GetExtendedUnknownList() const noexcept
    {
        return extended_unknown_list_;
    }


    const Internal::RefFEltNS::BasicRefFElt&
    SourceOrTargetData ::GetCommonBasicRefFElt(const Internal::RefFEltNS::RefLocalFEltSpace& ref_local_felt_space) const
    {

        const auto& ref_felt_list = ref_local_felt_space.GetRefFEltList();
        const auto& extended_unknown_list = GetExtendedUnknownList();

        // For this method to make sense, all extended unknown must share same unknown and numbering subset.
        assert(std::ranges::none_of(extended_unknown_list,

                                    Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));

        const auto& any_extended_unknown = *(extended_unknown_list.back());

        assert(std::ranges::all_of(extended_unknown_list,

                                   [&any_extended_unknown](const auto& current_unknown_ptr)
                                   {
                                       return current_unknown_ptr->GetNumberingSubset()
                                                  == any_extended_unknown.GetNumberingSubset()
                                              && current_unknown_ptr->GetShapeFunctionLabel()
                                                     == any_extended_unknown.GetShapeFunctionLabel();
                                   }));

        // Find any ref felt that is related to \a any_extended_unknown.

        auto it = std::ranges::find_if(ref_felt_list,

                                       [&any_extended_unknown](const auto& ref_felt_ptr)
                                       {
                                           assert(ref_felt_ptr != nullptr);
                                           return ref_felt_ptr->GetExtendedUnknown() == any_extended_unknown;
                                       });

        assert(it != ref_felt_list.cend());
        const auto& ref_felt_ptr = *it;
        assert(!(!ref_felt_ptr));
        return ref_felt_ptr->GetBasicRefFElt();
    }


} // namespace MoReFEM::Advanced::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
