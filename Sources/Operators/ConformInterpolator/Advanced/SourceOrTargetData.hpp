// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_SOURCEORTARGETDATA_DOT_HPP_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_SOURCEORTARGETDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/ConformInterpolator/Alias.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ConformInterpolatorNS
{


    //! Convenient alias to pairing.
    using pairing_type = ::MoReFEM::ConformInterpolatorNS::pairing_type;

    //! Enum class which specifies whether a source or a target is considered.
    enum class SourceOrTargetDataType { source, target };


    /*!
     * \class doxygen_hide_source_or_target_data_note
     *
     * \note Remember these objects go by pair: one is instantiated for the source, the other for the target.
     */


    /*!
     * \brief Helper object which includes most relevant data about the source or the target space.
     *
     * \copydoc doxygen_hide_source_or_target_data_note
     */
    class SourceOrTargetData
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SourceOrTargetData;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \class doxygen_hide_conform_interpolation_pairing_arg
         *
         * \param[in] pairing The list of pair source unknown/target unknown to consider. Depending on \a type,
         * only either the first or the second element of each pair is actually considered in this class.
         */

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_source_or_target_data_note
         *
         * \param[in] felt_space \a FEltSpace considered.
         * \param[in] numbering_subset \a NumberingSubset considered.
         * \copydoc doxygen_hide_conform_interpolation_pairing_arg
         * \param[in] type Whether the instantiated object deals with source or target data.
         *
         */
        explicit SourceOrTargetData(const FEltSpace& felt_space,
                                    const NumberingSubset& numbering_subset,
                                    const pairing_type& pairing,
                                    SourceOrTargetDataType type);


        //! Destructor.
        ~SourceOrTargetData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SourceOrTargetData(const SourceOrTargetData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SourceOrTargetData(SourceOrTargetData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SourceOrTargetData& operator=(const SourceOrTargetData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SourceOrTargetData& operator=(SourceOrTargetData&& rhs) = delete;

        ///@}

        //! Number of unknown components considered.
        Eigen::Index NunknownComponent() const noexcept;

        //! Access to the finite element space.
        const FEltSpace& GetFEltSpace() const noexcept;

        //! Access to the numbering subset.
        const NumberingSubset& GetNumberingSubset() const noexcept;

        //! Access to the extended unknown list relevant for the interpolator.
        const ExtendedUnknown::vector_const_shared_ptr& GetExtendedUnknownList() const noexcept;


        /*!
         * \brief Extract a BasicRefFElt that would be yielded by any of the extended unknown.
         *
         * This method is intended to be used in specific interpolators, such as P1_to_P2 into which all
         * extended unknowns of a given side source/target are expected to share the same numbering subset
         * and shape function label.
         *
         * \param[in] ref_local_felt_space \a RefLocalFEltSpace for which the \a BasicRefFElt is sought.
         *
         * \return \a BasicRefFElt matching the \a ref_local_felt_space.
         */
        const Internal::RefFEltNS::BasicRefFElt&
        GetCommonBasicRefFElt(const Internal::RefFEltNS::RefLocalFEltSpace& ref_local_felt_space) const;


      private:
        //! Finite element space.
        const FEltSpace& felt_space_;

        //! Numbering subset.
        const NumberingSubset& numbering_subset_;

        //! Number of unknown components considered in the felt space/numbering subset.
        Eigen::Index Nunknown_component_{};

        /*!
         * \brief List of unknowns inside the felt space/numbering subset considered by both side of the
         * interpolator.
         *
         * If for instance there is pressure in source finite element space but no counterpart in target
         * finite element space, this unknown will appear in neither of Source and Target objects.
         */
        ExtendedUnknown::vector_const_shared_ptr extended_unknown_list_;
    };


} // namespace MoReFEM::Advanced::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ADVANCED_SOURCEORTARGETDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
