// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ALIAS_DOT_HPP_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ALIAS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <utility>
#include <vector>

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM::ConformInterpolatorNS
{


    //! Convenient alias to pairing.
    using pairing_type = std::vector<std::pair<Unknown::const_shared_ptr, Unknown::const_shared_ptr>>;


} // namespace MoReFEM::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_ALIAS_DOT_HPP_
// *** MoReFEM end header guards *** < //
