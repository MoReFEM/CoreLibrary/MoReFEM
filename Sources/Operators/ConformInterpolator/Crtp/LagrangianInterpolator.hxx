// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_CRTP_LAGRANGIANINTERPOLATOR_DOT_HXX_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_CRTP_LAGRANGIANINTERPOLATOR_DOT_HXX_
// IWYU pragma: private, include "Operators/ConformInterpolator/Crtp/LagrangianInterpolator.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ConformInterpolatorNS::Crtp
{


    template<class DerivedT>
    LagrangianInterpolator<DerivedT>::LagrangianInterpolator(const FEltSpace& source_felt_space,
                                                             const NumberingSubset& source_numbering_subset,
                                                             const FEltSpace& target_felt_space,
                                                             const NumberingSubset& target_numbering_subset,
                                                             pairing_type&& pairing)
    : pairing_(std::move(pairing))
    {
        assert("Current interpolator assumes geometric conformity!"
               && source_felt_space.GetGodOfDofFromWeakPtr() == target_felt_space.GetGodOfDofFromWeakPtr());

        using type = Advanced::ConformInterpolatorNS::SourceOrTargetData;

        auto&& source_data = std::make_unique<type>(source_felt_space,
                                                    source_numbering_subset,
                                                    pairing_,
                                                    Advanced::ConformInterpolatorNS::SourceOrTargetDataType::source);

        auto&& target_data = std::make_unique<type>(target_felt_space,
                                                    target_numbering_subset,
                                                    pairing_,
                                                    Advanced::ConformInterpolatorNS::SourceOrTargetDataType::target);

        interpolation_data_ = std::make_unique<Advanced::ConformInterpolatorNS::InterpolationData>(
            std::move(source_data), std::move(target_data));
    }


    template<class DerivedT>
    template<typename... Args>
    void LagrangianInterpolator<DerivedT>::Init(Args&&... args)
    {
        AllocateInterpolationMatrix();
        static_cast<DerivedT&>(*this).ComputeInterpolationMatrix(std::forward<decltype(args)>(args)...);
    }


    template<class DerivedT>
    void LagrangianInterpolator<DerivedT>::AllocateInterpolationMatrix()
    {
        assert(interpolation_matrix_ == nullptr && "Should be called only once!");

        const auto& interpolation_data = GetInterpolationData();

        interpolation_matrix_ = std::make_unique<GlobalMatrix>(interpolation_data.GetTargetData().GetNumberingSubset(),
                                                               interpolation_data.GetSourceData().GetNumberingSubset());
    }


    template<class DerivedT>
    GodOfDof::const_shared_ptr LagrangianInterpolator<DerivedT>::GetGodOfDofFromWeakPtr() const noexcept
    {
        const auto& interpolation_data = GetInterpolationData();
        const auto& felt_space = interpolation_data.GetSourceData().GetFEltSpace();

        auto&& god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
        assert("Current interpolator assumes geometric conformity!"
               && god_of_dof_ptr->GetUniqueId()
                      == interpolation_data.GetTargetData().GetFEltSpace().GetGodOfDofFromWeakPtr()->GetUniqueId());

        return god_of_dof_ptr;
    }


    template<class DerivedT>
    inline const GlobalMatrix& LagrangianInterpolator<DerivedT>::GetInterpolationMatrix() const noexcept
    {
        assert(!(!interpolation_matrix_)
               && "Make sure Init() was properly called for the interpolator (constructor is not enough).");
        return *interpolation_matrix_;
    }


    template<class DerivedT>
    inline GlobalMatrix& LagrangianInterpolator<DerivedT>::GetNonCstInterpolationMatrix() const noexcept
    {
        return const_cast<GlobalMatrix&>(GetInterpolationMatrix());
    }


    template<class DerivedT>
    inline const Advanced::ConformInterpolatorNS::InterpolationData&
    LagrangianInterpolator<DerivedT>::GetInterpolationData() const noexcept
    {
        assert(!(!interpolation_data_));
        return *interpolation_data_;
    }


} // namespace MoReFEM::ConformInterpolatorNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_CRTP_LAGRANGIANINTERPOLATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
