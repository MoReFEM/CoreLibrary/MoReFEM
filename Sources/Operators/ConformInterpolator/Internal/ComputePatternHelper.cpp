// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <__tree>

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

#include "Operators/ConformInterpolator/Internal/ComputePatternHelper.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp" // IWYU pragma: keep

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


namespace MoReFEM::Internal::ConformInterpolatorNS
{


    namespace // anonymous
    {

        // Forward declaration.
        class LocalRowContent;


        /*!
         * \brief Take a local Eigen matrix and returns an expression of its content more easy to use in
         * ComputePattern() method.
         */
        template<class LocalMatrixT>
        std::vector<LocalRowContent> LocalMatrixContent(const LocalMatrixT& matrix);


        /*!
         * \brief Class that interprets in a more usable format the line of a local matrix.
         */
        class LocalRowContent
        {

          public:
            /// \name Special members.
            ///@{

            //! Constructor.
            explicit LocalRowContent() = default;

            //! Destructor.
            ~LocalRowContent() = default;

            //! Copy constructor.
            LocalRowContent(const LocalRowContent&) = delete;

            //! Move constructor.
            LocalRowContent(LocalRowContent&&) = default;

            //! Assignment operator.
            LocalRowContent& operator=(const LocalRowContent&) = delete;

            //! Move assignment operator..
            LocalRowContent& operator=(LocalRowContent&&) = delete;

            ///@}

            //! Add a new element in the line.
            void AddNewElement(Eigen::Index position, PetscScalar value);

            //! Get the position list.
            [[nodiscard]] const std::vector<Eigen::Index>& GetPositionList() const noexcept;

            //! Get the values list.
            [[nodiscard]] const std::vector<PetscScalar>& GetValueList() const noexcept;

            /*!
             * \brief Yields the position and values of non zero values in the global matrix for current row.
             *
             * Indexes are sort increasingly.
             *
             * \param[out] global_position_list Position in the global matrix.
             * \param[out] values_list Values matching each position of \a global_position_list
             */
            void ComputeRowPattern(const std::vector<PetscInt>& source_local_2_global,
                                   std::vector<PetscInt>& global_position_list,
                                   std::vector<PetscScalar>& global_value_list) const;


          private:
            //! Position of the non zero values.
            std::vector<Eigen::Index> position_list_;

            //! Non zero values; position of each is given by \a position_list_.
            std::vector<PetscScalar> value_list_;
        };


    } // namespace


    void ComputePatternFromRefGeomElt(
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
        const LocalFEltSpace::per_geom_elt_index& source_local_felt_space_list,
        const Eigen::MatrixXd& local_projection_matrix,
        const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data,
        std::map<::MoReFEM::DofNS::processor_wise_or_ghost_index, std::vector<PetscInt>>& map_pattern,
        std::unordered_map<::MoReFEM::DofNS::processor_wise_or_ghost_index, std::vector<PetscScalar>>& map_values)
    {
        const auto& source_data = interpolation_data.GetSourceData();
        const auto& target_data = interpolation_data.GetTargetData();

        const auto& source_unknown_storage = source_data.GetExtendedUnknownList();
        const auto& target_unknown_storage = target_data.GetExtendedUnknownList();

        const auto interpreted_local_projection_matrix = LocalMatrixContent(local_projection_matrix);

        const auto Nlocal_row = local_projection_matrix.rows();
        assert(static_cast<Eigen::Index>(interpreted_local_projection_matrix.size()) == Nlocal_row);

        const auto& target_felt_space = target_data.GetFEltSpace();

        const auto& target_local_felt_space_list =
            target_felt_space.GetLocalFEltSpaceList(ref_felt_space.GetRefGeomElt());

        const auto god_of_dof_ptr = target_felt_space.GetGodOfDofFromWeakPtr();
        const auto Nglobal_row = ::MoReFEM::DofNS::processor_wise_or_ghost_index{ god_of_dof_ptr->NprocessorWiseDof(
            target_data.GetNumberingSubset()) };

        const auto end_source_local_felt_space_list = source_local_felt_space_list.cend();

        for (const auto& target_local_felt_space_pair : target_local_felt_space_list)
        {
            const auto& target_local_felt_space_ptr = target_local_felt_space_pair.second;
            assert(!(!target_local_felt_space_ptr));
            const auto& target_local_felt_space = *target_local_felt_space_ptr;

            const auto& geometric_elt = target_local_felt_space.GetGeometricElt();

            // Look if local finite element space is present in source local felt space.
            const auto it_source_local_felt_space_list = source_local_felt_space_list.find(geometric_elt.GetIndex());

            // If none found skip it: these rows will be filled with zeros.
            if (it_source_local_felt_space_list == end_source_local_felt_space_list)
                continue;

            const auto& source_local_felt_space_ptr = it_source_local_felt_space_list->second;
            assert(!(!source_local_felt_space_ptr));
            const auto& source_local_felt_space = *source_local_felt_space_ptr;
            assert(source_local_felt_space.GetGeometricElt() == geometric_elt);

            const auto& source_local2global =
                source_local_felt_space.template GetLocal2Global<MpiScale::program_wise>(source_unknown_storage);

            const auto& target_local2global =
                target_local_felt_space.template GetLocal2Global<MpiScale::processor_wise>(target_unknown_storage);

            assert(static_cast<Eigen::Index>(source_local2global.size()) == local_projection_matrix.cols());
            assert(static_cast<Eigen::Index>(target_local2global.size()) == Nlocal_row);

            std::vector<PetscInt> global_position_list;
            std::vector<PetscScalar> global_value_list;

            for (auto row = Eigen::Index{}; row < Nlocal_row; ++row)
            {
                const auto& local_row_content = interpreted_local_projection_matrix[static_cast<std::size_t>(row)];
                const auto global_row_index = ::MoReFEM::DofNS::processor_wise_or_ghost_index{ static_cast<std::size_t>(
                    target_local2global[static_cast<std::size_t>(row)]) };

                // Skip the ghosts!
                if (global_row_index >= Nglobal_row)
                    continue;

                local_row_content.ComputeRowPattern(source_local2global, global_position_list, global_value_list);

                auto it = map_pattern.find(global_row_index);

                if (it == map_pattern.cend())
                {
                    map_pattern.insert({ global_row_index, global_position_list });
                    map_values.insert({ global_row_index, global_value_list });
                }
#ifndef NDEBUG
                else
                {

                    assert(it->second == global_position_list);
                    auto it_values = map_values.find(global_row_index);
                    assert(it_values != map_values.cend());
                    const auto& stored_value_list = it_values->second;
                    const auto size = stored_value_list.size();

                    for (auto i = 0UL; i < size; ++i)
                        NumericNS::AreEqual(stored_value_list[i], global_value_list[i]);
                }
#endif // NDEBUG
            }
        }
    }


    namespace // anonymous
    {


        template<class LocalMatrixT>
        std::vector<LocalRowContent> LocalMatrixContent(const LocalMatrixT& matrix)
        {
            const auto Nrow = matrix.rows();
            const auto Ncol = matrix.cols();

            assert(!matrix.isZero());

            std::vector<LocalRowContent> ret;
            ret.reserve(static_cast<std::size_t>(Nrow));

            for (auto row = Eigen::Index{}; row < Nrow; ++row)
            {
                LocalRowContent line_content;

                for (auto col = Eigen::Index{}; col < Ncol; ++col)
                {
                    const auto value = matrix(row, col);

                    if (!NumericNS::IsZero(value))
                    {
                        line_content.AddNewElement(col, static_cast<PetscScalar>(value));
                    }
                }

                ret.emplace_back(std::move(line_content));
            }

            assert(ret.size() == static_cast<std::size_t>(Nrow));
            return ret;
        }

        // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
        void LocalRowContent::AddNewElement(Eigen::Index position, PetscScalar value)
        {
            position_list_.push_back(position);
            value_list_.push_back(value);
        }


        const std::vector<Eigen::Index>& LocalRowContent::GetPositionList() const noexcept
        {
            assert(position_list_.size() == value_list_.size());
            return position_list_;
        }


        const std::vector<PetscScalar>& LocalRowContent::GetValueList() const noexcept
        {
            assert(position_list_.size() == value_list_.size());
            return value_list_;
        }


        void LocalRowContent::ComputeRowPattern(const std::vector<PetscInt>& source_local_2_global,
                                                std::vector<PetscInt>& global_position_list,
                                                std::vector<PetscScalar>& global_value_list) const
        {
            const auto& local_col_list = GetPositionList();
            const auto& local_value_list = GetValueList();

            std::vector<PetscInt> unsort_global_position_list(local_col_list.size());

            std::ranges::transform(local_col_list,

                                   unsort_global_position_list.begin(),
                                   [&source_local_2_global](std::size_t local_col)
                                   {
                                       assert(local_col < source_local_2_global.size());
                                       return source_local_2_global[local_col];
                                   });

            // Now sort the indexes by increasing order; same is done for values so that values are correctly
            // matched to their position.
            global_position_list = unsort_global_position_list;
            std::ranges::sort(global_position_list);


            const auto Nelt = local_value_list.size();
            assert(global_position_list.size() == Nelt);
            global_value_list.resize(Nelt);

            const auto begin = unsort_global_position_list.cbegin();
            const auto end = unsort_global_position_list.cend();


            for (auto i = 0UL; i < Nelt; ++i)
            {
                const auto row_index = global_position_list[i];

                // Find the position of the index in the list BEFORE sorting occurred.
                auto it = std::find(begin, end, row_index);
                assert(it != end);

                // Deduce the index of it.
                const auto pos = static_cast<std::size_t>(it - begin);

                // Position the correct value accordingly.
                global_value_list[pos] = local_value_list[i];
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
