// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"

#include "Utilities/Miscellaneous.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"
#include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hpp"


namespace MoReFEM::ConformInterpolatorNS::LagrangianNS
{


    LocalLagrangianInterpolator::LocalLagrangianInterpolator(
        const RefGeomElt& ref_geom_elt,
        const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
    : ref_geom_elt_(ref_geom_elt), interpolation_data_(interpolation_data)
    { }

    LocalLagrangianInterpolator::~LocalLagrangianInterpolator() = default;


    void LocalLagrangianInterpolator::FillMatrixFromNodeBlock(Eigen::MatrixXd&& node_block)
    {
        const auto& interpolation_data = GetInterpolationData();

        decltype(auto) source_data = interpolation_data.GetSourceData();

        const auto& source_unknown_list = source_data.GetExtendedUnknownList();

#ifndef NDEBUG
        decltype(auto) target_data = interpolation_data.GetTargetData();
        const auto& target_unknown_list = target_data.GetExtendedUnknownList();

        assert(std::ranges::none_of(source_unknown_list,

                                    Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));

        assert(std::ranges::none_of(target_unknown_list,

                                    Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
#endif // NDEBUG


        const auto Nunknown = source_unknown_list.size();
        assert(Nunknown == target_unknown_list.size());

        auto shift_row = Eigen::Index{};
        auto shift_col = Eigen::Index{};

        const auto dimension = source_data.GetFEltSpace().GetMeshDimension();

        const auto Nnode_in_row = node_block.rows();
        const auto Nnode_in_col = node_block.cols();

        auto& local_projection_matrix = GetNonCstProjectionMatrix();

        assert(local_projection_matrix.rows() % Nnode_in_row == 0);
        assert(local_projection_matrix.cols() % Nnode_in_col == 0);

        for (auto unknown_index = 0UL; unknown_index < Nunknown; ++unknown_index)
        {
            const auto& source_unknown_ptr = source_unknown_list[unknown_index];

            const auto Ncomponent = static_cast<int>(MoReFEM::Ncomponent(source_unknown_ptr->GetUnknown(), dimension));

#ifndef NDEBUG
            {
                const auto& target_unknown_ptr = target_unknown_list[unknown_index];
                assert(Ncomponent
                       == static_cast<int>(MoReFEM::Ncomponent(target_unknown_ptr->GetUnknown(), dimension)));
            }
#endif // NDEBUG

            for (auto component = Eigen::Index{}; component < Ncomponent; ++component)
            {
                for (auto row = Eigen::Index{}; row < Nnode_in_row; ++row)
                {
                    for (auto col = Eigen::Index{}; col < Nnode_in_col; ++col)
                        local_projection_matrix(row + shift_row, col + shift_col) = node_block(row, col);
                }

                shift_row += Nnode_in_row;
                shift_col += Nnode_in_col;
            }
        }
    }


    auto LocalLagrangianInterpolator::GetRefLocalFEltSpace(const FEltSpace& felt_space, const RefGeomElt& ref_geom_elt)
        -> const Internal::RefFEltNS::RefLocalFEltSpace&
    {
        return felt_space.GetRefLocalFEltSpace(ref_geom_elt);
    }


    const Advanced::ConformInterpolatorNS::InterpolationData&
    LocalLagrangianInterpolator::GetInterpolationData() const noexcept
    {
        return interpolation_data_;
    }


} // namespace MoReFEM::ConformInterpolatorNS::LagrangianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
