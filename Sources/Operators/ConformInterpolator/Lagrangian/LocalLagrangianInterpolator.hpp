// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_LAGRANGIAN_LOCALLAGRANGIANINTERPOLATOR_DOT_HPP_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_LAGRANGIAN_LOCALLAGRANGIANINTERPOLATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ConformInterpolatorNS::LagrangianNS
{


    /*!
     * \brief Performs the elementary part of the computation of a \a LagrangianInterpolator.
     *
     * \a LocalLagrangianInterpolator are given as second template arguments of \a LagrangianInterpolator
     */
    class LocalLagrangianInterpolator
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LocalLagrangianInterpolator;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \class doxygen_hide_local_lagrangian_interpolator_constructor
         *
         * \brief Constructor.
         *
         * \param[in] ref_geom_elt Reference geometric element being considered.
         * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
         *
         */

        //! \copydoc doxygen_hide_local_lagrangian_interpolator_constructor
        explicit LocalLagrangianInterpolator(
            const RefGeomElt& ref_geom_elt,
            const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);


        //! Destructor.
        virtual ~LocalLagrangianInterpolator();

        //! \copydoc doxygen_hide_copy_constructor
        LocalLagrangianInterpolator(const LocalLagrangianInterpolator& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LocalLagrangianInterpolator(LocalLagrangianInterpolator&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LocalLagrangianInterpolator& operator=(const LocalLagrangianInterpolator& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LocalLagrangianInterpolator& operator=(LocalLagrangianInterpolator&& rhs) = delete;

        ///@}

        //! Local projection matrix.
        const Eigen::MatrixXd& GetProjectionMatrix() const noexcept;

        //! Accessor to the RefGeomElt handled by current local operator.
        const RefGeomElt& GetRefGeomElt() const noexcept;


      protected:
        //! Accessor to the local projection matrix.
        Eigen::MatrixXd& GetNonCstProjectionMatrix() noexcept;

        /*!
         * \brief Get the \a RefLocalFEltSpace matching the given arguments.
         *
         * \param[in] felt_space \a FEltSpace considered.
         * \param[in] ref_geom_elt \a RefGeomElt considered.
         *
         * \return \a RefLocalFEltSpace matching both arguments.
         */
        static const Internal::RefFEltNS::RefLocalFEltSpace& GetRefLocalFEltSpace(const FEltSpace& felt_space,
                                                                                  const RefGeomElt& ref_geom_elt);

        //! Accessor to the object holding relevant interpolation data.
        const Advanced::ConformInterpolatorNS::InterpolationData& GetInterpolationData() const noexcept;


        /*!
         * \brief Once the content for the matrix (Nnode_in_row, Nnode_in_col) has been computed, copy it
         * as a block for all relevant components.
         *
         * \param[in] node_block Matrix which content should be reported as a block in larger matrix.
         */
        void FillMatrixFromNodeBlock(Eigen::MatrixXd&& node_block);


      private:
        //! RefGeomElt handled by current local operator.
        const RefGeomElt& ref_geom_elt_;

        //! Convenient object which stores data relevant for interpolation.
        const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data_;

        //! Local matrix.
        Eigen::MatrixXd local_projection_matrix_;
    };


} // namespace MoReFEM::ConformInterpolatorNS::LagrangianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_LAGRANGIAN_LOCALLAGRANGIANINTERPOLATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
