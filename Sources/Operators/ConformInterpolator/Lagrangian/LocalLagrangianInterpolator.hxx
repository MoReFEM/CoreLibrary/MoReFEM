// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_LAGRANGIAN_LOCALLAGRANGIANINTERPOLATOR_DOT_HXX_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_LAGRANGIAN_LOCALLAGRANGIANINTERPOLATOR_DOT_HXX_
// IWYU pragma: private, include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ConformInterpolatorNS::LagrangianNS
{


    inline auto LocalLagrangianInterpolator::GetProjectionMatrix() const noexcept -> const Eigen::MatrixXd&
    {
        return local_projection_matrix_;
    }


    inline auto LocalLagrangianInterpolator::GetNonCstProjectionMatrix() noexcept -> Eigen::MatrixXd&
    {
        return const_cast<Eigen::MatrixXd&>(GetProjectionMatrix());
    }


    inline const RefGeomElt& LocalLagrangianInterpolator::GetRefGeomElt() const noexcept
    {
        return ref_geom_elt_;
    }


} // namespace MoReFEM::ConformInterpolatorNS::LagrangianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_LAGRANGIAN_LOCALLAGRANGIANINTERPOLATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
