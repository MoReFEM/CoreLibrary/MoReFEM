// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_DETERMINEEXTENDEDUNKNOWNLIST_DOT_HPP_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_DETERMINEEXTENDEDUNKNOWNLIST_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <array>
#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::GlobalVariationalOperatorNS
{


    /*!
     * \class doxygen_hide_determine_extended_unknown_list_param_and_return
     *
     * \brief Determine the proper \a ExtendedUnknown for the operator given the \a Unknown and the
     * \a FEltSpace involved.
     *
     * \param[in] felt_space \a FEltSpace used to determine the \a ExtendedUnknown of choice to match the
     * given \a Unknown. All these \a Unknown must be present in the \a FEltSpace; if not an assert is raised
     * in debug mode.
     *
     * \return The list of relevant \a ExtendedUnknown.
     */


    /*!
     * \brief Version when multiple \a Unknown is dealt with in the \a GlobalVariationalOperator.
     *
     * \copydoc doxygen_hide_determine_extended_unknown_list_param_and_return
     * \param[in] unknown_list The unknowns we want to consider.
     *
     */
    template<std::size_t NunknownsT>
    ExtendedUnknown::vector_const_shared_ptr
    DetermineExtendedUnknownList(const FEltSpace& felt_space,
                                 const std::array<Unknown::const_shared_ptr, NunknownsT>& unknown_list);

    /*!
     * \brief Version when only one \a Unknown is dealt with in the \a GlobalVariationalOperator.
     *
     * \copydoc doxygen_hide_determine_extended_unknown_list_param_and_return
     * \param[in] unknown_ptr The sole unknown we want to consider.
     *
     * Needed because a call DetermineExtendedUnknownList(felt_space, {{unknown_ptr}}) does not compile because
     * the argument is not recognized as an array of size 1.
     */
    ExtendedUnknown::vector_const_shared_ptr DetermineExtendedUnknownList(const FEltSpace& felt_space,
                                                                          const Unknown::const_shared_ptr& unknown_ptr);


} // namespace MoReFEM::Advanced::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/GlobalVariationalOperator/Advanced/DetermineExtendedUnknownList.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_DETERMINEEXTENDEDUNKNOWNLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
