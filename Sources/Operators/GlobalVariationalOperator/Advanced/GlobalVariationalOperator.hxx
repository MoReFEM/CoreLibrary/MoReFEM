// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_GLOBALVARIATIONALOPERATOR_DOT_HXX_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_GLOBALVARIATIONALOPERATOR_DOT_HXX_
// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Advanced/GlobalVariationalOperator.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"

#include "Operators/GlobalVariationalOperator/Internal/ExtractLocalOperatorHelper.hpp"


namespace MoReFEM::Advanced::GlobalVariationalOperatorNS
{


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<typename... Args>
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GlobalVariationalOperator(
        const FEltSpace& felt_space,
        const ExtendedUnknown::vector_const_shared_ptr unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr test_unknown_storage,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
        DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global,
        Args&&... args)
    : extended_unknown_and_test_unknown_list_parent(unknown_storage, test_unknown_storage), felt_space_(felt_space),
      do_allocate_gradient_felt_phi_(do_allocate_gradient_felt_phi),
      quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {

        {
            decltype(auto) extended_unknown_list_per_numbering_subset =
                extended_unknown_and_test_unknown_list_parent::GetExtendedUnknownListPerNumberingSubset();

            for (const auto& pair : extended_unknown_list_per_numbering_subset)
            {
                felt_space.ComputeLocal2Global(pair.second, do_compute_processor_wise_local_2_global);
            }
        }

        {
            decltype(auto) test_extended_unknown_list_per_numbering_subset =
                extended_unknown_and_test_unknown_list_parent::GetExtendedTestUnknownListPerNumberingSubset();

            for (const auto& pair : test_extended_unknown_list_per_numbering_subset)
            {
                felt_space.ComputeLocal2Global(pair.second, do_compute_processor_wise_local_2_global);
            }
        }

        CreateLocalOperatorList(felt_space.GetMeshDimension(), std::forward<decltype(args)>(args)...);
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<std::size_t NunknownsT, std::size_t NtestUnknownsT, typename... Args>
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GlobalVariationalOperator(
        const FEltSpace& felt_space,
        const std::array<Unknown::const_shared_ptr, NunknownsT> unknown_list,
        const std::array<Unknown::const_shared_ptr, NtestUnknownsT> test_unknown_list,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
        DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global,
        Args&&... args)
    : GlobalVariationalOperator(
          felt_space,
          Advanced::GlobalVariationalOperatorNS::DetermineExtendedUnknownList(felt_space, unknown_list),
          Advanced::GlobalVariationalOperatorNS::DetermineExtendedUnknownList(felt_space, test_unknown_list),
          quadrature_rule_per_topology,
          do_allocate_gradient_felt_phi,
          do_compute_processor_wise_local_2_global,
          std::forward<Args>(args)...)
    {
        assert(this->GetNumberingSubsetList().size() == 1 && "The unknowns must be in the same NumberingSubset.");
        assert(this->GetTestNumberingSubsetList().size() == 1
               && "The test unknowns must be in the same NumberingSubset.");
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<typename... Args>
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GlobalVariationalOperator(
        const FEltSpace& felt_space,
        const Unknown::const_shared_ptr& unknown_ptr,
        const Unknown::const_shared_ptr& test_unknown_ptr,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
        DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global,
        Args&&... args)
    : GlobalVariationalOperator(
          felt_space,
          Advanced::GlobalVariationalOperatorNS::DetermineExtendedUnknownList(felt_space, unknown_ptr),
          Advanced::GlobalVariationalOperatorNS::DetermineExtendedUnknownList(felt_space, test_unknown_ptr),
          quadrature_rule_per_topology,
          do_allocate_gradient_felt_phi,
          do_compute_processor_wise_local_2_global,
          std::forward<Args>(args)...)
    {
        assert(this->GetNumberingSubsetList().size() == 1 && "The unknown must be in the same NumberingSubset.");
        assert(this->GetTestNumberingSubsetList().size() == 1
               && "The test unknown must be in the same NumberingSubset.");
    }

    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    constexpr Advanced::OperatorNS::Nature
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GetOperatorNature()
    {
        return NatureT;
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>

    inline const FEltSpace& GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GetFEltSpace() const noexcept
    {
        return felt_space_;
    }


    template<class... Us>
    struct is_valid_assemble_tuple<std::tuple<Us...>> : std::conjunction<is_valid_assemble_tuple_arg<Us>...>
    { };


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<class LinearAlgebraTupleT>
    auto GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::AnalyzeTuple() const
        -> std::pair<::MoReFEM::Internal::assemble_into_matrix, ::MoReFEM::Internal::assemble_into_vector>
    {
        static_assert(is_valid_assemble_tuple<LinearAlgebraTupleT>(),
                      "Tuple must be a mix of GlobalMatrixWithCoefficient& and GlobalVectorWithCoefficient& "
                      "arguments.");

        constexpr bool do_assemble_into_matrix =
            Utilities::Tuple::HasType<GlobalMatrixWithCoefficient&, std::is_same, LinearAlgebraTupleT>();
        constexpr bool do_assemble_into_vector =
            Utilities::Tuple::HasType<GlobalVectorWithCoefficient&, std::is_same, LinearAlgebraTupleT>();

        if constexpr (NatureT == Advanced::OperatorNS::Nature::linear)
            static_assert(!do_assemble_into_matrix);

        if constexpr (NatureT == Advanced::OperatorNS::Nature::bilinear)
            static_assert(!do_assemble_into_vector);

        static_assert(do_assemble_into_matrix || do_assemble_into_vector,
                      "Assemble() must specify either a matrix or a vector into which assembling must occur. "
                      "The tuple argument should be something like std::make_tuple(std::ref(M1), std::ref(V1)) "
                      "where M1 is a GlobalMatrixWithCoefficient and V1 a GlobalVectorWithCoefficient.");

        using ::MoReFEM::Internal::assemble_into_matrix;
        using ::MoReFEM::Internal::assemble_into_vector;

        return std::make_pair(do_assemble_into_matrix ? assemble_into_matrix::yes : assemble_into_matrix::no,
                              do_assemble_into_vector ? assemble_into_vector::yes : assemble_into_vector::no);
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<class LinearAlgebraTupleT, typename... Args, elementary_mode ModeT>
    void
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::AssembleImpl(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                                            const Domain& domain,
                                                                            Args&&... args) const
    {
        const auto& args_as_tuple = std::forward_as_tuple(args...);

        auto [do_assemble_into_matrix, do_assemble_into_vector] = AnalyzeTuple<LinearAlgebraTupleT>();

        // clang-format off
        Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator
        <
            local_operator_storage_type,
            0UL,
            std::tuple_size<local_operator_storage_type>::value,
            NatureT
        >::template Assemble<ModeT>(static_cast<const DerivedT&>(*this),
                                    local_operator_per_ref_geom_elt_,
                                    linear_algebra_tuple,
                                    domain,
                                    do_assemble_into_matrix,
                                    do_assemble_into_vector,
                                    args_as_tuple);
        // clang-format on
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    bool GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::DoConsider(
        Advanced::GeometricEltEnum ref_geom_elt_id) const
    {
        const auto num = EnumUnderlyingType(ref_geom_elt_id);
        assert(num < local_operator_per_ref_geom_elt_.size());

        return local_operator_per_ref_geom_elt_[num] != nullptr;
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<elementary_mode ModeT, class LocalVariationalOperatorT, class AdditionalArgTupleT>
    void GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::PerformElementaryCalculation(
        const LocalFEltSpace& local_felt_space,
        LocalVariationalOperatorT& local_operator,
        const AdditionalArgTupleT& add_arg_as_tuple) const
    {
        constexpr auto Nadditional_arg = std::tuple_size<AdditionalArgTupleT>::value;

        if constexpr (Nadditional_arg > 0)
        {
            static_cast<const DerivedT&>(*this).SetComputeEltArrayArguments(
                local_felt_space, local_operator, add_arg_as_tuple);
        }

        switch (ModeT)
        {
        case elementary_mode::full:
            local_operator.InitLocalComputation();
            local_operator.ComputeEltArray();
            local_operator.FinalizeLocalComputation();
            break;
        case elementary_mode::just_init:
            local_operator.InitLocalComputation();
            break;
        }
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<typename... Args>
    inline void GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::CreateLocalOperatorList(
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        Args&&... args)
    {
        Internal::GlobalVariationalOperatorNS ::LocalVariationalOperatorIterator<
            local_operator_storage_type,
            0UL,
            std::tuple_size<local_operator_storage_type>::value,
            NatureT>::FillLocalVariationalOperatorList(mesh_dimension,
                                                       static_cast<DerivedT&>(*this),
                                                       local_operator_per_ref_geom_elt_,
                                                       std::forward<decltype(args)>(args)...);
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    inline AllocateGradientFEltPhi
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::DoAllocateGradientFEltPhi() const noexcept
    {
        return do_allocate_gradient_felt_phi_;
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    inline const QuadratureRule&
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GetQuadratureRule(const RefGeomElt& ref_geom_elt) const
    {
        return GetQuadratureRulePerTopology().GetRule(ref_geom_elt.GetTopologyIdentifier());
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    const QuadratureRulePerTopology&
    GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GetQuadratureRulePerTopology() const noexcept
    {
        return (quadrature_rule_per_topology_ == nullptr ? GetFEltSpace().GetQuadratureRulePerTopology()
                                                         : *quadrature_rule_per_topology_);
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    auto GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::GetLocalOperatorPerRefGeomElt() const noexcept
        -> const local_operator_storage_type&
    {
        return local_operator_per_ref_geom_elt_;
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<Advanced::GeometricEltEnum GeomEltEnumT>
    auto GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::ExtractRawLocalOperator()
        -> local_operator_type_for_geom_elt_enum<GeomEltEnumT>&
    {
        auto& local_operator_geom_elt_enum =
            std::get<EnumUnderlyingType(GeomEltEnumT)>(local_operator_per_ref_geom_elt_);

        assert(local_operator_geom_elt_enum.RefGeomEltType() == GeomEltEnumT
               && "We use in this method the fact here that: \n"
                  " - The LocalOperatorForRefGeomElt are sort in a very specific ordering (checked by a "
                  "static_assert in hpp file) which follows the one used for GeometricEltEnum \n"
                  " - This enum nmbering starts at 0");

        if (!local_operator_geom_elt_enum.IsRelevant())
        {
            std::ostringstream oconv;
            decltype(auto) geom_elt_factory = Advanced::GeometricEltFactory::GetInstance();

            oconv << "You tried to get a local operator associated to "
                  << geom_elt_factory.GetRefGeomElt(GeomEltEnumT).GetName()
                  << " but there are none in the model submitted. Please check your input data are adequate for your "
                     "model.";

            throw Exception(oconv.str());
        }

        auto& local_operator = local_operator_geom_elt_enum.GetNonCstLocalOperator();

        return local_operator;
    }


    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, ::MoReFEM::Concept::Tuple LocalTupleT>
    template<Advanced::GeometricEltEnum GeomEltEnumT, typename... Args>
    auto GlobalVariationalOperator<DerivedT, NatureT, LocalTupleT>::ExtractReadiedLocalOperator(const Domain& domain,
                                                                                                Args&&... args)
        -> local_operator_type_for_geom_elt_enum<GeomEltEnumT>&
    {
        auto& local_operator = ExtractRawLocalOperator<GeomEltEnumT>();

        constexpr auto assemble_into_matrix =
            (NatureT != Advanced::OperatorNS::Nature::linear ? Internal::assemble_into_matrix::yes
                                                             : Internal::assemble_into_matrix::no);
        constexpr auto assemble_into_vector =
            (NatureT != Advanced::OperatorNS::Nature::bilinear ? Internal::assemble_into_vector::yes
                                                               : Internal::assemble_into_vector::no);

        local_operator.SetAssembleTarget(assemble_into_matrix, assemble_into_vector);

        decltype(auto) felt_space = GetFEltSpace();

        decltype(auto) felt_storage =
            felt_space.template GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>(domain);

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();

        decltype(auto) first_geom_elt =
            Internal::GlobalVariationalOperatorNS::FindFirstGeometricElt<GeomEltEnumT>(god_of_dof_ptr->GetMesh());

        decltype(auto) local_felt_space =
            Internal::GlobalVariationalOperatorNS::IdentifyLocalFeltSpace(felt_storage, first_geom_elt);

        const auto& args_as_tuple = std::forward_as_tuple(args...);

        local_operator.SetLocalFEltSpace(local_felt_space);

        if constexpr (sizeof...(Args) > 0)
        {
            static_cast<const DerivedT&>(*this).SetComputeEltArrayArguments(
                local_felt_space, local_operator, args_as_tuple);
        }

        return local_operator;
    }


} // namespace MoReFEM::Advanced::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_GLOBALVARIATIONALOPERATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
