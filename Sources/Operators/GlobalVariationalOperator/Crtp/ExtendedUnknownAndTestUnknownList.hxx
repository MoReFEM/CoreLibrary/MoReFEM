// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_CRTP_EXTENDEDUNKNOWNANDTESTUNKNOWNLIST_DOT_HXX_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_CRTP_EXTENDEDUNKNOWNANDTESTUNKNOWNLIST_DOT_HXX_
// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{


    template<class DerivedT>
    ExtendedUnknownAndTestUnknownList<DerivedT>::ExtendedUnknownAndTestUnknownList(
        const ExtendedUnknown::vector_const_shared_ptr operator_extended_unknown_list,
        const ExtendedUnknown::vector_const_shared_ptr operator_test_extended_unknown_list)
    : extended_unknown_list_(operator_extended_unknown_list),
      test_extended_unknown_list_(operator_test_extended_unknown_list)
    {
        {
            const auto& list = GetExtendedUnknownList();
#ifndef NDEBUG
            assert(!list.empty());
            assert(std::none_of(list.cbegin(), list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
#endif // NDEBUG

            for (const auto& ptr : list)
                numbering_subset_list_.push_back(ptr->GetNumberingSubsetPtr());

            Utilities::EliminateDuplicate(numbering_subset_list_,
                                          Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                          Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

            for (const auto& numbering_subset_ptr : numbering_subset_list_)
            {
                const auto& numbering_subset = *numbering_subset_ptr;

                ExtendedUnknown::vector_const_shared_ptr unknown_per_numbering_subset;

                std::copy_if(list.cbegin(),
                             list.cend(),
                             std::back_inserter(unknown_per_numbering_subset),
                             [&numbering_subset](const auto& extended_unknown_ptr)
                             {
                                 assert(!(!extended_unknown_ptr));
                                 return (extended_unknown_ptr->GetNumberingSubset() == numbering_subset);
                             });

                auto&& pair = std::make_pair(numbering_subset.GetUniqueId(), std::move(unknown_per_numbering_subset));

                [[maybe_unused]] const auto check = extended_unknown_list_per_numbering_subset_.insert(std::move(pair));
                assert(check.second && "A numbering subset should be present only once!");
            }
        }

        {
            const auto& list = GetExtendedTestUnknownList();

#ifndef NDEBUG
            assert(!list.empty());
            assert(std::none_of(list.cbegin(), list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
#endif // NDEBUG

            for (const auto& ptr : list)
                test_numbering_subset_list_.push_back(ptr->GetNumberingSubsetPtr());

            Utilities::EliminateDuplicate(test_numbering_subset_list_,
                                          Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                          Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

            for (const auto& numbering_subset_ptr : test_numbering_subset_list_)
            {
                const auto& numbering_subset = *numbering_subset_ptr;

                ExtendedUnknown::vector_const_shared_ptr test_unknown_per_numbering_subset;

                std::copy_if(list.cbegin(),
                             list.cend(),
                             std::back_inserter(test_unknown_per_numbering_subset),
                             [&numbering_subset](const auto& extended_test_unknown_ptr)
                             {
                                 assert(!(!extended_test_unknown_ptr));
                                 return (extended_test_unknown_ptr->GetNumberingSubset() == numbering_subset);
                             });

                auto&& pair =
                    std::make_pair(numbering_subset.GetUniqueId(), std::move(test_unknown_per_numbering_subset));

                [[maybe_unused]] const auto check =
                    test_extended_unknown_list_per_numbering_subset_.insert(std::move(pair));
                assert(check.second && "A numbering subset should be present only once!");
            }
        }
    }


    template<class DerivedT>
    inline const ExtendedUnknown::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownList() const
    {
        return extended_unknown_list_;
    }


    template<class DerivedT>
    inline const NumberingSubset::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetNumberingSubsetList() const
    {
        assert(!numbering_subset_list_.empty());
        return numbering_subset_list_;
    }


    template<class DerivedT>
    const ExtendedUnknown& ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthUnknown(std::size_t index) const
    {
        const auto& list = GetExtendedUnknownList();
        assert(index < list.size());
        assert(!(!list[index]));
        return *list[index];
    }


    template<class DerivedT>
    const ExtendedUnknown::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownList(const NumberingSubset& numbering_subset) const
    {
        auto it = extended_unknown_list_per_numbering_subset_.find(numbering_subset.GetUniqueId());
        assert(it != extended_unknown_list_per_numbering_subset_.cend()
               && "It's likely you tried to assemble into a linear algebra object not defined in the expected "
                  "numbering subset.");
        return it->second;
    }


    template<class DerivedT>
    inline const std::map<::MoReFEM::NumberingSubsetNS::unique_id, ExtendedUnknown::vector_const_shared_ptr>&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownListPerNumberingSubset() const noexcept
    {
        return extended_unknown_list_per_numbering_subset_;
    }


    template<class DerivedT>
    inline const ExtendedUnknown::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownList() const
    {
        return test_extended_unknown_list_;
    }


    template<class DerivedT>
    inline const NumberingSubset::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetTestNumberingSubsetList() const
    {
        assert(!test_numbering_subset_list_.empty());
        return test_numbering_subset_list_;
    }


    template<class DerivedT>
    const ExtendedUnknown& ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthTestUnknown(std::size_t index) const
    {
        const auto& list = GetExtendedTestUnknownList();
        assert(index < list.size());
        assert(!(!list[index]));
        return *list[index];
    }


    template<class DerivedT>
    const ExtendedUnknown::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownList(
        const NumberingSubset& numbering_subset) const
    {
        auto it = test_extended_unknown_list_per_numbering_subset_.find(numbering_subset.GetUniqueId());
        assert(it != test_extended_unknown_list_per_numbering_subset_.cend()
               && "It's likely you tried to assemble into a linear algebra object not defined in the expected "
                  "numbering subset.");
        return it->second;
    }


    template<class DerivedT>
    inline const std::map<::MoReFEM::NumberingSubsetNS::unique_id, ExtendedUnknown::vector_const_shared_ptr>&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownListPerNumberingSubset() const noexcept
    {
        return test_extended_unknown_list_per_numbering_subset_;
    }


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_CRTP_EXTENDEDUNKNOWNANDTESTUNKNOWNLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
