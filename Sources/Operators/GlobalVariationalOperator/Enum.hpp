// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ENUM_DOT_HPP_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::GlobalVariationalOperatorNS
{

    //! Convenient alias to perform only part of the elementary computation.
    enum class elementary_mode { full = 0, just_init = 1 };


    /*!
     * \class doxygen_hide_gvo_elementary_mode_tparam
     *
     * \tparam ModeT What is actually done in an assemble call:
     * - If 'full', normal assembling.
     * - If 'just_init', local_operator.InitLocalComputation() is called and there is no actual assembling operation
     * toward the linear algebra.
     */


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
