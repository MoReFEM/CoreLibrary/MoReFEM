// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{

    void ExtractLocalDofValues(const LocalFEltSpace& local_felt_space,
                               const ExtendedUnknown& extended_unknown,
                               const GlobalVector& vector,
                               Eigen::VectorXd& result)
    {
        assert(extended_unknown.GetNumberingSubset() == vector.GetNumberingSubset());

        const auto& local_2_global = local_felt_space.GetLocal2Global<MpiScale::processor_wise>(extended_unknown);

        assert(local_2_global.size() == static_cast<std::size_t>(result.size()));

        const Wrappers::Petsc::AccessGhostContent ghost_vector(vector);

        const auto& vector_with_ghost = ghost_vector.GetVectorWithGhost();

        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> ghost_vector_content(
            vector_with_ghost);

        auto counter = Eigen::Index{};

        for (auto global_index : local_2_global)
            result(counter++) =
                ghost_vector_content.GetValue(vector_processor_wise_index_type{ static_cast<PetscInt>(global_index) });
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
