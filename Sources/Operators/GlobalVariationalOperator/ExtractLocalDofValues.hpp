// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_EXTRACTLOCALDOFVALUES_DOT_HPP_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_EXTRACTLOCALDOFVALUES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalFEltSpace; }
namespace MoReFEM { class ExtendedUnknown; }
namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief Extract from a global vector the values required at a local level.
     *
     * \param[in] local_felt_space The pendant of GeometricElt for finite element analysis, which includes all
     * finite elements related to the given geometric element.
     * \param[in] unknown Unknown to consider, that must belong to the \a local_felt_space. Processor-wise local
     * to global array must have been computed for this unknown.
     * \param[in] vector Global vector from which we seek to extract relevant local values.
     * \param[out] result Vector into which result of the function is written. This vector is assumed to be properly
     * allocated before the call to this function. This vector follows the ordering convention of local vectors
     * (i.e. dofs are grouped by component: vx1 vx2 vx3 vy1 vy2 vy3...)
     */
    void ExtractLocalDofValues(const LocalFEltSpace& local_felt_space,
                               const ExtendedUnknown& unknown,
                               const GlobalVector& vector,
                               Eigen::VectorXd& result);


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_EXTRACTLOCALDOFVALUES_DOT_HPP_
// *** MoReFEM end header guards *** < //
