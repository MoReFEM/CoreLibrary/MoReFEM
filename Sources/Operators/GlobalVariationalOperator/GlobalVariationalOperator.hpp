// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_GLOBALVARIATIONALOPERATOR_DOT_HPP_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_GLOBALVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Operators/GlobalVariationalOperator/Advanced/GlobalVariationalOperator.hpp"  // IWYU pragma: export
#include "Operators/GlobalVariationalOperator/Advanced/LocalOperatorForRefGeomElt.hpp" // IWYU pragma: export


namespace MoReFEM::GlobalVariationalOperatorNS
{


//! Convenient macro to simplify reading of tuple in \a DependsOnRefGeomElt and \a SameForAllRefGeomElt.
#define MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(ref_geom_elt_enum, local_operator_type)                                   \
    Advanced::GlobalVariationalOperatorNS::LocalOperatorForRefGeomElt<ref_geom_elt_enum,                               \
                                                                      typename local_operator_type::unique_ptr>


//! Convenient macro to simplify reading of tuple in \a SameForAllRefGeomElt.
#define MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(ref_geom_elt_enum)                                                           \
    MOREFEM_GVO_LOCAL_TUPLE_ITEM_DEPENDS(ref_geom_elt_enum, LocalVariationalOperatorT)

//! Convenient macro if tuple for a RefGeomElt should be ignored.
#define MOREFEM_GVO_LOCAL_TUPLE_ITEM_IGNORE(ref_geom_elt_enum)                                                         \
    Advanced::GlobalVariationalOperatorNS::LocalOperatorForRefGeomElt<ref_geom_elt_enum, std::nullptr_t>

    /*!
     * \brief Most common base class for a \a GlobalVariationalOperator.
     *
     * This base class is used when a unique \a LocalVariationalOperator implementation may be used to cover
     * all the \a RefGeomElt cases. This is by far the most common case; currently there are two cases not covered
     * by this scheme:
     * - MITC operator, which requires wildly different implementation for Triangle3, Triangle6, Quadrangle4 and
     * Quadrangle9. Writing all of them in the same \a LocalVariationalOperator would be possible but very tedious
     * and difficult to udnerstand.
     * - Incompressible operators.
     *
     * \tparam DerivedT Name of the actual \a GlobalVariationalOperator implemented (current template class is a
     * curiously recurrent template pattern (CRTP)).
     * \copydoc doxygen_hide_global_operator_nature
     * \tparam LocalVariationalOperatorT Type of the \a LocalVariationalOperator used for all kinds of \a
     * RefGeomElt.
     */
    // clang-format off
        template
        <
            class DerivedT,
            Advanced::OperatorNS::Nature NatureT,
            class LocalVariationalOperatorT
        >
        using SameForAllRefGeomElt =
            Advanced::GlobalVariationalOperatorNS::GlobalVariationalOperator
            <
                DerivedT,
                NatureT,
                std::tuple
                <
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Point1),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Segment2),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Segment3),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Triangle3),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Triangle6),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Quadrangle4),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Quadrangle8),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Quadrangle9),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Tetrahedron4),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Tetrahedron10),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Hexahedron8),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Hexahedron20),
                    MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME(Advanced::GeometricEltEnum::Hexahedron27)
                >
            >;
    // clang-format on

    /*!
     * \typedef DependsOnRefGeomElt
     *
     * \brief Advanced base class for a \a GlobalVariationalOperator, when a specific implementation is required
     * for each \a RefGeomElt.
     *
     * Currently there are two cases to be covered by this scheme:
     * - MITC operator, which requires wildly different implementation for Triangle3, Triangle6, Quadrangle4 and
     * Quadrangle9. Writing all of them in the same \a LocalVariationalOperator would be possible but very tedious
     * and difficult to udnerstand.
     * - Incompressible operators.
     *
     * One might add the case of operators such as GradOnGradientBasedElasticityTensor which provides a single
     * implementation but is irrelevant for some of the \a RefGeomElt such as \a Point1 or \a Segment2. In this
     * case, set the second item of MOREFEM_GVO_LOCAL_TUPLE_ITEM_SAME for this \a RefGeomElt as std::nullptr_t.
     *
     * \tparam DerivedT Name of the actual \a GlobalVariationalOperator implemented (current template class is a
     * curiously recurrent template pattern (CRTP)).
     * \copydoc doxygen_hide_global_operator_nature
     * \copydoc doxygen_hide_global_operator_local_operator_tuple_type
     */
    template<class DerivedT, Advanced::OperatorNS::Nature NatureT, class LocalVariationalOperatorTupleT>
    using DependsOnRefGeomElt = Advanced::GlobalVariationalOperatorNS::
        GlobalVariationalOperator<DerivedT, NatureT, LocalVariationalOperatorTupleT>;


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_GLOBALVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
