// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_INTERNAL_EXTRACTLOCALOPERATORHELPER_DOT_HPP_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_INTERNAL_EXTRACTLOCALOPERATORHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"


namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{


    /*!
     * \brief Identify in \a FEltSpace storage the \a LocalFEltSpace related to a given \a GeometricElt.
     *
     *  This function is an helper for \a GlobalVariationalOperator::ExtractReadiedLocalOperator().
     *
     * \param[in] felt_storage Internal storage for finite elements in \a FEltSpace object.
     * \param[in] geom_elt Specific \a GeometricElt which matching \a LocalFEltSpace is sought. Typically it is just the very first one found
     * in the mesh (see \a FindFirstGeometricElt()).
     *
     * \return \a LocalFEltSpace matching the \a GeometricElt
     */
    template<class FEltStorageT>
    const LocalFEltSpace& IdentifyLocalFeltSpace(const FEltStorageT& felt_storage, const GeometricElt& geom_elt);


    /*!
     * \brief Identify in a \a Mesh the first \a GeometricElt which type is \a EnumT.
     *
     *  This function is an helper for \a GlobalVariationalOperator::ExtractReadiedLocalOperator().
     *
     * \tparam EnumT Type of the \a GeometricElt sought.
     * \param[in] mesh The \a Mesh in which we seek the \a GeometricElt.
     *
     * \return First \a GeometricElt with type \a EnumT.
     */
    template<Advanced::GeometricEltEnum EnumT>
    const GeometricElt& FindFirstGeometricElt(const Mesh& mesh);


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Operators/GlobalVariationalOperator/Internal/ExtractLocalOperatorHelper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_INTERNAL_EXTRACTLOCALOPERATORHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
