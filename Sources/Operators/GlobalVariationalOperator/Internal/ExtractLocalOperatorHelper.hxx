// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_INTERNAL_EXTRACTLOCALOPERATORHELPER_DOT_HXX_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_INTERNAL_EXTRACTLOCALOPERATORHELPER_DOT_HXX_
// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Internal/ExtractLocalOperatorHelper.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{


    template<class FEltStorageT>
    const LocalFEltSpace& IdentifyLocalFeltSpace(const FEltStorageT& felt_storage, const GeometricElt& geom_elt)
    {
        decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

        const auto geometric_elt_index = geom_elt.GetIndex();

        for (const auto& [ref_local_felt_space_ptr, local_felt_space_list] : felt_storage)
        {
            assert(!(!ref_local_felt_space_ptr));
            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

            if (ref_geom_elt != ref_local_felt_space.GetRefGeomElt())
                continue;

            assert(!local_felt_space_list.empty());

            auto it = local_felt_space_list.find(geometric_elt_index);
            assert(it != local_felt_space_list.cend());
            const auto& local_felt_space_ptr = it->second;
            assert(!(!local_felt_space_ptr));

            return *local_felt_space_ptr;
        }

        assert(false
               && "Invalid call: the test in which the function has been called is ill-constructed (the mesh "
                  "might not be correct for instance).");
        exit(EXIT_FAILURE);
    }


    template<Advanced::GeometricEltEnum EnumT>
    const GeometricElt& FindFirstGeometricElt(const Mesh& mesh)
    {
        decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

        auto it = std::find_if(geom_elt_list.cbegin(),
                               geom_elt_list.cend(),
                               [](const auto& geom_elt_ptr)
                               {
                                   assert(!(!geom_elt_ptr));

                                   return geom_elt_ptr->GetIdentifier() == EnumT;
                               });

        assert(it != geom_elt_list.cend());
        const auto& geom_elt_ptr = *it;
        assert(!(!geom_elt_ptr));

        return *geom_elt_ptr;
    }


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_INTERNAL_EXTRACTLOCALOPERATORHELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
