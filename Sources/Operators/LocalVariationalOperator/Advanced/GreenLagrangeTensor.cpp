// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "Operators/LocalVariationalOperator/Advanced/GreenLagrangeTensor.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    GreenLagrangeTensor::GreenLagrangeTensor(::MoReFEM::GeometryNS::dimension_type mesh_dimension)
    : mesh_dimension_(mesh_dimension)
    {
        auto& vector = GetNonCstVector();

        switch (mesh_dimension.Get())
        {
        case 1:
            vector.resize(1);
            break;
        case 2:
            vector.resize(3);
            break;
        case 3:
            vector.resize(6);
            break;
        default:
            assert(false && "Only implemented for dimensions 2 and 3.");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {

        void Update1D(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                      GreenLagrangeTensor::storage_type& result);

        void Update2D(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                      GreenLagrangeTensor::storage_type& result);

        void Update3D(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                      GreenLagrangeTensor::storage_type& result);


    } // namespace


    auto GreenLagrangeTensor::Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value)
        -> const storage_type&
    {
        const auto mesh_dimension = GetMeshDimension();

        auto& ret = GetNonCstVector();

        switch (mesh_dimension.Get())
        {
        case 1:
            Update1D(cauchy_green_tensor_value, ret);
            return ret;
        case 2:
            Update2D(cauchy_green_tensor_value, ret);
            return ret;
        case 3:
            Update3D(cauchy_green_tensor_value, ret);
            return ret;
        default:
            assert(false && "Only implemented for dimensions 2 and 3.");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        void Update1D(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                      GreenLagrangeTensor::storage_type& result)
        {
            assert(result.rows() == 1);
            assert(cauchy_green_tensor_value.size() == 1);
            result(0) = 0.5 * (cauchy_green_tensor_value(0) - 1.);
        }


        void Update2D(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                      GreenLagrangeTensor::storage_type& result)
        {
            assert(result.rows() == 3);
            assert(cauchy_green_tensor_value.size() == 3);
            result(0) = 0.5 * (cauchy_green_tensor_value(0) - 1.);
            result(1) = 0.5 * (cauchy_green_tensor_value(1) - 1.);
            result(2) = cauchy_green_tensor_value(2);
        }


        void Update3D(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value,
                      GreenLagrangeTensor::storage_type& result)
        {
            assert(result.rows() == 6);
            assert(cauchy_green_tensor_value.size() == 6);

            result(0) = 0.5 * (cauchy_green_tensor_value(0) - 1.);
            result(1) = 0.5 * (cauchy_green_tensor_value(1) - 1.);
            result(2) = 0.5 * (cauchy_green_tensor_value(2) - 1.);
            result(3) = cauchy_green_tensor_value(3);
            result(4) = cauchy_green_tensor_value(4);
            result(5) = cauchy_green_tensor_value(5);
        }

    } // namespace


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
