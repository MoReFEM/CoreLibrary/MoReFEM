// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/StrongType.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{

    /*!
     * \brief Class in charge of computing Green-Lagrange tensor.
     */
    class GreenLagrangeTensor final
    {
      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<GreenLagrangeTensor>;

        //! Eigen vector used to store the tensor.
        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        using storage_type = ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>;

      public:
        //! Constructor.
        //! \param[in] mesh_dimension Dimension of the mesh.
        explicit GreenLagrangeTensor(::MoReFEM::GeometryNS::dimension_type mesh_dimension);

        //! Destructor.
        ~GreenLagrangeTensor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GreenLagrangeTensor(const GreenLagrangeTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GreenLagrangeTensor(GreenLagrangeTensor&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        GreenLagrangeTensor& operator=(const GreenLagrangeTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GreenLagrangeTensor& operator=(GreenLagrangeTensor&& rhs) = delete;

        //! Update the values.
        //! \param[in] cauchy_green_tensor_value Value of the Cauchy-Green tensor.
        //! \return The updated GreenLagrangeTensor vector.
        const storage_type& Update(const ::MoReFEM::Wrappers::EigenNS::dWVector& cauchy_green_tensor_value);

      private:
        //! Dimension of the mesh considered.
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;

        /*!
         * \brief Access to the vector.
         *
         * \internal This one is at the moment private because all uses are currently covered by Update().
         * \endinternal
         *
         * \return Vector.
         */
        const storage_type& GetVector() const noexcept;

        //! Non constant access to the vector.
        storage_type& GetNonCstVector() noexcept;

      private:
        //! Mesh dimension.
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;

        //! The vector.
        storage_type vector_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Advanced/GreenLagrangeTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
