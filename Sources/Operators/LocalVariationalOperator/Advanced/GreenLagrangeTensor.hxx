// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/GreenLagrangeTensor.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    inline auto GreenLagrangeTensor::GetMeshDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        assert(mesh_dimension_ == ::MoReFEM::GeometryNS::dimension_type{ 2 }
               || mesh_dimension_ == ::MoReFEM::GeometryNS::dimension_type{ 3 });
        return mesh_dimension_;
    }


    inline auto GreenLagrangeTensor::GetVector() const noexcept -> const ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>&
    {
        return vector_;
    }


    inline auto GreenLagrangeTensor::GetNonCstVector() noexcept -> ::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>&
    {
        return const_cast<::MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>&>(GetVector());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
