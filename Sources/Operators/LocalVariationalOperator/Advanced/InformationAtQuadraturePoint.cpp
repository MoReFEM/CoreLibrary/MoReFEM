// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <optional>

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"

#include "Geometry/StrongType.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    namespace // anonymous
    {

        std::optional<::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList>
        InitForTestUnknown(const QuadraturePoint& quadrature_point,
                           const RefGeomElt& ref_geom_elt,
                           const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                           const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& test_ref_felt_list,
                           ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                           AllocateGradientFEltPhi do_allocate_gradient_felt_phi);


    }


    InformationAtQuadraturePoint ::InformationAtQuadraturePoint(
        const QuadraturePoint& quadrature_point,
        const RefGeomElt& ref_geom_elt,
        const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
        const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& test_ref_felt_list,
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
    : quadrature_point_(quadrature_point), mesh_dimension_(mesh_dimension),
      for_unknown_{ InfosAtQuadPointNS::ForUnknownList(quadrature_point,
                                                       ref_geom_elt,
                                                       ref_felt_list,
                                                       mesh_dimension,
                                                       do_allocate_gradient_felt_phi) },
      for_test_unknown_{ InitForTestUnknown(quadrature_point,
                                            ref_geom_elt,
                                            ref_felt_list,
                                            test_ref_felt_list,
                                            mesh_dimension,
                                            do_allocate_gradient_felt_phi) }
    { }


    void InformationAtQuadraturePoint::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
    {
        GetNonCstUnknownData().ComputeLocalFEltSpaceData(local_felt_space);

        if (for_test_unknown_)
            GetNonCstTestUnknownData().ComputeLocalFEltSpaceData(local_felt_space);
    }


    namespace // anonymous
    {

        std::optional<::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList>
        InitForTestUnknown(const QuadraturePoint& quadrature_point,
                           const RefGeomElt& ref_geom_elt,
                           const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
                           const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& test_ref_felt_list,
                           const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                           AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
        {
            if (ref_felt_list == test_ref_felt_list)
                return std::nullopt;

            return InfosAtQuadPointNS::ForUnknownList(
                quadrature_point, ref_geom_elt, test_ref_felt_list, mesh_dimension, do_allocate_gradient_felt_phi);
        }


    } // namespace


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
