// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <optional>

#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM { class LocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \class doxygen_hide_infos_at_quad_pt_arg
     *
     * \param[in] infos_at_quad_pt Object which stores data related to a given quadrature point, such as the
     * geometric and finite element shape function values.
     */


    /*!
     * \brief Stores data related to a given quadrature point, such as the geometric and finite element shape
     * function values.
     *
     * All unknowns (or test unknowns) are referenced in this object; it's up to the variational operator
     * developer to know how unknowns and tests unknowns are structured.
     *
     */
    class InformationAtQuadraturePoint final
    {

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         * \param[in] quadrature_point \a QuadraturePoint for which the helper data are computed.
         * \param[in] ref_geom_elt \a RefGeomElt considered.
         * \param[in] ref_felt_list List of reference finite elements (The brand adapted for usage in
         * \a LocalVariationalOperator).
         * \param[in] test_ref_felt_list List of reference finite elements for test functions. It might be the
         * same as \a ref_felt_list if test unknowns all match unknowns. \param[in] mesh_dimension Dimension of
         * the mesh (so might be higher than dimension of \a ref_geom_elt.
         */
        explicit InformationAtQuadraturePoint(
            const QuadraturePoint& quadrature_point,
            const RefGeomElt& ref_geom_elt,
            const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list,
            const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& test_ref_felt_list,
            ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
            AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

        //! Destructor.
        ~InformationAtQuadraturePoint() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InformationAtQuadraturePoint(const InformationAtQuadraturePoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InformationAtQuadraturePoint(InformationAtQuadraturePoint&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InformationAtQuadraturePoint& operator=(const InformationAtQuadraturePoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InformationAtQuadraturePoint& operator=(InformationAtQuadraturePoint&& rhs) = delete;

        ///@}


      public:
        //! Get quadrature point information.
        const QuadraturePoint& GetQuadraturePoint() const noexcept;

        //! Returns the dimension of the mesh.
        ::MoReFEM::GeometryNS::dimension_type GetMeshDimension() const noexcept;


        /*!
         * \brief Compute the attributes that depends on the \a local_felt_space.
         *
         * \param[in] local_felt_space \a LocalFEltSpace for which data are computed.
         */
        void ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space);

        //! Accessor to the data regarding unknown.
        const ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& GetUnknownData() const noexcept;

        /*!
         * \brief Accessor to the data regarding test unknown.
         *
         * \internal Contrary to what is usually done, this one is not implemented on top of
         * GetNonCstTestUnknownData(): it might be called and gives away the correct data whether the test
         * unknown is the same as the unknown or not (whereas the non constant and PRIVATE accessor is to
         * be called only when test unknown differ from unknown).
         * \endinternal
         *
         * \return Reference to the data regarding test unknown.
         */
        const ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& GetTestUnknownData() const noexcept;

      private:
        //! Non constant accessor to the data regarding unknown.
        ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& GetNonCstUnknownData() noexcept;

        /*!
         * \brief Non constant accessor to the data regarding test unknown.
         *
         * \internal This accessor should be called only when for_test_unknown_ actually handles a value.
         * If not, it means test unknown is same as unknown and the for_unknown_ object should be used.
         * \endinternal
         *
         * \return Reference to the data regarding test unknown.
         */
        ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& GetNonCstTestUnknownData() noexcept;

      private:
        /// \name Reference quantities
        ///@{

        //! Quadrature point considered in the class.
        const QuadraturePoint& quadrature_point_;

        //! Dimension of the mesh (might be higher than the one of finite elements here).
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;


        ///@}

        /*!
         * \brief Specific data related to the unknowns (by opposition to test unknowns).
         *
         * If test unknown is the same, these data are also used for it.
         */
        ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList for_unknown_;

        /*!
         * \brief Specific data related to the test unknowns.
         *
         * Optional is not filled if test unknowns are the same as unknowns.
         *
         * There might be a different number of unknowns and test unknowns, and no ordering relationship
         * is assumed: all these considerations are up to the developer of the variational operator
         * (in the order in which he/she puts the unknowns and test unknowns in the constructor).
         */
        std::optional<::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList> for_test_unknown_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
