// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <optional>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::InfosAtQuadPointNS { class ForUnknownList; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    inline const QuadraturePoint& InformationAtQuadraturePoint::GetQuadraturePoint() const noexcept
    {
        return quadrature_point_;
    }


    inline auto InformationAtQuadraturePoint::GetMeshDimension() const noexcept -> ::MoReFEM::GeometryNS::dimension_type
    {
        return mesh_dimension_;
    }


    inline auto InformationAtQuadraturePoint::GetUnknownData() const noexcept
        -> const ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList&
    {
        return for_unknown_;
    }


    inline auto InformationAtQuadraturePoint::GetTestUnknownData() const noexcept
        -> const ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList&
    {
        if (for_test_unknown_) // case in which test unknown is not the same as unknown
        {
            assert(for_test_unknown_.has_value());
            return for_test_unknown_.value();
        }

        return GetUnknownData();
    }


    inline auto InformationAtQuadraturePoint::GetNonCstUnknownData() noexcept
        -> ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList&
    {
        return const_cast<::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList&>(GetUnknownData());
    }


    inline auto InformationAtQuadraturePoint::GetNonCstTestUnknownData() noexcept
        -> ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList&
    {
        assert(for_test_unknown_.has_value()
               && "If not, this private method should not be called as it should mean "
                  "test unknown is the same as unknown.");

        return for_test_unknown_.value();
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
