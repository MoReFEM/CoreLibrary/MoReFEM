### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.cpp
		${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/DerivativeGreenLagrange.hpp
		${CMAKE_CURRENT_LIST_DIR}/DerivativeGreenLagrange.hxx
		${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.hpp
		${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.hxx
		${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint.hpp
		${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromLinearAlgebra/SourceList.cmake)
