// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_EXTENDEDUNKNOWNANDTESTUNKNOWNLIST_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_EXTENDEDUNKNOWNANDTESTUNKNOWNLIST_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    template<class DerivedT>
    ExtendedUnknownAndTestUnknownList<DerivedT>::ExtendedUnknownAndTestUnknownList(
        const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
        const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list)
    : extended_unknown_list_(extended_unknown_list), test_extended_unknown_list_(test_extended_unknown_list)
    {
        assert(!extended_unknown_list.empty());
#ifndef NDEBUG
        {
            const auto& list = GetExtendedTestUnknownList();
            assert(!list.empty());
            assert(std::none_of(list.cbegin(), list.cend(), Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
        }
#endif // NDEBUG
    }


    template<class DerivedT>
    inline const ExtendedUnknown::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedTestUnknownList() const noexcept
    {
        return test_extended_unknown_list_;
    }


    template<class DerivedT>
    const ExtendedUnknown&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthTestUnknown(std::size_t index) const noexcept
    {
        const auto& list = GetExtendedTestUnknownList();
        assert(index < list.size());
        assert(!(!list[index]));
        return *list[index];
    }


    template<class DerivedT>
    inline const ExtendedUnknown::vector_const_shared_ptr&
    ExtendedUnknownAndTestUnknownList<DerivedT>::GetExtendedUnknownList() const noexcept
    {
        return extended_unknown_list_;
    }


    template<class DerivedT>
    const ExtendedUnknown& ExtendedUnknownAndTestUnknownList<DerivedT>::GetNthUnknown(std::size_t index) const noexcept
    {
        const auto& list = GetExtendedUnknownList();
        assert(index < list.size());
        assert(!(!list[index]));
        return *list[index];
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_EXTENDEDUNKNOWNANDTESTUNKNOWNLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
