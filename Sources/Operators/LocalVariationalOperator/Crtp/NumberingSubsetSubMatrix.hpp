// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <algorithm>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Containers/Vector.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix/SubMatrixForNumberingSubsetPair.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    /*!
     * \brief Store elementary matrices of type \a MatrixType along with the numbering subset pair that can
     * pinpoint each of them.
     *
     */

    template<class DerivedT, class MatrixTypeT = Eigen::MatrixXd>
    class NumberingSubsetSubMatrix
    {

      public:
        //! Convenient alias.
        using list_type = typename SubMatrixForNumberingSubsetPair<MatrixTypeT>::vector_unique_ptr;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit NumberingSubsetSubMatrix() = default;

        //! Destructor.
        ~NumberingSubsetSubMatrix() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NumberingSubsetSubMatrix(const NumberingSubsetSubMatrix& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        NumberingSubsetSubMatrix(NumberingSubsetSubMatrix&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NumberingSubsetSubMatrix& operator=(const NumberingSubsetSubMatrix& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        NumberingSubsetSubMatrix& operator=(NumberingSubsetSubMatrix&& rhs) = default;

        ///@}

        //! Get the local matrix that matches the given numbering subsets.
        //! \param[in] row_numbering_subset \a NumberingSubset for rows used as filter.
        //! \param[in] col_numbering_subset \a NumberingSubset for columns used as filter.
        MatrixTypeT& GetSubMatrix(const NumberingSubset& row_numbering_subset,
                                  const NumberingSubset& col_numbering_subset) const;

        //! Allocate the sub-matrices.
        //! \param[in] elementary_data Data used to allocate properly the sub matrices.
        template<class ElementaryDataT>
        void AllocateSubMatrices(const ElementaryDataT& elementary_data);

      private:
        //! Accessor to the underlying list.
        const list_type& GetSubMatrixList() const;

      private:
        //! List of combination sub matrix/numbering subset pair.
        list_type sub_matrix_list_;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
