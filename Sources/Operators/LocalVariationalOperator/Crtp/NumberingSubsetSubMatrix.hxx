// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Core/NumberingSubset/Internal/FindFunctor.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    template<class DerivedT, class MatrixTypeT>
    MatrixTypeT&
    NumberingSubsetSubMatrix<DerivedT, MatrixTypeT>::GetSubMatrix(const NumberingSubset& row_numbering_subset,
                                                                  const NumberingSubset& col_numbering_subset) const
    {
        const auto& list = GetSubMatrixList();

        const auto end = list.end();

        using comp = typename Internal::NumberingSubsetNS::FindIfConditionForPair<typename list_type::value_type>;

        auto it = std::find_if(list.begin(), end, comp(row_numbering_subset, col_numbering_subset));

        assert(it != end);
        assert(!(!*it));
        return (*it)->GetSubMatrix();
    }


    template<class DerivedT, class MatrixTypeT>
    template<class ElementaryDataT>
    void NumberingSubsetSubMatrix<DerivedT, MatrixTypeT>::AllocateSubMatrices(const ElementaryDataT& elementary_data)
    {
        static_assert(!std::is_same<typename ElementaryDataT::matrix_type, std::false_type>(),
                      "THis method should be called only for valid elementary_data!");

        const auto& derived = static_cast<DerivedT&>(*this);

        decltype(auto) extended_unknown_list = derived.GetExtendedUnknownList();

        NumberingSubset::vector_const_shared_ptr numbering_subset_list;

        {

            for (const auto& extended_unknown_ptr : extended_unknown_list)
            {
                assert(!(!extended_unknown_ptr));
                numbering_subset_list.push_back(extended_unknown_ptr->GetNumberingSubsetPtr());
            }
        }

        Utilities::EliminateDuplicate(numbering_subset_list,
                                      Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                      Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

        decltype(auto) test_extended_unknown_list = derived.GetExtendedTestUnknownList();

        NumberingSubset::vector_const_shared_ptr test_numbering_subset_list;

        {

            for (const auto& extended_test_unknown_ptr : test_extended_unknown_list)
            {
                assert(!(!extended_test_unknown_ptr));
                test_numbering_subset_list.push_back(extended_test_unknown_ptr->GetNumberingSubsetPtr());
            }
        }

        Utilities::EliminateDuplicate(test_numbering_subset_list,
                                      Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                      Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

        NumberingSubset::vector_const_shared_ptr concatenate_numbering_subset_list;

        concatenate_numbering_subset_list.reserve(numbering_subset_list.size() + test_numbering_subset_list.size());

        std::set_union(numbering_subset_list.begin(),
                       numbering_subset_list.end(),
                       test_numbering_subset_list.begin(),
                       test_numbering_subset_list.end(),
                       std::back_inserter(concatenate_numbering_subset_list),
                       Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>());

        // Key : numbering_subset_id, Value : Ndof
        std::map<::MoReFEM::NumberingSubsetNS::unique_id, Eigen::Index> Ndof_per_numbering_subset;

        for (const auto& numbering_subset_ptr : concatenate_numbering_subset_list)
        {
            assert(numbering_subset_ptr != nullptr);
            const auto numbering_subset_id = numbering_subset_ptr->GetUniqueId();

            auto Ndof = Eigen::Index{};

            for (const auto& unknown_ptr : extended_unknown_list)
            {
                assert(!(!unknown_ptr));

                if (unknown_ptr->GetNumberingSubset().GetUniqueId() == numbering_subset_id)
                    Ndof += elementary_data.GetRefFElt(*unknown_ptr).Ndof();
            }

            [[maybe_unused]] auto check = Ndof_per_numbering_subset.insert({ numbering_subset_id, Ndof });
            assert(check.second && "Numbering subset within the list must be unique!");
        }

        // \todo #513 All combinations allocated here... However the memory used is quite limited: only a
        // handful of local matrices.
        for (const auto& row_numbering_subset_ptr : numbering_subset_list)
        {
            const auto& row_numbering_subset = *row_numbering_subset_ptr;

            auto it_row = Ndof_per_numbering_subset.find(row_numbering_subset.GetUniqueId());

            if (it_row != Ndof_per_numbering_subset.end())
            {
                const auto Nrow = it_row->second;

                for (const auto& col_numbering_subset_ptr : test_numbering_subset_list)
                {
                    const auto& col_numbering_subset = *col_numbering_subset_ptr;

                    auto it_col = Ndof_per_numbering_subset.find(col_numbering_subset.GetUniqueId());

                    if (it_col != Ndof_per_numbering_subset.end())
                    {
                        const auto Ncol = it_col->second;

                        auto ptr = std::make_unique<SubMatrixForNumberingSubsetPair<MatrixTypeT>>(row_numbering_subset,
                                                                                                  col_numbering_subset);

                        auto& sub_matrix = ptr->GetSubMatrix();

                        sub_matrix.resize(Nrow, Ncol);

                        sub_matrix_list_.emplace_back(std::move(ptr));
                    }
                }
            }
        }
    }


    template<class DerivedT, class MatrixTypeT>
    inline const typename NumberingSubsetSubMatrix<DerivedT, MatrixTypeT>::list_type&
    NumberingSubsetSubMatrix<DerivedT, MatrixTypeT>::GetSubMatrixList() const
    {
        return sub_matrix_list_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_DOT_HXX_
// *** MoReFEM end header guards *** < //
