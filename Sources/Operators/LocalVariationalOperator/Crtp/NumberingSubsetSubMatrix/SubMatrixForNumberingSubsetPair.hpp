// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_SUBMATRIXFORNUMBERINGSUBSETPAIR_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_SUBMATRIXFORNUMBERINGSUBSETPAIR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/Crtp/NumberingSubsetForMatrix.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    /*!
     * \brief Class which binds a local matrix object to a pair of numbering subset.
     *
     * It is intended to be used to store efficiently the matrix structure used to assemble
     * according to a given pair of numbering subsets.
     *
     */
    template<class MatrixTypeT = Eigen::MatrixXd>
    class SubMatrixForNumberingSubsetPair final
    : public Crtp::NumberingSubsetForMatrix<SubMatrixForNumberingSubsetPair<MatrixTypeT>>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SubMatrixForNumberingSubsetPair<MatrixTypeT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias for parent.
        using parent = Crtp::NumberingSubsetForMatrix<self>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] row_numbering_subset \a NumberingSubset used for numbering the rows.
        //! \param[in] col_numbering_subset \a NumberingSubset used for numbering the columns.
        explicit SubMatrixForNumberingSubsetPair(const NumberingSubset& row_numbering_subset,
                                                 const NumberingSubset& col_numbering_subset);

        //! Destructor.
        ~SubMatrixForNumberingSubsetPair() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SubMatrixForNumberingSubsetPair(const SubMatrixForNumberingSubsetPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SubMatrixForNumberingSubsetPair(SubMatrixForNumberingSubsetPair&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SubMatrixForNumberingSubsetPair& operator=(const SubMatrixForNumberingSubsetPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SubMatrixForNumberingSubsetPair& operator=(SubMatrixForNumberingSubsetPair&& rhs) = delete;

        ///@}

        //! Returns the local matrix.
        MatrixTypeT& GetSubMatrix();


      private:
        /*!
         * \brief Local matrix to be given to Petsc function for assembling.
         *
         * This matrix is a subset of the computed elementary matrix for the operator.
         */
        MatrixTypeT local_matrix_;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix/SubMatrixForNumberingSubsetPair.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_SUBMATRIXFORNUMBERINGSUBSETPAIR_DOT_HPP_
// *** MoReFEM end header guards *** < //
