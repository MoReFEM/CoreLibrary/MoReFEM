// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/LinearAlgebra/Type.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export

#include "Operators/Enum.hpp"
#include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM
{


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ::MoReFEM::ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    class GlobalParameterOperator;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //

namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{

    template
    <
        ::MoReFEM::Concept::Tuple TupleT,
        std::size_t I,
        std::size_t TupleSizeT,
        ::MoReFEM::Advanced::OperatorNS::Nature NatureT
    >
    struct LocalVariationalOperatorIterator;


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    /*!
     * \class doxygen_hide_elementary_data_unknown_and_test_unknown_param
     *
     * \param[in] unknown_storage List of unknown/numbering subset pairs involved in the current local operator.
     * There can be two at most; their relative ordering fix the structure of the local matrix (but it should be
     * completely transparent for the user anyway: the library takes care of fetching the appropriate elements
     * through RefFEltInLocalOperator class).
     * \param[in] test_unknown_storage Same as \a unknown_storage for test functions.
     */


    /*!
     * \brief This object holds all the data required to compute the elementary matrix or vector and the resulting
     * one.
     *
     *
     * \attention the values inside the local matrices and vectors may change frequently, but the allocation does
     * not: the size of the matrices and vectors is fixed early in the program and is not modified until
     * the deallocation when the program is done.
     *
     * \tparam MatrixTypeT Type of the elementary matrix, or std::false_type if irrelevant.
     * \tparam VectorTypeT Type of the elementary vector, or std::false_type if irrelevant.
     *
     */

    template<class MatrixTypeT, class VectorTypeT>
    class ElementaryData : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl
    {
      public:
        //! Convenient alias.
        using self = ElementaryData<MatrixTypeT, VectorTypeT>;

        //! Convenient alias.
        using matrix_type = MatrixTypeT;

        //! Convenient alias.
        using vector_type = VectorTypeT;

        //! Friendship to GlobalParameterOperator, one of the two classes allowed to build such an object.
        // clang-format off
        template
        <
            class DerivedT,
            class LocalParameterOperatorT,
            ::MoReFEM::ParameterNS::Type TypeT,
            TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
            template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
            ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
        >
        // clang-format on
        friend class ::MoReFEM::GlobalParameterOperator;

        //! Friendship to an internal helper structure.
        // clang-format off
        template
        <
            ::MoReFEM::Concept::Tuple TupleT,
            std::size_t I,
            std::size_t TupleSizeT,
            Advanced::OperatorNS::Nature NatureT
        >
        // clang-format on
        friend struct ::MoReFEM::Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;

      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
         * list of unknowns involved for the operator related to this ElementaryData will provide all the data
         * required to build RefFEltInLocalOperator objects that allow easy navigation through the
         * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the
         * required data).
         * \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
         * \copydoc doxygen_hide_elementary_data_unknown_and_test_unknown_param
         *
         * \param[in] felt_space_dimension Dimension considered in the finite element space.
         * \param[in] mesh_dimension Dimension in the mesh.
         *  Must be equal or higher than felt_space_dimension.
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         */
        explicit ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                const QuadratureRule& quadrature_rule,
                                const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                ::MoReFEM::GeometryNS::dimension_type felt_space_dimension,
                                ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

        //! Zero the underlying matrix and/or vector.
        void Zero();


        //! Constant access to the underlying elementary vector.
        const VectorTypeT& GetVectorResult() const noexcept
            requires(!std::is_same_v<VectorTypeT, std::false_type>);

        //! Non constant access to the underlying elementary vector.
        VectorTypeT& GetNonCstVectorResult() noexcept
            requires(!std::is_same_v<VectorTypeT, std::false_type>);


        //! Constant access to the underlying elementary matrix.
        const MatrixTypeT& GetMatrixResult() const noexcept
            requires(!std::is_same_v<MatrixTypeT, std::false_type>);

        //! Non constant access to the underlying elementary matrix.
        MatrixTypeT& GetNonCstMatrixResult() noexcept
            requires(!std::is_same_v<MatrixTypeT, std::false_type>);


      private:
        //! Underlying vector (or `std::false_type` for a bilinear operator).
        VectorTypeT vector_;

        //! Underlying matrix (or `std::false_type` for a linear operator).
        MatrixTypeT matrix_;
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/ElementaryData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
