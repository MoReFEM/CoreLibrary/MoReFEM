// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/ElementaryData.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced
{


    template<class MatrixTypeT, class VectorTypeT>
    ElementaryData<MatrixTypeT, VectorTypeT>::ElementaryData(
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
        const QuadratureRule& quadrature_rule,
        const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
        ::MoReFEM::GeometryNS::dimension_type felt_space_dimension,
        ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
    : Internal::LocalVariationalOperatorNS::ElementaryDataImpl(ref_felt_space,
                                                               quadrature_rule,
                                                               unknown_storage,
                                                               test_unknown_storage,
                                                               felt_space_dimension,
                                                               mesh_dimension,
                                                               do_allocate_gradient_felt_phi)
    {
        if constexpr (!std::is_same_v<MatrixTypeT, std::false_type>)
            matrix_.resize(this->NdofRow(), this->NdofCol());

        if constexpr (!std::is_same_v<VectorTypeT, std::false_type>)
            vector_.resize(this->NdofRow());
    }


    template<class MatrixTypeT, class VectorTypeT>
    void ElementaryData<MatrixTypeT, VectorTypeT>::Zero()
    {
        if constexpr (!std::is_same_v<MatrixTypeT, std::false_type>)
            matrix_.setZero();

        if constexpr (!std::is_same_v<VectorTypeT, std::false_type>)
            vector_.setZero();
    }


    template<class MatrixTypeT, class VectorTypeT>
    auto ElementaryData<MatrixTypeT, VectorTypeT>::GetVectorResult() const noexcept
        -> const VectorTypeT& requires(!std::is_same_v<VectorTypeT, std::false_type>) { return vector_; }

    template<class MatrixTypeT, class VectorTypeT>
    auto ElementaryData<MatrixTypeT, VectorTypeT>::GetNonCstVectorResult() noexcept
        -> VectorTypeT& requires(!std::is_same_v<VectorTypeT, std::false_type>) {
            return const_cast<VectorTypeT&>(GetVectorResult());
        }


    template<class MatrixTypeT, class VectorTypeT>
    auto ElementaryData<MatrixTypeT, VectorTypeT>::GetMatrixResult() const noexcept
        -> const MatrixTypeT& requires(!std::is_same_v<MatrixTypeT, std::false_type>) { return matrix_; }


    template<class MatrixTypeT, class VectorTypeT>
    auto ElementaryData<MatrixTypeT, VectorTypeT>::GetNonCstMatrixResult() noexcept
        -> MatrixTypeT& requires(!std::is_same_v<MatrixTypeT, std::false_type>) {
            return const_cast<MatrixTypeT&>(GetMatrixResult());
        }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
