// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <optional>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp"

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/Enum.hpp"
#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    ElementaryDataImpl::ElementaryDataImpl(const Internal::RefFEltNS::RefLocalFEltSpace& a_ref_felt_space,
                                           const QuadratureRule& quadrature_rule,
                                           const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                           const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                           ::MoReFEM::GeometryNS::dimension_type felt_space_dimension,
                                           ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                           AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
    : ref_felt_space_(a_ref_felt_space), quadrature_rule_(quadrature_rule),
      felt_space_dimension_(static_cast<Eigen::Index>(felt_space_dimension)),
      mesh_dimension_(static_cast<Eigen::Index>(mesh_dimension))
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);
        assert(felt_space_dimension <= mesh_dimension);

        const auto& ref_felt_space = GetRefLocalFEltSpace();

        const auto Nquadrature_point = quadrature_rule.NquadraturePoint();
        const auto& ref_geom_elt = ref_felt_space.GetRefGeomElt();

        InitAllReferenceFiniteElements(unknown_storage, test_unknown_storage);

        infos_at_quad_pt_list_.reserve(static_cast<std::size_t>(Nquadrature_point));

        SetGeomEltDimension();

        Ncoords_in_geom_ref_elt_ = ref_geom_elt.Ncoords();

        point_.resize(Ncoords_in_geom_ref_elt_.Get(), GetGeomEltDimension().Get());

        // Initialize correctly the information at each quadrature point.
        for (auto quadrature_point_index = Eigen::Index{}; quadrature_point_index < Nquadrature_point;
             ++quadrature_point_index)
        {
            const auto& quadrature_point = quadrature_rule.Point(quadrature_point_index);

            decltype(auto) test_ref_list = GetTestRefFEltList();

            Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint item(quadrature_point,
                                                                                    ref_geom_elt,
                                                                                    GetRefFEltList(),
                                                                                    test_ref_list,
                                                                                    mesh_dimension,
                                                                                    do_allocate_gradient_felt_phi);

            infos_at_quad_pt_list_.emplace_back(std::move(item));
        }

        assert(infos_at_quad_pt_list_.size() == static_cast<std::size_t>(Nquadrature_point));
    }


    void ElementaryDataImpl::UpdateCoordinates(const GeometricElt& geometric_element)
    {
        const auto Ncomponent = this->GetGeomEltDimension();
        const auto Nnode_in_geometric_element = this->NnodeInRefGeomElt();

        const auto& coords_list = geometric_element.GetCoordsList();
        assert(coords_list.size() == static_cast<std::size_t>(Nnode_in_geometric_element.Get()));

        for (auto local_coords_index = ::MoReFEM::GeomEltNS::Nlocal_coords_type{};
             local_coords_index < Nnode_in_geometric_element;
             ++local_coords_index)
        {
            const auto& current_point_ptr = coords_list[static_cast<std::size_t>(local_coords_index.Get())];
            assert(!(!current_point_ptr));

            const auto& current_point = *current_point_ptr;

            const auto node_index = local_coords_index;

            for (auto component = ::MoReFEM::GeometryNS::dimension_type{}; component < Ncomponent; ++component)
                point_(node_index.Get(), component.Get()) = current_point[component];
        }
    }


    void ElementaryDataImpl::ComputeLocalFEltSpaceData(const LocalFEltSpace& local_felt_space)
    {
        auto& infos_at_quad_pt_list = GetNonCstInformationAtQuadraturePointList();

        SetCurrentLocalFEltSpace(&local_felt_space);

        for (auto& infos_at_quad_pt : infos_at_quad_pt_list)
            infos_at_quad_pt.ComputeLocalFEltSpaceData(local_felt_space);
    }


    void ElementaryDataImpl::SetGeomEltDimension()
    {
        geom_elt_dimension_ = GetRefLocalFEltSpace().GetRefGeomElt().GetDimension();
    }


    namespace // anonymous
    {


        void InitRefFEltList(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                             const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                             LocalNodeNS::index_type& Nnode,
                             Eigen::Index& Ndof,
                             Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ret)
        {
            assert(Nnode == LocalNodeNS::index_type{});
            assert(Ndof == 0);

            for (const auto& extended_unknown_ptr : unknown_storage)
            {
                const auto& ref_felt = ref_felt_space.GetRefFElt(*extended_unknown_ptr);

                auto& new_elt = ret.emplace_back(
                    Internal::WrapUniqueToConst(new Advanced::RefFEltInLocalOperator(ref_felt, Nnode, Ndof)));

                Nnode += new_elt->Nnode();
                Ndof += new_elt->Ndof();
            }
        }


    } // namespace


    void ElementaryDataImpl ::InitAllReferenceFiniteElements(
        const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage)

    {
        const auto& ref_felt_space = GetRefLocalFEltSpace();

        assert(ref_felt_list_.empty());

        assert(Nnode_col_ == LocalNodeNS::index_type{});
        assert(Ndof_col_ == 0);
        assert(Nnode_row_ == LocalNodeNS::index_type{});
        assert(Ndof_row_ == 0);

        const bool are_test_and_unknown_equal = (unknown_storage == test_unknown_storage);

        InitRefFEltList(ref_felt_space, unknown_storage, Nnode_col_, Ndof_col_, ref_felt_list_);

        if (are_test_and_unknown_equal)
        {
            Nnode_row_ = Nnode_col_;
            Ndof_row_ = Ndof_col_;

            test_ref_felt_list_ = {}; // let optional empty, and accessors will fetch ref_felt_list_ which
                                      // holds the proper content.
        } else
        {
            test_ref_felt_list_ = std::make_optional(Advanced::RefFEltInLocalOperator::vector_const_unique_ptr());

            InitRefFEltList(ref_felt_space, test_unknown_storage, Nnode_row_, Ndof_row_, test_ref_felt_list_.value());
        }
    }


    namespace // anonymous
    {


        const Advanced::RefFEltInLocalOperator&
        GetRefFEltImpl(const ExtendedUnknown& unknown,
                       const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list);


    } // namespace


    const Advanced::RefFEltInLocalOperator& ElementaryDataImpl::GetRefFElt(const ExtendedUnknown& unknown) const
    {
        const auto& ref_felt_list = GetRefFEltList();
        return GetRefFEltImpl(unknown, ref_felt_list);
    }


    const Advanced::RefFEltInLocalOperator& ElementaryDataImpl::GetTestRefFElt(const ExtendedUnknown& unknown) const
    {
        const auto& test_ref_felt_list = GetTestRefFEltList();
        return GetRefFEltImpl(unknown, test_ref_felt_list);
    }


    namespace // anonymous
    {


        const Advanced::RefFEltInLocalOperator&
        GetRefFEltImpl(const ExtendedUnknown& unknown,
                       const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr& ref_felt_list)
        {
            auto it = std::ranges::find_if(ref_felt_list,

                                           [&unknown](const auto& current_ref_felt)
                                           {
                                               assert(!(!current_ref_felt));
                                               return current_ref_felt->GetExtendedUnknown() == unknown;
                                           });
            assert(it != ref_felt_list.cend());
            assert(!(!(*it)));
            return *(*it);
        }


    } // namespace


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
