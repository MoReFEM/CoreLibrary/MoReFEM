### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/ElementaryDataImpl.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/ElementaryDataImpl.hpp
		${CMAKE_CURRENT_LIST_DIR}/ElementaryDataImpl.hxx
		${CMAKE_CURRENT_LIST_DIR}/EnumClass.hpp
		${CMAKE_CURRENT_LIST_DIR}/LocalVariationalOperator.hpp
		${CMAKE_CURRENT_LIST_DIR}/LocalVariationalOperator.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

