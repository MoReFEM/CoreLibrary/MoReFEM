// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_LINEARLOCALVARIATIONALOPERATOR_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_LINEARLOCALVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief The base class for all linear form of local variational operators.
     *
     * \tparam VectorTypeT Type of the elementary vector.
     */
    template<class VectorTypeT>
    class LinearLocalVariationalOperator
    : public Internal::LocalVariationalOperatorNS::
          LocalVariationalOperator<OperatorNS::Nature::linear, std::false_type, VectorTypeT>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LinearLocalVariationalOperator<VectorTypeT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to parent.
        using parent = Internal::LocalVariationalOperatorNS::
            LocalVariationalOperator<OperatorNS::Nature::linear, std::false_type, VectorTypeT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to matrix type.
        using matrix_type = VectorTypeT;

        //! Alias to elementary data type.
        using elementary_data_type = typename parent::elementary_data_type;


      public:
        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_local_var_op_constructor
        explicit LinearLocalVariationalOperator(
            const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
            elementary_data_type&& elementary_data);

        //! Destructor.
        virtual ~LinearLocalVariationalOperator() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        LinearLocalVariationalOperator(const LinearLocalVariationalOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LinearLocalVariationalOperator(LinearLocalVariationalOperator&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LinearLocalVariationalOperator& operator=(const LinearLocalVariationalOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LinearLocalVariationalOperator& operator=(LinearLocalVariationalOperator&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/LinearLocalVariationalOperator.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_LINEARLOCALVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
