// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_LINEARLOCALVARIATIONALOPERATOR_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_LINEARLOCALVARIATIONALOPERATOR_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/LinearLocalVariationalOperator.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class VectorTypeT>
    LinearLocalVariationalOperator<VectorTypeT>::LinearLocalVariationalOperator(
        const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
        typename parent::elementary_data_type&& elementary_data)
    : parent(unknown_list, test_unknown_list, std::move(elementary_data))
    { }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_LINEARLOCALVARIATIONALOPERATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
