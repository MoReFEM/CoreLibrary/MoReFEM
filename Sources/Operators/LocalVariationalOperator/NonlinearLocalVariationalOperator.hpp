// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_NONLINEARLOCALVARIATIONALOPERATOR_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_NONLINEARLOCALVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix.hpp"
#include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief The base class for all nonlinear form of local variational operators.
     *
     * \tparam MatrixTypeT Type of the elementary matrix.
     * \tparam VectorTypeT Type of the elementary vector.
     */
    template<class MatrixTypeT, class VectorTypeT>
    class NonlinearLocalVariationalOperator
    : public Internal::LocalVariationalOperatorNS::
          LocalVariationalOperator<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>,
      public Internal::LocalVariationalOperatorNS::NumberingSubsetSubMatrix<
          NonlinearLocalVariationalOperator<MatrixTypeT, VectorTypeT>>
    {

      public:
        //! Convenient alias.
        using self = NonlinearLocalVariationalOperator<MatrixTypeT, VectorTypeT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to parent.
        using parent = Internal::LocalVariationalOperatorNS::
            LocalVariationalOperator<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Convenient alias to parent.
        using sub_matrix_parent = Internal::LocalVariationalOperatorNS::NumberingSubsetSubMatrix<self>;

        //! Alias to matrix type.
        using matrix_type = MatrixTypeT;

        //! Alias to vector type.
        using vector_type = VectorTypeT;

        //! Alias to elementary data type.
        using elementary_data_type = typename parent::elementary_data_type;


      public:
        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_local_var_op_constructor
        explicit NonlinearLocalVariationalOperator(
            const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
            const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
            elementary_data_type&& elementary_data);

        //! Destructor.
        virtual ~NonlinearLocalVariationalOperator() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        NonlinearLocalVariationalOperator(const NonlinearLocalVariationalOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonlinearLocalVariationalOperator(NonlinearLocalVariationalOperator&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonlinearLocalVariationalOperator& operator=(const NonlinearLocalVariationalOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonlinearLocalVariationalOperator& operator=(NonlinearLocalVariationalOperator&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_NONLINEARLOCALVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
