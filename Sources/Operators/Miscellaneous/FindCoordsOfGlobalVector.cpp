// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep

#include "Operators/Miscellaneous/FindCoordsOfGlobalVector.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/EnumInterface.hpp"
#include "Geometry/Interfaces/Interface.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Nodes_and_dofs/Node.hpp"


namespace MoReFEM
{


    FindCoordsOfGlobalVector::FindCoordsOfGlobalVector(const FEltSpace& felt_space, const GlobalVector& global_vector)
    {
        decltype(auto) felt_space_dof_list = felt_space.GetProcessorWiseDofList();

        const auto Ndof = static_cast<std::size_t>(global_vector.GetProcessorWiseSize().Get());
        coords_list_.resize(Ndof, nullptr);

        decltype(auto) numbering_subset = global_vector.GetNumberingSubset();

        for (const auto& dof_ptr : felt_space_dof_list)
        {
            assert(!(!dof_ptr));
            const auto& dof = *dof_ptr;

            if (dof.IsInNumberingSubset(numbering_subset))
            {
                const auto node_ptr = dof.GetNodeFromWeakPtr();
                assert(!(!node_ptr));

                const auto node_bearer_ptr = node_ptr->GetNodeBearerFromWeakPtr();

                const auto& interface = node_bearer_ptr->GetInterface();
                assert(interface.GetNature() == InterfaceNS::Nature::vertex
                       && "At the moment this function works only for P1 dofs...");

                decltype(auto) coords_list = interface.GetCoordsList();
                assert(coords_list.size() == 1 && "Still the P1 hypothesis...");
                assert(!(!coords_list.back()));

                const auto dof_processor_wise_index = dof.GetProcessorWiseOrGhostIndex(numbering_subset);

                assert(static_cast<std::size_t>(dof_processor_wise_index) < Ndof);
                coords_list_[dof_processor_wise_index.Get()] = coords_list.back();
            }
        }

        assert(std::ranges::none_of(coords_list_, Utilities::IsNullptr<Coords::shared_ptr>));
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
