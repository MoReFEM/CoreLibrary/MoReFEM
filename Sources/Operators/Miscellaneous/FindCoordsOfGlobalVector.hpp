// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_MISCELLANEOUS_FINDCOORDSOFGLOBALVECTOR_DOT_HPP_
#define MOREFEM_OPERATORS_MISCELLANEOUS_FINDCOORDSOFGLOBALVECTOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>

#include "Geometry/Coords/Coords.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief Returns for a global vector a vector if equal size with the Coords on which each dofs are located.
     *
     * This operation is purely processor-wise.
     *
     * \attention At the time being, it is rather crude: it works only for P1. It could be extended relatively easily to
     * P1b, P2, P0, etc... by computing a Coords object at the barycenter of the interface (not done yet as the
     * model that requires this class is purely P1). Higher order is more difficult, as a decision has to be made
     * on the spatial position given to each node of the node bearer_.
     *
     */
    class FindCoordsOfGlobalVector
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FindCoordsOfGlobalVector;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] felt_space \a FEltSpace used as filter.
        //! \param[in] global_vector \a GlobalVector for which \a Coords are retrieved.
        explicit FindCoordsOfGlobalVector(const FEltSpace& felt_space, const GlobalVector& global_vector);

        //! Destructor.
        ~FindCoordsOfGlobalVector() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FindCoordsOfGlobalVector(const FindCoordsOfGlobalVector& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FindCoordsOfGlobalVector(FindCoordsOfGlobalVector&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FindCoordsOfGlobalVector& operator=(const FindCoordsOfGlobalVector& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FindCoordsOfGlobalVector& operator=(FindCoordsOfGlobalVector&& rhs) = delete;

        ///@}

        //! Get the list of coordinates.
        const Coords::vector_shared_ptr& GetCoordsList() const noexcept;

      private:
        //! Coords for each of the dofs covered by the numbering subset in the finite element space.
        Coords::vector_shared_ptr coords_list_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/Miscellaneous/FindCoordsOfGlobalVector.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_MISCELLANEOUS_FINDCOORDSOFGLOBALVECTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
