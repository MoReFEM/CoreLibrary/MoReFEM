// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_MISCELLANEOUS_FINDCOORDSOFGLOBALVECTOR_DOT_HXX_
#define MOREFEM_OPERATORS_MISCELLANEOUS_FINDCOORDSOFGLOBALVECTOR_DOT_HXX_
// IWYU pragma: private, include "Operators/Miscellaneous/FindCoordsOfGlobalVector.hpp"
// *** MoReFEM header guards *** < //


#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM
{


    inline const Coords::vector_shared_ptr& FindCoordsOfGlobalVector::GetCoordsList() const noexcept
    {
        return coords_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_MISCELLANEOUS_FINDCOORDSOFGLOBALVECTOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
