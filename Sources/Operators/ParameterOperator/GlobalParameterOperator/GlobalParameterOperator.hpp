// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_PARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_DOT_HPP_
#define MOREFEM_OPERATORS_PARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

#include "Utilities/Containers/Tuple/Tuple.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/TimeManager/Concept.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtQuadraturePoint.hpp"


namespace MoReFEM
{


    /*!
     * \brief Parent class of all `GlobalParameterOperator`.
     *
     * This kind of operator is used to update Parameters.
     *
     * \attention Contrary to variational operators, no local2global is automatically defined here, as only a subset
     * of such parameter might need it. If you need one that might not already be defined, call
     * \a FEltSpace::ComputeLocal2Global. Make sure to do so before the end of the initialization phase (which
     * ends at the end of Model::Initialize() call).
     *
     * \tparam LocalParameterOperatorT Class that defines the expected elementary behaviour at each quadrature point;
     * it must be derived from LocalParameterOperator.
     */
    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>
    >
    // clang-format on
    class GlobalParameterOperator
    {
      public:
        //! Alias over the class that handles the elementary calculation.
        using local_parameter_operator_type = LocalParameterOperatorT;

        //! Alias to the `ParameterAtQuadraturePoint` to use with the object.
        using param_at_quad_pt_type = typename local_parameter_operator_type::param_at_quad_pt_type;

        /// \name Special members.
        ///@{

      protected:
        /*!
         * \brief Constructor.
         *
         * \param[in] felt_space Finite element space upon which the operator is defined.
         * \param[in] unknown Unknown considered.
         *
         * \param[in] parameter Parameter at quadrature point on which the operator is defined.
         *
         * \param[in] args Variadic arguments that will be perfect-forwarded to the constructor of each
         * local_parameter_operator_type.
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         *
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         */
        template<typename... Args>
        explicit GlobalParameterOperator(const FEltSpace& felt_space,
                                         const Unknown& unknown,
                                         const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                         AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                                         param_at_quad_pt_type& parameter,
                                         Args&&... args);

        //! Protected destructor: no direct instance of this class should occur!
        ~GlobalParameterOperator() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalParameterOperator(const GlobalParameterOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalParameterOperator(GlobalParameterOperator&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalParameterOperator& operator=(const GlobalParameterOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalParameterOperator& operator=(GlobalParameterOperator&& rhs) = delete;


        ///@}

        //! Constant access to the parameter.
        const param_at_quad_pt_type& GetParameter() const noexcept;

      protected:
        /*!
         * \brief This method is in charge of updating the parameter at each quadrature point.
         *
         * This method uses the new C++ 11 feature of variadic template; so that these methods can
         * handle generically all the operators, whatever argument they might require. The drawback is that
         * it isn't clear which arguments are expected for a specific global operator; that's the reason the
         * following method is NOT called Assemble() and is protected rather than public. When a developer
         * introduces a new operator, he must therefore define a public \a Assemble() that calls the present one.
         *
         * For instance for UpdateFiberDeformation operator, which requires an additional \a displacement_increment
         * argument:
         *
         * \code
         * inline void UpdateFiberDeformation::Update(const GlobalVector& increment_displacement) const
         * {
         *      return parent::UpdateImpl(increment_displacement);
         * }
         * \endcode
         *
         * \param[in] args Variadic template arguments, specific to the operator being implemented. These arguments
         * might be global: they are to be given to DerivedT::SetComputeEltArrayArguments() which will produce
         * the local ones.
         *
         */
        template<typename... Args>
        void UpdateImpl(Args&&... args) const;

        //! Return the Unknown and its associated numbering subset.
        const ExtendedUnknown::const_shared_ptr& GetExtendedUnknownPtr() const;

        //! Return the Unknown and its associated numbering subset.
        const ExtendedUnknown& GetExtendedUnknown() const;


      protected:
        //! FEltSpace.
        const FEltSpace& GetFEltSpace() const noexcept;

        /*!
         * \brief Access to the container that contains the match between a reference geometric element and a local
         * Parameters operator.
         */
        const Utilities::PointerComparison::Map<RefGeomElt::shared_ptr, typename LocalParameterOperatorT::unique_ptr>&
        GetLocalOperatorPerRefGeomElt() const noexcept;


      protected:
        //! Whether the gradient of finite element is required or not.
        AllocateGradientFEltPhi DoAllocateGradientFEltPhi() const noexcept;


        /*!
         * \brief Perform the elementary calculation (more exactly handle the call to the LocalParameterOperator...)
         *
         * \internal This method is called in UpdateImpl() and has no business being called elsewhere.
         * \endinternal
         *
         * \param[in] local_felt_space Local finite element space considered.
         * \param[in,out] local_operator Local operator in charge of the elementary computation at each quadrature
         * point.
         *
         */
        void PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                          LocalParameterOperatorT& local_operator,
                                          std::tuple<>&&) const;


        /*!
         * \brief Overload when there are variadic arguments to handle.
         *
         * \internal This method is called in UpdateImpl() and has no business being called elsewhere.
         * \endinternal
         *
         * \param[in] local_felt_space Local finite element space considered.
         * \param[in,out] local_operator Local operator in charge of the elementary computation.
         * \param[in] args Variadic template arguments, specific to the operator being implemented. These arguments
         * might be global: they are to be given to DerivedT::SetComputeEltArrayArguments() which will produce
         * the local ones.
         *
         */
        template<typename... Args>
        void PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                          LocalParameterOperatorT& local_operator,
                                          std::tuple<Args...>&& args) const;


      protected:
        /*!
         * \brief Fetch the local operator associated to the finite element type.
         *
         * \return Local operator associated to a given reference geometric element. This method assumes the
         * \a ref_geom_elt is valid and gets an associated LocalOperator (an assert is there in debug mode to check
         * that); the check of such an assumption may be performed by a call to DoConsider() in case there is a genuine
         * reason to check that in a release mode context.
         *
         * \param[in] ref_geom_elt_id Identifier of the reference geometric element which local Parameters operator is
         * requested.
         *
         */
        LocalParameterOperatorT& GetNonCstLocalOperator(Advanced::GeometricEltEnum ref_geom_elt_id) const;

      protected:
        /*!
         * \brief Whether there is a local Parameters operator related to a given \a ref_geom_elt.
         *
         * \param[in] ref_geom_elt_id Identifier of the reference geometric element which pertinence is evaluated.
         *
         * \return True if a local Parameters operator was found.
         *
         * It might not if there is a restriction of the domain of definition (for instance elastic stiffness
         * operator does not act upon geometric objects of dimension 1).
         *
         * This method should be called prior to GetNonCstLocalOperator(): the latter will assert if the
         * \a ref_felt is invalid.
         */
        bool DoConsider(Advanced::GeometricEltEnum ref_geom_elt_id) const;

      private:
        /*!
         * \brief Create a LocalParameterOperator for each \a RefFEltInFEltSpace and store it into the class.
         *
         * \param[in] mesh_dimension Dimension of the mesh considered.
         * \copydoc doxygen_hide_cplusplus_variadic_args
         *
         * \param[in,out] parameter The \a ParameterAtQuadraturePoint object to be updated by the current operator.
         */
        template<typename... Args>
        void CreateLocalOperatorList(::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                     param_at_quad_pt_type& parameter,
                                     Args&&... args);


        //! Return the quadrature rule that match a given reference geometric element.
        //! \param[in] ref_geom_elt \a RefGeomElt for which the \a QuadratureRule is sought.
        const QuadratureRule& GetQuadratureRule(const RefGeomElt& ref_geom_elt) const;

        /*!
         * \brief Returns the quadrature rule to use for each topology.
         *
         * \return List of quadrature rule to use for each topology.
         */
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

      private:
        //! List of nodes eligible for boundary conditions.
        NodeBearer::vector_shared_ptr node_for_boundary_condition_;

        //! Map the finite element type to the matching local operator.
        std::unordered_map<Advanced::GeometricEltEnum, typename LocalParameterOperatorT::unique_ptr>
            local_operator_per_ref_geom_elt_;

        //! Finite element space.
        const FEltSpace& felt_space_;

        /*!
         * \brief List of quadrature rules for each type of reference geometric element considered.
         *
         * If left empty (nullptr) default one from \a FEltSpace is used.
         *
         * \internal This list is given in the constructor; it is up to the caller of the operator to make sure
         * all cases are covered. If not an exception is thrown.
         * \endinternal
         */
        const QuadratureRulePerTopology* const quadrature_rule_per_topology_ = nullptr;

        /*!
         * \brief Constant reference to parameter
         *
         * Reference might be constant here as the global parameter won't attempt any change directly; it is the
         * responsibility of \a LocalParameterOperator.
         */
        const param_at_quad_pt_type& parameter_;

        //! \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi
        const AllocateGradientFEltPhi do_allocate_gradient_felt_phi_;

        //! Unknown/numbering subset list.
        const ExtendedUnknown::const_shared_ptr extended_unknown_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_PARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
