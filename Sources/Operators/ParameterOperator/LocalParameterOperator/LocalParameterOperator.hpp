// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_PARAMETEROPERATOR_LOCALPARAMETEROPERATOR_LOCALPARAMETEROPERATOR_DOT_HPP_
#define MOREFEM_OPERATORS_PARAMETEROPERATOR_LOCALPARAMETEROPERATOR_LOCALPARAMETEROPERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtQuadraturePoint.hpp"

namespace MoReFEM::Advanced
{


    /*!
     * \brief Base class for any \a LocalParameterOperator instance.
     *
     * Such a class does the computation at a local level related to a \a GlobalParameterOperator. For each
     * \a GlobalParameterOperator, there is a \a LocalParameterOperator for each \a RefGeomElt considered in the
     * \a FEltSpace.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>
    >
    // clang-format on
    class LocalParameterOperator
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT, StorageT>;

        //! Intermediate alias for vector - if `TypeT` is `Type::vector` only.
        using intermediate_vector_type =
            std::conditional_t<TypeT == ParameterNS::Type::vector, Eigen::VectorXd, std::false_type>;

        //! Intermediate alias for matrix - if `TypeT` is `Type::matrix` only.
        using intermediate_matrix_type =
            std::conditional_t<TypeT == ParameterNS::Type::matrix, Eigen::MatrixXd, std::false_type>;

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to elementary data type.
        using elementary_data_type = ElementaryData<intermediate_matrix_type, intermediate_vector_type>;

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using param_at_quad_pt_type = ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT, StorageT>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] unknown \a ExtendedUnknown upon which the operator is acting.
        //! \param[in] elementary_data \a ElementaryDataImpl child used to store the elementary data needed
        //! for the computation.
        //! \param[in,out] parameter The \a Parameter that the operator is modifying.
        explicit LocalParameterOperator(const ExtendedUnknown::const_shared_ptr& unknown,
                                        elementary_data_type&& elementary_data,
                                        param_at_quad_pt_type& parameter);

        //! Destructor.
        ~LocalParameterOperator() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LocalParameterOperator(const LocalParameterOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LocalParameterOperator(LocalParameterOperator&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LocalParameterOperator& operator=(const LocalParameterOperator& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LocalParameterOperator& operator=(LocalParameterOperator&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Link the LocalParameterOperator to a given local finite element space.
         *
         * \param[in] local_felt_space Local finite element space to which the DerivedT will be associated.
         *
         * The data in elementary_data_ (matrices or vector mostly) are allocated at the beginning of the program
         * but their values keeps changing; what makes them change is current method which is therefore called
         * very often (typical assembling iterate through a wide range of local finite element spaces).
         *
         */
        void SetLocalFEltSpace(const LocalFEltSpace& local_felt_space);

        //! Gain access to the elementary data.
        const elementary_data_type& GetElementaryData() const noexcept;

        //! Gain non constant access to the elementary data.
        elementary_data_type& GetNonCstElementaryData() noexcept;

        //! Constant access to the parameter.
        const param_at_quad_pt_type& GetParameter() const noexcept;

        //! Non constant access to the parameter.
        param_at_quad_pt_type& GetNonCstParameter();

      protected:
        //! Return the Unknown and its associated numbering subset.
        const ExtendedUnknown::const_shared_ptr& GetExtendedUnknownPtr() const;

        //! Return the Unknown and its associated numbering subset.
        const ExtendedUnknown& GetExtendedUnknown() const;

      private:
        //! ElementaryData (matrices and/or vectors used for elementary calculation).
        elementary_data_type elementary_data_;

        //! Parameter on which the operator is defined.
        param_at_quad_pt_type& parameter_;

        //! Unknown/numbering subset.
        const ExtendedUnknown::const_shared_ptr extended_unknown_;
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_PARAMETEROPERATOR_LOCALPARAMETEROPERATOR_LOCALPARAMETEROPERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
