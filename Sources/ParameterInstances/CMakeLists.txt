include(${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake)

get_property(current_module_files TARGET ${MOREFEM_PARAM_INSTANCES} PROPERTY SOURCES)
source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}" FILES ${current_module_files})