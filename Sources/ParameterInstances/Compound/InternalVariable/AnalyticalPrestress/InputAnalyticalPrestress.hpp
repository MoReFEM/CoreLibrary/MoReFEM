// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_ANALYTICALPRESTRESS_INPUTANALYTICALPRESTRESS_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_ANALYTICALPRESTRESS_INPUTANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/Parameter/AnalyticalPrestress/AnalyticalPrestress.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"


namespace MoReFEM::Advanced::ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    /*!
     * \brief Policy to use when \a InputAnalyticalPrestress is involved in \a
     * SecondPiolaKirchhoffStressTensor.
     *
     * \todo #9 (Gautier) Explain difference with AnalyticalPrestress.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct InputAnalyticalPrestress final
    {
      public:
        //! \copydoc doxygen_hide_alias_self
        using self = InputAnalyticalPrestress<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT>;

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         *
         * \copydoc doxygen_hide_parameter_domain_arg
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit InputAnalyticalPrestress(const MoReFEMDataT& morefem_data, const Domain& domain);

      public:
        //! Constant accessor on contractility.
        const scalar_parameter_type& GetContractility() const noexcept;

        //! Constant accessor on the initial value of the active stress.
        double GetInitialValueInternalVariable() const noexcept;

      private:
        //! Contracitility.
        typename scalar_parameter_type::unique_ptr contractility_ = nullptr;

        //! Initial value of the active stress.
        const double initial_value_internal_variable_;
    };


} // namespace MoReFEM::Advanced::ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_ANALYTICALPRESTRESS_INPUTANALYTICALPRESTRESS_DOT_HPP_
// *** MoReFEM end header guards *** < //
