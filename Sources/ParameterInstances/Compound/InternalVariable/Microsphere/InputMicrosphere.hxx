// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_MICROSPHERE_INPUTMICROSPHERE_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_MICROSPHERE_INPUTMICROSPHERE_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"
// *** MoReFEM header guards *** < //


// IWYU pragma: no_include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/Exceptions/InputMicrosphere.hpp"
#include "ParameterInstances/Compound/InternalVariable/Microsphere/Internal/IsDefined.hpp" // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void InputMicrosphere<TimeManagerT>::Print(std::ostream& stream) const
    {
        stream << "Values of parameters (assumed constant to be able to call the current method) are:" << std::endl;


        if (IsInPlaneFiberDispersionI4())
        {
            stream << "\t- InPlaneFiberDispersionI4 = " << GetInPlaneFiberDispersionI4().GetConstantValue()
                   << std::endl;
            stream << "\t- OutOfPlaneFiberDispersionI4 = " << GetOutOfPlaneFiberDispersionI4().GetConstantValue()
                   << std::endl;
            stream << "\t- FiberStiffnessDensityI4 = " << GetFiberStiffnessDensityI4().GetConstantValue() << std::endl;
            stream << "\t- InPlaneFiberDispersionI6 = " << GetInPlaneFiberDispersionI6().GetConstantValue()
                   << std::endl;
            stream << "\t- OutOfPlaneFiberDispersionI6 = " << GetOutOfPlaneFiberDispersionI6().GetConstantValue()
                   << std::endl;
            stream << "\t- FiberStiffnessDensityI6 = " << GetFiberStiffnessDensityI6().GetConstantValue() << std::endl;
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    InputMicrosphere<TimeManagerT>::InputMicrosphere(const MoReFEMDataT& morefem_data,
                                                     const Domain& domain,
                                                     const QuadratureRulePerTopology& quadrature_rule_per_topology)
    : domain_(domain), quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {

        using MicrosphereIP = InputDataNS::Microsphere;

        if constexpr (
            Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::InPlaneFiberDispersionI4>()
            || Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::OutOfPlaneFiberDispersionI4>()
            || Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::FiberStiffnessDensityI4>()
            || Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::InPlaneFiberDispersionI6>()
            || Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::InPlaneFiberDispersionI6>()
            || Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::FiberStiffnessDensityI6>())
        {
            static_assert(
                Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::InPlaneFiberDispersionI4>()
                    && Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::InPlaneFiberDispersionI4>()
                    && Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::FiberStiffnessDensityI4>()
                    && Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::InPlaneFiberDispersionI6>()
                    && Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT,
                                                               MicrosphereIP::OutOfPlaneFiberDispersionI6>()
                    && Internal::InputMicrosphereNS::IsDefined<MoReFEMDataT, MicrosphereIP::FiberStiffnessDensityI6>(),
                "It makes no sense to define one and not the others");

            std::get<0>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::InPlaneFiberDispersionI4>(
                "InPlaneFiberDispersionI4", domain, morefem_data);

            std::get<1>(input_microsphere_) =
                InitScalarParameterFromInputData<MicrosphereIP::OutOfPlaneFiberDispersionI4>(
                    "OutOfPlaneFiberDispersionI4", domain, morefem_data);

            std::get<2>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::FiberStiffnessDensityI4>(
                "FiberStiffnessDensityI4", domain, morefem_data);

            std::get<3>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::InPlaneFiberDispersionI6>(
                "InPlaneFiberDispersionI6", domain, morefem_data);

            std::get<4>(input_microsphere_) =
                InitScalarParameterFromInputData<MicrosphereIP::OutOfPlaneFiberDispersionI6>(
                    "OutOfPlaneFiberDispersionI6", domain, morefem_data);

            std::get<5>(input_microsphere_) = InitScalarParameterFromInputData<MicrosphereIP::FiberStiffnessDensityI6>(
                "FiberStiffnessDensityI6", domain, morefem_data);
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const Domain& InputMicrosphere<TimeManagerT>::GetDomain() const noexcept
    {
        return domain_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const QuadratureRulePerTopology&
    InputMicrosphere<TimeManagerT>::GetQuadratureRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto InputMicrosphere<TimeManagerT>::GetInPlaneFiberDispersionI4() const -> const scalar_parameter_type&
    {
        if (!IsInPlaneFiberDispersionI4())
            throw InputMicrosphereNS::UndefinedData("InPlaneFiberDispersionI4");

        decltype(auto) ptr = std::get<0>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto InputMicrosphere<TimeManagerT>::GetOutOfPlaneFiberDispersionI4() const -> const scalar_parameter_type&
    {
        if (!IsOutOfPlaneFiberDispersionI4())
            throw InputMicrosphereNS::UndefinedData("OutOfPlaneFiberDispersionI4");

        decltype(auto) ptr = std::get<1>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto InputMicrosphere<TimeManagerT>::GetFiberStiffnessDensityI4() const -> const scalar_parameter_type&
    {
        if (!IsFiberStiffnessDensityI4())
            throw InputMicrosphereNS::UndefinedData("FiberStiffnessDensityI4");

        decltype(auto) ptr = std::get<2>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto InputMicrosphere<TimeManagerT>::GetInPlaneFiberDispersionI6() const -> const scalar_parameter_type&
    {
        if (!IsInPlaneFiberDispersionI6())
            throw InputMicrosphereNS::UndefinedData("InPlaneFiberDispersionI6");

        decltype(auto) ptr = std::get<3>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto InputMicrosphere<TimeManagerT>::GetOutOfPlaneFiberDispersionI6() const -> const scalar_parameter_type&
    {
        if (!IsOutOfPlaneFiberDispersionI6())
            throw InputMicrosphereNS::UndefinedData("OutOfPlaneFiberDispersionI6");

        decltype(auto) ptr = std::get<4>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto InputMicrosphere<TimeManagerT>::GetFiberStiffnessDensityI6() const -> const scalar_parameter_type&
    {
        if (!IsFiberStiffnessDensityI6())
            throw InputMicrosphereNS::UndefinedData("FiberStiffnessDensityI6");

        decltype(auto) ptr = std::get<5>(input_microsphere_);
        assert(!(!ptr));
        return *ptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool InputMicrosphere<TimeManagerT>::IsInPlaneFiberDispersionI4() const noexcept
    {
        return std::get<0>(input_microsphere_) != nullptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool InputMicrosphere<TimeManagerT>::IsOutOfPlaneFiberDispersionI4() const noexcept
    {
        return std::get<1>(input_microsphere_) != nullptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool InputMicrosphere<TimeManagerT>::IsFiberStiffnessDensityI4() const noexcept
    {
        return std::get<2>(input_microsphere_) != nullptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool InputMicrosphere<TimeManagerT>::IsInPlaneFiberDispersionI6() const noexcept
    {
        return std::get<3>(input_microsphere_) != nullptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool InputMicrosphere<TimeManagerT>::IsOutOfPlaneFiberDispersionI6() const noexcept
    {
        return std::get<4>(input_microsphere_) != nullptr;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool InputMicrosphere<TimeManagerT>::IsFiberStiffnessDensityI6() const noexcept
    {
        return std::get<5>(input_microsphere_) != nullptr;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_MICROSPHERE_INPUTMICROSPHERE_DOT_HXX_
// *** MoReFEM end header guards *** < //
