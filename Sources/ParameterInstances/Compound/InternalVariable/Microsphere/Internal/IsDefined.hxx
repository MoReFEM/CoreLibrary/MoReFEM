// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_MICROSPHERE_INTERNAL_ISDEFINED_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_MICROSPHERE_INTERNAL_ISDEFINED_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Compound/InternalVariable/Microsphere/Internal/IsDefined.hpp"
// *** MoReFEM header guards *** < //


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/Exceptions/InputMicrosphere.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InputMicrosphereNS
{


    template<::MoReFEM::Concept::InputDataOrModelSettingsType DataT, class T>
    constexpr bool IsDefinedHelper()
    {
        if constexpr (Utilities::Tuple::IndexOf<T, typename DataT::underlying_tuple_type>::value
                      != NumericNS::UninitializedIndex<std::size_t>())
            return true;

        // If InputDataNS::Solid is defined, ALL parameters are defined and true should be returned!
        return (
            Utilities::Tuple::IndexOf<::MoReFEM::InputDataNS::Microsphere, typename DataT::underlying_tuple_type>::value
            != NumericNS::UninitializedIndex<std::size_t>());
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, class T>
    constexpr bool IsDefined()
    {
        // Check first in input_data_type
        {
            using input_data_type = typename MoReFEMDataT::input_data_type;

            constexpr auto is_defined_in_input_data = IsDefinedHelper<input_data_type, T>();

            if constexpr (is_defined_in_input_data)
                return true;
        }

        // Then in model_settings_type
        {
            using model_settings_type = typename MoReFEMDataT::model_settings_type;

            return IsDefinedHelper<model_settings_type, T>();
        }
    }


} // namespace MoReFEM::Internal::InputMicrosphereNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_MICROSPHERE_INTERNAL_ISDEFINED_DOT_HXX_
// *** MoReFEM end header guards *** < //
