// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <sstream>
#include <string>

#include "ParameterInstances/Compound/Solid/Exceptions/Solid.hpp"


namespace // anonymous
{


    std::string UndefinedDataMsg(const std::string& data_name);


} // namespace


namespace MoReFEM::SolidNS
{


    UndefinedData::UndefinedData(const std::string& data, const std::source_location location)
    : ::MoReFEM::Exception(UndefinedDataMsg(data), location)
    { }


    UndefinedData::~UndefinedData() = default;


} // namespace MoReFEM::SolidNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string UndefinedDataMsg(const std::string& data_name)
    {
        std::ostringstream oconv;
        oconv << "Data '" << data_name
              << "' is undefined: in all likelihood either the model is ill-formed and the "
                 "data is not in the InputData, or it is but in the Lua file the chosen nature was 'ignore'.";
        std::string ret = oconv.str();
        return ret;
    }


} // namespace
