// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_EXCEPTIONS_SOLID_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_EXCEPTIONS_SOLID_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::SolidNS
{


    //! Exception thrown when access to an undefined data is attempted.
    class UndefinedData : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] data Name of the data concerned, e.g. 'volumic mass'.
         * \copydoc doxygen_hide_source_location
         */
        explicit UndefinedData(const std::string& data,
                               const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~UndefinedData() override;

        //! \copydoc doxygen_hide_copy_constructor
        UndefinedData(const UndefinedData& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UndefinedData(UndefinedData&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UndefinedData& operator=(const UndefinedData& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UndefinedData& operator=(UndefinedData&& rhs) = default;
    };


} // namespace MoReFEM::SolidNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_EXCEPTIONS_SOLID_DOT_HPP_
// *** MoReFEM end header guards *** < //
