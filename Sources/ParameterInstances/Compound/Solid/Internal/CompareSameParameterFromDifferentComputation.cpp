// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cmath>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include "ParameterInstances/Compound/Solid/Internal/CompareSameParameterFromDifferentComputation.hpp"

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM::Internal::SolidNS
{


    CompareSameParameterFromDifferentComputation::CompareSameParameterFromDifferentComputation(
        std::string&& original_name,
        std::string&& recomputed_from,
        double tolerance)
    : original_name_(std::move(original_name)), recomputed_from_(std::move(recomputed_from)), tolerance_(tolerance)
    { }


    void CompareSameParameterFromDifferentComputation::Perform(double original, double recomputed)
    {
        if (NumericNS::AreEqual(original, recomputed))
            return;

        std::ostringstream oconv;
        oconv << "Inconsistency in Solid: parameters aren't self consistent:\n\n";

        // If unequal, it might be due to numerical precision. So if relative error is small enough, just print
        // a warning on screen.
        if (!NumericNS::IsZero(original))
        {
            const double relative_difference = std::fabs(original - recomputed) / original;

            if (relative_difference > tolerance_)
            {
                oconv << "\t- " << original_name_ << " read in file is " << original << " but if recomputed from "
                      << recomputed_from_ << " we obtain " << recomputed << " (difference is " << recomputed - original
                      << "; relative difference is " << relative_difference * 100. << "%).\n";
                throw Exception(oconv.str());
            }

            if (!was_warning_already_printed_)
            {
                std::cout << "===========================================\n";
                std::cout << "[WARNING] A slight inconsistency was found in Solid parameters:\n\n";

                std::cout << "\t- " << original_name_ << " read in file is " << original << " but if recomputed from "
                          << recomputed_from_ << " we obtain " << recomputed << " (difference is "
                          << recomputed - original << "; relative difference is " << relative_difference * 100.
                          << "%).\n";
                std::cout << "\t  Difference is deemed small enough that the computation may proceed." << '\n';
                std::cout
                    << "\t  This warning won't be issued again if another small discrepancy for the same set "
                       "of parameters is found; a huge one would of course trigger the abortion of the program.\n";
                std::cout << "===========================================" << '\n';

                was_warning_already_printed_ = true;
            }
        } else
        {
            const double difference = std::fabs(original - recomputed);

            if (difference > tolerance_)
            {
                oconv << "\t- " << original_name_ << " read in file is " << original << " but if recomputed from "
                      << recomputed_from_ << " we obtain " << recomputed << " (difference is " << recomputed - original
                      << ").\n";
                throw Exception(oconv.str());
            }

            if (!was_warning_already_printed_)
            {
                std::cout << "===========================================\n";
                std::cout << "[WARNING] A slight inconsistency was found in Solid parameters:\n\n";

                std::cout << "\t- " << original_name_ << " read in file is " << original << " but if recomputed from "
                          << recomputed_from_ << " we obtain " << recomputed << " (difference is "
                          << recomputed - original << ").\n";
                std::cout << "\t  Difference is deemed small enough that the computation may proceed." << '\n';
                std::cout
                    << "\t  This warning won't be issued again if another small discrepancy for the same set "
                       "of parameters is found; a huge one would of course trigger the abortion of the program.\n";
                std::cout << "===========================================" << '\n';

                was_warning_already_printed_ = true;
            }
        }
    }


} // namespace MoReFEM::Internal::SolidNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
