// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_INTERNAL_COMPARESAMEPARAMETERFROMDIFFERENTCOMPUTATION_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_INTERNAL_COMPARESAMEPARAMETERFROMDIFFERENTCOMPUTATION_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <memory>
#include <string>


namespace MoReFEM::Internal::SolidNS
{


    /*!
     * \brief Compare a parameter as given directly in the input file to the same recomputed from other parameters.
     *
     * For instance compare Young modulus to the one recomputed from Lame parameters.
     *
     * There are actually two checks done: first is to compare directly the numerical values and if they
     * do not match two outcomes are possible:
     * - If the relative difference between both is small, a simple warning is issued.
     * - If not, an exception that should terminate the program is thrown.
     *
     * If they match nothing is done.
     *
     */
    class CompareSameParameterFromDifferentComputation
    {
      public:
        //! Alias to self.
        using self = CompareSameParameterFromDifferentComputation;

        //! Convenient alias.
        using unique_ptr = std::unique_ptr<self>;

        /*!
         * \brief Constructor.
         *
         * \param[in] original_name Name of the value which is checked by the facility.
         * \param[in] recomputed_from Indicates the components from which recomputation may occur.
         * \param[in] tolerance Relative tolerance below which s a slight discrepancy may be given a pass.
         */
        CompareSameParameterFromDifferentComputation(std::string&& original_name,
                                                     std::string&& recomputed_from,
                                                     double tolerance);

        /*!
         * \brief The method that does the actual check onto the two provided values.
         *
         * \param[in] original Value that is kept for the material parameter
         * \param[in] recomputed Checked value obtained through recomputation of alternate solid material components.
         */
        void Perform(double original, double recomputed);

      private:
        //! Name of the parameter begin compared.
        std::string original_name_;

        //! Names of the parameters from which the recomputation was done.
        std::string recomputed_from_;

        //! Relative tolerance under which discrepancies may pass.
        const double tolerance_;

        //! Whether there was already a warning printed for very small discrepancy.
        bool was_warning_already_printed_ = false;
    };


} // namespace MoReFEM::Internal::SolidNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_INTERNAL_COMPARESAMEPARAMETERFROMDIFFERENTCOMPUTATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
