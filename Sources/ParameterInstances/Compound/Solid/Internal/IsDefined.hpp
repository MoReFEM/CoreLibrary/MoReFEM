// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_INTERNAL_ISDEFINED_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_INTERNAL_ISDEFINED_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "ParameterInstances/Compound/Solid/Exceptions/Solid.hpp"
#include "ParameterInstances/Compound/Solid/Internal/IsDefined.hpp"


namespace MoReFEM::Internal::SolidNS
{

    template<::MoReFEM::Concept::InputDataOrModelSettingsType InputDataT, class T>
    constexpr bool IsDefined();

    /*!
     * \brief Check whether type \a T is defined within \a DataT.
     *
     * This function is used as helper for \a IsDefined() that checks both \a InputData and \a ModelSettings.
     *
     * \return True if \a T was found directly or indirectly (within a \a ::MoReFEM::InputDataNS::Solid section)
     */
    template<::MoReFEM::Concept::InputDataOrModelSettingsType DataT, class T>
    constexpr bool IsDefinedHelper();


    /*!
     * \brief Check whether type \a T is defined within \a MoReFEMDataT::input_data_type
     * or \a MoReFEMDataT::model_settings_type
     *
     * \return True if \a T was found directly or indirectly (within a \a ::MoReFEM::InputDataNS::Solid section)
     * in either \a MoReFEMDataT::input_data_type or \a MoReFEMDataT::model_settings_type.
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, class T>
    constexpr bool IsDefined();


} // namespace MoReFEM::Internal::SolidNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Compound/Solid/Internal/IsDefined.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_INTERNAL_ISDEFINED_DOT_HPP_
// *** MoReFEM end header guards *** < //
