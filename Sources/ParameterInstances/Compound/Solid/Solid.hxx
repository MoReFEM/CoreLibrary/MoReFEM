// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_SOLID_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_SOLID_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Compound/Solid/Solid.hpp"
// *** MoReFEM header guards *** < //


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "ParameterInstances/Compound/Solid/Exceptions/Solid.hpp"
#include "ParameterInstances/Compound/Solid/Internal/CompareSameParameterFromDifferentComputation.hpp"
#include "ParameterInstances/Compound/Solid/Internal/IsDefined.hpp" // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    Solid<TimeManagerT>::Solid(const MoReFEMDataT& morefem_data,
                               const Domain& domain,
                               const QuadratureRulePerTopology& quadrature_rule_per_topology,
                               const double relative_tolerance)
    : domain_(domain), quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {

        using SolidIP = InputDataNS::Solid;

        volumic_mass_ =
            InitScalarParameterFromInputData<InputDataNS::Solid::VolumicMass>("Volumic mass", domain, morefem_data);

        if constexpr ((Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameLambda>())
                      || (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameMu>()))
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameLambda>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameMu>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(lame_coeff_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::LameLambda>("Lame lambda", domain, morefem_data);
            std::get<1>(lame_coeff_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::LameMu>("Lame mu", domain, morefem_data);
        }

        if constexpr ((Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::YoungModulus>())
                      || (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::PoissonRatio>()))
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::PoissonRatio>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::YoungModulus>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(young_poisson_) = InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>(
                "Young modulus", domain, morefem_data);


            std::get<1>(young_poisson_) = InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>(
                "Poisson ratio", domain, morefem_data);
        }

        if constexpr ((Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa1>())
                      || (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa2>()))
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa1>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa2>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(kappa_list_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Kappa1>("Kappa_1", domain, morefem_data);

            std::get<1>(kappa_list_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Kappa2>("Kappa_2", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::HyperelasticBulk>())
        {
            hyperelastic_bulk_ = InitScalarParameterFromInputData<InputDataNS::Solid::HyperelasticBulk>(
                "Hyperelastic bulk", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Viscosity>())
        {
            viscosity_ =
                InitScalarParameterFromInputData<InputDataNS::Solid::Viscosity>("Viscosity", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu1>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C0>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C1>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C2>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C3>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu2>())
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C0>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C1>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C3>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu1>(),
                          "It makes no sense to define one and not the others");

            std::get<0>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Mu1>("Mu1", domain, morefem_data);
            std::get<1>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Mu2>("Mu2", domain, morefem_data);
            std::get<2>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::C0>("C0", domain, morefem_data);

            std::get<3>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::C1>("C1", domain, morefem_data);

            std::get<4>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::C2>("C2", domain, morefem_data);

            std::get<5>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::C3>("C3", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C4>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C5>())
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C0>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C1>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C3>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C4>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C5>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu1>(),
                          "It makes no sense to define one and not the others");

            std::get<6>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::C4>("C4", domain, morefem_data);

            std::get<7>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::C5>("C5", domain, morefem_data);
        } // if constexpr C4 / C5

        if (relative_tolerance >= 0.)
            CheckConsistency(relative_tolerance);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const Domain& Solid<TimeManagerT>::GetDomain() const noexcept
    {
        return domain_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const QuadratureRulePerTopology& Solid<TimeManagerT>::GetQuadratureRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetVolumicMass() const -> const scalar_parameter_type&
    {
        if (!volumic_mass_)
            throw SolidNS::UndefinedData("Volumic mass");

        return *volumic_mass_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetHyperelasticBulk() const -> const scalar_parameter_type&
    {
        if (!IsHyperelasticBulk())
            throw SolidNS::UndefinedData("Hyperelastic bulk");

        assert(!(!hyperelastic_bulk_));
        return *hyperelastic_bulk_;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetKappa1() const -> const scalar_parameter_type&
    {
        if (!IsKappa1())
            throw SolidNS::UndefinedData("Kappa1");

        decltype(auto) ptr = std::get<0>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetKappa2() const -> const scalar_parameter_type&
    {
        if (!IsKappa2())
            throw SolidNS::UndefinedData("Kappa2");

        decltype(auto) ptr = std::get<1>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetYoungModulus() const -> const scalar_parameter_type&
    {
        if (!IsYoungModulus())
            throw SolidNS::UndefinedData("YoungModulus");

        decltype(auto) ptr = std::get<0>(young_poisson_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetPoissonRatio() const -> const scalar_parameter_type&
    {
        if (!IsPoissonRatio())
            throw SolidNS::UndefinedData("PoissonRatio");

        decltype(auto) ptr = std::get<1>(young_poisson_);
        assert(!(!ptr));
        return *ptr;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetLameLambda() const -> const scalar_parameter_type&
    {
        if (!IsLameLambda())
            throw SolidNS::UndefinedData("Lame lambda");

        decltype(auto) ptr = std::get<0>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetLameMu() const -> const scalar_parameter_type&
    {
        if (!IsLameMu())
            throw SolidNS::UndefinedData("Lame mu");

        decltype(auto) ptr = std::get<1>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetMu1() const -> const scalar_parameter_type&
    {
        if (!IsMu1())
            throw SolidNS::UndefinedData("Mu1");

        decltype(auto) ptr = std::get<0>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetMu2() const -> const scalar_parameter_type&
    {
        if (!IsMu2())
            throw SolidNS::UndefinedData("Mu2");

        decltype(auto) ptr = std::get<1>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetC0() const -> const scalar_parameter_type&
    {
        if (!IsC0())
            throw SolidNS::UndefinedData("C0");

        decltype(auto) ptr = std::get<2>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetC1() const -> const scalar_parameter_type&
    {
        if (!IsC1())
            throw SolidNS::UndefinedData("C1");

        decltype(auto) ptr = std::get<3>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetC2() const -> const scalar_parameter_type&
    {
        if (!IsC2())
            throw SolidNS::UndefinedData("C2");

        decltype(auto) ptr = std::get<4>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetC3() const -> const scalar_parameter_type&
    {
        if (!IsC3())
            throw SolidNS::UndefinedData("C3");

        decltype(auto) ptr = std::get<5>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetC4() const -> const scalar_parameter_type&
    {
        if (!IsC4())
            throw SolidNS::UndefinedData("C4");

        decltype(auto) ptr = std::get<6>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetC5() const -> const scalar_parameter_type&
    {
        if (!IsC5())
            throw SolidNS::UndefinedData("C5");

        decltype(auto) ptr = std::get<7>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline auto Solid<TimeManagerT>::GetViscosity() const -> const scalar_parameter_type&
    {
        if (!IsViscosity())
            throw SolidNS::UndefinedData("Viscosity");

        assert(!(!viscosity_));
        return *viscosity_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsHyperelasticBulk() const noexcept
    {
        return hyperelastic_bulk_ != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsKappa1() const noexcept
    {
        return std::get<0>(kappa_list_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsKappa2() const noexcept
    {
        return std::get<1>(kappa_list_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsYoungModulus() const noexcept
    {
        return std::get<0>(young_poisson_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsPoissonRatio() const noexcept
    {
        return std::get<1>(young_poisson_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsLameLambda() const noexcept
    {
        return std::get<0>(lame_coeff_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsLameMu() const noexcept
    {
        return std::get<1>(lame_coeff_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsMu1() const noexcept
    {
        return std::get<0>(mu_i_C_i_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsMu2() const noexcept
    {
        return std::get<1>(mu_i_C_i_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsC0() const noexcept
    {
        return std::get<2>(mu_i_C_i_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsC1() const noexcept
    {
        return std::get<3>(mu_i_C_i_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsC2() const noexcept
    {
        return std::get<4>(mu_i_C_i_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsC3() const noexcept
    {
        return std::get<5>(mu_i_C_i_) != nullptr;
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsC4() const noexcept
    {
        return std::get<6>(mu_i_C_i_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsC5() const noexcept
    {
        return std::get<7>(mu_i_C_i_) != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline bool Solid<TimeManagerT>::IsViscosity() const noexcept
    {
        return viscosity_ != nullptr;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Solid<TimeManagerT>::CheckConsistency(const double relative_tolerance) const
    {

        // ======================================
        // First check the obvious: material parameters go by pair...
        // ======================================

        const bool are_lame = IsLameLambda();

        if (are_lame != IsLameMu())
            throw Exception("Inconsistency in Solid: Lame coefficients should be either both defined or "
                            "both ignored.");

        const bool are_young_poisson = IsYoungModulus();

        if (are_young_poisson != IsPoissonRatio())
            throw Exception("Inconsistency in Solid: Young modulus and Poisson ratio should be either both "
                            "defined or both ignored.");

        const bool are_kappa_coeff = IsKappa1();

        if (are_kappa_coeff != IsKappa2())
            throw Exception("Inconsistency in Solid: kappa parameters should be either both "
                            "defined or both ignored.");

        if (are_kappa_coeff && !IsHyperelasticBulk())
            throw Exception("Inconsistency in Solid: if kappa coefficients are defined hyperelastic bulk "
                            "should be as well.");


        // ======================================
        // Now check numeric values at quadrature points are also consistent.
        // ======================================
        decltype(auto) domain = GetDomain();
        const auto& mesh = domain.GetMesh();

        decltype(auto) ref_geom_elt_list = mesh.template BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

        const double one_third = 1. / 3.;

        using CompareSameParameterFromDifferentComputation =
            Internal::SolidNS::CompareSameParameterFromDifferentComputation;

        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_lambda_and_young_poisson = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_mu_and_young_poisson = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_lambda_and_kappa = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_lame_mu_and_kappa = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_young_and_kappa = nullptr;
        CompareSameParameterFromDifferentComputation::unique_ptr compare_poisson_and_kappa = nullptr;

        if (are_lame && are_young_poisson)
        {
            compare_lame_lambda_and_young_poisson = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame lambda", "Young modulus and Poisson ratio", relative_tolerance);
            compare_lame_mu_and_young_poisson = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame mu", "Young modulus and Poisson ratio", relative_tolerance);
        }

        if (are_lame && are_kappa_coeff)
        {
            compare_lame_lambda_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame lambda", "Kappa parameters and hyperelastic bulk", relative_tolerance);

            compare_lame_mu_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Lame mu", "Kappa parameters", relative_tolerance);
        }

        if (are_young_poisson && are_kappa_coeff)
        {
            compare_young_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Young modulus", "Kappa parameters and hyperelastic bulk", relative_tolerance);

            compare_poisson_and_kappa = std::make_unique<CompareSameParameterFromDifferentComputation>(
                "Poisson ratio", "Kappa parameters and hyperelastic bulk", relative_tolerance);
        }

        const auto& quadrature_rule_per_topology = GetQuadratureRulePerTopology();

        for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
        {
            assert(!(!ref_geom_elt_ptr));
            const auto& ref_geom_elt = *ref_geom_elt_ptr;
            const auto topology_type = ref_geom_elt.GetTopologyIdentifier();

            decltype(auto) quadrature_rule = quadrature_rule_per_topology.GetRule(topology_type);
            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            decltype(auto) geom_elt_range =
                mesh.template GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(*ref_geom_elt_ptr);

            for (auto it = geom_elt_range.first; it != geom_elt_range.second; ++it)
            {
                const auto& geom_elt_ptr = *it;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                if (!domain.IsGeometricEltInside(geom_elt))
                    continue;

                for (const auto& quad_pt_ptr : quad_pt_list)
                {
                    assert(!(!quad_pt_ptr));
                    const auto& quad_pt = *quad_pt_ptr;

                    if (are_lame && are_young_poisson)
                    {
                        const double young = GetYoungModulus().GetValue(quad_pt, geom_elt);
                        const double poisson = GetPoissonRatio().GetValue(quad_pt, geom_elt);
                        const double lame_lambda = GetLameLambda().GetValue(quad_pt, geom_elt);
                        const double lame_mu = GetLameMu().GetValue(quad_pt, geom_elt);

                        const double lame_lbd_from_yp = young * poisson / ((1. + poisson) * (1. - 2. * poisson));
                        const double lame_mu_from_yp = young * 0.5 / (1. + poisson);

                        assert(!(!compare_lame_lambda_and_young_poisson));
                        compare_lame_lambda_and_young_poisson->Perform(lame_lambda, lame_lbd_from_yp);

                        assert(!(!compare_lame_mu_and_young_poisson));
                        compare_lame_mu_and_young_poisson->Perform(lame_mu, lame_mu_from_yp);
                    }

                    if (are_lame && are_kappa_coeff)
                    {
                        const double lame_lambda = GetLameLambda().GetValue(quad_pt, geom_elt);
                        const double lame_mu = GetLameMu().GetValue(quad_pt, geom_elt);

                        const double kappa_1 = GetKappa1().GetValue(quad_pt, geom_elt);
                        const double kappa_2 = GetKappa2().GetValue(quad_pt, geom_elt);

                        const double bulk = GetHyperelasticBulk().GetValue(quad_pt, geom_elt);

                        const double lame_mu_from_kappa = 2. * (kappa_1 + kappa_2);
                        const double lame_lbd_from_kappa_and_bulk = -4. * one_third * (kappa_1 + kappa_2) + bulk;

                        assert(!(!compare_lame_lambda_and_kappa));
                        compare_lame_lambda_and_kappa->Perform(lame_lambda, lame_lbd_from_kappa_and_bulk);

                        assert(!(!compare_lame_mu_and_kappa));
                        compare_lame_mu_and_kappa->Perform(lame_mu, lame_mu_from_kappa);
                    }


                    if (are_young_poisson && are_kappa_coeff && !are_lame) // the latter because if all are defined
                                                                           // tests above are already sufficient.
                    {
                        const double kappa_sum =
                            GetKappa1().GetValue(quad_pt, geom_elt) + GetKappa2().GetValue(quad_pt, geom_elt);
                        const double bulk = GetHyperelasticBulk().GetValue(quad_pt, geom_elt);

                        const double young = GetYoungModulus().GetValue(quad_pt, geom_elt);
                        const double poisson = GetPoissonRatio().GetValue(quad_pt, geom_elt);

                        const double young_from_kappa_and_bulk =
                            6. * bulk * kappa_sum / (bulk + 2. * one_third * kappa_sum);

                        const double poisson_from_kappa_and_bulk =
                            .5 * (-4. * one_third * kappa_sum + bulk) / (bulk + 2. * one_third * kappa_sum);

                        assert(!(!compare_young_and_kappa));
                        compare_young_and_kappa->Perform(young, young_from_kappa_and_bulk);

                        assert(!(!compare_poisson_and_kappa));
                        compare_poisson_and_kappa->Perform(poisson, poisson_from_kappa_and_bulk);
                    }
                }
            }
        }
    }

    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Solid<TimeManagerT>::Print(std::ostream& stream) const
    {
        stream << "Values of parameters (assumed constant to be able to call the current method) are:" << std::endl;

        stream << "\t- Volumic mass = " << GetVolumicMass().GetConstantValue() << std::endl;

        if (IsYoungModulus())
        {
            stream << "\t- Young modulus = " << GetYoungModulus().GetConstantValue() << std::endl;
            stream << "\t- Poisson ratio = " << GetPoissonRatio().GetConstantValue() << std::endl;
        }

        if (IsLameLambda())
        {
            stream << "\t- Lame lambda = " << GetLameLambda().GetConstantValue() << std::endl;
            stream << "\t- Lame mu = " << GetLameMu().GetConstantValue() << std::endl;
        }

        if (IsKappa1())
        {
            stream << "\t- Kappa1 = " << GetKappa1().GetConstantValue() << std::endl;
            stream << "\t- Kappa2 = " << GetKappa2().GetConstantValue() << std::endl;
        }

        if (IsHyperelasticBulk())
            stream << "\t- Hyperelastic bulk = " << GetHyperelasticBulk().GetConstantValue() << std::endl;

        if (IsMu1())
        {
            stream << "\t- Mu1 = " << GetMu1().GetConstantValue() << std::endl;
            stream << "\t- Mu2 = " << GetMu2().GetConstantValue() << std::endl;
            stream << "\t- C0 = " << GetC0().GetConstantValue() << std::endl;
            stream << "\t- C1 = " << GetC1().GetConstantValue() << std::endl;
            stream << "\t- C2 = " << GetC2().GetConstantValue() << std::endl;
            stream << "\t- C3 = " << GetC3().GetConstantValue() << std::endl;
            stream << "\t- C4 = " << GetC4().GetConstantValue() << std::endl;
            stream << "\t- C5 = " << GetC5().GetConstantValue() << std::endl;
        }
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_SOLID_SOLID_DOT_HXX_
// *** MoReFEM end header guards *** < //
