// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_THREEDIMENSIONALPARAMETER_THREEDIMENSIONALCOMPOUNDPARAMETER_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_THREEDIMENSIONALPARAMETER_THREEDIMENSIONALCOMPOUNDPARAMETER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/TimeDependency.hpp"


namespace MoReFEM::ParameterNS
{


    /*!
     * \brief Class to handle a 3D parameter (for instance a force).
     *
     * Such objects should in most if not all cases be initialized with InitThreeDimensionalCoumpoundParameter() free
     * function - that's the reason this class is in \a Advanced namespace.
     *
     * This is actually an aggregate of three \a Parameter that define current one.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT>
    class ThreeDimensionalCoumpoundParameter final : public VectorialParameter<TimeManagerT, TimeDependencyT>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

        //! Alias to base class.
        using parent = VectorialParameter<TimeManagerT, TimeDependencyT>;

        //! Alias to return type.
        using return_type = typename parent::return_type;

        //! Alias
        using value_type = typename parent::value_type;

        //! Alias to traits of parent class.
        using traits = typename parent::traits;

        //! Alias to scalar parameter. Template chosen is not a mistake: time dependency is handled at the \a
        //! ThreeDimensionalCoumpoundParameter level, not in each scalar parameter.
        using scalar_parameter_no_time_dep = ScalarParameter<TimeManagerT, TimeDependencyNS::None>;

        //! Alias to scalar parameter unique_ptr.
        using scalar_parameter_no_time_dep_ptr = typename scalar_parameter_no_time_dep::unique_ptr;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] name Name of the parameter.
         * \param[in] x_component The scalar \a Parameter that represents the x component.
         * \param[in] y_component The scalar \a Parameter that represents the y component.
         * \param[in] z_component The scalar \a Parameter that represents the z component.
         */
        template<class T>
        explicit ThreeDimensionalCoumpoundParameter(T&& name,
                                                    scalar_parameter_no_time_dep_ptr&& x_component,
                                                    scalar_parameter_no_time_dep_ptr&& y_component,
                                                    scalar_parameter_no_time_dep_ptr&& z_component);

        //! Destructor.
        ~ThreeDimensionalCoumpoundParameter() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        ThreeDimensionalCoumpoundParameter(const ThreeDimensionalCoumpoundParameter& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ThreeDimensionalCoumpoundParameter(ThreeDimensionalCoumpoundParameter&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ThreeDimensionalCoumpoundParameter& operator=(const ThreeDimensionalCoumpoundParameter& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ThreeDimensionalCoumpoundParameter& operator=(ThreeDimensionalCoumpoundParameter&& rhs) = delete;

        ///@}

        /*!
         * \brief Write the content of the Parameter in a stream.

         * \copydoc doxygen_hide_stream_inout
         */
        void SupplWrite(std::ostream& stream) const override;

        //! \copydoc doxygen_hide_parameter_suppl_time_update
        void SupplTimeUpdate() override;

        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        void SupplTimeUpdate(double time) override;

        /*!
         *
         * \copydoc doxygen_hide_parameter_suppl_get_any_value
         */
        return_type SupplGetAnyValue() const override;

        /*!
         * \brief Enables to modify the constant value of a parameter. Disabled for this Parameter.
         */
        void SetConstantValue(value_type) override;

      private:
        //! \copydoc doxygen_hide_parameter_suppl_get_constant_value
        return_type SupplGetConstantValue() const override;

        //! \copydoc doxygen_hide_parameter_suppl_get_value_local_coords
        return_type SupplGetValue(const local_coords_type& local_coords, const GeometricElt& geom_elt) const override;

        //! Whether the parameter varies spatially or not.
        bool IsConstant() const override;


      private:
        //! Access to contribution of component x.
        scalar_parameter_no_time_dep& GetScalarParameterX() const;

        //! Access to contribution of component y.
        scalar_parameter_no_time_dep& GetScalarParameterY() const;

        //! Access to contribution of component z.
        scalar_parameter_no_time_dep& GetScalarParameterZ() const;


      private:
        //! Contribution of the x component to the vectorial parameter.
        scalar_parameter_no_time_dep_ptr scalar_parameter_x_;

        //! Contribution of the y component to the vectorial parameter.
        scalar_parameter_no_time_dep_ptr scalar_parameter_y_;

        //! Contribution of the z component to the vectorial parameter.
        scalar_parameter_no_time_dep_ptr scalar_parameter_z_;

        //! Content of the parameter.
        mutable value_type content_;
    };


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_THREEDIMENSIONALPARAMETER_THREEDIMENSIONALCOMPOUNDPARAMETER_DOT_HPP_
// *** MoReFEM end header guards *** < //
