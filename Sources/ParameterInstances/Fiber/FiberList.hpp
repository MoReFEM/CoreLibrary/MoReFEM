// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLIST_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "Core/Parameter/FiberEnum.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/ParameterAtDof.hpp"
#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"

#include "ParameterInstances/Fiber/Internal/FillGlobalVector.hpp"
#include "ParameterInstances/Fiber/Internal/ReadFiberFile.hpp"
#include "ParameterInstances/Fiber/Internal/ReadFiberFileAtQuadPt.hpp"
#include "ParameterInstances/Fiber/UniqueId.hpp"
#include "ParameterInstances/FromParameterAtDof/FromParameterAtDof.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class Unknown; }


namespace MoReFEM::FiberNS
{
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    class FiberListManager;
}

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{

    /*!
     * \brief Read from an Ensight file values given at dofs and interpret then in term of a Parameter object.
     *
     * \attention It is assumed here a P1 mesh is considered!
     *
     * \tparam PolicyT Whether the fiber file was defined at the nodes of the mesh or at the quadrature points.
     * \tparam TypeT Whether a scalar or vectorial parameter is to be considered.
     */
    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>
    >
    class FiberList final
    : public Crtp::UniqueId<FiberList<FiberPolicyT, TypeT, TimeManagerT>, FiberListNS::unique_id, UniqueIdNS::AssignationMode::manual>,
    public Parameter<TypeT, QuadraturePoint, TimeManagerT, TimeDependencyT, StorageT>
    // clang-format on
    {

      public:
        static_assert(TypeT != ParameterNS::Type::matrix, "Current implementation can't deal with matrix parameters.");

        //! \copydoc doxygen_hide_alias_self
        using self = FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = QuadraturePoint;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to parent.
        using parent = Parameter<TypeT, QuadraturePoint, TimeManagerT, TimeDependencyT, StorageT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to underlying parameter.
        using parameter_type = ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT, StorageT>;

        //! 'Inherit' alias from parent.
        using return_type = typename parent::return_type;

        //! 'Inherit' alias from parent.
        using value_type = typename parent::value_type;

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend FiberNS::FiberListManager<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>;
        //! Friendship to FiberListManager, only class able to build a FiberList object.
        // Excluded as Doxygen doesn't handle it well.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Return the name of the class.
        static const std::string& ClassName();

      private:
        //! Alias to unique id parent.
        using unique_id_parent = Crtp::UniqueId<self, FiberListNS::unique_id, UniqueIdNS::AssignationMode::manual>;

      private:
        //! Alias to the way each value is stored.
        using storage_value_type = std::decay_t<return_type>;

        //! Alias to value holder.
        using value_holder_type = Internal::ParameterNS::AtQuadraturePointNS::ValueHolder<storage_value_type>;

        /*!
         * \brief Alias to the type of the value actually stored.
         *
         * Key is the unique identifier of a \a GeometricElement.
         * Index of the vector is the quadrature point unique id.
         */
        // clang-format off
        using storage_type =
            std::unordered_map
            <
                std::size_t,
                std::vector<value_holder_type>
            >;
        // clang-format on

      private:
        /// \name Special members.
        ///@{


        /*!
         *
         * \brief Constructor.
         *
         * \param[in] unique_id Unique id of the FilberList as read in the input file.
         * \param[in] fiber_file File at the Ensight format which will be interpreted. Its format is specified by
         * Ensight specifications;  typically scalar files get a 'scl' extension and their first line is 'Scalar per
         * node' whereas vectorial get a 'vct' extension and first line is 'Vector per node'.
         * \copydoc doxygen_hide_parameter_domain_arg
         *
         * \param[in] felt_space Finite element space upon which the Parameter should be defined.
         * \param[in] unknown A scalar or vectorial unknown that acts a bit as a strawman: dofs are defined only
         * in relationship to an unknown, so you must create one if none fulfill your purposes (for instance
         * if you deal with a vectorial unknown and need a scalar Dof field, you must create another unknown
         * only for the Parameter. To save space, it's better if this unknown is in its own numbering subset,
         * but this is not mandatory. Unknown should be scalar for TypeT == ParameterNS::Type::scalar and vectorial
         * for TypeT == ParameterNS::Type::vectorial.
         * \copydetails doxygen_hide_time_manager_arg
         *
         */
        explicit FiberList(FiberListNS::unique_id unique_id,
                           const FilesystemNS::File& fiber_file,
                           const Domain& domain,
                           const FEltSpace& felt_space,
                           const TimeManagerT& time_manager,
                           const Unknown& unknown);

      public:
        //! Destructor.
        ~FiberList() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        FiberList(const FiberList& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FiberList(FiberList&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FiberList& operator=(const FiberList& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FiberList& operator=(FiberList&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Initialize by converting into a \a ParameterAtQuadraturePoint.
         *
         * \param[in] quadrature_rule_per_topology If specified, the quadrature rule to use for each topology. If
         * nullptr, just take the choices stored in the \a FEltSpace.
         */
        void Initialize(const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Whether the parameter is constant spatially (False for this one).
        bool IsConstant() const noexcept override final;

        //! Accessor to the global vector which entails the values at dof.
        const GlobalVector& GetGlobalVector() const noexcept;

        /*!
         * \brief Enables to modify the constant value of a parameter. Deactivated for this kind of \a Parameter.
         */
        void SetConstantValue(storage_value_type) override;

        //! Accesspor to underlying parameter.
        const parameter_type& GetUnderlyingParameter() const noexcept;


      private:
        //! \copydoc doxygen_hide_parameter_suppl_get_constant_value
        virtual return_type SupplGetConstantValue() const override final;

        //! \copydoc doxygen_hide_parameter_suppl_get_value_quad_pt
        virtual return_type SupplGetValue(const local_coords_type& quad_pt,
                                          const GeometricElt& geom_elt) const override final;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        virtual return_type SupplGetAnyValue() const override final;

        //! Write the content of the Parameter in a stream.
        //! \copydoc doxygen_hide_stream_inout
        virtual void SupplWrite(std::ostream& stream) const override final;

        //! \copydoc doxygen_hide_parameter_suppl_time_update
        virtual void SupplTimeUpdate() override final;

        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        virtual void SupplTimeUpdate(double time) override final;


      private:
        //! Non constant accessor to the global vector which entails the values at dof.
        GlobalVector& GetNonCstGlobalVector() noexcept;

        //! Accessor to the finite element space.
        const FEltSpace& GetFeltSpace() const noexcept;

        //! Accessor to the time manager.
        const TimeManagerT& GetTimeManager() const noexcept;

        //! Accessor to the unknown.
        const Unknown& GetUnknown() const noexcept;


      private:
        //! Finite element space to define the Parameter at Dof.
        const FEltSpace& felt_space_;

        //! Time manager
        const TimeManagerT& time_manager_;

        //! Unknown.
        const Unknown& unknown_;

        //! Parameter upon which most functionalities will be built.
        typename parameter_type::unique_ptr underlying_parameter_{ nullptr };

        //! Global vector which entails the values at dof.
        GlobalVector::unique_ptr global_vector_{ nullptr };

        //! Index of the first geometric element which belongs to the domain's parameter. Used to reset the index of the
        //  values read in the fiber files defined at quadrature points.
        ::MoReFEM::GeomEltNS::index_type first_index_program_wise_{ 0UL };

        //! Number of elements used to do a sanity check on the length of the fiber files defined at quadrature points.
        std::size_t Nprogram_wise_geom_elt = 0UL;

        //! Number of elements used to check that each processor read the right number of values out of the fiber files
        //  defined at quadrature points.
        std::size_t Nprocessor_wise_geom_elt = 0UL;

        //! Fiber file.
        FilesystemNS::File fiber_file_;

#ifndef NDEBUG
        //! In debug mode, data attribute to check \a Initialize is not called more than one.
        bool was_initialize_already_called_{ false };
#endif // NDEBUG
    };


    /*!
     * \brief Convert into the \a FiberList::unique_id strong type the enum value defined in the input data file.
     *
     * In a typical Model, indexes related to fields (e.g. FiberList) are defined in an enum class named typically \a
     * FiberListIndex. This facility is a shortcut to convert this enum into a proper \a FiberListNS::unique_id that is
     * used in the library to identify \a FiberList.
     *
     * \param[in] enum_value The index related to the \a FiberList, stored in an enum class. See a typical 'InputData.hpp' file
     * to understand it better.
     *
     * \return The identifier for a \a FiberList as used in the library.
     */
    template<class EnumT>
    constexpr FiberListNS::unique_id AsFiberListId(EnumT enum_value);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Fiber/FiberList.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
