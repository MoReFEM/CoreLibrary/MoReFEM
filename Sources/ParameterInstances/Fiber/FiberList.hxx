// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLIST_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLIST_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Fiber/FiberList.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <sstream>

// IWYU pragma: begin_exports
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"
// IWYU pragma: end_exports

#include "ParameterInstances/Fiber/UniqueId.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    const std::string& FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::ClassName()
    {
        static const std::string ret = std::string("FiberList<") + ::MoReFEM::ParameterNS::Name<TypeT>() + ">";
        return ret;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::FiberList(
        const FiberListNS::unique_id unique_id,
        const FilesystemNS::File& fiber_file,
        const Domain& domain,
        const FEltSpace& felt_space,
        const TimeManagerT& time_manager,
        const Unknown& unknown)
    : unique_id_parent(unique_id), parent("Fiber manager", domain), felt_space_(felt_space),
      time_manager_(time_manager), unknown_(unknown), fiber_file_(fiber_file)
    {
        assert(felt_space.GetDomain().GetUniqueId() == domain.GetUniqueId()
               && "Domain to create the parameter and domain used to define the finite element space should be "
                  "the same.");

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
        const auto& god_of_dof = *god_of_dof_ptr;

        const auto& numbering_subset = felt_space.GetNumberingSubset(unknown);

        std::ostringstream oconv;
        oconv << "FiberList_" << FiberNS::AsString(FiberPolicyT) << '_' << ParameterNS::Name<TypeT>() << '_'
              << unique_id;

        global_vector_ = std::make_unique<GlobalVector>(numbering_subset, oconv.str());
        AllocateGlobalVector(god_of_dof, *global_vector_);

        // #1895 - #1714 This line is deeply unsatisfactory - we explicitly tell not to release properly
        // the ressource. See #1895 for more about it; #1714 should remve the need for this line!
        global_vector_->SetDoNotDestroyPetscVector();

        assert(god_of_dof.GetMesh().GetUniqueId() == domain.GetMesh().GetUniqueId());

        typename Internal::FiberNS::Traits<FiberPolicyT, TypeT>::value_list_per_coord_index_type
            value_list_per_coord_index;

        decltype(auto) mesh = god_of_dof.GetMesh();

        if constexpr (FiberPolicyT == FiberNS::AtNodeOrAtQuadPt::at_node)
        {
            Internal::FiberNS::ReadFiberFile<FiberPolicyT, TypeT>(
                god_of_dof.GetMpi(), fiber_file, mesh, domain, value_list_per_coord_index);

            std::cout << god_of_dof.GetMpi().GetRankPrefix() << ' ' << value_list_per_coord_index.size()
                      << " values read in the fiber file." << std::endl;

            auto& global_vector = GetNonCstGlobalVector();

            // Values of fiber are actually given at nodes; so build first a \a ParameterAtDof.
            Internal::FiberNS::FillGlobalVector<FiberPolicyT, TypeT>(value_list_per_coord_index,
                                                                     god_of_dof.GetProcessorWiseNodeBearerList(),
                                                                     unknown,
                                                                     numbering_subset,
                                                                     global_vector);
        }

        if constexpr (FiberPolicyT == FiberNS::AtNodeOrAtQuadPt::at_quad_pt)
        {
            const auto& mpi = god_of_dof.GetMpi();

            const auto& ref_geom_elt_list = mesh.template BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                    continue;

                decltype(auto) iterator_range =
                    mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

                Nprocessor_wise_geom_elt =
                    static_cast<std::size_t>(std::distance(iterator_range.first, iterator_range.second));

                Nprogram_wise_geom_elt = static_cast<std::size_t>(
                    mpi.AllReduce(Nprocessor_wise_geom_elt, MoReFEM::Wrappers::MpiNS::Op::Sum));

                assert(Nprogram_wise_geom_elt > 0 && "Domain containing the parameter is empty!");

                const auto& first_geom_elt = *iterator_range.first;
                const auto index = first_geom_elt->GetIndex();

                first_index_program_wise_ = mpi.AllReduce(index, MoReFEM::Wrappers::MpiNS::Op::Min);
            }
        }
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    void FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::Initialize(
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    {
#ifndef NDEBUG
        {
            assert(was_initialize_already_called_ == false && "Should be called only once!");
            was_initialize_already_called_ = true;
        }
#endif // NDEBUG

        assert(underlying_parameter_ == nullptr);
        const auto& domain = this->GetDomain();
        const auto& felt_space = GetFeltSpace();
        const auto& time_manager = GetTimeManager();
        const auto& unknown = GetUnknown();

        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

        if constexpr (FiberPolicyT == FiberNS::AtNodeOrAtQuadPt::at_node)
        {
            ParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, StorageT> param_at_dof(
                "Temporary", domain, felt_space, unknown, GetNonCstGlobalVector());

            // Then convert them into the actual attribute.
            using converter_type = ParameterNS::FromParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, StorageT, 1UL>;

            underlying_parameter_ = converter_type::Perform("Fiber manager",
                                                            domain,
                                                            quadrature_rule_per_topology == nullptr
                                                                ? felt_space.GetQuadratureRulePerTopologyRawPtr()
                                                                : quadrature_rule_per_topology,
                                                            time_manager,
                                                            param_at_dof);
        }

        if constexpr (FiberPolicyT == FiberNS::AtNodeOrAtQuadPt::at_quad_pt)
        {
            const auto& mesh = domain.GetMesh();

            typename Internal::FiberNS::Traits<FiberPolicyT, TypeT>::value_list_per_quad_pt_index_type
                value_list_per_quad_pt_index;

            const auto& ref_geom_elt_list = mesh.template BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                    continue;

                assert(!(!quadrature_rule_per_topology));
                const auto& quadrature_rule =
                    quadrature_rule_per_topology->GetRule(ref_geom_elt.GetTopologyIdentifier());

                const auto Nquadrature_point = quadrature_rule.NquadraturePoint();

                assert(Nquadrature_point > 0 && "Empty quadrature point list!");

                // Fill the unordered map 'value_list_per_coord_index' with the values read in the fiber file for each
                // processor.
                Internal::FiberNS::ReadFiberFileAtQuadPt<TypeT>(fiber_file_,
                                                                mesh,
                                                                domain,
                                                                Nquadrature_point,
                                                                first_index_program_wise_,
                                                                Nprogram_wise_geom_elt,
                                                                Nprocessor_wise_geom_elt,
                                                                value_list_per_quad_pt_index);
            }

            // Construct the parameter with default initialisation.
            if constexpr (TypeT == ParameterNS::Type::vector)
            {
                Eigen::Vector3d init_vector;
                init_vector.setZero();

                assert(!(!quadrature_rule_per_topology));
                underlying_parameter_ = std::make_unique<ParameterAtQuadraturePoint<TypeT, TimeManagerT>>(
                    "Fiber vector at quad points", domain, *quadrature_rule_per_topology, init_vector, time_manager);
            }

            if constexpr (TypeT == ParameterNS::Type::scalar)
            {
                const double init_value = 0.;

                assert(!(!quadrature_rule_per_topology));
                underlying_parameter_ = std::make_unique<ParameterAtQuadraturePoint<TypeT, TimeManagerT>>(
                    "Fiber scalar at quad points", domain, *quadrature_rule_per_topology, init_value, time_manager);
            }

            // Update the parameter with the coords read in the fiber file.
            for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
            {
                assert(!(!ref_geom_elt_ptr));
                const auto& ref_geom_elt = *ref_geom_elt_ptr;

                if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                    continue;

                const auto& quadrature_rule =
                    quadrature_rule_per_topology->GetRule(ref_geom_elt.GetTopologyIdentifier());

                const auto Nquadrature_point = quadrature_rule.NquadraturePoint();

                const auto iterator_range =
                    mesh.template GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

                // Iterate over all geometric elements that share the ref_geom_elt.
                for (auto it_geom_elt = iterator_range.first; it_geom_elt != iterator_range.second; ++it_geom_elt)
                {
                    const auto& geom_elt_ptr = *it_geom_elt;
                    assert(!(!geom_elt_ptr));
                    const auto geom_elt_index = geom_elt_ptr->GetIndex();
                    const auto& geom_elt = *geom_elt_ptr;

                    if (!domain.IsGeometricEltInside(geom_elt))
                        continue;

                    const auto target_index = (geom_elt_index - first_index_program_wise_).Get()
                                              * static_cast<std::size_t>(Nquadrature_point);

                    for (auto quad_pt_index = Eigen::Index{}; quad_pt_index < Nquadrature_point; ++quad_pt_index)
                    {
                        const auto& quad_pt = quadrature_rule.Point(quad_pt_index);
                        const auto size_t_quad_pt_index = static_cast<std::size_t>(quad_pt_index);

                        if constexpr (TypeT == ParameterNS::Type::vector)
                        {
                            auto functor = [&target_index, &size_t_quad_pt_index, &value_list_per_quad_pt_index](
                                               auto& fiber_vector)
                            {
                                fiber_vector(0) = value_list_per_quad_pt_index[target_index + size_t_quad_pt_index][0];
                                fiber_vector(1) = value_list_per_quad_pt_index[target_index + size_t_quad_pt_index][1];
                                fiber_vector(2) = value_list_per_quad_pt_index[target_index + size_t_quad_pt_index][2];
                            };
                            underlying_parameter_->UpdateValue(quad_pt, geom_elt, functor);
                        }

                        if constexpr (TypeT == ParameterNS::Type::scalar)
                        {
                            auto functor = [&target_index, &size_t_quad_pt_index, &value_list_per_quad_pt_index](
                                               auto& fiber_scalar)
                            {
                                fiber_scalar = value_list_per_quad_pt_index[target_index + size_t_quad_pt_index][0];
                            };
                            underlying_parameter_->UpdateValue(quad_pt, geom_elt, functor);
                        }
                    }
                }
            }
        }
        Advanced::Wrappers::EigenNS::IsMallocAllowed(true);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline bool FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::IsConstant() const noexcept
    {
        return GetUnderlyingParameter().IsConstant();
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline typename FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::return_type
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::SupplGetConstantValue() const
    {
        return GetUnderlyingParameter().SupplGetConstantValue();
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline typename FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::return_type
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::SupplGetAnyValue() const
    {
        return GetUnderlyingParameter().SupplGetAnyValue();
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline typename FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::return_type
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::SupplGetValue(
        const local_coords_type& local_coords,
        const GeometricElt& geom_elt) const
    {
        return GetUnderlyingParameter().SupplGetValue(local_coords, geom_elt);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline void
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::SupplWrite(std::ostream& stream) const
    {
        return GetUnderlyingParameter().SupplWrite(stream);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline GlobalVector&
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::GetNonCstGlobalVector() noexcept
    {
        return const_cast<GlobalVector&>(GetGlobalVector());
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline const GlobalVector&
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::GetGlobalVector() const noexcept
    {
        assert(!(!global_vector_));
        return *global_vector_;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline const typename FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::parameter_type&
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::GetUnderlyingParameter() const noexcept
    {
        assert(!(!underlying_parameter_) && "You should call Initialize() in your model.");
        return *underlying_parameter_;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    void FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::SupplTimeUpdate()
    {
        assert(!GetUnderlyingParameter().IsTimeDependent());
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    void FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::SupplTimeUpdate(
        [[maybe_unused]] double time)
    {
        assert(!GetUnderlyingParameter().IsTimeDependent());
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline void FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::SetConstantValue(
        [[maybe_unused]] storage_value_type value)
    {
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    inline const FEltSpace&
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::GetFeltSpace() const noexcept
    {
        return felt_space_;
    }


    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    const TimeManagerT&
    FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }

    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    const Unknown& FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>::GetUnknown() const noexcept
    {
        return unknown_;
    }


    template<class EnumT>
    constexpr FiberListNS::unique_id AsFiberListId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return FiberListNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
