// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLISTMANAGER_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLISTMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Concept.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS { template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> struct ClearSingletons; } // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::FiberNS
{


    /*!
     * \brief This class is used to create and retrieve fiber_list_type objects.
     *
     * fiber_list_type objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * fiber_list_type object given its unique id (which is the one that appears in the input data file).
     *
     * \tparam TypeT There is actually one manager for scalar parameters and another for vectorial ones.
     */
    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
             ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT =
                 ParameterNS::TimeDependencyNS::None,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>>
    class FiberListManager
    : public Utilities::Singleton<FiberListManager<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>>
    {
      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = InputDataNS::FiberTag<FiberPolicyT, TypeT>;

        //! Alias to the type of fiber_list_type stored.
        using fiber_list_type = FiberList<FiberPolicyT, TypeT, TimeManagerT, TimeDependencyT, StorageT>;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        friend struct MoReFEM::TestNS::ClearSingletons;

        //! Convenient alias.
        using storage_type = std::unordered_map<FiberListNS::unique_id, typename fiber_list_type::unique_ptr>;

      public:
        /*!
         * \brief Create a \a FiberList object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description,
                    const ModelSettingsT& model_settings,
                    const InputDataT& input_data);

      private:
        //! Destructor.
        virtual ~FiberListManager() override = default;

      public:
        //! Fetch the \a FiberList object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{fiber_list_type}
        const fiber_list_type& GetFiberList(FiberListNS::unique_id unique_id) const;

        //! Fetch the \a FiberList object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{fiber_list_type}
        fiber_list_type& GetNonCstFiberList(FiberListNS::unique_id unique_id);

        //! Fetch the \a FiberList object associated with \a unique_id unique identifier and returns it as a pointer.
        //! \unique_id_param_in_accessor{fiber_list_type}
        //! \attention Don't call \a delete on this pointer; the \a FiberListManager deals with ownership.
        const fiber_list_type* GetFiberListPtr(FiberListNS::unique_id unique_id) const;


        //! Access to the storage.
        const storage_type& GetStorage() const noexcept;

      private:
        /*!
         * \brief Method that cconstruct a new \a FiberList and store it into the class.
         *
         * \param[in] unique_id Unique identifier of the \a FiberList.
         * \param[in] fiber_file File from which the fiber data are read.
         * \copydoc doxygen_hide_parameter_domain_arg
         * \param[in] felt_space \a FEltSpace in which the \a Parameter is defined.
         * \param[in] unknown \a Unknown considered.
         *
         */
        void Create(const FiberListNS::unique_id unique_id,
                    const FilesystemNS::File& fiber_file,
                    const Domain& domain,
                    const FEltSpace& felt_space,
                    const Unknown& unknown);

        //! Time manager of the \a Model.
        const TimeManagerT& GetTimeManager() const noexcept;


      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_time_manager_arg
         */
        explicit FiberListManager(const TimeManagerT& time_manager);

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<FiberListManager>;
        ///@}

        //! \copydoc doxygen_hide_manager_clear
        void Clear();

        //! Non constant access to the storage.
        storage_type& GetNonCstStorage() noexcept;


      private:
        //! Store the god of dof objects by their unique identifier.
        storage_type storage_;

        //! Time manager of the \a Model.
        const TimeManagerT& time_manager_;
    };


} // namespace MoReFEM::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Fiber/FiberListManager.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLISTMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //
