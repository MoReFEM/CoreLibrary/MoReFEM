// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_FILLGLOBALVECTOR_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_FILLGLOBALVECTOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>
#include <unordered_map>

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/Parameter/FiberEnum.hpp"

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

#include "ParameterInstances/Fiber/Internal/Traits.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Unknown; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FiberNS
{

    /*!
     * \brief Fill the global vector from the data read in the file.
     *
     * This function is an helper function of FiberList constructor.
     *
     * \param[in] value_list_per_coord_index The output of ReadFiberFile() function.
     * \param[in] node_bearer_list List of all node bearers considered in the GodOfDof on current processor.
     * \param[in] unknown A scalar or vectorial unknown that acts a bit as a strawman: dofs are defined only
     * in relationship to an unknown, so you must create one if none fulfill your purposes (for instance
     * if you deal with a vectorial unknown and need a scalar Dof field, you must create another unknown
     * only for their \a Parameter. To save space, it's better if this unknown is in its own numbering subset,
     * but this is not mandatory. Unknown should be scalar for TypeT == ParameterNS::Type::scalar and vectorial
     * for TypeT == ParameterNS::Type::vectorial.
     * \param[in] numbering_subset Numbering subset upon which the unknown is defined.
     * \param[out] vector The global vector upon which Parameter is to be defined completely filled.
     *
     */

    //!
    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT = ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
             ::MoReFEM::ParameterNS::Type TypeT>
    void FillGlobalVector(
        const typename Traits<FiberPolicyT, TypeT>::value_list_per_coord_index_type& value_list_per_coord_index,
        const NodeBearer::vector_shared_ptr& node_bearer_list,
        const Unknown& unknown,
        const NumberingSubset& numbering_subset,
        GlobalVector& vector);


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Fiber/Internal/FillGlobalVector.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_FILLGLOBALVECTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
