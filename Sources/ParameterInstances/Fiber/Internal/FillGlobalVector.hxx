// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_FILLGLOBALVECTOR_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_FILLGLOBALVECTOR_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Fiber/Internal/FillGlobalVector.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::FiberNS
{


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    void FillGlobalVector(
        const typename Traits<FiberPolicyT, TypeT>::value_list_per_coord_index_type& value_list_per_coord_index,
        const NodeBearer::vector_shared_ptr& node_bearer_list,
        const Unknown& unknown,
        const NumberingSubset& numbering_subset,
        GlobalVector& vector)
    {
#ifndef NDEBUG
        if (TypeT == ParameterNS::Type::scalar)
            assert(unknown.GetNature() == UnknownNS::Nature::scalar);
        if (TypeT == ParameterNS::Type::vector)
            assert(unknown.GetNature() == UnknownNS::Nature::vectorial);
#endif // NDEBUG

        {
            ::MoReFEM::Wrappers::Petsc ::AccessVectorContent<Utilities::Access::read_and_write> vector_content(vector);

#ifndef NDEBUG
            const auto vector_proc_size = vector.GetProcessorWiseSize();
            PetscInt number_of_dofs_filled = 0;
#endif // NDEBUG

            for (const auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                const auto& node_bearer = *node_bearer_ptr;

                if (node_bearer.GetNature() != ::MoReFEM::InterfaceNS::Nature::vertex)
                    continue;

                const auto& coords_list = node_bearer.GetInterface().GetCoordsList();
                assert(coords_list.size() == 1UL && "Interface is a vertex...");
                const auto& coords_ptr = coords_list.back();
                assert(!(!coords_ptr));

                const auto geometric_vertex_index = coords_ptr->GetIndexFromMeshFile();

                auto it = value_list_per_coord_index.find(geometric_vertex_index);

                if (it != value_list_per_coord_index.cend())
                {
                    const auto& value_list_for_current_coord = it->second;

                    const auto& node_list = node_bearer.GetNodeList();

                    // Find the only node related to \a Unknown. There is no more direct accessor in
                    // \a NodeBearer s it would be error-prone: one should not be encouraged to select
                    // solely on \a Unknown regardless of shape function.
                    const auto& find_node_with_unknown = [&unknown](const auto& node_ptr)
                    {
                        assert(!(!node_ptr));
                        return node_ptr->GetUnknown() == unknown;
                    };

                    assert(std::count_if(node_list.cbegin(), node_list.cend(), find_node_with_unknown) == 1UL);

                    const auto it_node = std::find_if(node_list.cbegin(), node_list.cend(), find_node_with_unknown);

                    decltype(auto) node_ptr = *it_node;
                    assert(!(!node_ptr));

                    const auto& node = *node_ptr;

                    assert(node.IsInNumberingSubset(numbering_subset));

                    const auto& dof_list = node.GetDofList();

                    const std::size_t Ndof_in_node = dof_list.size();
                    assert(Ndof_in_node <= value_list_for_current_coord.size());

                    for (std::size_t i = 0UL; i < Ndof_in_node; ++i)
                    {
                        const auto& dof_ptr = dof_list[i];
                        assert(!(!dof_ptr));

                        const auto index = dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset);

                        assert(::MoReFEM::DofNS::ToVectorIndex(index) < vector_proc_size);

                        vector_content[::MoReFEM::DofNS::ToVectorIndex(index)] = value_list_for_current_coord[i];

#ifndef NDEBUG
                        ++number_of_dofs_filled;
#endif // NDEBUG
                    }
                }
            }

            assert(vector_proc_size.Get() == number_of_dofs_filled && "All the dofs have not been filled.");
        }

        vector.UpdateGhosts();
    }


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_FILLGLOBALVECTOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
