// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILE_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>
#include <unordered_map>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export

#include "Core/Parameter/FiberEnum.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "ParameterInstances/Fiber/Internal/Traits.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FiberNS
{


    /*!
     * \brief Read and interpret the Ensight fiber file.
     *
     * This function is an helper function of FiberList constructor.
     *
     * \copydetails doxygen_hide_mpi_param
     * \param[in] fiber_file File at the Ensight format which will be interpreted. Its format is specified by
     * Ensight specifications;  typically scalar files get a 'scl' extension and their first line is 'Scalar per
     * node' whereas vectorial get a 'vct' extension and first line is 'Vector per node'. \param[in] mesh Mesh
     * considered for the parameters; in peculiar vertices to consider will be taken from there. \param[in]
     * domain \a Domain considered for the parameters; in peculiar vertices to consider will be taken from
     * there. \param[out] out Values per each Coord managed by the current processor. Index used is the one read
     * from the file.
     *
     */
    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    void ReadFiberFile(const ::MoReFEM::Wrappers::Mpi& mpi,
                       const ::MoReFEM::FilesystemNS::File& fiber_file,
                       const Mesh& mesh,
                       const Domain& domain,
                       typename Traits<FiberPolicyT, TypeT>::value_list_per_coord_index_type& out);


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Fiber/Internal/ReadFiberFile.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
