// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILE_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILE_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Fiber/Internal/ReadFiberFile.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::FiberNS
{


    namespace // anonymous
    {


        //! Count the number of fibers in the fiber file.
        template<class TraitsT>
        [[maybe_unused]] std::size_t NfiberInFile(const ::MoReFEM::FilesystemNS::File& fiber_file);


    } // namespace


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    void ReadFiberFile(const ::MoReFEM::Wrappers::Mpi& mpi,
                       const ::MoReFEM::FilesystemNS::File& fiber_file,
                       const Mesh& mesh,
                       const Domain& domain, // \todo #1130 Take the domain into account.
                       typename Traits<FiberPolicyT, TypeT>::value_list_per_coord_index_type& out)
    {
        out.max_load_factor(Utilities::DefaultMaxLoadFactor());

        std::ifstream stream{ fiber_file.Read() };

        std::string line;

        // Read the line "Vector per node".
        getline(stream, line);

        Utilities::String::StripRight(line, " \t");

        using traits = Traits<FiberPolicyT, TypeT>;

        traits::CheckFirstLineOfFile(fiber_file, line);

        // Now read the content of the file and filter out in the process all that are not managed by current
        // processor.
        constexpr long value_size = 12;
        std::string str_value(value_size, ' ');

        const auto& processor_wise_coords_list = mesh.GetProcessorWiseCoordsList();
        const auto coords_begin = processor_wise_coords_list.cbegin();
        const auto coords_end = processor_wise_coords_list.cend();

        assert(std::none_of(coords_begin, coords_end, Utilities::IsNullptr<Coords::shared_ptr>));

        auto it_current_coords = coords_begin;

        typename traits::value_list_type value_list_for_current_coord;

        constexpr auto Nvertex_per_line = traits::NvertexPerLine();
        constexpr auto Nvalue_per_vertex = traits::NvaluePerVertex();

        // I need the total number of coords to make sure fiber file is not too long.
        const auto Ncoords_in_domain_program_wise = NcoordsInDomain<MpiScale::program_wise>(mpi, domain, mesh);

#ifndef NDEBUG
        const auto Ncoords_in_domain_processor_wise = NcoordsInDomain<MpiScale::processor_wise>(mpi, domain, mesh);
#endif // NDEBUG

        // =============================================
        // First check the number of vertices in the mesh is coherent with number of fibers.
        // =============================================

        if (mpi.IsRootProcessor())
        {
            const auto Nfiber_in_file = NfiberInFile<traits>(fiber_file);

            if (Nfiber_in_file != Ncoords_in_domain_program_wise)
                throw Exception("Discrepancy between domain and fiber file: " + std::to_string(Nfiber_in_file)
                                + " were read in the fiber file while "
                                  "the domain was composed of "
                                + std::to_string(Ncoords_in_domain_program_wise) + " vertices.");
        }


        // =============================================
        // Then read the file and extract relevant data for current processor.
        // =============================================

        ::MoReFEM::CoordsNS::program_wise_position Nvertex_read_in_fiber_file{ 0UL };

        while (getline(stream, line))
        {
            const auto begin_line = line.cbegin();
            const auto end_line = line.cend();

            for (std::size_t vertex_on_line = 0UL; vertex_on_line < Nvertex_per_line && it_current_coords < coords_end;
                 ++vertex_on_line)
            {
                const auto begin_current_coord_info =
                    begin_line + static_cast<std::ptrdiff_t>(vertex_on_line * Nvalue_per_vertex * value_size);

                if (begin_current_coord_info >= end_line) // to account for the very last line that might cover only one
                                                          // Coord. Consistency check already performed in NfiberInFile.
                    continue;

                const bool is_current_coord_a_match =
                    ((*it_current_coords)->GetProgramWisePosition() == Nvertex_read_in_fiber_file);

                ++Nvertex_read_in_fiber_file;

                if (is_current_coord_a_match) // means current Coord is handled by current processor.
                {


                    for (std::size_t value_index = 0UL; value_index < Nvalue_per_vertex; ++value_index)
                    {
                        const auto begin_value =
                            begin_current_coord_info + static_cast<std::ptrdiff_t>(value_index * value_size);
                        assert(begin_value < end_line);

                        str_value.assign(begin_value, begin_value + value_size);
                        value_list_for_current_coord[value_index] = std::stod(str_value);
                    }

                    [[maybe_unused]] auto check = out.insert(
                        std::make_pair((*it_current_coords)->GetIndexFromMeshFile(), value_list_for_current_coord));
                    assert(check.second);

                    ++it_current_coords;
                }


            } // for (std::size_t vertex_on_line ...
        }

        assert(out.size() == Ncoords_in_domain_processor_wise
               && "Incorrect number of fibers read on current processor.");
    }


    namespace // anonymous
    {


        template<class TraitsT>
        std::size_t NfiberInFile(const ::MoReFEM::FilesystemNS::File& fiber_file)
        {
            constexpr auto Nvertex_per_line = TraitsT::NvertexPerLine();
            constexpr auto Nvalue_per_vertex = TraitsT::NvaluePerVertex();

            std::ifstream stream{ fiber_file.Read() };

            std::string line;

            // Read the line "Vector per node".
            getline(stream, line);
            Utilities::String::StripRight(line, " \t");
            TraitsT::CheckFirstLineOfFile(fiber_file, line);

            constexpr long value_size = 12;

            std::size_t ret = 0UL;

            while (getline(stream, line))
            {
                const auto begin_line = line.cbegin();
                const auto end_line = line.cend();

                for (std::size_t vertex_on_line = 0UL; vertex_on_line < Nvertex_per_line; ++vertex_on_line)
                {
                    const auto begin_current_coord_info =
                        begin_line + static_cast<std::ptrdiff_t>(vertex_on_line * Nvalue_per_vertex * value_size);

                    if (begin_current_coord_info
                        >= end_line) // to account for the very last line that might cover only one Coord.
                        continue;

                    ++ret;
                }
            }

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILE_DOT_HXX_
// *** MoReFEM end header guards *** < //
