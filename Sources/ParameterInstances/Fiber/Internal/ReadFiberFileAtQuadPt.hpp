// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILEATQUADPT_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILEATQUADPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>
#include <unordered_map>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export

#include "Core/Parameter/FiberEnum.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "ParameterInstances/Fiber/Internal/Traits.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FiberNS
{


    /*!
     * \brief Read and interpret the Ensight fiber file.
     *
     * This function is an helper function of FiberList constructor.
     *
     * \param[in] fiber_file File at the Ensight format which will be interpreted. Its format is specified by
     * Ensight specifications;  typically scalar files get a 'scl' extension and their first line is 'Scalar per
     * node' whereas vectorial get a 'vct' extension and first line is 'Vector per node'.
     * \param[in] mesh Mesh considered for the parameters; in peculiar vertices to consider will be taken from there.
     * \param[in] domain \a Domain considered for the parameters; in peculiar vertices to consider will be taken from
     * there.
     * \param[in] Nquadrature_point Number of quadrature points per element.
     * \param[in] first_index_program_wise Index of the first geometric element of the domain. Used to read the fiber file.
     * \param[in] Nprogram_wise_geom_elt Number of geometric elements used to check the length of the file.
     * \param[in] Nprocessor_wise_geom_elt Number of geometric elements used to check the size of the output.
     * \param[out] out Values per each Coord managed by the current processor. Index used is an index to
     * identify the quadrature point involved.
     *
     */
    template<ParameterNS::Type TypeT>
    void ReadFiberFileAtQuadPt(const ::MoReFEM::FilesystemNS::File& fiber_file,
                               const Mesh& mesh,
                               const Domain& domain,
                               const Eigen::Index Nquadrature_point,
                               const ::MoReFEM::GeomEltNS::index_type first_index_program_wise,
                               const std::size_t Nprogram_wise_geom_elt,
                               const std::size_t Nprocessor_wise_geom_elt,
                               typename Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                               TypeT>::value_list_per_quad_pt_index_type& out);


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Fiber/Internal/ReadFiberFileAtQuadPt.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILEATQUADPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
