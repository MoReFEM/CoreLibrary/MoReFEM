// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILEATQUADPT_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILEATQUADPT_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Fiber/Internal/ReadFiberFileAtQuadPt.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::FiberNS
{


    template<ParameterNS::Type TypeT>
    void ReadFiberFileAtQuadPt(const ::MoReFEM::FilesystemNS::File& fiber_file,
                               const Mesh& mesh,
                               const Domain& domain,
                               const Eigen::Index Nquadrature_point,
                               const GeomEltNS::index_type first_index_program_wise,
                               const std::size_t Nprogram_wise_geom_elt,
                               const std::size_t Nprocessor_wise_geom_elt,
                               typename Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                               TypeT>::value_list_per_quad_pt_index_type& out)
    {
        out.max_load_factor(Utilities::DefaultMaxLoadFactor());

        std::ifstream stream{ fiber_file.Read() };

        std::string line;

        // Read the line "Vector per quad_pt".
        getline(stream, line);

        Utilities::String::StripRight(line, " \t");

        using traits = Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt, TypeT>;

        traits::CheckFirstLineOfFile(fiber_file, line);

        constexpr auto Nquad_pt_per_line = traits::NvertexPerLine();
        constexpr auto Nvalue_per_quad_pt = traits::NvaluePerVertex();

        // Total number of coords to make sure the fiber file has the right size.
        const auto Ncoords_in_domain_program_wise =
            Nprogram_wise_geom_elt * static_cast<std::size_t>(Nquadrature_point);
        const auto Ncoords_in_domain_processor_wise =
            Nprocessor_wise_geom_elt * static_cast<std::size_t>(Nquadrature_point);
        out.reserve(Ncoords_in_domain_processor_wise);

        // =============================================
        // First check the number of quadrature points in the mesh is coherent with number of fibers.
        // =============================================

        const auto Nfiber_in_file = NfiberInFile<traits>(fiber_file);

        if (Nfiber_in_file != Ncoords_in_domain_program_wise)
        {
            std::ostringstream oconv;
            oconv << "Discrepancy between quadrature on domain and fiber file " << fiber_file << ": " << Nfiber_in_file
                  << " values were read in the fiber file while "
                     "the domain was composed of "
                  << Ncoords_in_domain_program_wise << " quadrature points.";

            throw Exception(oconv.str());
        }

        const auto& ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

        // #1742 Index of what? Rather unclear...
        // I surmised GeometricElt index in #1719, but it didn't add up in the end.
        // (I surmised it due to comment "first_index_program_wise Index of the first geometric element of the domain.
        // Used to read the fiber file.")
        std::vector<std::size_t> index_list_processor_wise;

        for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
        {
            assert(!(!ref_geom_elt_ptr));
            const auto& ref_geom_elt = *ref_geom_elt_ptr;
            if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                continue;

            decltype(auto) iterator_range =
                mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

            // Get the list of geometric elements managed by each processor.
            for (auto it_geom_elt = iterator_range.first; it_geom_elt != iterator_range.second; ++it_geom_elt)
            {
                const auto& geom_elt_ptr = *it_geom_elt;
                assert(!(!geom_elt_ptr));
                const auto geom_elt_index = geom_elt_ptr->GetIndex();
                const auto& geom_elt = *geom_elt_ptr;

                if (!domain.IsGeometricEltInside(geom_elt))
                    continue;

                for (auto i = Eigen::Index{}; i < Nquadrature_point; ++i)
                {
                    const auto target_index_in_fiber_file =
                        (geom_elt_index - first_index_program_wise).Get() * static_cast<std::size_t>(Nquadrature_point)
                        + static_cast<std::size_t>(i);
                    index_list_processor_wise.push_back(target_index_in_fiber_file);
                }
            }
        }
        // =============================================
        // Then read the file and extract relevant data for current processor.
        // =============================================

        auto Nquad_pt_read_in_fiber_file{ 0UL };
        constexpr long value_size = 12;

        std::string str_value(value_size, ' ');
        typename traits::value_list_type value_list_for_current_coord;

        // Now read the content of the file and filter out in the process the values that are not managed by current
        // processor.
        while (getline(stream, line))
        {
            auto begin_line = line.cbegin();
            auto end_line = line.cend();

            // Puts iterator in front of the first value.
            for (std::size_t quad_pt_on_line = 0UL; quad_pt_on_line < Nquad_pt_per_line; ++quad_pt_on_line)
            {
                const auto begin_current_coord_info =
                    begin_line + static_cast<std::ptrdiff_t>(quad_pt_on_line * Nvalue_per_quad_pt * value_size);

                // To account for the very last line that might cover only one Coord.
                if (begin_current_coord_info >= end_line)
                    continue;

                // Checks that the current quadrature point is handled by the current processor.
                // #1742 This line is absolutely dreadful; the strong type here highlights something is askew with the
                // implementation here.
                const auto it = std::find(
                    index_list_processor_wise.cbegin(), index_list_processor_wise.cend(), Nquad_pt_read_in_fiber_file);

                if (it != index_list_processor_wise.cend())
                {
                    // Reads x1 y1 z1 (vector) or X (scalar) and puts it into value_list_for_current_coord.
                    for (std::size_t value_index = 0UL; value_index < Nvalue_per_quad_pt; ++value_index)
                    {
                        const auto begin_value =
                            begin_current_coord_info + static_cast<std::ptrdiff_t>(value_index * value_size);
                        assert(begin_value < end_line);

                        str_value.assign(begin_value, begin_value + value_size);

                        value_list_for_current_coord[value_index] = std::stod(str_value);
                    }
                    [[maybe_unused]] auto check =
                        out.insert({ Nquad_pt_read_in_fiber_file, value_list_for_current_coord });
                    assert(check.second);
                }
                ++Nquad_pt_read_in_fiber_file;
            }
        }
        assert(out.size() == Ncoords_in_domain_processor_wise
               && "Incorrect number of fibers read on current processor.");
    }


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_READFIBERFILEATQUADPT_DOT_HXX_
// *** MoReFEM end header guards *** < //
