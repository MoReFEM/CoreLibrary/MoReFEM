// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <sstream>
#include <string>

#include "ParameterInstances/Fiber/Internal/Traits.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::FiberNS
{


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Scalar per node")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was Ensight one for a scalar "
                     "quantity (the one with scl extension)";

            throw Exception(oconv.str());
        }
    }


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::scalar>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Scalar per quad_pt")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was an Ensight one for a scalar "
                     "quantity defined at the quadrature points.";

            throw Exception(oconv.str());
        }
    }


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Vector per node")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was Ensight one for a vectorial "
                     "quantity (the one with vct extension).";

            throw Exception(oconv.str());
        }
    }


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::vector>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Vector per quad_pt")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was an Ensight one for a vectorial "
                     "quantity defined at the quadrature points.";

            throw Exception(oconv.str());
        }
    }


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
