// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_TRAITS_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_TRAITS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <unordered_map>

#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::FiberNS { enum class AtNodeOrAtQuadPt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FiberNS
{

    /*!
     * \brief Traits class conveniently used to provide a generic interface for both scalar and vector
     * fiber managers.
     */
    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ::MoReFEM::ParameterNS::Type TypeT>
    struct Traits;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    struct Traits<FiberPolicyT, ParameterNS::Type::scalar>
    {

        using value_list_type = std::array<double, 1>;

        using value_list_per_coord_index_type =
            std::unordered_map<::MoReFEM::CoordsNS::index_from_mesh_file, value_list_type>;

        using value_list_per_quad_pt_index_type = std::unordered_map<std::size_t, value_list_type>;

        static void CheckFirstLineOfFile(const ::MoReFEM::FilesystemNS::File& fiber_file, const std::string& line);

        static constexpr std::size_t NvertexPerLine();

        static constexpr std::size_t NvaluePerVertex();
    };


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    struct Traits<FiberPolicyT, ParameterNS::Type::vector>
    {
        using value_list_type = std::array<double, 3>;

        using value_list_per_coord_index_type =
            std::unordered_map<::MoReFEM::CoordsNS::index_from_mesh_file, value_list_type>;

        using value_list_per_quad_pt_index_type = std::unordered_map<std::size_t, value_list_type>;

        static void CheckFirstLineOfFile(const ::MoReFEM::FilesystemNS::File& fiber_file, const std::string& line);

        static constexpr std::size_t NvertexPerLine();

        static constexpr std::size_t NvaluePerVertex();
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/Fiber/Internal/Traits.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_TRAITS_DOT_HPP_
// *** MoReFEM end header guards *** < //
