// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_TRAITS_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_TRAITS_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Fiber/Internal/Traits.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FiberNS { enum class AtNodeOrAtQuadPt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::FiberNS
{


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::scalar>::NvertexPerLine()
    {
        return 6UL;
    }

    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::vector>::NvertexPerLine()
    {
        return 2UL;
    }

    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::scalar>::NvaluePerVertex()
    {
        return 1UL;
    }

    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::vector>::NvaluePerVertex()
    {
        return 3UL;
    }


} // namespace MoReFEM::Internal::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_INTERNAL_TRAITS_DOT_HXX_
// *** MoReFEM end header guards *** < //
