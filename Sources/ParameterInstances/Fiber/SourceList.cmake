### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_PARAM_INSTANCES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/FiberList.hpp
		${CMAKE_CURRENT_LIST_DIR}/FiberList.hxx
		${CMAKE_CURRENT_LIST_DIR}/FiberListManager.hpp
		${CMAKE_CURRENT_LIST_DIR}/FiberListManager.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/UniqueId.hpp
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
