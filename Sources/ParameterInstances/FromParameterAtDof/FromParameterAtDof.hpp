// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_FROMPARAMETERATDOF_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_FROMPARAMETERATDOF_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Core/Parameter/Internal/Traits.hpp"

#include "Parameters/ParameterAtDof.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "ParameterInstances/FromParameterAtDof/Internal/FromAtDofToAtQuadPt.hpp"


namespace MoReFEM::ParameterNS
{


    /*!
     * \brief Define a convertor from a \a ParameterAtDof to a \a ParameterAtQuadraturePoint.
     *
     * \a ParameterAtDof is a bit overkill for a value defined at dofs that never vary: each time the value is
     * queried at a \a QuadraturePoint, the value is recomputed on the fly from the \a GlobalVector, which is much
     * more costly than what can achieve a \a ParameterAtQuadraturePoint, which stores the values directly.
     *
     * Current function (static method if you want to nitpick, just to ease friendship declaration) aims to provide
     * such a convertor.
     *
     * \copydetails doxygen_hide_at_dof_policy_tparam
     *
     * \tparam TimeDependencyT Policy in charge of time dependency.
     *
     * \internal This is a struct which static function to ease friendship declaration.
     * * \attention Current implementation is not secure enough with respect of quadrature rule! \todo #1031
     * \endinternal
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>,
        Eigen::Index NfeltSpaceT = 1UL
    >
    // clang-format on
    struct FromParameterAtDof
    {

        static_assert(TypeT != ParameterNS::Type::matrix, "Matricial parameter doesn't make sense for AtDof policy.");


        //! Convenient alias.
        using param_at_dof_type = ParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>;

        /*!
         * \brief
         *
         * \tparam StringT Any string type that might be interpreted directly into a std::string. Universal
         * reference tick is used here so it might be a pointer, a reference or a plain chain of characters.
         *
         * \param[in] name Name given to the parameter, just for output logs.
         * \copydoc doxygen_hide_parameter_domain_arg
         *
         * \param[in] quadrature_rule_per_topology \a QuadratureRule to use for each topology. Make sure you cover
         * all the cases met in the \a FEltSpace underlying \a param_at_dof.
         * \copydetails doxygen_hide_time_manager_arg
         * \param[in] param_at_dof \a ParameterAtDof to be converted into a \a ParameterAtQuadraturePoint.
         *
         * \return \a ParameterAtQuadraturePoint which contain same data as the source \a ParameterAtDof.
         */
        template<class StringT>
        static typename ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT, StorageT>::unique_ptr
        Perform(StringT&& name,
                const Domain& domain,
                const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                const TimeManagerT& time_manager,
                const param_at_dof_type& param_at_dof);
    };


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/FromParameterAtDof/FromParameterAtDof.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_FROMPARAMETERATDOF_DOT_HPP_
// *** MoReFEM end header guards *** < //
