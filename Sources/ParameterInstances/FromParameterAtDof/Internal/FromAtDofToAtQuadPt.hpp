// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_FROMATDOFTOATQUADPT_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_FROMATDOFTOATQUADPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtDof.hpp"
#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "ParameterInstances/FromParameterAtDof/Internal/Local/FromAtDofToAtQuadPt.hpp"


namespace MoReFEM::Internal::GlobalParameterOperatorNS
{

    /*!
     * \brief Child of \a GlobalParameterOperator which set the values at \a QuadraturePoint of
     * a \a ParameterAtQuadraturePoint from those of a \a ParameterAtDof.
     *
     * An instance of this class does the job for only one dimension; if several \a FEltSpace are covered
     * by the \a ParameterAtDof there will be one such object per \a FEltSpace.
     *
     * \copydetails doxygen_hide_at_dof_policy_tparam
     *
     * \tparam TimeDependencyT Policy in charge of time dependency.
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    class FromAtDofToAtQuadPt final
    : public GlobalParameterOperator
    <
        FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>,
        LocalParameterOperatorNS::FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>,
        TypeT,
        TimeManagerT,
        TimeDependencyT,
        StorageT
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to pendant local operator.
        using local_operator_type =
            LocalParameterOperatorNS::FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>;

        //! Convenient alias to pinpoint the GlobalParameterOperator parent.
        using parent =
            GlobalParameterOperator<self, local_operator_type, TypeT, TimeManagerT, TimeDependencyT, StorageT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Alias 'inherited' from local operator class,
        using param_at_dof_type = typename local_operator_type::param_at_dof_type;

        //! Alias 'inherited' from local operator class,
        using param_at_quad_pt_type = typename local_operator_type::param_at_quad_pt_type;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \class doxygen_hide_from_at_dof_to_quad_pt_param_args
         *
         * \param[in] param_at_dof \a ParameterAtDof from which values at \a QuadraturePoint are copied.
         * \param[in,out] parameter_to_set \a ParameterAtQuadraturePoint into which values at \a QuadraturePoint are written.
         */

        /*!
         * \brief Constructor.
         *
         * \param[in] felt_space \a FEltSpace which values are copied.
         * \copydoc doxygen_hide_from_at_dof_to_quad_pt_param_args
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit FromAtDofToAtQuadPt(const FEltSpace& felt_space,
                                     const param_at_dof_type& param_at_dof,
                                     const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                     param_at_quad_pt_type& parameter_to_set);

        //! Destructor.
        ~FromAtDofToAtQuadPt() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FromAtDofToAtQuadPt(const FromAtDofToAtQuadPt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FromAtDofToAtQuadPt(FromAtDofToAtQuadPt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FromAtDofToAtQuadPt& operator=(const FromAtDofToAtQuadPt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FromAtDofToAtQuadPt& operator=(FromAtDofToAtQuadPt&& rhs) = delete;

        ///@}

        /*!
         * \brief Update the \a ParameterAtQuadraturePoint (which is the whole point of current class!)
         */
        void Update() const;
    };


} // namespace MoReFEM::Internal::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/FromParameterAtDof/Internal/FromAtDofToAtQuadPt.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_FROMATDOFTOATQUADPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
