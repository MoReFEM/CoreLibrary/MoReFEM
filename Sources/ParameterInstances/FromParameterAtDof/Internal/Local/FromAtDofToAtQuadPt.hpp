// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_LOCAL_FROMATDOFTOATQUADPT_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_LOCAL_FROMATDOFTOATQUADPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp"

#include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"

#include "Parameters/Parameter.hpp"


namespace MoReFEM::Internal::LocalParameterOperatorNS
{


    /*!
     * \brief Local counterpart of \a GlobalParameterOperatorNS::FromAtDofToAtQuadPt.
     *
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>,
        Eigen::Index NfeltSpaceT = 1
    >
    // clang-format on
    class FromAtDofToAtQuadPt final
    : public Advanced::LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT, StorageT>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to parent.
        using parent = Advanced::LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT, StorageT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to 'inherit' from parent: elementary_data_type.
        using elementary_data_type = typename parent::elementary_data_type;

        //! Alias to traits class.
        using traits = ParameterNS::Traits<TypeT, StorageT>;

        //! Alias to the return type of the parameter.
        using return_type = typename traits::return_type;

        //! Alias to non constant reference to the parameter value.
        using non_constant_reference = typename traits::non_constant_reference;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to relevant \a ParameterAtQuadraturePoint instance.
        using param_at_quad_pt_type = ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT, StorageT>;

        //! Alias to relevant \a ParameterAtDof instance.
        using param_at_dof_type = ParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one scalar).
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         *
         * \copydoc doxygen_hide_from_at_dof_to_quad_pt_param_args
         *
         *
         * \internal This constructor must not be called manually: it is involved only in
         * GlobalParameterOperator<DerivedT, LocalParameterOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         */
        explicit FromAtDofToAtQuadPt(const ExtendedUnknown::const_shared_ptr& unknown,
                                     elementary_data_type&& elementary_data,
                                     param_at_quad_pt_type& parameter_to_set,
                                     const param_at_dof_type& param_at_dof);

        //! Destructor.
        ~FromAtDofToAtQuadPt() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FromAtDofToAtQuadPt(const FromAtDofToAtQuadPt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FromAtDofToAtQuadPt(FromAtDofToAtQuadPt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FromAtDofToAtQuadPt& operator=(const FromAtDofToAtQuadPt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FromAtDofToAtQuadPt& operator=(FromAtDofToAtQuadPt&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();


      private:
        //! Initial \a ParameterAtDof, from which we want to build a \a ParameterFromParameterAtDof.
        const param_at_dof_type& GetParamAtDof() const noexcept;


      private:
        //! Initial \a ParameterAtDof, from which we want to build a \a ParameterFromParameterAtDof.
        const param_at_dof_type& param_at_dof_;
    };


} // namespace MoReFEM::Internal::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/FromParameterAtDof/Internal/Local/FromAtDofToAtQuadPt.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_LOCAL_FROMATDOFTOATQUADPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
