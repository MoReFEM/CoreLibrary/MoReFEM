// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_LOCAL_FROMATDOFTOATQUADPT_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_LOCAL_FROMATDOFTOATQUADPT_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/Internal/Local/FromAtDofToAtQuadPt.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"


namespace MoReFEM::Internal::LocalParameterOperatorNS
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    const std::string& FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>::ClassName()
    {
        static const std::string ret =
            std::string("LocalParameterOperatorNS::FromAtDofToAtQuadPt<") + ::MoReFEM::ParameterNS::Name<TypeT>() + ">";
        return ret;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>::FromAtDofToAtQuadPt(
        const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
        elementary_data_type&& a_elementary_data,
        param_at_quad_pt_type& parameter_to_set,
        const ParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>& param_at_dof)
    : parent(a_unknown_storage, std::move(a_elementary_data), parameter_to_set), param_at_dof_(param_at_dof)
    { }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    inline const ParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>&
    FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>::GetParamAtDof() const noexcept
    {
        return param_at_dof_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    void FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>::ComputeEltArray()
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        auto& elementary_data = parent::GetNonCstElementaryData();
        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();
        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        decltype(auto) param_at_dof = GetParamAtDof();
        decltype(auto) param_to_set = parent::GetNonCstParameter();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            decltype(auto) value_from_param_at_dof = param_at_dof.GetValue(quad_pt, geom_elt);

            param_to_set.UpdateValue(quad_pt,
                                     geom_elt,
                                     [&value_from_param_at_dof](non_constant_reference value_to_set)
                                     {
                                         value_to_set = value_from_param_at_dof;
                                     });
        }
    }


} // namespace MoReFEM::Internal::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_LOCAL_FROMATDOFTOATQUADPT_DOT_HXX_
// *** MoReFEM end header guards *** < //
