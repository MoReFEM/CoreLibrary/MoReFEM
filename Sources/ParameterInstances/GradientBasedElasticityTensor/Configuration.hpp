// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_CONFIGURATION_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_CONFIGURATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"

#include "Geometry/StrongType.hpp"


namespace MoReFEM::ParameterNS
{


    //! This enum encompasses the possible configurations to consider in a GradientBasedElasticityTensor.
    enum class GradientBasedElasticityTensorConfiguration { dim1, dim2_plane_stress, dim2_plane_strain, dim3 };


    /*!
     * \brief Read the input data file to decide which configuration should be chosen for the
     * GradientBasedElasticityTensor.
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * \param[in] mesh_dimension DImension of the mesh.
     *
     * \return Enum that specified the configuration to use in the model.
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration
    ReadGradientBasedElasticityTensorConfigurationFromFile(const ::MoReFEM::GeometryNS::dimension_type mesh_dimension,
                                                           const MoReFEMDataT& morefem_data);


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_CONFIGURATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
