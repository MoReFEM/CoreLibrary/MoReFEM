// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_CONFIGURATION_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_CONFIGURATION_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::ParameterNS
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration
    ReadGradientBasedElasticityTensorConfigurationFromFile(const ::MoReFEM::GeometryNS::dimension_type dimension,
                                                           const MoReFEMDataT& morefem_data)
    {
        // First case: dimension is 3.
        if (dimension == ::MoReFEM::GeometryNS::dimension_type{ 3 })
        {
            return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3;
        } else if (dimension
                   == ::MoReFEM::GeometryNS::dimension_type{ 2 }) // If dimension is 2, read the kinematric parameter.
        {
            using Solid = InputDataNS::Solid;
            decltype(auto) str_kinematic_parameter =
                ::MoReFEM::InputDataNS::ExtractLeaf<Solid::PlaneStressStrain>(morefem_data);

            if (str_kinematic_parameter == "plane_strain")
                return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain;

            if (str_kinematic_parameter == "plane_stress")
                return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress;
        } else
        {
            return ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1;
        }

        assert(false && "OptionFile should have filtered out any other value!");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_CONFIGURATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
