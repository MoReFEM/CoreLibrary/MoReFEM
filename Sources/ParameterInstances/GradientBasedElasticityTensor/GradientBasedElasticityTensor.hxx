// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_GRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_GRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/GradientBasedElasticityTensor.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ParameterNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::GradientBasedElasticityTensor(
        const scalar_parameter_type& young_modulus,
        const scalar_parameter_type& poisson_ratio)
    : parent("Gradient-based elasticity tensor.", young_modulus.GetDomain()), young_modulus_(young_modulus),
      poisson_ratio_(poisson_ratio)
    {
        assert(parent::GetDomain() == poisson_ratio.GetDomain());

        // #926
        if (IsConstant())
            ComputeValue(young_modulus.GetConstantValue(), poisson_ratio.GetConstantValue());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    auto
    GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::SupplGetValue(const local_coords_type& local_coords,
                                                                               const GeometricElt& geom_elt) const
        -> return_type
    {
        const double young_modulus = GetYoungModulus().GetValue(local_coords, geom_elt);
        const double poisson_ratio = GetPoissonRatio().GetValue(local_coords, geom_elt);

        return ComputeValue(young_modulus, poisson_ratio);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    auto GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::GetYoungModulus() const
        -> const scalar_parameter_type&
    {
        return young_modulus_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    auto GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::GetPoissonRatio() const
        -> const scalar_parameter_type&
    {
        return poisson_ratio_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    bool GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::IsConstant() const
    {
        return GetYoungModulus().IsConstant() && GetPoissonRatio().IsConstant();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline auto
    GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::ComputeValue(const double young_modulus,
                                                                              const double poisson_ratio) const
        -> return_type
    {
        return helper_.Compute(young_modulus, poisson_ratio);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    auto GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::SupplGetConstantValue() const -> return_type
    {
        assert(IsConstant());

        return helper_.GetResult();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    void GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::SupplWrite(std::ostream& stream) const
    {
        stream << "# This matrix parameter is defined from Young modulus and Poisson ratio, which values are defined "
                  "the "
                  "following way:"
               << std::endl;
        stream << std::endl;
        GetYoungModulus().Write(stream);
        stream << std::endl;
        GetPoissonRatio().Write(stream);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    void GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::SupplTimeUpdate()
    {
        // #926
        assert(!GetYoungModulus().IsTimeDependent());
        assert(!GetPoissonRatio().IsTimeDependent());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    void GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::SupplTimeUpdate([[maybe_unused]] double time)
    {
        // #926
        assert(!GetYoungModulus().IsTimeDependent());
        assert(!GetPoissonRatio().IsTimeDependent());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    typename GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::return_type
    GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::SupplGetAnyValue() const
    {
        return helper_.GetResult();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline void
    GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>::SetConstantValue([[maybe_unused]] value_type value)
    {
        assert(false);
        exit(EXIT_FAILURE);
    }

} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_GRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
