// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
// IWYU pragma: no_include <type_traits>

#include "Utilities/Numeric/Numeric.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Internal::ParameterNS
{


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    template<>
    auto ComputeGradientBasedElasticityTensor<
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain>::
        ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio)
            -> const engineering_matrix_type&
    {
        assert(!NumericNS::AreEqual(poisson_ratio, -1.));
        assert(!NumericNS::AreEqual(poisson_ratio, 0.5));

        const double inv = 1. / (1. - poisson_ratio);

        auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

        const double factor = young_modulus * (1. - poisson_ratio) / ((1. + poisson_ratio) * (1. - 2 * poisson_ratio));

        engineering_elasticity_tensor(0, 0) = factor;
        engineering_elasticity_tensor(1, 1) = factor;
        engineering_elasticity_tensor(2, 2) = 0.5 * inv * factor * (1. - 2. * poisson_ratio);
        engineering_elasticity_tensor(0, 1) = factor * poisson_ratio * inv;
        engineering_elasticity_tensor(1, 0) = engineering_elasticity_tensor(0, 1);

        return engineering_elasticity_tensor;
    }


    template<>
    auto ComputeGradientBasedElasticityTensor<
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress>::
        ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio)
            -> const engineering_matrix_type&
    {
        assert(!NumericNS::AreEqual(poisson_ratio, 1.));
        assert(!NumericNS::IsZero(poisson_ratio));

        const double factor = young_modulus / (1. - NumericNS::Square(poisson_ratio));
        auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

        engineering_elasticity_tensor(0, 0) = factor;
        engineering_elasticity_tensor(1, 1) = factor;
        engineering_elasticity_tensor(2, 2) = 0.5 * factor * (1. - poisson_ratio);
        engineering_elasticity_tensor(0, 1) = factor * poisson_ratio;
        engineering_elasticity_tensor(1, 0) = engineering_elasticity_tensor(0, 1);

        return engineering_elasticity_tensor;
    }


    template<>
    auto
    ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3>::
        ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio)
            -> const engineering_matrix_type&
    {
        assert(!NumericNS::AreEqual(poisson_ratio, -1.));
        assert(!NumericNS::AreEqual(poisson_ratio, 0.5));

        const double factor = young_modulus * (1. - poisson_ratio) / ((1. + poisson_ratio) * (1. - 2. * poisson_ratio));

        auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

        engineering_elasticity_tensor(0, 0) = factor;
        engineering_elasticity_tensor(1, 1) = factor;
        engineering_elasticity_tensor(2, 2) = factor;

        const double inv = factor / (1. - poisson_ratio);

        engineering_elasticity_tensor(3, 3) = 0.5 * (1. - 2. * poisson_ratio) * inv;
        engineering_elasticity_tensor(4, 4) = engineering_elasticity_tensor(3, 3);
        engineering_elasticity_tensor(5, 5) = engineering_elasticity_tensor(3, 3);

        {
            const double non_diag_value = poisson_ratio * inv;

            for (auto i = Eigen::Index{}; i < 3; ++i)
            {
                for (auto j = i + 1; j < 3; ++j)
                    engineering_elasticity_tensor(i, j) = engineering_elasticity_tensor(j, i) = non_diag_value;
            }
        }

        return engineering_elasticity_tensor;
    }


    template<>
    auto
    ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1>::
        ComputeEngineeringElasticityTensor(const double young_modulus, [[maybe_unused]] const double poisson_ratio)
            -> const engineering_matrix_type&
    {
        assert(NumericNS::AreEqual(poisson_ratio, 0.));

        auto& engineering_elasticity_tensor = GetNonCstEngineeringElasticityTensor();

        engineering_elasticity_tensor(0, 0) = young_modulus;

        return engineering_elasticity_tensor;
    }
    // NOLINTEND(bugprone-easily-swappable-parameters)


} // namespace MoReFEM::Internal::ParameterNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
