// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"
#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Configuration.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    //! Helper class that actually computes the tensor given Young modulus and Poisson ratio.
    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    class ComputeGradientBasedElasticityTensor
    {
      private:
        //! Alias to traits class that enrich ConfigurationT.
        using traits = TraitsNS::Configuration<ConfigurationT>;

        //! Convenient alias.
        using engineering_matrix_type = ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>;

        //! Convenient alias.
        using result_matrix_type = ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<9>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit ComputeGradientBasedElasticityTensor();

        //! Destructor.
        ~ComputeGradientBasedElasticityTensor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ComputeGradientBasedElasticityTensor(const ComputeGradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ComputeGradientBasedElasticityTensor(ComputeGradientBasedElasticityTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ComputeGradientBasedElasticityTensor& operator=(const ComputeGradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ComputeGradientBasedElasticityTensor& operator=(ComputeGradientBasedElasticityTensor&& rhs) = delete;

        ///@}

        //! Compute the gradient based elasticity tensor and returns it.
        //! \param[in] young_modulus Young's modulus at a given spatial position.
        //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
        const result_matrix_type& Compute(double young_modulus, double poisson_ratio);

        /*!
         * \brief Returns the value of the gradient based elasticity tensor.
         *
         * Should not be called in non constant case (this is up to GradientBasedElasticityTensor class
         * to ensure that (except for GetAnyValue()); only this class is expected to manipulate present one).
         *
         * \return Value of the gradient based elasticity tensor.
         */
        const result_matrix_type& GetResult() const noexcept;

        //! Whether the tensor has already been computed. Meaningful only for spatially constant parameters.
        bool IsAlreadyComputed() const;


      private:
        //! Compute the Engineering elasticity tensor and returns it.
        //! \param[in] young_modulus Young's modulus at a given spatial position.
        //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
        const engineering_matrix_type& ComputeEngineeringElasticityTensor(double young_modulus, double poisson_ratio);

        //! Non constant access to engineering_elasticity_tensor_.
        engineering_matrix_type& GetNonCstEngineeringElasticityTensor();

        //! Non constant access to result_.
        result_matrix_type& GetNonCstResult();

      private:
        //! Storage of the matrix of interest.
        result_matrix_type result_;

        //! Engineering elasticity tensor.
        engineering_matrix_type engineering_elasticity_tensor_;
    };


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
