// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::ParameterNS { enum class GradientBasedElasticityTensorConfiguration; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ParameterNS
{


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    ComputeGradientBasedElasticityTensor<ConfigurationT>::ComputeGradientBasedElasticityTensor()
    {
        result_.resize(traits::result_size, traits::result_size);

        engineering_elasticity_tensor_.resize(traits::engineering_size, traits::engineering_size);

        engineering_elasticity_tensor_.setZero();
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline auto ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstResult() -> result_matrix_type&
    {
        return const_cast<result_matrix_type&>(GetResult());
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline auto ComputeGradientBasedElasticityTensor<ConfigurationT>::GetResult() const noexcept
        -> const result_matrix_type&
    {
        assert(result_.rows() == traits::result_size);
        assert(result_.cols() == traits::result_size);

        return result_;
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline auto ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstEngineeringElasticityTensor()
        -> engineering_matrix_type&
    {
        assert(engineering_elasticity_tensor_.rows() == traits::engineering_size);
        assert(engineering_elasticity_tensor_.cols() == traits::engineering_size);

        return engineering_elasticity_tensor_;
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    auto ComputeGradientBasedElasticityTensor<ConfigurationT>::Compute(const double young_modulus,
                                                                       const double poisson_ratio)
        -> const result_matrix_type&
    {
        const auto& engineering_elasticity_tensor = ComputeEngineeringElasticityTensor(young_modulus, poisson_ratio);

        const auto& gradient_2_strain = Gradient2Strain<traits::dimension>();

        auto& result = GetNonCstResult();
        result.noalias() =
            Gradient2Strain<traits::dimension>().transpose() * engineering_elasticity_tensor * gradient_2_strain;

        return result_;
    }


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
