// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_CONFIGURATION_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_CONFIGURATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"


namespace MoReFEM::Internal::ParameterNS::TraitsNS
{


    /*!
     * \brief Purpose of this traits struct is to provide few enum values depending of the configuration.
     *
     * \tparam Configuration Configuration to consider in the GradientBasedElasticityTensor (plane stress,
     * plain strain or 3d).
     */
    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    struct Configuration;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    template<>
    struct Configuration<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1>
    {
        enum { dimension = 1, engineering_size = 1, result_size = 1 };
    };


    template<>
    struct Configuration<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain>
    {
        enum { dimension = 2, engineering_size = 3, result_size = 4 };
    };


    template<>
    struct Configuration<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress>
    {
        enum { dimension = 2, engineering_size = 3, result_size = 4 };
    };


    template<>
    struct Configuration<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3>
    {
        enum { dimension = 3, engineering_size = 6, result_size = 9 };
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::ParameterNS::TraitsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_CONFIGURATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
