// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"


namespace MoReFEM::Internal::ParameterNS
{

    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    namespace // anonymous
    {

        ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix Gradient2Strain_1D()
        {
            ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix ret(1, 1);
            ret(0, 0) = 1.;

            return ret;
        }


        ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix Gradient2Strain_2D()
        {
            ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix ret(3, 4);
            ret.setZero();

            ret(0, 0) = 1.;
            ret(1, 3) = 1.;
            ret(2, 1) = 1.;
            ret(2, 2) = 1.;

            return ret;
        }


        ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix Gradient2Strain_3D()
        {
            ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix ret(6, 9);
            ret.setZero();

            ret(0, 0) = 1.;
            ret(1, 4) = 1.;
            ret(2, 8) = 1.;
            ret(3, 1) = 1.;
            ret(3, 3) = 1.;
            ret(4, 5) = 1.;
            ret(4, 7) = 1.;
            ret(5, 2) = 1.;
            ret(5, 6) = 1.;

            return ret;
        }


    } // namespace
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

    template<>
    const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& Gradient2Strain<1>()
    {
        static auto ret = Gradient2Strain_1D();
        return ret;
    };

    template<>
    const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& Gradient2Strain<2>()
    {
        static auto ret = Gradient2Strain_2D();
        return ret;
    };


    template<>
    const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& Gradient2Strain<3>()
    {
        static auto ret = Gradient2Strain_3D();
        return ret;
    };


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
