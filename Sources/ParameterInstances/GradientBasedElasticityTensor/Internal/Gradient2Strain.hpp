// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_GRADIENT2STRAIN_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_GRADIENT2STRAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    /*!
     * \brief Matrix defined so that strain = Gradient2Strain x gradient(displacement)
     *
     * No definition provided on purpose: this function is intended to be specialized.
     *
     * \tparam DimensionT Dimension of the mesh in which Parameter is defined.
     *
     * \return Matrix defined so that strain = Gradient2Strain x gradient(displacement)
     */
    template<int DimensionT>
    const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& Gradient2Strain();


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================

    template<>
    const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& Gradient2Strain<1>();

    template<>
    const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& Gradient2Strain<2>();

    template<>
    const ::MoReFEM::Wrappers::EigenNS::DerivateGreenLagrangeMatrix& Gradient2Strain<3>();

    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_GRADIENT2STRAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
