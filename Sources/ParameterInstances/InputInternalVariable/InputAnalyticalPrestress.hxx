// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_INPUTINTERNALVARIABLE_INPUTANALYTICALPRESTRESS_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_INPUTINTERNALVARIABLE_INPUTANALYTICALPRESTRESS_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/InputInternalVariable/InputAnalyticalPrestress.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    InputAnalyticalPrestress::InputAnalyticalPrestress(const MoReFEMDataT& morefem_data, const Domain& domain)
    : initial_value_internal_variable_(
          ::MoReFEM::InputDataNS::ExtractLeaf<
              ::MoReFEM::InputDataNS::AnalyticalPrestress::InitialCondition::ActiveStress>(morefem_data))
    {
        using type = ::MoReFEM::InputDataNS::AnalyticalPrestress::Contractility;

        contractility_ = InitScalarParameterFromInputData<type>("Contractility", domain, morefem_data);
    };


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto InputAnalyticalPrestress::GetContractility() const noexcept -> const scalar_parameter_type&
    {
        assert(!(!contractility_));
        return *contractility_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double InputAnalyticalPrestress::GetInitialValueInternalVariable() const noexcept
    {
        return initial_value_internal_variable_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_INPUTINTERNALVARIABLE_INPUTANALYTICALPRESTRESS_DOT_HXX_
// *** MoReFEM end header guards *** < //
