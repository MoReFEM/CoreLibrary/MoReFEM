// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMELAMBDA_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMELAMBDA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>
#include <limits>

#include "Geometry/Coords/LocalCoords.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ParameterNS
{


    /*!
     * \class doxygen_hide_lame_coefficient_from_young_and_poisson_warning
     *
     * This is present here as an example of a \a Parameter defined from others; avoid using it
     * if possible as using directly a \a Parameter with lame coefficient inside would be more
     * efficient.
     */


    /*!
     * \brief Yields Lame lambda coefficient from Young modulus and Poisson ratio.
     *
     * \copydoc doxygen_hide_parameter_without_time_dependency
     *
     * \copydoc doxygen_hide_lame_coefficient_from_young_and_poisson_warning
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class LameLambda final : public ScalarParameter<TimeManagerT, TimeDependencyNS::None>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LameLambda;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

        //! Alias to scalar parameter.
        using scalar_parameter_type = ScalarParameter<TimeManagerT, TimeDependencyNS::None>;

        //! Alias to parent.
        using parent = scalar_parameter_type;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to the return type (which is typically \a value_type with possible addition of const and reference),
        //! 'inherited' from parent class,
        using return_type = typename parent::return_type;

        //! Alias to the value type, 'inherited' from parent class,
        using value_type = typename parent::value_type;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] young_modulus Young's modulus.
         * \param[in] poisson_ratio Poisson ratio.
         */
        explicit LameLambda(const scalar_parameter_type& young_modulus, const scalar_parameter_type& poisson_ratio);

        //! Destructor.
        ~LameLambda() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        LameLambda(const LameLambda& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LameLambda(LameLambda&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LameLambda& operator=(const LameLambda& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LameLambda& operator=(LameLambda&& rhs) = delete;

        ///@}

        /*!
         * \brief Enables to modify the constant value of a parameter. Disabled for this Parameter.
         */
        void SetConstantValue(value_type) override;

      private:
        //! \copydoc doxygen_hide_parameter_suppl_get_value_local_coords
        return_type SupplGetValue(const local_coords_type& local_coords, const GeometricElt& geom_elt) const override;

        /*!
         * \brief Whether the parameter varies spatially or not.
         */
        bool IsConstant() const override;

        //! Young modulus.
        const scalar_parameter_type& GetYoungModulus() const;

        //! Poisson coefficient.
        const scalar_parameter_type& GetPoissonRatio() const;

        //! Compute the current value given the values of Young modulus and Poisson ratio.
        //! \param[in] young_modulus Young's modulus at a given spatial position.
        //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
        return_type ComputeValue(const double young_modulus, const double poisson_ratio) const;


        //! \copydoc doxygen_hide_parameter_suppl_get_constant_value
        return_type SupplGetConstantValue() const override;


        /*!
         * \brief Write the content of the Parameter in a stream.

         * \copydoc doxygen_hide_stream_inout
         */
        void SupplWrite(std::ostream& stream) const override;


        //! \copydoc doxygen_hide_parameter_suppl_time_update
        void SupplTimeUpdate() override;


        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        void SupplTimeUpdate(double time) override;

        /*!
         *
         * \copydoc doxygen_hide_parameter_suppl_get_any_value
         */
        return_type SupplGetAnyValue() const override;

      private:
        //! Young modulus.
        const scalar_parameter_type& young_modulus_;

        //! Poisson coefficient.
        const scalar_parameter_type& poisson_ratio_;

        //! Constant value. Irrelevant if IsConstant = false (the default value is in this case kept).
        double constant_value_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/LameCoefficientsFromYoungAndPoisson/LameLambda.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMELAMBDA_DOT_HPP_
// *** MoReFEM end header guards *** < //
