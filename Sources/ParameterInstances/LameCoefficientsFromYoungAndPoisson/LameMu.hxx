// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMEMU_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMEMU_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/LameCoefficientsFromYoungAndPoisson/LameMu.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstdlib>
#include <limits>
// IWYU pragma: no_include <type_traits>

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ParameterNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto LameMu<TimeManagerT>::SupplGetValue(const LocalCoords& local_coords, const GeometricElt& geom_elt) const
        -> return_type
    {
        const double young_modulus = GetYoungModulus().GetValue(local_coords, geom_elt);
        const double poisson_ratio = GetPoissonRatio().GetValue(local_coords, geom_elt);

        return ComputeValue(young_modulus, poisson_ratio);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto LameMu<TimeManagerT>::GetYoungModulus() const -> const scalar_parameter_type&
    {
        return young_modulus_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto LameMu<TimeManagerT>::GetPoissonRatio() const -> const scalar_parameter_type&
    {
        return poisson_ratio_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool LameMu<TimeManagerT>::IsConstant() const
    {
        return GetYoungModulus().IsConstant() && GetPoissonRatio().IsConstant();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline typename LameMu<TimeManagerT>::return_type
    LameMu<TimeManagerT>::ComputeValue(const double young_modulus, const double poisson_ratio) const
    {
        assert(!NumericNS::AreEqual(poisson_ratio, -1.));
        return young_modulus * 0.5 / (1. + poisson_ratio);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline typename LameMu<TimeManagerT>::return_type LameMu<TimeManagerT>::SupplGetConstantValue() const
    {
        assert(IsConstant());
        assert(!NumericNS::AreEqual(constant_value_, std::numeric_limits<double>::lowest()));
        return constant_value_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void LameMu<TimeManagerT>::SetConstantValue([[maybe_unused]] value_type value)
    {
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    LameMu<TimeManagerT>::LameMu(const scalar_parameter_type& young_modulus, const scalar_parameter_type& poisson_ratio)
    : scalar_parameter_type("Lame coefficient 'mu'", young_modulus.GetDomain()), young_modulus_(young_modulus),
      poisson_ratio_(poisson_ratio)
    {
        assert(young_modulus.GetDomain() == poisson_ratio.GetDomain());

        if (IsConstant())
            constant_value_ = ComputeValue(young_modulus.GetConstantValue(), poisson_ratio.GetConstantValue());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LameMu<TimeManagerT>::SupplWrite(std::ostream& stream) const
    {
        stream << "# Lame lambda is defined from Young modulus and Poisson ratio, which values are defined the "
                  "following way:"
               << std::endl;
        GetYoungModulus().Write(stream);
        GetPoissonRatio().Write(stream);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LameMu<TimeManagerT>::SupplTimeUpdate()
    {
        assert(!GetYoungModulus().IsTimeDependent());
        assert(!GetPoissonRatio().IsTimeDependent());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LameMu<TimeManagerT>::SupplTimeUpdate([[maybe_unused]] double time)
    {
        assert(!GetYoungModulus().IsTimeDependent());
        assert(!GetPoissonRatio().IsTimeDependent());
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    auto LameMu<TimeManagerT>::SupplGetAnyValue() const -> return_type
    {
        return 0.;
    }


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMEMU_DOT_HXX_
// *** MoReFEM end header guards *** < //
