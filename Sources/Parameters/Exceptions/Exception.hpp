// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_PARAMETERS_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp" // IWYU pragma: export

#include "Geometry/Domain/UniqueId.hpp" // IWYU pragma: export


namespace MoReFEM::ExceptionNS::ParameterNS
{

    /*!
     * \brief Thrown when the dimension of a vectorial \a Parameter doesn't match the content.
     *
     */
    class InconsistentVectorDimension final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] parameter_name Parameter for which the issue arose.
         * \param[in] expected_dimension Expected dimension of the vector.
         * \param[in] Nelts_read Number of elements that were actually read.
         * \copydoc doxygen_hide_source_location

         */
        explicit InconsistentVectorDimension(std::string_view parameter_name,
                                             Eigen::Index expected_dimension,
                                             std::size_t Nelts_read,
                                             const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentVectorDimension() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentVectorDimension(const InconsistentVectorDimension& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentVectorDimension(InconsistentVectorDimension&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentVectorDimension& operator=(const InconsistentVectorDimension& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentVectorDimension& operator=(InconsistentVectorDimension&& rhs) = delete;
    };


    /*!
     * \brief Thrown when the dimension of a vectorial \a Parameter doesn't match the content.
     *
     * This class is very similar to \a InconsistentVectorDimension but concerns the vector dimension for a specific \a
     * Domain in the case of a piecewise_constant_by_domain \a Parameter.
     */
    class InconsistentVectorDimensionForDomain final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] parameter_name Parameter for which the issue arose.
         * \param[in] domain_id Identifier of the \a Domain for which the associated matrix is not of the right
         dimension.
         * \param[in] expected_dimension Expected dimension of the vector
         * \param[in] Nelts_read Number of elements that were actually read.
         * \copydoc doxygen_hide_source_location
         */
        explicit InconsistentVectorDimensionForDomain(
            std::string_view parameter_name,
            ::MoReFEM::DomainNS::unique_id domain_id,
            Eigen::Index expected_dimension,
            std::size_t Nelts_read,
            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentVectorDimensionForDomain() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentVectorDimensionForDomain(const InconsistentVectorDimensionForDomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentVectorDimensionForDomain(InconsistentVectorDimensionForDomain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentVectorDimensionForDomain& operator=(const InconsistentVectorDimensionForDomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentVectorDimensionForDomain& operator=(InconsistentVectorDimensionForDomain&& rhs) = delete;
    };


    /*!
     * \brief Thrown when a Lua function parameter is attempted for a non scalar \a Parameter.
     *
     */
    class NoLuaFunctionForVectorialOrMatricial final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] parameter_name Parameter for which the issue arose.
         * \copydoc doxygen_hide_source_location
         */
        explicit NoLuaFunctionForVectorialOrMatricial(
            std::string_view parameter_name,
            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~NoLuaFunctionForVectorialOrMatricial() override;

        //! \copydoc doxygen_hide_copy_constructor
        NoLuaFunctionForVectorialOrMatricial(const NoLuaFunctionForVectorialOrMatricial& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NoLuaFunctionForVectorialOrMatricial(NoLuaFunctionForVectorialOrMatricial&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NoLuaFunctionForVectorialOrMatricial& operator=(const NoLuaFunctionForVectorialOrMatricial& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NoLuaFunctionForVectorialOrMatricial& operator=(NoLuaFunctionForVectorialOrMatricial&& rhs) = delete;
    };


    /*!
     * \brief Thrown when the vector representing the dimension of a matrix in option file doesn't include exactly two elements.
     *
     */
    class InvalidMatrixDimensionInOptionFile final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] parameter_name Parameter for which the issue arose.
         * \param[in] matrix_dimension Matrix dimension as read in the input data file.
         * \copydoc doxygen_hide_source_location
         */
        explicit InvalidMatrixDimensionInOptionFile(
            std::string_view parameter_name,
            const std::vector<Eigen::Index>& matrix_dimension,
            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InvalidMatrixDimensionInOptionFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidMatrixDimensionInOptionFile(const InvalidMatrixDimensionInOptionFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InvalidMatrixDimensionInOptionFile(InvalidMatrixDimensionInOptionFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidMatrixDimensionInOptionFile& operator=(const InvalidMatrixDimensionInOptionFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InvalidMatrixDimensionInOptionFile& operator=(InvalidMatrixDimensionInOptionFile&& rhs) = delete;
    };


    /*!
     * \brief Thrown when the dimension of a vectorial \a Parameter doesn't match the content.
     *
     */
    class InconsistentMatrixDimension final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] parameter_name Parameter for which the issue arose.
         * \param[in] expected_dimension Expected dimension of the matrix - this vector is expected to be of size 2,
         which
         * first entry representing the number of rows and second one the number of columns.
         * \param[in] Nelts_read Number of elements that were actually read.
         * \copydoc doxygen_hide_source_location

         */
        explicit InconsistentMatrixDimension(std::string_view parameter_name,
                                             const std::vector<Eigen::Index>& expected_dimension,
                                             std::size_t Nelts_read,
                                             const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentMatrixDimension() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentMatrixDimension(const InconsistentMatrixDimension& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentMatrixDimension(InconsistentMatrixDimension&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentMatrixDimension& operator=(const InconsistentMatrixDimension& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentMatrixDimension& operator=(InconsistentMatrixDimension&& rhs) = delete;
    };


    /*!
     * \brief Thrown when the dimension of a matricial \a Parameter doesn't match the content.
     *
     * This class is very similar to \a InconsistentMatrixDimension but concerns the matrix dimension for a specific \a
     * Domain in the case of a piecewise_constant_by_domain \a Parameter.
     */
    class InconsistentMatrixDimensionForDomain final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] parameter_name Parameter for which the issue arose.
         * \param[in] domain_id Identifier of the \a Domain for which the associated matrix is not of the right
         dimension.
         * \param[in] expected_dimension Expected dimension of the matrix - this vector is expected to be of size 2,
         which
         * first entry representing the number of rows and second one the number of columns.
         * \param[in] Nelts_read Number of elements that were actually read.
         * \copydoc doxygen_hide_source_location

         */
        explicit InconsistentMatrixDimensionForDomain(
            std::string_view parameter_name,
            ::MoReFEM::DomainNS::unique_id domain_id,
            const std::vector<Eigen::Index>& expected_dimension,
            std::size_t Nelts_read,
            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentMatrixDimensionForDomain() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentMatrixDimensionForDomain(const InconsistentMatrixDimensionForDomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentMatrixDimensionForDomain(InconsistentMatrixDimensionForDomain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentMatrixDimensionForDomain& operator=(const InconsistentMatrixDimensionForDomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentMatrixDimensionForDomain& operator=(InconsistentMatrixDimensionForDomain&& rhs) = delete;
    };

    /*!
     * \brief Thrown when a 3D Compound Parameter features "ignore" only for some components.
     *
     */
    class PartialIgnoredCompound final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] parameter_name Parameter for which the issue arose.
         * \param[in] nature_list Content of the nature field in the input data file.
         * \copydoc doxygen_hide_source_location

         */
        explicit PartialIgnoredCompound(std::string_view parameter_name,
                                        const std::vector<std::string>& nature_list,
                                        const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~PartialIgnoredCompound() override;

        //! \copydoc doxygen_hide_copy_constructor
        PartialIgnoredCompound(const PartialIgnoredCompound& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        PartialIgnoredCompound(PartialIgnoredCompound&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        PartialIgnoredCompound& operator=(const PartialIgnoredCompound& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        PartialIgnoredCompound& operator=(PartialIgnoredCompound&& rhs) = delete;
    };


} // namespace MoReFEM::ExceptionNS::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
