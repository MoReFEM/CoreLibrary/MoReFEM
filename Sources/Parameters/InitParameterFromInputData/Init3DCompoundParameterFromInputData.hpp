// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INIT3DCOMPOUNDPARAMETERFROMINPUTDATA_DOT_HPP_
#define MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INIT3DCOMPOUNDPARAMETERFROMINPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/MoReFEMData/Advanced/Concept.hpp" // IWYU pragma: export

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"
#include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Init a three dimensional parameter, e.g. a source.
     *
     * This is handled actually as three separate \a ScalarParameter, which nature may change: one might be defined
     * by a Lua function and another one by a constant.
     *
     * \param[in] name Name of the parameter, as it will appear in outputs.
     * \copydoc doxygen_hide_morefem_data_arg_in
     *
     * \copydoc doxygen_hide_parameter_domain_arg
     *
     * If any of the nature of the component is 'ignore', all components should be as well. In this case, nullptr is
     * returned.
     *
     * \return Parameter considered (or nullptr if nature is 'ignore').
     */
    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename Parameter<ParameterNS::Type::vector,
                       LocalCoords,
                       typename MoReFEMDataT::time_manager_type,
                       TimeDependencyT>::unique_ptr
    Init3DCompoundParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INIT3DCOMPOUNDPARAMETERFROMINPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
