// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INIT3DCOMPOUNDPARAMETERFROMINPUTDATA_DOT_HXX_
#define MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INIT3DCOMPOUNDPARAMETERFROMINPUTDATA_DOT_HXX_
// IWYU pragma: private, include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"
// *** MoReFEM header guards *** < //


#include "Core/MoReFEMData/Extract.hpp"

#include "Parameters/Exceptions/Exception.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    auto Init3DCompoundParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data) ->
        typename Parameter<ParameterNS::Type::vector,
                           LocalCoords,
                           typename MoReFEMDataT::time_manager_type,
                           TimeDependencyT>::unique_ptr
    {
        decltype(auto) nature_vector = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Nature>(morefem_data);

        assert(nature_vector.size() == 3UL);
        const auto& nature_x = nature_vector[0];
        const auto& nature_y = nature_vector[1];
        const auto& nature_z = nature_vector[2];

        if ((nature_x == "ignore" || nature_y == "ignore" || nature_z == "ignore")
            && (nature_x != nature_y || nature_x != nature_z))

            throw ExceptionNS::ParameterNS::PartialIgnoredCompound(SectionT::GetName(), nature_vector);

        if (nature_x == "ignore")
            return nullptr;

        decltype(auto) value_vector = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Value>(morefem_data);
        assert(value_vector.size() == 3UL);

        using time_manager_type = typename MoReFEMDataT::time_manager_type;

        auto&& component_x = InitScalarParameterFromInputData<time_manager_type, ParameterNS::TimeDependencyNS::None>(
            "Value X", domain, nature_x, value_vector[0]);

        auto&& component_y = InitScalarParameterFromInputData<time_manager_type, ParameterNS::TimeDependencyNS::None>(
            "Value Y", domain, nature_y, value_vector[1]);

        auto&& component_z = InitScalarParameterFromInputData<time_manager_type, ParameterNS::TimeDependencyNS::None>(
            "Value Z", domain, nature_z, value_vector[2]);

        using type = ParameterNS::ThreeDimensionalCoumpoundParameter<time_manager_type, TimeDependencyT>;

        return std::make_unique<type>(
            std::forward<StringT>(name), std::move(component_x), std::move(component_y), std::move(component_z));
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INIT3DCOMPOUNDPARAMETERFROMINPUTDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
