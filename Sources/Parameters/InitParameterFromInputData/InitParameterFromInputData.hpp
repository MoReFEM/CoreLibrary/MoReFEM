// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HPP_
#define MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/InputData/Advanced/Concept.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp" // IWYU pragma: export

#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"
#include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp"


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_parameter_init_facility_common
     *
     * \tparam TimeDependencyT Policy in charge of time dependency.
     *
     * \param[in] name Name of the parameter, as it will appear in outputs.

     * \copydoc doxygen_hide_parameter_domain_arg
     *
     * \tparam SectionT The type of the section from \a InputData that includes the data for the
     * \a Parameter to build.
     *
     * \attention Please notice all \a Parameter can't be initialized from the input data file. For instance a
     * parameter defined directly at quadrature points or at dofs must be built in hard in the code; current function
     * enables to choose among certain pre-definite choices (at the time of this writing, 'constant', 'lua function' and
     * 'piecewise constant by domain').
     *
     * \return Parameter considered, or nullptr if irrelevant (i.e. naure was 'ignore').
     */


    /*!
     * \brief Build a \a ScalarParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     *
     * \param[in] nature Value of 'nature' field (read from input data file)
     * \param[in] value Content of 'value' field (read from input data file).
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT
    >
    // clang-format on
    typename ScalarParameter<TimeManagerT, TimeDependencyT>::unique_ptr InitScalarParameterFromInputData(
        StringT&& name,
        const Domain& domain,
        const std::string& nature,
        const Internal::ParameterNS::Traits<ParameterNS::Type::scalar>::variant_type& value);


    /*!
     * \brief Build a \a ScalarParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     */
    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename ScalarParameter<typename MoReFEMDataT::time_manager_type, TimeDependencyT>::unique_ptr
    InitScalarParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


    /*!
     * \brief Build a \a VectorialParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     */
    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename VectorialParameter<typename MoReFEMDataT::time_manager_type, TimeDependencyT>::unique_ptr
    InitVectorialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


    /*!
     * \brief Build a \a VectorialParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     *
     * \copydoc doxygen_hide_morefem_data_arg_in
     */
    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename MatricialParameter<typename MoReFEMDataT::time_manager_type, TimeDependencyT>::unique_ptr
    InitMatricialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
