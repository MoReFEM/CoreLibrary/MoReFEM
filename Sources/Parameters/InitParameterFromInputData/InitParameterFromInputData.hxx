// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HXX_
#define MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HXX_
// IWYU pragma: private, include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
// *** MoReFEM header guards *** < //


#include "Core/MoReFEMData/Extract.hpp"

#include "Parameters/Exceptions/Exception.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT
    >
    // clang-format on
    typename ScalarParameter<TimeManagerT, TimeDependencyT>::unique_ptr InitScalarParameterFromInputData(
        StringT&& name,
        const Domain& domain,
        const std::string& nature,
        const typename Internal::ParameterNS::Traits<ParameterNS::Type::scalar>::variant_type& value)
    {
        if (nature == "ignore")
            return nullptr;

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::scalar>;

        if (nature == "constant")
        {
            // clang-format off
            using parameter_type =
                Internal::ParameterNS::ParameterInstance
                <
                    ParameterNS::Type::scalar,
                    ::MoReFEM::ParameterNS::Policy::Constant,
                    TimeManagerT,
                    TimeDependencyT,
                    Internal::ParameterNS::DefaultStorage<ParameterNS::Type::scalar>
                >;
            // clang-format on

            using storage_type = traits::value_type;
            static_assert(std::is_same<storage_type, double>());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else if (nature == "lua_function")
        {
            // clang-format off
             using parameter_type =
                 Internal::ParameterNS::ParameterInstance
                 <
                     ParameterNS::Type::scalar,
                     ::MoReFEM::ParameterNS::Policy::SpatialFunctionGlobalCoords,
                     TimeManagerT,
                     TimeDependencyT,
                     Internal::ParameterNS::DefaultStorage<ParameterNS::Type::scalar>
                 >;
            // clang-format on

            using storage_type = ::MoReFEM::Wrappers::Lua::spatial_function;

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else if (nature == "piecewise_constant_by_domain")
        {
            // clang-format off
             using parameter_type =
                 Internal::ParameterNS::ParameterInstance
                 <
                     ParameterNS::Type::scalar,
                     ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                     TimeManagerT,
                     TimeDependencyT,
                     Internal::ParameterNS::DefaultStorage<ParameterNS::Type::scalar>
                 >;
            // clang-format on

            using storage_type = traits::piecewise_constant_by_domain_type;

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    auto InitScalarParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data) ->
        typename ScalarParameter<typename MoReFEMDataT::time_manager_type, TimeDependencyT>::unique_ptr
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Nature>(morefem_data);
        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Value>(morefem_data);

        return InitScalarParameterFromInputData<typename MoReFEMDataT::time_manager_type, TimeDependencyT>(
            name, domain, nature, value);
    }


    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename VectorialParameter<typename MoReFEMDataT::time_manager_type, TimeDependencyT>::unique_ptr
    InitVectorialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data)
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Nature>(morefem_data);

        if (nature == "ignore")
            return nullptr;

        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Value>(morefem_data);
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::VectorDimension>(morefem_data);

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::vector>;

        if (nature == "constant")
        {
            assert(std::holds_alternative<std::vector<double>>(value));

            decltype(auto) interpreted_value = std::get<std::vector<double>>(value);

            // clang-format off
            using parameter_type = 
            Internal::ParameterNS::ParameterInstance
            <
                ParameterNS::Type::vector,
                ::MoReFEM::ParameterNS::Policy::Constant,
                typename MoReFEMDataT::time_manager_type,
                TimeDependencyT,
                Internal::ParameterNS::DefaultStorage<ParameterNS::Type::vector>
            >;
            // clang-format on

            using storage_type = traits::value_type;

            if (interpreted_value.size() != static_cast<std::size_t>(dimension))
                throw ExceptionNS::ParameterNS::InconsistentVectorDimension(name, dimension, interpreted_value.size());

            storage_type as_local_vector;
            as_local_vector.resize(dimension);

            std::copy(interpreted_value.cbegin(), interpreted_value.cend(), as_local_vector.begin());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, as_local_vector);
        } else if (nature == "piecewise_constant_by_domain")
        {
            using variant_alternative_type = traits::piecewise_constant_in_lua_file_type;
            assert(std::holds_alternative<variant_alternative_type>(value));

            decltype(auto) interpreted_value = std::get<variant_alternative_type>(value);

            // clang-format off
            using parameter_type =
            Internal::ParameterNS::ParameterInstance
            <
                ParameterNS::Type::vector,
                ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                typename MoReFEMDataT::time_manager_type,
                TimeDependencyT,
                Internal::ParameterNS::DefaultStorage<ParameterNS::Type::vector>
            >;
            // clang-format on

            using storage_type = traits::piecewise_constant_by_domain_type;

            storage_type content;

            for (const auto& [domain_id, as_vector] : interpreted_value)
            {
                if (as_vector.size() != static_cast<std::size_t>(dimension))
                    throw ExceptionNS::ParameterNS::InconsistentVectorDimensionForDomain(
                        name, domain_id, dimension, as_vector.size());
                traits::value_type as_local_vector;
                as_local_vector.resize(dimension);

                std::copy(as_vector.cbegin(), as_vector.cend(), as_local_vector.begin());

                content.insert({ domain_id, as_local_vector });
            }

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, content);
        } else if (nature == "lua_function")
            throw ExceptionNS::ParameterNS::NoLuaFunctionForVectorialOrMatricial(name);
        else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    auto InitMatricialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data) ->
        typename MatricialParameter<typename MoReFEMDataT::time_manager_type, TimeDependencyT>::unique_ptr
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Nature>(morefem_data);

        if (nature == "ignore")
            return nullptr;

        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Value>(morefem_data);
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::MatrixDimension>(morefem_data);

        if (dimension.size() != 2UL)
            throw ExceptionNS::ParameterNS::InvalidMatrixDimensionInOptionFile(name, dimension);

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::matrix>;

        if (nature == "constant")
        {
            assert(std::holds_alternative<traits::constant_in_lua_file_type>(value));

            decltype(auto) interpreted_value = std::get<traits::constant_in_lua_file_type>(value);

            // clang-format off
            using parameter_type =
            Internal::ParameterNS::ParameterInstance
            <
                ParameterNS::Type::matrix,
                ::MoReFEM::ParameterNS::Policy::Constant,
                typename MoReFEMDataT::time_manager_type,
                TimeDependencyT,
                Internal::ParameterNS::DefaultStorage<ParameterNS::Type::matrix>
            >;
            // clang-format on

            if (static_cast<Eigen::Index>(interpreted_value.size()) != dimension[0] * dimension[1])
                throw ExceptionNS::ParameterNS::InconsistentMatrixDimension(name, dimension, interpreted_value.size());

            traits::value_type as_local_matrix;
            as_local_matrix.resize(dimension[0], dimension[1]);

            {
                auto current_index = 0UL;
                for (auto row = Eigen::Index{}; row < dimension[0]; ++row)
                    for (auto col = Eigen::Index{}; col < dimension[1]; ++col)
                        as_local_matrix(row, col) = interpreted_value[current_index++];
            }

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, as_local_matrix);
        } else if (nature == "piecewise_constant_by_domain")
        {
            using variant_alternative_type = traits::piecewise_constant_in_lua_file_type;
            assert(std::holds_alternative<variant_alternative_type>(value));

            decltype(auto) interpreted_value = std::get<variant_alternative_type>(value);

            // clang-format off
            using parameter_type =
            Internal::ParameterNS::ParameterInstance
            <
                ParameterNS::Type::matrix,
                ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                typename MoReFEMDataT::time_manager_type,
                TimeDependencyT,
                Internal::ParameterNS::DefaultStorage<ParameterNS::Type::matrix>
            >;
            // clang-format on


            using storage_type = traits::piecewise_constant_by_domain_type;

            storage_type content;

            for (const auto& [domain_id, as_std_vector] : interpreted_value)
            {
                if (static_cast<Eigen::Index>(as_std_vector.size()) != dimension[0] * dimension[1])
                    throw ExceptionNS::ParameterNS::InconsistentMatrixDimensionForDomain(
                        name, domain_id, dimension, as_std_vector.size());

                traits::value_type as_local_matrix;
                as_local_matrix.resize(dimension[0], dimension[1]);

                {
                    auto current_index = 0UL;
                    for (auto row = Eigen::Index{}; row < dimension[0]; ++row)
                        for (auto col = Eigen::Index{}; col < dimension[1]; ++col)
                            as_local_matrix(row, col) = as_std_vector[current_index++];
                }

                content.insert({ domain_id, as_local_matrix });
            }

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, content);
        } else if (nature == "lua_function")
            throw ExceptionNS::ParameterNS::NoLuaFunctionForVectorialOrMatricial(name);
        else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
