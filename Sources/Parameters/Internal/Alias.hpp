// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INTERNAL_ALIAS_DOT_HPP_
#define MOREFEM_PARAMETERS_INTERNAL_ALIAS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/Parameter/TypeEnum.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    //! Convenient alias to avoid many ::MoReFEM::ParameterNS prefix.
    using Type = ::MoReFEM::ParameterNS::Type;

    //! Convenient alias to avoid many ::MoReFEM::ParameterNS prefix.
    using GradientBasedElasticityTensorConfiguration =
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration;


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INTERNAL_ALIAS_DOT_HPP_
// *** MoReFEM end header guards *** < //
