// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_PARAMETER_DOT_HPP_
#define MOREFEM_PARAMETERS_PARAMETER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <fstream>
#include <functional>
#include <memory>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "Parameters/TimeDependency/None.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class ScalarParameterFromFile; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{

    /*!
     * \class doxygen_hide_parameter_type_tparam
     *
     * \tparam TypeT  Type of the parameter considered.
     * If 'scalar', the underlying storage is a double.
     * If 'vector', the underlying storage is an Eigen vector (typically Eigen::VectorXd).
     * If 'matrix', the underlying storage is an Eigen matfrix (typically Eigen::MatrixXd).
     */


    /*!
     * \brief Abstract class used to define a Parameter.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     *
     * \tparam LocalCoordsT This type describe the object at which the evaluation of the \a Parameter may occur. There
     * are two expected possibilities: \a LocalCoords and \a QuadraturePoint.
     *
     * \copydoc doxygen_hide_param_time_dependancy
     *
     * \copydoc doxygen_hide_tparam_parameter_concept_storage
     *
     * There are two distinct categories of \a Parameter:
     * - Those that are defined from the input data file. They might be constant, piece constant by domain or given
     * by an analytical Lua function, at the choice of the user of the model. They should be initialized through the
     * template functions provided in InitParameterFromInputData file (namely \a InitScalarParameterFromInputData
     * and \a Init3DCompoundParameterFromInputData).
     * - Parameters defined at dofs (that extract values from an underlying \a GlobalVector) or directly at quadrature
     * points, which are hardcoded in each model.
     *
     * Most of the instantiated classes actually derives from ParameterInstance, which itself derives from current
     * template class (but not all - see \a FiberList for a counter-example).
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>
    >
    // clang-format on
    class Parameter
    {

      public:
        static_assert(std::is_same<LocalCoordsT, QuadraturePoint>() || std::is_same<LocalCoordsT, LocalCoords>(),
                      "LocalCoordsT is expected to be one of these types");

        //! \copydoc doxygen_hide_alias_self
        using self = Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT, StorageT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to unique pointer to const object.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to array of unique pointer.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;

        //! Alias to traits.
        using traits = Internal::ParameterNS::Traits<TypeT, StorageT>;

        //! Alias to return type.
        using return_type = typename traits::return_type;

        //! Alias to storage type.
        using value_type = typename traits::value_type;

        //! Alias to time dependency.
        using time_dependency_type = TimeDependencyT<TypeT, TimeManagerT>;


      protected:
        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor.
         *
         * \param[in] name Name that will appear in outputs.
         * \copydoc doxygen_hide_parameter_domain_arg
         *
         * \tparam T Type of name, in forwarding reference idiom. It must be convertible to a std::string.
         *
         * \copydoc doxygen_hide_param_time_dependancy
         *
         */
        template<class T>
        explicit Parameter(T&& name, const Domain& domain);


      public:
        //! Destructor.
        virtual ~Parameter() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Parameter(const Parameter& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Parameter(Parameter&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Parameter& operator=(const Parameter& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Parameter& operator=(Parameter&& rhs) = delete;

        ///@}

      public:
        //! Apply the time dependency if relevant.
        virtual void TimeUpdate();

        /*!
         * \brief Apply the time dependency if relevant.
         *
         * One should prefer to use the default one if one wants to use the current time.
         * Extra security to verify the synchro of the parameter to the current time is done in he default one.
         * This method is for particular cases only when the user knows exactly what is he doing.
         *
         * \param[in] time Time for the update.
         */
        virtual void TimeUpdate(double time);

        /*!
         * \brief Set the time dependency functor.
         *
         * This is relevant only for TimeDependencyT != TimeDependencyNS::None.
         *
         * \param[in] time_dependency Unique pointer to the time dependency object to set.
         */
        void SetTimeDependency(typename time_dependency_type::unique_ptr&& time_dependency);


        /*!
         * \brief Get the value of the parameter at a given local position in a given \a geom_elt.
         *
         * \internal <b><tt>[internal]</tt></b> This method is actually called when IsConstant() yields false; if true
         * GetConstantValue() is called instead.
         * \endinternal
         *
         * \param[in] local_coords Local object at which the \a Parameter is evaluated.
         * \param[in] geom_elt \a GeometricElt inside which the value is computed.
         *
         * \return Value of the parameter.
         */
        return_type GetValue(const LocalCoordsT& local_coords, const GeometricElt& geom_elt) const;


        /*!
         * \brief Returns the constant value (if the parameter is constant).
         *
         * If not constant, an assert is raised (in debug mode).
         *
         * \return Constant value of the parameter.
         */
        return_type GetConstantValue() const;


        /*!
         * \brief Return a value that could be returned by the \a Parameter.
         *
         * The point here is not the value itself, but information about the type - typically this should be used when
         * \a Parameter stores a vector or a matrix and you want to query the size of it.
         *
         * \return An example of value stored by the \a Parameter.
         */
        return_type GetAnyValue() const;


        /*!
         * \brief Enables to modify the constant value of a parameter.
         *
         * \param[in] value Value to modify the constant value with.
         */
        virtual void SetConstantValue(value_type value) = 0;

        //! Whether the parameter varies spatially or not.
        virtual bool IsConstant() const = 0;

        /*!
         * \brief Write the content of the Parameter in a stream.
         *
         * In first draft the output format is up to the policy (maybe later we may prefer to write at all quadrature
         * points for all cases); the exact content is indeed defined in the virtual method SupplWrite(), to be defined
         * in each inherited classes.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Write(std::ostream& stream) const;

        /*!
         * \brief Write the content of the Parameter in a file.
         *
         * This method calls the namesake method that writes on a stream.
         *
         * \param[in] filename Path to the file in which value will be written. The path must be valid (all directories
         * must exist) and if a namesake already exists it is overwritten.
         */
        void Write(const FilesystemNS::File& filename) const;

        //! Returns the \a Domain upon which the parameter is defined.
        const Domain& GetDomain() const noexcept;

        //! Whether the class is time-dependent or not.
        static constexpr bool IsTimeDependent();

      public:
        /*!
         * \brief Constant accessor to the object which handles if relevant the time dependency (computation of
         * the time related factor, etc...).
         *
         * Shouldn't be called very often...
         *
         * \return Time dependency object.
         */
        const time_dependency_type& GetTimeDependency() const noexcept;


      protected:
        //! Name that will appear in outputs.
        const std::string& GetName() const;

      private:
        /*!
         * \brief Non constant accessor to the object which handles if relevant the time dependency
         * (computation of the time related factor, etc...).
         */
        time_dependency_type& GetNonCstTimeDependency() noexcept;


      private:
        /*!
         * \class doxygen_hide_parameter_suppl_get_constant_value
         *
         * \brief Returns the constant value (if the parameters is constant).
         *
         * If the \a Parameter gets a time dependency (which is of the form f(x) * g(t)), current method returns only
         * f(x). This method is expected to be called only in GetConstantValue() method, which adds up the g(t)
         * contribution \internal This choice also makes us respect a C++ idiom that recommends avoiding virtual public
         * methods. \endinternal
         *
         * \return Constant value of the \a Parameter. If the \a Parameter is not constant, this method should never be
         * called.
         */

        //! \copydoc doxygen_hide_parameter_suppl_get_constant_value
        virtual return_type SupplGetConstantValue() const = 0;


        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        virtual return_type SupplGetAnyValue() const = 0;


        /*!
         * \copydoc doxygen_hide_parameter_suppl_get_value
         * \param[in] local_coords Local object at which the \a Parameter is evaluated.
         */
        virtual return_type SupplGetValue(const LocalCoordsT& local_coords, const GeometricElt& geom_elt) const = 0;

        /*!
         * \brief Write the content of the Parameter in a stream.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        virtual void SupplWrite(std::ostream& stream) const = 0;

        //! \copydoc doxygen_hide_parameter_suppl_time_update
        virtual void SupplTimeUpdate() = 0;

      private:
        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        virtual void SupplTimeUpdate(double time) = 0;


      private:
        //! Name that will appear in outputs.
        std::string name_;

        //! Domain upon which the parameter is defined. Must be consistent with mesh.
        const Domain& domain_;

        /*!
         * \brief Object which handles if relevant the time dependency (computation of the time related factor,
         * etc...).
         *
         * May remain nullptr if the policy is there is no time dependency.
         */
        typename time_dependency_type::unique_ptr time_dependency_ = nullptr;
    };


    // clang-format off
    //! Convenient alias to define a scalar parameter.
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT = TimeManager<TimeManagerNS::Policy::Static>,
        template<ParameterNS::Type, class> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<ParameterNS::Type::scalar>
    >
    using ScalarParameter = Parameter<ParameterNS::Type::scalar, LocalCoords, TimeManagerT, TimeDependencyT, StorageT>;

    //! Convenient alias to define a vectorial parameter.
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT = TimeManager<TimeManagerNS::Policy::Static>,
        template<ParameterNS::Type, class> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<ParameterNS::Type::vector>
    >
    using VectorialParameter = Parameter<ParameterNS::Type::vector, LocalCoords, TimeManagerT, TimeDependencyT, StorageT>;

    //! Convenient alias to define a matricial parameter.
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT = TimeManager<TimeManagerNS::Policy::Static>,
        template<ParameterNS::Type, class> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<ParameterNS::Type::matrix>
    >
    using MatricialParameter = Parameter<ParameterNS::Type::matrix, LocalCoords, TimeManagerT, TimeDependencyT, StorageT>;
    // clang-format on

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Parameter.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_PARAMETER_DOT_HPP_
// *** MoReFEM end header guards *** < //
