// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_PARAMETERATDOF_DOT_HPP_
#define MOREFEM_PARAMETERS_PARAMETERATDOF_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Parameters/Internal/ParameterAtDof.hpp"
#include "Parameters/Policy/AtDof/AtDof.hpp"


namespace MoReFEM
{


    /*!
     *
     * \brief Parameter class which provides values at \a LocalCoords of quantities defined on dofs.
     *
     * \copydetails doxygen_hide_param_at_dof_and_unknown
     *
     * \copydoc doxygen_hide_parameter_at_dof_template_args
     *
     * Currently Doxygen doesn't seem able to display the interface of the underlying class, despite the
     * TYPEDEF_HIDES_STRUCT = YES in Doxygen file. You may see the full interface of the class (on complete Doxygen
     * documentation) at
     * Internal::ParameterNS::ParameterInstance except the constructor (due to variadic arguments); its prototype is:
     *
     * \code
     * ParameterAtDof(const Mesh& mesh,
     *                const FEltSpace& felt_space, // one such line for each \a NfeltSpaceT
     *                const Unknown& unknown,
     *                const GlobalVector& global_vector);
     * \endcode
     *
     * where:
     * \copydetails doxygen_hide_at_dof_impl_constructor_args
     * \param[in] felt_space \a FEltSpace upon which the Parameter may be evaluated.
     *
     * \attention Current implementation is not secure enough with respect of quadrature rule! \todo #1031
     *
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ::MoReFEM::ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>,
        Eigen::Index NfeltSpaceT = 1UL
    >
    // clang-format on
    using ParameterAtDof = typename Internal::ParameterNS::
        ParameterAtDofImpl<TypeT, TimeManagerT, TimeDependencyT, StorageT, NfeltSpaceT>::type;

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_PARAMETERATDOF_DOT_HPP_
// *** MoReFEM end header guards *** < //
