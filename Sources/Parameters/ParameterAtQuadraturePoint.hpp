// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_PARAMETERATQUADRATUREPOINT_DOT_HPP_
#define MOREFEM_PARAMETERS_PARAMETERATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/TimeManager/Instances.hpp"

#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"


namespace MoReFEM
{


    /*!
     * \brief Convenient alias for a parameter defined at dof.
     *
     * Currently Doxygen doesn't seem able to display the interface of the underlying class, despite the
     * TYPEDEF_HIDES_STRUCT = YES in Doxygen file. You may see the full interface of the class (on complete Doxygen
     * documentation) at
     * Internal::ParameterNS::ParameterInstance except the constructor (due to variadic arguments)
     *
     * \code
     * template<class StringT>
     * ParameterAtQuadraturePoint(T&& name,
     *                            const Mesh& mesh,
     *                            const QuadratureRulePerTopology& quadrature_rule_per_topology,
     *                            storage_value_type initial_value,
     *                            const TimeManagerT& time_manager);
     * \endcode
     *
     * where:
     * - mesh Mesh considered. It is actually unused for this kind Parameter, but required nonetheless
     * to conform to generic parameter interface.
     * - initial_value A scalar, vectorial or matrix initial value to create the parameter.
     * - time_manager The time manager of MoReFEM.
     *
     * \copydoc doxygen_hide_tparam_parameter_concept_storage
     *
     * \attention Current implementation is not secure enough with respect of quadrature rule! \todo #1031
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT = Internal::ParameterNS::DefaultStorage<TypeT>
    >
    // clang-format on
    using ParameterAtQuadraturePoint = Internal::ParameterNS::
        ParameterInstance<TypeT, ParameterNS::Policy::AtQuadraturePoint, TimeManagerT, TimeDependencyT, StorageT>;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_PARAMETERATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
