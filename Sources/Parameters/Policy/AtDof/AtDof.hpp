// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp" // \todo #704 Should probably disappear shortly!

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"

#ifndef NDEBUG
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#endif // NDEBUG


#include "Core/Parameter/Internal/Traits.hpp"

#include "Parameters/Policy/AtDof/Internal/AtDof.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class GeometricElt; }

namespace MoReFEM
{

    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    class FiberList;

}

namespace MoReFEM::ParameterNS
{

    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    struct FromParameterAtDof;
}

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ParameterNS::Policy
{


    /*!
     * \brief Parameter policy when the parameter is expressed at dofs.
     *
     * \copydetails doxygen_hide_at_dof_policy_tparam
     *
     * \copydetails doxygen_hide_param_at_dof_and_unknown
     *
     * \copydoc doxygen_hide_tparam_parameter_concept_storage
     *
     * \attention This policy can't be used directly within ParameterInstance: ParameterInstance expects
     * only one template argument, whereas here two are displayed. You should look at \a ParameterAtDof
     * struct to see how to work around this issue (and you shouldn't anyway have to use current policy
     * directly: that's the whole point of \a ParameterAtDof...).
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT = 1
    >
    // clang-format on
    class AtDof
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

      private:
        //! Alias to traits class related to TypeT.
        using traits = Internal::ParameterNS::Traits<TypeT, StorageT>;

      public:
        static_assert(TypeT != ParameterNS::Type::matrix, "Irrelevant for this type of parameter.");


        //! Returns numbers of \a FEltSpace in the \a Parameter.
        static constexpr std::size_t NfeltSpace() noexcept;

        //! Alias to the return type.
        using return_type = typename traits::return_type;

        //! Alias.
        using value_type = std::decay_t<return_type>;

        //! Friendship to fiber manager.
        // clang-format off
        template
        <
            FiberNS::AtNodeOrAtQuadPt FiberPolicyTT,
            ParameterNS::Type TypeTT,
            TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT,
            template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD>
            class TimeDependencyTT,
            ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageTT
        >
        // clang-format on
        friend class FiberList;

        //! Frienship to the only class that needs underlying list of \a FEltSpace.
        // clang-format off
        template
        <
            ParameterNS::Type TypeTT,
            TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT,
            template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD>
            class TimeDependencyTT,
            ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageTT,
            Eigen::Index NfeltSpaceTT
        >
        // clang-format on
        friend struct ::MoReFEM::ParameterNS::FromParameterAtDof;

        static_assert(ParameterNS::Type::matrix != TypeT, "This type of parameter can't deal with Matrix parameter");


      public:
        /// \name Special members.
        ///@{

        /*!
         * \class doxygen_hide_at_dof_impl_constructor_args
         *
         * \param[in] name Name of the Parameter.
         * \copydoc doxygen_hide_parameter_domain_arg
         *  For this specific polcy the \a Domain is mostly left unused except for toms sanity checks in debug
         * mode.
         *
         * \param[in] unknown A scalar or vectorial unknown that acts a bit as a strawman: dofs are defined only
         * in relationship to an unknown, so you must create one if none fulfill your purposes (for instance
         * if you deal with a vectorial unknown and need a scalar Dof field, you must create another unknown
         * only for their \a Parameter). To save space, it's better if this unknown is in its own numbering
         * subset, but this is not mandatory.
         * \param[in] global_vector The vector which includes the actual values at the dofs. The values
         * at dofs may evolve should this vector change.
         *
         */


        /*!
         * \brief Constructor when Parameter cover only one \a FEltSpace.
         *
         * This should be the more frequent case.
         *
         * \copydetails doxygen_hide_at_dof_impl_constructor_args
         * \param[in] felt_space Finite element space that covers the area upon which the parameter should be
         * defined. This finite element space should cover the underlying \a NumberingSubset of \a
         * global_vector.
         *
         */
        explicit AtDof(const std::string& name,
                       const Domain& domain,
                       const FEltSpace& felt_space,
                       const Unknown& unknown,
                       const GlobalVector& global_vector);


        /*!
         * \brief Constructor when Parameter cover two \a FEltSpace: one for mesh dimension and another for
         * the dimension immediately below.
         *
         * \copydetails doxygen_hide_at_dof_impl_constructor_args
         *
         * \param[in] felt_space_dim Finite element space that covers the area upon which the parameter should
         * be defined for the dimension of the mesh. This finite element space should cover the underlying \a
         * NumberingSubset of \a global_vector. \param[in] felt_space_dim_minus_1 Same as \a felt_space_dim
         * except it covers the dimension minus 1 (e.g. finite element space that cover borders on a 2D mesh).
         *
         */
        explicit AtDof(const std::string& name,
                       const Domain& domain,
                       const FEltSpace& felt_space_dim,
                       const FEltSpace& felt_space_dim_minus_1,
                       const Unknown& unknown,
                       const GlobalVector& global_vector);


        /*!
         * \brief Constructor when Parameter cover three \a FEltSpace: one for mesh dimension, one for dimension
         * minus 1 and the last for dimension minus 2.
         *
         * \copydetails doxygen_hide_at_dof_impl_constructor_args
         *
         * \param[in] felt_space_dim Finite element space that covers the area upon which the parameter should
         * be defined for the dimension of the mesh. This finite element space should cover the underlying \a
         * NumberingSubset of \a global_vector. \param[in] felt_space_dim_minus_1 Same as \a felt_space_dim
         * except it covers the dimension minus 1 (e.g. finite element space that cover borders on a 2D mesh).
         * \param[in] felt_space_dim_minus_2 Same as \a felt_space_dim_minus_1 for dimension minus 2.
         *
         */
        explicit AtDof(const std::string& name,
                       const Domain& domain,
                       const FEltSpace& felt_space_dim,
                       const FEltSpace& felt_space_dim_minus_1,
                       const FEltSpace& felt_space_dim_minus_2,
                       const Unknown& unknown,
                       const GlobalVector& global_vector);


        //! Destructor.
        ~AtDof() = default;

        //! \copydoc doxygen_hide_copy_constructor
        AtDof(const AtDof& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AtDof(AtDof&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        AtDof& operator=(const AtDof& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AtDof& operator=(AtDof&& rhs) = delete;

        ///@}

        /*!
         * \brief Enables to modify the constant value of a parameter. Disabled for this policy.
         *
         */
        void SetConstantValue(value_type);

      public:
        //! Accessor to global vector.
        const GlobalVector& GetGlobalVector() const noexcept;

        //! Access to unknown covered.
        const Unknown& GetUnknown() const noexcept;

      protected:
        //! Provided here to make the code compile, but should never be called.
        [[noreturn]] return_type GetConstantValueFromPolicy() const;

        //! \copydoc doxygen_hide_parameter_get_value
        return_type GetValueFromPolicy(const local_coords_type& local_coords, const GeometricElt& geom_elt) const;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        return_type GetAnyValueFromPolicy() const;


      protected:
        /*!
         * \brief Whether the parameter varies spatially or not.
         *
         * \return False for current policy.
         */
        bool IsConstant() const noexcept;


        /*!
         * \brief Write the content of the parameter for which policy is used in a stream.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void WriteFromPolicy(std::ostream& stream) const;

      private:
        /*!
         * \brief  Access to the \a FEltSpace that encloses \a geom_elt.
         *
         * \param[in] geom_elt Geometric element at which we seek to evaluate the Parameter value.
         *
         * \return Reference to the \a FEltSpace that encloses \a geom_elt.
         *
         */
        const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const noexcept;
        /*!
         * \brief Common part to all constructors.
         *
         * \copydetails doxygen_hide_at_dof_impl_constructor_args
         *
         * \param[in] first_felt_space Any of the \a FEltSpace (this argument is used solely in debug mode
         * for sanity checks).
         */
        void Construct(const std::string& name,
                       const Domain& domain,
                       const FEltSpace& first_felt_space,
                       const Unknown& unknown,
                       const GlobalVector& global_vector);

      private:
        //! Helper object which keepts tracks of the \a FEltSpace(s) objects considered in the \a Parameter.
        const Internal::ParameterNS::Policy::AtDofNS::FEltSpaceStorage<NfeltSpaceT>&
        GetFEltSpaceStorage() const noexcept;


      private:
        /*!
         * \brief Helper object which keepts tracks of the \a FEltSpace(s) objects considered in the \a
         * Parameter.
         *
         */
        Internal::ParameterNS::Policy::AtDofNS::FEltSpaceStorage<NfeltSpaceT> felt_space_storage_;

        /*!
         * \brief Unknown used to enumerate the dofs considered by the parameter.
         *
         * Must be scalar if TypeT == ParameterNS::Type::scalar.
         *
         */
        const Unknown& unknown_;

        //! Global vector that provides the value at each dof.
        const GlobalVector& global_vector_;

        /*!
         * \brief Local value computed at a given \a LocalCoords.
         *
         * \internal <b><tt>[internal]</tt></b> This must really be seen as a work variable, with fleeting and
         *  non reliable values.
         * It's there only to avoid multiple allocations of LocalVectors in case of vectorial parameter.
         * \endinternal
         */
        mutable value_type local_value_;
    };


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Policy/AtDof/AtDof.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HPP_
// *** MoReFEM end header guards *** < //
