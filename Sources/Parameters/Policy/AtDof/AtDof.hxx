// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/AtDof/AtDof.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::ParameterNS::Policy
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::AtDof(const std::string& name,
                                                             const Domain& domain,
                                                             const FEltSpace& felt_space,
                                                             const Unknown& unknown,
                                                             const GlobalVector& global_vector)
    : felt_space_storage_(felt_space), unknown_(unknown), global_vector_(global_vector)
    {
        static_assert(NfeltSpaceT == 1, "This specific constructor should be called only in this case!");

        Construct(name, domain, felt_space, unknown, global_vector);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::AtDof(const std::string& name,
                                                             const Domain& domain,
                                                             const FEltSpace& felt_space_dim,
                                                             const FEltSpace& felt_space_dim_minus_1,
                                                             const Unknown& unknown,
                                                             const GlobalVector& global_vector)
    : felt_space_storage_(felt_space_dim, felt_space_dim_minus_1), unknown_(unknown), global_vector_(global_vector)
    {
        static_assert(NfeltSpaceT == 2, "This specific constructor should be called only in this case!");

        Construct(name, domain, felt_space_dim, unknown, global_vector);

        assert(felt_space_dim_minus_1.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
        assert(felt_space_dim_minus_1.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::AtDof(const std::string& name,
                                                             const Domain& domain,
                                                             const FEltSpace& felt_space_dim,
                                                             const FEltSpace& felt_space_dim_minus_1,
                                                             const FEltSpace& felt_space_dim_minus_2,
                                                             const Unknown& unknown,
                                                             const GlobalVector& global_vector)
    : felt_space_storage_(felt_space_dim, felt_space_dim_minus_1, felt_space_dim_minus_2), unknown_(unknown),
      global_vector_(global_vector)
    {
        static_assert(NfeltSpaceT == 3, "This specific constructor should be called only in this case!");

        Construct(name, domain, felt_space_dim, unknown, global_vector);

        assert(felt_space_dim_minus_1.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
        assert(felt_space_dim_minus_1.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
        assert(felt_space_dim_minus_2.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
        assert(felt_space_dim_minus_2.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    typename AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::return_type
    AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                                          const GeometricElt& geom_elt) const
    {
        const auto& felt_space = GetFEltSpace(geom_elt);

        const auto& ref_geom_elt = geom_elt.GetRefGeomElt();
        const auto& ref_felt_space = felt_space.GetRefLocalFEltSpace(ref_geom_elt);

        const auto& local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

        const auto& unknown = GetUnknown();

        const auto& felt = local_felt_space.GetFElt(unknown);

        const auto& node_list = felt.GetNodeList();

        const auto& global_vector = GetGlobalVector();
        const auto& numbering_subset = global_vector.GetNumberingSubset();

        Wrappers::Petsc::AccessGhostContent access_ghost_content(global_vector);

        const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();

        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> vector_with_ghost_content(vector_with_ghost);

        const auto& basic_ref_elt = ref_felt_space.GetRefFElt(felt_space, unknown).GetBasicRefFElt();

        Internal::ParameterNS::Policy::AtDofNS::ComputeLocalValue<TypeT>(
            basic_ref_elt, local_coords, node_list, numbering_subset, vector_with_ghost_content, local_value_);

        return local_value_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    auto AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::GetAnyValueFromPolicy() const -> return_type
    {
        return local_value_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    void AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::WriteFromPolicy(std::ostream& stream) const
    {
        stream << "AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::WriteFromPolicy() is not activated at the "
                  "moment; you can however "
                  "ask "
                  "instead "
               << "for GetGlobalVector() and then use one of its method to see its content." << std::endl;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    [[noreturn]] auto AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::GetConstantValueFromPolicy() const ->
        typename AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::return_type
    {
        assert(false && "Should yield IsConstant() = false!");
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    inline bool AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::IsConstant() const noexcept
    {
        return false;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    inline auto AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::GetGlobalVector() const noexcept
        -> const GlobalVector&
    {
        return global_vector_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    inline auto AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::GetUnknown() const noexcept -> const Unknown&
    {
        return unknown_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    inline auto
    AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::GetFEltSpace(const GeometricElt& geom_elt) const noexcept
        -> const FEltSpace&
    {
        return felt_space_storage_.GetFEltSpace(geom_elt);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    void
    AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::Construct([[maybe_unused]] const std::string& name,
                                                                 const Domain& domain,
                                                                 [[maybe_unused]] const FEltSpace& first_felt_space,
                                                                 [[maybe_unused]] const Unknown& unknown,
                                                                 [[maybe_unused]] const GlobalVector& global_vector)
    {
        assert(first_felt_space.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
        assert(first_felt_space.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());

#ifndef NDEBUG
        {
            if (TypeT == ParameterNS::Type::scalar && unknown.GetNature() == UnknownNS::Nature::vectorial)
                assert(false && "A scalar parameter given at dof can only be matched with a scalar Unknown.");
        }
#endif // NDEBUG

        decltype(auto) mesh = domain.GetMesh();

        assert(first_felt_space.GetMeshDimension() == mesh.GetDimension());

        static_assert(std::is_same_v<decltype(local_value_), value_type>);

        if constexpr (TypeT == ParameterNS::Type::scalar)
        {
            static_assert(std::is_floating_point_v<value_type>);
            local_value_ = 0.;
        } else
        {
            static_assert(::MoReFEM::Wrappers::EigenNS::IsVector<value_type>{});
            local_value_.resize(static_cast<Eigen::Index>(mesh.GetDimension()));
            local_value_.setZero();
        }
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    auto AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::GetFEltSpaceStorage() const noexcept
        -> const Internal::ParameterNS::Policy::AtDofNS::FEltSpaceStorage<NfeltSpaceT>&
    {
        return felt_space_storage_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    constexpr auto AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::NfeltSpace() noexcept -> std::size_t
    {
        return NfeltSpaceT;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        Eigen::Index NfeltSpaceT
    >
    // clang-format on
    inline void AtDof<TypeT, TimeManagerT, StorageT, NfeltSpaceT>::SetConstantValue([[maybe_unused]] value_type value)
    {
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
