// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <type_traits> // IWYU pragma: keep

#include "Parameters/Policy/AtDof/Internal/AtDof.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS
{


    FEltSpaceStorage<1>::FEltSpaceStorage(const FEltSpace& felt_space)
    : felt_space_(felt_space), mesh_dimension_(felt_space.GetMeshDimension())
    { }


    // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
    FEltSpaceStorage<2>::FEltSpaceStorage(const FEltSpace& felt_space_dim, const FEltSpace& felt_space_dim_minus_1)
    : felt_space_dim_(felt_space_dim), felt_space_dim_minus_1_(felt_space_dim_minus_1),
      mesh_dimension_(felt_space_dim.GetMeshDimension())
    {
        assert(mesh_dimension_ == felt_space_dim_minus_1.GetMeshDimension());
    }


    // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
    FEltSpaceStorage<3>::FEltSpaceStorage(const FEltSpace& felt_space_dim,
                                          const FEltSpace& felt_space_dim_minus_1,
                                          const FEltSpace& felt_space_dim_minus_2)
    : felt_space_dim_(felt_space_dim), felt_space_dim_minus_1_(felt_space_dim_minus_1),
      felt_space_dim_minus_2_(felt_space_dim_minus_2), mesh_dimension_(felt_space_dim.GetMeshDimension())
    {
        assert(mesh_dimension_ == felt_space_dim_minus_1.GetMeshDimension());
        assert(mesh_dimension_ == felt_space_dim_minus_2.GetMeshDimension());
    }


    const FEltSpace& FEltSpaceStorage<2>::GetFEltSpace(::MoReFEM::GeometryNS::dimension_type geom_elt_dimension) const
    {
        assert(geom_elt_dimension <= mesh_dimension_);
        const auto diff = mesh_dimension_ - geom_elt_dimension;
        assert(diff.Get() <= 2); // 2 is the same as the template parameter!

        switch (diff.Get())
        {
        case 0:
            return felt_space_dim_;
        case 1:
            return felt_space_dim_minus_1_;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    const FEltSpace& FEltSpaceStorage<3>::GetFEltSpace(::MoReFEM::GeometryNS::dimension_type geom_elt_dimension) const
    {
        assert(geom_elt_dimension <= mesh_dimension_);
        const auto diff = mesh_dimension_ - geom_elt_dimension;
        assert(diff.Get() <= 3); // 3 is the same as the template parameter!

        switch (diff.Get())
        {
        case 0:
            return felt_space_dim_;
        case 1:
            return felt_space_dim_minus_1_;
        case 2:
            return felt_space_dim_minus_2_;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //
