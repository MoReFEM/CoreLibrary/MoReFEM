// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Miscellaneous.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/LocalCoords.hpp"

#include "FiniteElement/Nodes_and_dofs/Node.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class FEltSpace; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }
namespace MoReFEM::Wrappers::Petsc { template <Utilities::Access AccessT> class AccessVectorContent; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS
{


    /*!
     * \class doxygen_hide_at_dof_compute_local_value_arg
     *
     * \param[in] basic_ref_felt \a BasicRefElt into which computation occurs, which provides the values
     * of shape functions.
     * \param[in] local_coords \a LocalCoords for which value is computed.
     * \param[in] node_list List of \a Node in the current \a FElt.
     * \param[in] numbering_subset \a NumberingSubset of the \a GlobalVector considered in the
     * \a ParameterAtDof policy.
     * \param[in] ghost_content Processor-wise and ghost content of the \a GlobalVector underneath
     * the \a ParameterAtDof.
     * \param[out] value Result of the computation.
     */


    /*!
     * \brief Compute local value, both for scalar and vectorial cases.
     *
     * \tparam T The type of the result to compute (scalar for scalar case, a specialization of `Eigen::Vector` for vectorial case).
     *
     * \copydoc doxygen_hide_at_dof_compute_local_value_arg
     */
    // clang-format off
    template
    <
        ::MoReFEM::ParameterNS::Type TypeT,
        class T
    >
    // clang-format on
    void ComputeLocalValue(
        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
        const LocalCoords& local_coords,
        const Node::vector_shared_ptr& node_list,
        const NumberingSubset& numbering_subset,
        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>& ghost_content,
        T& value);


    /*!
     * \brief An helper structure which purpose is to store references and provide access to \a
     * FEltSpace.
     *
     * \tparam Ndim Number of dimensions stored in the class.
     */
    template<std::size_t Ndim>
    struct FEltSpaceStorage;


    /*!
     * \brief Specialization when only one underlying \a FEltSpace.
     *
     * In this case, the \a FEltSpace might be any dimension, regardless of the mesh dimension.
     */
    template<>
    struct FEltSpaceStorage<1>
    {

        /*!
         * \brief Constructor
         *
         * \param[in] felt_space \a FEltSpace considered.
         */
        FEltSpaceStorage(const FEltSpace& felt_space);


        /*!
         * \brief Return the \a FEltSpace adequate for the \a dimension.
         *
         * \param[in] dimension Dimension for which \a FEltSpace is requested.
         *
         * \return \a FEltSpace.
         */
        const FEltSpace& GetFEltSpace(::MoReFEM::GeometryNS::dimension_type dimension) const;

        /*!
         * \brief Return the \a FEltSpace.
         *
         * \param[in] geom_elt Unused for current class.
         *
         * \return \a FEltSpace.
         */
        const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const;

      private:
        //! Underlying \a FEltSpace.
        const FEltSpace& felt_space_;

        //! Dimension of the mesh.
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;
    };


    /*!
     * \brief Specialization when there are two underlying \a FEltSpace.
     *
     * In this case,
     * - First one is expected to cover the dimension of the mesh.
     * - Second one is expected to cover the dimension of the mesh minus one.
     */
    template<>
    struct FEltSpaceStorage<2>
    {

        /*!
         * \brief Constructor
         *
         * \param[in] felt_space_dim \a FEltSpace that covers the same dimension as the mesh.
         * \param[in] felt_space_dim_minus_1 \a FEltSpace that covers the dimension as the mesh minus 1.
         */
        FEltSpaceStorage(const FEltSpace& felt_space_dim, const FEltSpace& felt_space_dim_minus_1);


        /*!
         * \brief Return the \a FEltSpace adequate for the \a dimension.
         *
         * \param[in] dimension Dimension for which \a FEltSpace is requested.
         *
         * \return \a FEltSpace.
         */
        const FEltSpace& GetFEltSpace(::MoReFEM::GeometryNS::dimension_type dimension) const;

        /*!
         * \brief Return the adequate \a FEltSpace for the given \a geom_elt.
         *
         * \param[in] geom_elt \a GeometricElt being considered.
         *
         * \return Relevant \a FEltSpace for \a geom_elt.
         */
        const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const;

      private:
        //! \a FEltSpace that covers the same dimension as the mesh.
        const FEltSpace& felt_space_dim_;

        //! \a FEltSpace that covers the dimension as the mesh minus 1.
        const FEltSpace& felt_space_dim_minus_1_;

        //! Dimension of the mesh.
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;
    };


    /*!
     * \brief Specialization when there are three underlying \a FEltSpace.
     *
     * In this case,
     * - First one is expected to cover the dimension of the mesh.
     * - Second one is expected to cover the dimension of the mesh minus one.
     * - Third one is expected to cover the dimension of the mesh minus two.
     */
    template<>
    struct FEltSpaceStorage<3>
    {


        /*!
         * \brief Constructor
         *
         * \param[in] felt_space_dim \a FEltSpace that covers the same dimension as the mesh.
         * \param[in] felt_space_dim_minus_1 \a FEltSpace that covers the dimension as the mesh minus 1.
         * \param[in] felt_space_dim_minus_2 \a FEltSpace that covers the dimension as the mesh minus 2.
         */
        FEltSpaceStorage(const FEltSpace& felt_space_dim,
                         const FEltSpace& felt_space_dim_minus_1,
                         const FEltSpace& felt_space_dim_minus_2);


        /*!
         * \brief Return the \a FEltSpace adequate for the \a dimension.
         *
         * \param[in] dimension Dimension for which \a FEltSpace is requested.
         *
         * \return \a FEltSpace.
         */
        const FEltSpace& GetFEltSpace(::MoReFEM::GeometryNS::dimension_type dimension) const;

        /*!
         * \brief Return the adequate \a FEltSpace for the given \a geom_elt.
         *
         * \param[in] geom_elt \a GeometricElt being considered.
         *
         * \return Relevant \a FEltSpace for \a geom_elt.
         */
        const FEltSpace& GetFEltSpace(const GeometricElt& geom_elt) const;

      private:
        //! \a FEltSpace that covers the same dimension as the mesh.
        const FEltSpace& felt_space_dim_;

        //! \a FEltSpace that covers the dimension as the mesh minus 1.
        const FEltSpace& felt_space_dim_minus_1_;

        //! \a FEltSpace that covers the dimension as the mesh minus 2.
        const FEltSpace& felt_space_dim_minus_2_;

        //! Dimension of the mesh.
        const ::MoReFEM::GeometryNS::dimension_type mesh_dimension_;
    };


} // namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Policy/AtDof/Internal/AtDof.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HPP_
// *** MoReFEM end header guards *** < //
