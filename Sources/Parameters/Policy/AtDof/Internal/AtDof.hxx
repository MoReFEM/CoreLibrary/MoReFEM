// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/AtDof/Internal/AtDof.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS
{


    inline auto FEltSpaceStorage<1>::GetFEltSpace(::MoReFEM::GeometryNS::dimension_type) const -> const FEltSpace&
    {
        return felt_space_;
    }


    inline auto FEltSpaceStorage<1>::GetFEltSpace(const GeometricElt&) const -> const FEltSpace&
    {
        return felt_space_;
    }


    inline auto FEltSpaceStorage<2>::GetFEltSpace(const GeometricElt& geom_elt) const -> const FEltSpace&
    {
        return GetFEltSpace(geom_elt.GetDimension());
    }


    inline auto FEltSpaceStorage<3>::GetFEltSpace(const GeometricElt& geom_elt) const -> const FEltSpace&
    {
        return GetFEltSpace(geom_elt.GetDimension());
    }


    // clang-format off
    template
    <
        ::MoReFEM::ParameterNS::Type TypeT,
        class T
    >
    // clang-format on
    void ComputeLocalValue(
        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
        const LocalCoords& local_coords,
        const Node::vector_shared_ptr& node_list,
        const NumberingSubset& numbering_subset,
        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>& vector_with_ghost_content,
        T& value)
    {
        const LocalNodeNS::index_type Nnode{ static_cast<LocalNodeNS::index_type::underlying_type>(node_list.size()) };

        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
        {
            static_assert(std::is_floating_point_v<T>);
            value = T{};
        } else
        {
            static_assert(TypeT == ::MoReFEM::ParameterNS::Type::vector,
                          "Matricial should have been filtered out previously as irrelevant!");

            value.setZero();
        }

        for (LocalNodeNS::index_type local_node_index{}; local_node_index < Nnode; ++local_node_index)
        {
            const auto local_node_index_as_size_t = static_cast<std::size_t>(local_node_index.Get());
            assert(local_node_index_as_size_t < node_list.size());
            const auto& node_ptr = node_list[local_node_index_as_size_t];
            assert(!(!node_ptr));
            const auto& node = *node_ptr;
            assert(node.IsInNumberingSubset(numbering_subset));

            const auto& dof_list = node.GetDofList();
            const auto Ndof = dof_list.size();

            for (auto dof_index = 0UL; dof_index < Ndof; ++dof_index)
            {
                const auto& dof_ptr = dof_list[dof_index];
                assert(!(!dof_ptr));
                const auto processor_wise_index = dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset);

                auto new_contribution = basic_ref_felt.ShapeFunction(local_node_index, local_coords)
                                        * vector_with_ghost_content.GetValue(vector_processor_wise_index_type{
                                            static_cast<PetscInt>(processor_wise_index.Get()) });
                if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
                {
                    assert(Ndof == 1UL);
                    value += new_contribution;
                } else
                {
                    assert(static_cast<Eigen::Index>(dof_index) < value.size());
                    value(static_cast<Eigen::Index>(dof_index)) += new_contribution;
                }
            }
        }
    }


} // namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
