// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_ATQUADRATUREPOINT_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_ATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/UnorderedMap.hpp"

#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

#include "Parameters/Policy/AtQuadraturePoint/Internal/AtQuadraturePoint.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS { class Model; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ParameterNS::Policy
{


    /*!
     * \brief Parameter policy when the parameter gets a value at each pair geometric element/quadrature point.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     *
     * \copydoc doxygen_hide_tparam_parameter_concept_storage
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on
    class AtQuadraturePoint
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = AtQuadraturePoint<TypeT, TimeManagerT, StorageT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = QuadraturePoint;

      private:
        //! Alias to traits class related to TypeT.
        using traits = Internal::ParameterNS::Traits<TypeT, StorageT>;

        //! Frienship to test class.
        friend class MoReFEM::TestNS::UpdateCauchyGreenTensorNS::Model;


      public:
        //! Alias to the return type.
        using return_type = typename traits::return_type;

      private:
        //! Alias to the way each value is stored.
        using storage_value_type = std::decay_t<return_type>;

        //! Alias.
        using value_type = std::decay_t<return_type>;

        //! Alias to value holder.
        using value_holder_type = Internal::ParameterNS::AtQuadraturePointNS::ValueHolder<storage_value_type>;

        /*!
         * \brief Alias to the type of the value actually stored.
         *
         * Key is the index of a GeometricElement.
         * Index of the vector is the quadrature point unique id.
         */
        using parameter_storage_type = std::unordered_map<GeomEltNS::index_type, std::vector<value_holder_type>>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_parameter_name_and_domain_arg
         *
         * \param[in] quadrature_rule_per_topology Specify which quadrature rule to use for each topology that
         * might be considered.
         * \param[in] initial_value When the object is built the convention is to set this value at all of the
         * quadrature points. Needless to say something more sophisticated could be done (reading each value
         * from a file for instance); if you need such feature don't hesitate to ask for it!
         * \param[in] time_manager \a TimeManager of the model.
         */
        explicit AtQuadraturePoint(const std::string& name,
                                   const Domain& domain,
                                   const QuadratureRulePerTopology& quadrature_rule_per_topology,
                                   storage_value_type initial_value,
                                   const TimeManagerT& time_manager);

        //! Destructor.
        ~AtQuadraturePoint() = default;

        //! \copydoc doxygen_hide_copy_constructor
        AtQuadraturePoint(const AtQuadraturePoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AtQuadraturePoint(AtQuadraturePoint&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        AtQuadraturePoint& operator=(const AtQuadraturePoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AtQuadraturePoint& operator=(AtQuadraturePoint&& rhs) = delete;

        ///@}

        /*!
         * \brief Enables to modify the constant value of a parameter. Disable for this Policy.
         *
         */
        void SetConstantValue(value_type);

      protected:
        //! Provided here to make the code compile, but should never be called.
        [[noreturn]] return_type GetConstantValueFromPolicy() const;

        //! \copydoc doxygen_hide_parameter_get_value_quad_pt
        return_type GetValueFromPolicy(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt) const;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        return_type GetAnyValueFromPolicy() const;


      public:
        /*!
         * \brief Update the value for the new time iteration.
         *
         * \internal <b><tt>[internal]</tt></b> This method is not an override: it can only be used when the
         * Parameter object is STATICALLY a child of AtQuadraturePointPolicy.
         * \endinternal
         *
         * \param[in] quad_pt Quadrature point for which parameter value needs to be updated.
         * \param[in] geom_elt Geometric element to which the quadrature point belongs to.
         * \param[in] update_functor Lambda function (or straight functor if you really fancy them) which gives
         * away the formula to use to update the parameter value.
         *
         *
         * \tparam UpdateFunctorT  The expected prototype of the update_functor functor is:
         *
         * \code
         * [](storage_value_type& value_to_modify) -> void
         * \endcode
         * For instance, argument type \a storage_value_type is \a double& for TypeT = \a scalar, a reference to an
         * Eigen vector for TypeT = \a vector and a reference to an Eigen matrix for TypeT = \a matrix.
         */
        template<class UpdateFunctorT>
        void
        UpdateValue(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt, const UpdateFunctorT& update_functor);


        /*!
         * \brief Update the value for the new time iteration and returns it.
         *
         * \copydetails AtQuadraturePoint::UpdateValue()
         *
         * \return Updated value.
         */
        template<class UpdateFunctorT>
        return_type UpdateAndGetValue(const QuadraturePoint& quad_pt,
                                      const GeometricElt& geom_elt,
                                      const UpdateFunctorT& update_functor);


        //! Copy the content of another \a AtQuadraturePoint.
        //! \param[in] parameter_at_quad_point \a Parameter which data are copied.
        void Copy(const AtQuadraturePoint& parameter_at_quad_point);

        //! Access to the object that indicates which quadrature rule is used for each topology.
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;


      protected:
        //! Whether the parameter varies spatially or not.
        bool IsConstant() const;


        /*!
         * \brief Write the content of the parameter for which policy is used in a stream.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void WriteFromPolicy(std::ostream& stream) const;


      private:
        //! Domain upon which the parameter is defined.
        const Domain& GetDomain() const noexcept;

        //! Get the reference to the requested value.
        //! \param[in] quad_pt \a QuadraturePoint for which the value is sought.
        //! \param[in] geom_elt \a GeometricElt for which the value is sought.
        const value_holder_type& FindValue(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt) const;


        //! Get the non constant reference to the requested value.
        //! \param[in] quad_pt \a QuadraturePoint for which the value is sought.
        //! \param[in] geom_elt \a GeometricElt for which the value is sought.
        value_holder_type& FindNonCstValue(const QuadraturePoint& quad_pt, const GeometricElt& geom_elt);

        //! Access to the storage.
        const parameter_storage_type& GetStorage() const noexcept;

        //! Access to the storage.
        parameter_storage_type& GetNonCstStorage() noexcept;

        //! Returns the quadrature rule used for a given topology.
        //! \param[in] topology_id Topology identifier.
        const QuadratureRule& GetQuadratureRule(TopologyNS::Type topology_id) const;

        //! Access to the time manager.
        const TimeManagerT& GetTimeManager() const noexcept;

        //! Name of the \a Parameter for which the policy is defined.
        const std::string& GetParameterName() const noexcept;

      private:
        //! Name of the \a Parameter for which the policy is defined.
        const std::string& parameter_name_;

        //! Part of the mesh upon which the parameter is defined.
        const Domain& domain_;

        //! Storage of all the values (see \a parameter_storage_type for details about its layout).
        parameter_storage_type storage_;

        //! Initial value.
        // \todo #26 Should probably not be stored once quadrature rules properly defined.
        storage_value_type initial_value_;

        //! Stores which quadrature rule is used for each topology.
        const QuadratureRulePerTopology& quadrature_rule_per_topology_;

        //! Time manager.
        const TimeManagerT& time_manager_;
    };


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_ATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
