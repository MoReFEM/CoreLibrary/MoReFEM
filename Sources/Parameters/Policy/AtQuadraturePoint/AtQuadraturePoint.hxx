// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_ATQUADRATUREPOINT_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_ATQUADRATUREPOINT_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Warnings/Pragma.hpp" // IWYU pragma: export // for Sonarqube sake only - is already included in the hpp file
                                         // which includes present file...


namespace MoReFEM::ParameterNS::Policy
{


    namespace // anonymous
    {


        // Pragma here because clang issues a warning about unused template - even if this template is clearly
        // used!
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-template.hpp"

        template<class StorageValueTypeT, class StorageTypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
        void InsertValuesInStorage(const TimeManagerT& time_manager,
                                   Mesh::subset_range&& range,
                                   StorageValueTypeT initial_value,
                                   const Eigen::Index Nquadrature_point,
                                   StorageTypeT& storage);

        PRAGMA_DIAGNOSTIC(pop)


    } // namespace


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::AtQuadraturePoint(
        const std::string& name,
        const Domain& domain,
        const QuadratureRulePerTopology& quadrature_rule_per_topology,
        storage_value_type initial_value,
        const TimeManagerT& time_manager)
    : parameter_name_(name), domain_(domain), initial_value_(initial_value),
      quadrature_rule_per_topology_(quadrature_rule_per_topology), time_manager_(time_manager)
    {
        auto& storage = GetNonCstStorage();

        storage.max_load_factor(Utilities::DefaultMaxLoadFactor());

        assert(domain.template IsConstraintOn<Advanced::DomainNS::Criterion::mesh>()
               && "It is an assert because this should have been checked earlier in ParameterInstance "
                  "constructor. At that place it is an exception, as a mere  user might provide an invalid "
                  "Domain.");

        const auto& mesh = domain.GetMesh();

        storage.reserve(mesh.NgeometricElt<RoleOnProcessor::processor_wise>()
                        + mesh.NgeometricElt<RoleOnProcessor::ghost>());

        decltype(auto) ref_geom_elt_list = mesh.BagOfEltType<RoleOnProcessorPlusBoth::both>();

        for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
        {
            assert(!(!ref_geom_elt_ptr));
            const auto& ref_geom_elt = *ref_geom_elt_ptr;

            const auto& quadrature_rule = GetQuadratureRule(ref_geom_elt.GetTopologyIdentifier());
            const auto Nquadrature_point = quadrature_rule.NquadraturePoint();

            // Iterate over all geometric elements that share the ref_geom_elt.
            InsertValuesInStorage<storage_value_type>(
                time_manager,
                mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt),
                initial_value,
                Nquadrature_point,
                storage);

            InsertValuesInStorage<storage_value_type>(
                time_manager,
                mesh.GetSubsetGeometricEltList<RoleOnProcessor::ghost>(ref_geom_elt),
                initial_value,
                Nquadrature_point,
                storage);
        }
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::return_type
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetValueFromPolicy(const QuadraturePoint& quad_pt,
                                                                         const GeometricElt& geom_elt) const
    {
        return FindValue(quad_pt, geom_elt).value;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::return_type
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetAnyValueFromPolicy() const
    {
        decltype(auto) storage = GetStorage();
        assert(!storage.empty());

        const auto& geom_elt_content = storage.cbegin()->second; // don't use decltype(auto) here

        assert(!geom_elt_content.empty());
        return geom_elt_content.back().value;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    const typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::value_holder_type&
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::FindValue(const QuadraturePoint& quad_pt,
                                                                const GeometricElt& geom_elt) const
    {
        auto it = storage_.find(geom_elt.GetIndex());
        assert(it != storage_.cend());

        const auto& values_in_geom_elt = it->second;

        const auto quad_pt_index = quad_pt.GetIndex();
        assert(quad_pt_index < values_in_geom_elt.size());

        return values_in_geom_elt[quad_pt_index];
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::value_holder_type&
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::FindNonCstValue(const QuadraturePoint& quad_pt,
                                                                      const GeometricElt& geom_elt)
    {
        return const_cast<typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::value_holder_type&>(
            FindValue(quad_pt, geom_elt));
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    template<class UpdateFunctorT>
    typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::return_type
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::UpdateAndGetValue(const QuadraturePoint& quad_pt,
                                                                        const GeometricElt& geom_elt,
                                                                        const UpdateFunctorT& update_functor)
    {
        auto& value_holder = FindNonCstValue(quad_pt, geom_elt);

        value_holder.last_update_index = GetTimeManager().NtimeModified();
        update_functor(value_holder.value);
        return value_holder.value;
    }

    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    template<class UpdateFunctorT>
    void AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::UpdateValue(const QuadraturePoint& quad_pt,
                                                                       const GeometricElt& geom_elt,
                                                                       const UpdateFunctorT& update_functor)
    {
        auto& value_holder = FindNonCstValue(quad_pt, geom_elt);

        value_holder.last_update_index = GetTimeManager().NtimeModified();
        update_functor(value_holder.value);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    void AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::Copy(const AtQuadraturePoint& parameter_at_quad_point)
    {
        GetNonCstStorage() = parameter_at_quad_point.GetStorage();
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    [[noreturn]] typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::return_type
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetConstantValueFromPolicy() const
    {
        assert(false && "Should yield IsConstant() = false!");
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    bool AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::IsConstant() const
    {
        return false;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    void AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::WriteFromPolicy(std::ostream& stream) const
    {
        decltype(auto) domain = this->GetDomain();
        const auto& mesh = domain.GetMesh();
        const auto& ref_geom_elt_list = mesh.template BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

        const auto& storage = GetStorage();

#ifndef NDEBUG
        const auto end_storage = storage.cend();
#endif // NDEBUG
        stream << "# Geometric element index; quadrature point; TimeManager::NtimeModified() at last update; "
                  "value"
               << std::endl;

        for (const auto& ref_geom_elt_ptr : ref_geom_elt_list)
        {
            assert(!(!ref_geom_elt_ptr));
            const auto& ref_geom_elt = *ref_geom_elt_ptr;

            if (!domain.DoRefGeomEltMatchCriteria(ref_geom_elt))
                continue;

            const auto& quadrature_rule = GetQuadratureRule(ref_geom_elt.GetTopologyIdentifier());

            decltype(auto) iterator_range =
                mesh.template GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_geom_elt);

            const auto quadrature_rule_name = quadrature_rule.GetName();

            const auto Nquadrature_point = quadrature_rule.NquadraturePoint();

            for (auto it_geom_elt = iterator_range.first; it_geom_elt != iterator_range.second; ++it_geom_elt)
            {
                const auto& geom_elt_ptr = *it_geom_elt;
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                if (!domain.IsGeometricEltInside(geom_elt))
                    continue;

                const auto geom_elt_index = geom_elt.GetIndex();

                auto it_in_storage = storage.find(geom_elt_index);
                assert(it_in_storage != end_storage);
                const auto& value_per_quad_pt = it_in_storage->second;

                for (auto i = 0UL; i < static_cast<std::size_t>(Nquadrature_point); ++i)
                {
                    stream << geom_elt_index << ';' << quadrature_rule_name << '_' << i << ';'
                           << value_per_quad_pt[i].last_update_index << ';';

                    if constexpr (TypeT == ParameterNS::Type::scalar)
                        stream << std::setprecision(16) << value_per_quad_pt[i].value;

                    // To get rid of the braces "{ }" of the xtensor output and have more control over the
                    // precision.
                    if constexpr (TypeT == ParameterNS::Type::vector)
                    {
                        stream.precision(16);
                        Utilities::PrintContainer<>::Do(value_per_quad_pt[i].value,
                                                        stream,
                                                        PrintNS::Delimiter::separator(" "),
                                                        PrintNS::Delimiter::opener(""),
                                                        PrintNS::Delimiter::closer(""));
                    }

                    stream << std::endl;
                }
            }
        }
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline const Domain& AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetDomain() const noexcept
    {
        return domain_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline const typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::parameter_storage_type&
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetStorage() const noexcept
    {
        return storage_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline typename AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::parameter_storage_type&
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetNonCstStorage() noexcept
    {
        return storage_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline const QuadratureRule&
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetQuadratureRule(TopologyNS::Type topology_id) const
    {
        return GetQuadratureRulePerTopology().GetRule(topology_id);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline const TimeManagerT& AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline const QuadratureRulePerTopology&
    AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetQuadratureRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }

    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    inline void AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::SetConstantValue([[maybe_unused]] value_type value)
    {
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT
    >
    // clang-format on

    const std::string& AtQuadraturePoint<TypeT, TimeManagerT, StorageT>::GetParameterName() const noexcept
    {
        return parameter_name_;
    }


    namespace // anonymous
    {


        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-template.hpp"

        template<class StorageValueTypeT, class StorageTypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
        void InsertValuesInStorage(const TimeManagerT& time_manager,
                                   Mesh::subset_range&& range,
                                   StorageValueTypeT initial_value,
                                   const Eigen::Index Nquadrature_point,
                                   StorageTypeT& storage)
        {

            for (auto it_geom_elt = range.first; it_geom_elt != range.second; ++it_geom_elt)
            {
                const auto& geom_elt_ptr = *it_geom_elt;
                assert(!(!geom_elt_ptr));
                const auto geom_elt_index = geom_elt_ptr->GetIndex();

                using value_type = Internal::ParameterNS::AtQuadraturePointNS::ValueHolder<StorageValueTypeT>;

                value_type default_value(initial_value, time_manager.NtimeModified());

                [[maybe_unused]] auto check = storage.insert(
                    { geom_elt_index,
                      std::vector<value_type>(static_cast<std::size_t>(Nquadrature_point), default_value) });
                assert(check.second && "A geometric element should be entered only once.");
            }
        }

        PRAGMA_DIAGNOSTIC(pop)


    } // namespace


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_ATQUADRATUREPOINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
