// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_INTERNAL_ATQUADRATUREPOINT_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_INTERNAL_ATQUADRATUREPOINT_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/AtQuadraturePoint/Internal/AtQuadraturePoint.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::ParameterNS::AtQuadraturePointNS
{


    template<class StorageValueTypeT>
    ValueHolder<StorageValueTypeT>::ValueHolder(StorageValueTypeT a_value, std::size_t time_manager_Ntime_modified)
    : value(a_value), last_update_index(time_manager_Ntime_modified)
    { }


} // namespace MoReFEM::Internal::ParameterNS::AtQuadraturePointNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_INTERNAL_ATQUADRATUREPOINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
