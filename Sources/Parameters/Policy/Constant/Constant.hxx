// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/Constant/Constant.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ParameterNS::Policy
{


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    Constant<TypeT, TimeManagerT, StorageT>::Constant(
        const std::string&,
        const Domain&,
        typename Constant<TypeT, TimeManagerT, StorageT>::parameter_storage_type value)
    : value_(value)
    { }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    [[noreturn]] typename Constant<TypeT, TimeManagerT, StorageT>::return_type
    Constant<TypeT, TimeManagerT, StorageT>::GetValueFromPolicy([[maybe_unused]] const local_coords_type& local_coords,
                                                                [[maybe_unused]] const GeometricElt& geom_elt) const
    {
        assert(false && "Parameter class should have guided toward GetConstantValue()!");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline typename Constant<TypeT, TimeManagerT, StorageT>::return_type
    Constant<TypeT, TimeManagerT, StorageT>::GetConstantValueFromPolicy() const
    {
        return value_;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline typename Constant<TypeT, TimeManagerT, StorageT>::return_type
    Constant<TypeT, TimeManagerT, StorageT>::GetAnyValueFromPolicy() const
    {
        return GetConstantValueFromPolicy();
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline constexpr bool Constant<TypeT, TimeManagerT, StorageT>::IsConstant() const noexcept
    {
        return true;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    void Constant<TypeT, TimeManagerT, StorageT>::WriteFromPolicy(std::ostream& stream) const
    {
        stream << "# Homogeneous value:" << std::endl;
        stream << GetConstantValueFromPolicy() << std::endl;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline void Constant<TypeT, TimeManagerT, StorageT>::SetConstantValue(parameter_storage_type value) noexcept
    {
        value_ = value;
    }


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HXX_
// *** MoReFEM end header guards *** < //
