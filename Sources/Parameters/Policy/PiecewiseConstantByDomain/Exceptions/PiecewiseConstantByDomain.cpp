// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>

#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp"

#include "Utilities/Containers/Print.hpp"

#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Index.hpp"


namespace // anonymous
{


    std::string
    InconsistentDomainsMsg(::MoReFEM::GeomEltNS::index_type geom_elt_index,
                           std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
                           std::string_view parameter_name);


} // namespace


namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS
{


    InconsistentDomains::~InconsistentDomains() = default;


    InconsistentDomains::InconsistentDomains(
        ::MoReFEM::GeomEltNS::index_type geom_elt_index,
        std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
        std::string_view parameter_name,
        const std::source_location location)
    : MoReFEM::Exception(InconsistentDomainsMsg(geom_elt_index, domain_ids, parameter_name), location)
    { }


    InconsistentLocalVectorShape::~InconsistentLocalVectorShape() = default;


    InconsistentLocalMatrixShape::~InconsistentLocalMatrixShape() = default;


} // namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string
    InconsistentDomainsMsg(::MoReFEM::GeomEltNS::index_type geom_elt_index,
                           std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
                           std::string_view parameter_name)
    {
        std::ostringstream oconv;
        oconv << "A geometric element (which index is " << geom_elt_index << ") is in at least two domains ("
              << domain_ids.first << " and " << domain_ids.second << ") with two different values for the '"
              << parameter_name << "' parameter.";

        return oconv.str();
    }


} // namespace
