// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_EXCEPTIONS_PIECEWISECONSTANTBYDOMAIN_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_EXCEPTIONS_PIECEWISECONSTANTBYDOMAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <utility>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/Parameter/Advanced/Concept.hpp"

#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Index.hpp"


namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS
{


    /*!
     * \brief Thrown when a \a GeometricElt is in two \a Domain involved in the same \a Parameter and the associated values are inconsistent.
     *
     */
    class InconsistentDomains final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] geom_elt_index The index of the \a GeometricElt, as returned by \a GeometricElt::GetIndex().
         * \param[in] domain_ids Both \a Domain in which the \a GeometricElt was found. The exception is thrown as soon
         as a discrepancy
         * is found, so if more than two \a Domain are involved in the issue only the first two ones for which it occurs
         will be mentioned in the
         * exception message.
         * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
         * \copydoc doxygen_hide_source_location
         */
        explicit InconsistentDomains(
            ::MoReFEM::GeomEltNS::index_type geom_elt_index,
            std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
            std::string_view parameter_name,
            const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentDomains() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentDomains(const InconsistentDomains& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentDomains(InconsistentDomains&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentDomains& operator=(const InconsistentDomains& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentDomains& operator=(InconsistentDomains&& rhs) = default;
    };


    /*!
     * \brief Thrown when a local vector associated with different \a Domains don't share the same shape.
     *
     */
    class InconsistentLocalVectorShape final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         *
         * \param[in] storage  The data provided to fill the internal storage to the \a Parameter.
         * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_tparam_parameter_concept_storage
         */
        template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        explicit InconsistentLocalVectorShape(const std::map<::MoReFEM::DomainNS::unique_id, StorageT>& storage,
                                              std::string_view parameter_name,
                                              const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentLocalVectorShape() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentLocalVectorShape(const InconsistentLocalVectorShape& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentLocalVectorShape(InconsistentLocalVectorShape&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentLocalVectorShape& operator=(const InconsistentLocalVectorShape& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentLocalVectorShape& operator=(InconsistentLocalVectorShape&& rhs) = default;
    };


    /*!
     * \brief Thrown when a local matrix associated with different \a Domains don't share the same shape.
     *
     */
    class InconsistentLocalMatrixShape final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] storage  The data provided to fill the internal storage to the \a Parameter.
         * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
         * \copydoc doxygen_hide_source_location
         */
        template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        explicit InconsistentLocalMatrixShape(const std::map<::MoReFEM::DomainNS::unique_id, StorageT>& storage,
                                              std::string_view parameter_name,
                                              const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentLocalMatrixShape() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentLocalMatrixShape(const InconsistentLocalMatrixShape& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentLocalMatrixShape(InconsistentLocalMatrixShape&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentLocalMatrixShape& operator=(const InconsistentLocalMatrixShape& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentLocalMatrixShape& operator=(InconsistentLocalMatrixShape&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_EXCEPTIONS_PIECEWISECONSTANTBYDOMAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
