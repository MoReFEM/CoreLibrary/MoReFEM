// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_EXCEPTIONS_PIECEWISECONSTANTBYDOMAIN_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_EXCEPTIONS_PIECEWISECONSTANTBYDOMAIN_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS
{


    namespace Impl
    {

        template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        std::string InconsistentLocalVectorShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, StorageT>& storage,
                                                    std::string_view parameter_name);


        template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        std::string InconsistentLocalMatrixShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, StorageT>& storage,
                                                    std::string_view parameter_name);

    } // namespace Impl


    template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    InconsistentLocalVectorShape::InconsistentLocalVectorShape(const std::map<DomainNS::unique_id, StorageT>& storage,
                                                               std::string_view parameter_name,
                                                               const std::source_location location)
    : MoReFEM::Exception(Impl::InconsistentLocalVectorShapeMsg(storage, parameter_name), location)
    { }


    template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    InconsistentLocalMatrixShape::InconsistentLocalMatrixShape(const std::map<DomainNS::unique_id, StorageT>& storage,
                                                               std::string_view parameter_name,
                                                               const std::source_location location)
    : MoReFEM::Exception(Impl::InconsistentLocalMatrixShapeMsg(storage, parameter_name), location)
    { }


    namespace Impl
    {


        template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        std::string InconsistentLocalVectorShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, StorageT>& storage,
                                                    std::string_view parameter_name)
        {
            using namespace MoReFEM;

            std::ostringstream oconv;
            oconv << "Parameter '" << parameter_name
                  << "' was defined as being piecewise constant by domain but the "
                     "shape of the provided vector provided don't match: "
                  << '\n';

            for (const auto& [domain_id, vector] : storage)
                oconv << "\t- Domain " << domain_id << " -> size = " << vector.size() << '\n';

            return oconv.str();
        }


        template<::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        std::string InconsistentLocalMatrixShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, StorageT>& storage,
                                                    std::string_view parameter_name)
        {
            using namespace MoReFEM;

            std::ostringstream oconv;
            oconv << "Parameter '" << parameter_name
                  << "' was defined as being piecewise constant by domain but the "
                     "shape of the provided matrix provided don't match: "
                  << '\n';

            for (const auto& [domain_id, matrix] : storage)
                oconv << "\t- Domain " << domain_id << " -> size = (" << matrix.rows() << ", " << matrix.cols()
                      << ")\n";

            return oconv.str();
        }


    } // namespace Impl


} // namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_EXCEPTIONS_PIECEWISECONSTANTBYDOMAIN_DOT_HXX_
// *** MoReFEM end header guards *** < //
