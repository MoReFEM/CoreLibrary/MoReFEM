// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_INTERNAL_HELPER_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/Parameter/Internal/Traits.hpp"

#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS
{


    /*!
     * \brief Check the values associated to all the \a Domain all share the same shapes.
     *
     * Obviously this make sense only for \a Type::vector and \a Type::matrix cases; in \a Type::scalar nothing is done.
     * If the shapes aren't the same, an exception is thrown (namely
     * \a ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalVectorShape or
     * \a ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalMatrixShape).
     *
     * \param[in] storage The data given in input at \a Parameter construction used to fill the values \a Parameter takes in the different \a Domain s
     * (key is the unique identifier of \a Domain).
     * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
     * \copydoc doxygen_hide_source_location
     *
     * \copydoc doxygen_hide_tparam_parameter_concept_storage
     */
    template<Type TypeT, ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    void CheckShapeConsistency(
        const std::map<::MoReFEM::DomainNS::unique_id, typename Traits<TypeT, StorageT>::value_type>& storage,
        std::string_view parameter_name,
        const std::source_location location = std::source_location::current());


} // namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Policy/PiecewiseConstantByDomain/Internal/Helper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
