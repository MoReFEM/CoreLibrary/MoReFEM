// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_INTERNAL_HELPER_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_INTERNAL_HELPER_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/PiecewiseConstantByDomain/Internal/Helper.hpp"
// *** MoReFEM header guards *** < //


#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS
{


    template<Type TypeT, ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    void CheckShapeConsistency(
        const std::map<::MoReFEM::DomainNS::unique_id, typename Traits<TypeT, StorageT>::value_type>& storage,
        std::string_view parameter_name,
        const std::source_location location)
    {
        if (storage.empty()) // weird case, but not the place to handle it.
            return;

        if constexpr (TypeT == Type::scalar)
            ;
        else if constexpr (TypeT == Type::vector)
        {
            using ::MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalVectorShape;

            const auto& first_shape = storage.cbegin()->second.size();

            if (!std::all_of(storage.cbegin(),
                             storage.cend(),
                             [&first_shape](const auto& item)
                             {
                                 const auto& [domain_id, vector] = item;
                                 return vector.size() == first_shape;
                             }))
                throw InconsistentLocalVectorShape(storage, parameter_name, location);
        } else if constexpr (TypeT == Type::matrix)
        {
            using ::MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalMatrixShape;

            const auto first_Nrow = storage.cbegin()->second.rows();
            const auto first_Ncol = storage.cbegin()->second.cols();

            if (!std::all_of(storage.cbegin(),
                             storage.cend(),
                             [first_Nrow, first_Ncol](const auto& item)
                             {
                                 const auto& [domain_id, matrix] = item;
                                 return matrix.rows() == first_Nrow && matrix.cols() == first_Ncol;
                             }))
                throw InconsistentLocalMatrixShape(storage, parameter_name, location);
        } else
        {
            assert(false
                   && "Case not foreseen. If a new type is added in the enum please add it in the "
                      "cases handled above.");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_INTERNAL_HELPER_DOT_HXX_
// *** MoReFEM end header guards *** < //
