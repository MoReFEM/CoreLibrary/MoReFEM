// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_PIECEWISECONSTANTBYDOMAIN_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_PIECEWISECONSTANTBYDOMAIN_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/UnorderedMap.hpp"

#include "Parameters/Policy/PiecewiseConstantByDomain/Internal/Helper.hpp"


namespace MoReFEM::ParameterNS::Policy
{


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::PiecewiseConstantByDomain(const std::string& name,
                                                                                        const Domain& domain,
                                                                                        parameter_storage_type value)
    : stored_values_per_domain_(value)
    {
        auto& mapping_geom_elt_domain = GetNonCstMapGeomEltDomain();
        mapping_geom_elt_domain.max_load_factor(Utilities::DefaultMaxLoadFactor());

        Internal::ParameterNS::PiecewiseConstantByDomainNS::CheckShapeConsistency<TypeT, StorageT>(value, name);

        decltype(auto) mesh = domain.GetMesh();

        const auto& geom_elem_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

        const auto& domain_manager_instance = DomainManager::GetInstance();

        decltype(auto) stored_values_per_domain = GetStoredValuesPerDomain();

        for (const auto& geom_elt_ptr : geom_elem_list)
        {
            for (const auto& [domain_index, stored_value] : stored_values_per_domain)
            {
                // Domain here is expected to be a subset of the whole \a Domain upon which the \a Parameter is
                // built.
                const auto& current_domain = domain_manager_instance.GetDomain(domain_index);

                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                if (!current_domain.IsGeometricEltInside(geom_elt))
                    continue;

                const auto geom_elt_index = geom_elt.GetIndex();

                auto it = mapping_geom_elt_domain.find(geom_elt_index);
                if (it == mapping_geom_elt_domain.cend())
                    mapping_geom_elt_domain.insert({ geom_elt_index, { current_domain.GetUniqueId() } });
                else
                {
                    // We authorize a \a GeometricElt to be in two distinct Domains for a Parameter definition
                    // only if the associated value is the same for both. If at some point we introduce the
                    // possibility to modify the value of a Parameter within a Domain, this should be revisited
                    // (one could forbid it or add the safety to ensure there is no discrepancy - a safety
                    // is present in debug mode and would trigger an assert if at some point two inconsistent
                    // values were made available for a same \a GeometricÈlt.
                    const auto& already_stored_domain_id_list = it->second;

                    for (const auto& already_stored_domain_id : already_stored_domain_id_list)
                    {
                        auto it_value_for_domain = value.find(already_stored_domain_id);

                        assert(it_value_for_domain != value.cend()
                               && "If not fulfilled something is askew in the internals of the policy.");

                        bool check{ true };

                        if constexpr (TypeT == ParameterNS::Type::scalar)
                            check = NumericNS::AreEqual(it_value_for_domain->second, stored_value);
                        else
                            check = it_value_for_domain->second.isApprox(stored_value);

                        if (!check)
                        {
                            throw ::MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentDomains(
                                geom_elt.GetIndex(),
                                std::make_pair(current_domain.GetUniqueId(), already_stored_domain_id),
                                name);
                        }
                    }
                }
            }
        }
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline auto PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::GetValueFromPolicy(
        [[maybe_unused]] const local_coords_type& local_coords,
        const GeometricElt& geom_elt) const -> return_type
    {
        // First identify the relevant domain for the current \a geom_elt.
        const auto geom_elt_index = geom_elt.GetIndex();

        const auto& mapping_geom_elt_domain = GetMapGeomEltDomain();

        const auto it_domain = mapping_geom_elt_domain.find(geom_elt_index);

        assert(it_domain != mapping_geom_elt_domain.cend());

        const auto& domain_list = it_domain->second;

        const DomainNS::unique_id domain_unique_id = domain_list.back();
        // < by convention if several \a Domain all should yield the same associated value.

        // Then fetch the value related to this domain.
        const auto& stored_values_per_domain = GetStoredValuesPerDomain();

        const auto it = stored_values_per_domain.find(domain_unique_id);
        assert(it != stored_values_per_domain.cend());

        const auto& ret = it->second;

#ifndef NDEBUG
        {
            if (domain_list.size() > 1UL)
            {
                assert(std::all_of(domain_list.cbegin(),
                                   domain_list.cend(),
                                   [&stored_values_per_domain, ret](const auto domain_id)
                                   {
                                       auto it_value = stored_values_per_domain.find(domain_id);
                                       assert(it_value != stored_values_per_domain.cend()
                                              && "If not fulfilled something is askew in the internals of the policy.");

                                       if constexpr (TypeT == ParameterNS::Type::scalar)
                                           return NumericNS::AreEqual(ret, it_value->second);
                                       else
                                           return ret.isApprox(it_value->second);
                                   }));
            }
        }
#endif // NDEBUG

        return ret;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline auto PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::GetAnyValueFromPolicy() const -> return_type
    {
        const auto& stored_values_per_domain = GetStoredValuesPerDomain();
        assert(!stored_values_per_domain.empty());
        return stored_values_per_domain.cbegin()->second;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    [[noreturn]] auto
    PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::GetConstantValueFromPolicy() const noexcept -> return_type
    {
        assert(false && "Parameter class should have guided toward GetValue()!");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline constexpr bool PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::IsConstant() const noexcept
    {
        return false;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    void PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::WriteFromPolicy(std::ostream& stream) const
    {
        stream << "# Domain index; Value by domain:" << std::endl;

        const auto& stored_values_per_domain = GetStoredValuesPerDomain();

        for (const auto& pair : stored_values_per_domain)
            stream << pair.first << ';' << pair.second << std::endl;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline auto PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::GetStoredValuesPerDomain() const noexcept
        -> const parameter_storage_type&
    {
        return stored_values_per_domain_;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline auto PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::GetMapGeomEltDomain() const noexcept
        -> const mapping_geom_elt_domain_type&
    {
        return mapping_geom_elt_domain_;
    }


    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline auto PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::GetNonCstMapGeomEltDomain() noexcept
        -> mapping_geom_elt_domain_type&
    {
        return const_cast<mapping_geom_elt_domain_type&>(GetMapGeomEltDomain());
    }

    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
    inline void
    PiecewiseConstantByDomain<TypeT, TimeManagerT, StorageT>::SetConstantValue([[maybe_unused]] value_type value)
    {
        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_PIECEWISECONSTANTBYDOMAIN_PIECEWISECONSTANTBYDOMAIN_DOT_HXX_
// *** MoReFEM end header guards *** < //
