### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_PARAM}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/AtDof/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/AtQuadraturePoint/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Constant/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/PiecewiseConstantByDomain/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/SpatialFunction/SourceList.cmake)
