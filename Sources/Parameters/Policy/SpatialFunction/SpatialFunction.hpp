// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"


namespace MoReFEM::ParameterNS::Policy
{

    /*!
     * \class doxygen_hide_parameter_type_tparam_for_spatial_function_policy
     *
     * \tparam TypeT This template arguments here is there just because it is expected in the specialization of
     * \a Parameter; for a Lua function it works only for a scalar type (enforced through `requires` keyword).
     */


    /*!
     * \brief Parameter policy when the parameter gets a value given by an analytic function provided in the
     * input data file in Lua language
     *
     * \copydoc doxygen_hide_parameter_type_tparam_for_spatial_function_policy
     *
     * \tparam CoordsTypeT Whether we deal with local or global coordinates for the function.
     *
     * \copydoc doxygen_hide_tparam_parameter_concept_storage
     *
     * \internal <b><tt>[internal]</tt></b> An enum or a template template parameter would be more elegant for
     * SettingsSpatialFunctionT, but it is unfortunately not possible, as the policy is passed through variadic
     * arguments, and variadic arguments can't mix type and non-type parameters.
     * \endinternal
     */
    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
             CoordsType CoordsTypeT>
    class SpatialFunction
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

      private:
        //! Alias to traits class related to TypeT.
        using traits = Internal::ParameterNS::Traits<TypeT, StorageT>;

      public:
        //! Alias to return type.
        using return_type = typename traits::return_type;

        //! Alias to the type of the Lua function.
        using lua_function_type = Wrappers::Lua::spatial_function;

        //! Alias to the storage of the Lua function.
        using parameter_storage_type = const lua_function_type&;

        //! Type of the values expressed at a given \a LocalCoords by the \a Parameter.
        using value_type = typename traits::value_type;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_parameter_name_and_domain_arg
         * \param[in] lua_function The lua function that returns the value of the Parameter.
         */
        explicit SpatialFunction(const std::string& name, const Domain& domain, parameter_storage_type lua_function);

        //! Destructor.
        ~SpatialFunction() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SpatialFunction(const SpatialFunction& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SpatialFunction(SpatialFunction&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SpatialFunction& operator=(const SpatialFunction& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SpatialFunction& operator=(SpatialFunction&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Enables to modify the constant value of a parameter. Disabled for this Policy.
         */
        void SetConstantValue(value_type);

      protected:
        /*!
         * \brief Provided here to make the code compile, but should never be called
         *
         * (Constant value should not be given through a function: it is less efficient and the constness
         * is not appearant in the code...).
         *
         * \return Irrelevant here.
         */
        [[noreturn]] return_type GetConstantValueFromPolicy() const;

        //! \copydoc doxygen_hide_parameter_get_value
        return_type GetValueFromPolicy(const local_coords_type& local_coords, const GeometricElt& geom_elt) const;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        return_type GetAnyValueFromPolicy() const;

      protected:
        //! Whether the parameter varies spatially or not.
        bool IsConstant() const;

        //! Write the content of the parameter for which policy is used in a stream.
        //! \copydoc doxygen_hide_stream_inout
        void WriteFromPolicy(std::ostream& stream) const;

      private:
        /*!
         * \brief Non constant accessor to the helper Coords object, useful when Lua function acts upon global
         * elements.
         *
         * \internal <b><tt>[internal]</tt></b> This is const because the helper coords is mutable to be
         * used in const methods.
         * \endinternal
         *
         * \return Reference to the helper Coords object.
         */
        SpatialPoint& GetNonCstWorkCoords() const noexcept;

      private:
        //! Store a reference to the Lua function (which is owned by the InputData class).
        parameter_storage_type lua_function_;

        //! Helper Coords object, useful when Lua function acts upon global elements.
        mutable SpatialPoint work_coords_;
    };


    /*!
     * \class doxygen_hide_parameter_lua_function_specialization
     *
     * \copydoc doxygen_hide_parameter_type_tparam_for_spatial_function_policy
     *
     * \internal This is helpful to run it within template programming context: alias defined here gets
     * this way a similar nature as Constant or PiecewiseConstantByDomain and hence may be used in functions
     * expecting a specific template template parameter.
     * \endinternal
     */

    /*!
     * \brief Specialization of the LuaFuntion policy for spatial functions with global coordinates.
     *
     * The \a Parameter is evaluated locally as are all \a Parameters, but the Lua function provided in the
     * Lua file is defined with x, y and z as GLOBAL coordinates.
     *
     * \copydoc doxygen_hide_parameter_lua_function_specialization
     *
     */
    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        requires(TypeT == ParameterNS::Type::scalar and std::is_same_v<StorageT, double>)
    using SpatialFunctionGlobalCoords = SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsType::global>;


    /*!
     * \brief Specialization of the LuaFuntion policy for spatial functions with local coordinates.
     *
     * \copydoc doxygen_hide_parameter_lua_function_specialization
     *
     */
    template<ParameterNS::Type TypeT,
             TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT>
        requires(TypeT == ParameterNS::Type::scalar and std::is_same_v<StorageT, double>)
    using SpatialFunctionLocalCoords = SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsType::local>;


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Policy/SpatialFunction/SpatialFunction.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
