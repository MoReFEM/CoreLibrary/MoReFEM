// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ParameterNS::Policy
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::SpatialFunction(const std::string&,
                                                                                 const Domain&,
                                                                                 parameter_storage_type lua_function)
    : lua_function_(lua_function)
    { }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    auto SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::GetValueFromPolicy(
        const local_coords_type& local_coords,
        const GeometricElt& geom_elt) const -> return_type
    {
        if constexpr (CoordsTypeT == CoordsType::local)
            return lua_function_(local_coords.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{}),
                                 local_coords.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{ 1 }),
                                 local_coords.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{ 2 }));

        else if constexpr (CoordsTypeT == CoordsType::global)
        {
            auto& coords = GetNonCstWorkCoords();
            Advanced::GeomEltNS::Local2Global(geom_elt, local_coords, coords);
            return lua_function_(coords.x(), coords.y(), coords.z());
        }

        assert(false
               && "Should never happen as all possible values of the enum class are addressed in the "
                  "cases above.");
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    auto SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::GetAnyValueFromPolicy() const -> return_type
    {
        return lua_function_(0., 0., 0.);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    [[noreturn]] auto SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::GetConstantValueFromPolicy() const
        -> return_type
    {
        assert(false && "A Lua function should yield IsConstant() = false!");
        exit(EXIT_FAILURE);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    bool SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::IsConstant() const
    {
        return false;
    }

    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    void SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::WriteFromPolicy(std::ostream& stream) const
    {
        stream << "# Given by the Lua function given in the input data file." << std::endl;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    inline SpatialPoint&
    SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::GetNonCstWorkCoords() const noexcept
    {
        return work_coords_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::Advanced::Concept::ParameterNS::Storage StorageT,
        CoordsType CoordsTypeT
    >
    // clang-format on
    inline void
    SpatialFunction<TypeT, TimeManagerT, StorageT, CoordsTypeT>::SetConstantValue([[maybe_unused]] value_type value)
    {
        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
