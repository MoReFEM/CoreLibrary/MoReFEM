### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_PARAM}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
		${CMAKE_CURRENT_LIST_DIR}/Parameter.hpp
		${CMAKE_CURRENT_LIST_DIR}/Parameter.hxx
		${CMAKE_CURRENT_LIST_DIR}/ParameterAtDof.hpp
		${CMAKE_CURRENT_LIST_DIR}/ParameterAtQuadraturePoint.hpp
		${CMAKE_CURRENT_LIST_DIR}/Parameters.doxygen
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InitParameterFromInputData/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TimeDependency/SourceList.cmake)
