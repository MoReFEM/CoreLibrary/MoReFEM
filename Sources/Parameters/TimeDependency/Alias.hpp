// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_ALIAS_DOT_HPP_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_ALIAS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <vector>

#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hpp"
#include "Parameters/TimeDependency/Internal/InitStoredQuantity.hpp"
#include "Parameters/TimeDependency/Policy/FromFile.hpp"
#include "Parameters/TimeDependency/Policy/Functor.hpp"


namespace MoReFEM::ParameterNS
{

    /*!
     * \brief An instantiation of \a TimeDependencyNS::Base where the function is given by a mere std::function.
     *
     * \tparam TypeT Whether the parameter at a local position yields a scalar, a vector or a matrix.
     */
    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    using TimeDependencyFunctor =
        TimeDependencyNS::Base<TypeT, TimeManagerT, TimeDependencyNS::PolicyNS::Functor<std::function<double(double)>>>;

    /*!
     * \brief An instantiation of \a TimeDependencyNS::Base where the time dependency policy is FromFile.
     *
     * \tparam TypeT Whether the parameter at a local position yields a scalar, a vector or a matrix.
     */
    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    using TimeDependencyFromFile = TimeDependencyNS::Base<TypeT, TimeManagerT, TimeDependencyNS::PolicyNS::FromFile>;

} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_ALIAS_DOT_HPP_
// *** MoReFEM end header guards *** < //
