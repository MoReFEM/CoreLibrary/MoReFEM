// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_APPLYTIMEFACTOR_DOT_HPP_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_APPLYTIMEFACTOR_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS
{


    /*!
     * \class doxygen_hide_apply_time_factor
     *
     * \brief Update the \a value_with_time_factor obtained after multiplying the time factor by the
     * purely spatial component.
     *
     * Or in short, compute f(x) * g(t) given both parts.
     *
     * \copydoc doxygen_hide_param_time_dependancy
     *
     * The difficulty is that f(x) might be a scalar, a vector or a matrix; the different overloads here
     * take care of each of this case.
     *
     * \param[in] value_without_time_factor Value which was obtained before applying time dependency.
     * \param[in] time_factor Time factor at the considered local position (it's the job of TimeDependency
     * class to provide the right value; we're at a very low-level here).
     * \param[out] value_with_time_factor Result of \a value_without_time_factor * \a time_factor.
     */

    /*!
     * \copydoc doxygen_hide_apply_time_factor
     *
     * <b>Overload for TypeT == Type::scalar.</b>
     */
    void ApplyTimeFactor(double value_without_time_factor, double time_factor, double& value_with_time_factor) noexcept;


    /*!
     * \copydoc doxygen_hide_apply_time_factor
     *
     * <b>Overload for TypeT == Type::vector and TypeT == Type::matrix </b>
     */
    template<class LocalLinalgT>
    void ApplyTimeFactor(const LocalLinalgT& value_without_time_factor,
                         double time_factor,
                         LocalLinalgT& value_with_time_factor);


} // namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_APPLYTIMEFACTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
