// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_INITSTOREDQUANTITY_DOT_HPP_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_INITSTOREDQUANTITY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Internal/Alias.hpp"


namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS
{


    /*!
     * \brief Helper class to mimic static if.
     *
     * The point is to properly allocate the object that stores the full value of a Parameter at a \a
     * QuadraturePoint, once time factor has been taken into account.
     *
     */
    template<Type TypeT>
    struct InitStoredQuantity;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================

    template<>
    struct InitStoredQuantity<Type::scalar>
    {

        static void Perform(double any_value, double& stored_value);
    };


    template<>
    struct InitStoredQuantity<Type::vector>
    {

        template<class LocalVectorT>
        static void Perform(const LocalVectorT& any_value, LocalVectorT& stored_value);
    };


    template<>
    struct InitStoredQuantity<Type::matrix>
    {

        template<class LocalMatrixT>
        static void Perform(const LocalMatrixT& any_value, LocalMatrixT& stored_value);
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Parameters/TimeDependency/Internal/InitStoredQuantity.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_INITSTOREDQUANTITY_DOT_HPP_
// *** MoReFEM end header guards *** < //
