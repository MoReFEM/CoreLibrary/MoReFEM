// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_INITSTOREDQUANTITY_DOT_HXX_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_INITSTOREDQUANTITY_DOT_HXX_
// IWYU pragma: private, include "Parameters/TimeDependency/Internal/InitStoredQuantity.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS
{


    template<class LocalVectorT>
    void InitStoredQuantity<Type::vector>::Perform(const LocalVectorT& any_value, LocalVectorT& stored_value)
    {
        stored_value.resize(any_value.size());
    }


    template<class LocalMatrixT>
    void InitStoredQuantity<Type::matrix>::Perform(const LocalMatrixT& any_value, LocalMatrixT& stored_value)
    {
        stored_value.resize(any_value.rows(), any_value.cols());
    }


} // namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_INITSTOREDQUANTITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
