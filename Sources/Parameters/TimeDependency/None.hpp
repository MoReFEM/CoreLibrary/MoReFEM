// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_NONE_DOT_HPP_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_NONE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>

#include "Core/TimeManager/Concept.hpp" // IWYU pragma: export


namespace MoReFEM::ParameterNS::TimeDependencyNS
{


    /*!
     * \brief Policy to use when there are no time dependency for the \a Parameter.
     */
    template<Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct None
    {

        //! Static value that tells there is no modification of the time for this policy.
        static inline constexpr bool no_time_dependency = true;

        //! Convenient alias.
        using unique_ptr = std::unique_ptr<None>;

        //! Defined for conveniency; do nothing.
        void Update()
        { }

        //! Defined for conveniency; do nothing.
        void Update(double)
        { }
    };


} // namespace MoReFEM::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_NONE_DOT_HPP_
// *** MoReFEM end header guards *** < //
