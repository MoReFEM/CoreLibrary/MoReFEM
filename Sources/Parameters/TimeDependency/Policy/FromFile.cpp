// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <fstream>
#include <ios>
#include <limits>
#include <sstream>
#include <utility>

#include "Parameters/TimeDependency/Policy/FromFile.hpp"

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS
{


    FromFile::FromFile(const FilesystemNS::File& file)
    {
        std::ifstream file_in{ file.Read() };

        // Skip the first line
        const std::streamsize max_stream_size = std::numeric_limits<std::streamsize>::max();

        file_in.ignore(max_stream_size, '\n');

        auto& storage = GetNonCstStorage();

        while (file_in)
        {
            std::array<double, 2UL> buf_values{};

            for (std::size_t i = 0UL; i < 2; ++i)
            {
                file_in >> buf_values[i];
            }

            storage.emplace_back(buf_values[0], buf_values[1]);
        }

        if (!std::ranges::is_sorted(storage,

                                    [](const auto& lhs, const auto& rhs)
                                    {
                                        return lhs.first < rhs.first;
                                    }))
        {
            std::ostringstream oconv;
            oconv << "Times in " << file << " are not sorted correctly!";

            throw Exception(oconv.str());
        }

        if (!file_in.eof())
        {
            std::ostringstream oconv;
            oconv << "Error while reading " << file;

            throw Exception(oconv.str());
        }
    }


    namespace // anonymous
    {


        struct Comp
        {
            bool operator()(const std::pair<double, double>& pair, const double time)
            {
                return pair.first < time;
            }

            bool operator()(const double time, const std::pair<double, double>& pair)
            {
                return time < pair.first;
            }
        };


    } // namespace


    double FromFile::GetCurrentTimeFactor(double time) const
    {
        constexpr auto epsilon = 1.e-12;
        const auto& storage = GetStorage();
        const auto begin = storage.cbegin();
        const auto end = storage.cend();

        const auto it = std::find_if(begin,
                                     end,
                                     [time, epsilon](const auto& pair)
                                     {
                                         return NumericNS::AreEqual(pair.first, time, epsilon);
                                     });

        if (it != end)
        {
            return it->second;
        } else
        {
            auto it_pair = std::equal_range(begin, end, time, Comp());

            auto& it_lower = it_pair.first;
            const auto& it_upper = it_pair.second;

            if (it_lower == begin && it_upper != end)
            {
                return it_upper->second;
            } else if (it_lower != begin && it_upper == end)
            {
                --it_lower;
                return it_lower->second;
            } else if (it_lower != begin && it_upper != end)
            {
                --it_lower;
                const double t_inf = it_lower->first;
                const double t_sup = it_upper->first;
                const double value_inf = it_lower->second;
                const double value_sup = it_upper->second;
                assert(!NumericNS::IsZero(t_sup - t_inf));
                const double alpha = (t_sup - time) / (t_sup - t_inf);

                return alpha * value_inf + (1. - alpha) * value_sup;
            } else
            {
                throw Exception("No value found for the time step for Time Dependency Policy From File. "
                                "It should not happen so verify the time step that you gave is in the "
                                "range of time of the simulation.");
            }
        }
    }


} // namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //
