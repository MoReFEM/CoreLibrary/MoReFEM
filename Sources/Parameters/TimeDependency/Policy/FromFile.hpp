// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FROMFILE_DOT_HPP_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FROMFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS
{


    /*!
     * \brief Policy of the time dependency based on a file.
     *
     * \copydoc doxygen_hide_param_time_dependancy
     */
    class FromFile
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = FromFile;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        //! Alias to the return type.
        using return_type = double;

      private:
        //! Alias to the way each value is stored.
        using storage_value_type = std::decay_t<return_type>;

        //! Alias to value holder.
        using value_holder_type = double;

        /*!
         * \brief Alias to the type of the value actually stored.
         *
         * First double is the time ie each time step.
         * Second double is the value of the parameter at the time step considered.
         */
        using storage_type = std::vector<std::pair<double, double>>;

      public:
        /// \name Special members.
        ///@{

        /*! Constructor.
         *
         * \param[in] file File in which time dependency is read.
         * The format is two columns: in the first one the time in seconds is read, in the second
         * the time-dependent value. First column is expected to be given in ascending order.
         * Currently all time steps are supposed to be found exactly (with
         * a very small leeway to take into account numerical imprecision...); later on values should
         * be interpolated if missing.
         */
        explicit FromFile(const FilesystemNS::File& file);

        //! Destructor.
        ~FromFile() = default;

        //! \copydoc doxygen_hide_copy_constructor
        FromFile(const FromFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FromFile(FromFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FromFile& operator=(const FromFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FromFile& operator=(FromFile&& rhs) = delete;

        ///@}

      public:
        //! Compute the current value at a given time.
        //! \param[in] time Time for which value is sought.
        double GetCurrentTimeFactor(double time) const;

      private:
        //! Constant accessor to storage.
        const storage_type& GetStorage() const noexcept;

        //! Constant accessor to storage.
        storage_type& GetNonCstStorage() noexcept;

      private:
        //! Values of the parameter at each time step.
        storage_type storage_;
    };


} // namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/TimeDependency/Policy/FromFile.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FROMFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
