// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FROMFILE_DOT_HXX_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FROMFILE_DOT_HXX_
// IWYU pragma: private, include "Parameters/TimeDependency/Policy/FromFile.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS
{

    inline const FromFile::storage_type& FromFile::GetStorage() const noexcept
    {
        return storage_;
    }


    inline FromFile::storage_type& FromFile::GetNonCstStorage() noexcept
    {
        return const_cast<storage_type&>(GetStorage());
    }


} // namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FROMFILE_DOT_HXX_
// *** MoReFEM end header guards *** < //
