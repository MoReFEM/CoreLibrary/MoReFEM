// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_TIMEDEPENDENCY_DOT_HXX_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_TIMEDEPENDENCY_DOT_HXX_
// IWYU pragma: private, include "Parameters/TimeDependency/TimeDependency.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::ParameterNS::TimeDependencyNS
{


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    template<typename... Args>
    Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::Base(const TimeManagerT& time_manager, Args&&... args)
    : TimeDependencyPolicyT(std::forward<Args>(args)...), time_manager_(time_manager)
    {
        const auto& dependency_policy_ptr = static_cast<TimeDependencyPolicyT*>(this);
        auto& dependency_policy = *dependency_policy_ptr;
        current_time_factor_ = dependency_policy.GetCurrentTimeFactor(time_manager.GetTime());
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    void Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::Init(return_type any_value)
    {
        Internal::ParameterNS::TimeDependencyNS::InitStoredQuantity<TypeT>::Perform(any_value,
                                                                                    GetNonCstValueWithTimeFactor());
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    typename Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::return_type
    Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::ApplyTimeFactor(return_type value_without_time_factor) const
    {
        auto& ret = GetNonCstValueWithTimeFactor();

        Internal::ParameterNS::TimeDependencyNS::ApplyTimeFactor(
            value_without_time_factor, GetCurrentTimeFactor(), ret);

        return ret;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    inline const TimeManagerT& Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    void Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::Update()
    {
        decltype(auto) time_manager = GetTimeManager();

        UpdateNtimeModifiedAtLastUpdate();

        const auto& dependency_policy_ptr = static_cast<TimeDependencyPolicyT*>(this);
        auto& dependency_policy = *dependency_policy_ptr;

        current_time_factor_ = dependency_policy.GetCurrentTimeFactor(time_manager.GetTime());
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    void Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::Update(double time)
    {
        UpdateNtimeModifiedAtLastUpdate();

        const auto& dependency_policy_ptr = static_cast<TimeDependencyPolicyT*>(this);
        auto& dependency_policy = *dependency_policy_ptr;

        current_time_factor_ = dependency_policy.GetCurrentTimeFactor(time);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    void Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::UpdateNtimeModifiedAtLastUpdate()
    {
        Ntimes_modified_at_last_update_ = GetTimeManager().NtimeModified();
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    std::size_t Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::GetNtimeModifiedAtLastUpdate() const noexcept
    {
        return Ntimes_modified_at_last_update_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    inline double Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::GetCurrentTimeFactor() const noexcept
    {
#ifndef NDEBUG
        {
            const auto lhs = GetNtimeModifiedAtLastUpdate();
            const auto rhs = GetTimeManager().NtimeModified();

            assert(lhs == rhs && "Check parameter is correctly up-to-date!");
        }
#endif // NDEBUG
        return current_time_factor_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, class TimeDependencyPolicyT>
    inline typename Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::storage_type&
    Base<TypeT, TimeManagerT, TimeDependencyPolicyT>::GetNonCstValueWithTimeFactor() const noexcept
    {
        return value_with_time_factor_;
    }


} // namespace MoReFEM::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_TIMEDEPENDENCY_DOT_HXX_
// *** MoReFEM end header guards *** < //
