// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_CONVERTLINEARALGEBRA_FREEFEM_MATRIXCONVERSION_DOT_HPP_
#define MOREFEM_POSTPROCESSING_CONVERTLINEARALGEBRA_FREEFEM_MATRIXCONVERSION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

#include "Utilities/Mpi/Mpi.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"

#include "PostProcessing/PostProcessing.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::FreefemNS
{


    /*!
     * \brief Convert a matrix in Freefem format.
     *
     * \note I need this to find a bug in poromechanics, so I targeted especially what I needed: P1 and P1b
     * on a sequential model. To handle parallel or other shape functions, more work is required (for P2
     * for instances Edges should also be extracted). It works probably only in 2D as well (but maybe not;
     * 3D might work as fine).
     */
    class MatrixConversion : public ::MoReFEM::Crtp::CrtpMpi<MatrixConversion>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MatrixConversion;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Mpi parent.
        using mpi_parent = ::MoReFEM::Crtp::CrtpMpi<MatrixConversion>;


      public:
        /*!
         * \class doxygen_hide_matrix_conv_unknown_in_matrix_list_arg
         *
         * \param[in] unknown_in_matrix_list List of unknowns that are represented in the matrix, correctly
         * sorted. This is there as for some modles there might be more unknowns than the one in the matrices,
         * so here you may tell which one to consider. The name given here must match exactly the one written
         * in unknowns.hhdata (which in turn were those from the Lua file).
         */


        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] matrix_binary_file File that include the matrix; this file must have been obtained
         * through Matrix::ViewBinary() method.
         * \param[in] freefem_numbering_information_file Freefem proceeds to an ad hoc renumbering
         * for shape functions other than P1; we need this information to map MoReFEM dofs to Freefem's.
         *
         * So we just need a file which gives the following information:
         * \verbatim
         10 14 9 12
         10 9 6 5
         ...
         \endverbatim
         * where each line provide the list of dofs for the geometric element that share the line number
         * Dofs on vertex come first, then on edges, then on faces.
         * See #936 discussion for more details (and for the information from Freefem's author).
         * The following code in Freefem provides the requested file:
         * \code
         * mesh Th=readmesh('*PATH TO THE MESH*');
         *
         * fespace Wh(Th, *SHAPE FUNCTION, e.g. P1b*);
         * int Nelem = Th.nt; // in case of triangles.
         * int kdf = Wh.ndofK;
         *
         * for (int elem = 0; elem < Nelem; ++elem)
         * {
         *      for (auto i = 0UL; i < kdf; i++)
         *          cout << Wh(elem, i) << " ";
         *      cout << endl;
         * }
         * \endcode
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_matrix_conv_unknown_in_matrix_list_arg
         *
         * \param[in] output_directory Directory into which both transfert matrix and renumbered matrix will be
         * written.
         * \param[in] data_directory Data directory into which all outputs of a given model are written; it is
         * the directory given in the input data file to store result.
         * \param[in] numbering_subset Numbering subset associated to the matrix (so far we assume a square
         * matrix with same numbering subset for rows and columns).
         * \param[in] mesh Mesh upon which the matrix was built (more exactly upon its associated \a GodOfDof).
         * \copydoc doxygen_hide_source_location
         *
         */
        explicit MatrixConversion(const Wrappers::Mpi& mpi,
                                  const FilesystemNS::File& freefem_numbering_information_file,
                                  const FilesystemNS::File& matrix_binary_file,
                                  const FilesystemNS::Directory& data_directory,
                                  const std::vector<std::string>& unknown_in_matrix_list,
                                  const FilesystemNS::Directory& output_directory,
                                  const NumberingSubset& numbering_subset,
                                  const Mesh& mesh,
                                  const std::source_location location = std::source_location::current());

        //! Destructor.
        ~MatrixConversion() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MatrixConversion(const MatrixConversion& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MatrixConversion(MatrixConversion&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MatrixConversion& operator=(const MatrixConversion& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MatrixConversion& operator=(MatrixConversion&& rhs) = delete;

        ///@}

      private:
        //! Constant accessor to the matrix loaded from the binary file.
        const Wrappers::Petsc::Matrix& GetOriginalMatrix() const noexcept;

        //! Constant accessor to post processing class.
        const PostProcessing& GetPostProcessingData() const noexcept;

        //! Index is the dof number in MoReFEM, value is the dof number in Freefem.
        const std::vector<std::size_t>& GetDofMapping() const noexcept;

        //! Set dof mapping.
        //! \param[in] value Value to assign as dof mapping.
        void SetDofMapping(std::vector<std::size_t>&& value);


        /*!
         * \brief Compute the transfert matrix.
         *
         */
        void ComputeTransfertMatrix() const;

        /*!
         * \brief Get the list of dofs in Freefem for a \a GeometricElt.
         *
         * \param[in] highest_dim_geom_elt_index Index that match the \a GeometricElt for which the list of dofs
         * is sought.
         * \warning It is not the output of GeometricElt::GetIndex(), as this one takes into account the
         * segments for instance in a 2D mesh.
         *
         * \return Indexes of dofs in Freefem related to the \a GeometricElt.
         */
        const std::vector<std::size_t>& GetFreefemDofList(std::size_t highest_dim_geom_elt_index) const noexcept;

        //! Accessor to the Freefem dof numbering.
        const std::vector<std::vector<std::size_t>>& GetFreefemDofNumberingPerGeomElt() const noexcept;

        /*!
         * \brief Compute the mapping of dof MoReFEM - Freefem.
         *
         * \param[in] numbering_subset \a NumberingSubset for which the dof mapping is computed.
         * \copydoc doxygen_hide_matrix_conv_unknown_in_matrix_list_arg
         */
        void ComputeDofMapping(const NumberingSubset& numbering_subset,
                               const std::vector<std::string>& unknown_in_matrix_list);

        //! Output directory.
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;


      private:
        //! Matrix loaded from the binary file.
        Wrappers::Petsc::Matrix original_matrix_;

        //! Post processing class.
        PostProcessing::const_unique_ptr post_processing_data_ = nullptr;

        //! Index is the dof number in MoReFEM, value is the dof number in Freefem.
        std::vector<std::size_t> dof_mapping_;

        /*!
         * \brief For each GeometricElement (index of outer vector), the list of related dofs in Freefem.
         */
        std::vector<std::vector<std::size_t>> freefem_dof_numbering_per_geom_elt_;

        //! Output directory.
        const FilesystemNS::Directory& output_directory_;
    };


} // namespace MoReFEM::PostProcessingNS::FreefemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/ConvertLinearAlgebra/Freefem/MatrixConversion.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_CONVERTLINEARALGEBRA_FREEFEM_MATRIXCONVERSION_DOT_HPP_
// *** MoReFEM end header guards *** < //
