// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_CONVERTLINEARALGEBRA_FREEFEM_MATRIXCONVERSION_DOT_HXX_
#define MOREFEM_POSTPROCESSING_CONVERTLINEARALGEBRA_FREEFEM_MATRIXCONVERSION_DOT_HXX_
// IWYU pragma: private, include "PostProcessing/ConvertLinearAlgebra/Freefem/MatrixConversion.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::Wrappers::Petsc { class Matrix; }
namespace MoReFEM::PostProcessingNS { class PostProcessing; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::FreefemNS
{


    inline const Wrappers::Petsc::Matrix& MatrixConversion::GetOriginalMatrix() const noexcept
    {
        return original_matrix_;
    }


    inline const PostProcessing& MatrixConversion::GetPostProcessingData() const noexcept
    {
        assert(!(!post_processing_data_));
        return *post_processing_data_;
    }


    inline const std::vector<std::vector<std::size_t>>&
    MatrixConversion ::GetFreefemDofNumberingPerGeomElt() const noexcept
    {
        return freefem_dof_numbering_per_geom_elt_;
    }


    inline const std::vector<std::size_t>&
    MatrixConversion ::GetFreefemDofList(const std::size_t highest_dim_geom_elt_index) const noexcept
    {
        decltype(auto) freefem_dof_numbering_per_geom_elt = GetFreefemDofNumberingPerGeomElt();

        assert(highest_dim_geom_elt_index < freefem_dof_numbering_per_geom_elt.size());

        return freefem_dof_numbering_per_geom_elt[highest_dim_geom_elt_index];
    }


    inline const std::vector<std::size_t>& MatrixConversion::GetDofMapping() const noexcept
    {
        return dof_mapping_;
    }


    inline void MatrixConversion::SetDofMapping(std::vector<std::size_t>&& value)
    {
        assert(dof_mapping_.empty());
        dof_mapping_ = std::move(value);
    }


    inline const FilesystemNS::Directory& MatrixConversion::GetOutputDirectory() const noexcept
    {
        return output_directory_;
    }


} // namespace MoReFEM::PostProcessingNS::FreefemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_CONVERTLINEARALGEBRA_FREEFEM_MATRIXCONVERSION_DOT_HXX_
// *** MoReFEM end header guards *** < //
