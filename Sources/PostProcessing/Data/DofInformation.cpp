// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>
// IWYU pragma: no_include <iosfwd>

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"

#include "Geometry/Interfaces/EnumInterface.hpp"

#include "PostProcessing/Data/DofInformation.hpp"
#include "PostProcessing/File/InterfaceFile.hpp"


namespace MoReFEM::PostProcessingNS::Data
{


    DofInformation::DofInformation(const std::string& line, const InterfaceFile& interface_file)
    {
        std::istringstream iconv(line);

        {
            iconv >> program_wise_index_;
            if (iconv.fail())
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);
        }

        iconv.ignore(); // the ';'

        {

            iconv >> processor_wise_index_;
            if (iconv.fail())
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);
        }

        iconv.ignore(); // the ';'

        // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
        InterfaceNS::Nature interface_nature;

        {
            std::string interface_name;
            iconv >> interface_name;
            if (iconv.fail())
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

            interface_nature = InterfaceNS::GetNature(interface_name);
        }

        std::size_t interface_index = 0;

        {
            iconv >> interface_index;
            if (iconv.fail())
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);
        }

        iconv.ignore(); // the ';'

        {
            iconv >> unknown_name_;
            if (iconv.fail())
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);
        }

        {
            iconv >> unknown_component_;
            if (iconv.fail())
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);
        }

        iconv.ignore(); // the ';'

        {
            iconv >> shape_function_label_;
            if (iconv.fail())
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);
        }


        interface_ = &(interface_file.GetInterface(interface_nature, interface_index));
    }


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
