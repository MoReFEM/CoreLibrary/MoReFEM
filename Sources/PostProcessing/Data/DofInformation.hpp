// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_DATA_DOFINFORMATION_DOT_HPP_
#define MOREFEM_POSTPROCESSING_DATA_DOFINFORMATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::PostProcessingNS { class InterfaceFile; }
namespace MoReFEM::PostProcessingNS::Data { class Interface; }
namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::Data
{


    /*!
     * \brief Class which holds the information obtained from one line of dof_information.hhdata output files.
     *
     * There is one such set of files per \a NumberingSubset (each under a 'Rank_' subfolder).
     *
     * Format of one such file is:
     *
     * \verbatim
     Ndof (processor_wise) = 291
     # First column: program-wise index.
     # Second column: processor-wise index.
     # Third column: the interface upon which the dof is located. Look at the interface file in same directory
     to relate it to the initial mesh. # Fourth column: unknown and component involved. # Fifth column: shape
     function label. 0;0;Vertex 0;fluid_velocity 0;P1b 1;1;Vertex 0;fluid_velocity 1;P1b 2;2;Vertex 0;fluid_mass
     0;P1 3;3;Vertex 1;fluid_velocity 0;P1b 4;4;Vertex 1;fluid_velocity 1;P1b
     ...
     \endverbatim
     */
    class DofInformation final
    {

      public:
        //! Alias to unique pointer.
        using const_shared_ptr = std::shared_ptr<const DofInformation>;

        //! Alias to vector of shared pointer.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] line Line from the output file generated by MoReFEM. Its expected format is:
         * . First column: program-wise index.
         * . Second column: processor-wise index.
         * . Third column: the interface upon which the dof is located. Look at the interface file in same
         * directory to relate it to the initial mesh. . Fourth column: unknown and component involved. . Fifth
         * column: shape function label. and the separator is a semicolon ';'.
         *
         * For instance:
         * 0;0;Vertex 0;displacement 0;P1
         *
         * \param[in] interface_file Object which holds the data loaded from the output interface file.
         */
        explicit DofInformation(const std::string& line, const InterfaceFile& interface_file);

        //! Destructor.
        ~DofInformation() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DofInformation(const DofInformation& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        DofInformation(DofInformation&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DofInformation& operator=(const DofInformation& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        DofInformation& operator=(DofInformation&& rhs) = default;

        ///@}


        //! Program-wise index.
        std::size_t GetProgramWiseIndex() const;

        //! Processor-wise index.
        std::size_t GetProcessorWiseIndex() const;

        //! Interface on which the dof is.
        InterfaceNS::Nature GetInterfaceNature() const;

        //! Return the interface onto which the dof is located.
        const Data::Interface& GetInterface() const noexcept;

        //! Interface on which the dof is.
        std::size_t GetInterfaceIndex() const;

        //! Unknown name.
        const std::string& GetUnknown() const;

        //! Component of the unknown (0 if scalar).
        Eigen::Index GetUnknownComponent() const;

        //! Shape function label.
        const std::string& GetShapeFunctionLabel() const;


      private:
        //! Program-wise index.
        std::size_t program_wise_index_{};

        //! Processor-wise index.
        std::size_t processor_wise_index_{};

        /*!
         * \brief Pointer to the interface onto which the dof is located.
         *
         * \attention Do not delete it: the original object is managed by a smart pointer.
         */
        const Data::Interface* interface_ = nullptr;

        //! Unknown name.
        std::string unknown_name_;

        //! Component of the unknown (0 if scalar).
        Eigen::Index unknown_component_{};

        //! Shape function label.
        std::string shape_function_label_;
    };


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/Data/DofInformation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_DATA_DOFINFORMATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
