// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_DATA_DOFINFORMATION_DOT_HXX_
#define MOREFEM_POSTPROCESSING_DATA_DOFINFORMATION_DOT_HXX_
// IWYU pragma: private, include "PostProcessing/Data/DofInformation.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <string>

#include "PostProcessing/Data/Interface.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::Data
{


    inline std::size_t DofInformation::GetProgramWiseIndex() const
    {
        return program_wise_index_;
    }


    inline std::size_t DofInformation::GetProcessorWiseIndex() const
    {
        return processor_wise_index_;
    }


    inline InterfaceNS::Nature DofInformation::GetInterfaceNature() const
    {
        return GetInterface().GetNature();
    }


    inline const Data::Interface& DofInformation::GetInterface() const noexcept
    {
        assert(interface_ != nullptr);
        return *interface_;
    }


    inline std::size_t DofInformation::GetInterfaceIndex() const
    {
        return GetInterface().GetIndex();
    }


    inline const std::string& DofInformation::GetUnknown() const
    {
        return unknown_name_;
    }


    inline auto DofInformation::GetUnknownComponent() const -> Eigen::Index
    {
        return unknown_component_;
    }


    inline const std::string& DofInformation::GetShapeFunctionLabel() const
    {
        return shape_function_label_;
    }


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_DATA_DOFINFORMATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
