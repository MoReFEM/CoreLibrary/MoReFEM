// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_DATA_INTERFACE_DOT_HPP_
#define MOREFEM_POSTPROCESSING_DATA_INTERFACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Coords/StrongType.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::Data
{


    /*!
     * \brief Class which holds the information obtained from one line of interfaces.hhdata output file.
     *
     * There is one such file per mesh.
     *
     * This file gives for each interface (vertex, edge, face or volume)  the list of \a Coords that delimit it.
     */
    class Interface
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Interface;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] nature Nature of the interface.
         * \param[in] index Index of the interface. This index is unique within interfaces of the same nature
         * (e.g. there is only one 'Edge 1' but there might be a 'Vertex 1' or a 'Face 1').
         * \param[in] coords_list  Indexes of the Coords that describe the interface. We assume P1 geometry here
         * (i.e. all Coords are vertex).
         */
        explicit Interface(InterfaceNS::Nature nature,
                           std::size_t index,
                           const std::vector<CoordsNS::processor_wise_position>& coords_list);

        //! Destructor.
        ~Interface() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Interface(const Interface& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Interface(Interface&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Interface& operator=(const Interface& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Interface& operator=(Interface&& rhs) = delete;

        ///@}

      public:
        //! Accessor to the nature of the interface.
        InterfaceNS::Nature GetNature() const noexcept;

        //! Constant accessor to the index of the interface.
        std::size_t GetIndex() const noexcept;

        //! Indexes of the Coords that describe the interface.
        const std::vector<CoordsNS::processor_wise_position>& GetCoordsProcessorWisePositionList() const noexcept;

      private:
        //! Nature of the interface.
        const InterfaceNS::Nature nature_;

        /*!
         * \brief Index of the interface.
         *
         * This index is unique within interfaces of the same nature (e.g. there is only one 'Edge 1' but there
         * might be a 'Vertex 1' or a 'Face 1').
         */
        const std::size_t index_;

        /*!
         * \brief Indexes of the Coords that describe the interface.
         */
        const std::vector<CoordsNS::processor_wise_position> coords_list_;
    };


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/Data/Interface.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_DATA_INTERFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
