// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_DATA_INTERFACE_DOT_HXX_
#define MOREFEM_POSTPROCESSING_DATA_INTERFACE_DOT_HXX_
// IWYU pragma: private, include "PostProcessing/Data/Interface.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Geometry/Coords/StrongType.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::Data
{


    inline InterfaceNS::Nature Interface::GetNature() const noexcept
    {
        return nature_;
    }


    inline std::size_t Interface::GetIndex() const noexcept
    {
        return index_;
    }


    inline const std::vector<CoordsNS::processor_wise_position>&
    Interface::GetCoordsProcessorWisePositionList() const noexcept
    {
        return coords_list_;
    }


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_DATA_INTERFACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
