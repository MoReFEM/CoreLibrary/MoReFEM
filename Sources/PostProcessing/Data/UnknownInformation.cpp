// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <sstream>
#include <string>

#include "PostProcessing/Data/UnknownInformation.hpp"

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"

#include "FiniteElement/Unknown/EnumUnknown.hpp" // IWYU pragma: keep


namespace MoReFEM::PostProcessingNS::Data
{


    UnknownInformation::UnknownInformation(const std::string& line)
    {
        std::istringstream iconv(line);

        iconv >> name_;

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

        iconv.ignore(2); // for colon ':'

        std::string buf;
        iconv >> buf;

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);


        if (buf == "scalar")
            nature_ = UnknownNature::scalar;
        else if (buf == "vectorial")
            nature_ = UnknownNature::vectorial;
        else
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);
    }


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
