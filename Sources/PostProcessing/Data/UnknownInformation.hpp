// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_DATA_UNKNOWNINFORMATION_DOT_HPP_
#define MOREFEM_POSTPROCESSING_DATA_UNKNOWNINFORMATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <string>
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::UnknownNS { enum class Nature; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::Data
{


    //! Convenient alias.
    using UnknownNature = UnknownNS::Nature;


    /*!
     * \brief Class which holds the information obtained from one line of unknowns.hhdata output file.
     *
     * This file gives for each unknown the nature ('scalar' or 'vectorial').
     */
    class UnknownInformation final
    {

      public:
        //! Alias to shared pointer.
        using const_shared_ptr = std::shared_ptr<const UnknownInformation>;

        //! Alias to vector of shared pointers.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] line Line as read in unknowns.hhdata output file. Format is:
         * unknown_name : unknown_nature.
         *
         * For instance: 'displacement : vectorial'.
         */
        explicit UnknownInformation(const std::string& line);

        //! Destructor.
        ~UnknownInformation() = default;

        //! \copydoc doxygen_hide_copy_constructor
        UnknownInformation(const UnknownInformation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UnknownInformation(UnknownInformation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UnknownInformation& operator=(const UnknownInformation& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UnknownInformation& operator=(UnknownInformation&& rhs) = delete;

        ///@}

      public:
        //! Name.
        const std::string& GetName() const;

        //! Nature.
        UnknownNature GetNature() const;


      private:
        //! Name.
        std::string name_;

        //! Nature (scalar or vectorial).
        UnknownNature nature_;
    };


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/Data/UnknownInformation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_DATA_UNKNOWNINFORMATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
