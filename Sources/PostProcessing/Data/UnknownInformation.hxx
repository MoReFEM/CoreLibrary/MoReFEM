// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_DATA_UNKNOWNINFORMATION_DOT_HXX_
#define MOREFEM_POSTPROCESSING_DATA_UNKNOWNINFORMATION_DOT_HXX_
// IWYU pragma: private, include "PostProcessing/Data/UnknownInformation.hpp"
// *** MoReFEM header guards *** < //


#include <string>


namespace MoReFEM::PostProcessingNS::Data
{


    inline const std::string& UnknownInformation::GetName() const
    {
        return name_;
    }


    inline UnknownNature UnknownInformation::GetNature() const
    {
        return nature_;
    }


} // namespace MoReFEM::PostProcessingNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_DATA_UNKNOWNINFORMATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
