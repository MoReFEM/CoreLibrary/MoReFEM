// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <fstream>
#include <memory>
#include <string>

#include "PostProcessing/File/DofInformationFile.hpp"

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Mpi/StrongType.hpp"

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"

#include "Geometry/StrongType.hpp"

#include "PostProcessing/Data/DofInformation.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS
{


    namespace // anonymous
    {


        using difference_type = std::string::iterator::difference_type;


        std::size_t ExtractNdof(std::string line)
        {
            const std::string first_line_key = "Ndof (processor_wise) = ";

            if (!Utilities::String::StartsWith(line, first_line_key))
                throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

            line.erase(line.begin(), line.begin() + static_cast<difference_type>(first_line_key.size()));

            return (std::stoul(line));
        }


    } // namespace


    DofInformationFile::DofInformationFile(rank_type processor,
                                           const FilesystemNS::File& input_file,
                                           const InterfaceFile& interface_file)
    : processor_(processor)
    {
        std::ifstream interface_stream{ input_file.Read() };

        std::string line;

        getline(interface_stream, line);
        const std::size_t n_dof = ExtractNdof(line);

        dof_information_list_.reserve(n_dof);

        while (getline(interface_stream, line))
        {
            // Ignore comment lines.
            if (Utilities::String::StartsWith(line, "#"))
                continue;

            dof_information_list_.emplace_back(std::make_shared<Data::DofInformation>(line, interface_file));
        }

        if (n_dof != dof_information_list_.size())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInFile(
                input_file,
                "the number of announced dofs is not the number "
                "of dofs effectively read.");

        assert(n_dof == Ndof());
    }


    Data::DofInformation::vector_const_shared_ptr DofInformationFile ::GetDof(InterfaceNS::Nature nature,
                                                                              const std::size_t interface_index,
                                                                              const std::string& unknown) const
    {
        Data::DofInformation::vector_const_shared_ptr ret;

        auto match_condition =
            [nature, interface_index, &unknown](const Data::DofInformation::const_shared_ptr& dof_info_ptr)
        {
            assert(!(!dof_info_ptr));
            const auto& dof_info = *dof_info_ptr;


            return dof_info.GetInterfaceNature() == nature && dof_info.GetInterfaceIndex() == interface_index
                   && dof_info.GetUnknown() == unknown;
        };

        auto end = dof_information_list_.cend();

        auto it = std::find_if(dof_information_list_.cbegin(), end, match_condition);

        assert(it != end);

        ret.push_back(*it);

        // We make use of the fact components are stored together in the output format.
        // NOLINTNEXTLINE(bugprone-inc-dec-in-conditions)
        while (++it != end && match_condition(*it))
            ret.push_back(*it);

        return ret;
    }


    const Data::DofInformation& DofInformationFile ::GetDof(InterfaceNS::Nature nature,
                                                            const std::size_t interface_index,
                                                            const std::string& unknown,
                                                            const ::MoReFEM::GeometryNS::dimension_type component) const
    {
        const Data::DofInformation::vector_const_shared_ptr ret;

        auto match_condition =
            [nature, interface_index, &unknown, component](const Data::DofInformation::const_shared_ptr& dof_info_ptr)
        {
            assert(!(!dof_info_ptr));
            const auto& dof_info = *dof_info_ptr;

            return dof_info.GetInterfaceNature() == nature && dof_info.GetInterfaceIndex() == interface_index
                   && dof_info.GetUnknown() == unknown && dof_info.GetUnknownComponent() == component.Get();
        };

        auto end = dof_information_list_.cend();

        auto it = std::find_if(dof_information_list_.cbegin(), end, match_condition);
        assert(it != end);

        decltype(auto) dof_info_ptr = *it;
        assert(dof_info_ptr != nullptr);

        return *dof_info_ptr;
    }


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
