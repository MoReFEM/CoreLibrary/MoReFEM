// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_FILE_DOFINFORMATIONFILE_DOT_HPP_
#define MOREFEM_POSTPROCESSING_FILE_DOFINFORMATIONFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Geometry/StrongType.hpp"

#include "PostProcessing/Data/DofInformation.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::PostProcessingNS { class InterfaceFile; }
namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS
{


    /*!
     * \brief Class which holds the information obtained from dof_information.hhdata output files.
     *
     * There is one such set of files per \a NumberingSubset (each under a 'Rank_' subfolder).
     *
     * Format of one such file is:
     *
     * \verbatim
     Ndof (processor_wise) = 291
     # First column: program-wise index.
     # Second column: processor-wise index.
     # Third column: the interface upon which the dof is located. Look at the interface file in same directory to
     relate it to the initial mesh. # Fourth column: unknown and component involved. # Fifth column: shape function
     label. 0;0;Vertex 0;fluid_velocity 0;P1b 1;1;Vertex 0;fluid_velocity 1;P1b 2;2;Vertex 0;fluid_mass 0;P1
     3;3;Vertex 1;fluid_velocity 0;P1b
     4;4;Vertex 1;fluid_velocity 1;P1b
     ...
     \endverbatim
     */
    class DofInformationFile final
    {
      public:
        //! Alias for unique_ptr;
        using const_unique_ptr = std::unique_ptr<const DofInformationFile>;

        //! Alias for vector of unique_ptr;
        using vector_unique_ptr = std::vector<const_unique_ptr>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] processor Mpi rank of the processor during the computation. It has been used to index the
         * outputs.
         * \param[in] input_file The file which encompasses the dofs to be loaded.
         * \param[in] interface_file The object \a InterfaceFile which stores in memory the content of the
         * interface file.
         *
         */
        explicit DofInformationFile(rank_type processor,
                                    const FilesystemNS::File& input_file,
                                    const InterfaceFile& interface_file);

        //! Destructor.
        ~DofInformationFile() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DofInformationFile(const DofInformationFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        DofInformationFile(DofInformationFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DofInformationFile& operator=(const DofInformationFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DofInformationFile& operator=(DofInformationFile&& rhs) = delete;

        ///@}

      public:
        //! Return the entire list of dof.
        const Data::DofInformation::vector_const_shared_ptr& GetDofList() const noexcept;

        /*!
         * \brief Return the list of dof information that match the interface for the given unknown.
         *
         * \param[in] nature Nature of the interface considered (edge, face and so forth...)
         * \param[in] interface_index The position in the storage vector of the sought dof (between 0 and
         * Ndof on interface - 1).
         * \param[in] unknown Name of the unknown considered (must match the one chosen in the input data file).
         */
        Data::DofInformation::vector_const_shared_ptr
        GetDof(InterfaceNS::Nature nature, std::size_t interface_index, const std::string& unknown) const;

        /*!
         * \brief Return the dof information that match the interface for the given unknown/component.
         *
         * \param[in] nature Nature of the interface considered (edge, face and so forth...)
         * \param[in] interface_index The position in the storage vector of the sought dof (between 0 and
         * Ndof on interface - 1).
         * \param[in] unknown Name of the unknown considered (must match the one chosen in the input data file).
         * \param[in] component Component considered.
         */
        const Data::DofInformation& GetDof(InterfaceNS::Nature nature,
                                           std::size_t interface_index,
                                           const std::string& unknown,
                                           ::MoReFEM::GeometryNS::dimension_type component) const;

        //! Return the number of dofs.
        std::size_t Ndof() const noexcept;

        //! Return the processor.
        rank_type GetProcessor() const noexcept;


      private:
        //! Processor the data are related to.
        const rank_type processor_;

        //! List of dofs with their related information.
        Data::DofInformation::vector_const_shared_ptr dof_information_list_;
    };


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/File/DofInformationFile.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_FILE_DOFINFORMATIONFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
