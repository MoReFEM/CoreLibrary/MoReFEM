// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_FILE_INTERFACEFILE_DOT_HPP_
#define MOREFEM_POSTPROCESSING_FILE_INTERFACEFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <map>
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Coords/StrongType.hpp"

#include "PostProcessing/Data/Interface.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS
{


    /*!
     * \brief Class which holds the information obtained from interfaces.hhdata output file.
     *
     * There is one such file per mesh.
     *
     * This file gives for each interface (vertex, edge, face or volume)  the list of \a Coords that delimit it.
     *
     * \attention Some of the methods of this class were implemented for debug purposes, and are not necessarily
     * very efficient (noticeably accessors to interface). If they are to be used extensively in some post
     * processing information, there are many ways to improve them tremendously.
     */
    class InterfaceFile final
    {

      public:
        //! Alias to unique_ptr of a constant object.
        using const_unique_ptr = std::unique_ptr<const InterfaceFile>;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<InterfaceFile>;

        //! Alias to a vector of unique_ptr.
        using vector_unique_ptr = std::vector<unique_ptr>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] input_file FIle which includes the interfaces considered; should be named "interfaces.hhdata"
        //! and be positioned inside a mesh-related folder.
        explicit InterfaceFile(const FilesystemNS::File& input_file);

        /*!
         * \brief Constructor from several \a InterfaceFile.
         *
         * Typically build a program-wise object from the processor-wise ones (there is one file per rank but in
         * PostProcessing we in fact need the aggre- gated data).
         *
         * \param[in] rank_interface_file_list The list of \a InterfaceFile objects - one for each rank.
         */
        explicit InterfaceFile(InterfaceFile::vector_unique_ptr&& rank_interface_file_list);

        //! Destructor.
        ~InterfaceFile() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InterfaceFile(const InterfaceFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InterfaceFile(InterfaceFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InterfaceFile& operator=(const InterfaceFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InterfaceFile& operator=(InterfaceFile&& rhs) = delete;

        ///@}


        //! Return the number of a given type of interface.
        template<InterfaceNS::Nature NatureT>
        std::size_t Ninterface() const;

        //! Return the adequate interface.
        //! \param[in] nature Nature of the interface used as filter.
        //! \param[in] index Position in the local vector of the interface sought (value in [0, Ninterface[)
        const Data::Interface& GetInterface(InterfaceNS::Nature nature, std::size_t index) const;

        //! Return the vertex interface that matches a given coord_index.
        //! \param[in] coords_index Index of the \a Coord related to the sought interface.
        const Data::Interface& GetVertexInterfaceFromCoordIndex(CoordsNS::processor_wise_position coords_index) const;

        //! Return the face interface that matches a list of coords.
        //! \param[in] coords_list List of \a Coords that delimit the sought face interface.
        const Data::Interface& GetFaceInterfaceFromCoordsList(const Coords::vector_shared_ptr& coords_list) const;


      private:
        //! Return the list of interfaces of \a nature.
        //! \param[in] nature Nature of the interface used as filter.
        const Data::Interface::vector_unique_ptr& GetInterfaceList(InterfaceNS::Nature nature) const;

        //! Non constant accessor to the list of interface (sort per nature which acts as a key of the map).
        std::map<InterfaceNS::Nature, Data::Interface::vector_unique_ptr>& GetNonCstContent() noexcept;

      private:
        /*!
         * \brief Content of the interface file.
         *
         * Key is the nature of the interface.
         * Value is for each node the list of Coords that delimit it.
         */
        std::map<InterfaceNS::Nature, Data::Interface::vector_unique_ptr> content_;
    };


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/File/InterfaceFile.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_FILE_INTERFACEFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
