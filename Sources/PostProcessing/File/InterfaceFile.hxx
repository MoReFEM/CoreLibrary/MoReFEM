// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_FILE_INTERFACEFILE_DOT_HXX_
#define MOREFEM_POSTPROCESSING_FILE_INTERFACEFILE_DOT_HXX_
// IWYU pragma: private, include "PostProcessing/File/InterfaceFile.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <map>
#include <utility>
#include <vector>

#include "PostProcessing/Data/Interface.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS
{


    template<InterfaceNS::Nature NatureT>
    std::size_t InterfaceFile::Ninterface() const
    {
        auto it = content_.find(NatureT);

        if (it == content_.cend())
            return 0UL;

        return it->second.size();
    }


    inline std::map<InterfaceNS::Nature, Data::Interface::vector_unique_ptr>&
    InterfaceFile ::GetNonCstContent() noexcept
    {
        return content_;
    }


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_FILE_INTERFACEFILE_DOT_HXX_
// *** MoReFEM end header guards *** < //
