// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_FILE_TIMEITERATIONFILE_DOT_HPP_
#define MOREFEM_POSTPROCESSING_FILE_TIMEITERATIONFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp" // IWYU pragma: export


namespace MoReFEM::PostProcessingNS
{


    //! Alias to the class defined in Core (as it is also needed for restart during Model runs).
    using TimeIterationFile = ::MoReFEM::InterpretOutputFilesNS::TimeIterationFile;


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_FILE_TIMEITERATIONFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
