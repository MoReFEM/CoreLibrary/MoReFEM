// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <fstream>
#include <memory>
#include <string>

#include "PostProcessing/File/UnknownInformationFile.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "PostProcessing/Data/UnknownInformation.hpp"


namespace MoReFEM::PostProcessingNS
{


    UnknownInformationFile::UnknownInformationFile(const FilesystemNS::File& input_file)
    {
        std::ifstream stream{ input_file.Read() };

        std::string line;

        while (getline(stream, line))
            unknown_list_.emplace_back(std::make_unique<Data::UnknownInformation>(line));
    }


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
