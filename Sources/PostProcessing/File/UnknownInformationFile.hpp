// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_FILE_UNKNOWNINFORMATIONFILE_DOT_HPP_
#define MOREFEM_POSTPROCESSING_FILE_UNKNOWNINFORMATIONFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>

#include "PostProcessing/Data/UnknownInformation.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS
{


    /*!
     * \brief Class which holds the information obtained from one line of unknowns.hhdata output file.
     *
     * This file gives for each unknown the nature ('scalar' or 'vectorial').
     */
    class UnknownInformationFile final
    {
      public:
        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<const UnknownInformationFile>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] input_file Result file of MoReFEM program which gives the unknowns related information;
         * it is named 'unknowns.hhdata'.
         */
        explicit UnknownInformationFile(const FilesystemNS::File& input_file);

        //! Destructor.
        ~UnknownInformationFile() = default;

        //! \copydoc doxygen_hide_copy_constructor
        UnknownInformationFile(const UnknownInformationFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UnknownInformationFile(UnknownInformationFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        UnknownInformationFile& operator=(const UnknownInformationFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UnknownInformationFile& operator=(UnknownInformationFile&& rhs) = delete;


        ///@}

      public:
        //! Get the list of unknowns.
        const Data::UnknownInformation::vector_const_shared_ptr& GetExtendedUnknownList() const;


      private:
        //! List of unknowns.
        Data::UnknownInformation::vector_const_shared_ptr unknown_list_;
    };


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/File/UnknownInformationFile.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_FILE_UNKNOWNINFORMATIONFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
