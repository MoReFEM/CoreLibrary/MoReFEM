// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <typeinfo>

#include "PostProcessing/OutputDeformedMesh/OutputDeformedMesh.hpp"

#include "Geometry/Coords/Internal/Factory.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/Instances/Triangle/Triangle3.hpp"
#include "Geometry/RefGeometricElt/Internal/Topology/LocalData.hpp"

#include "PostProcessing/PostProcessing.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        OutputDeformedMesh::OutputDeformedMesh(const FilesystemNS::Directory& data_directory,
                                               const std::string& unknown_id,
                                               const std::size_t numbering_subset_id,
                                               Internal::MeshNS::MeshManager& mesh_manager,
                                               const Mesh& mesh,
                                               const Domain& domain,
                                               const std::size_t output_mesh_index,
                                               const std::string& output_name,
                                               const std::string& output_format,
                                               const double output_space_unit,
                                               const std::size_t output_offset,
                                               const std::size_t output_frequence)
        {
            FilesystemNS::Directory mesh_directory(
                data_directory, "Mesh_" + std::to_string(mesh.GetUniqueId()), __FILE__, __LINE__);

            FilesystemNS::Directory output_mesh_directory(mesh_directory,
                                                          "OutputDeformedMesh_" + std::to_string(output_mesh_index),
                                                          __FILE__,
                                                          __LINE__,
                                                          FilesystemNS::behaviour::overwrite);

            const auto mesh_dimension = mesh.GetDimension();
            const auto unique_id_output_mesh = mesh_manager.GenerateUniqueId();
            const auto& geometric_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

            std::vector<std::shared_ptr<GeometricElt>> output_mesh_geom_elemts;

            for (const auto& geometric_elt_ptr : geometric_elt_list)
            {
                assert(!(!geometric_elt_ptr));

                if (domain.IsGeometricEltInside(*geometric_elt_ptr))
                {
                    assert(!(!geometric_elt_ptr));
                    output_mesh_geom_elemts.push_back(geometric_elt_ptr);
                }
            }

            std::vector<std::size_t> coords_index_list_new_to_old;
            std::map<std::size_t, std::size_t> coords_index_list_old_to_new;
            std::vector<std::size_t> element_index_list_new_to_old;
            std::map<std::size_t, std::size_t> element_index_list_old_to_new;
            Coords::vector_shared_ptr initial_coords_list;
            Coords::vector_shared_ptr coords_list;

            std::size_t number_of_elements = output_mesh_geom_elemts.size();

            std::size_t Nnodes_new = 0;

            const double mesh_space_unit = mesh.GetSpaceUnit();
            assert(mesh_space_unit > 0.);

            const double space_unit_ratio = output_space_unit / mesh_space_unit;

            for (std::size_t i = 0; i < number_of_elements; ++i)
            {
                const auto& geom_elt = *output_mesh_geom_elemts[i];

                const auto& coords_list_elem = geom_elt.GetCoordsList();

                const std::size_t size = coords_list_elem.size();

                for (std::size_t j = 0; j < size; ++j)
                {
                    const auto& coords = *coords_list_elem[j];

                    std::size_t coord_index_old = coords.GetIndexFromMeshFile();

                    // #1094 find here not really the best thing to do.
                    if (std::find(
                            coords_index_list_new_to_old.begin(), coords_index_list_new_to_old.end(), coord_index_old)
                        == coords_index_list_new_to_old.end())
                    {
                        coords_index_list_new_to_old.push_back(coord_index_old);
                        coords_index_list_old_to_new[coord_index_old] = Nnodes_new + 1;

                        // divide by space unit to put it in mm for visualization.
                        auto coords_ptr = Internal::CoordsNS::Factory::FromComponents(
                            coords[0], coords[1], coords[2], space_unit_ratio);
                        coords_ptr->SetIndexFromMeshFile(Nnodes_new + 1);

                        auto coords_init_ptr = Internal::CoordsNS::Factory::FromComponents(
                            coords[0], coords[1], coords[2], space_unit_ratio);
                        coords_init_ptr->SetIndexFromMeshFile(Nnodes_new);

                        coords_list.emplace_back(std::move(coords_ptr));
                        initial_coords_list.emplace_back(std::move(coords_init_ptr));

                        ++Nnodes_new;
                    }
                }
            }

            std::vector<std::shared_ptr<GeometricElt>> new_output_mesh_geom_elemts;
            std::size_t Nelement_created = 0ul;

            for (std::size_t i = 0; i < number_of_elements; ++i)
            {
                const auto& geom_elt_ptr = output_mesh_geom_elemts[i];
                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                std::size_t element_index_old = geom_elt.GetIndex();

                element_index_list_new_to_old.push_back(element_index_old);

                element_index_list_old_to_new[element_index_old] = Nelement_created;

                std::vector<std::size_t> vertices;

                const auto& mesh_label_ptr = geom_elt_ptr->GetMeshLabelPtr();

                const auto coords_list_size = geom_elt.GetCoordsList().size();

                for (std::size_t j = 0; j < coords_list_size; ++j)
                {
                    const auto coord_index_old = geom_elt.GetCoord(j).GetIndexFromMeshFile();

                    const auto it = coords_index_list_old_to_new.find(coord_index_old);

                    assert(it != coords_index_list_old_to_new.cend());

                    std::size_t coord_index_new = it->second;

                    vertices.push_back(coord_index_new);
                    assert(coord_index_new - 1 < Nnodes_new);
                }

                const auto& geometric_elt_factory =
                    ::MoReFEM::Advanced::GeometricEltFactory::CreateOrGetInstance(__FILE__, __LINE__);

                GeometricElt::shared_ptr new_geom_elmt_ptr(
                    geometric_elt_factory.CreateFromIdentifier(geom_elt.GetIdentifier(), unique_id_output_mesh));

                decltype(auto) new_geom_elmt = *new_geom_elmt_ptr;
                new_geom_elmt.SetCoordsList(coords_list, std::move(vertices));
                new_geom_elmt.SetIndex(Nelement_created);
                new_geom_elmt.SetMeshLabel(mesh_label_ptr);
                new_output_mesh_geom_elemts.push_back(new_geom_elmt_ptr);

                ++Nelement_created;
            }

            MeshLabel::vector_const_shared_ptr new_label_list;

            if (domain.IsConstraintOn<DomainNS::Criterion::label>())
            {
                const auto& mesh_label_list = domain.GetMeshLabelList();

                new_label_list = mesh_label_list;
            } else
            {
                const auto& mesh_label_list = mesh.GetLabelList();

                new_label_list = mesh_label_list;
            }

            Mesh::BuildEdge do_build_edge = Mesh::BuildEdge::no;
            Mesh::BuildFace do_build_face = Mesh::BuildFace::no;
            Mesh::BuildVolume do_build_volume = Mesh::BuildVolume::no;

            // Construction of the mesh.
            mesh_manager.Create(mesh_dimension,
                                output_space_unit,
                                std::move(new_output_mesh_geom_elemts),
                                std::vector<std::shared_ptr<GeometricElt>>(),
                                std::move(coords_list),
                                std::move(new_label_list),
                                do_build_edge,
                                do_build_face,
                                do_build_volume,
                                Mesh::BuildPseudoNormals::no);

            const auto& new_mesh = mesh_manager.GetMesh(unique_id_output_mesh);

            if (output_format == "Medit")
            {
                new_mesh.Write<MeshNS::Format::Medit>(
                    output_mesh_directory.AddFile("initial_" + output_name + ".mesh"));
            } else if (output_format == "Ensight")
            {
                new_mesh.Write<MeshNS::Format::Ensight>(
                    output_mesh_directory.AddFile("initial_" + output_name + ".geo"));
            } else
            {
                throw Exception(
                    "Not done yet for other format than Ensight and Medit for the output mesh.", __FILE__, __LINE__);
            }

            //===============================================================================================
            //===============================================================================================
            //===============================================================================================
            std::vector<std::size_t> numbering_subset_id_list;
            numbering_subset_id_list.push_back(numbering_subset_id);

            std::vector<std::string> unknown_list;
            unknown_list.push_back(unknown_id);


            PostProcessing post_processing(data_directory, numbering_subset_id_list, mesh);

            const auto& time_iteration_list = post_processing.GetTimeIterationList();

            decltype(auto) complete_unknown_list = post_processing.GetExtendedUnknownList();
            Data::UnknownInformation::vector_const_shared_ptr selected_unknown_list;

            // Associate the more complete unknown object from the name given in input.
            {
                for (const auto& unknown_name : unknown_list)
                {
                    const auto it = std::find_if(complete_unknown_list.cbegin(),
                                                 complete_unknown_list.cend(),
                                                 [&unknown_name](const auto& unknown_ptr)
                                                 {
                                                     assert(!(!unknown_ptr));
                                                     return unknown_ptr->GetName() == unknown_name;
                                                 });

                    if (it == complete_unknown_list.cend())
                        throw Exception("Unknown '" + unknown_name
                                            + "' required in the constructor was not one of "
                                              "those used in the model.",
                                        __FILE__,
                                        __LINE__);

                    selected_unknown_list.push_back(*it);
                }
            }

            assert(selected_unknown_list.size() == 1 && "Limited to one unknown from the start.");

            const auto& unknown = *selected_unknown_list[0];

            auto& output_mesh_coords_list = new_mesh.GetProcessorWiseCoordsList();

            //===============================================================================================
            //===============================================================================================
            //===============================================================================================

            // Iterate over time:
            for (const auto& time_iteration_ptr : time_iteration_list)
            {
                assert(!(!time_iteration_ptr));
                const auto& time_iteration = *time_iteration_ptr;

                std::size_t iteration = time_iteration.GetIteration();

                if (iteration >= output_offset && (iteration - output_offset) % output_frequence == 0)
                {
                    if (time_iteration.GetNumberingSubsetId() == numbering_subset_id)
                    {
                        const auto& unknown_name = unknown.GetName();

                        const std::size_t Nprocessor = post_processing.Nprocessor();

                        using DofWithValuesType = std::pair<Data::DofInformation::const_shared_ptr, double>;

                        std::vector<DofWithValuesType> dof_with_values_type;

                        decltype(auto) filename_prototype = time_iteration.GetSolutionFilename();

                        for (std::size_t processor = 0ul; processor < Nprocessor; ++processor)
                        {
                            const auto& dof_list =
                                post_processing.GetDofInformationList(numbering_subset_id, processor);

                            std::string filename = filename_prototype;

                            const auto check = Utilities::String::Replace("*", std::to_string(processor), filename);

                            assert(check == 1);
                            static_cast<void>(check);

                            const auto& solution = LoadVector(filename);

                            for (const auto& dof_ptr : dof_list)
                            {
                                assert(!(!dof_ptr));
                                const auto& dof = *dof_ptr;

                                if (dof.GetUnknown() != unknown_name)
                                    continue;

                                const std::size_t processor_wise_index = dof.GetProcessorWiseIndex();

                                assert(processor_wise_index < solution.size());

                                dof_with_values_type.push_back({ dof_ptr, solution[processor_wise_index] });
                            }

                            const auto dof_with_values_type_size = dof_with_values_type.size();

                            for (std::size_t i = 0; i < dof_with_values_type_size; ++i)
                            {
                                const auto& dof_information = *dof_with_values_type[i].first;

                                const auto& interface = dof_information.GetInterface();

                                assert(interface.GetNature() == ::MoReFEM::InterfaceNS::Nature::vertex
                                       && "Limited to vertex.");

                                // const auto vertex_index = interface.GetIndex();
                                const auto vertex_index_list = interface.GetCoordsProcessorWisePositionList();
                                assert(vertex_index_list.size() == 1);
                                const auto vertex_index = vertex_index_list[0];

                                auto it = std::find(coords_index_list_new_to_old.begin(),
                                                    coords_index_list_new_to_old.end(),
                                                    vertex_index);

                                if (it != coords_index_list_new_to_old.end())
                                {
                                    const auto it_coord_index_new = coords_index_list_old_to_new.find(vertex_index);
                                    assert(it_coord_index_new != coords_index_list_old_to_new.cend());
                                    const auto coord_index_new = it_coord_index_new->second;
                                    assert(coord_index_new <= output_mesh_coords_list.size());
                                    auto& coord = *output_mesh_coords_list[coord_index_new - 1];
                                    assert(coord_index_new <= initial_coords_list.size());
                                    const auto& initial_coord_ptr = initial_coords_list[coord_index_new - 1];
                                    const Coords& initial_coord = *initial_coord_ptr;
                                    assert(i < dof_with_values_type.size());
                                    double disp = dof_with_values_type[i].second;

                                    const auto component_index = dof_information.GetUnknownComponent();
                                    // divide by space unit to put it in mm for visualization.
                                    assert(component_index < initial_coord.Nobjects());

                                    coord.GetNonCstValue(component_index) =
                                        initial_coord[component_index] + disp * (output_space_unit / mesh_space_unit);
                                }
                            }
                        }

                        if (output_format == "Medit")
                        {
                            new_mesh.Write<MeshNS::Format::Medit>(
                                output_mesh_directory.AddFile(output_name + "_" + std::to_string(iteration) + ".mesh"));
                        } else if (output_format == "Ensight")
                        {
                            new_mesh.Write<MeshNS::Format::Ensight>(
                                output_mesh_directory.AddFile(output_name + "_" + std::to_string(iteration) + ".geo"));
                        } else
                        {
                            throw Exception("Not done yet for other format than Ensight and Medit for the output mesh.",
                                            __FILE__,
                                            __LINE__);
                        }
                    }
                }
            }
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
