// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <ios>
#include <memory>
#include <ostream>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>
// IWYU pragma: no_include <iosfwd>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/Unknown/EnumUnknown.hpp"

#include "PostProcessing/Data/DofInformation.hpp"
#include "PostProcessing/Data/Interface.hpp"
#include "PostProcessing/Data/TimeIteration.hpp"
#include "PostProcessing/Data/UnknownInformation.hpp"
#include "PostProcessing/File/InterfaceFile.hpp"
#include "PostProcessing/OutputFormat/Ensight6.hpp"
#include "PostProcessing/PostProcessing.hpp"


namespace MoReFEM::PostProcessingNS::OutputFormat
{


    namespace // anonymous
    {


        void CreateCaseFile(const FilesystemNS::Directory& output_directory,
                            const Data::UnknownInformation::vector_const_shared_ptr& unknown_list,
                            const Data::TimeIteration::vector_const_unique_ptr& time_iteration_list);


        // Class in charge of writing dof values in the Ensight file while respecting expected format (6 values per
        // line at most)
        class WriteDofValue
        {
          public:
            // Constructor.
            explicit WriteDofValue(std::ostream& stream);

            // Write a single new dof value onto the stream.
            void NewValue(double value);

          private:
            std::ostream& stream_;

            std::size_t index_ = 0UL;
        };


        class IndexMatcher
        {
          public:
            IndexMatcher(std::size_t processor_wise_index, std::size_t ensight_position);

            [[nodiscard]] std::size_t GetProcessorWiseIndex() const noexcept;

            [[nodiscard]] std::size_t GetEnsightPosition() const noexcept;

          private:
            std::size_t processor_wise_index_;
            std::size_t ensight_position_;
        };


        // The purpose of this class is to store for a given time iteration the values of the dofs related to a
        // specific unknown in the right order.
        class EnsightSolution
        {
          public:
            EnsightSolution(const Data::TimeIteration& time_iteration, std::vector<double> content);

            [[nodiscard]] const Data::TimeIteration& GetTimeIteration() const noexcept;

            [[nodiscard]] const std::vector<double>& GetContent() const noexcept;

            std::vector<double>& GetNonCstContent() noexcept;

          private:
            const Data::TimeIteration& time_iteration_;

            std::vector<double> content_;
        };


        void CreateUnknownFile(const EnsightSolution& ensight_solution_holder,
                               const FilesystemNS::Directory& output_directory,
                               const Data::UnknownInformation& unknown);


        std::vector<const Data::TimeIteration*> ComputeTimeIterationFileListForNumberingSubset(
            const Data::TimeIteration::vector_const_unique_ptr& original_list,
            NumberingSubsetNS::unique_id numbering_subset_id);


        std::vector<double> LoadVectorForTimeIteration(const Data::TimeIteration& time_iteration, rank_type rank);


        std::vector<const Data::DofInformation*>
        ComputeDofListOnVertex(const PostProcessing& post_processing,
                               NumberingSubsetNS::unique_id numbering_subset_id,
                               rank_type rank,
                               const std::string& unknown_name);

        // From the string names of the unknowns, fetch the post-processing related objects which hold more intel.
        Data::UnknownInformation::vector_const_shared_ptr
        GenerateUnknownInformationList(const PostProcessing& post_processing,
                                       const std::vector<std::string>& unknown_list);


        std::vector<IndexMatcher> GenerateIndexMatcherList(const PostProcessing& post_processing,
                                                           const InterfaceFile& interface_data,
                                                           NumberingSubsetNS::unique_id numbering_subset_id,
                                                           rank_type rank,
                                                           const Data::UnknownInformation& unknown);


        void ComputeEnsightSolutions(const PostProcessing& post_processing,
                                     const InterfaceFile& interface_data,
                                     NumberingSubsetNS::unique_id numbering_subset_id,
                                     const Data::UnknownInformation& unknown,
                                     std::vector<EnsightSolution>& ensight_solution_per_time_step);


    } // namespace


    Ensight6::Ensight6(const FilesystemNS::Directory& data_directory,
                       const std::vector<std::string>& unknown_list,
                       const std::vector<NumberingSubsetNS::unique_id>& numbering_subset_id_list,
                       const Mesh& mesh,
                       [[maybe_unused]] RefinedMesh is_mesh_refined, // to handle in #1503
                       const FilesystemNS::Directory* ensight_directory_ptr)
    {
        assert(unknown_list.size() == numbering_subset_id_list.size());
        const auto Nunknown = unknown_list.size();

        const FilesystemNS::Directory mesh_directory(data_directory,
                                                     "Mesh_" + std::to_string(mesh.GetUniqueId().Get()));

        mesh_directory.ActOnFilesystem();

        // Its sole role is to provide RAII at the end of the constructor.
        FilesystemNS::Directory::unique_ptr ensight_directory_smart_ptr = nullptr;

        if (ensight_directory_ptr == nullptr)
        {
            ensight_directory_smart_ptr = std::make_unique<FilesystemNS::Directory>(
                mesh_directory, "Ensight6", FilesystemNS::behaviour::overwrite);
            ensight_directory_ptr = ensight_directory_smart_ptr.get();
        }

        assert(!(!ensight_directory_ptr));

        const auto& ensight_directory = *ensight_directory_ptr;

        ensight_directory.ActOnFilesystem();

        const PostProcessing post_processing(data_directory, numbering_subset_id_list, mesh);

        mesh.Write<MeshNS::Format::Ensight>(ensight_directory.AddFile("mesh.geo"));

        const auto& time_iteration_list = post_processing.GetTimeIterationList();

        Data::UnknownInformation::vector_const_shared_ptr selected_unknown_list =
            GenerateUnknownInformationList(post_processing, unknown_list);

        assert(selected_unknown_list.size() == Nunknown);
        CreateCaseFile(ensight_directory, selected_unknown_list, time_iteration_list);

        assert(numbering_subset_id_list.size() == selected_unknown_list.size());

        const auto& interface_data = post_processing.GetInterfaceData();

        // Note: loop ordering below might seem weird but is chosen to avoid recomputing stuff for each time step.
        // Work objects could have been used but as seen below we can do without.
        for (auto unknown_index = 0UL; unknown_index < Nunknown; ++unknown_index)
        {
            const auto numbering_subset_id = numbering_subset_id_list[unknown_index];
            const auto& unknown_ptr = selected_unknown_list[unknown_index];
            assert(!(!unknown_ptr));
            const auto& unknown = *unknown_ptr;

            // Ensight expects 3D dimension no matter what - even if the mesh is actually 2D the third component
            // must be specified with 0..
            const auto unknown_dimension = (unknown.GetNature() == UnknownNS::Nature::vectorial ? 3UL : 1UL);

            // Extract the relevant time iteration files
            const auto relevant_time_iteration_file_list =
                ComputeTimeIterationFileListForNumberingSubset(time_iteration_list, numbering_subset_id);

            if (relevant_time_iteration_file_list.empty())
                continue;

            const auto NprogramWiseCoord = mesh.NprocessorWiseCoord() * unknown_dimension; // what we actually want here
            // is the number of program-wise coords that are also vertices (#248) but as EnsightOutput is run
            // sequentially by reading the full mesh the method called here yields the expected value.

            const std::vector<double> ensight_file_content(NprogramWiseCoord, 0.);

            std::vector<EnsightSolution> ensight_solution_per_time_step;
            ensight_solution_per_time_step.reserve(relevant_time_iteration_file_list.size());

            for (const auto& time_iteration_ptr : relevant_time_iteration_file_list)
            {
                assert(!(!time_iteration_ptr));

                const EnsightSolution content(*time_iteration_ptr, ensight_file_content);

                ensight_solution_per_time_step.emplace_back(content);
            }

            // Compute Ensight solutions for each time step and each rank; in output ensight_solution_per_time_step
            // will include the data required to write the solution files in the format expected in Ensight.
            ComputeEnsightSolutions(
                post_processing, interface_data, numbering_subset_id, unknown, ensight_solution_per_time_step);

            // Finally create the unknown file in Ensight format for each time step.
            for (const auto& ensight_solution_holder : ensight_solution_per_time_step)
            {
                CreateUnknownFile(ensight_solution_holder, ensight_directory, unknown);
            }
        }
    }


    namespace // anonymous
    {


        void CreateCaseFile(const FilesystemNS::Directory& output_directory,
                            const Data::UnknownInformation::vector_const_shared_ptr& unknown_list,
                            const Data::TimeIteration::vector_const_unique_ptr& time_iteration_list)
        {
            const FilesystemNS::File file{ output_directory.AddFile("problem.case") };

            std::ofstream stream{ file.NewContent() };

            stream << "FORMAT" << '\n';
            stream << "type: ensight" << '\n';
            stream << "GEOMETRY" << '\n';
            stream << "model: 1 mesh.geo" << '\n';
            stream << "VARIABLE" << '\n';

            for (const auto& unknown_ptr : unknown_list)
            {
                assert(!(!unknown_ptr));

                switch (unknown_ptr->GetNature())
                {
                case Data::UnknownNature::scalar:
                    stream << "scalar";
                    break;
                case Data::UnknownNature::vectorial:
                    stream << "vector";
                    break;
                }

                const auto& name = unknown_ptr->GetName();

                stream << " per node: 1 " << name << ' ' << name << ".*****.scl" << '\n';
            }

            stream << "TIME" << '\n';
            stream << "time set: 1" << '\n';

            std::size_t count = 0UL;

            auto previous_index = static_cast<std::size_t>(-1);

            std::ostringstream time_values_stream;
            std::ostringstream time_index_stream;

            auto Ntime_iteration = 0UL;

            constexpr auto arbitrary_max_Nitem_on_line{ 5UL };

            for (const auto& time_iteration_ptr : time_iteration_list)
            {
                assert(!(!time_iteration_ptr));
                const auto& time_iteration = *time_iteration_ptr;

                const auto current_iteration = time_iteration.GetIteration();

                if (current_iteration == previous_index) // May happen if several unknowns for same time step.
                    continue;

                ++Ntime_iteration;

                time_index_stream << current_iteration;
                time_values_stream << time_iteration.GetTime();

                if (++count % arbitrary_max_Nitem_on_line
                    == 0UL) // lines must not exceed 79 characters; I'm truly on the very safe side here!
                {
                    time_index_stream << '\n';
                    time_values_stream << '\n';
                } else
                {
                    time_index_stream << ' ';
                    time_values_stream << ' ';
                }

                previous_index = current_iteration;
            }

            stream << "number of steps: " << Ntime_iteration << '\n';
            stream << "filename numbers: ";
            stream << time_index_stream.str();

            stream << '\n';

            stream << "time values: ";
            stream << time_values_stream.str();

            stream << '\n';
        }


        WriteDofValue::WriteDofValue(std::ostream& stream) : stream_(stream)
        { }


        void WriteDofValue::NewValue(double value)
        {
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            stream_ << std::setw(12) << std::scientific << std::setprecision(5) << value;

            if (++index_ % 6UL == 0UL)
                stream_ << '\n';
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        }


        void CreateUnknownFile(const EnsightSolution& ensight_solution_holder,
                               const FilesystemNS::Directory& output_directory,
                               const Data::UnknownInformation& unknown)
        {
            std::ostringstream oconv;
            oconv << output_directory;
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            oconv << '/' << unknown.GetName() << '.' << std::setfill('0') << std::setw(5)
                  << ensight_solution_holder.GetTimeIteration().GetIteration() << ".scl";
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

            const std::string ensight_output_file(oconv.str());

            const FilesystemNS::File file(ensight_output_file);
            std::ofstream ensight_output_stream{ file.NewContent() };

            const auto unknown_nature = unknown.GetNature();

            switch (unknown_nature)
            {
            case Data::UnknownNature::scalar:
                ensight_output_stream << "Scalar per node" << '\n';
                break;
            case Data::UnknownNature::vectorial:
                ensight_output_stream << "Vector per node" << '\n';
                break;
            }

            WriteDofValue write_dof_value_helper(ensight_output_stream);

            const auto& content = ensight_solution_holder.GetContent();
            for (const auto& item : content)
                write_dof_value_helper.NewValue(item);
        }


        // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
        IndexMatcher::IndexMatcher(std::size_t processor_wise_index, std::size_t ensight_position)
        : processor_wise_index_(processor_wise_index), ensight_position_(ensight_position)
        { }


        std::size_t IndexMatcher::GetProcessorWiseIndex() const noexcept
        {
            return processor_wise_index_;
        }


        std::size_t IndexMatcher::GetEnsightPosition() const noexcept
        {
            return ensight_position_;
        }


        std::vector<const Data::TimeIteration*> ComputeTimeIterationFileListForNumberingSubset(
            const Data::TimeIteration::vector_const_unique_ptr& original_list,
            NumberingSubsetNS::unique_id numbering_subset_id)
        {
            std::vector<const Data::TimeIteration*> ret;
            ret.reserve(original_list.size());

            for (const auto& time_iteration_ptr : original_list)
            {
                assert(!(!time_iteration_ptr));
                if (time_iteration_ptr->GetNumberingSubsetId() == numbering_subset_id)
                    ret.push_back(time_iteration_ptr.get());
            }

            return ret;
        }


        std::vector<double> LoadVectorForTimeIteration(const Data::TimeIteration& time_iteration, rank_type rank)
        {
            decltype(auto) filename = time_iteration.GetSolutionFilename();

            auto as_string = static_cast<std::string>(filename);
            [[maybe_unused]] const auto check = Utilities::String::Replace("*", std::to_string(rank.Get()), as_string);
            assert(check == 1 && "Exactly one wildcard occurrence is expected in given filename");

            FilesystemNS::File wildcard_replaced{ as_string };

            return LoadVector(std::move(wildcard_replaced));
        }


        std::vector<const Data::DofInformation*>
        ComputeDofListOnVertex(const PostProcessing& post_processing,
                               NumberingSubsetNS::unique_id numbering_subset_id,
                               rank_type rank,
                               const std::string& unknown_name)
        {
            const auto& full_dof_list = post_processing.GetDofInformationList(numbering_subset_id, rank);

            std::vector<const Data::DofInformation*> ret;
            ret.reserve(full_dof_list.size());

            for (const auto& dof_ptr : full_dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;

                // Ensight is only P1 so we drop higher order interfaces.
                if (dof.GetInterfaceNature() != InterfaceNS::Nature::vertex)
                    continue;

                if (!Utilities::String::StartsWith(dof.GetUnknown(), unknown_name))
                    continue;

                ret.push_back(dof_ptr.get());
            }

            return ret;
        }


        Data::UnknownInformation::vector_const_shared_ptr
        GenerateUnknownInformationList(const PostProcessing& post_processing,
                                       const std::vector<std::string>& unknown_list)
        {
            Data::UnknownInformation::vector_const_shared_ptr selected_unknown_list;
            const auto& complete_unknown_list = post_processing.GetExtendedUnknownList();

            for (const auto& unknown_name : unknown_list)
            {
                const auto it = std::ranges::find_if(complete_unknown_list,

                                                     [&unknown_name](const auto& unknown_ptr)
                                                     {
                                                         assert(!(!unknown_ptr));
                                                         return unknown_ptr->GetName() == unknown_name;
                                                     });

                if (it == complete_unknown_list.cend())
                    throw Exception("Unknown '" + unknown_name
                                    + "' required in the constructor was not one of "
                                      "those used in the model.");

                selected_unknown_list.push_back(*it);
            }

            return selected_unknown_list;
        }


        std::vector<IndexMatcher> GenerateIndexMatcherList(const PostProcessing& post_processing,
                                                           const InterfaceFile& interface_data,
                                                           const NumberingSubsetNS::unique_id numbering_subset_id,
                                                           const rank_type rank,
                                                           const Data::UnknownInformation& unknown)
        {
            std::vector<IndexMatcher> ret;

            const auto dof_list = ComputeDofListOnVertex(post_processing, numbering_subset_id, rank, unknown.GetName());

            const auto Nprocessor_wise_dof = dof_list.size();
            ret.reserve(Nprocessor_wise_dof);

            // Ensight expects 3D dimension no matter what - even if the mesh is actually 2D the third component
            // must be specified with 0..
            const auto unknown_dimension = (unknown.GetNature() == UnknownNS::Nature::vectorial ? 3UL : 1UL);


            for (const auto& dof_ptr : dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;

                assert(dof.GetInterfaceNature() == InterfaceNS::Nature::vertex);
                const auto vertex_index = dof_ptr->GetInterfaceIndex();

                const auto& vertex = interface_data.GetInterface(InterfaceNS::Nature::vertex, vertex_index);

                const auto& coords_list = vertex.GetCoordsProcessorWisePositionList();
                assert(coords_list.size() == 1UL);

                const auto coords_index = coords_list.back();

                const IndexMatcher index_matcher(dof.GetProcessorWiseIndex(),
                                                 coords_index.Get() * unknown_dimension
                                                     + static_cast<std::size_t>(dof.GetUnknownComponent()));

                ret.emplace_back(index_matcher);
            }

            assert(Nprocessor_wise_dof == ret.size());

            return ret;
        }


        EnsightSolution::EnsightSolution(const Data::TimeIteration& time_iteration, std::vector<double> content)
        : time_iteration_(time_iteration), content_(std::move(content))
        { }


        const Data::TimeIteration& EnsightSolution::GetTimeIteration() const noexcept
        {
            return time_iteration_;
        }


        const std::vector<double>& EnsightSolution::GetContent() const noexcept
        {
            return content_;
        }


        std::vector<double>& EnsightSolution::GetNonCstContent() noexcept
        {
            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast) - acceptable C++ idiom here!
            return const_cast<std::vector<double>&>(GetContent());
        }


        void ComputeEnsightSolutions(const PostProcessing& post_processing,
                                     const InterfaceFile& interface_data,
                                     const NumberingSubsetNS::unique_id numbering_subset_id,
                                     const Data::UnknownInformation& unknown,
                                     std::vector<EnsightSolution>& ensight_solution_per_time_step)
        {
            const auto Nprocessor = post_processing.Nprocessor();

            for (auto rank = rank_type{ 0UL }; rank < Nprocessor; ++rank)
            {
                const std::vector<IndexMatcher> index_matcher_list =
                    GenerateIndexMatcherList(post_processing, interface_data, numbering_subset_id, rank, unknown);

                const auto Nprocessor_wise_dof = index_matcher_list.size();

                for (auto& ensight_solution_holder : ensight_solution_per_time_step)
                {
                    const auto& time_iteration = ensight_solution_holder.GetTimeIteration();

                    if (time_iteration.GetNumberingSubsetId() != numbering_subset_id)
                        continue;

                    const auto solution = LoadVectorForTimeIteration(time_iteration, rank);

                    auto& ensight_solution = ensight_solution_holder.GetNonCstContent();

                    assert(solution.size()
                           >= Nprocessor_wise_dof); // solution might include dofs not considered here
                                                    // (not on vertex, related to a different unknown, etc...)

                    for (auto i = 0UL; i < Nprocessor_wise_dof; ++i)
                    {
                        const auto& index_matcher = index_matcher_list[i];
                        const auto ensight_sol_index = index_matcher.GetEnsightPosition();
                        assert(ensight_sol_index < ensight_solution.size());
                        ensight_solution[ensight_sol_index] = solution[index_matcher.GetProcessorWiseIndex()];
                    }
                }
            }
        }


    } // namespace


} // namespace MoReFEM::PostProcessingNS::OutputFormat


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
