// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <__tree>

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <optional>
#include <set>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "PostProcessing/PostProcessing.hpp"

#include "Utilities/AsciiOrBinary/BinarySerialization.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Filesystem/Internal/GetRankDirectory.hpp"
#include "Utilities/String/String.hpp"

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"

#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::PostProcessingNS
{


    PostProcessing::PostProcessing(FilesystemNS::Directory data_directory,
                                   std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list,
                                   const Mesh& mesh)
    : data_directory_(std::move(data_directory)), mesh_(mesh),
      Nprocessor_(NumericNS::UninitializedIndex<std::size_t>()), interface_file_(nullptr), unknown_file_(nullptr),
      time_iteration_file_(nullptr), numbering_subset_id_list_(std::move(numbering_subset_id_list))
    {
        data_directory_.SetBehaviour(FilesystemNS::behaviour::read);

        //! Read the unknown file and store its content into the class.
        ReadUnknownFile();

        //! Read the time iteration file and store its content into the class.
        ReadTimeIterationFile();

        //! Read how many processors were involved in the simulation.
        ReadNprocessor();

        // Read the different files.
        ReadInterfaceFiles();

        //! Read the dof files (one per processor) and store its content into the class.
        ReadDofFiles();
    }


    std::vector<double> LoadVector(FilesystemNS::File&& file)
    {
        std::vector<double> ret;

        const auto binary_or_ascii_choice = Utilities::AsciiOrBinary::GetInstance().IsBinaryOutput();

        switch (binary_or_ascii_choice)
        {
        case binary_or_ascii::binary:
        {
            return Advanced::UnserializeVectorFromBinaryFile<double>(file);
        }
        case binary_or_ascii::ascii:
        {
            std::ifstream stream{ file.Read() };

            std::string line;

            while (getline(stream, line))
                ret.push_back(std::stod(line));
            break;
        }
        case binary_or_ascii::from_input_data:
        {
            assert(false && "Should not happen!");
            exit(EXIT_FAILURE);
        }
        }

        return ret;
    }


    void PostProcessing::ReadInterfaceFiles()
    {
        assert(!interface_file_);

        assert(GetDataDirectory().GetBehaviour() == FilesystemNS::behaviour::read);
        const FilesystemNS::Directory root_dir(GetDataDirectory(), "..", FilesystemNS::behaviour::read);

        // Assumes number of processors has been retrieved earlier; constructor is built to ensure this is the
        // case.
        const auto Nprocessor = this->Nprocessor();

        const std::string mesh_id = std::to_string(GetMesh().GetUniqueId().Get());

        InterfaceFile::vector_unique_ptr interface_file_per_rank;

        for (auto rank = rank_type{ 0UL }; rank < Nprocessor; ++rank)
        {
            FilesystemNS::Directory mesh_dir(root_dir, "Rank_" + std::to_string(rank.Get()));

            mesh_dir.AddSubdirectory("Mesh_" + mesh_id);
            mesh_dir.ActOnFilesystem();

            const auto input_file = mesh_dir.AddFile("interfaces.hhdata");

            interface_file_per_rank.emplace_back(std::make_unique<InterfaceFile>(input_file));
        }

        interface_file_ = std::make_unique<InterfaceFile>(std::move(interface_file_per_rank));
    }


    void PostProcessing::ReadUnknownFile()
    {
        auto input_file = GetDataDirectory().AddFile("unknowns.hhdata");

        assert(!unknown_file_);
        unknown_file_ = std::make_unique<UnknownInformationFile>(input_file);
    }


    void PostProcessing::ReadTimeIterationFile()
    {
        decltype(auto) data_dir = GetDataDirectory();

        // First check whether a 'filtered_time_iteration.hhdata' is present (it would have been generated through
        // the PostProcessing/FilterTimeIteration script). If found, use it.
        auto input_file = data_dir.AddFile("filtered_time_iteration.hhdata");

        assert(!time_iteration_file_);

        if (input_file.DoExist())
            std::cout << "[Time iterations] Using the filtered_time_iteration.hhdata file." << '\n';
        else
            input_file = data_dir.AddFile("time_iteration.hhdata");

        time_iteration_file_ = std::make_unique<TimeIterationFile>(input_file);
    }


    void PostProcessing::ReadNprocessor()
    {
        assert(Nprocessor_ == rank_type{ NumericNS::UninitializedIndex<std::size_t>() });

        const FilesystemNS::File file{ GetDataDirectory().AddFile("mpi.hhdata") };
        std::ifstream in{ file.Read() };

        std::string line;
        getline(in, line);

        const std::string sequence("Nprocessor:");

        if (!Utilities::String::StartsWith(line, sequence))
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

        line.erase(0, sequence.size());

        Nprocessor_ = rank_type{ std::stoul(line) };
    }


    void PostProcessing::ReadDofFiles()
    {
        const auto Nproc = Nprocessor();

        assert(Ndof_ == 0UL);

        const auto& interface_file = GetInterfaceData();

        decltype(auto) numbering_subset_id_list = GetNumberingSubsetIdList();

        auto set_numbering_subset_id_list =
            std::set<NumberingSubsetNS::unique_id>(numbering_subset_id_list.cbegin(), numbering_subset_id_list.cend());

        for (const auto& numbering_subset_id : set_numbering_subset_id_list)
        {
            DofInformationFile::vector_unique_ptr dof_information_file_list_for_numbering_subset;
            dof_information_file_list_for_numbering_subset.reserve(Nproc.Get());

            const std::ostringstream oconv;

            for (auto rank = rank_type{ 0UL }; rank < Nproc; ++rank)
            {
                decltype(auto) numbering_subset_directory = GetNumberingSubsetDirectory(rank, numbering_subset_id);

                const auto dof_infos_data_file = numbering_subset_directory.AddFile("dof_information.hhdata");

                auto&& dof_list_ptr = std::make_unique<DofInformationFile>(rank, dof_infos_data_file, interface_file);
                Ndof_ += dof_list_ptr->Ndof();

                dof_information_file_list_for_numbering_subset.emplace_back(std::move(dof_list_ptr));
            }

            assert(dof_information_file_list_for_numbering_subset.size() == Nproc.Get());

            using pair_type = std::pair<NumberingSubsetNS::unique_id, DofInformationFile::vector_unique_ptr>;

            pair_type&& pair = { numbering_subset_id, std::move(dof_information_file_list_for_numbering_subset) };

            dof_file_per_processor_list_per_numbering_subset_.insert(std::move(pair));
        }
    }


    const DofInformationFile& PostProcessing::GetDofFile(const NumberingSubsetNS::unique_id numbering_subset_id,
                                                         const rank_type processor) const
    {
        assert(processor < Nprocessor());

        auto it = dof_file_per_processor_list_per_numbering_subset_.find(numbering_subset_id);
        assert(it != dof_file_per_processor_list_per_numbering_subset_.cend());

        const auto& dof_file_per_processor_list = it->second;

        assert(Nprocessor().Get() == dof_file_per_processor_list.size());
        return *dof_file_per_processor_list[processor.Get()];
    }


    FilesystemNS::Directory
    PostProcessing::GetNumberingSubsetDirectory(const rank_type rank,
                                                const NumberingSubsetNS::unique_id numbering_subset_id) const
    {
        decltype(auto) data_directory = GetDataDirectory();

        const FilesystemNS::Directory rank_data_directory =
            Internal::FilesystemNS::GetRankDirectory(data_directory, rank);

        const FilesystemNS::Directory mesh_dir(rank_data_directory,
                                               "Mesh_" + std::to_string(GetMesh().GetUniqueId().Get()));

        FilesystemNS::Directory ret(mesh_dir, "NumberingSubset_" + std::to_string(numbering_subset_id.Get()));

        return ret;
    }


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
