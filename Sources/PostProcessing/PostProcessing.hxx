// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_POSTPROCESSING_DOT_HXX_
#define MOREFEM_POSTPROCESSING_POSTPROCESSING_DOT_HXX_
// IWYU pragma: private, include "PostProcessing/PostProcessing.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib> // IWYU pragma: keep
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"
#include "Core/NumberingSubset/UniqueId.hpp"

#include "PostProcessing/Data/DofInformation.hpp"
#include "PostProcessing/Data/TimeIteration.hpp"
#include "PostProcessing/Data/UnknownInformation.hpp"
#include "PostProcessing/File/DofInformationFile.hpp"
#include "PostProcessing/File/UnknownInformationFile.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::PostProcessingNS { class InterfaceFile; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS
{


    inline const Data::UnknownInformation::vector_const_shared_ptr& PostProcessing ::GetExtendedUnknownList() const
    {
        return GetUnknownFile().GetExtendedUnknownList();
    }


    inline const Data::TimeIteration::vector_const_unique_ptr& PostProcessing::GetTimeIterationList() const
    {
        return GetTimeIterationFile().GetTimeIterationList();
    }


    inline const Data::DofInformation::vector_const_shared_ptr&
    PostProcessing::GetDofInformationList(const NumberingSubsetNS::unique_id numbering_subset_id,
                                          const rank_type processor) const
    {
        return GetDofFile(numbering_subset_id, processor).GetDofList();
    }


    inline const FilesystemNS::Directory& PostProcessing::GetDataDirectory() const noexcept
    {
        return data_directory_;
    }


    inline const Mesh& PostProcessing::GetMesh() const noexcept
    {
        return mesh_;
    }


    inline auto PostProcessing::Nprocessor() const noexcept -> rank_type
    {
        assert(Nprocessor_ != rank_type{ NumericNS::UninitializedIndex<std::size_t>() });
        return Nprocessor_;
    }


    inline const InterfaceFile& PostProcessing::GetInterfaceData() const noexcept
    {
        assert(!(!interface_file_));
        return *interface_file_;
    }


    inline const UnknownInformationFile& PostProcessing::GetUnknownFile() const noexcept
    {
        assert(!(!unknown_file_));
        return *unknown_file_;
    }


    inline const InterpretOutputFilesNS::TimeIterationFile& PostProcessing::GetTimeIterationFile() const noexcept
    {
        assert(!(!time_iteration_file_));
        return *time_iteration_file_;
    }


    inline std::size_t PostProcessing::Ndof() const noexcept
    {
        return Ndof_;
    }


    inline const std::vector<NumberingSubsetNS::unique_id>& PostProcessing::GetNumberingSubsetIdList() const noexcept
    {
        return numbering_subset_id_list_;
    }


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_POSTPROCESSING_DOT_HXX_
// *** MoReFEM end header guards *** < //
