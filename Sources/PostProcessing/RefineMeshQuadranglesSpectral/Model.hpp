// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_MODEL_DOT_HPP_
#define MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Model/Model.hpp"

#include "PostProcessing/RefineMeshQuadranglesSpectral/RefineMesh.hpp"


namespace MoReFEM::RefineMeshNS
{


    /*!
     * \brief Model used to init stuff required for RefineMesh algorithm.
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    class Model : public ::MoReFEM::Model<Model<MoReFEMDataT>, MoReFEMDataT, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model<MoReFEMDataT>;

        //! Convenient alias.
        using parent = MoReFEM::Model<self, MoReFEMDataT, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_in
         */
        Model(const MoReFEMDataT& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialize the problem.
         *
         * This initialisation includes the resolution of the static problem.
         *
         */
        void SupplInitialize();


        //! Manage time iteration.
        void Forward();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


      private:
        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();


        ///@}
    };


} // namespace MoReFEM::RefineMeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


#include "PostProcessing/RefineMeshQuadranglesSpectral/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
