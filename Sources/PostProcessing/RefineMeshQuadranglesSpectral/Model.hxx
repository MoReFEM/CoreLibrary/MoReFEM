// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_MODEL_DOT_HXX_
#define MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_MODEL_DOT_HXX_
// IWYU pragma: private, include "PostProcessing/RefineMeshQuadranglesSpectral/Model.hpp"
// *** MoReFEM header guards *** < //


#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::RefineMeshNS
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    Model<MoReFEMDataT>::Model(const MoReFEMDataT& morefem_data) : parent(morefem_data)
    { }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void Model<MoReFEMDataT>::SupplInitialize()
    {
        decltype(auto) god_of_dof = GodOfDofManager::GetInstance().GetGodOfDof(MeshNS::unique_id{ 1UL });
        decltype(auto) felt_space = god_of_dof.GetFEltSpace(1);
        decltype(auto) unknown = UnknownManager::GetInstance().GetUnknown(1);
        decltype(auto) extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

        felt_space.ComputeLocal2Global(std::move(extended_unknown_ptr), DoComputeProcessorWiseLocal2Global::yes);

        RefineMeshNS::RefineMeshSpectral(felt_space, god_of_dof.GetMesh(), parent::GetOutputDirectory());
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    inline const std::string& Model<MoReFEMDataT>::ClassName()
    {
        static std::string name("RefineMesh");
        return name;
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    inline bool Model<MoReFEMDataT>::SupplHasFinishedConditions() const
    {
        return true; // No time iteration!
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    inline void Model<MoReFEMDataT>::SupplInitializeStep()
    { }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    inline void Model<MoReFEMDataT>::Forward()
    { }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    inline void Model<MoReFEMDataT>::SupplFinalizeStep()
    { }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    inline void Model<MoReFEMDataT>::SupplFinalize()
    { }


} // namespace MoReFEM::RefineMeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
