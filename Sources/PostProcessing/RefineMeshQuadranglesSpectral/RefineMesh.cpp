// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "PostProcessing/RefineMeshQuadranglesSpectral/RefineMesh.hpp"

#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/Internal/Factory.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"
#include "Geometry/GeometricElt/Instances/Quadrangle/Quadrangle4.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM::RefineMeshNS
{


    namespace // anonymous
    {

        Coords::vector_shared_ptr InitCoordsList(Eigen::Index n_nodes);

    }


    void
    RefineMeshSpectral(const FEltSpace& felt_space, const Mesh& mesh, const FilesystemNS::Directory& output_directory)
    {

        const auto& unknown_list = felt_space.GetExtendedUnknownList();
        assert(unknown_list.size() == 1);
        const auto& unknown_ptr = unknown_list.back();
        assert(!(!unknown_ptr));
        const auto& unknown = *unknown_ptr;

        const auto Ncomponent =
            (unknown.GetNature() == UnknownNS::Nature::scalar ? ::MoReFEM::GeometryNS::dimension_type{ 1 }
                                                              : mesh.GetDimension());

        const auto& local_felt_space_storage =
            felt_space.GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();
        assert(!local_felt_space_storage.empty());


        const auto n_dof = static_cast<Eigen::Index>(felt_space.GetProcessorWiseDofList().size());
        const auto n_nodes = n_dof / Ncomponent.Get();

        // Get instance for new refined mesh.
        auto& mesh_manager = Internal::MeshNS::MeshManager::GetInstance();

        // Generate a unique id for the new mesh.
        const auto unique_id_new_mesh = MoReFEM::Internal::MeshNS::MeshManager::GenerateUniqueId();
        ::MoReFEM::GeomEltNS::index_type Nelement_created{};
        for (const auto& [ref_local_felt_space_ptr, local_felt_space_list_per_geom_elt] : local_felt_space_storage)
        {
            auto coords_list = InitCoordsList(n_nodes);

            assert(!(!ref_local_felt_space_ptr));
            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

            if (ref_local_felt_space.GetRefGeomElt().GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            // Get the position of the nodes on the reference element.
            const auto& ref_felt_list = ref_local_felt_space.GetRefFEltList();
            assert(!ref_felt_list.empty());
            assert(ref_felt_list.size() == 1UL);

            const auto& spectral_ref_felt = ref_felt_list.back()->GetBasicRefFElt();

            const Eigen::Index order = spectral_ref_felt.GetOrder();

            std::vector<std::shared_ptr<GeometricElt>> new_quadrangle_vector;

            const auto n_elem = local_felt_space_list_per_geom_elt.size();
            std::cout << "n_elem = " << n_elem << '\n';

            new_quadrangle_vector.reserve(static_cast<std::size_t>(order * order) * n_elem);

            // Get the local2global array.
            for (const auto& [geom_elt_index, local_felt_space_ptr] : local_felt_space_list_per_geom_elt)
            {
                const auto& original_geom_elt =
                    mesh.GetGeometricEltFromIndex<RoleOnProcessor::processor_wise>(geom_elt_index);
                const auto& mesh_label_ptr = original_geom_elt.GetMeshLabelPtr();

                assert(!(!local_felt_space_ptr));
                const auto& local_felt_space = *local_felt_space_ptr;

                const auto& loc_2_glob = local_felt_space.GetLocal2Global<MpiScale::processor_wise>(unknown_list);

                const auto Nlocal_node = spectral_ref_felt.NlocalNode();

                assert(static_cast<std::size_t>(Nlocal_node.Get() * Ncomponent.Get()) == loc_2_glob.size());

                for (auto local_node_index = LocalNodeNS::index_type{}; local_node_index < Nlocal_node;
                     ++local_node_index)
                {
                    const auto& local_coord = spectral_ref_felt.GetLocalNode(local_node_index).GetLocalCoords();

                    const auto global_index =
                        static_cast<std::size_t>(loc_2_glob[static_cast<std::size_t>(local_node_index.Get())]);
                    assert(global_index % static_cast<std::size_t>(Ncomponent.Get()) == 0UL);

                    const auto coords_index = global_index / static_cast<std::size_t>(Ncomponent.Get());

                    assert(coords_index < coords_list.size());
                    auto& coord_in_mesh = *coords_list[coords_index];

                    Advanced::GeomEltNS::Local2Global(local_felt_space.GetGeometricElt(), local_coord, coord_in_mesh);
                }

                // Create new refined quadrangles.
                for (auto r = Eigen::Index{}; r < order; ++r)
                {
                    for (auto s = Eigen::Index{}; s < order; ++s)
                    {
                        const auto R = static_cast<std::size_t>((order + 1) * s + r);

                        std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> vertices(4);

                        assert(R + static_cast<std::size_t>(order + 2) < loc_2_glob.size());

                        vertices[0] =
                            ::MoReFEM::CoordsNS::index_from_mesh_file(static_cast<std::size_t>(loc_2_glob[R]));
                        vertices[1] =
                            ::MoReFEM::CoordsNS::index_from_mesh_file(static_cast<std::size_t>(loc_2_glob[R + 1]));
                        vertices[2] = ::MoReFEM::CoordsNS::index_from_mesh_file(
                            static_cast<std::size_t>(loc_2_glob[R + static_cast<std::size_t>(order + 2)]));
                        vertices[3] = ::MoReFEM::CoordsNS::index_from_mesh_file(
                            static_cast<std::size_t>(loc_2_glob[R + static_cast<std::size_t>(order + 1)]));


                        for (auto& vertex : vertices)
                        {
                            assert(vertex.Get() % static_cast<std::size_t>(Ncomponent.Get()) == 0UL);
                            vertex.Get() /= static_cast<std::size_t>(Ncomponent.Get());
                        }


                        assert(std::ranges::all_of(vertices,

                                                   [n_nodes](const auto& vertex)
                                                   {
                                                       return vertex.Get() < static_cast<std::size_t>(n_nodes);
                                                   }));

                        // Create a quadrangle and add it in the list.
                        auto new_quadrangle_ptr =
                            std::make_shared<Quadrangle4>(unique_id_new_mesh, coords_list, std::move(vertices));
                        new_quadrangle_ptr->SetIndex(Nelement_created++);
                        new_quadrangle_ptr->SetMeshLabel(mesh_label_ptr);
                        new_quadrangle_vector.push_back(new_quadrangle_ptr);
                    }
                }
            }

            assert(new_quadrangle_vector.size() == static_cast<std::size_t>(order * order) * n_elem);

            const auto mesh_dimension = felt_space.GetDimension();

            const auto space_unit = ::MoReFEM::CoordsNS::space_unit_type{ 1. }; // Mesh::GetSpaceUnit();

            MeshLabel::vector_const_shared_ptr new_label_list(mesh.GetLabelList());

            const Mesh::BuildEdge do_build_edge = Mesh::BuildEdge::no;
            const Mesh::BuildFace do_build_face = Mesh::BuildFace::no;
            const Mesh::BuildVolume do_build_volume = Mesh::BuildVolume::no;

            // Construction of the mesh.
            mesh_manager.Create(mesh_dimension,
                                space_unit,
                                std::move(new_quadrangle_vector),
                                std::vector<std::shared_ptr<GeometricElt>>(),
                                std::move(coords_list),
                                std::move(new_label_list),
                                do_build_edge,
                                do_build_face,
                                do_build_volume,
                                Mesh::BuildPseudoNormals::no);

            const auto& new_mesh = mesh_manager.GetMesh(unique_id_new_mesh);

            new_mesh.Write<MeshNS::Format::Ensight>(output_directory.AddFile("refined_mesh.geo"));
            // new_mesh.Write<MeshNS::Format::Medit>(output_directory + "/refined_mesh.mesh"); // Version 2
            // implicitly defined.
        }
    }


    namespace // anonymous
    {

        Coords::vector_shared_ptr InitCoordsList(Eigen::Index n_nodes)
        {

            Coords::vector_shared_ptr coords_list;
            coords_list.reserve(static_cast<std::size_t>(n_nodes));

            for (auto node = Eigen::Index{}; node < n_nodes; ++node)
            {
                auto coords_ptr = Internal::CoordsNS::Factory::Origin();

                coords_ptr->SetIndexFromMeshFile(
                    ::MoReFEM::CoordsNS::index_from_mesh_file(static_cast<std::size_t>(node)));
                // < \todo Very suspect! (Index is format-dependent, and Medit starts as 1 for instance...)
                coords_list.emplace_back(std::move(coords_ptr));
            }

            assert(coords_list.size() == static_cast<std::size_t>(n_nodes));
            return coords_list;
        }
    } // namespace


} // namespace MoReFEM::RefineMeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
