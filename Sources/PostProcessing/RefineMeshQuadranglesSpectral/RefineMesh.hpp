// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_REFINEMESH_DOT_HPP_
#define MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_REFINEMESH_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::RefineMeshNS
{


    /*!
     * \brief Create a new refined mesh, whose vertices are the coordinates of the DOFs.
     *
     *  Useful for higher order shape functions. Remark: the
     *  numbering is specific for spectral elements and the algorithm is only valid for quadrangles.
     * \param[in] felt_space finite element space instance,
     * \param[in] mesh initial mesh to be refined,
     * \param[in] output_directory output directory in which the refined mesh will be saved.
     */
    void
    RefineMeshSpectral(const FEltSpace& felt_space, const Mesh& mesh, const FilesystemNS::Directory& output_directory);


} // namespace MoReFEM::RefineMeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_REFINEMESHQUADRANGLESSPECTRAL_REFINEMESH_DOT_HPP_
// *** MoReFEM end header guards *** < //
