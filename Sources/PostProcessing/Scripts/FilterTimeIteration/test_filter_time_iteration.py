# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# You may contact its developers by writing at morefem-maint@inria.fr.
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import unittest
import filecmp
import FilterTimeIteration as fti


class TestFilterTimeIteration(unittest.TestCase):
    def test_keep_all(self):
        fti.FilterTimeIteration(
            directory="Input/KeepAll",
            initial_time=0.,
            time_step=0.1,
            final_time=0.5,
            epsilon=1.e-6)

        self.assertTrue(
            filecmp.cmp("Input/KeepAll/Mesh_1/filtered_time_iteration.hhdata",
                        "Input/KeepAll/Mesh_1/time_iteration.hhdata"))

    def test_invalid_time(self):
        with self.assertRaises(ValueError):
            fti.FilterTimeIteration(
                directory="Input/KeepAll",
                initial_time=10.,
                time_step=0.1,
                final_time=0.5,
                epsilon=1.e-6)

    def test_remove_one(self):
        fti.FilterTimeIteration(
            directory="Input/RemoveOne",
            initial_time=0.,
            time_step=0.1,
            final_time=0.5,
            epsilon=1.e-6)

        self.assertTrue(
            filecmp.cmp(
                "Input/RemoveOne/Mesh_1/filtered_time_iteration.hhdata",
                "Input/KeepAll/Mesh_1/time_iteration.hhdata")
        )  # 'KeepAll' not a mistake!

    def test_no_mesh_dir(self):
        with self.assertRaises(ValueError):
            fti.FilterTimeIteration(
                directory="Input/NoMeshDir",
                initial_time=0.,
                time_step=0.1,
                final_time=0.5,
                epsilon=1.e-6)

    def test_invalid_dir(self):
        with self.assertRaises(NotADirectoryError):
            fti.FilterTimeIteration(
                directory="Input/NonExisting",
                initial_time=0.,
                time_step=0.1,
                final_time=0.5,
                epsilon=1.e-6)
                
    def test_no_time_file(self):
        with self.assertRaises(FileNotFoundError):
            fti.FilterTimeIteration(
                directory="Input/NoTimeFile",
                initial_time=0.,
                time_step=0.1,
                final_time=0.5,
                epsilon=1.e-6)                
