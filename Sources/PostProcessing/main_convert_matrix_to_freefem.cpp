// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "PostProcessing/ConvertLinearAlgebra/Freefem/MatrixConversion.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{

    constexpr const auto mesh_id = 1;
    constexpr const auto numbering_subset_id = 15;


    try
    {
        using IPMesh = InputDataNS::Mesh<1>;

        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple<InputDataNS::Mesh<mesh_id>,

                                          InputDataNS::NumberingSubset<numbering_subset_id>,

                                          InputDataNS::Result::OutputDirectory,
                                          InputDataNS::Result::BinaryOutput>;

        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        MoReFEMData<InputData, program_type::post_processing, Utilities::InputDataNS::DoTrackUnusedFields::no>
            morefem_data(argc, argv);

        const auto& input_data = morefem_data.GetInputData();
        const auto& mpi = morefem_data.GetMpi();

        // Path to the matrix to convert, hardcoded at the moment.
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        const std::string matrix_path =
            environment.SubstituteValues("/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Mesh_1/"
                                         "NumberingSubset_15/full_matrix_1.binary.hhdata");
        const std::string freefem_dof_numbering =
            environment.SubstituteValues("/Users/${USER}/Desktop/freefem_numbering.txt");
        const std::string output_dir_path = environment.SubstituteValues("/Volumes/Data/${USER}/Sandbox/Poromechanics");

        try
        {
            namespace ipl = Utilities::InputDataNS;

            decltype(auto) mesh_file = ipl::Extract<IPMesh::Path>::Path(input_data);
            using Result = InputDataNS::Result;

            decltype(auto) result_directory_path = ipl::Extract<Result::OutputDirectory>::Path(input_data);

            FilesystemNS::Directory result_directory(mpi, result_directory_path, FilesystemNS::behaviour::read);

            FilesystemNS::Directory output_directory(mpi, output_dir_path, FilesystemNS::behaviour::read);

            auto& mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<>(input_data, mesh_manager);

            const Mesh& mesh = mesh_manager.GetMesh(mesh_id);

            auto& numbering_subset_manager =
                Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputData<Internal::NumberingSubsetNS::NumberingSubsetManager>(input_data,
                                                                                            numbering_subset_manager);

            const auto& numbering_subset =
                Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance(__FILE__, __LINE__)
                    .GetNumberingSubset(numbering_subset_id);

            PostProcessingNS::FreefemNS::MatrixConversion conversion(
                mpi,
                freefem_dof_numbering,
                matrix_path,
                result_directory,
                { "fluid_mass", "fluid_pressure", "fluid_velocity" },
                //{ "fluid_velocity"},
                //{ "fluid_pressure"},
                output_directory,
                numbering_subset,
                mesh,
                __FILE__,
                __LINE__);
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
    catch (const ExceptionNS::GracefulExit&)
    {
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;

        std::cout << oconv.str();
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
