// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/UniqueId/UniqueId.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/InputData/InputData.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hpp"

#include "PostProcessing/PostProcessing.hpp"
#include "PostProcessing/RefineMeshQuadranglesSpectral/Model.hpp"

// #include "ModelInstances/main_refined_mesh_output/Model.hpp"
// #include "ModelInstances/main_refined_mesh_output/InputData.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{

    using namespace MoReFEM;


    try
    {


        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple<InputDataNS::TimeManager,

                                          InputDataNS::Mesh<1>,

                                          InputDataNS::FEltSpace<1>,

                                          InputDataNS::Domain<1>,

                                          InputDataNS::Unknown<1>,

                                          InputDataNS::NumberingSubset<1>,

                                          InputDataNS::Result>;

        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        using morefem_data_type =
            MoReFEMData<InputData, program_type::post_processing, Utilities::InputDataNS::DoTrackUnusedFields::no>;

        morefem_data_type morefem_data(argc, argv);

        const auto& mpi = morefem_data.GetMpi();

        try
        {
            RefineMeshNS::Model<morefem_data_type> model(morefem_data);
            model.Run();
            std::cout << "End of Post-Processing. Refined mesh created." << std::endl;
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
    catch (const ExceptionNS::GracefulExit&)
    {
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //
