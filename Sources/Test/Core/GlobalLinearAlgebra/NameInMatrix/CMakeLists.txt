add_executable(TestGlobalMatrixName)


target_sources(TestGlobalMatrixName
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestGlobalMatrixName
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})
                      
morefem_organize_IDE(TestGlobalMatrixName Test/Core/GlobalLinearAlgebra Test/Core/GlobalLinearAlgebra/NameInMatrix)

morefem_boost_test_sequential_mode(NAME GlobalMatrixName
                                   EXE TestGlobalMatrixName
                                   TIMEOUT 10)
