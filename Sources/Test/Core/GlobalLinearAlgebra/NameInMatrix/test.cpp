// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <utility>

#define BOOST_TEST_MODULE global_matrix_name

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/NumberingSubsetCreator.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(petsc_wrapper, TestNS::FixtureNS::TestEnvironment)
{
    auto usual_constructed_matrix = Wrappers::Petsc::Matrix("usual_matrix");
    BOOST_CHECK_EQUAL(usual_constructed_matrix.GetName(), std::string("usual_matrix"));

#ifndef NDEBUG
    // Reminder why we call init below...
    BOOST_CHECK_THROW(auto copied_matrix = Wrappers::Petsc::Matrix(usual_constructed_matrix, "copied_matrix"),
                      Advanced::Assertion);
#endif // NDEBUG

    const auto Nrow = row_processor_wise_index_type{ 5UL };
    const auto Ncol = col_processor_wise_index_type{ 3UL };

    {
        // Copied matrix case

        // No need to bother with defining a sparse matrix with what we test here!
        usual_constructed_matrix.InitSequentialDenseMatrix(Nrow, Ncol, GetMpi());

        auto copied_matrix = Wrappers::Petsc::Matrix(usual_constructed_matrix, "copied_matrix");
        BOOST_CHECK_EQUAL(copied_matrix.GetName(), "copied_matrix");
    }

    {
        // Moved matrix case
        auto moved_matrix = Wrappers::Petsc::Matrix(std::move(usual_constructed_matrix));
        BOOST_CHECK_EQUAL(moved_matrix.GetName(), "usual_matrix");

        // NOLINTBEGIN(bugprone-use-after-move,clang-analyzer-cplusplus.Move,hicpp-invalid-access-moved) - that's the
        // whole point of the test!
        BOOST_CHECK_EQUAL(usual_constructed_matrix.GetName(), "");
        // NOLINTEND(bugprone-use-after-move,clang-analyzer-cplusplus.Move,hicpp-invalid-access-moved)
    }
}


BOOST_FIXTURE_TEST_CASE(global_matrix, TestNS::FixtureNS::TestEnvironment)
{
    const auto Nrow = row_processor_wise_index_type{ 5UL };
    const auto Ncol = col_processor_wise_index_type{ 3UL };

    decltype(auto) row_numbering_subset =
        TestNS::NumberingSubsetCreator::Create(::MoReFEM::NumberingSubsetNS::unique_id{ 0UL });
    decltype(auto) col_numbering_subset =
        TestNS::NumberingSubsetCreator::Create(::MoReFEM::NumberingSubsetNS::unique_id{ 1UL });

    auto usual_constructed_matrix = GlobalMatrix(row_numbering_subset, col_numbering_subset, "usual_matrix");
    BOOST_CHECK_EQUAL(usual_constructed_matrix.GetName(), std::string("usual_matrix"));

#ifndef NDEBUG
    // Reminder why we call init below...
    BOOST_CHECK_THROW(auto copied_matrix = GlobalMatrix(usual_constructed_matrix, "copied_matrix"),
                      Advanced::Assertion);
#endif // NDEBUG

    {
        // Copied matrix case
        usual_constructed_matrix.InitSequentialDenseMatrix(Nrow, Ncol, GetMpi());
        auto copied_matrix = GlobalMatrix(usual_constructed_matrix, "copied_matrix");
        BOOST_CHECK_EQUAL(copied_matrix.GetName(), "copied_matrix");
    }

    // No moved case as the move copy constructor has been deleted, contrary to what was done for GlobalVector.
}


BOOST_AUTO_TEST_CASE(global_matrix_default_value)
{
    decltype(auto) manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();

    decltype(auto) row_numbering_subset = manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 0UL });
    decltype(auto) col_numbering_subset = manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 1UL });

    auto usual_constructed_matrix = GlobalMatrix(row_numbering_subset, col_numbering_subset);
    BOOST_CHECK_EQUAL(usual_constructed_matrix.GetName(), "Matrix_ns_0_1");
}


BOOST_FIXTURE_TEST_CASE(global_matrix_default_value_in_copy, TestNS::FixtureNS::TestEnvironment)
{
    const auto Nrow = row_processor_wise_index_type{ 5UL };
    const auto Ncol = col_processor_wise_index_type{ 3UL };

    decltype(auto) manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
    decltype(auto) row_numbering_subset = manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 0UL });
    decltype(auto) col_numbering_subset = manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 1UL });

    auto usual_constructed_matrix = GlobalMatrix(row_numbering_subset, col_numbering_subset, "usual_matrix");
    BOOST_CHECK_EQUAL(usual_constructed_matrix.GetName(), std::string("usual_matrix"));
    usual_constructed_matrix.InitSequentialDenseMatrix(Nrow, Ncol, GetMpi());

    auto copied_matrix = GlobalMatrix(usual_constructed_matrix); // no name provided here
    BOOST_CHECK_EQUAL(copied_matrix.GetName(),
                      "Matrix_ns_0_1"); // default value rather than the one from original matrix
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
