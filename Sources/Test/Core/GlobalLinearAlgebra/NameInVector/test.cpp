// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <utility>

#define BOOST_TEST_MODULE global_vector_name

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/NumberingSubsetCreator.hpp"


using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(petsc_wrapper, TestNS::FixtureNS::TestEnvironment)
{
    auto usual_constructed_vector = Wrappers::Petsc::Vector("usual_vector");
    BOOST_CHECK_EQUAL(usual_constructed_vector.GetName(), std::string("usual_vector"));

#ifndef NDEBUG
    // Reminder why we call init below...
    BOOST_CHECK_THROW(auto copied_vector = Wrappers::Petsc::Vector(usual_constructed_vector, "copied_vector"),
                      Advanced::Assertion);
#endif // NDEBUG


    {
        // Copied vector case
        usual_constructed_vector.InitSequentialVector(GetMpi(), vector_processor_wise_index_type{ 5UL });

        auto copied_vector = Wrappers::Petsc::Vector(usual_constructed_vector, "copied_vector");
        BOOST_CHECK_EQUAL(copied_vector.GetName(), "copied_vector");
    }

    // NOLINTBEGIN(hicpp-invalid-access-moved,bugprone-use-after-move,clang-analyzer-cplusplus.Move,hicpp-invalid-access-moved)
    // - we are checking the bahaviour if user does the use after move bug!
    {
        // Moved vector case
        auto moved_vector = Wrappers::Petsc::Vector(std::move(usual_constructed_vector));
        BOOST_CHECK_EQUAL(moved_vector.GetName(), "usual_vector");

        BOOST_CHECK_EQUAL(usual_constructed_vector.GetName(), "");
    }
    // NOLINTEND(hicpp-invalid-access-moved,bugprone-use-after-move,clang-analyzer-cplusplus.Move,hicpp-invalid-access-moved)
}


BOOST_FIXTURE_TEST_CASE(global_vector, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) numbering_subset =
        TestNS::NumberingSubsetCreator::Create(::MoReFEM::NumberingSubsetNS::unique_id{ 0UL });

    auto usual_constructed_vector = GlobalVector(numbering_subset, "usual_vector");
    BOOST_CHECK_EQUAL(usual_constructed_vector.GetName(), std::string("usual_vector"));

#ifndef NDEBUG
    // Reminder why we call init below...
    BOOST_CHECK_THROW(auto copied_vector = GlobalVector(usual_constructed_vector, "copied_vector"),
                      Advanced::Assertion);
#endif // NDEBUG

    {
        // Copied vector case
        usual_constructed_vector.InitSequentialVector(GetMpi(), vector_processor_wise_index_type{ 5UL });
        auto copied_vector = GlobalVector(usual_constructed_vector, "copied_vector");
        BOOST_CHECK_EQUAL(copied_vector.GetName(), "copied_vector");
    }

    // NOLINTBEGIN(hicpp-invalid-access-moved,bugprone-use-after-move,clang-analyzer-cplusplus.Move,hicpp-invalid-access-moved)
    // - we are checking the bahaviour if user does the use after move bug!
    {
        // Moved vector case
        auto moved_vector = GlobalVector(std::move(usual_constructed_vector));
        BOOST_CHECK_EQUAL(moved_vector.GetName(), "usual_vector");
        BOOST_CHECK_EQUAL(usual_constructed_vector.GetName(), "");
    }
    // NOLINTEND(hicpp-invalid-access-moved,bugprone-use-after-move,clang-analyzer-cplusplus.Move,hicpp-invalid-access-moved)
}


BOOST_AUTO_TEST_CASE(global_vector_default_value)
{
    decltype(auto) manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
    decltype(auto) numbering_subset = manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 0UL });

    auto usual_constructed_vector = GlobalVector(numbering_subset);
    BOOST_CHECK_EQUAL(usual_constructed_vector.GetName(), "Vector_ns_0");
}


BOOST_FIXTURE_TEST_CASE(global_vector_default_value_in_copy, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
    decltype(auto) numbering_subset = manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 0UL });

    auto usual_constructed_vector = GlobalVector(numbering_subset, "usual_vector");
    BOOST_CHECK_EQUAL(usual_constructed_vector.GetName(), std::string("usual_vector"));
    usual_constructed_vector.InitSequentialVector(GetMpi(), vector_processor_wise_index_type{ 5UL });

    auto copied_vector = GlobalVector(usual_constructed_vector); // no name provided here
    BOOST_CHECK_EQUAL(copied_vector.GetName(), "Vector_ns_0"); // default value rather than the one from original vector
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
