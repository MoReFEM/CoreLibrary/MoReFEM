// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <memory>
#include <string>
#include <tuple>

#include "Core/InputData/Instances/Result.hpp"
#define BOOST_TEST_MODULE cli_options
#include "Utilities/InputData/InputData.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct NewArguments
    {

        void Add(TCLAP::CmdLine& command);

        [[nodiscard]] const std::string& GetAdditionalStringValue() const;

        [[nodiscard]] int GetAdditionalIntValue() const;

        [[nodiscard]] bool IsFlagPresent() const;

      private:
        std::unique_ptr<TCLAP::ValueArg<std::string>> additional_string_argument_ = nullptr;

        std::unique_ptr<TCLAP::ValueArg<int>> additional_int_argument_ = nullptr;

        std::unique_ptr<TCLAP::SwitchArg> additional_bool_argument_ = nullptr;
    };


    struct Fixture : public TestNS::FixtureNS::TestEnvironment
    { };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// NOLINTBEGIN(clang-analyzer-optin.cplusplus.VirtualCall,cppcoreguidelines-avoid-c-arrays,modernize-avoid-c-arrays,
// cppcoreguidelines-avoid-c-array,modernize-avoid-c-arrays,cppcoreguidelines-pro-type-const-cast,cppcoreguidelines-pro-bounds-array-to-pointer-decay,hicpp-no-array-decay)
BOOST_FIXTURE_TEST_CASE(cli_options, Fixture)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();

    /* BOOST_REQUIRE_NO_THROW */ (environment.GetEnvironmentVariable("MOREFEM_ROOT"));

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/CommandLineOptions/"
                                     "demo_morefem_data.lua");

    using input_data_tuple = std::tuple<InputDataNS::Result>;

    using input_data_type = InputData<input_data_tuple>;
    // clang-format off
    using morefem_data_type = MoReFEMData
    <
        TestNS::EmptyModelSettings,
        input_data_type,
        TimeManagerNS::Instance::None,
        program_type::test,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields::no,
        NewArguments
    >;
    // clang-format on

    const std::string whatever_dir = "${MOREFEM_TEST_OUTPUT_DIR}/Seq/Core/MoReFEMData/CLIOptions";

    char program_name[] = "ProgramName";
    char option1[] = "-i";
    char* input_file_char = const_cast<char*>(input_file.c_str());
    char option2[] = "-e";
    const std::string arg2_as_string = std::string("MOREFEM_WHATEVER_DIR=") + whatever_dir;
    char* arg2 = const_cast<char*>(arg2_as_string.c_str());
    char option3[] = "-e";
    char arg3[] = "MOREFEM_WHATEVER=42";
    char option4[] = "--suppl_string";
    char arg4[] = "Foo";
    char option5[] = "--suppl_int";
    char arg5[] = "42";
    char arg6[] = "--flag_present";

    char* argv[] = { program_name, option1, input_file_char, option2, arg2, option3, arg3,
                     option4,      arg4,    option5,         arg5,    arg6, nullptr };

    const morefem_data_type::const_unique_ptr ptr = std::make_unique<morefem_data_type>(12, argv);

    BOOST_CHECK_EQUAL(environment.GetEnvironmentVariable("MOREFEM_WHATEVER"), "42");

    const auto& morefem_data = *ptr;

    BOOST_CHECK_EQUAL(
        ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Result::OutputDirectory>(morefem_data).string(),
        environment.SubstituteValues(whatever_dir) + "/Foo");

    BOOST_CHECK(morefem_data.GetAdditionalStringValue() == std::string("Foo"));
    BOOST_CHECK(morefem_data.GetAdditionalIntValue() == 42);
    BOOST_CHECK(morefem_data.IsFlagPresent());
}
// NOLINTEND(clang-analyzer-optin.cplusplus.VirtualCall,cppcoreguidelines-avoid-c-arrays,modernize-avoid-c-arrays,
// cppcoreguidelines-avoid-c-array,modernize-avoid-c-arrays,cppcoreguidelines-pro-type-const-cast,cppcoreguidelines-pro-bounds-array-to-pointer-decay,hicpp-no-array-decay)

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    void NewArguments::Add(TCLAP::CmdLine& command)
    {
        additional_string_argument_ =
            std::make_unique<TCLAP::ValueArg<std::string>>("",
                                                           "suppl_string",
                                                           "additional command only for this model",
                                                           true, // is mandatory or not
                                                           "",
                                                           "string",
                                                           command);

        additional_int_argument_ = std::make_unique<TCLAP::ValueArg<int>>("",
                                                                          "suppl_int",
                                                                          "additional command only for this model",
                                                                          false, // is mandatory or not
                                                                          0,
                                                                          "int",
                                                                          command);

        additional_bool_argument_ = std::make_unique<TCLAP::SwitchArg>("", "flag_present", "", command, false);
    }


    const std::string& NewArguments::GetAdditionalStringValue() const
    {
        assert(!(!additional_string_argument_));
        return additional_string_argument_->getValue();
    }

    int NewArguments::GetAdditionalIntValue() const
    {
        assert(!(!additional_int_argument_));
        return additional_int_argument_->getValue();
    }

    bool NewArguments::IsFlagPresent() const
    {
        assert(!(!additional_bool_argument_));
        return additional_bool_argument_->getValue();
    }


} // namespace
