// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <variant>

#include "Test/Tools/Fixture/Enum.hpp"
#define BOOST_TEST_MODULE core_initial_condition
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


PRAGMA_DIAGNOSTIC(push)
#include <cstdlib>

#include "Utilities/InputData/InputData.hpp"
#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Core/MoReFEMData/InitialCondition/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace // anonymous
{

    struct MockModel
    {

        using morefem_data_type = MoReFEM::TestNS::InputDataSolidNS::morefem_data_type;
    };


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        MockModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on


} // namespace


BOOST_FIXTURE_TEST_CASE(initial_condition_properly_read, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    decltype(auto) initial_condition1_value_list =
        ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::InitialCondition<1>::Value>(morefem_data);

    BOOST_REQUIRE_EQUAL(initial_condition1_value_list.size(), 3UL);

    const auto constant_value = initial_condition1_value_list[0];

    BOOST_REQUIRE(std::get_if<0>(&constant_value) != nullptr);
    BOOST_CHECK(NumericNS::AreEqual(std::get<0>(constant_value), 4.));

    const auto lua_value = initial_condition1_value_list[1];

    {
        // Index is the position of the Lua function file in the std::variant used in InitialCondition.
        // I really don't like this code. See #1728 for more details.
        constexpr auto index = 2UL;
        BOOST_REQUIRE(std::get_if<index>(&lua_value) != nullptr);
        BOOST_CHECK(NumericNS::AreEqual(std::get<index>(lua_value)(0., 0., 0.), 0.));
        BOOST_CHECK(NumericNS::AreEqual(std::get<index>(lua_value)(1., -1.5, 10.), -44.75));
    }

    const auto piecewise_value = initial_condition1_value_list[2];

    {
        // Index is the position of the Lua function file in the std::variant used in InitialCondition.
        // I really don't like this code. See #1728 for more details.
        constexpr auto index = 1UL;
        BOOST_REQUIRE(std::get_if<index>(&piecewise_value) != nullptr);
        BOOST_REQUIRE(std::get<index>(piecewise_value).size() == 2UL);

        const auto it1 = std::get<index>(piecewise_value).find(DomainNS::unique_id{ 1UL });

        BOOST_CHECK(it1 != std::get<index>(piecewise_value).cend());
        BOOST_CHECK(NumericNS::AreEqual(it1->second, 10.));

        const auto it2 = std::get<index>(piecewise_value).find(DomainNS::unique_id{ 2UL });

        BOOST_CHECK(it2 != std::get<index>(piecewise_value).cend());
        BOOST_CHECK(NumericNS::AreEqual(it2->second, -15.));

        const auto it3 = std::get<index>(piecewise_value).find(DomainNS::unique_id{ 0UL });

        BOOST_CHECK(it3 == std::get<index>(piecewise_value).cend());
    }
}


BOOST_FIXTURE_TEST_CASE(initial_condition_ignored, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    decltype(auto) initial_condition4_value_list =
        ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::InitialCondition<4>::Value>(morefem_data);

    BOOST_REQUIRE_EQUAL(initial_condition4_value_list.size(), 3UL);
    BOOST_CHECK(std::get<3>(initial_condition4_value_list[0]) == nullptr);
    BOOST_CHECK(std::get<3>(initial_condition4_value_list[1]) == nullptr);
    BOOST_CHECK(std::get<3>(initial_condition4_value_list[2]) == nullptr);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
