// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#include "Utilities/Numeric/Numeric.hpp"

#include "Test/Tools/Fixture/Enum.hpp"

#define BOOST_TEST_MODULE core_input_data
#include "Utilities/InputData/InputData.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/Extract.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Core/MoReFEMData/InputData/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::InputDataSolidNS::ModelSettings,
                                              TestNS::InputDataSolidNS::input_data_type,
                                              TimeManagerNS::Instance::None,
                                              program_type::test>;
    };


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        MockModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on


} // namespace

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(check_value, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    const auto coeff = ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::ReactionNS::ReactionCoefficient>(morefem_data);

    BOOST_CHECK(NumericNS::AreEqual(coeff, 4.));
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
