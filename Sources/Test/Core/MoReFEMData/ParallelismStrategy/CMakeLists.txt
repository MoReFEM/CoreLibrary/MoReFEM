add_executable(TestParallelismStrategy)

target_sources(TestParallelismStrategy
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/demo_no_parallel_field.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_parallel.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_parallel_no_write.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_precompute.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_run_from_preprocessed.lua
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp  
)

target_link_libraries(TestParallelismStrategy
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_organize_IDE(TestParallelismStrategy Test/Core/MoReFEMData Test/Core/MoReFEMData/ParallelismStrategy)

morefem_boost_test_sequential_mode(NAME ParallelismStrategy
                                   EXE TestParallelismStrategy
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo.lua
                                   TIMEOUT 20)
