// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_CORE_MOREFEMDATA_PARALLELISMSTRATEGY_INPUTDATA_DOT_HPP_
#define MOREFEM_TEST_CORE_MOREFEMDATA_PARALLELISMSTRATEGY_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
// IWYU pragma: end_exports

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"


namespace MoReFEM::TestNS::WithParallelismNS
{

    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Parallelism
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type =
        MoReFEMData<TestNS::EmptyModelSettings, input_data_type, time_manager_type, program_type::test>;


} // namespace MoReFEM::TestNS::WithParallelismNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_CORE_MOREFEMDATA_PARALLELISMSTRATEGY_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
