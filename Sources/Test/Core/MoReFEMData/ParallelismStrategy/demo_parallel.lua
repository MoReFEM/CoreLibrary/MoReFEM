-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

Parallelism = {


    -- What should be done for a parallel run. There are 4 possibilities:
    --  'Precompute': Precompute the data for a later parallel run and stop once it's done.
    --  'ParallelNoWrite': Run the code in parallel without using any pre-processed data and do not write down
    -- the processed data.
    --  'Parallel': Run the code in parallel without using any pre-processed data and write down the processed
    -- data.
    --  'RunFromPreprocessed': Run the code in parallel using pre-processed data.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Precompute', 'ParallelNoWrite', 'Parallel', 'RunFromPreprocessed'})
    policy = 'Parallel',

    -- Directory in which parallelism data will be written or read (depending on the policy).
    -- Expected format: "VALUE"
    directory = '${MOREFEM_TEST_OUTPUT_DIR}/Core/MoReFEMData/Parallelism',
    
    
} -- Parallelism
