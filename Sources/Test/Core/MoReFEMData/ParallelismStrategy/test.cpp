// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <tuple>
#include <utility>

#include "Test/Tools/EmptyModelSettings.hpp"
#define BOOST_TEST_MODULE core_parallelism_strategy
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Core/MoReFEMData/ParallelismStrategy/InputData.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{

    using fixture = MoReFEM::TestNS::FixtureNS::TestEnvironment;

    using morefem_data_type = MoReFEM::TestNS::WithParallelismNS::morefem_data_type;

    using input_data_type = morefem_data_type::input_data_type;


} // namespace


BOOST_FIXTURE_TEST_CASE(parallel, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_parallel.lua" } };

    const MoReFEMData<TestNS::EmptyModelSettings, input_data_type, TimeManagerNS::Instance::None, program_type::test>
        morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data) == Advanced::parallelism_strategy::parallel);
}

BOOST_FIXTURE_TEST_CASE(parallel_no_write, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_parallel_no_write.lua" } };

    const MoReFEMData<TestNS::EmptyModelSettings, input_data_type, TimeManagerNS::Instance::None, program_type::test>
        morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data)
                == Advanced::parallelism_strategy::parallel_no_write);
}

BOOST_FIXTURE_TEST_CASE(run_from_preprocessed, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_run_from_preprocessed.lua" } };

    const MoReFEMData<TestNS::EmptyModelSettings, input_data_type, TimeManagerNS::Instance::None, program_type::test>
        morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data)
                == Advanced::parallelism_strategy::run_from_preprocessed);
}


BOOST_FIXTURE_TEST_CASE(precompute, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_precompute.lua" } };

    const MoReFEMData<TestNS::EmptyModelSettings, input_data_type, TimeManagerNS::Instance::None, program_type::test>
        morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data) == Advanced::parallelism_strategy::precompute);
}


BOOST_FIXTURE_TEST_CASE(no_parallel_block, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_no_parallel_field.lua" } };

    using empty_data_tuple = std::tuple<>;

    using empty_input_data_type = InputData<empty_data_tuple>;

    const MoReFEMData<TestNS::EmptyModelSettings,
                      empty_input_data_type,
                      TimeManagerNS::Instance::None,
                      program_type::test>
        morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data) == Advanced::parallelism_strategy::none);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
