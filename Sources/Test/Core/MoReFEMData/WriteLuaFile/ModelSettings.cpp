// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Core/MoReFEMData/WriteLuaFile/InputData.hpp"


namespace MoReFEM::TestNS::WriteLuaFileNS
{


    void ModelSettings::Init()
    {
        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        SetDescription<InputDataNS::Mesh<5>>("Mesh indexed 5");
        SetDescription<InputDataNS::ScalarTransientSource<1>>("Scalar transient source indexed 1");
        SetDescription<InputDataNS::ScalarTransientSource<72>>("Scalar transient source indexed 72");
        SetDescription<InputDataNS::VectorialTransientSource<4>>("Vectorial transient source indexed 4");
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace MoReFEM::TestNS::WriteLuaFileNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
