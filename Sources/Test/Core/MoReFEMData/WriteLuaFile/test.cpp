// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <filesystem>
#include <utility>


#define BOOST_TEST_MODULE core_write_lua_file
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Internal/Write/Write.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Core/MoReFEMData/WriteLuaFile/InputData.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    const FilesystemNS::Directory& GetOutputDirectory();


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(create_explicitly_default_lua_file, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) output_directory = GetOutputDirectory();

    auto explicitly_generated_lua = output_directory.AddFile("explicitly_default_generated.lua");

    using input_data_type = MoReFEM::TestNS::WriteLuaFileNS::input_data_type;

    Internal::InputDataNS::CreateDefaultInputFile<input_data_type>(explicitly_generated_lua,
                                                                   TestNS::EmptyModelSettings());
}


BOOST_FIXTURE_TEST_CASE(write_full_fledged_lua_file, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/WriteLuaFile/demo.lua" };
    FilesystemNS::File full_fledged_lua_file{ std::move(path) };

    // Write in output directory the content of the Lua file. We can't however do a direct comparison: Lua file may
    // not be paginated (indentation, spaces, new lines, ...) and so forth the same way.
    decltype(auto) output_directory = GetOutputDirectory();
    auto lua_file_with_content = output_directory.AddFile("explicitly_with_content.lua");

    {
        const TestNS::WriteLuaFileNS::morefem_data_type morefem_data{ std::move(full_fledged_lua_file) };
        Internal::InputDataNS::Write(
            morefem_data.GetModelSettings(), morefem_data.GetInputData(), lua_file_with_content);
    }

    // However if now we load the newly generated file and rewrite it, we should obtain the exact same file.
    auto regenerated_lua_file_with_content = output_directory.AddFile("regenerated_from_explicitly_with_content.lua");

    {
        const TestNS::WriteLuaFileNS::morefem_data_type morefem_data_from_generated{ std::move(lua_file_with_content) };
        Internal::InputDataNS::Write(morefem_data_from_generated.GetModelSettings(),
                                     morefem_data_from_generated.GetInputData(),
                                     regenerated_lua_file_with_content);
    }

    lua_file_with_content = output_directory.AddFile("explicitly_with_content.lua");
    BOOST_CHECK(FilesystemNS::AreEquals(lua_file_with_content, regenerated_lua_file_with_content));
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    FilesystemNS::Directory ComputeDirectory()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();

        // Deliberately not the same as in the Lua file (to avoid automatic deletion of the directory during
        // MoReFEMData initialization).
        const std::filesystem::path dir =
            environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/Core/MoReFEMData/WriteLuaFile_bis");

        std::filesystem::remove_all(dir);
        std::filesystem::create_directories(dir);

        FilesystemNS::Directory ret{ dir, FilesystemNS::behaviour::ignore };

        return ret;
    }


    const FilesystemNS::Directory& GetOutputDirectory()
    {
        static auto ret = ComputeDirectory();

        return ret;
    }


} // namespace
