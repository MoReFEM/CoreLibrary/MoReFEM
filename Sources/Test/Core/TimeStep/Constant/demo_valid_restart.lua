-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

-- transient
transient = {	

	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.057,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,
    
	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 2.,
}


Restart = {


    -- Time iteration from which we should restart the model. Put 0 if no restart intended. This value makes
    -- only sense in 'RunFromPreprocessed' mode.
    -- Expected format: VALUE
    time_iteration = 2,


    -- Result directory of the previous run. Leave empty if no restart intended.
    -- Expected format: "VALUE"
    data_directory = '${MOREFEM_ROOT}/Sources/Test/Core/TimeStep/Constant/RestartData/Valid',
    
    -- Whether restart data are written or not. Such data are written only on root processor and only in binary
    -- format.
    -- Expected format: 'true' or 'false' (without the quote)
    do_write_restart_data = true

} -- Restart




-- Result
Result = {
	-- Directory in which all the results will be written. 
	-- Expected format: "VALUE"
	output_directory = '${MOREFEM_TEST_OUTPUT_DIR}/Core/TimeStep/RestartForConstant/Valid',
    
    -- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
    -- Expected format: VALUE
    display_value = 1,
    
    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: VALUE
    binary_output = false
}

