// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <string>
#include <tuple>
#include <utility>

#include "Utilities/Filesystem/Behaviour.hpp"

#include "Core/InputData/Instances/Result.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"

#include "Test/Tools/Fixture/Enum.hpp"

#define BOOST_TEST_MODULE constant_time_step
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Core/TimeStep/Constant/InputData.hpp"
#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/Model.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::EmptyModelSettings,
                                              TestNS::ConstantTimeStepNS::input_data_type,

                                              TestNS::ConstantTimeStepNS::time_manager_type,
                                              program_type::test>;
    };


    // clang-format off
    //! \copydoc doxygen_hide_input_data_tuple
    using restart_input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        InputDataNS::Restart,
        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using restart_input_data_type = InputData<restart_input_data_tuple>;

    //! \copydoc doxygen_hide_morefem_data_type
    using restart_morefem_data_type = MoReFEMData<TestNS::EmptyModelSettings,
                                                  restart_input_data_type,
                                                  TestNS::ConstantTimeStepNS::time_manager_type,
                                                  program_type::test>;


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = MoReFEM::TestNS::ConstantTimeStepNS::time_manager_type;


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        MockModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on

    constexpr auto epsilon = 1.e-9;

    constexpr auto initial_time = 0.057;

    constexpr auto time_step = 0.1;


} // namespace


namespace MoReFEM::TestNS::TimeManagerNS
{

    //! An helper class able through friendship to access internal content of \a TimeManager instance
    template<>
    class Viewer<time_manager_type>
    {
      public:
        explicit Viewer(time_manager_type& time_manager);

        [[nodiscard]] std::size_t NtimeModified() const noexcept;

        std::size_t TimeStepIndex() noexcept;

      private:
        time_manager_type& time_manager_;
    };

} // namespace MoReFEM::TestNS::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(check_api, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());

    TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::static_);

    BOOST_CHECK_EQUAL(time_manager.IsTimeStepConstant(), true);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);
    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 0UL);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetInverseTimeStep(), 10., epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 1UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 1UL);

    // As soon as time is modified, it should be set to dynamic and stay there.
    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::dynamic_);

    for (auto time_index{ 0UL }; time_index < 5UL; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 6UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 6. * time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 6UL);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 5UL);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 7UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 5. * time_step, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetMaximumTime(), 2., epsilon);

    for (auto time_index{ 0UL }; time_index < 14UL; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 19UL);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 21UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19 * time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 20UL);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 22UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 20 * time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), true);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 23UL);
    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 19UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19. * time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.ResetTimeManagerAtInitialTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 24UL);
    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 0UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
}

BOOST_FIXTURE_TEST_CASE(time_iteration_file, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    const time_manager_type time_manager(morefem_data.GetMpi(),
                                         morefem_data.GetModelSettings(),
                                         morefem_data.GetInputData(),
                                         program_type::test,
                                         morefem_data.GetResultDirectory());

    decltype(auto) file = time_manager.GetTimeIterationFile();

    BOOST_REQUIRE(file.DoExist());

    std::ifstream in{ file.Read() };

    std::string line;
    getline(in, line);

    BOOST_CHECK_EQUAL(line, "# Time iteration; time; numbering subset id; filename");

    // There is no additional content at the moment: it is up to `VariationalFormulation` class
    // to fill the data in this file.
}


BOOST_FIXTURE_TEST_CASE(valid_restart, fixture_type)
{
    TestNS::ClearSingletons<time_manager_type>::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Core/TimeStep/Constant/demo_valid_restart.lua") };

    const restart_morefem_data_type morefem_data{ std::move(lua_file) };

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());


    TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);

    decltype(auto) restart_dir_path =
        ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Restart::DataDirectory>(morefem_data);

    auto restart_dir = FilesystemNS::Directory(GetMpi(), restart_dir_path, FilesystemNS::behaviour::read);

    decltype(auto) time_iteration_file = restart_dir.AddFile("time_iteration.hhdata");

    BOOST_REQUIRE(time_iteration_file.DoExist());

    time_manager.SetRestartIfRelevant();
    BOOST_CHECK_EQUAL(viewer.TimeStepIndex(), 2UL);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 2UL); // value read in the Lua file
    BOOST_CHECK_CLOSE(time_manager.GetTime(), 0.257, epsilon);
    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::dynamic_);
}


BOOST_FIXTURE_TEST_CASE(invalid_restart, fixture_type)
{
    TestNS::ClearSingletons<time_manager_type>::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Core/TimeStep/Constant/demo_invalid_restart.lua") };

    const restart_morefem_data_type morefem_data{ std::move(lua_file) };

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());
    const TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);

    decltype(auto) restart_dir_path =
        ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Restart::DataDirectory>(morefem_data);

    auto restart_dir = FilesystemNS::Directory(GetMpi(), restart_dir_path, FilesystemNS::behaviour::read);

    decltype(auto) time_iteration_file = restart_dir.AddFile("time_iteration.hhdata");

    BOOST_REQUIRE(time_iteration_file.DoExist());

    BOOST_CHECK_THROW(time_manager.SetRestartIfRelevant(),
                      ExceptionNS::TimeManagerNS::ImproperRestartTimeForConstantTimeStep);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


namespace MoReFEM::TestNS::TimeManagerNS
{

    Viewer<time_manager_type>::Viewer(time_manager_type& time_manager) : time_manager_(time_manager)
    { }


    std::size_t Viewer<time_manager_type>::NtimeModified() const noexcept
    {
        return time_manager_.NtimeModified();
    }

    std::size_t Viewer<time_manager_type>::TimeStepIndex() noexcept
    {
        return time_manager_.TimeStepIndex();
    }

} // namespace MoReFEM::TestNS::TimeManagerNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
