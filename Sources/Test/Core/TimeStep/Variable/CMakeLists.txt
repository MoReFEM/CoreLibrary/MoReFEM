add_executable(TestVariableTimeStep)


target_sources(TestVariableTimeStep
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
)

target_link_libraries(TestVariableTimeStep
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)
    
morefem_organize_IDE(TestVariableTimeStep Test/Core Test/Core/TimeStep/Variable)

morefem_boost_test_sequential_mode(NAME VariableTimeStep
                                   EXE TestVariableTimeStep
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Core/TimeStep/Variable/demo.lua
                                   TIMEOUT 20)
