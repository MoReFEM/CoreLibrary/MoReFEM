// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <fstream>
#include <string>

#include "Core/TimeManager/Exceptions/Exception.hpp"

#include "Test/Tools/Fixture/Enum.hpp"

#define BOOST_TEST_MODULE variable_time_step
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/Policy/Evolution/VariableTimeStep.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Core/TimeStep/Variable/InputData.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::EmptyModelSettings,
                                              TestNS::VariableTimeStepNS::input_data_type,
                                              TestNS::VariableTimeStepNS::time_manager_type,
                                              program_type::test>;
    };


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        MockModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = MoReFEM::TestNS::VariableTimeStepNS::time_manager_type;


    constexpr auto epsilon = 1.e-9;

    constexpr auto initial_time = 0.057;

    constexpr auto initial_time_step = 0.1;

    constexpr auto minimum_time_step = 0.007;

} // namespace

namespace MoReFEM::TestNS::TimeManagerNS
{


    //! An helper class able through friendship to access internal content of \a TimeManager instance.
    template<>
    class Viewer<time_manager_type>
    {
      public:
        explicit Viewer(time_manager_type& time_manager);

        std::size_t TimeStepIndex() noexcept;

        [[nodiscard]] double GetMaximumTimeStep() const noexcept;

        [[nodiscard]] double GetMinimumTimeStep() const noexcept;

      private:
        time_manager_type& time_manager_;
    };


} // namespace MoReFEM::TestNS::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(constant_like_api, fixture_type)
{
    // Here we perform almost the same checks as for the constant time step case.

    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());

    const TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::static_);

    BOOST_CHECK_EQUAL(time_manager.IsTimeStepConstant(), false);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetInverseTimeStep(), 10., epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 1UL);

    // As soon as time is modified, it should be set to dynamic and stay there.
    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::dynamic_);

    for (auto time_index{ 0UL }; time_index < 5UL; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 6. * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 6UL);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 7UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 5. * initial_time_step, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetMaximumTime(), 2., epsilon);

    for (auto time_index{ 0UL }; time_index < 14UL; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 21UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19 * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 22UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 20 * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), true);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 23UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19. * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.ResetTimeManagerAtInitialTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 24UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
}

BOOST_FIXTURE_TEST_CASE(time_iteration_file, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    const time_manager_type time_manager(morefem_data.GetMpi(),
                                         morefem_data.GetModelSettings(),
                                         morefem_data.GetInputData(),
                                         program_type::test,
                                         morefem_data.GetResultDirectory());

    decltype(auto) file = time_manager.GetTimeIterationFile();

    BOOST_REQUIRE(file.DoExist());

    std::ifstream in{ file.Read() };

    std::string line;
    getline(in, line);

    BOOST_CHECK_EQUAL(line, "# Time iteration; time; numbering subset id; filename");

    // There is no additional content at the moment: it is up to `VariationalFormulation` class
    // to fill the data in this file.
}


BOOST_FIXTURE_TEST_CASE(set_time_step, fixture_type)
{
    // Here we perform almost the same checks as for the constant time step case.

    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());
    const TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);

    time_manager.SetTimeStep(10.21);
    time_manager.IncrementTime();

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 10.21, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 1UL);
}


BOOST_FIXTURE_TEST_CASE(adapt_time_step_increasing_when_already_max, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    decltype(auto) mpi = morefem_data.GetMpi();

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());
    const TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMaximumTimeStep(), 0.1, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::increasing);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
}


BOOST_FIXTURE_TEST_CASE(adapt_time_step_decreasing, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    decltype(auto) mpi = morefem_data.GetMpi();

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());
    const TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(time_manager.GetMaximumTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMinimumTimeStep(), minimum_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .5 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .25 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::decreasing);
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .125 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::decreasing);
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), minimum_time_step, epsilon);

    BOOST_CHECK_THROW(time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::decreasing),
                      ExceptionNS::TimeManagerNS::TimeStepAdaptationMinimumReached);
}


BOOST_FIXTURE_TEST_CASE(adapt_time_step_heal_after_decreasing, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetNonCstMoReFEMData();

    decltype(auto) mpi = morefem_data.GetMpi();

    time_manager_type time_manager(morefem_data.GetMpi(),
                                   morefem_data.GetModelSettings(),
                                   morefem_data.GetInputData(),
                                   program_type::test,
                                   morefem_data.GetResultDirectory());
    const TestNS::TimeManagerNS::Viewer<time_manager_type> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMaximumTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMinimumTimeStep(), minimum_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .5 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .25 * initial_time_step, epsilon);

    // So far exactly the same as previous test case... but now trying to recover.
    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::increasing);
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 0UL); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.043, epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 1UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 0.043, epsilon);

    time_manager.AdaptTimeStep(mpi, TimeManagerNS::adapt_time_step::increasing);
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), initial_time_step, epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(time_manager.NtimeModified(), 2UL);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 0.043 + initial_time_step, epsilon);
}

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


namespace MoReFEM::TestNS::TimeManagerNS
{


    Viewer<time_manager_type>::Viewer(time_manager_type& time_manager) : time_manager_(time_manager)
    { }


    double Viewer<time_manager_type>::GetMaximumTimeStep() const noexcept
    {
        return time_manager_.GetMaximumTimeStep();
    }


    double Viewer<time_manager_type>::GetMinimumTimeStep() const noexcept
    {
        return time_manager_.GetMinimumTimeStep();
    }


} // namespace MoReFEM::TestNS::TimeManagerNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
