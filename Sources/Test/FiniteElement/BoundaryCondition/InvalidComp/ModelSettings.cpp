// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/FiniteElement/BoundaryCondition/InvalidComp/InputData.hpp"


namespace MoReFEM::TestNS::BoundaryConditionNS::InvalidCompNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>({ " sole)" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::sole)>>({ " sole)" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ " sole)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full)>>({ " full)" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>>(
            { " sole)" });
    }


} // namespace MoReFEM::TestNS::BoundaryConditionNS::InvalidCompNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
