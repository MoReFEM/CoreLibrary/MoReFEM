Check an invalid component configuration in boundary condition is properly caught with an exception.

There are two cofigurations tested:

- With 'demo_too_high_dimension.lua', a 'Comp123' configuration is attempted with a mesh of dimension 2.
- With 'demo_scalar.lua', a 'Comp12' configuration is attempted with a mesh of dimension 2 for a scalar unknown.
