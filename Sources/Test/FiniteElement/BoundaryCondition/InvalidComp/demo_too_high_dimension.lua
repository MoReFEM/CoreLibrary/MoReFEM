-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.



Unknown1 = {


	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = "unknown",


	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'scalar', 'vectorial'})
	nature = "vectorial"

} -- Unknown1

Mesh1 = {


	-- Path of the mesh file to use. 
	-- IMPORTANT: if run from preprocessed data, this path won't be used (the already cut mesh will be used 
	-- instead). 
	-- Expected format: "VALUE"
	mesh = "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh",


	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = "Medit",


	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 2,


	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1.

} -- Mesh1

Domain1 = {


	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_index = { 1 },


	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 1 },


	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = { 1 },


	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = { }
    
} -- Domain1

EssentialBoundaryCondition1 = {


	-- Comp1, Comp2 or Comp3
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
	component = "Comp123",


	-- Name of the unknown addressed by the boundary condition.
	-- Expected format: "VALUE"
	unknown = "unknown",


	-- Values at each of the relevant component.
	-- Expected format: { VALUE1, VALUE2, ...}
	value = { 0., 0., 0. },


	-- Index of the domain onto which essential boundary condition is defined.
	-- Expected format: VALUE
	domain_index = 1,


	-- Whether the values of the boundary condition may vary over time.
	-- Expected format: 'true' or 'false' (without the quote)
	is_mutable = false

} -- EssentialBoundaryCondition1

FiniteElementSpace1 = {


	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,


	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 1,


	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	unknown_list = { "unknown" },


	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = { "P2" },


	-- List of the numbering subset to use for each unknown;
	-- Expected format: { VALUE1, VALUE2, ...}
	numbering_subset_list = { 1 }

} -- FiniteElementSpace1

Result = {


	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. You may also use 
	-- ${MOREFEM_START_TIME} in the value which will be replaced by a time under format 
	-- YYYY_MM_DD_HH_MM_SS.Please do not read the value directly from this Lua file: whenever you need the path 
	-- to the result directory, use instead MoReFEMData::GetResultDirectory(). 
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/FiniteElement/BoundaryCondition/InvalidComp/TooHighDimension",


	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,


	-- Defines the solutions output format. Set to false for ascii or true for binary.
	-- Expected format: 'true' or 'false' (without the quote)
	binary_output = false

} -- Result

