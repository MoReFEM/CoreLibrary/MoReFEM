add_executable(TestBCTwoMeshes)

target_sources(TestBCTwoMeshes
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
)


target_link_libraries(TestBCTwoMeshes
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestBCTwoMeshes Test/FiniteElement/BoundaryCondition Test/FiniteElement/BoundaryCondition/TwoMeshes)

morefem_boost_test_both_modes(NAME BCTwoMeshes
                              EXE TestBCTwoMeshes
                              TIMEOUT 20)
