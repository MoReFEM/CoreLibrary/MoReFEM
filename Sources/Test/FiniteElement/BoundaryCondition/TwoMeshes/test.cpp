// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <string_view>

#include "Test/Tools/Fixture/Enum.hpp"

#define BOOST_TEST_MODULE bc_two_meshes
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/FiniteElement/BoundaryCondition/TwoMeshes/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    using model_type = TestNS::BareModel<MoReFEM::TestNS::BoundaryConditionNS::TwoMeshesNS::morefem_data_type>;


    struct OutputDirWrapper
    {

        static constexpr std::string_view Path()
        {
            return "FiniteElement/BoundaryCondition/TwoMeshes";
        }
    };


    // clang-format off
    using fixture_type = TestNS::FixtureNS::ModelNoInputData
    <
        model_type,
        OutputDirWrapper,
        TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>,
        TestNS::FixtureNS::call_run_method_at_first_call::yes
    >;
    // clang-format on


} // namespace


BOOST_FIXTURE_TEST_CASE(check, fixture_type)
{
    GetModel();
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
