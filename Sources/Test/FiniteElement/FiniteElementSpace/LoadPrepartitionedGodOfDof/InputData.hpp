// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_LOADPREPARTITIONEDGODOFDOF_INPUTDATA_DOT_HPP_
#define MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_LOADPREPARTITIONEDGODOFDOF_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
// IWYU pragma: end_exports

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { original = 1UL, reconstructed = 11UL };


    //! Default value for some input datum that are required by a MoReFEM model but are actually unused
    //! for current test.
    constexpr auto original = 1UL;

    //! Index associated to stuff built from pre-partitioned data (which were themselves written from original
    //! ones).
    constexpr auto reconstructed = 11UL;

    //! We take the convention of adding this quantity to get reconstructed index from original one.
    //! For instance the pendant of original separated FEltSpaceIndex (1) is 11 (we add 10).
    constexpr auto reconstructed_overhead = 10UL;

    //! Enum class for unknowns.
    enum class UnknownIndex : std::size_t { scalar = 1, vectorial };


    //! Enum class for NumberingSubsets
    enum class NumberingSubsetIndex : std::size_t { scalar = 1, vectorial, mixed };


    //! Enum class for NumberingSubsets
    enum class FEltSpaceIndex : std::size_t { separated = 1, mixed, reconstructed_separated = 11, reconstructed_mixed };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::original)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>,

        InputDataNS::Domain<original>,
        InputDataNS::Domain<reconstructed>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::separated)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mixed)>,

        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::scalar)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::vectorial)>,

        InputDataNS::Result,
        InputDataNS::Parallelism
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::separated)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mixed)>::IndexedSectionDescription,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::original)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mixed)>::IndexedSectionDescription,
        InputDataNS::Domain<original>::IndexedSectionDescription,
        InputDataNS::Domain<reconstructed>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::vectorial)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::test>;


} // namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_LOADPREPARTITIONEDGODOFDOF_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
