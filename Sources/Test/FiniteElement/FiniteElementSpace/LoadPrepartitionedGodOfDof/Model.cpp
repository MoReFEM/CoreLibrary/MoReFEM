// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <filesystem>
#include <memory>
#include <string>
#include <utility>

#include "Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/Model.hpp"

#include "Utilities/Filesystem/Behaviour.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#include "Core/Enum.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Information.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Output/WritePrepartitionedData.hpp"

#include "Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/InputData.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS
{


    namespace // anonymous
    {


        FEltSpace::vector_unique_ptr GenerateReconstructedFEltSpaceList()
        {
            FEltSpace::vector_unique_ptr ret;

            decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
            decltype(auto) domain_manager = DomainManager::GetInstance();

            decltype(auto) original_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::original));

            decltype(auto) reconstructed_god_of_dof_ptr =
                god_of_dof_manager.GetGodOfDofPtr(AsMeshId(MeshIndex::reconstructed));
            decltype(auto) reconstructed_domain = domain_manager.GetDomain(DomainNS::unique_id{ reconstructed });

            const auto original_felt_space_unique_id_list = { FEltSpaceIndex::separated, FEltSpaceIndex::mixed };

            for (auto original_felt_space_unique_id : original_felt_space_unique_id_list)
            {
                decltype(auto) original_separated_felt_space =
                    original_god_of_dof.GetFEltSpace(AsFEltSpaceId(original_felt_space_unique_id));

                // Copy on purpose!
                auto extended_unknown_list = original_separated_felt_space.GetExtendedUnknownList();

                auto felt_space_ptr = std::make_unique<FEltSpace>(
                    reconstructed_god_of_dof_ptr,
                    reconstructed_domain,
                    AsFEltSpaceId(original_felt_space_unique_id) + FEltSpaceNS::unique_id{ reconstructed_overhead },
                    std::move(extended_unknown_list));

                ret.emplace_back(std::move(felt_space_ptr));
            }

            return ret;
        }


    } // namespace


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) mpi = parent::GetMpi();
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
        decltype(auto) mesh = mesh_manager.GetMesh(MeshNS::unique_id{ 1UL });

        decltype(auto) input_data = parent::GetMoReFEMData().GetInputData();

        decltype(auto) format = Internal::MeshNS::FormatNS::GetType(
            Internal::InputDataNS::ExtractLeaf<InputDataNS::Mesh<1>::Format>::Value(input_data));

        decltype(auto) prepartitioned_data_dir_str =
            ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Parallelism::Directory>(parent::GetMoReFEMData());

        const FilesystemNS::Directory prepartitioned_data_dir(
            mpi, prepartitioned_data_dir_str, FilesystemNS::behaviour::read);

        const FilesystemNS::Directory mesh_subdir(prepartitioned_data_dir, "Mesh_1");

        std::string filename = "mesh.";

        switch (format)
        {
        case MeshNS::Format::Medit:
            filename += Internal::MeshNS::FormatNS::Information<::MoReFEM::MeshNS::Format::Medit>::Extension();
            break;
        case MeshNS::Format::Ensight:
            filename += Internal::MeshNS::FormatNS::Information<::MoReFEM::MeshNS::Format::Ensight>::Extension();
            break;
        case MeshNS::Format::Vizir:
        case MeshNS::Format::VTK_PolygonalData:
        case MeshNS::Format::End:
            assert(false && "Only Ensight and Medit formats are covered by current test.");
            exit(EXIT_FAILURE);
        }

        const auto reduced_mesh_path = FilesystemNS::File{ mesh_subdir.AddFile(filename) };
        const auto mesh_prepartitioned_data_path = FilesystemNS::File{ mesh_subdir.AddFile("mesh_data.lua") };

        assert(reduced_mesh_path.DoExist() && "Should have been defined in Model::Init()");
        assert(mesh_prepartitioned_data_path.DoExist() && "Should have been defined in Model::Init()");

        Wrappers::Lua::OptionFile mesh_prepartitioned_data(mesh_prepartitioned_data_path);

        mesh_manager.LoadFromPrepartitionedData(
            mpi, AsMeshId(MeshIndex::reconstructed), mesh_prepartitioned_data, mesh.GetDimension(), format);
    }


    void Model::SupplFinalize()
    { }


    void Model::LoadGodOfDofFromPrepartionedData() const
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) mpi = parent::GetMpi();
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
        decltype(auto) mesh = mesh_manager.GetNonCstMesh(AsMeshId(MeshIndex::reconstructed));

        god_of_dof_manager.Create(mpi, mesh);

        // Circumvent the fact Directory can't create non mpi directories - with good reasons!
        std::filesystem::path directory_path{ GetOutputDirectory().GetDirectoryEntry().path() };
        directory_path /= std::filesystem::path("Reconstructed");

        std::filesystem::create_directories(directory_path);

        const FilesystemNS::Directory output_directory(directory_path, FilesystemNS::behaviour::ignore);

        BOOST_CHECK(output_directory.DoExist());

        FEltSpace::vector_unique_ptr felt_space_list = GenerateReconstructedFEltSpaceList();

        decltype(auto) reconstructed_god_of_dof =
            god_of_dof_manager.GetNonCstGodOfDof(AsMeshId(MeshIndex::reconstructed));


        decltype(auto) prepartitioned_data_dir_str =
            ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Parallelism::Directory>(parent::GetMoReFEMData());

        const FilesystemNS::Directory prepartitioned_data_dir(
            mpi, prepartitioned_data_dir_str, FilesystemNS::behaviour::read);

        const FilesystemNS::Directory mesh_subdir(prepartitioned_data_dir, "Mesh_1");

        const auto god_of_dof_prepartitioned_data_file = mesh_subdir.AddFile("god_of_dof_data.lua");

        Wrappers::Lua::OptionFile god_of_dof_prepartitioned_data(god_of_dof_prepartitioned_data_file);

        // Initialize the reconstructed \a GodOfDof.
        // The purpose here is truly to check the internal without incurring a new Lua file; in a proper model
        // you should NEVER do what is done here (and requires anyway friendship to \a GodOfDof class).
        {
            reconstructed_god_of_dof.SetFEltSpaceList(std::move(felt_space_list));
            reconstructed_god_of_dof.InitOutputDirectories(output_directory);

#ifndef NDEBUG
            reconstructed_god_of_dof.do_consider_processor_wise_local_2_global_ =
                DoConsiderProcessorWiseLocal2Global::yes;
#endif // NDEBUG
            reconstructed_god_of_dof.InitFromPreprocessedDataHelper(god_of_dof_prepartitioned_data);
            reconstructed_god_of_dof.FinalizeInitialization(DoConsiderProcessorWiseLocal2Global::yes, nullptr);
        }
    }


    void Model::CheckProcessorWiseNodeBearerList() const
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) original_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::original));
        decltype(auto) reconstructed_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::reconstructed));

        {
            decltype(auto) original_node_bearer_list = original_god_of_dof.GetProcessorWiseNodeBearerList();
            decltype(auto) reconstructed_node_bearer_list = reconstructed_god_of_dof.GetProcessorWiseNodeBearerList();

            BOOST_CHECK_EQUAL(original_node_bearer_list.size(),
                              reconstructed_god_of_dof.GetProcessorWiseNodeBearerList().size());

            const auto mpi_rank = GetMpi().GetRank();

            const auto size = original_node_bearer_list.size();

            for (auto i = 0UL; i < size; ++i)
            {
                const auto& original_node_bearer_ptr = original_node_bearer_list[i];
                const auto& reconstructed_node_bearer_ptr = reconstructed_node_bearer_list[i];

                assert(!(!original_node_bearer_ptr));
                assert(!(!reconstructed_node_bearer_ptr));

                BOOST_CHECK_EQUAL(original_node_bearer_ptr->GetProgramWiseIndex(),
                                  reconstructed_node_bearer_ptr->GetProgramWiseIndex());

                BOOST_CHECK(original_node_bearer_ptr->GetInterface()
                                .ComputeCoordsIndexList<CoordsNS::index_enum::program_wise_position>()
                            == reconstructed_node_bearer_ptr->GetInterface()
                                   .ComputeCoordsIndexList<CoordsNS::index_enum::program_wise_position>());

                BOOST_CHECK_EQUAL(original_node_bearer_ptr->GetProcessor(),
                                  reconstructed_node_bearer_ptr->GetProcessor());

                BOOST_CHECK_EQUAL(original_node_bearer_ptr->GetProcessor(), mpi_rank);
            }
        }
    }


    void Model::CheckGhostNodeBearerList()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) original_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::original));
        decltype(auto) reconstructed_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::reconstructed));

        decltype(auto) original_ghost_node_bearer_list = original_god_of_dof.GetGhostNodeBearerList();
        decltype(auto) reconstructed_ghost_node_bearer_list = reconstructed_god_of_dof.GetGhostNodeBearerList();

        BOOST_CHECK_EQUAL(original_ghost_node_bearer_list.size(), reconstructed_ghost_node_bearer_list.size());

        const auto size = original_ghost_node_bearer_list.size();

        for (auto i = 0UL; i < size; ++i)
        {
            const auto& original_ghost_node_bearer_ptr = original_ghost_node_bearer_list[i];
            const auto& reconstructed_ghost_node_bearer_ptr = reconstructed_ghost_node_bearer_list[i];

            assert(!(!original_ghost_node_bearer_ptr));
            assert(!(!reconstructed_ghost_node_bearer_ptr));

            BOOST_CHECK_EQUAL(original_ghost_node_bearer_ptr->GetProgramWiseIndex(),
                              reconstructed_ghost_node_bearer_ptr->GetProgramWiseIndex());

            BOOST_CHECK(original_ghost_node_bearer_ptr->GetInterface()
                            .ComputeCoordsIndexList<CoordsNS::index_enum::program_wise_position>()
                        == reconstructed_ghost_node_bearer_ptr->GetInterface()
                               .ComputeCoordsIndexList<CoordsNS::index_enum::program_wise_position>());

            BOOST_CHECK_EQUAL(original_ghost_node_bearer_ptr->GetProcessor(),
                              reconstructed_ghost_node_bearer_ptr->GetProcessor());
        }
    }


    void Model::CheckDofListInGodOfDof()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) original_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::original));
        decltype(auto) reconstructed_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::reconstructed));

        BOOST_CHECK_EQUAL(original_god_of_dof.NprocessorWiseDof(), reconstructed_god_of_dof.NprocessorWiseDof());

        BOOST_CHECK_EQUAL(original_god_of_dof.NprogramWiseDof(), reconstructed_god_of_dof.NprogramWiseDof());

        {
            decltype(auto) original_dof_list = original_god_of_dof.GetProcessorWiseDofList();
            decltype(auto) reconstructed_dof_list = reconstructed_god_of_dof.GetProcessorWiseDofList();

            BOOST_CHECK_EQUAL(original_dof_list.size(), reconstructed_dof_list.size());

            const auto size = original_dof_list.size();

            for (auto i = 0UL; i < size; ++i)
            {
                decltype(auto) original_dof_ptr = original_dof_list[i];
                decltype(auto) reconstructed_dof_ptr = reconstructed_dof_list[i];

                assert(!(!original_dof_ptr));
                assert(!(!reconstructed_dof_ptr));

                const auto& original_dof = *original_dof_ptr;
                const auto& reconstructed_dof = *reconstructed_dof_ptr;

                BOOST_CHECK_EQUAL(original_dof.GetInternalProcessorWiseOrGhostIndex(),
                                  reconstructed_dof.GetInternalProcessorWiseOrGhostIndex());
            }
        }
    }


    void Model::CheckDofListInGodOfDofForNumberingSubset(NumberingSubsetIndex numbering_subset_index)
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) original_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::original));
        decltype(auto) reconstructed_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::reconstructed));

        decltype(auto) numbering_subset =
            original_god_of_dof.GetNumberingSubset(AsNumberingSubsetId(numbering_subset_index));

        {
            decltype(auto) original_dof_list = original_god_of_dof.GetProcessorWiseDofList();
            decltype(auto) reconstructed_dof_list = reconstructed_god_of_dof.GetProcessorWiseDofList();

            assert(original_dof_list.size() == reconstructed_dof_list.size()
                   && "Should have been checked by previous test.");

            const auto size = original_dof_list.size();

            for (auto i = 0UL; i < size; ++i)
            {
                decltype(auto) original_dof_ptr = original_dof_list[i];
                decltype(auto) reconstructed_dof_ptr = reconstructed_dof_list[i];

                assert(!(!original_dof_ptr));
                assert(!(!reconstructed_dof_ptr));

                const auto& original_dof = *original_dof_ptr;
                const auto& reconstructed_dof = *reconstructed_dof_ptr;

                if (original_dof.IsInNumberingSubset(numbering_subset))
                {
                    BOOST_CHECK(reconstructed_dof.IsInNumberingSubset(numbering_subset));

                    BOOST_CHECK_EQUAL(original_dof.GetProcessorWiseOrGhostIndex(numbering_subset),
                                      reconstructed_dof.GetProcessorWiseOrGhostIndex(numbering_subset));

                    BOOST_CHECK_EQUAL(original_dof.GetProgramWiseIndex(numbering_subset),
                                      reconstructed_dof.GetProgramWiseIndex(numbering_subset));
                } else
                {
                    BOOST_CHECK(!reconstructed_dof.IsInNumberingSubset(numbering_subset));
                }
            }
        }
    }


    void Model::CheckDofListInFEltSpace(FEltSpaceIndex felt_space_unique_id)
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) original_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::original));
        decltype(auto) reconstructed_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::reconstructed));

        decltype(auto) original_felt_space = original_god_of_dof.GetFEltSpace(AsFEltSpaceId(felt_space_unique_id));

        decltype(auto) reconstructed_felt_space = reconstructed_god_of_dof.GetFEltSpace(
            AsFEltSpaceId(felt_space_unique_id) + FEltSpaceNS::unique_id{ reconstructed_overhead });

        {
            decltype(auto) original_dof_list = original_felt_space.GetProcessorWiseDofList();
            decltype(auto) reconstructed_dof_list = reconstructed_felt_space.GetProcessorWiseDofList();

            BOOST_CHECK_EQUAL(original_dof_list.size(), reconstructed_dof_list.size());

            const auto size = original_dof_list.size();

            for (auto i = 0UL; i < size; ++i)
            {
                decltype(auto) original_dof = original_dof_list[i];
                decltype(auto) reconstructed_dof = reconstructed_dof_list[i];

                assert(!(!original_dof));
                assert(!(!reconstructed_dof));

                BOOST_CHECK_EQUAL(original_dof->GetInternalProcessorWiseOrGhostIndex(),
                                  reconstructed_dof->GetInternalProcessorWiseOrGhostIndex());
            }
        }

        {
            decltype(auto) original_ghost_dof_list = original_felt_space.GetGhostDofList();
            decltype(auto) reconstructed_ghost_dof_list = reconstructed_felt_space.GetGhostDofList();

            BOOST_CHECK_EQUAL(original_ghost_dof_list.size(), reconstructed_ghost_dof_list.size());

            const auto size = original_ghost_dof_list.size();

            for (auto i = 0UL; i < size; ++i)
            {
                decltype(auto) original_dof = original_ghost_dof_list[i];
                decltype(auto) reconstructed_dof = reconstructed_ghost_dof_list[i];

                assert(!(!original_dof));
                assert(!(!reconstructed_dof));

                BOOST_CHECK_EQUAL(original_dof->GetInternalProcessorWiseOrGhostIndex(),
                                  reconstructed_dof->GetInternalProcessorWiseOrGhostIndex());
            }
        }
    }


    void Model::CheckMatrixPattern()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) original_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::original));
        decltype(auto) reconstructed_god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::reconstructed));

        decltype(auto) scalar_numbering_subset =
            original_god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::scalar));
        decltype(auto) vectorial_numbering_subset =
            original_god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::vectorial));
        decltype(auto) mixed_numbering_subset =
            original_god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::mixed));

        BOOST_CHECK(original_god_of_dof.GetMatrixPattern(scalar_numbering_subset, scalar_numbering_subset)
                    == reconstructed_god_of_dof.GetMatrixPattern(scalar_numbering_subset, scalar_numbering_subset));

        BOOST_CHECK(
            original_god_of_dof.GetMatrixPattern(vectorial_numbering_subset, vectorial_numbering_subset)
            == reconstructed_god_of_dof.GetMatrixPattern(vectorial_numbering_subset, vectorial_numbering_subset));

        BOOST_CHECK(original_god_of_dof.GetMatrixPattern(mixed_numbering_subset, mixed_numbering_subset)
                    == reconstructed_god_of_dof.GetMatrixPattern(mixed_numbering_subset, mixed_numbering_subset));

        BOOST_CHECK(original_god_of_dof.GetMatrixPattern(scalar_numbering_subset, vectorial_numbering_subset)
                    == reconstructed_god_of_dof.GetMatrixPattern(scalar_numbering_subset, vectorial_numbering_subset));

        BOOST_CHECK(original_god_of_dof.GetMatrixPattern(mixed_numbering_subset, scalar_numbering_subset)
                    == reconstructed_god_of_dof.GetMatrixPattern(mixed_numbering_subset, scalar_numbering_subset));
    }


} // namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
