// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_LOADPREPARTITIONEDGODOFDOF_MODEL_DOT_HPP_
#define MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_LOADPREPARTITIONEDGODOFDOF_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"

#include "Model/Model.hpp"

#include "Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/InputData.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS
{


    /*!
     * \brief Toy model used to perform tests about reconstruction of a \a Mesh from pre-partitioned data.
     *
     * This test must be run in parallel; its principle is to:
     * - Initialize the model normally (including separating the mesh among all mpi processors and reducing it).
     * - Write the data related to the partition of said mesh.
     * - Load from the data written a new mesh.
     * - Check both mesh are indistinguishable.
     */
    //! \copydoc doxygen_hide_model_4_test
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<self, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());


      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();

        /*!
         * \brief Load the \a GodOfDof from prepartitioned data.
         */
        void LoadGodOfDofFromPrepartionedData() const;

        //! Check the \a NodeBearer list for processor-wise data is the same as in the original run.
        void CheckProcessorWiseNodeBearerList() const;

        //! Check the \a NodeBearer list for ghost data is the same as in the original run.
        static void CheckGhostNodeBearerList();

        //! Check the \a Dof list is the same as in the original run in \a GodOfDof.
        static void CheckDofListInGodOfDof();

        //! Check the indexes of \a Dof related to the \a NumberingSubset indexed by \a numbering_subset_index
        //! are on par with original run. \param[in] numbering_subset_index Index of the \a NumberingSubset
        //! considered.
        static void CheckDofListInGodOfDofForNumberingSubset(NumberingSubsetIndex numbering_subset_index);

        //! Check the \a Dof list is the same as in the original run in \a FEltSpace.
        //! \param[in] felt_space_unique_id Index of the \a FEltSpace considered.
        static void CheckDofListInFEltSpace(FEltSpaceIndex felt_space_unique_id);

        //! Check the matrix pattern match.
        static void CheckMatrixPattern();


        ///@}
    };


} // namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_LOADPREPARTITIONEDGODOFDOF_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
