// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/InputData.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::separated)>>(
            { "Finite element space for separated" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mixed)>>(
            { "Finite element space for mixed" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::original)>>({ " original)" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>>({ " scalar)" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>>({ " vectorial)" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>>({ " scalar)" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>>(
            { " vectorial)" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mixed)>>({ " mixed)" });
        SetDescription<InputDataNS::Domain<original>>({ " original" });
        SetDescription<InputDataNS::Domain<reconstructed>>({ " reconstructed" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::scalar)>>(
            { " scalar)" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::vectorial)>>(
            { " vectorial)" });
    }


} // namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
