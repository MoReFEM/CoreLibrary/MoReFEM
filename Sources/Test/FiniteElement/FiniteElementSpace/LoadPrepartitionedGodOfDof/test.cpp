// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdlib>

#include "Utilities/Exceptions/GracefulExit.hpp"

#define BOOST_TEST_MODULE load_god_of_dof_prepartitioned_data_tri
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/InputData.hpp"
#include "Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::LoadPrepartitionedGodOfDofNS::Model
    >;
    // clang-format on


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // # __clang__


BOOST_FIXTURE_TEST_CASE(load_god_of_dof_from_prepartitioned, fixture_type)
{
    try
    {
        decltype(auto) model = GetModel();
        model.LoadGodOfDofFromPrepartionedData();
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_processor_wise_node_bearer_list, fixture_type)
{
    try
    {
        decltype(auto) model = GetModel();
        model.CheckProcessorWiseNodeBearerList();
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_ghost_node_bearer_list, fixture_type)
{
    try
    {
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckGhostNodeBearerList();
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof, fixture_type)
{
    try
    {
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckDofListInGodOfDof();
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof_scalar_numbering_subset, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckDofListInGodOfDofForNumberingSubset(
            NumberingSubsetIndex::scalar);
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof_vectorial_numbering_subset, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckDofListInGodOfDofForNumberingSubset(
            NumberingSubsetIndex::vectorial);
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_god_of_dof_mixed_numbering_subset, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckDofListInGodOfDofForNumberingSubset(
            NumberingSubsetIndex::mixed);
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_separated_felt_space, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckDofListInFEltSpace(FEltSpaceIndex::separated);
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_dof_list_in_mixed_felt_space, fixture_type)
{
    try
    {
        using namespace TestNS::LoadPrepartitionedGodOfDofNS;
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckDofListInFEltSpace(FEltSpaceIndex::mixed);
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


BOOST_FIXTURE_TEST_CASE(check_matrix_pattern, fixture_type)
{
    try
    {
        // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
        [[maybe_unused]] decltype(auto) model = GetModel();
        MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS::Model::CheckMatrixPattern();
    }
    // NOLINTNEXTLINE(bugprone-empty-catch)
    catch (const ExceptionNS::GracefulExit&)
    { }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
