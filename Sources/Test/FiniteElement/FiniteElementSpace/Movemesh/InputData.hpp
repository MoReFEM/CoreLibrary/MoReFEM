// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_MOVEMESH_INPUTDATA_DOT_HPP_
#define MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_MOVEMESH_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: begin_exports
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/InputData/InputData.hpp"

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/None.hpp"
#include "Core/TimeManager/TimeManager.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::TestNS::MovemeshNS
{

    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { movable = 1UL, unmoved = 2UL };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { domain = 10UL, placeholder_for_unmoved = 20UL };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t {
        main = 10UL,
        placeholder_for_unmoved = 20UL

    };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t {
        displacement = 10UL,
        displacement_no_movemesh = 11UL,
        other_vectorial_unknown = 12UL,
        placeholder_for_unmoved = 20UL
    };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex

    {
        solver = 1
    };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        displacement = 10UL,
        displacement_no_movemesh = 11UL,
        placeholder_for_unmoved = 20UL

    };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>::DoMoveMesh,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_no_movemesh)>::DoMoveMesh,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::placeholder_for_unmoved)>::DoMoveMesh,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_no_movemesh)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::placeholder_for_unmoved)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::movable)>,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::unmoved)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::placeholder_for_unmoved)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::main)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::placeholder_for_unmoved)>,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::main)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::placeholder_for_unmoved)>::IndexedSectionDescription,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_no_movemesh)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::placeholder_for_unmoved)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_no_movemesh)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::placeholder_for_unmoved)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::movable)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::unmoved)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::placeholder_for_unmoved)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::None>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::test>;

    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using scalar_unknown_test_input_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(TestNS::MovemeshNS::NumberingSubsetIndex::displacement)>::DoMoveMesh,
        InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::displacement)>,
        InputDataNS::Mesh<EnumUnderlyingType(TestNS::MovemeshNS::MeshIndex::movable)>,
        InputDataNS::Domain<EnumUnderlyingType(TestNS::MovemeshNS::DomainIndex::domain)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(TestNS::MovemeshNS::FEltSpaceIndex::main)>,
        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using scalar_unknown_test_input_data_type = InputData<scalar_unknown_test_input_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    using scalar_unknown_test_model_settings_tuple = std::tuple<
        InputDataNS::NumberingSubset<EnumUnderlyingType(
            TestNS::MovemeshNS::NumberingSubsetIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(
            TestNS::MovemeshNS::UnknownIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(TestNS::MovemeshNS::MeshIndex::movable)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(TestNS::MovemeshNS::DomainIndex::domain)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(
            TestNS::MovemeshNS::FEltSpaceIndex::main)>::IndexedSectionDescription>;

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettingsForScalarUnknownTest : public ::MoReFEM::ModelSettings<scalar_unknown_test_model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using several_unknown_case_input_tuple_type =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(TestNS::MovemeshNS::NumberingSubsetIndex::displacement)>::DoMoveMesh,
        InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::displacement)>,
        InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::other_vectorial_unknown)>,
        InputDataNS::Mesh<EnumUnderlyingType(TestNS::MovemeshNS::MeshIndex::movable)>,
        InputDataNS::Domain<EnumUnderlyingType(TestNS::MovemeshNS::DomainIndex::domain)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(TestNS::MovemeshNS::FEltSpaceIndex::main)>,
        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_settings_tuple
    using several_unknown_case_input_data_type = InputData<several_unknown_case_input_tuple_type>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using several_unknown_test_model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(TestNS::MovemeshNS::NumberingSubsetIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::other_vectorial_unknown)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(TestNS::MovemeshNS::MeshIndex::movable)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(TestNS::MovemeshNS::DomainIndex::domain)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(TestNS::MovemeshNS::FEltSpaceIndex::main)>::IndexedSectionDescription
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettingsForSeveralUnknownTest
    : public ::MoReFEM::ModelSettings<several_unknown_test_model_settings_tuple>
    {
        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

} // namespace MoReFEM::TestNS::MovemeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_FINITEELEMENT_FINITEELEMENTSPACE_MOVEMESH_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
