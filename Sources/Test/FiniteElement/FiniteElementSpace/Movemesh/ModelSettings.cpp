// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/FiniteElement/FiniteElementSpace/Movemesh/InputData.hpp"


namespace MoReFEM::TestNS::MovemeshNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::main)>>(
            { "Main finite element space" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::placeholder_for_unmoved)>>(
            { "Placeholder_for_unmoved" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>>(
            { " displacement" });
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_no_movemesh)>>(
            { " displacement_no_movemesh" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::placeholder_for_unmoved)>>(
            { " placeholder_for_unmoved" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>({ " displacement" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_no_movemesh)>>(
            { " displacement_no_movemesh" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::placeholder_for_unmoved)>>(
            { " placeholder_for_unmoved" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::movable)>>({ " movable" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::unmoved)>>({ " unmoved" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::placeholder_for_unmoved)>>(
            { " placeholder_for_unmoved" });
    }


    void ModelSettingsForScalarUnknownTest::Init()
    {
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(TestNS::MovemeshNS::NumberingSubsetIndex::displacement)>>(
            { " displacement" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::displacement)>>(
            { " displacement" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(TestNS::MovemeshNS::MeshIndex::movable)>>({ " movable" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(TestNS::MovemeshNS::DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(TestNS::MovemeshNS::FEltSpaceIndex::main)>>(
            { " main" });
    }


    void ModelSettingsForSeveralUnknownTest::Init()
    {
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(TestNS::MovemeshNS::NumberingSubsetIndex::displacement)>>(
            { " displacement" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::displacement)>>(
            { " displacement" });
        SetDescription<
            InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::other_vectorial_unknown)>>(
            { " other_vectorial_unknown" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(TestNS::MovemeshNS::MeshIndex::movable)>>({ " movable" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(TestNS::MovemeshNS::DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(TestNS::MovemeshNS::FEltSpaceIndex::main)>>(
            { " main" });
    }


} // namespace MoReFEM::TestNS::MovemeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
