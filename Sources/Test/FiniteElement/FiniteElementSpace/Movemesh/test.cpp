// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <filesystem>
#include <random>
#include <sstream>
#include <unordered_map>
#include <utility>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Nodes_and_dofs/Dof.hpp"
#include "FiniteElement/Nodes_and_dofs/DofIndexesTypes.hpp"

#define BOOST_TEST_MODULE movemesh
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/None.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/Exceptions/Movemesh.hpp"

#include "Test/FiniteElement/FiniteElementSpace/Movemesh/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace // anonymous
{

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::None>;


    using model_type = TestNS::BareModel<TestNS::MovemeshNS::morefem_data_type>;


    /*!
     * \brief Generate a random displacement field - our purpose is to check is Coords is displaced as expected without mixup between them.// IWYU pragma:
     *
     * It is "semi" random as the main contribution is the rank (to be able to trace exactly mishaps if the test fail).
     */
    GlobalVector GenerateVectorWithSemiRandomContent(const GodOfDof& god_of_dof,
                                                     const NumberingSubset& numbering_subset);

    using mapping_type = std::unordered_map<Coords::shared_ptr, Dof::vector_shared_ptr>;

    /*!
     * \brief Identify for each \a Coords of the mesh the related \a Dofs (there should be three of them)
     */
    mapping_type GenerateMappingCoordsDof(const FEltSpace& felt_space, const NumberingSubset& numbering_subset);

    Coords::shared_ptr AttemptToFindUnmovedCoords(const Mesh& unmoved_mesh, const Coords& moved_coords);

    template<class ModelT>
    void CheckMeshesPathAreIdentical(const ModelT& model);


    std::pair<Coords::shared_ptr, Dof::vector_shared_ptr>
    Fetch(const Coords::shared_ptr& movable_coords_ptr, const Mesh& unmoved_mesh, const mapping_type& mapping);

    /*!
     * \brief Check the mesh moved properly.
     */
    void CheckMovemesh(const Wrappers::Mpi& mpi,
                       const Coords::vector_shared_ptr& moved_coords_list,
                       const Mesh& unmoved_mesh,
                       const NumberingSubset& numbering_subset,
                       const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>& displacement,
                       const mapping_type& mapping);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // # __clang__

BOOST_FIXTURE_TEST_SUITE(get_environment_variable, TestNS::FixtureNS::TestEnvironment)


BOOST_AUTO_TEST_CASE(no_movemesh_data)
{
    TestNS::ClearSingletons<time_manager_type>::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/Movemesh/demo_mesh_Q1_felt_Q1.lua") };

    TestNS::MovemeshNS::morefem_data_type morefem_data{ std::move(lua_file) };

    model_type model(morefem_data);
    model.Initialize();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MoReFEM::TestNS::MovemeshNS::MeshIndex::movable));
    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(MoReFEM::TestNS::MovemeshNS::FEltSpaceIndex::main));
    decltype(auto) numbering_subset_no_movemesh_data = god_of_dof.GetNumberingSubset(
        AsNumberingSubsetId(MoReFEM::TestNS::MovemeshNS::NumberingSubsetIndex::displacement_no_movemesh));

    GlobalVector vector(numbering_subset_no_movemesh_data);
    AllocateGlobalVector(god_of_dof, vector);

    BOOST_CHECK_THROW(felt_space.MoveMesh(vector), Internal::ExceptionsNS::Movemesh::NoMovemeshData);
}


BOOST_AUTO_TEST_CASE(movemesh_mesh_Q1_felt_Q1)
{
    TestNS::ClearSingletons<time_manager_type>::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/Movemesh/demo_mesh_Q1_felt_Q1.lua") };

    TestNS::MovemeshNS::morefem_data_type morefem_data{ std::move(lua_file) };

    model_type model(morefem_data);
    model.Initialize();

    CheckMeshesPathAreIdentical(model);

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MoReFEM::TestNS::MovemeshNS::MeshIndex::movable));
    decltype(auto) mesh = god_of_dof.GetMesh();

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(MoReFEM::TestNS::MovemeshNS::FEltSpaceIndex::main));
    decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(
        AsNumberingSubsetId(MoReFEM::TestNS::MovemeshNS::NumberingSubsetIndex::displacement));

    const auto mapping = GenerateMappingCoordsDof(felt_space, numbering_subset);

    const GlobalVector displacement_field = GenerateVectorWithSemiRandomContent(god_of_dof, numbering_subset);
    // displacement_field.View(god_of_dof.GetMpi());

    felt_space.MoveMesh(displacement_field);

    decltype(auto) unmoved_god_of_dof = model.GetGodOfDof(AsMeshId(MoReFEM::TestNS::MovemeshNS::MeshIndex::unmoved));
    decltype(auto) unmoved_mesh = unmoved_god_of_dof.GetMesh();
    decltype(auto) mpi = morefem_data.GetMpi();

    {
        const Wrappers::Petsc::AccessGhostContent access_content(displacement_field);
        const auto& vector_with_ghost = access_content.GetVectorWithGhost();
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> displacement(vector_with_ghost);

        CheckMovemesh(mpi, mesh.GetProcessorWiseCoordsList(), unmoved_mesh, numbering_subset, displacement, mapping);

        CheckMovemesh(mpi, mesh.GetGhostCoordsList(), unmoved_mesh, numbering_subset, displacement, mapping);
    }

    GlobalVector zero(displacement_field);
    zero.ZeroEntries();

    // Apply a new displacement from initial position - and here this displacement is chosen null...
    felt_space.MoveMeshFromInitialPosition(zero);

    {
        const Wrappers::Petsc::AccessGhostContent access_ghost_content(zero);
        const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> zero_displacement(vector_with_ghost);

        CheckMovemesh(
            mpi, mesh.GetProcessorWiseCoordsList(), unmoved_mesh, numbering_subset, zero_displacement, mapping);

        CheckMovemesh(mpi, mesh.GetGhostCoordsList(), unmoved_mesh, numbering_subset, zero_displacement, mapping);
    }
}


BOOST_AUTO_TEST_CASE(movemesh_mesh_Q1_felt_Q2)
{
    TestNS::ClearSingletons<time_manager_type>::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/Movemesh/demo_mesh_Q1_felt_Q2.lua") };

    TestNS::MovemeshNS::morefem_data_type morefem_data{ std::move(lua_file) };

    model_type model(morefem_data);
    model.Initialize();

    CheckMeshesPathAreIdentical(model);

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MoReFEM::TestNS::MovemeshNS::MeshIndex::movable));
    decltype(auto) mesh = god_of_dof.GetMesh();

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(MoReFEM::TestNS::MovemeshNS::FEltSpaceIndex::main));
    decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(
        AsNumberingSubsetId(MoReFEM::TestNS::MovemeshNS::NumberingSubsetIndex::displacement));

    const auto mapping = GenerateMappingCoordsDof(felt_space, numbering_subset);

    const GlobalVector displacement_field = GenerateVectorWithSemiRandomContent(god_of_dof, numbering_subset);
    // displacement_field.View(god_of_dof.GetMpi());

    felt_space.MoveMesh(displacement_field);

    decltype(auto) unmoved_god_of_dof = model.GetGodOfDof(AsMeshId(MoReFEM::TestNS::MovemeshNS::MeshIndex::unmoved));
    decltype(auto) unmoved_mesh = unmoved_god_of_dof.GetMesh();
    decltype(auto) mpi = morefem_data.GetMpi();

    {
        const Wrappers::Petsc::AccessGhostContent access_content(displacement_field);
        const auto& vector_with_ghost = access_content.GetVectorWithGhost();
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> displacement(vector_with_ghost);

        CheckMovemesh(mpi, mesh.GetProcessorWiseCoordsList(), unmoved_mesh, numbering_subset, displacement, mapping);

        CheckMovemesh(mpi, mesh.GetGhostCoordsList(), unmoved_mesh, numbering_subset, displacement, mapping);
    }

    GlobalVector zero(displacement_field);
    zero.ZeroEntries();

    // Apply a new displacement from initial position - and here this displacement is chosen null...
    felt_space.MoveMeshFromInitialPosition(zero);

    {
        const Wrappers::Petsc::AccessGhostContent access_ghost_content(zero);
        const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> zero_displacement(vector_with_ghost);

        CheckMovemesh(
            mpi, mesh.GetProcessorWiseCoordsList(), unmoved_mesh, numbering_subset, zero_displacement, mapping);

        CheckMovemesh(mpi, mesh.GetGhostCoordsList(), unmoved_mesh, numbering_subset, zero_displacement, mapping);
    }
}


BOOST_AUTO_TEST_CASE(scalar_unknown)
{
    TestNS::ClearSingletons<time_manager_type>::Do();


    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/Movemesh/demo_scalar_unknown.lua") };

    MoReFEMData<MoReFEM::TestNS::MovemeshNS::ModelSettingsForScalarUnknownTest,
                TestNS::MovemeshNS::scalar_unknown_test_input_data_type,
                time_manager_type,
                program_type::test>
        morefem_data{ std::move(lua_file) };

    decltype(auto) unknown_nature = ::MoReFEM::InputDataNS::ExtractLeaf<
        InputDataNS::Unknown<EnumUnderlyingType(TestNS::MovemeshNS::UnknownIndex::displacement)>::Nature>(morefem_data);


    BOOST_CHECK_EQUAL(unknown_nature, "scalar"); // Checking the behaviour of scalar unknown is the whole point of the
                                                 // test so ensure it wasn't changed in the Lua file!

    TestNS::BareModel<decltype(morefem_data)> model(morefem_data);

    BOOST_CHECK_THROW(model.Initialize(), Internal::ExceptionsNS::Movemesh::ScalarUnknown);
}


BOOST_AUTO_TEST_CASE(several_unknown)
{
    TestNS::ClearSingletons<time_manager_type>::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/Movemesh/demo_several_unknowns.lua") };

    MoReFEMData<TestNS::MovemeshNS::ModelSettingsForSeveralUnknownTest,
                TestNS::MovemeshNS::several_unknown_case_input_data_type,
                time_manager_type,
                program_type::test>
        morefem_data{ std::move(lua_file) };

    TestNS::BareModel<decltype(morefem_data)> model(morefem_data);

    BOOST_CHECK_THROW(model.Initialize(), Internal::ExceptionsNS::Movemesh::NumberOfUnknowns);
}


BOOST_AUTO_TEST_SUITE_END()


namespace // anonymous
{


    void FillMappingCoordsDof(const Dof::vector_shared_ptr& dof_list,
                              const NumberingSubset& numbering_subset,
                              mapping_type& mapping)
    {
        for (const auto& dof_ptr : dof_list)
        {
            assert(!(!dof_ptr));

            if (!dof_ptr->IsInNumberingSubset(numbering_subset))
                continue;

            auto node_ptr = dof_ptr->GetNodeFromWeakPtr();
            auto node_bearer_ptr = node_ptr->GetNodeBearerFromWeakPtr();

            decltype(auto) interface = node_bearer_ptr->GetInterface();

            decltype(auto) coords_list = interface.GetCoordsList();

            if (coords_list.size() > 1UL) // #1679 See how to handle properly Q2 when available
                continue;

            decltype(auto) coords_ptr = coords_list.back();

            auto it = mapping.find(coords_ptr);

            if (it == mapping.end())
                mapping.insert({ coords_ptr, Dof::vector_shared_ptr{ dof_ptr } });
            else
            {
                auto& mapping_dof_list = it->second;
                mapping_dof_list.push_back(dof_ptr);
            }
        }
    }


    mapping_type GenerateMappingCoordsDof(const FEltSpace& felt_space, const NumberingSubset& numbering_subset)
    {
        mapping_type ret;
        ret.max_load_factor(Utilities::DefaultMaxLoadFactor());

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();

        decltype(auto) mesh = god_of_dof_ptr->GetMesh();

        FillMappingCoordsDof(felt_space.GetProcessorWiseDofList(), numbering_subset, ret);
        FillMappingCoordsDof(felt_space.GetGhostDofList(), numbering_subset, ret);

        BOOST_CHECK_EQUAL(mesh.NprocessorWiseCoord() + mesh.NghostCoord(), ret.size());

        for (const auto& [coords_ptr, dof_list] : ret)
            BOOST_CHECK_EQUAL(dof_list.size(), static_cast<std::size_t>(mesh.GetDimension().Get()));

        return ret;
    }


    GlobalVector GenerateVectorWithSemiRandomContent(const GodOfDof& god_of_dof,
                                                     const NumberingSubset& numbering_subset)
    {
        GlobalVector vector(numbering_subset);
        AllocateGlobalVector(god_of_dof, vector);

        decltype(auto) mpi = god_of_dof.GetMpi();

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector);

            const auto size = content.GetSize();

            std::random_device random_device;
            std::mt19937 generator(random_device()); // Standard mersenne_twister_engine seeded
            std::uniform_real_distribution<> distribution(0., 0.1);

            const auto rank_contribution = 10. * static_cast<double>(mpi.GetRank<int>());

            for (auto i = vector_processor_wise_index_type{ 0UL }; i < size; ++i)
                content[i] = rank_contribution + distribution(generator);
        }

        return vector;
    }


    Coords::shared_ptr AttemptToFindUnmovedCoords(const Mesh& unmoved_mesh, const Coords& moved_coords)
    {
        // By construct moved mesh and unmoved mesh are built upon the same file, so IndexFromMeshFile is a reliable
        // way to match Coords between both.
        const auto moved_coords_index_from_mesh_file = moved_coords.GetIndexFromMeshFile();

        auto find_unmoved_coords = [moved_coords_index_from_mesh_file](const auto& unmoved_coords_ptr)
        {
            assert(!(!unmoved_coords_ptr));
            return unmoved_coords_ptr->GetIndexFromMeshFile() == moved_coords_index_from_mesh_file;
        };

        {
            decltype(auto) unmoved_processor_wise_coords_list = unmoved_mesh.GetProcessorWiseCoordsList();
            const auto end = unmoved_processor_wise_coords_list.cend();

            auto it = std::find_if(unmoved_processor_wise_coords_list.cbegin(), end, find_unmoved_coords);

            if (it != end)
                return *it;
        }

        {
            decltype(auto) unmoved_ghost_coords_list = unmoved_mesh.GetGhostCoordsList();
            const auto end = unmoved_ghost_coords_list.cend();

            auto it = std::find_if(unmoved_ghost_coords_list.cbegin(), end, find_unmoved_coords);

            if (it != end)
                return *it;
        }

        return nullptr;
    }


    template<class ModelT>
    void CheckMeshesPathAreIdentical(const ModelT& model)
    {
        decltype(auto) morefem_data = model.GetMoReFEMData();

        // Check both mesh files are equals: the point of unmoved is to be a sort of memory of what happens before
        decltype(auto) movable_mesh_path =
            ::MoReFEM::Internal::InputDataNS::ExtractLeaf<InputDataNS::Mesh<EnumUnderlyingType(
                TestNS::MovemeshNS::MeshIndex::movable)>::Path>::Value(morefem_data.GetInputData());

        decltype(auto) unmoved_mesh_path =
            ::MoReFEM::Internal::InputDataNS::ExtractLeaf<InputDataNS::Mesh<EnumUnderlyingType(
                TestNS::MovemeshNS::MeshIndex::unmoved)>::Path>::Value(morefem_data.GetInputData());

        BOOST_REQUIRE_EQUAL(movable_mesh_path, unmoved_mesh_path);
    }


    std::pair<Coords::shared_ptr, Dof::vector_shared_ptr>
    Fetch(const Coords::shared_ptr& movable_coords_ptr, const Mesh& unmoved_mesh, const mapping_type& mapping)
    {
        assert(!(!movable_coords_ptr));
        const auto& coords = *movable_coords_ptr;

        auto it = mapping.find(movable_coords_ptr);
        assert(it != mapping.cend()
               && "If not fulfilled this would mean the GenerateMappingCoordsDof() function "
                  "defined in this test is faulty.");
        decltype(auto) coords_dof_list = it->second;

        // On a short mesh the risk is tiny, but no guarantee the same partition will be applied on both meshes,
        // hence these seemingly convoluted lines.
        decltype(auto) unmoved_coords_ptr = AttemptToFindUnmovedCoords(unmoved_mesh, coords);

        BOOST_CHECK_EQUAL(coords_dof_list.size(), static_cast<std::size_t>(unmoved_mesh.GetDimension().Get()));

        return std::make_pair(unmoved_coords_ptr, coords_dof_list);
    }


    void CheckMovemesh(const Wrappers::Mpi& mpi,
                       const Coords::vector_shared_ptr& moved_coords_list,
                       const Mesh& unmoved_mesh,
                       const NumberingSubset& numbering_subset,
                       const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>& displacement,
                       const mapping_type& mapping)
    {
        const auto dimension = unmoved_mesh.GetDimension();

        for (const auto& coords_ptr : moved_coords_list)
        {
            const auto& coords = *coords_ptr;
            auto [unmoved_coords_ptr, coords_dof_list] = Fetch(coords_ptr, unmoved_mesh, mapping);

            // We accept not to check the few \a Coords that might not be handled by the same processor at all
            // in the unmoved mesh.
            if (unmoved_coords_ptr == nullptr)
                continue;

            const auto& unmoved_coords = *unmoved_coords_ptr;

            BOOST_REQUIRE_EQUAL(coords_dof_list.size(), static_cast<std::size_t>(dimension.Get()));

            for (auto component = ::MoReFEM::GeometryNS::dimension_type{}; component < dimension; ++component)
            {
                BOOST_REQUIRE(coords_dof_list[static_cast<std::size_t>(component.Get())] != nullptr);
                const auto coords_processor_wise_index =
                    coords_dof_list[static_cast<std::size_t>(component.Get())]->GetProcessorWiseOrGhostIndex(
                        numbering_subset);

                BOOST_REQUIRE(DofNS::ToVectorIndex(coords_processor_wise_index) < displacement.GetSize());

                if (!NumericNS::AreEqual(
                        coords[component],
                        unmoved_coords[component]
                            + displacement.GetValue(DofNS::ToVectorIndex(coords_processor_wise_index))))
                {
                    std::ostringstream oconv;

                    oconv << mpi.GetRankPrefix() << " Failure in movemesh: expected position = "
                          << unmoved_coords[component]
                                 + displacement.GetValue(DofNS::ToVectorIndex(coords_processor_wise_index))
                          << " but computed position = " << coords[component] << " for Coords "
                          << coords.GetIndexFromMeshFile()
                          << " (the index from the original mesh file is used here) and component " << component;

                    throw Exception(oconv.str());
                }
            }
        }
    }


} // namespace


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
