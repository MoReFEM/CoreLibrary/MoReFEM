// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>
#include <memory>

#include "Test/FiniteElement/ForUnknownList/Fixture.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Test/FiniteElement/ForUnknownList/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::ForUnknownListNS
{


    Fixture::Fixture()
    {
        decltype(auto) model = GetModel();


        // Set Mesh, and extract three geometric elements which will be used for tests.
        {
            decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
            decltype(auto) tetra_mesh = mesh_manager.GetMesh(AsMeshId(ForUnknownListNS::MeshIndex::tetra));
            decltype(auto) tri_mesh = mesh_manager.GetMesh(AsMeshId(ForUnknownListNS::MeshIndex::tri));

            decltype(auto) geom_elt_factory = Advanced::GeometricEltFactory::GetInstance();

            {
                decltype(auto) ref_tetra4 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Tetrahedron4);
                auto [begin, end] = tetra_mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_tetra4);
                assert(end - begin == 1);
                tetra_in_3d_mesh_ = *begin;
            }

            {
                decltype(auto) ref_tri6 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);
                auto [begin, end] = tetra_mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_tri6);
                assert(end != begin);
                triangle_in_3d_mesh_ = *begin;
            }

            {
                decltype(auto) ref_seg2 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Segment2);
                auto [begin, end] = tetra_mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_seg2);
                assert(end != begin);
                segment_in_3d_mesh_ = *begin;
            }

            {
                decltype(auto) ref_tri6 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);
                auto [begin, end] = tri_mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_tri6);
                assert(end != begin);
                triangle_in_2d_mesh_ = *begin;
            }

            {
                decltype(auto) ref_seg2 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Segment2);
                auto [begin, end] = tri_mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_seg2);
                assert(end != begin);
                segment_in_2d_mesh_ = *begin;
            }
        }

        quadrature_rule_per_topology_ = std::make_unique<QuadratureRulePerTopology>(3, 2);


        // Ensure the singletons are created!
        {
            [[maybe_unused]] decltype(auto) domain_manager = DomainManager::GetInstance();

            {
                decltype(auto) unknown_manager = UnknownManager::GetInstance();
                [[maybe_unused]] decltype(auto) unknown =
                    unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));
            }

            {
                decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::tetra));
                [[maybe_unused]] decltype(auto) felt_space =
                    god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::tetra_dim3));
            }

            {
                decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::tri));
                [[maybe_unused]] decltype(auto) felt_space =
                    god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::tri_dim2));
            }
        }

        time_manager_ = std::make_unique<TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>>();
    }


    const GeometricElt& Fixture::GetTetrahedronIn3DMesh() const noexcept
    {
        assert(!(!tetra_in_3d_mesh_));
        return *tetra_in_3d_mesh_;
    }


    const GeometricElt& Fixture::GetTriangleIn3DMesh() const noexcept
    {
        assert(!(!triangle_in_3d_mesh_));
        return *triangle_in_3d_mesh_;
    }


    const GeometricElt& Fixture::GetSegmentIn3DMesh() const noexcept
    {
        assert(!(!segment_in_3d_mesh_));
        return *segment_in_3d_mesh_;
    }


    const GeometricElt& Fixture::GetTriangleIn2DMesh() const noexcept
    {
        assert(!(!triangle_in_2d_mesh_));
        return *triangle_in_2d_mesh_;
    }


    const GeometricElt& Fixture::GetSegmentIn2DMesh() const noexcept
    {
        assert(!(!segment_in_2d_mesh_));
        return *segment_in_2d_mesh_;
    }


    const QuadratureRulePerTopology& Fixture::GetQuadratureRulePerTopology() const noexcept
    {
        assert(!(!quadrature_rule_per_topology_));
        return *quadrature_rule_per_topology_;
    }


    const QuadraturePoint& Fixture::GetFirstQuadraturePoint(const GeometricElt& geom_elt) const noexcept
    {
        decltype(auto) quadrature_rule_per_topology = GetQuadratureRulePerTopology();
        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());
        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();
        assert(!quad_pt_list.empty());
        assert(!(!quad_pt_list[0]));
        return *quad_pt_list[0];
    }


} // namespace MoReFEM::TestNS::ForUnknownListNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
