// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/FiniteElement/ForUnknownList/ModelSettings.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::ForUnknownListNS
{


    void ModelSettings::Init()
    {
        // ****** Mesh ******
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tetra)>>({ "Tetra mesh" });

        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tetra)>::Path>("${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tetra)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tetra)>::Dimension>(3);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tetra)>::SpaceUnit>(1.);

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tri)>>({ "Triangle mesh" });

        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tri)>::Path>("${MOREFEM_ROOT}/Data/Mesh/one_triangle.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tri)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tri)>::Dimension>(2);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tri)>::SpaceUnit>(1.);


        // ****** Domain ******
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim3)>>(
            { "Dimension 3 elements of the tetra mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim3)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::tetra) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim3)>::DimensionList>({ 3 });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim3)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim3)>::GeomEltTypeList>({});

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim2)>>(
            { "Dimension 2 elements of the tetra mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim2)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::tetra) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim2)>::DimensionList>({ 2 });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim2)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim2)>::GeomEltTypeList>({});

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim1)>>(
            { "Dimension 1 elements of the tetra mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim1)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::tetra) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim1)>::DimensionList>({ 1 });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim1)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tetra_dim1)>::GeomEltTypeList>({});

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim2)>>(
            { "Dimension 2 elements of the tri mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim2)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::tri) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim2)>::DimensionList>({ 2 });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim2)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim2)>::GeomEltTypeList>({});

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim1)>>(
            { "Dimension 1 elements of the tri mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim1)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::tri) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim1)>::DimensionList>({ 1 });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim1)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::tri_dim1)>::GeomEltTypeList>({});

        // ****** Unknown ******
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>("Solid displacement");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Name>("displacement");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Nature>("vectorial");

        // ****** Numbering subset ******
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::tetra)>>(
            "Numbering subset in tetra");
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::tri)>>(
            "Numbering subset in tri");


        // ****** Finite element space ******
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim3)>>(
            "Finite element space - tetra mesh - dim 3");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim3)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::tetra));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim3)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim3)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::tetra_dim3));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim3)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::tetra) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim3)>::ShapeFunctionList>({ "P2" });

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim2)>>(
            "Finite element space - tetra mesh - dim 2");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim2)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::tetra));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim2)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim2)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::tetra_dim2));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim2)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::tetra) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim2)>::ShapeFunctionList>({ "P2" });

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim1)>>(
            "Finite element space - tetra mesh - dim 1");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim1)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::tetra));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim1)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim1)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::tetra_dim1));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim1)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::tetra) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tetra_dim1)>::ShapeFunctionList>({ "P2" });

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim2)>>(
            "Finite element space - tri mesh - dim 2");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim2)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::tri));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim2)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim2)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::tri_dim2));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim2)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::tri) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim2)>::ShapeFunctionList>({ "P2" });

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim1)>>(
            "Finite element space - tri mesh - dim 1");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim1)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::tri));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim1)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim1)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::tri_dim1));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim1)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::tri) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::tri_dim1)>::ShapeFunctionList>({ "P2" });
    }


} // namespace MoReFEM::TestNS::ForUnknownListNS

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
