// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_FINITEELEMENT_FORUNKNOWNLIST_MODELSETTINGS_DOT_HPP_
#define MOREFEM_TEST_FINITEELEMENT_FORUNKNOWNLIST_MODELSETTINGS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Utilities/InputData/SectionsAndLeaves.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::ForUnknownListNS
{

    //! Enum class
    enum class MeshIndex : std::size_t { tetra, tri };

    //! Enum class
    enum class DomainIndex : std::size_t { tetra_dim3, tetra_dim2, tetra_dim1, tri_dim2, tri_dim1 };

    //! Enum class
    enum class FEltSpaceIndex : std::size_t { tetra_dim3, tetra_dim2, tetra_dim1, tri_dim2, tri_dim1 };

    //! Enum class
    enum class UnknownIndex : std::size_t { displacement };

    //! Enum class
    enum class NumberingSubsetIndex : std::size_t { tetra, tri };


    //! \brief Tuple with content for a \a ModelSettings instance.
    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple = std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::tetra),
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tetra)>,
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::tri),
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::tri)>,

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::tetra),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::tri),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::tetra_dim3),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::tetra_dim3),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::tetra_dim2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::tetra_dim2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::tetra_dim1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::tetra_dim1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::tri_dim2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::tri_dim2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::tri_dim1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::tri_dim1),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tetra_dim1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tetra_dim1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tetra_dim2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tetra_dim2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tetra_dim3),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tetra_dim3),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tri_dim1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tri_dim1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tri_dim2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::tri_dim2),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::displacement)
    >;



    //! \copydoc doxygen_hide_model_specific_input_data
    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


} // namespace MoReFEM::TestNS::ForUnknownListNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_FINITEELEMENT_FORUNKNOWNLIST_MODELSETTINGS_DOT_HPP_
// *** MoReFEM end header guards *** < //

