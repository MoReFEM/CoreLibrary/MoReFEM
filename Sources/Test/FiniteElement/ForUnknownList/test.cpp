// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <variant>

#include "Utilities/Filesystem/Directory.hpp"

#define BOOST_TEST_MODULE for_unknown_list

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"

#include "Test/FiniteElement/ForUnknownList/Fixture.hpp"
#include "Test/FiniteElement/ForUnknownList/ModelSettings.hpp"
#include "Test/Tools/PredicateEigen.hpp"

using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // #__clang__


namespace // anonymous
{
    constexpr auto one_third = 1. / 3.;
    constexpr auto one_sixth = 1. / 6.;
    constexpr auto one_ninth = 1. / 9.;

    constexpr auto epsilon = 1.e-14;
} // namespace

namespace MoReFEM::TestNS
{

    struct AccessForUnknownList
    {
        explicit AccessForUnknownList(::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& list);

        Advanced::GeomEltNS::ComputeJacobian& GetNonCstComputeJacobianHelper();

      private:
        ::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& list_;
    };

} // namespace MoReFEM::TestNS


BOOST_FIXTURE_TEST_CASE(tetrahedron_in_3d_mesh, TestNS::ForUnknownListNS::Fixture)
{
    decltype(auto) geom_elt = GetTetrahedronIn3DMesh();
    decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ForUnknownListNS::DomainIndex::tetra_dim3));

    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::ForUnknownListNS::MeshIndex::tetra));

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(TestNS::ForUnknownListNS::FEltSpaceIndex::tetra_dim3));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) displacement =
        unknown_manager.GetUnknownPtr(AsUnknownId(TestNS::ForUnknownListNS::UnknownIndex::displacement));

    auto mass_operator =
        GlobalVariationalOperatorNS::Mass(felt_space, displacement, displacement, &GetQuadratureRulePerTopology());

    auto& local_operator = mass_operator.ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Tetrahedron4>(domain);

    Advanced::InfosAtQuadPointNS::ForUnknownList for_unknown_list(quad_pt,
                                                                  ref_geom_elt,
                                                                  local_operator.GetElementaryData().GetRefFEltList(),
                                                                  ::MoReFEM::GeometryNS::dimension_type{ 3 },
                                                                  AllocateGradientFEltPhi::yes);


    {
        BOOST_CHECK_EQUAL(for_unknown_list.Nnode(), 10);
        BOOST_CHECK_EQUAL(for_unknown_list.GetMeshDimension(), ::MoReFEM::GeometryNS::dimension_type{ 3 });
        BOOST_CHECK_EQUAL(for_unknown_list.GetQuadraturePoint().GetIndex(), quad_pt.GetIndex());
    }


    {
        decltype(auto) ref_geom_elt_phi = for_unknown_list.GetRefGeometricPhi();
        const Eigen::Vector4d expected{ 0.5, one_sixth, one_sixth, one_sixth };
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                              (ref_geom_elt_phi)(expected)(epsilon));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(0))(0.5)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(1))(one_sixth)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(2))(one_sixth)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(3))(one_sixth)(epsilon));
    }

    {
        decltype(auto) gradient = for_unknown_list.GetGradientRefGeometricPhi();
        Eigen::Matrix<double, 4, 3> expected;

        // clang-format off
        expected << -1., -1., -1,
                     1. , 0., 0.,
                     0., 1., 0.,
                     0., 0., 1.;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::MatrixXd>, (gradient)(expected)(epsilon));
    }

    {
        decltype(auto) ref_felt_phi = for_unknown_list.GetRefFEltPhi();
        decltype(auto) felt_phi = for_unknown_list.GetFEltPhi();

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::MatrixXd>,
                              (ref_felt_phi)(felt_phi)(epsilon));

        Eigen::Matrix<double, 10, 1> expected;
        expected << 0., -one_ninth, -one_ninth, -one_ninth, one_third, one_ninth, one_third, one_third, one_ninth,
            one_ninth;

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::MatrixXd>,
                              (ref_felt_phi)(expected)(epsilon));
    }

    decltype(auto) local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

    {
        for_unknown_list.ComputeLocalFEltSpaceData(local_felt_space);

        TestNS::AccessForUnknownList helper(for_unknown_list);

        decltype(auto) compute_jacobian = helper.GetNonCstComputeJacobianHelper();

        // This was actually already done in ComputeLocalFEltSpaceData(), but no interface to access it outside
        // of this compute method, and it would be stupid to define it just for the sake of current test!
        decltype(auto) jacobian = compute_jacobian.Compute(geom_elt, quad_pt);

        using jacobian_type = Eigen::Matrix<double, 3, 3>;

        BOOST_REQUIRE(std::holds_alternative<jacobian_type>(jacobian));

        BOOST_CHECK_PREDICATE(
            MoReFEM::NumericNS::AreEqual<double>,
            (for_unknown_list.GetJacobianDeterminant())(std::get<jacobian_type>(jacobian).determinant())(epsilon));


        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                              (for_unknown_list.GetJacobianDeterminant())(1.)(epsilon));

        decltype(auto) grad_felt_phi = for_unknown_list.GetGradientFEltPhi();

        using matrix_type = Eigen::Matrix<double, 10, 3>;
        matrix_type expected;

        // clang-format off
        expected << -1., -1., -1,
                    -one_third, 0., 0.,
                    0., -one_third, 0.,
                    0., 0., -one_third,
                    1. + one_third, -1. + one_third, -1. + one_third,
                    1. - one_third, 1. - one_third, 0.,
                    -1. + one_third, 1. + one_third, -1. + one_third,
                    -1. + one_third, -1. + one_third, 1. + one_third,
                    1. - one_third, 0., 1. - one_third,
                    0., 1. - one_third, 1. - one_third;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<matrix_type>,
                              (grad_felt_phi)(expected)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(tetrahedron_in_3d_mesh_no_gradient_allocated, TestNS::ForUnknownListNS::Fixture)
{


    decltype(auto) geom_elt = GetTetrahedronIn3DMesh();
    decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ForUnknownListNS::DomainIndex::tetra_dim3));

    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::ForUnknownListNS::MeshIndex::tetra));

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(TestNS::ForUnknownListNS::FEltSpaceIndex::tetra_dim3));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) displacement =
        unknown_manager.GetUnknownPtr(AsUnknownId(TestNS::ForUnknownListNS::UnknownIndex::displacement));

    auto mass_operator =
        GlobalVariationalOperatorNS::Mass(felt_space, displacement, displacement, &GetQuadratureRulePerTopology());

    auto& local_operator = mass_operator.ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Tetrahedron4>(domain);

    const Advanced::InfosAtQuadPointNS::ForUnknownList for_unknown_list(
        quad_pt,
        ref_geom_elt,
        local_operator.GetElementaryData().GetRefFEltList(),
        ::MoReFEM::GeometryNS::dimension_type{ 3 },
        AllocateGradientFEltPhi::no);


    {
        BOOST_CHECK_EQUAL(for_unknown_list.Nnode(), 10);
        BOOST_CHECK_EQUAL(for_unknown_list.GetMeshDimension(), ::MoReFEM::GeometryNS::dimension_type{ 3 });
        BOOST_CHECK_EQUAL(for_unknown_list.GetQuadraturePoint().GetIndex(), quad_pt.GetIndex());
    }


    {
        decltype(auto) ref_geom_elt_phi = for_unknown_list.GetRefGeometricPhi();
        const Eigen::Vector4d expected{ 0.5, one_sixth, one_sixth, one_sixth };
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                              (ref_geom_elt_phi)(expected)(epsilon));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(0))(0.5)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(1))(one_sixth)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(2))(one_sixth)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(3))(one_sixth)(epsilon));
    }

    {
        decltype(auto) gradient = for_unknown_list.GetGradientRefGeometricPhi();
        Eigen::Matrix<double, 4, 3> expected;

        // clang-format off
        expected << -1., -1., -1,
                     1. , 0., 0.,
                     0., 1., 0.,
                     0., 0., 1.;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::MatrixXd>, (gradient)(expected)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(triangle_in_3d_mesh, TestNS::ForUnknownListNS::Fixture)
{
    decltype(auto) geom_elt = GetTriangleIn3DMesh();
    decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ForUnknownListNS::DomainIndex::tetra_dim2));

    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::ForUnknownListNS::MeshIndex::tetra));

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(TestNS::ForUnknownListNS::FEltSpaceIndex::tetra_dim2));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) displacement =
        unknown_manager.GetUnknownPtr(AsUnknownId(TestNS::ForUnknownListNS::UnknownIndex::displacement));

    auto mass_operator =
        GlobalVariationalOperatorNS::Mass(felt_space, displacement, displacement, &GetQuadratureRulePerTopology());

    auto& local_operator = mass_operator.ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Triangle3>(domain);

    Advanced::InfosAtQuadPointNS::ForUnknownList for_unknown_list(quad_pt,
                                                                  ref_geom_elt,
                                                                  local_operator.GetElementaryData().GetRefFEltList(),
                                                                  ::MoReFEM::GeometryNS::dimension_type{ 3 },
                                                                  AllocateGradientFEltPhi::yes);

    {
        BOOST_CHECK_EQUAL(for_unknown_list.Nnode(), 6);
        BOOST_CHECK_EQUAL(for_unknown_list.GetMeshDimension(), ::MoReFEM::GeometryNS::dimension_type{ 3 });
        BOOST_CHECK_EQUAL(for_unknown_list.GetQuadraturePoint().GetIndex(), quad_pt.GetIndex());
    }


    {
        decltype(auto) ref_geom_elt_phi = for_unknown_list.GetRefGeometricPhi();

        const Eigen::Vector3d expected{ 0.2, 0.6, 0.2 };
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                              (ref_geom_elt_phi)(expected)(epsilon));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(0))(0.2)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(1))(0.6)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(2))(0.2)(epsilon));
    }

    {
        decltype(auto) gradient = for_unknown_list.GetGradientRefGeometricPhi();

        Eigen::Matrix<double, 3, 2> expected;

        // clang-format off
        expected << -1., -1.,
                     1. , 0.,
                     0., 1.;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::MatrixXd>, (gradient)(expected)(epsilon));
    }

    {
        decltype(auto) ref_felt_phi = for_unknown_list.GetRefFEltPhi();
        decltype(auto) felt_phi = for_unknown_list.GetFEltPhi();

        using vector_type = Wrappers::EigenNS::VectorNd<6>;

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(felt_phi)(epsilon));

        vector_type expected;
        expected << -0.12, 0.12, -0.12, 0.48, 0.48, 0.16;

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(expected)(epsilon));
    }

    decltype(auto) local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

    {
        for_unknown_list.ComputeLocalFEltSpaceData(local_felt_space);

        TestNS::AccessForUnknownList helper(for_unknown_list);

        decltype(auto) compute_jacobian = helper.GetNonCstComputeJacobianHelper();

        // This was actually already done in ComputeLocalFEltSpaceData(), but no interface to access it outside
        // of this compute method, and it would be stupid to define it just for the sake of current test!
        decltype(auto) jacobian = compute_jacobian.Compute(geom_elt, quad_pt);

        using jacobian_type = Eigen::Matrix<double, 3, 3>;
        BOOST_REQUIRE(std::holds_alternative<jacobian_type>(jacobian));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                              (for_unknown_list.GetJacobianDeterminant())(1.)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(segment_in_3d_mesh, TestNS::ForUnknownListNS::Fixture)
{

    decltype(auto) geom_elt = GetSegmentIn3DMesh();
    decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ForUnknownListNS::DomainIndex::tetra_dim1));

    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::ForUnknownListNS::MeshIndex::tetra));

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(TestNS::ForUnknownListNS::FEltSpaceIndex::tetra_dim1));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) displacement =
        unknown_manager.GetUnknownPtr(AsUnknownId(TestNS::ForUnknownListNS::UnknownIndex::displacement));

    auto mass_operator =
        GlobalVariationalOperatorNS::Mass(felt_space, displacement, displacement, &GetQuadratureRulePerTopology());

    auto& local_operator = mass_operator.ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Segment2>(domain);

    Advanced::InfosAtQuadPointNS::ForUnknownList for_unknown_list(quad_pt,
                                                                  ref_geom_elt,
                                                                  local_operator.GetElementaryData().GetRefFEltList(),
                                                                  ::MoReFEM::GeometryNS::dimension_type{ 3 },
                                                                  AllocateGradientFEltPhi::yes);


    {
        BOOST_CHECK_EQUAL(for_unknown_list.Nnode(), 3);
        BOOST_CHECK_EQUAL(for_unknown_list.GetMeshDimension(), ::MoReFEM::GeometryNS::dimension_type{ 3 });
        BOOST_CHECK_EQUAL(for_unknown_list.GetQuadraturePoint().GetIndex(), quad_pt.GetIndex());
    }


    {
        decltype(auto) ref_geom_elt_phi = for_unknown_list.GetRefGeometricPhi();

        const Eigen::Vector2d expected{ 0.887298334620742, 0.112701665379258 };
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                              (ref_geom_elt_phi)(expected)(epsilon));
    }

    {
        decltype(auto) gradient = for_unknown_list.GetGradientRefGeometricPhi();

        Eigen::Matrix<double, 2, 1> expected;

        // clang-format off
        expected << -0.5, 0.5;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::MatrixXd>, (gradient)(expected)(epsilon));
    }

    {
        decltype(auto) ref_felt_phi = for_unknown_list.GetRefFEltPhi();
        decltype(auto) felt_phi = for_unknown_list.GetFEltPhi();

        using vector_type = Eigen::Vector3d;

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(felt_phi)(epsilon));


        vector_type expected;
        expected << 0.687298334620742, -0.0872983346207417, 0.4;
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(expected)(epsilon));
    }

    decltype(auto) local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

    {
        for_unknown_list.ComputeLocalFEltSpaceData(local_felt_space);

        TestNS::AccessForUnknownList helper(for_unknown_list);
        decltype(auto) compute_jacobian = helper.GetNonCstComputeJacobianHelper();

        // This was actually already done in ComputeLocalFEltSpaceData(), but no interface to access it outside
        // of this compute method, and it would be stupid to define it just for the sake of current test!
        decltype(auto) jacobian = compute_jacobian.Compute(geom_elt, quad_pt);

        using jacobian_type = Eigen::Matrix<double, 3, 3>;
        BOOST_REQUIRE(std::holds_alternative<jacobian_type>(jacobian));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                              (for_unknown_list.GetJacobianDeterminant())(0.5)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(triangle_in_2d_mesh, TestNS::ForUnknownListNS::Fixture)
{

    decltype(auto) geom_elt = GetTriangleIn2DMesh();
    decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ForUnknownListNS::DomainIndex::tri_dim2));

    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::ForUnknownListNS::MeshIndex::tri));

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(TestNS::ForUnknownListNS::FEltSpaceIndex::tri_dim2));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) displacement =
        unknown_manager.GetUnknownPtr(AsUnknownId(TestNS::ForUnknownListNS::UnknownIndex::displacement));

    auto mass_operator =
        GlobalVariationalOperatorNS::Mass(felt_space, displacement, displacement, &GetQuadratureRulePerTopology());

    auto& local_operator = mass_operator.ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Triangle3>(domain);

    Advanced::InfosAtQuadPointNS::ForUnknownList for_unknown_list(quad_pt,
                                                                  ref_geom_elt,
                                                                  local_operator.GetElementaryData().GetRefFEltList(),
                                                                  ::MoReFEM::GeometryNS::dimension_type{ 2 },
                                                                  AllocateGradientFEltPhi::yes);

    {
        BOOST_CHECK_EQUAL(for_unknown_list.Nnode(), 6);
        BOOST_CHECK_EQUAL(for_unknown_list.GetMeshDimension(), ::MoReFEM::GeometryNS::dimension_type{ 2 });
        BOOST_CHECK_EQUAL(for_unknown_list.GetQuadraturePoint().GetIndex(), quad_pt.GetIndex());
    }


    {
        decltype(auto) ref_geom_elt_phi = for_unknown_list.GetRefGeometricPhi();

        const Eigen::Vector3d expected{ 0.2, 0.6, 0.2 };
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::Vector3d>,
                              (ref_geom_elt_phi)(expected)(epsilon));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(0))(0.2)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(1))(0.6)(epsilon));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (ref_geom_elt_phi(2))(0.2)(epsilon));
    }

    {
        decltype(auto) gradient = for_unknown_list.GetGradientRefGeometricPhi();
        using matrix_type = Eigen::Matrix<double, 3, 2>;
        matrix_type expected;

        // clang-format off
        expected << -1., -1.,
                     1. , 0.,
                     0., 1.;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<matrix_type>, (gradient)(expected)(epsilon));
    }

    {
        using vector_type = Wrappers::EigenNS::VectorNd<6>;

        decltype(auto) ref_felt_phi = for_unknown_list.GetRefFEltPhi();
        decltype(auto) felt_phi = for_unknown_list.GetFEltPhi();

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(felt_phi)(epsilon));

        vector_type expected;
        expected << -0.12, 0.12, -0.12, 0.48, 0.48, 0.16;

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(expected)(epsilon));
    }

    decltype(auto) local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

    {
        for_unknown_list.ComputeLocalFEltSpaceData(local_felt_space);

        TestNS::AccessForUnknownList helper(for_unknown_list);

        decltype(auto) compute_jacobian = helper.GetNonCstComputeJacobianHelper();

        // This was actually already done in ComputeLocalFEltSpaceData(), but no interface to access it outside
        // of this compute method, and it would be stupid to define it just for the sake of current test!
        decltype(auto) jacobian = compute_jacobian.Compute(geom_elt, quad_pt);

        using jacobian_type = Eigen::Matrix2d;
        BOOST_REQUIRE(std::holds_alternative<jacobian_type>(jacobian));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                              (for_unknown_list.GetJacobianDeterminant())(3.38)(epsilon));

        decltype(auto) grad_felt_phi = for_unknown_list.GetGradientFEltPhi();

        using matrix_type = Eigen::Matrix<double, 6, 2>;
        matrix_type expected;

        // clang-format off
        expected << 0.272189349112426,  0.0236686390532545,
             2.07100591715976,  -0.124260355029586,
           0.0236686390532544, -0.0414201183431953,
            -2.08284023668639,  -0.355029585798817,
            0.899408284023669,    0.42603550295858,
            -1.18343195266272,  0.0710059171597633;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<matrix_type>,
                              (grad_felt_phi)(expected)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(segment_in_2d_mesh, TestNS::ForUnknownListNS::Fixture)
{

    decltype(auto) geom_elt = GetSegmentIn2DMesh();
    decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ForUnknownListNS::DomainIndex::tri_dim1));

    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::ForUnknownListNS::MeshIndex::tri));

    decltype(auto) felt_space =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(TestNS::ForUnknownListNS::FEltSpaceIndex::tri_dim1));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) displacement =
        unknown_manager.GetUnknownPtr(AsUnknownId(TestNS::ForUnknownListNS::UnknownIndex::displacement));

    auto mass_operator =
        GlobalVariationalOperatorNS::Mass(felt_space, displacement, displacement, &GetQuadratureRulePerTopology());

    auto& local_operator = mass_operator.ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Segment2>(domain);

    Advanced::InfosAtQuadPointNS::ForUnknownList for_unknown_list(quad_pt,
                                                                  ref_geom_elt,
                                                                  local_operator.GetElementaryData().GetRefFEltList(),
                                                                  ::MoReFEM::GeometryNS::dimension_type{ 2 },
                                                                  AllocateGradientFEltPhi::yes);

    {
        BOOST_CHECK_EQUAL(for_unknown_list.Nnode(), 3);
        BOOST_CHECK_EQUAL(for_unknown_list.GetMeshDimension(), ::MoReFEM::GeometryNS::dimension_type{ 2 });
        BOOST_CHECK_EQUAL(for_unknown_list.GetQuadraturePoint().GetIndex(), quad_pt.GetIndex());
    }


    {
        decltype(auto) ref_geom_elt_phi = for_unknown_list.GetRefGeometricPhi();

        const Eigen::Vector2d expected{ 0.887298334620742, 0.112701665379258 };
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::Vector2d>,
                              (ref_geom_elt_phi)(expected)(epsilon));
    }

    {
        decltype(auto) gradient = for_unknown_list.GetGradientRefGeometricPhi();
        using matrix_type = Eigen::Matrix<double, 2, 1>;
        matrix_type expected;

        // clang-format off
        expected << -0.5, 0.5;
        // clang-format on

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<matrix_type>, (gradient)(expected)(epsilon));
    }

    {
        using vector_type = Eigen::Vector3d;
        decltype(auto) ref_felt_phi = for_unknown_list.GetRefFEltPhi();
        decltype(auto) felt_phi = for_unknown_list.GetFEltPhi();

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(felt_phi)(epsilon));

        vector_type expected;
        expected << 0.687298334620742, -0.0872983346207417, 0.4;
        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<vector_type>, (ref_felt_phi)(expected)(epsilon));
    }

    decltype(auto) local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

    {
        for_unknown_list.ComputeLocalFEltSpaceData(local_felt_space);

        TestNS::AccessForUnknownList helper(for_unknown_list);
        decltype(auto) compute_jacobian = helper.GetNonCstComputeJacobianHelper();

        // This was actually already done in ComputeLocalFEltSpaceData(), but no interface to access it outside
        // of this compute method, and it would be stupid to define it just for the sake of current test!
        decltype(auto) jacobian = compute_jacobian.Compute(geom_elt, quad_pt);

        using jacobian_type = Eigen::Matrix2d;
        BOOST_REQUIRE(std::holds_alternative<jacobian_type>(jacobian));

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                              (for_unknown_list.GetJacobianDeterminant())(2.3086792761230388)(epsilon));
    }
}


namespace MoReFEM::TestNS
{

    AccessForUnknownList::AccessForUnknownList(::MoReFEM::Advanced::InfosAtQuadPointNS::ForUnknownList& list)
    : list_{ list }
    { }


    Advanced::GeomEltNS::ComputeJacobian& AccessForUnknownList::GetNonCstComputeJacobianHelper()
    {
        decltype(auto) helper = list_.GetNonCstComputeHelper();


        return std::visit(
            [](auto& variant) -> Advanced::GeomEltNS::ComputeJacobian&
            {
                return variant.GetNonCstComputeJacobianHelper();
            },
            helper);
    }

} // namespace MoReFEM::TestNS


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
