// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_GEOMETRY_COLORING_INPUTDATA_DOT_HPP_
#define MOREFEM_TEST_GEOMETRY_COLORING_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::TestNS::ColoringNS
{


    //! Default value for some input datum that are required by a MoReFEM model but are actually unused for
    //! current test.
    enum class Unused { value = 1 };


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! Enum used to index numbering subsets, domains and finite element spaces.
    enum class DimensionIndex { Dim1 = 1, Dim2 = 2 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <


        InputDataNS::Unknown<EnumUnderlyingType(Unused::value)>,

        InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim1)>,
        InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim2)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim1)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim2)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim1)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim2)>::IndexedSectionDescription,

        InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim1)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim2)>::IndexedSectionDescription,

        InputDataNS::Unknown<EnumUnderlyingType(Unused::value)>::IndexedSectionDescription,

        InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim1)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim2)>::IndexedSectionDescription,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription

    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::test>;

} // namespace MoReFEM::TestNS::ColoringNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_GEOMETRY_COLORING_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
