// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Geometry/CoordsInParallel/InputData.hpp"


namespace MoReFEM::TestNS::CoordsInParallelNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh1)>>(
            { "Finite element space for mesh 1" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh2)>>(
            { "Finite element space for mesh 2" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh1)>>({ "For mesh 1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh2)>>({ "For mesh 2" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>>({ "Sole" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh1)>>({ "For mesh 1" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh2)>>({ "For mesh 2" });

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh1)>>({ "For mesh 1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh2)>>({ "For mesh 2" });
    }


} // namespace MoReFEM::TestNS::CoordsInParallelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
