// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#define BOOST_TEST_MODULE coords_in_parallel
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Geometry/CoordsInParallel/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    using model_type = TestNS::BareModel<MoReFEM::TestNS::CoordsInParallelNS::morefem_data_type>;

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


} // namespace


BOOST_FIXTURE_TEST_CASE(count_coords_in_parallel, fixture_type)
{

#ifndef NDEBUG

    decltype(auto) model = GetModel();

    decltype(auto) mesh1 = model.GetMesh(AsMeshId(TestNS::CoordsInParallelNS::MeshIndex::mesh1));
    decltype(auto) mesh2 = model.GetMesh(AsMeshId(TestNS::CoordsInParallelNS::MeshIndex::mesh2));

    const auto Nmesh1_coords = mesh1.NprocessorWiseCoord() + mesh1.NghostCoord();
    const auto Nmesh2_coords = mesh2.NprocessorWiseCoord() + mesh2.NghostCoord();

    const auto Ncoords_in_meshes = Nmesh1_coords + Nmesh2_coords;

    BOOST_CHECK_EQUAL(Ncoords_in_meshes, Coords::Nobjects());

#endif // NDEBUG
}

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
