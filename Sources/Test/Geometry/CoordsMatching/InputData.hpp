// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_GEOMETRY_COORDSMATCHING_INPUTDATA_DOT_HPP_
#define MOREFEM_TEST_GEOMETRY_COORDSMATCHING_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/InputData/InputData.hpp"
#include "Utilities/InputData/ModelSettings.hpp"

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Geometry/CoordsMatchingFile.hpp"
// IWYU pragma: end_exports

#include "Core/MoReFEMData/Enum.hpp"


namespace MoReFEM::TestNS::CoordsMatchingNS
{


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::CoordsMatchingFile<1>
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::CoordsMatchingFile<1>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


} // namespace MoReFEM::TestNS::CoordsMatchingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_GEOMETRY_COORDSMATCHING_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
