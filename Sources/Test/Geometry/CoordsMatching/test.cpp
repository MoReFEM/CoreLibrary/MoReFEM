// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <exception>
#include <memory>
#include <utility>

#define BOOST_TEST_MODULE coords_matching
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Interpolator/CoordsMatching.hpp"

#include "Test/Geometry/CoordsMatching/InputData.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;


#include "Test/Geometry/CoordsMatching/AnonymousTestData.hpp"

namespace // anonymous
{

    constexpr auto mesh_ids = std::make_pair(MeshNS::unique_id{ 1UL }, MeshNS::unique_id{ 42UL });


} // namespace


BOOST_FIXTURE_TEST_SUITE(coords_matching, Fixture)


BOOST_AUTO_TEST_CASE(ok_case)
{
    TestData generated_data("ok_case", mesh_ids, { 1, 2, 3, 4, 5 }, { 11, 12, 13, 14, 15 });

    const TestNS::CoordsMatchingNS::input_data_type input_data(TestNS::EmptyModelSettings(),
                                                               generated_data.GetLuaFile());

    auto coords_matching_ptr = generated_data.GenerateCoordsMatching();
    const auto& coords_matching = *coords_matching_ptr;

    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 11 }),
                      CoordsNS::index_from_mesh_file{ 1 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 12 }),
                      CoordsNS::index_from_mesh_file{ 2 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 13 }),
                      CoordsNS::index_from_mesh_file{ 3 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 14 }),
                      CoordsNS::index_from_mesh_file{ 4 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 15 }),
                      CoordsNS::index_from_mesh_file{ 5 });

    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 1 }),
                      CoordsNS::index_from_mesh_file{ 11 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 2 }),
                      CoordsNS::index_from_mesh_file{ 12 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 3 }),
                      CoordsNS::index_from_mesh_file{ 13 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 4 }),
                      CoordsNS::index_from_mesh_file{ 14 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 5 }),
                      CoordsNS::index_from_mesh_file{ 15 });

    BOOST_CHECK_EQUAL(coords_matching.GetSourceMeshId(), mesh_ids.first);
    BOOST_CHECK_EQUAL(coords_matching.GetTargetMeshId(), mesh_ids.second);
}

BOOST_AUTO_TEST_CASE(repeated_target_value)
{
    TestData generated_data("repeated_target_value", mesh_ids, { 1, 2, 3, 4, 5 }, { 11, 11, 13, 14, 15 });

    const TestNS::CoordsMatchingNS::input_data_type input_data(TestNS::EmptyModelSettings(),
                                                               generated_data.GetLuaFile());

    MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = generated_data.GenerateCoordsMatching(), std::exception);
}

BOOST_AUTO_TEST_CASE(repeated_source_value)
{
    TestData generated_data("repeated_source_value", mesh_ids, { 1, 1, 3, 4, 5 }, { 11, 12, 13, 14, 15 });

    const TestNS::CoordsMatchingNS::input_data_type input_data(TestNS::EmptyModelSettings(),
                                                               generated_data.GetLuaFile());

    MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = generated_data.GenerateCoordsMatching(), std::exception);
}


BOOST_AUTO_TEST_CASE(imbalanced_data)
{
    TestData generated_data("imbalanced_data", mesh_ids, { 1, 2, 3 }, { 11, 12 });

    const TestNS::CoordsMatchingNS::input_data_type input_data(TestNS::EmptyModelSettings(),
                                                               generated_data.GetLuaFile());

    MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = generated_data.GenerateCoordsMatching(), std::exception);
}


BOOST_AUTO_TEST_CASE(missing_meshes_line)
{
    TestData generated_data(
        "missing_meshes_line", mesh_ids, { 1, 2, 3 }, { 11, 12, 13 }, compute_reverse::no, print_meshes_line::no);

    const TestNS::CoordsMatchingNS::input_data_type input_data(TestNS::EmptyModelSettings(),
                                                               generated_data.GetLuaFile());

    MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = generated_data.GenerateCoordsMatching(), std::exception);
}


BOOST_AUTO_TEST_CASE(botched_meshes_line)
{
    TestData generated_data(
        "botched_meshes_line", mesh_ids, { 1, 2, 3 }, { 11, 12, 13 }, compute_reverse::no, print_meshes_line::botched);

    const TestNS::CoordsMatchingNS::input_data_type input_data(TestNS::EmptyModelSettings(),
                                                               generated_data.GetLuaFile());

    MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr coords_matching{ nullptr };

    BOOST_CHECK_THROW(coords_matching = generated_data.GenerateCoordsMatching(), std::exception);
}


BOOST_AUTO_TEST_CASE(array)
{
    TestData generated_data("array", mesh_ids, { 1, 2, 3, 4, 5 }, { 11, 12, 13, 14, 15 });

    const TestNS::CoordsMatchingNS::input_data_type input_data(TestNS::EmptyModelSettings(),
                                                               generated_data.GetLuaFile());

    auto coords_matching_ptr = generated_data.GenerateCoordsMatching();
    const auto& coords_matching = *coords_matching_ptr;

    {
        auto array = IntToStrongType::Perform({ 1, 3 });

        BOOST_CHECK(coords_matching.FindTargetIndex(array) == IntToStrongType::Perform({ 11, 13 }));
    }

    {
        auto array = IntToStrongType::Perform({ 3, 1 });

        BOOST_CHECK(coords_matching.FindTargetIndex(array) == IntToStrongType::Perform({ 11, 13 }));
    }

    {
        auto array = IntToStrongType::Perform({ 2, 3, 5 });

        BOOST_CHECK(coords_matching.FindTargetIndex(array) == IntToStrongType::Perform({ 12, 13, 15 }));
    }

    {
        auto array = IntToStrongType::Perform({ 11, 13 });

        BOOST_CHECK(coords_matching.FindSourceIndex(array) == IntToStrongType::Perform({ 1, 3 }));
    }

    {
        auto array = IntToStrongType::Perform({ 13, 11 });

        BOOST_CHECK(coords_matching.FindSourceIndex(array) == IntToStrongType::Perform({ 1, 3 }));
    }

    {
        auto array = IntToStrongType::Perform({ 12, 13, 15 });

        BOOST_CHECK(coords_matching.FindSourceIndex(array) == IntToStrongType::Perform({ 2, 3, 5 }));
    }

    BOOST_CHECK_EQUAL(coords_matching.GetSourceMeshId(), mesh_ids.first);
    BOOST_CHECK_EQUAL(coords_matching.GetTargetMeshId(), mesh_ids.second);
}


BOOST_AUTO_TEST_SUITE_END()


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
