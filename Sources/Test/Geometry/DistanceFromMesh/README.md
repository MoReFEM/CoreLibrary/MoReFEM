Tests the DistanceFromMesh algorithm on a set of two plate meshes, a master and a slave.
The slave mesh corresponds to a translation of the master mesh along the Z axis by 4 meters.
We expect the distance vectors between the points from the master mesh and the slave one to be along the Z direction.
    This means that the expected components for the distance vectors will be [0, 0, distance].