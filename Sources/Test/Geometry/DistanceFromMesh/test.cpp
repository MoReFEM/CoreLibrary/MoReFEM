// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <filesystem>
#include <utility>

#include "Utilities/Filesystem/Directory.hpp"

#define BOOST_TEST_MODULE distance_from_mesh

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Mesh/Advanced/DistanceFromMesh.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // #__clang__


BOOST_FIXTURE_TEST_CASE(distance_from_mesh, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();

    constexpr auto dimension = ::MoReFEM::GeometryNS::dimension_type{ 3UL };
    constexpr auto space_unit = ::MoReFEM::CoordsNS::space_unit_type{ 1. };
    constexpr auto medit_master_unique_id = MeshNS::unique_id{ 1UL };
    {

        std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/Geometry/DistanceFromMesh/plate_master.mesh" };
        const FilesystemNS::File mesh_file{ std::move(path) };

        mesh_manager.Create(medit_master_unique_id,
                            mesh_file,
                            dimension,
                            MeshNS::Format::Medit,
                            space_unit,
                            Mesh::BuildEdge::yes,
                            Mesh::BuildFace::yes,
                            Mesh::BuildVolume::yes,
                            Mesh::BuildPseudoNormals::yes);
    }
    constexpr auto medit_slave_unique_id = MeshNS::unique_id{ 2UL };
    {

        std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/Geometry/DistanceFromMesh/plate_slave.mesh" };
        const FilesystemNS::File mesh_file{ std::move(path) };

        mesh_manager.Create(medit_slave_unique_id,
                            mesh_file,
                            dimension,
                            MeshNS::Format::Medit,
                            space_unit,
                            Mesh::BuildEdge::yes,
                            Mesh::BuildFace::yes,
                            Mesh::BuildVolume::yes,
                            Mesh::BuildPseudoNormals::yes);
    }

    decltype(auto) master_mesh = mesh_manager.GetMesh(medit_master_unique_id);
    decltype(auto) slave_mesh = mesh_manager.GetMesh(medit_slave_unique_id);

    auto distance_from_mesh = Advanced::MeshNS::DistanceFromMesh();
    const auto Ncoords = master_mesh.NprocessorWiseCoord();

    SpatialPoint work_projection{};
    SpatialPoint work_normal_projection{};
    SpatialPoint work_distance_vector{};
    constexpr auto x_component = ::MoReFEM::GeometryNS::dimension_type{ 0UL };
    constexpr auto y_component = ::MoReFEM::GeometryNS::dimension_type{ 1UL };
    constexpr auto z_component = ::MoReFEM::GeometryNS::dimension_type{ 2UL };
    constexpr auto epsilon = NumericNS::DefaultEpsilon<double>();
    for (auto coords_index = 0UL; coords_index < Ncoords; ++coords_index)
    {
        const auto& coords = master_mesh.GetCoord(coords_index);
        const auto distance =
            distance_from_mesh.ComputeDistance(coords, slave_mesh, work_projection, work_normal_projection);
        const auto expected_distance = work_projection[z_component] - coords[z_component];

        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (expected_distance)(distance)(epsilon));

        for (::MoReFEM::GeometryNS::dimension_type i{}; i < dimension; ++i)
            work_distance_vector.GetNonCstValue(i) = work_projection[i] - coords[i];

        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (work_distance_vector[x_component])(0.)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (work_distance_vector[y_component])(0.)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (work_distance_vector[z_component])(distance)(epsilon));
    }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
