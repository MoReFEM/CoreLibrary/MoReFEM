add_executable(TestDomainListInCoords)

target_sources(TestDomainListInCoords
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt        
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua        
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/README.md
)
          
target_link_libraries(TestDomainListInCoords
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_BASIC_TEST_TOOLS}
                     )

morefem_organize_IDE(TestDomainListInCoords Test/Geometry Test/Geometry/DomainListInCoords)

morefem_boost_test_both_modes(NAME DomainListInCoords
                              EXE TestDomainListInCoords
                              LUA ${MOREFEM_ROOT}/Sources/Test/Geometry/DomainListInCoords/demo.lua
                              TIMEOUT 10)

