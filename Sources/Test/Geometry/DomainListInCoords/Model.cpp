// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Test/Geometry/DomainListInCoords/Model.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/MacroEncapsulation/Op.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Test/Geometry/DomainListInCoords/InputData.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::DomainListInCoordsNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data, create_domain_list_for_coords::yes)
    { }


    namespace // anonymous
    {


        void CheckValue(std::string&& name, std::size_t expected_value, std::size_t computed_value);


    } // namespace


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) mesh = god_of_dof.GetMesh();

        decltype(auto) coords_list = mesh.GetProcessorWiseCoordsList();

        decltype(auto) domain_manager = DomainManager::GetInstance();

        decltype(auto) domain_volume = domain_manager.GetDomain(AsDomainId(DomainIndex::volume));

        decltype(auto) domain_ring = domain_manager.GetDomain(AsDomainId(DomainIndex::ring));

        decltype(auto) domain_exterior = domain_manager.GetDomain(AsDomainId(DomainIndex::exterior_surface));

        decltype(auto) domain_interior = domain_manager.GetDomain(AsDomainId(DomainIndex::interior_surface));

        // Domains through lightweight domain list
        decltype(auto) domain_exterior_and_ring = domain_manager.GetDomain(AsDomainId(DomainIndex::exterior_and_ring));

        decltype(auto) domain_interior_and_ring = domain_manager.GetDomain(AsDomainId(DomainIndex::interior_and_ring));

        for (decltype(auto) coords_ptr : coords_list)
        {
            assert(!(!coords_ptr));

            decltype(auto) coords = *coords_ptr;

            if (coords.IsInDomain(domain_volume))
                ++Ncoords_in_volume_;

            if (coords.IsInDomain(domain_ring))
                ++Ncoords_in_ring_;

            if (coords.IsInDomain(domain_exterior))
                ++Ncoords_in_exterior_;

            if (coords.IsInDomain(domain_interior))
                ++Ncoords_in_interior_;

            if (coords.IsInDomain(domain_exterior_and_ring))
                ++Ncoords_in_exterior_and_ring_;

            if (coords.IsInDomain(domain_interior_and_ring))
                ++Ncoords_in_interior_and_ring_;
        }
    }


    void Model::SupplFinalize()
    { }


    void Model::CheckNormalDomain() const
    {
        decltype(auto) mpi = parent::GetMpi();
        const auto Nprocessor = mpi.Nprocessor<int>();

        auto Ncoords_in_volume = Ncoords_in_volume_;
        auto Ncoords_in_ring = Ncoords_in_ring_;
        auto Ncoords_in_exterior = Ncoords_in_exterior_;
        auto Ncoords_in_interior = Ncoords_in_interior_;

        if (Nprocessor > 1)
        {
            Ncoords_in_volume = mpi.AllReduce(Ncoords_in_volume_, Wrappers::MpiNS::Op::Sum);
            Ncoords_in_ring = mpi.AllReduce(Ncoords_in_ring_, Wrappers::MpiNS::Op::Sum);
            Ncoords_in_exterior = mpi.AllReduce(Ncoords_in_exterior_, Wrappers::MpiNS::Op::Sum);
            Ncoords_in_interior = mpi.AllReduce(Ncoords_in_interior_, Wrappers::MpiNS::Op::Sum);
        }

        CheckValue("Ncoords_in_volume", 48, Ncoords_in_volume);
        CheckValue("Ncoords_in_ring", 8, Ncoords_in_ring);
        CheckValue("Ncoords_in_exterior", 24, Ncoords_in_exterior);
        CheckValue("Ncoords_in_interior", 24, Ncoords_in_interior);
    }


    void Model::CheckLightweightDomain() const
    {
        decltype(auto) mpi = parent::GetMpi();
        const auto Nprocessor = mpi.Nprocessor<int>();

        auto Ncoords_in_exterior_and_ring = Ncoords_in_exterior_and_ring_;
        auto Ncoords_in_interior_and_ring = Ncoords_in_interior_and_ring_;

        if (Nprocessor > 1)
        {
            Ncoords_in_exterior_and_ring = mpi.AllReduce(Ncoords_in_exterior_and_ring, Wrappers::MpiNS::Op::Sum);
            Ncoords_in_interior_and_ring = mpi.AllReduce(Ncoords_in_interior_and_ring, Wrappers::MpiNS::Op::Sum);
        }

        CheckValue("Ncoords_in_exterior_and_ring", 28, Ncoords_in_exterior_and_ring);
        CheckValue("Ncoords_in_interior_and_ring", 28, Ncoords_in_interior_and_ring);
    }


    namespace // anonymous
    {


        PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // # __clang__


        void CheckValue(std::string&& name, std::size_t expected_value, std::size_t computed_value)
        {
            BOOST_TEST_INFO("Expected value for " << name << " was " << expected_value
                                                  << " but computation "
                                                     "through Coords iteration yielded "
                                                  << computed_value);
            BOOST_TEST(expected_value == computed_value);
        }


        PRAGMA_DIAGNOSTIC(pop)

        // > *** MoReFEM Doxygen end of group *** //
        ///@} // \addtogroup TestGroup
        // *** MoReFEM Doxygen end of group *** < //


    } // namespace


} // namespace MoReFEM::TestNS::DomainListInCoordsNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
