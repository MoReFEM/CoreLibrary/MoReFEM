// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Geometry/DomainListInCoords/InputData.hpp"


namespace MoReFEM::TestNS::DomainListInCoordsNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<sole>>({ "Sole finite element space" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>({ " volume)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::exterior_surface)>>(
            { " exterior_surface)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::interior_surface)>>(
            { " interior_surface)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::ring)>>({ " ring)" });
        SetDescription<InputDataNS::LightweightDomainList<sole>>({ " sole" });
        SetDescription<InputDataNS::Unknown<sole>>({ " sole" });
        SetDescription<InputDataNS::NumberingSubset<sole>>({ " sole" });
    }


} // namespace MoReFEM::TestNS::DomainListInCoordsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
