add_executable(TestEnsightMeditMesh)

target_sources(TestEnsightMeditMesh
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestEnsightMeditMesh
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
                      ${MOREFEM_BASIC_TEST_TOOLS})


morefem_organize_IDE(TestEnsightMeditMesh Test/Geometry Test/Geometry/EnsightMeditMesh)

morefem_boost_test_sequential_mode(NAME EnsightMeditMesh
                                   EXE TestEnsightMeditMesh
                                   TIMEOUT 10)
