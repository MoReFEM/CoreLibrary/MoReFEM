// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <filesystem>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/SpatialPoint.hpp"
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#define BOOST_TEST_MODULE geometry_free_functions
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    using fixture_type = MoReFEM::TestNS::FixtureNS::TestEnvironment;

    constexpr auto space_unit = ::MoReFEM::CoordsNS::space_unit_type{ 1. };

    constexpr auto mesh_index = MeshNS::unique_id{ 1UL };


} // namespace


BOOST_FIXTURE_TEST_CASE(jacobian, fixture_type)
{
    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();

    const FilesystemNS::File mesh_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh") };

    manager.Create(mesh_index,
                   mesh_file,
                   ::MoReFEM::GeometryNS::dimension_type{ 2 },
                   ::MoReFEM::MeshNS::Format::Medit,
                   ::MoReFEM::CoordsNS::space_unit_type{ 1. },
                   Mesh::BuildEdge::no,
                   Mesh::BuildFace::no,
                   Mesh::BuildVolume::no);

    decltype(auto) mesh = manager.GetMesh(mesh_index);
    decltype(auto) geom_elt =
        mesh.GetGeometricEltFromIndex<RoleOnProcessor::processor_wise>(::MoReFEM::GeomEltNS::index_type{ 1UL });

    const LocalCoords local_coords({ 0.15 });

    auto compute_jacobian_helper = Advanced::GeomEltNS::ComputeJacobian(mesh.GetDimension());

    const auto jacobian = compute_jacobian_helper.Compute(geom_elt, local_coords);

    const Eigen::Matrix2d expected_matrix{ { 0.2, 0. }, { 0., 0. } };

    BOOST_CHECK_EQUAL(std::get<Eigen::Matrix2d>(jacobian), expected_matrix);
}


BOOST_FIXTURE_TEST_CASE(barycenter, fixture_type)
{
    // Expects the first test to have run first.
    decltype(auto) manager = Internal::MeshNS::MeshManager::GetInstance();

    decltype(auto) mesh = manager.GetMesh(mesh_index);
    decltype(auto) geom_elt =
        mesh.GetGeometricEltFromIndex<RoleOnProcessor::processor_wise>(::MoReFEM::GeomEltNS::index_type{ 1UL });

    SpatialPoint barycenter;
    Advanced::GeomEltNS::ComputeBarycenter(geom_elt, barycenter);

    const SpatialPoint expected(0.2, 0., 0., space_unit);

    const double dist = Distance(barycenter, expected);

    BOOST_CHECK_PREDICATE(NumericNS::IsZero<double>, (dist)(1.e-8));
}


BOOST_FIXTURE_TEST_CASE(local_2_global, fixture_type)
{
    // Expects the first test to have run first.
    decltype(auto) manager = Internal::MeshNS::MeshManager::GetInstance();

    decltype(auto) mesh = manager.GetMesh(mesh_index);
    decltype(auto) geom_elt =
        mesh.GetGeometricEltFromIndex<RoleOnProcessor::processor_wise>(::MoReFEM::GeomEltNS::index_type{ 1UL });

    const LocalCoords local_coords({ 0.15 });

    SpatialPoint global;
    Advanced::GeomEltNS::Local2Global(geom_elt, local_coords, global);

    const SpatialPoint expected(0.23, -1, 0, space_unit);

    const double dist = Distance(global, expected);

    BOOST_CHECK_PREDICATE(NumericNS::IsZero<double>, (dist)(1.e-8));
}

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
