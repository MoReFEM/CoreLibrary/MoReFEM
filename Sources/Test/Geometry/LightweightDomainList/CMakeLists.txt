add_executable(TestLightweightDomainList)

target_sources(TestLightweightDomainList
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp     
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
        ${CMAKE_CURRENT_LIST_DIR}/README.md
)
          
target_link_libraries(TestLightweightDomainList
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestLightweightDomainList Test/Geometry Test/Geometry/LightweightDomainList)

morefem_boost_test_both_modes(NAME LightweightDomainList
                              EXE TestLightweightDomainList
                              TIMEOUT 5
                              LUA ${MOREFEM_ROOT}/Sources/Test/Geometry/LightweightDomainList/demo.lua)
