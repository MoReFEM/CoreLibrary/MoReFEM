// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Geometry/LightweightDomainList/InputData.hpp"


namespace MoReFEM::TestNS::LightweightDomainListNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<sole>>({ "Sole finite element space" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain)" });

        // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        SetDescription<InputDataNS::Domain<13UL>>({ " Reference 13" });
        SetDescription<InputDataNS::Domain<14UL>>({ " Reference 14" });
        SetDescription<InputDataNS::Domain<15UL>>({ " Reference 15" });
        SetDescription<InputDataNS::Domain<16UL>>({ " Reference 16" });
        // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

        SetDescription<InputDataNS::LightweightDomainList<1>>({ " 1" });
        SetDescription<InputDataNS::Unknown<sole>>({ " sole" });
        SetDescription<InputDataNS::NumberingSubset<sole>>({ " sole" });
    }


} // namespace MoReFEM::TestNS::LightweightDomainListNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
