// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <iterator>

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"

#define BOOST_TEST_MODULE lightweight_domain_list
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Geometry/LightweightDomainList/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{

    using model_type = TestNS::BareModel<TestNS::LightweightDomainListNS::morefem_data_type>;


    using fixture_type = TestNS::FixtureNS::Model<model_type>;

    GeometricElt::vector_shared_ptr ComputeGeomEltListInDomain(const auto& geom_elt_list, const Domain& domain);

    constexpr auto mesh_index = MeshNS::unique_id{ 1UL };

} // namespace


BOOST_FIXTURE_TEST_CASE(lightweight_domain_definition, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance();

    decltype(auto) mesh = model.GetMesh(mesh_index);

    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    // We compare here a full-fledged domain to the same domain defined through LightweightDomainList;
    // convention used is that full-fledged domain index = lightweight domain index + 10.
    constexpr auto shift = DomainNS::unique_id{ 10UL };

    {
        const auto lightweight_index = DomainNS::unique_id{ 3UL };
        auto domain_content = ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(lightweight_index));
        auto lightweight_domain_content =
            ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(shift + lightweight_index));

        BOOST_CHECK(domain_content == lightweight_domain_content);
    }

    {
        const auto lightweight_index = DomainNS::unique_id{ 4UL };

        auto domain_content = ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(lightweight_index));
        auto lightweight_domain_content =
            ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(shift + lightweight_index));

        BOOST_CHECK(domain_content == lightweight_domain_content);
    }

    {
        const auto lightweight_index = DomainNS::unique_id{ 5UL };

        auto domain_content = ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(lightweight_index));
        auto lightweight_domain_content =
            ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(shift + lightweight_index));

        BOOST_CHECK(domain_content == lightweight_domain_content);
    }

    {
        const auto lightweight_index = DomainNS::unique_id{ 6UL };

        auto domain_content = ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(lightweight_index));
        auto lightweight_domain_content =
            ComputeGeomEltListInDomain(geom_elt_list, domain_manager.GetDomain(shift + lightweight_index));

        BOOST_CHECK(domain_content == lightweight_domain_content);
    }
}


namespace // anonymous
{

    GeometricElt::vector_shared_ptr ComputeGeomEltListInDomain(const auto& geom_elt_list, const Domain& domain)
    {
        GeometricElt::vector_shared_ptr ret;
        ret.reserve(geom_elt_list.size());

        std::copy_if(geom_elt_list.cbegin(),
                     geom_elt_list.cend(),
                     std::back_inserter(ret),
                     [&domain](const auto& geom_elt_ptr)
                     {
                         assert(!(!geom_elt_ptr));
                         return domain.IsGeometricEltInside(*geom_elt_ptr);
                     });

        return ret;
    }


} // namespace


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
