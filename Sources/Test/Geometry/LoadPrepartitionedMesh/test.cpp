// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdlib>

#define BOOST_TEST_MODULE load_mesh_prepartitioned_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Geometry/LoadPrepartitionedMesh/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::LoadPrepartitionedMeshNS::Model
    >;
    // clang-format on


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // # __clang__


BOOST_FIXTURE_TEST_CASE(geometric_elt_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckGeometricEltList();
}


BOOST_FIXTURE_TEST_CASE(mesh_label_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckLabelList();
}


BOOST_FIXTURE_TEST_CASE(coords_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckCoordsList();
}


BOOST_FIXTURE_TEST_CASE(interface_consistency, fixture_type)
{
    decltype(auto) model = GetModel();
    model.CheckInterfaceList();
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
