add_executable(TestLocalCoords)

target_sources(TestLocalCoords
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt        
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestLocalCoords
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_GEOMETRY}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestLocalCoords Test/Geometry Test/Geometry/LocalCoords)

morefem_boost_test_sequential_mode(NAME LocalCoords
                                   EXE TestLocalCoords
                                   TIMEOUT 5)
