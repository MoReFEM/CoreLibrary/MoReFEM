// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Geometry/Coloring/InputData.hpp"


namespace MoReFEM::TestNS::ColoringNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim1)>>(
            { "Finite element space for dimension 1" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim2)>>(
            { "Finite element space for dimension 2" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim1)>>({ "Dimension 1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim2)>>({ "Dimension 2" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(Unused::value)>>({ "Sole" });

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim1)>>({ "Dimension 1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim2)>>({ "Dimension 2" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Sole" });
    }


} // namespace MoReFEM::TestNS::ColoringNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
