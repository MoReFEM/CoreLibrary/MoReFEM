// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE local_coords
#include <sstream>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/LocalCoords.hpp"

#include "Test/Tools/PredicateEigen.hpp"

using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(dimension_1)
{
    constexpr auto x{ 1.02 };
    const LocalCoords dim1{ x };
    BOOST_CHECK_EQUAL(dim1.GetDimension(), ::MoReFEM::GeometryNS::dimension_type{ 1 });
    BOOST_CHECK_CLOSE(dim1.r(), x, NumericNS::DefaultEpsilon<double>());
    // dim1.s() and dim1.t() would assert!
    BOOST_CHECK_CLOSE(
        dim1.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{ 0 }), x, NumericNS::DefaultEpsilon<double>());
    BOOST_CHECK_CLOSE(
        dim1.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{ 1 }), 0., NumericNS::DefaultEpsilon<double>());

    BOOST_CHECK_CLOSE(dim1[::MoReFEM::GeometryNS::dimension_type{ 0 }], x, NumericNS::DefaultEpsilon<double>());

    std::ostringstream oconv;
    dim1.Print(oconv);
    BOOST_CHECK_EQUAL(oconv.str(), "[1.02]");
}


BOOST_AUTO_TEST_CASE(dimension_3)
{
    constexpr auto x{ 1.02 };
    constexpr auto y{ -241.17 };
    constexpr auto z{ 48.7 };

    LocalCoords dim3{ x, y, z };
    BOOST_CHECK_EQUAL(dim3.GetDimension(), ::MoReFEM::GeometryNS::dimension_type{ 3 });
    BOOST_CHECK_CLOSE(dim3.r(), x, NumericNS::DefaultEpsilon<double>());
    BOOST_CHECK_CLOSE(dim3.s(), y, NumericNS::DefaultEpsilon<double>());
    BOOST_CHECK_CLOSE(dim3.t(), z, NumericNS::DefaultEpsilon<double>());

    BOOST_CHECK_CLOSE(
        dim3.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{ 0 }), x, NumericNS::DefaultEpsilon<double>());
    BOOST_CHECK_CLOSE(
        dim3.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{ 1 }), y, NumericNS::DefaultEpsilon<double>());
    BOOST_CHECK_CLOSE(
        dim3.GetValueOrZero(::MoReFEM::GeometryNS::dimension_type{ 2 }), z, NumericNS::DefaultEpsilon<double>());

    BOOST_CHECK_CLOSE(dim3[::MoReFEM::GeometryNS::dimension_type{ 0 }], x, NumericNS::DefaultEpsilon<double>());
    BOOST_CHECK_CLOSE(dim3[::MoReFEM::GeometryNS::dimension_type{ 1 }], y, NumericNS::DefaultEpsilon<double>());
    BOOST_CHECK_CLOSE(dim3[::MoReFEM::GeometryNS::dimension_type{ 2 }], z, NumericNS::DefaultEpsilon<double>());

    {
        const Eigen::Vector3d expected{ x, y, z };

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                              (dim3.GetCoordinates())(expected)(NumericNS::DefaultEpsilon<double>()));
    }

    dim3.GetNonCstValue(::MoReFEM::GeometryNS::dimension_type{ 0 }) = z;
    dim3.GetNonCstValue(::MoReFEM::GeometryNS::dimension_type{ 1 }) = x;
    dim3.GetNonCstValue(::MoReFEM::GeometryNS::dimension_type{ 2 }) = y;

    {
        const Eigen::Vector3d expected{ z, x, y };

        BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                              (dim3.GetCoordinates())(expected)(NumericNS::DefaultEpsilon<double>()));
    }

    std::ostringstream oconv;
    dim3.Print(oconv);

    BOOST_CHECK_EQUAL(oconv.str(), "[48.7, 1.02, -241.17]");
}


BOOST_AUTO_TEST_CASE(dimension_4)
{
    // Would trigger an assert!
    // LocalCoords dim4 { x, y, z, t };
}


BOOST_AUTO_TEST_CASE(dimension_0)
{
    // May be allocated but shouldn't...
    const LocalCoords dim0{};
    BOOST_CHECK_EQUAL(dim0.GetDimension(), ::MoReFEM::GeometryNS::dimension_type{ 0 });
}


BOOST_AUTO_TEST_CASE(equality)
{
    constexpr auto a{ 1.02 };
    constexpr auto b{ -0.7 };
    constexpr auto c{ 1.e-11 };
    constexpr auto d{ -1.e-17 };

    const LocalCoords lc1{ a, b, b };
    const LocalCoords lc1_bis{ a, b, b };

    BOOST_CHECK(lc1 == lc1);
    BOOST_CHECK(lc1 == lc1_bis);

    const LocalCoords almost_zero{ d, d, 0. };
    BOOST_CHECK(almost_zero == almost_zero);

    const LocalCoords another_almost_zero{ 0., d, d };
    BOOST_CHECK(almost_zero == another_almost_zero);

    const LocalCoords tiny_but_not_zero{ c, c, c };
    BOOST_CHECK(tiny_but_not_zero != almost_zero);

    const LocalCoords zero{ 0., 0., 0. };
    BOOST_CHECK(zero == almost_zero);
}


BOOST_AUTO_TEST_CASE(center_of_gravity)
{
    const LocalCoords a{ 1, 2, 3 };
    const LocalCoords b{ -2, 3, 7 };
    const LocalCoords c{ 4, 8, -3 };
    const LocalCoords d{ 0, 1, -5 };


    const LocalCoords expected{ 0.75, 3.5, 0.5 };

    const std::vector<LocalCoords> list{ a, b, c, d };
    BOOST_CHECK_EQUAL(ComputeCenterOfGravity(list), expected);
}


BOOST_AUTO_TEST_CASE(extract_mismatched_indexes)
{
    constexpr auto x{ 1.02 };
    constexpr auto y{ -241.17 };
    constexpr auto z{ 48.7 };


    {
        const LocalCoords lhs{ x, y, z };
        const LocalCoords rhs{ x, y, -z };

        // Form when exactly one is expected
        BOOST_CHECK_EQUAL(ExtractMismatchedComponentIndex(lhs, rhs), ::MoReFEM::GeometryNS::dimension_type{ 2 });
    }


    {
        const LocalCoords lhs{ x, y, z };
        const LocalCoords rhs{ 0., y, -z };

        // Form when several may pop up
        const auto mismatched_indexes = ExtractMismatchedComponentIndexes(lhs, rhs);

        BOOST_REQUIRE_EQUAL(mismatched_indexes.size(), 2UL);

        BOOST_CHECK_EQUAL(mismatched_indexes[0], ::MoReFEM::GeometryNS::dimension_type{ 0 });
        BOOST_CHECK_EQUAL(mismatched_indexes[1], ::MoReFEM::GeometryNS::dimension_type{ 2 });
    }

    {
        const LocalCoords lhs{ x, y, z };
        const LocalCoords rhs{ x, y, z };

        const auto mismatched_indexes = ExtractMismatchedComponentIndexes(lhs, rhs);
        BOOST_CHECK(mismatched_indexes.empty());
    }
}


BOOST_AUTO_TEST_CASE(extract_identical_indexes)
{
    constexpr auto x{ 1.02 };
    constexpr auto y{ -241.17 };
    constexpr auto z{ 48.7 };


    {
        const LocalCoords a{ x, y, z };
        const LocalCoords b{ x, -y, -z };
        const LocalCoords c{ x, 0., 0. };
        const std::vector<LocalCoords> list{ a, b, c };

        // Form when exactly one is expected
        const auto [index, value] = ExtractIdenticalComponentIndex(list);

        BOOST_CHECK_EQUAL(index, ::MoReFEM::GeometryNS::dimension_type{ 0 });
        BOOST_CHECK_CLOSE(value, x, NumericNS::DefaultEpsilon<double>());
    }


    {
        const LocalCoords a{ x, y, z };
        const LocalCoords b{ x, 0., z };
        const LocalCoords c{ x, 1., z };
        const std::vector<LocalCoords> list{ a, b, c };

        // Form when several may pop up
        const auto identical_indexes = ExtractIdenticalComponentIndexes(list);

        BOOST_REQUIRE_EQUAL(identical_indexes.size(), 2UL);
        BOOST_CHECK_EQUAL(identical_indexes[0], ::MoReFEM::GeometryNS::dimension_type{ 0 });
        BOOST_CHECK_EQUAL(identical_indexes[1], ::MoReFEM::GeometryNS::dimension_type{ 2 });
    }

    {
        const LocalCoords a{ x, y, z };
        const LocalCoords b{ z, x, y };
        const LocalCoords c{ y, z, x };
        const std::vector<LocalCoords> list{ a, b, c };

        // Form when several may pop up
        const auto identical_indexes = ExtractIdenticalComponentIndexes(list);
        BOOST_CHECK(identical_indexes.empty());
    }
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
