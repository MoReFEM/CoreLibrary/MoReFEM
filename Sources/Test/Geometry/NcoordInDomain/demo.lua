-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.


Unknown1 = {
    
    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "potential_1",
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",
    
} -- Unknown1


Mesh1 = {
    
    -- Path of the mesh file to use.
    -- Expected format: "VALUE"
    mesh = "${MOREFEM_ROOT}/Data/Mesh/cubeQ2.mesh",
    
    -- Format of the input mesh.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Ensight', 'Medit'})
    format = "Medit",
    
    -- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the
    -- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an
    -- exception is thrown.
    -- Expected format: VALUE
    -- Constraint: v <= 3 and v > 0
    dimension = 3,
    
    -- Space unit of the mesh.
    -- Expected format: VALUE
    space_unit = 1.
    
} -- Mesh1



Domain1 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 3 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at OptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain1


Domain2 = {
    
    mesh_index = { 1 },
    
    dimension_list = { },
    
    mesh_label_list = { 3 },
    
    geometric_element_type_list = { }
    
} -- Domain2


Domain3 = {
    
    mesh_index = { 1 },
    
    dimension_list = { },
    
    mesh_label_list = { 2, 5 },
    
    geometric_element_type_list = { }
    
} -- Domain3


Domain4 = {
    
    mesh_index = { 1 },
    
    dimension_list = { },
    
    mesh_label_list = { 1, 4, 5, 6 },
    
    geometric_element_type_list = { }
    
} -- Domain4


FiniteElementSpace1 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "potential_1" },
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "Q2" },
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace1



Result = {

	-- Directory in which all the results will be written.  
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/Geometry/NcoordInDomain"

} -- Result

