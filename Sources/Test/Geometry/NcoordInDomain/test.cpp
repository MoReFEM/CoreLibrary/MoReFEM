// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <functional>

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#define BOOST_TEST_MODULE Ncoords_in_domain
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/Geometry/NcoordInDomain/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/InitMoReFEMDataFromCLI.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::TestEnvironment;


}


BOOST_FIXTURE_TEST_CASE(check, fixture_type)
{
    using namespace TestNS::NcoordInDomainNS;

    // clang-format off
    decltype(auto) morefem_data =
        TestNS::InitMoReFEMDataFromCLI
        <
            TestNS::NcoordInDomainNS::ModelSettings,
            TestNS::NcoordInDomainNS::morefem_data_type::input_data_type,
            TestNS::NcoordInDomainNS::time_manager_type
        >();
    // clang-format on

    TestNS::BareModel<TestNS::NcoordInDomainNS::morefem_data_type> model(morefem_data,
                                                                         create_domain_list_for_coords::yes);
    model.Initialize();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) mesh = god_of_dof.GetMesh();

    decltype(auto) mpi = model.GetMpi();

    decltype(auto) domain_manager = DomainManager::GetInstance();

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::full_mesh);

        const auto Ncoord_in_domain =
            NcoordsInDomain<MpiScale::program_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 27UL;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain =
            NcoordsInDomain<MpiScale::processor_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::label_3);

        const auto Ncoord_in_domain =
            NcoordsInDomain<MpiScale::program_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 9UL;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain =
            NcoordsInDomain<MpiScale::processor_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::label_2_5);

        const auto Ncoord_in_domain =
            NcoordsInDomain<MpiScale::program_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 15UL;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain =
            NcoordsInDomain<MpiScale::processor_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::label_1_4_5_6);

        const auto Ncoord_in_domain =
            NcoordsInDomain<MpiScale::program_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 23UL;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain =
            NcoordsInDomain<MpiScale::processor_wise>(mpi, domain_manager.GetDomain(domain_index), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
