// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <functional>
#include <vector>

#include "Geometry/Coords/Coords.hpp"

#define BOOST_TEST_MODULE order_coords_list
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;


namespace MoReFEM::TestNS
{


    struct GenerateCoord
    {

        static Coords::shared_ptr NewCoords(CoordsNS::processor_wise_position index)
        {
            auto ret = Coords::shared_ptr(new Coords);
            ret->SetProcessorWisePosition(index);
            return ret;
        }

        static Coords::shared_ptr NewCoords(CoordsNS::program_wise_position index)
        {
            auto ret = Coords::shared_ptr(new Coords);
            ret->SetProgramWisePosition(index);
            return ret;
        }

        static Coords::shared_ptr NewCoords(CoordsNS::index_from_mesh_file index)
        {
            auto ret = Coords::shared_ptr(new Coords);
            ret->SetIndexFromMeshFile(index);
            return ret;
        }
    };

} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

BOOST_AUTO_TEST_CASE(two_coords)
{
    auto a = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 3UL });
    auto b = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 5UL });
    auto c = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 7UL });

    {
        Coords::vector_shared_ptr list{ a, b };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b }));
    }

    {
        Coords::vector_shared_ptr list{ b, a };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b }));
    }

    {
        Coords::vector_shared_ptr list{ a, c };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, c }));
    }

    {
        Coords::vector_shared_ptr list{ c, a };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, c }));
    }

    {
        Coords::vector_shared_ptr list{ b, c };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ b, c }));
    }

    {
        Coords::vector_shared_ptr list{ c, b };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ b, c }));
    }
}


BOOST_AUTO_TEST_CASE(more_coords)
{
    auto a = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 3UL });
    auto b = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 5UL });
    auto c = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 7UL });
    auto d = TestNS::GenerateCoord::NewCoords(CoordsNS::processor_wise_position{ 9UL });

    {
        Coords::vector_shared_ptr list{ a, b, c, d };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b, c, d }));
    }


    {
        Coords::vector_shared_ptr list{ a, d, c, b };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, b, c, d }));
    }

    {
        Coords::vector_shared_ptr list{ a, d, b, c };
        Internal::InterfaceNS::OrderCoordsList(list);
        BOOST_CHECK((list == Coords::vector_shared_ptr{ a, c, b, d }));
    }
}


BOOST_AUTO_TEST_CASE(two_coords_index_from_mesh_file)
{
    // Same test as two_coords but upon a vector of Coords indexes.
    auto a = CoordsNS::index_from_mesh_file{ 3UL };
    auto b = CoordsNS::index_from_mesh_file{ 5UL };
    auto c = CoordsNS::index_from_mesh_file{ 7UL };

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ a, b };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, b }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ b, a };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, b }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ a, c };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, c }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ c, a };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ a, c }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ b, c };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ b, c }));
    }

    {
        std::vector<CoordsNS::index_from_mesh_file> list{ c, b };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::index_from_mesh_file>{ b, c }));
    }
}

BOOST_AUTO_TEST_CASE(more_coords_program_wise_index)
{
    auto a = CoordsNS::program_wise_position{ 3UL };
    auto b = CoordsNS::program_wise_position{ 5UL };
    auto c = CoordsNS::program_wise_position{ 7UL };
    auto d = CoordsNS::program_wise_position{ 9UL };

    {
        std::vector<CoordsNS::program_wise_position> list{ a, b, c, d };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::program_wise_position>{ a, b, c, d }));
    }

    {
        std::vector<CoordsNS::program_wise_position> list{ a, d, c, b };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::program_wise_position>{ a, b, c, d }));
    }

    {
        std::vector<CoordsNS::program_wise_position> list{ a, d, b, c };
        Internal::InterfaceNS::OrderCoordsList(list, std::less<>());
        BOOST_CHECK((list == std::vector<CoordsNS::program_wise_position>{ a, c, b, d }));
    }
}

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
