add_executable(TestP2Mesh)

target_sources(TestP2Mesh
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp     
        ${CMAKE_CURRENT_LIST_DIR}/README.md
)
          
target_link_libraries(TestP2Mesh
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_GEOMETRY}>
                      ${MOREFEM_BASIC_TEST_TOOLS}
                     )

morefem_organize_IDE(TestP2Mesh Test/Geometry Test/Geometry/P2Mesh)

morefem_boost_test_sequential_mode(NAME P2Mesh
                                   EXE TestP2Mesh
                                   TIMEOUT 10)
