// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <utility>
#include <vector>

#define BOOST_TEST_MODULE p2_mesh
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // #__clang__


BOOST_FIXTURE_TEST_CASE(p2_mesh, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();

    constexpr auto medit_unique_id = MeshNS::unique_id{ 1UL };
    constexpr auto dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };
    constexpr auto space_unit = ::MoReFEM::CoordsNS::space_unit_type{ 1. };

    std::filesystem::path path{ "${MOREFEM_ROOT}/Data/Mesh/cubeQ2.mesh" };
    const FilesystemNS::File mesh_file{ std::move(path) };

    mesh_manager.Create(medit_unique_id, mesh_file, dimension, MeshNS::Format::Medit, space_unit);

    decltype(auto) medit_mesh = mesh_manager.GetMesh(medit_unique_id);

    decltype(auto) medit_mesh_label_list = medit_mesh.GetLabelList();

    std::vector<MeshLabelNS::index_type> medit_mesh_label_index_list;
    medit_mesh_label_index_list.reserve(medit_mesh_label_list.size());

    std::ranges::transform(medit_mesh_label_list,

                           std::back_inserter(medit_mesh_label_index_list),
                           [](const auto& ptr)
                           {
                               assert(!(!ptr));
                               return ptr->GetIndex();
                           });

    assert(medit_mesh_label_index_list.size() == medit_mesh_label_list.size());

    for (auto& val : medit_mesh_label_index_list)
        std::cout << val << '\n';

    const auto& geo_list = medit_mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    for (const auto& geo_elt : geo_list)
    {
        // const auto& vertex_list = geo_elt->GetVertexList();
        const auto& coords_list = geo_elt->GetCoordsList();
        std::cout << "Ncoords: " << geo_elt->Ncoords() << '\n';
        for (const auto& coord_ptr : coords_list)
        {
            const auto& coord = *coord_ptr;
            coord.Print(std::cout);
        }
    }

    auto medit_bag = medit_mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

    std::ranges::sort(medit_bag,

                      [](const auto& lhs, const auto& rhs)
                      {
                          assert(!(!lhs));
                          assert(!(!rhs));
                          return lhs->GetIdentifier() < rhs->GetIdentifier();
                      });


    // decltype(auto) medit_geom_elt_list = medit_mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
