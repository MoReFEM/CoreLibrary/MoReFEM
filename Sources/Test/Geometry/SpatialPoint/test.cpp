// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <cmath>
#include <sstream>
#include <utility>

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/SpatialPoint.hpp"

#define BOOST_TEST_MODULE spatial_point

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/PredicateEigen.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(empty_constructor)
{
    const auto zero = SpatialPoint();

    BOOST_CHECK_EQUAL(zero.x(), 0.);
    BOOST_CHECK_EQUAL(zero.y(), 0.);
    BOOST_CHECK_EQUAL(zero.z(), 0.);
}

BOOST_AUTO_TEST_CASE(constructor_from_eigen_vector)
{
    const auto point = SpatialPoint(Eigen::Vector3d{ 1., 2., 3. });

    BOOST_CHECK_EQUAL(point.x(), 1.);
    BOOST_CHECK_EQUAL(point.y(), 2.);
    BOOST_CHECK_EQUAL(point.z(), 3.);
}


BOOST_AUTO_TEST_CASE(constructor_3d)
{
    const auto point = SpatialPoint(1., 2., 3., CoordsNS::space_unit_type{ 2. });

    BOOST_CHECK_EQUAL(point.x(), 2.);
    BOOST_CHECK_EQUAL(point.y(), 4.);
    BOOST_CHECK_EQUAL(point.z(), 6.);
}


BOOST_AUTO_TEST_CASE(constructor_from_std_array)
{
    const std::array<double, 3UL> array{ { 1., 2., 3. } };

    {
        const auto point = SpatialPoint(array, CoordsNS::space_unit_type{ 2. });
        BOOST_CHECK_EQUAL(point.x(), 2.);
        BOOST_CHECK_EQUAL(point.y(), 4.);
        BOOST_CHECK_EQUAL(point.z(), 6.);
    }

    {
        // NOLINTNEXTLINE(hicpp-move-const-arg,performance-move-const-arg)
        const auto point = SpatialPoint(std::move(array), CoordsNS::space_unit_type{ 2. });
        BOOST_CHECK_EQUAL(point.x(), 2.);
        BOOST_CHECK_EQUAL(point.y(), 4.);
        BOOST_CHECK_EQUAL(point.z(), 6.);
    }

    {
        const auto point = SpatialPoint(std::array<double, 3UL>{ { 1., 2., 3. } }, CoordsNS::space_unit_type{ 2. });
        BOOST_CHECK_EQUAL(point.x(), 2.);
        BOOST_CHECK_EQUAL(point.y(), 4.);
        BOOST_CHECK_EQUAL(point.z(), 6.);
    }
}


BOOST_AUTO_TEST_CASE(constructor_from_input_stream)
{
    std::istringstream iconv("1. 2.");

    const auto point = SpatialPoint(GeometryNS::dimension_type{ 2 }, iconv, CoordsNS::space_unit_type{ 2. });

    BOOST_CHECK_EQUAL(point.x(), 2.);
    BOOST_CHECK_EQUAL(point.y(), 4.);
    BOOST_CHECK_EQUAL(point.z(), 0.);
}


BOOST_AUTO_TEST_CASE(accessors_mutators)
{
    auto point = SpatialPoint(1., 2., 3., CoordsNS::space_unit_type{ 1. });

    BOOST_CHECK_EQUAL(point.x(), point[GeometryNS::dimension_type{ 0 }]);
    BOOST_CHECK_EQUAL(point.y(), point[GeometryNS::dimension_type{ 1 }]);
    BOOST_CHECK_EQUAL(point.z(), point[GeometryNS::dimension_type{ 2 }]);

    point.GetNonCstValue(GeometryNS::dimension_type{ 0 }) = 4.;

    BOOST_CHECK_EQUAL(point.x(), 4.);
}

BOOST_AUTO_TEST_CASE(print)
{
    const auto point = SpatialPoint(1., 2., 3., CoordsNS::space_unit_type{ 1. });

    std::ostringstream oconv;
    oconv << point;

    BOOST_CHECK_EQUAL(oconv.str(), "(1, 2, 3)");
}


BOOST_AUTO_TEST_CASE(reset)
{
    auto point = SpatialPoint(1., 2., 3., CoordsNS::space_unit_type{ 1. });

    point.Reset();
    BOOST_CHECK_EQUAL(point.x(), 0.);
    BOOST_CHECK_EQUAL(point.y(), 0.);
    BOOST_CHECK_EQUAL(point.z(), 0.);
}


BOOST_AUTO_TEST_CASE(underlying_eigen)
{
    const auto point1 = SpatialPoint(1., 2., 3., CoordsNS::space_unit_type{ 1. });

    const Eigen::Vector3d expected{ 1., 2., 3. };

    BOOST_CHECK_EQUAL(point1.GetUnderlyingVector(), expected);
}

BOOST_AUTO_TEST_CASE(operator_plus)
{
    const auto point1 = SpatialPoint(1., 2., 3., CoordsNS::space_unit_type{ 1. });
    const auto point2 = SpatialPoint(6., 5., 4., CoordsNS::space_unit_type{ 1. });

    const auto expected_sum = SpatialPoint(7., 7., 7., CoordsNS::space_unit_type{ 1. });
    const auto obtained_sum = point1 + point2;

    BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                          (obtained_sum.GetUnderlyingVector())(expected_sum.GetUnderlyingVector())(1.e-15));

    const auto expected_diff = SpatialPoint(-5., -3., -1., CoordsNS::space_unit_type{ 1. });
    const auto obtained_diff = point1 - point2;

    BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                          (obtained_diff.GetUnderlyingVector())(expected_diff.GetUnderlyingVector())(1.e-15));
}


BOOST_AUTO_TEST_CASE(distance)
{
    const auto point1 = SpatialPoint(1., 2., 3., CoordsNS::space_unit_type{ 1. });
    const auto point2 = SpatialPoint(6., 5., 4., CoordsNS::space_unit_type{ 1. });

    using MoReFEM::NumericNS::Square;
    const auto expected_square_distance =
        Square(point1.x() - point2.x()) + Square(point1.y() - point2.y()) + Square(point1.z() - point2.z());

    BOOST_CHECK_CLOSE(Distance(point1, point2), std::sqrt(expected_square_distance), 1.e-15);

    BOOST_CHECK_CLOSE(SquaredDistance(point1, point2), expected_square_distance, 1.e-15);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
