// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE topology_concept

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/Point/Topology/Point.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// NOLINTBEGIN(readability-simplify-boolean-expr)

BOOST_AUTO_TEST_CASE(point)
{
    using type = Advanced::RefGeomEltNS::TopologyNS::Point;
    static_assert(Advanced::Concept::TopologyNS::TraitsClass<type>);
    static_assert(!Advanced::Concept::TopologyNS::with_edge<type>);
    static_assert(!Advanced::Concept::TopologyNS::with_face<type>);
}


BOOST_AUTO_TEST_CASE(hexahedron)
{
    using type = Advanced::RefGeomEltNS::TopologyNS::Hexahedron;
    static_assert(Advanced::Concept::TopologyNS::TraitsClass<type>);
    static_assert(Advanced::Concept::TopologyNS::with_edge<type>);
    static_assert(Advanced::Concept::TopologyNS::with_face<type>);
}


BOOST_AUTO_TEST_CASE(quadrangle)
{
    using type = Advanced::RefGeomEltNS::TopologyNS::Quadrangle;
    static_assert(Advanced::Concept::TopologyNS::TraitsClass<type>);
    static_assert(Advanced::Concept::TopologyNS::with_edge<type>);
    static_assert(Advanced::Concept::TopologyNS::with_face<type>);
}

BOOST_AUTO_TEST_CASE(segment)
{
    using type = Advanced::RefGeomEltNS::TopologyNS::Segment;
    static_assert(Advanced::Concept::TopologyNS::TraitsClass<type>);
    static_assert(Advanced::Concept::TopologyNS::with_edge<type>);
    static_assert(!Advanced::Concept::TopologyNS::with_face<type>);
}


BOOST_AUTO_TEST_CASE(triangle)
{
    using type = Advanced::RefGeomEltNS::TopologyNS::Triangle;
    static_assert(Advanced::Concept::TopologyNS::TraitsClass<type>);
    static_assert(Advanced::Concept::TopologyNS::with_edge<type>);
    static_assert(Advanced::Concept::TopologyNS::with_face<type>);
}


BOOST_AUTO_TEST_CASE(tetrahedron)
{
    using type = Advanced::RefGeomEltNS::TopologyNS::Tetrahedron;
    static_assert(Advanced::Concept::TopologyNS::TraitsClass<type>);
    static_assert(Advanced::Concept::TopologyNS::with_edge<type>);
    static_assert(Advanced::Concept::TopologyNS::with_face<type>);
}


// NOLINTEND(readability-simplify-boolean-expr)


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
