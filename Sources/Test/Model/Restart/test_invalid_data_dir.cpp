// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <tuple>
#include <utility>

#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#define BOOST_TEST_MODULE restart_invalid_data_dir
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        InputDataNS::Restart,
        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    using time_manager_type =
        ::MoReFEM::TimeManagerNS::Instance::ConstantTimeStep<TimeManagerNS::support_restart_mode::yes>;

    using morefem_data_type =
        MoReFEMData<MoReFEM::TestNS::EmptyModelSettings, input_data_type, time_manager_type, program_type::test>;


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(incorrect, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) environment = Utilities::Environment::GetInstance();

    FilesystemNS::File lua_file{ environment.SubstituteValuesInPath(
        "${MOREFEM_ROOT}/Sources/Test/Model/Restart/demo_invalid_data_dir.lua") };

    // NOLINTNEXTLINE(bugprone-use-after-move,hicpp-invalid-access-moved) - I don't see it here?!?
    BOOST_CHECK_THROW(auto morefem_data = morefem_data_type(std::move(lua_file)),
                      ExceptionNS::TimeManagerNS::RestartDirShouldntBeInResultDir);
}


PRAGMA_DIAGNOSTIC(pop)

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
