// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    //! An arbitrary Cauchy-Green tensor that appeared in the hyperelastic model. Might not be realistic - that's not
    //! the point of current test anyway.
    Eigen::Matrix<double, 6, 1> SampleCauchyGreenTensor3d();

    //! An arbitrary Cauchy-Green tensor that appeared in the hyperelastic model. Might not be realistic - that's not
    //! the point of current test anyway.
    Eigen::Vector3d SampleCauchyGreenTensor2d();

    //! An arbitrary Cauchy-Green tensor that appeared in the hyperelastic model. Might not be realistic - that's not
    //! the point of current test anyway.
    Eigen::Matrix<double, 1, 1> SampleCauchyGreenTensor1d();

    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    constexpr auto epsilon = 1.e-9;


} // namespace


namespace // anonymous
{

    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
    Eigen::Matrix<double, 6, 1> SampleCauchyGreenTensor3d()
    {
        return Eigen::Matrix<double, 6, 1>{ { 1.001654, 0.999784, 0.999909, -0.000358, 0.000206, -0.000215 } };
    }

    [[maybe_unused]] Eigen::Vector3d SampleCauchyGreenTensor2d()
    {
        return Eigen::Vector3d{ { 1.001654, 0.999784, 0.999909 } };
    }

    [[maybe_unused]] Eigen::Matrix<double, 1, 1> SampleCauchyGreenTensor1d()
    {
        return Eigen::Matrix<double, 1, 1>{ { 1.001654 } };
    }
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)


} // namespace
