// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>
#include <memory>
#include <string_view>

#include "Test/Operators/HyperelasticLaws/Fixture.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::HyperelasticLawNS
{


    Fixture::Fixture()
    {
        decltype(auto) model = GetModel();


        // Set Mesh, and extract three geometric elements which will be used for tests.
        {
            decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
            decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(HyperelasticLawNS::MeshIndex::sole));

            decltype(auto) geom_elt_factory = Advanced::GeometricEltFactory::GetInstance();

            {
                decltype(auto) ref_tetra4 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Tetrahedron4);
                auto [begin, end] = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_tetra4);
                assert(end - begin == 1);
                tetra_ = *begin;
            }

            {
                decltype(auto) ref_tri6 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Triangle3);
                auto [begin, end] = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_tri6);
                assert(end != begin);
                triangle_ = *begin;
            }

            {
                decltype(auto) ref_seg2 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Segment2);
                auto [begin, end] = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_seg2);
                assert(end != begin);
                segment_ = *begin;
            }
        }

        // Set Domain
        decltype(auto) domain_manager = DomainManager::GetInstance();
        // Set Solid
        quadrature_rule_per_topology_ = std::make_unique<QuadratureRulePerTopology>(3, 2);


        decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::sole));

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::dim3));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        decltype(auto) unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));

        time_manager_ = std::make_unique<TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>>();

        // Dim 3
        {
            const auto dim3 = EnumUnderlyingType(DomainIndex::dim3);
            decltype(auto) domain3d = domain_manager.GetDomain(AsDomainId(HyperelasticLawNS::DomainIndex::dim3));

            const Eigen::VectorXd initial_value{ { 0., 0., 0., 0., 0., 0. } };

            solid_[dim3] = std::make_unique<Solid<time_manager_type>>(
                model.GetMoReFEMData(), domain3d, *quadrature_rule_per_topology_);

            cauchy_green_tensor_[dim3] = std::make_unique<cauchy_green_tensor_type>(
                "Cauchy-Green tensor", domain3d, *quadrature_rule_per_topology_, initial_value, *time_manager_);

            cauchy_green_tensor_operator_[dim3] = std::make_unique<update_cauchy_green_op_type>(
                felt_space, unknown, *cauchy_green_tensor_[dim3], quadrature_rule_per_topology_.get());
        }

        // Dim 2
        {
            const auto dim2 = EnumUnderlyingType(DomainIndex::dim2);
            const Eigen::VectorXd initial_value{ { 0., 0., 0. } };

            decltype(auto) domain2d = domain_manager.GetDomain(AsDomainId(HyperelasticLawNS::DomainIndex::dim2));

            solid_[dim2] = std::make_unique<Solid<time_manager_type>>(
                model.GetMoReFEMData(), domain2d, *quadrature_rule_per_topology_);

            cauchy_green_tensor_[dim2] = std::make_unique<cauchy_green_tensor_type>(
                "Cauchy-Green tensor", domain2d, *quadrature_rule_per_topology_, initial_value, *time_manager_);

            cauchy_green_tensor_operator_[dim2] = std::make_unique<update_cauchy_green_op_type>(
                felt_space, unknown, *cauchy_green_tensor_[dim2], quadrature_rule_per_topology_.get());
        }

        // Dim 1
        {
            const auto dim1 = EnumUnderlyingType(DomainIndex::dim1);
            const Eigen::VectorXd initial_value{ { 0. } };

            decltype(auto) domain1d = domain_manager.GetDomain(AsDomainId(HyperelasticLawNS::DomainIndex::dim1));

            solid_[dim1] = std::make_unique<Solid<time_manager_type>>(
                model.GetMoReFEMData(), domain1d, *quadrature_rule_per_topology_);

            cauchy_green_tensor_[dim1] = std::make_unique<cauchy_green_tensor_type>(
                "Cauchy-Green tensor", domain1d, *quadrature_rule_per_topology_, initial_value, *time_manager_);

            cauchy_green_tensor_operator_[dim1] = std::make_unique<update_cauchy_green_op_type>(
                felt_space, unknown, *cauchy_green_tensor_[dim1], quadrature_rule_per_topology_.get());
        }
    }


    auto Fixture::GetSolid(::MoReFEM::GeometryNS::dimension_type dimension) const noexcept
        -> const Solid<time_manager_type>&
    {
        switch (dimension.Get())
        {
        case 3:
        {
            const auto dim_index = EnumUnderlyingType(DomainIndex::dim3);
            assert(!(!solid_[dim_index]));
            return *solid_[dim_index];
        }
        case 2:
        {
            const auto dim_index = EnumUnderlyingType(DomainIndex::dim2);
            assert(!(!solid_[dim_index]));
            return *solid_[dim_index];
        }
        case 1:
        {
            const auto dim_index = EnumUnderlyingType(DomainIndex::dim1);
            assert(!(!solid_[dim_index]));
            return *solid_[dim_index];
        }
        }

        assert(false && "Switch should handle all relevant cases!");
        exit(EXIT_FAILURE);
    }


    const GeometricElt& Fixture::GetTetrahedron() const noexcept
    {
        assert(!(!tetra_));
        return *tetra_;
    }


    const GeometricElt& Fixture::GetTriangle() const noexcept
    {
        assert(!(!triangle_));
        return *triangle_;
    }


    const GeometricElt& Fixture::GetSegment() const noexcept
    {
        assert(!(!segment_));
        return *segment_;
    }


    const QuadratureRulePerTopology& Fixture::GetQuadratureRulePerTopology() const noexcept
    {
        assert(!(!quadrature_rule_per_topology_));
        return *quadrature_rule_per_topology_;
    }


    const QuadraturePoint& Fixture::GetFirstQuadraturePoint(const GeometricElt& geom_elt) const noexcept
    {
        decltype(auto) quadrature_rule_per_topology = GetQuadratureRulePerTopology();
        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());
        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();
        assert(!quad_pt_list.empty());
        assert(!(!quad_pt_list[0]));
        return *quad_pt_list[0];
    }


    constexpr std::string_view OutputDirWrapper::Path()
    {
        return "Operators/HyperelasticLaws/CiarletGeymonat";
    }


} // namespace MoReFEM::TestNS::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
