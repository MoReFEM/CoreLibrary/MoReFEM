// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_HYPERELASTICLAWS_FIXTURE_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_HYPERELASTICLAWS_FIXTURE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/InputData/Advanced/SetFromInputData.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::HyperelasticLawNS
{


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;


    //! Alias for mode type.
    // clang-format off
    using model_type = TestNS::BareModel
    <
        MoReFEMDataForTest<TestNS::HyperelasticLawNS::ModelSettings, time_manager_type>,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on


    //! Helper object to pass string information at compile time.
    struct OutputDirWrapper
    {
        //! The method that does the actual work.
        static constexpr std::string_view Path();
    };

    //! Alias for the fixture parent.
    // clang-format off
    using fixture_parent_type = TestNS::FixtureNS::ModelNoInputData
    <
        model_type,
        OutputDirWrapper,
        time_manager_type,
        TestNS::FixtureNS::call_run_method_at_first_call::yes,
        create_domain_list_for_coords::yes
    >;
    // clang-format on


    //! Fixture to use for hyperelastic laws tests.
    class Fixture : public fixture_parent_type
    {
      public:
        //! Alias for Cauchy-Green tensor type,
        // clang-format off
        using cauchy_green_tensor_type =
            ParameterAtQuadraturePoint
            <
                ParameterNS::Type::vector, time_manager_type,
                ParameterNS::TimeDependencyNS::None,
                Wrappers::EigenNS::dWVector
            >;
        // clang-format on

        //! Alias to the operator used to update Cauchy Green tensor.
        using update_cauchy_green_op_type = GlobalParameterOperatorNS::UpdateCauchyGreenTensor<time_manager_type>;


      public:
        //! Constructor.
        Fixture();

        /*!
         * \brief Accessor to Solid.
         *
         * \param[in] dimension Dimension considered.
         */
        const Solid<time_manager_type>& GetSolid(::MoReFEM::GeometryNS::dimension_type dimension) const noexcept;

        //! Get the only tetrahedron of the mesh.
        const GeometricElt& GetTetrahedron() const noexcept;

        //! Get the one triangle extracted from the mesh for tests.
        const GeometricElt& GetTriangle() const noexcept;

        //! Get the one segment extracted from the mesh for tests.
        const GeometricElt& GetSegment() const noexcept;

        //! Get the quadrature rules to use.
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

        //! Get the first \a QuadraturePoint of the \a geom_elt.
        //!  \param[in] geom_elt \a GeometricElt for which a \a QuadraturePoint is sought.
        //!  \return First quadrature point found.
        const QuadraturePoint& GetFirstQuadraturePoint(const GeometricElt& geom_elt) const noexcept;


      private:
        //! Storage for solid object.
        typename Solid<time_manager_type>::array_const_unique_ptr<3UL> solid_{ nullptr, nullptr, nullptr };

        //! Storage of all quadrature rules to use.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_{ nullptr };

        //! Only tetrahedron element in the mesh.
        GeometricElt::shared_ptr tetra_{ nullptr };

        //! Triangle element in the mesh.
        GeometricElt::shared_ptr triangle_{ nullptr };

        //! Segment element in the mesh.
        GeometricElt::shared_ptr segment_{ nullptr };

        //! Operator that updates Cauchy-Green tensor.
        typename update_cauchy_green_op_type::array_const_unique_ptr<3UL> cauchy_green_tensor_operator_{ nullptr,
                                                                                                         nullptr,
                                                                                                         nullptr };

        //! Cauchy-Green tensor.
        typename cauchy_green_tensor_type::array_unique_ptr<3UL> cauchy_green_tensor_{ nullptr, nullptr, nullptr };

        //! Time manager.
        time_manager_type::unique_ptr time_manager_{ nullptr };
    };


} // namespace MoReFEM::TestNS::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_HYPERELASTICLAWS_FIXTURE_DOT_HPP_
// *** MoReFEM end header guards *** < //
