add_executable(TestInvariantHolder)

target_sources(TestInvariantHolder
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
 

target_link_libraries(TestInvariantHolder
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_OP_INSTANCES}>                      
)

morefem_boost_test_both_modes(NAME InvariantHolder
                              EXE TestInvariantHolder
                              TIMEOUT 20)

morefem_organize_IDE(TestInvariantHolder Test/Operators/HyperelasticLaws Test/Operators/HyperelasticLaws/InvariantHolder )

