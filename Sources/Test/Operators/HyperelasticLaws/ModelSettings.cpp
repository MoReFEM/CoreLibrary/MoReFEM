// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::HyperelasticLawNS
{


    void ModelSettings::Init()
    {
        // ****** Mesh ******
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ "Sole mesh" });

        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Path>("${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Dimension>(3UL);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::SpaceUnit>(1.);

        // ****** Domain ******
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim3)>>(
            { "Dimension 3 elements of the mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim3)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::sole) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim3)>::DimensionList>({ 3UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim3)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim3)>::GeomEltTypeList>({});

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim2)>>(
            { "Dimension 2 elements of the mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim2)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::sole) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim2)>::DimensionList>({ 2UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim2)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim2)>::GeomEltTypeList>({});

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim1)>>(
            { "Dimension 1 elements of the mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim1)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::sole) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim1)>::DimensionList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim1)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dim1)>::GeomEltTypeList>({});

        // ****** Solid ******
        Add<InputDataNS::Solid::VolumicMass::Nature>("constant");
        Add<InputDataNS::Solid::Kappa1::Nature>("constant");
        Add<InputDataNS::Solid::Kappa2::Nature>("constant");
        Add<InputDataNS::Solid::LameLambda::Nature>("constant");
        Add<InputDataNS::Solid::LameMu::Nature>("constant");
        Add<InputDataNS::Solid::HyperelasticBulk::Nature>("constant");
        Add<InputDataNS::Solid::Mu1::Nature>("constant");
        Add<InputDataNS::Solid::Mu2::Nature>("constant");
        Add<InputDataNS::Solid::C0::Nature>("constant");
        Add<InputDataNS::Solid::C1::Nature>("constant");
        Add<InputDataNS::Solid::C2::Nature>("constant");
        Add<InputDataNS::Solid::C3::Nature>("constant");
        Add<InputDataNS::Solid::C4::Nature>("constant");
        Add<InputDataNS::Solid::C5::Nature>("constant");

        Add<InputDataNS::Solid::VolumicMass::Value>(1.2);
        Add<InputDataNS::Solid::Kappa1::Value>(500.);
        Add<InputDataNS::Solid::Kappa2::Value>(403346.);
        Add<InputDataNS::Solid::LameLambda::Value>(1.21154e+06);
        Add<InputDataNS::Solid::LameMu::Value>(807692.);
        Add<InputDataNS::Solid::HyperelasticBulk::Value>(1750000.);

        Add<InputDataNS::Solid::Mu1::Value>(7000.);
        Add<InputDataNS::Solid::Mu2::Value>(3500.);
        Add<InputDataNS::Solid::C0::Value>(8500.);
        Add<InputDataNS::Solid::C1::Value>(0.11);
        Add<InputDataNS::Solid::C2::Value>(5700.);
        Add<InputDataNS::Solid::C3::Value>(0.13);
        Add<InputDataNS::Solid::C4::Value>(6200.);
        Add<InputDataNS::Solid::C5::Value>(0.16);


        // ****** Unknown ******
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>("Solid displacement");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Name>("displacement");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Nature>("vectorial");

        // ****** Numbering subset ******
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>(
            "Numbering subset");

        // ****** Finite element space ******
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim3)>>(
            "Finite element space - dim 3");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim3)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::sole));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim3)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim3)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::dim3));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim3)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::sole) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim3)>::ShapeFunctionList>(
            { "P1" }); // #1824 'P2' here triggers an assert.

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim2)>>(
            "Finite element space - dim 2");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim2)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::sole));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim2)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim2)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::dim2));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim2)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::sole) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim2)>::ShapeFunctionList>(
            { "P1" }); // #1824 'P2' here triggers an assert.

        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim1)>>(
            "Finite element space - dim 1");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim1)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::sole));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim1)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim1)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::dim1));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim1)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::sole) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::dim1)>::ShapeFunctionList>(
            { "P1" }); // #1824 'P2' here triggers an assert.

        // ****** Fibers ******
        {
            // clang-format off
            using fiber_type = InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fiberI4),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >;
            // clang-format on

            SetDescription<fiber_type>("Fiber I4 at node");
            Add<fiber_type::EnsightFile>("${MOREFEM_ROOT}/Data/Fiber/fibers_one_tetra.vct");
            Add<fiber_type::DomainIndex>(EnumUnderlyingType(DomainIndex::dim3));
            Add<fiber_type::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::dim3));
            Add<fiber_type::UnknownName>("displacement");
        }


        {
            // clang-format off
            using fiber_type = InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fiberI6),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >;
            // clang-format on

            SetDescription<fiber_type>("Fiber I6 at node");
            Add<fiber_type::EnsightFile>("${MOREFEM_ROOT}/Data/Fiber/fibers_one_tetra.vct");
            Add<fiber_type::DomainIndex>(EnumUnderlyingType(DomainIndex::dim3));
            Add<fiber_type::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::dim3));
            Add<fiber_type::UnknownName>("displacement");
        }
    }


} // namespace MoReFEM::TestNS::HyperelasticLawNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
