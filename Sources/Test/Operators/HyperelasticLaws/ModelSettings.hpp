// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_HYPERELASTICLAWS_MODELSETTINGS_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_HYPERELASTICLAWS_MODELSETTINGS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Utilities/InputData/SectionsAndLeaves.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::HyperelasticLawNS
{

    //! Enum class
    enum class MeshIndex : std::size_t { sole = 1UL };

    //! Enum class
    enum class DomainIndex : std::size_t { dim3 = 0UL, dim2, dim1 };

    //! Enum class
    enum class FEltSpaceIndex : std::size_t { dim3 = 0UL, dim2, dim1 };

    //! Enum class
    enum class UnknownIndex : std::size_t { displacement = 1UL };

    //! Enum class
    enum class NumberingSubsetIndex : std::size_t { sole = 1UL };

    //! Enum class
    enum class FiberIndex : std::size_t { fiberI4, fiberI6 };


    //! \brief Tuple with content for a \a ModelSettings instance.
    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple = std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::sole),
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>,

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::sole),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dim3),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dim3),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dim2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dim2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dim1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dim1),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::dim1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::dim1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::dim2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::dim2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::dim3),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::dim3),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::displacement),

        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::LameLambda,
        InputDataNS::Solid::LameMu,
        InputDataNS::Solid::Mu1,
        InputDataNS::Solid::Mu2,
        InputDataNS::Solid::C0,
        InputDataNS::Solid::C1,
        InputDataNS::Solid::C2,
        InputDataNS::Solid::C3,
        InputDataNS::Solid::C4,
        InputDataNS::Solid::C5,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiberI4),
            ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiberI6),
            ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiberI4),
            ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >::IndexedSectionDescription,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiberI6),
            ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >::IndexedSectionDescription
    >;



    //! \copydoc doxygen_hide_model_specific_input_data
    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


} // namespace MoReFEM::TestNS::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_HYPERELASTICLAWS_MODELSETTINGS_DOT_HPP_
// *** MoReFEM end header guards *** < //

