The results of the tests provided here are not deemed to be physically sound; they are here mostly to check we don't break anything whenever we make a change in the library (some functionalities / laws provided are not used otherwise in the core library models).

At the moment:

- **Generalized** coords aren't handled yet - todo in #1842 before implementing them in the tests (to be more precise I will of course write the test before the refactoring, but the answers will help me add some asserts in the way).

- Other laws shall wait likewise these values.


Expected values were rounded by default.
