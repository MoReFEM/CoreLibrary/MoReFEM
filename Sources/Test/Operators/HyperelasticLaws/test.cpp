// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Core/TimeManager/TimeManager.hpp"
#define BOOST_TEST_MODULE hyperelastic_laws

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/TimeManager/Policy/Evolution/None.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4Deviatoric.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4J6.hpp"
// #include "OperatorInstances/HyperelasticLaws/FiberDensityJ1J4J6.hpp"
#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"
#include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"
#include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<TimeManagerNS::Policy::None>;


} // namespace


// NOLINTBEGIN(readability-simplify-boolean-expr)

BOOST_AUTO_TEST_CASE(CiarletGeymonat)
{
    using type = MoReFEM::HyperelasticLawNS::CiarletGeymonat<time_manager_type>;

    static_assert(type::Ninvariant == 3UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == false);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == false);
}


BOOST_AUTO_TEST_CASE(CiarletGeymonatDeviatoric)
{
    using type = MoReFEM::HyperelasticLawNS::CiarletGeymonatDeviatoric<time_manager_type>;

    static_assert(type::Ninvariant == 3UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == false);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == false);
}

BOOST_AUTO_TEST_CASE(ExponentialJ1J4)
{
    using type = MoReFEM::HyperelasticLawNS::ExponentialJ1J4<time_manager_type>;

    static_assert(type::Ninvariant == 4UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == false);
}


BOOST_AUTO_TEST_CASE(ExponentialJ1J4Deviatoric)
{
    using type = MoReFEM::HyperelasticLawNS::ExponentialJ1J4Deviatoric<time_manager_type>;

    static_assert(type::Ninvariant == 4UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == false);
}

BOOST_AUTO_TEST_CASE(ExponentialJ1J4J6)
{
    using type = MoReFEM::HyperelasticLawNS::ExponentialJ1J4J6<time_manager_type>;

    static_assert(type::Ninvariant == 5UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == true);
}

// BOOST_AUTO_TEST_CASE(FiberDensityJ1J4J6)
//{
//     using type = MoReFEM::HyperelasticLawNS::FiberDensityJ1J4J6<>;
//
//     static_assert(type::Ninvariant == 5UL);
//     static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == true);
//     static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == true);
// }


BOOST_AUTO_TEST_CASE(LogI3Penalization)
{
    using type = MoReFEM::HyperelasticLawNS::LogI3Penalization<time_manager_type>;

    static_assert(type::Ninvariant == 1UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == false);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == false);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == false);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == false);
}

BOOST_AUTO_TEST_CASE(MooneyRivlin)
{
    using type = MoReFEM::HyperelasticLawNS::MooneyRivlin<time_manager_type>;

    static_assert(type::Ninvariant == 3UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == false);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == false);
}

BOOST_AUTO_TEST_CASE(StVenantKirchhoff)
{
    using type = MoReFEM::HyperelasticLawNS::StVenantKirchhoff<time_manager_type>;

    static_assert(type::Ninvariant == 3UL);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I1>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I2>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I3>() == true);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I4>() == false);
    static_assert(type::invariant_holder_parent::template IsPresent<::MoReFEM::InvariantNS::index::I6>() == false);
}

// NOLINTEND(readability-simplify-boolean-expr)


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
