// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE fiber_density_J1J4J6

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/HyperelasticLaws/FiberDensityJ1J4J6.hpp"

#include "Test/Operators/HyperelasticLaws/AnonymousNamespace.hpp"
#include "Test/Operators/HyperelasticLaws/Fixture.hpp"
#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HyperelasticLawNS;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(tetrahedron_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::FiberDensityJ1J4J6<>;

    decltype(auto) fiber_list_manager =
        FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>::GetInstance();

    decltype(auto) fiberI4 = fiber_list_manager.GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI4));
    fiberI4.Initialize(&GetQuadratureRulePerTopology());

    decltype(auto) fiberI6 = fiber_list_manager.GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI6));
    fiberI6.Initialize(&GetQuadratureRulePerTopology());

    law_type law(3ul,
                 GetSolid(),
                 fiber_list_manager.GetFiberListPtr(AsFiberListId(FiberIndex::fiberI4)),
                 fiber_list_manager.GetFiberListPtr(AsFiberListId(FiberIndex::fiberI6)));

    decltype(auto) invariant_holder = law.GetNonCstInvariantHolder();

    const auto cauchy_green_tensor = SampleCauchyGreenTensor();

    decltype(auto) geom_elt = GetTetrahedron();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    invariant_holder.UpdateCartesianForInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 14200.013948;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 6996.863906;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 3496.862164;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -13980.919824;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 1868.323499;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 22972.251625;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndSecondInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -4195.801558;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondAndThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -2328.107124;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWFourthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -0.798916;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndFourthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondAndFourthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWThirdAndFourthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -492.582791;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWSixthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -492.582791;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSixthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -492.582791;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndSixthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -492.582791;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondAndSixthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -492.582791;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWThirdAndSixthInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -492.582791;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
