// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Utilities/Numeric/Numeric.hpp"
#define BOOST_TEST_MODULE log_I3_penalization

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"

#include "Test/Operators/HyperelasticLaws/AnonymousNamespace.hpp"
#include "Test/Operators/HyperelasticLaws/Fixture.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HyperelasticLawNS;


PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
BOOST_FIXTURE_TEST_CASE(tetrahedron_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::LogI3Penalization<time_manager_type>;

    law_type law(::MoReFEM::GeometryNS::dimension_type{ 3 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 3 }));


    const auto cauchy_green_tensor = SampleCauchyGreenTensor3d();

    decltype(auto) geom_elt = GetTetrahedron();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 0.39604232276142598;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 588.01434174808594;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 436030.75458904577;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(triangle_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::LogI3Penalization<time_manager_type>;
    law_type law(::MoReFEM::GeometryNS::dimension_type{ 2 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 2 }));

    const auto cauchy_green_tensor = SampleCauchyGreenTensor2d();

    decltype(auto) geom_elt = GetTriangle();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 3942788.6442877958;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = -518503357.58041918;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 326848047347.91211;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(segment_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::LogI3Penalization<time_manager_type>;
    law_type law(::MoReFEM::GeometryNS::dimension_type{ 1 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 1 }));

    const auto cauchy_green_tensor = SampleCauchyGreenTensor1d();

    decltype(auto) geom_elt = GetSegment();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 0.59761415210221536;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 722.13162255735369;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 435695.86313048989;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
