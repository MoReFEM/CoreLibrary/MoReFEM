// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Utilities/Numeric/Numeric.hpp"
#define BOOST_TEST_MODULE mooney_rivlin

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"

#include "Test/Operators/HyperelasticLaws/AnonymousNamespace.hpp"
#include "Test/Operators/HyperelasticLaws/Fixture.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HyperelasticLawNS;


PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
BOOST_FIXTURE_TEST_CASE(tetrahedron_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::MooneyRivlin<time_manager_type>;

    law_type law(::MoReFEM::GeometryNS::dimension_type{ 3 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 3 }));


    const auto cauchy_green_tensor = SampleCauchyGreenTensor3d();

    decltype(auto) geom_elt = GetTetrahedron();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 1.3204895631292166;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.dWdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 499.77581866795595;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 402984.39051005617;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = -804930.27375179098;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 2214775.2293594321;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI1dI3(quad_pt, geom_elt);
        constexpr auto expected_value = -166.36795919293726;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI2dI3(quad_pt, geom_elt);
        constexpr auto expected_value = -268295.05602915381;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}

BOOST_FIXTURE_TEST_CASE(triangle_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::MooneyRivlin<time_manager_type>;
    law_type law(::MoReFEM::GeometryNS::dimension_type{ 2 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 2 }));

    const auto cauchy_green_tensor = SampleCauchyGreenTensor2d();

    decltype(auto) geom_elt = GetTriangle();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 58994634.453747757;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.dWdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 4257.5948835849467;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 29245996.004721425;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = -24157383413.481262;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 24828821235234.406;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI1dI3(quad_pt, geom_elt);
        constexpr auto expected_value = -876246.05054993823;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI2dI3(quad_pt, geom_elt);
        constexpr auto expected_value = -12038105641.445356;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(segment_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::MooneyRivlin<time_manager_type>;
    law_type law(::MoReFEM::GeometryNS::dimension_type{ 1 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 1 }));

    const auto cauchy_green_tensor = SampleCauchyGreenTensor1d();

    decltype(auto) geom_elt = GetSegment();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 1.5634807645765525;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.dWdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 499.72463691127746;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 402901.85593303305;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = -804413.89638268307;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 2213548.5931093288;
        BOOST_CHECK_PREDICATE(NumericNS::AreEquivalentThroughRelativeComparison<double>,
                              (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI1dI3(quad_pt, geom_elt);
        constexpr auto expected_value = -166.29981906968453;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI2dI3(quad_pt, geom_elt);
        constexpr auto expected_value = -268157.70444553572;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}
PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
