// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE st_venant_kirchhoff

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"

#include "Test/Operators/HyperelasticLaws/AnonymousNamespace.hpp"
#include "Test/Operators/HyperelasticLaws/Fixture.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HyperelasticLawNS;


PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)
BOOST_FIXTURE_TEST_CASE(tetrahedron_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::StVenantKirchhoff<time_manager_type>;
    law_type law(::MoReFEM::GeometryNS::dimension_type{ 3 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 3 }));


    const auto cauchy_green_tensor = SampleCauchyGreenTensor3d();

    decltype(auto) geom_elt = GetTetrahedron();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 2.511786180346506;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.dWdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 808643.96665700036;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI2(quad_pt, geom_elt);
        constexpr auto expected_value = -403846.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 2356.0163701923375;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 706731.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 1750000.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI3(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2dI3(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(triangle_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::StVenantKirchhoff<time_manager_type>;
    law_type law(::MoReFEM::GeometryNS::dimension_type{ 2 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 2 }));


    const auto cauchy_green_tensor = SampleCauchyGreenTensor2d();

    decltype(auto) geom_elt = GetTriangle();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 1275941.3133741624;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.dWdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 808708.27917800017;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI2(quad_pt, geom_elt);
        constexpr auto expected_value = -403846.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = -1747165.6397037501;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 706731.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 1750000.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI3(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2dI3(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(segment_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::StVenantKirchhoff<time_manager_type>;
    law_type law(::MoReFEM::GeometryNS::dimension_type{ 1 }, GetSolid(::MoReFEM::GeometryNS::dimension_type{ 1 }));


    const auto cauchy_green_tensor = SampleCauchyGreenTensor1d();

    decltype(auto) geom_elt = GetSegment();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 3.3604591524348351;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.dWdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 808860.93307400018;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI2(quad_pt, geom_elt);
        constexpr auto expected_value = -403846.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.dWdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 2894.5000000000778;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI1(quad_pt, geom_elt);
        constexpr auto expected_value = 706731.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.d2WdI3(quad_pt, geom_elt);
        constexpr auto expected_value = 1750000.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI2(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI1dI3(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law_type::d2WdI2dI3(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
