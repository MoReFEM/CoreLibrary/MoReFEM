// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_CHECKMATRIX_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_CHECKMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>


namespace MoReFEM::TestNS::FromCoordsMatchingNS
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Model;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Analyze the results of the test model and check the interpolation matrix works as intended.
     *
     * \param[in] model The dummy \a Model used in current test.
     */
    void CheckMatrix(const Model& model);


} // namespace MoReFEM::TestNS::FromCoordsMatchingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_CHECKMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
