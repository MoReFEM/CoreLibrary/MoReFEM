// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_MODEL_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/InputData.hpp"


namespace MoReFEM::TestNS::FromCoordsMatchingNS
{


    //! \copydoc doxygen_hide_model_4_test
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{


      private:
        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();
        ///@}

      public:
        //! Interpolation matrix to interpolate unknown from solid mesh to fluid mesh.
        const GlobalMatrix& GetMatrixUnknownFluidToSolid() const noexcept;

        //! Vertex matching operator to interpolate unknown from solid mesh to fluid mesh.
        const NonConformInterpolatorNS::FromCoordsMatching& GetOperatorUnknownFluidToSolid() const noexcept;


      private:
        //! Vertex matching operator to interpolate unknown from solid mesh to fluid mesh.
        NonConformInterpolatorNS::FromCoordsMatching::const_unique_ptr unknown_solid_2_fluid_ = nullptr;
    };


} // namespace MoReFEM::TestNS::FromCoordsMatchingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_NONCONFORMINTERPOLATOR_FROMCOORDSMATCHING_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
