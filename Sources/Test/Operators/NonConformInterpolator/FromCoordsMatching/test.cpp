// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE from_coords_matching
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/CheckDofList.hpp"
#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/CheckMatrix.hpp"
#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::FromCoordsMatchingNS::Model
    >;
    // clang-format on


} // namespace


BOOST_FIXTURE_TEST_CASE(check_interpolation_matrix, fixture_type)
{
    decltype(auto) model = GetModel();
    CheckMatrix(model);
}


BOOST_FIXTURE_TEST_CASE(check_dof_list, fixture_type)
{
    decltype(auto) model = GetModel();
    CheckDofList(model);
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
