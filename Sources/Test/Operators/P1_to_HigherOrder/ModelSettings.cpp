// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/P1_to_HigherOrder/InputData.hpp"


namespace MoReFEM::TestNS::P1_to_P_HigherOrder_NS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid_velocity)>>(
            { "Finite element space for fluid velocity" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid_velocity)>>(
            { "Finite element space for solid velocity" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::fluid_velocity)>>(
            { " fluid_velocity" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::solid_velocity)>>(
            { " solid_velocity" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>>({ " velocity" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>>({ " pressure" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::higher_order_velocity)>>(
            { " higher_order_velocity" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
    }


} // namespace MoReFEM::TestNS::P1_to_P_HigherOrder_NS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
