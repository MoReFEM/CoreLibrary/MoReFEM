These tests were written before the test suite was present, and adapted during #1707.

They could easily be improved as they clearly do not go very deep; they are however added as they are better than nothing.