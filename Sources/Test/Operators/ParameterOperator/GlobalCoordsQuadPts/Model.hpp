// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_GLOBALCOORDSQUADPTS_MODEL_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_GLOBALCOORDSQUADPTS_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/ParameterOperator/GlobalCoordsQuadPoints.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/InputData.hpp"


namespace MoReFEM::TestNS::GlobalCoordsQuadPt
{


    //! \copydoc doxygen_hide_model_4_test
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        //! Manage time iteration.
        void Forward();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();


        ///@}


      public:
        /*!
         * \brief Checks that the global coordinates for low order quadrature are the expected ones.
         *
         */
        void CheckLowOrderQuadrature() const;

        /*!
         * \brief Checks that the global coordinates for "medium" order quadrature are the expected ones.
         *
         */
        void CheckMediumOrderQuadrature() const;

        /*!
         * \brief Checks that the global coordinates for high order quadrature are the expected ones.
         *
         */
        void CheckHighOrderQuadrature() const;

      private:
        //! Accessor to the directory object which holds the main output directory.
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

        /*!
         * \brief Checks that the global coordinates for the quadrature given as argument are the expected ones.
         *
         * \param[in] quadrature_rule_per_topology \a QuadratureRule fow which the check is done for each topology.
         * \param[in] quadrature_order Quadrature order.
         */
        void CheckQuadrature(const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                             std::string_view quadrature_order) const;
    };


} // namespace MoReFEM::TestNS::GlobalCoordsQuadPt


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_GLOBALCOORDSQUADPTS_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
