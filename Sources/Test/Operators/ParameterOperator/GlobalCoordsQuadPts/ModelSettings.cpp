// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/InputData.hpp"


namespace MoReFEM::TestNS::GlobalCoordsQuadPt
{


    void ModelSettings::Init()
    {
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_numbering_subset)>>(
            { " generic_numbering_subset" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_unknown)>>({ " generic_unknown" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::GlobalCoordsQuadPt


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
