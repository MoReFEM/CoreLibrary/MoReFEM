// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE global_coords_quad_pt_1d_edge
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/ParameterOperator/GlobalCoordsQuadPts/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::GlobalCoordsQuadPt::Model
    >;
    // clang-format on


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_SUITE(CheckQuadPtCoords, fixture_type)

BOOST_AUTO_TEST_CASE(lower_order)
{
    Advanced::Wrappers::EigenNS::SetPrintIsMallocAllowedBehaviour(
        Advanced::Wrappers::EigenNS::print_is_malloc_allowed::each_time);


    GetModel().CheckLowOrderQuadrature();
}

BOOST_AUTO_TEST_CASE(medium_order)
{
    GetModel().CheckMediumOrderQuadrature();
}

BOOST_AUTO_TEST_CASE(high_order)
{
    GetModel().CheckHighOrderQuadrature();
}

BOOST_AUTO_TEST_SUITE_END()

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
