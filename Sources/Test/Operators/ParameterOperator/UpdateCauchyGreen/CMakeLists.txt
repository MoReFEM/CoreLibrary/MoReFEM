add_library(TestUpdateCauchyGreen_lib ${LIBRARY_TYPE} "")

target_sources(TestUpdateCauchyGreen_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/demo_2D.lua 
        ${CMAKE_CURRENT_LIST_DIR}/demo_3D.lua
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/CauchyGreenValue.cpp
        ${CMAKE_CURRENT_LIST_DIR}/CauchyGreenValue.hpp
)

target_link_libraries(TestUpdateCauchyGreen_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)


morefem_organize_IDE(TestUpdateCauchyGreen_lib Test/Operators/ParameterOperator/UpdateCauchyGreen Test/Operators/ParameterOperator/UpdateCauchyGreen)

add_executable(TestUpdateCauchyGreen)

target_sources(TestUpdateCauchyGreen
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/GlimpseCauchyGreenContent.cpp
        ${CMAKE_CURRENT_LIST_DIR}/GlimpseCauchyGreenContent.hpp
)

target_link_libraries(TestUpdateCauchyGreen
                      TestUpdateCauchyGreen_lib
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestUpdateCauchyGreen Test/Operators/ParameterOperator/UpdateCauchyGreen Test/Operators/ParameterOperator/UpdateCauchyGreen)

add_executable(TestUpdateCauchyGreenTensorUpdateLuaFile
               ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)

target_link_libraries(TestUpdateCauchyGreenTensorUpdateLuaFile
                      TestUpdateCauchyGreen_lib
                      ${MOREFEM_POST_PROCESSING})
                      
morefem_organize_IDE(TestUpdateCauchyGreenTensorUpdateLuaFile Test/Operators/ParameterOperator/UpdateCauchyGreen Test/Operators/ParameterOperator/UpdateCauchyGreen)

morefem_boost_test_sequential_mode(NAME UpdateCauchyGreen3D
                                   EXE TestUpdateCauchyGreen
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Operators/ParameterOperator/UpdateCauchyGreen/demo_3D.lua)

morefem_boost_test_sequential_mode(NAME UpdateCauchyGreen2D
                                   EXE TestUpdateCauchyGreen
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Operators/ParameterOperator/UpdateCauchyGreen/demo_2D.lua)


