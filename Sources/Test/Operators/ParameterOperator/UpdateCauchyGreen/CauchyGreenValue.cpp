// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
 * \file
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 */

// IWYU pragma: private, include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/CauchyGreenValue.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS
{


    const std::string& Initial::NameInFile()
    {
        static const std::string ret("cauchy_green_initial_value");
        return ret;
    }


    const std::string& Initial::Description()
    {
        static const std::string ret("Arbitrary initial Cauchy-Green value put in the tensor");
        return ret;
    }


    const std::string& AfterOperator::NameInFile()
    {
        static const std::string ret("cauchy_green_value_after_operator");
        return ret;
    }


    const std::string& AfterOperator::Description()
    {
        static const std::string ret("Expected Cauchy-Green value after the operator was applied.");
        return ret;
    }


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
