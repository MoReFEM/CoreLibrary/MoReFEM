// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_CAUCHYGREENVALUE_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_CAUCHYGREENVALUE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS
{


    /*!
     * \brief Input data which gives the initial content at a \a QuadraturePoint of the Cauchy-Green tensor.
     */
    // clang-format off
    struct Initial
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
            <
                Initial,
                ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection,
                std::vector<double>
            >
    // clang-format on
    {

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    // clang-format off


    /*!
     * \brief Input data which gives the expected result at a \a QuadraturePoint of the Cauchy-Green tensor after the arbitrary upgrade operator implemented
     * in the test is applied.
     */
    struct AfterOperator
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
            <
                AfterOperator,
                ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection,
                std::vector<double>
            >
    // clang-format on
    {

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_CAUCHYGREENVALUE_DOT_HPP_
// *** MoReFEM end header guards *** < //
