// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/GlimpseCauchyGreenContent.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    auto GlimpseCauchyGreenContent(const Model& model) -> const Model::glimpse_cauchy_green_type&
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) mesh = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::mesh)).GetMesh();

        const auto& [it_begin, it_end] =
            mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(mesh.GetDimension());

        // The tests are run only on specific meshes with only one \a GeometricElt.
        // There are no strong content for that, but no need either to check further.
        BOOST_CHECK_EQUAL(it_end - it_begin, 1L);

        BOOST_CHECK(!(!*it_begin));

        const auto geom_elt_id = (*it_begin)->GetIndex();

        decltype(auto) content = model.GetCauchyGreenTensorStorage();

        auto it = content.find(geom_elt_id);

        BOOST_CHECK(it != content.cend());

        constexpr auto arbitrary_quad_pt_chosen = 2UL;

        BOOST_CHECK(arbitrary_quad_pt_chosen < content.size());

        return it->second[arbitrary_quad_pt_chosen].value;
    }


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
