// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Model/Model.hpp"

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/CauchyGreenValue.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    namespace // anonymous
    {

        //! \tparam WhichT Either InputDataNS::CauchyGreenValueNS::Initial or InputDataNS::CauchyGreenValueNS::AfterOperator.
        template<class WhichT, ::MoReFEM::Concept::InputDataType InputDataT>
        Eigen::VectorXd ReadCauchyGreenValue(const InputDataT& input_data);


    } // namespace


    Model::Model(morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();

        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        decltype(auto) input_data = GetMoReFEMData().GetInputData();

        initial_value_ = ReadCauchyGreenValue<InputDataNS::CauchyGreenValueNS::Initial>(input_data);

        expected_value_ = ReadCauchyGreenValue<InputDataNS::CauchyGreenValueNS::AfterOperator>(input_data);

        decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

        decltype(auto) time_manager = parent::GetTimeManager();

        cauchy_green_tensor_ = std::make_unique<cauchy_green_tensor_type>(
            "Cauchy-Green tensor", felt_space.GetDomain(), quadrature_rule_per_topology, initial_value_, time_manager);
        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        decltype(auto) unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::generic_unknown));

        upgrade_operator_ = std::make_unique<update_cauchy_green_tensor_op_type>(
            felt_space, unknown, *cauchy_green_tensor_, &quadrature_rule_per_topology);

        decltype(auto) numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::generic_numbering_subset));

        displacement_ = std::make_unique<GlobalVector>(numbering_subset);
        AllocateGlobalVector(god_of_dof, *displacement_);
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


    namespace // anonymous
    {

        template<class WhichT, ::MoReFEM::Concept::InputDataType InputDataT>
        Eigen::VectorXd ReadCauchyGreenValue(const InputDataT& input_data)
        {
            const auto value = ::MoReFEM::Internal::InputDataNS::ExtractLeaf<WhichT>::Value(input_data);

            Eigen::VectorXd ret(static_cast<Eigen::Index>(value.size()));

            std::copy(value.cbegin(), value.cend(), ret.begin());

            return ret;
        }


    } // namespace


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
