// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_MODEL_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    //! \copydoc doxygen_hide_model_4_test
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

        //! Alias.
        using cauchy_green_tensor_type = ParameterAtQuadraturePoint<ParameterNS::Type::vector,
                                                                    time_manager_type,
                                                                    ParameterNS::TimeDependencyNS::None,
                                                                    Wrappers::EigenNS::dWVector>;

        //! Alias
        using glimpse_cauchy_green_type = cauchy_green_tensor_type::value_holder_type::type;

        //! Alias to the operatir used to update the Cauchy-Green tensor.
        using update_cauchy_green_tensor_op_type =
            GlobalParameterOperatorNS::UpdateCauchyGreenTensor<time_manager_type>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        //! Manage time iteration.
        void Forward();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();


        ///@}


        /*!
         * \brief Non constant accessor tio displacement.
         */
        GlobalVector& GetNonCstDisplacement() noexcept;

        //! Accessor to upgrade operator.
        const update_cauchy_green_tensor_op_type& GetUpgradeOperator() const noexcept;

        //! Accessor to Cauchy-Green tensor \a Parameter.
        const cauchy_green_tensor_type& GetCauchyGreenTensor() const noexcept;

        //! Accessor to Cauchy-Green tensor underlying storage.
        auto GetCauchyGreenTensorStorage() const noexcept -> const cauchy_green_tensor_type::parameter_storage_type&;


        /*!
         * \brief Access Cauchy-Green initial value.
         *
         * \return Initial value.
         */
        const Eigen::VectorXd& GetInitialValue() const noexcept;

        /*!
         * \brief Access Cauchy-Green expected value after operator is applied (assuming the current arbitrary displacement vector computed in main file).
         *
         * \return Expected value.
         */
        const Eigen::VectorXd& GetExpectedValueAfterOperator() const noexcept;


      private:
        //! Accessor to the directory object which holds the main output directory.
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

      private:
        //! Cauchy-Green tensor.
        typename cauchy_green_tensor_type::unique_ptr cauchy_green_tensor_{ nullptr };

        //! Cauchy-green initial value.
        Eigen::VectorXd initial_value_;

        //! Cauchy-green expected value after operator is applied.
        Eigen::VectorXd expected_value_;

        //! Update operator
        typename update_cauchy_green_tensor_op_type::const_unique_ptr upgrade_operator_{ nullptr };

        //! Displacement vector used to upgrade the Cauchy-Green tensor.
        GlobalVector::unique_ptr displacement_{ nullptr };
    };


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
