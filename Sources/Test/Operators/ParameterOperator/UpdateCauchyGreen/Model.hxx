// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Wed, 6 June 2020 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test UpdateCauchyGreen operator");
        return name;
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    inline void Model::SupplInitializeStep()
    { }


    inline const FilesystemNS::Directory& Model::GetOutputDirectory() const noexcept
    {
        return GetMoReFEMData().GetResultDirectory();
    }


    inline GlobalVector& Model::GetNonCstDisplacement() noexcept
    {
        assert(!(!displacement_));
        return *displacement_;
    }


    inline auto Model::GetCauchyGreenTensor() const noexcept -> const cauchy_green_tensor_type&
    {
        assert(!(!cauchy_green_tensor_));
        return *cauchy_green_tensor_;
    }


    inline auto Model::GetCauchyGreenTensorStorage() const noexcept
        -> const cauchy_green_tensor_type::parameter_storage_type&
    {
        return GetCauchyGreenTensor().GetStorage();
    }


    inline auto Model::GetUpgradeOperator() const noexcept -> const update_cauchy_green_tensor_op_type&
    {
        assert(!(!upgrade_operator_));
        return *upgrade_operator_;
    }


    inline const Eigen::VectorXd& Model::GetInitialValue() const noexcept
    {
        return initial_value_;
    }


    inline const Eigen::VectorXd& Model::GetExpectedValueAfterOperator() const noexcept
    {
        return expected_value_;
    }


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
