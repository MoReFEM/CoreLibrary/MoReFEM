// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Model/Main/MainUpdateLuaFile.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    // NOLINTNEXTLINE(clang-analyzer-optin.cplusplus.VirtualCall)  due to TCLAP bug
    return ModelNS::MainUpdateLuaFile<TestNS::UpdateCauchyGreenTensorNS::Model>(argc, argv);
}
// NOLINTEND(bugprone-exception-escape)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
