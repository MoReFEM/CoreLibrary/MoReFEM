// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Wed, 6 June 2020 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cmath>
#include <cstdlib>

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE update_cauchy_green
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/GlimpseCauchyGreenContent.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::UpdateCauchyGreenTensorNS::Model
    >;
    // clang-format on

    auto ComputeArbitraryDisplacement(TestNS::UpdateCauchyGreenTensorNS::Model& model) -> GlobalVector&;


} // namespace

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(CheckUpdateCauchyGreen, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) content = GlimpseCauchyGreenContent(model);
    BOOST_CHECK_EQUAL(model.GetInitialValue(), content);

    auto& displacement = ComputeArbitraryDisplacement(model);

    decltype(auto) cauchy_green_tensor_operator = model.GetUpgradeOperator();

    cauchy_green_tensor_operator.Update(displacement);

    BOOST_CHECK(model.GetInitialValue() != content); // sanity check...

    decltype(auto) expected_result = model.GetExpectedValueAfterOperator();

    BOOST_CHECK_EQUAL(expected_result.size(), content.size());

    const auto size = static_cast<Eigen::Index>(expected_result.size());

    for (auto i = Eigen::Index{}; i < size; ++i)
        BOOST_CHECK_CLOSE(content[i], expected_result[i], 1.e-13);

    // Doesn't change if reapplied:
    cauchy_green_tensor_operator.Update(displacement);

    for (auto i = Eigen::Index{}; i < size; ++i)
        BOOST_CHECK_CLOSE(content[i], expected_result[i], 1.e-13);
}

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    auto ComputeArbitraryDisplacement(TestNS::UpdateCauchyGreenTensorNS::Model& model) -> GlobalVector&
    {
        decltype(auto) displacement = model.GetNonCstDisplacement();

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(displacement);

            for (auto i = vector_processor_wise_index_type{ 0UL }; i < content.GetSize(); ++i)
            {
                // Arbitrary displacement!
                content[i] = 0.12 * std::cos(i.Get()) - 0.78 * std::sin(i.Get());
            }
        }

        return displacement;
    }


} // namespace
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
