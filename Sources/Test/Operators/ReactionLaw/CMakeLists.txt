# --------- Library for reaction - diffusion tests --------- #
add_library(TestReactionDiffusionLaws_lib INTERFACE "")

target_sources(TestReactionDiffusionLaws_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/CommonModelSettings.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CommonModelSettings.hxx
        ${CMAKE_CURRENT_LIST_DIR}/Fixture.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Fixture.hxx
)

target_link_libraries(TestReactionDiffusionLaws_lib
                      INTERFACE
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_OP_INSTANCES}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestReactionDiffusionLaws_lib Test/Operators/ReactionLaw Test/Operators/ReactionLaw)


# ---------Fitz Hugh Nagumo --------- #
add_executable(TestFitzHughNagumo)

target_sources(TestFitzHughNagumo
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test_fitz_hugh_nagumo.cpp
)
          
target_link_libraries(TestFitzHughNagumo
                      TestReactionDiffusionLaws_lib
                      )

morefem_organize_IDE(TestFitzHughNagumo Test/Operators/ReactionLaw Test/Operators/ReactionLaw)
                  
morefem_boost_test_sequential_mode(NAME FitzHughNagumo
                                   EXE TestFitzHughNagumo
                                   TIMEOUT 10)


# ---------MitchellSchaeffer--------- #
add_executable(TestMitchellSchaeffer)

target_sources(TestMitchellSchaeffer
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test_mitchell_schaeffer.cpp
)
          
target_link_libraries(TestMitchellSchaeffer
                      TestReactionDiffusionLaws_lib
                      )

morefem_organize_IDE(TestMitchellSchaeffer Test/Operators/ReactionLaw Test/Operators/ReactionLaw)
                  
morefem_boost_test_sequential_mode(NAME MitchellSchaeffer
                                   EXE TestMitchellSchaeffer
                                   TIMEOUT 10)


# ---------CourtemancheRamirezNattel--------- #
add_executable(TestCourtemancheRamirezNattel)

target_sources(TestCourtemancheRamirezNattel
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test_courtemanche_ramirez_nattel.cpp
)
          
target_link_libraries(TestCourtemancheRamirezNattel
                      TestReactionDiffusionLaws_lib
                      )

morefem_organize_IDE(TestCourtemancheRamirezNattel Test/Operators/ReactionLaw Test/Operators/ReactionLaw)
                  
morefem_boost_test_sequential_mode(NAME CourtemancheRamirezNattel
                                   EXE TestCourtemancheRamirezNattel
                                   TIMEOUT 10)
