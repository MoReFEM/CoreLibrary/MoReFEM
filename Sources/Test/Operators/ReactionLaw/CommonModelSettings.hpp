// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_REACTIONLAW_COMMONMODELSETTINGS_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_REACTIONLAW_COMMONMODELSETTINGS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Utilities/InputData/SectionsAndLeaves.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::ReactionLawNS
{

    //! Enum class
    enum class MeshIndex : std::size_t { sole = 1UL };

    //! Enum class
    enum class DomainIndex : std::size_t { volume = 1UL };

    //! Enum class
    enum class FEltSpaceIndex : std::size_t { sole = 1UL };

    //! Enum class
    enum class UnknownIndex : std::size_t { potential = 1UL };

    //! Enum class
    enum class NumberingSubsetIndex : std::size_t { sole = 1UL };

    //! \brief Tuple with content for a \a ModelSettings instance.
    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using common_model_settings_tuple = std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::sole),
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>,

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::sole),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::volume),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::volume),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::sole),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::sole),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::potential),

        InputDataNS::TimeManager,

        InputDataNS::Result::DisplayValue
    >;


    //! \copydoc doxygen_hide_model_specific_input_data
    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    template<class DerivedT>
    struct CommonModelSettings
    {

        //! Should be called in the \a DerivedT class at the beginning of \a Init() method.
        void InitCommon();
    };


} // namespace MoReFEM::TestNS::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Test/Operators/ReactionLaw/CommonModelSettings.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_REACTIONLAW_COMMONMODELSETTINGS_DOT_HPP_
// *** MoReFEM end header guards *** < //

