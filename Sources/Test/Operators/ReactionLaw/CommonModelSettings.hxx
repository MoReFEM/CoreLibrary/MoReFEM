// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
 * \file
 *
 */

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_REACTIONLAW_COMMONMODELSETTINGS_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_REACTIONLAW_COMMONMODELSETTINGS_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/ReactionLaw/CommonModelSettings.hpp"
// *** MoReFEM header guards *** < //


#include <filesystem>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::ReactionLawNS
{


    template<class DerivedT>
    void CommonModelSettings<DerivedT>::InitCommon()
    {
        // ****** Mesh ******
        static_cast<DerivedT&>(*this).template SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>(
            { "Sole mesh" });

        static_cast<DerivedT&>(*this).template Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Path>(
            "${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh");
        static_cast<DerivedT&>(*this).template Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Format>(
            "Medit");
        static_cast<DerivedT&>(*this).template Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Dimension>(
            3UL);
        static_cast<DerivedT&>(*this).template Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::SpaceUnit>(
            1.);

        // ****** Domain ******
        static_cast<DerivedT&>(*this)
            .template SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>(
                { "Dimension 3 elements of the mesh" });

        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::sole) });
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::DimensionList>({ 3UL });
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshLabelList>({});
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::GeomEltTypeList>({});


        // ****** Unknown ******
        static_cast<DerivedT&>(*this)
            .template SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential)>>(
                "Scalar unknown");
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential)>::Name>("potential");
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential)>::Nature>("scalar");

        // ****** Numbering subset ******
        static_cast<DerivedT&>(*this)
            .template SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>(
                "Numbering subset");

        // ****** Finite element space ******
        static_cast<DerivedT&>(*this)
            .template SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
                "Finite element space");
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::sole));
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::UnknownList>(
                { "potential" });
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::volume));
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::sole) });
        static_cast<DerivedT&>(*this)
            .template Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::ShapeFunctionList>(
                { "P1" });

        // ****** Time manager ******
        static_cast<DerivedT&>(*this).template Add<InputDataNS::TimeManager::TimeInit>(0.);
        static_cast<DerivedT&>(*this).template Add<InputDataNS::TimeManager::TimeStep>(0.1);
        static_cast<DerivedT&>(*this).template Add<InputDataNS::TimeManager::TimeMax>(0.5);

        static_cast<DerivedT&>(*this).template Add<InputDataNS::Result::DisplayValue>(1);
    }


} // namespace MoReFEM::TestNS::ReactionLawNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_REACTIONLAW_COMMONMODELSETTINGS_DOT_HXX_
// *** MoReFEM end header guards *** < //
