// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
 * \file
 *
 */


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_REACTIONLAW_FIXTURE_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_REACTIONLAW_FIXTURE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/InputData/Advanced/SetFromInputData.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Test/Operators/ReactionLaw/CommonModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::ReactionLawNS
{


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep<>>;


    //! Alias for mode type.
    // clang-format off
    template<class ReactionLawSpecificDataT>
    using model_type = TestNS::BareModel
    <
        MoReFEMDataForTest<typename ReactionLawSpecificDataT::model_settings_type, time_manager_type>,
        DoConsiderProcessorWiseLocal2Global::no
    >;
    // clang-format on


    //! Alias for the fixture parent.
    // clang-format off
    template<class ReactionLawSpecificDataT>
    using fixture_parent_type = TestNS::FixtureNS::ModelNoInputData
    <
        model_type<ReactionLawSpecificDataT>,
        typename ReactionLawSpecificDataT::output_directory_wrapper,
        time_manager_type,
        TestNS::FixtureNS::call_run_method_at_first_call::yes,
        create_domain_list_for_coords::yes
    >;
    // clang-format on


    //! Fixture to use for hyperelastic laws tests.
    template<class ReactionLawSpecificDataT>
    class Fixture : public fixture_parent_type<ReactionLawSpecificDataT>
    {
        //! Alias to parent class.
        using parent = fixture_parent_type<ReactionLawSpecificDataT>;

      public:
        //! Constructor.
        Fixture();

        //! Get the only tetrahedron of the mesh.
        const GeometricElt& GetTetrahedron() const noexcept;

        //! Get the quadrature rules to use.
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

        //! Get the first \a QuadraturePoint of the \a geom_elt.
        //!  \param[in] geom_elt \a GeometricElt for which a \a QuadraturePoint is sought.
        //!  \return First quadrature point found.
        const QuadraturePoint& GetFirstQuadraturePoint(const GeometricElt& geom_elt) const noexcept;


      private:
        //! Storage of all quadrature rules to use.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_{ nullptr };

        //! Only tetrahedron element in the mesg.
        GeometricElt::shared_ptr tetra_{ nullptr };
    };


} // namespace MoReFEM::TestNS::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/ReactionLaw/Fixture.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_REACTIONLAW_FIXTURE_DOT_HPP_
// *** MoReFEM end header guards *** < //
