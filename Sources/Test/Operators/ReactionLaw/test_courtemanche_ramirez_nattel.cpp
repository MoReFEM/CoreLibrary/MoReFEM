// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>
#include <string_view>
#include <tuple>

#define BOOST_TEST_MODULE courtemanche_ramirez_nattel
#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/CourtemancheRamirezNattel.hpp"

#include "Test/Operators/ReactionLaw/CommonModelSettings.hpp"
#include "Test/Operators/ReactionLaw/Fixture.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    //! Helper object to pass string information at compile time.
    struct OutputDirWrapper
    {
        //! The method that does the actual work.
        static constexpr std::string_view Path();
    };


    using crn_model_settings_tuple = std::tuple<

        >;


    // clang-format off
    using model_settings_tuple = Utilities::Tuple::concatenated_type
    <
        TestNS::ReactionLawNS::common_model_settings_tuple,
        crn_model_settings_tuple
    >;
    // clang-format on


    struct ModelSettings : public TestNS::ReactionLawNS::CommonModelSettings<ModelSettings>,
                           ::MoReFEM::ModelSettings<model_settings_tuple>
    {
        using common_parent = TestNS::ReactionLawNS::CommonModelSettings<ModelSettings>;

        void Init() override;
    };


    struct CourtemancheRamizerNattelSpecificData
    {

        using model_settings_type = ModelSettings;

        using output_directory_wrapper = OutputDirWrapper;
    };


    constexpr auto epsilon = NumericNS::DefaultEpsilon<double>();


} // namespace


BOOST_FIXTURE_TEST_CASE(test, TestNS::ReactionLawNS::Fixture<CourtemancheRamizerNattelSpecificData>)
{
    decltype(auto) morefem_data = GetNonCstMoReFEMData();
    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ReactionLawNS::DomainIndex::volume));

    auto courtemanche_ramirez_nattel = Advanced::ReactionLawNS::CourtemancheRamirezNattel<time_manager_type>(
        morefem_data, domain, GetQuadratureRulePerTopology());

    const double arbitrary_local_potential = -0.000213283;

    {
        // In real models the local potential is set by \a NonLinearSource local operator.
        // The value below is an arbitrary value seen in the run of the external ReactionDiffusion model.
        courtemanche_ramirez_nattel.GetNonCstLocalPotential() = arbitrary_local_potential;
    }


    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                          (courtemanche_ramirez_nattel.GetLocalPotential())(arbitrary_local_potential)(epsilon));

    decltype(auto) output_file = morefem_data.GetResultDirectory().AddFile("write_gate.txt");

    courtemanche_ramirez_nattel.WriteGate(output_file);

    {
        auto input_stream = output_file.Read();

        std::string line;

        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line[0], '#');
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line[0], '#');

        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_0;0;0.00291");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_1;0;0.00291");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_2;0;0.00291");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_3;0;0.00291");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_4;0;0.00291");
    }

    {
        decltype(auto) tetra = GetTetrahedron();
        const double expected_result = -5.5809103445045043;
        decltype(auto) result = courtemanche_ramirez_nattel.ReactionLawFunction(
            arbitrary_local_potential, GetFirstQuadraturePoint(tetra), tetra);
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                              (result)(expected_result)(1.e-9)); // default epsilon is too sharp in CI runs
    }
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    constexpr std::string_view OutputDirWrapper::Path()
    {
        return "Operators/ReactionLaw/CourtemancheRamizerNattel";
    }


    void ModelSettings::Init()
    {
        common_parent::InitCommon();
    }


} // namespace
