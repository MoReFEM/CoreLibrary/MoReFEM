// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>
#include <string_view>
#include <tuple>
#include <utility>

#define BOOST_TEST_MODULE mitchell_schaeffer

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/MitchellSchaeffer.hpp"

#include "Test/Operators/ReactionLaw/CommonModelSettings.hpp"
#include "Test/Operators/ReactionLaw/Fixture.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    //! Helper object to pass string information at compile time.
    struct OutputDirWrapper
    {
        //! The method that does the actual work.
        static constexpr std::string_view Path();
    };


    using ms_model_settings_tuple =
        std::tuple<InputDataNS::ReactionNS::MitchellSchaeffer, InputDataNS::InitialConditionGate>;


    // clang-format off
    using model_settings_tuple = Utilities::Tuple::concatenated_type
    <
        TestNS::ReactionLawNS::common_model_settings_tuple,
        ms_model_settings_tuple
    >;
    // clang-format on


    struct ModelSettings : public TestNS::ReactionLawNS::CommonModelSettings<ModelSettings>,
                           ::MoReFEM::ModelSettings<model_settings_tuple>
    {
        using common_parent = TestNS::ReactionLawNS::CommonModelSettings<ModelSettings>;

        void Init() override;
    };


    struct MitchellSchaefferSpecificData
    {

        using model_settings_type = ModelSettings;

        using output_directory_wrapper = OutputDirWrapper;
    };


    constexpr auto epsilon = NumericNS::DefaultEpsilon<double>();

    constexpr auto initial_gate_value = 0.3;


} // namespace


BOOST_FIXTURE_TEST_CASE(test, TestNS::ReactionLawNS::Fixture<MitchellSchaefferSpecificData>)
{
    decltype(auto) morefem_data = GetNonCstMoReFEMData();
    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) domain = domain_manager.GetDomain(AsDomainId(TestNS::ReactionLawNS::DomainIndex::volume));

    auto mitchell_schaeffer = Advanced::ReactionLawNS::MitchellSchaeffer<time_manager_type>(
        morefem_data, domain, GetQuadratureRulePerTopology());

    const double arbitrary_local_potential = -0.000213283;

    {
        // In real models the local potential is set by \a NonLinearSource local operator.
        // The value below is an arbitrary value seen in the run of the external ReactionDiffusion model.
        mitchell_schaeffer.GetNonCstLocalPotential() = arbitrary_local_potential;
    }

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                          (mitchell_schaeffer.GetLocalPotential())(arbitrary_local_potential)(epsilon));

    decltype(auto) output_file = morefem_data.GetResultDirectory().AddFile("write_gate.txt");

    mitchell_schaeffer.WriteGate(output_file);

    {
        auto input_stream = output_file.Read();

        std::string line;

        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line[0], '#');
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line[0], '#');

        // Just to indicate we now exactly what the "0.3" in the file stands for
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (0.3)(initial_gate_value)(epsilon));

        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_0;0;0.3");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_1;0;0.3");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_2;0;0.3");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_3;0;0.3");
        getline(input_stream, line);
        BOOST_CHECK_EQUAL(line, "11;tetrahedron_5_points_4;0;0.3");
    }

    {
        decltype(auto) tetra = GetTetrahedron();
        const double expected_result = 3.5615548383458967e-05;
        decltype(auto) result =
            mitchell_schaeffer.ReactionLawFunction(arbitrary_local_potential, GetFirstQuadraturePoint(tetra), tetra);
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (result)(expected_result)(epsilon));
    }
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    constexpr std::string_view OutputDirWrapper::Path()
    {
        return "Operators/ReactionLaw/MitchellSchaeffer";
    }


    void ModelSettings::Init()
    {
        common_parent::InitCommon();

        Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauIn>(0.2);
        Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauOut>(6.);
        Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauOpen>(120);
        Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauClose::Nature>("constant");
        Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauClose::Value>(302.);
        Add<InputDataNS::ReactionNS::MitchellSchaeffer::PotentialGate>(0.13);
        Add<InputDataNS::ReactionNS::MitchellSchaeffer::PotentialMin>(0.);
        Add<InputDataNS::ReactionNS::MitchellSchaeffer::PotentialMax>(1.);

        auto value = initial_gate_value;
        // NOLINTNEXTLINE([hicpp-move-const-arg,performance-move-const-arg]
        Add<InputDataNS::InitialConditionGate::Value>(std::move(value));

        // < 0. in original model but not very insteresting for a test!
        Add<InputDataNS::InitialConditionGate::WriteGate>(false);
    }


} // namespace
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
