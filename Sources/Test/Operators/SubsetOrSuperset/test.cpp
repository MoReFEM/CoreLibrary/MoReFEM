// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <string>
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Core/Enum.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Nodes_and_dofs/DofIndexesTypes.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#define BOOST_TEST_MODULE conform_projector
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "OperatorInstances/ConformInterpolator/SubsetOrSuperset.hpp"

#include "Test/Operators/SubsetOrSuperset/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    using model_type =
        TestNS::BareModel<TestNS::ConformProjectorNS::morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


    // Create a vector with same value for all velocity dofs and another for all pressure dofs.
    // \param[in] felt_space The \a FEltSpace which meld both unknowns considered
    // \param[in] numbering_subset The \a NumberingSubset which meld both unknowns considered
    // \param[in] prescription1 The value to set for the \a Unknown given as first element of the pair.
    GlobalVector GenerateAdHocVector(const FEltSpace& felt_space,
                                     const NumberingSubset& numbering_subset,
                                     std::pair<const Unknown&, double> prescription1,
                                     std::pair<const Unknown&, double> prescription2);

    // Check all values are the same (including on ghost locations).
    bool CheckUniformValue(const GlobalVector& vector, double expected_value);


} // namespace


BOOST_FIXTURE_TEST_CASE(subset, fixture_type)
{
    decltype(auto) model = GetModel();

    using namespace TestNS::ConformProjectorNS;
    using SubsetOrSuperset = ConformInterpolatorNS::SubsetOrSuperset;

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) felt_space_velocity_pressure =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::velocity_pressure));
    decltype(auto) felt_space_velocity = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::velocity));
    decltype(auto) felt_space_pressure = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::pressure));

    decltype(auto) numbering_subset_manager = Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance();

    decltype(auto) ns_velocity_pressure =
        numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::velocity_pressure));
    decltype(auto) ns_velocity =
        numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::velocity));
    decltype(auto) ns_pressure =
        numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::pressure));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) velocity_unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::velocity));
    decltype(auto) pressure_unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::pressure));

    const GlobalVector velocity_10_pressure_2 = GenerateAdHocVector(felt_space_velocity_pressure,
                                                                    ns_velocity_pressure,
                                                                    { std::ref(velocity_unknown), 10. },
                                                                    { std::ref(pressure_unknown), 2. });

    // ------------
    // (velocity, pressure) -> (velocity)
    // ------------
    auto interpolator_1_2 =
        SubsetOrSuperset(felt_space_velocity_pressure, ns_velocity_pressure, felt_space_velocity, ns_velocity);
    interpolator_1_2.Init();

    GlobalVector velocity_result(ns_velocity);
    AllocateGlobalVector(god_of_dof, velocity_result);

    Wrappers::Petsc::MatMult(interpolator_1_2.GetInterpolationMatrix(), velocity_10_pressure_2, velocity_result);

    BOOST_CHECK(CheckUniformValue(velocity_result, 10.));


    // ------------
    // (velocity, pressure) -> (pressure)
    // ------------
    auto interpolator_1_3 =
        SubsetOrSuperset(felt_space_velocity_pressure, ns_velocity_pressure, felt_space_pressure, ns_pressure);
    interpolator_1_3.Init();

    GlobalVector pressure_result(ns_pressure);
    AllocateGlobalVector(god_of_dof, pressure_result);

    Wrappers::Petsc::MatMult(interpolator_1_3.GetInterpolationMatrix(), velocity_10_pressure_2, pressure_result);

    BOOST_CHECK(CheckUniformValue(pressure_result, 2.));
}


BOOST_FIXTURE_TEST_CASE(superset, fixture_type)
{
    decltype(auto) model = GetModel();

    using namespace TestNS::ConformProjectorNS;
    using SubsetOrSuperset = ConformInterpolatorNS::SubsetOrSuperset;

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) felt_space_velocity_pressure =
        god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::velocity_pressure));
    decltype(auto) felt_space_velocity = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::velocity));
    decltype(auto) felt_space_pressure = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::pressure));

    decltype(auto) numbering_subset_manager = Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance();

    decltype(auto) ns_velocity_pressure =
        numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::velocity_pressure));
    decltype(auto) ns_velocity =
        numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::velocity));
    decltype(auto) ns_pressure =
        numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::pressure));

    decltype(auto) unknown_manager = UnknownManager::GetInstance();
    decltype(auto) velocity_unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::velocity));
    decltype(auto) pressure_unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::pressure));

    const GlobalVector velocity_10_pressure_2 = GenerateAdHocVector(felt_space_velocity_pressure,
                                                                    ns_velocity_pressure,
                                                                    { std::ref(velocity_unknown), 10. },
                                                                    { std::ref(pressure_unknown), 2. });

    GlobalVector velocity_vector(ns_velocity);
    GlobalVector pressure_vector(ns_pressure);
    AllocateGlobalVector(god_of_dof, velocity_vector);
    AllocateGlobalVector(god_of_dof, pressure_vector);

    velocity_vector.SetUniformValue(10.);
    pressure_vector.SetUniformValue(2.);

    GlobalVector global_vector(ns_velocity_pressure);
    AllocateGlobalVector(god_of_dof, global_vector);
    global_vector.ZeroEntries();
    GlobalVector intermediate_step(global_vector);

    auto superset_velocity =
        SubsetOrSuperset(felt_space_velocity, ns_velocity, felt_space_velocity_pressure, ns_velocity_pressure);
    superset_velocity.Init();

    auto superset_pressure =
        SubsetOrSuperset(felt_space_pressure, ns_pressure, felt_space_velocity_pressure, ns_velocity_pressure);
    superset_pressure.Init();

    Wrappers::Petsc::MatMult(superset_velocity.GetInterpolationMatrix(), velocity_vector, intermediate_step);
    Wrappers::Petsc::MatMultAdd(
        superset_pressure.GetInterpolationMatrix(), pressure_vector, intermediate_step, global_vector);

    std::string description_if_failure{};

    BOOST_CHECK(Wrappers::Petsc::AreEqual(global_vector, velocity_10_pressure_2, 1.e-8, description_if_failure));

    if (!description_if_failure.empty())
        std::cerr << description_if_failure << '\n';
}


// This case is in fact handled by an assert. If we change the test tool at some point and use one that is able to
// check this it could be activated.

// BOOST_FIXTURE_TEST_CASE(forbid_invalid_combination, fixture_type)
//{
//     decltype(auto) model = GetModel();
//
//     using namespace TestNS::ConformProjectorNS;
//     using SubsetOrSuperset = ConformInterpolatorNS::SubsetOrSuperset;
//
//     decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
//
//     decltype(auto) felt_space_velocity = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::velocity));
//     decltype(auto) felt_space_pressure = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::pressure));
//
//     decltype(auto) numbering_subset_manager =
//         Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance();
//
//     decltype(auto) ns_velocity =
//     numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::velocity)); decltype(auto)
//     ns_pressure = numbering_subset_manager.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::pressure));
//
//     auto interpolator = SubsetOrSuperset(felt_space_velocity, ns_velocity, felt_space_pressure, ns_pressure);
//
//     interpolator.Init(); // Triggers an assert!
// }


namespace // anonymous
{


    GlobalVector GenerateAdHocVector(const FEltSpace& felt_space,
                                     const NumberingSubset& numbering_subset,
                                     std::pair<const Unknown&, double> prescription1,
                                     std::pair<const Unknown&, double> prescription2)
    {
        GlobalVector ret(numbering_subset);

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();

        AllocateGlobalVector(*god_of_dof_ptr, ret);

        BOOST_REQUIRE(!NumericNS::AreEqual(prescription1.second, prescription2.second, 1.));

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(ret);

            decltype(auto) dof_list = felt_space.GetProcessorWiseDofList();

            for (const auto& dof_ptr : dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;
                const auto proc_wise_index = dof.GetProcessorWiseOrGhostIndex(numbering_subset);
                assert(DofNS::ToVectorIndex(proc_wise_index) < content.GetSize());

                const auto node_ptr = dof.GetNodeFromWeakPtr();

                if (node_ptr->GetUnknown() == prescription1.first)
                    content[DofNS::ToVectorIndex(proc_wise_index)] = prescription1.second;
                else
                {
                    BOOST_REQUIRE(node_ptr->GetUnknown() == prescription2.first);
                    content[DofNS::ToVectorIndex(proc_wise_index)] = prescription2.second;
                }
            }
        }

        return ret;
    }


    bool CheckUniformValue(const GlobalVector& vector, double expected_value)
    {
        const Wrappers::Petsc::AccessGhostContent access_ghost_content(vector);

        const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();

        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content_with_ghost(vector_with_ghost);

        const auto size = content_with_ghost.GetSize();

        for (auto i = vector_processor_wise_index_type{ 0UL }; i < size; ++i)
        {
            if (!NumericNS::AreEqual(content_with_ghost.GetValue(i), expected_value, 1.))
                return false;
        }

        return true;
    }


} // namespace

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
