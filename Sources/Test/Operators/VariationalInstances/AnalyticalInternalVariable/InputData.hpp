// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_INPUTDATA_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Geometry/PseudoNormals.hpp"
#include "Core/InputData/Instances/Parameter/AnalyticalPrestress/AnalyticalPrestress.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
// IWYU pragma: end_exports

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/ConstantTimeStep.hpp"
#include "Core/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::AnalyticalInternalVariable
{


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { displacement = 1, electrical_activation = 2 };

    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { displacement = 1, electrical_activation = 2 };

    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };

    //! \copydoc doxygen_hide_fiber_enum
    enum class FiberIndex : std::size_t { fiber = 1 };

    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        highest_dimension = 1,
        face1 = 2,
        full_mesh = 3,
        all_faces = 4,
        edge1 = 5,
        edge2 = 6,
        edge3 = 7
    };

    //! \copydoc doxygen_hide_boundary_condition_enum
    enum class BoundaryConditionIndex : std::size_t { edge1 = 1, edge2 = 2, edge3 = 3 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { highest_dimension = 1, face1 = 2 };

    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex

    {
        solver = 1
    };

    //! \copydoc doxygen_hide_source_enum
    enum class ForceIndexList {
        surfacic_force_face1 = 1,
    };

    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::electrical_activation)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::all_faces)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge1)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge2)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge3)>,

        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge1)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge2)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge3)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face1)>,

        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>,

        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
        InputDataNS::Solid::CheckInvertedElements,

        InputDataNS::AnalyticalPrestress,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber),
            ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::electrical_activation)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::electrical_activation)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::all_faces)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge1)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge2)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge3)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge1)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge2)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge3)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face1)>::IndexedSectionDescription,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber),
            ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >::IndexedSectionDescription,

        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::IndexedSectionDescription,
        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>::IndexedSectionDescription
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = ::MoReFEM::TimeManagerNS::Instance::ConstantTimeStep<>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::model>;


} // namespace MoReFEM::TestNS::AnalyticalInternalVariable


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
