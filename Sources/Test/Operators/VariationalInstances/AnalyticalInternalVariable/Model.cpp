// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <memory>

#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/Model.hpp"

#include "ThirdParty/Wrappers/Petsc/Print.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/InputData.hpp"
#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/VariationalFormulation.hpp"


namespace MoReFEM::TestNS::AnalyticalInternalVariable
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data, create_domain_list_for_coords::yes)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();

        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        {
            const DirichletBoundaryConditionManager& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = {
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::edge1)),
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::edge2)),
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::edge3))
            };

            const auto& unknown_manager = UnknownManager::GetInstance();

            const auto& displacement = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));
            const auto& electrical_activation =
                unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::electrical_activation));

            variational_formulation_ =
                std::make_unique<VariationalFormulation>(felt_space_volume.GetNumberingSubset(displacement),
                                                         felt_space_volume.GetNumberingSubset(electrical_activation),
                                                         god_of_dof,
                                                         bc_list,
                                                         morefem_data);
        }

        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);

        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();
        const auto& electrical_activation_numbering_subset =
            variational_formulation.GetElectricalActivationNumberingSubset();

        const auto& mpi = GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);


        variational_formulation.SolveNonLinear(displacement_numbering_subset, displacement_numbering_subset);

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        variational_formulation.WriteSolution(GetTimeManager(), electrical_activation_numbering_subset);
    }


    void Model::SupplInitializeStep()
    { }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::AnalyticalInternalVariable


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
