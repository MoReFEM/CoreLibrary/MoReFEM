// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/InputData.hpp"


namespace MoReFEM::TestNS::AnalyticalInternalVariable
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
            { "Finite element space for highest geometric dimension" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face1)>>(
            { "Finite element space for face 1" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>>(
            { " displacement" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::electrical_activation)>>(
            { " electrical_activation" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>({ " displacement" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::electrical_activation)>>(
            { " electrical_activation" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ " full_mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>>({ " face1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
            { " highest_dimension" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::all_faces)>>({ " all_faces" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge1)>>({ " edge1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge2)>>({ " edge2" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge3)>>({ " edge3" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge1)>>(
            { " edge1" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge2)>>(
            { " edge2" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge3)>>(
            { " edge3" });

        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiber),
                                          ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::vector>>({ "Vectorial fiber at node" });

        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Unused" });


        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>>(
            { "Surfacic force on face 1" });
    }


} // namespace MoReFEM::TestNS::AnalyticalInternalVariable


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
