// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <source_location>
#include <tuple>
#include <utility>

#include "VariationalFormulation.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/StaticOrDynamic.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"

#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/InputData.hpp"


namespace MoReFEM::TestNS::AnalyticalInternalVariable
{


    VariationalFormulation::~VariationalFormulation() = default;


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    VariationalFormulation::VariationalFormulation(
        const NumberingSubset& displacement_numbering_subset,
        const NumberingSubset& electrical_activation_numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data),
      displacement_numbering_subset_(displacement_numbering_subset),
      electrical_activation_numbering_subset_(electrical_activation_numbering_subset)
    {
        assert(parent::GetTimeManager().IsTimeStepConstant() && "Current instantiation relies on this assumption!");
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
        const auto& electrical_activation_numbering_subset = GetElectricalActivationNumberingSubset();

        parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        parent::AllocateSystemVector(displacement_numbering_subset);

        parent::AllocateSystemVector(electrical_activation_numbering_subset);

        const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);

        vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_face_1_ = std::make_unique<GlobalVector>(system_rhs);

        matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);

        vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
    }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        const std::size_t degree_of_exactness = 2;
        const std::size_t shape_function_order = 1;

        quadrature_rule_per_topology_parameter_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

        quadrature_rule_per_topology_for_operators_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);
        auto& fiber = FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                ParameterNS::Type::vector,
                                                time_manager_type>::GetInstance()
                          .GetNonCstFiberList(AsFiberListId(FiberIndex::fiber));

        fiber.Initialize(quadrature_rule_per_topology_parameter_.get());

        decltype(auto) domain_full_mesh = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        solid_ = std::make_unique<Solid<time_manager_type>>(
            morefem_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

        input_analytical_prestress_ = std::make_unique<InputAnalyticalPrestress>(morefem_data, domain_full_mesh);

        DefineStaticOperators(morefem_data);
    }


    void VariationalFormulation::DefineStaticOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
        const auto& felt_space_face1 = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::face1));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));


        hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), GetSolid());

        InputAnalyticalPrestress* raw_input_analytical_prestress = input_analytical_prestress_.get();

        stiffness_operator_ = std::make_unique<StiffnessOperatorType>(felt_space_volume,
                                                                      displacement_ptr,
                                                                      displacement_ptr,
                                                                      GetSolid(),
                                                                      parent::GetTimeManager(),
                                                                      hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                                      quadrature_rule_per_topology_for_operators_.get(),
                                                                      raw_input_analytical_prestress);

        decltype(auto) domain_force_face_1 = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::face1));

        using parameter_type_face1 =
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>;

        force_parameter_face_1_ = Init3DCompoundParameterFromInputData<parameter_type_face1>(
            "Surfacic force", domain_force_face_1, morefem_data);

        if (force_parameter_face_1_ != nullptr)
        {
            surfacic_force_operator_face_1_ = std::make_unique<
                GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>>(
                felt_space_face1, displacement_ptr, *force_parameter_face_1_);
        }
    }


    void VariationalFormulation::UpdateVectorsAndMatrices(const GlobalVector& evaluation_state)
    {
        AssembleOperators(evaluation_state);
    }

    void VariationalFormulation::AssembleOperators(const GlobalVector& evaluation_state)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            AssembleNewtonStaticOperators(evaluation_state);
            break;
        case StaticOrDynamic::dynamic_:
            break;
        }
    }


    void VariationalFormulation::AssembleNewtonStaticOperators(const GlobalVector& evaluation_state)
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            const auto& electrical_activation_numbering_subset = GetElectricalActivationNumberingSubset();
            const GlobalVector& u0 = GetSystemRhs(electrical_activation_numbering_subset);
            const GlobalVector& u1 = GetSystemSolution(electrical_activation_numbering_subset);

            const bool do_update_sigma_c = GetSnes().GetSnesIteration() == 0;

            GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            DisplacementGlobalVector(evaluation_state),
                                            PreviousElectricalActivationGlobalVector(u0),
                                            CurrentElectricalActivationGlobalVector(u1),
                                            do_update_sigma_c);
        }

        const std::size_t newton_iteration = GetSnes().GetSnesIteration();

        if (newton_iteration == 0)
        {
            const double time = parent::GetTimeManager().GetTime();

            auto& vector_surfacic_force_face1 = GetNonCstVectorSurfacicForceFace1();
            vector_surfacic_force_face1.ZeroEntries();

            GlobalVectorWithCoefficient vec_face1(vector_surfacic_force_face1, 1.);

            GetSurfacicForceOperatorFace1().Assemble(std::make_tuple(std::ref(vec_face1)), time);
        }
    }


    void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {
        UpdateVectorsAndMatrices(evaluation_state);

        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticResidual(residual);
            break;
        case StaticOrDynamic::dynamic_:
            break;
        }

        ApplyEssentialBoundaryCondition(residual);
    }


    void VariationalFormulation::ComputeStaticResidual(GlobalVector& residual)
    {
        Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual);

        Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace1(), residual);
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters) - true but strong type hindered by not movable object
    void VariationalFormulation::ComputeTangent([[maybe_unused]] const GlobalVector& evaluation_state,
                                                GlobalMatrix& tangent,
                                                [[maybe_unused]] GlobalMatrix& preconditioner)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    {
        assert(tangent.Internal(std::source_location::current())
               == preconditioner.Internal(std::source_location::current()));

        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticTangent(tangent);
            break;
        case StaticOrDynamic::dynamic_:
            break;
        }

        ApplyEssentialBoundaryCondition(tangent);
    }


    void VariationalFormulation::ComputeStaticTangent(GlobalMatrix& tangent)
    {
        Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent);
    }


} // namespace MoReFEM::TestNS::AnalyticalInternalVariable


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
