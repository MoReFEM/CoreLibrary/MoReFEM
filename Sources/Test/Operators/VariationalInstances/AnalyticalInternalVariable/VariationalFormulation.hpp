// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

#include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
#include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"
#include "FormulationSolver/Internal/Snes/SnesInterface.hpp"
#include "FormulationSolver/VariationalFormulation.hpp"

#include "InputData.hpp"


namespace MoReFEM::TestNS::AnalyticalInternalVariable
{


    //! \copydoc doxygen_hide_simple_varf
    class VariationalFormulation final
    : public MoReFEM::VariationalFormulation<VariationalFormulation,
                                             EnumUnderlyingType(SolverIndex::solver),
                                             time_manager_type,
                                             enable_non_linear_solver::yes>,
      public FormulationSolverNS::HyperelasticLaw<VariationalFormulation,
                                                  HyperelasticLawNS::CiarletGeymonat<time_manager_type>>
    {
      private:
        //! \copydoc doxygen_hide_alias_self
        using self = VariationalFormulation;

        //! Alias to the parent class.
        using parent = MoReFEM::VariationalFormulation<VariationalFormulation,
                                                       EnumUnderlyingType(SolverIndex::solver),
                                                       time_manager_type,
                                                       enable_non_linear_solver::yes>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;

        //! Alias to hyperlastic law parent,
        using hyperelastic_law_parent =
            FormulationSolverNS::HyperelasticLaw<VariationalFormulation,
                                                 HyperelasticLawNS::CiarletGeymonat<time_manager_type>>;

        //! Alias to the viscoelasticity policy used.
        using ViscoelasticityPolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<
                time_manager_type>;


        //! Alias to the active stress policy used.
        using InternalVariablePolicy = GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
            InternalVariablePolicyNS ::AnalyticalPrestress<EnumUnderlyingType(FiberIndex::fiber), time_manager_type>;

        //! Alias to the hyperelasticity policy used.
        using HyperelasticityPolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS ::Hyperelasticity<
                typename hyperelastic_law_parent::hyperelastic_law_type>;

        //! Alias to the type of the source parameter.
        using force_parameter_type =
            Parameter<ParameterNS::Type::vector, LocalCoords, time_manager_type, ParameterNS::TimeDependencyNS::None>;

        //! Alias on a pair of Unknown.
        using UnknownPair = std::pair<const Unknown&, const Unknown&>;

        //! Strong type for displacement global vectors.
        using DisplacementGlobalVector =
            StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

        //! Strong type for electrical activation vectors.
        using PreviousElectricalActivationGlobalVector =
            StrongType<const GlobalVector&,
                       struct MoReFEM::GlobalVariationalOperatorNS::PreviousElectricalActivationTag>;

        //! Strong type for electrical activation vectors.
        using CurrentElectricalActivationGlobalVector =
            StrongType<const GlobalVector&,
                       struct MoReFEM::GlobalVariationalOperatorNS::CurrentElectricalActivationTag>;

        //! Alias to the type of the input for the active stress policy.
        using InputAnalyticalPrestress = Advanced::ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::
            InternalVariablePolicyNS::InputAnalyticalPrestress<time_manager_type>;


        //! Friendship to the class which implements the prototyped functions required by Petsc Snes algorithm.
        friend struct Internal::SolverNS::SnesInterface<self>;

      public:
        //! Alias to the stiffness operator type used.
        using StiffnessOperatorType =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor<HyperelasticityPolicy,
                                                                          ViscoelasticityPolicy,
                                                                          InternalVariablePolicy,
                                                                          time_manager_type>;

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \copydoc doxygen_hide_varf_constructor
         *
         * \param[in] displacement_numbering_subset \a NumberingSubset related to displacement.
         * \param[in] electrical_activation_numbering_subset \a NumberingSubset related to electrical activation.
         */
        explicit VariationalFormulation(const NumberingSubset& displacement_numbering_subset,
                                        const NumberingSubset& electrical_activation_numbering_subset,
                                        const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type& morefem_data);

        //! Destructor.
        ~VariationalFormulation() override;

        //! Copy constructor.
        VariationalFormulation(const VariationalFormulation&) = delete;

        //! Move constructor.
        VariationalFormulation(VariationalFormulation&&) = delete;

        //! Copy affectation.
        VariationalFormulation& operator=(const VariationalFormulation&) = delete;

        //! Move affectation.
        VariationalFormulation& operator=(VariationalFormulation&&) = delete;

        ///@}

        /*!
         * \brief Get the displacement numbering subset relevant for this VariationalFormulation.
         *
         * There is a more generic accessor in the base class but its use is more unwieldy.
         *
         * \return \a NumberingSubset related to displacement.
         */
        const NumberingSubset& GetDisplacementNumberingSubset() const;

        /*!
         * \brief Get the displacement numbering subset relevant for this VariationalFormulation.
         *
         * There is a more generic accessor in the base class but its use is more unwieldy.
         *
         * \return \a NumberingSubset related to electrical activation.
         */
        const NumberingSubset& GetElectricalActivationNumberingSubset() const;

      private:
        /// \name CRTP-required methods.
        ///@{

        //! \copydoc doxygen_hide_varf_suppl_init
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();

        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

        ///@}


      private:
        /*!
         * \brief Assemble method for the mass operator.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleStaticOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Assemble method for all the dynamic operators.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Assemble method for all the static operators.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleNewtonStaticOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
         * and the residual.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         *
         */
        void UpdateVectorsAndMatrices(const GlobalVector& evaluation_state);

        //! \copydoc doxygen_hide_compute_tangent
        void ComputeTangent(const GlobalVector& evaluation_state, GlobalMatrix& tangent, GlobalMatrix& preconditioner);

        //! Compute the matrix of the system for a static case.
        //! \copydoc doxygen_hide_out_tangent_arg
        void ComputeStaticTangent(GlobalMatrix& tangent);

        //! Compute the residual for a static case.
        //! \copydoc doxygen_hide_out_residual_arg
        void ComputeStaticResidual(GlobalVector& residual);

        //! \copydoc doxygen_hide_compute_residual
        void ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual);

      private:
        /*!
         * \brief Define the properties of all the static global variational operators involved.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        void DefineStaticOperators(const morefem_data_type& morefem_data);

        //! Get the hyperelastic stiffness operator.
        const StiffnessOperatorType& GetStiffnessOperator() const noexcept;

        //! Accessor to the surfacic source operator on face 1.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperatorFace1() const noexcept;

      private:
        /// \name Global variational operators.
        ///@{

        //! Stiffness operator.
        StiffnessOperatorType::const_unique_ptr stiffness_operator_ = nullptr;

        //! Surfacic source operator on face1.
        GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>::const_unique_ptr
            surfacic_force_operator_face_1_ = nullptr;

        ///@}

      private:
        /// \name Accessors to the global vectors and matrices managed by the class.
        ///@{

        //! Accessor.
        const GlobalVector& GetVectorStiffnessResidual() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorStiffnessResidual() noexcept;

        //! Accessor.
        const GlobalMatrix& GetMatrixTangentStiffness() const noexcept;

        //! Non constant accessor.
        GlobalMatrix& GetNonCstMatrixTangentStiffness() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForceFace1() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorSurfacicForceFace1() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorCurrentDisplacement() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorCurrentDisplacement() noexcept;

        ///@}

        //! Access to the solid.
        const Solid<time_manager_type>& GetSolid() const noexcept;

      private:
        /// \name Global vectors and matrices specific to the problem.
        ///@{

        //! Stiffness residual vector.
        GlobalVector::unique_ptr vector_stiffness_residual_ = nullptr;

        //! Residual of the transient source on face1.
        GlobalVector::unique_ptr vector_surfacic_force_face_1_ = nullptr;

        //! Matrix stiffness tangent.
        GlobalMatrix::unique_ptr matrix_tangent_stiffness_ = nullptr;

        //! Displacement from previous time iteration.
        GlobalVector::unique_ptr vector_current_displacement_ = nullptr;


        ///@}

      private:
        //! Quadrature rule topology used for the parameters.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_parameter_ = nullptr;

        //! Quadrature rule topology used for the operators.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_ = nullptr;

      private:
        /// \name Numbering subsets used in the formulation.
        ///@{

        //! Numbering subset related to displacement.
        const NumberingSubset& displacement_numbering_subset_;

        //! Numbering subset related to the strawman unknown used in the InternalVariablePolicy.
        const NumberingSubset& electrical_activation_numbering_subset_;

        ///@}

      private:
        //! Material parameters of the solid.
        typename Solid<time_manager_type>::const_unique_ptr solid_ = nullptr;

        //! Struct that stores the input data for the InternalVariablePolicy.
        InputAnalyticalPrestress::unique_ptr input_analytical_prestress_ = nullptr;

        //! Force parameter for the static force on face1.
        force_parameter_type::unique_ptr force_parameter_face_1_ = nullptr;
    };


} // namespace MoReFEM::TestNS::AnalyticalInternalVariable


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "VariationalFormulation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
