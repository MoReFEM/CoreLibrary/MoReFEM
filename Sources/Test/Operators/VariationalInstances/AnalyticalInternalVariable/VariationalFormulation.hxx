// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::TestNS::AnalyticalInternalVariable
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    inline const VariationalFormulation::StiffnessOperatorType&
    VariationalFormulation::GetStiffnessOperator() const noexcept
    {
        assert(!(!stiffness_operator_));
        return *stiffness_operator_;
    }


    inline const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
    VariationalFormulation::GetSurfacicForceOperatorFace1() const noexcept
    {
        assert(!(!surfacic_force_operator_face_1_));
        return *surfacic_force_operator_face_1_;
    }


    inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
    {
        assert(!(!vector_stiffness_residual_));
        return *vector_stiffness_residual_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
    {
        assert(!(!matrix_tangent_stiffness_));
        return *matrix_tangent_stiffness_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace1() const noexcept
    {
        assert(!(!vector_surfacic_force_face_1_));
        return *vector_surfacic_force_face_1_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace1() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForceFace1());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentDisplacement() const noexcept
    {
        assert(!(!vector_current_displacement_));
        return *vector_current_displacement_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentDisplacement() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
    }


    inline const Solid<time_manager_type>& VariationalFormulation::GetSolid() const noexcept
    {
        assert(!(!solid_));
        return *solid_;
    }


    inline const NumberingSubset& VariationalFormulation::GetDisplacementNumberingSubset() const
    {
        return displacement_numbering_subset_;
    }


    inline const NumberingSubset& VariationalFormulation::GetElectricalActivationNumberingSubset() const
    {
        return electrical_activation_numbering_subset_;
    }


} // namespace MoReFEM::TestNS::AnalyticalInternalVariable


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_ANALYTICALINTERNALVARIABLE_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
