// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "Model/Main/MainEnsightOutput.hpp"

#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/InputData.hpp"
#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/Model.hpp"

using namespace MoReFEM;


// NOLINTBEGIN(bugprone-exception-escape)
int main(int argc, char** argv)
{
    const std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{
        AsNumberingSubsetId(MoReFEM::TestNS::AnalyticalInternalVariable::NumberingSubsetIndex::displacement),
        AsNumberingSubsetId(MoReFEM::TestNS::AnalyticalInternalVariable::NumberingSubsetIndex::electrical_activation)
    };

    const std::vector<std::string> unknown_list{ "displacement", "electrical_activation" };

    // NOLINTNEXTLINE(clang-analyzer-optin.cplusplus.VirtualCall)  due to TCLAP bug
    return ModelNS::MainEnsightOutput<TestNS::AnalyticalInternalVariable::Model>(
        argc,
        argv,
        AsMeshId(TestNS::AnalyticalInternalVariable::MeshIndex::mesh),
        numbering_subset_id_list,
        unknown_list);
}
// NOLINTEND(bugprone-exception-escape)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
