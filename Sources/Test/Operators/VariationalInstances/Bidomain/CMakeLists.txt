add_library(TestOperatorBidomain_lib ${LIBRARY_TYPE} "")

target_sources(TestOperatorBidomain_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.cpp        
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
)

target_link_libraries(TestOperatorBidomain_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestOperatorBidomain_lib Test/Operators/VariationalInstances/Bidomain Test/Operators/VariationalInstances/Bidomain)                      


add_executable(TestOperatorBidomain ${CMAKE_CURRENT_LIST_DIR}/test.cpp)
target_link_libraries(TestOperatorBidomain TestOperatorBidomain_lib)

morefem_organize_IDE(TestOperatorBidomain Test/Operators/VariationalInstances/Bidomain Test/Operators/VariationalInstances/Bidomain)    

morefem_boost_test_sequential_mode(NAME OperatorBidomain
                                   EXE TestOperatorBidomain
                                   TIMEOUT 20)
