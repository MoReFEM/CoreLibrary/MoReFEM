// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>

#include "Test/Operators/VariationalInstances/Bidomain/ExpectedResults.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Test/Operators/VariationalInstances/Bidomain/ModelSettings.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::Bidomain
{


    namespace // anonymous
    {

        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP1P1();

        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP2P1();

    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix> GetExpectedMatrix(const NumberingSubset& row_numbering_subset,
                                                                           const NumberingSubset& col_numbering_subset)
    {
        const auto row_ns_id = row_numbering_subset.GetUniqueId().Get();
        const auto col_ns_id = col_numbering_subset.GetUniqueId().Get();

        if ((row_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1)
             && col_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1))
            || (row_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1)
                && col_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1)))
        {
            return MatrixP1P1();
        } else if (row_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1)
                   && col_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1))
        {
            return MatrixP2P1();
        }

        assert(false && "Case not foreseen in the test!");
        exit(EXIT_FAILURE);
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP1P1()
        {
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            constexpr auto f1 = 1.1596094780766935e-03;
            constexpr auto f2 = -2.4736511285464901e-04;
            constexpr auto f3 = -2.9441133679104379e-04;
            constexpr auto f4 = -6.1783302843100032e-04;
            constexpr auto f5 = -5.4473022570929815e-04;
            constexpr auto f6 = 2.0604303031007001e-04;
            constexpr auto f7 = 6.5543904510829665e-06;
            constexpr auto f8 = 3.4767692093496073e-05;
            constexpr auto f9 = 1.3108780902165935e-05;
            constexpr auto f10 = 6.9535384186992147e-05;
            constexpr auto f11 = 2.3270762317952656e-04;
            constexpr auto f12 = 5.5149323160434207e-05;
            constexpr auto f13 = 1.1029864632086843e-04;
            constexpr auto f14 = -6.1783302843100042e-04;
            constexpr auto f15 = 5.2791601317707013e-04;

            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { f1, f1, f2, f2, f3, f3, f4, f4 },
                { f1, 2.4692189561533865e-03, f2, f5, f3, -6.3882267358208750e-04, f4, -1.2856660568620010e-03 },
                { f2, f2, f6, f6, f7, f7, f8, f8 },
                { f2, f5, f6, 4.6208606062014005e-04, f7, f9, f8, f10 },
                { f3, f3, f7, f7, f11, f11, f12, f12 },
                { f3, -6.3882267358208761e-04, f7, f9, f11, 5.1541524635905314e-04, f12, f13 },
                { f14, f14, f8, f8, f12, f12, f15, f15 },
                { f14, -1.2856660568620008e-03, f8, f10, f12, f13, f15, 1.1058320263541404e-03 }
            };
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

            assert(false);
            exit(EXIT_FAILURE);
        }


        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP2P1()
        {
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            constexpr auto f1 = -1.2346261419869870e-04;
            constexpr auto f2 = 2.8102918741260647e-06;
            constexpr auto f3 = 7.4234211133983812e-05;
            constexpr auto f4 = 4.6418111190588863e-05;
            constexpr auto f5 = 2.5547044272748409e-05;
            constexpr auto f6 = -2.8849152809167586e-06;
            constexpr auto f7 = -3.3609239448408643e-06;
            constexpr auto f8 = -1.9301205046990787e-05;
            constexpr auto f9 = -6.2394852221924092e-05;
            constexpr auto f10 = 9.2305405290689425e-06;
            constexpr auto f11 = 1.4650859304929972e-05;
            constexpr auto f12 = 3.8513452387925184e-05;
            constexpr auto f13 = -1.3289669927179131e-05;
            constexpr auto f14 = 3.4164035621714751e-06;
            constexpr auto f15 = 4.0458542732297392e-06;
            constexpr auto f16 = 5.8274120917779144e-06;
            constexpr auto f17 = 9.0236859713220281e-04;
            constexpr auto f18 = -1.5732428717610184e-05;
            constexpr auto f19 = -3.0729457006628194e-04;
            constexpr auto f20 = -5.7934159834831082e-04;
            constexpr auto f21 = -5.7883235020995331e-04;
            constexpr auto f22 = 2.1213171213152327e-04;
            constexpr auto f23 = 2.4834941186123910e-04;
            constexpr auto f24 = 1.1835122621719083e-04;
            constexpr auto f25 = 1.0591505621604463e-03;
            constexpr auto f26 = -2.7167418116101204e-04;
            constexpr auto f27 = -1.3283178033666882e-04;
            constexpr auto f28 = -6.5464460066276553e-04;
            constexpr auto f29 = 6.0462502525813458e-04;
            constexpr auto f30 = -2.1294419957892800e-04;
            constexpr auto f31 = -2.9716474544710437e-04;
            constexpr auto f32 = -9.4516080232102132e-05;
            constexpr auto f33 = -8.6936056117704473e-04;
            constexpr auto f34 = 2.2145660776806044e-04;
            constexpr auto f35 = 7.8775786996890430e-05;
            constexpr auto f36 = 5.6912816641209365e-04;
            constexpr auto f37 = -9.4435118108873240e-04;
            constexpr auto f38 = 5.4190168873516809e-05;
            constexpr auto f39 = 3.2059589622462291e-04;
            constexpr auto f40 = 5.6956511599059285e-04;


            return expected_results_type<LinearAlgebraNS::type::matrix>{ { f1, f1, f2, f2, f3, f3, f4, f4 },
                                                                         { f1,
                                                                           -2.4692522839739740e-04,
                                                                           f2,
                                                                           5.6205837482521193e-06,
                                                                           f3,
                                                                           1.4846842226796762e-04,
                                                                           f4,
                                                                           9.2836222381177726e-05 },
                                                                         { f5, f5, f6, f6, f7, f7, f8, f8 },
                                                                         { f5,
                                                                           5.1094088545496817e-05,
                                                                           f6,
                                                                           -5.7698305618335274e-06,
                                                                           f7,
                                                                           -6.7218478896817295e-06,
                                                                           f8,
                                                                           -3.8602410093981581e-05 },
                                                                         { f9, f9, f10, f10, f11, f11, f12, f12 },
                                                                         { f9,
                                                                           -1.2478970444384821e-04,
                                                                           f10,
                                                                           1.8461081058137885e-05,
                                                                           f11,
                                                                           2.9301718609859938e-05,
                                                                           f12,
                                                                           7.7026904775850369e-05 },
                                                                         { f13, f13, f14, f14, f15, f15, f16, f16 },
                                                                         { f13,
                                                                           -2.6579339854358290e-05,
                                                                           f14,
                                                                           6.8328071243429501e-06,
                                                                           f15,
                                                                           8.0917085464594801e-06,
                                                                           f16,
                                                                           1.1654824183555856e-05 },
                                                                         { f17, f17, f18, f18, f19, f19, f20, f20 },
                                                                         { f17,
                                                                           1.9047371942644059e-03,
                                                                           f18,
                                                                           -3.1464857435220368e-05,
                                                                           f19,
                                                                           -6.6458914013256380e-04,
                                                                           f20,
                                                                           -1.2086831966966216e-03 },
                                                                         { f21, f21, f22, f22, f23, f23, f24, f24 },
                                                                         { f21,
                                                                           -1.2576647004199067e-03,
                                                                           f22,
                                                                           4.7426342426304662e-04,
                                                                           f23,
                                                                           5.4669882372247833e-04,
                                                                           f24,
                                                                           2.3670245243438166e-04 },
                                                                         { f25, f25, f26, f26, f27, f27, f28, f28 },
                                                                         { f25,
                                                                           2.2183011243208928e-03,
                                                                           f26,
                                                                           -5.9334836232202422e-04,
                                                                           f27,
                                                                           -2.6566356067333764e-04,
                                                                           f28,
                                                                           -1.3592892013255314e-03 },
                                                                         { f29, f29, f30, f30, f31, f31, f32, f32 },
                                                                         { f29,
                                                                           1.3092500505162688e-03,
                                                                           f30,
                                                                           -4.7588839915785596e-04,
                                                                           f31,
                                                                           -6.4432949089420876e-04,
                                                                           f32,
                                                                           -1.8903216046420424e-04 },
                                                                         { f33, f33, f34, f34, f35, f35, f36, f36 },
                                                                         { f33,
                                                                           -1.8387211223540893e-03,
                                                                           f34,
                                                                           4.9291321553612084e-04,
                                                                           f35,
                                                                           1.5755157399378091e-04,
                                                                           f36,
                                                                           1.1882563328241872e-03 },
                                                                         { f37, f37, f38, f38, f39, f39, f40, f40 },
                                                                         { f37,
                                                                           -1.9887023621774648e-03,
                                                                           f38,
                                                                           1.0838033774703363e-04,
                                                                           f39,
                                                                           6.9119179244924583e-04,
                                                                           f40,
                                                                           1.1891302319811856e-03 } };
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

            assert(false);
            exit(EXIT_FAILURE);
        }


    } // namespace


} // namespace MoReFEM::TestNS::Bidomain

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
