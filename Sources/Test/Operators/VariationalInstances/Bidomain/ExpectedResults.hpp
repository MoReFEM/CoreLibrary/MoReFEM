// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_BIDOMAIN_EXPECTEDRESULTS_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_BIDOMAIN_EXPECTEDRESULTS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/Type.hpp"       // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/EnumUnknown.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS::Bidomain
{


    /*!
     * \brief Convenient alias to choose the underlying storage for the expected result, depending on the
     * \a TypeT template parameter.
     *
     * \tparam TypeT Whether matrix or vector is considered.
     */
    // clang-format off
    template<LinearAlgebraNS::type TypeT>
    using expected_results_type =
        std::conditional_t
        <
            TypeT == LinearAlgebraNS::type::matrix,
            std::vector<std::vector<PetscScalar>>,
            std::vector<PetscScalar>
        >;
    // clang-format on


    /*!
     * \brief Returns the expected matrix when unknown and test unknowns are both P1.
     *
     * \copydoc doxygen_hide_check_bilinear_operator_ns_arg
     *
     * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
     */
    expected_results_type<LinearAlgebraNS::type::matrix> GetExpectedMatrix(const NumberingSubset& row_numbering_subset,
                                                                           const NumberingSubset& col_numbering_subset);


} // namespace MoReFEM::TestNS::Bidomain


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_BIDOMAIN_EXPECTEDRESULTS_DOT_HPP_
// *** MoReFEM end header guards *** < //
