// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <functional>
#include <memory>
#include <tuple>

#include "Model/Model.hpp"

#include "Utilities/Exceptions/Exception.hpp"

#include "Geometry/Coords/Coords.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Bidomain.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "Test/Operators/VariationalInstances/Bidomain/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/Bidomain/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::Bidomain
{


    Model::Model(morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    {
        decltype(auto) mpi = parent::GetMpi();

        if (mpi.Nprocessor<int>() > 1)
        {
            throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                            "a vector; the expected values assume the dof numbering of the sequential case. Please "
                            "run it sequentially.");
        }
    }


    void Model::SupplInitialize()
    {
        constexpr auto degree_of_exactness{ 10 };
        constexpr auto shape_function_order{ 3 };
        quadrature_rule_per_topology_for_operators_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

        // Required to enable construction of an operator after initialization step.
        parent::SetClearGodOfDofTemporaryDataToFalse();

        // clang-format off
        decltype(auto) fibers =
            FiberNS::FiberListManager
            <
                FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector,
                time_manager_type
            >::GetInstance().GetNonCstFiberList(AsFiberListId(FiberIndex::fiber));
        // clang-format on

        fibers.Initialize();
    }


    void Model::SupplFinalize()
    { }


    void Model::CheckOperator(const NumberingSubset& row_numbering_subset,
                              const NumberingSubset& col_numbering_subset) const
    {
        decltype(auto) god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) morefem_data = parent::GetMoReFEMData();

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        Unknown::const_shared_ptr test_transcellular_potential_ptr{};
        Unknown::const_shared_ptr test_extracellular_potential_ptr{};

        switch (row_numbering_subset.GetUniqueId().Get())
        {
        case EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1):
            test_transcellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::transcellular_potential_P1));
            test_extracellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::extracellular_potential_P1));
            break;
        case EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1):
            test_transcellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::other_transcellular_potential_P1));
            test_extracellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::other_extracellular_potential_P1));
            break;
        case EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1):
            test_transcellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::transcellular_potential_P2));
            test_extracellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::extracellular_potential_P2));
            break;
        }

        Unknown::const_shared_ptr transcellular_potential_ptr{};
        Unknown::const_shared_ptr extracellular_potential_ptr{};

        switch (col_numbering_subset.GetUniqueId().Get())
        {
        case EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1):
            transcellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::transcellular_potential_P1));
            extracellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::extracellular_potential_P1));
            break;
        case EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1):
            transcellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::other_transcellular_potential_P1));
            extracellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::other_extracellular_potential_P1));
            break;
        case EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1):
            transcellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::transcellular_potential_P1));
            extracellular_potential_ptr =
                unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::transcellular_potential_P1));
            break;
        }


        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        // clang-format off
        auto intracellular_trans_diffusion_tensor =
            InitScalarParameterFromInputData
            <
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_trans_diffusion_tensor)>
            >("Intracellular Trans Diffusion tensor",
              domain,
              morefem_data);
        
        auto extracellular_trans_diffusion_tensor =
            InitScalarParameterFromInputData
            <
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_trans_diffusion_tensor)>
            >("Extracellular Trans Diffusion tensor",
              domain,
              morefem_data);
                
        auto intracellular_fiber_diffusion_tensor =
        InitScalarParameterFromInputData
        <
            InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_fiber_diffusion_tensor)>
        >("Intracellular Fiber Diffusion tensor",
          domain,
          morefem_data);
        
        auto extracellular_fiber_diffusion_tensor =
        InitScalarParameterFromInputData
        <
            InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_fiber_diffusion_tensor)>
        >("Extracellular Fiber Diffusion tensor",
          domain,
          morefem_data);
        
        decltype(auto) fibers =
            FiberNS::FiberListManager
            <
                FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector,
                time_manager_type
            >::GetInstance().GetFiberList(AsFiberListId(FiberIndex::fiber));
        // clang-format on

        const std::array<Unknown::const_shared_ptr, 2> unknown_pair{ { transcellular_potential_ptr,
                                                                       extracellular_potential_ptr } };

        const std::array<Unknown::const_shared_ptr, 2> test_unknown_pair{ { test_transcellular_potential_ptr,
                                                                            test_extracellular_potential_ptr } };

        const GlobalVariationalOperatorNS::Bidomain<time_manager_type> bidomain_operator(
            felt_space,
            unknown_pair,
            test_unknown_pair,
            *intracellular_trans_diffusion_tensor,
            *extracellular_trans_diffusion_tensor,
            *intracellular_fiber_diffusion_tensor,
            *extracellular_fiber_diffusion_tensor,
            fibers);

        GlobalMatrix matrix(row_numbering_subset, col_numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix);

        matrix.ZeroEntries();

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        bidomain_operator.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

        /* BOOST_CHECK_NO_THROW */ //(
        CheckMatrix(god_of_dof,
                    matrix,
                    GetExpectedMatrix(row_numbering_subset, col_numbering_subset),
                    1.e-10); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace MoReFEM::TestNS::Bidomain


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
