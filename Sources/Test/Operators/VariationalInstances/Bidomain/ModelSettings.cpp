// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/VariationalInstances/Bidomain/ModelSettings.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::Bidomain
{


    void ModelSettings::Init()
    {
        // ****** Numbering subsets ******
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1)>>(
                { "monolithic_p2p1" });
            SetDescription<
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1)>>(
                { "monolithic_unknown_p1" });
            SetDescription<
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1)>>(
                { "monolithic_other_unknown_p1" });

            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::fiber_unknown)>>(
                { "fiber_unknown" });
        }

        // ****** Unknowns ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P1)>>(
                { "transcellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P1)>>(
                { "extracellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_transcellular_potential_P1)>>(
                { "other_transcellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_extracellular_potential_P1)>>(
                { "other_extracellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P2)>>(
                { "transcellular_potential_P2" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P2)>>(
                { "extracellular_potential_P2" });

            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_for_fiber)>>(
                { "vectorial_unknown_for_fiber" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P1)>::Name>(
                { "transcellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P1)>::Name>(
                { "extracellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_transcellular_potential_P1)>::Name>(
                { "other_transcellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_extracellular_potential_P1)>::Name>(
                { "other_extracellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P2)>::Name>(
                { "transcellular_potential_P2" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P2)>::Name>(
                { "extracellular_potential_P2" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_for_fiber)>::Name>(
                { "vectorial_unknown_for_fiber" });


            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P1)>::Nature>(
                { "scalar" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P1)>::Nature>(
                { "scalar" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_transcellular_potential_P1)>::Nature>(
                { "scalar" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_extracellular_potential_P1)>::Nature>(
                { "scalar" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P2)>::Nature>(
                { "scalar" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P2)>::Nature>(
                { "scalar" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_for_fiber)>::Nature>(
                { "vectorial" });
        }

        // ****** Mesh ******
        {
            using input_mesh = InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>;

            SetDescription<input_mesh>({ "mesh" });
            Add<input_mesh::Path>({ "${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh" });
            Add<input_mesh::Format>({ "Medit" });
            Add<input_mesh::Dimension>(3);
            Add<input_mesh::SpaceUnit>(1.);
        }

        // ****** Domains ******
        {
            using input_domain_full_mesh = InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>;
            using input_domain_highest_dim = InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>;

            SetDescription<input_domain_highest_dim>({ "highest_dimension" });
            Add<input_domain_highest_dim::MeshIndexList>({ EnumUnderlyingType(MeshIndex::mesh) });
            Add<input_domain_highest_dim::DimensionList>({ 3 });
            Add<input_domain_highest_dim::MeshLabelList>({});
            Add<input_domain_highest_dim::GeomEltTypeList>({});

            SetDescription<input_domain_full_mesh>({ "full_mesh" });
            Add<input_domain_full_mesh::MeshIndexList>({ EnumUnderlyingType(MeshIndex::mesh) });
            Add<input_domain_full_mesh::DimensionList>({});
            Add<input_domain_full_mesh::MeshLabelList>({});
            Add<input_domain_full_mesh::GeomEltTypeList>({});
        }

        // ****** Finite element space ******
        {
            SetDescription<input_felt_space>({ "Sole finite element space" });

            Add<input_felt_space::GodOfDofIndex>(EnumUnderlyingType(MeshIndex::mesh));
            Add<input_felt_space::DomainIndex>(EnumUnderlyingType(DomainIndex::highest_dimension));
            Add<input_felt_space::UnknownList>({ "transcellular_potential_P1",
                                                 "extracellular_potential_P1",
                                                 "other_transcellular_potential_P1",
                                                 "other_extracellular_potential_P1",
                                                 "transcellular_potential_P2",
                                                 "extracellular_potential_P2",
                                                 "vectorial_unknown_for_fiber" });

            Add<input_felt_space::ShapeFunctionList>({ "P1", "P1", "P1", "P1", "P2", "P2", "P1" });

            constexpr auto monolithic_unknown_p1_ns = EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1);
            constexpr auto monolithic_other_unknown_p1_ns =
                EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1);
            constexpr auto monolithic_p2p1_ns = EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1);

            constexpr auto fiber_numbering_subset = EnumUnderlyingType(NumberingSubsetIndex::fiber_unknown);

            Add<input_felt_space::NumberingSubsetList>({ monolithic_unknown_p1_ns,
                                                         monolithic_unknown_p1_ns,
                                                         monolithic_other_unknown_p1_ns,
                                                         monolithic_other_unknown_p1_ns,
                                                         monolithic_p2p1_ns,
                                                         monolithic_p2p1_ns,
                                                         fiber_numbering_subset });
        }

        // ****** Diffusion ******
        {
            Add<InputDataNS::Diffusion::Density::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Density::Value>(0.2);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_trans_diffusion_tensor)>>(
                { "intracellular_trans_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_trans_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_trans_diffusion_tensor)>::Value>(1.2e-3);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_trans_diffusion_tensor)>>(
                { "extracellular_trans_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_trans_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_trans_diffusion_tensor)>::Value>(1.5e-3);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_fiber_diffusion_tensor)>>(
                { "intracellular_fiber_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_fiber_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_fiber_diffusion_tensor)>::Value>(3.4e-3);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_fiber_diffusion_tensor)>>(
                { "extracellular_fiber_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_fiber_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_fiber_diffusion_tensor)>::Value>(3.7e-3);
        }

        // ****** Fiber vector ******
        {
            SetDescription<fiber_input_section>({ "fiber_vector" });
            Add<fiber_input_section::DomainIndex>(EnumUnderlyingType(DomainIndex::highest_dimension));
            Add<fiber_input_section::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::sole));
            Add<fiber_input_section::UnknownName>("vectorial_unknown_for_fiber");
            Add<fiber_input_section::EnsightFile>(
                "${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/Bidomain/Data/fibers_one_tetra.vct");
        }

        // ****** Rectangular source time parameter ******
        {
            SetDescription<input_rectangular_source_time_parameter>({ "rectangular_source_time_parameter" });
            Add<input_rectangular_source_time_parameter::InitialTimeOfActivationList>(0.);
            Add<input_rectangular_source_time_parameter::FinalTimeOfActivationList>(1.);
        }
    }


} // namespace MoReFEM::TestNS::Bidomain
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
