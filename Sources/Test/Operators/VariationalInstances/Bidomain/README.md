**WARNING** The output of this operator was not validated at the time this test was written (it was when 
the operator was first written many years ago).

The main purpose of current test is to ensure when refactoring occurs that we do not change unwillingly 
the results obtained with former code.

The data used here are not meant to be realistic: for instance for the Fiber file we chose intentionally
a different value for all the nodes, whereas in the full (external) model a most "boring" input file was used
with many 0. and 1. inside.

It's better to avoid too similar or exactly same values in tests as it might hide mistakes in coding.

