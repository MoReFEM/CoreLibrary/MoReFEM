add_library(TestOperatorGradGrad_lib ${LIBRARY_TYPE} "")

target_sources(TestOperatorGradGrad_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hpp        
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P1P1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P2P1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P2P2.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/SameUnknown.cpp
        ${CMAKE_CURRENT_LIST_DIR}/UnknownP1TestP1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/UnknownP2TestP1.cpp
)

target_link_libraries(TestOperatorGradGrad_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestOperatorGradGrad_lib Test/Operators/VariationalInstances/GradGrad Test/Operators/VariationalInstances/GradGrad)                         


add_executable(TestOperatorGradGrad)

target_sources(TestOperatorGradGrad
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo_1D.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_2D.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_3D.lua        
)

target_link_libraries(TestOperatorGradGrad TestOperatorGradGrad_lib)

morefem_organize_IDE(TestOperatorGradGrad Test/Operators/VariationalInstances/GradGrad Test/Operators/VariationalInstances/GradGrad)                         


morefem_boost_test_sequential_mode(NAME OperatorGradGrad3D
                                   EXE TestOperatorGradGrad
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/GradGrad/demo_3D.lua)

morefem_boost_test_sequential_mode(NAME OperatorGradGrad2D
                                   EXE TestOperatorGradGrad
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/GradGrad/demo_2D.lua)

morefem_boost_test_sequential_mode(NAME OperatorGradGrad1D
                                   EXE TestOperatorGradGrad
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/GradGrad/demo_1D.lua)
