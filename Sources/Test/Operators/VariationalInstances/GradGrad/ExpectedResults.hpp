// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_GRADGRAD_EXPECTEDRESULTS_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_GRADGRAD_EXPECTEDRESULTS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/StrongType.hpp" // IWYU pragma: export

#include "FiniteElement/Unknown/EnumUnknown.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS::GradGrad
{


    /*!
     * \brief Convenient alias to choose the underlying storage for the expected result, depending on the
     * \a TypeT template parameter.
     *
     * \tparam TypeT Whether matrix or vector is considered.
     */
    // clang-format off
    template<LinearAlgebraNS::type TypeT>
    using expected_results_type =
        std::conditional_t
        <
            TypeT == LinearAlgebraNS::type::matrix,
            std::vector<std::vector<PetscScalar>>,
            std::vector<PetscScalar>
        >;
    // clang-format on


    /*!
     * \brief Returns the expected matrix when unknown and test unknowns are both P1.
     *
     * \param[in] dimension Dimension of the mesh considered.
     * \param[in] scalar_or_vectorial Whether we're considering a scalar or a vectorial unknown.
     *
     * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
     */
    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP1P1(::MoReFEM::GeometryNS::dimension_type dimension, UnknownNS::Nature scalar_or_vectorial);


    /*!
     * \brief Returns the expected matrix when unknown is P2 and test unknowns is P1.
     *
     * \param[in] dimension Dimension of the mesh considered.
     *
     * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
     */
    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP2P1(::MoReFEM::GeometryNS::dimension_type dimension);

    /*!
     * \brief Returns the expected matrix when unknown is P2 and test unknowns is P2.
     *
     * \param[in] dimension Dimension of the mesh considered.
     *
     * \return The expected content of the matrix, in \a content_type format. The pre-filled matrix is dense.
     */
    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP2P2(::MoReFEM::GeometryNS::dimension_type dimension);


} // namespace MoReFEM::TestNS::GradGrad


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_GRADGRAD_EXPECTEDRESULTS_DOT_HPP_
// *** MoReFEM end header guards *** < //
