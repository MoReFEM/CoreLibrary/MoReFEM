// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "Utilities/LinearAlgebra/Type.hpp"

#include "FiniteElement/Unknown/EnumUnknown.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::GradGrad
{


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D(UnknownNS::Nature scalar_or_vectorial);
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D(UnknownNS::Nature scalar_or_vectorial);
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D();


    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP1P1(::MoReFEM::GeometryNS::dimension_type dimension, UnknownNS::Nature scalar_or_vectorial)
    {
        switch (dimension.Get())
        {
        case 3:
            return Matrix3D(scalar_or_vectorial);
        case 2:
            return Matrix2D(scalar_or_vectorial);
        case 1:
            return Matrix1D();
        default:
            assert(false && "Invalid case!");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D(UnknownNS::Nature scalar_or_vectorial)
        {
            constexpr double one_6th = 1. / 6.;

            switch (scalar_or_vectorial)
            {
            case UnknownNS::Nature::scalar:
                return expected_results_type<LinearAlgebraNS::type::matrix>{
                    { .5, -one_6th, -one_6th, -one_6th },
                    { -one_6th, one_6th, 0., 0. },
                    { -one_6th, 0., one_6th, 0. },
                    { -one_6th, 0., 0., one_6th },
                };
            case UnknownNS::Nature::vectorial:
                return expected_results_type<LinearAlgebraNS::type::matrix>{
                    { .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0. },
                    { 0., .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th, 0. },
                    { 0., 0., .5, 0., 0., -one_6th, 0., 0., -one_6th, 0., 0., -one_6th },

                    { -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0., 0., 0. },
                    { 0., -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0., 0. },
                    { 0., 0., -one_6th, 0., 0., one_6th, 0., 0., 0., 0., 0., 0. },

                    { -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0., 0., 0. },
                    { 0., -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0., 0. },
                    { 0., 0., -one_6th, 0., 0., 0., 0., 0., one_6th, 0., 0., 0. },

                    { -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th, 0., 0. },
                    { 0., -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th, 0. },
                    { 0., 0., -one_6th, 0., 0., 0., 0., 0., 0., 0., 0., one_6th },

                };

            } // switch

            assert(false);
            exit(EXIT_FAILURE);
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D(UnknownNS::Nature scalar_or_vectorial)
        {

            switch (scalar_or_vectorial)
            {
            case UnknownNS::Nature::scalar:
                return expected_results_type<LinearAlgebraNS::type::matrix>{
                    { 1., -.5, -.5 },
                    { -.5, .5, 0. },
                    { -.5, 0., .5 },
                };
            case UnknownNS::Nature::vectorial:
                return expected_results_type<LinearAlgebraNS::type::matrix>{
                    { 1., 0., -.5, 0., -.5, 0. }, { 0., 1., 0., -.5, 0., -.5 }, { -.5, 0., .5, 0., 0., 0. },
                    { 0., -.5, 0., .5, 0., 0. },  { -.5, 0., 0., 0., .5, 0. },  { 0., -.5, 0., 0., 0., .5 }
                };

            } // switch

            assert(false);
            exit(EXIT_FAILURE);
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D()
        {
            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { 1., -1. },
                { -1., 1. },
            };
        }


    } // namespace


} // namespace MoReFEM::TestNS::GradGrad

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
