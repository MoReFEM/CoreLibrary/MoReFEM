// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "Utilities/LinearAlgebra/Type.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::GradGrad
{


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D();


    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP2P1(::MoReFEM::GeometryNS::dimension_type dimension)
    {
        switch (dimension.Get())
        {
        case 3:
            return Matrix3D();
        case 2:
            return Matrix2D();
        case 1:
            return Matrix1D();
        default:
            assert(false && "Invalid case!");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D()
        {
            constexpr double one_3rd = 1. / 3.;
            constexpr double one_6th = 1. / 6.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{ { 0., 0., 0., 0. },
                                                                         { 0., 0., 0., 0. },
                                                                         { 0., 0., 0., 0. },
                                                                         { 0., 0., 0., 0. },
                                                                         { one_3rd, 0., -one_6th, -one_6th },
                                                                         { -one_3rd, one_6th, one_6th, 0. },
                                                                         { one_3rd, -one_6th, 0., -one_6th },
                                                                         { one_3rd, -one_6th, -one_6th, 0. },
                                                                         { -one_3rd, one_6th, 0., one_6th },
                                                                         { -one_3rd, 0., one_6th, one_6th } };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D()
        {
            constexpr double one_3rd = 1. / 3.;
            constexpr double one_6th = 1. / 6.;
            constexpr double two_3rd = 2. * one_3rd;
            constexpr double four_3rd = 4. * one_3rd;

            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { one_3rd, -one_6th, -one_6th }, { -one_6th, one_6th, 0. },       { -one_6th, 0., one_6th },
                { two_3rd, 0., -two_3rd },       { -four_3rd, two_3rd, two_3rd }, { two_3rd, -two_3rd, 0. }
            };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D()
        {
            return expected_results_type<LinearAlgebraNS::type::matrix>{ { 1., -1. }, { -1., 1. }, { 0., 0. } };
        }


    } // namespace


} // namespace MoReFEM::TestNS::GradGrad

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
