// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "Utilities/LinearAlgebra/Type.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::GradGrad
{


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D();


    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP2P2(::MoReFEM::GeometryNS::dimension_type dimension)
    {
        switch (dimension.Get())
        {
        case 3:
            return Matrix3D();
        case 2:
            return Matrix2D();
        case 1:
            return Matrix1D();
        default:
            assert(false && "Invalid case!");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D()
        {
            constexpr double one_30th = 1. / 30.;
            constexpr double one_15th = 1. / 15.;
            constexpr double minus_four_15th = -4. * one_15th;
            constexpr double two_15th = 2. * one_15th;
            constexpr double eight_15th = 8. * one_15th;

            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { .3, one_30th, one_30th, one_30th, -.2, one_15th, -.2, -.2, one_15th, one_15th },
                { one_30th, .1, 0., 0., -two_15th, -one_30th, one_30th, one_30th, -one_30th, 0. },
                { one_30th, 0., .1, 0., one_30th, -one_30th, -two_15th, one_30th, 0., -one_30th },
                { one_30th, 0., 0., .1, one_30th, 0., one_30th, -two_15th, -one_30th, -one_30th },
                { -.2,
                  -two_15th,
                  one_30th,
                  one_30th,
                  .8,
                  minus_four_15th,
                  two_15th,
                  two_15th,
                  minus_four_15th,
                  minus_four_15th },
                { one_15th,
                  -one_30th,
                  -one_30th,
                  0.,
                  minus_four_15th,
                  eight_15th,
                  minus_four_15th,
                  minus_four_15th,
                  two_15th,
                  two_15th },
                { -.2,
                  one_30th,
                  -two_15th,
                  one_30th,
                  two_15th,
                  minus_four_15th,
                  .8,
                  two_15th,
                  minus_four_15th,
                  minus_four_15th },
                { -.2,
                  one_30th,
                  one_30th,
                  -two_15th,
                  two_15th,
                  minus_four_15th,
                  two_15th,
                  .8,
                  minus_four_15th,
                  minus_four_15th },
                { one_15th,
                  -one_30th,
                  0.,
                  -one_30th,
                  minus_four_15th,
                  two_15th,
                  minus_four_15th,
                  minus_four_15th,
                  eight_15th,
                  two_15th },
                { one_15th,
                  0.,
                  -one_30th,
                  -one_30th,
                  minus_four_15th,
                  two_15th,
                  minus_four_15th,
                  minus_four_15th,
                  two_15th,
                  eight_15th },
            };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D()
        {
            constexpr double one_6th = 1. / 6.;
            constexpr double two_3rd = 2. / 3.;
            constexpr double four_3rd = 4. / 3.;
            constexpr double eight_3rd = 8. / 3.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { 1., one_6th, one_6th, -two_3rd, 0., -two_3rd },
                { one_6th, .5, 0., -two_3rd, 0., 0. },
                { one_6th, 0., .5, 0., 0., -two_3rd },
                { -two_3rd, -two_3rd, 0., eight_3rd, -four_3rd, 0. },
                { 0., 0., 0., -four_3rd, eight_3rd, -four_3rd },
                { -two_3rd, 0., -two_3rd, 0., -four_3rd, eight_3rd },
            };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D()
        {
            constexpr auto one_3rd = 1. / 3.;
            constexpr auto eight_3rd = 8. * one_3rd;
            constexpr auto seven_3rd = 7. * one_3rd;

            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { seven_3rd, one_3rd, -eight_3rd },
                { one_3rd, seven_3rd, -eight_3rd },
                { -eight_3rd, -eight_3rd, 16. * one_3rd },
            };
        }


    } // namespace


} // namespace MoReFEM::TestNS::GradGrad

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
