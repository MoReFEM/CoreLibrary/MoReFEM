// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_GRADGRAD_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_GRADGRAD_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::TestNS::GradGrad
{

    inline const std::string& Model::ClassName()
    {
        static std::string name("Test GradGrad operator");
        return name;
    }

} // namespace MoReFEM::TestNS::GradGrad


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_GRADGRAD_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
