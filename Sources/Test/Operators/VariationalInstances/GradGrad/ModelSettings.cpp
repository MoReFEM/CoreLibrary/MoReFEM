// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/VariationalInstances/GradGrad/InputData.hpp"


namespace MoReFEM::TestNS::GradGrad
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P1)>>(
            { " scalar_unknown_P1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_scalar_unknown_P1)>>(
            { " other_scalar_unknown_P1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P2)>>(
            { " scalar_unknown_P2" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial_unknown_P1)>>(
            { " vectorial_unknown_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P1)>>(
            { " scalar_unknown_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_scalar_unknown_P1)>>(
            { " other_scalar_unknown_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P2)>>(
            { " scalar_unknown_P2" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_P1)>>(
            { " vectorial_unknown_P1" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });

        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::GradGrad


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
