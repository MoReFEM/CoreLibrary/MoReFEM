// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <tuple>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/EnumUnknown.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/InputData.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::GradGrad
{


    void Model::SameUnknownP1(UnknownNS::Nature scalar_or_vectorial) const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto unknown_id =
            AsUnknownId(scalar_or_vectorial == UnknownNS::Nature::scalar ? UnknownIndex::scalar_unknown_P1
                                                                         : UnknownIndex::vectorial_unknown_P1);

        const auto numbering_subset_id = AsNumberingSubsetId(scalar_or_vectorial == UnknownNS::Nature::scalar
                                                                 ? NumberingSubsetIndex::scalar_unknown_P1
                                                                 : NumberingSubsetIndex::vectorial_unknown_P1);


        const auto& unknown_ptr = unknown_manager.GetUnknownPtr(unknown_id);

        const GlobalVariationalOperatorNS::GradPhiGradPhi variational_operator(felt_space, unknown_ptr, unknown_ptr);

        decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(numbering_subset_id);

        GlobalMatrix matrix(numbering_subset, numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        matrix.ZeroEntries();

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        variational_operator.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

        /* BOOST_CHECK_NO_THROW */ (
            CheckMatrix(god_of_dof,
                        matrix,
                        GetExpectedMatrixP1P1(dimension, scalar_or_vectorial),
                        1.e-5)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


    void Model::SameUnknownP2() const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& unknown_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::scalar_unknown_P2));

        const GlobalVariationalOperatorNS::GradPhiGradPhi variational_operator(felt_space, unknown_ptr, unknown_ptr);

        decltype(auto) numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::scalar_unknown_P2));

        GlobalMatrix matrix(numbering_subset, numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        matrix.ZeroEntries();

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        variational_operator.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

        /* BOOST_CHECK_NO_THROW */ (
            CheckMatrix(god_of_dof,
                        matrix,
                        GetExpectedMatrixP2P2(dimension),
                        1.e-5)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace MoReFEM::TestNS::GradGrad


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
