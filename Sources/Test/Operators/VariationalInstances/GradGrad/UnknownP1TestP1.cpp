// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <functional>
#include <tuple>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/EnumUnknown.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"

#include "Test/Operators/VariationalInstances/GradGrad/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/InputData.hpp"
#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::GradGrad
{


    void Model::UnknownP1TestP1() const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& scalar_unknown_P1_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::scalar_unknown_P1));

        const auto& other_scalar_unknown_P1_ptr =
            unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::other_scalar_unknown_P1));


        const GlobalVariationalOperatorNS::GradPhiGradPhi variational_operator(
            felt_space, scalar_unknown_P1_ptr, other_scalar_unknown_P1_ptr);

        decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::scalar_unknown_P1));
        decltype(auto) numbering_subset_other_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::other_scalar_unknown_P1));

        GlobalMatrix matrix_p1_p1(numbering_subset_other_p1, numbering_subset_p1);
        AllocateGlobalMatrix(god_of_dof, matrix_p1_p1);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        matrix_p1_p1.ZeroEntries();

        GlobalMatrixWithCoefficient matrix(matrix_p1_p1, 1.);

        variational_operator.Assemble(std::make_tuple(std::ref(matrix)));

        /* BOOST_CHECK_NO_THROW */ (
            CheckMatrix(god_of_dof,
                        matrix_p1_p1,
                        GetExpectedMatrixP1P1(dimension, UnknownNS::Nature::scalar),
                        1.e-5)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace MoReFEM::TestNS::GradGrad


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
