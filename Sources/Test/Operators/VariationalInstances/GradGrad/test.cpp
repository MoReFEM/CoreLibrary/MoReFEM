// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>


#define BOOST_TEST_MODULE grad_grad_operator_3d

#include "Test/Operators/VariationalInstances/GradGrad/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/MacroVariationalOperator.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
       TestNS::GradGrad::Model
    >;
    // clang-format on


} // namespace


TEST_VARIATIONAL_OPERATOR

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
