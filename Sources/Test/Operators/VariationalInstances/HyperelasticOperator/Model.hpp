// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_MODEL_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/VariationalInstances/HyperelasticOperator/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"


namespace MoReFEM::TestNS::HyperelasticOperatorNS
{


    //! \copydoc doxygen_hide_model_4_test
    template<class HyperelasticLawT>
    class Model
    : public ::MoReFEM::Model<Model<HyperelasticLawT>, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>,
      public FormulationSolverNS::HyperelasticLaw<Model<HyperelasticLawT>, HyperelasticLawT>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model<HyperelasticLawT>;

        //! Convenient alias.
        using parent = ::MoReFEM::Model<self, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

        //! Alias to hyperlastic law parent,
        using hyperelastic_law_parent = FormulationSolverNS::HyperelasticLaw<self, HyperelasticLawT>;

        //! Alias to the viscoelasticity policy used.
        using ViscoelasticityPolicyNone =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<
                time_manager_type>;


        //! Alias to the active stress policy used.
        using InternalVariablePolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<
                time_manager_type>;

        //! Alias to the hyperelasticity policy used.
        using hyperelasticity_policy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS ::Hyperelasticity<
                typename hyperelastic_law_parent::hyperelastic_law_type>;

        //! Alias to the stiffness operator type used.
        using StiffnessOperatorType =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor<hyperelasticity_policy,
                                                                          ViscoelasticityPolicyNone,
                                                                          InternalVariablePolicy,
                                                                          time_manager_type>;

        //! Strong type for displacement global vectors.
        using DisplacementGlobalVector =
            StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{

        //! Manage the change of time iteration.
        void Forward();

      private:
        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


      public:
        /*!
         * \brief Case when the test function uses up the same \a Unknown.
         *
         * \param[in] do_assemble_into_matrix Whether we're assembling into a matrix or not.
         * \param[in] do_assemble_into_vector Whether we're assembling into a vector or not.
         *
         * The three cases yes/yes, yes/no and no/yes are considered in the unit tests.
         */
        void SameUnknown(::MoReFEM::Internal::assemble_into_matrix do_assemble_into_matrix,
                         ::MoReFEM::Internal::assemble_into_vector do_assemble_into_vector) const;


        /*!
         * \brief Case when the test function uses up the same \a Unknown but the values are not physical and an
         * exception should be thrown.
         *
         * \param[in] do_assemble_into_matrix Whether we're assembling into a matrix or not.
         * \param[in] do_assemble_into_vector Whether we're assembling into a vector or not.
         *
         * The three cases yes/yes, yes/no and no/yes are considered in the unit tests.
         */
        void SameUnknownInverted(::MoReFEM::Internal::assemble_into_matrix do_assemble_into_matrix,
                                 ::MoReFEM::Internal::assemble_into_vector do_assemble_into_vector) const;


        /*!
         * \brief Case when the test function uses up a different \a Unknown with the same shape label.
         *
         * As the tests are only sequential, the expected result should be the same as for \a SameUnknown (it would
         * be another story in parallel...)
         */
        void UnknownP1TestP1() const;

        /*!
         * \brief Case with a P2 unknown and a P1 test function.
         *
         */
        void UnknownP2TestP1() const;


      private:
        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();


        ///@}


      private:
        //! Material parameters of the solid.
        typename Solid<time_manager_type>::const_unique_ptr solid_ = nullptr;


      private:
        //! Quadrature rule topology used for the operators.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_ = nullptr;
    };


} // namespace MoReFEM::TestNS::HyperelasticOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/VariationalInstances/HyperelasticOperator/Model.hxx"           // IWYU pragma: export
#include "Test/Operators/VariationalInstances/HyperelasticOperator/SameUnknown.hxx"     // IWYU pragma: export
#include "Test/Operators/VariationalInstances/HyperelasticOperator/UnknownP1TestP1.hxx" // IWYU pragma: export
#include "Test/Operators/VariationalInstances/HyperelasticOperator/UnknownP2TestP1.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
