// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/HyperelasticOperator/Model.hpp"
// *** MoReFEM header guards *** < //


#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4J6.hpp"


namespace MoReFEM::TestNS::HyperelasticOperatorNS
{


    template<class HyperelasticLawT>
    Model<HyperelasticLawT>::Model(morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    {
        decltype(auto) mpi = parent::GetMpi();

        if (mpi.template Nprocessor<int>() > 1)
        {
            throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                            "a vector; the expected values assume the dof numbering of the sequential case. Please "
                            "run it sequentially.");
        }
    }


    template<class HyperelasticLawT>
    void Model<HyperelasticLawT>::SupplInitialize()
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetMoReFEMData();

        decltype(auto) domain_manager = DomainManager::GetInstance();

        decltype(auto) domain_volume = domain_manager.GetDomain(AsDomainId(DomainIndex::volume));

        constexpr auto degree_of_exactness{ 10 };
        constexpr auto shape_function_order{ 3 };
        quadrature_rule_per_topology_for_operators_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

        solid_ = std::make_unique<Solid<time_manager_type>>(morefem_data,
                                                            domain_volume,
                                                            quadrature_rule_per_topology,
                                                            -1.); // Negative to cancel CheckConsistancy issues.


        constexpr bool is_I4 = HyperelasticLawT::template IsPresent<InvariantNS::index::I4>();
        constexpr bool is_I6 = HyperelasticLawT::template IsPresent<InvariantNS::index::I6>();

        if constexpr (!is_I4 && !is_I6)
        {
            hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), *solid_);
        } else if constexpr (is_I4 && !is_I6)
        {
            decltype(auto) fiber_list_manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                                          ParameterNS::Type::vector,
                                                                          time_manager_type>::GetInstance();

            decltype(auto) fiberI4 = fiber_list_manager.GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI4));
            fiberI4.Initialize(&quadrature_rule_per_topology);

            hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), *solid_, fiberI4);
        } else if constexpr (is_I4 && is_I6)
        {
            decltype(auto) fiber_list_manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                                          ParameterNS::Type::vector,
                                                                          time_manager_type>::GetInstance();

            decltype(auto) fiberI4 = fiber_list_manager.GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI4));
            fiberI4.Initialize(&quadrature_rule_per_topology);

            decltype(auto) fiberI6 = fiber_list_manager.GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI6));
            fiberI6.Initialize(&quadrature_rule_per_topology);

            hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), *solid_, fiberI4, fiberI6);
        } else
        {
            // Currently `static_assert(false)` is not allowed; see for instance
            // https://stackoverflow.com/questions/44059557/whats-the-right-way-to-call-static-assertfalse
            static_assert(std::is_same<HyperelasticLawT, std::false_type>(),
                          "You must supply the initialization of the hyperelastic law in the test "
                          "Model::SupplInitialize() method!");
        }

        // Required to enable construction of an operator after initialization step.
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    template<class HyperelasticLawT>
    void Model<HyperelasticLawT>::Forward()
    { }


    template<class HyperelasticLawT>
    void Model<HyperelasticLawT>::SupplFinalizeStep()
    { }


    template<class HyperelasticLawT>
    void Model<HyperelasticLawT>::SupplFinalize()
    { }


    template<class HyperelasticLawT>
    inline const std::string& Model<HyperelasticLawT>::ClassName()
    {
        static std::string name("Test for second-piola Kirchhoff operator");
        return name;
    }


    template<class HyperelasticLawT>
    inline bool Model<HyperelasticLawT>::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    template<class HyperelasticLawT>
    inline void Model<HyperelasticLawT>::SupplInitializeStep()
    { }


} // namespace MoReFEM::TestNS::HyperelasticOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
