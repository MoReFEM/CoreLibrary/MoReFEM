// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/VariationalInstances/HyperelasticOperator/ModelSettings.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::HyperelasticOperatorNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p1)>>(
            { " displacement_p1 " });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p2)>>(
            { " displacement_p2" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_displacement_p1)>>(
            { " other_displacement_p1" });


        // ****** Unknowns ******
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>>({ " displacement_p1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>>({ " displacement_p2" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>>(
            { " other_displacement_p1" });

        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>::Name>("displacement_P1");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>::Nature>("vectorial");

        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>::Name>("displacement_P2");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>::Nature>("vectorial");

        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>::Name>(
            "other_displacement_P1");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>::Nature>("vectorial");


        // ****** Mesh ******
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });

        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::Path>("${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::Dimension>(3UL);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::SpaceUnit>(1.);

        // ****** Domain ******
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>({ " volume" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshIndexList>({ 1UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::DimensionList>({ 3UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::GeomEltTypeList>({});

        // ****** FEltSpace  ******
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });

        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::mesh));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::volume));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::UnknownList>(
            { "displacement_P1", "displacement_P2", "other_displacement_P1" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::ShapeFunctionList>({ "P1", "P2", "P1" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::displacement_p1),
              EnumUnderlyingType(NumberingSubsetIndex::displacement_p2),
              EnumUnderlyingType(NumberingSubsetIndex::other_displacement_p1) });

        // ****** TimeManager ******
        Add<InputDataNS::TimeManager::TimeInit>(0.);
        Add<InputDataNS::TimeManager::TimeStep>(0.1);
        Add<InputDataNS::TimeManager::TimeMax>(0.);

        // ****** Solid ******
        Add<InputDataNS::Solid::Mu1::Nature>("constant");
        Add<InputDataNS::Solid::Mu2::Nature>("constant");
        Add<InputDataNS::Solid::C0::Nature>("constant");
        Add<InputDataNS::Solid::C1::Nature>("constant");
        Add<InputDataNS::Solid::C2::Nature>("constant");
        Add<InputDataNS::Solid::C3::Nature>("constant");
        Add<InputDataNS::Solid::C4::Nature>("constant");
        Add<InputDataNS::Solid::C5::Nature>("constant");

        Add<InputDataNS::Solid::Mu1::Value>(700.);
        Add<InputDataNS::Solid::Mu2::Value>(380.);
        Add<InputDataNS::Solid::C0::Value>(85.);
        Add<InputDataNS::Solid::C1::Value>(0.11);
        Add<InputDataNS::Solid::C2::Value>(57.);
        Add<InputDataNS::Solid::C3::Value>(0.13);
        Add<InputDataNS::Solid::C4::Value>(62.);
        Add<InputDataNS::Solid::C5::Value>(0.16);

        Add<InputDataNS::Solid::VolumicMass::Nature>("constant");
        Add<InputDataNS::Solid::VolumicMass::Value>(10.4);

        Add<InputDataNS::Solid::HyperelasticBulk::Nature>("constant");
        Add<InputDataNS::Solid::HyperelasticBulk::Value>(1750000.);

        Add<InputDataNS::Solid::Kappa1::Nature>("constant");
        Add<InputDataNS::Solid::Kappa1::Value>(500.);

        Add<InputDataNS::Solid::Kappa2::Nature>("constant");
        Add<InputDataNS::Solid::Kappa2::Value>(403346.1538461538);

        Add<InputDataNS::Solid::PoissonRatio::Nature>("constant");
        Add<InputDataNS::Solid::PoissonRatio::Value>(0.3);

        Add<InputDataNS::Solid::YoungModulus::Nature>("constant");
        Add<InputDataNS::Solid::YoungModulus::Value>(21.e5);

        Add<InputDataNS::Solid::LameLambda::Nature>("constant");
        Add<InputDataNS::Solid::LameLambda::Value>(1211538.4615384615);

        Add<InputDataNS::Solid::LameMu::Nature>("constant");
        Add<InputDataNS::Solid::LameMu::Value>(807692.3076923076);

        Add<InputDataNS::Solid::Viscosity::Nature>("constant");
        Add<InputDataNS::Solid::Viscosity::Value>(8.2);

        Add<InputDataNS::Solid::CheckInvertedElements>(true);


        // ****** Solver ******
        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });

        Add<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::AbsoluteTolerance>(
            ::MoReFEM::Wrappers::Petsc::absolute_tolerance_type{ 1.e-50 });
        Add<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::GmresRestart>(
            ::MoReFEM::Wrappers::Petsc::set_restart_type{ 200 });
        Add<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::MaxIteration>(
            ::MoReFEM::Wrappers::Petsc::max_iteration_type{ 1000 });
        Add<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::Preconditioner>(
            ::MoReFEM::Wrappers::Petsc::preconditioner_name_type{ "lu" });
        Add<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::RelativeTolerance>(
            ::MoReFEM::Wrappers::Petsc::relative_tolerance_type{ 1.e-9 });
        Add<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::StepSizeTolerance>(
            ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type{ 1.e-8 });
        Add<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::Solver>(
            ::MoReFEM::Wrappers::Petsc::solver_name_type{ "SuperLU_dist" });


        // ****** Result ******
        Add<InputDataNS::Result::OutputDirectory>("${MOREFEM_TEST_OUTPUT_DIR}/Operators/Variational/Hyperelastic/3D");
        Add<InputDataNS::Result::DisplayValue>(1UL);
        Add<InputDataNS::Result::BinaryOutput>(false);


        // ****** Fibers ******
        {
            // clang-format off
            using fiber_type = InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fiberI4),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >;
            // clang-format on

            SetDescription<fiber_type>("Fiber I4 at node");
            Add<fiber_type::EnsightFile>("${MOREFEM_ROOT}/Data/Fiber/fibers_one_tetra.vct");
            Add<fiber_type::DomainIndex>(EnumUnderlyingType(DomainIndex::volume));
            Add<fiber_type::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::sole));
            Add<fiber_type::UnknownName>("displacement_P1");
        }


        {
            // clang-format off
            using fiber_type = InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fiberI6),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >;
            // clang-format on

            SetDescription<fiber_type>("Fiber I6 at node");
            Add<fiber_type::EnsightFile>("${MOREFEM_ROOT}/Data/Fiber/fibers_one_tetra.vct");
            Add<fiber_type::DomainIndex>(EnumUnderlyingType(DomainIndex::volume));
            Add<fiber_type::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::sole));
            Add<fiber_type::UnknownName>("displacement_P1");
        }
    }


} // namespace MoReFEM::TestNS::HyperelasticOperatorNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
