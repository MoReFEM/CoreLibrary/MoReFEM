// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_UNKNOWNP1TESTP1_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_UNKNOWNP1TESTP1_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/HyperelasticOperator/UnknownP1TestP1.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/HyperelasticOperator/ExpectedResults.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::HyperelasticOperatorNS
{


    template<class HyperelasticLawT>
    void Model<HyperelasticLawT>::UnknownP1TestP1() const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& displacement_p1_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement_p1));
        const auto& other_displacement_p1_ptr =
            unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::other_displacement_p1));

        StiffnessOperatorType pk2_operator(felt_space,
                                           other_displacement_p1_ptr,
                                           displacement_p1_ptr,
                                           *solid_,
                                           parent::GetTimeManager(),
                                           hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                           nullptr,
                                           nullptr);

        decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement_p1));
        decltype(auto) numbering_subset_other_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::other_displacement_p1));

        GlobalMatrix matrix_p1_other_p1(numbering_subset_p1, numbering_subset_other_p1);
        AllocateGlobalMatrix(god_of_dof, matrix_p1_other_p1);

        GlobalVector vector_p1(numbering_subset_p1);
        AllocateGlobalVector(god_of_dof, vector_p1);

        GlobalVector previous_iteration_other_p1(numbering_subset_other_p1);
        AllocateGlobalVector(god_of_dof, previous_iteration_other_p1);


        {
            matrix_p1_other_p1.ZeroEntries();
            vector_p1.ZeroEntries();
            previous_iteration_other_p1.ZeroEntries();

            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(
                previous_iteration_other_p1);

            {
                // Arbitrary values; we don't care too much if they're realistic or not.
                const auto size = content.GetSize();

                for (auto index = vector_processor_wise_index_type{ 0UL }; index < size; ++index)
                    content[index] = -2. + 0.32 * static_cast<double>(index.Get());
            }


            GlobalMatrixWithCoefficient matrix(matrix_p1_other_p1, 1.);
            GlobalVectorWithCoefficient vec(vector_p1, 1.);

            pk2_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)),
                                  DisplacementGlobalVector(previous_iteration_other_p1));

            const auto tolerance = 1.e-5;

            /* BOOST_CHECK_NO_THROW */ (
                CheckMatrix(god_of_dof, matrix_p1_other_p1, GetExpectedMatrixP1P1<HyperelasticLawT>(), tolerance));

            /* BOOST_CHECK_NO_THROW */ (
                CheckVector(god_of_dof, vector_p1, GetExpectedVectorP1P1<HyperelasticLawT>(), tolerance));
        }
    }


} // namespace MoReFEM::TestNS::HyperelasticOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_HYPERELASTICOPERATOR_UNKNOWNP1TESTP1_DOT_HXX_
// *** MoReFEM end header guards *** < //
