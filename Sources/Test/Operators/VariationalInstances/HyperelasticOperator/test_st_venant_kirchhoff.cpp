// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <string_view>

#define BOOST_TEST_MODULE second_piola_kirchhoff_operator_st_venant_kirchhoff
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Coords/Coords.hpp"

#include "Operators/LocalVariationalOperator/Internal/EnumClass.hpp"

#include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"

#include "Test/Operators/VariationalInstances/HyperelasticOperator/Model.hpp"
#include "Test/Operators/VariationalInstances/HyperelasticOperator/ModelSettings.hpp"
#include "Test/Tools/Fixture/Enum.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct OutputDirWrapper
    {

        static constexpr std::string_view Path()
        {
            return "${MOREFEM_TEST_OUTPUT_DIR}/Operators/Variational/Hyperelastic/StVenantKirchhoff";
        }
    };

    using model_type = TestNS::HyperelasticOperatorNS::Model<
        HyperelasticLawNS::StVenantKirchhoff<TestNS::HyperelasticOperatorNS::time_manager_type>>;

    // clang-format off
    using fixture_type = TestNS::FixtureNS::ModelNoInputData
    <
        model_type,
        OutputDirWrapper,
        TestNS::HyperelasticOperatorNS::time_manager_type,
        TestNS::FixtureNS::call_run_method_at_first_call::yes,
        create_domain_list_for_coords::yes
    >;
    // clang-format on

    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().SameUnknown(assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
