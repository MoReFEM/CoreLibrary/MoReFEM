// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::Mass
{


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D(UnknownNS::Nature scalar_or_vectorial);
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D(UnknownNS::Nature scalar_or_vectorial);
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D(UnknownNS::Nature scalar_or_vectorial);


    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP1P1(::MoReFEM::GeometryNS::dimension_type dimension, UnknownNS::Nature scalar_or_vectorial)
    {
        switch (dimension.Get())
        {
        case 3:
            return Matrix3D(scalar_or_vectorial);
        case 2:
            return Matrix2D(scalar_or_vectorial);
        case 1:
            return Matrix1D(scalar_or_vectorial);
        default:
            assert(false && "Invalid case!");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D(UnknownNS::Nature scalar_or_vectorial)
        {
            constexpr double one_60th = 1. / 60.;
            constexpr double one_120th = 1. / 120.;

            switch (scalar_or_vectorial)
            {
            case UnknownNS::Nature::scalar:
                return expected_results_type<LinearAlgebraNS::type::matrix>{
                    { one_60th, one_120th, one_120th, one_120th },
                    { one_120th, one_60th, one_120th, one_120th },
                    { one_120th, one_120th, one_60th, one_120th },
                    { one_120th, one_120th, one_120th, one_60th }
                };
            case UnknownNS::Nature::vectorial:
                return expected_results_type<LinearAlgebraNS::type::matrix>{
                    { one_60th, 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_120th, 0., 0. },
                    { 0., one_60th, 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_120th, 0. },
                    { 0., 0., one_60th, 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_120th },
                    { one_120th, 0., 0., one_60th, 0., 0., one_120th, 0., 0., one_120th, 0., 0. },
                    { 0., one_120th, 0., 0., one_60th, 0., 0., one_120th, 0., 0., one_120th, 0. },
                    { 0., 0., one_120th, 0., 0., one_60th, 0., 0., one_120th, 0., 0., one_120th },
                    { one_120th, 0., 0., one_120th, 0., 0., one_60th, 0., 0., one_120th, 0., 0. },
                    { 0., one_120th, 0., 0., one_120th, 0., 0., one_60th, 0., 0., one_120th, 0. },
                    { 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_60th, 0., 0., one_120th },
                    { one_120th, 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_60th, 0., 0. },
                    { 0., one_120th, 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_60th, 0. },
                    { 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_120th, 0., 0., one_60th }
                };

            } // switch

            assert(false);
            exit(EXIT_FAILURE);
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D(UnknownNS::Nature scalar_or_vectorial)
        {
            constexpr double one_12th = 1. / 12.;
            constexpr double one_24th = 1. / 24.;

            switch (scalar_or_vectorial)
            {
            case UnknownNS::Nature::scalar:
                return expected_results_type<LinearAlgebraNS::type::matrix>{ { one_12th, one_24th, one_24th },
                                                                             { one_24th, one_12th, one_24th },
                                                                             { one_24th, one_24th, one_12th } };
            case UnknownNS::Nature::vectorial:
                return expected_results_type<LinearAlgebraNS::type::matrix>{
                    { one_12th, 0., one_24th, 0., one_24th, 0. }, { 0., one_12th, 0., one_24th, 0., one_24th },
                    { one_24th, 0., one_12th, 0., one_24th, 0. }, { 0., one_24th, 0., one_12th, 0., one_24th },
                    { one_24th, 0., one_24th, 0., one_12th, 0. }, { 0., one_24th, 0., one_24th, 0., one_12th },
                };

            } // switch

            assert(false);
            exit(EXIT_FAILURE);
        }


        expected_results_type<LinearAlgebraNS::type::matrix>
        Matrix1D([[maybe_unused]] UnknownNS::Nature scalar_or_vectorial)
        {
            constexpr double one_6th = 1. / 6.;
            constexpr double one_3rd = 1. / 3.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{ { one_3rd, one_6th }, { one_6th, one_3rd } };
        }


    } // namespace


} // namespace MoReFEM::TestNS::Mass

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
