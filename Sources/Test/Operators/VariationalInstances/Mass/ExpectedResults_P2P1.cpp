// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::Mass
{


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D();


    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP2P1(::MoReFEM::GeometryNS::dimension_type dimension)
    {
        switch (dimension.Get())
        {
        case 3:
            return Matrix3D();
        case 2:
            return Matrix2D();
        case 1:
            return Matrix1D();
        default:
            assert(false && "Invalid case!");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D()
        {
            constexpr double one_90th = 1. / 90.;
            constexpr double one_180th = 1. / 180.;
            constexpr double one_360th = 1. / 360.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { 0., -one_360th, -one_360th, -one_360th },   { -one_360th, 0., -one_360th, -one_360th },
                { -one_360th, -one_360th, 0., -one_360th },   { -one_360th, -one_360th, -one_360th, 0. },
                { one_90th, one_90th, one_180th, one_180th }, { one_180th, one_90th, one_90th, one_180th },
                { one_90th, one_180th, one_90th, one_180th }, { one_90th, one_180th, one_180th, one_90th },
                { one_180th, one_90th, one_180th, one_90th }, { one_180th, one_180th, one_90th, one_90th }
            };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D()
        {
            constexpr double one_60th = 1. / 60.;
            constexpr double minus_one_120th = -1. / 120.;
            constexpr double one_15th = 1. / 15.;
            constexpr double one_30th = 1. / 30.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{ { one_60th, minus_one_120th, minus_one_120th },
                                                                         { minus_one_120th, one_60th, minus_one_120th },
                                                                         { minus_one_120th, minus_one_120th, one_60th },
                                                                         { one_15th, one_15th, one_30th },
                                                                         { one_30th, one_15th, one_15th },
                                                                         { one_15th, one_30th, one_15th } };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D()
        {
            constexpr double one_sixth = 1. / 6.;
            constexpr double one_third = 1. / 3.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{ { one_sixth, 0. },
                                                                         { 0., one_sixth },
                                                                         { one_third, one_third } };
        }


    } // namespace


} // namespace MoReFEM::TestNS::Mass


// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
