// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::Mass
{


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D();
        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D();


    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP2P2(::MoReFEM::GeometryNS::dimension_type dimension)
    {
        switch (dimension.Get())
        {
        case 3:
            return Matrix3D();
        case 2:
            return Matrix2D();
        case 1:
            return Matrix1D();
        default:
            assert(false && "Invalid case!");
            exit(EXIT_FAILURE);
        }
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix3D()
        {
            constexpr double one_420th = 1. / 420.;
            constexpr double one_2520th = 1. / 2520.;
            constexpr double one_630th = 1. / 630.;
            constexpr double one_315th = 1. / 315.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{ { one_420th,
                                                                           one_2520th,
                                                                           one_2520th,
                                                                           one_2520th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           -one_420th },
                                                                         { one_2520th,
                                                                           one_420th,
                                                                           one_2520th,
                                                                           one_2520th,
                                                                           -one_630th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           -one_420th },
                                                                         { one_2520th,
                                                                           one_2520th,
                                                                           one_420th,
                                                                           one_2520th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           -one_420th,
                                                                           -one_630th },
                                                                         { one_2520th,
                                                                           one_2520th,
                                                                           one_2520th,
                                                                           one_420th,
                                                                           -one_420th,
                                                                           -one_420th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           -one_630th,
                                                                           -one_630th },
                                                                         { -one_630th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           -one_420th,
                                                                           4. * one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           one_315th },
                                                                         { -one_420th,
                                                                           -one_630th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           2. * one_315th,
                                                                           4. * one_315th,
                                                                           2. * one_315th,
                                                                           one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th },
                                                                         { -one_630th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           4. * one_315th,
                                                                           2. * one_315th,
                                                                           one_315th,
                                                                           2. * one_315th },
                                                                         { -one_630th,
                                                                           -one_420th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           2. * one_315th,
                                                                           one_315th,
                                                                           2. * one_315th,
                                                                           4. * one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th },
                                                                         { -one_420th,
                                                                           -one_630th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           one_315th,
                                                                           2. * one_315th,
                                                                           4. * one_315th,
                                                                           2. * one_315th },
                                                                         { -one_420th,
                                                                           -one_420th,
                                                                           -one_630th,
                                                                           -one_630th,
                                                                           one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           2. * one_315th,
                                                                           4. * one_315th } };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix2D()
        {
            constexpr double one_60th = 1. / 60.;
            constexpr double minus_one_90th = -1. / 90.;
            constexpr double two_45th = 2. / 45.;
            constexpr double four_45th = 4. / 45.;
            constexpr double minus_one_360th = -1. / 360.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { one_60th, minus_one_360th, minus_one_360th, 0., minus_one_90th, 0. },
                { minus_one_360th, one_60th, minus_one_360th, 0., 0., minus_one_90th },
                { minus_one_360th, minus_one_360th, one_60th, minus_one_90th, 0., 0. },
                { 0., 0., minus_one_90th, four_45th, two_45th, two_45th },
                { minus_one_90th, 0., 0., two_45th, four_45th, two_45th },
                { 0., minus_one_90th, 0., two_45th, two_45th, four_45th }
            };
        }


        expected_results_type<LinearAlgebraNS::type::matrix> Matrix1D()
        {
            constexpr auto one_15th = 1. / 15.;
            constexpr auto two_15th = 2. / 15.;
            constexpr auto minus_one_30th = -1. / 30.;

            return expected_results_type<LinearAlgebraNS::type::matrix>{ { two_15th, minus_one_30th, one_15th },
                                                                         { minus_one_30th, two_15th, one_15th },
                                                                         { one_15th, one_15th, 8. * one_15th } };
        }


    } // namespace


} // namespace MoReFEM::TestNS::Mass


// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
