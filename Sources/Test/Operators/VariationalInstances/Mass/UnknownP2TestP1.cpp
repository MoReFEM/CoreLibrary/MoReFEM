// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <functional>
#include <tuple>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/Mass/InputData.hpp"
#include "Test/Operators/VariationalInstances/Mass/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::Mass
{


    void Model::UnknownP2TestP1() const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& potential_1_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::potential_P1));

        const auto& potential_2_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::potential_P2));


        const GlobalVariationalOperatorNS::Mass mass_op(felt_space, potential_1_ptr, potential_2_ptr);

        decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::potential_P1));
        decltype(auto) numbering_subset_p2 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::potential_P2));

        GlobalMatrix matrix(numbering_subset_p2, numbering_subset_p1);
        AllocateGlobalMatrix(god_of_dof, matrix);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        matrix.ZeroEntries();

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        mass_op.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

        /* BOOST_CHECK_NO_THROW */ (
            CheckMatrix(god_of_dof,
                        matrix,
                        GetExpectedMatrixP2P1(dimension),
                        1.e-5)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace MoReFEM::TestNS::Mass


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
