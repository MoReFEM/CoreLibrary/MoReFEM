add_library(MoReFEM4Microsphere_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4Microsphere_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp        
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx
        
)

target_link_libraries(MoReFEM4Microsphere_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)

morefem_organize_IDE(MoReFEM4Microsphere_lib Test/Operators/VariationalInstances/Microsphere Test/Operators/VariationalInstances/Microsphere)   

add_executable(TestMicrosphereOperator)

target_sources(TestMicrosphereOperator
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)

target_link_libraries(TestMicrosphereOperator
                      MoReFEM4Microsphere_lib)

morefem_organize_IDE(TestMicrosphereOperator Test/Operators/VariationalInstances/Microsphere Test/Operators/VariationalInstances/Microsphere)   

add_executable(TestMicrosphereOperatorEnsightOutput
               ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)

target_link_libraries(TestMicrosphereOperatorEnsightOutput
                      MoReFEM4Microsphere_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(TestMicrosphereOperatorEnsightOutput Test/Operators/VariationalInstances/Microsphere Test/Operators/VariationalInstances/Microsphere)                       

add_executable(TestMicrosphereOperatorCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
target_link_libraries(TestMicrosphereOperatorCheckResults
                      MoReFEM4Microsphere_lib
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestMicrosphereOperatorCheckResults Test/Operators/VariationalInstances/Microsphere Test/Operators/VariationalInstances/Microsphere)


morefem_test_run_model_in_both_modes(BOOST_TEST
                                     NAME MicrosphereOperator
                                     LUA ${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/Microsphere/demo.lua
                                     EXE TestMicrosphereOperator
                                     TIMEOUT 30)

morefem_boost_test_check_results(BOOST_TEST
                                 NAME MicrosphereOperator
                                 EXE TestMicrosphereOperatorCheckResults
                                 TIMEOUT 20)
