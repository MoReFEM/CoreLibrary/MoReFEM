Geometry file
Geometry file
node id given
element id assign
coordinates
       8
       1 0.00000e+00 0.00000e+00 0.00000e+00
       2 1.00000e+00 0.00000e+00 0.00000e+00
       3 1.00000e+00 1.00000e+00 0.00000e+00
       4 0.00000e+00 1.00000e+00 0.00000e+00
       5 0.00000e+00 0.00000e+00 1.00000e+00
       6 1.00000e+00 0.00000e+00 1.00000e+00
       7 1.00000e+00 1.00000e+00 1.00000e+00
       8 0.00000e+00 1.00000e+00 1.00000e+00
part       1
MeshLabel_1
quad4
       1
       5       1       4       8
part       2
MeshLabel_2
quad4
       1
       1       2       6       5
part       3
MeshLabel_3
quad4
       1
       1       2       3       4
part       4
MeshLabel_4
quad4
       1
       6       2       3       7
part       5
MeshLabel_5
quad4
       1
       8       7       3       4
part       6
MeshLabel_6
quad4
       1
       5       6       7       8
part       7
MeshLabel_30
hexa8
       1
       1       2       3       4       5       6       7       8
part       8
MeshLabel_201
bar2
       1
       1       2
part       9
MeshLabel_202
bar2
       1
       2       3
part      10
MeshLabel_203
bar2
       1
       3       4
part      11
MeshLabel_204
bar2
       1
       4       1
part      12
MeshLabel_211
bar2
       1
       5       6
part      13
MeshLabel_212
bar2
       1
       6       7
part      14
MeshLabel_213
bar2
       1
       7       8
part      15
MeshLabel_214
bar2
       1
       8       5
part      16
MeshLabel_231
bar2
       1
       8       4
part      17
MeshLabel_232
bar2
       1
       3       7
part      18
MeshLabel_233
bar2
       1
       5       1
part      19
MeshLabel_234
bar2
       1
       2       6
