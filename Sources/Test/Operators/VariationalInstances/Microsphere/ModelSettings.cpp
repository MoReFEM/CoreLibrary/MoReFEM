// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Test/Operators/VariationalInstances/Microsphere/InputData.hpp"


namespace MoReFEM::TestNS::Microsphere
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
            { "Finite element space for geometric elements of the highest dimension" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face1)>>(
            { "Finite element space for face 1" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face2)>>(
            { "Finite element space for face 2" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face3)>>(
            { "Finite element space for face 3" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face4)>>(
            { "Finite element space for face 4" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face5)>>(
            { "Finite element space for face 5" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face6)>>(
            { "Finite element space for face 6" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>>(
            { " displacement" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>({ " displacement" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ " full_mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>>({ " face1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face2)>>({ " face2" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face3)>>({ " face3" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face4)>>({ " face4" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face5)>>({ " face5" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face6)>>({ " face6" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
            { " highest_dimension" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::all_faces)>>({ " all_faces" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge1)>>({ " edge1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge2)>>({ " edge2" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge3)>>({ " edge3" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge1)>>(
            { " edge1" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge2)>>(
            { " edge2" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge3)>>(
            { " edge3" });

        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiberI4),
                                          ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::vector>>({ " First vectorial fiber at node " });
        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiberI6),
                                          ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::vector>>({ " Second vectorial fiber at node " });

        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>>(
            { "Surfacic force on face 1" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face2)>>(
            { "Surfacic force on face 2" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face3)>>(
            { "Surfacic force on face 3" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face4)>>(
            { "Surfacic force on face 4" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face5)>>(
            { "Surfacic force on face 5" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face6)>>(
            { "Surfacic force on face 6" });


        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::Microsphere


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
