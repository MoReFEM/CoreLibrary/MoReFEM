// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <source_location>
#include <tuple>
#include <utility>

#include "VariationalFormulation.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/StaticOrDynamic.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"
#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "ParameterInstances/Fiber/FiberList.hpp"

#include "Test/Operators/VariationalInstances/Microsphere/InputData.hpp"


namespace MoReFEM::TestNS::Microsphere
{


    VariationalFormulation::~VariationalFormulation() = default;


    VariationalFormulation::VariationalFormulation(
        const NumberingSubset& displacement_numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data),
      displacement_numbering_subset_(displacement_numbering_subset)
    {
        assert(parent::GetTimeManager().IsTimeStepConstant() && "Current instantiation relies on this assumption!");
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();

        parent::AllocateSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        parent::AllocateSystemVector(displacement_numbering_subset);

        const auto& system_matrix = GetSystemMatrix(displacement_numbering_subset, displacement_numbering_subset);
        const auto& system_rhs = GetSystemRhs(displacement_numbering_subset);

        vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);

        vector_surfacic_force_face_1_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_face_2_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_face_3_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_face_4_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_face_5_ = std::make_unique<GlobalVector>(system_rhs);
        vector_surfacic_force_face_6_ = std::make_unique<GlobalVector>(system_rhs);

        vector_velocity_at_newton_iteration_ = std::make_unique<GlobalVector>(system_rhs);

        matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);

        vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
        vector_midpoint_position_ = std::make_unique<GlobalVector>(system_rhs);
        vector_midpoint_velocity_ = std::make_unique<GlobalVector>(system_rhs);
        vector_diff_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        matrix_mass_per_square_time_step_ = std::make_unique<GlobalMatrix>(system_matrix);
    }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        constexpr std::size_t degree_of_exactness = 2;
        constexpr std::size_t shape_function_order = 1;

        quadrature_rule_per_topology_parameter_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

        quadrature_rule_per_topology_for_operators_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);

        auto& fiberI4 = FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                  ParameterNS::Type::vector,
                                                  time_manager_type>::GetInstance()
                            .GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI4));

        fiberI4.Initialize(quadrature_rule_per_topology_parameter_.get());

        auto& fiberI6 = FiberNS::FiberListManager<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                                  ParameterNS::Type::vector,
                                                  time_manager_type>::GetInstance()
                            .GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI6));

        fiberI6.Initialize(quadrature_rule_per_topology_parameter_.get());

        decltype(auto) domain_full_mesh = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        solid_ = std::make_unique<Solid<time_manager_type>>(
            morefem_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

        input_microsphere_ = std::make_unique<input_microsphere_type>(
            morefem_data, domain_full_mesh, felt_space_volume.GetQuadratureRulePerTopology());

        DefineStaticOperators(morefem_data);
    }


    void VariationalFormulation::DefineStaticOperators(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
        const auto& felt_space_face1 = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::face1));
        const auto& felt_space_face2 = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::face2));
        const auto& felt_space_face3 = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::face3));
        const auto& felt_space_face4 = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::face4));
        const auto& felt_space_face5 = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::face5));
        const auto& felt_space_face6 = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::face6));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));


        hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), GetSolid());

        input_microsphere_type* raw_input_microsphere = input_microsphere_.get();

        stiffness_operator_ = std::make_unique<StiffnessOperatorType>(felt_space_volume,
                                                                      displacement_ptr,
                                                                      displacement_ptr,
                                                                      GetSolid(),
                                                                      GetTimeManager(),
                                                                      hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                                      quadrature_rule_per_topology_for_operators_.get(),
                                                                      raw_input_microsphere);

        decltype(auto) domain_force_face_1 = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::face1));

        decltype(auto) domain_force_face_2 = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::face2));

        decltype(auto) domain_force_face_3 = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::face3));

        decltype(auto) domain_force_face_4 = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::face4));

        decltype(auto) domain_force_face_5 = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::face5));

        decltype(auto) domain_force_face_6 = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::face6));

        using parameter_type_face1 =
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>;

        force_parameter_face_1_ = Init3DCompoundParameterFromInputData<parameter_type_face1>(
            "Surfacic force", domain_force_face_1, morefem_data);

        using parameter_type_face2 =
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face2)>;

        force_parameter_face_2_ = Init3DCompoundParameterFromInputData<parameter_type_face2>(
            "Surfacic force", domain_force_face_2, morefem_data);

        using parameter_type_face3 =
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face3)>;

        force_parameter_face_3_ = Init3DCompoundParameterFromInputData<parameter_type_face3>(
            "Surfacic force", domain_force_face_3, morefem_data);

        using parameter_type_face4 =
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face4)>;

        force_parameter_face_4_ = Init3DCompoundParameterFromInputData<parameter_type_face4>(
            "Surfacic force", domain_force_face_4, morefem_data);

        using parameter_type_face5 =
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face5)>;

        force_parameter_face_5_ = Init3DCompoundParameterFromInputData<parameter_type_face5>(
            "Surfacic force", domain_force_face_5, morefem_data);

        using parameter_type_face6 =
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face6)>;

        force_parameter_face_6_ = Init3DCompoundParameterFromInputData<parameter_type_face6>(
            "Surfacic force", domain_force_face_6, morefem_data);

        if (force_parameter_face_1_ != nullptr && force_parameter_face_2_ != nullptr
            && force_parameter_face_3_ != nullptr && force_parameter_face_4_ != nullptr
            && force_parameter_face_5_ != nullptr && force_parameter_face_6_ != nullptr)
        {
            surfacic_force_operator_face_1_ =
                std::make_unique<transient_source_type>(felt_space_face1, displacement_ptr, *force_parameter_face_1_);

            surfacic_force_operator_face_2_ =
                std::make_unique<transient_source_type>(felt_space_face2, displacement_ptr, *force_parameter_face_2_);

            surfacic_force_operator_face_3_ =
                std::make_unique<transient_source_type>(felt_space_face3, displacement_ptr, *force_parameter_face_3_);

            surfacic_force_operator_face_4_ =
                std::make_unique<transient_source_type>(felt_space_face4, displacement_ptr, *force_parameter_face_4_);

            surfacic_force_operator_face_5_ =
                std::make_unique<transient_source_type>(felt_space_face5, displacement_ptr, *force_parameter_face_5_);

            surfacic_force_operator_face_6_ =
                std::make_unique<transient_source_type>(felt_space_face6, displacement_ptr, *force_parameter_face_6_);
        }
    }


    void VariationalFormulation::UpdateVectorsAndMatrices(const GlobalVector& evaluation_state)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            break;
        case StaticOrDynamic::dynamic_:
            UpdateDynamicVectorsAndMatrices(evaluation_state);
            break;
        }

        AssembleOperators(evaluation_state);
    }


    void VariationalFormulation::UpdateDynamicVectorsAndMatrices(const GlobalVector& evaluation_state)
    {
        UpdateVelocityAtNewtonIteration(evaluation_state);

        auto& midpoint_position = GetNonCstVectorMidpointPosition();

        midpoint_position.Copy(GetVectorCurrentDisplacement());
        Wrappers::Petsc::AXPY(1., evaluation_state, midpoint_position);

        midpoint_position.Scale(0.5); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

        midpoint_position.UpdateGhosts();

        auto& midpoint_velocity = GetNonCstVectorMidpointVelocity();

        midpoint_velocity.Copy(evaluation_state);

        Wrappers::Petsc::AXPY(-1., GetVectorCurrentDisplacement(), midpoint_velocity);

        midpoint_velocity.Scale(1. / GetTimeManager().GetTimeStep());

        midpoint_velocity.UpdateGhosts();

        GetNonCstVectorCurrentDisplacement().UpdateGhosts();
    }


    void VariationalFormulation::UpdateVelocityAtNewtonIteration(const GlobalVector& evaluation_state)
    {
        auto& velocity_at_newton_iteration = GetNonCstVectorVelocityAtNewtonIteration();
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

        if (GetSnes().GetSnesIteration() == 0)
        {
            velocity_at_newton_iteration.Copy(GetVectorCurrentVelocity());
        } else
        {
            const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();

            auto& diff_displacement = GetNonCstVectorDiffDisplacement();

            diff_displacement.Copy(evaluation_state);
            Wrappers::Petsc::AXPY(-1., displacement_previous_time_iteration, diff_displacement);

            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            diff_displacement.Scale(2. / GetTimeManager().GetTimeStep());
            velocity_at_newton_iteration.Copy(diff_displacement);
            Wrappers::Petsc::AXPY(-1., velocity_previous_time_iteration, velocity_at_newton_iteration);
        }

        velocity_at_newton_iteration.UpdateGhosts();
    }


    void VariationalFormulation::AssembleOperators(const GlobalVector& evaluation_state)
    {
        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            AssembleNewtonStaticOperators(evaluation_state);
            break;
        case StaticOrDynamic::dynamic_:
            AssembleNewtonDynamicOperators();
            break;
        }
    }


    void VariationalFormulation::AssembleNewtonStaticOperators(const GlobalVector& evaluation_state)
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            DisplacementGlobalVector(evaluation_state));
        }

        const std::size_t newton_iteration = GetSnes().GetSnesIteration();

        if (newton_iteration == 0)
        {
            const double time = parent::GetTimeManager().GetTime();

            auto& vector_surfacic_force_face1 = GetNonCstVectorSurfacicForceFace1();
            vector_surfacic_force_face1.ZeroEntries();

            auto& vector_surfacic_force_face2 = GetNonCstVectorSurfacicForceFace2();
            vector_surfacic_force_face2.ZeroEntries();

            auto& vector_surfacic_force_face3 = GetNonCstVectorSurfacicForceFace3();
            vector_surfacic_force_face3.ZeroEntries();

            auto& vector_surfacic_force_face4 = GetNonCstVectorSurfacicForceFace4();
            vector_surfacic_force_face4.ZeroEntries();

            auto& vector_surfacic_force_face5 = GetNonCstVectorSurfacicForceFace5();
            vector_surfacic_force_face5.ZeroEntries();

            auto& vector_surfacic_force_face6 = GetNonCstVectorSurfacicForceFace6();
            vector_surfacic_force_face6.ZeroEntries();

            GlobalVectorWithCoefficient vec_face1(vector_surfacic_force_face1, 1.);
            GlobalVectorWithCoefficient vec_face2(vector_surfacic_force_face2, 1.);
            GlobalVectorWithCoefficient vec_face3(vector_surfacic_force_face3, 1.);
            GlobalVectorWithCoefficient vec_face4(vector_surfacic_force_face4, 1.);
            GlobalVectorWithCoefficient vec_face5(vector_surfacic_force_face5, 1.);
            GlobalVectorWithCoefficient vec_face6(vector_surfacic_force_face6, 1.);

            GetSurfacicForceOperatorFace1().Assemble(std::make_tuple(std::ref(vec_face1)), time);
            GetSurfacicForceOperatorFace2().Assemble(std::make_tuple(std::ref(vec_face2)), time);
            GetSurfacicForceOperatorFace3().Assemble(std::make_tuple(std::ref(vec_face3)), time);
            GetSurfacicForceOperatorFace4().Assemble(std::make_tuple(std::ref(vec_face4)), time);
            GetSurfacicForceOperatorFace5().Assemble(std::make_tuple(std::ref(vec_face5)), time);
            GetSurfacicForceOperatorFace6().Assemble(std::make_tuple(std::ref(vec_face6)), time);
        }
    }

    void VariationalFormulation::AssembleNewtonDynamicOperators()
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            const GlobalVector& displacement_vector = GetVectorMidpointPosition();

            GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            DisplacementGlobalVector(displacement_vector));
        }
    }


    void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {
        UpdateVectorsAndMatrices(evaluation_state);

        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticResidual(residual);
            break;
        case StaticOrDynamic::dynamic_:
            ComputeDynamicResidual(evaluation_state, residual);
            break;
        }

        ApplyEssentialBoundaryCondition(residual);
    }


    void VariationalFormulation::ComputeStaticResidual(GlobalVector& residual)
    {
        Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual);

        Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace1(), residual);

        Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace2(), residual);

        Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace3(), residual);

        Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace4(), residual);

        Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace5(), residual);

        Wrappers::Petsc::AXPY(-1., GetVectorSurfacicForceFace6(), residual);
    }


    void VariationalFormulation::ComputeDynamicResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {
        Wrappers::Petsc::AXPY(1., GetVectorStiffnessResidual(), residual);

        const auto& time_manager = GetTimeManager();
        const double time_step = time_manager.GetTimeStep();
        const auto& displacement_previous_time_iteration = GetVectorCurrentDisplacement();
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();
        auto& diff_displacement = GetNonCstVectorDiffDisplacement();
        diff_displacement.Copy(evaluation_state);

        Wrappers::Petsc::AXPY(-1., displacement_previous_time_iteration, diff_displacement);

        Wrappers::Petsc::AXPY(-time_step, velocity_previous_time_iteration, diff_displacement);

        Wrappers::Petsc::MatMultAdd(GetMatrixMassPerSquareTimeStep(), diff_displacement, residual, residual);
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters) - true but strong type hindered by not movable object
    void VariationalFormulation::ComputeTangent([[maybe_unused]] const GlobalVector& evaluation_state,
                                                GlobalMatrix& tangent,
                                                [[maybe_unused]] GlobalMatrix& preconditioner)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    {
        assert(tangent.Internal(std::source_location::current())
               == preconditioner.Internal(std::source_location::current()));

        switch (GetTimeManager().GetStaticOrDynamic())
        {
        case StaticOrDynamic::static_:
            ComputeStaticTangent(tangent);
            break;
        case StaticOrDynamic::dynamic_:
            ComputeDynamicTangent(tangent);
            break;
        }

        ApplyEssentialBoundaryCondition(tangent);
    }


    void VariationalFormulation::ComputeStaticTangent(GlobalMatrix& tangent)
    {
        tangent.ZeroEntries();

        Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent);
    }


    void VariationalFormulation::ComputeDynamicTangent(GlobalMatrix& tangent)
    {
        tangent.ZeroEntries();

#ifndef NDEBUG
        AssertSameNumberingSubset(GetMatrixTangentStiffness(), tangent);
        AssertSameNumberingSubset(GetMatrixMassPerSquareTimeStep(), tangent);
#endif // NDEBUG

        Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., GetMatrixMassPerSquareTimeStep(), tangent);

        // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        Wrappers::Petsc::AXPY<NonZeroPattern::same>(0.5, GetMatrixTangentStiffness(), tangent);
    }


    void VariationalFormulation::PrepareDynamicRuns()
    {
        DefineDynamicOperators();

        AssembleDynamicOperators();

        UpdateForNextTimeStep();
    }


    void VariationalFormulation::DefineDynamicOperators()
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));

        namespace GVO = GlobalVariationalOperatorNS;

        mass_operator_ = std::make_unique<GVO::Mass>(felt_space_volume, displacement_ptr, displacement_ptr);
    }


    void VariationalFormulation::AssembleDynamicOperators()
    {
        const double volumic_mass = GetSolid().GetVolumicMass().GetConstantValue();

        const double time_step = GetTimeManager().GetTimeStep();

        const double mass_coefficient = 2. * volumic_mass / (time_step * time_step);

        GlobalMatrixWithCoefficient matrix(GetNonCstMatrixMassPerSquareTimeStep(), mass_coefficient);

        GetMassOperator().Assemble(std::make_tuple(std::ref(matrix)));
    }


    void VariationalFormulation::UpdateForNextTimeStep()
    {
        UpdateDisplacementBetweenTimeStep();

        UpdateVelocityBetweenTimeStep();

        // ComputeGuessForNextTimeStep();
    }


    void VariationalFormulation::UpdateDisplacementBetweenTimeStep()
    {
        GetNonCstVectorCurrentDisplacement().Copy(GetSystemSolution(GetDisplacementNumberingSubset()));

        GetNonCstVectorCurrentDisplacement().UpdateGhosts();
    }


    void VariationalFormulation::UpdateVelocityBetweenTimeStep()
    {
        GetNonCstVectorCurrentVelocity().Copy(GetVectorVelocityAtNewtonIteration());

        GetNonCstVectorCurrentVelocity().UpdateGhosts();
    }


    void VariationalFormulation::ComputeGuessForNextTimeStep()
    {
        const auto& displacement_numbering_subset = GetDisplacementNumberingSubset();
        auto& system_solution = GetNonCstSystemSolution(displacement_numbering_subset);
        const auto& velocity_previous_time_iteration = GetVectorCurrentVelocity();

        Wrappers::Petsc::AXPY(GetTimeManager().GetTimeStep(), velocity_previous_time_iteration, system_solution);

        system_solution.UpdateGhosts();
    }


} // namespace MoReFEM::TestNS::Microsphere


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
