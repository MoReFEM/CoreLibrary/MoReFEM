// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_MICROSPHERE_VARIATIONALFORMULATION_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_MICROSPHERE_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"

#include "Geometry/Domain/Domain.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
#include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"
#include "FormulationSolver/Internal/Snes/SnesInterface.hpp"
#include "FormulationSolver/VariationalFormulation.hpp"

#include "InputData.hpp"


namespace MoReFEM::TestNS::Microsphere
{


    //! \copydoc doxygen_hide_simple_varf
    class VariationalFormulation final
    : public MoReFEM::VariationalFormulation<VariationalFormulation,
                                             EnumUnderlyingType(SolverIndex::solver),
                                             time_manager_type,
                                             enable_non_linear_solver::yes>,
      public FormulationSolverNS::HyperelasticLaw<VariationalFormulation,
                                                  HyperelasticLawNS::CiarletGeymonat<time_manager_type>>
    {
      private:
        //! \copydoc doxygen_hide_alias_self
        using self = VariationalFormulation;

        //! Alias to the parent class.
        using parent = MoReFEM::VariationalFormulation<VariationalFormulation,
                                                       EnumUnderlyingType(SolverIndex::solver),
                                                       time_manager_type,
                                                       enable_non_linear_solver::yes>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;

        //! Alias to hyperlastic law parent,
        using hyperelastic_law_parent =
            FormulationSolverNS::HyperelasticLaw<VariationalFormulation,
                                                 HyperelasticLawNS::CiarletGeymonat<time_manager_type>>;

        //! Alias to the viscoelasticity policy used.
        using ViscoelasticityPolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<
                time_manager_type>;

        //! Alias to the active stress policy used.
        using InternalVariablePolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS ::Microsphere<
                EnumUnderlyingType(FiberIndex::fiberI4),
                EnumUnderlyingType(FiberIndex::fiberI6),
                time_manager_type>;

        //! Alias to the hyperelasticity policy used.
        using HyperelasticityPolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS ::Hyperelasticity<
                typename hyperelastic_law_parent::hyperelastic_law_type>;

        //! Alias to the type of the source parameter.
        using force_parameter_type =
            Parameter<ParameterNS::Type::vector, LocalCoords, time_manager_type, ParameterNS::TimeDependencyNS::None>;

        //! Alias on a pair of Unknown.
        using UnknownPair = std::pair<const Unknown&, const Unknown&>;

        //! Strong type for displacement global vectors.
        using DisplacementGlobalVector =
            StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

        //! Alias to the transient source operator.
        using transient_source_type =
            GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>;

        //! Friendship to the class which implements the prototyped functions required by Petsc Snes algorithm.
        friend struct Internal::SolverNS::SnesInterface<self>;

        //! Alias to the class that stores input for microsphere.
        using input_microsphere_type = ::MoReFEM::InputMicrosphere<time_manager_type>;

      public:
        //! Alias to the stiffness operator type used.
        using StiffnessOperatorType =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensor<HyperelasticityPolicy,
                                                                          ViscoelasticityPolicy,
                                                                          InternalVariablePolicy,
                                                                          time_manager_type>;

      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \copydoc doxygen_hide_varf_constructor
         * \param[in] displacement_numbering_subset \a NumberingSubset related to displacement.
         */
        explicit VariationalFormulation(const NumberingSubset& displacement_numbering_subset,
                                        const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type& morefem_data);

        //! Destructor.
        ~VariationalFormulation() override;

        //! Copy constructor.
        VariationalFormulation(const VariationalFormulation&) = delete;

        //! Move constructor.
        VariationalFormulation(VariationalFormulation&&) = delete;

        //! Copy affectation.
        VariationalFormulation& operator=(const VariationalFormulation&) = delete;

        //! Move affectation.
        VariationalFormulation& operator=(VariationalFormulation&&) = delete;

        ///@}

        /*!
         * \brief Get the displacement numbering subset relevant for this VariationalFormulation.
         *
         * There is a more generic accessor in the base class but its use is more unwieldy.
         *
         * \return \a NumberingSubset related to displacement.
         */
        const NumberingSubset& GetDisplacementNumberingSubset() const;

        //! Update for next time step. (not called after each dynamic iteration).
        void UpdateForNextTimeStep();

        //! Prepare dynamic runs.
        void PrepareDynamicRuns();

      private:
        /// \name CRTP-required methods.
        ///@{

        //! \copydoc doxygen_hide_varf_suppl_init
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();

        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;

        ///@}


      private:
        /*!
         * \brief Assemble method for the mass operator.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleStaticOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Assemble method for the mass operator.
         */
        void AssembleDynamicOperators();

        /*!
         * \brief Assemble method for all the dynamic operators.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Assemble method for all the static operators.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void AssembleNewtonStaticOperators(const GlobalVector& evaluation_state);

        /*!
         * \brief Assemble method for all the dynamic operators.
         */
        void AssembleNewtonDynamicOperators();

        /*!
         * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
         * and the residual.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         *
         */
        void UpdateVectorsAndMatrices(const GlobalVector& evaluation_state);

        /*!
         * \brief Update the content of all the vectors and matrices relevant to the computation of the tangent
         * and the residual.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         *
         */
        void UpdateDynamicVectorsAndMatrices(const GlobalVector& evaluation_state);

        //! \copydoc doxygen_hide_compute_tangent
        void ComputeTangent(const GlobalVector& evaluation_state, GlobalMatrix& tangent, GlobalMatrix& preconditioner);

        //! Compute the matrix of the system for a static case.
        //! \copydoc doxygen_hide_out_tangent_arg
        void ComputeStaticTangent(GlobalMatrix& tangent);

        //! Compute the residual for a static case.
        //! \copydoc doxygen_hide_out_residual_arg
        void ComputeStaticResidual(GlobalVector& residual);

        //! Compute the matrix of the system for a dynamic case.
        //! \copydoc doxygen_hide_out_tangent_arg
        void ComputeDynamicTangent(GlobalMatrix& tangent);

        //! Compute the residual for a dynamic case.
        //! \copydoc doxygen_hide_evaluation_state_arg
        //! \copydoc doxygen_hide_out_residual_arg
        void ComputeDynamicResidual(const GlobalVector& evaluation_state, GlobalVector& residual);

        //! \copydoc doxygen_hide_compute_residual
        void ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual);

        //! Update current displacement. Already called in UpdateForNextTimeStep().
        void UpdateDisplacementBetweenTimeStep();

        //! Update current displacement. Already called in UpdateForNextTimeStep().
        void UpdateVelocityBetweenTimeStep();

        //! Compute the guess for next time step with the new velocity.
        void ComputeGuessForNextTimeStep();

      private:
        /*!
         * \brief Define the properties of all the static global variational operators involved.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        void DefineStaticOperators(const morefem_data_type& morefem_data);

        /*!
         * \brief Define the properties of all the dynamic global variational operators involved.
         */
        void DefineDynamicOperators();

        //! Get the mass per square time step operator.
        const GlobalVariationalOperatorNS::Mass& GetMassOperator() const noexcept;

        //! Get the hyperelastic stiffness operator.
        const StiffnessOperatorType& GetStiffnessOperator() const noexcept;

        //! Accessor to the surfacic source operator on face 1.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperatorFace1() const noexcept;

        //! Accessor to the surfacic source operator on face 2.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperatorFace2() const noexcept;

        //! Accessor to the surfacic source operator on face 3.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperatorFace3() const noexcept;

        //! Accessor to the surfacic source operator on face 4.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperatorFace4() const noexcept;

        //! Accessor to the surfacic source operator on face 5.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperatorFace5() const noexcept;

        //! Accessor to the surfacic source operator on face 6.
        const GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector, time_manager_type>&
        GetSurfacicForceOperatorFace6() const noexcept;


      private:
        /// \name Global variational operators.
        ///@{

        //! Mass operator.
        GlobalVariationalOperatorNS::Mass::const_unique_ptr mass_operator_ = nullptr;

        //! Stiffness operator.
        StiffnessOperatorType::const_unique_ptr stiffness_operator_ = nullptr;

        //! Surfacic source operator on face1.
        typename transient_source_type::const_unique_ptr surfacic_force_operator_face_1_ = nullptr;

        //! Surfacic source operator on face2.
        typename transient_source_type::const_unique_ptr surfacic_force_operator_face_2_ = nullptr;

        //! Surfacic source operator on face3.
        typename transient_source_type::const_unique_ptr surfacic_force_operator_face_3_ = nullptr;

        //! Surfacic source operator on face4.
        typename transient_source_type::const_unique_ptr surfacic_force_operator_face_4_ = nullptr;

        //! Surfacic source operator on face5.
        typename transient_source_type::const_unique_ptr surfacic_force_operator_face_5_ = nullptr;

        //! Surfacic source operator on face6.
        typename transient_source_type::const_unique_ptr surfacic_force_operator_face_6_ = nullptr;

        ///@}

      private:
        /// \name Accessors to the global vectors and matrices managed by the class.
        ///@{


        //! Accessor.
        const GlobalMatrix& GetMatrixMassPerSquareTimeStep() const noexcept;

        //! Non constant accessor.
        GlobalMatrix& GetNonCstMatrixMassPerSquareTimeStep() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorStiffnessResidual() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorStiffnessResidual() noexcept;

        //! Accessor.
        const GlobalMatrix& GetMatrixTangentStiffness() const noexcept;

        //! Non constant accessor.
        GlobalMatrix& GetNonCstMatrixTangentStiffness() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForceFace1() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorSurfacicForceFace1() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForceFace2() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorSurfacicForceFace2() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForceFace3() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorSurfacicForceFace3() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForceFace4() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorSurfacicForceFace4() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForceFace5() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorSurfacicForceFace5() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorSurfacicForceFace6() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorSurfacicForceFace6() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorCurrentDisplacement() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorCurrentDisplacement() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorCurrentVelocity() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorCurrentVelocity() noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorVelocityAtNewtonIteration() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorVelocityAtNewtonIteration() const noexcept;

        //! Accessor.
        const GlobalVector& GetVectorMidpointPosition() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorMidpointPosition() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorMidpointVelocity() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorMidpointVelocity() noexcept;

        //! Accessor.
        const GlobalVector& GetVectorDiffDisplacement() const noexcept;

        //! Non constant accessor.
        GlobalVector& GetNonCstVectorDiffDisplacement() noexcept;

        ///@}

        //! Access to the solid.
        const Solid<time_manager_type>& GetSolid() const noexcept;

        //! Access to the input of the microsphere model.
        const input_microsphere_type& GetInputMicrosphere() const noexcept;

      private:
        /*!
         * \brief Update the vector that contains the values seeked at the moment the residual is evaluated.
         *
         * \copydoc doxygen_hide_evaluation_state_arg
         */
        void UpdateVelocityAtNewtonIteration(const GlobalVector& evaluation_state);

      private:
        /// \name Global vectors and matrices specific to the problem.
        ///@{

        //! Stiffness residual vector.
        GlobalVector::unique_ptr vector_stiffness_residual_ = nullptr;

        //! Residual of the transient source on face1.
        GlobalVector::unique_ptr vector_surfacic_force_face_1_ = nullptr;

        //! Residual of the transient source on face2.
        GlobalVector::unique_ptr vector_surfacic_force_face_2_ = nullptr;

        //! Residual of the transient source on face3.
        GlobalVector::unique_ptr vector_surfacic_force_face_3_ = nullptr;

        //! Residual of the transient source on face4.
        GlobalVector::unique_ptr vector_surfacic_force_face_4_ = nullptr;

        //! Residual of the transient source on face5.
        GlobalVector::unique_ptr vector_surfacic_force_face_5_ = nullptr;

        //! Residual of the transient source on face6.
        GlobalVector::unique_ptr vector_surfacic_force_face_6_ = nullptr;

        //! Mass matrix.
        GlobalMatrix::unique_ptr matrix_mass_per_square_time_step_ = nullptr;

        //! Matrix stiffness tangent.
        GlobalMatrix::unique_ptr matrix_tangent_stiffness_ = nullptr;

        //! Evaluation state of the residual of the problem (only useful in SNES method)
        GlobalVector::unique_ptr vector_velocity_at_newton_iteration_ = nullptr;

        //! Velocity from previous time iteration.
        GlobalVector::unique_ptr vector_current_velocity_ = nullptr;

        //! Displacement from previous time iteration.
        GlobalVector::unique_ptr vector_current_displacement_ = nullptr;

        //! Midpoint position.
        GlobalVector::unique_ptr vector_midpoint_position_ = nullptr;

        //! Difference displacement Yn+1 - Yn. Here just to avoid allocate it every time step.
        GlobalVector::unique_ptr vector_diff_displacement_ = nullptr;

        //! Midpoint velocity.
        GlobalVector::unique_ptr vector_midpoint_velocity_ = nullptr;

        ///@}

      private:
        //! Quadrature rule topology used for the parameters.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_parameter_ = nullptr;

        //! Quadrature rule topology used for the operators.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_ = nullptr;

      private:
        /// \name Numbering subsets used in the formulation.
        ///@{

        //! Numbering subset related to displacement.
        const NumberingSubset& displacement_numbering_subset_;

        ///@}

      private:
        //! Material parameters of the solid.
        typename Solid<time_manager_type>::const_unique_ptr solid_ = nullptr;

        //! Struct that stores the input data for the microsphere internal variable.
        typename input_microsphere_type::unique_ptr input_microsphere_ = nullptr;

        //! Force parameter for the static force on face1.
        force_parameter_type::unique_ptr force_parameter_face_1_ = nullptr;

        //! Force parameter for the static force on face2.
        force_parameter_type::unique_ptr force_parameter_face_2_ = nullptr;

        //! Force parameter for the static force on face3.
        force_parameter_type::unique_ptr force_parameter_face_3_ = nullptr;

        //! Force parameter for the static force on face4.
        force_parameter_type::unique_ptr force_parameter_face_4_ = nullptr;

        //! Force parameter for the static force on face5.
        force_parameter_type::unique_ptr force_parameter_face_5_ = nullptr;

        //! Force parameter for the static force on face6.
        force_parameter_type::unique_ptr force_parameter_face_6_ = nullptr;
    };


} // namespace MoReFEM::TestNS::Microsphere


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "VariationalFormulation.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_MICROSPHERE_VARIATIONALFORMULATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
