// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_MICROSPHERE_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_MICROSPHERE_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/Microsphere/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::TestNS::Microsphere
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation::GetMassOperator() const noexcept
    {
        assert(!(!mass_operator_));
        return *mass_operator_;
    }


    inline const VariationalFormulation::StiffnessOperatorType&
    VariationalFormulation::GetStiffnessOperator() const noexcept
    {
        assert(!(!stiffness_operator_));
        return *stiffness_operator_;
    }


    inline auto VariationalFormulation::GetSurfacicForceOperatorFace1() const noexcept -> const transient_source_type&
    {
        assert(!(!surfacic_force_operator_face_1_));
        return *surfacic_force_operator_face_1_;
    }


    inline auto VariationalFormulation::GetSurfacicForceOperatorFace2() const noexcept -> const transient_source_type&
    {
        assert(!(!surfacic_force_operator_face_2_));
        return *surfacic_force_operator_face_2_;
    }


    inline auto VariationalFormulation::GetSurfacicForceOperatorFace3() const noexcept -> const transient_source_type&
    {
        assert(!(!surfacic_force_operator_face_3_));
        return *surfacic_force_operator_face_3_;
    }


    inline auto VariationalFormulation::GetSurfacicForceOperatorFace4() const noexcept -> const transient_source_type&
    {
        assert(!(!surfacic_force_operator_face_4_));
        return *surfacic_force_operator_face_4_;
    }


    inline auto VariationalFormulation::GetSurfacicForceOperatorFace5() const noexcept -> const transient_source_type&
    {
        assert(!(!surfacic_force_operator_face_5_));
        return *surfacic_force_operator_face_5_;
    }


    inline auto VariationalFormulation::GetSurfacicForceOperatorFace6() const noexcept -> const transient_source_type&
    {
        assert(!(!surfacic_force_operator_face_6_));
        return *surfacic_force_operator_face_6_;
    }


    inline const GlobalVector& VariationalFormulation::GetVectorStiffnessResidual() const noexcept
    {
        assert(!(!vector_stiffness_residual_));
        return *vector_stiffness_residual_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorStiffnessResidual() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorStiffnessResidual());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorVelocityAtNewtonIteration() const noexcept
    {
        assert(!(!vector_velocity_at_newton_iteration_));
        return *vector_velocity_at_newton_iteration_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorVelocityAtNewtonIteration() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorVelocityAtNewtonIteration());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixMassPerSquareTimeStep() const noexcept
    {
        assert(!(!matrix_mass_per_square_time_step_));
        return *matrix_mass_per_square_time_step_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixMassPerSquareTimeStep() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixMassPerSquareTimeStep());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixTangentStiffness() const noexcept
    {
        assert(!(!matrix_tangent_stiffness_));
        return *matrix_tangent_stiffness_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixTangentStiffness() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixTangentStiffness());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace1() const noexcept
    {
        assert(!(!vector_surfacic_force_face_1_));
        return *vector_surfacic_force_face_1_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace1() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForceFace1());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace2() const noexcept
    {
        assert(!(!vector_surfacic_force_face_2_));
        return *vector_surfacic_force_face_2_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace2() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForceFace2());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace3() const noexcept
    {
        assert(!(!vector_surfacic_force_face_3_));
        return *vector_surfacic_force_face_3_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace3() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForceFace3());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace4() const noexcept
    {
        assert(!(!vector_surfacic_force_face_4_));
        return *vector_surfacic_force_face_4_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace4() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForceFace4());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace5() const noexcept
    {
        assert(!(!vector_surfacic_force_face_5_));
        return *vector_surfacic_force_face_5_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace5() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForceFace5());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorSurfacicForceFace6() const noexcept
    {
        assert(!(!vector_surfacic_force_face_6_));
        return *vector_surfacic_force_face_6_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorSurfacicForceFace6() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorSurfacicForceFace6());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentDisplacement() const noexcept
    {
        assert(!(!vector_current_displacement_));
        return *vector_current_displacement_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentDisplacement() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentVelocity() const noexcept
    {
        assert(!(!vector_current_velocity_));
        return *vector_current_velocity_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentVelocity() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorMidpointPosition() const noexcept
    {
        assert(!(!vector_midpoint_position_));
        return *vector_midpoint_position_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointPosition() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorMidpointPosition());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorMidpointVelocity() const noexcept
    {
        assert(!(!vector_midpoint_velocity_));
        return *vector_midpoint_velocity_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorMidpointVelocity() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorMidpointVelocity());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorDiffDisplacement() const noexcept
    {
        assert(!(!vector_diff_displacement_));
        return *vector_diff_displacement_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorDiffDisplacement() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorDiffDisplacement());
    }


    inline const Solid<time_manager_type>& VariationalFormulation::GetSolid() const noexcept
    {
        assert(!(!solid_));
        return *solid_;
    }

    inline auto VariationalFormulation::GetInputMicrosphere() const noexcept -> const input_microsphere_type&
    {
        assert(!(!input_microsphere_));
        return *input_microsphere_;
    }

    inline const NumberingSubset& VariationalFormulation::GetDisplacementNumberingSubset() const
    {
        return displacement_numbering_subset_;
    }


} // namespace MoReFEM::TestNS::Microsphere


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_MICROSPHERE_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
