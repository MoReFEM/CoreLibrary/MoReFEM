add_library(TestNonLinearMembrane_lib ${LIBRARY_TYPE} "")

target_sources(TestNonLinearMembrane_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/TestP1P1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P1P1.cpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/Enum.hpp
		${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hpp
)

target_link_libraries(TestNonLinearMembrane_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_OP_INSTANCES}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestNonLinearMembrane_lib Test/Operators/VariationalInstances/NonlinearMembrane Test/Operators/VariationalInstances/NonlinearMembrane)                         


add_executable(TestNonLinearMembrane ${CMAKE_CURRENT_LIST_DIR}/test.cpp)
target_link_libraries(TestNonLinearMembrane
                      TestNonLinearMembrane_lib)

morefem_organize_IDE(TestNonLinearMembrane Test/Operators/VariationalInstances/NonlinearMembrane Test/Operators/VariationalInstances/NonlinearMembrane)    


morefem_boost_test_sequential_mode(NAME NonLinearMembrane
                                   EXE TestNonLinearMembrane
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/NonlinearMembrane/demo.lua)
