// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/LinearAlgebra/Type.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Enum.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/ExpectedResults.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS
{


    namespace // anonymous
    {


        class FillMatrix
        {

          public:
            FillMatrix(double factor, expected_results_type<LinearAlgebraNS::type::matrix>& matrix);

            void AddRow(std::vector<double>&& row) const;

          private:
            double factor_;

            expected_results_type<LinearAlgebraNS::type::matrix>& matrix_;
        };


        void ApplyFactorToVector(double factor, expected_results_type<LinearAlgebraNS::type::vector>& vector);


        // Matrix given as expected one is the local matrix computed in Matlab.
        // In the test I want the global matrix (after assembling result); as it is a P1 matrix it is easy
        // to apply the 'translation'.
        void ConvertLocal2Global(expected_results_type<LinearAlgebraNS::type::matrix>& matrix);

        // Same for vector.
        void ConvertLocal2Global(expected_results_type<LinearAlgebraNS::type::vector>& vector);


    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix> GetExpectedMatrixP1P1(pretension is_pretension)
    {
        // The values below have been computed independently in Matlab by Dominique Chapelle.
        expected_results_type<LinearAlgebraNS::type::matrix> ret;

        const FillMatrix fill_matrix(1.e10, ret);

        if (is_pretension == pretension::no)
        {
            fill_matrix.AddRow({ 1.322827726106691,
                                 -1.208373780858154,
                                 -0.114453945248537,
                                 -0.225666852725472,
                                 0.175183327027966,
                                 0.050483525697505,
                                 -0.128292178434872,
                                 0.089556017116076,
                                 0.038736161318796 });
            fill_matrix.AddRow({ -1.208373780858154,
                                 1.279892021580330,
                                 -0.071518240722176,
                                 0.183879623586336,
                                 -0.135513798722450,
                                 -0.048365824863886,
                                 0.092512935042351,
                                 -0.068300996237284,
                                 -0.024211938805067 });
            fill_matrix.AddRow({ -0.114453945248537,
                                 -0.071518240722176,
                                 0.185972185970713,
                                 0.041787229139135,
                                 -0.039669528305516,
                                 -0.002117700833619,
                                 0.035779243392521,
                                 -0.021255020878792,
                                 -0.014524222513729 });
            fill_matrix.AddRow({ -0.225666852725472,
                                 0.183879623586336,
                                 0.041787229139135,
                                 3.408682812520989,
                                 -3.153385499509070,
                                 -0.255297313011918,
                                 0.967863611537033,
                                 -0.868275409254392,
                                 -0.099588202282641 });
            fill_matrix.AddRow({ 0.175183327027966,
                                 -0.135513798722450,
                                 -0.039669528305516,
                                 -3.153385499509070,
                                 3.406001782573789,
                                 -0.252616283064719,
                                 -0.858938048391076,
                                 0.875461739270461,
                                 -0.016523690879386 });
            fill_matrix.AddRow({ 0.050483525697505,
                                 -0.048365824863886,
                                 -0.002117700833619,
                                 -0.255297313011918,
                                 -0.252616283064719,
                                 0.507913596076638,
                                 -0.108925563145957,
                                 -0.007186330016070,
                                 0.116111893162027 });
            fill_matrix.AddRow({ -0.128292178434872,
                                 0.092512935042351,
                                 0.035779243392521,
                                 0.967863611537033,
                                 -0.858938048391076,
                                 -0.108925563145957,
                                 1.741649648938485,
                                 -1.571831792261287,
                                 -0.169817856677199 });
            fill_matrix.AddRow({ 0.089556017116076,
                                 -0.068300996237284,
                                 -0.021255020878792,
                                 -0.868275409254392,
                                 0.875461739270461,
                                 -0.007186330016070,
                                 -1.571831792261287,
                                 1.630205008542017,
                                 -0.058373216280731 });
            fill_matrix.AddRow({ 0.038736161318796,
                                 -0.024211938805067,
                                 -0.014524222513729,
                                 -0.099588202282641,
                                 -0.016523690879386,
                                 0.116111893162027,
                                 -0.169817856677199,
                                 -0.058373216280731,
                                 0.228191072957930 });
        } else
        {
            fill_matrix.AddRow({ 1.683147665196165,
                                 -1.464625146663350,
                                 -0.218522518532815,
                                 -0.225666852725472,
                                 0.175183327027966,
                                 0.050483525697505,
                                 -0.128292178434872,
                                 0.089556017116076,
                                 0.038736161318796 });
            fill_matrix.AddRow({ -1.464625146663350,
                                 1.536143387385526,
                                 -0.071518240722176,
                                 0.183879623586336,
                                 -0.135513798722450,
                                 -0.048365824863886,
                                 0.092512935042351,
                                 -0.068300996237284,
                                 -0.024211938805067 });
            fill_matrix.AddRow({ -0.218522518532815,
                                 -0.071518240722176,
                                 0.290040759254991,
                                 0.041787229139135,
                                 -0.039669528305516,
                                 -0.002117700833619,
                                 0.035779243392521,
                                 -0.021255020878792,
                                 -0.014524222513729 });
            fill_matrix.AddRow({ -0.225666852725472,
                                 0.183879623586336,
                                 0.041787229139135,
                                 3.769002751610463,
                                 -3.409636865314267,
                                 -0.359365886296197,
                                 0.967863611537033,
                                 -0.868275409254392,
                                 -0.099588202282641 });
            fill_matrix.AddRow({ 0.175183327027966,
                                 -0.135513798722450,
                                 -0.039669528305516,
                                 -3.409636865314267,
                                 3.662253148378986,
                                 -0.252616283064719,
                                 -0.858938048391076,
                                 0.875461739270461,
                                 -0.016523690879386 });
            fill_matrix.AddRow({ 0.050483525697505,
                                 -0.048365824863886,
                                 -0.002117700833619,
                                 -0.359365886296197,
                                 -0.252616283064719,
                                 0.611982169360916,
                                 -0.108925563145957,
                                 -0.007186330016070,
                                 0.116111893162027 });
            fill_matrix.AddRow({ -0.128292178434872,
                                 0.092512935042351,
                                 0.035779243392521,
                                 0.967863611537033,
                                 -0.858938048391076,
                                 -0.108925563145957,
                                 2.101969588027960,
                                 -1.828083158066483,
                                 -0.273886429961477 });
            fill_matrix.AddRow({ 0.089556017116076,
                                 -0.068300996237284,
                                 -0.021255020878792,
                                 -0.868275409254392,
                                 0.875461739270461,
                                 -0.007186330016070,
                                 -1.828083158066483,
                                 1.886456374347214,
                                 -0.058373216280731 });
            fill_matrix.AddRow({ 0.038736161318796,
                                 -0.024211938805067,
                                 -0.014524222513729,
                                 -0.099588202282641,
                                 -0.016523690879386,
                                 0.116111893162027,
                                 -0.273886429961477,
                                 -0.058373216280731,
                                 0.332259646242208 });
        }

        ConvertLocal2Global(ret);

        return ret;
    }


    expected_results_type<LinearAlgebraNS::type::vector> GetExpectedVectorP1P1(pretension is_pretension)
    {
        // The values below have been computed independently in Matlab by Dominique Chapelle.
        expected_results_type<LinearAlgebraNS::type::vector> ret;

        if (is_pretension == pretension::no)
        {
            ret = { 0.141635307789950,  -0.106231526876560, -0.035403780913390, -1.600020566269832, 1.784325243232761,
                    -0.184304676962930, -0.696115804469422, 0.720769508888880,  -0.024653704419458 };
        } else
        {
            ret = { 0.193277587691539,  -0.131856663457079, -0.061420924234460, -1.911219907130632, 2.140514641701985,
                    -0.229294734571352, -0.857379108160713, 0.869395301055894,  -0.012016192895181 };
        }

        ApplyFactorToVector(1.e12, ret);

        ConvertLocal2Global(ret);

        return ret;
    }


    namespace // anonymous
    {


        FillMatrix::FillMatrix(double factor, expected_results_type<LinearAlgebraNS::type::matrix>& matrix)
        : factor_(factor), matrix_(matrix)
        { }


        void FillMatrix::AddRow(std::vector<double>&& row) const
        {
            for (auto& item : row)
                item *= factor_;

            matrix_.emplace_back(std::move(row));
        }


        void ApplyFactorToVector(double factor, expected_results_type<LinearAlgebraNS::type::vector>& vector)
        {
            for (auto& item : vector)
                item *= factor;
        }


        void ConvertLocal2Global(expected_results_type<LinearAlgebraNS::type::matrix>& matrix)
        {
            assert(matrix.size() == 9UL);

            const std::vector<std::size_t> loc2glob_array{ 0, 3, 6, 1, 4, 7, 2, 5, 8 };

            expected_results_type<LinearAlgebraNS::type::matrix> reordered_matrix;
            reordered_matrix.reserve(matrix.size());
            std::vector<double> reordered_row;
            reordered_row.reserve(matrix.size());

            for (auto loc2glob : loc2glob_array)
            {
                const auto& original_row = matrix[loc2glob];
                assert(original_row.size() == 9UL);
                reordered_row.clear();

                for (auto loc2glob_col : loc2glob_array)
                {
                    reordered_row.push_back(original_row[loc2glob_col]);
                }

                reordered_matrix.push_back(reordered_row);
            }

            matrix = reordered_matrix;
        }


        void ConvertLocal2Global(expected_results_type<LinearAlgebraNS::type::vector>& vector)
        {
            assert(vector.size() == 9UL);

            const std::vector<std::size_t> loc2glob_array{ 0, 3, 6, 1, 4, 7, 2, 5, 8 };

            expected_results_type<LinearAlgebraNS::type::vector> reordered_vector;
            reordered_vector.reserve(vector.size());

            for (auto loc2glob : loc2glob_array)
            {
                reordered_vector.push_back(vector[loc2glob]);
            }

            vector = reordered_vector;
        }


    } // namespace


} // namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
