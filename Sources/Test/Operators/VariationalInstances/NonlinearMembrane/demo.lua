-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.


Unknown1 = {

	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = "displacementP1",

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'scalar', 'vectorial'})
	nature = "vectorial"

} -- Unknown1


Unknown2 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "displacementP2",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "vectorial"

} -- Unknown2

Mesh1 = {

	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
    mesh =  "${MOREFEM_ROOT}/Data/Mesh/Single_Surf_Tria3.mesh",

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = "Medit",

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 3,

	-- Space unit of the mesh.
	-- Expected format: VALUE
	space_unit = 1.

} -- Mesh1

-- Domain1 - Full mesh.
Domain1 = {

	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at Ops level, as some geometric element types could be added after 
	-- generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built.
 	-- The known types when this file was generated are: 
 	-- . Point1
 	-- . Segment2, Segment3
 	-- . Triangle3, Triangle6
 	-- . Quadrangle4, Quadrangle8, Quadrangle9
 	-- . Tetrahedron4, Tetrahedron10
 	-- . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }

} -- Domain1

-- Domain2 - The surface elements of the mesh.
Domain2 = {

	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at Ops level, as some geometric element types could be added after 
	-- generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built.
 	-- The known types when this file was generated are: 
 	-- . Point1
 	-- . Segment2, Segment3
 	-- . Triangle3, Triangle6
 	-- . Quadrangle4, Quadrangle8, Quadrangle9
 	-- . Tetrahedron4, Tetrahedron10
 	-- . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = { }

} -- Domain2

-- Domain3 - Domain on which force is applied
Domain3 = {

-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
-- left empty if domain not limited to one mesh; at most one value is expected here.
-- Expected format: {VALUE1, VALUE2, ...}
mesh_index = { 1 },

-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
-- dimensions.
-- Expected format: {VALUE1, VALUE2, ...}
-- Constraint: value_in(v, {0, 1, 2, 3})
dimension_list = { 2 },

-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
-- labels. This parameter does not make sense if no mesh is defined for the domain.
-- Expected format: {VALUE1, VALUE2, ...}
mesh_label_list = { },

-- List of geometric element types considered in the domain. Might be left empty if no restriction upon
-- these. No constraint is applied at Ops level, as some geometric element types could be added after
-- generation of current input data file. Current list is below; if an incorrect value is put there it
-- will be detected a bit later when the domain object is built.
-- The known types when this file was generated are:
-- . Point1
-- . Segment2, Segment3
-- . Triangle3, Triangle6
-- . Quadrangle4, Quadrangle8, Quadrangle9
-- . Tetrahedron4, Tetrahedron10
-- . Hexahedron8, Hexahedron20, Hexahedron27.
-- Expected format: {"VALUE1", "VALUE2", ...}
geometric_element_type_list = { }

} -- Domain4


-- Domain4 - upon which Dirichlet boundary condition is applied.
Domain4 = {

	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: {VALUE1, VALUE2, ...}
	mesh_index = { 1 },

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: {VALUE1, VALUE2, ...}
	mesh_label_list = { 1 },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at Ops level, as some geometric element types could be added after 
	-- generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built.
 	-- The known types when this file was generated are: 
 	-- . Point1
 	-- . Segment2, Segment3
 	-- . Triangle3, Triangle6
 	-- . Quadrangle4, Quadrangle8, Quadrangle9
 	-- . Tetrahedron4, Tetrahedron10
 	-- . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = { }

} -- Domain4


-- Surface
FiniteElementSpace1 = {

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,

	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 2,

	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "displacementP1", "displacementP2" },

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "P1", "P2" },

	-- List of the numbering subset to use for each unknown;
	-- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1, 2 }

} -- FiniteElementSpace1


Petsc1 = {

	-- Absolute tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	absoluteTolerance = 1e-8,

	-- gmresStart
	-- Expected format: VALUE
	-- Constraint: v >= 0
	gmresRestart = 200,

	-- Maximum iteration
	-- Expected format: VALUE
	-- Constraint: v > 0
	maxIteration = 1000,

	-- Preconditioner to use. Must be lu for any direct solver.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'lu', 'none'})
	preconditioner = 'lu',

	-- Relative tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	relativeTolerance = 1e-5,

	-- Step size tolerance
	-- Expected format: VALUE
	-- Constraint: v > 0.
	stepSizeTolerance = 1e-8,

	-- Solver to use.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, { 'Mumps', 'Umfpack', 'Gmres' })
	solver = 'SuperLU_dist'

} -- Petsc1

-- Surfacic
VectorialTransientSource1 = {

    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = { 'constant', 'constant', 'constant'},

    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix.
    value = { 0., 0., 0. }

} -- VectorialTransientSource1

Solid = {

	VolumicMass = {

		-- How is given the Young modulus (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function', 'piecewise_constant_by_domain'})
        nature = "constant",

        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix.
        value = 1.

	}, -- VolumicMass

	YoungModulus = {

		-- How is given the Young modulus (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function', 'piecewise_constant_by_domain'})
        nature = "constant",

        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix.
        value = 3.e7

	}, -- YoungModulus

	PoissonRatio = {

		-- How is given the Young modulus (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function', 'piecewise_constant_by_domain'})
        nature = "constant",

        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix.
        value = 0.3

	}, -- PoissonRatio

} -- Solid

Result = {

	-- Directory in which all the results will be written.  
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/Operators/Variational/NonlinearMembrane",

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	display_value = 1,
    
    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: VALUE
    binary_output = false
} -- Result

