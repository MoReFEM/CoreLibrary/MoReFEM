// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Operators/LocalVariationalOperator/Internal/EnumClass.hpp"

#define BOOST_TEST_MODULE non_linear_membrane_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Enum.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"

using namespace MoReFEM;


namespace // anonymous
{

    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
       TestNS::NonLinearMembraneOperatorNS::Model
    >;
    // clang-format on


    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_SUITE(no_pretension, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::no, assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::no, assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::no, assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(pretension, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::yes, assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::yes, assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().TestP1P1(
        TestNS::NonLinearMembraneOperatorNS::pretension::yes, assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
