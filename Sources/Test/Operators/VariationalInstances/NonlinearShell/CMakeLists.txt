include(${CMAKE_CURRENT_LIST_DIR}/TyingPointsPolicy/CMakeLists.txt)

add_executable(TestOperatorNonlinearShell)

target_sources(TestOperatorNonlinearShell
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P1P1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P2P1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/SameUnknown.cpp
        ${CMAKE_CURRENT_LIST_DIR}/UnknownP1TestP1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/UnknownP2TestP1.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo_3D.lua
        ${CMAKE_CURRENT_LIST_DIR}/one_tetra_not_ref.mesh
        ${CMAKE_CURRENT_LIST_DIR}/test_3D.cpp
)
            
target_link_libraries(TestOperatorNonlinearShell
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestOperatorNonlinearShell Test/Operators/VariationalInstances/NonlinearShell Test/Operators/VariationalInstances/NonlinearShell)

# The very high timeout is for the sake of gcc in debug mode, which is really very slow...
morefem_boost_test_sequential_mode(NAME OperatorNonlinearShell
                                   EXE TestOperatorNonlinearShell
                                   TIMEOUT 200
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/NonlinearShell/demo_3D.lua)

