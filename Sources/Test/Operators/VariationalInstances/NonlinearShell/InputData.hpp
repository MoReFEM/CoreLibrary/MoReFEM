// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_INPUTDATA_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/EnumClass.hpp"

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
// IWYU pragma: end_exports

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { volume = 1 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { sole = 1 };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { displacement_p1 = 1, displacement_p2 = 2, other_displacement_p1 = 3 };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        displacement_p1 = 1,
        displacement_p2 = 2,
        other_displacement_p1 = 3
    };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex

    {
        solver = 1
    };


    //! \copydoc doxygen_hide_input_data_tuple
    using input_data_tuple = std::tuple<

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,

        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
        InputDataNS::Solid::CheckInvertedElements,

        InputDataNS::Result>;


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p1)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p2)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_displacement_p1)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription,
        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<ModelSettings, input_data_type, time_manager_type, program_type::test>;


} // namespace MoReFEM::TestNS::NonlinearShell


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
