// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <iostream>
#include <memory>

#include "Model/Model.hpp"

#include "Utilities/Exceptions/Exception.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/InputData.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    Model::Model(morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    {
        decltype(auto) mpi = parent::GetMpi();

        if (mpi.Nprocessor<int>() > 1)
        {
            throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                            "a vector; the expected values assume the dof numbering of the sequential case. Please "
                            "run it sequentially.");
        }
    }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetMoReFEMData();


        decltype(auto) domain_manager = DomainManager::GetInstance();

        decltype(auto) domain_volume = domain_manager.GetDomain(AsDomainId(DomainIndex::volume));

        constexpr auto degree_of_exactness{ 3 };
        constexpr auto shape_function_order{ 2 };
        quadrature_rule_per_topology_for_operators_ =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);
        quadrature_rule_per_topology_for_operators_->Print(std::cout);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        solid_ = std::make_unique<Solid<time_manager_type>>(
            morefem_data, domain_volume, felt_space.GetQuadratureRulePerTopology());

        hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), *solid_);

        // Required to enable construction of an operator after initialization step.
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::NonlinearShell


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
