// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/VariationalInstances/NonlinearShell/InputData.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p1)>>(
            { " displacement_p1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p2)>>(
            { " displacement_p2" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_displacement_p1)>>(
            { " other_displacement_p1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>>({ " displacement_p1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>>({ " displacement_p2" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>>(
            { " other_displacement_p1" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>({ " volume" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>({ " sole" });
        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::NonlinearShell


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
