// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_TYINGPOINTSPOLICY_MODEL_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_TYINGPOINTSPOLICY_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearShell.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/InputData.hpp"


namespace MoReFEM::TestNS::MITCNS
{


    //! \copydoc doxygen_hide_model_4_test
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>,
                  public FormulationSolverNS::HyperelasticLaw<
                      Model,
                      HyperelasticLawNS::CiarletGeymonat<time_manager_type, InvariantNS::coords::generalized>>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        //! Alias to hyperlastic law parent,
        // clang-format off
        using hyperelastic_law_parent =
            FormulationSolverNS::HyperelasticLaw
            <
                self,
                HyperelasticLawNS::CiarletGeymonat
                <
        time_manager_type,
                    InvariantNS::coords::generalized
                >
            >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Convenient alias.
        // clang-format off
        using hyperelastic_policy_for_operator =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
            HyperelasticityPolicyNS::Hyperelasticity
            <
                typename hyperelastic_law_parent::hyperelastic_law_type
            >;
        // clang-format on

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}

        /*!
         * \brief Compute the content of \a ElementaryData after the *local* operator has been computed for the first time.
         *
         * \param[in] global_operator The \a NonlinearShellOperator (specialized for one or the other of the MITC policies).
         * \param[in] previous_iteration The displacement from previous iteration - or at least that what it would be in a true use of the operator
         * in a \a Model where \a Assemble is called. Here it is a plausible made up value which follows the same vector
         * format.
         *
         * \return The \a ElementaryData object computed for the local operator.
         */
        template<class GlobalOperatorT>
        const auto& ComputeForFirstLocalOperator(GlobalOperatorT& global_operator,
                                                 const GlobalVector& previous_iteration) const;


        /// \name Crtp-required methods.
        ///@{


      private:
        /*!
         * \brief Initialise the problem.
         *, ::MoReFEM::TimeManagerNS::Policy::None
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();

        ///@}


      private:
        //! Quadrature rule topology used for the operators.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_ = nullptr;

        //! Material parameters of the solid.
        typename Solid<time_manager_type>::const_unique_ptr solid_ = nullptr;
    };


} // namespace MoReFEM::TestNS::MITCNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_TYINGPOINTSPOLICY_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
