// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_TYINGPOINTSPOLICY_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_TYINGPOINTSPOLICY_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/Model.hpp"
// *** MoReFEM header guards *** < //

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::MITCNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test MITC for operator");
        return name;
    }


    template<class GlobalOperatorT>
    const auto& Model::ComputeForFirstLocalOperator(GlobalOperatorT& global_operator,
                                                    const GlobalVector& previous_iteration) const
    {
        Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto unknown_id = AsUnknownId(UnknownIndex::vectorial);

        const auto& unknown_ptr = unknown_manager.GetUnknownPtr(unknown_id);

        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::domain));

        auto& local_operator =
            global_operator.template ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Hexahedron27>(
                domain, previous_iteration);

        const auto& elementary_data = local_operator.GetElementaryData();

        local_operator.InitTyingPointData(elementary_data.GetInformationAtQuadraturePointList(),
                                          elementary_data.GetRefGeomElt(),
                                          elementary_data.GetRefFElt(felt_space.GetExtendedUnknown(*unknown_ptr)),
                                          elementary_data.GetTestRefFElt(felt_space.GetExtendedUnknown(*unknown_ptr)));


        local_operator.ComputeEltArray();

        return elementary_data;
    }


} // namespace MoReFEM::TestNS::MITCNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_TYINGPOINTSPOLICY_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
