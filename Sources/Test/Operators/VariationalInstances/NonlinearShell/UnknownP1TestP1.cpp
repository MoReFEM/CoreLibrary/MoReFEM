// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <functional>
#include <tuple>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/InputData.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::NonlinearShell
{


    void Model::UnknownP1TestP1() const
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& displacement_p1_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement_p1));
        const auto& other_displacement_p1_ptr =
            unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::other_displacement_p1));

        const NonlinearShellType shell_operator(felt_space,
                                                other_displacement_p1_ptr,
                                                displacement_p1_ptr,
                                                hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                                nullptr);

        decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement_p1));
        decltype(auto) numbering_subset_other_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::other_displacement_p1));

        GlobalMatrix matrix_p1_other_p1(numbering_subset_p1, numbering_subset_other_p1);
        AllocateGlobalMatrix(god_of_dof, matrix_p1_other_p1);

        GlobalVector vector_p1(numbering_subset_p1);
        AllocateGlobalVector(god_of_dof, vector_p1);

        GlobalVector previous_iteration_other_p1(numbering_subset_other_p1);
        AllocateGlobalVector(god_of_dof, previous_iteration_other_p1);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        {
            matrix_p1_other_p1.ZeroEntries();
            vector_p1.ZeroEntries();
            previous_iteration_other_p1.ZeroEntries();

            previous_iteration_other_p1.SetValue(vector_program_wise_index_type{ 3 }, 2., INSERT_VALUES);
            previous_iteration_other_p1.SetValue(vector_program_wise_index_type{ 2 }, 2., INSERT_VALUES);
            previous_iteration_other_p1.SetValue(vector_program_wise_index_type{ 0 }, -1., INSERT_VALUES);
            previous_iteration_other_p1.SetValue(vector_program_wise_index_type{ 1 }, -2., INSERT_VALUES);
        }

        previous_iteration_other_p1.Assembly();

        GlobalMatrixWithCoefficient matrix(matrix_p1_other_p1, 1.);
        GlobalVectorWithCoefficient vec(vector_p1, 1.);

        shell_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)), previous_iteration_other_p1);

        CheckMatrix(god_of_dof, matrix_p1_other_p1, GetExpectedMatrixP1P1(dimension), 1.e-5); // check no throw

        CheckVector(god_of_dof, vector_p1, GetExpectedVectorP1P1(dimension), 1.e-5); // check no throw
    }

} // namespace MoReFEM::TestNS::NonlinearShell
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
