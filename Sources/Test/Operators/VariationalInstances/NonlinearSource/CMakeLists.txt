add_library(TestOperatorNonlinearSource_lib ${LIBRARY_TYPE} "")

target_sources(TestOperatorNonlinearSource_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hxx
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
)

target_link_libraries(TestOperatorNonlinearSource_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestOperatorNonlinearSource_lib Test/Operators/VariationalInstances/NonlinearSource Test/Operators/VariationalInstances/NonlinearSource)                      


add_executable(TestOperatorNonlinearSource ${CMAKE_CURRENT_LIST_DIR}/test.cpp)
target_link_libraries(TestOperatorNonlinearSource TestOperatorNonlinearSource_lib)

morefem_organize_IDE(TestOperatorNonlinearSource Test/Operators/VariationalInstances/NonlinearSource Test/Operators/VariationalInstances/NonlinearSource)    

morefem_boost_test_sequential_mode(NAME OperatorNonlinearSource
                                   EXE TestOperatorNonlinearSource
                                   TIMEOUT 20)
