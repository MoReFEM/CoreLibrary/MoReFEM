// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSOURCE_EXPECTEDRESULTS_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSOURCE_EXPECTEDRESULTS_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearSource/ExpectedResults.hpp"
// *** MoReFEM header guards *** < //

#include <cassert>
#include <cstdlib>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Test/Operators/VariationalInstances/NonlinearSource/ModelSettings.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::LinearSource
{


    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    template<Advanced::Concept::ReactionLaw LawT>
    std::vector<PetscScalar> GetExpectedVector(const NumberingSubset& numbering_subset)
    {
        if constexpr (std::is_same_v<LawT, Advanced::ReactionLawNS::CourtemancheRamirezNattel<time_manager_type>>)
        {
            if (numbering_subset.GetUniqueId() == AsNumberingSubsetId(NumberingSubsetIndex::p1))
            {
                return std::vector<PetscScalar>{
                    -2.7297678021936091e+00, -3.5689411667013040e+00, -2.7657079020306239e+00, -3.2927911966661365e+00
                };
            } else if (numbering_subset.GetUniqueId() == AsNumberingSubsetId(NumberingSubsetIndex::p2))
            {
                return std::vector<PetscScalar>{ 8.7211576047560857e-03,  -1.8213048288565004e-01,
                                                 -2.9722326786122105e-02, -2.1087144992557820e+00,
                                                 -1.7746353606718090e+00, -3.6959692066143637e+00,
                                                 -7.3991349384250837e-02, -1.9986130119749806e-01,
                                                 -2.2636488570638749e+00, -2.8524604561597462e+00 };
            }
        } else if constexpr (std::is_same_v<LawT, Advanced::ReactionLawNS::FitzHughNagumo<time_manager_type>>)
        {
            if (numbering_subset.GetUniqueId() == AsNumberingSubsetId(NumberingSubsetIndex::p1))
            {
                return std::vector<PetscScalar>{
                    -3.3970453333333339e+01, -2.2380496601111446e+01, -3.0911246083935981e+02, -5.5042097948274410e+01
                };
            } else if (numbering_subset.GetUniqueId() == AsNumberingSubsetId(NumberingSubsetIndex::p2))
            {
                return std::vector<PetscScalar>{ -2.5183514998112571e+01, -4.9168222314922502e+02,
                                                 -6.1053038999465070e+02, 3.9625182524491665e+03,
                                                 -1.3822006373676811e+02, 4.2777817374264805e+03,
                                                 -4.7384573829052641e+02, -2.1463850968199709e+04,
                                                 2.0750000494460168e+04,  1.7322235484380283e+03 };
            }
        } else if constexpr (std::is_same_v<LawT, Advanced::ReactionLawNS::MitchellSchaeffer<time_manager_type>>)
        {
            if (numbering_subset.GetUniqueId() == AsNumberingSubsetId(NumberingSubsetIndex::p1))
            {
                return std::vector<PetscScalar>{
                    -1.3508650891845372e+02, -7.1551087300035732e+01, -1.3122898995116198e+03, -2.2542060425059645e+02
                };
            } else if (numbering_subset.GetUniqueId() == AsNumberingSubsetId(NumberingSubsetIndex::p2))
            {
                return std::vector<PetscScalar>{ -1.7515927428837517e+02, -2.3727453463326128e+03,
                                                 -2.8998472107251691e+03, 1.8583902719086607e+04,
                                                 -3.2883683850603711e+02, 1.9773258122334795e+04,
                                                 -2.1992528186391851e+03, -9.5224873889093375e+04,
                                                 9.5217356832904741e+04,  8.3756569600208950e+03 };
            }
        }


        assert(false && "Case not foreseen in the test!");
        exit(EXIT_FAILURE);
    }
    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


} // namespace MoReFEM::TestNS::LinearSource

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSOURCE_EXPECTEDRESULTS_DOT_HXX_
// *** MoReFEM end header guards *** < //
