// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSOURCE_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSOURCE_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearSource/Model.hpp"
// *** MoReFEM header guards *** < //

#include <array>

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/CourtemancheRamirezNattel.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/FitzHughNagumo.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/MitchellSchaeffer.hpp"

#include "Test/Operators/VariationalInstances/NonlinearSource/ExpectedResults.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::LinearSource
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test LinearSource operator");
        return name;
    }


    template<Advanced::Concept::ReactionLaw LawT>
    void Model::CheckOperator(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) morefem_data = parent::GetMoReFEMData();

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::main));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        Unknown::const_shared_ptr potential_ptr{ nullptr };

        switch (numbering_subset.GetUniqueId().Get())
        {
        case EnumUnderlyingType(NumberingSubsetIndex::p1):
            potential_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::potential_P1));
            break;
        case EnumUnderlyingType(NumberingSubsetIndex::p2):
            potential_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::potential_P2));
            break;
        }


        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        auto law = LawT(morefem_data, domain, *quadrature_rule_per_topology_for_operators_);

        const GlobalVariationalOperatorNS::NonLinearSource<LawT> non_linear_source_operator(
            felt_space, potential_ptr, law);

        GlobalVector vector(numbering_subset);
        AllocateGlobalVector(god_of_dof, vector);

        GlobalVector previous_iteration_data(vector);
        vector.ZeroEntries();

        decltype(auto) node_bearer_list = god_of_dof.GetProcessorWiseNodeBearerList();

        // We are forced here to some shaenanigan we wouldn't need to in a real model.
        // As LocalFEltSpace::per_geom_elt_index is an unordered map, we do not control for sure
        // the numbering of the dofs. In a real model it is not an issue: the values from the previous
        // iteration would be computer with the same convention, and it would be self consistant.
        // Here however we give a completely arbitrary vector to check the operator.
        // We need to order it properly to match the choices made by the compiler / STL
        // (which is not the same for gcc/libstdc++ and clang/libc++...)
        // hence this weird step where we assign values depending on the program-wise index
        // of the interface.
        const std::vector<double> arbitrary_values_for_vertices{ -2., 0.5, -8.4, 21.1 };
        const std::vector<double> arbitrary_values_for_edges{ 101.14, 12.1, -64.7, 15., -87.5, -3. };

        std::vector<double> previous_iteration_content;

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            decltype(auto) node_bearer = *node_bearer_ptr;

            const auto index = node_bearer_ptr->GetInterface().GetProgramWiseIndex();

            if (node_bearer.GetInterface().GetNature() == InterfaceNS::Nature::vertex)
                previous_iteration_content.push_back(arbitrary_values_for_vertices[index.Get()]);
            else if (numbering_subset.GetUniqueId() == AsNumberingSubsetId(NumberingSubsetIndex::p2))
                previous_iteration_content.push_back(
                    arbitrary_values_for_edges[index.Get()]); // I don't want edge values for P1.
        }

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(previous_iteration_data);
            const auto size = content.GetSize();
            assert(size.Get() == 4UL || size.Get() == 10UL); // P1 or P2

            for (vector_processor_wise_index_type index{ 0UL }; index < size; ++index)
                content[index] = previous_iteration_content[static_cast<std::size_t>(index.Get())];
        }


        GlobalVectorWithCoefficient vector_with_coeff(vector, 1.);

        non_linear_source_operator.Assemble(std::make_tuple(std::ref(vector_with_coeff)), previous_iteration_data);

        /* BOOST_CHECK_NO_THROW */ //(
        CheckVector(god_of_dof,
                    vector,
                    GetExpectedVector<LawT>(numbering_subset),
                    1.e-10); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    }


} // namespace MoReFEM::TestNS::LinearSource


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSOURCE_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
