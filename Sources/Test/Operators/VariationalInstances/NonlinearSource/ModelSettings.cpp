// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/VariationalInstances/NonlinearSource/ModelSettings.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::LinearSource
{


    void ModelSettings::Init()
    {
        // ****** Time manager ******
        Add<InputDataNS::TimeManager::TimeInit>(0.);
        Add<InputDataNS::TimeManager::TimeStep>(0.1);
        Add<InputDataNS::TimeManager::TimeMax>(0.1);

        Add<InputDataNS::Result::DisplayValue>(0);


        // ****** Numbering subsets ******
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::p2)>>({ "p2" });
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::p1)>>({ "p1" });
        }

        // ****** Unknowns ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P1)>>({ "potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P2)>>({ "potential_P2" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P1)>::Name>({ "potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P2)>::Name>({ "potential_P2" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P1)>::Nature>({ "scalar" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P2)>::Nature>({ "scalar" });
        }

        // ****** Mesh ******
        {
            using input_mesh = InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>;

            SetDescription<input_mesh>({ "mesh" });
            Add<input_mesh::Path>({ "${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh" });
            Add<input_mesh::Format>({ "Medit" });
            Add<input_mesh::Dimension>(3);
            Add<input_mesh::SpaceUnit>(1.);
        }

        // ****** Domains ******
        {
            using input_domain_full_mesh = InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>;
            using input_domain_highest_dim_minus_one =
                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension_minus_one)>;

            SetDescription<input_domain_highest_dim_minus_one>({ "highest_dimension_minus_one" });
            Add<input_domain_highest_dim_minus_one::MeshIndexList>({ EnumUnderlyingType(MeshIndex::mesh) });
            Add<input_domain_highest_dim_minus_one::DimensionList>({ 2 });
            Add<input_domain_highest_dim_minus_one::MeshLabelList>({});
            Add<input_domain_highest_dim_minus_one::GeomEltTypeList>({});

            SetDescription<input_domain_full_mesh>({ "full_mesh" });
            Add<input_domain_full_mesh::MeshIndexList>({ EnumUnderlyingType(MeshIndex::mesh) });
            Add<input_domain_full_mesh::DimensionList>({});
            Add<input_domain_full_mesh::MeshLabelList>({});
            Add<input_domain_full_mesh::GeomEltTypeList>({});
        }

        // ****** Finite element space ******
        {
            {
                using main_section_felt_space = InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::main)>;

                SetDescription<main_section_felt_space>({ "Main finite element space" });

                Add<main_section_felt_space::GodOfDofIndex>(EnumUnderlyingType(MeshIndex::mesh));
                Add<main_section_felt_space::DomainIndex>(EnumUnderlyingType(DomainIndex::highest_dimension_minus_one));
                Add<main_section_felt_space::UnknownList>({ "potential_P1", "potential_P2" });

                Add<main_section_felt_space::ShapeFunctionList>({ "P1", "P2" });

                constexpr auto p1_ns = EnumUnderlyingType(NumberingSubsetIndex::p1);
                constexpr auto p2_ns = EnumUnderlyingType(NumberingSubsetIndex::p2);


                Add<main_section_felt_space::NumberingSubsetList>({
                    p1_ns,
                    p2_ns,
                });
            }
        }


        // ****** Fitz Hugh Nagumo ******
        {
            using fhn = InputDataNS::ReactionNS::FitzHughNagumo;

            Add<fhn::ACoefficient>({ 0.08 });
            Add<fhn::BCoefficient>({ 0.7 });
            Add<fhn::CCoefficient>({ 0.9 });

            Add<InputDataNS::InitialConditionGate::Value>(0.3);
            Add<InputDataNS::InitialConditionGate::WriteGate>(false);
        }


        // ****** Mitchell Schaeffer ******
        {
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauIn>(0.2);
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauOut>(6.);
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauOpen>(120);
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauClose::Nature>("constant");
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::TauClose::Value>(302.);
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::PotentialGate>(0.13);
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::PotentialMin>(0.);
            Add<InputDataNS::ReactionNS::MitchellSchaeffer::PotentialMax>(1.);
        }
    }


} // namespace MoReFEM::TestNS::LinearSource
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
