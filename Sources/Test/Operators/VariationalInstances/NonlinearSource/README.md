**WARNING** The output of this operator was not validated at the time this test was written (it was when 
the operator was first written many years ago).

The main purpose of current test is to ensure when refactoring occurs that we do not change unwillingly 
the results obtained with former code.

It's better to avoid too similar or exactly same values in tests as it might hide mistakes in coding.

