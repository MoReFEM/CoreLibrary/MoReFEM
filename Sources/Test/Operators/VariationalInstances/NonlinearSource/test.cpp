// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <string_view>

#define BOOST_TEST_MODULE linear_source_operator

#include "Utilities/Warnings/Pragma.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/CourtemancheRamirezNattel.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/FitzHughNagumo.hpp"
#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/MitchellSchaeffer.hpp"

#include "Test/Operators/VariationalInstances/NonlinearSource/Model.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::LinearSource;


namespace // anonymous
{

    // clang-format off
    struct OutputDirWrapper
    {
        
        static constexpr std::string_view Path()
        {
            return "${MOREFEM_TEST_OUTPUT_DIR}/Operators/Variational/LinearSource/";
        }
    };


    using fixture_type = MoReFEM::TestNS::FixtureNS::ModelNoInputData
    <
        TestNS::LinearSource::Model,
        OutputDirWrapper,
        time_manager_type
    >;
    // clang-format on


} // namespace

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)

BOOST_AUTO_TEST_CASE(CRN_P1)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) p1_ns = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::p1));

    GetModel().CheckOperator<Advanced::ReactionLawNS::CourtemancheRamirezNattel<time_manager_type>>(p1_ns);
}

BOOST_AUTO_TEST_CASE(CRN_P2)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) p2_ns = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::p2));

    GetModel().CheckOperator<Advanced::ReactionLawNS::CourtemancheRamirezNattel<time_manager_type>>(p2_ns);
}

BOOST_AUTO_TEST_CASE(FHN_P1)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) p1_ns = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::p1));

    GetModel().CheckOperator<Advanced::ReactionLawNS::FitzHughNagumo<time_manager_type>>(p1_ns);
}

BOOST_AUTO_TEST_CASE(FHN_P2)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) p2_ns = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::p2));

    GetModel().CheckOperator<Advanced::ReactionLawNS::FitzHughNagumo<time_manager_type>>(p2_ns);
}

BOOST_AUTO_TEST_CASE(MS_P1)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) p1_ns = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::p1));

    GetModel().CheckOperator<Advanced::ReactionLawNS::MitchellSchaeffer<time_manager_type>>(p1_ns);
}


BOOST_AUTO_TEST_CASE(MS_P2)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) p2_ns = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::p2));

    GetModel().CheckOperator<Advanced::ReactionLawNS::MitchellSchaeffer<time_manager_type>>(p2_ns);
}

BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
