// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_QUASIINCOMPRESSIBLEPK2_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_QUASIINCOMPRESSIBLEPK2_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::TestNS::QuasiIncompressiblePk2
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test QuasiIncompressiblePk2 operator");
        return name;
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    inline void Model::SupplInitializeStep()
    { }


    inline auto Model::GetPenalizationVolumetricPart() const noexcept -> const penalization_law_type&
    {
        assert(!(!penalization_volumetric_));
        return *penalization_volumetric_;
    }

    inline auto Model::GetPenalizationVDeviatoricPart() const noexcept -> const penalization_law_type&
    {
        assert(!(!penalization_deviatoric_));
        return *penalization_deviatoric_;
    }

    inline auto Model::GetNonCstPenalizationVolumetricPart() noexcept -> penalization_law_type&
    {
        return const_cast<penalization_law_type&>(GetPenalizationVolumetricPart());
    }

    inline auto Model::GetNonCstPenalizationVDeviatoricPart() noexcept -> penalization_law_type&
    {
        return const_cast<penalization_law_type&>(GetPenalizationVDeviatoricPart());
    }

} // namespace MoReFEM::TestNS::QuasiIncompressiblePk2


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_QUASIINCOMPRESSIBLEPK2_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
