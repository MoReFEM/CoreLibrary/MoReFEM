// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <array>
#include <cassert>
#include <cstdlib>
#include <functional>
#include <tuple>

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/Unknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Operators/LocalVariationalOperator/Internal/EnumClass.hpp"

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/InputData.hpp"
#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::QuasiIncompressiblePk2
{


    namespace // anonymous
    {


        using ::MoReFEM::Internal::assemble_into_matrix;

        using ::MoReFEM::Internal::assemble_into_vector;


    } // namespace


    void Model::SameCauchyGreen(assemble_into_matrix do_assemble_into_matrix,
                                assemble_into_vector do_assemble_into_vector) const
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::monolithic));

        const auto& displacement_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));
        const auto& pressure_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::pressure));

        const std::array<Unknown::const_shared_ptr, 2> displacement_pressure{ { displacement_ptr, pressure_ptr } };

        const SameCauchyGreenIncompressibleStiffnessOperatorType incompressible_stiffness_operator(
            felt_space,
            displacement_pressure,
            displacement_pressure,
            *solid_,
            GetTimeManager(),
            hyperelastic_deviatoric_law_parent::GetHyperelasticLawPtr(),
            penalization_volumetric_.get(),
            nullptr,
            nullptr,
            nullptr);


        decltype(auto) monolithic_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        GlobalMatrix matrix_p1b_p1(monolithic_numbering_subset, monolithic_numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix_p1b_p1);

        GlobalVector vector_p1b_p1(monolithic_numbering_subset);
        AllocateGlobalVector(god_of_dof, vector_p1b_p1);

        GlobalVector previous_iteration_p1b_p1(vector_p1b_p1);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        {
            matrix_p1b_p1.ZeroEntries();
            vector_p1b_p1.ZeroEntries();
            previous_iteration_p1b_p1.ZeroEntries();

            switch (dimension.Get())
            {
            case 3:
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 0 }, 1., INSERT_VALUES);
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 1 }, 2., INSERT_VALUES);
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 2 }, 3., INSERT_VALUES);
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 3 }, 100., INSERT_VALUES);
                break;
            case 2:
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 0 }, 1., INSERT_VALUES);
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 1 }, 2., INSERT_VALUES);
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 2 }, 100., INSERT_VALUES);
                break;
            case 1:
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 0 }, 1., INSERT_VALUES);
                previous_iteration_p1b_p1.SetValue(vector_program_wise_index_type{ 1 }, 100., INSERT_VALUES);
                break;
            default:
                assert(false);
                exit(EXIT_FAILURE);
            }

            previous_iteration_p1b_p1.Assembly();

            GlobalMatrixWithCoefficient matrix(matrix_p1b_p1, 1.);
            GlobalVectorWithCoefficient vec(vector_p1b_p1, 1.);

            const auto tolerance = 1.e-7;

            if (do_assemble_into_matrix == assemble_into_matrix::yes
                && do_assemble_into_vector == assemble_into_vector::yes)
            {
                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), tolerance));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), tolerance));
            } else if (do_assemble_into_matrix == assemble_into_matrix::yes
                       && do_assemble_into_vector == assemble_into_vector::no)
            {
                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(matrix)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), tolerance));
            } else if (do_assemble_into_matrix == assemble_into_matrix::no
                       && do_assemble_into_vector == assemble_into_vector::yes)
            {
                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(vec)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), tolerance));
            }
        }
    }


} // namespace MoReFEM::TestNS::QuasiIncompressiblePk2
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
