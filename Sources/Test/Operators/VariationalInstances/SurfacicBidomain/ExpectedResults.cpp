// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>

#include "Test/Operators/VariationalInstances/SurfacicBidomain/ExpectedResults.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Test/Operators/VariationalInstances/SurfacicBidomain/ModelSettings.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

namespace MoReFEM::TestNS::SurfacicBidomain
{


    namespace // anonymous
    {

        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP1P1();

        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP2P1();

    } // namespace


    expected_results_type<LinearAlgebraNS::type::matrix> GetExpectedMatrix(const NumberingSubset& row_numbering_subset,
                                                                           const NumberingSubset& col_numbering_subset)
    {
        const auto row_ns_id = row_numbering_subset.GetUniqueId().Get();
        const auto col_ns_id = col_numbering_subset.GetUniqueId().Get();

        if ((row_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1)
             && col_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1))
            || (row_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1)
                && col_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1)))
        {
            return MatrixP1P1();
        } else if (row_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1)
                   && col_ns_id == EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1))
        {
            return MatrixP2P1();
        }

        assert(false && "Case not foreseen in the test!");
        exit(EXIT_FAILURE);
    }


    namespace // anonymous
    {


        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP1P1()
        {
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            constexpr auto f1 = 4.7254948249225402e-03;
            constexpr auto f2 = -1.3542331984682813e-03;
            constexpr auto f3 = -2.1390046759763386e-03;
            constexpr auto f4 = -1.2322569504779200e-03;
            constexpr auto f5 = -3.0084663969365631e-03;
            constexpr auto f6 = -2.7645139009558400e-03;
            constexpr auto f7 = 2.0056691821564330e-03;
            constexpr auto f8 = -3.1982316913856308e-04;
            constexpr auto f9 = -3.3161281454958870e-04;
            constexpr auto f10 = -7.2624887865557015e-04;
            constexpr auto f11 = -7.4982816947762141e-04;
            constexpr auto f12 = 2.9312913467165274e-03;
            constexpr auto f13 = -4.7246350160162564e-04;
            constexpr auto f14 = -1.0315295435816955e-03;
            constexpr auto f15 = 2.0363332666291341e-03;


            return expected_results_type<LinearAlgebraNS::type::matrix>{
                { f1, f1, f2, f2, f3, f3, f4, f4 },
                { f1, 1.0350989649845082e-02, f2, f5, f3, -4.5780093519526780e-03, f4, f6 },
                { f2, f2, f7, f7, f8, f8, f9, f9 },
                { f2, f5, f7, 4.4845434450697542e-03, f8, f10, f9, f11 },
                { f3, f3, f8, f8, f12, f12, f13, f13 },
                { f3, -4.5780093519526771e-03, f8, f10, f12, 6.3357877741899429e-03, f13, f14 },
                { f4, f4, f9, f9, f13, f13, f15, f15 },
                { f4, f6, f9, f11, f13, f14, f15, 4.5458716140151564e-03 }
            };
            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

            assert(false);
            exit(EXIT_FAILURE);
        }


        expected_results_type<LinearAlgebraNS::type::matrix> MatrixP2P1()
        {
            // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            constexpr auto f1 = 1.3150767645280655e-03;
            constexpr auto f2 = -3.0408645199659007e-04;
            constexpr auto f3 = -5.9850358101336178e-04;
            constexpr auto f4 = -4.1248673151811376e-04;
            constexpr auto f5 = -6.1178820542769031e-04;
            constexpr auto f6 = 7.1199791425966561e-04;
            constexpr auto f7 = 5.3667621428140039e-06;
            constexpr auto f8 = -1.0557647097478938e-04;
            constexpr auto f9 = -7.8560473248184034e-04;
            constexpr auto f10 = -1.1208476914336870e-04;
            constexpr auto f11 = 1.0374997738571033e-03;
            constexpr auto f12 = -1.3981027223189418e-04;
            constexpr auto f13 = 2.6432724162894811e-03;
            constexpr auto f14 = -2.4360761301489732e-04;
            constexpr auto f15 = -1.5496447802966662e-03;
            constexpr auto f16 = -8.5002002297791747e-04;
            constexpr auto f17 = 1.9479475014044676e-03;
            constexpr auto f18 = -9.8039810002498390e-04;
            constexpr auto f19 = -1.5812086302360328e-04;
            constexpr auto f20 = -8.0942853835588044e-04;
            constexpr auto f21 = -2.4077439584975005e-03;
            constexpr auto f22 = 1.4692894131714930e-03;
            constexpr auto f23 = 1.9812773252896260e-03;
            constexpr auto f24 = -1.0428227799636183e-03;
            constexpr auto f25 = -3.6335411514085679e-04;
            constexpr auto f26 = -1.2211523584785869e-04;
            constexpr auto f27 = -2.2705236624733971e-04;
            constexpr auto f28 = 7.1252171723605508e-04;
            constexpr auto f29 = 2.2296162030949998e-03;
            constexpr auto f30 = -8.7628777990350138e-04;
            constexpr auto f31 = -1.3732365466056844e-03;
            constexpr auto f32 = 1.9908123414185568e-05;
            constexpr auto f33 = -2.2470034298959639e-03;
            constexpr auto f34 = -9.0436811313689797e-04;
            constexpr auto f35 = 1.9644266834528265e-03;
            constexpr auto f36 = 1.1869448595800357e-03;
            constexpr auto f37 = -1.7204184438731627e-03;
            constexpr auto f38 = 1.3616607356369394e-03;
            constexpr auto f39 = -1.0820124075557138e-03;
            constexpr auto f40 = 1.4407701157919373e-03;

            return expected_results_type<LinearAlgebraNS::type::matrix>{ { f1, f1, f2, f2, f3, f3, f4, f4 },
                                                                         { f1,
                                                                           2.9301535290561309e-03,
                                                                           f2,
                                                                           -7.0817290399318019e-04,
                                                                           f3,
                                                                           -1.2970071620267238e-03,
                                                                           f4,
                                                                           -9.2497346303622757e-04 },
                                                                         { f5, f5, f6, f6, f7, f7, f8, f8 },
                                                                         { f5,
                                                                           -1.3235764108553805e-03,
                                                                           f6,
                                                                           1.5817308554382939e-03,
                                                                           f7,
                                                                           -1.8133989173853298e-05,
                                                                           f8,
                                                                           -2.4002045540906010e-04 },
                                                                         { f9, f9, f10, f10, f11, f11, f12, f12 },
                                                                         { f9,
                                                                           -1.6712094649636807e-03,
                                                                           f10,
                                                                           -2.5303705174621876e-04,
                                                                           f11,
                                                                           2.2327345746331693e-03,
                                                                           f12,
                                                                           -3.0848805792326974e-04 },
                                                                         { f13, f13, f14, f14, f15, f15, f16, f16 },
                                                                         { f13,
                                                                           5.6865448325789615e-03,
                                                                           f14,
                                                                           -4.8721522602979453e-04,
                                                                           f15,
                                                                           -3.2992895605933321e-03,
                                                                           f16,
                                                                           -1.9000400459558348e-03 },
                                                                         { f17, f17, f18, f18, f19, f19, f20, f20 },
                                                                         { f17,
                                                                           4.2958950028089355e-03,
                                                                           f18,
                                                                           -2.1607962000499679e-03,
                                                                           f19,
                                                                           -3.1624172604720666e-04,
                                                                           f20,
                                                                           -1.8188570767117605e-03 },
                                                                         { f21, f21, f22, f22, f23, f23, f24, f24 },
                                                                         { f21,
                                                                           -5.2154879169950012e-03,
                                                                           f22,
                                                                           3.2540488801809115e-03,
                                                                           f23,
                                                                           4.2780247044171774e-03,
                                                                           f24,
                                                                           -2.3165856676030869e-03 },
                                                                         { f25, f25, f26, f26, f27, f27, f28, f28 },
                                                                         { f25,
                                                                           -8.2670823028171351e-04,
                                                                           f26,
                                                                           -2.7309798515519877e-04,
                                                                           f27,
                                                                           -4.8297224595416082e-04,
                                                                           f28,
                                                                           1.5827784613910729e-03 },
                                                                         { f29, f29, f30, f30, f31, f31, f32, f32 },
                                                                         { f29,
                                                                           4.8592324061900016e-03,
                                                                           f30,
                                                                           -1.9525755598070031e-03,
                                                                           f31,
                                                                           -2.9464730932113685e-03,
                                                                           f32,
                                                                           3.9816246828371245e-05 },
                                                                         { f33, f33, f34, f34, f35, f35, f36, f36 },
                                                                         { f33,
                                                                           -4.8940068597919280e-03,
                                                                           f34,
                                                                           -2.0396763339496464e-03,
                                                                           f35,
                                                                           4.2443234207435775e-03,
                                                                           f36,
                                                                           2.6893597729979964e-03 },
                                                                         { f37, f37, f38, f38, f39, f39, f40, f40 },
                                                                         { f37,
                                                                           -3.8408368877463255e-03,
                                                                           f38,
                                                                           3.0387915251118046e-03,
                                                                           f39,
                                                                           -2.3949649227872783e-03,
                                                                           f40,
                                                                           3.1970102854217996e-03 } };

            // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


            assert(false);
            exit(EXIT_FAILURE);
        }


    } // namespace


} // namespace MoReFEM::TestNS::SurfacicBidomain

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
