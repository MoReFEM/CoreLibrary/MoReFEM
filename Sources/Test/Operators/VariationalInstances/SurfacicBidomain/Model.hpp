// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_SURFACICBIDOMAIN_MODEL_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_SURFACICBIDOMAIN_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/SurfacicBidomain.hpp"

#include "Model/Model.hpp" // IWYU pragma: export

#include "Test/Operators/VariationalInstances/SurfacicBidomain/ModelSettings.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::SurfacicBidomain
{


    //! \copydoc doxygen_hide_model_4_test
    class Model : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<time_manager_type, ParameterNS::TimeDependencyNS::None>;

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type =
            FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector, time_manager_type>;

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{

      private:
        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();
        ///@}

      public:
        /*!
         * \brief Check the operator yields expected results.
         *
         * Three main cases are checked:
         * - Unknowns and test unknowns are the same.
         * - Unknowns and test unknowns differ but share same shape function label.
         * Result should be the same (at least in sequential - in parallel repartition on ranks may differ).
         * - Unknowns and test unknowns differ and don't share shape function label.
         *
         * \copydoc doxygen_hide_check_bilinear_operator_ns_arg
         */
        void CheckOperator(const NumberingSubset& row_numbering_subset,
                           const NumberingSubset& col_numbering_subset) const;


      private:
        //! Quadrature rule topology used for the operators.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_{ nullptr };
    };


} // namespace MoReFEM::TestNS::SurfacicBidomain


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/VariationalInstances/SurfacicBidomain/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_SURFACICBIDOMAIN_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
