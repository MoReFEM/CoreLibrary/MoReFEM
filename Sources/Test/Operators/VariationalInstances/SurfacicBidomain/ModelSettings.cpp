// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/VariationalInstances/SurfacicBidomain/ModelSettings.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::SurfacicBidomain
{


    void ModelSettings::Init()
    {
        // ****** Numbering subsets ******
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1)>>(
                { "monolithic_p2p1" });
            SetDescription<
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1)>>(
                { "monolithic_unknown_p1" });
            SetDescription<
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1)>>(
                { "monolithic_other_unknown_p1" });

            SetDescription<
                InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::transcellular_potential)>>(
                { "transcellular_potential" });

            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::fiber_unknown)>>(
                { "fiber_unknown" });
        }

        // ****** Unknowns ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P1)>>(
                { "transcellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P1)>>(
                { "extracellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_transcellular_potential_P1)>>(
                { "other_transcellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_extracellular_potential_P1)>>(
                { "other_extracellular_potential_P1" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P2)>>(
                { "transcellular_potential_P2" });
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P2)>>(
                { "extracellular_potential_P2" });

            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_for_fiber)>>(
                { "vectorial_unknown_for_fiber" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P1)>::Name>(
                { "transcellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P1)>::Name>(
                { "extracellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_transcellular_potential_P1)>::Name>(
                { "other_transcellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_extracellular_potential_P1)>::Name>(
                { "other_extracellular_potential_P1" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P2)>::Name>(
                { "transcellular_potential_P2" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P2)>::Name>(
                { "extracellular_potential_P2" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_for_fiber)>::Name>(
                { "vectorial_unknown_for_fiber" });


            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P1)>::Nature>(
                { "scalar" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P1)>::Nature>(
                { "scalar" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_transcellular_potential_P1)>::Nature>(
                { "scalar" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_extracellular_potential_P1)>::Nature>(
                { "scalar" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::transcellular_potential_P2)>::Nature>(
                { "scalar" });
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::extracellular_potential_P2)>::Nature>(
                { "scalar" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_for_fiber)>::Nature>(
                { "vectorial" });
        }

        // ****** Mesh ******
        {
            using input_mesh = InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>;

            SetDescription<input_mesh>({ "mesh" });
            Add<input_mesh::Path>({ "${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh" });
            Add<input_mesh::Format>({ "Medit" });
            Add<input_mesh::Dimension>(3);
            Add<input_mesh::SpaceUnit>(1.);
        }

        // ****** Domains ******
        {
            using input_domain_full_mesh = InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>;
            using input_domain_highest_dim_minus_one =
                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension_minus_one)>;

            SetDescription<input_domain_highest_dim_minus_one>({ "highest_dimension_minus_one" });
            Add<input_domain_highest_dim_minus_one::MeshIndexList>({ EnumUnderlyingType(MeshIndex::mesh) });
            Add<input_domain_highest_dim_minus_one::DimensionList>({ 2 });
            Add<input_domain_highest_dim_minus_one::MeshLabelList>({});
            Add<input_domain_highest_dim_minus_one::GeomEltTypeList>({});

            SetDescription<input_domain_full_mesh>({ "full_mesh" });
            Add<input_domain_full_mesh::MeshIndexList>({ EnumUnderlyingType(MeshIndex::mesh) });
            Add<input_domain_full_mesh::DimensionList>({});
            Add<input_domain_full_mesh::MeshLabelList>({});
            Add<input_domain_full_mesh::GeomEltTypeList>({});
        }

        // ****** Finite element space ******
        {
            {
                using main_section_felt_space = InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::main)>;

                SetDescription<main_section_felt_space>({ "Main finite element space" });

                Add<main_section_felt_space::GodOfDofIndex>(EnumUnderlyingType(MeshIndex::mesh));
                Add<main_section_felt_space::DomainIndex>(EnumUnderlyingType(DomainIndex::highest_dimension_minus_one));
                Add<main_section_felt_space::UnknownList>({ "transcellular_potential_P1",
                                                            "extracellular_potential_P1",
                                                            "other_transcellular_potential_P1",
                                                            "other_extracellular_potential_P1",
                                                            "transcellular_potential_P2",
                                                            "extracellular_potential_P2",
                                                            "vectorial_unknown_for_fiber" });

                Add<main_section_felt_space::ShapeFunctionList>({ "P1", "P1", "P1", "P1", "P2", "P2", "P1" });

                constexpr auto monolithic_unknown_p1_ns =
                    EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1);
                constexpr auto monolithic_other_unknown_p1_ns =
                    EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1);
                constexpr auto monolithic_p2p1_ns = EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1);

                constexpr auto fiber_numbering_subset = EnumUnderlyingType(NumberingSubsetIndex::fiber_unknown);

                Add<main_section_felt_space::NumberingSubsetList>({ monolithic_unknown_p1_ns,
                                                                    monolithic_unknown_p1_ns,
                                                                    monolithic_other_unknown_p1_ns,
                                                                    monolithic_other_unknown_p1_ns,
                                                                    monolithic_p2p1_ns,
                                                                    monolithic_p2p1_ns,
                                                                    fiber_numbering_subset });
            }


            {
                using section_felt_space =
                    InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::transcellular_potential)>;

                SetDescription<section_felt_space>({ "Finite element space for transcellular potential alone" });

                Add<section_felt_space::GodOfDofIndex>(EnumUnderlyingType(MeshIndex::mesh));
                Add<section_felt_space::DomainIndex>(EnumUnderlyingType(DomainIndex::highest_dimension_minus_one));
                Add<section_felt_space::UnknownList>({ "transcellular_potential_P1" });

                Add<section_felt_space::ShapeFunctionList>({ "P1" });
                Add<section_felt_space::NumberingSubsetList>(
                    { EnumUnderlyingType(NumberingSubsetIndex::transcellular_potential) });
            }
        }

        // ****** Diffusion ******
        {
            Add<InputDataNS::Diffusion::Density::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Density::Value>(0.2);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_trans_diffusion_tensor)>>(
                { "intracellular_trans_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_trans_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_trans_diffusion_tensor)>::Value>(1.2e-3);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_trans_diffusion_tensor)>>(
                { "extracellular_trans_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_trans_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_trans_diffusion_tensor)>::Value>(1.5e-3);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_fiber_diffusion_tensor)>>(
                { "intracellular_fiber_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_fiber_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::intracellular_fiber_diffusion_tensor)>::Value>(3.4e-3);

            SetDescription<
                InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_fiber_diffusion_tensor)>>(
                { "extracellular_fiber_diffusion_tensor" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_fiber_diffusion_tensor)>::Nature>({ "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(
                TensorIndex::extracellular_fiber_diffusion_tensor)>::Value>(3.7e-3);

            // Note: in the Bidomain model, this coefficient is much more complex (piecewise constant by domain, with 7
            // different domains!) For the scope of current test on a very simple mesh I have cut down to constant
            // value. Name 'conductivity_coefficient' has hence been changed into 'conductivity_coefficient'
            SetDescription<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::conductivity_coefficient)>>(
                { "conductivity_coefficient" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::conductivity_coefficient)>::Nature>(
                { "constant" });
            Add<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::conductivity_coefficient)>::Value>(0.45);
        }

        // ****** Fiber ******
        {
            SetDescription<vectorial_fiber_input_section>({ "fiber_vector" });
            Add<vectorial_fiber_input_section::DomainIndex>(
                EnumUnderlyingType(DomainIndex::highest_dimension_minus_one));
            Add<vectorial_fiber_input_section::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::main));
            Add<vectorial_fiber_input_section::UnknownName>("vectorial_unknown_for_fiber");
            Add<vectorial_fiber_input_section::EnsightFile>(
                "${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/Bidomain/Data/fibers_one_tetra.vct");
            // < not a mistake: we're using the same as the one from the Bidomain test.

            SetDescription<scalar_fiber_input_section>({ "fiber_scalar" });
            Add<scalar_fiber_input_section::DomainIndex>(EnumUnderlyingType(DomainIndex::highest_dimension_minus_one));
            Add<scalar_fiber_input_section::FEltSpaceIndex>(
                EnumUnderlyingType(FEltSpaceIndex::transcellular_potential));
            Add<scalar_fiber_input_section::UnknownName>("transcellular_potential_P1");
            Add<scalar_fiber_input_section::EnsightFile>("${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/"
                                                         "SurfacicBidomain/Data/angles_one_tetra.scl");
        }

        // ****** Rectangular source time parameter ******
        {
            SetDescription<input_rectangular_source_time_parameter>({ "rectangular_source_time_parameter" });
            Add<input_rectangular_source_time_parameter::InitialTimeOfActivationList>(0.);
            Add<input_rectangular_source_time_parameter::FinalTimeOfActivationList>(1.);
        }
    }


} // namespace MoReFEM::TestNS::SurfacicBidomain
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
