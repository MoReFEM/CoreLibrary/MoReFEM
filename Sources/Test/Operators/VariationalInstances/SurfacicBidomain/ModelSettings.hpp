// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_SURFACICBIDOMAIN_MODELSETTINGS_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_SURFACICBIDOMAIN_MODELSETTINGS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/RectangularSourceTimeParameter.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Reaction/ReactionCoefficient.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
// IWYU pragma: end_exports

#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Tools/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::SurfacicBidomain
{

    //! Convenient enum class.
    enum class ForceIndexList : std::size_t { transcellular_current_applied_on_square = 1UL };


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1UL };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { highest_dimension_minus_one = 1UL, full_mesh = 2UL };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { main = 1UL, transcellular_potential = 2UL };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t {
        transcellular_potential_P1 = 1,
        extracellular_potential_P1 = 2,

        other_transcellular_potential_P1 = 11,
        other_extracellular_potential_P1 = 12,

        transcellular_potential_P2 = 21,
        extracellular_potential_P2 = 22,

        vectorial_unknown_for_fiber = 100
    };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        monolithic_unknown_p1 = 1,
        monolithic_other_unknown_p1 = 2,
        monolithic_p2p1 = 3,

        transcellular_potential = 10,

        fiber_unknown = 100
    };


    //! Convenient enum class.
    enum class TensorIndex : std::size_t {
        intracellular_trans_diffusion_tensor = 1UL,
        extracellular_trans_diffusion_tensor = 2UL,
        intracellular_fiber_diffusion_tensor = 3UL,
        extracellular_fiber_diffusion_tensor = 4UL,
        conductivity_coefficient = 5UL
    };


    //! Convenient enum class.
    enum class FiberIndex : std::size_t { fiber = 1UL, angles = 2UL };


    //! Convenient enum class.
    enum class RectangularSourceTimeParameterIndex : std::size_t { transcellular_current_applied_on_square = 1UL };


    //! Convenient alias to a section.
    using vectorial_fiber_input_section = InputDataNS::
        Fiber<EnumUnderlyingType(FiberIndex::fiber), FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>;

    //! Convenient alias to a section.
    using scalar_fiber_input_section = InputDataNS::
        Fiber<EnumUnderlyingType(FiberIndex::angles), FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>;


    //! Convenient alias to a section.
    using input_rectangular_source_time_parameter = InputDataNS::RectangularSourceTimeParameter<EnumUnderlyingType(
        RectangularSourceTimeParameterIndex::transcellular_current_applied_on_square)>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::main)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::main)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::transcellular_potential)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::transcellular_potential)>,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_unknown_p1)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_p2p1)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic_other_unknown_p1)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::transcellular_potential)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::fiber_unknown)>::IndexedSectionDescription,

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::transcellular_potential_P1 ),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::transcellular_potential_P2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::extracellular_potential_P1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::extracellular_potential_P2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::other_extracellular_potential_P1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::other_transcellular_potential_P1),
        

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::vectorial_unknown_for_fiber),

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension_minus_one)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension_minus_one)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

        InputDataNS::Diffusion::Density,

        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_trans_diffusion_tensor)>::IndexedSectionDescription,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_trans_diffusion_tensor)>::IndexedSectionDescription,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_fiber_diffusion_tensor)>::IndexedSectionDescription,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_fiber_diffusion_tensor)>::IndexedSectionDescription,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::conductivity_coefficient)>::IndexedSectionDescription,

        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_trans_diffusion_tensor)>,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_trans_diffusion_tensor)>,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::intracellular_fiber_diffusion_tensor)>,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::extracellular_fiber_diffusion_tensor)>,
        InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::conductivity_coefficient)>,

        vectorial_fiber_input_section::IndexedSectionDescription,
        vectorial_fiber_input_section,

        scalar_fiber_input_section::IndexedSectionDescription,
        scalar_fiber_input_section,

        input_rectangular_source_time_parameter::IndexedSectionDescription,
        input_rectangular_source_time_parameter,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMDataForTest<ModelSettings, time_manager_type>;


} // namespace MoReFEM::TestNS::SurfacicBidomain


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_SURFACICBIDOMAIN_MODELSETTINGS_DOT_HPP_
// *** MoReFEM end header guards *** < //
