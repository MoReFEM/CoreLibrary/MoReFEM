// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <string_view>

#define BOOST_TEST_MODULE surfacic_bidomain_operator

#include "Utilities/Warnings/Pragma.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Test/Operators/VariationalInstances/SurfacicBidomain/Model.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::SurfacicBidomain;


namespace // anonymous
{

    // clang-format off
    struct OutputDirWrapper
    {
        
        static constexpr std::string_view Path()
        {
            return "${MOREFEM_TEST_OUTPUT_DIR}/Operators/Variational/SurfacicBidomain/";
        }
    };


    using fixture_type = MoReFEM::TestNS::FixtureNS::ModelNoInputData
    <
        TestNS::SurfacicBidomain::Model,
        OutputDirWrapper
    >;
    // clang-format on


} // namespace

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)

BOOST_AUTO_TEST_CASE(same_unknown)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) monolithic_unknown_p1_ns =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic_unknown_p1));

    GetModel().CheckOperator(monolithic_unknown_p1_ns, monolithic_unknown_p1_ns);
}


BOOST_AUTO_TEST_CASE(different_unknown_P1_P1)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) monolithic_unknown_p1_ns =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic_unknown_p1));
    decltype(auto) monolithic_other_unknown_p1_ns =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic_other_unknown_p1));

    GetModel().CheckOperator(monolithic_unknown_p1_ns, monolithic_other_unknown_p1_ns);
}


BOOST_AUTO_TEST_CASE(different_unknown_P2_P1)
{
    decltype(auto) god_of_dof = GetModel().GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) monolithic_unknown_p1_ns =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic_unknown_p1));
    decltype(auto) monolithic_p2p1_ns =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic_p2p1));

    GetModel().CheckOperator(monolithic_p2p1_ns, monolithic_unknown_p1_ns);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
