// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/Viscoelasticity/ExpectedResults.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,modernize-use-std-numbers)
namespace MoReFEM::TestNS::ViscoelasticityNS
{


    template<>
    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP1P1<Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                              ViscoelasticityPolicyNS::DerivatesWithRespectTo::displacement>()
    {
        constexpr auto f1 = -4.0464991915660015e-01;
        constexpr auto f2 = -7.3375315164003918e-01;
        constexpr auto f3 = 8.0329073988478439e-01;
        constexpr auto f4 = -2.4685782753512582e+00;
        constexpr auto f5 = 2.9252600994659650e+00;
        constexpr auto f6 = -7.1406174587593774e-01;

        return expected_results_type<LinearAlgebraNS::type::matrix>{
            { 1.6683175088192581e+01,
              -2.0529251886063387e+00,
              -2.9188566575876380e+00,
              -4.5681762884643220e+00,
              -4.5257965335727135e-01,
              -2.6503080316848532e-01,
              -5.3860163531953535e+00,
              4.6182109776118785e-01,
              8.0613305394024792e-01,
              -6.7289824465329060e+00,
              2.0436837442024212e+00,
              2.3777544068158751e+00 },
            { 1.5057733282050412e+00,
              1.4066972233595427e+01,
              -3.5759379643337290e+00,
              -8.7951103436058276e-01,
              -1.9013336547564581e+00,
              3.5192268666221782e+00,
              8.5259600765142662e-02,
              -8.3300526478630772e+00,
              -3.5984384759086767e+00,
              -7.1152189460960191e-01,
              -3.8355859309758888e+00,
              3.6551495736202302e+00 },
            { -2.0230068810850574e+00,
              1.2733577171134840e+01,
              3.1765687953075712e+01,
              7.5873684257856666e-01,
              -3.4538002077758194e+00,
              -8.8350767061537674e+00,
              7.8609293034862915e-01,
              -3.3137438568633057e+00,
              -9.4458691314808423e+00,
              4.7817710815786280e-01,
              -5.9660331064957148e+00,
              -1.3484742115441092e+01 },
            { -4.7730810419837431e+00,
              1.9046964073935877e+00,
              2.4871573327338456e+00,
              -6.9617647429100996e-02,
              f1,
              f2,
              2.8352686503917552e+00,
              -3.0624114485193105e-02,
              -3.5796524175555858e-02,
              2.0074300390210880e+00,
              -1.4694223737517953e+00,
              -1.7176076569182503e+00 },
            { -6.7028253911190583e-01,
              -4.3094793828622802e+00,
              7.0774499184933037e-01,
              f3,
              -1.9071744028395452e+00,
              f4,
              -2.2665131358774057e-01,
              5.3995343783828380e+00,
              3.0005306018401834e+00,
              9.3643112814861651e-02,
              8.1711940731898580e-01,
              -1.2396973183382569e+00 },
            { 9.7313774751760629e-01,
              -5.8594599230845992e+00,
              -1.1649044593723657e+01,
              -7.1406174587593785e-01,
              f5,
              3.5090070695971822e+00,
              -1.8373705836491821e-02,
              2.0809385694944285e-01,
              3.0758055728892897e+00,
              -2.4070229580517638e-01,
              2.7261059666691909e+00,
              5.0642319512371907e+00 },
            { -5.2710914229763084e+00,
              4.7353026189114866e-01,
              7.0386110047961781e-01,
              2.9851295365959114e+00,
              1.0073110855947742e-01,
              -4.3714350581992398e-02,
              5.7700729253887295e-01,
              f1,
              f2,
              1.7089545938415265e+00,
              -1.6961145129402611e-01,
              7.3606401742413730e-02 },
            { -4.5739157628104499e-01,
              -3.3094788363793053e+00,
              2.3694679153892690e+00,
              -1.6048720551369508e-01,
              2.7266028338538195e+00,
              4.5984339892703160e-02,
              f3,
              -1.2605494628715721e+00,
              f4,
              -1.8541195809004435e-01,
              1.8434254653970565e+00,
              5.3126020069286253e-02 },
            { 1.4340824549973930e-01,
              -3.3020346927333462e+00,
              -8.7905314698735388e+00,
              9.4066566852946898e-02,
              6.2107552906822280e-02,
              2.8056118268010577e+00,
              f6,
              f5,
              4.1556320095651573e+00,
              4.7658693352325160e-01,
              3.1466704036055976e-01,
              1.8292876335073234e+00 },
            { -6.6390026232325283e+00,
              -3.2530148067839920e-01,
              -2.7216177562582483e-01,
              1.6526643992975112e+00,
              7.5649846395439380e-01,
              1.0424983053905168e+00,
              1.9737404102647236e+00,
              -2.6547064119394885e-02,
              -3.6583378124652732e-02,
              3.0125978136702920e+00,
              f1,
              f2 },
            { -3.7809921281209113e-01,
              -6.4480140143538405e+00,
              4.9872505709513154e-01,
              2.3670749998949314e-01,
              1.0819052237421845e+00,
              -1.0966329311636240e+00,
              -6.6189902706218640e-01,
              4.1910677323518097e+00,
              3.0664861494197515e+00,
              f3,
              1.1750410582598463e+00,
              f4 },
            { 9.0646088806771297e-01,
              -3.5720825553168960e+00,
              -1.1326111889478495e+01,
              -1.3874166355557555e-01,
              4.6643255540303236e-01,
              2.5204578097555288e+00,
              -5.3657478636199539e-02,
              1.8038990044789902e-01,
              2.2144315490263953e+00,
              f6,
              f5,
              6.5912225306965739e+00 },
        };
    }


    template<>
    expected_results_type<LinearAlgebraNS::type::vector>
    GetExpectedVectorP1P1<Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                              ViscoelasticityPolicyNS::DerivatesWithRespectTo::displacement>()
    {
        return expected_results_type<LinearAlgebraNS::type::vector>{
            2.4985545370828355, -0.8709239322195668, -8.855154072237047,  -1.317393820675634,
            2.337240538619038,  2.2937270086654062,  -0.1770158969446071, -1.6502452152078242,
            2.889029931944329,  -1.004144819462594,  0.1839286088083532,  3.6723971316273096
        };
    }


} // namespace MoReFEM::TestNS::ViscoelasticityNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,modernize-use-std-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
