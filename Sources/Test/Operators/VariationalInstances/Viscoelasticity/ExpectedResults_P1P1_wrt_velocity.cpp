// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep

#include "Test/Operators/VariationalInstances/Viscoelasticity/ExpectedResults.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::ViscoelasticityNS
{


    template<>
    expected_results_type<LinearAlgebraNS::type::matrix>
    GetExpectedMatrixP1P1<Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                              ViscoelasticityPolicyNS::DerivatesWithRespectTo::velocity>()
    {
        constexpr auto f1 = 2.3394433189211983e-01;
        constexpr auto f2 = -3.8175954720866578e-01;
        constexpr auto f3 = -4.7187026091209638e-01;
        constexpr auto f4 = -6.9597717371807155e-01;
        constexpr auto f5 = -3.9873882760838608e-01;
        constexpr auto f6 = 4.1293547765060878e-01;
        constexpr auto f7 = 1.0981908477905373e+00;
        constexpr auto f8 = -4.8664924130949250e-01;
        constexpr auto f9 = -1.6982833966308604e+00;
        constexpr auto f10 = -8.2332631460827410e-01;
        constexpr auto f11 = -1.6116466690037161e+00;
        constexpr auto f12 = 8.6224557239821253e-01;
        constexpr auto f13 = -2.2118139181654977e+00;
        constexpr auto f14 = 8.6803283125562047e-01;
        constexpr auto f15 = 7.7237733602497516e-01;
        constexpr auto f16 = -1.9988674626888405e+00;
        constexpr auto f17 = -3.8924817393577615e-01;
        constexpr auto f18 = -2.3196003924451163e+00;
        constexpr auto f19 = 6.9165103781866855e-01;
        constexpr auto f20 = 1.6066577262365114e-01;
        constexpr auto f21 = -8.3863631416026196e-01;
        constexpr auto f22 = -6.3265722501585249e-03;
        constexpr auto f23 = 6.6550971210556182e-03;
        constexpr auto f24 = -3.9007604141827472e-03;
        constexpr auto f25 = -1.8716833853324399e-01;
        constexpr auto f26 = 2.0256480440536611e+00;
        constexpr auto f27 = -6.7490997337013769e-01;
        constexpr auto f28 = 5.3030494101202219e-01;
        constexpr auto f29 = -5.5784250090912135e-01;
        constexpr auto f30 = -2.1910045272357700e-01;
        constexpr auto f31 = 2.3047785348632083e-01;
        constexpr auto f32 = -1.3509027304512833e-01;
        constexpr auto f33 = -8.3863631416026219e-01;
        constexpr auto f34 = 1.8380688903767735e+00;
        constexpr auto f35 = 4.2989678001672409e-02;
        constexpr auto f36 = -4.5222036671891203e-02;
        constexpr auto f37 = 2.6506049016224331e-02;
        constexpr auto f38 = 5.6318057353331252e-01;
        constexpr auto f39 = 1.0652708149561416e-02;
        constexpr auto f40 = -8.9292962183904545e-01;
        constexpr auto f41 = -7.2386195097030856e-02;
        constexpr auto f42 = 7.6886788859255326e-03;
        constexpr auto f43 = -6.4447922853626660e-01;
        constexpr auto f44 = -1.9763147592177739e-02;
        constexpr auto f45 = 1.6565834394476562e+00;

        return expected_results_type<LinearAlgebraNS::type::matrix>{

            { 1.4764755485351235e+00,
              f1,
              -2.1929571325824364e+00,
              f2,
              f3,
              2.3246606262527716e-01,
              f4,
              -1.7500954863063228e-01,
              8.6230022216662228e-01,
              f5,
              f6,
              f7 },
            { f1,
              4.1332563802428508e+00,
              -8.6616048448824490e-01,
              f8,
              f9,
              1.3125573021837704e+00,
              2.0195890820337245e-01,
              f10,
              -1.3086423900937387e+00,
              5.0746001214000289e-02,
              f11,
              f12 },
            { -2.1929571325824373e+00,
              -8.6616048448824501e-01,
              6.5302817732994569e+00,
              1.0297054131076890e+00,
              4.8303132239904567e-01,
              f13,
              f14,
              f15,
              f16,
              2.9521888821912734e-01,
              f17,
              f18 },
            { f2,
              f8,
              1.0297054131076888e+00,
              f19,
              f20,
              f21,
              f22,
              f23,
              f24,
              -3.0356491835984423e-01,
              3.1932837156478577e-01,
              f25 },
            { f3, f9, 4.8303132239904534e-01, f20, f26, f27, f28, f29, 3.2696892401622057e-01, f30, f31, f32 },
            { 2.3246606262527722e-01,
              1.3125573021837706e+00,
              f13,
              f33,
              f27,
              f34,
              f35,
              f36,
              f37,
              f38,
              -5.9242529214174167e-01,
              3.4723897877249965e-01 },
            { f4, 2.0195890820337217e-01, f14, f22, f28, f35, f19, f20, f21, f39, f40, f41 },
            { -1.7500954863063234e-01,
              f10,
              f15,
              f23,
              f29,
              f36,
              1.6066577262365117e-01,
              f26,
              f27,
              f42,
              f43,
              -5.2245325982946553e-02 },
            { 8.6230022216662328e-01,
              -1.3086423900937383e+00,
              f16,
              f24,
              3.2696892401622052e-01,
              f37,
              f33,
              f27,
              f34,
              f44,
              f45,
              1.3429252329584310e-01 },
            { f5,
              5.0746001214000337e-02,
              2.9521888821912723e-01,
              -3.0356491835984445e-01,
              f30,
              f38,
              f39,
              f42,
              f44,
              f19,
              1.6066577262365106e-01,
              f21 },
            { f6, f11, f17, 3.1932837156478583e-01, f31, -5.9242529214174156e-01, f40, f43, f45, f20, f26, f27 },
            { f7,
              f12,
              f18,
              f25,
              f32,
              3.4723897877249960e-01,
              f41,
              -5.2245325982946546e-02,
              1.3429252329584307e-01,
              f33,
              f27,
              f34 },
        };
    }


    template<>
    expected_results_type<LinearAlgebraNS::type::vector>
    GetExpectedVectorP1P1<Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::
                              ViscoelasticityPolicyNS::DerivatesWithRespectTo::velocity>()
    {
        return expected_results_type<LinearAlgebraNS::type::vector>{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                                                     0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    }


} // namespace MoReFEM::TestNS::ViscoelasticityNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
