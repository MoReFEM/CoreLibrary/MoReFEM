// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_VISCOELASTICITY_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_VISCOELASTICITY_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/Viscoelasticity/Model.hpp"
// *** MoReFEM header guards *** < //
#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4J6.hpp"


namespace MoReFEM::TestNS::ViscoelasticityNS
{


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    Model<DerivatesWithRespectToT>::Model(morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    {
        decltype(auto) mpi = parent::GetMpi();

        if (mpi.template Nprocessor<int>() > 1)
        {
            throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                            "a vector; the expected values assume the dof numbering of the sequential case. Please "
                            "run it sequentially.");
        }
    }


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    void Model<DerivatesWithRespectToT>::SupplInitialize()
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetMoReFEMData();

        decltype(auto) domain_manager = DomainManager::GetInstance();

        decltype(auto) domain_volume = domain_manager.GetDomain(AsDomainId(DomainIndex::volume));

        quadrature_rule_per_topology_for_operators_ = std::make_unique<const QuadratureRulePerTopology>(10, 3);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

        solid_ = std::make_unique<Solid<time_manager_type>>(morefem_data,
                                                            domain_volume,
                                                            quadrature_rule_per_topology,
                                                            -1.); // Negative to cancel CheckConsistancy issues.

        // Operator gets a contribution only in dynamic mode
        parent::GetNonCstTimeManager().IncrementTime();

        // Required to enable construction of an operator after initialization step.
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    void Model<DerivatesWithRespectToT>::Forward()
    { }


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    void Model<DerivatesWithRespectToT>::SupplFinalizeStep()
    { }


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    void Model<DerivatesWithRespectToT>::SupplFinalize()
    { }


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    inline const std::string& Model<DerivatesWithRespectToT>::ClassName()
    {
        static std::string name("Test for second-piola Kirchhoff operator");
        return name;
    }


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    inline bool Model<DerivatesWithRespectToT>::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    // clang-format off
    template
    <
        Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ViscoelasticityPolicyNS::DerivatesWithRespectTo DerivatesWithRespectToT
    >
    // clang-format on
    inline void Model<DerivatesWithRespectToT>::SupplInitializeStep()
    { }


} // namespace MoReFEM::TestNS::ViscoelasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_VISCOELASTICITY_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
