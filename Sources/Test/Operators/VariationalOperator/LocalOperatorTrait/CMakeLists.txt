add_executable(TestTraitLocalOperator)


target_sources(TestTraitLocalOperator
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestTraitLocalOperator
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)

morefem_organize_IDE(TestTraitLocalOperator Test/Operators/VariationalOperator Test/Operators/VariationalOperator/LocalOperatorTrait)
                  
morefem_boost_test_sequential_mode(NAME TraitLocalOperator
                                   EXE TestTraitLocalOperator
                                   TIMEOUT 2)
