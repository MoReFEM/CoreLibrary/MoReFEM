// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <type_traits>
#define BOOST_TEST_MODULE trait_local_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/TimeManager/Policy/Evolution/None.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(operator_with_same_for_each_ref_geom_elt)
{
    using mass = GlobalVariationalOperatorNS::Mass;

    static_assert(
        std::is_same<typename mass::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Triangle3>,
                     Advanced::LocalVariationalOperatorNS::Mass>());

    static_assert(
        std::is_same<typename mass::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Segment2>,
                     Advanced::LocalVariationalOperatorNS::Mass>());
}


BOOST_AUTO_TEST_CASE(operator_with_different_for_each_ref_geom_elt)
{
    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<TimeManagerNS::Policy::None>;

    // GradOnGradientBasedElasticityTensor is not defined for a Point1 geometry.
    // It is currently the only global operator for which the local operator is not the same for each RefGeomElt:
    // this differentiation was introduced to implement the shells in MoReFEM but it was never completed (and the
    // current way to introduce them - the NonlinearShell operator - doesn't use this functionality).
    using elast = GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor<time_manager_type>;

    static_assert(
        std::is_same<typename elast::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Point1>,
                     std::false_type>());

    static_assert(
        std::is_same<typename elast::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Segment2>,
                     Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor<time_manager_type>>());
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
