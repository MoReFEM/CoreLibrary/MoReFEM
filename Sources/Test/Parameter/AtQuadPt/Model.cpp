// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <fstream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include "Model/Model.hpp"

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Coords/Coords.hpp"

#include "FiniteElement/QuadratureRules/QuadratureRulePerTopology.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "Test/Parameter/AtQuadPt/InputData.hpp"
#include "Test/Parameter/AtQuadPt/Model.hpp"


namespace MoReFEM::TestNS::FibersAtQuadPt
{


    namespace // anonymous
    {


        //! Converts a ParameterAtQuadPt into a std::vector.
        template<ParameterNS::Type TypeT>
        std::vector<double>
        ConvertParameterAtQuadPoint(const FilesystemNS::Directory& result_directory_path,
                                    std::string_view quadrature_order,
                                    const ParameterAtQuadraturePoint<TypeT, time_manager_type>& obtained_global_coords);


        //! Checks that the fibers defined at nodes have the same interpolated value as the ones defined at quad_pts.
        void CheckResults(const std::vector<double>& expected, const std::vector<double>& obtained);

    } // namespace


    Model::Model(morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    { }


    // NOLINTNEXTLINE(readability-make-member-function-const)
    void Model::SupplInitialize()
    {
        constexpr auto degree_of_exactness{ 5 };
        constexpr auto shape_function_order{ 5 };
        auto quadrature_rule =
            std::make_unique<const QuadratureRulePerTopology>(degree_of_exactness, shape_function_order);
        CheckScalar(quadrature_rule.get());
        CheckVectorial(quadrature_rule.get());
    }


    void Model::CheckScalar(const QuadratureRulePerTopology* const quadrature_rule) const
    {
        auto& fiber_scalar_at_node = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                               ParameterNS::Type::scalar,
                                                               time_manager_type>::GetInstance()
                                         .GetNonCstFiberList(AsFiberListId(FiberIndex::fiber_scalar_at_node));

        fiber_scalar_at_node.Initialize(quadrature_rule);

        const auto& scalar_node_to_quad =
            ConvertParameterAtQuadPoint(GetOutputDirectory(), "foo.txt", fiber_scalar_at_node.GetUnderlyingParameter());

        auto& fiber_scalar_at_quad_pt = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                                                  ParameterNS::Type::scalar,
                                                                  time_manager_type>::GetInstance()
                                            .GetNonCstFiberList(AsFiberListId(FiberIndex::fiber_scalar_at_quad_pt));

        fiber_scalar_at_quad_pt.Initialize(quadrature_rule);
        const auto& scalar_quad_to_quad = ConvertParameterAtQuadPoint(
            GetOutputDirectory(), "foo.txt", fiber_scalar_at_quad_pt.GetUnderlyingParameter());

        CheckResults(scalar_node_to_quad, scalar_quad_to_quad);
    }


    void Model::CheckVectorial(const QuadratureRulePerTopology* const quadrature_rule) const
    {
        auto& fiber_vector_at_node = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                               ParameterNS::Type::vector,
                                                               time_manager_type>::GetInstance()
                                         .GetNonCstFiberList(AsFiberListId(FiberIndex::fiber_vector_at_node));

        fiber_vector_at_node.Initialize(quadrature_rule);

        const auto& vector_node_to_quad =
            ConvertParameterAtQuadPoint(GetOutputDirectory(), "foo.txt", fiber_vector_at_node.GetUnderlyingParameter());

        auto& fiber_vector_at_quad_pt = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                                                  ParameterNS::Type::vector,
                                                                  time_manager_type>::GetInstance()
                                            .GetNonCstFiberList(AsFiberListId(FiberIndex::fiber_vector_at_quad_pt));

        fiber_vector_at_quad_pt.Initialize(quadrature_rule);
        const auto& vector_quad_to_quad = ConvertParameterAtQuadPoint(
            GetOutputDirectory(), "foo.txt", fiber_vector_at_quad_pt.GetUnderlyingParameter());

        CheckResults(vector_node_to_quad, vector_quad_to_quad);
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


    namespace // anonymous
    {


        /*!
         * \brief Convert the obtained global coords into a vector of double which is filled with same ordering as the
         * expected ones.
         *
         */
        template<ParameterNS::Type TypeT>
        std::vector<double>
        ConvertParameterAtQuadPoint(const FilesystemNS::Directory& result_directory_path,
                                    std::string_view quadrature_order,
                                    const ParameterAtQuadraturePoint<TypeT, time_manager_type>& obtained_global_coords)
        {
            const auto output_file = result_directory_path.AddFile(quadrature_order);
            obtained_global_coords.Write(output_file);

            std::ifstream file_stream{ output_file.Read() };

            std::string line;

            // skip first two lines
            for (int i = 0; i < 2; ++i)
                getline(file_stream, line);

            std::vector<double> ret;

            while (getline(file_stream, line))
            {
                const auto item_list = Utilities::String::Split(line, ";");

                BOOST_CHECK_EQUAL(item_list.size(), 4UL);

                const auto coords_list = std::string(item_list.back()); // istringstream can't act upon a
                                                                        // std::string_view hence this copy.
                std::istringstream iconv(coords_list);

                // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
                double value;
                while (iconv >> value)
                    ret.push_back(value);
            }

            return ret;
        }


        void CheckResults(const std::vector<double>& expected, const std::vector<double>& obtained)
        {
            BOOST_CHECK_EQUAL(expected.size(), obtained.size());

            constexpr auto percentage_precision = 1.e-3;

            for (auto it_obtained = obtained.cbegin(), it_expected = expected.cbegin(); it_obtained != obtained.cend();
                 ++it_obtained, ++it_expected)
            {
                const auto obtained_value = *it_obtained;
                const auto expected_value = *it_expected;

                BOOST_CHECK_CLOSE(expected_value, obtained_value, percentage_precision);
            }
        }


    } // namespace


} // namespace MoReFEM::TestNS::FibersAtQuadPt


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
