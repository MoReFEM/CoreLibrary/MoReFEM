This test checks that fibers defined at the node level and their interpolated values at the quadrature points matches the values we get by defining the fibers directly at the quadrature point level, in both sequential and parallel.
![fibers](fiber_test.png) 
The fibers at the node level are in blue and the ones at the quadrature points are in purple.