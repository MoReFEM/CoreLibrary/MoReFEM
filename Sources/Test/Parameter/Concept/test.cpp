// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdlib>
#include <string>

#define BOOST_TEST_MODULE parameter_concept
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/Parameter/Advanced/Concept.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_AUTO_TEST_CASE(storage_concept)
{


    static_assert(Advanced::Concept::ParameterNS::Storage<double>);

    static_assert(Advanced::Concept::ParameterNS::Storage<int>);

    class Foo
    { };
    static_assert(!Advanced::Concept::ParameterNS::Storage<Foo>);

    static_assert(!Advanced::Concept::ParameterNS::Storage<std::string>);

    static_assert(Advanced::Concept::ParameterNS::Storage<Eigen::MatrixXd>);

    static_assert(Advanced::Concept::ParameterNS::Storage<Eigen::VectorXd>);

    static_assert(Advanced::Concept::ParameterNS::Storage<
                  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor, 3, 4>>);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
