The purpose of the current tests is to check the construction of \a Parameter from the input data file in Lua work as intended.

This is not as straightforward as it may seem (we need in fact quite a lot of flexibility to handle scalar, vectorial and matricial \a Parameter which nature may vary - hence in C++ the use of `std::variant` to enable the genericity of the way values are stored) so the tests have been split in two steps:

- In a first bunch of tests [here](../HardcodedConstruction/README.md), it was checked the (internal) constructors used to build these flexible \a Parameter work as intended.
- In tests in current directory, we check the data read from the input data file is properly interpreted to build the intended \a Parameter.

So if at some point one of the test here fails, your first move should be to check whether the tests about HardcodedConstruction work correctly or not. If not, you should begin your investigation there first - as under the hood these constructors are called in current tests.

The note about parallelism detailed in [hardcoded construction README](../HardcodedConstruction/README.md) does apply here as well.
