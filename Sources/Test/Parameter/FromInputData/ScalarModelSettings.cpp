// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Parameter/FromInputData/ScalarInputData.hpp"


namespace MoReFEM::TestNS::FromInputDataNS
{


    void ModelSettings::Init()
    {
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>>(
            { " generic_vector_numbering_subset" });
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>>(
            { " generic_scalar_numbering_subset" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>>(
            { " generic_vectorial_unknown" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>>(
            { " generic_scalar_unknown" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>>({ " domain_3d" });
        SetDescription<InputDataNS::LightweightDomainList<1>>({ " 1" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
    }


} // namespace MoReFEM::TestNS::FromInputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
