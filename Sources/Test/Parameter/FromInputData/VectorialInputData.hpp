// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_PARAMETER_FROMINPUTDATA_VECTORIALINPUTDATA_DOT_HPP_
#define MOREFEM_TEST_PARAMETER_FROMINPUTDATA_VECTORIALINPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

// IWYU pragma: begin_exports
#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
// IWYU pragma: end_exports

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Parameter/FromInputData/CommonInputData.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::FromInputDataNS
{


    //! Parameter defined for this test.
    struct ConstantParameter
    // clang-format off
    : public Internal::InputDataNS::ParamNS::VectorialParameter
             <
                ConstantParameter,
                Advanced::InputDataNS::NoEnclosingSection
             >
    // clang-format on
    {
        //! Convenient alias.
        using self = ConstantParameter;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static const std::string ret("ConstantParameter");
            return ret;
        }
    };


    //! Parameter defined for this test.
    struct PiecewiseConstantByDomainParameter
    // clang-format off
    : public Internal::InputDataNS::ParamNS::VectorialParameter
             <
                PiecewiseConstantByDomainParameter,
                Advanced::InputDataNS::NoEnclosingSection
             >
    // clang-format on
    {
        //! Convenient alias.
        using self = PiecewiseConstantByDomainParameter;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static const std::string ret("PiecewiseConstantByDomainParameter");
            return ret;
        }
    };


    //! Parameter defined for this test.
    struct IgnoredParameter
    // clang-format off
    : public Internal::InputDataNS::ParamNS::VectorialParameter
             <
                IgnoredParameter,
                Advanced::InputDataNS::NoEnclosingSection
             >
    // clang-format on
    {
        //! Convenient alias.
        using self = IgnoredParameter;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static const std::string ret("IgnoredParameter");
            return ret;
        }
    };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>,

        InputDataNS::LightweightDomainList<1>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,

        ConstantParameter,
        PiecewiseConstantByDomainParameter,
        IgnoredParameter,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>::IndexedSectionDescription,
        InputDataNS::LightweightDomainList<1>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        void Init() override;
    };

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type =
        MoReFEMData<ModelSettings, input_data_type, ::MoReFEM::TimeManagerNS::Instance::None, program_type::test>;


} // namespace MoReFEM::TestNS::FromInputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_PARAMETER_FROMINPUTDATA_VECTORIALINPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
