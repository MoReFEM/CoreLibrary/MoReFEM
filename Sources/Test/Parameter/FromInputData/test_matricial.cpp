// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <map>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#define BOOST_TEST_MODULE matricial_parameter_from_input_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Parameter/FromInputData/MatricialInputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/PredicateEigen.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"

using namespace MoReFEM;
using namespace MoReFEM::TestNS::FromInputDataNS;


namespace // anonymous
{


    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

    constexpr auto epsilon = 1.e-12;


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}

BOOST_FIXTURE_TEST_CASE(constant, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr = InitMatricialParameterFromInputData<ConstantParameter>("constant", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    const Eigen::MatrixXd expected_value{ { 1., -0.5, 2., 4.21 }, { 2., -1.24, 0.54, 1.47 } };

    {
        decltype(auto) read_constant_value = param1.GetConstantValue();
        TestNS::CheckLocalMatrix(read_constant_value, expected_value, epsilon); // throws if something askew happens
    }

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& geom_elt_ptr : geom_elt_list)
    {
        assert(!(!geom_elt_ptr));
        const auto& geom_elt = *geom_elt_ptr;

        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

        for (const auto& quad_pt_ptr : quad_pt_list)
        {
            assert(!(!quad_pt_ptr));

            {
                decltype(auto) read_value = param1.GetValue(*quad_pt_ptr, geom_elt);
                TestNS::CheckLocalMatrix(read_value, expected_value, epsilon); // throws if something askew happens
            }
        }
    }
}


BOOST_FIXTURE_TEST_CASE(piecewise_constant_by_domain, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();


    auto param1_ptr = InitMatricialParameterFromInputData<PiecewiseConstantByDomainParameter>(
        "piecewise_parameter1", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    const std::map<DomainNS::unique_id, Eigen::MatrixXd> expected_value_by_domain{
        { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad1),
          Eigen::MatrixXd{ { 1., 2. }, { 3., 4. }, { 5., 6. } } },
        { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad2),
          Eigen::MatrixXd{ { 10., 2. }, { -54.1, 1.e-4 }, { 3., -9. } } },
        { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad3),
          Eigen::MatrixXd{ { 2., -1.2 }, { 0.21, -9.87 }, { -5., 3.14 } } }
    };

    for (const auto& [domain_id, value] : expected_value_by_domain)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id);
        bool at_least_one_geom_elt_handled{ false };

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();
            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::MatrixXd>,
                                      (param1.GetValue(*quad_pt_ptr, geom_elt))(value)(epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


BOOST_FIXTURE_TEST_CASE(ignored, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr = InitMatricialParameterFromInputData<IgnoredParameter>("ignored", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr == nullptr);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
