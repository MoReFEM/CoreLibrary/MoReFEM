// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>
#include <map>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Geometry/Coords/SpatialPoint.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#define BOOST_TEST_MODULE scalar_parameter_from_input_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Parameter/FromInputData/ScalarInputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::FromInputDataNS;


namespace // anonymous
{

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

    constexpr auto epsilon = 1.e-12;


} // namespace


// NOLINTBEGIN(readability-function-cognitive-complexity)


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}

BOOST_FIXTURE_TEST_CASE(constant, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr =
        InitScalarParameterFromInputData<ConstantParameter>("scalar_parameter1", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (param1.GetConstantValue())(500.)(epsilon));

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& geom_elt_ptr : geom_elt_list)
    {
        assert(!(!geom_elt_ptr));
        const auto& geom_elt = *geom_elt_ptr;

        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

        for (const auto& quad_pt_ptr : quad_pt_list)
        {
            assert(!(!quad_pt_ptr));
            BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                  (param1.GetValue(*quad_pt_ptr, geom_elt))(500.)(epsilon));
        }
    }
}


BOOST_FIXTURE_TEST_CASE(piecewise_constant_by_domain, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();


    auto param1_ptr = InitScalarParameterFromInputData<PiecewiseConstantByDomainParameter>(
        "piecewise_parameter1", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    const std::map<DomainNS::unique_id, double> expected_value_by_domain{
        { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad1), 1.21 },
        { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad2), 7.41 },
        { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad3), -17.2 }
    };

    for (const auto& [domain_id, value] : expected_value_by_domain)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id);
        bool at_least_one_geom_elt_handled{ false };

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                      (param1.GetValue(*quad_pt_ptr, geom_elt))(value)(epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


BOOST_FIXTURE_TEST_CASE(lua_function, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));


    auto param1_ptr = InitScalarParameterFromInputData<LuaFunctionParameter>("lua_function", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    const Wrappers::Lua::Function<double(double, double, double)> expected_lua_function(
        "function (x, y, z)"
        " return 3. * x + y - 17.4 * math.cos(z)"
        " end");

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    SpatialPoint global_coords;

    for (const auto& geom_elt_ptr : geom_elt_list)
    {
        assert(!(!geom_elt_ptr));
        const auto& geom_elt = *geom_elt_ptr;

        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

        for (const auto& quad_pt_ptr : quad_pt_list)
        {
            assert(!(!quad_pt_ptr));
            const auto& quad_pt = *quad_pt_ptr;

            {
                Advanced::GeomEltNS::Local2Global(geom_elt, quad_pt, global_coords);
                const auto expected_value =
                    expected_lua_function(global_coords[::MoReFEM::GeometryNS::dimension_type{ 0 }],
                                          global_coords[::MoReFEM::GeometryNS::dimension_type{ 1 }],
                                          global_coords[::MoReFEM::GeometryNS::dimension_type{ 2 }]);
                BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                      (param1.GetValue(*quad_pt_ptr, geom_elt))(expected_value)(epsilon));
            }
        }
    }
}


BOOST_FIXTURE_TEST_CASE(ignored, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr = InitScalarParameterFromInputData<IgnoredParameter>("ignored", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr == nullptr);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
// NOLINTEND(readability-function-cognitive-complexity)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
