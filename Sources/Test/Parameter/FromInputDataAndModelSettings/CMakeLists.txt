add_library(TestParameterFromInputDataAndModelSettings_lib ${LIBRARY_TYPE} "")

target_sources(TestParameterFromInputDataAndModelSettings_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/README.md
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
)

target_link_libraries(TestParameterFromInputDataAndModelSettings_lib
            ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestParameterFromInputDataAndModelSettings_lib Test/Parameter/FromInputDataAndModelSettings Test/Parameter/FromInputDataAndModelSettings)

# --------------------
# Executable
# --------------------
add_executable(TestParameterFromInputDataAndModelSettings 
              ${CMAKE_CURRENT_LIST_DIR}/test.cpp
              ${CMAKE_CURRENT_LIST_DIR}/demo.lua
              )

target_link_libraries(TestParameterFromInputDataAndModelSettings
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_PARAM_INSTANCES}>
                      TestParameterFromInputDataAndModelSettings_lib)

morefem_organize_IDE(TestParameterFromInputDataAndModelSettings Test/Parameter/FromInputDataAndModelSettings Test/Parameter/FromInputDataAndModelSettings)

morefem_boost_test_sequential_mode(NAME ParameterFromInputDataAndModelSettings
                                   EXE TestParameterFromInputDataAndModelSettings
                                   TIMEOUT 5
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Parameter/FromInputDataAndModelSettings/demo.lua)
