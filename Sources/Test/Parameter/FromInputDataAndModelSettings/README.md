The purpose of the current tests is to check whether a Parameter definition may be split between `InputData` and the newly introduced `ModelSettings`

The purpose here is to check we can do all the combination between input data and model settings for a parameter - we don't make all the variations about the types of parameters as in 'FromInputData' (for instance we play only we scalar parameter - doing then all again would of course be better but more time consuming).
