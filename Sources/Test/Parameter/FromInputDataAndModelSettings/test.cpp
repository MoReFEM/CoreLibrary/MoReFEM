// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/Enum.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#define BOOST_TEST_MODULE parameter_from_input_data_and_model_settings
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Parameter/FromInputDataAndModelSettings/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::FromInputDataAndModelSettingsNS;


namespace // anonymous
{

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

    constexpr auto epsilon = 1.e-12;

    constexpr auto expected_value{ 5.42 };


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}


BOOST_FIXTURE_TEST_CASE(all_in_input_data, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();


    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr = InitScalarParameterFromInputData<ConstantParameterAllInInputData>(
        "all_in_input_data", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (param1.GetConstantValue())(expected_value)(epsilon));
}


BOOST_FIXTURE_TEST_CASE(all_in_model_settings, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();


    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr = InitScalarParameterFromInputData<ConstantParameterAllInModelSettings>(
        "all_in_model_settings", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (param1.GetConstantValue())(expected_value)(epsilon));
}


BOOST_FIXTURE_TEST_CASE(nature_in_model_settings, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();


    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr = InitScalarParameterFromInputData<ConstantParameterNatureInModelSettings>(
        "nature_in_model_settings", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (param1.GetConstantValue())(expected_value)(epsilon));
}


BOOST_FIXTURE_TEST_CASE(value_in_model_settings, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();


    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    auto param1_ptr = InitScalarParameterFromInputData<ConstantParameterValueInModelSettings>(
        "value_in_model_settings", full_domain, morefem_data);

    BOOST_REQUIRE(param1_ptr != nullptr);

    const auto& param1 = *param1_ptr;

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (param1.GetConstantValue())(expected_value)(epsilon));
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
