// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <map>
#include <utility>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Geometry/Coords/SpatialPoint.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Parameters/TimeDependency/None.hpp"

#include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp"

#define BOOST_TEST_MODULE parameter_hardcoded_construction_compound_3d
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Parameter/HardcodedConstruction/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/PredicateEigen.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HardcodedConstructionNS;


namespace // anonymous
{

    constexpr auto epsilon = NumericNS::DefaultEpsilon<double>();

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

    // clang-format off
    using model_time_dep_type =
    TestNS::BareModel
    <
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_dep_type = MoReFEM::TestNS::FixtureNS::Model<model_time_dep_type>;


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}


BOOST_FIXTURE_TEST_CASE(compound_3d, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    // clang-format off
    using parameter_type =
        ParameterNS::ThreeDimensionalCoumpoundParameter
        <
            time_manager_type,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on

    std::map<DomainNS::unique_id, double> y_value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), 1.21 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), 7.41 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), -17.2 }
    };

    using variant_type = typename Internal::ParameterNS::Traits<::MoReFEM::ParameterNS::Type::scalar>::variant_type;

    const variant_type x_value = 5.12;

    auto&& component_x = InitScalarParameterFromInputData<time_manager_type, ParameterNS::TimeDependencyNS::None>(
        "Value X", full_domain, "constant", x_value);

    const variant_type y_value = y_value_by_domain;

    auto&& component_y = InitScalarParameterFromInputData<time_manager_type, ParameterNS::TimeDependencyNS::None>(
        "Value Y", full_domain, "piecewise_constant_by_domain", y_value);

    Wrappers::Lua::Function<double(double, double, double)> lua_function("function (x, y, z)"
                                                                         " return 3. * x + y - 17.4 * math.cos(z)"
                                                                         " end");

    const variant_type z_value = lua_function;

    auto&& component_z = InitScalarParameterFromInputData<time_manager_type, ParameterNS::TimeDependencyNS::None>(
        "Value Z", full_domain, "lua_function", z_value);


    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    auto compound_3d =
        parameter_type("compound", std::move(component_x), std::move(component_y), std::move(component_z));

    Eigen::VectorXd expected_result(3);
    expected_result.resize(3);
    expected_result[0] = std::get<double>(x_value);
    SpatialPoint global_coords;

    for (const auto& [domain_id, value] : y_value_by_domain)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id);
        bool at_least_one_geom_elt_handled{ false };

        expected_result[1] = value;

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                const auto& quad_pt = *quad_pt_ptr;

                Advanced::GeomEltNS::Local2Global(geom_elt, quad_pt, global_coords);
                expected_result[2] = lua_function(global_coords[::MoReFEM::GeometryNS::dimension_type{ 0 }],
                                                  global_coords[::MoReFEM::GeometryNS::dimension_type{ 1 }],
                                                  global_coords[::MoReFEM::GeometryNS::dimension_type{ 2 }]);

                BOOST_CHECK_PREDICATE(MoReFEM::TestNS::EigenNS::PredicateEqual<Eigen::VectorXd>,
                                      (compound_3d.GetValue(*quad_pt_ptr, geom_elt))(expected_result)(epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
