// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/Enum.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#define BOOST_TEST_MODULE parameter_hardcoded_construction_constant
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/Internal/ParameterInstance.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "Test/Parameter/HardcodedConstruction/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HardcodedConstructionNS;


namespace // anonymous
{

    constexpr auto epsilon = NumericNS::DefaultEpsilon<double>();

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}

BOOST_FIXTURE_TEST_CASE(scalar, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::scalar,
            ::MoReFEM::ParameterNS::Policy::Constant,
            time_manager_type,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None,
            double
        >;
    // clang-format on

    const parameter_type param1("scalar_parameter", full_domain, 500.);

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (param1.GetConstantValue())(500.)(epsilon));

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& geom_elt_ptr : geom_elt_list)
    {
        assert(!(!geom_elt_ptr));
        const auto& geom_elt = *geom_elt_ptr;

        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

        for (const auto& quad_pt_ptr : quad_pt_list)
        {
            assert(!(!quad_pt_ptr));
            BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                  (param1.GetValue(*quad_pt_ptr, geom_elt))(500.)(epsilon));
        }
    }
}


BOOST_FIXTURE_TEST_CASE(vector, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::vector,
            ::MoReFEM::ParameterNS::Policy::Constant,
            time_manager_type,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None,
            Eigen::VectorXd
        >;
    // clang-format on

    const Eigen::VectorXd constant_value{ { 1., -0.5, 2., 4.21 } };

    const parameter_type param1("vector_parameter", full_domain, constant_value);

    {
        decltype(auto) read_constant_value = param1.GetConstantValue();
        TestNS::CheckLocalVector(read_constant_value, constant_value, epsilon); // throws if something askew happens
    }

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& geom_elt_ptr : geom_elt_list)
    {
        assert(!(!geom_elt_ptr));
        const auto& geom_elt = *geom_elt_ptr;

        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

        for (const auto& quad_pt_ptr : quad_pt_list)
        {
            assert(!(!quad_pt_ptr));

            {
                decltype(auto) read_value = param1.GetValue(*quad_pt_ptr, geom_elt);
                TestNS::CheckLocalVector(read_value, constant_value, epsilon); // throws if something askew happens
            }
        }
    }
}


BOOST_FIXTURE_TEST_CASE(vector_with_custom_storage, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::vector,
            ::MoReFEM::ParameterNS::Policy::Constant,
            time_manager_type,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None,
            Eigen::Vector4d
        >;
    // clang-format on

    const Eigen::Vector4d constant_value{ { 1., -0.5, 2., 4.21 } };

    const parameter_type param1("vector_parameter", full_domain, constant_value);

    {
        decltype(auto) read_constant_value = param1.GetConstantValue();
        TestNS::CheckLocalVector(read_constant_value, constant_value, epsilon); // throws if something askew happens
    }

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& geom_elt_ptr : geom_elt_list)
    {
        assert(!(!geom_elt_ptr));
        const auto& geom_elt = *geom_elt_ptr;

        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

        for (const auto& quad_pt_ptr : quad_pt_list)
        {
            assert(!(!quad_pt_ptr));

            {
                decltype(auto) read_value = param1.GetValue(*quad_pt_ptr, geom_elt);
                TestNS::CheckLocalVector(read_value, constant_value, epsilon); // throws if something askew happens
            }
        }
    }
}


BOOST_FIXTURE_TEST_CASE(matrix, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::matrix,
            ::MoReFEM::ParameterNS::Policy::Constant,
            time_manager_type,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None,
    Eigen::MatrixXd
        >;
    // clang-format on

    const Eigen::MatrixXd constant_value{ { 1., -0.5, 2., 4.21 }, { 2., -1.24, 0.54, 1.47 } };

    const parameter_type param1("matrix_parameter", full_domain, constant_value);

    {
        decltype(auto) read_constant_value = param1.GetConstantValue();

        TestNS::CheckLocalMatrix(read_constant_value, constant_value, epsilon); // throws if something askew happens
    }

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& geom_elt_ptr : geom_elt_list)
    {
        assert(!(!geom_elt_ptr));
        const auto& geom_elt = *geom_elt_ptr;

        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

        for (const auto& quad_pt_ptr : quad_pt_list)
        {
            assert(!(!quad_pt_ptr));

            {
                decltype(auto) read_value = param1.GetValue(*quad_pt_ptr, geom_elt);
                TestNS::CheckLocalMatrix(read_value, constant_value, epsilon); // throws if something askew happens
            }
        }
    }
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
