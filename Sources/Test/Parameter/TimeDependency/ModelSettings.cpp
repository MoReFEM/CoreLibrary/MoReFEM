// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Parameter/TimeDependency/InputData.hpp"


namespace MoReFEM::TestNS::ParameterNS::TimeDependencyNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>>({ " scalar" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>>(
            { " vectorial" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>>({ " scalar" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>>({ " vectorial" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ " full_mesh" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        SetDescription<InputDataNS::Source::PressureFromFile<EnumUnderlyingType(PressureIndex::pressure)>>(
            { "Pressure" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::source)>>({ "Source" });
    }


} // namespace MoReFEM::TestNS::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
