// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdlib>
#include <memory>
#include <utility>

#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#define BOOST_TEST_MODULE parameter_time_dependency
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/TimeDependency/TimeDependency.hpp"

#include "Test/Parameter/TimeDependency/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::ParameterNS::TimeDependencyNS;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        TestNS::ParameterNS::TimeDependencyNS::morefem_data_type
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

    constexpr auto epsilon = 1.e-8;

} // namespace


BOOST_FIXTURE_TEST_CASE(current_time, fixture_type)
{
    using namespace TestNS::ParameterNS::TimeDependencyNS;
    decltype(auto) model = GetNonCstModel();
    decltype(auto) time_manager = model.GetNonCstTimeManager();
    decltype(auto) morefem_data = model.GetMoReFEMData();


    decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

    auto scalar_param_ptr =
        InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus, ParameterNS::TimeDependencyFunctor>(
            "Young modulus", domain, morefem_data);
    auto& scalar_param = *scalar_param_ptr;

    {
        auto time_dep = [](double time)
        {
            return time + 1;
        };

        auto time_dep_functor =
            std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::scalar, time_manager_type>>(
                time_manager, std::move(time_dep));

        scalar_param.SetTimeDependency(std::move(time_dep_functor));
    }

    using vectorial_param_type = InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::source)>;

    auto vectorial_param_ptr =
        Init3DCompoundParameterFromInputData<vectorial_param_type, ParameterNS::TimeDependencyFunctor>(
            "Source", domain, morefem_data);

    auto& vectorial_param = *vectorial_param_ptr;

    {
        auto time_dep = [](double time)
        {
            return 1.7 * time - 0.2;
        };

        auto time_dep_functor =
            std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::vector, time_manager_type>>(
                time_manager, std::move(time_dep));

        vectorial_param.SetTimeDependency(std::move(time_dep_functor));
    }

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(), 0.);
    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (scalar_param.GetConstantValue())(20.)(epsilon));

    {
        decltype(auto) vectorial_values = vectorial_param.GetConstantValue();

        BOOST_CHECK_EQUAL(vectorial_values[0], -2.);
        BOOST_CHECK_EQUAL(vectorial_values[1], -4.);
        BOOST_CHECK_EQUAL(vectorial_values[2], -6.);
    }

    time_manager.IncrementTime();
    scalar_param.TimeUpdate(); // in real model, should be done in Model::InitializeStep()
    vectorial_param.TimeUpdate();

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(), 0.1);

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (scalar_param.GetConstantValue())(22.)(epsilon));


    {
        decltype(auto) vectorial_values = vectorial_param.GetConstantValue();

        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[0])(-0.3)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[1])(-0.6)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[2])(-0.9)(epsilon));
    }


    time_manager.IncrementTime();
    scalar_param.TimeUpdate(); // in real model, should be done in Model::InitializeStep()
    vectorial_param.TimeUpdate();

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(), 0.2);
    BOOST_CHECK_EQUAL(scalar_param.GetConstantValue(), 24.);

    {
        decltype(auto) vectorial_values = vectorial_param.GetConstantValue();

        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[0])(1.4)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[1])(2.8)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[2])(4.2)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(fixed_time, fixture_type)
{
    using namespace TestNS::ParameterNS::TimeDependencyNS;
    decltype(auto) model = GetNonCstModel();
    decltype(auto) time_manager = model.GetNonCstTimeManager();
    decltype(auto) morefem_data = model.GetMoReFEMData();


    decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

    auto scalar_param_ptr =
        InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus, ParameterNS::TimeDependencyFunctor>(
            "Young modulus", domain, morefem_data);
    auto& scalar_param = *scalar_param_ptr;

    {
        auto time_dep = [](double time)
        {
            return time + 1;
        };

        auto time_dep_functor =
            std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::scalar, time_manager_type>>(
                time_manager, std::move(time_dep));

        scalar_param.SetTimeDependency(std::move(time_dep_functor));
    }

    constexpr auto new_time = 10.;
    scalar_param.TimeUpdate(new_time);

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(),
                        0.2); // with fixed time parameter is decorrelated from time manager;
                              // this is to be used with extreme caution! (or better, not at all).

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (scalar_param.GetConstantValue())(220.)(epsilon));
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
