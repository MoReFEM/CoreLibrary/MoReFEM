-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.


Solid = {

	VolumicMass = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 10.4

	}, -- VolumicMass

	HyperelasticBulk = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 1750000.

	}, -- HyperelasticBulk

	Kappa1 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 500.

	}, -- Kappa1

	Kappa2 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 403346.1538461538,

	}, -- Kappa2

	PoissonRatio = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = .3

	}, -- PoissonRatio

	YoungModulus = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 21.e5

	}, -- YoungModulus

	LameLambda = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "ignore",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 1211538.4615384615,

	}, -- LameLambda

	LameMu = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "ignore",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 807692.3076923076,

	}, -- LameMu

	Viscosity = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 8.

	}, -- Viscosity

	Mu1 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 9.

	}, -- Mu1

	Mu2 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 10.

	}, -- Mu2

	C0 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 11.

	}, -- C0

	C1 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 12.

	}, -- C1

	C2 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 13.

	}, -- C2

	C3 = {


		-- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose 
		-- "ignore" if you do not want this parameter (in this case it will stay at nullptr). 
		-- Expected format: "VALUE"
		-- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
		nature = "constant",


		-- The value for the parameter, which type depends directly on the nature chosen: 
		--  If nature is 'constant', expected format is VALUE 
		--  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...} 
		--  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)  
		-- return x + y - z 
		-- end 
		-- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix. 
		-- Expected format: see the variant description...
		value = 14.

	}, -- C3

    C4 = {
        
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
        nature = "constant",
        
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix.
        -- Expected format: see the variant description...
        value = 15.
        
    }, -- C4
    
    C5 = {
        
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
        nature = "constant",
        
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix.
        -- Expected format: see the variant description...
        value = 16.
        
    }, -- C5
    
	-- For 2D operators, which approximation to use.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'irrelevant', 'plane_strain', 'plane_stress'})
	PlaneStressStrain = "plane_strain",
 
    -- If the displacement induced by the applied loading is strong enough it can lead to inverted elements:
    -- some vertices of the element are moved such that the volume of the finite element becomes negative.
    -- This means that the resulting deformed mesh is no longer valid.
    -- This parameter enables a computationally expensive test on all quadrature points to check that the volume
    -- of all finite elements remains positive throughout the computation.
    CheckInvertedElements = false

} -- Solid

Result = {


	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. Please do not read this value 
	-- directly: it might have been extended in MoReFEMData class! Rather call the GetResultDirectory() from 
	-- this class. 
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/ParameterInstances/Solid/LameIgnored",


	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,
    
    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: VALUE
    binary_output = false
} -- Result

