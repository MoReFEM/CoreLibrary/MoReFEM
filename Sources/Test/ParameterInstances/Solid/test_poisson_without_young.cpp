// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <exception>
#include <memory>
#include <tuple>
#include <utility>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Result.hpp"

#include "Geometry/Mesh/Format.hpp"
#define BOOST_TEST_MODULE solid_poisson_without_young
#include <cstdlib>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace MoReFEM
{


    struct TestHelper
    {
        TestHelper()
        {
            static bool first = true;

            if (first)
            {
                first = false;
                DomainManager::CreateOrGetInstance().Create(DomainNS::unique_id{ 1 },
                                                            { MeshNS::unique_id{ 1UL } },
                                                            { GeometryNS::dimension_type{ 2 } },
                                                            {},
                                                            {});
            }
        }
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(poisson_no_young, TestNS::FixtureNS::TestEnvironment)
{
    // clang-format off
    using input_data_tuple =
        std::tuple
        <
            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::PoissonRatio,
            InputDataNS::Result
        >;
    // clang-format on

    using input_data_type = InputData<input_data_tuple>;

    FilesystemNS::File lua_file{ std::filesystem::path{ "${MOREFEM_ROOT}/Sources/Test/ParameterInstances/Solid/"
                                                        "demo_poisson_without_young.lua" } };

    using time_manager_type = ::MoReFEM::TimeManagerNS::Instance::None;

    const MoReFEMData<MoReFEM::TestNS::EmptyModelSettings, input_data_type, time_manager_type, program_type::test>
        morefem_data{ std::move(lua_file) };

    std::filesystem::path path{ "${MOREFEM_ROOT}/Data/Mesh/one_triangle.mesh" };
    const FilesystemNS::File mesh_file{ std::move(path) };

    Internal::MeshNS::MeshManager::CreateOrGetInstance().Create(MeshNS::unique_id{ 1UL },
                                                                mesh_file,
                                                                GeometryNS::dimension_type{ 2 },
                                                                MeshNS::Format::Medit,
                                                                ::MoReFEM::CoordsNS::space_unit_type{ 1. });

    const TestHelper test;

    const QuadratureRulePerTopology quad_rule_per_topology(3, 3);

    BOOST_CHECK_THROW(
        std::make_unique<Solid<time_manager_type>>(
            morefem_data, DomainManager::GetInstance().GetDomain(DomainNS::unique_id{ 1 }), quad_rule_per_topology),
        std::exception);
}

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
