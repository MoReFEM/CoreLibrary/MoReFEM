// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iomanip>
#include <memory>
#include <source_location>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "Core/NumberingSubset/UniqueId.hpp"

#define BOOST_TEST_MODULE ensight_several_unknowns
#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "PostProcessing/OutputFormat/Ensight6.hpp"

#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    void TestCase(const Wrappers::Mpi& mpi);


    using fixture = MoReFEM::TestNS::FixtureNS::TestEnvironment;


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(ensight_several_unknowns, fixture)
{
    TestCase(GetMpi());
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    void TestCase(const Wrappers::Mpi& mpi)
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();
        decltype(auto) environment = Utilities::Environment::GetInstance();
        Utilities::AsciiOrBinary::CreateOrGetInstance(std::source_location::current(), false);

        const std::string data_directory_path =
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/PostProcessing/Ensight/SeveralUnknowns/Data");

        const FilesystemNS::Directory data_directory(mpi, data_directory_path, FilesystemNS::behaviour::read);

        std::filesystem::path path{ "${MOREFEM_ROOT}/Data/Mesh/Bar.mesh" };
        const FilesystemNS::File mesh_file{ std::move(path) };

        constexpr auto dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };
        constexpr auto mesh_unique_id = MeshNS::unique_id{ 1UL };
        constexpr auto space_unit = ::MoReFEM::CoordsNS::space_unit_type{ 1. };

        mesh_manager.Create(mesh_unique_id, mesh_file, dimension, MeshNS::Format::Medit, space_unit);

        decltype(auto) mesh = mesh_manager.GetMesh(mesh_unique_id);

        const std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ NumberingSubsetNS::unique_id{ 1 },
                                                                                  NumberingSubsetNS::unique_id{ 2 },
                                                                                  NumberingSubsetNS::unique_id{ 3 } };


        const std::vector<std::string> unknown_list{ "displacement", "second_unknown", "third_unknown" };
        const std::string ensight_directory_path =
            environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/PostProcessing/Ensight/SeveralUnknowns/Ensight6");

        const FilesystemNS::Directory::const_unique_ptr ensight_directory = std::make_unique<FilesystemNS::Directory>(
            mpi, ensight_directory_path, FilesystemNS::behaviour::overwrite, FilesystemNS::add_rank::yes);

        const PostProcessingNS::OutputFormat::Ensight6 ensight_output(data_directory,
                                                                      unknown_list,
                                                                      numbering_subset_id_list,
                                                                      mesh,
                                                                      PostProcessingNS::RefinedMesh::no,
                                                                      ensight_directory.get());

        const std::string ref_dir_path =
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/PostProcessing/Ensight/SeveralUnknowns/"
                                         "ExpectedResult/Ensight6");

        const FilesystemNS::Directory ref_dir(ref_dir_path, FilesystemNS::behaviour::read);

        std::ostringstream oconv;
        for (auto time_iteration = 0UL; time_iteration < 1UL; ++time_iteration)
        {
            oconv.str("");
            oconv << "displacement." << std::setw(5) << std::setfill('0') << time_iteration << ".scl";
            [[maybe_unused]] auto first =
                TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, *ensight_directory, oconv.str(), 1.e-11);

            oconv.str("");
            oconv << "second_unknown." << std::setw(5) << std::setfill('0') << time_iteration << ".scl";
            [[maybe_unused]] auto second =
                TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, *ensight_directory, oconv.str(), 1.e-11);

            oconv.str("");
            oconv << "third_unknown." << std::setw(5) << std::setfill('0') << time_iteration << ".scl";
            [[maybe_unused]] auto third =
                TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, *ensight_directory, oconv.str(), 1.e-11);
        }
    }


} // namespace

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
