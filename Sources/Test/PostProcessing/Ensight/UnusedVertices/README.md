The point here is to check the Ensight output behaviour is correct when some vertices of the meshes aren't used to bear dofs (it wasn't the case prior to #1399).

