// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iomanip>
#include <memory>
#include <source_location>
#include <sstream>
#include <string>
#include <vector>

#include "Core/NumberingSubset/UniqueId.hpp"

#define BOOST_TEST_MODULE ensight_unused_vertices
#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "PostProcessing/OutputFormat/Ensight6.hpp"

#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    MeshNS::unique_id GenerateNewMeshId();

    void TestCase(const Wrappers::Mpi& mpi, ::MoReFEM::CoordsNS::space_unit_type space_unit);


    using fixture = MoReFEM::TestNS::FixtureNS::TestEnvironment;


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(ensight_unused_vertices, fixture)


BOOST_AUTO_TEST_CASE(space_unit_1)
{
    TestCase(GetMpi(), ::MoReFEM::CoordsNS::space_unit_type{ 1. });
}

BOOST_AUTO_TEST_CASE(space_unit_2)
{
    TestCase(GetMpi(), ::MoReFEM::CoordsNS::space_unit_type{ 2. });
}

BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    MeshNS::unique_id GenerateNewMeshId()
    {
        static auto ret = MeshNS::unique_id{ 0UL };
        return ++ret;
    }


    void TestCase(const Wrappers::Mpi& mpi, ::MoReFEM::CoordsNS::space_unit_type space_unit)
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();
        decltype(auto) environment = Utilities::Environment::GetInstance();
        Utilities::AsciiOrBinary::CreateOrGetInstance(std::source_location::current(), false);

        const std::string data_directory_path =
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/PostProcessing/Ensight/UnusedVertices/Data");

        const FilesystemNS::Directory data_directory(mpi, data_directory_path, FilesystemNS::behaviour::read);

        auto mesh_unique_id = GenerateNewMeshId();
        const auto mesh_file = data_directory.AddFile("cylinder_unused_vertices.mesh");
        constexpr auto dimension = ::MoReFEM::GeometryNS::dimension_type{ 3 };

        mesh_manager.Create(mesh_unique_id, mesh_file, dimension, MeshNS::Format::Medit, space_unit);

        decltype(auto) mesh = mesh_manager.GetMesh(mesh_unique_id);

        const std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ NumberingSubsetNS::unique_id{ 1 } };
        const std::vector<std::string> unknown_list{ "solid_displacement" };
        const std::string ensight_directory_path =
            environment.SubstituteValues("${MOREFEM_TEST_OUTPUT_DIR}/PostProcessing/Ensight/UnusedVertices/Ensight6");

        const FilesystemNS::Directory::const_unique_ptr ensight_directory = std::make_unique<FilesystemNS::Directory>(
            mpi, ensight_directory_path, FilesystemNS::behaviour::overwrite, FilesystemNS::add_rank::yes);

        const PostProcessingNS::OutputFormat::Ensight6 ensight_output(data_directory,
                                                                      unknown_list,
                                                                      numbering_subset_id_list,
                                                                      mesh,
                                                                      PostProcessingNS::RefinedMesh::no,
                                                                      ensight_directory.get());

        const std::string ref_dir_path =
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/PostProcessing/Ensight/UnusedVertices/"
                                         "ExpectedResult/Ensight6");

        const FilesystemNS::Directory ref_dir(ref_dir_path, FilesystemNS::behaviour::read);

        std::ostringstream oconv;
        for (auto time_iteration = 0UL; time_iteration < 2UL; ++time_iteration)
        {
            oconv.str("");
            oconv << "solid_displacement." << std::setw(5) << std::setfill('0') << time_iteration << ".scl";
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, *ensight_directory, oconv.str(), 1.e-11);
        }
    }


} // namespace
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
