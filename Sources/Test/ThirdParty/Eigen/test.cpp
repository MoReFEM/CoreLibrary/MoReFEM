// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cmath>
#include <vector>

#define BOOST_TEST_MODULE eigen_functionalities

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"
#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,modernize-use-std-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_AUTO_TEST_CASE(eigen_det)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Matrix<double, 3, 3> a{ { -2., 2., -3. }, { -1., 1., 3. }, { 2., 0., -1. } };

    const auto d = a.determinant();
    constexpr auto expected_value = 18.;

    BOOST_CHECK_EQUAL(d, expected_value);
}

namespace // anonymous
{

    const double local_inf = 1. / 0.;

    const double local_nan = local_inf * 0.;

} // namespace


BOOST_AUTO_TEST_CASE(vector_isfinite)
{
    Eigen::Vector3d vector;
    vector(0) = 0.;
    vector(1) = -10.;
    vector(2) = 10.;

    BOOST_CHECK(Eigen::isfinite(vector.array()).all() == true);

    vector(2) = local_nan;
    BOOST_CHECK(Eigen::isfinite(vector.array()).all() == false);

    vector(2) = 0.;
    BOOST_CHECK(Eigen::isfinite(vector.array()).all() == true);

    vector(0) = local_inf;
    BOOST_CHECK(Eigen::isfinite(vector.array()).all() == false);
}


BOOST_AUTO_TEST_CASE(matrix_isfinite)
{
    Eigen::Matrix3d matrix;
    matrix.setZero();

    BOOST_CHECK(Eigen::isfinite(matrix.array()).all() == true);

    matrix(2, 0) = local_nan;
    BOOST_CHECK(Eigen::isfinite(matrix.array()).all() == false);

    matrix(2, 0) = 0.;
    BOOST_CHECK(Eigen::isfinite(matrix.array()).all() == true);

    matrix(1, 1) = local_inf;
    BOOST_CHECK(Eigen::isfinite(matrix.array()).all() == false);
}


BOOST_AUTO_TEST_CASE(vector_is_zero)
{
    Eigen::Vector3d vector;
    vector.setZero();

    BOOST_CHECK(vector.isZero() == true);

    vector(0) = 1.;
    BOOST_CHECK(vector.isZero() == false);

    constexpr auto epsilon{ 2. };
    BOOST_CHECK(vector.isZero(epsilon) == true);
}


BOOST_AUTO_TEST_CASE(matrix_is_zero)
{
    Eigen::Matrix3d matrix;
    matrix.setZero();

    BOOST_CHECK(matrix.isZero() == true);

    matrix(0, 1) = 1.;
    BOOST_CHECK(matrix.isZero() == false);

    constexpr auto epsilon{ 2. };
    BOOST_CHECK(matrix.isZero(epsilon) == true);
}


BOOST_AUTO_TEST_CASE(forbidden_dynamic_vector)
{
    // Eigen::MatrixXd m(50, 57); // this line should not compile due to EIGEN_RUNTIME_NO_MALLOC
    // This test seems therefore nonsensical, but current test file has been written to harness Eigen and understand
    // its behaviour, to avoid doing sub-optimal operations.
}


BOOST_AUTO_TEST_CASE(scalar_mult)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    Eigen::Matrix3d matrix = Eigen::Matrix3d::Identity();
    matrix(2, 1) = 5.;

    matrix *= -2.;

    const Eigen::Matrix3d expected{ { -2., 0., 0. }, { 0., -2., 0. }, { 0., -10, -2. } };

    BOOST_CHECK(matrix.isApprox(expected, 1.e-15));
}


// See https://eigen.tuxfamily.org/dox/group__TutorialSlicingIndexing.html
BOOST_AUTO_TEST_CASE(subset_view)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Matrix4d matrix{
        { 0., 1., 2., 3. }, { 10., 11., 12., 13. }, { 20., 21., 22., 23. }, { 30., 31., 32., 33. }
    };

    {
        auto first_row = matrix(0, Eigen::all);
        BOOST_CHECK_EQUAL(first_row(0), 0.);
        BOOST_CHECK_EQUAL(first_row(1), 1.);
        BOOST_CHECK_EQUAL(first_row(2), 2.);
        BOOST_CHECK_EQUAL(first_row(3), 3.);
    }


    {
        auto block = matrix(Eigen::seq(2, 3), Eigen::seq(1, 2));

        BOOST_CHECK_EQUAL(block(0, 0), 21.);
        BOOST_CHECK_EQUAL(block(0, 1), 22.);

        BOOST_CHECK_EQUAL(block(1, 0), 31.);
        BOOST_CHECK_EQUAL(block(1, 1), 32.);
    }

    {
        auto last_row = matrix(Eigen::last, Eigen::all);

        BOOST_CHECK_EQUAL(last_row(0), 30.);
        BOOST_CHECK_EQUAL(last_row(1), 31.);
        BOOST_CHECK_EQUAL(last_row(2), 32.);
        BOOST_CHECK_EQUAL(last_row(3), 33.);
    }

    {
        auto last_col = matrix(Eigen::all, Eigen::last);

        BOOST_CHECK_EQUAL(last_col(0), 3.);
        BOOST_CHECK_EQUAL(last_col(1), 13.);
        BOOST_CHECK_EQUAL(last_col(2), 23.);
        BOOST_CHECK_EQUAL(last_col(3), 33.);
    }
}

BOOST_AUTO_TEST_CASE(transpose)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Matrix4d matrix{
        { 0., 1., 2., 3. }, { 10., 11., 12., 13. }, { 20., 21., 22., 23. }, { 30., 31., 32., 33. }
    };


    const Eigen::Matrix4d expected{
        { 0., 10., 20., 30. }, { 1., 11., 21., 31. }, { 2., 12., 22., 32. }, { 3., 13., 23., 33. }
    };

    BOOST_CHECK_EQUAL(matrix.transpose(), expected);
}


BOOST_AUTO_TEST_CASE(norm)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Matrix<double, 3, 3> a{ { -2., 2., -3. }, { -1., 1., 3. }, { 2., 0., -1. } };

    const auto expected = 5.74456264654;

    constexpr auto eps = 1.e-11;

    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (a.norm())(expected)(eps));
}


BOOST_AUTO_TEST_CASE(outer_product)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Vector<double, 6> a{ { 1.1, 1.2, 1.3, 0.1, -0.17, 0.04 } };

    const Eigen::Vector<double, 6> b{ { 1.000215, 0.998886, 0.999058, -0.00008, 0.000132, -0.000266 } };

    // Expected was the result handled by Xtensor with high precision requested in print.
    const Eigen::Matrix<double, 6, 6> expected{
        { 1.1002365, 1.0987746, 1.0989638, -0.000088, 0.0001452, -0.0002926 },
        { 1.200258, 1.1986632, 1.1988696, -0.000096, 0.0001584, -0.0003192 },
        { 1.3002795, 1.2985518, 1.2987754, -0.000104, 0.0001716, -0.0003458 },
        { 0.1000215, 0.0998886, 0.0999058, -0.000008, 0.0000132, -0.0000266 },
        { -0.17003655, -0.16981062, -0.16983986, 0.0000136, -0.00002244, 0.00004522 },
        { 0.0400086, 0.03995544, 0.03996232, -0.0000032, 0.00000528, -0.00001064 }
    };

    const Eigen::Matrix<double, 6, 6> obtained = a * b.transpose();

    BOOST_CHECK(obtained.isApprox(expected, 1.e-15));
}


// To replace xt::linalg::dot used with Xtensor
BOOST_AUTO_TEST_CASE(dot)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

    const Eigen::MatrixXd a{ { -1.1, -1.74, -3.1 }, { 1.2, 0., 1.75 }, { 20., -17., 31.2 }, { 0., -0.75, 1. } };

    const Eigen::MatrixXd b{ { -0.75, 0.15, 0.5 }, { 0., -0.51, 0.58 }, { 0.92, 0.12, 0. } };

    const Eigen::MatrixXd obtained = a * b;

    const Eigen::MatrixXd expected{
        { -2.027, 0.3504, -1.5592 }, { 0.71, 0.39, 0.6 }, { 13.704, 15.414, 0.14 }, { 0.92, 0.5025, -0.435 }
    };

    BOOST_CHECK(obtained.isApprox(expected, 1.e-15));
}


BOOST_AUTO_TEST_CASE(no_malloc_when_existing)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

    const Eigen::MatrixXd a{ { -1.1, -1.74, -3.1 }, { 1.2, 0., 1.75 }, { 20., -17., 31.2 }, { 0., -0.75, 1. } };

    const Eigen::MatrixXd b{ { -0.75, 0.15, 0.5 }, { 0., -0.51, 0.58 }, { 0.92, 0.12, 0. } };

    Eigen::MatrixXd obtained(3, 4);

    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    // Without the noalias there would be a memory allocation here!
    obtained.noalias() = a * b;
}


BOOST_AUTO_TEST_CASE(cross_product_from_matrix)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Matrix3d m{ { 1., 2., 3. }, { 11., 12., 13. }, { 31., 32., 33. } };

    auto col1 = m(Eigen::all, 0);
    auto col2 = m(Eigen::all, 1);

    Eigen::Vector3d obtained;
    obtained.noalias() = col1.cross(col2);

    const Eigen::Vector3d expected{ { -20., 30., -10. } };

    BOOST_CHECK(obtained.isApprox(expected, 1.e-15));
}


BOOST_AUTO_TEST_CASE(inverse)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Matrix3d m{ { -2., -2., 0. }, { 0., 0., 2. }, { -2., 0., 0. } };

    const Eigen::Matrix3d expected{ { 0., 0., -0.5 }, { -0.5, 0., 0.5 }, { 0., 0.5, 0. } };

    Eigen::Matrix3d obtained;
    obtained.noalias() = m.inverse();

    BOOST_CHECK(obtained.isApprox(expected, 1.e-15));
}


BOOST_AUTO_TEST_CASE(vdot)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    const Eigen::Vector<double, 6> v1{
        { 1.210000e+00, 8.125000e-01, 1.020304e+00, 5.500001e-02, -7.976151e-10, -2.633869e-08 }
    };

    const Eigen::Vector<double, 6> v2{ { 0., 3.015369e-02, 9.698463e-01, -0., 1.710101e-01, 0. } };

    constexpr auto expected = 1.0140379322638;
    constexpr auto eps = 1.e-15;

    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (v1.dot(v2))(expected)(eps));
}


// No actual test here; this is just to show how to define a vector which maximum size is set at compile-time.
// Please note memory allocation in Eigen is forbidden - this is done on the stack!
BOOST_AUTO_TEST_CASE(vector_max_size)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    constexpr auto Nrow_max = 3;
    constexpr auto Ncol = 1;

    // clang-format off
    using vector_type = Eigen::Matrix
    <
        double,
        Eigen::Dynamic,
        Ncol,
        Eigen::ColMajor,
        Nrow_max
    >;
    // clang-format on

    [[maybe_unused]] auto ok_vector_size_2 = vector_type(2);
    [[maybe_unused]] auto ok_vector_size_3 = vector_type(3);

    // auto ko_vector = vector_type(4); // would fail at runtime! (through an assert in Eigen)
}


// Focus on memory allocation here!
BOOST_AUTO_TEST_CASE(determinant)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(true);
    constexpr auto epsilon = 1.e-15;

    {
        // There is dynamic allocation in `determinant()` call here!
        const Eigen::MatrixXd dynamic{ { 3, 7 }, { 1, -4 } };
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (dynamic.determinant())(-19.)(epsilon));
    }

    {
        // Even specifying max size leads to a dynamic allocation
        using matrix_3d_max_type = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, 3, 3>;
        const matrix_3d_max_type special{ { 3, 7 }, { 1, -4 } };
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (special.determinant())(-19.)(epsilon));
    }

    {
        // No dynamic allocation if size is known - at least up to 4 (I wasn't sure it would work for more than 3 tbh)
        MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);
        const Eigen::Matrix2d static_matrix_2d{ { 3, 7 }, { 1, -4 } };
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (static_matrix_2d.determinant())(-19.)(epsilon));

        const Eigen::Matrix3d static_matrix_3d{ { 3, 7, 5 }, { 1, -4, -8 }, { 4, 7, 8 } };
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (static_matrix_3d.determinant())(-93)(epsilon));

        const Eigen::Matrix4d static_matrix_4d{ { 3, 7, 5, -7 }, { 0, 1, -4, -8 }, { 4, 7, 8, 2 }, { 3, 7, 5, 1 } };
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (static_matrix_4d.determinant())(-192)(epsilon));
    }


    {
        MoReFEM::Wrappers::EigenNS::DimensionMatrixVariant variant_matrix;

        {
            variant_matrix = Eigen::Matrix2d{ { 3, 7 }, { 1, -4 } };
            BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                                  (MoReFEM::Wrappers::EigenNS::ComputeDeterminant(variant_matrix))(-19.)(epsilon));
        }

        {
            variant_matrix = Eigen::Matrix3d{ { 3, 7, 5 }, { 1, -4, -8 }, { 4, 7, 8 } };
            BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                                  (MoReFEM::Wrappers::EigenNS::ComputeDeterminant(variant_matrix))(-93)(epsilon));
        }

        {
            variant_matrix = Eigen::Matrix<double, 1, 1>{ { 5 } };
            BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                                  (MoReFEM::Wrappers::EigenNS::ComputeDeterminant(variant_matrix))(5)(epsilon));
        }
    }
}


// We used to define a function ExtractGradientBasedBlockRowMatrix() which filled a smaller matrix from part
// of a fuller one. It is of course better to use views for that purpose.
// It's very similar to subset_view test above; the instruction below are really what replace former functions (see
// #1941).
BOOST_AUTO_TEST_CASE(slicing_for_extract_gradient_based_block_row_matrix)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    Eigen::Matrix<double, 9, 1> full_vector{ { 0, 1, 2, 3, 4, 5, 6, 7, 8 } };

    BOOST_CHECK(full_vector(Eigen::seqN(0, 3)).isApprox(Eigen::Vector3d{ { 0, 1, 2 } }, 1.e-15));
    BOOST_CHECK(full_vector(Eigen::seqN(6, 3)).isApprox(Eigen::Vector3d{ { 6, 7, 8 } }, 1.e-15));

    // Main check here is no memory allocation!
    const MoReFEM::Wrappers::EigenNS::RowVectorMaxNd<3> extraction = full_vector(Eigen::seqN(0, 3));
    BOOST_CHECK(extraction.isApprox(MoReFEM::Wrappers::EigenNS::RowVectorMaxNd<3>{ { 0, 1, 2 } }, 1.e-15));
}


BOOST_AUTO_TEST_CASE(check_eigen_types)
{
    {
        using type = double;
        static_assert(!MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }

    {
        using type = int;
        static_assert(!MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }

    {
        using type = Eigen::MatrixXd;
        static_assert(MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }

    {
        using type = Eigen::VectorXd;
        static_assert(MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }

    {
        using type = Eigen::Matrix<double, 9, 1>;
        static_assert(MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }

    {
        using type = MoReFEM::Wrappers::EigenNS::VectorMaxNd<6>;
        static_assert(MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }

    {
        using type = MoReFEM::Wrappers::EigenNS::MatrixMaxNd<6>;
        static_assert(MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }


    {
        using type = Eigen::RowVector3d;
        static_assert(MoReFEM::Wrappers::EigenNS::IsLinearAlgebra<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsVector<type>{});
        static_assert(!MoReFEM::Wrappers::EigenNS::IsMatrix<type>{});
    }
}


// Checks related to changes performed in `ForUnknownList::ComputeLocalFEltSpaceData()`.
BOOST_AUTO_TEST_CASE(determinant_sub_matrix)
{
    using MoReFEM::NumericNS::Square;

    {
        Eigen::Matrix3d jacobian3d = Eigen::Matrix3d::Random();
        auto col1 = jacobian3d(Eigen::all, 0);
        auto col2 = jacobian3d(Eigen::all, 1);

        Eigen::Vector3d cross_product = col1.cross(col2);

        BOOST_CHECK_CLOSE(std::sqrt(Square(cross_product(0)) + Square(cross_product(1)) + Square(cross_product(2))),
                          cross_product.norm(),
                          1.e-15);
    }

    {
        Eigen::Matrix2d jacobian2d = Eigen::Matrix2d::Random();

        BOOST_CHECK_CLOSE(std::sqrt(Square(jacobian2d(0, 0)) + Square(jacobian2d(1, 0))),
                          jacobian2d(Eigen::seq(0, 1), 0).norm(),
                          1.e-15);
    }
}


BOOST_AUTO_TEST_CASE(extract_sub_matrix)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

    Eigen::MatrixXi matrix;
    matrix.resize(3, 3);
    Eigen::MatrixXi submatrix;
    submatrix.resize(2, 2);

    matrix << 0, 1, 2, 10, 11, 12, 20, 21, 22;

    const std::vector<Eigen::Index> row_selection{ 0, 2 };
    const std::vector<Eigen::Index> col_selection{ 1, 2 };

    const Eigen::Matrix2i expected_submatrix{ { 1, 2 }, { 21, 22 } };

    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    submatrix.noalias() = matrix(row_selection, col_selection);

    BOOST_CHECK(submatrix.isApprox(expected_submatrix));
}


// Check slicing gives exactly same result as pre-existing code.
namespace // anonymous
{

    double DotProductHelper(Eigen::Index component,
                            const ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3>& contravariant_basis,
                            const Eigen::VectorXd& tau_interpolate_at_quad_point)
    {
        double ret = 0.;
        for (auto i = Eigen::Index{}, size = tau_interpolate_at_quad_point.size(); i < size; ++i)
            ret += tau_interpolate_at_quad_point(i) * contravariant_basis(i, component);

        return ret;
    };

} // namespace


BOOST_AUTO_TEST_CASE(dot_product_helper)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(true);

    ::MoReFEM::Wrappers::EigenNS::MatrixMaxNd<3> contravariant_basis = Eigen::Matrix3d::Random();
    Eigen::Vector3d tau_interpolate_at_quad_point = Eigen::Vector3d::Random();

    constexpr auto epsilon = 1.e-15;

    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                          (DotProductHelper(0, contravariant_basis, tau_interpolate_at_quad_point))(
                              tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, 0))(epsilon));

    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                          (DotProductHelper(1, contravariant_basis, tau_interpolate_at_quad_point))(
                              tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, 1))(epsilon));

    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                          (DotProductHelper(2, contravariant_basis, tau_interpolate_at_quad_point))(
                              tau_interpolate_at_quad_point.transpose() * contravariant_basis(Eigen::all, 2))(epsilon));
}


BOOST_AUTO_TEST_CASE(matrix_from_vectors)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    ::Eigen::Vector4i v1;
    v1 << 0, 10, 100, 1000;

    ::Eigen::Vector3i v2;
    v2 << 1, 2, 3;

    ::Eigen::Matrix<int, 4, 3> obtained;


    ::Eigen::Matrix<int, 4, 3> expected;

    expected << 0, 0, 0, 10, 20, 30, 100, 200, 300, 1000, 2000, 3000;


    MoReFEM::Wrappers::EigenNS::BroadcastTwoVectorsIntoMatrix(v1, v2, obtained);

    BOOST_CHECK_EQUAL(obtained, expected);
}


BOOST_AUTO_TEST_CASE(init_dimension_matrix_variant)
{
    MoReFEM::Advanced::Wrappers::EigenNS::IsMallocAllowed(false);

    {
        const Eigen::Index dimension{ 1 };
        [[maybe_unused]] auto dim2 = MoReFEM::Wrappers::EigenNS::InitDimensionMatrixVariant(dimension);
    }

    {
        const Eigen::Index dimension{ 2 };
        [[maybe_unused]] auto dim2 = MoReFEM::Wrappers::EigenNS::InitDimensionMatrixVariant(dimension);
    }

    {
        const Eigen::Index dimension{ 3 };
        [[maybe_unused]] auto dim2 = MoReFEM::Wrappers::EigenNS::InitDimensionMatrixVariant(dimension);
    }
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,modernize-use-std-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
