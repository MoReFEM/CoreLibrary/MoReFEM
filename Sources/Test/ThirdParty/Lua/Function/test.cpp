// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <exception>
#include <string>
#include <utility>

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#define BOOST_TEST_MODULE lua_function
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    std::string Fct1()
    {
        static const std::string ret("function (x, y, z) "
                                     "return 3 * x + y - z "
                                     "end");

        return ret;
    }


    std::string Fct2()
    {
        static const std::string ret("function (x, y, z) "
                                     "return -10 * x + 5 * y - z "
                                     "end");

        return ret;
    }


    using lua_function_type = Wrappers::Lua::Function<int(int, int, int)>;


} // namespace


BOOST_AUTO_TEST_CASE(simple_construction)
{
    const lua_function_type function(Fct1());

    BOOST_CHECK_EQUAL(function(1, 2, 3), 2);
}


BOOST_AUTO_TEST_CASE(default_constructed_cant_be_used)
{
    const lua_function_type function;

    BOOST_CHECK_THROW(function(1, 2, 3), std::exception);
}


BOOST_AUTO_TEST_CASE(copy_constructor)
{
    const lua_function_type function1(Fct1());

    BOOST_CHECK_EQUAL(function1(1, 2, 3), 2);

    // NOLINTNEXTLINE(performance-unnecessary-copy-initialization)
    const lua_function_type function2(function1);

    BOOST_CHECK_EQUAL(function2(-2, 4, -7), 5);
    BOOST_CHECK_EQUAL(function1(4, -11, 5), -4);
}


BOOST_AUTO_TEST_CASE(move_constructor)
{
    lua_function_type function1(Fct1());

    BOOST_CHECK_EQUAL(function1(1, 2, 3), 2);

    // NOLINTNEXTLINE(performance-unnecessary-copy-initialization)
    const lua_function_type function2(std::move(function1));

    BOOST_CHECK_EQUAL(function2(-2, 4, -7), 5);

    // NOLINTBEGIN(bugprone-use-after-move,hicpp-invalid-access-moved,clang-analyzer-cplusplus.Move) - that's the
    // whole point of the test!
    BOOST_CHECK_THROW(function1(4, -11, 5), std::exception);
    // NOLINTEND(bugprone-use-after-move,hicpp-invalid-access-moved,clang-analyzer-cplusplus.Move)
}


BOOST_AUTO_TEST_CASE(copy_assignment)
{
    const lua_function_type function1(Fct1());

    lua_function_type function3;
    function3 = function1;
    BOOST_CHECK_EQUAL(function3(10, -2, 6), 22);

    BOOST_CHECK_EQUAL(function1(4, -11, 5), -4);

    const lua_function_type function2(Fct2());

    function3 = function2;

    BOOST_CHECK_EQUAL(function3(4, -11, 5), -100);
}


BOOST_AUTO_TEST_CASE(move_assignment)
{
    lua_function_type function1(Fct1());

    lua_function_type function3;
    function3 = std::move(function1);
    BOOST_CHECK_EQUAL(function3(10, -2, 6), 22);

    // NOLINTBEGIN(bugprone-use-after-move,hicpp-invalid-access-moved,clang-analyzer-cplusplus.Move) - that's the
    // whole point of the test!
    BOOST_CHECK_THROW(function1(4, -11, 5), std::exception);
    // NOLINTEND(bugprone-use-after-move,hicpp-invalid-access-moved,clang-analyzer-cplusplus.Move)

    lua_function_type function2(Fct2());

    function3 = std::move(function2);

    BOOST_CHECK_EQUAL(function3(4, -11, 5), -100);
}


BOOST_AUTO_TEST_CASE(self_copy)
{
    lua_function_type function1(Fct1());

    BOOST_CHECK_EQUAL(function1(1, 2, 3), 2);

#ifdef __clang__
    PRAGMA_DIAGNOSTIC(push)
    PRAGMA_DIAGNOSTIC(ignored "-Wself-assign-overloaded") // that's on purpose here!
#endif                                                    // __clang__

    function1 = function1;

#ifdef __clang__
    PRAGMA_DIAGNOSTIC(pop)
#endif // __clang__

    BOOST_CHECK_EQUAL(function1(4, -11, 5), -4);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
