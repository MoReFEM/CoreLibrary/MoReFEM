simple_map_value = { [3] = 5, [4] = 7, [5] = 8 }

map_of_string = { ["first"] = "foo", ["second"] = "bar", ["third"] = "baz" }

map_of_vector = { ["first"] = { 2, 3, 5, 7}, ["second"] = { 11, 13, 17, 23, 29 } }
