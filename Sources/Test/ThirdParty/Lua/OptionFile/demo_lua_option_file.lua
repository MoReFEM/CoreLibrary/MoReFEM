section1 = {
    string_value = "string",

    double_value = 5.215,

    int_value = 10,

    vector_value = { "foo", "bar", "baz"},
    
    vector_int_value = { 2, 3, 5 },

    map_value = { [3] = 5, [4] = 7, [5] = 8 }

}


root_value = 2.44




section2 = {
    one_arg_function = [[
    function (x)
        return -x;
    end
    ]],

    several_arg_function = [[
    function (x, y, z)
        return x + y - z;
    end
    ]],

	function_with_if =
		   [[
			   function(x, y, z)
				   if (((x>=4) and (x<=5.99)) or ((x>=7.01) and (x<=8.99))) then
					   return 1.;
				   else
					   return 0.
				   end
			   end
		   ]]

}


section3 = {
    invalid_function = [[
    fnction (x)
    return -x;
    end
    ]],

}
