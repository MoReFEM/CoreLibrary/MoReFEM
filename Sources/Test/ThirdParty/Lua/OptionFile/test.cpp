// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <exception>
#include <filesystem>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#define BOOST_TEST_MODULE lua_option_file
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    /*!
     * \brief Just an helper function to make the tests more concise.
     *
     * The function returns by value what was read, and constraint is bypassed in the call by default.
     */
    template<class T>
    T ReadOptionFile(::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
                     const std::string& key,
                     std::string_view constraint = "")
    {
        T ret;
        lua_option_file.Read(key, constraint, ret);
        return ret;
    }


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(parameters_properly_read, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr = std::make_unique<::MoReFEM::Wrappers::Lua::OptionFile>(input_file));

    auto& lua_option_file = *(ptr);

    BOOST_CHECK(NumericNS::AreEqual(ReadOptionFile<double>(lua_option_file, "root_value"), 2.44));

    BOOST_CHECK(ReadOptionFile<std::string>(lua_option_file, "section1.string_value") == "string");
    BOOST_CHECK(NumericNS::AreEqual(ReadOptionFile<double>(lua_option_file, "section1.double_value"), 5.215));

    BOOST_CHECK(ReadOptionFile<std::size_t>(lua_option_file, "section1.int_value") == 10UL);
    BOOST_CHECK_THROW(ReadOptionFile<std::size_t>(lua_option_file, "section1.double_value"), std::exception);

    BOOST_CHECK(ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value").size() == 3UL);
    BOOST_CHECK(ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value")[0] == "foo");
    BOOST_CHECK(ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value")[1] == "bar");
    BOOST_CHECK(ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value")[2] == "baz");
    BOOST_CHECK_THROW(ReadOptionFile<int>(lua_option_file, "section1.vector_value"), std::exception);
    BOOST_CHECK_THROW(ReadOptionFile<std::vector<int>>(lua_option_file, "section1.int_value"), std::exception);

    BOOST_CHECK(ReadOptionFile<std::string>(lua_option_file, "section1.string_value") == "string");

    BOOST_CHECK((ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value").size() == 3UL));
    BOOST_CHECK((ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value")[3] == 5));
    BOOST_CHECK((ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value")[4] == 7));
    BOOST_CHECK((ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value")[5] == 8));

    BOOST_CHECK_THROW(ReadOptionFile<std::string>(lua_option_file, "unknown_key"), std::exception);
    BOOST_CHECK_THROW(ReadOptionFile<double>(lua_option_file, "section1.string_value"), std::exception);

    BOOST_CHECK_THROW(ReadOptionFile<double>(lua_option_file, "section1.invalid_value"), std::exception);

    // When I have time to investigate how to do so in Boost
    // CHECK_ABORT(ReadOptionFile<double>(lua_option_file));
}


BOOST_FIXTURE_TEST_CASE(constraints, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr = std::make_unique<::MoReFEM::Wrappers::Lua::OptionFile>(input_file));
    auto& lua_option_file = *(ptr);

    BOOST_CHECK_THROW(NumericNS::AreEqual(ReadOptionFile<double>(lua_option_file, "root_value", "v > 5."), 2.44),
                      std::exception);
    /* BOOST_CHECK_NO_THROW */ (
        NumericNS::AreEqual(ReadOptionFile<double>(lua_option_file, "root_value", "v < 3."), 2.44));

    /* BOOST_CHECK_NO_THROW */ (ReadOptionFile<std::vector<std::string>>(
        lua_option_file, "section1.vector_value", "value_in(v, { 'foo', 'bar', 'baz' })"));

    BOOST_CHECK_THROW(ReadOptionFile<std::vector<std::string>>(
                          lua_option_file, "section1.vector_value", "value_in(v, { 'bar', 'baz' })"),
                      std::exception);

    ReadOptionFile<std::vector<int>>(lua_option_file, "section1.vector_int_value", "v < 10");

    BOOST_CHECK_THROW(ReadOptionFile<std::vector<int>>(lua_option_file, "section1.vector_int_value", "v > 10"),
                      std::exception);
}


BOOST_FIXTURE_TEST_CASE(lua_functions, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    ptr = std::make_unique<::MoReFEM::Wrappers::Lua::OptionFile>(input_file);
    auto& lua_option_file = *(ptr);

    decltype(auto) one_arg_fct =
        ReadOptionFile<Wrappers::Lua::Function<double(double)>>(lua_option_file, "section2.one_arg_function");

    BOOST_CHECK(NumericNS::AreEqual(one_arg_fct(3.), -3.));

    decltype(auto) several_arg_function = ReadOptionFile<Wrappers::Lua::Function<double(double, double, double)>>(
        lua_option_file, "section2.several_arg_function");

    BOOST_CHECK(NumericNS::AreEqual(several_arg_function(3., 4., 5.), 2.));


    decltype(auto) function_with_if = ReadOptionFile<Wrappers::Lua::Function<double(double, double, double)>>(
        lua_option_file, "section2.function_with_if");

    BOOST_CHECK(NumericNS::AreEqual(function_with_if(5., 0., 0.), 1.));
    BOOST_CHECK(NumericNS::AreEqual(function_with_if(15., 0., 0.), 0.));
}


BOOST_FIXTURE_TEST_CASE(redundant, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/redundancy.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr = nullptr;
    BOOST_REQUIRE_THROW(ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file)), std::exception);
}


BOOST_FIXTURE_TEST_CASE(redundant_in_section, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/redundancy_in_section.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr = nullptr;
    BOOST_REQUIRE_THROW(ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file)), std::exception);
}


BOOST_FIXTURE_TEST_CASE(invalid_lua, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    ptr = std::make_unique<::MoReFEM::Wrappers::Lua::OptionFile>(input_file);
    auto& lua_option_file = *(ptr);

    BOOST_REQUIRE_THROW(
        ReadOptionFile<Wrappers::Lua::Function<double(double)>>(lua_option_file, "section3.invalid_function"),
        std::exception);
}


BOOST_FIXTURE_TEST_CASE(map_in_vector, TestNS::FixtureNS::TestEnvironment)
{
    // Introduced after #1468, in which two braces on the same line wreak havoc...
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/map_in_vector.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    const ::MoReFEM::Wrappers::Lua::OptionFile lua(input_file);

    std::ostringstream oconv;

    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(),
                      "[ValueOutsideBrace, "
                      "TransientSource4.nature, TransientSource4.value, "
                      "TransientSource5.nature, TransientSource5.value, "
                      "TransientSource5.whatever.sublevel, "
                      "TransientSource5.whatever.sublevel2, "
                      "InitialCondition1.nature, InitialCondition1.value]");
}


BOOST_FIXTURE_TEST_CASE(no_section, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/no_section.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    const ::MoReFEM::Wrappers::Lua::OptionFile lua(input_file);

    std::ostringstream oconv;
    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(), "[a, b, c, d, e, f]");
}


BOOST_FIXTURE_TEST_CASE(entry_with_underscore, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/entry_with_underscore.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    const ::MoReFEM::Wrappers::Lua::OptionFile lua(input_file);

    std::ostringstream oconv;
    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(), "[transient.init_time, transient.timeStep, transient.timeMax]");
}


BOOST_FIXTURE_TEST_CASE(advanced_map, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/advanced_map.lua" };
    const FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr = std::make_unique<::MoReFEM::Wrappers::Lua::OptionFile>(input_file));

    auto& lua_option_file = *(ptr);

    {
        const auto simple_map_value = ReadOptionFile<std::map<int, int>>(lua_option_file, "simple_map_value");

        BOOST_CHECK_EQUAL(simple_map_value.size(), 3UL);
        BOOST_CHECK_EQUAL(simple_map_value.at(3), 5);
        BOOST_CHECK_EQUAL(simple_map_value.at(4), 7);
        BOOST_CHECK_EQUAL(simple_map_value.at(5), 8);
    }

    {
        const auto map_of_string = ReadOptionFile<std::map<std::string, std::string>>(lua_option_file, "map_of_string");

        BOOST_CHECK_EQUAL(map_of_string.size(), 3UL);
        BOOST_CHECK_EQUAL(map_of_string.at("first"), "foo");
        BOOST_CHECK_EQUAL(map_of_string.at("second"), "bar");
        BOOST_CHECK_EQUAL(map_of_string.at("third"), "baz");
    }

    {
        const auto map_of_vector =
            ReadOptionFile<std::map<std::string, std::vector<int>>>(lua_option_file, "map_of_vector");
        const std::vector<int> expected_first{ 2, 3, 5, 7 };
        BOOST_CHECK(map_of_vector.at("first") == expected_first);
        const std::vector<int> expected_second{ 11, 13, 17, 23, 29 };
        BOOST_CHECK(map_of_vector.at("second") == expected_second);
    }
}


BOOST_FIXTURE_TEST_CASE(keys_with_numbers, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/keys_with_numbers_inside.lua" };
    const FilesystemNS::File input_file{ std::move(path) };

    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr = std::make_unique<::MoReFEM::Wrappers::Lua::OptionFile>(input_file));

    auto& lua_option_file = *(ptr);

    BOOST_CHECK(ReadOptionFile<std::string>(lua_option_file, "field_1_with_number_inside_key_1") == "foo");
    BOOST_CHECK(ReadOptionFile<std::string>(lua_option_file, "field_2_with_number_inside_key_1") == "bar");
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
