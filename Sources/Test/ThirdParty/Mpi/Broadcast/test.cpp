// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <vector>

#define BOOST_TEST_MODULE mpi_broadcast
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_broadcast, MoReFEM::TestNS::FixtureNS::TestEnvironment)


BOOST_AUTO_TEST_CASE(broadcast_vector_root_sender)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    const std::vector<std::size_t> expected_result{ 2, 5, 7, 11 };
    std::vector<std::size_t> broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_vector_no_root_sender)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    constexpr auto sender_rank = rank_type{ 2UL };

    const std::vector<std::size_t> expected_result{ 2, 5, 7, 11 };
    std::vector<std::size_t> broadcasted_data;

    if (mpi.GetRank() == sender_rank)
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data, sender_rank);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_single_value)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    const long expected_result{ 15457215L };
    long broadcasted_data = 0;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}

BOOST_AUTO_TEST_CASE(broadcast_bool_true)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    const bool expected_result{ true };
    bool broadcasted_data = false;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}

BOOST_AUTO_TEST_CASE(broadcast_bool_false)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    const bool expected_result{ false };
    bool broadcasted_data = false;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_vector_bool)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    const std::vector<bool> expected_result{ false, true, true, false, true };
    std::vector<bool> broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_CASE(broadcast_empty_vector)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    const std::vector<bool> expected_result;
    std::vector<bool> broadcasted_data;

    if (mpi.IsRootProcessor())
        broadcasted_data = expected_result;
    else
        broadcasted_data.resize(expected_result.size());

    mpi.Broadcast(broadcasted_data);

    BOOST_CHECK(broadcasted_data == expected_result);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
