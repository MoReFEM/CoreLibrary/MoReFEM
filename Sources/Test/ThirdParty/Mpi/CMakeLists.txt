include(${CMAKE_CURRENT_LIST_DIR}/Broadcast/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Gather/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/SendReceive/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/IsSameForAllRanks/CMakeLists.txt)
