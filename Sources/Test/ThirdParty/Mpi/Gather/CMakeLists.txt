add_executable(TestMpiGather)

target_sources(TestMpiGather
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestMpiGather
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)
    
morefem_boost_test_parallel_mode(NAME MpiGather
                                 EXE TestMpiGather
                                 NPROC 3
                                 TIMEOUT 20)

morefem_organize_IDE(TestMpiGather Test/ThirdParty/Mpi Test/ThirdParty/Mpi/Gather)
