// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <vector>

#define BOOST_TEST_MODULE mpi_gather
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    const std::vector<int>& GetImbalancedSentData(const Wrappers::Mpi& mpi);

    const std::vector<int>& GetBalancedSentData(const Wrappers::Mpi& mpi);


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_gather, MoReFEM::TestNS::FixtureNS::TestEnvironment)


BOOST_AUTO_TEST_CASE(gather)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) balanced_sent_data = GetBalancedSentData(mpi);

    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.Gather(balanced_sent_data, gathered_data));

    if (mpi.GetRank<int>() == 0)
    {
        const std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23 };
        BOOST_CHECK(gathered_data == expected);
    } else
        BOOST_CHECK(gathered_data.empty());
}


BOOST_AUTO_TEST_CASE(all_gather)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) balanced_sent_data = GetBalancedSentData(mpi);

    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.AllGather(balanced_sent_data, gathered_data));

    const std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23 };
    BOOST_CHECK(gathered_data == expected);
}


//// \todo See if Boost may handle a check abort in some way
////    BOOST_AUTO_TEST_CASE(gather_invalid_size)
////    {
////        decltype(auto) imbalanced_sent_data = GetImbalancedSentData();
////        decltype(auto) mpi = GetMpi();
////
////        std::vector<int> gathered_data;
////        BOOST_CHECK_ABORT(mpi.Gather(imbalanced_sent_data, gathered_data));
////    }
//
//
BOOST_AUTO_TEST_CASE(gatherv)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) imbalanced_sent_data = GetImbalancedSentData(mpi);


    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.Gatherv(imbalanced_sent_data, gathered_data));

    if (mpi.GetRank<int>() == 0)
    {
        const std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23, 24 };
        BOOST_CHECK(gathered_data == expected);
    } else
        BOOST_CHECK(gathered_data.empty());
}


BOOST_AUTO_TEST_CASE(allgatherv)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!
    decltype(auto) imbalanced_sent_data = GetImbalancedSentData(mpi);

    std::vector<int> gathered_data;
    /* BOOST_CHECK_NO_THROW */ (mpi.AllGatherv(imbalanced_sent_data, gathered_data));

    const std::vector<int> expected{ 1, 2, 3, 11, 12, 13, 21, 22, 23, 24 };
    BOOST_CHECK(gathered_data == expected);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    template<bool IsBalancedT>
    std::vector<int> GenerateSentData(rank_type rank)
    {
        std::vector<int> ret;

        switch (rank.Get())
        {
        case 0UL:
            ret = { 1, 2, 3 };
            break;
        case 1UL:
            ret = { 11, 12, 13 };
            break;
        case 2UL:
            if (IsBalancedT)
                ret = { 21, 22, 23 };
            else
                ret = { 21, 22, 23, 24 };
            break;
        }

        return ret;
    }


    const std::vector<int>& GetImbalancedSentData(const Wrappers::Mpi& mpi)
    {
        static const std::vector<int> ret = GenerateSentData<false>(mpi.GetRank());
        return ret;
    }


    const std::vector<int>& GetBalancedSentData(const Wrappers::Mpi& mpi)
    {
        static const std::vector<int> ret = GenerateSentData<true>(mpi.GetRank());
        return ret;
    }


} // namespace
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
