// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <vector>

#define BOOST_TEST_MODULE mpi_is_same_for_all_ranks
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Mpi/IsSameForAllRanks.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_is_same_for_all_ranks, MoReFEM::TestNS::FixtureNS::TestEnvironment)


BOOST_AUTO_TEST_CASE(integer_success)
{
    decltype(auto) mpi = GetMpi();

    const std::vector<std::size_t> test{ 2, 3, 5, 7, 9 };

    BOOST_CHECK(IsSameForAllRanks(mpi, test) == true);
}


BOOST_AUTO_TEST_CASE(integer_failure_size)
{
    decltype(auto) mpi = GetMpi();

    std::vector<std::size_t> test{ 2, 3, 5, 7, 9 };

    if (mpi.GetRank<int>() == 1)
        test.push_back(11);

    BOOST_CHECK(IsSameForAllRanks(mpi, test) == false);
}


BOOST_AUTO_TEST_CASE(integer_failure_content)
{
    decltype(auto) mpi = GetMpi();

    std::vector<std::size_t> test{ 2, 3, 5, 7, 9 };

    if (mpi.GetRank<int>() == 2)
        test[0] = 1;

    BOOST_CHECK(IsSameForAllRanks(mpi, test) == false);
}


BOOST_AUTO_TEST_CASE(float_epsilon)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    std::vector<float> test{ 2.34F, 3.84F, 0.25F, 1.47F, 9.F };

    if (mpi.GetRank<int>() == 2)
        test[1] += 0.004F;

    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-2F) == true);
    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-5F) == false);
}


BOOST_AUTO_TEST_CASE(double_epsilon)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor<int>() == 3); // This test has been written for three processors specifically!

    std::vector<double> test{ 2.34, 3.84, 0.25, 1.47, 9. };

    if (mpi.GetRank<int>() == 2)
        test[1] += 0.004;

    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-2) == true);
    BOOST_CHECK(IsSameForAllRanks(mpi, test, 1.e-5) == false);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
