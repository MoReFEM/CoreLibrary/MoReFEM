add_executable(TestMpiSendReceive)

target_sources(TestMpiSendReceive
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)

target_link_libraries(TestMpiSendReceive
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)
                      
morefem_boost_test_parallel_mode(NAME MpiSendReceive
                                 EXE TestMpiSendReceive
                                 TIMEOUT 20)

morefem_organize_IDE(TestMpiSendReceive Test/ThirdParty/Mpi Test/ThirdParty/Mpi/SendReceive)
