// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <limits>
#include <vector>

#define BOOST_TEST_MODULE mpi_send_receive
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(mpi_send_receive, MoReFEM::TestNS::FixtureNS::TestEnvironment)

BOOST_AUTO_TEST_CASE(single_value)
{
    decltype(auto) mpi = GetMpi();
    BOOST_REQUIRE(mpi.Nprocessor().Get() == 4UL);

    const auto rank = mpi.GetRank();
    const auto Nproc = mpi.Nprocessor();

    double token = std::numeric_limits<double>::lowest();

    switch (rank.Get())
    {
    case 1UL:
        token = mpi.Receive<double>(rank - rank_type{ 1UL });
        BOOST_CHECK_CLOSE(token, 1.245, 1.e-6);
        break;
    case 2UL:
        token = mpi.Receive<double>(rank - rank_type{ 1UL });
        BOOST_CHECK_CLOSE(token, 11.245, 1.e-6);
        break;
    case 3UL:
        token = mpi.Receive<double>(rank - rank_type{ 1UL });
        BOOST_CHECK_CLOSE(token, 31.245, 1.e-6);
        break;
    case 0UL:
        // Set the token's value if you are process 0
        token = 1.245;
        break;
    }

    token += static_cast<double>(10 * rank.Get());

    mpi.Send(rank_type{ (rank + rank_type{ 1UL }).Get() % Nproc.Get() }, token);

    // Now process 0 can receive from the last process.
    if (rank == rank_type{ 0UL })
    {
        token = mpi.Receive<double>(Nproc - rank_type{ 1UL });

        BOOST_CHECK_CLOSE(token, 61.245, 1.e-6);
    }
}

BOOST_AUTO_TEST_CASE(array)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor().Get() == 4UL);

    const auto rank = mpi.GetRank();
    const auto Nproc = mpi.Nprocessor();


    std::vector<int> token{ NumericNS::UninitializedIndex<int>(), NumericNS::UninitializedIndex<int>() };

    constexpr auto MAX_LENGTH = 6UL;

    switch (rank.Get())
    {
    case 1UL:
        token = mpi.Receive<int>(rank - rank_type{ 1UL }, MAX_LENGTH);
        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0 }));
        break;
    case 2UL:
        token = mpi.Receive<int>(rank - rank_type{ 1UL }, MAX_LENGTH);
        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0, 10 }));
        break;
    case 3UL:
        token = mpi.Receive<int>(rank - rank_type{ 1UL }, MAX_LENGTH);
        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0, 10, 20 }));
        break;
    case 0UL:
        // Set the token's value if you are process 0
        token = { -1, -2 };
        break;
    }

    token.push_back(static_cast<int>(rank.Get()) * 10);

    mpi.SendContainer(rank_type{ (rank + rank_type{ 1UL }).Get() % Nproc.Get() }, token);

    // Now process 0 can receive from the last process.
    if (rank == rank_type{ 0UL })
    {
        token = mpi.Receive<int>(Nproc - rank_type{ 1UL }, MAX_LENGTH);

        BOOST_CHECK((token == std::vector<int>{ -1, -2, 0, 10, 20, 30 }));
    }
}


BOOST_AUTO_TEST_CASE(array_strong_type)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor().Get() == 4UL);

    const auto rank = mpi.GetRank();
    const auto Nproc = mpi.Nprocessor();

    using IntType = StrongType<int,
                               struct IntTypeTag,
                               StrongTypeNS::DefaultConstructible,
                               StrongTypeNS::Comparable,
                               StrongTypeNS::AsMpiDatatype>;


    std::vector<IntType> token(2UL);

    constexpr auto MAX_LENGTH = 6UL;

    switch (rank.Get())
    {
    case 1UL:
        token = mpi.Receive<IntType>(rank - rank_type{ 1UL }, MAX_LENGTH);
        BOOST_CHECK((token == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0) }));
        break;
    case 2UL:
        token = mpi.Receive<IntType>(rank - rank_type{ 1UL }, MAX_LENGTH);
        BOOST_CHECK((token == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0), IntType(10) }));
        break;
    case 3UL:
        token = mpi.Receive<IntType>(rank - rank_type{ 1UL }, MAX_LENGTH);
        BOOST_CHECK((token == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0), IntType(10), IntType(20) }));
        break;
    case 0UL:
        // Set the token's value if you are process 0
        token = { IntType(-1), IntType(-2) };
        break;
    }

    token.emplace_back(static_cast<int>(rank.Get()) * 10);

    mpi.SendContainer(rank_type{ static_cast<std::size_t>((rank.Get() + 1) % Nproc.Get()) }, token);

    // Now process 0 can receive from the last process.
    if (rank == rank_type{ 0UL })
    {
        token = mpi.Receive<IntType>(Nproc - rank_type{ 1UL }, MAX_LENGTH);

        BOOST_CHECK(
            (token
             == std::vector<IntType>{ IntType(-1), IntType(-2), IntType(0), IntType(10), IntType(20), IntType(30) }));
    }
}

BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
