add_executable(TestPetscMatrixIO)

target_sources(TestPetscMatrixIO
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
)
          
target_link_libraries(TestPetscMatrixIO
                      TestPetscMatrix_lib)                    

morefem_boost_test_both_modes(NAME PetscMatrixIO
                              EXE TestPetscMatrixIO
                              TIMEOUT 10
                              LUA ${MOREFEM_ROOT}/Sources/Test/ThirdParty/PETSc/Matrix/IO/demo.lua)

morefem_organize_IDE(TestPetscMatrixIO Test/ThirdParty/PETSc/Matrix Test/ThirdParty/PETSc/Matrix/IO)
