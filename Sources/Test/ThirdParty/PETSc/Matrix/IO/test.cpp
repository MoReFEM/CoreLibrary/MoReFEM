// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <source_location>
#include <sstream>

#include "ThirdParty/Wrappers/Petsc/Matrix/FreeFunctions.hpp"

#include "Test/Tools/Fixture/Enum.hpp"

#define BOOST_TEST_MODULE petsc_matrix_io
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::PetscNS::MatrixNS::ToyModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on


    /*!
     * \brief Determines the path and name of the matrix on file and make sure it doesn't exist yet.
     */
    FilesystemNS::File PrepareFile(const TestNS::PetscNS::MatrixNS::ToyModel& model)
    {
        std::ostringstream oconv;
        oconv << model.GetOutputDirectory() << "/matrix_test_IO.bin";

        const auto file = FilesystemNS::File{ oconv.str() };

        if (model.GetMpi().IsRootProcessor())
        {
            if (file.DoExist())
                file.Remove();
        }

        return file;
    }


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(creation, fixture_type)
{
    [[maybe_unused]] decltype(auto) model = GetModel();
}


BOOST_FIXTURE_TEST_CASE(load_with_duplicated, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) matrix = model.GetMatrix();

    const auto matrix_file = PrepareFile(model);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == false);

    matrix.View(mpi, matrix_file, std::source_location::current(), PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == true);

    GlobalMatrix loaded_matrix(matrix.GetRowNumberingSubset(), matrix.GetColNumberingSubset());

    loaded_matrix.DuplicateLayout(matrix, MAT_DO_NOT_COPY_VALUES);

    loaded_matrix.LoadBinary(mpi, matrix_file);

    BOOST_CHECK(Wrappers::Petsc::AreStrictlyEqual(matrix, loaded_matrix));
}


BOOST_FIXTURE_TEST_CASE(load_with_minimal_init, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) matrix = model.GetMatrix();

    const auto matrix_file = PrepareFile(model);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == false);

    matrix.View(mpi, matrix_file, std::source_location::current(), PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == true);

    GlobalMatrix loaded_matrix(matrix.GetRowNumberingSubset(), matrix.GetColNumberingSubset());

    loaded_matrix.InitMinimalCase(mpi, matrix.GetType()); // the line that changed from previous case
    loaded_matrix.LoadBinary(mpi, matrix_file);

    auto Noriginal_program_wise_size = matrix.GetProgramWiseSize();
    auto Nloaded_program_wise_size = matrix.GetProgramWiseSize();

    // That is all we can check: there is no guarantee PETSc will use the same layout than if you specified it
    // yourself - as was done in the previous example. In MoReFEM context, it is unlikely you will ever need
    // the current situation - usually when we load data it is to reuse the same partitioning as was defined
    // in a previous run.
    BOOST_CHECK(Noriginal_program_wise_size == Nloaded_program_wise_size);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
