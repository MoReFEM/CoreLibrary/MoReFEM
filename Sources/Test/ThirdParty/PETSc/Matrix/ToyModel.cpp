// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <functional>
#include <memory>
#include <string>
#include <tuple>

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/InputData/Instances/Result.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/MoReFEMData/Extract.hpp"

#include "Geometry/Mesh/Internal/CreateMeshDataDirectory.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"

#include "Model/Internal/InitializeHelper.hpp"

#include "Test/ThirdParty/PETSc/Matrix/InputData.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixNS
{


    namespace // anonymous
    {


        FilesystemNS::Directory ExtractResultDirectory(const morefem_data_type& morefem_data)
        {
            const std::string path =
                ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Result::OutputDirectory>(morefem_data);

            FilesystemNS::Directory ret(morefem_data.GetMpi(), path, FilesystemNS::behaviour::overwrite);

            ret.ActOnFilesystem();

            return ret;
        }


    } // namespace


    ToyModel::ToyModel(morefem_data_type& morefem_data)
    : Crtp::CrtpMpi<ToyModel>(morefem_data.GetMpi()), output_directory_(ExtractResultDirectory(morefem_data))
    {
        decltype(auto) model_settings = GetNonCstModelSettings();
        model_settings.Init();
        model_settings.CheckTupleCompletelyFilled();

        Internal::ModelNS::InitMostSingletonManager(morefem_data);

        [[maybe_unused]] const Internal::GodOfDofNS::InitAllGodOfDof init(
            morefem_data,
            model_settings,
            DoConsiderProcessorWiseLocal2Global::no,
            Internal::MeshNS::CreateMeshDataDirectory(GetOutputDirectory()));

        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::sole));

        decltype(auto) row_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::row));
        decltype(auto) col_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::column));

        matrix_ = std::make_unique<GlobalMatrix>(row_numbering_subset, col_numbering_subset);
        auto& matrix = *matrix_;

        AllocateGlobalMatrix(god_of_dof, matrix);

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        decltype(auto) unknown1 = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::unknown1));
        decltype(auto) unknown2 = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::unknown2));

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const GlobalVariationalOperatorNS::Mass mass_op(felt_space, unknown2, unknown1);

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        mass_op.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));
    }


} // namespace MoReFEM::TestNS::PetscNS::MatrixNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
