// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_THIRDPARTY_PETSC_MATRIX_TOYMODEL_DOT_HPP_
#define MOREFEM_TEST_THIRDPARTY_PETSC_MATRIX_TOYMODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"

#include "Test/ThirdParty/PETSc/Matrix/InputData.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixNS
{


    /*!
     * \brief Toy model used to perform tests related to PETSc matrices
     *
     * Its role is mostly to fill a \a GlobalMatrix in a realistic case (mass operator  over a mesh).
     */
    class ToyModel : public Crtp::CrtpMpi<ToyModel>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = ToyModel;

      public:
        //! Define as a trait - I need that to use it with \a TestNS::FixtureNS::Model .
        using morefem_data_type = ::MoReFEM::TestNS::PetscNS::MatrixNS::morefem_data_type;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

        //! \copydoc doxygen_hide_model_specific_model_settings
        using model_settings_type = ModelSettings;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        ToyModel(morefem_data_type& morefem_data);

        //! Destructor.
        ~ToyModel() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ToyModel(const ToyModel& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ToyModel(ToyModel&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ToyModel& operator=(const ToyModel& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ToyModel& operator=(ToyModel&& rhs) = delete;

        ///@}

        //! Returns the path to result directory.
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

        //! Accessor to the matrix used for tests.
        const GlobalMatrix& GetMatrix() const noexcept;

        //! \copydoc doxygen_hide_model_settings_non_constant_accessor
        ModelSettings& GetNonCstModelSettings() noexcept;

      private:
        //! Global matrix which is used in the tests.
        GlobalMatrix::unique_ptr matrix_ = nullptr;

        //! Path to result directory.
        const FilesystemNS::Directory output_directory_;

        //! \copydoc doxygen_hide_model_settings_attribute
        ModelSettings model_settings_;
    };


} // namespace MoReFEM::TestNS::PetscNS::MatrixNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_THIRDPARTY_PETSC_MATRIX_TOYMODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
