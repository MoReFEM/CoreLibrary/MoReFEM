// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_THIRDPARTY_PETSC_MATRIX_TOYMODEL_DOT_HXX_
#define MOREFEM_TEST_THIRDPARTY_PETSC_MATRIX_TOYMODEL_DOT_HXX_
// IWYU pragma: private, include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::TestNS::PetscNS::MatrixNS
{


    inline const GlobalMatrix& ToyModel::GetMatrix() const noexcept
    {
        assert(!(!matrix_));
        return *matrix_;
    }


    inline const FilesystemNS::Directory& ToyModel::GetOutputDirectory() const noexcept
    {
        return output_directory_;
    }


    inline auto ToyModel::GetNonCstModelSettings() noexcept -> ModelSettings&
    {
        return model_settings_;
    }


} // namespace MoReFEM::TestNS::PetscNS::MatrixNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_THIRDPARTY_PETSC_MATRIX_TOYMODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
