// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cmath>
#include <memory>

#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"

#include "Utilities/Containers/Array.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "Test/ThirdParty/PETSc/MatrixOperations/ModelSettings.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::sole));
        decltype(auto) scalar_numbering_subset = GetScalarNumberingSubset();
        decltype(auto) vectorial_numbering_subset = GetVectorialNumberingSubset();
        decltype(auto) mpi = god_of_dof.GetMpi();

        for (auto i = 0UL; i < 2UL; ++i)
        {
            initialized_matrix_list_[i] =
                std::make_unique<GlobalMatrix>(scalar_numbering_subset, scalar_numbering_subset);
            initialized_matrix_list_[2UL + i] =
                std::make_unique<GlobalMatrix>(scalar_numbering_subset, vectorial_numbering_subset);
            initialized_matrix_list_[4UL + i] =
                std::make_unique<GlobalMatrix>(vectorial_numbering_subset, scalar_numbering_subset);
            initialized_matrix_list_[6UL + i] =
                std::make_unique<GlobalMatrix>(vectorial_numbering_subset, vectorial_numbering_subset);
        }

        for (auto i = 0UL; i < Utilities::ArraySize<decltype(initialized_matrix_list_)>::GetValue(); ++i)
            AllocateGlobalMatrix(god_of_dof, *initialized_matrix_list_[i]);


        initialized_vector_list_[0] = std::make_unique<GlobalVector>(scalar_numbering_subset);
        initialized_vector_list_[1] = std::make_unique<GlobalVector>(scalar_numbering_subset);
        AllocateGlobalVector(god_of_dof, *initialized_vector_list_[0]);
        AllocateGlobalVector(god_of_dof, *initialized_vector_list_[1]);

        initialized_vector_list_[2] = std::make_unique<GlobalVector>(vectorial_numbering_subset);
        initialized_vector_list_[3] = std::make_unique<GlobalVector>(vectorial_numbering_subset);
        AllocateGlobalVector(god_of_dof, *initialized_vector_list_[2]);
        AllocateGlobalVector(god_of_dof, *initialized_vector_list_[3]);


        // Put values into the first vector for each numbering subset.
        {
            const auto rank_plus_one = static_cast<double>(1 + mpi.GetRank<int>());

            {
                auto& vector = GetNonCstInitializedVector<0>();
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector);
                const auto size = content.GetSize();
                for (auto i = vector_processor_wise_index_type{ 0UL }; i < size; ++i)
                    content[i] = rank_plus_one * std::sqrt(i.Get()); // completely arbitrary value with no redundancy!
            }

            {
                auto& vector = GetNonCstInitializedVector<2>();
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector);
                const auto size = content.GetSize();
                for (auto i = vector_processor_wise_index_type{ 0UL }; i < size; ++i)
                    content[i] = rank_plus_one * std::sqrt(i.Get()); // completely arbitrary value with no redundancy!
            }
        }
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
