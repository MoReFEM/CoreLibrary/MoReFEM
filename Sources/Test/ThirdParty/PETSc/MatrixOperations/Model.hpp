// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODEL_DOT_HPP_
#define MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/Containers/Array.hpp"

#include "Core/InputData/Instances/Result.hpp"

#include "Model/Model.hpp"

#include "Test/ThirdParty/PETSc/MatrixOperations/ModelSettings.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
{


    /*!
     * \brief Toy model used to perform tests about reconstruction of a \a Mesh from pre-partitioned data.
     *
     * This test must be run in parallel; its principle is to:
     * - Initialize the model normally (including separating the mesh among all mpi processors and reducing it).
     * - Write the data related to the partition of said mesh.
     * - Load from the data written a new mesh.
     * - Check both mesh are indistinguishable.
     */
    //! \copydoc doxygen_hide_model_4_test
    // clang-format off
    class Model
    : public ::MoReFEM::Model
    <
        Model,
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::no
    >
    // clang-format on
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        // clang-format off
        using parent = MoReFEM::Model
        <
            self,
            morefem_data_type,
            DoConsiderProcessorWiseLocal2Global::no
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());


      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{

      private:
        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


        ///@}

      public:
        /*!
         * \brief Constant access to a matrix that was fully initialized (i.e. with layout also defined, not just
         * the constructor called).
         *
         * \tparam I Index of the requested matrix (some tests require several so several are stored). There are
         * static check for the consistency of this index.
         *
         * \return Constant accessor to an initialized matrix.
         */
        template<std::size_t I>
        const GlobalMatrix& GetInitializedMatrix() const noexcept;

        /*!
         * \brief Non-constant access to a matrix that was fully initialized (i.e. with layout also defined, not
         * just the constructor called).
         *
         * \tparam I Index of the requested matrix (some tests require several so several are stored). There are
         * static check for the consistency of this index.
         *
         * \return Non-constant accessor to an initialized matrix.
         */
        template<std::size_t I>
        GlobalMatrix& GetNonCstInitializedMatrix() noexcept;


        /*!
         * \brief Constant access to a vector that was fully initialized (i.e. with layout also defined, not just
         * the constructor called).
         *
         * \tparam I Index of the requested vector (some tests require several so several are stored). There are
         * static check for the consistency of this index.
         *
         * \return Constant accessor to an initialized vector.
         */
        template<std::size_t I>
        const GlobalVector& GetInitializedVector() const noexcept;

        /*!
         * \brief Non-constant access to a vector that was fully initialized (i.e. with layout also defined, not
         * just the constructor called).
         *
         * \tparam I Index of the requested vector (some tests require several so several are stored). There are
         * static check for the consistency of this index.
         *
         * \return Non-constant accessor to an initialized vector.
         */
        template<std::size_t I>
        GlobalVector& GetNonCstInitializedVector() noexcept;

        //! Get the scalar \a NumberingSubset used in the tests.
        const NumberingSubset& GetScalarNumberingSubset() const noexcept;

        //! Get the vectorial \a NumberingSubset used in the tests.
        const NumberingSubset& GetVectorialNumberingSubset() const noexcept;


      private:
        /*!
         * \brief Matrices used in the test that have been fully initialized (mpi layout is assigned).
         *
         * 0, 1 -> numbering subsets (1, 1)
         * 2, 3 -> numbering subsets (1, 2)
         * 4, 5 -> numbering subsets (2, 1)
         * 6, 7 -> numbering subsets (2, 2)
         */
        GlobalMatrix::array_unique_ptr<8> initialized_matrix_list_ =
            Utilities::NullptrArray<GlobalMatrix::unique_ptr, 8>();

        /*!
         * \brief Vectors used in the test that have been fully initialized (mpi layout is assigned).
         *
         * 0, 1 -> numbering subset 1
         * 2, 3 -> numbering subset 2
         */
        GlobalVector::array_unique_ptr<4> initialized_vector_list_ =
            Utilities::NullptrArray<GlobalVector::unique_ptr, 4>();
    };


} // namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
