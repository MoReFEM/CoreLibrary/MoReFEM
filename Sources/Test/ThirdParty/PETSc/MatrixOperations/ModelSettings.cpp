// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Test/ThirdParty/PETSc/MatrixOperations/ModelSettings.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
{


    void ModelSettings::Init()
    {
        // ****** Mesh ******
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ "Sole" });
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Path>(
            "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx1_Ny1_force_label.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Dimension>(2UL);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::SpaceUnit>(1.);

        // ****** Domain ******
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>>({ "Sole" });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::sole) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>::DimensionList>({ 2UL });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>::GeomEltTypeList>({});

        // ****** Unknown ******
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>>({ "scalar" });
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>::Name>("scalar");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>::Nature>("scalar");

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>>({ "vectorial" });
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>::Name>("vectorial");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>::Nature>("vectorial");

        // ****** Finite element space ******
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::sole));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::UnknownList>({ "scalar", "vectorial" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::sole));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::scalar), EnumUnderlyingType(NumberingSubsetIndex::vectorial) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::ShapeFunctionList>({ "P2", "P1" });

        // ****** Numbering subset ******
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>>({ "Scalar" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>>(
            { "Vectorial" });
    }


} // namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
