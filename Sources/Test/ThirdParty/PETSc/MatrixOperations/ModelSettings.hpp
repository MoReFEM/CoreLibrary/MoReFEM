// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODELSETTINGS_DOT_HPP_
#define MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODELSETTINGS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Tools/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
{


    //! Sole index used for this very simple model.
    enum class MeshIndex : std::size_t { sole = 1 };


    //! Sole index used for this very simple model.
    enum class UnknownIndex : std::size_t { scalar = 1, vectorial = 2 };


    //! Sole index used for this very simple model.
    enum class DomainIndex : std::size_t { sole = 1 };


    //! Sole index used for this very simple model.
    enum class FEltSpaceIndex : std::size_t { sole = 1 };

    //! Sole index used for this very simple model.
    enum class NumberingSubsetIndex : std::size_t { scalar = 1, vectorial = 2 };


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
        std::tuple
        <
            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::IndexedSectionDescription,
            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>::IndexedSectionDescription,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>::IndexedSectionDescription,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>::IndexedSectionDescription,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>::IndexedSectionDescription,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>::IndexedSectionDescription
        >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMDataForTest<ModelSettings, time_manager_type>;

} // namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODELSETTINGS_DOT_HPP_
// *** MoReFEM end header guards *** < //
