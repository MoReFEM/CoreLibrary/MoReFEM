// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <string_view>

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"

#include "Core/LinearAlgebra/Assertion/Assertion.hpp"

#include "Geometry/Coords/Coords.hpp"

#include "Test/Tools/Fixture/Enum.hpp"

#define BOOST_TEST_MODULE petsc_matrix_operations
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/LinearAlgebra/GlobalMatrixOpResult.hpp"
#include "Core/LinearAlgebra/Operations.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"
#include "Test/ThirdParty/PETSc/MatrixOperations/ModelSettings.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"

using namespace MoReFEM;


namespace // anonymous
{

    using time_manager_type = TestNS::PetscNS::MatrixOperationsNS::time_manager_type;


    //! Alias for mode type.
    using model_type = TestNS::PetscNS::MatrixOperationsNS::Model;


    //! Helper object to pass string information at compile time.
    struct OutputDirWrapper
    {
        //! The method that does the actual work.
        static constexpr std::string_view Path();
    };


    //! Alias for the fixture parent.
    // clang-format off
    using fixture_type = TestNS::FixtureNS::ModelNoInputData
    <
        model_type,
        OutputDirWrapper,
        time_manager_type,
        TestNS::FixtureNS::call_run_method_at_first_call::yes,
        create_domain_list_for_coords::yes
    >;
    // clang-format on


} // namespace

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(model_initialization, fixture_type)
{
    [[maybe_unused]] decltype(auto) model = GetModel();
}


BOOST_FIXTURE_TEST_CASE(mat_shift, fixture_type)
{
    decltype(auto) model = GetNonCstModel();
    GlobalLinearAlgebraNS::MatShift(1., model.GetNonCstInitializedMatrix<0>());
}


BOOST_FIXTURE_TEST_CASE(mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();
    decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
    decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();

    decltype(auto) initial_matrix = model.GetInitializedMatrix<2>();
    BOOST_REQUIRE_EQUAL(initial_matrix.GetRowNumberingSubset().GetUniqueId(), scalar_numbering_subset.GetUniqueId());
    BOOST_REQUIRE_EQUAL(initial_matrix.GetColNumberingSubset().GetUniqueId(), vectorial_numbering_subset.GetUniqueId());

    decltype(auto) v1 = model.GetInitializedVector<2>();
    decltype(auto) v2 = model.GetNonCstInitializedVector<1>();
    BOOST_REQUIRE_EQUAL(v1.GetNumberingSubset().GetUniqueId(), vectorial_numbering_subset.GetUniqueId());
    BOOST_REQUIRE_EQUAL(v2.GetNumberingSubset().GetUniqueId(), scalar_numbering_subset.GetUniqueId());

#ifndef NDEBUG
    {
        decltype(auto) wrong_vector = model.GetNonCstInitializedVector<3>();
        BOOST_REQUIRE_EQUAL(wrong_vector.GetNumberingSubset().GetUniqueId(), vectorial_numbering_subset.GetUniqueId());

        BOOST_CHECK_THROW(GlobalLinearAlgebraNS::MatMult(initial_matrix, v1, wrong_vector),
                          GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);
    }
#endif // NDEBUG
}


BOOST_FIXTURE_TEST_CASE(create_transpose_and_mat_mult, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
    decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();

    decltype(auto) initial_matrix = model.GetInitializedMatrix<2>();

    BOOST_REQUIRE_EQUAL(initial_matrix.GetRowNumberingSubset().GetUniqueId(), scalar_numbering_subset.GetUniqueId());
    BOOST_REQUIRE_EQUAL(initial_matrix.GetColNumberingSubset().GetUniqueId(), vectorial_numbering_subset.GetUniqueId());

    GlobalMatrixOpResult result(vectorial_numbering_subset, scalar_numbering_subset);
    GlobalLinearAlgebraNS::MatCreateTranspose(initial_matrix, result);

#ifndef NDEBUG
    {
        GlobalMatrixOpResult wrong_result(scalar_numbering_subset, scalar_numbering_subset);

        BOOST_CHECK_THROW(GlobalLinearAlgebraNS::MatCreateTranspose(initial_matrix, wrong_result),
                          GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);
    }
#endif // NDEBUG


    // Check the transpose matrix may be used in a matrix operation.
    decltype(auto) v1 = model.GetInitializedVector<0>();
    BOOST_REQUIRE_EQUAL(v1.GetNumberingSubset().GetUniqueId(), scalar_numbering_subset.GetUniqueId());

    decltype(auto) v2 = GetNonCstModel().GetNonCstInitializedVector<3>();
    BOOST_REQUIRE_EQUAL(v2.GetNumberingSubset().GetUniqueId(), vectorial_numbering_subset.GetUniqueId());

    GlobalLinearAlgebraNS::MatMult(result, v1, v2);
}


BOOST_FIXTURE_TEST_CASE(axpy, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();
    decltype(auto) other_matrix_ns_1_2 = model.GetNonCstInitializedMatrix<3>();
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId(),
                        other_matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId());
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId(),
                        other_matrix_ns_1_2.GetColNumberingSubset().GetUniqueId());

    Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., matrix_ns_1_2, other_matrix_ns_1_2);

    // Unmatching sizes for matrices is handled directly by PETSc; the wrapper just coat it in an Exception.
    decltype(auto) matrix_ns_2_2 = model.GetNonCstInitializedMatrix<6>();
    BOOST_CHECK_THROW(Wrappers::Petsc::AXPY<NonZeroPattern::same>(1., matrix_ns_1_2, matrix_ns_2_2),
                      Wrappers::Petsc::ExceptionNS::Exception);

#ifndef NDEBUG
    {
        // If using the GlobalLinearAlgebraNS version, it is trapped with the numbering subset comparison.
        BOOST_CHECK_THROW(GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., matrix_ns_1_2, matrix_ns_2_2),
                          GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);

        // No such check when using the other policies, so we fall back on the PETSc exception.
        BOOST_CHECK_THROW(GlobalLinearAlgebraNS::AXPY<NonZeroPattern::subset>(1., matrix_ns_1_2, matrix_ns_2_2),
                          Wrappers::Petsc::ExceptionNS::Exception);

        BOOST_CHECK_THROW(GlobalLinearAlgebraNS::AXPY<NonZeroPattern::different>(1., matrix_ns_1_2, matrix_ns_2_2),
                          Wrappers::Petsc::ExceptionNS::Exception);
    }
#endif // NDEBUG
}


BOOST_FIXTURE_TEST_CASE(mat_mult_add, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();

    decltype(auto) vector_ns_1 = model.GetInitializedVector<0>();
    decltype(auto) other_vector_ns_1 = model.GetNonCstInitializedVector<1>();
    decltype(auto) vector_ns_2 = model.GetInitializedVector<2>();

    BOOST_REQUIRE_EQUAL(vector_ns_1.GetNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(other_vector_ns_1.GetNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vector_ns_2.GetNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);

    GlobalLinearAlgebraNS::MatMultAdd(matrix_ns_1_2, vector_ns_2, vector_ns_1, other_vector_ns_1);

#ifndef NDEBUG
    // We check here one possible mismatch among many others
    BOOST_CHECK_THROW(GlobalLinearAlgebraNS::MatMultAdd(matrix_ns_1_2, vector_ns_2, vector_ns_2, other_vector_ns_1),
                      GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);
#endif // NDEBUG
}


BOOST_FIXTURE_TEST_CASE(mat_mult_transpose, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) matrix_ns_2_1 = model.GetInitializedMatrix<4>();
    decltype(auto) vector_ns_1 = model.GetNonCstInitializedVector<0>();
    decltype(auto) vector_ns_2 = model.GetInitializedVector<2>();

    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetRowNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetColNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vector_ns_1.GetNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vector_ns_2.GetNumberingSubset().GetUniqueId().Get(), 2UL);

    GlobalLinearAlgebraNS::MatMultTranspose(matrix_ns_2_1, vector_ns_2, vector_ns_1);

#ifndef NDEBUG
    {
        decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();
        BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
        BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);


        // We check here one possible mismatch among many others
        BOOST_CHECK_THROW(GlobalLinearAlgebraNS::MatMultTranspose(matrix_ns_1_2, vector_ns_2, vector_ns_1),
                          GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);
    }
#endif // NDEBUG
}


BOOST_FIXTURE_TEST_CASE(mat_mult_transpose_add, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) matrix_ns_2_1 = model.GetInitializedMatrix<4>();
    decltype(auto) vector_ns_1 = model.GetNonCstInitializedVector<0>();
    decltype(auto) other_vector_ns_1 = model.GetInitializedVector<1>();
    decltype(auto) vector_ns_2 = model.GetInitializedVector<2>();

    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetRowNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetColNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vector_ns_1.GetNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(other_vector_ns_1.GetNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vector_ns_2.GetNumberingSubset().GetUniqueId().Get(), 2UL);

    GlobalLinearAlgebraNS::MatMultTransposeAdd(matrix_ns_2_1, vector_ns_2, other_vector_ns_1, vector_ns_1);

#ifndef NDEBUG
    {
        decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();
        BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
        BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);

        // We check here one possible mismatch among many others
        BOOST_CHECK_THROW(
            GlobalLinearAlgebraNS::MatMultTransposeAdd(matrix_ns_2_1, vector_ns_2, vector_ns_2, vector_ns_1),
            GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);
    }
#endif // NDEBUG
}


BOOST_FIXTURE_TEST_CASE(mat_mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
    decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();
    BOOST_REQUIRE_EQUAL(scalar_numbering_subset.GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vectorial_numbering_subset.GetUniqueId().Get(), 2UL);

    decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();
    decltype(auto) matrix_ns_2_1 = model.GetInitializedMatrix<4>();

    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetRowNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetColNumberingSubset().GetUniqueId().Get(), 1UL);

    GlobalMatrixOpResult matrix_product_result(scalar_numbering_subset, scalar_numbering_subset);

    GlobalLinearAlgebraNS::MatMatMult(matrix_ns_1_2, matrix_ns_2_1, matrix_product_result);

    // Check second calls works fine!
    GlobalLinearAlgebraNS::MatMatMult(matrix_ns_1_2, matrix_ns_2_1, matrix_product_result);
}

#ifndef NDEBUG
BOOST_FIXTURE_TEST_CASE(mat_mat_mult_wrong_ns, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
    decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();
    BOOST_REQUIRE_EQUAL(scalar_numbering_subset.GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vectorial_numbering_subset.GetUniqueId().Get(), 2UL);

    decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();
    decltype(auto) matrix_ns_2_1 = model.GetInitializedMatrix<4>();

    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetRowNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetColNumberingSubset().GetUniqueId().Get(), 1UL);

    GlobalMatrixOpResult wrong_ns(scalar_numbering_subset, vectorial_numbering_subset);

    BOOST_CHECK_THROW(GlobalLinearAlgebraNS::MatMatMult(matrix_ns_1_2, matrix_ns_2_1, wrong_ns),
                      GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);

    decltype(auto) matrix_ns_1_1 = model.GetInitializedMatrix<0>();

    BOOST_CHECK_THROW(GlobalLinearAlgebraNS::MatMatMult(matrix_ns_1_2, matrix_ns_1_1, wrong_ns),
                      GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubsetInMatrixProduct);
}
#endif // NDEBUG

BOOST_FIXTURE_TEST_CASE(mat_transpose_mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
    decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();
    BOOST_REQUIRE_EQUAL(scalar_numbering_subset.GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vectorial_numbering_subset.GetUniqueId().Get(), 2UL);

    decltype(auto) matrix_ns_2_1 = model.GetInitializedMatrix<4>();
    decltype(auto) other_matrix_ns_1_2 = model.GetInitializedMatrix<5>();

    BOOST_REQUIRE_EQUAL(other_matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(other_matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetRowNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetColNumberingSubset().GetUniqueId().Get(), 1UL);

    GlobalMatrixOpResult matrix_product_result(scalar_numbering_subset, scalar_numbering_subset);

    GlobalLinearAlgebraNS::MatTransposeMatMult(other_matrix_ns_1_2, matrix_ns_2_1, matrix_product_result);

    // Check second calls works fine!
    GlobalLinearAlgebraNS::MatTransposeMatMult(other_matrix_ns_1_2, matrix_ns_2_1, matrix_product_result);
}


BOOST_FIXTURE_TEST_CASE(mat_mat_transpose_mat_mult, fixture_type)
{
    if (GetMpi().Nprocessor<int>() == 1)
    {
        decltype(auto) model = GetNonCstModel();

        decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
        decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();
        BOOST_REQUIRE_EQUAL(scalar_numbering_subset.GetUniqueId().Get(), 1UL);
        BOOST_REQUIRE_EQUAL(vectorial_numbering_subset.GetUniqueId().Get(), 2UL);

        decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();
        decltype(auto) other_matrix_ns_1_2 = model.GetInitializedMatrix<3>();

        BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
        BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);
        BOOST_REQUIRE_EQUAL(other_matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
        BOOST_REQUIRE_EQUAL(other_matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);

        GlobalMatrixOpResult matrix_product_result(scalar_numbering_subset, scalar_numbering_subset);

        GlobalLinearAlgebraNS::MatMatTransposeMult(matrix_ns_1_2, other_matrix_ns_1_2, matrix_product_result);

        // Check second calls works fine!
        GlobalLinearAlgebraNS::MatMatTransposeMult(matrix_ns_1_2, other_matrix_ns_1_2, matrix_product_result);
    }
}


BOOST_FIXTURE_TEST_CASE(mat_mat_mat_mult, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
    decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();
    BOOST_REQUIRE_EQUAL(scalar_numbering_subset.GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vectorial_numbering_subset.GetUniqueId().Get(), 2UL);

    decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();
    decltype(auto) other_matrix_ns_1_2 = model.GetInitializedMatrix<3>();
    decltype(auto) matrix_ns_2_1 = model.GetInitializedMatrix<4>();

    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetRowNumberingSubset().GetUniqueId().Get(), 2UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_2_1.GetColNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(other_matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(other_matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);


    GlobalMatrixOpResult matrix_product_result(scalar_numbering_subset, vectorial_numbering_subset);

    GlobalLinearAlgebraNS::MatMatMatMult(matrix_ns_1_2, matrix_ns_2_1, other_matrix_ns_1_2, matrix_product_result);

    // Check second calls works fine!
    GlobalLinearAlgebraNS::MatMatMatMult(matrix_ns_1_2, matrix_ns_2_1, other_matrix_ns_1_2, matrix_product_result);
}


BOOST_FIXTURE_TEST_CASE(pt_a_p, fixture_type)
{
    decltype(auto) model = GetNonCstModel();

    decltype(auto) scalar_numbering_subset = model.GetScalarNumberingSubset();
    decltype(auto) vectorial_numbering_subset = model.GetVectorialNumberingSubset();
    BOOST_REQUIRE_EQUAL(scalar_numbering_subset.GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(vectorial_numbering_subset.GetUniqueId().Get(), 2UL);

    decltype(auto) matrix_ns_1_1 = model.GetInitializedMatrix<0>();
    decltype(auto) matrix_ns_1_2 = model.GetInitializedMatrix<2>();

    BOOST_REQUIRE_EQUAL(matrix_ns_1_1.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_1.GetColNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetRowNumberingSubset().GetUniqueId().Get(), 1UL);
    BOOST_REQUIRE_EQUAL(matrix_ns_1_2.GetColNumberingSubset().GetUniqueId().Get(), 2UL);

    GlobalMatrixOpResult matrix_product_result(vectorial_numbering_subset, vectorial_numbering_subset);

    GlobalLinearAlgebraNS::PtAP(matrix_ns_1_1, matrix_ns_1_2, matrix_product_result);

    // Check second calls works fine!
    GlobalLinearAlgebraNS::PtAP(matrix_ns_1_1, matrix_ns_1_2, matrix_product_result);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    constexpr std::string_view OutputDirWrapper::Path()
    {
        return "ThirdParty/PETSc/MatrixOperations";
    }


} // namespace
