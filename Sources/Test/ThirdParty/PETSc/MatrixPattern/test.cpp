// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <utility>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#define BOOST_TEST_MODULE matrix_pattern
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    std::vector<std::vector<PetscInt>> MatrixContent()
    {
        static const std::vector<std::vector<PetscInt>> ret{
            { 10, 20, 0, 0, 0, 0 }, { 0, 30, 0, 40, 0, 0 }, { 0, 0, 50, 60, 70, 0 }, { 0, 0, 0, 0, 0, 80 }
        };

        return ret;
    };


    std::vector<PetscInt> iCSR()
    {
        static const std::vector<PetscInt> ret{ 0, 2, 4, 7, 8 };

        return ret;
    }


    std::vector<PetscInt> jCSR()
    {
        static const std::vector<PetscInt> ret{ 0, 1, 1, 3, 2, 3, 4, 5 };

        return ret;
    }


    template<class T>
    std::vector<std::vector<PetscInt>> NnonZeroPerRow(const std::vector<std::vector<T>>& matrix);


} // namespace


BOOST_AUTO_TEST_CASE(check_csr)
{
    const Wrappers::Petsc::MatrixPattern pattern(NnonZeroPerRow(MatrixContent()));

    BOOST_CHECK_EQUAL(pattern.GetICsr().size(), iCSR().size());
    BOOST_CHECK(pattern.GetICsr() == iCSR());

    BOOST_CHECK_EQUAL(pattern.GetJCsr().size(), jCSR().size());
    BOOST_CHECK(pattern.GetJCsr() == jCSR());
}


BOOST_AUTO_TEST_CASE(non_zero_position_per_row)
{
    const Wrappers::Petsc::MatrixPattern pattern(NnonZeroPerRow(MatrixContent()));

    const auto non_zero_position_per_row = Wrappers::Petsc::ExtractNonZeroPositionsPerRow(pattern);

    BOOST_CHECK_EQUAL(static_cast<PetscInt>(non_zero_position_per_row.size()), pattern.Nrow().Get());
    BOOST_CHECK_EQUAL(non_zero_position_per_row.size(), 4UL);

    BOOST_CHECK(non_zero_position_per_row[0] == std::vector<PetscInt>({ 0, 1 }));
    BOOST_CHECK(non_zero_position_per_row[1] == std::vector<PetscInt>({ 1, 3 }));
    BOOST_CHECK(non_zero_position_per_row[2] == std::vector<PetscInt>({ 2, 3, 4 }));
    BOOST_CHECK(non_zero_position_per_row[3] == std::vector<PetscInt>({ 5 }));
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    template<class T>
    std::vector<std::vector<PetscInt>> NnonZeroPerRow(const std::vector<std::vector<T>>& matrix)
    {
        std::vector<std::vector<PetscInt>> ret;
        ret.reserve(matrix.size());
        assert(!matrix.empty());
        const auto Ncol = matrix.back().size();

        for (const auto& row : matrix)
        {
            assert(row.size() == Ncol);

            std::vector<PetscInt> non_zero_position;

            for (auto col_index = 0UL; col_index < Ncol; ++col_index)
            {
                if (!NumericNS::IsZero(row[col_index]))
                    non_zero_position.push_back(static_cast<PetscInt>(col_index));
            }

            ret.emplace_back(std::move(non_zero_position));
        }

        return ret;
    }


} // namespace
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
