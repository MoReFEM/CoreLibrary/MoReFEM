// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <tuple>
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"

#define BOOST_TEST_MODULE solver_parallel_not_supported
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    using input_data_tuple_type = std::tuple<InputDataNS::Petsc<10>>;


    using input_data_type = InputData<input_data_tuple_type>;


} // namespace


#ifdef MOREFEM_WITH_UMFPACK

BOOST_FIXTURE_TEST_CASE(NonParallelUmfpack, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor<int>() > 1);

    Internal::Wrappers::Petsc::SolverNS::Settings settings(Wrappers::Petsc::solver_name_type{ "Umfpack" });

    // NOLINTBEGIN(bugprone-use-after-move,hicpp-invalid-access-moved)
    BOOST_CHECK_THROW(
        Wrappers::Petsc::Snes(mpi,
                              std::move(settings),
                              nullptr, // current test isn't about the functions provided for non linear use
                              nullptr,
                              nullptr,
                              nullptr),
        MoReFEM::Wrappers::Petsc::ExceptionNS::SolverNotParallel);
    // NOLINTEND(bugprone-use-after-move,hicpp-invalid-access-moved)
}

#endif // MOREFEM_WITH_UMFPACK


#ifdef MOREFEM_WITH_SUPERLU_DIST

BOOST_FIXTURE_TEST_CASE(ParallelSuperLU_dist, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor<int>() > 1);

    Internal::Wrappers::Petsc::SolverNS::Settings settings(Wrappers::Petsc::solver_name_type{ "SuperLU_dist" });

    Wrappers::Petsc::Snes(mpi,
                          std::move(settings),
                          nullptr, // current test isn't about the functions provided for non linear use
                          nullptr,
                          nullptr,
                          nullptr);
}

#endif // MOREFEM_WITH_SUPERLU_DIST

#ifdef MOREFEM_WITH_MUMPS

BOOST_FIXTURE_TEST_CASE(ParallelMumps, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor<int>() > 1);

    Internal::Wrappers::Petsc::SolverNS::Settings settings(Wrappers::Petsc::solver_name_type{ "Mumps" });

    Wrappers::Petsc::Snes(mpi,
                          std::move(settings),
                          nullptr, // current test isn't about the functions provided for non linear use
                          nullptr,
                          nullptr,
                          nullptr);
}

#endif // MOREFEM_WITH_MUMPS


BOOST_FIXTURE_TEST_CASE(NonParallelPetsc, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor<int>() > 1);

    Internal::Wrappers::Petsc::SolverNS::Settings settings(Wrappers::Petsc::solver_name_type{ "Petsc" });

    // NOLINTBEGIN(bugprone-use-after-move,hicpp-invalid-access-moved)
    BOOST_CHECK_THROW(
        Wrappers::Petsc::Snes(mpi,
                              std::move(settings),
                              nullptr, // current test isn't about the functions provided for non linear use
                              nullptr,
                              nullptr,
                              nullptr),
        MoReFEM::Wrappers::Petsc::ExceptionNS::SolverNotParallel);
    // NOLINTEND(bugprone-use-after-move,hicpp-invalid-access-moved)
}


BOOST_FIXTURE_TEST_CASE(ParallelGmres, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    BOOST_REQUIRE(mpi.Nprocessor<int>() > 1);

    // Gmres requires a non default value for restart.
    Internal::Wrappers::Petsc::SolverNS::Settings settings(Wrappers::Petsc::absolute_tolerance_type{ 1.e-8 },
                                                           Wrappers::Petsc::relative_tolerance_type{ 1.e-8 },
                                                           Wrappers::Petsc::set_restart_type{ 1 },
                                                           Wrappers::Petsc::max_iteration_type{ 1000 },
                                                           Wrappers::Petsc::preconditioner_name_type{ PCLU },
                                                           Wrappers::Petsc::solver_name_type{ "Gmres" },
                                                           Wrappers::Petsc::step_size_tolerance_type{ 1.e-8 });

    // NOLINTBEGIN(bugprone-use-after-move,hicpp-invalid-access-moved)
    Wrappers::Petsc::Snes(mpi,
                          std::move(settings),
                          nullptr, // current test isn't about the functions provided for non linear use
                          nullptr,
                          nullptr,
                          nullptr);
    // NOLINTEND(bugprone-use-after-move,hicpp-invalid-access-moved)
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
