// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <utility>

#define BOOST_TEST_MODULE snes
#include "Utilities/Exceptions/Factory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace MoReFEM::TestNS
{


    struct Snes
    {
        Snes() = default;

        static SNES GetPetscSnes(Wrappers::Petsc::Snes& snes)
        {
            return snes.Internal();
        }
    };


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{

    constexpr auto epsilon = 1.e-12;


} // namespace


BOOST_FIXTURE_TEST_CASE(petsc_default_behaviour, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    // Read first values put by default in a newly created SNES object by PETSc
    PetscInt default_max_it = 0;

    // NOLINTBEGIN(cppcoreguidelines-init-variables)
    PetscReal default_absolute_tol;
    PetscReal default_relative_tol;
    PetscReal default_step_tol;
    // NOLINTEND(cppcoreguidelines-init-variables)

    {
        SNES snes = nullptr;

        int error_code = SNESCreate(mpi.GetCommunicator(), &snes);
        BOOST_REQUIRE(!error_code);

        error_code = SNESGetTolerances(
            snes, &default_absolute_tol, &default_relative_tol, &default_step_tol, &default_max_it, MOREFEM_PETSC_NULL);
        BOOST_REQUIRE(!error_code);
    }

    // Then create a MoReFEM object with the SolverNS::Settings which uses up mostly default values
    PetscInt morefem_max_it = 0;

    // NOLINTBEGIN(cppcoreguidelines-init-variables)
    PetscReal morefem_absolute_tol;
    PetscReal morefem_relative_tol;
    PetscReal morefem_step_tol;
    // NOLINTEND(cppcoreguidelines-init-variables)

    {
        Internal::Wrappers::Petsc::SolverNS::Settings solver_settings_with_petsc_default(
            Wrappers::Petsc::solver_name_type{ "SuperLU_dist" });

        auto morefem_snes =
            Wrappers::Petsc::Snes(mpi,
                                  std::move(solver_settings_with_petsc_default),
                                  nullptr, // current test isn't about the functions provided for non linear use
                                  nullptr,
                                  nullptr,
                                  nullptr);

        auto* snes_from_morefem = MoReFEM::TestNS::Snes::GetPetscSnes(morefem_snes);


        const int error_code = SNESGetTolerances(snes_from_morefem,
                                                 &morefem_absolute_tol,
                                                 &morefem_relative_tol,
                                                 &morefem_step_tol,
                                                 &morefem_max_it,
                                                 MOREFEM_PETSC_NULL);
        BOOST_REQUIRE(!error_code);
    }

    // And check the default are really well preserved.
    BOOST_CHECK_CLOSE(default_absolute_tol, morefem_absolute_tol, epsilon);
    BOOST_CHECK_CLOSE(default_relative_tol, morefem_relative_tol, epsilon);
    BOOST_CHECK_CLOSE(default_step_tol, morefem_step_tol, epsilon);
    BOOST_CHECK_EQUAL(default_max_it, morefem_max_it);
}


BOOST_FIXTURE_TEST_CASE(invalid_solver_name, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    Internal::Wrappers::Petsc::SolverNS::Settings solver_settings_with_petsc_default(
        Wrappers::Petsc::solver_name_type{ "Whatever" });

    BOOST_CHECK_THROW(auto morefem_snes = Wrappers::Petsc::Snes(
                          mpi,
                          // NOLINTNEXTLINE(bugprone-use-after-move,hicpp-invalid-access-moved) - I don't see it here?!?
                          std::move(solver_settings_with_petsc_default),
                          nullptr, // current test isn't about the functions provided for non linear use
                          nullptr,
                          nullptr,
                          nullptr),
                      ExceptionNS::Factory::UnregisteredName);
}


BOOST_FIXTURE_TEST_CASE(solver_not_supporting_parallel, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    Internal::Wrappers::Petsc::SolverNS::Settings solver_settings_with_petsc_default(
        Wrappers::Petsc::solver_name_type{ "Whatever" });

    BOOST_CHECK_THROW(auto morefem_snes = Wrappers::Petsc::Snes(
                          mpi,
                          // NOLINTNEXTLINE(bugprone-use-after-move,hicpp-invalid-access-moved) - I don't see it here?!?
                          std::move(solver_settings_with_petsc_default),
                          nullptr, // current test isn't about the functions provided for non linear use
                          nullptr,
                          nullptr,
                          nullptr),
                      ExceptionNS::Factory::UnregisteredName);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
