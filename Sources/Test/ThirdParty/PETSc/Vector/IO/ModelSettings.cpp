// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/ThirdParty/PETSc/Vector/IO/InputData.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>({ "Sole" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ "Sole" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::sole)>>({ "Sole" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>>({ "Sole" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>({ "Sole" });
    }


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
