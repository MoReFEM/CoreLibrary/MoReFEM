// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cmath>
#include <memory>
#include <sstream>
#include <string>

#include "Test/ThirdParty/PETSc/Vector/IO/ToyModel.hpp"

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Mpi/Mpi.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/InputData/Instances/Result.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Internal/CreateMeshDataDirectory.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"

#include "Model/Internal/InitializeHelper.hpp"

#include "Test/ThirdParty/PETSc/Vector/IO/InputData.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    namespace // anonymous
    {


        std::string ComputeProcessorWiseAsciiFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/vector_" << model.GetMpi().GetRank<int>() << ".hhdata";
            return oconv.str();
        }


        std::string ComputeProcessorWiseBinaryFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/vector_" << model.GetMpi().GetRank<int>() << ".bin";
            return oconv.str();
        }


        std::string ComputeProgramWiseAsciiFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/program_wise_vector.m";
            std::string ret = oconv.str();

            oconv.str("");
            oconv << "Rank_" << model.GetMpi().GetRank<int>();
            Utilities::String::Replace(oconv.str(), "Rank_0", ret);
            return ret;
        }


        std::string ComputeProgramWiseBinaryFile(const ToyModel& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/program_wise_vector.bin";
            std::string ret = oconv.str();

            //            oconv.str("");
            //            oconv << "Rank_" << model.GetMpi().GetRank<int>();
            //            Utilities::String::Replace(oconv.str(), "Rank_0", ret);
            return ret;
        }


        FilesystemNS::Directory ExtractResultDirectory(const morefem_data_type& morefem_data)
        {
            const std::string path =
                ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Result::OutputDirectory>(morefem_data);

            FilesystemNS::Directory ret(morefem_data.GetMpi(), path, FilesystemNS::behaviour::overwrite);

            ret.ActOnFilesystem();

            return ret;
        }


    } // namespace


    ToyModel::ToyModel(morefem_data_type& morefem_data)
    : Crtp::CrtpMpi<ToyModel>(morefem_data.GetMpi()), output_directory_(ExtractResultDirectory(morefem_data))
    {
        decltype(auto) mpi = GetMpi();

        decltype(auto) model_settings = GetNonCstModelSettings();

        model_settings.Init();

        Internal::ModelNS::InitMostSingletonManager(morefem_data);

        [[maybe_unused]] auto init =
            Internal::GodOfDofNS::InitAllGodOfDof(morefem_data,
                                                  model_settings,
                                                  DoConsiderProcessorWiseLocal2Global::no,
                                                  Internal::MeshNS::CreateMeshDataDirectory(GetOutputDirectory()));

        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::sole));

        decltype(auto) numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::sole));

        vector_ = std::make_unique<GlobalVector>(numbering_subset);
        auto& vector = *vector_;

        AllocateGlobalVector(god_of_dof, vector);

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector);


            const auto size = content.GetSize();
            const auto rank_plus_one = static_cast<double>(1 + mpi.GetRank<int>());

            for (auto i = vector_processor_wise_index_type{ 0UL }; i < size; ++i)
                content[i] = rank_plus_one * std::sqrt(i.Get()); // completely arbitrary value with no redundancy!
        }

        {
            const auto binary_file = FilesystemNS::File{ GetProcessorWiseBinaryFile() };

            if (binary_file.DoExist())
                binary_file.Remove();
        }

        {
            const auto ascii_file = FilesystemNS::File{ GetProcessorWiseAsciiFile() };

            if (ascii_file.DoExist())
                ascii_file.Remove();
        }

        if (mpi.IsRootProcessor())
        {
            {
                const auto binary_file = FilesystemNS::File{ GetProgramWiseBinaryFile() };

                if (binary_file.DoExist())
                    binary_file.Remove();
            }

            {
                const auto ascii_file = FilesystemNS::File{ GetProgramWiseAsciiFile() };

                if (ascii_file.DoExist())
                    ascii_file.Remove();
            }
        }
    }


    const std::string& ToyModel::GetProcessorWiseBinaryFile() const
    {
        static auto ret = ComputeProcessorWiseBinaryFile(*this);
        return ret;
    }


    const std::string& ToyModel::GetProcessorWiseAsciiFile() const
    {
        static auto ret = ComputeProcessorWiseAsciiFile(*this);
        return ret;
    }


    const std::string& ToyModel::GetProgramWiseBinaryFile() const
    {
        static auto ret = ComputeProgramWiseBinaryFile(*this);
        return ret;
    }


    const std::string& ToyModel::GetProgramWiseAsciiFile() const
    {
        static auto ret = ComputeProgramWiseAsciiFile(*this);
        return ret;
    }


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
