// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_THIRDPARTY_PETSC_VECTOR_IO_TOYMODEL_DOT_HPP_
#define MOREFEM_TEST_THIRDPARTY_PETSC_VECTOR_IO_TOYMODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Test/ThirdParty/PETSc/Vector/IO/InputData.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    /*!
     * \brief Toy model used to perform tests about I/O operations related to \a GlobalVector.
     *
     * This toy model role is to:
     * - Initialize the data (including partitioning among the different ranks in the case of parallel run)
     * - Create and store a \a GlobalVector onto which different I/O operations will be performed.
     * - Set filenames for output files and ensure these files don't exist at first - they will be created by the
     * tests themselves.
     */
    class ToyModel : public Crtp::CrtpMpi<ToyModel>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = ToyModel;

      public:
        //! Define as a trait - I need that to use it with \a TestNS::FixtureNS::Model .
        using morefem_data_type = ::MoReFEM::TestNS::PetscNS::VectorIONS::morefem_data_type;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>;

        //! \copydoc doxygen_hide_model_specific_model_settings
        using model_settings_type = ModelSettings;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        ToyModel(morefem_data_type& morefem_data);

        //! Destructor.
        ~ToyModel() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ToyModel(const ToyModel& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ToyModel(ToyModel&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ToyModel& operator=(const ToyModel& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ToyModel& operator=(ToyModel&& rhs) = delete;

        ///@}

        //! Returns the path to result directory.
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

        //! Accessor to the vector used for tests.
        const GlobalVector& GetVector() const noexcept;

        //! Accessor to the path to the binary file for current rank.
        const std::string& GetProcessorWiseBinaryFile() const;

        //! Accessor to the path to the ascii file for current rank.
        const std::string& GetProcessorWiseAsciiFile() const;

        //! Accessor to the path to the program-wise binary file.
        const std::string& GetProgramWiseBinaryFile() const;

        //! Accessor to the path to the program-wise ascii file.
        const std::string& GetProgramWiseAsciiFile() const;

        //! \copydoc doxygen_hide_model_settings_non_constant_accessor
        ModelSettings& GetNonCstModelSettings() noexcept;

      private:
        //! Global vector which is used in the tests.
        GlobalVector::unique_ptr vector_ = nullptr;

        //! Path to result directory.
        const FilesystemNS::Directory output_directory_;

        //! \copydoc doxygen_hide_model_settings_attribute
        ModelSettings model_settings_;
    };


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/ThirdParty/PETSc/Vector/IO/ToyModel.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_THIRDPARTY_PETSC_VECTOR_IO_TOYMODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
