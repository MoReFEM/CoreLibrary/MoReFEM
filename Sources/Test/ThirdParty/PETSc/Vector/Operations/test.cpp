// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <source_location>
#include <vector>

#include "Core/LinearAlgebra/Assertion/Assertion.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#define BOOST_TEST_MODULE petsc_vector_operations

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Core/LinearAlgebra/Operations.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/NumberingSubsetCreator.hpp"

using namespace MoReFEM;


// NOLINTBEGIN(misc-non-private-member-variables-in-classes)
namespace // anonymous
{


    struct Fixture : public MoReFEM::TestNS::FixtureNS::TestEnvironment
    {


        Fixture();

        GlobalVector::unique_ptr v1_ns1;
        GlobalVector::unique_ptr v2_ns1;
        GlobalVector::unique_ptr v1_ns2;
    };

    constexpr double epsilon = 1.e-8;


} // namespace
// NOLINTEND(misc-non-private-member-variables-in-classes)


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(axpy, Fixture)
{
    const GlobalVector v2_initial{ *v2_ns1 };

    GlobalLinearAlgebraNS::AXPY(2., *v1_ns1, *v2_ns1, std::source_location::current());

    {
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> v2_initial_content(v2_initial);
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> v1_content(*v1_ns1);
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> v2_content(*v2_ns1);

        const auto local_size = v2_content.GetSize();

        BOOST_REQUIRE_EQUAL(local_size, v1_content.GetSize());
        BOOST_REQUIRE_EQUAL(local_size, v2_initial_content.GetSize());

        for (auto index = vector_processor_wise_index_type{ 0UL }; index < local_size; ++index)
        {
            BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                  (v2_content.GetValue(index))(2. * v1_content.GetValue(index)
                                                               + v2_initial_content.GetValue(index))(epsilon));
        }
    }

#ifndef NDEBUG

    BOOST_CHECK_THROW(GlobalLinearAlgebraNS::AXPY(1., *v1_ns1, *v1_ns2, std::source_location::current()),
                      GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);

#endif // NDEBUG
}


BOOST_FIXTURE_TEST_CASE(dot_product, Fixture)
{
    BOOST_CHECK_PREDICATE(
        NumericNS::AreEqual<double>,
        (GlobalLinearAlgebraNS::DotProduct(*v1_ns1, *v2_ns1, std::source_location::current()))(18.)(epsilon));

#ifndef NDEBUG
    BOOST_CHECK_THROW(GlobalLinearAlgebraNS::DotProduct(*v1_ns1, *v1_ns2, std::source_location::current()),
                      GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);
#endif // NDEBUG
}


namespace // anonymous
{

    Fixture::Fixture()
    {
        decltype(auto) mpi = GetMpi();

        static bool first_call = true;

        if (first_call)
        {
            MoReFEM::TestNS::NumberingSubsetCreator::Create(::MoReFEM::NumberingSubsetNS::unique_id{ 1UL });
            MoReFEM::TestNS::NumberingSubsetCreator::Create(::MoReFEM::NumberingSubsetNS::unique_id{ 2UL });
            first_call = false;
        }

        decltype(auto) ns_manager = Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance();

        decltype(auto) ns1 = ns_manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 1UL });
        decltype(auto) ns2 = ns_manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 2UL });


        v1_ns1 = std::make_unique<GlobalVector>(ns1, "v1_ns1");
        v2_ns1 = std::make_unique<GlobalVector>(ns1, "v2_ns1");
        v1_ns2 = std::make_unique<GlobalVector>(ns2, "v1_ns2");


        if (mpi.Nprocessor<int>() == 1)
        {
            const auto ns1_local_size = vector_processor_wise_index_type{ 10UL };
            const auto ns2_local_size = vector_processor_wise_index_type{ 6UL };

            v1_ns1->InitSequentialVector(mpi, ns1_local_size);
            v2_ns1->InitSequentialVector(mpi, ns1_local_size);
            v1_ns2->InitSequentialVector(mpi, ns2_local_size);
        } else
        {
            // Arbitrary repartition between the four ranks (but same vector as in sequential case!)
            const auto ns1_local_size = vector_processor_wise_index_type{ 1 + mpi.GetRank<PetscInt>() };
            const auto ns1_global_size = vector_program_wise_index_type{ 10 }; // 4 ranks and local size above

            const auto ns2_local_size = vector_processor_wise_index_type{ mpi.GetRank<PetscInt>() };
            const auto ns2_global_size = vector_program_wise_index_type{ 6 }; // 4 ranks and local size above

            v1_ns1->InitMpiVector(mpi, ns1_local_size, ns1_global_size);
            v2_ns1->InitMpiVector(mpi, ns1_local_size, ns1_global_size);
            v1_ns2->InitMpiVector(mpi, ns2_local_size, ns2_global_size);
        }


        const std::vector<PetscInt> index_list_v1{ 0, 2, 4, 6 };
        std::vector<PetscScalar> values_v1{ 2., 3., 5., 7. };

        const std::vector<PetscInt> index_list_v2{ 0, 1, 2, 8, 9 };
        std::vector<PetscScalar> values_v2{ 3., 1., 4., 5. };

        v1_ns1->SetValues(index_list_v1, values_v1.data(), INSERT_VALUES);
        v2_ns1->SetValues(index_list_v2, values_v2.data(), INSERT_VALUES);

        v1_ns1->Assembly();
        v2_ns1->Assembly();
    }


} // namespace


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
