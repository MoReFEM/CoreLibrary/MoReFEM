// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <numeric>
#include <source_location>
#include <vector>

#define BOOST_TEST_MODULE petsc_vector_set_and_access_values

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(misc-non-private-member-variables-in-classes)
namespace // anonymous
{

    constexpr auto epsilon = NumericNS::DefaultEpsilon<PetscScalar>();


    struct Fixture : public MoReFEM::TestNS::FixtureNS::TestEnvironment
    {


        Fixture();

        Wrappers::Petsc::Vector vector;

        const vector_processor_wise_index_type local_size{ 2 };
        const vector_program_wise_index_type global_size{ local_size.Get()
                                                          * static_cast<PetscInt>(GetMpi().Nprocessor().Get()) };

        static PetscScalar ComputeValueForIndex(PetscInt index);
    };


} // namespace
// NOLINTEND(misc-non-private-member-variables-in-classes)

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(size_accessors, Fixture)
{
    BOOST_CHECK_EQUAL(vector.GetProcessorWiseSize(), local_size);
    BOOST_CHECK_EQUAL(vector.GetProgramWiseSize(), global_size);
}


BOOST_FIXTURE_TEST_CASE(get_value, Fixture)
{
    // Check only the processor-wise values for each rank - or PETSc will yell!
    const auto begin_index =
        vector_program_wise_index_type{ GetMpi().GetRank<PetscInt>() * static_cast<PetscInt>(local_size.Get()) };
    const auto end_index = begin_index + vector_program_wise_index_type{ static_cast<PetscInt>(local_size.Get()) };

    for (auto index = begin_index; index < end_index; ++index)
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                              (vector.GetValue(index))(ComputeValueForIndex(index.Get()))(epsilon));
}


BOOST_FIXTURE_TEST_CASE(get_value_outside_rank, Fixture)
{
    // This test should only fail in parallel; no need to check all combinations.
    if (GetMpi().GetRank<int>() > 0)
    {
        BOOST_CHECK_THROW(vector.GetValue(vector_program_wise_index_type{}), Wrappers::Petsc::ExceptionNS::Exception);
    }
}


BOOST_FIXTURE_TEST_CASE(get_values, Fixture)
{
    const auto begin_index = vector_program_wise_index_type{ GetMpi().GetRank<PetscInt>() * local_size.Get() };

    std::vector<PetscInt> index_list(static_cast<std::size_t>(local_size.Get()));

    std::iota(index_list.begin(), index_list.end(), begin_index.Get());

    const auto local_values = vector.GetValues(index_list);

    BOOST_CHECK_EQUAL(static_cast<PetscInt>(local_values.size()), local_size.Get());

    for (auto index = vector_program_wise_index_type{}; index < vector_program_wise_index_type{ local_size.Get() };
         ++index)
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                              (local_values[static_cast<std::size_t>(index.Get())])(
                                  ComputeValueForIndex(begin_index.Get() + index.Get()))(epsilon));
}


BOOST_FIXTURE_TEST_CASE(get_values_output_argument, Fixture)
{
    const auto begin_index = vector_program_wise_index_type{ GetMpi().GetRank<PetscInt>() * local_size.Get() };

    std::vector<PetscInt> index_list(static_cast<std::size_t>(local_size.Get()));

    std::iota(index_list.begin(), index_list.end(), begin_index.Get());

    std::vector<PetscScalar> value_list(static_cast<std::size_t>(local_size.Get()));

    vector.GetValues(index_list, value_list);

    BOOST_CHECK_EQUAL(value_list.size(), static_cast<std::size_t>(local_size.Get()));

    for (auto index = vector_program_wise_index_type{}; index < vector_program_wise_index_type{ local_size.Get() };
         ++index)
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                              (value_list[static_cast<std::size_t>(index.Get())])(
                                  ComputeValueForIndex(begin_index.Get() + index.Get()))(epsilon));
}

#ifndef NDEBUG
BOOST_FIXTURE_TEST_CASE(get_values_output_argument_not_properly_allocated, Fixture)
{
    const PetscInt begin_index = GetMpi().GetRank<PetscInt>() * local_size.Get();

    std::vector<PetscInt> index_list(static_cast<std::size_t>(local_size.Get()));

    std::iota(index_list.begin(), index_list.end(), begin_index);

    std::vector<PetscScalar> value_list; // no allocation here.

    // Don't be astonished: PETSC ERROR will nonetheless appear on screen...
    BOOST_CHECK_THROW(vector.GetValues(index_list, value_list), MoReFEM::Advanced::Assertion);
}
#endif // NDEBUG


BOOST_FIXTURE_TEST_CASE(access_vector_content, Fixture)
{
    const auto begin_index = vector_program_wise_index_type{ GetMpi().GetRank<PetscInt>() * local_size.Get() };

    const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content(vector);

    BOOST_CHECK_EQUAL(content.GetSize(), local_size);

    for (auto index = vector_processor_wise_index_type{}; index < local_size; ++index)
        BOOST_CHECK_PREDICATE(
            NumericNS::AreEqual<double>,
            (content.GetValue(index))(ComputeValueForIndex(begin_index.Get() + index.Get()))(epsilon));
}


BOOST_FIXTURE_TEST_CASE(access_vector_content_and_modify, Fixture)
{
    const auto begin_index = vector_program_wise_index_type{ GetMpi().GetRank<PetscInt>() * local_size.Get() };

    Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector);
    {
        BOOST_CHECK_EQUAL(content.GetSize(), local_size);

        for (auto i = vector_processor_wise_index_type{}; i < local_size; ++i)
            content[i] *= 2.;
    }

    for (auto index = vector_processor_wise_index_type{}; index < local_size; ++index)
        BOOST_CHECK_PREDICATE(
            NumericNS::AreEqual<double>,
            (content.GetValue(index))(2. * ComputeValueForIndex(begin_index.Get() + index.Get()))(epsilon));
}

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    Fixture::Fixture() : vector("Vector in fixture")
    {
        decltype(auto) mpi = GetMpi();

        if (mpi.IsSequential())
            vector.InitSequentialVector(mpi, local_size);
        else
            vector.InitMpiVector(mpi, local_size, global_size);

        if (mpi.IsRootProcessor())
        {
            std::vector<PetscInt> index_list(static_cast<std::size_t>(global_size.Get()));
            std::vector<PetscScalar> value_list(static_cast<std::size_t>(global_size.Get()));

            std::iota(index_list.begin(), index_list.end(), 0);

            std::ranges::transform(index_list, value_list.begin(), ComputeValueForIndex);

            Utilities::PrintContainer<>::Do(value_list);

            vector.SetValues(index_list, value_list.data(), ADD_VALUES);
        }

        vector.Assembly(std::source_location::current(), update_ghost::no);
    }


    PetscScalar Fixture::ComputeValueForIndex(PetscInt index)
    {
        return 2. * static_cast<PetscScalar>(index) - 3.;
    };


} // namespace


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
