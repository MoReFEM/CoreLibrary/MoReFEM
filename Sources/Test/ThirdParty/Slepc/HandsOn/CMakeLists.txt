add_executable(TestSlepcHandsOn0)
add_executable(TestSlepcHandsOn1)
add_executable(TestSlepcHandsOn2)
add_executable(TestSlepcHandsOn3)

target_sources(TestSlepcHandsOn0
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/handson0.cpp
)
            
target_link_libraries(TestSlepcHandsOn0
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_organize_IDE(TestSlepcHandsOn0 Test/ThirdParty/Slepc/HandsOn Test/ThirdParty/Slepc/HandsOn)


morefem_boost_test_sequential_mode(NAME Slepc_HandsOn0
                                   EXE TestSlepcHandsOn0
                                   TIMEOUT 5)

target_sources(TestSlepcHandsOn1
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/handson1.cpp
)
            
target_link_libraries(TestSlepcHandsOn1
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_organize_IDE(TestSlepcHandsOn1 Test/ThirdParty/Slepc/HandsOn Test/ThirdParty/Slepc/HandsOn)

morefem_boost_test_sequential_mode(NAME Slepc_HandsOn1
                                   EXE TestSlepcHandsOn1
                                   TIMEOUT 5)


target_sources(TestSlepcHandsOn2
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/handson2.cpp
)
            
target_link_libraries(TestSlepcHandsOn2
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_organize_IDE(TestSlepcHandsOn2 Test/ThirdParty/Slepc/HandsOn Test/ThirdParty/Slepc/HandsOn)

morefem_boost_test_sequential_mode(NAME Slepc_HandsOn2
                                   EXE TestSlepcHandsOn2
                                   TIMEOUT 5)

target_sources(TestSlepcHandsOn3
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/handson3.cpp
)
     

target_link_libraries(TestSlepcHandsOn3
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_organize_IDE(TestSlepcHandsOn3 Test/ThirdParty/Slepc/HandsOn Test/ThirdParty/Slepc/HandsOn)

morefem_boost_test_sequential_mode(NAME Slepc_HandsOn3
                                   EXE TestSlepcHandsOn3
                                   TIMEOUT 5)

