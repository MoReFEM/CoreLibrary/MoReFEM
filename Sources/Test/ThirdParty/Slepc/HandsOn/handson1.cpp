// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE slepc_handson_1

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"


// NOLINTBEGIN
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    constexpr auto epsilon = 1.e-12;


} // namespace

// This test is mostly the verbatim of original Slepc test; I don't bother fixing clang-tidy suggestions!

BOOST_AUTO_TEST_CASE(Standard_Symmetric_Eigenvalue_Problem)
{
    static char help[] = "Standard symmetric eigenproblem corresponding to the Laplacian operator in 1 dimension.\n\n"
                         "The command line options are:\n"
                         "  -n <n>, where <n> = number of grid subdivisions = matrix dimension.\n\n";

    decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
    auto argc = master_test_suite.argc;
    auto* argv = master_test_suite.argv;

    Mat A = nullptr;   /* problem matrix */
    EPS eps = nullptr; /* eigenproblem solver context */
    EPSType type = nullptr;
    PetscReal error, tol, re, im;
    PetscScalar kr, ki;
    Vec xr = nullptr, xi = nullptr;
    PetscInt n = 30, i = 0, Istart = 0, Iend = 0, nev = 0, maxit = 0, its = 0, nconv = 0;

    int ierr = 0;

    PetscFunctionBeginUser;
    ierr = SlepcInitialize(&argc, &argv, MOREFEM_PETSC_NULL, help);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = PetscOptionsGetInt(MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, "-n", &n, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\n1-D Laplacian Eigenproblem, n=%" PetscInt_FMT "\n\n", n);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute the operator matrix that defines the eigensystem, Ax=kx
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    ierr = MatCreate(PETSC_COMM_WORLD, &A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, n, n);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetFromOptions(A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetUp(A);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = MatGetOwnershipRange(A, &Istart, &Iend);
    for (i = Istart; i < Iend; i++)
    {
        if (i > 0)
            ierr = MatSetValue(A, i, i - 1, -1.0, INSERT_VALUES);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        if (i < n - 1)
            ierr = MatSetValue(A, i, i + 1, -1.0, INSERT_VALUES);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        ierr = MatSetValue(A, i, i, 2.0, INSERT_VALUES);
        BOOST_REQUIRE_EQUAL(ierr, 0);
    }
    ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = MatCreateVecs(A, MOREFEM_PETSC_NULL, &xr);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatCreateVecs(A, MOREFEM_PETSC_NULL, &xi);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create the eigensolver and set various options
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
     Create eigensolver context
     */
    ierr = EPSCreate(PETSC_COMM_WORLD, &eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    /*
     Set operators. In this case, it is a standard eigenvalue problem
     */
    ierr = EPSSetOperators(eps, A, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSSetProblemType(eps, EPS_HEP);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /*
     Set solver parameters at runtime
     */
    ierr = EPSSetFromOptions(eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Solve the eigensystem
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    ierr = EPSSolve(eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /*
     Optional: Get some information from the solver and display it
     */
    ierr = EPSGetIterationNumber(eps, &its);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Number of iterations of the method: %" PetscInt_FMT "\n", its);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSGetType(eps, &type);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Solution method: %s\n\n", type);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSGetDimensions(eps, &nev, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Number of requested eigenvalues: %" PetscInt_FMT "\n", nev);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSGetTolerances(eps, &tol, &maxit);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(
        PETSC_COMM_WORLD, " Stopping condition: tol=%.4g, maxit=%" PetscInt_FMT "\n", static_cast<double>(tol), maxit);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Display solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
     Get number of converged approximate eigenpairs
     */
    ierr = EPSGetConverged(eps, &nconv);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Number of converged eigenpairs: %" PetscInt_FMT "\n\n", nconv);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    // Additions for MoReFEM test
    BOOST_CHECK_EQUAL(n, 30);
    BOOST_CHECK(nconv > 0);
    // End of additions for MoReFEM test

    if (nconv > 0)
    {
        /*
         Display eigenvalues and relative errors
         */
        ierr = PetscPrintf(PETSC_COMM_WORLD,
                           "           k          ||Ax-kx||/||kx||\n"
                           "   ----------------- ------------------\n");
        BOOST_REQUIRE_EQUAL(ierr, 0);

        for (i = 0; i < nconv; i++)
        {
            /*
             Get converged eigenpairs: i-th eigenvalue is stored in kr (real part) and
             ki (imaginary part)
             */
            ierr = EPSGetEigenpair(eps, i, &kr, &ki, xr, xi);
            BOOST_REQUIRE_EQUAL(ierr, 0);
            /*
             Compute the relative error associated to each eigenpair
             */
            ierr = EPSComputeError(eps, i, EPS_ERROR_RELATIVE, &error);
            BOOST_REQUIRE_EQUAL(ierr, 0);

#if defined(PETSC_USE_COMPLEX)
            re = PetscRealPart(kr);
            im = PetscImaginaryPart(kr);
#else
            re = kr;
            im = ki;
#endif

            BOOST_CHECK(MoReFEM::NumericNS::IsZero(im));

            ierr = PetscPrintf(
                PETSC_COMM_WORLD, "   %12f       %12g\n", static_cast<double>(re), static_cast<double>(error));
            BOOST_REQUIRE_EQUAL(ierr, 0);
            // Additions for MoReFEM test
            BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (re)(3.989739)(1.e-6));
            BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(4.72989e-09)(epsilon));
            // End of additions for MoReFEM test
        }
        ierr = PetscPrintf(PETSC_COMM_WORLD, "\n");
        BOOST_REQUIRE_EQUAL(ierr, 0);
    }
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /*
     Free work space
     */
    ierr = EPSDestroy(&eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatDestroy(&A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = VecDestroy(&xr);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = VecDestroy(&xi);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = SlepcFinalize();
    BOOST_REQUIRE_EQUAL(ierr, 0);
}
// NOLINTEND

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
