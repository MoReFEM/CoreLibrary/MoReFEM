// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE slepc_handson_2

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"


// This test is mostly the verbatim of original Slepc test; I don't bother fixing clang-tidy suggestions!
// NOLINTBEGIN
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    constexpr auto epsilon = 1.e-12;

    // Forward declaration.
    PetscErrorCode MatMarkovModel(PetscInt m, Mat A);

} // namespace


BOOST_AUTO_TEST_CASE(Standard_Symmetric_Eigenvalue_Problem)
{
    static char help[] = "Eigenvalue problem associated with a Markov model of a random walk on a triangular grid. "
                         "It is a standard nonsymmetric eigenproblem with real eigenvalues and the rightmost "
                         "eigenvalue is known to be 1.\n"
                         "This example illustrates how the user can set the initial vector.\n\n"
                         "The command line options are:\n"
                         "  -m <m>, where <m> = number of grid subdivisions in each dimension.\n\n";

    decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
    auto argc = master_test_suite.argc;
    auto* argv = master_test_suite.argv;


    /*
       User-defined routines
    */

    Vec v0 = nullptr;  /* initial vector */
    Mat A = nullptr;   /* operator matrix */
    EPS eps = nullptr; /* eigenproblem solver context */
    EPSType type = nullptr;
    PetscInt N = 0, m = 15, nev = 0;
    PetscMPIInt rank = 0;
    PetscBool terse;
    int ierr = 0;

    PetscFunctionBeginUser;
    ierr = SlepcInitialize(&argc, &argv, MOREFEM_PETSC_NULL, help);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = PetscOptionsGetInt(MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, "-m", &m, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    N = m * (m + 1) / 2;
    ierr = PetscPrintf(PETSC_COMM_WORLD, "\nMarkov Model, N=%" PetscInt_FMT " (m=%" PetscInt_FMT ")\n\n", N, m);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute the operator matrix that defines the eigensystem, Ax=kx
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    ierr = MatCreate(PETSC_COMM_WORLD, &A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetSizes(A, PETSC_DECIDE, PETSC_DECIDE, N, N);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetFromOptions(A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetUp(A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatMarkovModel(m, A);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create the eigensolver and set various options
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    /*
     Create eigensolver context
     */
    ierr = EPSCreate(PETSC_COMM_WORLD, &eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /*
     Set operators. In this case, it is a standard eigenvalue problem
     */
    ierr = EPSSetOperators(eps, A, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = EPSSetProblemType(eps, EPS_NHEP);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    /*
     Set solver parameters at runtime
     */
    ierr = EPSSetFromOptions(eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    /*
     Set the initial vector. This is optional, if not done the initial
     vector is set to random values
     */
    ierr = MatCreateVecs(A, &v0, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    if (!rank)
    {
        ierr = VecSetValue(v0, 0, 1.0, INSERT_VALUES);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        ierr = VecSetValue(v0, 1, 1.0, INSERT_VALUES);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        ierr = VecSetValue(v0, 2, 1.0, INSERT_VALUES);
        BOOST_REQUIRE_EQUAL(ierr, 0);
    }

    ierr = VecAssemblyBegin(v0);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = VecAssemblyEnd(v0);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSSetInitialSpace(eps, 1, &v0);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Solve the eigensystem
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    ierr = EPSSolve(eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    /*
     Optional: Get some information from the solver and display it
     */
    ierr = EPSGetType(eps, &type);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = PetscPrintf(PETSC_COMM_WORLD, " Solution method: %s\n\n", type);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = EPSGetDimensions(eps, &nev, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = PetscPrintf(PETSC_COMM_WORLD, " Number of requested eigenvalues: %" PetscInt_FMT "\n", nev);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    // Additions for MoReFEM test
    BOOST_CHECK_EQUAL(nev, 1);
    // End of additions for MoReFEM test

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Display solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    /* show detailed info unless -terse option is given by user */
    ierr = PetscOptionsHasName(MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, "-terse", &terse);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    // Additions for MoReFEM test
    BOOST_CHECK(!terse);
    // End of additions for MoReFEM test

    if (terse)
    {
        ierr = EPSErrorView(eps, EPS_ERROR_RELATIVE, MOREFEM_PETSC_NULL);
        BOOST_REQUIRE_EQUAL(ierr, 0);
    } else
    {
        ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_INFO_DETAIL);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        ierr = EPSConvergedReasonView(eps, PETSC_VIEWER_STDOUT_WORLD);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        ierr = EPSErrorView(eps, EPS_ERROR_RELATIVE, PETSC_VIEWER_STDOUT_WORLD);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);
        BOOST_REQUIRE_EQUAL(ierr, 0);

        // Additions for MoReFEM test
        PetscScalar re, im;
        ierr = EPSGetEigenvalue(eps, 0, &re, &im);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (re)(-1.)(1.e-6));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (im)(0.)(epsilon));

        PetscScalar error;
        ierr = EPSComputeError(eps, 0, EPS_ERROR_RELATIVE, &error);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(4.89349e-09)(epsilon));

        ierr = EPSGetEigenvalue(eps, 1, &re, &im);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (re)(1.)(1.e-6));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (im)(0.)(epsilon));

        ierr = EPSComputeError(eps, 1, EPS_ERROR_RELATIVE, &error);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(2.41456e-9)(epsilon));
        // End of additions for MoReFEM test
    }
    ierr = EPSDestroy(&eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatDestroy(&A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = VecDestroy(&v0);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = SlepcFinalize();
    BOOST_REQUIRE_EQUAL(ierr, 0);
}

namespace
{
    /*
     Matrix generator for a Markov model of a random walk on a triangular grid.

     This subroutine generates a test matrix that models a random walk on a
     triangular grid. This test example was used by G. W. Stewart ["{SRRIT} - a
     FORTRAN subroutine to calculate the dominant invariant subspaces of a real
     matrix", Tech. report. TR-514, University of Maryland (1978).] and in a few
     papers on eigenvalue problems by Y. Saad [see e.g. LAA, vol. 34, pp. 269-295
     (1980) ]. These matrices provide reasonably easy test problems for eigenvalue
     algorithms. The transpose of the matrix  is stochastic and so it is known
     that one is an exact eigenvalue. One seeks the eigenvector of the transpose
     associated with the eigenvalue unity. The problem is to calculate the steady
     state probability distribution of the system, which is the eigevector
     associated with the eigenvalue one and scaled in such a way that the sum all
     the components is equal to one.

     Note: the code will actually compute the transpose of the stochastic matrix
     that contains the transition probabilities.
     */
    PetscErrorCode MatMarkovModel(PetscInt m, Mat A)
    {
        const PetscReal cst = 0.5 / static_cast<PetscReal>(m - 1);
        PetscReal pd, pu;
        PetscInt Istart = 0, Iend = 0, i = 0, j = 0, jmax = 0, ix = 0;
        int ierr = 0;

        PetscFunctionBeginUser;
        ierr = MatGetOwnershipRange(A, &Istart, &Iend);
        BOOST_REQUIRE_EQUAL(ierr, 0);

        for (i = 1; i <= m; i++)
        {
            jmax = m - i + 1;
            for (j = 1; j <= jmax; j++)
            {
                ix = ix + 1;
                if (ix - 1 < Istart || ix > Iend)
                    continue; /* compute only owned rows */
                if (j != jmax)
                {
                    pd = cst * static_cast<PetscReal>(i + j - 1);
                    /* north */
                    if (i == 1)
                        ierr = MatSetValue(A, ix - 1, ix, 2 * pd, INSERT_VALUES);
                    else
                        ierr = MatSetValue(A, ix - 1, ix, pd, INSERT_VALUES);

                    BOOST_REQUIRE_EQUAL(ierr, 0);

                    /* east */
                    if (j == 1)
                        ierr = MatSetValue(A, ix - 1, ix + jmax - 1, 2 * pd, INSERT_VALUES);
                    else
                        ierr = MatSetValue(A, ix - 1, ix + jmax - 1, pd, INSERT_VALUES);

                    BOOST_REQUIRE_EQUAL(ierr, 0);
                }
                /* south */
                pu = 0.5 - cst * static_cast<PetscReal>(i + j - 3);
                if (j > 1)
                    ierr = MatSetValue(A, ix - 1, ix - 2, pu, INSERT_VALUES);

                BOOST_REQUIRE_EQUAL(ierr, 0);

                /* west */
                if (i > 1)
                    ierr = MatSetValue(A, ix - 1, ix - jmax - 2, pu, INSERT_VALUES);

                BOOST_REQUIRE_EQUAL(ierr, 0);
            }
        }
        ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
        BOOST_REQUIRE_EQUAL(ierr, 0);
        return ierr;
    }

} // namespace
// NOLINTEND


/*TEST

 test:
suffix: 1
nsize: 2
args: -eps_largest_real -eps_nev 4 -eps_two_sided {{0 1}} -eps_krylovschur_locking {{0 1}} -ds_parallel synchronized
-terse filter: sed -e "s/90424/90423/" | sed -e "s/85715/85714/"

TEST*/

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
