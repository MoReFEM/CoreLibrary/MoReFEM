The purpose here is to test the code is working correctly in parallel mode as well, by testing directly the eigen solutions on few test vectors.

`HandsOn` and `Wrappers` Slepc tests are adapted from the hands on available on Slepc website, but have been a bit simplified (no command line communication for instance).

As they were really close to the original code, they do not leverage some of the helpers from MoReFEM library: for instance `Wrappers::Petsc::Vector` are used and not `GlobalVector`.

However, to test parallel it is easier if some operations are readily available (for instance being able to determine easily the layout of the resulting vector); that's the reason a toy model is defined here. Its sole purpose is to provide a square matrix with proper layout.

