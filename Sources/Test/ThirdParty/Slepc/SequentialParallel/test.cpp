// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <string>

#include "Geometry/Mesh/Mesh.hpp"

#include "Test/ThirdParty/Slepc/SequentialParallel/InputData.hpp"
#include "Test/Tools/Fixture/Enum.hpp"

#define BOOST_TEST_MODULE slepc_sequential_parallel


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"
#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Slepc/Solver/Eps.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "Test/ThirdParty/Slepc/SequentialParallel/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;

namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::SlepcNS::ToyModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on

    constexpr auto epsilon = 1.e-9;


} // namespace


BOOST_FIXTURE_TEST_CASE(Standard_Symmetric_Eigenvalue_Problem, fixture_type)
{
    // The model essentially initializes basic stuff and provides a square matrix (sequentially or in parallel)
    decltype(auto) model = GetModel();
    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::SlepcNS::MeshIndex::sole));
    decltype(auto) mpi = GetMpi();

    decltype(auto) problem_matrix = model.GetMatrix();

    Wrappers::Slepc::Eps eps(mpi, problem_matrix, Wrappers::Slepc::problem_type::non_hermitian);

    eps.Solve();

    BOOST_REQUIRE_EQUAL(eps.NconvergedEigenPairs(), 1UL);

    const auto [re, im] = eps.GetEigenPair(0UL);

    GlobalVector lhs_result(problem_matrix.GetColNumberingSubset());
    AllocateGlobalVector(god_of_dof, lhs_result);

    GlobalVector rhs_result(problem_matrix.GetColNumberingSubset());
    AllocateGlobalVector(god_of_dof, rhs_result);

    {
        Wrappers::Petsc::MatMult(problem_matrix, re.GetEigenVector(), lhs_result);

        rhs_result.Copy(re.GetEigenVector());
        rhs_result.Scale(re.GetEigenValue());

        std::string inequality_description;

        BOOST_CHECK(AreEqual(lhs_result, rhs_result, epsilon, inequality_description));
    }

    {
        Wrappers::Petsc::MatMult(problem_matrix, im.GetEigenVector(), lhs_result);

        rhs_result.Copy(im.GetEigenVector());
        rhs_result.Scale(im.GetEigenValue());

        std::string inequality_description;

        BOOST_CHECK(AreEqual(lhs_result, rhs_result, epsilon, inequality_description));
    }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
