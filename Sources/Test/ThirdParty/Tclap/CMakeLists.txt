add_executable(TestTclap)

target_sources(TestTclap
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
               
target_link_libraries(TestTclap
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)
                      
morefem_boost_test_sequential_mode(NAME Tclap
                                   EXE TestTclap
                                   TIMEOUT 10)

morefem_organize_IDE(TestTclap Test/ThirdParty Test/ThirdParty/Tclap)
