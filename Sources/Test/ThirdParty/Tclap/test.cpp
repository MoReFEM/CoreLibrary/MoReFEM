// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <exception>
#include <memory>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE tclap
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

using namespace MoReFEM;


// NOLINTBEGIN(misc-non-private-member-variables-in-classes,cppcoreguidelines-non-private-member-variables-in-classes)
namespace // anonymous
{


    template<class TCLAPArgTypeT>
    class Case
    {
      public:
        using cli_arg_type = TCLAPArgTypeT;

        Case();

        TCLAP::CmdLine& GetCommand() noexcept;

        TCLAPArgTypeT& GetArg() noexcept;

      protected:
        std::unique_ptr<TCLAP::CmdLine> cmd_ = nullptr;

        std::unique_ptr<TCLAPArgTypeT> cli_arg_ = nullptr;
    };


    class Case1 : public Case<TCLAP::MultiArg<Wrappers::Tclap::StringPair>>
    {
      public:
        Case1();
    };


    class Case2 : public Case<TCLAP::ValueArg<std::string>>
    {
      public:
        Case2();
    };


} // namespace
// NOLINTEND(misc-non-private-member-variables-in-classes,cppcoreguidelines-non-private-member-variables-in-classes)


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_SUITE(string_pair, Case1)

// NOLINTBEGIN(clang-analyzer-optin.cplusplus.VirtualCall) due to TCLAP bug
BOOST_AUTO_TEST_CASE(shorthand_notation)
{
    std::vector<std::string> short_args{
        "program_name",
        "-e FOO=BAR",
        "-e BAZ=42 " // space is intentional!
    };

    GetCommand().parse(short_args);
    auto& env_arg = GetArg();

    BOOST_REQUIRE(env_arg.getValue().size() == 2UL);

    BOOST_CHECK(env_arg.getValue()[0].GetValue().first == "FOO");
    BOOST_CHECK(env_arg.getValue()[0].GetValue().second == "BAR");

    BOOST_CHECK(env_arg.getValue()[1].GetValue().first == "BAZ");
    BOOST_CHECK(env_arg.getValue()[1].GetValue().second == "42");
};


BOOST_AUTO_TEST_CASE(longer_notation)
{
    std::vector<std::string> long_args{
        "program_name",
        "--env FOO=BAR",
        "--env BAZ=42 " // space is intentional!
    };


    GetCommand().parse(long_args);
    auto& env_arg = GetArg();

    BOOST_REQUIRE(env_arg.getValue().size() == 2UL);

    BOOST_CHECK(env_arg.getValue()[0].GetValue().first == "FOO");
    BOOST_CHECK(env_arg.getValue()[0].GetValue().second == "BAR");

    BOOST_CHECK(env_arg.getValue()[1].GetValue().first == "BAZ");
    BOOST_CHECK(env_arg.getValue()[1].GetValue().second == "42");
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(invalid_cases, Case1)

BOOST_AUTO_TEST_CASE(two_equals)
{
    std::vector<std::string> invalid_args{
        "program_name",
        "--env FOO=BAR=BAZ",
    };

    BOOST_CHECK_THROW(GetCommand().parse(invalid_args), std::exception);
}


BOOST_AUTO_TEST_CASE(missing_argument)
{
    std::vector<std::string> invalid_args{
        "program_name",
        "-e",
    };

    BOOST_CHECK_THROW(GetCommand().parse(invalid_args), std::exception);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(mandatory_arg, Case2)

BOOST_AUTO_TEST_CASE(valid_argument_provided_short_form)
{
    std::vector<std::string> valid_args{ "program_name", "-i path" };

    /* BOOST_REQUIRE_NO_THROW */ (GetCommand().parse(valid_args));
    BOOST_CHECK_EQUAL(GetArg().getValue(), "path");
}

BOOST_AUTO_TEST_CASE(valid_argument_provided_long_form)
{
    std::vector<std::string> valid_args{ "program_name", "--input_file path" };

    /* BOOST_REQUIRE_NO_THROW */ (GetCommand().parse(valid_args));
    BOOST_CHECK_EQUAL(GetArg().getValue(), "path");
}


BOOST_AUTO_TEST_CASE(invalid_argument)
{
    std::vector<std::string> invalid_args{ "program_name" };

    BOOST_CHECK_THROW(GetCommand().parse(invalid_args), std::exception);
}


BOOST_AUTO_TEST_SUITE_END()

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    template<class TCLAPArgTypeT>
    Case<TCLAPArgTypeT>::Case()
    {
        cmd_ = std::make_unique<TCLAP::CmdLine>("Command description message");
        cmd_->setExceptionHandling(false);
    }


    Case1::Case1()
    {
        cli_arg_ =
            std::make_unique<cli_arg_type>("e", "env", "environment_variable", false, "string=string", GetCommand());
    }


    Case2::Case2()
    {
        cli_arg_ = std::make_unique<cli_arg_type>(
            "i", "input_file", "Input file to interpret", true, "", "string", GetCommand());
    }


    template<class TCLAPArgTypeT>
    TCLAP::CmdLine& Case<TCLAPArgTypeT>::GetCommand() noexcept
    {
        assert(!(!cmd_));
        return *cmd_;
    }


    template<class TCLAPArgTypeT>
    TCLAPArgTypeT& Case<TCLAPArgTypeT>::GetArg() noexcept
    {
        assert(!(!cli_arg_));
        return *cli_arg_;
    }


} // namespace

// NOLINTEND(clang-analyzer-optin.cplusplus.VirtualCall)
