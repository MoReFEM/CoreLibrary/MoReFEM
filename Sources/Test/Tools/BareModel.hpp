// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_BAREMODEL_DOT_HPP_
#define MOREFEM_TEST_TOOLS_BAREMODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/TimeManager/Concept.hpp"
#include "Core/TimeManager/Policy/Evolution/None.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Model/Model.hpp"


namespace MoReFEM::TestNS
{


    /*!
     * \brief Helper class when in a test you need a very bare-bone \a Model which implements nothing more than what is in the \a Model CRTP of the
     * library.
     *
     * \attention Don't forget to call \a Initialize() after construction!
     *
     *
     * \tparam TimeDependencyT Time dependency policy; None is chosen in most tests.
     *
     * \copydoc doxygen_hide_do_consider_processor_wise_local_2_global_tparam
     *
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
            = DoConsiderProcessorWiseLocal2Global::no
    >
    class BareModel
    : public ::MoReFEM::Model
             <
                 BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>,
                 MoReFEMDataTypeT,
                 DoConsiderProcessorWiseLocal2GlobalT
             >
    // clang-format on
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>;

        //! Convenient alias.
        using parent = ::MoReFEM::Model<self, MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Alias to the object responsible for time management.
        using time_manager_type = typename MoReFEMDataTypeT::time_manager_type;

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

        //! For tests, we may want access to this protected member. Don't copy this in production code!
        using parent::GetNonCstTimeManager;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         *
         * \param[in] a_create_domain_list_for_coords Whether the model will compute the list of domains a coord is in
         * or not.
         */
        BareModel(MoReFEMDataTypeT& morefem_data,
                  create_domain_list_for_coords a_create_domain_list_for_coords =
                      create_domain_list_for_coords::yes); // #1876 not sure we should bother with this argument...

        //! Destructor.
        ~BareModel() = default;

        //! \copydoc doxygen_hide_copy_constructor
        BareModel(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        BareModel(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        self& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{

        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


        //! Manage time iteration.
        void Forward()
            requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>);

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep()
            requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>);


      private:
        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const
            requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>);


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep()
            requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>);


        ///@}
    };


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Tools/BareModel.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_BAREMODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
