// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_BAREMODEL_DOT_HXX_
#define MOREFEM_TEST_TOOLS_BAREMODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/BareModel.hpp"
// *** MoReFEM header guards *** < //


#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::TestNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::BareModel(
        MoReFEMDataTypeT& morefem_data,
        create_domain_list_for_coords a_create_domain_list_for_coords)
    : parent(morefem_data, a_create_domain_list_for_coords)
    {
        // In tests we may need easily to check the temporary data that are usually flushed when a Model is
        // fully initialized. The following method is however not recommended in a normal Model...
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::SupplInitialize()
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::SupplFinalize()
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline const std::string& BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::ClassName()
    {
        static std::string name("BareModel");
        return name;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::Forward()
        requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>)
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::SupplFinalizeStep()
        requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>)
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    bool BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::SupplHasFinishedConditions() const
        requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>)
    {
        return true;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT>::SupplInitializeStep()
        requires(!std::is_same_v<time_manager_type, ::MoReFEM::TimeManagerNS::Instance::None>)
    { }


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_BAREMODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
