// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <sstream>
#include <string>

#include "Test/Tools/CheckIdenticalFiles.hpp"

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"


namespace MoReFEM::TestNS
{


    void CheckIdenticalFiles(const FilesystemNS::Directory& ref_dir,
                             const FilesystemNS::Directory& obtained_dir,
                             std::string&& filename,
                             const std::source_location location)
    {
        std::ostringstream oconv;

        if (!ref_dir.DoExist())
        {
            oconv << "Reference folder " << ref_dir << " does not exist.";
            throw Exception(oconv.str(), location);
        }

        if (!obtained_dir.DoExist())
        {
            oconv << "Result folder " << obtained_dir << " does not exist.";
            throw Exception(oconv.str(), location);
        }

        auto ref_input_data = ::MoReFEM::FilesystemNS::File{ ref_dir.AddFile(filename) };

        if (!ref_input_data.DoExist())
        {
            oconv << "Reference file " << ref_input_data << " does not exist.";
            throw Exception(oconv.str(), location);
        }

        auto obtained_input_data = ::MoReFEM::FilesystemNS::File{ obtained_dir.AddFile(filename) };

        if (!obtained_input_data.DoExist())
        {
            oconv << "Result file " << obtained_input_data << " does not exist.";
            throw Exception(oconv.str(), location);
        }

        if (!FilesystemNS::AreEquals(ref_input_data, obtained_input_data, location))
        {
            oconv << "Reference file " << ref_input_data << " and result file " << obtained_input_data
                  << " are not identical.";
            throw Exception(oconv.str(), location);
        }
    }


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
