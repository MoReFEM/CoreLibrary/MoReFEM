// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_CHECKIDENTICALFILES_DOT_HPP_
#define MOREFEM_TEST_TOOLS_CHECKIDENTICALFILES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
#include <source_location>
// IWYU pragma: no_include <string>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS
{


    /*!
     * \brief Check a file obtained in a test about a model is exactly the same as a reference one.
     *
     * This function is assumed to be called within a Catch TEST_CASE; in case there is an issue (non existent
     * file or non consistent content) the Catch test fails.
     *
     * \param[in] ref_dir The reference directory. It is a subdirectory of ModelInstances/ *YourModel*
     * /ExpectedResults, e.g. ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/ExpectedResults/1D/Mesh_1/Ensight6.
     * \param[in] obtained_dir The pendant of \a ref_dir for the outputs given by the model that is expected to have
     * run juste before the call to the result check. It is expected to be a subdirectory somwehere in
     * ${MOREFEM_TEST_OUTPUT_DIR}.
     * \param[in] filename Name of the file being compared. It is expected to be the same in \a ref_dir and in
     * \a obtained_dir and to be located directly there (not in a subdirectory).
     * \copydoc doxygen_hide_source_location
     */
    void CheckIdenticalFiles(const FilesystemNS::Directory& ref_dir,
                             const FilesystemNS::Directory& obtained_dir,
                             std::string&& filename,
                             const std::source_location location = std::source_location::current());


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Tools/CheckIdenticalFiles.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_CHECKIDENTICALFILES_DOT_HPP_
// *** MoReFEM end header guards *** < //
