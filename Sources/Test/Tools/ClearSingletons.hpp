// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_CLEARSINGLETONS_DOT_HPP_
#define MOREFEM_TEST_TOOLS_CLEARSINGLETONS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Core/TimeManager/Concept.hpp"


namespace MoReFEM::TestNS
{


    /*!
     * \brief A struct to shelter the ability to clear all defined unique ids.
     *
     * \internal A struct is used as the friendship to a mere function cause many -Wredundant-decls in gcc which doesn't undesrtand correctly
     * the forward declarations. Forward declaration to a struct is better handled by this compiler.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct ClearSingletons
    {

        /*!
         * \brief Clear all the defined unique ids.
         *
         * In some tests, we would like to load several Lua files which uses up the same \a input_data_tuple. In
         * fulll-fledged models we don't want to encourage that as it could lead to painful bugs to solve, but for tests
         * it is really handy to be able to clear the content so that many tests can be placed in the same file (without
         * that we need to multiply the number of executables to generate).
         */
        static void Do();
    };

} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
#include "Test/Tools/ClearSingletons.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_CLEARSINGLETONS_DOT_HPP_
// *** MoReFEM end header guards *** < //
