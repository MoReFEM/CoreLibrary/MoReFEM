// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_CLEARSINGLETONS_DOT_HXX_
#define MOREFEM_TEST_TOOLS_CLEARSINGLETONS_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/ClearSingletons.hpp"
// *** MoReFEM header guards *** < //


#include <ostream>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Singleton/Exceptions/Singleton.hpp"

#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ParameterInstances/Fiber/FiberListManager.hpp"


namespace MoReFEM::TestNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void ClearSingletons<TimeManagerT>::Do()
    {
        {
            decltype(auto) manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();
            manager.Clear();
        }

        {
            decltype(auto) manager = DomainManager::CreateOrGetInstance();
            manager.Clear();
        }

        {
            decltype(auto) manager = UnknownManager::CreateOrGetInstance();
            manager.Clear();
        }

        {
            decltype(auto) manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
            manager.Clear();
        }

        {
            decltype(auto) manager = GodOfDofManager::CreateOrGetInstance();
            manager.Clear();
        }

        {
            decltype(auto) manager = Internal::MeshNS::CoordsMatchingManager::CreateOrGetInstance();
            manager.Clear();
        }

        {
            try
            {
                decltype(auto) manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                                   ParameterNS::Type::scalar,
                                                                   TimeManagerT>::GetInstance();
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                                   ParameterNS::Type::vector,
                                                                   TimeManagerT>::GetInstance();
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                                                   ParameterNS::Type::scalar,
                                                                   TimeManagerT>::GetInstance();
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                                                   ParameterNS::Type::vector,
                                                                   TimeManagerT>::GetInstance();
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager = Advanced::LightweightDomainListManager::GetInstance();
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }
    }


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_CLEARSINGLETONS_DOT_HXX_
// *** MoReFEM end header guards *** < //
