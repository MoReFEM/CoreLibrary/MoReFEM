// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
// IWYU pragma: no_include <iosfwd>

#include "Utilities/Filesystem/File.hpp"

#include "Test/Tools/CompareDataFiles.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS
{


    template<>
    std::vector<double> CompareDataFiles<MeshNS::Format::Ensight>::ExtractDoubleValues(const FilesystemNS::File& file)
    {
        std::ifstream in{ file.Read() };

        std::string line;
        getline(in, line); // skip first line

        std::string str_value;

        std::vector<double> ret;

        while (getline(in, line))
        {
            const auto max = std::min(line.size(), 72UL);

            for (auto index = 0UL; index < max; index += 12UL)
            {
                str_value.assign(line, index, 12UL);
                ret.push_back(std::stod(str_value));
            }
        }

        return ret;
    }


    template<>
    std::vector<double> CompareDataFiles<MeshNS::Format::Vizir>::ExtractDoubleValues(const FilesystemNS::File& file)
    {
        std::ifstream in{ file.Read() };

        std::string line;
        for (int i = 0; i < 9; ++i)
            getline(in, line); // skip first 9 line.

        std::vector<double> ret;

        while (getline(in, line))
        {
            std::istringstream iconv(line);

            // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
            double value;
            while (iconv >> value)
                ret.push_back(value);
        }

        return ret;
    }


} // namespace MoReFEM::TestNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
