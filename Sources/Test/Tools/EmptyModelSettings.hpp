// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_EMPTYMODELSETTINGS_DOT_HPP_
#define MOREFEM_TEST_TOOLS_EMPTYMODELSETTINGS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <tuple>

#include "Utilities/InputData/ModelSettings.hpp"


namespace MoReFEM::TestNS
{


    /*!
     * \brief Placeholder when there are no \a ModelSettings.
     *
     * Shouldn't be used much - in fact most of the tests should probably from now use exclusively
     * \a ModelSettings and let \a InputData empty.
     */
    struct EmptyModelSettings : public ModelSettings<std::tuple<>>
    {
        //! Override of \a Init() method - which doesn't do anything.
        void Init() override;
    };

} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_EMPTYMODELSETTINGS_DOT_HPP_
// *** MoReFEM end header guards *** < //
