// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>
#include <string_view>

#include "Test/Tools/Exceptions/Exception.hpp"


namespace // anonymous
{


    std::string MismatchLinearAlgebraMsg(std::string_view msg);


} // namespace


namespace MoReFEM::TestNS::ExceptionNS
{


    MismatchLinearAlgebra::~MismatchLinearAlgebra() = default;


    MismatchLinearAlgebra::MismatchLinearAlgebra(std::string_view msg, const std::source_location location)
    : MoReFEM::Exception(MismatchLinearAlgebraMsg(msg), location)
    { }


} // namespace MoReFEM::TestNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string MismatchLinearAlgebraMsg(std::string_view msg)
    {
        std::ostringstream oconv;
        oconv << "Mismatch between obtained and expected linear algebra: ";
        oconv << msg;
        return oconv.str();
    }


} // namespace
