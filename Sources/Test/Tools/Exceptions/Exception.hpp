// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_TEST_TOOLS_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>
#include <string_view>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::ExceptionNS
{


    //! Called when a method makes no sense for a given time step policy.,
    class MismatchLinearAlgebra : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] msg Message to print.
         * \copydoc doxygen_hide_source_location
         */
        explicit MismatchLinearAlgebra(std::string_view msg, const std::source_location location);

        //! Destructor
        virtual ~MismatchLinearAlgebra() override;

        //! \copydoc doxygen_hide_copy_constructor
        MismatchLinearAlgebra(const MismatchLinearAlgebra& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MismatchLinearAlgebra(MismatchLinearAlgebra&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MismatchLinearAlgebra& operator=(const MismatchLinearAlgebra& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MismatchLinearAlgebra& operator=(MismatchLinearAlgebra&& rhs) = delete;
    };


} // namespace MoReFEM::TestNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
