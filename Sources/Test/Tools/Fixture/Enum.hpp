// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_FIXTURE_ENUM_DOT_HPP_
#define MOREFEM_TEST_TOOLS_FIXTURE_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //

namespace MoReFEM::TestNS::FixtureNS
{


    //! Whether a Run() method should be called at first call or not.
    enum class call_run_method_at_first_call { no, yes };


} // namespace MoReFEM::TestNS::FixtureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_FIXTURE_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
