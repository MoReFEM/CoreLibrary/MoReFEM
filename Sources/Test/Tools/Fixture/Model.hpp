// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_FIXTURE_MODEL_DOT_HPP_
#define MOREFEM_TEST_TOOLS_FIXTURE_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Tools/Fixture/Enum.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/InitMoReFEMDataFromCLI.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    /*!
     * \brief An helper class to build tests with Boost test.
     *
     * The goal is to provide in a main different tests:
     *
     * \code
     namespace // anonymous
     {

        using fixture_type =
            TestNS::FixtureNS::Model
            <
                TestNS::WhateverModelYouProvide
            >;
     } // namespace anonymous

     BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are identical")
     {
        GetModel().SameUnknown();
     }

     BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are different but share the same P1
     shape function label")
     {
        GetModel().UnknownP1TestP1();
     }

     BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test
     function P1")
     {
        GetModel().UnknownP2TestP1();
     }
     * \endcode
     *
     * The fixture here is a hack to run once and only once most of the initialization steps (MPI initialization,
     * building of the different singletons, etc...).
     *
     * Basically it builds and run the \a Model throughout all of the integration tests.
     *
     * \attention Contrary to the original intent of the fixture, it does NOT make tabula rasa of the state after
     * each test. So the tests should be as independent as possible from one another.
     *
     * \tparam ModelT Type of the model to build.
     * \tparam call_run_method_at_first_call Whether a Run() method should be called at first call or not. In fact
     when C++ 20 is out a concept
     * checking whether Run() exists or not would be more elegant than this explicit template parameter. Should be
     yes in the cases an actual \a Model
     * derived from \a Model class is used (see PetscVectorIO test for a counter-example).
     *
     * The expected command line arguments are:
     * - First one is the value for environment variable MOREFEM_ROOT
     * - Second one is the value for environment variable MOREFEM_TEST_OUTPUT_DIR
     * - Third one is the path to the Lua file (that may use ${MOREFEM_ROOT}).
     */
    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT = call_run_method_at_first_call::yes
    >
    // clang-format on
    struct Model : public TestEnvironment
    {
      public:
        //! Alias to MoReFEMData.
        using morefem_data_type = typename ModelT::morefem_data_type;

        //! \copydoc doxygen_hide_input_data_type_alias
        using input_data_type = typename morefem_data_type::input_data_type;

        //! \copydoc doxygen_hide_model_specific_model_settings
        using model_settings_type = typename morefem_data_type::model_settings_type;

        /*!
         * \brief Static method which yields the model considered for the tests.
         *
         * \return Constant reference to the \a Model.
         */
        static const ModelT& GetModel();


        /*!
         * \brief Static method which yields the model considered for the tests.
         *
         * \return Non constant reference to the \a Model.
         */
        static ModelT& GetNonCstModel();

        /*!
         * \brief Static method which yields the \a MoReFEMData considered for the tests.
         *
         * This static method is not really useful for a full-fledged model, which provides access to this anyway,
         * but is really handy for mock models used in some unit tests.
         *
         * \return Reference to the \a MoReFEMData.
         */
        static morefem_data_type& GetNonCstMoReFEMData();

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Model();

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::TestNS::FixtureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Tools/Fixture/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_FIXTURE_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
