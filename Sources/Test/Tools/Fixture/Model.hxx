// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_FIXTURE_MODEL_DOT_HXX_
#define MOREFEM_TEST_TOOLS_FIXTURE_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/Fixture/Model.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    // clang-format on
    Model<ModelT, CallRunMethodT>::Model()
    {
        static bool first_call = true;

        if (first_call)
        {
            const auto root_value = std::getenv("MOREFEM_RESULT_DIR");

            if (root_value != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_RESULT_DIR environment variables unset! "
                         "(currently it is set to "
                      << root_value << ").";
                throw Exception(oconv.str());
            }


            const auto prepartitioned_data_dir = std::getenv("MOREFEM_PREPARTITIONED_DATA_DIR");

            if (prepartitioned_data_dir != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_PREPARTITIONED_DATA_DIR environment variables unset! "
                         "(currently it is set to "
                      << prepartitioned_data_dir << ").";
                throw Exception(oconv.str());
            }

            first_call = false;
        }
    }


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    // clang-format on
    const ModelT& Model<ModelT, CallRunMethodT>::GetModel()
    {
        static ModelT model(GetNonCstMoReFEMData());

        if constexpr (CallRunMethodT == call_run_method_at_first_call::yes)
        {
            static bool first_call = true;

            if (first_call)
            {
                model.Run();
                first_call = false;
            }
        }

        return model;
    }


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    // clang-format on
    ModelT& Model<ModelT, CallRunMethodT>::GetNonCstModel()
    {
        return const_cast<ModelT&>(GetModel());
    }


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    auto Model<ModelT, CallRunMethodT>::GetNonCstMoReFEMData() -> morefem_data_type&
    {
        static auto ret
        {
            InitMoReFEMDataFromCLI
            <
                typename morefem_data_type::model_settings_type,
                typename morefem_data_type::input_data_type,
                typename morefem_data_type::time_manager_type
            >()
        };
        return ret;
    }
    // clang-format on


} // namespace MoReFEM::TestNS::FixtureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_FIXTURE_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
