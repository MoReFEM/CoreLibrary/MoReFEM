// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_FIXTURE_MODELNOINPUTDATA_DOT_HPP_
#define MOREFEM_TEST_TOOLS_FIXTURE_MODELNOINPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Geometry/Coords/Coords.hpp"

#include "Test/Tools/Fixture/Enum.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/InitMoReFEMDataFromCLI.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    /*!
     * \brief An helper class to build tests with Boost test.
     *
     * The goal is to provide in a main different tests:
     * \code
     namespace // anonymous
     {

         struct OutputDirWrapper
         {
            static constexpr std::string Path()
                 {
                    return "relative/path/for/my/test";
                 }
         };

         using fixture_type = TestNS::FixtureNS::ModelNoInputData
         <
             model_type,
             OutputDirWrapper,
             TestNS::FixtureNS::call_run_method_at_first_call::yes
         >;
     } // namespace anonymous

     BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are identical")
     {
        GetModel().SameUnknown();
     }

     BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are different but share the same P1
     shape function label")
     {
        GetModel().UnknownP1TestP1();
     }

     BOOST_FIXTURE_TEST_CASE(fixture_type, "Unknown and test function unknown are different; unknown is P2 and test
     function P1")
     {
        GetModel().UnknownP2TestP1();
     }
     * \endcode
     *
     * The fixture here is a hack to run once and only once most of the initialization steps (MPI initialization,
     * building of the different singletons, etc...).
     *
     * Basically it builds and run the \a Model throughout all of the integration tests.
     *
     * \attention Contrary to the original intent of the fixture, it does NOT make tabula rasa of the state after
     * each test. So the tests should be as independent as possible from one another.
     *
     * \tparam ModelT Type of the model to build.
     * \tparam OutputDirWrapperT Structure which should provide a static method with signature
     * `static constexpr std::string Path();` which returns within $MOREFEM_TEST_OUTPUT_DIR  the
     * subpath in which the outputs for current test will be written. This is a workaround to the fact
     * `std::string` can't be provided as template argument.
     * \tparam call_run_method_at_first_call Whether a Run() method should be called at first call or not.
     *
     * The expected command line arguments are:
     * - First one is the value for environment variable MOREFEM_ROOT
     * - Second one is the value for environment variable MOREFEM_TEST_OUTPUT_DIR
     *
     *  \tparam DoCreateDomainListForCoordsT Whether the model will compute the list of domains a coord is in
     * or not.
     */
    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT = TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>,
        call_run_method_at_first_call CallRunMethodT = call_run_method_at_first_call::yes,
        create_domain_list_for_coords DoCreateDomainListForCoordsT = create_domain_list_for_coords::no
    >
    // clang-format on
    struct ModelNoInputData : public TestEnvironment
    {
      public:
        /*!
         * \brief Static method which yields the model considered for the tests.
         *
         * \return Constant reference to the \a Model.
         */
        static const ModelT& GetModel();


        /*!
         * \brief Static method which yields the model considered for the tests.
         *
         * \return Non constant reference to the \a Model.
         */
        static ModelT& GetNonCstModel();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit ModelNoInputData();

        //! Destructor.
        ~ModelNoInputData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ModelNoInputData(const ModelNoInputData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ModelNoInputData(ModelNoInputData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ModelNoInputData& operator=(const ModelNoInputData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ModelNoInputData& operator=(ModelNoInputData&& rhs) = delete;

        ///@}

      public:
        //! Convenient alias.
        using morefem_data_type = typename ModelT::morefem_data_type;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

        //! \copydoc doxygen_hide_model_specific_model_settings
        using model_settings_type = typename morefem_data_type::model_settings_type;

        //! Returns the special \a MoReFEMData object for tests, which doesn't feature an `InputData`.
        static morefem_data_type& GetNonCstMoReFEMData();
    };


} // namespace MoReFEM::TestNS::FixtureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Tools/Fixture/ModelNoInputData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_FIXTURE_MODELNOINPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
