// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_FIXTURE_MODELNOINPUTDATA_DOT_HXX_
#define MOREFEM_TEST_TOOLS_FIXTURE_MODELNOINPUTDATA_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/Fixture/ModelNoInputData.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    ModelNoInputData<ModelT, OutputDirWrapperT, TimeManagerT, CallRunMethodT, DoCreateDomainListForCoordsT>::
        ModelNoInputData()
    {
        static bool first_call = true;

        if (first_call)
        {
            const auto root_value = std::getenv("MOREFEM_RESULT_DIR");

            if (root_value != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_RESULT_DIR environment variables unset! "
                         "(currently it is set to "
                      << root_value << ").";
                throw Exception(oconv.str());
            }


            const auto prepartitioned_data_dir = std::getenv("MOREFEM_PREPARTITIONED_DATA_DIR");

            if (prepartitioned_data_dir != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_PREPARTITIONED_DATA_DIR environment variables unset! "
                         "(currently it is set to "
                      << prepartitioned_data_dir << ").";
                throw Exception(oconv.str());
            }

            first_call = false;
        }
    }


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    const ModelT&
    ModelNoInputData<ModelT, OutputDirWrapperT, TimeManagerT, CallRunMethodT, DoCreateDomainListForCoordsT>::GetModel()
    {
        static ModelT model(GetNonCstMoReFEMData());

        if constexpr (CallRunMethodT == call_run_method_at_first_call::yes)
        {
            static bool first_call = true;

            if (first_call)
            {
                model.Run();
                first_call = false;
            }
        }

        return model;
    }


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    ModelT& ModelNoInputData<ModelT, OutputDirWrapperT, TimeManagerT, CallRunMethodT, DoCreateDomainListForCoordsT>::
        GetNonCstModel()
    {
        return const_cast<ModelT&>(GetModel());
    }


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    auto ModelNoInputData<ModelT, OutputDirWrapperT, TimeManagerT, CallRunMethodT, DoCreateDomainListForCoordsT>::
        GetNonCstMoReFEMData() -> morefem_data_type&
    {
        static auto ret{ morefem_data_type(OutputDirWrapperT::Path()) };
        return ret;
    }


} // namespace MoReFEM::TestNS::FixtureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_FIXTURE_MODELNOINPUTDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
