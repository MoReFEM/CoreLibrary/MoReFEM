// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <utility>

#include "Test/Tools/Fixture/TestEnvironment.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"

#include "Core/CommandLineFlags/Advanced/CommandLineFlags.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    TestEnvironment::TestEnvironment()
    {
        static bool first_call = true;

        if (first_call)
        {
            decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
            auto* cli_args = master_test_suite.argv;

            BOOST_CHECK(master_test_suite.argc > 2);

            {
                // First set environment variables, which by convention for tests are the second and third argument
                // in CLI.
                decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();

                environment.SetEnvironmentVariable(std::make_pair("MOREFEM_ROOT", cli_args[1]));
                environment.SetEnvironmentVariable(std::make_pair("MOREFEM_TEST_OUTPUT_DIR", cli_args[2]));
            }

            {
                Internal::PetscNS::RAII::CreateOrGetInstance(
                    std::source_location::current(), master_test_suite.argc, master_test_suite.argv);

#ifdef MOREFEM_WITH_SLEPC
                Internal::SlepcNS::RAII::CreateOrGetInstance();
#endif // MOREFEM_WITH_SLEPC
            }

            // Init the singleton which stores flag values - for models - with values adapted for tests.
            Advanced::CommandLineFlagsNS::InitCommandLineFlagsForTests(std::source_location::current());

            first_call = false;
        }
    }


    const Wrappers::Mpi& TestEnvironment::GetMpi()
    {
        decltype(auto) raii = Internal::PetscNS::RAII::GetInstance();
        return raii.GetMpi();
    }


} // namespace MoReFEM::TestNS::FixtureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
