// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_INITMOREFEMDATAFROMCLI_DOT_HXX_
#define MOREFEM_TEST_TOOLS_INITMOREFEMDATAFROMCLI_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/InitMoReFEMDataFromCLI.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


namespace MoReFEM::TestNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::TimeManager TimeManagerT
    >
    // clang-format on
    MoReFEMData<ModelSettingsT, InputDataT, TimeManagerT, program_type::test> InitMoReFEMDataFromCLI()
    {
        decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
        auto* cli_args = master_test_suite.argv;

        BOOST_CHECK(master_test_suite.argc > 3);

        FilesystemNS::File lua_file{ std::filesystem::path{ cli_args[3] } };

        return MoReFEMData<ModelSettingsT, InputDataT, TimeManagerT, program_type::test>{ std::move(lua_file) };
    }


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_INITMOREFEMDATAFROMCLI_DOT_HXX_
// *** MoReFEM end header guards *** < //
