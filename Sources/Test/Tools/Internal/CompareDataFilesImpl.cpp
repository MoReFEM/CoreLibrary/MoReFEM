// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <source_location>
#include <sstream>
#include <string>
#include <vector>

#include "Test/Tools/Internal/CompareDataFilesImpl.hpp"

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"


namespace MoReFEM::TestNS::Internal
{


    CompareDataFilesImpl::CompareDataFilesImpl(const FilesystemNS::Directory& ref_dir,
                                               const FilesystemNS::Directory& obtained_dir,
                                               std::string&& filename,
                                               double epsilon,
                                               const std::source_location location)
    : epsilon_{ epsilon }
    {
        std::ostringstream oconv;

        if (!ref_dir.DoExist())
        {
            oconv << "Reference folder " << ref_dir << " does not exist.";
            throw Exception(oconv.str(), location);
        }

        if (!obtained_dir.DoExist())
        {
            oconv << "Result folder " << obtained_dir << " does not exist.";
            throw Exception(oconv.str(), location);
        }

        reference_file_ = ::MoReFEM::FilesystemNS::File{ ref_dir.AddFile(filename) };
        obtained_file_ = ::MoReFEM::FilesystemNS::File{ obtained_dir.AddFile(filename) };

        if (!reference_file_.DoExist())
        {
            oconv << "Reference file " << reference_file_ << " does not exist.";
            throw Exception(oconv.str(), location);
        }

        if (!obtained_file_.DoExist())
        {
            oconv << "Result file " << obtained_file_ << " does not exist.";
            throw Exception(oconv.str(), location);
        }
    }


    void CompareDataFilesImpl::CheckAreEquals(const std::vector<double>& ref,
                                              const std::vector<double>& obtained,
                                              const std::source_location location) const
    {
        assert(ref.size() == obtained.size()
               && "Should have been tested properly by the test facility "
                  "before this function call.");

        const auto end_obtained = obtained.cend();

        for (auto it_obtained = obtained.cbegin(), it_ref = ref.cbegin(); it_obtained != end_obtained;
             ++it_obtained, ++it_ref)
        {
            const auto obtained_value = *it_obtained;
            const auto ref_value = *it_ref;

            // Crude condition, that is ok for most of the cases!
            if (!NumericNS::AreEqual(ref_value, obtained_value, epsilon_))
            {
                std::ostringstream oconv;
                oconv << it_ref - ref.cbegin() << "-th value in " << reference_file_
                      << " is not identical to what "
                         "was stored as a reference in file "
                      << obtained_file_ << "(values are respectively " << ref_value << " and " << obtained_value
                      << ").";
                throw Exception(oconv.str(), location);
            }
        }
    }


} // namespace MoReFEM::TestNS::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
