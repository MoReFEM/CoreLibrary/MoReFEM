// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_INTERNAL_COMPAREDATAFILESIMPL_DOT_HXX_
#define MOREFEM_TEST_TOOLS_INTERNAL_COMPAREDATAFILESIMPL_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/Internal/CompareDataFilesImpl.hpp"
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS::Internal
{


    inline const FilesystemNS::File& CompareDataFilesImpl::GetReferenceFile() const noexcept
    {
        return reference_file_;
    }


    inline const FilesystemNS::File& CompareDataFilesImpl::GetObtainedFile() const noexcept
    {
        return obtained_file_;
    }


    inline double CompareDataFilesImpl::GetEpsilon() const noexcept
    {
        return epsilon_;
    }


} // namespace MoReFEM::TestNS::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_INTERNAL_COMPAREDATAFILESIMPL_DOT_HXX_
// *** MoReFEM end header guards *** < //
