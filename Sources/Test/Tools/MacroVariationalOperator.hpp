// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_MACROVARIATIONALOPERATOR_DOT_HPP_
#define MOREFEM_TEST_TOOLS_MACROVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

/*!
 * \class doxygen_hide_TEST_VARIATIONAL_OPERATOR
 *
 * \brief Convenient macro for testing operators that acts upon both scalar and vectorial unknowns.
 * It's ugly as the code is duplicated just to ignore a pragma only for clang compiler, but I can't
 * use something else than the macro I despised as Boost test is itself relying heavily on them.
 */

#ifdef __clang__
//! \copydoc doxygen_hide_TEST_VARIATIONAL_OPERATOR
#define TEST_VARIATIONAL_OPERATOR                                                                                      \
    PRAGMA_DIAGNOSTIC(push)                                                                                            \
    PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")                                                            \
                                                                                                                       \
    BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)                                                      \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p1_scalar_same)                                                                               \
    {                                                                                                                  \
        GetModel().SameUnknownP1(UnknownNS::Nature::scalar);                                                           \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p1_vectorial_same)                                                                            \
    {                                                                                                                  \
        GetModel().SameUnknownP1(UnknownNS::Nature::vectorial);                                                        \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p2_scalar_same)                                                                               \
    {                                                                                                                  \
        GetModel().SameUnknownP2();                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_SUITE_END()                                                                                        \
                                                                                                                       \
    BOOST_FIXTURE_TEST_SUITE(different_unknown, fixture_type)                                                          \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p1_scalar_same)                                                                               \
    {                                                                                                                  \
        GetModel().UnknownP1TestP1();                                                                                  \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(unknown_p2_test_p1)                                                                           \
    {                                                                                                                  \
        GetModel().UnknownP2TestP1();                                                                                  \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_SUITE_END()                                                                                        \
                                                                                                                       \
    PRAGMA_DIAGNOSTIC(pop)


#else // __clang__

//! \copydoc doxygen_hide_TEST_VARIATIONAL_OPERATOR
#define TEST_VARIATIONAL_OPERATOR                                                                                      \
    BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)                                                      \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p1_scalar_same)                                                                               \
    {                                                                                                                  \
        GetModel().SameUnknownP1(UnknownNS::Nature::scalar);                                                           \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p1_vectorial_same)                                                                            \
    {                                                                                                                  \
        GetModel().SameUnknownP1(UnknownNS::Nature::vectorial);                                                        \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p2_scalar_same)                                                                               \
    {                                                                                                                  \
        GetModel().SameUnknownP2();                                                                                    \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_SUITE_END()                                                                                        \
                                                                                                                       \
    BOOST_FIXTURE_TEST_SUITE(different_unknown, fixture_type)                                                          \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(p1_scalar_same)                                                                               \
    {                                                                                                                  \
        GetModel().UnknownP1TestP1();                                                                                  \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_CASE(unknown_p2_test_p1)                                                                           \
    {                                                                                                                  \
        GetModel().UnknownP2TestP1();                                                                                  \
    }                                                                                                                  \
                                                                                                                       \
    BOOST_AUTO_TEST_SUITE_END()

#endif // __clang__

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_MACROVARIATIONALOPERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
