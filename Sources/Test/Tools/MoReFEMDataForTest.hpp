// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_MOREFEMDATAFORTEST_DOT_HPP_
#define MOREFEM_TEST_TOOLS_MOREFEMDATAFORTEST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <filesystem>
#include <memory>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Internal/EmptyInputData.hpp"

// IWYU pragma: begin_exports
#include "Utilities/InputData/InputData.hpp"
#include "Utilities/InputData/ModelSettings.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"

#include "Core/MoReFEMData/Enum.hpp"
#include "Core/MoReFEMData/Internal/AbstractClass.hpp"
#include "Core/MoReFEMData/Internal/Helper.hpp"
#include "Core/MoReFEMData/Internal/Parallelism.hpp"
#include "Core/TimeManager/Instances.hpp"
// IWYU pragma: end_exports


namespace MoReFEM
{


    /*!
     * \brief A specific version of \a MoReFEMData which is to be used in most of the tests written after July 2023.
     *
     * Classical \a MoReFEMData holds `InputData` information, but for tests we typically do not need it: there
     * are little reasons to provide data that may be modified by the end user through a Lua file, and it is more
     * sensible to set the data through a `ModelSettings` object instead.
     *
     * The purpose of current class is to provide an alternate for \a MoReFEMData just for those tests; it should not
     * be used outside of this use case for genuine models.
     *
     */
    // clang-format off
    template
    <
        class ModelSettingsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    class MoReFEMDataForTest
    : public Internal::MoReFEMDataNS::
          AbstractClass<program_type::test, TimeManagerT, ModelSettingsT, Internal::InputDataNS::EmptyInputData>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MoReFEMDataForTest<ModelSettingsT, TimeManagerT>;

        //! Alias to parent.
        // clang-format off
        using parent =
            Internal::MoReFEMDataNS::AbstractClass
            <
                program_type::test,
                TimeManagerT,
                ModelSettingsT,
                Internal::InputDataNS::EmptyInputData
            >;
        // clang-format on

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_input_data_type_alias
        //!
        //! Here it is a dedicated class that holds no value.
        using input_data_type = Internal::InputDataNS::EmptyInputData;

        //! Alias to model settings type.
        using model_settings_type = typename parent::model_settings_type;

        static_assert(std::is_same<model_settings_type, ModelSettingsT>());

        //! Alias to the object in charge of time management.
        using time_manager_type = TimeManagerT;

        //! Helper variable to define the \a MoReFEMDataType concept.
        static inline constexpr is_morefem_data ConceptIsMoReFEMData = is_morefem_data::for_test;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] result_directory_path The path of the
         * output directory to place the outputs specifically related to the test considered.
         */
        explicit MoReFEMDataForTest(const std::filesystem::path& result_directory_path);

        //! Destructor.
        virtual ~MoReFEMDataForTest() override;

        //! \copydoc doxygen_hide_copy_constructor
        MoReFEMDataForTest(const MoReFEMDataForTest& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MoReFEMDataForTest(MoReFEMDataForTest&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MoReFEMDataForTest& operator=(const MoReFEMDataForTest& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MoReFEMDataForTest& operator=(MoReFEMDataForTest&& rhs) = delete;

        ///@}

      public:
        //! No \a Parallelism object foreseen so far so this method returns `nullptr`.
        const Internal::Parallelism* GetParallelismPtr() const noexcept;

        //! Specify there are no `Parallelism` field.
        static constexpr bool HasParallelismField()
        {
            return false;
        }
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Test/Tools/MoReFEMDataForTest.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_MOREFEMDATAFORTEST_DOT_HPP_
// *** MoReFEM end header guards *** < //
