// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_NUMBERINGSUBSETCREATOR_DOT_HPP_
#define MOREFEM_TEST_TOOLS_NUMBERINGSUBSETCREATOR_DOT_HPP_
// *** MoReFEM header guards *** < //

// Due to specific use in tests we do not especially leverage forward declaration here.
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS
{


    /*!
     * \brief Structure to create on the fly new \a NumberingSubset objects.
     */
    struct NumberingSubsetCreator
    {


        /*!
         * \brief Creates on the fly a new \a NumberingSubset with given \a unique_id.
         *
         * We just call the private method of \a NumberingSubsetManager; therefore we take advantage of safeties against
         * two same \a unique_id.
         *
         * \param[in] unique_id Unique Id of the \a NumberingSubset to create.
         *
         * \return Reference to the newly created \a NumberingSubset.
         */
        static const NumberingSubset& Create(::MoReFEM::NumberingSubsetNS::unique_id unique_id);
    };


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_NUMBERINGSUBSETCREATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
