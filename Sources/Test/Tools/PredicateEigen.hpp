// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_PREDICATEEIGEN_DOT_HPP_
#define MOREFEM_TEST_TOOLS_PREDICATEEIGEN_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::TestNS::EigenNS
{


    /*!
     * \class doxygen_hide_eigen_predicate_for_boost_test
     *
     * When we want to compare two Eigen vectors or matrices, we also would like in case of failure
     * that both expected and result are printed on screen.
     *
     * The way to achieve that with Boost test is to use BOOST_CHECK_PREDICATE macro, which expects as parameter:
     * - A predicate
     * - Arguments to pass to this predicate.
     *
     * On the other hand, Eigen uses up a method to do the same.
     *
     * Goal of present function is to provide Boost test the interface it needs to do the comparison.
     *
     * \param[in] lhs Left-hand side
     * \param[in] rhs Right-hand side
     * \param[in] epsilon The precision used by Eigen; documentation is
     * [here](https://eigen.tuxfamily.org/dox/classEigen_1_1DenseBase.html#ae8443357b808cd393be1b51974213f9c)
     */

    /*!
     * \brief Provide a predicate that meshes well with Boost test macros to check two vectors or matrices are equal.
     *
     * \copydetails doxygen_hide_eigen_predicate_for_boost_test
     *
     * \return True if both linear algebra are deemed equal (or close enough)
     */
    template<class LinAlgT>
    bool PredicateEqual(const LinAlgT& lhs, const LinAlgT& rhs, double epsilon);


    /*!
     * \brief Provide a predicate that meshes well with Boost test macros to check two vectors or matrices are not equal.
     *
     * \copydetails doxygen_hide_eigen_predicate_for_boost_test
     *
     * \return True if both linear algebra are **NOT** deemed equal (or close enough)
     */
    template<class LinAlgT>
    bool PredicateNotEqual(const LinAlgT& lhs, const LinAlgT& rhs, double epsilon);


} // namespace MoReFEM::TestNS::EigenNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Test/Tools/PredicateEigen.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_PREDICATEEIGEN_DOT_HPP_
// *** MoReFEM end header guards *** < //
