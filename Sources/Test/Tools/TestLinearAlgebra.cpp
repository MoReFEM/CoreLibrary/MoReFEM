// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <format>
#include <ios>
#include <iostream>
#include <memory>
#include <source_location>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "Test/Tools/TestLinearAlgebra.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Test/Tools/Exceptions/Exception.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS
{


    namespace // anonymous
    {


        /*!
         * \brief Analyze string rendering of lhs and rhs until they differ.
         *
         * This function is far from being efficient and `std::format` would enable to write it more cleanly,
         * but as the function is used when an exception is thrown it's not really an issue.
         *
         * \attention This is expected to be called when lhs and rhs are not deemed equald by `AreEqual` function.
         * There is no assert to check this as it is an anonymous function, but don't pass it with strictly equals lhs
         * and rhs.
         *
         * \param[in] precision_limit If both strings are still equal when this precision is reached, stop the loop
         * and return false.
         *
         * \return The string rendering for lhs and rhs for the precision that showed a difference. If none found because
         * precision_limit is reached; it is expected to return two identifcal values; it is not the job of current
         * function to check it.
         */
        std::pair<std::string, std::string>
        FindDivergentExpression(double lhs, double rhs, std::streamsize precision_limit = 16);


        /*!
         * \brief The fallback when there is a discrepancy in direct comparison.
         *
         * \copydoc doxygen_hide_test_global_linear_alg_common_arg
         *
         * \param[in] obtained The matrix obtained by the computation. As it is for a test, it is expected to be a
         * fairly small matrix.
         * \param[in] expected The expected (program-wise) matrix, given in dense format. Interior vector features all
         * the values on a given row.
         *
         * If there is still a discrepancy a \a TestNS::ExceptionNS::MismatchLinearAlgebra exception is thrown.
         *
         */
        void CheckSortMatrixLexicographically(const Wrappers::Mpi& mpi,
                                              const GlobalMatrix& obtained,
                                              const std::vector<std::vector<PetscScalar>>& expected,
                                              double epsilon = 1.e-12,
                                              std::source_location location = std::source_location::current());


        /*!
         * \brief The fallback when there is a discrepancy in direct comparison.
         *
         * \copydoc doxygen_hide_test_global_linear_alg_common_arg
         *
         * \param[in] obtained The vector obtained by the computation. As it is for a test, it is expected to be a
         * fairly small vector.
         * \param[in] expected The expected (program-wise) vector, given in dense format. Interior vector features all
         * the values on a given row.
         *
         * If there is still a discrepancy a \a TestNS::ExceptionNS::MismatchLinearAlgebra exception is thrown.
         *
         */
        void CheckSortVectorLexicographically(const Wrappers::Mpi& mpi,
                                              const GlobalVector& obtained,
                                              const std::vector<PetscScalar>& expected,
                                              double epsilon = 1.e-12,
                                              std::source_location location = std::source_location::current());


    } // namespace


    void CheckMatrix(const GodOfDof& god_of_dof,
                     const GlobalMatrix& obtained,
                     const std::vector<std::vector<PetscScalar>>& expected,
                     const double epsilon,
                     const std::source_location location)
    {
        auto [Nrow, Ncol] = obtained.GetProgramWiseSize(location);

        const auto Nexpected_program_wise_row = expected.size();

        assert(Nexpected_program_wise_row > 0);

        const auto Ncol_first_row = expected.front().size();

#ifndef NDEBUG
        {
            for (auto i = 1UL; i < Nexpected_program_wise_row; ++i)
            {
                assert("The expected matrix is assumed to be dense in this function."
                       && expected[i].size() == Ncol_first_row);
            }
        }
#endif // NDEBUG

        if (Ncol_first_row != static_cast<std::size_t>(Ncol.Get()))
        {
            auto msg = std::format("Mismatch between expected and obtained matrix format: a matrix with {}"
                                   " columns was expected but the obtained matrix got {}.",
                                   Ncol_first_row,
                                   Ncol.Get());

            throw TestNS::ExceptionNS::MismatchLinearAlgebra(msg, location);
        }


        if (Nexpected_program_wise_row != static_cast<std::size_t>(Nrow.Get()))
        {
            auto msg = std::format("Mismatch between expected and obtained matrix format: a matrix with {}"
                                   " program-wise rows was expected but the obtained matrix got {}.",
                                   Nexpected_program_wise_row,
                                   Nrow.Get());

            throw TestNS::ExceptionNS::MismatchLinearAlgebra(msg, location);
        }

        decltype(auto) row_numbering_subset = obtained.GetRowNumberingSubset();

        decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();

        try
        {

            for (const auto& dof_ptr : processor_wise_dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;

                if (!dof.IsInNumberingSubset(row_numbering_subset))
                    continue;

                const auto program_wise_index = row_program_wise_index_type{ static_cast<PetscInt>(
                    dof.GetProgramWiseIndex(row_numbering_subset).Get()) };

                assert(program_wise_index.Get() < static_cast<PetscInt>(expected.size())
                       && "Should be guaranteed by previous asserts!");
                decltype(auto) expected_row = expected[static_cast<std::size_t>(program_wise_index.Get())];

                for (auto col = col_program_wise_index_type{}; col < Ncol; ++col)
                {
                    // Remember: Petsc convention is that 0 is returned if location is sparse.
                    const auto obtained_value = obtained.GetValue(program_wise_index, col, location);

                    const auto expected_value = expected_row[static_cast<std::size_t>(col.Get())];

                    if (!NumericNS::AreEqual(obtained_value, expected_value, epsilon))
                    {
                        auto [obtained_str, expected_str] = FindDivergentExpression(obtained_value, expected_value);

                        std::string msg;

                        if (obtained_str != expected_str)
                            msg = std::format("Mismatch between expected and obtained matrix: value obtained "
                                              "for program-wise ({}, {}) was {} whereas {} was expected.",
                                              program_wise_index,
                                              col,
                                              obtained_str,
                                              expected_str);
                        else
                            msg = std::format("Mismatch between expected and obtained matrix for the value obtained "
                                              "for program-wise ({}, {}); however the difference is so small it "
                                              "doesn't appear in string rendering. Probably loosen up the epsilon "
                                              "chosen in the comparison.",
                                              program_wise_index,
                                              col);

                        throw TestNS::ExceptionNS::MismatchLinearAlgebra(msg, location);
                    }
                }
            }
        }
        catch (const TestNS::ExceptionNS::MismatchLinearAlgebra& e)
        {
            std::cout << "[WARNING] The direct comparison of expected and obtained matrix fails, with the "
                         "error message: "
                      << e.what() << "\n";
            std::cout
                << "However, this might be due to a change of ordering convention, for instance if the "
                   "compiler is not the same as the one used to create the expected result (MoReFEM uses up "
                   "std::unordered_map for efficiency reasons but it means the indexing might not be the same in the "
                   "end, even in a sequentiel run - see #1954 for more details).\n";
            std::cout
                << "A loosen check will be attempted: we will check values inside the matrices are the same "
                   "row by row (each row might be matched with another of course). If it fails, an exception will be "
                   "thrown again.\n";

            CheckSortMatrixLexicographically(god_of_dof.GetMpi(), obtained, expected, epsilon, location);
        }
    }


    void CheckVector(const GodOfDof& god_of_dof,
                     const GlobalVector& obtained,
                     const std::vector<PetscScalar>& expected,
                     const double epsilon,
                     const std::source_location location)
    {
        const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content(obtained, location);

        const auto size = obtained.GetProgramWiseSize(location);

        if (size != vector_program_wise_index_type{ static_cast<PetscInt>(expected.size()) })
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained vector format: a vector with " << expected.size()
                  << " rows was expected but the obtained vector got " << size << '.';
            throw Exception(oconv.str(), location);
        }

        decltype(auto) numbering_subset = obtained.GetNumberingSubset();
        decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();

        try
        {

            for (const auto& dof_ptr : processor_wise_dof_list)
            {
                assert(!(!dof_ptr));
                const auto& dof = *dof_ptr;

                if (!dof.IsInNumberingSubset(numbering_subset))
                    continue;

                const auto processor_wise_index = dof.GetProcessorWiseOrGhostIndex(numbering_subset);

                const auto program_wise_index = dof.GetProgramWiseIndex(numbering_subset);

                const auto obtained_value = content.GetValue(DofNS::ToVectorIndex(processor_wise_index));

                assert(static_cast<std::size_t>(processor_wise_index) < expected.size());
                const auto expected_value = expected[static_cast<std::size_t>(program_wise_index)];

                if (!NumericNS::AreEqual(obtained_value, expected_value, epsilon))
                {
                    auto [obtained_str, expected_str] = FindDivergentExpression(obtained_value, expected_value);

                    std::ostringstream oconv;

                    if (obtained_str != expected_str)
                        oconv << "Mismatch between expected and obtained vector: value obtained for program-wise ("
                              << dof.GetProgramWiseIndex(numbering_subset) << ") was " << obtained_str << " whereas "
                              << expected_str << " was expected.";
                    else
                        oconv
                            << "Mismatch between expected and obtained vector for the value obtained for program-wise ("
                            << dof.GetProgramWiseIndex(numbering_subset)
                            << "); however the difference is so small it "
                               "doesn't appear in string rendering. Probably loosen up the epsilon chosen in the "
                               "comparison.";

                    throw TestNS::ExceptionNS::MismatchLinearAlgebra(oconv.str(), location);
                }
            }
        }
        catch (const TestNS::ExceptionNS::MismatchLinearAlgebra& e)
        {
            std::cout << "[WARNING] The direct comparison of expected and obtained vectors fails, with the "
                         "error message: "
                      << e.what() << "\n";
            std::cout
                << "However, this might be due to a change of ordering convention, for instance if the "
                   "compiler is not the same as the one used to create the expected result (MoReFEM uses up "
                   "std::unordered_map for efficiency reasons but it means the indexing might not be the same in the "
                   "end, even in a sequentiel run - see #1954 for more details).\n";
            std::cout << "A loosen check will be attempted: we just check the values are overall the same, regardless "
                         "of their positions. If it fails, an exception will be thrown again.\n";

            CheckSortVectorLexicographically(god_of_dof.GetMpi(), obtained, expected, epsilon, location);
        }
    }


    namespace // anonymous
    {


        /*!
         * \brief Analyze string rendering of lhs and rhs until they differ.
         *
         * This function is far from being efficient and `std::format` would enable to write it more cleanly,
         * but as the function is used when an exception is thrown it's not really an issue.
         *
         * \attention This is expected to be called when lhs and rhs are not deemed equald by `AreEqual` function.
         * There is no assert to check this as it is an anonymous function, but don't pass it with strictly equals lhs
         * and rhs.
         *
         * \param[in] precision_limit If both strings are still equal when this precision is reached, stop the loop
         * and return false.
         *
         * \return The string rendering for lhs and rhs for the precision that showed a difference. If none found because
         * precision_limit is reached; it is expected to return two identifcal values; it is not the job of current
         * function to check it.
         */
        std::pair<std::string, std::string>
        FindDivergentExpression(double lhs, double rhs, std::streamsize precision_limit)
        {
            std::ostringstream lhs_conv;
            std::ostringstream rhs_conv;

            lhs_conv << lhs;
            rhs_conv << rhs;

            if (lhs_conv.str() != rhs_conv.str())
                return { lhs_conv.str(), rhs_conv.str() };

            auto precision = lhs_conv.precision();

            while ((lhs_conv.str() == rhs_conv.str()) && (precision < precision_limit))
            {
                ++precision;
                lhs_conv.precision(precision);
                rhs_conv.precision(precision);

                lhs_conv.str("");
                rhs_conv.str("");

                lhs_conv << lhs;
                rhs_conv << rhs;
            }

            return { lhs_conv.str(), rhs_conv.str() };
        }


        void CheckSortMatrixLexicographically([[maybe_unused]] const Wrappers::Mpi& mpi,
                                              const GlobalMatrix& obtained,
                                              const std::vector<std::vector<PetscScalar>>& expected,
                                              double epsilon,
                                              const std::source_location location)
        {
            assert(mpi.IsSequential()
                   && "Crude test that would likely fail - GetRow() is "
                      "very touchy about ranks... And it's anyway intended to be used for operators tests "
                      "that are done in sequential only.");

            const auto [Nrow, Ncol] = obtained.GetProgramWiseSize(location);

            // ===================================================================
            // Obtained matrix: read each row and sort it by increasing value.
            // Rows are then sort likewise.
            // ===================================================================
            std::vector<std::vector<PetscScalar>> row_obtained_sort;

            for (auto row = row_program_wise_index_type{}; row < Nrow; ++row)
            {
                std::vector<PetscInt> row_content_position_list;
                std::vector<PetscScalar> row_content_value_list;

                obtained.GetRow(row, row_content_position_list, row_content_value_list, location);
                // Pad with zero if there are non zero values.
                row_content_value_list.resize(static_cast<std::size_t>(Ncol.Get()), 0.);

                std::ranges::sort(row_content_value_list);

                row_obtained_sort.emplace_back(std::move(row_content_value_list));
            }

            // Then sort the rows themselves by lexicographical order.
            std::ranges::sort(row_obtained_sort);


            // ===================================================================
            // Expected matrix: do the same lexographical ordering
            // ===================================================================
            std::vector<std::vector<PetscScalar>> expected_sort = expected;

            for (auto& row : expected_sort)
                std::ranges::sort(row);

            // Then sort the rows themselves by lexicographical order.
            std::ranges::sort(expected_sort);

            // ===================================================================
            // Now the expected and obtained should exactly match!
            // ===================================================================
            for (auto row = row_program_wise_index_type{}; row < Nrow; ++row)
            {
                const auto row_index = static_cast<std::size_t>(row.Get());
                for (auto col = col_program_wise_index_type{}; col < Ncol; ++col)
                {
                    const auto col_index = static_cast<std::size_t>(col.Get());

                    if (!NumericNS::AreEqual(
                            row_obtained_sort[row_index][col_index], expected_sort[row_index][col_index], epsilon))
                    {
                        std::ostringstream oconv;
                        oconv << "Even with loosening the check and only comparing sort row contents, obtained "
                                 "and expected matrices don't match.";

                        throw TestNS::ExceptionNS::MismatchLinearAlgebra(oconv.str(), location);
                    }
                }
            }
        }


        void CheckSortVectorLexicographically([[maybe_unused]] const Wrappers::Mpi& mpi,
                                              const GlobalVector& obtained,
                                              const std::vector<PetscScalar>& expected,
                                              double epsilon,
                                              std::source_location location)
        {
            assert(mpi.IsSequential() && "Intended to be used for operators tests that are done in sequential only.");

            const auto size = obtained.GetProcessorWiseSize(location);

            // ===================================================================
            // Sort obtained vector values.
            // ===================================================================
            std::vector<PetscScalar> obtained_sort;
            obtained_sort.reserve(static_cast<std::size_t>(size.Get()));

            const Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content(obtained, location);

            for (auto index = vector_processor_wise_index_type{}; index < size; ++index)
                obtained_sort.push_back(content.GetValue(index));

            std::ranges::sort(obtained_sort);

            // ===================================================================
            // Expected vector: do the same lexographical ordering
            // ===================================================================
            std::vector<PetscScalar> expected_sort = expected;

            std::ranges::sort(expected_sort);

            // ===================================================================
            // Now the expected and obtained should exactly match!
            // ===================================================================
            for (auto index = 0UL; index < static_cast<std::size_t>(size.Get()); ++index)
            {
                if (!NumericNS::AreEqual(obtained_sort[index], expected_sort[index], epsilon))
                {
                    auto msg = std::format("Even with loosening the check and only comparing sort contents, obtained "
                                           "and expected vectors don't match.");
                    throw TestNS::ExceptionNS::MismatchLinearAlgebra(msg, location);
                }
            }
        }


    } // namespace


} // namespace MoReFEM::TestNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
