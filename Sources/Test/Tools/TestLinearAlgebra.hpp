// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HPP_
#define MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "Test/Tools/Exceptions/Exception.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GodOfDof; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::TestNS
{


    /*!
     * \class doxygen_hide_test_global_linear_alg_common_arg
     *
     * The function is written to work both in sequential and in parallel; in parallel ghost values aren't checked.
     *
     * \copydoc doxygen_hide_test_linear_alg_common_arg
     * \param[in] god_of_dof \a GodOfDof onto which the dofs present in the matrix or vector checked is defined.
     */


    /*!
     * \brief Check whether a Petsc matrix includes the expected content.
     *
     * \copydoc doxygen_hide_test_global_linear_alg_common_arg
     *
     * \param[in] obtained The matrix obtained by the computation. As it is for a test, it is expected to be a
     * fairly small matrix.
     * \param[in] expected The expected (program-wise) matrix, given in dense format. Interior vector features all
     * the values on a given row.
     *
     * \attention First level of check is to verify that all the rows are close enough. However, as there are unordered
     * component in the indexing, this direct test may fail due to different indexing (another compiler may choose
     * another behaviour for its `std::unordered_map` implementation). In this case, we fall back to a more brute-force
     * check: the content of the matrix is sort lexicographically, the expected matrix as well, and we check those are
     * close enough. There is a warning message in this case, but the test is declared passed if this second check is.
     * See #1954 for more details.
     */
    void CheckMatrix(const GodOfDof& god_of_dof,
                     const GlobalMatrix& obtained,
                     const std::vector<std::vector<PetscScalar>>& expected,
                     double epsilon = 1.e-12,
                     const std::source_location location = std::source_location::current());


    /*!
     * \brief Check whether a Petsc vector includes the expected content.
     *
     * \copydoc doxygen_hide_test_global_linear_alg_common_arg
     *
     * \param[in] obtained The vector obtained by the computation.
     * \param[in] expected The expected (program-wise) values to be found in the vector.
     */
    void CheckVector(const GodOfDof& god_of_dof,
                     const GlobalVector& obtained,
                     const std::vector<PetscScalar>& expected,
                     double epsilon = 1.e-12,
                     const std::source_location location = std::source_location::current());


    /*!
     * \brief Check whether a local matrix includes the expected content.
     *
     * \copydoc doxygen_hide_test_linear_alg_common_arg
     *
     * \param[in] obtained The matrix obtained by the computation.
     * \param[in] expected The expected matrix.
     *
     * Note: you may also use Eigen's
     * [`isApprox`](https://eigen.tuxfamily.org/dox/classEigen_1_1DenseBase.html#ae8443357b808cd393be1b51974213f9c)
     * which is more efficient but will provide a slightly terser error message.
     */
    template<class LocalMatrixT>
    void CheckLocalMatrix(const LocalMatrixT& obtained,
                          const LocalMatrixT& expected,
                          double epsilon = 1.e-12,
                          const std::source_location location = std::source_location::current());

    /*!
     * \brief Check whether a local vector includes the expected content.
     *
     * \copydoc doxygen_hide_test_linear_alg_common_arg
     *
     * \param[in] obtained The vector obtained by the computation.
     * \param[in] expected The expected vector
     *
     * Note: you may also use Eigen's
     * [`isApprox`](https://eigen.tuxfamily.org/dox/classEigen_1_1DenseBase.html#ae8443357b808cd393be1b51974213f9c)
     * which is more efficient but will provide a slightly terser error message.
     */
    template<class LocalVectorT>
    void CheckLocalVector(const LocalVectorT& obtained,
                          const LocalVectorT& expected,
                          double epsilon = 1.e-12,
                          const std::source_location location = std::source_location::current());


} // namespace MoReFEM::TestNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Tools/TestLinearAlgebra.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HPP_
// *** MoReFEM end header guards *** < //
