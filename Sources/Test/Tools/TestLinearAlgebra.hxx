// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HXX_
#define MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/TestLinearAlgebra.hpp"
// *** MoReFEM header guards *** < //

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS
{


    template<class LocalMatrixT>
    void CheckLocalMatrix(const LocalMatrixT& obtained,
                          const LocalMatrixT& expected,
                          double epsilon,
                          const std::source_location location)
    {
        if ((obtained.rows() != expected.rows()) || (obtained.cols() != expected.cols()))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local matrix: shape is not the same.";
            throw Exception(oconv.str(), location);
        }

        const auto diff = obtained - expected;

        if (!diff.isZero(epsilon))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local matrix: values don't match." << '\n';
            oconv << "Expected = " << expected << '\n';
            oconv << "Obtained = " << obtained << '\n';
            throw Exception(oconv.str(), location);
        }
    }


    template<class LocalVectorT>
    void CheckLocalVector(const LocalVectorT& obtained,
                          const LocalVectorT& expected,
                          double epsilon,
                          const std::source_location location)
    {
        if (obtained.cols() != expected.cols())
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local vector: shape is not the same.";
            throw Exception(oconv.str(), location);
        }

        const auto diff = obtained - expected;

        if (!diff.isZero(epsilon))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local vector: values don't match.";
            oconv << "Expected = " << expected << '\n';
            oconv << "Obtained = " << obtained << '\n';
            throw Exception(oconv.str(), location);
        }
    }


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HXX_
// *** MoReFEM end header guards *** < //
