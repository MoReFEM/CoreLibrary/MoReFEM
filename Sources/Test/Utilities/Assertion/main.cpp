// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <exception>
#include <string>

#define BOOST_TEST_MODULE assertion

#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(strong_type_test)
{
#ifndef NDEBUG
    const std::string message{ "Assertion only defined in debug mode." };

    try
    {
        throw Advanced::Assertion(std::string{ message }, std::source_location::current());
    }
    catch (const std::exception&)
    {
        BOOST_CHECK(false); // Assertion doesn't inherit from std::exception!
    }
    catch (const Advanced::Assertion& e)
    {
        const auto& what = e.what();

        BOOST_CHECK(Utilities::String::EndsWith(what, message));
    }
#endif
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
