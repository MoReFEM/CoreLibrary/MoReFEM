add_executable(TestBinarySerialization)

target_sources(TestBinarySerialization
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
 
target_link_libraries(TestBinarySerialization
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)


morefem_boost_test_both_modes(NAME BinarySerialization
                              EXE TestBinarySerialization                              
                              TIMEOUT 20)

morefem_organize_IDE(TestBinarySerialization Test/Utilities Test/Utilities/BinarySerialization)
