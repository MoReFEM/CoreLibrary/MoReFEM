include(${CMAKE_CURRENT_LIST_DIR}/Assertion/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/BinarySerialization/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Environment/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Filesystem/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/GetTypeName/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/InputData/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/IsSmartPtr/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/NowAsString/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Numeric/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/OstreamFormatter/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/PrintContainer/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/SourceLocation/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/String/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/StrongType/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/Tuple/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/UniqueId/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/WrapPointers/CMakeLists.txt)



