// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>
#include <exception>
#include <filesystem>
#include <string>
#include <utility>
#define BOOST_TEST_MODULE environment
#include "Utilities/Environment/Environment.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


using namespace MoReFEM;


// NOLINTBEGIN(misc-non-private-member-variables-in-classes)
namespace // anonymous
{


    struct Fixture
    {
        Fixture();

        Utilities::Environment& environment;

        static const std::string& UnixUser();
    };


    Fixture::Fixture() : environment(Utilities::Environment::CreateOrGetInstance())
    { }


    const std::string& Fixture::UnixUser()
    {
        static const std::string ret{ std::getenv("USER") };
        // < I assume here USER is rather universal and should be defined in
        // all Unix environments.
        return ret;
    }


} // namespace
// NOLINTEND(misc-non-private-member-variables-in-classes)


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_SUITE(get_environment_variable, Fixture)

BOOST_AUTO_TEST_CASE(unix_user_as_string)
{
    BOOST_CHECK(environment.DoExist(std::string("USER")));
    BOOST_CHECK(environment.GetEnvironmentVariable(std::string("USER")) == UnixUser());
}

BOOST_AUTO_TEST_CASE(unix_user_as_char_array)
{
    BOOST_CHECK(environment.DoExist("USER"));
    BOOST_CHECK(environment.GetEnvironmentVariable("USER") == UnixUser());
}

BOOST_AUTO_TEST_CASE(unexisting_variable)
{
    BOOST_CHECK(environment.DoExist("FHLKJHFFLKJ") == false);
    BOOST_CHECK_THROW(environment.GetEnvironmentVariable("FHLKJHFFLKJ"), std::exception);
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_CASE(substitute_values, Fixture)
{
    const std::string line = "/Volumes/Blah/${USER}";
    BOOST_CHECK(environment.SubstituteValues(line) == "/Volumes/Blah/" + UnixUser());

    const std::filesystem::path path{ "/Volumes/Blah/${USER}" };
    BOOST_CHECK(environment.SubstituteValuesInPath(path) == std::filesystem::path("/Volumes/Blah/" + UnixUser()));
}


BOOST_FIXTURE_TEST_CASE(internal_storage, Fixture)
{
    BOOST_CHECK(environment.DoExist("ASDFGHJK") == false);

    /* BOOST_CHECK_NO_THROW */ (environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "42 ")));

    BOOST_CHECK(environment.DoExist("ASDFGHJK") == true);

    BOOST_CHECK(environment.GetEnvironmentVariable("ASDFGHJK") == "42 ");

    BOOST_CHECK_THROW(environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "47 ")), std::exception);

    BOOST_CHECK_THROW(environment.SetEnvironmentVariable(std::make_pair("USER", "42")), std::exception);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
