// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#define BOOST_TEST_MODULE utilities_directory

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct fixture : public TestNS::FixtureNS::TestEnvironment
    {
        fixture();


        [[nodiscard]] const std::filesystem::path& GetMainDirectory() const noexcept;

      private:
        std::filesystem::path main_directory_;
    };


    enum class stdin_case : std::uint8_t { yes, no };

    template<stdin_case CaseT>
    void SetStdin(std::ifstream& in);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// NOLINTBEGIN(readability-function-cognitive-complexity)
BOOST_FIXTURE_TEST_CASE(set_up_other_tests, fixture)
{
    // The purpose here is to remove the main output directory related to this test to
    // make sure the tests here are fully reentrant.
    decltype(auto) mpi = GetMpi();

    decltype(auto) main_directory = GetMainDirectory();

    if (mpi.IsRootProcessor())
    {
        std::filesystem::remove_all(main_directory);

        const FilesystemNS::Directory remove_if_existing(main_directory, FilesystemNS::behaviour::ignore);

        BOOST_CHECK(remove_if_existing.DoExist() == false);
        BOOST_CHECK(remove_if_existing.IsWithRank() == false);
        BOOST_CHECK(remove_if_existing.IsMpi() == false);
    }

    mpi.Barrier();
}

#ifndef NDEBUG

BOOST_FIXTURE_TEST_CASE(empty_path, fixture)
{
    const std::filesystem::path empty;

    BOOST_CHECK_THROW(const FilesystemNS::Directory create(empty, FilesystemNS::behaviour::read),
                      FilesystemNS::DirectoryNS::ExceptionNS::EmptyPath);
}

#endif // NDEBUG


BOOST_FIXTURE_TEST_CASE(create_mpi, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("CreateMpi");
    const FilesystemNS::Directory create(mpi, directory, FilesystemNS::behaviour::create);

    BOOST_CHECK(create.DoExist() == false);
    BOOST_CHECK(create.IsWithRank() == true);
    BOOST_CHECK(create.IsMpi() == true);
    BOOST_CHECK_NO_THROW(create.ActOnFilesystem());
    BOOST_CHECK(create.DoExist() == true);
    BOOST_CHECK_THROW(create.ActOnFilesystem(), Exception);

    std::ostringstream oconv;
    oconv << "Rank_" << mpi.GetRank<int>();

    auto expected = directory / std::filesystem::path(oconv.str());
    expected = std::filesystem::weakly_canonical(expected);

    BOOST_CHECK_EQUAL(create.GetDirectoryEntry().path(), expected);
}


BOOST_FIXTURE_TEST_CASE(overwrite, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("Overwrite");
    const FilesystemNS::Directory overwrite(mpi, directory, FilesystemNS::behaviour::overwrite);

    BOOST_CHECK(overwrite.DoExist() == false);
    BOOST_CHECK(overwrite.IsWithRank() == true);
    BOOST_CHECK(overwrite.IsMpi() == true);
    BOOST_CHECK_NO_THROW(overwrite.ActOnFilesystem());
    BOOST_CHECK(overwrite.DoExist() == true);
    BOOST_CHECK_NO_THROW(overwrite.ActOnFilesystem());
}


BOOST_FIXTURE_TEST_CASE(read_no_mpi, fixture)
{
    auto directory = GetMainDirectory();
    directory /= std::filesystem::path("Read");

    {
        // As Directory can't create a directory without a "Rank_" subbdir, let's use
        // directly the STL API to do so (to avoid in models of course - there is
        // a good reason this restriction is in place...)
        std::filesystem::create_directories(directory);
    }

    const FilesystemNS::Directory read(directory, FilesystemNS::behaviour::read);

    BOOST_CHECK(read.DoExist() == true);
    BOOST_CHECK(read.IsWithRank() == false);
    BOOST_CHECK(read.IsMpi() == false);
    BOOST_CHECK_NO_THROW(read.ActOnFilesystem());


    {
        auto file = directory / "file_not_directory";

        std::ofstream out(file);
        out << "Hello!" << '\n';
        out.close();

        const FilesystemNS::Directory read_not_directory(file, FilesystemNS::behaviour::read);

        BOOST_CHECK_THROW(read_not_directory.ActOnFilesystem(),
                          FilesystemNS::DirectoryNS::ExceptionNS::AlreadyExistsButNotAsDirectory);
    }

    directory /= std::filesystem::path("NotExisting");
    const FilesystemNS::Directory non_existing(directory, FilesystemNS::behaviour::read);
    BOOST_CHECK(non_existing.DoExist() == false);

    BOOST_CHECK_THROW(non_existing.ActOnFilesystem(), FilesystemNS::DirectoryNS::ExceptionNS::Unexisting);
}


BOOST_FIXTURE_TEST_CASE(read_mpi, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("CreateMpi"); // not a mistake
    const FilesystemNS::Directory read(mpi, directory, FilesystemNS::behaviour::read);

    BOOST_CHECK(read.DoExist() == true);
    BOOST_CHECK(read.IsWithRank() == true);
    BOOST_CHECK(read.IsMpi() == true);
    BOOST_CHECK_NO_THROW(read.ActOnFilesystem());

    std::ostringstream oconv;
    oconv << "Rank_" << mpi.GetRank<int>();

    {
        auto expected = directory / std::filesystem::path(oconv.str());
        expected = std::filesystem::weakly_canonical(expected);
        BOOST_CHECK_EQUAL(read.GetDirectoryEntry().path(), expected);
    }

    directory /= std::filesystem::path("NotExisting");
    const FilesystemNS::Directory non_existing(mpi, directory, FilesystemNS::behaviour::read);
    BOOST_CHECK(non_existing.DoExist() == false);

    {
        auto expected = directory / std::filesystem::path(oconv.str());
        expected = std::filesystem::weakly_canonical(expected);
        BOOST_CHECK_EQUAL(non_existing.GetDirectoryEntry().path(), expected);
    }

    BOOST_CHECK_THROW(non_existing.ActOnFilesystem(), FilesystemNS::DirectoryNS::ExceptionNS::Unexisting);
}


BOOST_FIXTURE_TEST_CASE(read_case, fixture) // 'read' can't be used here hence the _case.
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("Read");

    {
        const FilesystemNS::Directory create(mpi, directory, FilesystemNS::behaviour::create);
        create.ActOnFilesystem();
    }

    const FilesystemNS::Directory read(mpi, directory, FilesystemNS::behaviour::read);
    BOOST_CHECK_NO_THROW(read.ActOnFilesystem());

    {
        std::ostringstream oconv;
        oconv << "Rank_" << mpi.GetRank<int>();

        std::filesystem::path path{ directory };
        path /= oconv.str();

        path = std::filesystem::weakly_canonical(path);

        const FilesystemNS::Directory expected(path, FilesystemNS::behaviour::ignore);

        BOOST_CHECK_PREDICATE(FilesystemNS::IsSameDirectory, (read)(expected));

        BOOST_CHECK(expected.DoExist());
    }

    {
        std::ostringstream oconv;
        oconv << directory << "/qwert";
        const FilesystemNS::Directory read_inexistant(
            mpi, std::filesystem::path{ oconv.str() }, FilesystemNS::behaviour::read);

        BOOST_CHECK_THROW(read_inexistant.ActOnFilesystem(), FilesystemNS::DirectoryNS::ExceptionNS::Unexisting);
    }
}


BOOST_FIXTURE_TEST_CASE(quit, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("Quit");

    {
        const FilesystemNS::Directory create(mpi, directory, FilesystemNS::behaviour::create);

        create.ActOnFilesystem();
    }

    const FilesystemNS::Directory quit(mpi, directory, FilesystemNS::behaviour::quit);

    BOOST_CHECK_THROW(quit.ActOnFilesystem(), ExceptionNS::GracefulExit);
}


BOOST_FIXTURE_TEST_CASE(ask, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("Ask");

    const FilesystemNS::Directory ask(mpi, directory, FilesystemNS::behaviour::ask);
    BOOST_CHECK(ask.DoExist() == false);

    // The directory doesn't exist yet, so nothing is to be asked here and the directory should
    // be created silently when ActOnFilesystem is called.
    BOOST_CHECK_NO_THROW(ask.ActOnFilesystem());

    BOOST_CHECK(ask.DoExist() == true);

    // However if we try again to create it the prompt will be called to determine what to do.
    // Let's check the code exit gracefully when 'no' is requested.
    std::ifstream in;

    if (mpi.IsRootProcessor())
        SetStdin<stdin_case::no>(in);

    BOOST_CHECK_THROW(ask.ActOnFilesystem(), ExceptionNS::GracefulExit);

    // Now let's try a positive answer. In this case it should be overwritten.
    in.close();

    if (mpi.IsRootProcessor())
        SetStdin<stdin_case::yes>(in);

    BOOST_CHECK_NO_THROW(ask.ActOnFilesystem());
}


//// When the directory already exists ONLY for one of the non root rank...
BOOST_FIXTURE_TEST_CASE(ask_on_rank, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("AskAsymmetric");

    if (mpi.Nprocessor<int>() > 1)
    {
        if (mpi.IsRootProcessor())
        {
            const std::filesystem::path rank_2_dir = directory / std::filesystem::path("Rank_2");

            // As Directory can't create a directory without a "Rank_" subbdir, let's use
            // directly the STL API to do so (to avoid in models of course - there is
            // a good reason this restriction is in place...)
            std::filesystem::create_directories(rank_2_dir);
        }

        mpi.Barrier();

        std::ifstream in;
        SetStdin<stdin_case::yes>(in);

        const FilesystemNS::Directory ask_yes(mpi, directory, FilesystemNS::behaviour::ask);

        BOOST_CHECK_NO_THROW(ask_yes.ActOnFilesystem());
    }
}


BOOST_FIXTURE_TEST_CASE(subdirectory, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("Subdirectory");

    const FilesystemNS::Directory root_directory(mpi, directory, FilesystemNS::behaviour::overwrite);

    BOOST_CHECK_NO_THROW(root_directory.ActOnFilesystem());

    const FilesystemNS::Directory subdirectory(root_directory, "NewSubdirectory");

    BOOST_CHECK_EQUAL(root_directory.IsMpi(), subdirectory.IsMpi());
    BOOST_CHECK_EQUAL(root_directory.IsWithRank(), subdirectory.IsWithRank());

    BOOST_CHECK_NO_THROW(subdirectory.ActOnFilesystem());

    BOOST_CHECK(subdirectory.DoExist() == true);

    std::ostringstream oconv;
    oconv << "Rank_" << mpi.GetRank<int>();

    std::filesystem::path path{ directory };
    path /= oconv.str();
    path /= "NewSubdirectory";

    auto test_existence = FilesystemNS::Directory(path, FilesystemNS::behaviour::read);

    BOOST_CHECK_NO_THROW(test_existence.ActOnFilesystem());
}


BOOST_FIXTURE_TEST_CASE(many_subdirectories, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("ManySubdirectories");

    const FilesystemNS::Directory create(mpi, directory, FilesystemNS::behaviour::create);

    BOOST_CHECK_NO_THROW(create.ActOnFilesystem());

    FilesystemNS::Directory subdirectory(create,
                                         std::vector<std::string>{ "Subfolder", "Subsubfolder", "Subsubsubfolder" });

    BOOST_CHECK_NO_THROW(subdirectory.ActOnFilesystem());

    std::ostringstream oconv;
    oconv << "Rank_" << mpi.GetRank<int>();

    std::filesystem::path expected_path = directory / oconv.str() / "Subfolder/Subsubfolder/Subsubsubfolder";
    expected_path = std::filesystem::weakly_canonical(expected_path);

    BOOST_CHECK_EQUAL(subdirectory.GetPath(), expected_path);

    subdirectory.SetBehaviour(FilesystemNS::behaviour::read);
    BOOST_CHECK_NO_THROW(subdirectory.ActOnFilesystem());
}

BOOST_FIXTURE_TEST_CASE(with_rank, fixture)
{
    auto directory = GetMainDirectory();
    decltype(auto) mpi = GetMpi();

    directory /= std::filesystem::path("WithRank");
    const FilesystemNS::Directory create(mpi, directory, FilesystemNS::behaviour::create, FilesystemNS::add_rank::yes);

    BOOST_CHECK(create.IsWithRank() == true);

    BOOST_CHECK_NO_THROW(create.ActOnFilesystem());

    std::unique_ptr<FilesystemNS::Directory> directory_ptr{ nullptr };


    BOOST_CHECK_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                          mpi, directory, FilesystemNS::behaviour::create, FilesystemNS::add_rank::no),
                      FilesystemNS::DirectoryNS::ExceptionNS::SetBehaviourNoRank);

    BOOST_CHECK_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                          mpi, directory, FilesystemNS::behaviour::overwrite, FilesystemNS::add_rank::no),
                      FilesystemNS::DirectoryNS::ExceptionNS::SetBehaviourNoRank);

    BOOST_CHECK_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                          mpi, directory, FilesystemNS::behaviour::quit, FilesystemNS::add_rank::no),
                      FilesystemNS::DirectoryNS::ExceptionNS::SetBehaviourNoRank);

    BOOST_CHECK_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                          mpi, directory, FilesystemNS::behaviour::create, FilesystemNS::add_rank::no),
                      FilesystemNS::DirectoryNS::ExceptionNS::SetBehaviourNoRank);

    BOOST_CHECK_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                          mpi, directory, FilesystemNS::behaviour::create_if_necessary, FilesystemNS::add_rank::no),
                      FilesystemNS::DirectoryNS::ExceptionNS::SetBehaviourNoRank);

    BOOST_CHECK_NO_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                             mpi,
                             directory,
                             FilesystemNS::behaviour::read,
                             FilesystemNS::add_rank::no)); // was created in the call with add_rank=yes

    BOOST_CHECK(directory_ptr->IsWithRank() == false);

    BOOST_CHECK_NO_THROW(directory_ptr = std::make_unique<FilesystemNS::Directory>(
                             mpi, directory, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no));

    BOOST_CHECK(directory_ptr->IsWithRank() == false);

    BOOST_CHECK_THROW(directory_ptr->SetBehaviour(FilesystemNS::behaviour::create),
                      FilesystemNS::DirectoryNS::ExceptionNS::SetBehaviourNoRank);
}


BOOST_FIXTURE_TEST_CASE(is_inside, fixture)
{
    // I'm working here with directories that (probably) do not exist: it is not a requirement for
    // the functionality to work.

    decltype(auto) mpi = GetMpi();

    const FilesystemNS::Directory directory(GetMainDirectory(), FilesystemNS::behaviour::ignore);

    const std::filesystem::path root = "/whatever/folder/acting/root/";
    const std::filesystem::path not_inside = "/not/inside";
    const std::filesystem::path inside = "/whatever/folder/acting/root/simple_case";
    const std::filesystem::path root_no_trailing = "/whatever/folder/acting/root";
    const std::filesystem::path one_level_removed = "/whatever/folder/acting/root/subfolder1/subfolder1/../../../";
    const std::filesystem::path inside_with_relative = "/whatever/folder/acting/root/subfolder1/../subfolder2";
    const std::filesystem::path inside_with_further_relative =
        "/whatever/folder/acting/root/subfolder1/../../root/subfolder1/subfolder3";

    auto root_dir = FilesystemNS::Directory(mpi, root, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);
    auto not_inside_dir =
        FilesystemNS::Directory(mpi, not_inside, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);
    auto inside_dir = FilesystemNS::Directory(mpi, inside, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);
    auto root_no_trailing_dir =
        FilesystemNS::Directory(mpi, root_no_trailing, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);
    auto one_level_removed_dir =
        FilesystemNS::Directory(mpi, one_level_removed, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);
    auto inside_with_relative_dir =
        FilesystemNS::Directory(mpi, inside_with_relative, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);
    auto inside_with_further_relative_dir = FilesystemNS::Directory(
        mpi, inside_with_further_relative, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);

    BOOST_CHECK(!FilesystemNS::IsFirstSubfolderOfSecond(root_dir, not_inside_dir));
    BOOST_CHECK(!FilesystemNS::IsFirstSubfolderOfSecond(not_inside_dir, root_dir));

    BOOST_CHECK(FilesystemNS::IsFirstSubfolderOfSecond(root_dir, root_no_trailing_dir));
    BOOST_CHECK(FilesystemNS::IsFirstSubfolderOfSecond(root_no_trailing_dir, root_dir));

    BOOST_CHECK(FilesystemNS::IsFirstSubfolderOfSecond(inside_dir, root_dir));
    BOOST_CHECK(!FilesystemNS::IsFirstSubfolderOfSecond(root_dir, inside_dir));

    BOOST_CHECK(FilesystemNS::IsFirstSubfolderOfSecond(root_dir, one_level_removed_dir));
    BOOST_CHECK(!FilesystemNS::IsFirstSubfolderOfSecond(one_level_removed_dir, root_dir));

    BOOST_CHECK(!FilesystemNS::IsFirstSubfolderOfSecond(root_dir, inside_with_relative_dir));
    BOOST_CHECK(FilesystemNS::IsFirstSubfolderOfSecond(inside_with_relative_dir, root_dir));

    BOOST_CHECK(!FilesystemNS::IsFirstSubfolderOfSecond(root_dir, inside_with_further_relative_dir));
    BOOST_CHECK(FilesystemNS::IsFirstSubfolderOfSecond(inside_with_further_relative_dir, root_dir));
}


BOOST_FIXTURE_TEST_CASE(trailing_slash, fixture)
{
    decltype(auto) mpi = GetMpi();
    auto directory_without_slash = GetMainDirectory() / std::filesystem::path("TrailingSlashTest");
    auto directory_with_slash = GetMainDirectory() / std::filesystem::path("TrailingSlashTest/");

    {
        auto create_without_slash = FilesystemNS::Directory(
            mpi, directory_without_slash, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);
        auto create_with_slash = FilesystemNS::Directory(
            mpi, directory_with_slash, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::no);

        BOOST_CHECK(FilesystemNS::IsSameDirectory(create_with_slash, create_without_slash));
        BOOST_CHECK_EQUAL(create_with_slash.GetDirectoryEntry().path(),
                          create_without_slash.GetDirectoryEntry().path());

        std::cout << create_without_slash.GetDirectoryEntry().path() << '\n';
    }

    {
        auto create_without_slash = FilesystemNS::Directory(
            mpi, directory_without_slash, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::yes);
        auto create_with_slash = FilesystemNS::Directory(
            mpi, directory_with_slash, FilesystemNS::behaviour::ignore, FilesystemNS::add_rank::yes);

        BOOST_CHECK(FilesystemNS::IsSameDirectory(create_with_slash, create_without_slash));
    }
}


PRAGMA_DIAGNOSTIC(pop)

// NOLINTEND(readability-function-cognitive-complexity)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    template<stdin_case CaseT>
    void SetStdin(std::ifstream& in)

    {
        const std::filesystem::path temp_directory = std::filesystem::temp_directory_path();

        std::ostringstream oconv;
        oconv << "input_" << (CaseT == stdin_case::yes ? "yes" : "no") << ".txt";

        auto stdin_file = temp_directory / std::filesystem::path(oconv.str());

        const FilesystemNS::File file(std::move(stdin_file));
        std::ofstream out{ file.NewContent() };

        out << "w w e s f 4 515 s j" << '\n';

        if (CaseT == stdin_case::yes)
            out << 'y';
        else
            out << 'n';

        out.close();

        in.close();

        in = file.Read();

        // See //https://stackoverflow.com/questions/10150468/how-to-redirect-cin-and-cout-to-files.
        std::cin.rdbuf(in.rdbuf()); // redirect std::cin to in.txt!
    }


    fixture::fixture()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();
        decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR");

        main_directory_ = std::filesystem::path{ test_dir };
        main_directory_ /= std::filesystem::path{ "Directory" };
    }


    const std::filesystem::path& fixture::GetMainDirectory() const noexcept
    {
        return main_directory_;
    }


} // namespace
