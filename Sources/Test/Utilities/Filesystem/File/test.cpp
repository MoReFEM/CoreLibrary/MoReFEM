// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <cassert>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iterator>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#define BOOST_TEST_MODULE utilities_file

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    const std::string& TestSubdirectory()
    {
        static const std::string ret{ "File" };
        return ret;
    }

    struct fixture : public TestNS::FixtureNS::TestEnvironment
    {

        explicit fixture();

        [[nodiscard]] const FilesystemNS::Directory& GetPlaygroundDirectory() const noexcept;

      private:
        std::unique_ptr<FilesystemNS::Directory> playground_{ nullptr };
    };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(basic_operations, fixture)
{
    const FilesystemNS::File file = GetPlaygroundDirectory().AddFile("created_file.txt");

    std::filesystem::path file_without_facility(GetPlaygroundDirectory().GetPath());
    file_without_facility /= "created_file.txt";

    BOOST_CHECK_EQUAL(static_cast<std::string>(file), file_without_facility.native());

    std::ostringstream oconv;
    oconv << file;

    BOOST_CHECK_EQUAL(oconv.str(), file_without_facility.native());


    BOOST_CHECK(file.Extension() == ".txt");

    BOOST_CHECK(file.DoExist() == false); // the directory was just recreated so can't exist.

    {
        std::ofstream out = file.NewContent();
        out << "Hello world!" << '\n';
    }

    BOOST_CHECK(file.DoExist() == true);
    file.Remove();
    BOOST_CHECK(file.DoExist() == false);
}


BOOST_FIXTURE_TEST_CASE(equivalent, fixture)
{
    const FilesystemNS::File file1 = GetPlaygroundDirectory().AddFile("file1.txt");

    const FilesystemNS::File file1bis = GetPlaygroundDirectory().AddFile("file1.txt"); // same name intentionally

    const FilesystemNS::File file2 = GetPlaygroundDirectory().AddFile("file2.txt"); // same name intentionally

    BOOST_CHECK_PREDICATE(FilesystemNS::IsSameFile, (file1)(file1bis));

    auto not_same_file = [](const auto& lhs, const auto& rhs)
    {
        return !FilesystemNS::IsSameFile(lhs, rhs);
    };

    BOOST_CHECK_PREDICATE(not_same_file, (file1)(file2));

    {
        std::ofstream out = file1.NewContent();
        out << "Hello world!" << '\n';
    }

    BOOST_CHECK_PREDICATE(FilesystemNS::IsSameFile, (file1)(file1bis)); // existence or not of the file doesn't matter
}


BOOST_FIXTURE_TEST_CASE(copy, fixture)
{
    const FilesystemNS::File source = GetPlaygroundDirectory().AddFile("original.txt");

    {
        std::ofstream out = source.NewContent();
        out << "Hello world!" << '\n';
    }

    BOOST_CHECK_THROW(
        FilesystemNS::Copy(source, source, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no),
        std::exception);

    BOOST_CHECK_NO_THROW(
        FilesystemNS::Copy(source, source, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::yes));


    const FilesystemNS::File target = GetPlaygroundDirectory().AddFile("target.txt");

    FilesystemNS::Copy(source, target, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no);

    BOOST_CHECK_NO_THROW(
        FilesystemNS::Copy(source, target, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no));

    // Throw as it was created by command above...
    BOOST_CHECK_THROW(
        FilesystemNS::Copy(source, target, FilesystemNS::fail_if_already_exist::yes, FilesystemNS::autocopy::no),
        std::exception);

    BOOST_CHECK_NO_THROW(
        FilesystemNS::Copy(source, target, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no));

    BOOST_CHECK(FilesystemNS::AreEquals(source, target));

    const FilesystemNS::File other = GetPlaygroundDirectory().AddFile("other.txt");

    BOOST_CHECK_THROW(FilesystemNS::AreEquals(other, target),
                      Exception); // file doesn't exist yet

    {
        std::ofstream out = other.NewContent();
        out << "Hello world!" << '\n';
    }

    BOOST_CHECK(FilesystemNS::AreEquals(other, target) == true);

    {
        std::ofstream out = other.NewContent();
        out << "Goodbye!" << '\n';
    }

    BOOST_CHECK(FilesystemNS::AreEquals(other, target) == false);
}


BOOST_FIXTURE_TEST_CASE(read_and_append, fixture)
{
    const FilesystemNS::File source = GetPlaygroundDirectory().AddFile("original.txt");

    const std::string content{ "Hello world!" };

    {
        std::ofstream out = source.NewContent();
        out << content << '\n';
    }

    {
        std::ifstream in = source.Read();
        std::string read;

        std::getline(in, read);
        BOOST_CHECK_EQUAL(content, read);
    }

    const std::string new_content{ "Goodbye!" };

    {
        std::ofstream out = source.Append();
        out << new_content << '\n';
    }

    {
        std::ifstream in = source.Read();
        std::string read;

        std::getline(in, read);
        BOOST_CHECK(in);
        BOOST_CHECK_EQUAL(content, read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(new_content, read);

        std::getline(in, read);
        BOOST_CHECK(read.empty());

        BOOST_CHECK(in.eof());
    }
}


BOOST_FIXTURE_TEST_CASE(binary, fixture)
{
    const FilesystemNS::File ascii = GetPlaygroundDirectory().AddFile("ascii.txt");
    const FilesystemNS::File binary = GetPlaygroundDirectory().AddFile("binary.txt");

    const std::vector<unsigned int> prime{ 2, 3, 5, 7, 11 };

    {
        std::ofstream out = ascii.NewContent();
        for (auto value : prime)
            out << value << "\t";

        out.close();
        BOOST_CHECK(ascii.DoExist() == true);
    }

    {
        std::ofstream out = binary.NewContent(binary_or_ascii::binary);
        // NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)
        for (auto value : prime)
            out.write(reinterpret_cast<const char*>(&value), sizeof(char));
        // NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)

        out.close();
        BOOST_CHECK(binary.DoExist() == true);
    }

    auto not_equal = [](const FilesystemNS::File& lhs, const FilesystemNS::File& rhs)
    {
        return !FilesystemNS::AreEquals(lhs, rhs);
    };

    BOOST_CHECK_PREDICATE(not_equal, (ascii)(binary));

    {
        std::vector<unsigned int> read_from_ascii;

        std::ifstream in{ ascii.Read() };

        unsigned int value = 0;

        while (in >> value)
            read_from_ascii.push_back(value);

        BOOST_CHECK(prime == read_from_ascii);
    }

    {
        std::ifstream in{ binary.Read(binary_or_ascii::binary) };

        const std::vector<unsigned int> read_from_binary((std::istreambuf_iterator<char>(in)),
                                                         (std::istreambuf_iterator<char>()));

        BOOST_CHECK(prime == read_from_binary);
    }
}


BOOST_FIXTURE_TEST_CASE(concatenate, fixture)
{
    const FilesystemNS::File file1 = GetPlaygroundDirectory().AddFile("file1.txt");
    const FilesystemNS::File file2 = GetPlaygroundDirectory().AddFile("file2.txt");
    const FilesystemNS::File file3 = GetPlaygroundDirectory().AddFile("file3.txt");

    std::array<std::string, 3UL> content{ { "Content 1", "Content 2", "Content 3" } };

    {
        std::ofstream out = file1.NewContent();
        out << content[0] << '\n';
    }
    {
        std::ofstream out = file2.NewContent();
        out << content[1] << '\n';
    }
    {
        std::ofstream out = file3.NewContent();
        out << content[2] << '\n';
    }

    const FilesystemNS::File amalgamated1 = GetPlaygroundDirectory().AddFile("amalgamated1.txt");

    FilesystemNS::ConcatenateAsciiFiles({ file1 }, amalgamated1);

    BOOST_CHECK(FilesystemNS::AreEquals(file1, amalgamated1));


    BOOST_CHECK_THROW(FilesystemNS::ConcatenateAsciiFiles({ file1 }, file1), Exception);

    BOOST_CHECK_THROW(FilesystemNS::ConcatenateAsciiFiles({ file1, file2, file3, file1 }, amalgamated1),
                      Exception); // throws as the target file already exists!

    const FilesystemNS::File amalgamated2 = GetPlaygroundDirectory().AddFile("amalgamated2.txt");

    FilesystemNS::ConcatenateAsciiFiles({ file1, file2, file3, file1 }, amalgamated2);

    {
        std::ifstream in = amalgamated2.Read();
        std::string read;

        std::getline(in, read);
        BOOST_CHECK(in);
        BOOST_CHECK_EQUAL(content[0], read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(content[1], read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(content[2], read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(content[0], read);
        std::getline(in, read);
        BOOST_CHECK(read.empty());
        BOOST_CHECK(in.eof());
    }
}


BOOST_FIXTURE_TEST_CASE(created_from_ill_formed_string, fixture)
{
    std::filesystem::path filename("/path42/that/shouldn't//exist//foo.txt"); // double slashes intended

    const FilesystemNS::File file(std::move(filename));

    BOOST_CHECK(file.DoExist() == false);

    BOOST_CHECK_EQUAL(file.Extension(), ".txt");
}


BOOST_FIXTURE_TEST_CASE(environment_substitution, fixture)
{
    std::ostringstream oconv;
    oconv << "${MOREFEM_TEST_OUTPUT_DIR}/" << TestSubdirectory() << "/Rank_" << GetMpi().GetRank<int>() << "/foo.txt";

    const FilesystemNS::File from_env_substitution{ std::filesystem::path{ oconv.str() } };

    // Here the substitution occurred much earlier - see fixture connstructor
    const FilesystemNS::File usual_way{ GetPlaygroundDirectory().AddFile("foo.txt") };

    BOOST_CHECK_PREDICATE(FilesystemNS::IsSameFile, (from_env_substitution)(usual_way));
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    fixture::fixture()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();
        decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR");

        decltype(auto) mpi = GetMpi();

        std::filesystem::path path(test_dir);
        path /= TestSubdirectory();

        playground_ = std::make_unique<FilesystemNS::Directory>(mpi, path, FilesystemNS::behaviour::overwrite);

        static bool is_first_call{ true };

        if (is_first_call)
        {
            playground_->ActOnFilesystem();
            is_first_call = false;
        }
    }


    const FilesystemNS::Directory& fixture::GetPlaygroundDirectory() const noexcept
    {
        assert(!(!playground_));
        return *playground_;
    }


} // namespace
