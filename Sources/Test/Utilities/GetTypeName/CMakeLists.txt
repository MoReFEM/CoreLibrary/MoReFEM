add_executable(TestGetTypeName)

target_sources(TestGetTypeName
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestGetTypeName
                      ${MOREFEM_UTILITIES}
                      ${LIB_BOOST_TEST})

morefem_boost_test_sequential_mode(NAME GetTypeName
                                   EXE TestGetTypeName
                                   TIMEOUT 20)

morefem_organize_IDE(TestGetTypeName Test/Utilities Test/Utilities/GetTypeName)
