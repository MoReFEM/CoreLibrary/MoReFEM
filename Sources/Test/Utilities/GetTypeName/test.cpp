// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <iostream>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE typename
#include "Utilities/Type/PrintTypeName.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(typename_test)
{
    BOOST_REQUIRE(GetTypeName<int>() == "int");
    BOOST_REQUIRE(GetTypeName<double>() == "double");

#ifdef __clang__
    constexpr auto expected = "const int &";
#elif defined(__GNUC__)
    constexpr auto expected = "const int&";
#endif

    BOOST_REQUIRE(GetTypeName<const int&>() == expected);

    // The tests below are not complete: it's highly possible a compiler will yield a correct result not present here.
    // Feel free to extend the test; currently it covers output by AppleClang, Clang and gcc.
    std::cout << "The result for std::vector<double> is |" << GetTypeName<std::vector<double>>() << '|' << '\n';

    BOOST_REQUIRE(( // double parenthesis on purpose!
        GetTypeName<std::vector<double>>() == "std::__1::vector<double, std::__1::allocator<double> >"
        || GetTypeName<std::vector<double>>() == "std::__1::vector<double, std::__1::allocator<double>>"
        || GetTypeName<std::vector<double>>() == "std::vector<double, std::allocator<double> >"
        || GetTypeName<std::vector<double>>() == "std::vector<double>"
        || GetTypeName<std::vector<double>>() == "std::__1::vector<double>"));

    std::cout << "The result for std::string is |" << GetTypeName<std::string>() << '|' << '\n';

    BOOST_REQUIRE(( // double parenthesis on purpose!
        GetTypeName<std::string>() == "std::__1::basic_string<char>"
        || GetTypeName<std::string>()
               == "std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >"
        || GetTypeName<std::string>()
               == "std::__1::basic_string<char, std::__1::char_traits<char>, std::__1::allocator<char>>"
        || GetTypeName<std::string>() == "std::__cxx11::basic_string<char>"
        || GetTypeName<std::string>() == "std::string" || GetTypeName<std::string>() == "std::basic_string<char>"));
}

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
