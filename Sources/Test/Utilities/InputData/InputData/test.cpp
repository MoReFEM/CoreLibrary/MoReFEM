// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <tuple>
#include <vector>

#include "Utilities/Filesystem/Behaviour.hpp"
#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
#define BOOST_TEST_MODULE input_data

#include <filesystem>
#include <string>

#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Utilities/InputData/InputData/InputData.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

using namespace MoReFEM;
using namespace MoReFEM::TestNS::ReadInputDataNS;


namespace // anonymous
{


    FilesystemNS::File InputDataFile()
    {
        FilesystemNS::File lua_file{ std::filesystem::path{
            "${MOREFEM_ROOT}/Sources/Test/Utilities/InputData/InputData/"
            "demo.lua" } };

        return lua_file;
    }


} // namespace


BOOST_FIXTURE_TEST_CASE(find_leaf, TestNS::FixtureNS::TestEnvironment)
{
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    static_assert(input_data_type::Find<LeafInNoEnclosingSection>());

    static_assert(input_data_type::Find<Section2::Leaf1InSection2>());
    static_assert(!input_data_type::Find<Section2::Leaf2InSection2>());

    static_assert(input_data_type::Find<Section1::SubsectionInSection1>());
}


BOOST_FIXTURE_TEST_CASE(find_section, TestNS::FixtureNS::TestEnvironment)
{
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    static_assert(input_data_type::Find<Section1>());

    static_assert(input_data_type::Find<Section1::SubsectionInSection1>());

    static_assert(!input_data_type::Find<Section2::SubsectionInSection2>());
}


BOOST_FIXTURE_TEST_CASE(leaf, TestNS::FixtureNS::TestEnvironment)
{
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    decltype(auto) value = Internal::InputDataNS::ExtractLeaf<LeafInNoEnclosingSection>::Value(input_data);
    BOOST_CHECK_EQUAL(value, "Hello world!");
}


BOOST_FIXTURE_TEST_CASE(leaf_in_section, TestNS::FixtureNS::TestEnvironment)
{
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    {
        decltype(auto) value = Internal::InputDataNS::ExtractLeaf<Section1::LeafInSection1>::Value(input_data);

        BOOST_REQUIRE_EQUAL(value.size(), 3UL);
        BOOST_CHECK_EQUAL(value[0], 2.1);
        BOOST_CHECK_EQUAL(value[1], -3.2);
        BOOST_CHECK_EQUAL(value[2], 5.);
    }

    {
        decltype(auto) value = Internal::InputDataNS::ExtractLeaf<Section2::Leaf1InSection2>::Value(input_data);

        BOOST_REQUIRE_EQUAL(value.size(), 2UL);

        {
            auto it = value.find(3);
            BOOST_REQUIRE(it != value.cend());
            BOOST_CHECK_EQUAL(it->second, 7.3);
        }

        {
            auto it = value.find(2);
            BOOST_REQUIRE(it != value.cend());
            BOOST_CHECK_EQUAL(it->second, -12.);
        }
    }
}


BOOST_FIXTURE_TEST_CASE(path, TestNS::FixtureNS::TestEnvironment)
{
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    decltype(auto) value =
        Internal::InputDataNS::ExtractLeaf<Section1::SubsectionInSection1::LeafInSubSection1>::Value(input_data);
    BOOST_CHECK_EQUAL(value, "${MOREFEM_ROOT}/Sources/Utilities");

    decltype(auto) path =
        Internal::InputDataNS::ExtractLeaf<Section1::SubsectionInSection1::LeafInSubSection1>::Path(input_data);

    const std::filesystem::directory_entry entry{ path };
    BOOST_CHECK(entry.exists());
}


BOOST_FIXTURE_TEST_CASE(extract_section, TestNS::FixtureNS::TestEnvironment)
{
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    {
        decltype(auto) section = Internal::InputDataNS::ExtractSection<Section1>::Value(input_data);
        BOOST_CHECK_EQUAL(section.GetFullName(), "Section1");
    }

    {
        decltype(auto) section =
            Internal::InputDataNS::ExtractSection<Section1::SubsectionInSection1>::Value(input_data);
        BOOST_CHECK_EQUAL(section.GetFullName(), "Section1.SubsectionInSection1");
    }
}


BOOST_FIXTURE_TEST_CASE(print_keys, TestNS::FixtureNS::TestEnvironment)
{
    if (GetMpi().IsRootProcessor())
    {
        const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

        auto output_dir =
            FilesystemNS::Directory(GetMpi(),
                                    std::filesystem::path{ "${MOREFEM_TEST_OUTPUT_DIR}/Utilities/InputData" },
                                    FilesystemNS::behaviour::overwrite);

        output_dir.ActOnFilesystem();

        // Write into a file all the keys found
        {
            const FilesystemNS::File out = output_dir.AddFile("test_input_data.txt");
            auto stream = out.NewContent();
            input_data.PrintKeys(stream);
        }

        // Read this file line by line to fill a std::vector with all keys.
        std::vector<std::string> content;

        {
            const FilesystemNS::File in = output_dir.AddFile("test_input_data.txt");

            BOOST_REQUIRE(in.DoExist());

            auto stream = in.Read();
            std::string buf;
            while (stream >> buf)
                content.emplace_back(buf);
        }

        std::ranges::sort(content);
        BOOST_REQUIRE_EQUAL(content.size(), 4UL);

        BOOST_CHECK_EQUAL(content[0], "LeafInNoEnclosingSection");
        BOOST_CHECK_EQUAL(content[1], "Section1.LeafInSection1");
        BOOST_CHECK_EQUAL(content[2], "Section1.SubsectionInSection1.LeafInSubSection1");
        BOOST_CHECK_EQUAL(content[3], "Section2.FirstLeafInSection2");
    }
}


BOOST_FIXTURE_TEST_CASE(extract_keys, TestNS::FixtureNS::TestEnvironment)
{
    if (GetMpi().IsRootProcessor())
    {
        const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

        auto keys = input_data.ExtractKeys();

        BOOST_REQUIRE_EQUAL(keys.size(), 4UL);

        BOOST_CHECK_EQUAL(keys[0], "LeafInNoEnclosingSection");
        BOOST_CHECK_EQUAL(keys[1], "Section1.LeafInSection1");
        BOOST_CHECK_EQUAL(keys[2], "Section1.SubsectionInSection1.LeafInSubSection1");
        BOOST_CHECK_EQUAL(keys[3], "Section2.FirstLeafInSection2");
    }
}


BOOST_FIXTURE_TEST_CASE(duplicate_keys, TestNS::FixtureNS::TestEnvironment)
{
    using input_data_tuple_with_duplicates = std::tuple<Section1, Section1::LeafInSection1>;

    using input_data_with_duplicates_type = InputData<input_data_tuple_with_duplicates>;

    const FilesystemNS::File doesnt_matter{};

    BOOST_CHECK_THROW(auto input_data = input_data_with_duplicates_type(TestNS::EmptyModelSettings(), doesnt_matter),
                      MoReFEM::InputDataNS::ExceptionNS::DuplicateInTuple);
}


BOOST_FIXTURE_TEST_CASE(unused, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    {
        // At this point, all elements are left unused.
        auto unused_leaf_list = input_data.ComputeUnusedLeafList(mpi);

        if (mpi.IsRootProcessor())
        {
            BOOST_REQUIRE_EQUAL(unused_leaf_list.size(), 4UL);

            BOOST_CHECK_EQUAL(unused_leaf_list[0UL], "LeafInNoEnclosingSection");
            BOOST_CHECK_EQUAL(unused_leaf_list[1UL], "Section1.LeafInSection1");
            BOOST_CHECK_EQUAL(unused_leaf_list[2UL], "Section1.SubsectionInSection1.LeafInSubSection1");
            BOOST_CHECK_EQUAL(unused_leaf_list[3UL], "Section2.FirstLeafInSection2");
        } else
            BOOST_CHECK(unused_leaf_list.empty()); // filled only on root processor!
    }

    {
        // NOLINTBEGIN(bugprone-unused-local-non-trivial-variable) - this is a known clang-tidy limitation - see
        // https://www.mail-archive.com/llvm-bugs@lists.llvm.org/msg73154.html.
        [[maybe_unused]] auto leaf_count_as_used =
            Internal::InputDataNS::ExtractLeaf<Section1::SubsectionInSection1::LeafInSubSection1>::Value(input_data);
        // NOLINTEND(bugprone-unused-local-non-trivial-variable)

        // Now one is counted as used
        auto unused_leaf_list = input_data.ComputeUnusedLeafList(mpi);

        if (mpi.IsRootProcessor())
        {
            BOOST_REQUIRE_EQUAL(unused_leaf_list.size(), 3UL);

            BOOST_CHECK_EQUAL(unused_leaf_list[0UL], "LeafInNoEnclosingSection");
            BOOST_CHECK_EQUAL(unused_leaf_list[1UL], "Section1.LeafInSection1");
            BOOST_CHECK_EQUAL(unused_leaf_list[2UL], "Section2.FirstLeafInSection2");
        } else
            BOOST_CHECK(unused_leaf_list.empty()); // filled only on root processor!
    }

    if (mpi.Nprocessor<int>() == 4)
    {
        if (mpi.GetRank<int>() == 0)
        {
            [[maybe_unused]] auto leaf_count_as_used =
                Internal::InputDataNS::ExtractLeaf<Section1::LeafInSection1>::Value(input_data);
        }

        if (mpi.GetRank<int>() == 2)
        {
            [[maybe_unused]] auto leaf_count_as_used =
                Internal::InputDataNS::ExtractLeaf<Section2::Leaf1InSection2>::Value(input_data);
        }

        auto unused_leaf_list = input_data.ComputeUnusedLeafList(mpi);

        if (mpi.IsRootProcessor())
        {
            BOOST_REQUIRE_EQUAL(mpi.GetRank<int>(), 0);
            BOOST_REQUIRE_EQUAL(unused_leaf_list.size(), 1UL);
            BOOST_CHECK_EQUAL(unused_leaf_list[0UL], "LeafInNoEnclosingSection");
        } else
            BOOST_CHECK(unused_leaf_list.empty()); // filled only on root processor!
    }
}


BOOST_FIXTURE_TEST_CASE(match_identifiers, TestNS::FixtureNS::TestEnvironment)
{
    const input_data_type input_data(TestNS::EmptyModelSettings(), InputDataFile());

    using tuple_iteration = Internal::InputDataNS::TupleIteration<input_data_tuple, 0UL>;

    BOOST_CHECK(!tuple_iteration::DoMatchIdentifier("whatever", "not in tuple"));
    BOOST_CHECK(tuple_iteration::DoMatchIdentifier("Section1.SubsectionInSection1", "LeafInSubSection1"));
    BOOST_CHECK(tuple_iteration::DoMatchIdentifier("", "LeafInNoEnclosingSection"));
}


BOOST_FIXTURE_TEST_CASE(Nleaves, TestNS::FixtureNS::TestEnvironment)
{
    BOOST_CHECK_EQUAL(input_data_type::Nleaves(), 4UL);

    {
        using tuple = std::tuple<Section1::LeafInSection1>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 1UL);
    }

    {
        using tuple = std::tuple<Section1::SubsectionInSection1::LeafInSubSection1>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 1UL);
    }

    {
        using tuple = std::tuple<Section1::SubsectionInSection1>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 1UL);
    }

    {
        using tuple = std::tuple<Section1>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 2UL);
    }

    {
        using tuple = std::tuple<LeafInNoEnclosingSection>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 1UL);
    }

    {
        using tuple = std::tuple<Section2::SubsectionInSection2>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 1UL);
    }

    {
        using tuple = std::tuple<Section2>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 2UL);
    }

    {
        using tuple = std::tuple<Section1,
                                 LeafInNoEnclosingSection,
                                 Section2,
                                 Section2::SubsectionInSection2 // which is NOT included by default in Section2
                                 >;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 6UL);
    }

    {
        using tuple = std::tuple<Section1, Section2>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 4UL);
    }


    {
        using tuple = std::tuple<Section1::LeafInSection1, Section2::SubsectionInSection2::LeafInSubsectionInSection2>;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 2UL);
    }


    {
        using tuple = std::tuple<

            >;

        BOOST_CHECK_EQUAL(InputData<tuple>::Nleaves(), 0UL);
    }
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
