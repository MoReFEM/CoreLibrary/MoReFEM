These tests check we read properly model-related data.

They might be stored:

- In a Lua file (that the end-user may modify)
- In a ModelSettings file (which should be written by the model author and left untouched by an end-user).

Here both tests check same data stored one way or another.
