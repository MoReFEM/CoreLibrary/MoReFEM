// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Test/Utilities/InputData/SectionsAndLeaves.hpp"

#include "Utilities/String/EmptyString.hpp"


namespace MoReFEM::TestNS::ReadInputDataNS
{


    const std::string& LeafInNoEnclosingSection::NameInFile()
    {
        static const std::string ret("LeafInNoEnclosingSection");
        return ret;
    }


    const std::string& LeafInNoEnclosingSection::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section1::GetName()
    {
        static const std::string ret("Section1");
        return ret;
    }


    const std::string& Section1::LeafInSection1::NameInFile()
    {
        static const std::string ret("LeafInSection1");
        return ret;
    }


    const std::string& Section1::LeafInSection1::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section1::SubsectionInSection1::GetName()
    {
        static const std::string ret("SubsectionInSection1");
        return ret;
    }


    const std::string& Section1::SubsectionInSection1::LeafInSubSection1::NameInFile()
    {
        static const std::string ret("LeafInSubSection1");
        return ret;
    }


    const std::string& Section1::SubsectionInSection1::LeafInSubSection1::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section2::GetName()
    {
        static const std::string ret("Section2");
        return ret;
    }


    const std::string& Section2::Leaf1InSection2::NameInFile()
    {
        static const std::string ret("FirstLeafInSection2");
        return ret;
    }


    const std::string& Section2::Leaf1InSection2::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section2::Leaf2InSection2::NameInFile()
    {
        static const std::string ret("SecondLeafInSection2");
        return ret;
    }


    const std::string& Section2::Leaf2InSection2::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section2::SubsectionInSection2::GetName()
    {
        static const std::string ret("SubsectionInSection2");
        return ret;
    }


    const std::string& Section2::SubsectionInSection2::LeafInSubsectionInSection2::NameInFile()
    {
        static const std::string ret("LeafInSubsectionInSection2");
        return ret;
    }


    const std::string& Section2::SubsectionInSection2::LeafInSubsectionInSection2::Description()
    {
        return Utilities::EmptyString();
    }


} // namespace MoReFEM::TestNS::ReadInputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
