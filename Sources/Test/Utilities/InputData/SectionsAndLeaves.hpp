// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_UTILITIES_INPUTDATA_SECTIONSANDLEAVES_DOT_HPP_
#define MOREFEM_TEST_UTILITIES_INPUTDATA_SECTIONSANDLEAVES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::ReadInputDataNS
{

    //! A section which few content; all will be added in the tuple of the test.
    struct Section1
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Section1,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! Convenient alias.
        using self = Section1;

        //! Convenient alias.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship
        friend parent;

        //! Convenient alias.
        using enclosing_section_type = self;

        //! Name of the section
        static const std::string& GetName();

        //! Leaf in subsection.
        struct LeafInSection1
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<LeafInSection1, self, std::vector<double>>
        {
            //! Name of the leaf.
            static const std::string& NameInFile();

            //! Description, left empty for this test.
            static const std::string& Description();

            //! Default value,  left empty for this test.
            static const std::string& DefaultValue();
        };

        //! A subsection in the section 1.
        struct SubsectionInSection1 : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<SubsectionInSection1, self>
        {
            //! Convenient alias.
            using self = SubsectionInSection1;

            //! Convenient alias.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, enclosing_section_type>;

            static_assert(std::is_convertible<self*, parent*>());

            //! Friendship
            friend parent;

            //! Name of the section
            static const std::string& GetName();

            //! A leaf in the subsection.
            struct LeafInSubSection1
            : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<LeafInSubSection1, SubsectionInSection1, std::string>
            {
                //! Name of the leaf.
                static const std::string& NameInFile();

                //! Description, left empty for this test.
                static const std::string& Description();
            };

            //! Convenient alias.
            using section_content_type = std::tuple<LeafInSubSection1>;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            LeafInSection1,
            SubsectionInSection1
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Section1


    //! Another section, which will be partially included in the input data tuple.
    struct Section2
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Section2,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! Convenient alias.
        using self = Section2;

        //! Convenient alias.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship
        friend parent;

        //! Convenient alias.
        using enclosing_section_type = self;

        //! Name of the section
        static const std::string& GetName();

        //! A leaf of section 2, which will be  included in the input data tuple.
        struct Leaf1InSection2
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Leaf1InSection2, self, std::map<int, double>>
        {
            //! Name of the leaf.
            static const std::string& NameInFile();

            //! Description, left empty for this test.
            static const std::string& Description();

            //! Default value,  left empty for this test.
            static const std::string& DefaultValue();
        };

        //! Another leaf of section 2, which will **NOT** be  included in the input data tuple.
        struct Leaf2InSection2 : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Leaf2InSection2, self, std::string>
        {
            //! Name of the leaf.
            static const std::string& NameInFile();

            //! Description, left empty for this test.
            static const std::string& Description();

            //! Default value,  left empty for this test.
            static const std::string& DefaultValue();
        };

        //! A subsection of section 2, which will **NOT** be  included in the input data tuple.
        struct SubsectionInSection2 : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<SubsectionInSection2, self>
        {
            //! Convenient alias.
            using self = SubsectionInSection2;

            //! Convenient alias.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, enclosing_section_type>;

            static_assert(std::is_convertible<self*, parent*>());

            //! Friendship
            friend parent;

            //! Name of the section
            static const std::string& GetName();

            //! A leaf in Subsection
            struct LeafInSubsectionInSection2 : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                                    Leaf<SubsectionInSection2, SubsectionInSection2, std::string>
            {
                //! Name of the leaf.
                static const std::string& NameInFile();

                //! Description, left empty for this test.
                static const std::string& Description();
            };

            //! Convenient alias.
            using section_content_type = std::tuple<LeafInSubsectionInSection2>;
            // clang-format on


          private:
            //! Content of the section.
            section_content_type section_content_;
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Leaf1InSection2,
            Leaf2InSection2
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Section1


    //! A leaf not enclosed in a section, which will be encompassed in the input data tyuple.
    struct LeafInNoEnclosingSection
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::
          Leaf<LeafInNoEnclosingSection, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection, std::string>
    {
        //! Name of the leaf.
        static const std::string& NameInFile();

        //! Description, left empty for this test.
        static const std::string& Description();

        //! Default value,  left empty for this test.
        static const std::string& DefaultValue();
    };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { numbering_subset = 1 };


} // namespace MoReFEM::TestNS::ReadInputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_UTILITIES_INPUTDATA_SECTIONSANDLEAVES_DOT_HPP_
// *** MoReFEM end header guards *** < //
