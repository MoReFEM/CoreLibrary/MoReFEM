add_executable(TestIsSmartPtr)

target_sources(TestIsSmartPtr
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)              
          
target_link_libraries(TestIsSmartPtr
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_boost_test_sequential_mode(NAME IsSmartPtr
                                   EXE TestIsSmartPtr
                                   TIMEOUT 5)

morefem_organize_IDE(TestIsSmartPtr Test/Utilities Test/Utilities/IsSmartPtr)
