add_executable(TestNumericUtilities)

target_sources(TestNumericUtilities
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp     
)
          
target_link_libraries(TestNumericUtilities
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)
                      
morefem_boost_test_sequential_mode(NAME NumericUtilities
                                   EXE TestNumericUtilities
                                   TIMEOUT 4)

set_tests_properties(NumericUtilities  PROPERTIES TIMEOUT 4)

morefem_organize_IDE(TestNumericUtilities Test/Utilities Test/Utilities/Numeric)
