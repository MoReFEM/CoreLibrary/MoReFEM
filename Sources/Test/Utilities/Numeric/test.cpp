// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cmath>
#include <limits>

#define BOOST_TEST_MODULE numeric_utitilies
#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


using namespace MoReFEM::NumericNS;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(power)
{
    BOOST_CHECK_EQUAL(Square(2), 4);
    BOOST_CHECK_EQUAL(Cube(-3), -27);
    BOOST_CHECK_EQUAL(Cube(5.), 125.);
    BOOST_CHECK_EQUAL(Square(-4.), 16.);

    BOOST_CHECK_EQUAL(PowerFour(2), 16);
    BOOST_CHECK_EQUAL(PowerFour(-4.), 256.);
}


BOOST_AUTO_TEST_CASE(abs_plus)
{
    BOOST_CHECK_EQUAL(AbsPlus(5.23), 5.23);
    BOOST_CHECK_EQUAL(AbsPlus(-5.23), 0.);
    BOOST_CHECK_EQUAL(AbsPlus(134), 134);
    BOOST_CHECK_EQUAL(AbsPlus(0.), 0.);
}


BOOST_AUTO_TEST_CASE(sign)
{
    BOOST_CHECK_EQUAL(Sign(5.23), 1.);
    BOOST_CHECK_EQUAL(Sign(-5.23), -1.);
    BOOST_CHECK_EQUAL(Sign(134), 1);
    BOOST_CHECK_EQUAL(Sign(-134), -1);
    BOOST_CHECK_EQUAL(Sign(0), 0);
}


BOOST_AUTO_TEST_CASE(true_sign)
{
    BOOST_CHECK_EQUAL(TrueSign(5.23), 1.);
    BOOST_CHECK_EQUAL(TrueSign(-5.23), -1.);
    BOOST_CHECK_EQUAL(TrueSign(134), 1);
    BOOST_CHECK_EQUAL(TrueSign(-134), -1);
    BOOST_CHECK_EQUAL(TrueSign(0), 1);
}

BOOST_AUTO_TEST_CASE(heaviside)
{
    BOOST_CHECK_EQUAL(Heaviside(5.23), 1.);
    BOOST_CHECK_EQUAL(Heaviside(-5.23), 0.);
    BOOST_CHECK_EQUAL(Heaviside(134.F), 1.F);
    BOOST_CHECK_EQUAL(Heaviside(-134.F), 0.F);
    BOOST_CHECK_EQUAL(Heaviside(0.), 0.5);
}


BOOST_AUTO_TEST_CASE(is_zero)
{
    BOOST_CHECK(!IsZero(5.23));
    BOOST_CHECK(!IsZero(-5.23));
    BOOST_CHECK(!IsZero(134.F));
    BOOST_CHECK(!IsZero(-134.F));
    BOOST_CHECK(IsZero(0.));

    BOOST_CHECK(IsZero(0));
    BOOST_CHECK(!IsZero(5));
    BOOST_CHECK(!IsZero(-3));

    BOOST_CHECK(IsZero(0.2, 1.));
    BOOST_CHECK(!IsZero(0.2, 0.1));

    BOOST_CHECK(!IsZero(1.e-8, 1.e-12));
    BOOST_CHECK(IsZero(1.e-8, 1.e-6)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

    {
        const Eigen::VectorXd non_zero{ { 5., 1., -2., 0. } };
        BOOST_CHECK(!non_zero.isZero());

        const Eigen::VectorXd zero{ { 0., 0., 0., 0., 0., 0. } };
        BOOST_CHECK(zero.isZero());

        const Eigen::VectorXd empty;
        BOOST_CHECK(empty.isZero());
    }

    {
        const Eigen::MatrixXd non_zero{ { 5., 1., -2., 0. }, { 1., 2., 3., 4. }, { 1., 2., 3., 0. } };
        BOOST_CHECK(!non_zero.isZero());

        const Eigen::MatrixXd zero{ { 0., 0., 0., 0. }, { 0., 0., 0., 0. }, { 0., 0., 0., 0. } };
        BOOST_CHECK(zero.isZero());

        const Eigen::MatrixXd empty;
        BOOST_CHECK(empty.isZero());
    }
}


BOOST_AUTO_TEST_CASE(are_equal)
{
    BOOST_CHECK(AreEqual(1.e-6, 1.e-6));   // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    BOOST_CHECK(!AreEqual(1.1e-6, 1.e-6)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    BOOST_CHECK(
        AreEqual(1.1e-6, 1.e-6, 1.e-5)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

    {
        const Eigen::VectorXd vector1{ { 5., 1., -2., 0. } };

        // NOLINTNEXTLINE(performance-unnecessary-copy-initialization)
        const Eigen::VectorXd vector1_bis{ vector1 };

        const Eigen::VectorXd vector2{ { -5., 1., -2., 0. } };

        BOOST_CHECK(vector1.isApprox(vector1));
        BOOST_CHECK(vector1.isApprox(vector1_bis));
        BOOST_CHECK(!vector1.isApprox(vector2));
    }

    {
        const Eigen::MatrixXd matrix1{ { 5., 1., -2., 0. }, { 1., 2., 3., 4. }, { 1., 2., 3., 0. } };

        // NOLINTNEXTLINE(performance-unnecessary-copy-initialization)
        const Eigen::MatrixXd matrix1_bis{ matrix1 };

        const Eigen::MatrixXd matrix2{ { -5., 1., -2., 0. }, { 1., 2., 3., 4. }, { 1., 2., 3., 0. } };

        BOOST_CHECK(matrix1.isApprox(matrix1));
        BOOST_CHECK(matrix1.isApprox(matrix1_bis));
        BOOST_CHECK(!matrix1.isApprox(matrix2));
    }
}


BOOST_AUTO_TEST_CASE(are_equivalent_through_absolute_comparison)
{
    BOOST_CHECK(AreEquivalentThroughRelativeComparison(
        1.e-6, 1.e-6)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    BOOST_CHECK(!AreEquivalentThroughRelativeComparison(
        3.e-6, 1.e-6)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    BOOST_CHECK(AreEquivalentThroughRelativeComparison(
        -24634152765.245289,
        -24634152765.246811,
        1.e-12)); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
}


BOOST_AUTO_TEST_CASE(is_number)
{
    // This test was previously fulfilled with a homemade function but I found out STL
    // already provides the functionality. So the former `IsNumber` has been replaced
    // here by `std::isfinite`.
    BOOST_CHECK(std::isfinite(3.14));
    BOOST_CHECK(std::isfinite(-7.));

    BOOST_CHECK(!std::isfinite(std::numeric_limits<double>::infinity()));

#include "Utilities/Warnings/Internal/IgnoreWarning/div-by-zero.hpp" // IWYU pragma: keep
    const double a = 1. / 0;
    BOOST_CHECK(!std::isfinite(a));
}


BOOST_AUTO_TEST_CASE(pow_wrapper)
{
    BOOST_CHECK_EQUAL(Pow(-3., 2.), 9.);
    BOOST_CHECK_THROW(Pow(-11., -1. / 3.), ::MoReFEM::Exception);
    BOOST_CHECK_THROW(Pow(-0., -3.), ::MoReFEM::Exception);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
