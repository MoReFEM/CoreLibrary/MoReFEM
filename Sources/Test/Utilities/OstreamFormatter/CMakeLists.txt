add_executable(TestOstreamFormatter)

target_sources(TestOstreamFormatter
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestOstreamFormatter
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_boost_test_sequential_mode(NAME OstreamFormatter
                                   EXE TestOstreamFormatter
                                   TIMEOUT 20)

morefem_organize_IDE(TestOstreamFormatter Test/Utilities Test/Utilities/OstreamFormatter)
