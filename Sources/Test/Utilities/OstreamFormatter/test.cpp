// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <format>
#include <ostream>
#include <sstream>
#include <string>
#include <string_view>

#define BOOST_TEST_MODULE ostream_formatter
#include "Utilities/String/OstreamFormatter.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    struct Printable
    {
        explicit Printable(int a, std::string_view prefix);

        int a_{};
        std::string prefix_;
    };
    // NOLINTEND(misc-non-private-member-variables-in-classes)


    std::ostream& operator<<(std::ostream& stream, const Printable& rhs);


} // namespace


namespace std
{


    template<>
    struct formatter<Printable> : ::MoReFEM::basic_ostream_formatter
    { };


} // namespace std


BOOST_AUTO_TEST_CASE(formatter)
{
    Printable p(5, "Embedded value is ");

    std::ostringstream oconv;
    oconv << p;

    BOOST_CHECK_EQUAL(oconv.str(), "Embedded value is 5");

    const auto from_fmt = std::format("From std::format: {}", p);

    BOOST_CHECK_EQUAL(from_fmt, "From std::format: Embedded value is 5");
}

PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


namespace // anonymous
{

    Printable::Printable(int a, std::string_view prefix) : a_{ a }, prefix_{ prefix }
    { }


    std::ostream& operator<<(std::ostream& stream, const Printable& rhs)
    {
        stream << rhs.prefix_ << rhs.a_;
        return stream;
    }


} // namespace


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
