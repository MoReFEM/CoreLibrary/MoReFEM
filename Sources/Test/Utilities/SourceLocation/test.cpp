// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE source_location
#include <format>
#include <source_location>
#include <string>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/SourceLocation/SourceLocation.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture = TestNS::FixtureNS::TestEnvironment;


} // namespace


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(generate_key, fixture)
{

    const auto location = std::source_location::current();

    // To update if renaming/relocation occurs!
    const auto current_file = FilesystemNS::File{ "${MOREFEM_ROOT}/Sources/Test/Utilities/SourceLocation/test.cpp" };
    BOOST_CHECK_EQUAL(static_cast<std::string>(current_file), location.file_name());

    BOOST_CHECK_EQUAL(MoReFEM::Utilities::GenerateSourceLocationKey(location),
                      std::format("{}_{}", location.file_name(), location.line()));
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
