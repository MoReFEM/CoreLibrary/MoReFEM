// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE string
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM::Utilities::String;

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(strip)
{
    const std::string string = "  \t   ABCDEF \n\n  ";

    {
        auto copy = string;
        StripLeft(copy);
        BOOST_CHECK_EQUAL(copy, "ABCDEF \n\n  ");
    }

    {
        auto copy = string;
        StripRight(copy);
        BOOST_CHECK_EQUAL(copy, "  \t   ABCDEF");
    }

    {
        auto copy = string;
        Strip(copy);
        BOOST_CHECK_EQUAL(copy, "ABCDEF");
    }

    {
        auto copy = string;
        Strip(copy, " ");
        BOOST_CHECK_EQUAL(copy, "\t   ABCDEF \n\n");
    }
}


BOOST_AUTO_TEST_CASE(start_end)
{
    const std::string string = "Hello world!";

    BOOST_CHECK(StartsWith(string, "Hello") == true);
    BOOST_CHECK(StartsWith(string, "Bye") == false);

    BOOST_CHECK(EndsWith(string, "!") == true);
    BOOST_CHECK(EndsWith(string, "Bye") == false);
}


BOOST_AUTO_TEST_CASE(replace)
{
    std::string string = "Hello world!";


    BOOST_CHECK_EQUAL(Replace("Bonjour", "Hello", string), 0UL);

    BOOST_CHECK_EQUAL(Replace("Hello", "Bonjour", string), 1UL);
    BOOST_CHECK_EQUAL(Replace("world", "le monde", string), 1UL);

    BOOST_CHECK_EQUAL(string, "Bonjour le monde!");
}


BOOST_AUTO_TEST_CASE(repeat)
{
    const std::string string = "parrot";

    BOOST_CHECK_EQUAL(Repeat(string, 5UL), "parrotparrotparrotparrotparrot");
    BOOST_CHECK_EQUAL(Repeat("parrot", 5UL), "parrotparrotparrotparrotparrot");
}


BOOST_AUTO_TEST_CASE(convert_char_array)
{
    std::vector<char> char_hello{ 'H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', '!' };

    const std::string hello = "Hello world!";

    BOOST_CHECK(ConvertCharArray(char_hello) == hello);

    char_hello.push_back('\0');

    BOOST_CHECK(ConvertCharArray(char_hello) == hello);
}


BOOST_AUTO_TEST_CASE(random_string)
{
    const std::string charset = "+-0123456789";


    const auto string = GenerateRandomString(10, charset);

    BOOST_CHECK_EQUAL(string.size(), 10UL);

    BOOST_CHECK(std::ranges::all_of(string,

                                    [&charset](char character)
                                    {
                                        return std::ranges::find(charset, character) != charset.cend();
                                    }));
}


BOOST_AUTO_TEST_CASE(split)
{
    const std::string string = "Triangle 3; 0; 5; P1; 0.16 5.14 3.2";

    BOOST_CHECK(Split(string, ";")
                == (std::vector<std::string_view>{ "Triangle 3", " 0", " 5", " P1", " 0.16 5.14 3.2" }));

    BOOST_CHECK(Split(string, "!") == (std::vector<std::string_view>{ string }));
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
