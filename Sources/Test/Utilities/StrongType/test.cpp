// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <format>
#include <map>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#define BOOST_TEST_MODULE strongtype

#include "Utilities/Type/StrongType/Internal/Convert.hpp"
#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Divisible.hpp"
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(strong_type_test)
{
    using StrongIntType = StrongType<int, struct IntTag>;
    using StrongIntRefType = StrongType<int&, struct IntRefTag>;
    using OtherStrongIntType = StrongType<int, struct OtherIntTag>;

    int raw_int = 42;
    auto* raw_int_ptr = &raw_int;

    auto strong_int = StrongIntType(raw_int);
    auto strong_int_ref = StrongIntRefType(raw_int);

    BOOST_REQUIRE(strong_int.Get() == raw_int);
    BOOST_REQUIRE(strong_int_ref.Get() == raw_int);
    BOOST_REQUIRE(&strong_int_ref.Get() == raw_int_ptr);
    BOOST_REQUIRE(sizeof(StrongIntType) == sizeof(int));

    auto other_strong_int = OtherStrongIntType(raw_int);
    auto casted_strong_int = StrongIntType(other_strong_int.Get());

    BOOST_REQUIRE(casted_strong_int.Get() == strong_int.Get());

    strong_int_ref.Get() = 3;
    BOOST_REQUIRE(raw_int == 3);

    using StrongStringType = StrongType<std::string, struct StringTag>;
    using StrongStringRefType = StrongType<std::string&, struct StringRefTag>;

    std::string raw_string = "foo";
    auto* raw_string_ref = &raw_string;

    auto strong_string = StrongStringType(raw_string);
    auto strong_string_ref = StrongStringRefType(raw_string);

    BOOST_REQUIRE(strong_string.Get() == raw_string);
    BOOST_REQUIRE(strong_string_ref.Get() == raw_string);
    BOOST_REQUIRE(&strong_string_ref.Get() == raw_string_ref);
    BOOST_REQUIRE(sizeof(StrongStringType) == sizeof(std::string));

    strong_string_ref.Get() = "bar";
    BOOST_REQUIRE(raw_string == "bar");
}


BOOST_AUTO_TEST_CASE(addable)
{
    using Width = StrongType<double, struct WidthTag, StrongTypeNS::Addable>;
    Width w1(5.);
    Width w2(10.);
    Width sum = w1 + w2;

    BOOST_CHECK_EQUAL(sum.Get(), w1.Get() + w2.Get());

    Width diff = sum - w2;
    BOOST_CHECK_EQUAL(diff.Get(), w1.Get());
}


BOOST_AUTO_TEST_CASE(addable_const)
{
    // Note: this case was failing compilation prior to #1620; a failure here would probably be at compile time rather
    // than runtime.
    using Width = StrongType<double, struct WidthTag, StrongTypeNS::Addable>;
    const Width w1(5.);
    Width w2(10.);
    Width sum = w1 + w2;
    Width sum2 = w2 + w1;
    BOOST_CHECK_EQUAL(sum.Get(), w1.Get() + w2.Get());
    BOOST_CHECK_EQUAL(sum2.Get(), w1.Get() + w2.Get());

    Width diff = sum - w1;
    Width diff2 = w1 - sum;
    BOOST_CHECK_EQUAL(diff.Get(), w2.Get());
    BOOST_CHECK_EQUAL(diff2.Get(), -w2.Get());
}


BOOST_AUTO_TEST_CASE(addable_and_incrementable)
{
    {
        // First block for sanity check: ensure the tests are valid!
        // This is not DRY, but that's not a crime in a test...
        using MyInt = std::size_t;
        MyInt w1(10UL);
        MyInt w2(5UL);

        const MyInt sum = w1++ + w2;
        BOOST_CHECK_EQUAL(w1, 11UL);
        BOOST_CHECK_EQUAL(w2, 5UL);
        BOOST_CHECK_EQUAL(sum, 15UL);

        MyInt sum2 = ++w1 + w2;
        BOOST_CHECK_EQUAL(w1, 12UL);
        BOOST_CHECK_EQUAL(w2, 5UL);
        BOOST_CHECK_EQUAL(sum2, 17UL);

        const MyInt sum3 = w1 + w2--;
        BOOST_CHECK_EQUAL(w1, 12UL);
        BOOST_CHECK_EQUAL(w2, 4UL);
        BOOST_CHECK_EQUAL(sum3, 17UL);

        const MyInt diff = --w1 - w2;
        BOOST_CHECK_EQUAL(w1, 11UL);
        BOOST_CHECK_EQUAL(w2, 4UL);
        BOOST_CHECK_EQUAL(diff, 7UL);

        w2 += w1;
        BOOST_CHECK_EQUAL(w2, 15UL);

        sum2 -= w2;
        BOOST_CHECK_EQUAL(sum2, 2UL);
    }

    {
        using MyInt = StrongType<std::size_t, struct WidthTag, StrongTypeNS::Addable, StrongTypeNS::Incrementable>;
        MyInt w1(10UL);
        MyInt w2(5UL);

        MyInt sum = w1++ + w2;
        BOOST_CHECK_EQUAL(w1.Get(), 11UL);
        BOOST_CHECK_EQUAL(w2.Get(), 5UL);
        BOOST_CHECK_EQUAL(sum.Get(), 15UL);

        MyInt sum2 = ++w1 + w2;
        BOOST_CHECK_EQUAL(w1.Get(), 12UL);
        BOOST_CHECK_EQUAL(w2.Get(), 5UL);
        BOOST_CHECK_EQUAL(sum2.Get(), 17UL);

        MyInt sum3 = w1 + w2--;
        BOOST_CHECK_EQUAL(w1.Get(), 12UL);
        BOOST_CHECK_EQUAL(w2.Get(), 4UL);
        BOOST_CHECK_EQUAL(sum3.Get(), 17UL);

        MyInt diff = --w1 - w2;
        BOOST_CHECK_EQUAL(w1.Get(), 11UL);
        BOOST_CHECK_EQUAL(w2.Get(), 4UL);
        BOOST_CHECK_EQUAL(diff.Get(), 7UL);

        w2 += w1;
        BOOST_CHECK_EQUAL(w2.Get(), 15UL);

        sum2 -= w2;
        BOOST_CHECK_EQUAL(sum2.Get(), 2UL);
    }
}


BOOST_AUTO_TEST_CASE(comparable)
{
    using Width = StrongType<int, struct WidthTag, StrongTypeNS::Comparable>;
    const Width w1(5);
    const Width w2(5);
    const Width w3(10);

    BOOST_CHECK(w1 == w2);
    BOOST_CHECK(w1 < w3);
    BOOST_CHECK(w1 <= w2);
    BOOST_CHECK(w1 >= w2);
    BOOST_CHECK(w3 > w2);
    BOOST_CHECK(w2 != w3);
}


BOOST_AUTO_TEST_CASE(unordered_map)
{
    using SerialNumber =
        StrongType<std::string, struct SerialNumberTag, StrongTypeNS::Comparable, StrongTypeNS::Hashable>;

    const std::string serial_string("ABC");
    const SerialNumber serial_number(serial_string);

    std::unordered_map<SerialNumber, int> hash_map{ { SerialNumber{ "AA11" }, 10 }, { SerialNumber{ "BB22" }, 20 } };

    std::map<SerialNumber, int> map{ { SerialNumber{ "AA11" }, 10 }, { SerialNumber{ "BB22" }, 20 } };

    BOOST_CHECK_EQUAL(map[SerialNumber{ "AA11" }], 10);
    BOOST_CHECK_EQUAL(hash_map[SerialNumber{ "BB22" }], 20);
}


BOOST_AUTO_TEST_CASE(printable)
{
    using SerialNumber = StrongType<std::string, struct SerialNumberTag, StrongTypeNS::Printable>;

    const SerialNumber serial_number("ABC");

    std::ostringstream oconv;
    oconv << serial_number;

    BOOST_CHECK_EQUAL(oconv.str(), "ABC");

    const auto from_format = std::format("From std::format: {}", serial_number);

    BOOST_CHECK_EQUAL(from_format, "From std::format: ABC");
}


// Should not compile by design!
// But compilation error message should be crystal clear.
// BOOST_AUTO_TEST_CASE(non_printable)
//{
//    using SerialNumber = StrongType<std::string, struct SerialNumberTag>;
//
//    const SerialNumber serial_number("ABC");
//
//    std::cout << serial_number << std::endl;
//}


// Should not compile by design!
// But compilation error message should be crystal clear.
// BOOST_AUTO_TEST_CASE(non_formattable)
//{
//    using SerialNumber = StrongType<std::string, struct SerialNumberTag>;
//
//    const SerialNumber serial_number("ABC");
//
//    const auto from_format = std::format("{}", serial_number);
//}


BOOST_AUTO_TEST_CASE(default_constructible)
{
    using Dbl = StrongType<double, struct DoubleTypeTag, StrongTypeNS::DefaultConstructible>;

    // This doesn't seem much, but the actual test failure is if the following line doesn't compile
    // (and it won't if the DefaultConstructible is omitted).
    [[maybe_unused]] const Dbl a;

    // One of the thing we want to be able to do with some StrongTypes...
    // Without DefaultConstructible resize() wouldn't work (you could work around with reserve / back_inserter in most
    // cases but still...)
    std::vector<Dbl> v;
    v.resize(5UL);

    // Check both way to initialize a zero value work (not that trivial - didn't work with former instantiation pre
    // require clause.
    using Int = StrongType<int, struct IntTypeTag, StrongTypeNS::DefaultConstructible>;

    [[maybe_unused]] const Int no_explicit_zero_arg{};

    [[maybe_unused]] const Int zero_arg{ 0 };
}


namespace TestNS::StrongTypeNS
{

    using Width = StrongType<int, struct WidthTag, ::MoReFEM::StrongTypeNS::Comparable>;

} // namespace TestNS::StrongTypeNS


BOOST_AUTO_TEST_CASE(comparable_in_namespace)
{
    const TestNS::StrongTypeNS::Width w1(5);
    const TestNS::StrongTypeNS::Width w2(5);
    const TestNS::StrongTypeNS::Width w3(10);

    BOOST_CHECK(w1 == w2);
    BOOST_CHECK(w1 < w3);
}


BOOST_AUTO_TEST_CASE(divisible_int)
{
    using UnsignedInt = StrongType<std::size_t, struct UnsignedIntTag, StrongTypeNS::Divisible>;
    const UnsignedInt w1(23UL);
    const UnsignedInt w2(10UL);

    UnsignedInt quotient = w1 / w2;
    BOOST_CHECK_EQUAL(quotient.Get(), 2UL);

    UnsignedInt remainder = w1 % w2;
    BOOST_CHECK_EQUAL(remainder.Get(), 3UL);
}


BOOST_AUTO_TEST_CASE(divisible_double)
{
    using Double = StrongType<double, struct DoubleTag, StrongTypeNS::Divisible>;
    const Double w1(23.5);
    const Double w2(10.);

    Double result = w1 / w2;
    BOOST_CHECK_EQUAL(result.Get(), 2.35);
}


BOOST_AUTO_TEST_CASE(vector_conversion)
{
    using Int = StrongType<int, struct IntTag>;
    std::vector<int> vector{ 2, 3, 5, 7 };

    auto strong_type_vector = Internal::StrongTypeNS::Convert<Int>(vector);

    BOOST_REQUIRE_EQUAL(vector.size(), strong_type_vector.size());
    for (auto i = 0UL; i < vector.size(); ++i)
        BOOST_CHECK_EQUAL(vector[i], strong_type_vector[i].Get());
}


BOOST_AUTO_TEST_CASE(static_cast_test)
{
    using Int = StrongType<int, struct IntTag>;
    const Int n(2);

    BOOST_CHECK_EQUAL(static_cast<int>(n), 2);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
