This test checks `Tuple::IndexOf ` works as advertised.

The test is purely static - if it fails it should be at compile time.