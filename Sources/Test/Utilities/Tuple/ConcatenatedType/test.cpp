// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE tuple_concatenated_type
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Containers/Tuple/Tuple.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


// clang-format off
BOOST_AUTO_TEST_CASE(two_tuples)
{
    using pod_tuple = std::tuple<int, double, float>;
    using another_pod_tuple = std::tuple<char, long>;
    
    static_assert(std::is_same
                  <
                     Utilities::Tuple::concatenated_type<pod_tuple, another_pod_tuple>,
                     std::tuple<int, double, float, char, long>
                  >());
                  
    static_assert(std::is_same
                  <
                     Utilities::Tuple::concatenated_type<pod_tuple, pod_tuple>,
                     std::tuple<int, double, float, int, double, float>
                  >());
}


BOOST_AUTO_TEST_CASE(with_refs_and_pointers)
{
    class Foo { };
    class Bar { };
    
    using value_tuple = std::tuple<int, double, float, Foo, Bar>;
    using with_ref_and_pointers = std::tuple<int*, Foo&, const Bar*>;
    
    static_assert(std::is_same
                  <
                     Utilities::Tuple::concatenated_type<value_tuple, with_ref_and_pointers>,
                     std::tuple<int, double, float, Foo, Bar, int*, Foo&, const Bar*>
                  >());
    
}
                  
BOOST_AUTO_TEST_CASE(empty_tuple)
{
    static_assert(std::is_same
                  <
                     Utilities::Tuple::concatenated_type<>,
                     std::tuple<>
                  >());
}
                  
                  
BOOST_AUTO_TEST_CASE(one_tuple)
{
    
    using pod_tuple = std::tuple<int, double, float>;
    
    static_assert(std::is_same
                  <
                     Utilities::Tuple::concatenated_type<pod_tuple>,
                     pod_tuple
                  >());
}
                  
                  
BOOST_AUTO_TEST_CASE(more_tuples)
{
    using tuple1 = std::tuple<char>;
    using tuple2 = std::tuple<int, double>;
    using tuple3 = std::tuple<float>;
    using tuple4 = std::tuple<long>;
    
    static_assert(std::is_same
                  <
                     Utilities::Tuple::concatenated_type<tuple1, tuple2, tuple3, tuple4>,
                     std::tuple<char, int, double, float, long>
                  >());
}

// clang-format on

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
