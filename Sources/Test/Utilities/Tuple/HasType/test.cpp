// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <tuple>
#include <type_traits>
#define BOOST_TEST_MODULE tuple_has_type
#include "Utilities/Containers/Tuple/Tuple.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_AUTO_TEST_CASE(simple_is_same)
{
    using tuple = std::tuple<int, float, double, float>;
    BOOST_REQUIRE((Utilities::Tuple::HasType<int, std::is_same, tuple>()));
    BOOST_REQUIRE((!Utilities::Tuple::HasType<char, std::is_same, tuple>()));
}


BOOST_AUTO_TEST_CASE(is_same_with_decay)
{
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays,modernize-avoid-c-arrays)
    using tuple = std::tuple<int&&, float[3], double&>;
    using decayed_tuple = Utilities::Tuple::Decay<tuple>::type;

    BOOST_REQUIRE((Utilities::Tuple::HasType<int, std::is_same, decayed_tuple>()));
    BOOST_REQUIRE((Utilities::Tuple::HasType<float*, std::is_same, decayed_tuple>()));
    BOOST_REQUIRE((Utilities::Tuple::HasType<double, std::is_same, decayed_tuple>()));
}


BOOST_AUTO_TEST_CASE(is_base_of)
{
    struct Base
    { };

    struct Derived : public Base
    { };

    using tuple = std::tuple<int, Derived, double>;

    BOOST_REQUIRE((!Utilities::Tuple::HasType<Base, std::is_same, tuple>()));
    BOOST_REQUIRE((Utilities::Tuple::HasType<Base, std::is_base_of, tuple>()));
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
