add_executable(TestTupleIndexOf)

target_sources(TestTupleIndexOf
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)

target_link_libraries(TestTupleIndexOf
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_boost_test_sequential_mode(NAME TupleIndexOf
                                   EXE TestTupleIndexOf
                                   TIMEOUT 5)

morefem_organize_IDE(TestTupleIndexOf Test/Utilities/Tuple Test/Utilities/Tuple/IndexOf)
