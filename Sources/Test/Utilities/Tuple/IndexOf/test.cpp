// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef>
#include <tuple>
#include <utility>
#define BOOST_TEST_MODULE tuple_index_of
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_AUTO_TEST_CASE(non_empty_tuple)
{
    using tuple = std::tuple<int, float, double*, float, int&>;

    static_assert(Utilities::Tuple::IndexOf<int, tuple>::value == 0UL);
    static_assert(Utilities::Tuple::IndexOf<float, tuple>::value == 3UL);
    // < not the best choice ever but this is assumed in the implementation

    static_assert(Utilities::Tuple::IndexOf<double*, tuple>::value == 2UL);
    static_assert(Utilities::Tuple::IndexOf<int&, tuple>::value == 4UL);

    struct A
    { };


    static_assert(Utilities::Tuple::IndexOf<A, tuple>::value == NumericNS::UninitializedIndex<std::size_t>());
}


BOOST_AUTO_TEST_CASE(empty_tuple)
{
    using tuple = std::tuple<>;

    // Uncomment this breaks compilation; it shouldn't.
    struct A
    { };
    static_assert(Utilities::Tuple::IndexOf<A, tuple>::value == NumericNS::UninitializedIndex<std::size_t>());
}


BOOST_AUTO_TEST_CASE(pair)
{
    using pair = std::pair<int, double>;

    static_assert(Utilities::Tuple::IndexOf<int, pair>::value == 0UL);
    static_assert(Utilities::Tuple::IndexOf<double, pair>::value == 1UL);

    struct A
    { };
    static_assert(Utilities::Tuple::IndexOf<A, pair>::value == NumericNS::UninitializedIndex<std::size_t>());
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
