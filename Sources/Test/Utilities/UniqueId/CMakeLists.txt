add_executable(TestUniqueId)

target_sources(TestUniqueId
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestUniqueId
                      PRIVATE
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_GEOMETRY}>
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      )

morefem_boost_test_sequential_mode(NAME UniqueId
                                   EXE TestUniqueId
                                   TIMEOUT 5)

morefem_organize_IDE(TestUniqueId Test/Utilities Test/Utilities/UniqueId)
