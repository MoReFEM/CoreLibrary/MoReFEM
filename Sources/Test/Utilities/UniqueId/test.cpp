// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef>
#include <string>

#include "Utilities/Filesystem/File.hpp"

#include "Geometry/Mesh/Format.hpp"

#define BOOST_TEST_MODULE unique_id
#include "Utilities/UniqueId/Exceptions/UniqueId.hpp"
#include "Utilities/UniqueId/UniqueId.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

// NOLINTBEGIN(readability-duplicate-include)

namespace // anonymous
{


    struct ClassNoIdAllowed : public MoReFEM::Crtp::UniqueId<ClassNoIdAllowed,
                                                             std::size_t,
                                                             MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                             MoReFEM::UniqueIdNS::DoAllowNoId::yes>
    {
        using parent = MoReFEM::Crtp::UniqueId<ClassNoIdAllowed,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::manual,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::yes>;

        ClassNoIdAllowed() : parent{ nullptr }
        { }


        explicit ClassNoIdAllowed(std::size_t id) : parent(id)
        { }

        static const std::string& ClassName()
        {
            static const std::string ret = "ClassNoIdAllowed";
            return ret;
        }
    };


    struct ClassManualId1 : public MoReFEM::Crtp::UniqueId<ClassManualId1,
                                                           std::size_t,
                                                           MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                           MoReFEM::UniqueIdNS::DoAllowNoId::no>
    {
        using parent = MoReFEM::Crtp::UniqueId<ClassManualId1,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::manual,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::no>;

        explicit ClassManualId1(std::size_t id) : parent(id)
        { }

        static const std::string& ClassName()
        {
            static const std::string ret = "ClassManualId1";
            return ret;
        }
    };


    template<class T>
    struct ClassManualId2 : public MoReFEM::Crtp::UniqueId<ClassManualId2<T>,
                                                           std::size_t,
                                                           MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                           MoReFEM::UniqueIdNS::DoAllowNoId::no>
    {
        using parent = MoReFEM::Crtp::UniqueId<ClassManualId2<T>,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::manual,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::no>;

        explicit ClassManualId2(std::size_t id) : parent(id)
        { }

        static const std::string& ClassName()
        {
            static const std::string ret = "ClassManualId2";
            return ret;
        }
    };


    struct ClassAutomaticId : public MoReFEM::Crtp::UniqueId<ClassAutomaticId,
                                                             std::size_t,
                                                             MoReFEM::UniqueIdNS::AssignationMode::automatic,
                                                             MoReFEM::UniqueIdNS::DoAllowNoId::no>
    {

        using parent = MoReFEM::Crtp::UniqueId<ClassAutomaticId,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::automatic,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::no>;

        ClassAutomaticId() = default;
    };


} // namespace


namespace Foo::Bar
{

    struct ClassWithinNamespaces : public MoReFEM::Crtp::UniqueId<ClassWithinNamespaces,
                                                                  std::size_t,
                                                                  MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                                  MoReFEM::UniqueIdNS::DoAllowNoId::no>
    {
        using parent = MoReFEM::Crtp::UniqueId<ClassWithinNamespaces,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::manual,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::no>;

        explicit ClassWithinNamespaces(std::size_t id) : parent(id)
        { }

        static const std::string& ClassName()
        {
            static const std::string ret = "ClassWithinNamespaces";
            return ret;
        }
    };

} // namespace Foo::Bar


BOOST_AUTO_TEST_CASE(manual_id)
{
    const ClassManualId1 first_object(4UL);
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 4UL);

    const ClassManualId1 second_object(10UL);
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), 10UL);

    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-variable.hpp"

    BOOST_CHECK_THROW(auto redundant_object = ClassManualId1(4UL), Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    BOOST_CHECK_THROW(auto reserved_value_object = ClassManualId1(NumericNS::UninitializedIndex<std::size_t>()),
                      Internal::ExceptionsNS::UniqueId::ReservedValue);
    PRAGMA_DIAGNOSTIC(pop)

    // > *** MoReFEM Doxygen end of group *** //
    ///@} // \addtogroup TestGroup
    // *** MoReFEM Doxygen end of group *** < //

    // Should not throw: this is not the same class.
    const ClassManualId2<int> int_template(10UL);
    BOOST_CHECK_EQUAL(int_template.GetUniqueId(), 10UL);

    const ClassManualId2<int> second_int_template(4UL);
    BOOST_CHECK_EQUAL(second_int_template.GetUniqueId(), 4UL);

    const ClassManualId2<double> double_template(10UL);
    BOOST_CHECK_EQUAL(int_template.GetUniqueId(), 10UL);

    const ClassManualId2<double> second_double_template(4UL);
    BOOST_CHECK_EQUAL(second_double_template.GetUniqueId(), 4UL);
}

BOOST_AUTO_TEST_CASE(automatic_id)
{
    const ClassAutomaticId first_object;
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 0UL);

    const ClassAutomaticId second_object;
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), 1UL);
}


BOOST_AUTO_TEST_CASE(no_id)
{
    const ClassNoIdAllowed first_object;
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), NumericNS::UninitializedIndex<std::size_t>());

    const ClassNoIdAllowed second_object;
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), NumericNS::UninitializedIndex<std::size_t>());

    const ClassNoIdAllowed third_object(4UL);
    BOOST_CHECK_EQUAL(third_object.GetUniqueId(), 4UL);
}


BOOST_AUTO_TEST_CASE(clear)
{
    const ClassManualId1 first_object(100UL);
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 100UL);

    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-variable.hpp"

    // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
    BOOST_CHECK_THROW(auto redundant_object = ClassManualId1(100UL),
                      Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    PRAGMA_DIAGNOSTIC(pop)

    // > *** MoReFEM Doxygen end of group *** //
    ///@} // \addtogroup TestGroup
    // *** MoReFEM Doxygen end of group *** < //

    ClassManualId1::ClearUniqueIdList();

    const ClassManualId1 second_object(100UL); // no throw as content was cleared.
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), 100UL);

    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), second_object.GetUniqueId());
    // < To show TestNS::ClearSingletons<time_manager_type>::Do() should not be used
    // < lightly: if ill-used our unique ids become not so unique...
}


BOOST_FIXTURE_TEST_CASE(generate_unique_id, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();

    const ::MoReFEM::FilesystemNS::File mesh_file{ "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx1_Ny1_force_label.mesh" };

    mesh_manager.Create(MeshNS::unique_id{ 1UL },
                        mesh_file,
                        ::MoReFEM::GeometryNS::dimension_type{ 2 },
                        ::MoReFEM::MeshNS::Format::Medit,
                        ::MoReFEM::CoordsNS::space_unit_type{ 1. });

    const auto generated_id = MoReFEM::Internal::MeshNS::MeshManager::GenerateUniqueId();

    BOOST_CHECK_EQUAL(generated_id, MeshNS::unique_id{ 2UL }); // +1 after last one found in the list

    BOOST_CHECK_EQUAL(mesh_manager.GenerateUniqueId(),
                      MeshNS::unique_id{ 2UL }); // A call to GenerateUniqueId() don't add the result in the
    // list; this is done only if a new mesh is created using this result.

    mesh_manager.Create(generated_id,
                        mesh_file,
                        ::MoReFEM::GeometryNS::dimension_type{ 2 },
                        ::MoReFEM::MeshNS::Format::Medit,
                        ::MoReFEM::CoordsNS::space_unit_type{ 1. });

    BOOST_CHECK_EQUAL(mesh_manager.GenerateUniqueId(), MeshNS::unique_id{ 3UL });
}


BOOST_AUTO_TEST_CASE(equivalent_namespace_path)
{
    const Foo::Bar::ClassWithinNamespaces first_object(3UL);

    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 3UL);

    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-variable.hpp"

    // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
    BOOST_CHECK_THROW(auto redundant_object = ::Foo::Bar::ClassWithinNamespaces(3UL),
                      Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    using namespace Foo;

    // NOLINTNEXTLINE(clang-diagnostic-unused-variable)
    BOOST_CHECK_THROW(auto redundant_object = Bar::ClassWithinNamespaces(3UL),
                      Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    PRAGMA_DIAGNOSTIC(pop)

    // > *** MoReFEM Doxygen end of group *** //
    ///@} // \addtogroup TestGroup
    // *** MoReFEM Doxygen end of group *** < //
}


PRAGMA_DIAGNOSTIC(pop)

// NOLINTEND(readability-duplicate-include)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
