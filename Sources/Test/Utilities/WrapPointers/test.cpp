// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <memory>
#include <string>
#include <type_traits>

#define BOOST_TEST_MODULE wrap_pointers
#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    class FriendlyClass;

    class WithPrivateConstructor
    {
        friend FriendlyClass;

        explicit WithPrivateConstructor(int a, const std::string& str) : a_{ a }, str_(str)
        { }

        int a_;

        const std::string& str_;

      public:
        [[nodiscard]] int GetA() const noexcept
        {
            return a_;
        }

        [[nodiscard]] const std::string& GetStr() const noexcept
        {
            return str_;
        }
    };


    class FriendlyClass
    {
      public:
        static std::shared_ptr<WithPrivateConstructor> CreateShared(int a, const std::string& str)
        {
            return MoReFEM::Internal::WrapShared(new WithPrivateConstructor(a, str));
        }

        static std::unique_ptr<WithPrivateConstructor> CreateUnique(int a, const std::string& str)
        {
            return MoReFEM::Internal::WrapUnique(new WithPrivateConstructor(a, str));
        }

        static std::shared_ptr<const WithPrivateConstructor> CreateConstShared(int a, const std::string& str)
        {
            return MoReFEM::Internal::WrapSharedToConst(new WithPrivateConstructor(a, str));
        }

        static std::unique_ptr<const WithPrivateConstructor> CreateConstUnique(int a, const std::string& str)
        {
            return MoReFEM::Internal::WrapUniqueToConst(new WithPrivateConstructor(a, str));
        }
    };

} // namespace


BOOST_AUTO_TEST_CASE(wrap_shared)
{
    const std::string hello = "Hello world!";
    auto shared = FriendlyClass::CreateShared(5, hello);
    static_assert(std::is_same<decltype(shared), std::shared_ptr<WithPrivateConstructor>>());
    BOOST_CHECK(shared->GetA() == 5);
    BOOST_CHECK(shared->GetStr() == hello);
}

BOOST_AUTO_TEST_CASE(wrap_const_shared)
{
    const std::string hello = "Hello world!";
    auto shared = FriendlyClass::CreateConstShared(5, hello);
    static_assert(std::is_same<decltype(shared), std::shared_ptr<const WithPrivateConstructor>>());
    BOOST_CHECK(shared->GetA() == 5);
    BOOST_CHECK(shared->GetStr() == hello);
}

BOOST_AUTO_TEST_CASE(wrap_unique)
{
    const std::string hello = "Hello world!";
    auto unique = FriendlyClass::CreateUnique(15, hello);
    static_assert(std::is_same<decltype(unique), std::unique_ptr<WithPrivateConstructor>>());
    BOOST_CHECK(unique->GetA() == 15);
    BOOST_CHECK(unique->GetStr() == hello);
}

BOOST_AUTO_TEST_CASE(wrap_const_unique)
{
    const std::string hello = "Hello world!";
    auto unique = FriendlyClass::CreateConstUnique(15, hello);
    static_assert(std::is_same<decltype(unique), std::unique_ptr<const WithPrivateConstructor>>());
    BOOST_CHECK(unique->GetA() == 15);
    BOOST_CHECK(unique->GetStr() == hello);
}


PRAGMA_DIAGNOSTIC(pop)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
