// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_EIGEN_EIGEN_DOT_HPP_
#define MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_EIGEN_EIGEN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/alloca.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/class-memaccess.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated-copy-with-dtor.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/extra-semi-stmt.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/extra-semi.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/float-equal.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/maybe-uninitialized.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/missing-noreturn.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/old_style_cast.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-identifier.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/sign-conversion.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/stack-protector.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/strict-aliasing.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-template.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"

//! Define this symbol to enable runtime tests for allocations.
#define EIGEN_RUNTIME_NO_MALLOC

#include "eigen3/Eigen/Dense" // IWYU pragma: export

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_EIGEN_EIGEN_DOT_HPP_
// *** MoReFEM end header guards *** < //
