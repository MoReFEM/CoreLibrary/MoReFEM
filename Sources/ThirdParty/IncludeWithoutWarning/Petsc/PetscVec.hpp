// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_PETSC_PETSCVEC_DOT_HPP_
#define MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_PETSC_PETSCVEC_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Warnings/Pragma.hpp" // IWYU pragma: export

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")

#include "Utilities/Warnings/Internal/IgnoreWarning/comma.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/float-equal.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/redundant-decls.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-identifier.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/undef.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"

#include "petscvec.h" // IWYU pragma: export

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_PETSC_PETSCVEC_DOT_HPP_
// *** MoReFEM end header guards *** < //
