### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/PetscConf.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscMacros.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscMat.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscMatPrivate.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscSfTypes.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscSnes.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscSys.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscSysTypes.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscVec.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscVersion.hpp
		${CMAKE_CURRENT_LIST_DIR}/PetscViewer.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

