// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_SCOTCH_SCOTCH_DOT_HPP_
#define MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_SCOTCH_SCOTCH_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Warnings/Pragma.hpp" // IWYU pragma: export

PRAGMA_DIAGNOSTIC(push)

PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-align")

#include "Utilities/Warnings/Internal/IgnoreWarning/cast-function-type.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/suggest-destructor-override.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/suggest-override.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/undef.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/weak-vtables.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"

PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

// Deactivating the formatting is important below: clang-format would put this in alphabetic order, and metis/parmetis
// stubs need first the Scotch includes.

// clang-format off
#include "scotch.h"  // IWYU pragma: export
#include "ptscotch.h"  // IWYU pragma: export

#include "metis.h"  // IWYU pragma: export
#include "parmetis.h"  // IWYU pragma: export
// clang-format on

PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_SCOTCH_SCOTCH_DOT_HPP_
// *** MoReFEM end header guards *** < //
