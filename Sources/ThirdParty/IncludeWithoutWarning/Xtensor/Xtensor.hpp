// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_XTENSOR_XTENSOR_DOT_HPP_
#define MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_XTENSOR_XTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

PRAGMA_DIAGNOSTIC(ignored "-Wshadow")
PRAGMA_DIAGNOSTIC(ignored "-Wundef")
PRAGMA_DIAGNOSTIC(ignored "-Wswitch-enum")
PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")

#include "Utilities/Warnings/Internal/IgnoreWarning/array-bounds.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/comma.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/extra-semi-stmt.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/float-equal.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/maybe-uninitialized.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/newline-eof.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reorder.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/shadow-field-in-constructor.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/stringop-overflow.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-local-typedef.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-template.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/weak-vtables.hpp"

#include "xtensor-blas/xlinalg.hpp" // IWYU pragma: export
#include "xtensor/xio.hpp"          // IWYU pragma: export
#include "xtensor/xnoalias.hpp"     // IWYU pragma: export
#include "xtensor/xtensor.hpp"      // IWYU pragma: export
#include "xtensor/xview.hpp"        // IWYU pragma: export

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_XTENSOR_XTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
