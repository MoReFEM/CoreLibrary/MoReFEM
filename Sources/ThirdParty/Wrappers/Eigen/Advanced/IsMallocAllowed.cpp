// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <format>
#include <iostream>
#include <set>
#include <source_location>
#include <string>

#include "ThirdParty/Wrappers/Eigen/Advanced/IsMallocAllowed.hpp"

#include "Utilities/SourceLocation/SourceLocation.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp" // IWYU pragma: keep


namespace MoReFEM::Advanced::Wrappers::EigenNS
{


    namespace // anonymous
    {


        /*!
         * \brief Print information about where Eigen::internal::set_is_malloc_allowed() is invoked.
         *
         * This may be helpful if you get the following assert
         \code
         Assertion failed: (is_malloc_allowed()
         && "heap allocation is forbidden (EIGEN_RUNTIME_NO_MALLOC is defined and g_is_malloc_allowed is false)")
         \endcode
         * and want to identify where the assert was triggered.
         *
         */
        struct PrintIfRequired
        {
            PrintIfRequired(bool value, print_is_malloc_allowed behaviour, std::source_location location);


          private:
            // Helper variable to enforce do_print_duplicate = false.
            static inline std::set<std::string> already_printed_list;
        };

    } // namespace


    print_is_malloc_allowed& GetNonCstPrintIsMallocAllowedBehaviour()
    {
        static print_is_malloc_allowed ret = print_is_malloc_allowed::no;
        return ret;
    }


    void SetPrintIsMallocAllowedBehaviour(print_is_malloc_allowed behaviour)
    {
        GetNonCstPrintIsMallocAllowedBehaviour() = behaviour;
    }

#ifndef NDEBUG


    void IsMallocAllowed(bool value, const std::source_location location)
    {
        const print_is_malloc_allowed behaviour = GetNonCstPrintIsMallocAllowedBehaviour();

        PrintIfRequired(value, behaviour, location);

        Eigen::internal::set_is_malloc_allowed(value);
    }
#else

    void IsMallocAllowed([[maybe_unused]] bool value, [[maybe_unused]] const std::source_location location)
    { }

#endif // NDEBUG


    namespace // anonymous
    {


        [[maybe_unused]] PrintIfRequired::PrintIfRequired(bool value,
                                                          print_is_malloc_allowed behaviour,
                                                          const std::source_location location)
        {
            if (behaviour != print_is_malloc_allowed::no)
            {
                const auto key = MoReFEM::Utilities::GenerateSourceLocationKey(location);

                if (behaviour == print_is_malloc_allowed::each_time || not already_printed_list.contains(key))
                {
                    std::cout << std::format("Set IsMallocAllowed to {} in {} at line {}",
                                             value,
                                             location.file_name(),
                                             location.line())
                              << std::endl; // NOLINT(performance-avoid-endl) - we really want to print it exactly when
                                            // the call occurs.
                    already_printed_list.insert(key);
                }
            }
        }


    } // namespace


} // namespace MoReFEM::Advanced::Wrappers::EigenNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
