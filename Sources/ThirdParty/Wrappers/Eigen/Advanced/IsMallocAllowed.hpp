// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_ADVANCED_ISMALLOCALLOWED_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_ADVANCED_ISMALLOCALLOWED_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>


namespace MoReFEM::Advanced::Wrappers::EigenNS
{


    /*!
     * \brief Enum class to dictate what is printed at a `IsMallocAllowed` call (in debug mode only)
     *
     * - `no`: Nothing is printed.
     * - `first_occurrence`: A call at a given file and line is printed only the first time it is invoked.
     * - `each_time`: Each call is logged.
     */

    enum class print_is_malloc_allowed { no, each_time, first_occurrence };


#ifndef NDEBUG

    /*!
     * \brief Thin wrapper over  `Eigen::internal::set_is_malloc_allowed()` that may print the location where it is
     called.
     *
     * When you obtain the Eigen assert:
     *
     * \code
     Assertion failed: (is_malloc_allowed()
     && "heap allocation is forbidden (EIGEN_RUNTIME_NO_MALLOC is defined and g_is_malloc_allowed is false)")
     \endcode
     *
     * it is not easy to figure out where it happens.
     * Current wrapper just add the location of each `IsMallocAllowed` call if a variable is set to true in the
     IsMallocAllowed.cpp file,
     * prints will in this case be added which will help figuring out in which portion of the code the assert was
     triggered.
     *
     * \param[in] value True if malloc is allowed, false otherwise. In production false is expected almost elsewhere!
     * \copydoc doxygen_hide_source_location
     */
    void IsMallocAllowed(bool value, const std::source_location location = std::source_location::current());


#else

    //! Does nothing - this is really a debug mode feature!
    void IsMallocAllowed(bool value, const std::source_location location = std::source_location::current());


#endif

    /*!
     * \brief Accessor to a variable which tells what should be printed when \a IsMallocAllowed() is called.
     *
     * \return Reference to the value which dictates the behaviour.
     *
     * \attention You shouldn't call it directly: \a SetPrintIsMallocAllowedBehaviour() is more expressive.
     */
    print_is_malloc_allowed& GetNonCstPrintIsMallocAllowedBehaviour();


    /*!
     * \brief Choose the behaviour when \a IsMallocAllowed() is called.
     *
     * \param[in] behaviour Chosen behaviour.
     * - `no`: Nothing is printed.
     * - `first_occurrence`: A call at a given file and line is printed only the first time it is invoked.
     * - `each_time`: Each call is logged.
     *
     */
    void SetPrintIsMallocAllowedBehaviour(print_is_malloc_allowed behaviour);


} // namespace MoReFEM::Advanced::Wrappers::EigenNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_ADVANCED_ISMALLOCALLOWED_DOT_HPP_
// *** MoReFEM end header guards *** < //
