// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>

#include "ThirdParty/Wrappers/Eigen/Eigen.hpp"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Wrappers::EigenNS
{


    // NOLINTBEGIN(modernize-return-braced-init-list)
    DerivateGreenLagrangeMatrix InitDerivateGreenLagrangeMatrix(::Eigen::Index dimension)
    {
        switch (dimension)
        {
        case 1:
            return DerivateGreenLagrangeMatrix(1, 1);
        case 2:
            return DerivateGreenLagrangeMatrix(3, 4);
        case 3:
            return DerivateGreenLagrangeMatrix(6, 9);
        default:
            assert(false && "Dimension must be 1, 2 or 3!");
            exit(EXIT_FAILURE);
        }
    }


    d2WMatrix Initd2WMatrix(::Eigen::Index dimension)
    {
        switch (dimension)
        {
        case 1:
            return d2WMatrix(1, 1);
        case 2:
            return d2WMatrix(3, 3);
        case 3:
            return d2WMatrix(6, 6);
        default:
            assert(false && "Dimension must be 1, 2 or 3!");
            exit(EXIT_FAILURE);
        }
    }


    dWVector InitdWVector(::Eigen::Index dimension)
    {
        switch (dimension)
        {
        case 1:
            return dWVector(1);
        case 2:
            return dWVector(3);
        case 3:
            return dWVector(6);
        default:
            assert(false && "Dimension must be 1, 2 or 3!");
            exit(EXIT_FAILURE);
        }
    }
    // NOLINTEND(modernize-return-braced-init-list)


    DimensionMatrixVariant InitDimensionMatrixVariant(::Eigen::Index dimension)
    {
        switch (dimension)
        {
        case 1:

            return ::Eigen::Matrix<double, 1, 1>{};
        case 2:
            return ::Eigen::Matrix2d{};
        case 3:
            return ::Eigen::Matrix3d{};
        default:
            assert(false && "Dimension must be 1, 2 or 3!");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Wrappers::EigenNS
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
