// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_EIGEN_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_EIGEN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <variant>

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::Wrappers::EigenNS
{


    // ************************************************************************************************************
    // Aliases
    //
    // Eigen provides many useful aliases in its `Eigen` namespace, but we actually need more!
    // ************************************************************************************************************
    // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)


    /*!
     * \brief Alias to vector of  `SizeT`
     *
     * Eigen provides already some (e.g. `Eigen::Vector3d` but doesn't go high enough for our purpose (we need 6 and 9
     * at least for instance that are not covered).
     */
    // clang-format off
    template<int SizeT>
    using VectorNd =
    Eigen::Matrix
    <
        double,
        SizeT,
        1,
        Eigen::ColMajor
    >;
    // clang-format on

    //! Alias to vector which max size is given by the template argument `SizeMaxT`
    // clang-format off
    template<int SizeMaxT>
    using VectorMaxNd =
    Eigen::Matrix
    <
        double,
        Eigen::Dynamic,
        1,
        Eigen::ColMajor,
        SizeMaxT
    >;
    // clang-format on


    //! Alias to row vector which max size is given by the template argument `SizeMaxT`
    // clang-format off
    template<int SizeMaxT>
    using RowVectorMaxNd =
    Eigen::Matrix
    <
        double,
        1,
        Eigen::Dynamic,
        Eigen::RowMajor,
        1,
        SizeMaxT
    >;
    // clang-format on


    //! Alias to matrix which max size is (`NrowMaxT`, `NcolMaxT`).
    // clang-format off
    template<int NrowMaxT, int NcolMaxT = NrowMaxT>
    using MatrixMaxNd =
    Eigen::Matrix
    <
        double,
        Eigen::Dynamic,
        Eigen::Dynamic,
        Eigen::ColMajor,
        NrowMaxT,
        NcolMaxT
    >;
    // clang-format on

    /*!
     * \brief Type for a d2W square matrix
     *
     * A derivate Green-Lagrange matrix gets its size depending on the geometry dimension:
     * - (1, 1) for dimension 1
     * - (3, 3) for dimension 2
     * - (6, 6) for dimension 3
     *
     * \internal We could have used a `std::variant`, but when this type was introduced my priority was to avoid memory allocation in
     * local operators (#1937) and using the fourth and fifth template parameter of `Eigen::Matrix` just does the job
     * without incurring the cumbersome visitors (that might as well incur runtime overhead).
     */
    using d2WMatrix = MatrixMaxNd<6>;


    /*!
     * \brief Type for a Green-Lagrange matrix
     *
     * A derivate Green-Lagrange matrix gets its size depending on the geometry dimension:
     * - (1, 1) for dimension 1
     * - (3, 4) for dimension 2
     * - (6, 9) for dimension 3
     *
     * \internal We could have used a `std::variant`, but when this type was introduced my priority was to avoid memory allocation in
     * local operators (#1937) and using the fourth and fifth template parameter of `Eigen::Matrix` just does the job
     * without incurring the cumbersome visitors (that might as well incur runtime overhead).
     */
    using DerivateGreenLagrangeMatrix = MatrixMaxNd<6, 9>;


    /*!
     * \brief Type for a dW vector.
     *
     * Its size is:
     * - 1 for dimension 1
     * - 3 for dimension 2
     * - 6 for dimension 3
     */
    using dWVector = VectorMaxNd<6>;


    /*!
     * \brief Alias to a std::variant object which stores type for a square matrix of dimension 1, 2 or 3.
     *
     * This type should be used when the matrix requires computation of its determinant. If we do not use a
     * `std::variant` and rely instead of the aliases above that specify the maximum size the matrix may reach, the
     * determinant computation is not optimized for the specific matrix at hand and involves memory allocation... that
     * we really want to avoid in local operators, which is where the determinant might be required.
     *
     * The visitor used for the determinant computation might rebuke some devs, so a dedicated `ComputeDeterminant()`
     * function has been devised to avoid making it explicit.
     */
    // clang-format off
    using DimensionMatrixVariant =
    std::variant
    <
        Eigen::Matrix<double, 1, 1>,
        Eigen::Matrix2d,
        Eigen::Matrix3d
    >;
    // clang-format on


    // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers,readability-function-cognitive-complexity)


    // ************************************************************************************************************
    // Initialization of some matrices and vectors that depends on dimension.
    //
    // ************************************************************************************************************

    /*!
     * \brief Init a \a DerivateGreenLagrangeMatrix depending on the provided \a dimension
     *
     * \param[in] dimension Dimension of the geometry considered; must be 1, 2 or 3.
     *
     * \return The initialized matrix. Beware: its content is undefined - make sure to call `setZero()` if you want it filled with zeros.
     *
     *  \internal Strong type GeometryNS::dimension_type is not used here as \a Geometry is a higher level layer than \a
     * ThirdParty.
     */
    DerivateGreenLagrangeMatrix InitDerivateGreenLagrangeMatrix(Eigen::Index dimension);

    /*!
     * \brief Init a \a d2WMatrix which size is specified by argument \a dimension.
     *
     * \param[in] dimension Dimension of the geometry; must be 1, 2 or 3.
     *
     * \return The initialized matrix. Beware: its content is undefined - make sure to call `setZero()` if you want it filled with zeros.
     */
    d2WMatrix Initd2WMatrix(Eigen::Index dimension);


    /*!
     * \brief Init a \a dWVector which size is specified by argument \a dimension.
     *
     * \param[in] dimension Dimension of the geometry; must be 1, 2 or 3.
     *
     * \return The initialized matrix. Beware: its content is undefined - make sure to call `setZero()` if you want it filled with zeros.
     */
    dWVector InitdWVector(Eigen::Index dimension);


    /*!
     * \brief Init a \a DimensionMatrix depending on the provided \a dimension
     *
     * \param[in] dimension Dimension of the matrix; must be 1, 2 or 3.
     *
     * \internal Strong type GeometryNS::dimension_type is not used here as \a Geometry is a higher level layer than \a ThirdParty.
     *
     * \return The initialized matrix. Beware: its content is undefined - make sure to call `setZero()` if you want it filled with zeros.
     */
    DimensionMatrixVariant InitDimensionMatrixVariant(Eigen::Index dimension);


    /*!
     * \brief Compute the determinant of \a variant_matrix.
     *
     * \a DimensionMatrix is a `std::variant`, and the visitor syntax is not the prettiest to use.
     *  Goal of current function is just to provide syntactic sugar over it.
     *
     * \param[in] variant_matrix std::variant object which stores a square matrix of dimension 1, 2 or 3.
     *
     * \return Determinant of the matrix stored in the variant variable \a variant_matrix.
     */
    double ComputeDeterminant(const DimensionMatrixVariant& variant_matrix);


    /*!
     * \brief Tells whether `T` is an instantiation of an Eigen linear algebra object or not.
     *
     * An Eigen vector is considered here as well (in Eigen both are instantiations of `Eigen::Matrix` template class).
     */
    template<typename T>
    struct IsLinearAlgebra : std::false_type
    { };

    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // clang-format off
    template
    <
        class ScalarT,
        int RowsAtCompileTime,
        int ColsAtCompileTime, 
        int Options,
        int MaxRowsAtCompileTime,
        int MaxColsAtCompileTime
    >
    // clang-format on
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    struct IsLinearAlgebra<
        Eigen::
            Matrix<ScalarT, RowsAtCompileTime, ColsAtCompileTime, Options, MaxRowsAtCompileTime, MaxColsAtCompileTime>>
    : std::true_type
    { };


    /*!
     * \brief Tells whether `T` is an instantiation of an Eigen vector or not.
     *
     * \attention `RowVector` are not identified by this facility!
     */
    template<typename T>
    struct IsVector : std::false_type
    { };


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // clang-format off
    template
    <
        class ScalarT,
        int RowsAtCompileTime,
        int Options,
        int MaxRowsAtCompileTime
    >
    // clang-format on
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    struct IsVector<Eigen::Matrix<ScalarT, RowsAtCompileTime, 1, Options, MaxRowsAtCompileTime>> : std::true_type
    { };


    /*!
     * \brief Tells whether `T` is an instantiation of an Eigen matrix or not.
     *
     * \attention `Vector` and `RowVector` yield false by design: the purpose is to be able to check whether we're considering a full fledged matrix.
     * If you want `Vector` to pass as well, use `IsLinearAlgebra` instead.
     */
    template<typename T>
    struct IsMatrix : std::false_type
    { };


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // clang-format off
    template
    <
        class ScalarT,
        int RowsAtCompileTime,
        int Options,
        int MaxRowsAtCompileTime,
        int MaxColsAtCompileTime
    >
    // clang-format on
    struct IsMatrix<Eigen::Matrix<ScalarT, RowsAtCompileTime, 1, Options, MaxRowsAtCompileTime, MaxColsAtCompileTime>>
    : std::false_type
    { };


    // clang-format off
    template
    <
        class ScalarT,
        int ColsAtCompileTime,
        int Options,
        int MaxRowsAtCompileTime,
        int MaxColsAtCompileTime
    >
    // clang-format on
    struct IsMatrix<Eigen::Matrix<ScalarT, 1, ColsAtCompileTime, Options, MaxRowsAtCompileTime, MaxColsAtCompileTime>>
    : std::false_type
    { };


    // clang-format off
    template
    <
        class ScalarT,
        int RowsAtCompileTime,
        int ColsAtCompileTime,
        int Options,
        int MaxRowsAtCompileTime,
        int MaxColsAtCompileTime
    >
    // clang-format on
    struct IsMatrix<
        Eigen::
            Matrix<ScalarT, RowsAtCompileTime, ColsAtCompileTime, Options, MaxRowsAtCompileTime, MaxColsAtCompileTime>>
    : std::true_type
    { };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    /*!
     * \brief Broadcast the content of two vectors into a matrix
     *
     * so that
     \verbatim
     M(i, j) = v1(i) * v2(j)
     \endverbatim
     *
     * For instance:
     *
     * \verbatim
     v1 = [0, 10, 100, 1000];
     v2 = [1, 2, 3];

     Expected result is matrix (4, 3) with:

     [
        0, 0, 0
        10, 20, 30
        100, 200, 300
        1000, 2000, 3000
     ]
     \endverbatim
     *
     * \param[in] row_vector The vector that acts as \a v1 in the example above. **Beware:** it is expected to be a
     column vector, not a \a RowVector specialization!
     * \param[in] col_vector The vector that acts as \a v2 in the example above.
     * \param[out] ret The expected matrix. It is expected to be already allocated.
     */
    template<class VectorForRowT, class VectorForColT, class MatrixT>
    void BroadcastTwoVectorsIntoMatrix(const VectorForRowT& row_vector, const VectorForColT& col_vector, MatrixT& ret)
        requires(IsVector<VectorForRowT>::value && IsVector<VectorForColT>::value && IsMatrix<MatrixT>::value);


} // namespace MoReFEM::Wrappers::EigenNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Eigen/Eigen.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_EIGEN_DOT_HPP_
// *** MoReFEM end header guards *** < //
