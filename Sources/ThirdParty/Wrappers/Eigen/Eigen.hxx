// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_EIGEN_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_EIGEN_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Eigen/Eigen.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Wrappers::EigenNS
{


    inline double ComputeDeterminant(const DimensionMatrixVariant& variant_matrix)
    {
        return std::visit(
            [](const auto& matrix) -> double
            {
                return matrix.determinant();
            },
            variant_matrix);
    }


    template<class VectorForRowT, class VectorForColT, class MatrixT>
    void BroadcastTwoVectorsIntoMatrix(const VectorForRowT& row_vector, const VectorForColT& col_vector, MatrixT& ret)
        requires(IsVector<VectorForRowT>::value && IsVector<VectorForColT>::value && IsMatrix<MatrixT>::value)
    {
        assert(ret.rows() == row_vector.size());
        assert(ret.cols() == col_vector.size());

        ret.colwise() = row_vector;
        ret = ret * col_vector.transpose().asDiagonal();
    }


} // namespace MoReFEM::Wrappers::EigenNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_EIGEN_DOT_HXX_
// *** MoReFEM end header guards *** < //
