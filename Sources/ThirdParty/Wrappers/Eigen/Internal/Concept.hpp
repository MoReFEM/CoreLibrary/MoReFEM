// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_INTERNAL_CONCEPT_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_INTERNAL_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::Internal::Concept
{

    /*!
     * \brief Concept to identify whether \a T is an instantiation of an `Eigen::Matrix` template or not.
     */
    template<class T>
    constexpr bool IsEigenMatrixInstantiation = false;


    /*!
     * \brief Concept to identify whether \a T is an instantiation of an `Eigen::Matrix` template meant to represent a (column) vector.
     */
    template<class T>
    constexpr bool IsEigenVectorInstantiation = false;


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    template<typename Scalar,
             int RowsAtCompileTime,
             int ColsAtCompileTime,
             int Options,
             int MaxRowsAtCompileTime,
             int MaxColsAtCompileTime>
    constexpr bool IsEigenMatrixInstantiation<
        Eigen::
            Matrix<Scalar, RowsAtCompileTime, ColsAtCompileTime, Options, MaxRowsAtCompileTime, MaxColsAtCompileTime>> =
        true;
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    template<typename Scalar, int RowsAtCompileTime, int Options, int MaxRowsAtCompileTime>
    constexpr bool
        IsEigenVectorInstantiation<Eigen::Matrix<Scalar, RowsAtCompileTime, 1, Options, MaxRowsAtCompileTime>> = true;
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::Concept


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_EIGEN_INTERNAL_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
