// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LUA_FUNCTION_FUNCTION_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_LUA_FUNCTION_FUNCTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <functional>
#include <memory>
#include <sstream>
#include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"
#include "ThirdParty/Wrappers/Lua/LuaState/LuaState.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/Internal/LuaUtilityFunctions.hpp"


namespace MoReFEM::Wrappers::Lua
{


    //! \cond IGNORE_BLOCK_IN_DOXYGEN

    template<class T>
    struct Function;

    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    /*!
     * \brief High-level functor that handles the call to lua to get the result of a function
     * defined as an input datum.
     *
     * \internal <b><tt>[internal]</tt></b> This is defined as a child of std::function because it is really one
     * specific instance of it; nevertheless the only feature inherited is result_type alias. The derivation
     * could have been skipped and \code alias ResultTypeT result_type \endcode put instead in this class.
     * \endinternal
     *
     * \tparam ReturnTypeT Return type of the function.
     * \tparam Args Types of the arguments of the function. Currently all the arguments
     * should share the same type, due to a limitation in current Ops implementation. Hopefully Ops
     * will be extended to allow use of different arguments; anyway this interface is ready to face it
     * when possible.
     */
    template<typename ReturnTypeT, typename... Args>
    struct Function<ReturnTypeT(Args...)>
    {

        /// \name Special members.
        ///@{

        /*!
         * \brief Default constructor.
         *
         * Required only to be able to store a Function object in a tuple; should not be used otherwise.
         */
        explicit Function();

        /*!
         * \brief Effective constructor.
         *
         * \param[in] content The Lua function that is written as a string in the Lua file. For instance:
         * \verbatim
         function (x, y, z)
         return 3. * x + y - z
         end
         * \endverbatim
         */
        explicit Function(const std::string& content);

        //! Destructor.
        ~Function() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Function(const Function& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Function(Function&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Function& operator=(const Function& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Function& operator=(Function&& rhs) = default;


        ///@}

        /*!
         * \brief Ensure functor behaviour.
         *
         * \copydoc doxygen_hide_cplusplus_variadic_args
         *
         * \return Value computed by the functor.
         */
        ReturnTypeT operator()(Args... args) const;

        //! Returns the string that defined the Lua function in the first place.
        const std::string& GetString() const noexcept;

      private:
        //! Get internal lua state.
        lua_State* GetInternalState() const noexcept;

      private:
        //! Lua stack that will be used for function computations.
        Internal::LuaNS::LuaState state_;
    };


    //! Convenient alias for the most usual type of Lua function: a Lua function which returns a double
    //! and takes three arguments representing a point in 3D.
    using spatial_function = Function<double(double, double, double)>;


} // namespace MoReFEM::Wrappers::Lua


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Lua/Function/Function.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LUA_FUNCTION_FUNCTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
