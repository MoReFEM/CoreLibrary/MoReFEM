// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <new>
#include <sstream>
#include <string>
#include <utility>

#include "ThirdParty/Wrappers/Lua/LuaState/LuaState.hpp"

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Warnings/Pragma.hpp"


namespace MoReFEM::Internal::LuaNS
{


    namespace // anonymous
    {


        void OpenState(lua_State* state, const std::string& content);


    } // namespace


    LuaState::~LuaState()
    {
        if (state_ != nullptr) // the condition is not trivial: see the move assignment operator for instance to
                               // convince yourself...
            lua_close(state_);
    }


    LuaState::LuaState() : state_(luaL_newstate())
    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp" // IWYU pragma: keep
        if (state_ == nullptr)
            throw std::bad_alloc();
        PRAGMA_DIAGNOSTIC(pop)

        // > *** MoReFEM Doxygen end of group *** //
        ///@} // \addtogroup ThirdPartyGroup
        // *** MoReFEM Doxygen end of group *** < //
    }


    // NOLINTBEGIN(cppcoreguidelines-noexcept-move-operations,hicpp-noexcept-move,performance-noexcept-move-constructor,bugprone-exception-escape,cppcoreguidelines-prefer-member-initializer)
    // - please see class description to understand why we have to neutralize the move related warnings.
    // - regarding cppcoreguidelines-prefer-member-initializer, it is one of rare false positive (that happens only
    // in Linux run of clang-tidy ?!?) because here as we are using delegating constructors we can't use
    // member initialization.
    LuaState::LuaState(const std::string& content) : LuaState()

    {
        content_ = content;
        OpenState(state_, content_);
    }


    LuaState::LuaState(const LuaState& rhs) : LuaState()
    {
        content_ = rhs.content_;
        OpenState(state_, content_);
    }


    LuaState::LuaState(LuaState&& rhs) : LuaState()
    {
        content_ = std::move(rhs.content_);

        OpenState(state_, content_);

        if (rhs.state_ != nullptr)
        {
            lua_close(rhs.state_);
            rhs.state_ = nullptr;
        }

        rhs.content_.clear();
    }
    // NOLINTEND(cppcoreguidelines-noexcept-move-operations,hicpp-noexcept-move,performance-noexcept-move-constructor,bugprone-exception-escape,cppcoreguidelines-prefer-member-initializer)


    LuaState& LuaState::operator=(const LuaState& rhs)
    {
        if (this != &rhs)
        {
            if (state_ != nullptr)
                lua_close(state_);

            state_ = luaL_newstate();

            content_ = rhs.content_;
            OpenState(state_, content_);
        }

        return *this;
    }


    // NOLINTBEGIN(cppcoreguidelines-noexcept-move-operations,hicpp-noexcept-move,performance-noexcept-move-constructor,bugprone-exception-escape)
    // - please see class description to understand why we have to neutralize it.
    LuaState& LuaState::operator=(LuaState&& rhs)
    {
        if (this != &rhs)
        {
            if (state_ != nullptr)
                lua_close(state_);

            state_ = luaL_newstate();

            OpenState(state_, rhs.content_);
            content_ = rhs.content_;

            if (rhs.state_ != nullptr)
            {
                lua_close(rhs.state_);
                rhs.state_ = nullptr;
            }

            rhs.content_ = "";
        }

        return *this;
    }
    // NOLINTEND(cppcoreguidelines-noexcept-move-operations,hicpp-noexcept-move,performance-noexcept-move-constructor,bugprone-exception-escape)


    namespace // anonymous
    {


        void OpenState(lua_State* state, const std::string& content)
        {
            luaL_openlibs(state);

            if (!content.empty())
            {
                std::ostringstream oconv;
                oconv << "f = " << content; // function is arbitrarily called f.

                if (luaL_dostring(state, oconv.str().c_str()))
                    throw Exception("The string with the definition of the function couldn't be interpreted correctly; "
                                    "it was: \n"
                                    + content + "\n");
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::LuaNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
