// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LUA_LUASTATE_LUASTATE_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_LUA_LUASTATE_LUASTATE_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Lua/LuaState/LuaState.hpp"
// *** MoReFEM header guards *** < //


#include <string>

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    inline const std::string& LuaState::GetString() const noexcept
    {
        return content_;
    }


    inline lua_State* LuaState::GetInternal() const noexcept
    {
        return state_;
    }


} // namespace MoReFEM::Internal::LuaNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LUA_LUASTATE_LUASTATE_DOT_HXX_
// *** MoReFEM end header guards *** < //
