// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef>
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>

#include "ThirdParty/Wrappers/Lua/OptionFile/Exceptions/Exception.hpp"


namespace // anonymous
{


    std::string TooManyEntriesInVectorMsg(std::string_view field_name, std::size_t expected_dimension);


} // namespace


namespace MoReFEM::ExceptionNS::LuaOptionFileNS
{


    TooManyEntriesInVector::~TooManyEntriesInVector() = default;


    TooManyEntriesInVector::TooManyEntriesInVector(std::string_view field_name,
                                                   std::size_t expected_dimension,
                                                   const std::source_location location)
    : MoReFEM::Exception(TooManyEntriesInVectorMsg(field_name, expected_dimension), location)
    { }


} // namespace MoReFEM::ExceptionNS::LuaOptionFileNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string TooManyEntriesInVectorMsg(std::string_view field_name, std::size_t expected_dimension)
    {
        std::ostringstream oconv;

        oconv << "Something wrong happened while reading content of field " << field_name << ": " << expected_dimension
              << " value" << (expected_dimension > 1 ? "s were" : " was")
              << " expected but more were actually read. "
                 "This is a fairly low level catch so we can't be more specific; it might happen "
                 "for instance if for your entry in the Lua file a dimension was given and more "
                 "values were actually put as content.";

        return oconv.str();
    }

} // namespace
