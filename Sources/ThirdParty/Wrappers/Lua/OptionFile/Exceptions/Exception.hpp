// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <iosfwd>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::ExceptionNS::LuaOptionFileNS
{


    /*!
     * \brief Exception when there are too many values in a vector.
     *
     */
    class TooManyEntriesInVector final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] field_name Name of the field with too many entries.
         * \param[in] expected_dimension Expected dimension of the vector,
         * \copydoc doxygen_hide_source_location

         */
        explicit TooManyEntriesInVector(std::string_view field_name,
                                        std::size_t expected_dimension,
                                        const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~TooManyEntriesInVector() override;

        //! \copydoc doxygen_hide_copy_constructor
        TooManyEntriesInVector(const TooManyEntriesInVector& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        TooManyEntriesInVector(TooManyEntriesInVector&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TooManyEntriesInVector& operator=(const TooManyEntriesInVector& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        TooManyEntriesInVector& operator=(TooManyEntriesInVector&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::LuaOptionFileNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
