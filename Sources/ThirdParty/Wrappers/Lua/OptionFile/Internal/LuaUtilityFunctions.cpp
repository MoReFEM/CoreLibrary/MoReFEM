// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <ostream>
#include <sstream>
#include <string>

#include "ThirdParty/Wrappers/Lua/OptionFile/Internal/LuaUtilityFunctions.hpp"

#include "Utilities/Exceptions/Exception.hpp"

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    namespace // anonymous
    {


        /*!
         * \brief Sift through the sections/subsections and so forth to put them on Lua stack.
         *
         *
         * \param[in] full_name The full name with all the layers (e.g. VolumicMass.nature)
         * \param[in] entry_name Name of an entry that is accessible from the entry
         currently on top of the stack (e.g. nature).
         */
        void WalkDown(const std::string& full_name,
                      const std::string& entry_name,
                      lua_State* state,
                      std::source_location location);


    } // namespace


    void LuaStackDump(lua_State* state, std::ostream& stream)
    {
        stream << "Content of Lua stack is: " << '\n';

        int i = 0;
        const int top = lua_gettop(state);

        for (i = 1; i <= top; i++) /* repeat for each level */
        {
            const int t = lua_type(state, i);

            stream << "\t- ";

            switch (t)
            {
            case LUA_TSTRING: /* strings */
                stream << lua_tostring(state, i) << '\n';
                break;

            case LUA_TBOOLEAN: /* booleans */
                stream << (lua_toboolean(state, i) != 0 ? "true" : "false") << '\n';
                break;

            case LUA_TNUMBER: /* numbers */
                stream << lua_tonumber(state, i) << '\n';
                break;

            default: /* other values */
                stream << lua_typename(state, t) << '\n';
                break;
            }
        }
    }


    void PutOnStack(lua_State* state, const std::string& name, const std::source_location location)
    {
        assert(!name.empty()); // \todo Note: was handled in Ops but very likely not relevant for us.

        const auto end = name.find('.');
        assert(end != 0UL);

        // No section involved: the parameter was directly defined in root level.
        if (end == std::string::npos)
        {
            lua_getglobal(state, name.c_str());
        } else
        {
            lua_getglobal(state, name.substr(0, end).c_str());

            assert(end < name.size());
            assert(name[end] == '.');
            WalkDown(name, name.substr(end + 1), state, location);
        }
    }


    namespace // anonymous
    {


        // NOLINTBEGIN(bugprone-easily-swappable-parameters,misc-no-recursion)
        void WalkDown(const std::string& full_name,
                      const std::string& name,
                      lua_State* state,
                      const std::source_location location)
        {
            assert(!name.empty());

            if (lua_isnil(state, -1))
            {
                std::ostringstream oconv;
                oconv << "Invalid Lua file: unable to find field '" << full_name << "'.";
                throw Exception(oconv.str(), location);
            }

            // The sub-entries are introduced with ".".
            const auto end = name.find('.');

            if (end == std::string::npos)
            {
                lua_pushstring(state, name.c_str());
                lua_gettable(state, -2);
            } else
            {
                assert(end < name.size());
                assert(name[end] == '.');
                // One step down.
                {
                    lua_pushstring(state, name.substr(0, end).c_str());
                    lua_gettable(state, -2);
                    WalkDown(name, name.substr(end + 1), state, location);
                }
            }
        }
        // NOLINTEND(bugprone-easily-swappable-parameters,misc-no-recursion)


    } // namespace


} // namespace MoReFEM::Internal::LuaNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
