// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_INTERNAL_LUAUTILITYFUNCTIONS_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_INTERNAL_LUAUTILITYFUNCTIONS_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Lua/OptionFile/Internal/LuaUtilityFunctions.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstdlib>
#include <iostream>
#include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Miscellaneous.hpp"
#include "Utilities/Type/PrintTypeName.hpp"

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    template<typename T>
    void PushOnStack(lua_State* state, T value)
    {
        if constexpr (Utilities::IsSpecializationOf<std::tuple, T>())
        {
            // See https://stackoverflow.com/questions/16387354/template-tuple-calling-a-function-on-each-element.
            std::apply(
                [state](auto... x)
                {
                    (..., PushOnStack(state, x));
                },
                value);
        } else
        {
            if constexpr (std::is_same<T, bool>())
                lua_pushboolean(state, value);
            else if constexpr (std::is_same<T, std::string>())
                lua_pushstring(state, value.c_str());
            else if constexpr (std::is_integral<T>())
                lua_pushinteger(state, value);
            else if constexpr (std::is_floating_point<T>())
                lua_pushnumber(state, value);
            else
            {
                std::cerr << "PushingOnStack type " << GetTypeName<T>() << " was not foreseen..." << std::endl;
                assert(false);
                exit(EXIT_FAILURE);
            }
        }
    }


    template<typename T>
    T PullFromStack(lua_State* state, int index)
    {
        if constexpr (std::is_same<T, bool>())
            return lua_toboolean(state, index);
        else if constexpr (std::is_same<T, std::string>())
            return lua_tostring(state, index);
        else if constexpr (std::is_integral<T>())
            return static_cast<T>(lua_tointeger(state, index));
        else if constexpr (std::is_floating_point<T>())
            return static_cast<T>(lua_tonumber(state, index));
        else
        {
            std::cerr << "PullFromStack type " << GetTypeName<T>() << " was not foreseen..." << std::endl;
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::LuaNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_INTERNAL_LUAUTILITYFUNCTIONS_DOT_HXX_
// *** MoReFEM end header guards *** < //
