// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_OPTIONFILE_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_OPTIONFILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <iosfwd>
#include <memory>
#include <string>
#include <vector>

// IWYU pragma: no_include <__nullptr>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"      // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"        // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Wrappers::Lua
{


    /*!
     * \brief A class to load input data stored in a Lua file.
     *
     * This class is very inspired from https://gitlab.com/libops, which was used for a long time in MoReFEM. The
     * reason I no longer use it directly is that I needed extensions (e.g. handling of map containers) I wasn't
     * comfortable in adding directly in the sometimes weird libops logic. Current class is not a mere extension of
     * libops:
     * - Some libops features aren't present here, such as the reading of individual values of an array. The reason is
     * that OptionFile is used only by higher-level InputData which doesn't need this feature.
     * - On the other hand, (ordered) associative containers are now supported.
     * - Using a Lua function will be more efficient: libops reallocates a std::vector at each call, whereas I'm using
     * instead variadic parameters. So I do not have a limit of the number of parameters in a Lua function (but they
     * still should all be of the same type).
     *
     * It must be noted that I'm not familiar with Lua and Lua code has been mostly been left untouched.
     *
     * \attention This class requires Lua 5.3, whereas libops expected 5.1. The reason is that I wanted lua_isinteger
     * not yet in 5.1, whereas 5.2+ made compilation errors with part of libops code I didn't retain.
     */
    class OptionFile
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = OptionFile;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] filename Path of the Lua file to load.
         * \copydoc doxygen_hide_source_location
         */
        explicit OptionFile(const FilesystemNS::File& filename,
                            const std::source_location location = std::source_location::current());

        //! Destructor.
        ~OptionFile();

        //! \copydoc doxygen_hide_copy_constructor
        OptionFile(const OptionFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        OptionFile(OptionFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        OptionFile& operator=(const OptionFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        OptionFile& operator=(OptionFile&& rhs) = delete;

        ///@}

      public:
        /*!
         * \class doxygen_hide_lua_option_file_constraint
         *
         * \param[in] constraint constraint that the entry value must satisfy. The value is named 'v' by convention; the
         * constraint might be:
         * - A direct comparison, e.g. 'v > 10', 'v <= 3'. or  'v == "foo"'
         * - Checking it is among a list of choices, e.b. 'value_in(v, { "constant", "piecewise_per_domain", "function"}
         * )'
         * - Empty! (i.e. "")
         *
         * An exception is thrown is the constraint is not fulfilled.
         * In the case of a table, each entry in the table must fulfill the constraint.
         */


        /*!
         * \class doxygen_hide_lua_option_file_read
         *
         * \param[in] entry_name name of the entry.
         * \copydoc doxygen_hide_lua_option_file_constraint
         * \copydoc doxygen_hide_source_location
         */

        /*!
         * \class doxygen_hide_variant_selector_arg
         *
         * \tparam VariantSelectorT Type of \a variant_selector, which is assumed to be a specialization of
         * std::variant OR a std::vector of several such variants (this is clearly an internal level feature and
         * hopefully a end-user shouldn't deal with it directly).
         *
         * \param[in] variant_selector This is the variant (or a vector of variants) which has been properly
         * initialized with the right type. For instance, for a \a Parameter from the input data file, if the field
         * that address the nature says we expect a piecewise-constant per domain, \a variant_selector is expected
         * to have been initialized with a value of this type (the value itself is not yet the one written in the
         * input data - this is a preliminary step before the right value is assigned with the use of a std::visit).
         * The input data objects that make use of this are expected to define a static method called Selector which
         * initializes correctly the variant_selector; see for instance Internal::InputDataNS::ParamNS::Value class.
         * When \a variant_selector are completely irrelevant, convention is to pass nullptr (and set appropriately
         * \a VariantSelectorT to std::nullptr_t).
         */

        /*!
         * \brief Retrieves a value from the configuration file.
         *
         * \tparam T C++ type of the value read in the Lua file. Possible values are:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         * - std::vector<> (with the template parameter chosen among the ones mentioned above)
         * - std::map<> (with the template parameters for key and values chosen among the ones mentioned above).
         * - A specialization of Wrappers::Lua::LuaFunction
         * - A std::variant, for which the relevant alternatives are all the choices above.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         * \copydoc doxygen_hide_variant_selector_arg
         *
         * \param[out] ret The value of the entry.
         *
         * \return ret True if the value was properly read, false otherwise. In many cases false is not even an option (it is for the case we want to update a
         * Lua file).
         */
        template<class T, bool TolerateMissingFieldT = false, class VariantSelectorT = std::nullptr_t>
        bool Read(const std::string& entry_name,
                  std::string_view constraint,
                  T& ret,
                  VariantSelectorT&& variant_selector = nullptr,
                  const std::source_location location = std::source_location::current());

        //! Whether a given key is in the entry key list or not.
        //!  \param[in] entry_name name of the entry.
        //! \return True if it does exist, false otherwise. This is a strict comparison - typo, spaces or case must be
        //! absolutely the same!
        bool IsKey(std::string_view entry_name) const;

      public:
        /*!
         * \brief Get the list of all entries in the Lua file.
         *
         * Entries are in the format "section1.subsection.subsubsection.entry".
         *
         * \return List of all entries found.
         */
        const std::vector<std::string>& GetEntryKeyList() const noexcept;

        /*!
         * \brief Return the path of the Lua file which was interpreted.
         *
         * \return Path to the Lua file.
         */
        const FilesystemNS::File& GetFilename() const noexcept;

      private:
        /*!
         * \brief Open a Lua file and load it.
         *
         * The file is examined in a first step to determine all the entry keys.
         * Then luaL_dofile is called on it.
         *
         * \param[in] filename Path of the Lua file to load.
         * \copydoc doxygen_hide_source_location
         */
        void Open(const FilesystemNS::File& filename,
                  const std::source_location location = std::source_location::current());


      private:
        //! Get the Lua state.
        lua_State* GetNonCstLuaState() noexcept;


        /*!
         * \brief Convert the \a index_in_lua_stack -th element into a C++ type.
         *
         * \tparam T C++ type into which Lua data will be converted. Possible choices are:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         *
         * \tparam StringT The universal reference trick; read it as either const std::string& or std::string&&.
         *
         * \param[in] index_in_lua_stack Index to indicate which element in Lua stack must be converted. Usually -1 or
         * -2.
         * \param[in] entry_name Name of the entry for which the conversion occurs. Only used to display meaningful
         * error message,
         * \copydoc doxygen_hide_source_location
         * \copydoc doxygen_hide_variant_selector_arg
         *
         * \param[out] ret The value in a C++ type.
         */
        template<class T, class StringT, class VariantSelectorT>
        void Convert(int index_in_lua_stack,
                     StringT&& entry_name,
                     T& ret,
                     const VariantSelectorT& variant_selector,
                     const std::source_location location = std::source_location::current());

        //! Clears the Lua stack.
        void ClearStack();

        /*!
         * \brief Checks whether an entry satisfies a constraint.
         *
         * \tparam StringT The universal reference trick; read it as either const std::string& or std::string&&.
         *
         * \param[in] entry_name The entry for which the constraint is to be checked.
         * \copydoc doxygen_hide_lua_option_file_constraint
         * \copydoc doxygen_hide_source_location
         *
         */
        template<class StringT>
        void CheckConstraint(StringT&& entry_name,
                             std::string_view constraint,
                             const std::source_location location = std::source_location::current());


      private:
        /*!
         * \brief Sub-function of Read when T is a std::vector.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         *
         * \copydoc doxygen_hide_variant_selector_arg
         *
         * \param[out] vector The resulting std::vector with all its value properly filled from Lua data.
         */
        template<class T, class VariantSelectorT>
        void ReadVector(const std::string& entry_name,
                        std::string_view constraint,
                        T& vector,
                        VariantSelectorT&& variant_selector,
                        const std::source_location location = std::source_location::current());


        /*!
         * \brief Sub-function of Read when T is a std::map.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         *
         * \copydoc doxygen_hide_variant_selector_arg
         *
         * \param[out] map The resulting std::map with all its value properly filled from Lua data.
         */
        template<class T, class VariantSelectorT>
        void ReadMap(const std::string& entry_name,
                     std::string_view constraint,
                     T& map,
                     VariantSelectorT&& variant_selector,
                     const std::source_location location = std::source_location::current());


        /*!
         * \brief Set the list of all entry keys.
         *
         * This method should only be called once.
         *
         * \param[in] entry_key_list List of all the entries in the Lua file to be set.
         */
        void SetEntryKeyList(std::vector<std::string>&& entry_key_list);


      private:
        //! Lua state.
        lua_State* state_ = nullptr;

        /*!
         * \brief List of all entry keys found in the Lua file.
         *
         * \internal This list was provided by manual parsing, not by Lua directly.
         * \endinternal
         */
        std::vector<std::string> entry_key_list_;

        //! Filename read.
        FilesystemNS::File filename_;

        //! To avoid magic number.
        static constexpr auto most_recent_in_stack = -1;
    };


} // namespace MoReFEM::Wrappers::Lua


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_OPTIONFILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
