// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_OPTIONFILE_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_OPTIONFILE_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"
// *** MoReFEM header guards *** < //


// IWYU pragma: no_include <__nullptr>

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <variant>
#include <vector>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Type/PrintTypeName.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"
#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/Exceptions/Exception.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/Internal/LuaUtilityFunctions.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Lua { class OptionFile; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Lua
{


    inline lua_State* OptionFile::GetNonCstLuaState() noexcept
    {
        assert(!(!state_));
        return state_;
    }


    template<class T, bool TolerateMissingFieldT, class VariantSelectorT>
    bool OptionFile::Read(const std::string& entry_name,
                          std::string_view constraint,
                          T& ret,
                          VariantSelectorT&& variant_selector,
                          const std::source_location location)
    {
        Internal::LuaNS::PutOnStack(state_, entry_name, location);

        bool was_properly_read{ false };

        try
        {
            if (lua_isnil(state_, most_recent_in_stack))
            {
                decltype(auto) filename = GetFilename();
                std::ostringstream oconv;
                oconv << "Couldn't read entry '" << entry_name << "' in file '" << filename << "'.";
                throw Exception(oconv.str(), location);
            }

            if constexpr (Utilities::IsSpecializationOf<std::vector, T>())
                ReadVector(entry_name, constraint, ret, std::move(variant_selector), location);
            else if constexpr (Utilities::IsSpecializationOf<std::map, T>())
                ReadMap(entry_name, constraint, ret, std::move(variant_selector), location);
            else if constexpr (std::is_same_v<std::nullptr_t, T>)
                ret = nullptr;
            else
            {
                Convert(most_recent_in_stack, entry_name, ret, variant_selector, location);
                CheckConstraint(entry_name, constraint, location);
            }

            was_properly_read = true;
        }
        catch (const Exception&)
        {
            if (!TolerateMissingFieldT)
                throw;
        }

        ClearStack();

        return was_properly_read;
    }


    template<class T, class StringT, class VariantSelectorT>
    void OptionFile::Convert([[maybe_unused]] int index_in_lua_stack,
                             StringT&& entry_name,
                             [[maybe_unused]] T& ret,
                             const VariantSelectorT& variant_selector,
                             [[maybe_unused]] const std::source_location location)
    {
        if constexpr (Utilities::IsSpecializationOf<std::variant, T>())
        {
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/return-std-move-in-c++11.hpp" // IWYU pragma: keep
            const auto lbd = [this, index_in_lua_stack, entry_name, location](auto& value) -> T
            {
                using type = std::decay_t<decltype(value)>;

                type selected_value;

                this->Convert<type>(index_in_lua_stack, entry_name, selected_value, nullptr, location);

                return selected_value;
            };
            PRAGMA_DIAGNOSTIC(pop)

            ret = variant_selector;
            ret = std::visit(lbd, ret);
        } else if constexpr (std::is_same<T, bool>())
        {
            if (!lua_isboolean(state_, index_in_lua_stack))
                throw Exception("Entry " + entry_name
                                    + " is not a bool. Error is probably in the Read<>() type given in "
                                      "the call.",
                                location);

            ret = static_cast<bool>(lua_toboolean(state_, index_in_lua_stack));
        } else if constexpr (std::is_same<T, std::string>())
        {
            if (!lua_isstring(state_, index_in_lua_stack))
                throw Exception("Entry " + entry_name
                                    + " is not a string. Error is probably in the Read<>() type given in "
                                      "the call.",
                                location);

            ret = static_cast<std::string>(lua_tostring(state_, index_in_lua_stack));
        } else if constexpr (std::is_same<T, std::filesystem::path>())
        {
            if (!lua_isstring(state_, index_in_lua_stack))
                throw Exception("Entry " + entry_name
                                    + " is not a string. Error is probably in the Read<>() type given in "
                                      "the call.",
                                location);

            ret = std::filesystem::path{ static_cast<std::string>(lua_tostring(state_, index_in_lua_stack)) };
        } else if constexpr (std::is_integral<T>())
        {
            if (!lua_isinteger(state_, index_in_lua_stack))
            {
                std::ostringstream oconv;
                oconv << "Entry " << entry_name
                      << " is not an integer. Error is probably in the Read<>() type given "
                         "in the call.";

                throw Exception(oconv.str(), location);
            }

            ret = static_cast<T>(lua_tointeger(state_, index_in_lua_stack));
        } else if constexpr (std::is_floating_point<T>())
        {
            if (!lua_isnumber(state_, index_in_lua_stack))
            {
                std::ostringstream oconv;
                oconv << "Entry " << entry_name
                      << " is not a floating point. Error is probably in the Read<>() type "
                         "given in the call.";

                throw Exception(oconv.str(), location);
            }

            ret = static_cast<T>(lua_tonumber(state_, index_in_lua_stack));
        } else if constexpr (IsStrongType<T>())
        {
            typename T::underlying_type tmp;

            this->Convert<typename T::underlying_type>(index_in_lua_stack, entry_name, tmp, nullptr, location);
            ret = T(tmp);
        } else if constexpr (std::is_same<T, std::nullptr_t>()) // might happen to ignore a field for instance.
            ret = nullptr;
        else if constexpr (Utilities::IsSpecializationOf<Wrappers::Lua::Function, T>())
        {
            std::string function;

            this->Convert<std::string>(index_in_lua_stack, entry_name, function, nullptr, location);
            ret = T(function);
        } else if constexpr (Utilities::IsSpecializationOf<std::map, T>())
        {
            ReadMap(entry_name,
                    "", // no constraint checked when called from a variant (at least so far)
                    ret,
                    variant_selector,
                    location);
        } else if constexpr (Utilities::IsSpecializationOf<std::vector, T>())
        {
            ReadVector(entry_name,
                       "", // no constraint checked when called from a variant or as value of a map (at least so far)
                       ret,
                       variant_selector,
                       location);
        } else
        {
            std::cerr << "Conversion into " << GetTypeName<T>() << " was not foreseen..." << std::endl;
            assert(false && "Conversion not foreseen...");
            exit(EXIT_FAILURE);
        }
    }


    template<class StringT>
    void OptionFile::CheckConstraint(StringT&& name, std::string_view constraint, const std::source_location location)
    {
        if (constraint == "")
            return;

        std::ostringstream oconv;
        oconv << "function lua_opt_file_check_constraint(v)\nreturn " << constraint
              << "\nend\nlua_opt_file_result = lua_opt_file_check_constraint(" << name << ')';

        if (luaL_dostring(state_, oconv.str().c_str()))
        {
            oconv.str("");
            oconv << "Error in CheckConstraint while checking " << name << ":\n  "
                  << lua_tostring(state_, most_recent_in_stack);

            throw Exception(oconv.str(), location);
        }

        Internal::LuaNS::PutOnStack(state_, "lua_opt_file_result", location);

        assert(lua_isboolean(state_, most_recent_in_stack));
        const bool result = static_cast<bool>(lua_toboolean(state_, most_recent_in_stack));

        if (!result)
        {
            oconv.str("");
            oconv << "Constraint \"" << constraint << "\" is not fulfilled for \"" << name << "\"!";
            throw Exception(oconv.str(), location);
        }
    }


    template<class T, class VariantSelectorT>
    void OptionFile::ReadVector(const std::string& entry_name,
                                std::string_view constraint,
                                T& vector,
                                VariantSelectorT&& variant_selector,
                                const std::source_location location)
    {
        if (!lua_istable(state_, most_recent_in_stack))
            throw Exception("Error in Read: entry " + entry_name + " is not a table.", location);

        using element_type = typename T::value_type;

        // Now loops over all elements of the table.
        lua_pushnil(state_);

        [[maybe_unused]] std::size_t index = 0UL; // unused if VariantSelectorT amounts to something akin to  nullptr_t

        while (lua_next(state_, -2) != 0)
        {
            // Duplicates the key and value so that 'lua_tostring' (applied to
            // them) should not interfere with 'lua_next'.
            lua_pushvalue(state_, -2);
            lua_pushvalue(state_, -2);

            element_type element;

            using type = std::decay_t<decltype(variant_selector)>;

            if constexpr (std::is_same<type, std::nullptr_t>())
                Convert(most_recent_in_stack, entry_name, element, variant_selector, location);
            else
            {
                static_assert(Utilities::IsSpecializationOf<std::vector, type>());

                if (index >= variant_selector.size())
                    throw ExceptionNS::LuaOptionFileNS::TooManyEntriesInVector(entry_name, variant_selector.size());

                Convert(most_recent_in_stack, entry_name, element, variant_selector[index++], location);
            }

            vector.push_back(element);

            lua_pop(state_, 3);
        }

        const auto Nelem = vector.size();
        for (auto i = 0UL; i < Nelem; ++i)
            CheckConstraint(entry_name + "[" + std::to_string(i + 1) + "]", constraint, location);
    }


    template<class T, class VariantSelectorT>
    void OptionFile::ReadMap(const std::string& entry_name,
                             std::string_view constraint,
                             T& map,
                             VariantSelectorT&& variant_selector,
                             const std::source_location location)
    {
        if (!lua_istable(state_, most_recent_in_stack))
            throw Exception("Error in Read: entry " + entry_name + " is not an associative container.", location);

        using key_type = typename T::key_type;
        using mapped_type = typename T::mapped_type;

        // Now loops over all elements of the table.
        lua_pushnil(state_);
        while (lua_next(state_, -2) != 0)
        {
            // Duplicates the key and value so that 'lua_tostring' (applied to
            // them) should not interfere with 'lua_next'.
            lua_pushvalue(state_, -2);
            lua_pushvalue(state_, -2);

            key_type key;
            mapped_type element;

            Convert(-2, entry_name, key, variant_selector, location);
            Convert(most_recent_in_stack, entry_name, element, variant_selector, location);

            const auto check = map.insert(std::make_pair(key, element));

            if (!check.second)
                throw Exception("Error while reading entry \"" + entry_name + "\": same key read twice!");

            lua_pop(state_, 3);
        }

        for (const auto& elem : map)
        {
            std::ostringstream oconv;

            if (std::is_same<key_type, std::string>())
                oconv << entry_name << "[\"" << elem.first << "\"]";
            else
                oconv << entry_name << "[" << elem.first << "]";

            CheckConstraint(oconv.str(), constraint, location);
        }
    }


    inline const FilesystemNS::File& OptionFile::GetFilename() const noexcept
    {
        return filename_;
    }


    inline const std::vector<std::string>& OptionFile::GetEntryKeyList() const noexcept
    {
        return entry_key_list_;
    }


} // namespace MoReFEM::Wrappers::Lua


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LUA_OPTIONFILE_OPTIONFILE_DOT_HXX_
// *** MoReFEM end header guards *** < //
