// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <limits>
#include <source_location>
#include <sstream>
#include <string>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp" // NOLINT

#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Mpi/Exceptions/Mpi.hpp"


namespace // anonymous
{


    //! Prepare exception message from the error code provided by mpi API.
    std::string InterpretErrorCode(MoReFEM::rank_type rank, int error_code);


} // namespace


namespace MoReFEM::Wrappers::ExceptionNS::Mpi
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    Exception::Exception(rank_type rank, int error_code, const std::source_location location)
    : MoReFEM::Exception(InterpretErrorCode(rank, error_code), location)
    { }


} // namespace MoReFEM::Wrappers::ExceptionNS::Mpi


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string InterpretErrorCode(MoReFEM::rank_type rank, int error_code)
    {
        std::vector<char> error_string(MPI_MAX_ERROR_STRING);
        int length_of_error_string = 0;

        const int bootstrap_error_code = MPI_Error_string(error_code, error_string.data(), &length_of_error_string);

        if (bootstrap_error_code != MPI_SUCCESS)
            throw MoReFEM::Exception("Interpretation of the mpi error code failed!");

        std::ostringstream oconv;
        if (rank != MoReFEM::rank_type{ std::numeric_limits<int>::max() }) // value used when no rank available
            oconv << '[' << rank << "] ";

        oconv << MoReFEM::Utilities::String::ConvertCharArray(error_string);

        return oconv.str();
    }


} // namespace
