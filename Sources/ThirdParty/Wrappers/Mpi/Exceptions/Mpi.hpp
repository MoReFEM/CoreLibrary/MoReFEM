// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_MPI_EXCEPTIONS_MPI_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_MPI_EXCEPTIONS_MPI_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/StrongType.hpp" // IWYU pragma: export


namespace MoReFEM::Wrappers::ExceptionNS::Mpi
{


    //! Generic class
    struct Exception : public MoReFEM::Exception
    {


        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message.
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        /*!
         * \brief Constructor with error code, to use when a mpi function call failed.
         *
         * \attention It should not be relevant at the moment due to the MPI_ERRORS_ARE_FATAL setting in
         * Mpi class constructor. However it is functional should we reverse our position and choose instead
         * MPI_ERRORS_RETURN; in this case return code checking should be added around each mpi function
         * call.
         *
         * \param[in] rank Mpi rank on on which the exception was thrown.
         * \param[in] error_code Error code returned by mpi API. MPI provides a function to make it readable
         * directly.
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(rank_type rank,
                           int error_code,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


} // namespace MoReFEM::Wrappers::ExceptionNS::Mpi


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_MPI_EXCEPTIONS_MPI_DOT_HPP_
// *** MoReFEM end header guards *** < //
