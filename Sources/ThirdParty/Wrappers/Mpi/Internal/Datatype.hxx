// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_MPI_INTERNAL_DATATYPE_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_MPI_INTERNAL_DATATYPE_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Mpi/Internal/Datatype.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Warnings/Pragma.hpp" // IWYU pragma: keep


namespace MoReFEM::Internal::Wrappers::MpiNS
{


    PRAGMA_DIAGNOSTIC(push)
    PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")


    template<>
    struct Datatype<unsigned long>
    {
        static auto Type()
        {
            return MPI_UNSIGNED_LONG;
        }
    };


    template<>
    struct Datatype<char>
    {
        static auto Type()
        {
            return MPI_CHAR;
        }
    };


    template<>
    struct Datatype<short>
    {
        static auto Type()
        {
            return MPI_SHORT;
        }
    };


    template<>
    struct Datatype<int>
    {
        static auto Type()
        {
            return MPI_INT;
        }
    };


    template<>
    struct Datatype<long>
    {
        static auto Type()
        {
            return MPI_LONG;
        }
    };


    template<>
    struct Datatype<signed char>
    {
        static auto Type()
        {
            return MPI_SIGNED_CHAR;
        }
    };


    template<>
    struct Datatype<unsigned char>
    {
        static auto Type()
        {
            return MPI_UNSIGNED_CHAR;
        }
    };


    template<>
    struct Datatype<unsigned short>
    {
        static auto Type()
        {
            return MPI_UNSIGNED_SHORT;
        }
    };


    template<>
    struct Datatype<unsigned int>
    {
        static auto Type()
        {
            return MPI_UNSIGNED;
        }
    };


    template<>
    struct Datatype<float>
    {
        static auto Type()
        {
            return MPI_FLOAT;
        }
    };

    template<>
    struct Datatype<double>
    {
        static auto Type()
        {
            return MPI_DOUBLE;
        }
    };


    template<>
    struct Datatype<long double>
    {
        static auto Type()
        {
            return MPI_LONG_DOUBLE;
        }
    };


    template<>
    struct Datatype<long long>
    {
        static auto Type()
        {
            return MPI_LONG_LONG;
        }
    };


    template<>
    struct Datatype<unsigned long long>
    {
        static auto Type()
        {
            return MPI_UNSIGNED_LONG_LONG;
        }
    };


    template<class T, class ParameterT, template<class> class... Skills>
    struct Datatype<StrongType<T, ParameterT, Skills...>>
    {
        using type = StrongType<T, ParameterT, Skills...>;

        using check_is_hashable = typename std::enable_if<type::use_in_mpi_datatype, void>::type;

        static auto Type()
        {
            return Datatype<T>::Type();
        }
    };


    PRAGMA_DIAGNOSTIC(pop)


} // namespace MoReFEM::Internal::Wrappers::MpiNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_MPI_INTERNAL_DATATYPE_DOT_HXX_
// *** MoReFEM end header guards *** < //
