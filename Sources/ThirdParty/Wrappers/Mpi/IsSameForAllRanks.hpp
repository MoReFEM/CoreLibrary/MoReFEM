// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_MPI_ISSAMEFORALLRANKS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_MPI_ISSAMEFORALLRANKS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <optional>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Check whether a container is the same on all ranks or not.
     *
     * \copydoc doxygen_hide_mpi_param
     * \param[in] container Container which is under scrutiny.
     * \param[in] epsilon Optional that must be activated ONLY IF Container::value_type is a floating point.
     *
     * \return True if the container is the same for all ranks.
     *
     * \internal Considering the use case I have, this method is implemented to be simple, not the fastest possible. I
     * have no doubt there are more clever and efficient way to implement such a functionality, but it would be overkill
     * in my case.
     *
     * \tparam ContainerT A container with either integer or floating point values.
     */
    template<class ContainerT>
    bool IsSameForAllRanks(const Wrappers::Mpi& mpi,
                           const ContainerT& container,
                           std::optional<typename ContainerT::value_type> epsilon = std::nullopt);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ThirdParty/Wrappers/Mpi/IsSameForAllRanks.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_MPI_ISSAMEFORALLRANKS_DOT_HPP_
// *** MoReFEM end header guards *** < //
