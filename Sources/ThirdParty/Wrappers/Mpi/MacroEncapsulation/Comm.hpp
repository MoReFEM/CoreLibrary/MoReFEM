// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_MPI_MACROENCAPSULATION_COMM_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_MPI_MACROENCAPSULATION_COMM_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"


namespace MoReFEM::Wrappers::MpiNS
{


    /*!
     * \brief Enum that encapsulates MPI_Comm.
     *
     * The reason for this is that MPI_Comm are often macros that trigger the Wold-style-cast warning;
     * the level of indirection allows to neutralize it.
     *
     * \internal <b><tt>[internal]</tt></b> This enum is populated only for the operation I needed at some
     * point; some should be added as soon as they are required.
     * \endinternal
     */
    enum class Comm { World, Self };


    /*!
     * \brief Function used to choose the Mpi communicator from the MoReFEM defined enum.
     *
     * \param[in] communicator Communicator enum value defined in MoReFEM.
     *
     * \return MPI_Comm The Openmpi communicator object.
     */
    MPI_Comm Communicator(Comm communicator);


} // namespace MoReFEM::Wrappers::MpiNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_MPI_MACROENCAPSULATION_COMM_DOT_HPP_
// *** MoReFEM end header guards *** < //
