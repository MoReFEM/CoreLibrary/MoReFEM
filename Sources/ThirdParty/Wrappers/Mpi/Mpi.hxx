// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_MPI_MPI_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_MPI_MPI_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
// *** MoReFEM header guards *** < //


#include <bitset>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <limits>
#include <numeric>
#include <optional>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/Wrappers/Mpi/Internal/Datatype.hpp"


namespace MoReFEM::Wrappers
{


    inline constexpr int Mpi::AnyTag()
    {
        return 0;
    }


    template<typename T>
    void Mpi::Gather(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
    {
        static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

        GatherImpl(sent_data, gathered_data);
    }


    template<typename T>
    void Mpi::Gatherv(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
    {
        static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

        GathervImpl(sent_data, gathered_data);
    }


    template<typename T>
    void Mpi::AllGather(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
    {
        static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

        AllGatherImpl(sent_data, gathered_data);
    }


    template<typename T>
    void Mpi::AllGatherv(const std::vector<T>& sent_data, std::vector<T>& gathered_data) const
    {
        static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

        AllGathervImpl(sent_data, gathered_data);
    }


    template<typename T>
    std::vector<T> Mpi::AllReduce(const std::vector<T>& sent_data, MpiNS::Op mpi_operation) const
    {
        static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

        std::vector<T> ret(sent_data.size());
        AllReduce(sent_data, ret, mpi_operation);

        return ret;
    }


    template<typename T>
    T Mpi::AllReduce(T sent_data, MpiNS::Op mpi_operation) const
    {
        std::vector<T> buf{ sent_data };
        std::vector<T> gathered_data = AllReduce(buf, mpi_operation);

        assert(gathered_data.size() == 1);
        return gathered_data[0];
    }


    template<typename T>
    std::vector<T> Mpi::ReduceOnRootProcessor(const std::vector<T>& sent_data, MpiNS::Op mpi_operation) const
    {
        static_assert(!std::is_same<bool, T>::value, "Specialization method should have been called!");

        std::vector<T> ret;
        ReduceImpl(sent_data, ret, GetRootProcessor(), mpi_operation);

        return ret;
    }


    template<typename T>
    T Mpi::ReduceOnRootProcessor(T sent_data, MpiNS::Op mpi_operation) const
    {
        std::vector<T> buf{ sent_data };
        std::vector<T> gathered_data = ReduceOnRootProcessor(buf, mpi_operation);

        assert(gathered_data.size() == 1);
        return gathered_data[0];
    }


    inline auto Mpi::GetRank() const -> rank_type
    {

#ifndef NDEBUG
        {
            int mpi_rank;
            int error_code = MPI_Comm_rank(GetCommunicator(), &mpi_rank);
            AbortIfErrorCode(rank_type{ std::numeric_limits<std::size_t>::max() }, error_code);
            // < Rank was not properly read so put a value which is egregiously high!
            assert(mpi_rank == static_cast<int>(rank_.Get()));
        }
#endif // NDEBUG

        return rank_;
    }


    template<typename IntT>
    inline IntT Mpi::GetRank() const
    {
        static_assert(std::is_integral<IntT>::value, "Return type should be integral!");
        return static_cast<IntT>(GetRank().Get());
    }


    inline MPI_Comm Mpi::GetCommunicator() const
    {
        return comm_;
    }


    inline bool Mpi::IsRootProcessor() const
    {
        return GetRank() == GetRootProcessor();
    }


    inline bool Mpi::IsSequential() const
    {
        return Nprocessor() == rank_type{ 1UL };
    }


    inline auto Mpi::Nprocessor() const -> rank_type
    {
#ifndef NDEBUG
        {
            int mpi_Nprocessor;
            int error_code = MPI_Comm_size(comm_, &mpi_Nprocessor);
            AbortIfErrorCode(GetRank(), error_code);
            assert(mpi_Nprocessor == static_cast<int>(Nprocessor_.Get()));
        }
#endif // NDEBUG

        return Nprocessor_;
    }


    template<typename IntT>
    inline IntT Mpi::Nprocessor() const
    {
        return static_cast<IntT>(Nprocessor().Get());
    }


    template<class ContainerT>
    void Mpi::GatherImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
    {
        int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

        if (IsRootProcessor())
            gathered_data.resize(Nprocessor<std::size_t>() * sent_data.size());

        using value_type = typename ContainerT::value_type;
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code = MPI_Gather(sent_data.data(),
                                    Ndata_sent_by_each_proc,
                                    mpi_datatype,
                                    gathered_data.data(),
                                    Ndata_sent_by_each_proc,
                                    mpi_datatype,
                                    static_cast<int>(GetRootProcessor().Get()),
                                    GetCommunicator());

        AbortIfErrorCode(GetRank(), error_code);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
    }


    template<class ContainerT>
    void Mpi::GathervImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
    {
        int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

        using value_type = typename ContainerT::value_type;
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        // When the suffix on_root_processor is present it means the quantity is only relevant on root processor.
        std::vector<int> gathered_sizes_on_root_processor;
        std::vector<int> disps_on_root_processor;
        std::vector<int> sent_size_on_each_proc = { static_cast<int>(sent_data.size()) };

        Gather(sent_size_on_each_proc, gathered_sizes_on_root_processor);

        if (IsRootProcessor())
        {
            const std::size_t total_size_on_root_processor = std::accumulate(
                gathered_sizes_on_root_processor.cbegin(), gathered_sizes_on_root_processor.cend(), 0UL);

            gathered_data.resize(total_size_on_root_processor);

            const auto Nproc_on_root_processor = gathered_sizes_on_root_processor.size();
            disps_on_root_processor.resize(Nproc_on_root_processor, 0);

            for (auto i = 1UL; i < Nproc_on_root_processor; ++i)
                disps_on_root_processor[i] =
                    disps_on_root_processor[i - 1UL] + gathered_sizes_on_root_processor[i - 1UL];
        }

        int error_code = MPI_Gatherv(sent_data.data(),
                                     Ndata_sent_by_each_proc,
                                     mpi_datatype,
                                     gathered_data.data(),
                                     gathered_sizes_on_root_processor.data(),
                                     disps_on_root_processor.data(),
                                     mpi_datatype,
                                     static_cast<int>(GetRootProcessor().Get()),
                                     GetCommunicator());

        // gathered_data is relevant only on root processor.

        AbortIfErrorCode(GetRank(), error_code);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
    }


    template<class ContainerT>
    void Mpi::AllGatherImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
    {
        const int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

        gathered_data.resize(Nprocessor<std::size_t>() * sent_data.size());

        using value_type = typename ContainerT::value_type;
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code = MPI_Allgather(sent_data.data(),
                                       Ndata_sent_by_each_proc,
                                       mpi_datatype,
                                       gathered_data.data(),
                                       Ndata_sent_by_each_proc,
                                       mpi_datatype,
                                       GetCommunicator());

        AbortIfErrorCode(GetRank(), error_code);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
    }


    template<class ContainerT>
    void Mpi::AllGathervImpl(const ContainerT& sent_data, ContainerT& gathered_data) const
    {
        const int Ndata_sent_by_each_proc = static_cast<int>(sent_data.size());

        using value_type = typename ContainerT::value_type;
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        std::vector<int> gathered_sizes_on_root_processor;
        std::vector<int> sent_size_on_each_proc = { static_cast<int>(sent_data.size()) };

        Gatherv(sent_size_on_each_proc, gathered_sizes_on_root_processor);

        const auto Nproc = Nprocessor<std::size_t>();
        std::vector<int> disps_on_each_processor(Nproc);
        std::vector<int> gathered_sizes_on_each_processor(Nproc);

        std::size_t total_size_on_root_processor = 0UL;

        if (IsRootProcessor())
        {
            total_size_on_root_processor = std::accumulate(
                gathered_sizes_on_root_processor.cbegin(), gathered_sizes_on_root_processor.cend(), 0UL);

            const auto gathered_sizes_on_root_processor_size = gathered_sizes_on_root_processor.size();

            disps_on_each_processor[0UL] = 0;
            for (auto i = 1UL; i < gathered_sizes_on_root_processor_size; ++i)
                disps_on_each_processor[i] =
                    disps_on_each_processor[i - 1UL] + gathered_sizes_on_root_processor[i - 1UL];

            gathered_sizes_on_each_processor = gathered_sizes_on_root_processor;
        }

        std::vector<std::size_t> total_size_on_each_processor = { total_size_on_root_processor };

        Barrier();

        Broadcast(total_size_on_each_processor);
        Broadcast(gathered_sizes_on_each_processor);
        Broadcast(disps_on_each_processor);

        Barrier();

        assert(total_size_on_each_processor.size() == 1UL);

        gathered_data.resize(total_size_on_each_processor[0UL]);

        int error_code = MPI_Allgatherv(sent_data.data(),
                                        Ndata_sent_by_each_proc,
                                        mpi_datatype,
                                        gathered_data.data(),
                                        gathered_sizes_on_each_processor.data(),
                                        disps_on_each_processor.data(),
                                        mpi_datatype,
                                        GetCommunicator());

        AbortIfErrorCode(GetRank(), error_code);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
    }


    template<class ContainerT>
    void Mpi::AllReduce(const ContainerT& sent_data, ContainerT& gathered_data, MpiNS::Op mpi_operation) const
    {
        const auto size = sent_data.size();
        assert(gathered_data.size() == size);

        using value_type = typename ContainerT::value_type;
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();


#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code = MPI_Allreduce(sent_data.data(),
                                       gathered_data.data(),
                                       static_cast<int>(size),
                                       mpi_datatype,
                                       MpiNS::Operator(mpi_operation),
                                       GetCommunicator());
        AbortIfErrorCode(GetRank(), error_code);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
    }


    template<class ContainerT>
    void Mpi::ReduceImpl(const ContainerT& sent_data,
                         ContainerT& gathered_data,
                         rank_type target_processor,
                         MpiNS::Op mpi_operation) const
    {
        gathered_data.resize(sent_data.size());

        using value_type = typename ContainerT::value_type;
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();


#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code = MPI_Reduce(sent_data.data(),
                                    gathered_data.data(),
                                    static_cast<int>(gathered_data.size()),
                                    mpi_datatype,
                                    MpiNS::Operator(mpi_operation),
                                    static_cast<int>(target_processor.Get()),
                                    GetCommunicator());

        AbortIfErrorCode(GetRank(), error_code);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_
    }


    template<std::size_t N>
    std::bitset<N> Mpi::ReduceOnRootProcessor(const std::bitset<N>& sent_data, MpiNS::Op mpi_operation) const
    {
        std::vector<short int> buf(N);

        for (std::size_t i = 0UL; i < N; ++i)
            buf[i] = (sent_data[i] ? 1 : 0);

        auto&& result = ReduceOnRootProcessor(buf, mpi_operation);

        std::bitset<N> ret;

        for (std::size_t i = 0UL; i < N; ++i)
            ret[i] = (result[i] > 0 ? 1 : 0);

        return ret;
    }


    template<typename T>
    std::vector<T> Mpi::CollectFromEachProcessor(T sent_data) const
    {
        std::vector<T> buf(Nprocessor<std::size_t>(), 0);

        buf[GetRank<std::size_t>()] = sent_data;

        return AllReduce(buf, Wrappers::MpiNS::Op::Sum);
    }


    inline void Mpi::Barrier() const
    {
        int error_code = MPI_Barrier(GetCommunicator());
        AbortIfErrorCode(GetRank(), error_code);
    }


    inline auto Mpi::GetRootProcessor() const -> rank_type
    {
        return root_processor_;
    }


    template<typename T>
    void Mpi::Broadcast(std::vector<T>& data, std::optional<rank_type> send_processor) const
    {
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code = MPI_Bcast(
            data.data(),
            static_cast<int>(data.size()),
            mpi_datatype,
            static_cast<int>(send_processor.has_value() ? send_processor.value().Get() : GetRootProcessor().Get()),
            GetCommunicator());

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

        AbortIfErrorCode(GetRank(), error_code);
    }


    template<typename T>
    void Mpi::Broadcast(T& data, std::optional<rank_type> send_processor) const
    {
        std::vector<T> data_as_vector{ data };
        Broadcast(data_as_vector, send_processor);
        data = data_as_vector.back();
    }


    template<typename T>
    void Mpi::Send(rank_type destination, T data) const
    {
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code =
            MPI_Send(&data, 1, mpi_datatype, static_cast<int>(destination.Get()), AnyTag(), GetCommunicator());

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

        AbortIfErrorCode(GetRank(), error_code);
    }


    template<class ContainerT>
    void Mpi::SendContainer(rank_type destination, const ContainerT& data) const
    {
        // assert(!data.empty());

        using value_type = typename ContainerT::value_type;
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<value_type>::Type();

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code = MPI_Send(data.data(),
                                  static_cast<int>(data.size()),
                                  mpi_datatype,
                                  static_cast<int>(destination.Get()),
                                  AnyTag(),
                                  GetCommunicator());

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

        AbortIfErrorCode(GetRank(), error_code);
    }

    template<typename T>
    std::vector<T> Mpi::Receive(rank_type sender_rank, std::size_t max_length) const
    {
        assert(max_length > 0UL);

        std::vector<T> ret(max_length);
        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

        MPI_Status status;

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_

        int error_code = MPI_Recv(ret.data(),
                                  static_cast<int>(max_length),
                                  mpi_datatype,
                                  static_cast<int>(sender_rank.Get()),
                                  AnyTag(),
                                  GetCommunicator(),
                                  &status);

#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
        PRAGMA_DIAGNOSTIC(pop)
#endif // __clang_

        AbortIfErrorCode(GetRank(), error_code);

        int count;

        error_code = MPI_Get_count(&status, mpi_datatype, &count);

        AbortIfErrorCode(GetRank(), error_code);

        ret.resize(static_cast<std::size_t>(count));

        return ret;
    }


    template<typename T>
    T Mpi::Receive(rank_type sender_rank) const
    {
        T ret;

        const auto& mpi_datatype = Internal::Wrappers::MpiNS::Datatype<T>::Type();

        PRAGMA_DIAGNOSTIC(push)
#if defined(__clang__) && !defined(SONARQUBE_4_MOREFEM)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
#endif // __clang_
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

        int error_code = MPI_Recv(&ret,
                                  1,
                                  mpi_datatype,
                                  static_cast<int>(sender_rank.Get()),
                                  AnyTag(),
                                  GetCommunicator(),
                                  nullptr); // MPI_STATUS_IGNORE is a 0 macro and triggers a warning!

        PRAGMA_DIAGNOSTIC(pop)

        AbortIfErrorCode(GetRank(), error_code);

        return ret;
    }


} // namespace MoReFEM::Wrappers


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_MPI_MPI_DOT_HXX_
// *** MoReFEM end header guards *** < //
