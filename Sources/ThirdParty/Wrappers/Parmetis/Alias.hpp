// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_ALIAS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_ALIAS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Scotch/Scotch.hpp" // IWYU pragma: export


namespace MoReFEM
{


    //! Alias to the type used in Parmetis for integers.
    using parmetis_int = idx_t;

    //! Alias to the type used in Parmetis for reals.
    using parmetis_real = real_t;

    static_assert(std::is_same<parmetis_real, double>(),
                  "Check Scotch or Metis was properly installed with 'double' setting for its floating point type.");


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_ALIAS_DOT_HPP_
// *** MoReFEM end header guards *** < //
