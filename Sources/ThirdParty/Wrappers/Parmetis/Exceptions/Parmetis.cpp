// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>

#include "ThirdParty/Wrappers/Parmetis/Exceptions/Parmetis.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string ErrorCodeMsg(int error_code, const std::string& parmetis_function);


} // namespace


namespace MoReFEM::Wrappers::Parmetis::ExceptionNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    Exception::Exception(int error_code, const std::string& parmetis_function, const std::source_location location)
    : MoReFEM::Exception(ErrorCodeMsg(error_code, parmetis_function), location)
    { }


} // namespace MoReFEM::Wrappers::Parmetis::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions declared at the beginning of the file.
    std::string ErrorCodeMsg(int error_code, const std::string& parmetis_function)
    {
        std::ostringstream oconv;
        oconv << "Parmetis function " << parmetis_function << " returned the error code " << error_code << '.';

        return oconv.str();
    }


} // namespace
