// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_EXCEPTIONS_PARMETIS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_EXCEPTIONS_PARMETIS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::Wrappers::Parmetis::ExceptionNS
{


    //! Generic class
    struct Exception : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location

         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());


        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] error_code Error code returned by Parmetis.
         * \param[in] parmetis_function Name of the Parmetis function that returned the error code.
         * \copydoc doxygen_hide_source_location

         */
        explicit Exception(int error_code,
                           const std::string& parmetis_function,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


} // namespace MoReFEM::Wrappers::Parmetis::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_EXCEPTIONS_PARMETIS_DOT_HPP_
// *** MoReFEM end header guards *** < //
