// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_PARMETIS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_PARMETIS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Scotch/Scotch.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"                    // IWYU pragma: export
#include "ThirdParty/Wrappers/Parmetis/Alias.hpp"             // IWYU pragma: export


namespace MoReFEM::Wrappers::Parmetis
{

    /*!
     * \brief Call Parmetis to the partitioning, and returns a vector that is adequate for our need.
     *
     * \param[in] processor_partition This vector has for length the number of processors involved,
     * and gives for each of them the number of nodes affected there. This partition was performed manually;
     * currently it is a simple euclidean division with the remaining affected to the last processor.
     * \param[in] iCSR iCSR vector of the sparse matrix built with \a processor_partition.
     * \param[in] jCSR jCSR vector of the sparse matrix built with \a processor_partition.
     * \copydetails doxygen_hide_mpi_param
     *
     * This function call in fact PartKway() below, but performs on top of it a Mpi-gathering
     * to dispose of the information for all nodes, no matter what
     *
     * \return A vector that gives for each \a NodeBearer the processor in charge. This covers program-wise \a
     * NodeBearer: every \a NodeBearer is returned here, be it on the local processor or not. The NodeBearer are
     * sort according to their internal index.
     */
    std::vector<rank_type> CreateNewPartitioning(const std::vector<std::size_t>& processor_partition,
                                                 const std::vector<parmetis_int>& iCSR,
                                                 const std::vector<parmetis_int>& jCSR,
                                                 const Mpi& mpi);


    /*!
     * \brief Call to ParMETIS_V3_PartKway(), with some argument fixed for the common usage in MoReFEM.
     *
     * \param[in] processor_partition This vector has for length the number of processors involved,
     * and gives for each of them the number of nodes affected there. This partition was performed manually;
     * currently it is a simple euclidean division with the reste affected to the last processor.
     * \param[in] iCSR iCSR vector of the sparse matrix built with \a processor_partition.
     * \param[in] jCSR jCSR vector of the sparse matrix built with \a processor_partition.
     * \copydetails doxygen_hide_mpi_param
     *
     * \attention Parmetis expects a CSR pattern WITHOUT self-connection terms. Not respecting that might
     * create havoc and unclear Petsc error message when applied with an important mesh and P2 finite elements.
     *
     * Note: \a iCSR and \a jCSR are not const due to prototype of C function, but they act as such:
     * they aren't modified by the call to PartKway.
     *
     * \return For each dof that was on the current processor the vector gives the processor now in charge
     * of the dof.
     * For instance, of processor 2 is in charge initially of dof between 2000 and 2999 (global numbering),
     * the vector will contain 1000 values that indicates the rank of the processor now in charge. If for
     * instance the first value of the vector is 1, it means processor 1 is in charge of previous first local
     * dof (labelled 2000 in the global numbering).
     */
    std::vector<parmetis_int> PartKway(const std::vector<std::size_t>& processor_partition,
                                       const std::vector<parmetis_int>& iCSR,
                                       const std::vector<parmetis_int>& jCSR,
                                       const Mpi& mpi);

} // namespace MoReFEM::Wrappers::Parmetis


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PARMETIS_PARMETIS_DOT_HPP_
// *** MoReFEM end header guards *** < //
