// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ADVANCED_ENUM_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ADVANCED_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Advanced::Wrappers::Petsc
{

    //! Whether a message is printed in standard output when the vector is deleted.
    enum class print_linalg_destruction { yes, no };


    //! Enum class to tell whether \a VecDestroy or \a MatDestroy must be called in destructor or not.
    //! Should be true most of the time, but there are edge cases in which 'no' is the desired outcome.
    enum class call_petsc_destroy { yes, no };

} // namespace MoReFEM::Advanced::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ADVANCED_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
