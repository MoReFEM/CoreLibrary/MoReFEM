// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"


namespace MoReFEM::Wrappers::Petsc::AssertionNS
{

#ifndef NDEBUG

    ShouldNotHaveBeenInitialized::~ShouldNotHaveBeenInitialized() = default;

    ShouldNotHaveBeenInitialized::ShouldNotHaveBeenInitialized(const std::source_location location)
    : ::MoReFEM::Advanced::Assertion("The internal PETSc object was expected not to be initialized "
                                     "at this stage.",
                                     location)
    { }


    ShouldHaveBeenInitialized::~ShouldHaveBeenInitialized() = default;

    ShouldHaveBeenInitialized::ShouldHaveBeenInitialized(const std::source_location location)
    : ::MoReFEM::Advanced::Assertion("The internal PETSc object was expected to be properly initialized "
                                     "at this stage.",
                                     location)
    { }


#endif // NDEBUG

} // namespace MoReFEM::Wrappers::Petsc::AssertionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
