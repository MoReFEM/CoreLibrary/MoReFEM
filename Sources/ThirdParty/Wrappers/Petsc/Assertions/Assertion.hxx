// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ASSERTIONS_ASSERTION_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ASSERTIONS_ASSERTION_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/Exceptions/Exception.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"

namespace MoReFEM::Wrappers::Petsc::AssertionNS
{


#ifndef NDEBUG
    template<class PetscInternalT>
    void InternalNotNull(PetscInternalT petsc_internal, const std::source_location location)
    {
        if (petsc_internal == MOREFEM_PETSC_NULL)
            throw ShouldHaveBeenInitialized(location);
    }


    template<class PetscInternalT>
    void InternalNull(PetscInternalT petsc_internal, const std::source_location location)
    {
        if (petsc_internal != MOREFEM_PETSC_NULL)
            throw ShouldNotHaveBeenInitialized(location);
    }


#else  // NDEBUG
    template<class PetscInternalT>
    void InternalNotNull(PetscInternalT, const std::source_location)
    { }


    template<class PetscInternalT>
    void InternalNull(PetscInternalT, const std::source_location)
    { }
#endif // NDEBUG


} // namespace MoReFEM::Wrappers::Petsc::AssertionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ASSERTIONS_ASSERTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
