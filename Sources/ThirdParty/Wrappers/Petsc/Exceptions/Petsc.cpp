// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"

#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string ErrorCodeMsg(int error_code, std::string_view petsc_function);

    std::string WrongMatlabExtensionMsg(const MoReFEM::FilesystemNS::File& filename);

    std::string SolverNotParallelMsg(const std::string& solver_name);

    std::string SolverNotSetUpMsg(const MoReFEM::Wrappers::Petsc::solver_name_type& solver_name);

} // namespace


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    Exception::Exception(int error_code, std::string&& petsc_function, const std::source_location location)
    : MoReFEM::Exception(ErrorCodeMsg(error_code, std::move(petsc_function)), location)
    { }


    WrongMatlabExtension::~WrongMatlabExtension() = default;

    WrongMatlabExtension::WrongMatlabExtension(const MoReFEM::FilesystemNS::File& filename,
                                               const std::source_location location)
    : Exception(WrongMatlabExtensionMsg(filename), location)
    { }


    SolverNotParallel::~SolverNotParallel() = default;


    SolverNotParallel::SolverNotParallel(const std::string& solver_name, const std::source_location location)
    : Exception(SolverNotParallelMsg(solver_name), location)
    { }


    SolverNotSetUp::~SolverNotSetUp() = default;


    SolverNotSetUp::SolverNotSetUp(const ::MoReFEM::Wrappers::Petsc::solver_name_type& solver_name,
                                   const std::source_location location)
    : Exception(SolverNotSetUpMsg(solver_name), location)
    { }


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file
    std::string ErrorCodeMsg(int error_code, std::string_view petsc_function)
    {
        std::ostringstream oconv;
        oconv << "Petsc (or Slepc) function " << petsc_function << " returned the error code " << error_code << '.';

        return oconv.str();
    }


    std::string WrongMatlabExtensionMsg(const MoReFEM::FilesystemNS::File& filename)
    {
        std::ostringstream oconv;
        oconv << "Invalid name for Matlab output (\"" << filename << "\"): a '.m'extension was expected.";
        return oconv.str();
    }


    std::string SolverNotParallelMsg(const std::string& solver_name)
    {
        std::ostringstream oconv;
        oconv << "Solver " << solver_name << " only tackles sequential solve!";
        return oconv.str();
    }


    std::string SolverNotSetUpMsg(const MoReFEM::Wrappers::Petsc::solver_name_type& solver_name)
    {
        std::ostringstream oconv;
        oconv << "Solver " << solver_name
              << " was found in the solver factory but it was not set up in MoReFEM configuration file.";
        return oconv.str();
    }


} // namespace
