// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_EXCEPTIONS_PETSC_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_EXCEPTIONS_PETSC_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    //! Generic class
    struct Exception : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());


        /*!
         * \brief Constructor with simple message
         *
         * \param[in] error_code Error code returned by Petsc.
         * \param[in] petsc_function Name of the Petsc function that returned the error code.
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(int error_code,
                           std::string&& petsc_function,
                           const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! When a matlab output file doesn't end with '.m'
    struct WrongMatlabExtension final : public Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] filename Name in which data was expected in MATLAB format.
         * \copydoc doxygen_hide_source_location
         */
        explicit WrongMatlabExtension(const MoReFEM::FilesystemNS::File& filename,
                                      const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~WrongMatlabExtension() override;

        //! \copydoc doxygen_hide_copy_constructor
        WrongMatlabExtension(const WrongMatlabExtension& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        WrongMatlabExtension(WrongMatlabExtension&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        WrongMatlabExtension& operator=(const WrongMatlabExtension& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        WrongMatlabExtension& operator=(WrongMatlabExtension&& rhs) = default;
    };


    //! When a non parallel is attempted in parallel
    struct SolverNotParallel final : public Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] solver_name Name of the solver which doesn't support parallel linear algebra.
         * \copydoc doxygen_hide_source_location
         */
        explicit SolverNotParallel(const std::string& solver_name,
                                   const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~SolverNotParallel() override;

        //! \copydoc doxygen_hide_copy_constructor
        SolverNotParallel(const SolverNotParallel& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        SolverNotParallel(SolverNotParallel&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        SolverNotParallel& operator=(const SolverNotParallel& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        SolverNotParallel& operator=(SolverNotParallel&& rhs) = default;
    };


    //! When a solver was not activated within MoReFEM
    struct SolverNotSetUp final : public Exception
    {
        /*!
         * \brief Constructor
         *
         * \param[in] solver_name Name of the solver that was requested.
         * \copydoc doxygen_hide_source_location
         */
        explicit SolverNotSetUp(const ::MoReFEM::Wrappers::Petsc::solver_name_type& solver_name,
                                const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~SolverNotSetUp() override;

        //! \copydoc doxygen_hide_copy_constructor
        SolverNotSetUp(const SolverNotSetUp& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SolverNotSetUp(SolverNotSetUp&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SolverNotSetUp& operator=(const SolverNotSetUp& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SolverNotSetUp& operator=(SolverNotSetUp&& rhs) = delete;
    };


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_EXCEPTIONS_PETSC_DOT_HPP_
// *** MoReFEM end header guards *** < //
