// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <memory> // IWYU pragma: keep
#include <string>

#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM::Internal::PetscNS
{


    RAII::RAII(int argc, char** argv)
    {
        ::MoReFEM::Wrappers::Mpi::InitEnvironment(argc, argv);

        mpi_ = std::make_unique<::MoReFEM::Wrappers::Mpi>(rank_type{ 0UL }, ::MoReFEM::Wrappers::MpiNS::Comm::World);

        // PetscOptionsSetValue(nullptr, "-snes_linesearch_type", "basic");

        const int error_code = PetscInitialize(MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, "");

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "PetscInitialize");
    }


    RAII::~RAII()
    {
        [[maybe_unused]] const int error_code = PetscFinalize();
        assert(!error_code && "Error in PetscFinalize call!"); // No exception in destructors...
    }


    const std::string& RAII::ClassName()
    {
        static const std::string ret("Internal::PetscNS::RAII");
        return ret;
    }


} // namespace MoReFEM::Internal::PetscNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
