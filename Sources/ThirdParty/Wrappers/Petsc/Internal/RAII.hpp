// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_INTERNAL_RAII_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_INTERNAL_RAII_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::PetscNS
{


    /*!
     * \brief RAII class to initialize / close properly PETSc and mpi.
     */
    class RAII final : public Utilities::Singleton<RAII>
    {

      private:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] argc The first argument from main() function.
         * \param[in] argv The second argument from main() function.
         */
        explicit RAII(int argc, char** argv);

        //! Destructor.
        virtual ~RAII() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<RAII>;

        //! Name of the class.
        static const std::string& ClassName();

        ///@}

      public:
        //! Accessor to mpi.
        const ::MoReFEM::Wrappers::Mpi& GetMpi() const noexcept;

      private:
        //! Holds Mpi object.
        ::MoReFEM::Wrappers::Mpi::const_unique_ptr mpi_ = nullptr;
    };


} // namespace MoReFEM::Internal::PetscNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_INTERNAL_RAII_DOT_HPP_
// *** MoReFEM end header guards *** < //
