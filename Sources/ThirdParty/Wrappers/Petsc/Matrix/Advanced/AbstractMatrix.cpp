// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <source_location>
#include <string>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Matrix/Advanced/AbstractMatrix.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMatPrivate.hpp" // IWYU pragma: export
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"


namespace MoReFEM::Advanced::Wrappers::Petsc
{


    void AbstractMatrix::Unused() const
    {
        assert(false
               && "This method exists solely to provide an out-of-line virtual method definition and is not intended "
                  "to be ever used!");
        exit(EXIT_FAILURE);
    }


    AbstractMatrix::AbstractMatrix(std::string&& name,
                                   Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction)
    : petsc_matrix_{ MOREFEM_PETSC_NULL }, do_call_petsc_destroy_{ call_petsc_destroy::no }, name_{ name },
      do_print_linalg_destruction_{ do_print_linalg_destruction }
    { }


    AbstractMatrix::AbstractMatrix(const AbstractMatrix& rhs, std::string&& name)
    : petsc_matrix_{ MOREFEM_PETSC_NULL }, do_call_petsc_destroy_{ rhs.do_call_petsc_destroy_ }, name_{ name },
      do_print_linalg_destruction_{ rhs.do_print_linalg_destruction_ }
    { }


    AbstractMatrix::AbstractMatrix(AbstractMatrix&& rhs) noexcept
    : petsc_matrix_(rhs.petsc_matrix_), do_call_petsc_destroy_(rhs.do_call_petsc_destroy_),
      name_{ std::move(rhs.name_) }, do_print_linalg_destruction_{ rhs.do_print_linalg_destruction_ }
    {
        rhs.do_call_petsc_destroy_ = call_petsc_destroy::no; // important line!
        // We no longer invalidate all rhs data here, as changing rhs.name_ leads to clang tidy warning
        // related to exceptions; it is expected that clang-tidy with `bugprone-use-after-move` is
        // run regularly (it is in CI).
    }


    AbstractMatrix::~AbstractMatrix()
    {
        switch (do_call_petsc_destroy_)
        {
        case call_petsc_destroy::no:
            break;
        case call_petsc_destroy::yes:
        {
            assert(petsc_matrix_ != MOREFEM_PETSC_NULL);

            if (do_print_linalg_destruction_ == Advanced::Wrappers::Petsc::print_linalg_destruction::yes)
            {
                try
                {
                    std::cout << "=== Destroy matrix '" << name_ << "' (address was " << petsc_matrix_ << ")." << '\n';
                }
                catch (...) // can't throw in destructor!
                {
                    assert(false && "Shouldn't happen...");
                }
            }

            [[maybe_unused]] const int error_code = MatDestroy(&petsc_matrix_);
            assert(!error_code && "Error in Mat destruction."); // no exception in destructors!
        }
        }
    }


    void Swap(AbstractMatrix& A, AbstractMatrix& B)
    {
        using std::swap;
        swap(A.do_call_petsc_destroy_, B.do_call_petsc_destroy_);
        swap(A.petsc_matrix_, B.petsc_matrix_);
        swap(A.name_, B.name_);
        swap(A.do_print_linalg_destruction_, B.do_print_linalg_destruction_);
    }


    void AbstractMatrix::ZeroEntries(const std::source_location location)
    {
        const int error_code = MatZeroEntries(Internal(location));
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatZeroEntries", location);
    }


    void AbstractMatrix::ZeroRows(const std::vector<PetscInt>& row_indexes,
                                  PetscScalar diagonal_value,
                                  const std::source_location location)
    {
        const int error_code = MatZeroRows(Internal(location),
                                           static_cast<PetscInt>(row_indexes.size()),
                                           row_indexes.data(),
                                           diagonal_value,
                                           MOREFEM_PETSC_NULL,
                                           MOREFEM_PETSC_NULL);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatZeroRows", location);
    }


    void AbstractMatrix::ZeroRowsColumns(const std::vector<PetscInt>& row_indexes,
                                         PetscScalar diagonal_value,
                                         const std::source_location location)
    {
        const int error_code = MatZeroRowsColumns(Internal(location),
                                                  static_cast<PetscInt>(row_indexes.size()),
                                                  row_indexes.data(),
                                                  diagonal_value,
                                                  MOREFEM_PETSC_NULL,
                                                  MOREFEM_PETSC_NULL);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatZeroRowsColumns", location);
    }


    PetscScalar AbstractMatrix::GetValue(row_program_wise_index_type row_index,
                                         col_program_wise_index_type column_index,
                                         const std::source_location location) const
    {
        // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
        PetscScalar ret;

        const PetscInt row = row_index.Get();
        const PetscInt col = column_index.Get();

        const int error_code = MatGetValues(InternalForReadOnly(location), 1, &row, 1, &col, &ret);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetValues", location);

        return ret;
    }


    void AbstractMatrix::Scale(PetscScalar a, const std::source_location location)
    {
        const int error_code = MatScale(Internal(location), a);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatScale", location);
    }


    auto AbstractMatrix::GetProcessorWiseSize(const std::source_location location) const
        -> std::pair<row_processor_wise_index_type, col_processor_wise_index_type>
    {
        // NOLINTBEGIN(cppcoreguidelines-init-variables)
        PetscInt Nrow;
        PetscInt Ncol;
        // NOLINTEND(cppcoreguidelines-init-variables)

        const int error_code = MatGetLocalSize(InternalForReadOnly(location), &Nrow, &Ncol);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetLocalSize", location);

        return { row_processor_wise_index_type{ Nrow }, col_processor_wise_index_type{ Ncol } };
    }


    auto AbstractMatrix::GetProgramWiseSize(const std::source_location location) const
        -> std::pair<row_program_wise_index_type, col_program_wise_index_type>
    {
        // NOLINTBEGIN(cppcoreguidelines-init-variables)
        PetscInt Nrow;
        PetscInt Ncol;
        // NOLINTEND(cppcoreguidelines-init-variables)

        const int error_code = MatGetSize(InternalForReadOnly(location), &Nrow, &Ncol);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetSize", location);

        return { row_program_wise_index_type{ Nrow }, col_program_wise_index_type{ Ncol } };
    }


    void AbstractMatrix::View(const ::MoReFEM::Wrappers::Mpi& mpi, const std::source_location location) const
    {
        const int error_code = MatView(InternalForReadOnly(location), PETSC_VIEWER_STDOUT_(mpi.GetCommunicator()));
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatView", location);
    }


    void AbstractMatrix::View(const ::MoReFEM::Wrappers::Mpi& mpi,
                              const FilesystemNS::File& output_file,
                              const std::source_location location,
                              PetscViewerFormat format,
                              PetscFileMode file_mode) const
    {
        ::MoReFEM::Wrappers::Petsc::Viewer viewer(mpi, output_file, format, file_mode, location);

        const int error_code = MatView(InternalForReadOnly(location), viewer.GetUnderlyingPetscObject());
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatView", location);
    }


    void AbstractMatrix::ViewBinary(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    const FilesystemNS::File& output_file,
                                    const std::source_location location) const
    {
        View(mpi, output_file, location, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE);
    }


    void AbstractMatrix::GetRow(row_program_wise_index_type row_index,
                                std::vector<std::pair<PetscInt, PetscScalar>>& row_content,
                                const std::source_location location) const
    {
        std::vector<PetscInt> row_content_position_list;
        std::vector<PetscScalar> row_content_value_list;

        GetRow(row_index, row_content_position_list, row_content_value_list, location);

        const auto size = row_content_position_list.size();
        assert(size == row_content_value_list.size());

        row_content.reserve(size);

        for (auto i = 0UL; i < size; ++i)
            row_content.emplace_back(row_content_position_list[i], row_content_value_list[i]);
    }


    void AbstractMatrix::GetRow(row_program_wise_index_type row_index,
                                std::vector<PetscInt>& row_content_position_list,
                                std::vector<PetscScalar>& row_content_value_list,
                                const std::source_location location) const
    {
        assert(row_content_position_list.empty());
        assert(row_content_value_list.empty());

        PetscInt Nnon_zero_cols = 0;
        const PetscInt* columns = nullptr;
        const PetscScalar* values = nullptr;

        // Petsc MatGetRow() crashed if row index is not in the interval given below, so I have added
        // this very crude test to return nothing in this case.
        if (row_index.Get() < InternalForReadOnly(location)->rmap->rstart
            || row_index.Get() > InternalForReadOnly(location)->rmap->rend)
            return;

        int error_code = MatGetRow(InternalForReadOnly(location), row_index.Get(), &Nnon_zero_cols, &columns, &values);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetRow", location);

        // This condition is there because Petsc might not be very consistent and return for instance:
        // - Nnon_zero_cols = 0
        // - columns = 0x0
        // - values to an address in memory (0x0 would be much better...)
        if (Nnon_zero_cols > 0)
        {
            assert(values != nullptr);
            assert(values != nullptr);

            const auto size = static_cast<std::size_t>(Nnon_zero_cols);
            row_content_position_list.resize(size);
            row_content_value_list.resize(size);

            for (auto i = 0UL; i < size; ++i)
            {
                row_content_position_list[i] = columns[i];
                row_content_value_list[i] = values[i];
            }
        }

        error_code = MatRestoreRow(InternalForReadOnly(location), row_index.Get(), &Nnon_zero_cols, &columns, &values);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatRestoreRow", location);
    }


    void AbstractMatrix::GetColumnVector(PetscInt column_index,
                                         ::MoReFEM::Wrappers::Petsc::Vector& column,
                                         const std::source_location location) const
    {
        const int error_code =
            MatGetColumnVector(InternalForReadOnly(location), column.Internal(location), column_index);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetColumnVector", location);
    }


    double AbstractMatrix::Norm(NormType type, const std::source_location location) const
    {
        // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
        PetscReal norm;

        assert(type == NORM_1 || type == NORM_FROBENIUS || type == NORM_INFINITY);

        const int error_code = MatNorm(InternalForReadOnly(location), type, &norm);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatNorm", location);

        return static_cast<double>(norm);
    }


    void AbstractMatrix::GetInfo(MatInfo* infos, const std::source_location location)
    {
        const int error_code = MatGetInfo(Internal(location), MAT_LOCAL, infos);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetInfo", location);
    }


    void AbstractMatrix::GetOption(MatOption op, PetscBool* flg, const std::source_location location)
    {
        const int error_code = MatGetOption(Internal(location), op, flg);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetOption", location);
    }


    const std::string& AbstractMatrix::GetName() const noexcept
    {
        return name_;
    }


    void AbstractMatrix::SetDoNotDestroyPetscMatrix()
    {
        do_call_petsc_destroy_ = call_petsc_destroy::no;
    }


    void AbstractMatrix::SetDoCallPetscDestroy(call_petsc_destroy value)
    {
        do_call_petsc_destroy_ = value;
    }


    void AbstractMatrix::SetOption(MatOption op, PetscBool flg, const std::source_location location)
    {
        const int error_code = MatSetOption(Internal(location), op, flg);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatSetOption", location);
    }


    void AbstractMatrix::ChangeInternal(const ::MoReFEM::Wrappers::Mpi& mpi, Mat new_petsc_matrix)
    {
        petsc_matrix_ = new_petsc_matrix;
        mpi.Barrier();
    }


    MatType AbstractMatrix::GetType(const std::source_location location) const
    {
        MatType ret = nullptr;
        const int error_code = MatGetType(InternalForReadOnly(location), &ret);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetType", location);

        return ret;
    }


    void AbstractMatrix::SetFromPetscMat(Mat petsc_matrix, const std::source_location location)
    {
        auto& underlying_petsc_object = InternalNoCheck();
        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNotNull(petsc_matrix, location);
        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNull(underlying_petsc_object, location);

        assert(DoCallPetscDestroy() == call_petsc_destroy::no);
        underlying_petsc_object = petsc_matrix;
        SetDoNotDestroyPetscMatrix();
    }


} // namespace MoReFEM::Advanced::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
