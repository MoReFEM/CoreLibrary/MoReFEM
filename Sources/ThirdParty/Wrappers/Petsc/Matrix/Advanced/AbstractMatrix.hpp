// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_ADVANCED_ABSTRACTMATRIX_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_ADVANCED_ABSTRACTMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <iosfwd>
#include <source_location>
#include <vector>

#include "Utilities/LinearAlgebra/StrongType.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp" // IWYU pragma: export
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Petsc/Advanced/Enum.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Wrappers { class Mpi; }
namespace MoReFEM::Wrappers::Petsc { class Vector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::Wrappers::Petsc
{


    /*!
     * \brief An abstract class which provides backbone for both `MoReFEM::Wrappers::Petsc::Matrix` and
     * `MoReFEM::Wrappers::Petsc::MatrixOpResult`.
     *
     * In many PETSc functions, a `Mat` object is given as argument. In some of them, it is fully expected that said
     * object is properly allocated with a very defined pattern, whereas others (those implying matrix matrix product)
     * on the contrary will generate an error if you have already allocated it.
     *
     * For years, MoReFEM used the same wrapper for both: an object called `MoReFEM::Wrappers::Petsc::Matrix`.
     *
     * However it proved to be cumbersome: there was a check in debug mode throwing an `Assertion` when it was
     * initialized whereas it should not or the opposite, but it was a daunting process to run the code, see if one
     * matrix crash, stop allocating it, rince and repeat until everything works in the end.
     *
     * So now we instead define two different entities, one of which that simply can't be allocated at all.
     *
     * This way, we enforce by design the conditions and the end user doesn't have to bother with whether it should be
     * initialized or not - most if not all of `MoReFEM::Wrappers::Petsc::Matrix` should be and
     * `MoReFEM::Wrappers::Petsc::MatrixOpResult` simply can't.
     *
     * Current abstract class is there to define the common functionalities to both.
     */

    class AbstractMatrix
    {


        //! Friendship to Swap function.
        friend void Swap(AbstractMatrix&, AbstractMatrix&);


      protected:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         *
         * \copydetails doxygen_hide_print_linalg_arg
         */
        explicit AbstractMatrix(std::string&& name,
                                Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction);

        //! Destructor.
        virtual ~AbstractMatrix() = 0;

        /*!
         * \copydoc doxygen_hide_copy_constructor
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         *
         * \attention Standard mandates that additional parameters for copy constructor get default value.
         *
         * Nothing is done here except setting the data attributes, most of them from `rhs` (the exception being the
         * `name`.
         *
         * \attention This abstract class does NOT initialize in any way the underlying `Mat` object; this responsability is deferred to the
         * inherited class (if it requires it - for `MatrixOpResult` the choice was simply to forbid the recopy
         * constructor).
         */
        AbstractMatrix(const AbstractMatrix& rhs, std::string&& name = "Default value");

        //! \copydoc doxygen_hide_move_constructor
        AbstractMatrix(AbstractMatrix&& rhs) noexcept;

        //! \copydoc doxygen_hide_copy_affectation
        AbstractMatrix& operator=(const AbstractMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AbstractMatrix& operator=(AbstractMatrix&& rhs) = delete;

        //! Unused method that should never been called to satisfy the condition of at least one out of line virtual
        //! method definition.
        [[noreturn]] virtual void Unused() const;

        ///@}
        ///


      public:
        /*!
         * \brief Init a Matrix from a Petsc Mat object.
         *
         * Ideally it shouldn't be used at all except in the implementation of few of the associated
         * function such as MatMatMult.
         *
         * \param[in] petsc_matrix Native Petsc matrix which should be put inside current object.
         * \copydoc doxygen_hide_source_location
         */
        void SetFromPetscMat(Mat petsc_matrix, const std::source_location location = std::source_location::current());


        /*!
         * \brief  Get the name given to the vector at construction.
         *
         * This is intended as a developer tool for debug, especially when something goes awry with PetscFinalize().
         *
         * \return Name given as construction.
         *
         * It should be noticed that inherited class \a GlobalVector may provide a default name if none was given; no
         * default name however is foreseen in current base class.
         */
        const std::string& GetName() const noexcept;


        /*!
         * \brief Handle over the internal Mat object.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Internal Mat object, which is indeed a pointer in Petsc.
         *
         * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
         * method should be implemented over the function that might need access to the \a Mat internal object.
         */
        Mat Internal(std::source_location location);

        /*!
         * \brief Handle over the internal Mat object - when you can guarantee the call is only to read the
         * value, not act upon it.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Internal Mat object, which is indeed a pointer in Petsc.
         *
         * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
         * method should be implemented over the function that might need access to the \a Mat internal object.
         */
        Mat InternalForReadOnly(std::source_location location) const;

      protected:
        /*!
         * \brief Handle over the internal Mat object, with no check whether it is initialized or not.
         *
         * \return Internal Mat object, which is indeed a pointer in Petsc.
         */
        Mat& InternalNoCheck();

        /*!
         * \brief Same as `InternalNoCheck` with `const` qualifier.
         *
         * \return Internal Mat object, which is indeed a pointer in Petsc.
         */
        const Mat& InternalNoCheckForReadOnly() const;


      public:
        /*!
         * \brief Get the number of elements in the local matrix.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return First index is number of rows, second one the number of columns.
         */
        std::pair<row_processor_wise_index_type, col_processor_wise_index_type>
        GetProcessorWiseSize(const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Get the number of elements in the global matrix (first is number of rows, second number of
         * columns).
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return First index is number of rows, second one the number of columns.
         */
        std::pair<row_program_wise_index_type, col_program_wise_index_type>
        GetProgramWiseSize(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Set all the entries to zero.
         *
         * \copydoc doxygen_hide_source_location
         */
        void ZeroEntries(const std::source_location location = std::source_location::current());


        /*!
         * \brief Zeros all entries (except possibly the main diagonal) of a set of rows of a matrix.
         *
         * \param[in] row_indexes Program-wise indexes of the row considered (C - numbering).
         * \param[in] diagonal_value Value for the diagonal term. Put 0. if you want it zeroed too.
         * \copydoc doxygen_hide_source_location
         *
         * \internal We do not use strong type here for performance reason: the underlying C array is propagated
         * directly to PETSc C API, and we would lose that with a vector of strong types.
         */
        void ZeroRows(const std::vector<PetscInt>& row_indexes,
                      PetscScalar diagonal_value,
                      const std::source_location location = std::source_location::current());


        /*!
         * \brief Zeros all entries (except possibly the main diagonal) of a set of rows and columns of a
         * matrix.
         *
         *
         * \param[in] row_indexes Program-wise indexes of the row considered (C - numbering). All values on this row
         * (except the diagonal value) AND all values on the columns will be zeroed.
         * \param[in] diagonal_value Value for the diagonal term. Put 0. if you want it zeroed too.
         * \copydoc doxygen_hide_source_location
         *
         * \internal We do not use strong type here for performance reason: the underlying C array is propagated
         * directly to PETSc C API, and we would lose that with a vector of strong types.
         */
        void ZeroRowsColumns(const std::vector<PetscInt>& row_indexes,
                             PetscScalar diagonal_value,
                             const std::source_location location = std::source_location::current());


        /*!
         * \brief Wrapper over MatScale, that performs Y = a * Y.
         *
         * \param[in] a Factor by which the vector is scaled.
         * \copydoc doxygen_hide_source_location
         */
        void Scale(PetscScalar a, const std::source_location location = std::source_location::current());

        /*!
         * \brief Wrapper over MatNorm.
         *
         * \param[in] type Type of norm. Available norms are NORM_1, NORM_FROBENIUS and NORM_INFINITY.
         * \copydoc doxygen_hide_source_location
         *
         * \return Norm of the vector of the chose \a type.
         */
        double Norm(NormType type, const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Wrapper over MatGetInfo()
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] infos Matrix information context (see
         * [Petsc
         * documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatGetInfo.html) for
         * more details).
         */
        void GetInfo(MatInfo* infos, const std::source_location location = std::source_location::current());

        /*!
         * \brief Wrapper over MatGetOption()
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] op The
         * [Petsc
         * MatOption](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatOption.html#MatOption)
         * which status is sought.
         * \param[out] flg True if the option is set, false otherwise.
         */
        void
        GetOption(MatOption op, PetscBool* flg, const std::source_location location = std::source_location::current());

        /*!
         * \brief Wrapper over MatSetOption()
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] op The
         * [Petsc
         * MatOption](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatOption.html#MatOption)
         * which status is to be modified.
         * \param[in] flg Whether the option is activated or not.
         */
        void
        SetOption(MatOption op, PetscBool flg, const std::source_location location = std::source_location::current());

        /*!
         * \brief Return the type of the matrix.
         *
         * Thin wrapper over  https://petsc.org/release/docs/manualpages/Mat/MatGetType
         * \copydoc doxygen_hide_source_location
         *
         * \return The \a MatType of the matrix,
         */
        MatType GetType(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Get the value of an element of the matrix.
         *
         * The underlying Petsc function requires that the row belong to the current processor; however for
         * the column it is much more liberal: if there is an attempt to get a value that doesn't belong
         * to the CSR pattern, the function simply returns 0.
         *
         * \param[in] row_index Program-wise index of the row. The row MUST be
         * one of the row handled by the current processor.
         * \param[in] column_index Program-wise index of the column.
         * \copydoc doxygen_hide_source_location
         *
         * \return Value of the chosen element in the matrix.
         */
        PetscScalar GetValue(row_program_wise_index_type row_index,
                             col_program_wise_index_type column_index,
                             const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Get a row for the matrix.
         *
         * This is a wrapper over MatGetRow; it should be noticed that Petsc people hope this function is
         * actually not used...
         *
         * \param[in] row_index Program-wise index of the row. The row MUST be
         * one of the row handled by the current processor.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[out] row_content_position_list Position of the non-zero values in the row.
         * \param[out] row_content_value_list Values of non-zero values in the row. This container is the same
         * size as \a row_content_position
         * \todo At the moment `row_content_position_list` and `row_content_value_list` are allocated each time;
         * it might be useful not to do this... But this means structure has to be kept for each line of the matrix
         * considered.
         */
        void GetRow(row_program_wise_index_type row_index,
                    std::vector<PetscInt>& row_content_position_list,
                    std::vector<PetscScalar>& row_content_value_list,
                    const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Get a row for the matrix.
         *
         * This is a wrapper over MatGetRow; it should be noticed that Petsc people hope this function is
         * actually not used...
         *
         * \param[in] row_index Program-wise index of the row. The row MUST be
         * one of the row handled by the current processor.
         * \copydoc doxygen_hide_source_location
         *
         * \param[out] row_content Key is position of the non-zero values, value the actual value.
         * \todo At the moment row_content is allocated each time; it might be useful not to do this... But this
         * means structure has to be kept for each line of the matrix considered.
         */
        void GetRow(row_program_wise_index_type row_index,
                    std::vector<std::pair<PetscInt, PetscScalar>>& row_content,
                    const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Get the column of the matrix.
         *
         * This is a wrapper over MatGetColumnVector; it should be noticed that Petsc people hope this function
         * is actually not used...
         *
         * \param[in] column_index Program-wise index of the column.
         * \param[out] column Vector containing the column.
         *
         * \copydoc doxygen_hide_source_location
         */
        void GetColumnVector(PetscInt column_index,
                             ::MoReFEM::Wrappers::Petsc::Vector& column,
                             const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Wrapper over MatView.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_source_location
         */
        void View(const ::MoReFEM::Wrappers::Mpi& mpi,
                  const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Wrapper over MatView in the case the viewer is a file.
         *
         * \a MatView write the content of the matrix only on the root processor;  if you need it on several you
         * need to copy the file afterwards (for instance in the case of prepartitioned data).
         *
         * \copydoc doxygen_hide_mpi_param
         *
         * \param[in] format Format in which the matrix is written. See Petsc manual pages to get all the
         * formats available; relevant ones so far are PETSC_VIEWER_DEFAULT and PETSC_VIEWER_ASCII_MATLAB.
         * \param[in] output_file File into which the vector content will be written.
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_petsc_file_mode
         */
        void View(const ::MoReFEM::Wrappers::Mpi& mpi,
                  const FilesystemNS::File& output_file,
                  const std::source_location location = std::source_location::current(),
                  PetscViewerFormat format = PETSC_VIEWER_DEFAULT,
                  PetscFileMode file_mode = FILE_MODE_WRITE) const;


        /*!
         * \brief Wrapper over MatView in the case the viewer is a binary file.
         *
         * \param[in] output_file File into which the vector content will be written. This file will be written only on the root processor; for other
         * ranks nothing is written at all.
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_mpi_param
         */
        void ViewBinary(const ::MoReFEM::Wrappers::Mpi& mpi,
                        const FilesystemNS::File& output_file,
                        const std::source_location location = std::source_location::current()) const;


      protected:
        /*!
         * \brief Change the underlying \a Mat object pointed by the class.
         *
         * \attention This should not be encouraged and is provided only t comply with PETSc non linear solve
         * interface.
         *
         * \param[in] mpi \a Mpi object. It is provided so that \a Barrier() may be called: we do not want the
         * ranks oto be inconsistent... \param[in] new_petsc_matrix The \a Mat object that should now be used
         * internally (for thecurrent mpi rank).
         */
        void ChangeInternal(const ::MoReFEM::Wrappers::Mpi& mpi, Mat new_petsc_matrix);


        /*!
         * \brief Tells the class not to destroy the underlying vector through a call to \a MatDestroy().
         *
         * \attention This method should be avoided most of the time; it is there only for an edge case
         * related to PETSc Snes interface!
         */
        void SetDoNotDestroyPetscMatrix();

        //! Get the value of \a do_call_petsc_destroy_.
        call_petsc_destroy DoCallPetscDestroy() const noexcept;

        /*!
         * \brief Set the value of do_call_petsc_destroy_.
         *
         *  You should rather use \a SetDoNotDestroyPetscMatrix(); this one is just helpful to define
         *  recopy in derived classes.
         *
         *  \param[in] value The value to set.
         */
        void SetDoCallPetscDestroy(call_petsc_destroy value);


      private:
        // =====================================================================================================
        // \attention Do not forget to update Swap() and recopy constructors if a new data member is added!
        // =====================================================================================================

        //! Underlying Petsc matrix.
        Mat petsc_matrix_;

        /*!
         * \brief Whether the underlying Petsc matrix will be destroyed upon destruction of the object.
         *
         * Default behaviour is to do so, but in some cases it is wiser not to.
         */
        call_petsc_destroy do_call_petsc_destroy_{ call_petsc_destroy::yes };

        //! \copydetails doxygen_hide_petsc_matrix_name_text
        std::string name_;

        //! Whether there is a print on standard output to inform of the deletion of the \a Matrix.
        Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction_{
            Advanced::Wrappers::Petsc::print_linalg_destruction::no
        };
    };


    /*!
     * \brief Swap two matrices.
     */
    void Swap(AbstractMatrix&, AbstractMatrix&);


} // namespace MoReFEM::Advanced::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Matrix/Advanced/AbstractMatrix.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_ADVANCED_ABSTRACTMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
