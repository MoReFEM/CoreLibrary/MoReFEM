// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_ADVANCED_ABSTRACTMATRIX_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_ADVANCED_ABSTRACTMATRIX_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/Advanced/AbstractMatrix.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::Wrappers::Petsc
{


    inline Mat AbstractMatrix::Internal(std::source_location location)
    {
        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNotNull(petsc_matrix_, location);
        return petsc_matrix_;
    }


    inline Mat AbstractMatrix::InternalForReadOnly(std::source_location location) const
    {
        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNotNull(petsc_matrix_, location);
        return petsc_matrix_;
    }


    inline Mat& AbstractMatrix::InternalNoCheck()
    {
        return petsc_matrix_;
    }


    inline const Mat& AbstractMatrix::InternalNoCheckForReadOnly() const
    {
        return petsc_matrix_;
    }


    inline call_petsc_destroy AbstractMatrix::DoCallPetscDestroy() const noexcept
    {
        return do_call_petsc_destroy_;
    }


} // namespace MoReFEM::Advanced::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_ADVANCED_ABSTRACTMATRIX_DOT_HXX_
// *** MoReFEM end header guards *** < //
