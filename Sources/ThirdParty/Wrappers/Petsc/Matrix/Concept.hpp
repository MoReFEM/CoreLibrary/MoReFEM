// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_CONCEPT_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify a type is a wrapper of sort over a Petsc Matrix.
     *
     * The result of a matrix matrix product doesn't qualify; it is instead covered by `PetscMatrixOperationResult`.
     */
    template<typename T>
    concept PetscStrictlyMatrix = requires {
        { T::ConceptIsPetscMatrix };
    };


    /*!
     * \brief Defines a concept to identify a type is a wrapper of sort over a Petsc Matrix that is intended to be the result as a matrix - matrix
     * or matrix - matrix - matrix operation.
     *
     */
    template<typename T>
    concept PetscMatrixOperationResult = requires(T object) {
        { T::ConceptIsPetscMatrixOperationResult };
        { object.IsAlreadyInitialized() } -> std::same_as<bool>;
    };


    /*!
     * \brief Defines a concept to identify a type is a wrapper of sort over a Petsc Matrix.
     *
     * The result of a matrix matrix product also qualify.
     *
     */
    template<typename T>
    concept PetscMatrix = PetscStrictlyMatrix<T> || PetscMatrixOperationResult<T>;


    /*!
     * \class doxygen_hide_petsc_matrix_concept_keyword
     *
     * \brief Keyword to indicate that this class qualify for the Concept::PetscStrictlyMatrix
     *
     */


    /*!
     * \class doxygen_hide_petsc_matrix_operation_result_concept_keyword
     *
     * \brief Keyword to indicate that this class qualify for the Concept::PetscMatrixOperationResult
     *
     */


} // namespace MoReFEM::Concept

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
