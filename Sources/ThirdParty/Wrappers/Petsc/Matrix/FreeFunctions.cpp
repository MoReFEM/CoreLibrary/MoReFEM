// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>
#include <source_location>

#include "ThirdParty/Wrappers/Petsc/Matrix/FreeFunctions.hpp"

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Advanced/AbstractMatrix.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    bool AreStrictlyEqual(const ::MoReFEM::Advanced::Wrappers::Petsc::AbstractMatrix& lhs,
                          const ::MoReFEM::Advanced::Wrappers::Petsc::AbstractMatrix& rhs,
                          const std::source_location location)
    {
        PetscBool value{};
        const int error_code = MatEqual(lhs.InternalForReadOnly(location), rhs.InternalForReadOnly(location), &value);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatEqual", location);

        switch (value)
        {
        case PETSC_TRUE:
            return true;
        case PETSC_FALSE:
            return false;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
