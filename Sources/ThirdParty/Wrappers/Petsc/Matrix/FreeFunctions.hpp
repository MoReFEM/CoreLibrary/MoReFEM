// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_FREEFUNCTIONS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_FREEFUNCTIONS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::Wrappers::Petsc { class AbstractMatrix; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief Checks whether Petsc matrices are equal.
     *
     * This is a thin wrapper over PETSc's \a MatEqual, it probably won't work if there are rounding errors (see
     * the namesake for PETSc vectors - I had to resort to something else than \a VecEqual for this reason).
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \copydoc doxygen_hide_source_location
     *
     * \return True if lhs and rhs are identical (same pointer or bitwise equality).
     */
    bool AreStrictlyEqual(const ::MoReFEM::Advanced::Wrappers::Petsc::AbstractMatrix& lhs,
                          const ::MoReFEM::Advanced::Wrappers::Petsc::AbstractMatrix& rhs,
                          const std::source_location location = std::source_location::current());


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_FREEFUNCTIONS_DOT_HPP_
// *** MoReFEM end header guards *** < //
