// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_INTERNAL_NONZEROPATTERN_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_INTERNAL_NONZEROPATTERN_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/Internal/NonZeroPattern.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::Wrappers::Petsc
{


    template<NonZeroPattern NonZeroPatternT>
    constexpr MatStructure NonZeroPatternPetsc()
    {
        switch (NonZeroPatternT)
        {
        case NonZeroPattern::same:
            return SAME_NONZERO_PATTERN;
        case NonZeroPattern::subset:
            return SUBSET_NONZERO_PATTERN;
        case NonZeroPattern::different:
            return DIFFERENT_NONZERO_PATTERN;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_INTERNAL_NONZEROPATTERN_DOT_HXX_
// *** MoReFEM end header guards *** < //
