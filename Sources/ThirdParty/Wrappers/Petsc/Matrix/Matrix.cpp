// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <cstring>
#include <source_location>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


    Matrix::Matrix(std::string&& name, Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction)
    : parent(std::move(name), do_print_linalg_destruction)
    { }


    Matrix::Matrix(const Matrix& rhs, std::string&& name) : parent(rhs, std::move(name))
    {
        CompleteCopy(rhs);
    }


    Matrix::Matrix(Matrix&& rhs) noexcept : parent(std::move(rhs))
    { }


    Matrix::~Matrix() = default;


    void Swap(Matrix& A, Matrix& B)
    {
        Swap(static_cast<Matrix::parent&>(A), static_cast<Matrix::parent&>(B));

        // No additional data attributes in the derived class so far.
    }


    void Matrix::DuplicateLayout(const Matrix& original, MatDuplicateOption option, const std::source_location location)
    {
        auto& underlying_petsc_object = parent::InternalNoCheck();

        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNull(underlying_petsc_object, location);

        const int error_code = MatDuplicate(original.InternalForReadOnly(location), option, &underlying_petsc_object);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatDuplicate", location);

        parent::SetDoCallPetscDestroy(original.DoCallPetscDestroy());
    }


    void Matrix::InitMinimalCase(const Mpi& mpi, MatType petsc_matrix_type, const std::source_location location)
    {
        auto& underlying_petsc_object = parent::InternalNoCheck();

        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNull(underlying_petsc_object, location);

        // Follow Petsc's advice and build the matrix step by step (see MatCreateSeqAIJ for this advice).
        int error_code = MatCreate(mpi.GetCommunicator(), &underlying_petsc_object);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatCreate", location);

        error_code = MatSetType(underlying_petsc_object, petsc_matrix_type);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetType", location);

        parent::SetDoCallPetscDestroy(call_petsc_destroy::yes);
    }


    void Matrix::InitMatrixHelper(const Mpi& mpi,
                                  MatType petsc_matrix_type,
                                  row_processor_wise_index_type Nlocal_row,
                                  col_processor_wise_index_type Nlocal_column,
                                  row_program_wise_index_type Nglobal_row,
                                  col_program_wise_index_type Nglobal_column,
                                  const std::source_location location)
    {
        InitMinimalCase(mpi, petsc_matrix_type, location);

        const auto Nlocal_row_int = static_cast<PetscInt>(Nlocal_row.Get());
        const auto Nlocal_col_int = static_cast<PetscInt>(Nlocal_column.Get());
        const auto Nglobal_row_int = static_cast<PetscInt>(Nglobal_row.Get());
        const auto Nglobal_col_int = static_cast<PetscInt>(Nglobal_column.Get());

        auto& underlying_petsc_object = parent::InternalNoCheck();

        const int error_code =
            MatSetSizes(underlying_petsc_object, Nlocal_row_int, Nlocal_col_int, Nglobal_row_int, Nglobal_col_int);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetSizes", location);
    }


    void Matrix::InitSequentialMatrix(row_processor_wise_index_type Nrow,
                                      col_processor_wise_index_type Ncolumn,
                                      const MatrixPattern& matrix_pattern,
                                      const Mpi& mpi,
                                      const std::source_location location)
    {
        InitMatrixHelper(
            mpi,
            MATSEQAIJ,
            Nrow,
            Ncolumn,
            row_program_wise_index_type{ Nrow.Get() }, // 'conversion' of strong type - underlying value is the same!
            col_program_wise_index_type{ Ncolumn.Get() },
            location);

        // Preallocate the sparse positions. Petsc indicates it speeds up tremendously the creation
        // process. This must be called AFTER size of the matrix is set.
        int error_code = MatSeqAIJSetPreallocationCSR(
            Internal(location), matrix_pattern.GetICsr().data(), matrix_pattern.GetJCsr().data(), MOREFEM_PETSC_NULL);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSeqAIJSetPreallocation", location);

        auto& underlying_petsc_object = parent::InternalNoCheck();

        error_code = MatSetOption(underlying_petsc_object, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetOption", location);
    }


    void Matrix::InitParallelMatrix(row_processor_wise_index_type Nlocal_row,
                                    col_processor_wise_index_type Nlocal_column,
                                    row_program_wise_index_type Nglobal_row,
                                    col_program_wise_index_type Nglobal_column,
                                    const MatrixPattern& matrix_pattern,
                                    const Mpi& mpi,
                                    const std::source_location location)
    {
        assert(matrix_pattern.Nrow() == Nlocal_row);
        assert(Nlocal_row.Get() <= Nglobal_row.Get());
        assert(Nlocal_column.Get() <= Nglobal_column.Get());

        InitMatrixHelper(mpi, MATAIJ, Nlocal_row, Nlocal_column, Nglobal_row, Nglobal_column, location);

        // Preallocate the sparse positions. Petsc indicates it speeds up tremendously the creation
        // process. This must be called AFTER size of the matrix is set.
        int error_code = MatMPIAIJSetPreallocationCSR(
            Internal(location), matrix_pattern.GetICsr().data(), matrix_pattern.GetJCsr().data(), MOREFEM_PETSC_NULL);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMPIAIJSetPreallocation", location);

        auto& underlying_petsc_object = parent::InternalNoCheck();

        error_code = MatSetOption(underlying_petsc_object, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetOption", location);

        error_code = MatSetOption(Internal(location), MAT_NO_OFF_PROC_ZERO_ROWS, PETSC_TRUE);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetOption", location);
    }


    void Matrix::InitSequentialDenseMatrix(row_processor_wise_index_type Nrow,
                                           col_processor_wise_index_type Ncolumn,
                                           const Mpi& mpi,
                                           const std::source_location location)
    {
        InitMatrixHelper(mpi,
                         MATSEQDENSE,
                         Nrow,
                         Ncolumn,
                         row_program_wise_index_type{ Nrow.Get() },
                         col_program_wise_index_type{ Ncolumn.Get() },
                         location);

        const int error_code = MatSetUp(Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetUp", location);
    }


    void Matrix::InitParallelDenseMatrix(row_processor_wise_index_type Nlocal_row,
                                         col_processor_wise_index_type Nlocal_column,
                                         row_program_wise_index_type Nglobal_row,
                                         col_program_wise_index_type Nglobal_column,
                                         const Mpi& mpi,
                                         const std::source_location location)
    {
        assert(Nlocal_row.Get() <= Nglobal_row.Get());
        assert(Nlocal_column.Get() <= Nglobal_column.Get());

        InitMatrixHelper(mpi, MATMPIDENSE, Nlocal_row, Nlocal_column, Nglobal_row, Nglobal_column, location);

        const int error_code = MatSetUp(Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetUp", location);
    }


    void Matrix::SetSequentialNonZeroPatternWithValues(const MatrixPattern& matrix_pattern,
                                                       const std::vector<double>& non_zero_values_list,
                                                       const std::source_location location)
    {
        assert(matrix_pattern.GetJCsr().size() == non_zero_values_list.size()
               && "Number of non zero indices should match the number of values to set.");
        const int error_code = MatSeqAIJSetPreallocationCSR(Internal(location),
                                                            matrix_pattern.GetICsr().data(),
                                                            matrix_pattern.GetJCsr().data(),
                                                            non_zero_values_list.data());

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSeqAIJSetPreallocationCSR", location);
    }


    void Matrix::SetParallelNonZeroPatternWithValues(const MatrixPattern& matrix_pattern,
                                                     const std::vector<double>& non_zero_values_list,
                                                     const std::source_location location)
    {
        assert(matrix_pattern.GetJCsr().size() == non_zero_values_list.size()
               && "Number of non zero indices should match the number of values to set.");
        const int error_code = MatMPIAIJSetPreallocationCSR(Internal(location),
                                                            matrix_pattern.GetICsr().data(),
                                                            matrix_pattern.GetJCsr().data(),
                                                            non_zero_values_list.data());

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMPIAIJSetPreallocationCSR", location);
    }


    void Matrix::Assembly(const std::source_location location)
    {
        const Mat& internal = Internal(location); // const is indeed ignored below as Mat is truly a pointer...

        int error_code = MatAssemblyBegin(internal, MAT_FINAL_ASSEMBLY);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatAssemblyBegin", location);

        error_code = MatAssemblyEnd(internal, MAT_FINAL_ASSEMBLY);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatAssemblyEnd", location);
    }


    bool Matrix::IsAssembled(const std::source_location location) const
    {
        const Mat& internal = InternalForReadOnly(location);

        PetscBool value{};

        const int error_code = MatAssembled(internal, &value);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatAssembled", location);

        return (value == PETSC_TRUE);
    }


    void Matrix::SetValues(const std::vector<PetscInt>& row_indexing,
                           const std::vector<PetscInt>& column_indexing,
                           const PetscScalar* values,
                           InsertMode insertOrAppend,
                           const std::source_location location,
                           ignore_zero_entries do_ignore_zero_entries)
    {
        int error_code = 0;

        if (do_ignore_zero_entries == ignore_zero_entries::yes)
        {
            error_code = MatSetOption(Internal(location), MAT_IGNORE_ZERO_ENTRIES, PETSC_TRUE);
            if (error_code)
                throw ExceptionNS::Exception(
                    error_code, "MatSetOption - MAT_IGNORE_ZERO_ENTRIES - PETSC_TRUE", location);
        }

        error_code = MatSetValues(Internal(location),
                                  static_cast<PetscInt>(row_indexing.size()),
                                  row_indexing.data(),
                                  static_cast<PetscInt>(column_indexing.size()),
                                  column_indexing.data(),
                                  values,
                                  insertOrAppend);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetValues", location);

        if (do_ignore_zero_entries == ignore_zero_entries::yes)
        {
            error_code = MatSetOption(Internal(location), MAT_IGNORE_ZERO_ENTRIES, PETSC_FALSE);
            if (error_code)
                throw ExceptionNS::Exception(
                    error_code, "MatSetOption - MAT_IGNORE_ZERO_ENTRIES - PETSC_TRUE", location);
        }
    }


    void Matrix::SetValuesRow(PetscInt row_index, const PetscScalar* values, const std::source_location location)
    {
        const int error_code = MatSetValuesRow(Internal(location), row_index, values);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetValuesRow", location);
    }


    void Matrix::SetValue(PetscInt row_index,
                          PetscInt column_index,
                          PetscScalar value,
                          InsertMode insertOrAppend,
                          const std::source_location location)
    {
        const int error_code = MatSetValue(Internal(location), row_index, column_index, value, insertOrAppend);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatSetValue", location);
    }


    void Matrix::Copy(const Matrix& source, const MatStructure& structure, const std::source_location location)
    {
        const int error_code = MatCopy(source.InternalForReadOnly(location), Internal(location), structure);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatCopy", location);
    }


    void Matrix::CompleteCopy(const Matrix& source, const std::source_location location)
    {
        DuplicateLayout(source, MAT_SHARE_NONZERO_PATTERN, location);
        Copy(source, SAME_NONZERO_PATTERN, location);
    }


    void Matrix::LoadBinary(const Mpi& mpi, const FilesystemNS::File& input_file, const std::source_location location)
    {
        Viewer viewer(mpi, input_file, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_READ, location);

        const int error_code = MatLoad(Internal(location), viewer.GetUnderlyingPetscObject());
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatLoad", location);
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
