// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXINFO_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXINFO_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
#include <iostream>
#include <source_location> // IWYU pragma: export
// IWYU pragma: no_include <string>


#ifndef NDEBUG


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Petsc { class Matrix; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{

    /*!
     * \brief Wrapper over MatGetInfo(), which provides information about the structure of the matrix.
     *
     * This is intended to be used only in debug mode!
     *
     * \param[in] tag String that identifies the matrix in the output stream (useful if several calls to
     * this function are present in the same code).
     * \param[in] matrix Matrix for which information are requested.
     * \copydoc doxygen_hide_stream_inout
     * \copydetails doxygen_hide_source_location
     */
    void MatrixInfo(const std::string& tag,
                    const Matrix& matrix,
                    const std::source_location location = std::source_location::current(),
                    std::ostream& stream = std::cout);

} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

#endif // NDEBUG

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXINFO_DOT_HPP_
// *** MoReFEM end header guards *** < //
