// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOpResult.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Advanced/Enum.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    MatrixOpResult::MatrixOpResult(std::string&& name,
                                   Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction)
    : parent(std::move(name), do_print_linalg_destruction)
    { }


    MatrixOpResult::~MatrixOpResult() = default;


    MatrixOpResult::MatrixOpResult(MatrixOpResult&& rhs) noexcept : parent(std::move(rhs))
    { }


    bool MatrixOpResult::IsAlreadyInitialized() const noexcept
    {
        return parent::InternalNoCheckForReadOnly() != MOREFEM_PETSC_NULL;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
