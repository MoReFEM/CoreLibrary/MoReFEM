// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPRESULT_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPRESULT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <type_traits>

#include "ThirdParty/Wrappers/Petsc/Matrix/Advanced/AbstractMatrix.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief Wrapper over a PETSc \a Mat  matrix that is the result of a matrix-matrix product.
     *
     * PETSc expects such matrix NOT to be properly initialized with a defined pattern; if not it triggers an error.
     *
     * It was cumbersome so far to bookkeep which should be initialized or not, so this object was introduced for the
     * ones that should never been initialized - simply by not providing the adequate methods!
     */
    class MatrixOpResult : public Advanced::Wrappers::Petsc::AbstractMatrix
    {

      public:
        //! Alias to self.
        using self = MatrixOpResult;

        //! Alias to parent.
        using parent = Advanced::Wrappers::Petsc::AbstractMatrix;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_petsc_matrix_operation_result_concept_keyword
        static inline constexpr bool ConceptIsPetscMatrixOperationResult = true;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         *
         * \copydetails doxygen_hide_print_linalg_arg
         */
        explicit MatrixOpResult(std::string&& name,
                                Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction =
                                    Advanced::Wrappers::Petsc::print_linalg_destruction::no);

        //! Destructor.
        ~MatrixOpResult() override;

        //! \copydoc doxygen_hide_move_constructor
        MatrixOpResult(MatrixOpResult&& rhs) noexcept;

        /*!
         * \copydoc doxygen_hide_copy_constructor
         *
         * \internal The object is not really intended to be copied - be cautious if at some point this copy constructor is deemed to be required!
         */
        MatrixOpResult(const MatrixOpResult& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MatrixOpResult& operator=(const MatrixOpResult& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MatrixOpResult& operator=(MatrixOpResult&& rhs) = delete;

        ///@}


        /*!
         * \brief Tells whether the underlying matrix has already been set or not.
         *
         * If so, matrix operations involving the underlying PETSc `Mat` object will be given the value
         * MAT_REUSE_MATRIX; otherwise it will be MAT_INITIAL_MATRIX. Formerly I provided an enum to choose which entry
         * to choose, but it was actually a pain to manage, as if the wrong value was provided this resulted in a PETSc
         * crash... so writing a model was a lot of trials and errors. Now we automate this with `MatrixOpResult`: if
         * the underlying value is already set it will automatically assume MAT_REUSE_MATRIX; if not it will call
         * MAT_INITIAL_MATRIX.
         *
         */
        bool IsAlreadyInitialized() const noexcept;
    };


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPRESULT_DOT_HPP_
// *** MoReFEM end header guards *** < //
