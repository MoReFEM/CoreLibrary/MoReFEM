// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPERATIONS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPERATIONS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Concept.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \class doxygen_hide_matrix_axpy
     *
     * \brief Wrapper over MatAXPY, that performs Y = a * X + Y.
     *
     *
     * \tparam NonZeroPatternT This value indicates the level of similarity between X and Y non zero patterns.
     * \warning Beware: if X and Y aren't following the same pattern, 'Same' won't yield what
     * you expect! In the case you're considering adding a part computed by a transfert matrix, you should
     * use 'NonZeroPattern::subset'. In MoReFEM you should consider using the namesake provided in
     * `GlobalLinearAlgebraNS` namespace that provides additional safeties in debug mode by checking the
     * `NumberingSubset` involved.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \param[in] a See formula above.
     * \param[in] X See formula above.
     * \param[in] Y See formula above.
     *
     * The check about matching sizes for both matrices is properly handled by PETSc itself and is therefore not
     * replicated in the wrapper.
     */

    //! \copydoc doxygen_hide_matrix_axpy
    template<NonZeroPattern NonZeroPatternT, Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU>
    void AXPY(PetscScalar a,
              const MatrixT& X,
              MatrixU& Y,
              const std::source_location location = std::source_location::current());


    /*!
     * \class doxygen_hide_mat_shift
     *
     * \brief Wrapper over MatShift, that performs M = M + a I, where a is a PetscScalar and I is the identity
     * matrix.
     *
     * \param[in] matrix See formula above.
     * \param[in] a See formula above.
     * \copydoc doxygen_hide_source_location
     */

    //! \copydoc doxygen_hide_mat_shift
    template<Concept::PetscMatrix MatrixT>
    void MatShift(const PetscScalar a,
                  MatrixT& matrix,
                  const std::source_location location = std::source_location::current());


    /*!
     * \class doxygen_hide_mat_mult
     *
     * \brief Wrapper over MatMult, that performs v2 = matrix * v1.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \param[in] matrix See formula above.
     * \param[in] v1 See formula above.
     * \param[in] v2 See formula above.
     * \copydoc doxygen_hide_do_update_ghost_arg
     */

    //! \copydoc doxygen_hide_mat_mult
    template<Concept::PetscMatrix MatrixT>
    void MatMult(const MatrixT& matrix,
                 const Vector& v1,
                 Vector& v2,
                 const std::source_location location = std::source_location::current(),
                 update_ghost do_update_ghost = update_ghost::yes);


    /*!
     * \class doxygen_hide_mat_mult_add
     *
     * \brief Wrapper over MatMultAdd, that performs v3 = v2 + matrix * v1.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \param[in] matrix See formula above.
     * \param[in] v1 See formula above.
     * \param[in] v2 See formula above.
     * \param[in] v3 See formula above.
     * \copydoc doxygen_hide_do_update_ghost_arg
     */

    //! \copydoc doxygen_hide_mat_mult_add
    template<Concept::PetscMatrix MatrixT>
    void MatMultAdd(const MatrixT& matrix,
                    const Vector& v1,
                    const Vector& v2,
                    Vector& v3,
                    const std::source_location location = std::source_location::current(),
                    update_ghost do_update_ghost = update_ghost::yes);


    /*!
     * \class doxygen_hide_mat_mult_transpose
     *
     * \brief Wrapper over MatMultTranspose, that performs v2 = transpose(matrix) * v1.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \copydoc doxygen_hide_do_update_ghost_arg
     *
     * \param[in] matrix See formula above.
     * \param[in] v1 See formula above.
     * \param[in] v2 See formula above.
     */

    //! \copydoc doxygen_hide_mat_mult_transpose
    template<Concept::PetscMatrix MatrixT>
    void MatMultTranspose(const MatrixT& matrix,
                          const Vector& v1,
                          Vector& v2,
                          const std::source_location location = std::source_location::current(),
                          update_ghost do_update_ghost = update_ghost::yes);


    /*!
     * \class doxygen_hide_mat_mult_transpose_add
     *
     * \brief Wrapper over MatMultTransposeAdd, that performs v3 = v2 + transpose(matrix) * v1.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \copydoc doxygen_hide_do_update_ghost_arg
     *
     * \param[in] matrix See formula above.
     * \param[in] v1 See formula above.
     * \param[in] v2 See formula above.
     * \param[in] v3 See formula above.
     */

    //! \copydoc doxygen_hide_mat_mult_transpose_add
    template<Concept::PetscMatrix MatrixT>
    void MatMultTransposeAdd(const MatrixT& matrix,
                             const Vector& v1,
                             const Vector& v2,
                             Vector& v3,
                             const std::source_location location = std::source_location::current(),
                             update_ghost do_update_ghost = update_ghost::yes);

    /*!
     * \class doxygen_hide_matmatmult_warning
     *
     * \attention In Petsc, matrix-matrix multiplication functions compute on the fly the
     * pattern for the resulting matrix. However, it is possible this pattern isn't the one expected;
     * in the richer MoReFEM interface you should call in debug mode \a AssertMatrixRespectPattern()
     * to make sure the resulting matrix respects the pattern defined for the \a GlobalMatrix pair
     * of \a NumberingSubset.
     *
     * \internal <b><tt>[internal]</tt></b> If you know Petsc, you might see I didn't give access to
     * argument MatReuse, setting it each time to MAT_INITIAL_MATRIX and skipping entirely MAT_REUSE_MATRIX.
     * This is because at the time being MatMatMult operations are seldom in the code (only Poromechanics so
     * far) and using Symbolic/Numeric seems more elegant. Of course, reintroducing the argument is really easy;
     * feel free to do so if you need it (for instance for MatMatMatMult support: Symbolic/Numeric doesn't
     * work for them and Petsc guys seemed unlikely to fix that in our exchanges).
     *
     * <b><tt>[internal]</tt></b> \todo #684 Investigate to use the argument fill, which provides an
     * estimation of the non zero of the resulting matrix. Currently PETSC_DEFAULT is used.

     * \endinternal
     */


    /*!
     * \class doxygen_hide_mat_mat_mult
     *
     * \brief Wrapper over MatMatMult, that performs m3 = m1 * m2.
     *
     * \copydetails doxygen_hide_matmatmult_warning
     *
     * If the operation is performed many times with each time the same non zero pattern for the matrices,
     * it might be worth investigating MatMatMultSymbolic/MatMatMultNumeric to improve efficiency (not already
     implemented in MoReFEM,
     * but it was hinted by PETSc developers - feel free to open an issue if you need it!

     * \param[in] m1 See formula above.
     * \param[in] m2 See formula above.
     * \param[in] m3 See formula above.
     *

     *
     * \copydoc doxygen_hide_source_location
     */

    //! \copydoc doxygen_hide_mat_mat_mult
    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void MatMatMult(const MatrixT& m1,
                    const MatrixU& m2,
                    MatrixV& m3,
                    const std::source_location location = std::source_location::current());


    /*!
     * \class doxygen_hide_mat_mat_mat_mult
     *
     * \brief Wrapper over MatMatMatMult, that performs m4 = m1 * m2 * m3.
     *
     * \param[in] m1 See formula above.
     * \param[in] m2 See formula above.
     * \param[in] m3 See formula above.
     * \param[in] m4 See formula above.

     *
     * \copydoc doxygen_hide_source_location
     */

    //! \copydoc doxygen_hide_mat_mat_mat_mult
    template<Concept::PetscMatrix MatrixT,
             Concept::PetscMatrix MatrixU,
             Concept::PetscMatrix MatrixV,
             Concept::PetscMatrixOperationResult MatrixW>
    void MatMatMatMult(const MatrixT& m1,
                       const MatrixU& m2,
                       const MatrixV& m3,
                       MatrixW& m4,
                       const std::source_location location = std::source_location::current());

    /*!
     * \class doxygen_hide_mat_create_transpose
     *
     * \brief Creates a new matrix object that behaves like A'.
     *
     *
     * \param[in] A matrix to transpose.
     * \param[out] transpose The matrix that figuratively represents A'. This matrix must not have been
     * allocated!
     * \copydoc doxygen_hide_source_location
     *
     * \attention From [underlying PETSc function](https://petsc.org/release/manualpages/Mat/MatCreateTranspose/):
     * The transpose A' is NOT actually formed! Rather the new matrix object performs the
     * matrix-vector product by using the MatMultTranspose() on the original matrix.
     *
     */

    //! \copydoc doxygen_hide_mat_create_transpose
    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrixOperationResult MatrixU>
    void MatCreateTranspose(const MatrixT& A,
                            MatrixU& transpose,
                            const std::source_location location = std::source_location::current());


    /*!
     * \class doxygen_hide_mat_transpose_mat_mult
     *
     * Formula is:
     * \verbatim
     m3 = m1^T * m2.
     * \endverbatim
     *
     * \copydetails doxygen_hide_matmatmult_warning
     *
     * \param[in] m1 See formula above.
     * \param[in] m2 See formula above.
     * \param[out] m3 See formula above. The matrix must be not allocated when this function is called.
     * \copydoc doxygen_hide_source_location
     */


    //! \copydoc doxygen_hide_mat_transpose_mat_mult
    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void MatTransposeMatMult(const MatrixT& m1,
                             const MatrixU& m2,
                             MatrixV& m3,
                             const std::source_location location = std::source_location::current());

    /*!
     * \class doxygen_hide_mat_mat_transpose_mult
     *
     * \brief Performs Matrix-Matrix Multiplication C = A * B^T.
     *
     * \copydetails doxygen_hide_matmatmult_warning
     *
     * \param[in] matrix1 A in C = A * B^T.
     * \param[in] matrix2 B in C = A * B^T.
     * \param[out] matrix3 C in B in C = A * B^T. The matrix must be not allocated when this function is called.
     * \copydoc doxygen_hide_source_location
     *
     * \attention As of PETSc v3.20.4, there is no support for this operation for MPIAIJ and MPIAIJ matrices - so the code you would write with it will not
     * run in parallel. You should avoid using it as a result.
     */

    //! \copydoc doxygen_hide_mat_mat_transpose_mult
    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void MatMatTransposeMult(const MatrixT& matrix1,
                             const MatrixU& matrix2,
                             MatrixV& matrix3,
                             const std::source_location location = std::source_location::current());


    /*!
     * \class doxygen_hide_mat_pt_a_p
     *
     * \brief Performs the matrix product C = P^T * A * P
     *
     * \warning Unfortunately a simple test with P = I leads to error
     * \verbatim
     [0;39m[0;49m[0]PETSC ERROR: Nonconforming object sizes
     [0]PETSC ERROR: Expected fill=-2 must be >= 1.0
     \endverbatim
     * The reason is that PETSC_DEFAULT may not be supported (I've asked Petsc developers); but even with
     * hand-tailored fill it doesn't seem to work...
     * So unfortunately I have to advise instead MatMatMult followed by MatTransposeMatMult instead...
     *
     * \copydetails doxygen_hide_matmatmult_warning
     *
     * \param[in] A A in C = P^T * A * P.
     * \param[in] P P in C = P^T * A * P
     * \param[out] out C in C = P^T * A * P. The matrix must be not allocated when this function is called.
     * \copydetails doxygen_hide_source_location
     *
     */

    //! \copydoc doxygen_hide_mat_pt_a_p
    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void PtAP(const MatrixT& A,
              const MatrixU& P,
              MatrixV& out,
              const std::source_location location = std::source_location::current());


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPERATIONS_DOT_HPP_
// *** MoReFEM end header guards *** < //
