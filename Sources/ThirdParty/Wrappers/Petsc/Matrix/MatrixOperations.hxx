// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPERATIONS_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPERATIONS_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
// *** MoReFEM header guards *** < //

#include "Core/LinearAlgebra/Assertion/Assertion.hpp" // IWYU pragma: export


namespace MoReFEM::Wrappers::Petsc
{


    template<Concept::PetscMatrix MatrixT>
    void MatMultTranspose(const MatrixT& matrix,
                          const Vector& v1,
                          Vector& v2,
                          const std::source_location location,
                          update_ghost do_update_ghost)
    {
        int error_code = ::MatMultTranspose(
            matrix.InternalForReadOnly(location), v1.InternalForReadOnly(location), v2.Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMultTranspose", location);

        v2.UpdateGhosts(location, do_update_ghost);
    }


    template<Concept::PetscMatrix MatrixT>
    void MatMultTransposeAdd(const MatrixT& matrix,
                             const Vector& v1,
                             const Vector& v2,
                             Vector& v3,
                             const std::source_location location,
                             update_ghost do_update_ghost)
    {
#ifndef NDEBUG
        {
            // This case is explicitly forbidden by PETSc documentation.
            // See [here](https://petsc.org/release/manualpages/Mat/MatMultTransposeAdd)
            if (v1.InternalForReadOnly(location) == v3.InternalForReadOnly(location))
                throw GlobalLinearAlgebraNS::AssertionNS::ForbiddenSameVector(
                    "MatMultTransposeAdd", std::make_pair(1UL, 3UL), location);
        }
#endif // NDEBUG


        int error_code = ::MatMultTransposeAdd(matrix.InternalForReadOnly(location),
                                               v1.InternalForReadOnly(location),
                                               v2.InternalForReadOnly(location),
                                               v3.Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMultTransposeAdd", location);

        v3.UpdateGhosts(location, do_update_ghost);
    }


    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void MatMatMult(const MatrixT& matrix1, const MatrixU& matrix2, MatrixV& out, const std::source_location location)
    {
        Mat result;
        int error_code{};

        const bool was_matrix_result_already_used = out.IsAlreadyInitialized();

        if (was_matrix_result_already_used)
        {
            result = out.Internal(location);
            error_code = ::MatMatMult(matrix1.InternalForReadOnly(location),
                                      matrix2.InternalForReadOnly(location),
                                      MAT_REUSE_MATRIX,
                                      PETSC_DEFAULT,
                                      &result);
        } else
        {
            error_code = ::MatMatMult(matrix1.InternalForReadOnly(location),
                                      matrix2.InternalForReadOnly(location),
                                      MAT_INITIAL_MATRIX,
                                      PETSC_DEFAULT,
                                      &result);
            out.SetFromPetscMat(result, location);
        }

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMatMult", location);
    }


    // clang-format off
    template
    <
        Concept::PetscMatrix MatrixT,
        Concept::PetscMatrix MatrixU,
        Concept::PetscMatrix MatrixV,
        Concept::PetscMatrixOperationResult MatrixW
    >
    // clang-format on
    void MatMatMatMult(const MatrixT& matrix1,
                       const MatrixU& matrix2,
                       const MatrixV& matrix3,
                       MatrixW& out,
                       const std::source_location location)
    {
        Mat result;
        int error_code{};

        const bool was_matrix_result_already_used = out.IsAlreadyInitialized();

        if (was_matrix_result_already_used)
        {
            result = out.Internal(location);
            error_code = ::MatMatMatMult(matrix1.InternalForReadOnly(location),
                                         matrix2.InternalForReadOnly(location),
                                         matrix3.InternalForReadOnly(location),
                                         MAT_REUSE_MATRIX,
                                         PETSC_DEFAULT,
                                         &result);
        } else
        {
            error_code = ::MatMatMatMult(matrix1.InternalForReadOnly(location),
                                         matrix2.InternalForReadOnly(location),
                                         matrix3.InternalForReadOnly(location),
                                         MAT_INITIAL_MATRIX,
                                         PETSC_DEFAULT,
                                         &result);

            out.SetFromPetscMat(result, location);
        }

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMatMatMult", location);
    }


    template<NonZeroPattern NonZeroPatternT, Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU>
    void AXPY(PetscScalar a, const MatrixT& X, MatrixU& Y, const std::source_location location)
    {
        int error_code = ::MatAXPY(Y.Internal(location),
                                   a,
                                   X.InternalForReadOnly(location),
                                   Internal::Wrappers::Petsc::NonZeroPatternPetsc<NonZeroPatternT>());

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatAXPY", location);
    }


    template<Concept::PetscMatrix MatrixT>
    void MatShift(const PetscScalar a, MatrixT& matrix, const std::source_location location)
    {
        int error_code = ::MatShift(matrix.Internal(location), a);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatShift", location);
    }


    template<Concept::PetscMatrix MatrixT>
    void MatMult(const MatrixT& matrix,
                 const Vector& v1,
                 Vector& v2,
                 const std::source_location location,
                 update_ghost do_update_ghost)
    {
        int error_code =
            ::MatMult(matrix.InternalForReadOnly(location), v1.InternalForReadOnly(location), v2.Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMult", location);

        v2.UpdateGhosts(location, do_update_ghost);
    }


    template<Concept::PetscMatrix MatrixT>
    void MatMultAdd(const MatrixT& matrix,
                    const Vector& v1,
                    const Vector& v2,
                    Vector& v3,
                    const std::source_location location,
                    update_ghost do_update_ghost)
    {
        int error_code = ::MatMultAdd(matrix.InternalForReadOnly(location),
                                      v1.InternalForReadOnly(location),
                                      v2.InternalForReadOnly(location),
                                      v3.Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMultAdd", location);

        v3.UpdateGhosts(location, do_update_ghost);
    }


    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrixOperationResult MatrixU>
    void MatCreateTranspose(const MatrixT& A, MatrixU& transpose, const std::source_location location)
    {
        Mat result;

        int error_code = ::MatCreateTranspose(A.InternalForReadOnly(location), &result);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatCreateTranspose", location);

        transpose.SetFromPetscMat(result, location);
    }


    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void MatTransposeMatMult(const MatrixT& matrix1,
                             const MatrixU& matrix2,
                             MatrixV& matrix3,
                             const std::source_location location)
    {
        Mat result;

        int error_code{};

        const bool was_matrix_result_already_used = matrix3.IsAlreadyInitialized();

        if (was_matrix_result_already_used)
        {
            result = matrix3.Internal(location);

            error_code = ::MatTransposeMatMult(matrix1.InternalForReadOnly(location),
                                               matrix2.InternalForReadOnly(location),
                                               MAT_REUSE_MATRIX,
                                               PETSC_DEFAULT,
                                               &result);
        } else
        {
            error_code = ::MatTransposeMatMult(matrix1.InternalForReadOnly(location),
                                               matrix2.InternalForReadOnly(location),
                                               MAT_INITIAL_MATRIX,
                                               PETSC_DEFAULT,
                                               &result);

            matrix3.SetFromPetscMat(result, location);
        }

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatTransposeMatMult", location);
    }


    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void MatMatTransposeMult(const MatrixT& matrix1,
                             const MatrixU& matrix2,
                             MatrixV& matrix3,
                             const std::source_location location)
    {
        Mat result;
        int error_code{};

        std::cout << "[WARNING] You're using MatMatTransposeMult(), that does not support most of parallel matrix "
                     "(as of PETSc 3.20.4 - see https://petsc.org/release/manualpages/Mat/MatMatTransposeMult)."
                  << std::endl;

        const bool was_matrix_result_already_used = matrix3.IsAlreadyInitialized();

        if (was_matrix_result_already_used)
        {
            result = matrix3.Internal(location);
            error_code = ::MatMatTransposeMult(matrix1.InternalForReadOnly(location),
                                               matrix2.InternalForReadOnly(location),
                                               MAT_REUSE_MATRIX,
                                               PETSC_DEFAULT,
                                               &result);
        } else
        {
            error_code = ::MatMatTransposeMult(matrix1.InternalForReadOnly(location),
                                               matrix2.InternalForReadOnly(location),
                                               MAT_INITIAL_MATRIX,
                                               PETSC_DEFAULT,
                                               &result);

            matrix3.SetFromPetscMat(result, location);
        }

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatMatTransposeMult", location);
    }


    template<Concept::PetscMatrix MatrixT, Concept::PetscMatrix MatrixU, Concept::PetscMatrixOperationResult MatrixV>
    void PtAP(const MatrixT& A, const MatrixU& P, MatrixV& out, const std::source_location location)
    {
        Mat result;
        int error_code{};

        const bool was_matrix_result_already_used = out.IsAlreadyInitialized();

        if (was_matrix_result_already_used)
        {
            result = out.Internal(location);
            error_code = ::MatPtAP(A.InternalForReadOnly(location),
                                   P.InternalForReadOnly(location),
                                   MAT_REUSE_MATRIX,
                                   PETSC_DEFAULT,
                                   &result);
        } else
        {
            error_code = ::MatPtAP(A.InternalForReadOnly(location),
                                   P.InternalForReadOnly(location),
                                   MAT_INITIAL_MATRIX,
                                   PETSC_DEFAULT,
                                   &result);

            out.SetFromPetscMat(result, location);
        }

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatPtAP", location);
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXOPERATIONS_DOT_HXX_
// *** MoReFEM end header guards *** < //
