// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <ostream>
#include <sstream>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Utilities/Containers/Print.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    MatrixPattern::MatrixPattern(const std::vector<std::vector<PetscInt>>& non_zero_slots_per_local_row)
    {
        iCSR_.reserve(non_zero_slots_per_local_row.size() + 1);

        iCSR_.push_back(0);
        std::size_t iCSR_index = 0;

        for (const auto& row : non_zero_slots_per_local_row)
        {
            iCSR_index += row.size();
            iCSR_.push_back(static_cast<PetscInt>(iCSR_index));
            std::ranges::move(row, std::back_inserter(jCSR_));
        }
    }


    MatrixPattern::MatrixPattern(std::vector<PetscInt>&& iCSR, std::vector<PetscInt>&& jCSR)
    : iCSR_(std::move(iCSR)), jCSR_(std::move(jCSR))
    { }


    bool operator==(const MatrixPattern& lhs, const MatrixPattern& rhs)
    {
        if (lhs.GetICsr() != rhs.GetICsr())
            return false;

        return lhs.GetJCsr() == rhs.GetJCsr();
    }


    std::ostream& operator<<(std::ostream& stream, const MatrixPattern& rhs)
    {
        std::ostringstream internal_out; // internal step to guarantee no interleaving in parallel.
        internal_out.copyfmt(stream);

        const auto nnz_per_row = ExtractNonZeroPositionsPerRow(rhs);

        auto row_index{ 0 };
        std::ostringstream oconv;

        for (const auto& row : nnz_per_row)
        {
            oconv.str("");
            oconv << "For row " << row_index++ << ": [";

            Utilities::PrintContainer<>::Do(row,
                                            internal_out,
                                            ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                            ::MoReFEM::PrintNS::Delimiter::opener(oconv.str()));
        }

        stream << internal_out.str();

        return stream;
    }


    std::vector<std::vector<PetscInt>> ExtractNonZeroPositionsPerRow(const Wrappers::Petsc::MatrixPattern& pattern)
    {
        decltype(auto) iCSR = pattern.GetICsr();

        const auto Nrow = pattern.Nrow();
        assert(Nrow > row_processor_wise_index_type{});

        decltype(auto) jCSR = pattern.GetJCsr();

        std::vector<std::vector<PetscInt>> ret;

        PetscInt cumulative_iCSR_value{};
        assert(iCSR.size() == static_cast<std::size_t>(Nrow.Get() + 1));

        PetscInt current_jcsr_index{};

        for (auto row_index = row_processor_wise_index_type{}; row_index < Nrow; ++row_index)
        {
            std::vector<PetscInt> row_content;

            auto current_icsr_value = iCSR[static_cast<std::size_t>(row_index.Get()) + 1UL];

            auto Nnnz_for_row = current_icsr_value - cumulative_iCSR_value;
            cumulative_iCSR_value = current_icsr_value;

            for (auto _ = PetscInt{}; _ < Nnnz_for_row; ++_)
            {
                assert(current_jcsr_index < static_cast<PetscInt>(jCSR.size()));
                row_content.push_back(jCSR[static_cast<std::size_t>(current_jcsr_index++)]);
            }

            ret.emplace_back(row_content);
        }


        return ret;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
