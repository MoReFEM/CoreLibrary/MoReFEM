// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_NONZEROPATTERN_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_NONZEROPATTERN_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM
{


    /*!
     * \brief Convenient enum used to typify more strongly Petsc macro values.
     *
     * \internal Should be in Wrappers::Petsc sub-namespace as well but it's more convenient for a model
     * developer not to have to preffix this enum class each time.
     */
    enum class NonZeroPattern { same, different, subset };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Matrix/Internal/NonZeroPattern.hpp"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_NONZEROPATTERN_DOT_HPP_
// *** MoReFEM end header guards *** < //
