// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_SHELLMATRIX_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_SHELLMATRIX_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/ShellMatrix.hpp"
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp" // IWYU pragma: export


namespace MoReFEM::Wrappers::Petsc
{


    template<class ContextT>
    ShellMatrix<ContextT>::~ShellMatrix()
    {
        assert(petsc_matrix_ != MOREFEM_PETSC_NULL);
        [[maybe_unused]] auto error_code = MatDestroy(&petsc_matrix_);
        assert(!error_code && "Error in Mat destruction."); // no exception in destructors!
    }


    template<class ContextT>
    ShellMatrix<ContextT>::ShellMatrix(const Wrappers::Mpi& mpi,
                                       row_processor_wise_index_type Nlocal_row,
                                       col_processor_wise_index_type Nlocal_column,
                                       row_program_wise_index_type Nglobal_row,
                                       col_program_wise_index_type Nglobal_column,
                                       ContextT* context,
                                       MatOperation mat_op,
                                       const std::source_location location)
    {
        int error_code = MatCreateShell(mpi.GetCommunicator(),
                                        static_cast<PetscInt>(Nlocal_row),
                                        static_cast<PetscInt>(Nlocal_column),
                                        static_cast<PetscInt>(Nglobal_row),
                                        static_cast<PetscInt>(Nglobal_column),
                                        context,
                                        &petsc_matrix_);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatCreateShell", location);

        SetOperation(mat_op, location);
    }


    template<class ContextT>
    inline PetscInt ApplyOperation(Mat shell_matrix, Vec x, Vec y)
    {
        ContextT* context;

        auto error_code = MatShellGetContext(shell_matrix, &context);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatShellGetContext");

        context->ShellMatrixOperation(x, y);

        return 0;
    }


    template<class ContextT>
    void ShellMatrix<ContextT>::SetOperation(MatOperation mat_op, const std::source_location location)
    {
        int error_code = MatShellSetOperation(
            Internal(location), mat_op, reinterpret_cast<void (*)(void)>(ApplyOperation<ContextT>));

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatShellSetOperation", location);
    }


    template<class ContextT>
    inline Mat ShellMatrix<ContextT>::Internal(std::source_location location)
    {
        AssertionNS::InternalNotNull(petsc_matrix_, location);
        return petsc_matrix_;
    }


    template<class ContextT>
    inline Mat ShellMatrix<ContextT>::InternalForReadOnly(std::source_location location) const noexcept
    {
        AssertionNS::InternalNotNull(petsc_matrix_, location);
        return petsc_matrix_;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_SHELLMATRIX_DOT_HXX_
// *** MoReFEM end header guards *** < //
