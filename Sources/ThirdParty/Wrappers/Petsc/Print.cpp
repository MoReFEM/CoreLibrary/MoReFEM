// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdio>
#include <source_location>

#include "ThirdParty/Wrappers/Petsc/Print.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    void SynchronizedFlush(const Mpi& mpi, FILE* C_file, const std::source_location location)
    {
        const int error_code = PetscSynchronizedFlush(mpi.GetCommunicator(), C_file);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscSynchronizedFlush", location);
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
