// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_PRINT_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_PRINT_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstdio>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"                // IWYU pragma: export // IWYU pragma: keep
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp" // IWYU pragma: keep


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief Print a message on the first processor only.
     *
     * \param[in] message Message to be printed on screen.
     * \copydetails doxygen_hide_mpi_param
     *
     * \copydoc doxygen_hide_source_location
     *
     * \param[in] arguments Variadic arguments that fill the same role as printf variadic arguments.
     * So for instance if there is a '%d' in \a message an integer is expected here.
     */
    template<typename... Args>
    void PrintMessageOnFirstProcessor(const std::string& message,
                                      const Mpi& mpi,
                                      const std::source_location location = std::source_location::current(),
                                      Args&&... arguments);


    /*!
     * \brief Print messages from several processors with synchronization (to prevent interleaving).
     *
     * A call to SynchronizedFlush() is mandatory to ensure all messages are printed.
     *
     * \param[in] message Message to be printed on screen.
     * \copydetails doxygen_hide_mpi_param
     *
     * \param[in] C_file FILE object used in C. No conversion with C++ streams; however as sync_with_stdio
     * is true for std::cout, std::cerr and std::clog in all MoReFEM, you can provide here stdout, stderr
     * and stdlog respectively.
     * \copydoc doxygen_hide_source_location
     *
     * \param[in] arguments Variadic arguments that fill the same role as printf variadic arguments.
     * So for instance if there is a '%d' in \a message an integer is expected here.
     */
    template<typename... Args>
    void PrintSynchronizedMessage(const std::string& message,
                                  const Mpi& mpi,
                                  FILE* C_file,
                                  const std::source_location location = std::source_location::current(),
                                  Args&&... arguments);


    /*!
     * \brief Flush synchronized messages.
     *
     * Must be called to ensure all synchronized messages are printed.
     *
     * \internal <b><tt>[internal]</tt></b> This function is called in Petsc RAII for all standard outputs to
     * ensure it's called at least once; however developers may need to call it more often than that.
     * \endinternal
     *
     * \copydetails doxygen_hide_mpi_param
     *
     * \param[in] C_file FILE object used in C. No conversion with C++ streams; however as sync_with_stdio
     * is true for std::cout, std::cerr and std::clog in all MoReFEM, you can provide here stdout, stderr
     * and stdlog respectively.
     *
     * \copydoc doxygen_hide_source_location
     */
    void SynchronizedFlush(const Mpi& mpi,
                           FILE* C_file,
                           const std::source_location location = std::source_location::current());


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Print.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_PRINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
