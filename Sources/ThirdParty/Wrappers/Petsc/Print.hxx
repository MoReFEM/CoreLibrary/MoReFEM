// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_PRINT_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_PRINT_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Print.hpp"
// *** MoReFEM header guards *** < //


#include <cstdio>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    template<typename... Args>
    void PrintMessageOnFirstProcessor(const std::string& message,
                                      const Mpi& mpi,
                                      const std::source_location location,
                                      Args&&... arguments)
    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/format-nonliteral.hpp" // IWYU pragma: keep
#include "Utilities/Warnings/Internal/IgnoreWarning/format-security.hpp"   // IWYU pragma: keep
        int error_code = PetscPrintf(mpi.GetCommunicator(), message.c_str(), std::forward<Args>(arguments)...);
        PRAGMA_DIAGNOSTIC(pop)

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscPrintf", location);
    }


    template<typename... Args>
    void PrintSynchronizedMessage(const std::string& message,
                                  const Mpi& mpi,
                                  FILE* C_file,
                                  const std::source_location location,
                                  Args&&... arguments)
    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/format-nonliteral.hpp" // IWYU pragma: keep
#include "Utilities/Warnings/Internal/IgnoreWarning/format-security.hpp"   // IWYU pragma: keep
        int error_code =
            PetscSynchronizedFPrintf(mpi.GetCommunicator(), C_file, message.c_str(), std::forward<Args>(arguments)...);
        PRAGMA_DIAGNOSTIC(pop)

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscSynchronizedFPrintf", location);
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_PRINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
