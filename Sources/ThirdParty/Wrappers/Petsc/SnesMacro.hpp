// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SNESMACRO_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SNESMACRO_DOT_HPP_
// *** MoReFEM header guards *** < //


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// ============================

/*!
 * In debug mode, we need to also be able to catch MoReFEM assertions. As I can't define a macro inside a macro,
 * I need to work around.
 */

#ifndef NDEBUG
#define MOREFEM_TRAP_SNES_ASSERTION_CATCH                                                                              \
    catch (const Advanced::Assertion& e)                                                                               \
    {                                                                                                                  \
        std::cerr << "MoReFEM assertion caught in the Petsc Snes definition: " << e.what() << std::endl;               \
        return PETSC_ERR_MIN_VALUE;                                                                                    \
    }
#else // NDEBUG
#define MOREFEM_TRAP_SNES_ASSERTION_CATCH
#endif // NDEBUG


#if defined(MOREFEM_NO_TRAP_SNES_EXCEPTION) || defined(SONARQUBE_4_MOREFEM)

#define MOREFEM_TRAP_SNES_EXCEPTION_TRY
#define MOREFEM_TRAP_SNES_EXCEPTION_CATCH(Snes)

#else // MOREFEM_NO_TRAP_SNES_EXCEPTION

/*!
 * If the macro is not defined, in standard SnesInterface functions exceptions are caught and transformed into
 * Petsc error codes. This prevent libc++ abi issues if exceptions are thrown from one of functions fed to Petsc
 * through a pointer.
 */
#define MOREFEM_TRAP_SNES_EXCEPTION_TRY try
#define MOREFEM_TRAP_SNES_EXCEPTION_CATCH(Snes)                                                                        \
    catch (const std::exception& e)                                                                                    \
    {                                                                                                                  \
        std::cerr << "Exception caught in the Petsc Snes definition: " << e.what() << std::endl;                       \
        return PETSC_ERR_MIN_VALUE;                                                                                    \
    }                                                                                                                  \
    MOREFEM_TRAP_SNES_ASSERTION_CATCH
#endif // MOREFEM_NO_TRAP_SNES_EXCEPTION


// ============================
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SNESMACRO_DOT_HPP_
// *** MoReFEM end header guards *** < //
