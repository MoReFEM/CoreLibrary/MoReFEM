// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_ADVANCED_CONCEPT_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/InputData/Concept.hpp" // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Advanced::Concept::InputDataNS
{


    /*!
     * \brief Defines a concept to identify a type is a section regarding PETSc settings.
     *
     * InputDataNS::Petsc which is defined in Core sub-library is expected to follow this concept.
     *
     */
    template<typename T>
    concept SolverSectionType = requires {
        { Advanced::Concept::InputDataNS::SectionType<T>, T::ConceptIsSolverSection };
    };

} // namespace MoReFEM::Advanced::Concept::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
