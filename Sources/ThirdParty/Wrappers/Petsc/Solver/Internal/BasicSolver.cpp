// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <string_view>
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/BasicSolver.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::Wrappers::Petsc { class Snes; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc
{


    BasicSolver::~BasicSolver() = default;


    BasicSolver::BasicSolver(Solver::solver_type type,
                             parallel_support is_parallel_supported,
                             SolverNS::Settings&& solver_settings,
                             std::string_view petsc_name)
    : Solver(type, is_parallel_supported, std::move(solver_settings)), petsc_name_{ petsc_name }
    { }


    void BasicSolver::SetSolveLinearOptions([[maybe_unused]] Snes& snes,
                                            [[maybe_unused]] const std::source_location location)
    { }

    void BasicSolver::SupplInitOptions([[maybe_unused]] Snes& snes,
                                       [[maybe_unused]] const std::source_location location)
    { }

    void BasicSolver::SupplPrintSolverInfos([[maybe_unused]] Snes& snes,
                                            [[maybe_unused]] const std::source_location location) const
    { }


    const std::string& BasicSolver::GetPetscName() const
    {
        return petsc_name_;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
