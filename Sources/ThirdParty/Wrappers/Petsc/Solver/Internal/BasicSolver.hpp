// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_BASICSOLVER_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_BASICSOLVER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>
#include <memory>
#include <source_location>
#include <string>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::Wrappers::Petsc { class Snes; } // IWYU pragma: keep
namespace MoReFEM::Wrappers { class Mpi; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc
{


    /*!
     * \brief Polymorphic base class over a solver used in Petsc with default settings.
     *
     * This abstract class defines all the virtual methods (save the one related to the destructor) to empty content.
     *
     */
    class BasicSolver : public Solver
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = BasicSolver;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] type An enum in MoReFEM which tells which kind of solver is considered (direct or
        //! iterative).
        //! \copydoc doxygen_hide_solver_settings_param
        //!
        //! \copydoc doxygen_hide_solver_is_parallel_supported
        //!
        //! \param[in] petsc_name Macro that holds the name of the solver in PETSc, e.g. 'MATSOLVERMUMPS'
        explicit BasicSolver(solver_type type,
                             parallel_support is_parallel_supported,
                             SolverNS::Settings&& solver_settings,
                             std::string_view petsc_name);

        //! Destructor.
        virtual ~BasicSolver() override = 0;

        //! \copydoc doxygen_hide_copy_constructor
        BasicSolver(const BasicSolver& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        BasicSolver(BasicSolver&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        BasicSolver& operator=(const BasicSolver& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        BasicSolver& operator=(BasicSolver&& rhs) = delete;

        ///@}

        //! Set the options that are specific to the solver in SolveLinear().
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_source_location
        virtual void
        SetSolveLinearOptions(Snes& snes,
                              const std::source_location location = std::source_location::current()) override;

        //! Set the options that are specific to the solver.
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_source_location
        virtual void SupplInitOptions(Snes& snes,
                                      const std::source_location location = std::source_location::current()) override;

        //! Print informtations after solve that are specific to the solver.
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_source_location
        virtual void
        SupplPrintSolverInfos(Snes& snes,
                              const std::source_location location = std::source_location::current()) const override;

        //! \copydoc doxygen_hide_petsc_solver_name
        virtual const std::string& GetPetscName() const override;

      private:
        //! Petsc name
        std::string petsc_name_;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_BASICSOLVER_DOT_HPP_
// *** MoReFEM end header guards *** < //
