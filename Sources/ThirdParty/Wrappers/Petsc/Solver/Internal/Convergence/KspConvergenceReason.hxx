// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_KSPCONVERGENCEREASON_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_KSPCONVERGENCEREASON_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{


    inline constexpr convergence_status KspConvergenceReason<KSP_CONVERGED_RTOL_NORMAL>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_CONVERGED_ATOL_NORMAL>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_CONVERGED_RTOL>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_CONVERGED_ATOL>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_CONVERGED_ITS>::GetConvergenceStatus() noexcept
    {
        return convergence_status::unspecified;
    }


#if PETSC_VERSION_LT(3, 19, 0)
    inline constexpr convergence_status
    KspConvergenceReason<KSP_CONVERGED_CG_NEG_CURVE>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status
    KspConvergenceReason<KSP_CONVERGED_CG_CONSTRAINED>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }
#endif


    inline constexpr convergence_status KspConvergenceReason<KSP_CONVERGED_STEP_LENGTH>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status
    KspConvergenceReason<KSP_CONVERGED_HAPPY_BREAKDOWN>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes; // at least I suppose...
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_DIVERGED_NULL>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_DIVERGED_ITS>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_DIVERGED_DTOL>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_DIVERGED_BREAKDOWN>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status
    KspConvergenceReason<KSP_DIVERGED_BREAKDOWN_BICG>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_DIVERGED_NONSYMMETRIC>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status
    KspConvergenceReason<KSP_DIVERGED_INDEFINITE_PC>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status KspConvergenceReason<KSP_DIVERGED_NANORINF>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status
    KspConvergenceReason<KSP_DIVERGED_INDEFINITE_MAT>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


#if PETSC_VERSION_LT(3, 11, 0)
    inline constexpr convergence_status
    KspConvergenceReason<KSP_DIVERGED_PCSETUP_FAILED>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }
#endif


#if PETSC_VERSION_GE(3, 11, 0)
    inline constexpr convergence_status KspConvergenceReason<KSP_DIVERGED_PC_FAILED>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }
#endif


    inline constexpr convergence_status KspConvergenceReason<KSP_CONVERGED_ITERATING>::GetConvergenceStatus() noexcept
    {
        return convergence_status::pending;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_KSPCONVERGENCEREASON_DOT_HXX_
// *** MoReFEM end header guards *** < //
