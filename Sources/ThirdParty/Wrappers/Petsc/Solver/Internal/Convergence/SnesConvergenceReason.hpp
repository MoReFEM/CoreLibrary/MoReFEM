// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_SNESCONVERGENCEREASON_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_SNESCONVERGENCEREASON_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp" // IWYU pragma: keep


namespace MoReFEM::Internal::Wrappers::Petsc
{


    //! \copydoc doxygen_hide_namespace_cluttering
    using convergence_status = ::MoReFEM::Wrappers::Petsc::convergence_status;


    /*!
     * \brief The purpose of this family of class is to provide two information about Snes: whether
     * it converged or not, and a small text that explain exactly the reason.
     *
     * This text might be very short: it is taken from Petsc documentation.
     *
     * Each specialization must provide the method:
     *
     * \code
     * static constexpr convergence_status GetConvergenceStatus() noexcept;
     * static const std::string& Explanation();
     * \endcode
     *
     */
    template<SNESConvergedReason ReasonT>
    struct SnesConvergenceReason;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    template<>
    struct SnesConvergenceReason<SNES_CONVERGED_FNORM_ABS>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_CONVERGED_FNORM_RELATIVE>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_CONVERGED_SNORM_RELATIVE>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_CONVERGED_ITS>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


#if PETSC_VERSION_LT(3, 12, 0)
    template<>
    struct SnesConvergenceReason<SNES_CONVERGED_TR_DELTA>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };
#endif


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_FUNCTION_DOMAIN>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_FUNCTION_COUNT>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_LINEAR_SOLVE>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_FNORM_NAN>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


#if PETSC_VERSION_GE(3, 8, 0)
    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_DTOL>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };
#endif // PETSC_VERSION_GE(3, 8, 0)


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_MAX_IT>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_LINE_SEARCH>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_INNER>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_LOCAL_MIN>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct SnesConvergenceReason<SNES_CONVERGED_ITERATING>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


#if PETSC_VERSION_GE(3, 11, 0)
    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_JACOBIAN_DOMAIN>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };
#endif // PETSC_VERSION_GE(3, 11, 0)


#if PETSC_VERSION_GE(3, 12, 0)
    template<>
    struct SnesConvergenceReason<SNES_DIVERGED_TR_DELTA>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };
#endif // PETSC_VERSION_GE(3, 11, 0)


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_SNESCONVERGENCEREASON_DOT_HPP_
// *** MoReFEM end header guards *** < //
