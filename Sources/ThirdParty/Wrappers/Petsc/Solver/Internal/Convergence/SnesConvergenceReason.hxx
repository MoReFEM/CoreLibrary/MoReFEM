// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_SNESCONVERGENCEREASON_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_SNESCONVERGENCEREASON_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{


    inline constexpr convergence_status SnesConvergenceReason<SNES_CONVERGED_FNORM_ABS>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status
    SnesConvergenceReason<SNES_CONVERGED_FNORM_RELATIVE>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status
    SnesConvergenceReason<SNES_CONVERGED_SNORM_RELATIVE>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }


    inline constexpr convergence_status SnesConvergenceReason<SNES_CONVERGED_ITS>::GetConvergenceStatus() noexcept
    {
        return convergence_status::unspecified;
    }


#if PETSC_VERSION_LT(3, 12, 0)
    inline constexpr convergence_status SnesConvergenceReason<SNES_CONVERGED_TR_DELTA>::GetConvergenceStatus() noexcept
    {
        return convergence_status::yes;
    }
#endif


    inline constexpr convergence_status
    SnesConvergenceReason<SNES_DIVERGED_FUNCTION_DOMAIN>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status
    SnesConvergenceReason<SNES_DIVERGED_FUNCTION_COUNT>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status
    SnesConvergenceReason<SNES_DIVERGED_LINEAR_SOLVE>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status SnesConvergenceReason<SNES_DIVERGED_FNORM_NAN>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status SnesConvergenceReason<SNES_DIVERGED_MAX_IT>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status
    SnesConvergenceReason<SNES_DIVERGED_LINE_SEARCH>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status SnesConvergenceReason<SNES_DIVERGED_INNER>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


#if PETSC_VERSION_GE(3, 8, 0)
    inline constexpr convergence_status SnesConvergenceReason<SNES_DIVERGED_DTOL>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }
#endif // PETSC_VERSION_GE(3, 8, 0)


    inline constexpr convergence_status SnesConvergenceReason<SNES_DIVERGED_LOCAL_MIN>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }


    inline constexpr convergence_status SnesConvergenceReason<SNES_CONVERGED_ITERATING>::GetConvergenceStatus() noexcept
    {
        return convergence_status::pending;
    }


#if PETSC_VERSION_GE(3, 11, 0)
    inline constexpr convergence_status
    SnesConvergenceReason<SNES_DIVERGED_JACOBIAN_DOMAIN>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }
#endif


#if PETSC_VERSION_GE(3, 12, 0)
    inline constexpr convergence_status SnesConvergenceReason<SNES_DIVERGED_TR_DELTA>::GetConvergenceStatus() noexcept
    {
        return convergence_status::no;
    }
#endif


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_SNESCONVERGENCEREASON_DOT_HXX_
// *** MoReFEM end header guards *** < //
