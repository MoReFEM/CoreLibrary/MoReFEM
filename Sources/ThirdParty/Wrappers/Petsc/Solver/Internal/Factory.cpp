// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <functional>
#include <map>
#include <string>
#include <utility>
#include <vector>

#include "Utilities/Exceptions/Factory.hpp"

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    Factory::~Factory() = default;


    const std::string& Factory::ClassName()
    {
        static const std::string ret("SolverFactory");
        return ret;
    }


    Factory::Factory() = default;


    Solver::unique_ptr Factory::Create(Settings&& solver_settings) const
    {
        const auto& solver_name_object = solver_settings.GetSolverName();
        const auto solver_name = solver_name_object.Get();

        auto it = callbacks_.find(solver_name);

        if (it == callbacks_.cend())
            throw ExceptionNS::Factory::UnregisteredName(solver_name, "solver");

        Solver::unique_ptr ret = it->second(std::move(solver_settings));

        if (ret == nullptr)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::SolverNotSetUp(solver_name_object);

        return ret;
    }


    namespace // anonymous
    {

        auto ReturnNullptr([[maybe_unused]] Internal::Wrappers::Petsc::SolverNS::Settings&& unused)
        {
            return nullptr;
        }

    } // namespace


    bool Factory::RegisterUnavailableSolver(const std::string& solver_name)
    {
        const CallBack::value_type item = std::make_pair(solver_name, ReturnNullptr);

        auto [iterator, is_inserted] = callbacks_.insert(item);

        if (!is_inserted)
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(solver_name, "solver"));

        return true;
    }


    std::vector<std::string> Factory::GenerateSolverList() const
    {
        std::vector<std::string> ret;
        ret.reserve(callbacks_.size());

        for (const auto& [name, callback_function] : callbacks_)
            ret.push_back(name);

        assert(ret.size() == callbacks_.size());

        return ret;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
