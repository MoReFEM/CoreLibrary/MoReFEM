// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_FACTORY_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_FACTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <functional>
#include <map>
#include <string>
#include <vector>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    /*!
     * \brief The purpose of this class is to create on demand a pointer to a newly created object
     * which type depends on the name given in a specific format.
     *
     * By convention:
     * - A solver is known by MoReFEM only if its exact name (case included) is present in the factory.
     * - If the solver is known by MoReFEM but not provided in your PETSc installation, the callback function
     * is `nullptr` rather than a function that creates a new \a Solver object owned by a unique pointer.
     *
     *
     */
    class Factory final : public Utilities::Singleton<Factory>
    {
      public:
        //! Alias to the function returned by the \a Create calls.
        using FunctionPrototype = std::function<Solver::unique_ptr(Settings&&)>;


        /*!
         * \brief Alias for call back.
         *
         * If the solver is not supported by your PETSc installation or not activated in MoReFEM configuration, the
         * value is by convention `nullptr`.
         *
         * \internal <b><tt>[internal]</tt></b> As very few variables are expected, a std::map is used rather
         * than a std::unordered_map. This choice might be questioned: I read once that for more than 3 keys
         * the has map is alreadyy more efficient...
         * \endinternal
         */
        using CallBack = std::map<std::string, FunctionPrototype>;


        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();


      public:
        /*!
         * \brief Register a solver..
         *
         * \tparam SolverT Solver to register; SolverT::Name() is expected to return a name
         * which will be used as a key in the internal storage.
         * \param[in] function New function to register that should be able to create a \a SolverT object.
         * Choose a \a FunctionPrototype that returns nullptr if \a SolverT is not supported by your PETSc installation
         * (some solvers such as Mumps or Umfpack need to be  installed specifically in the PETSc configurations
         * script).
         *
         * \return Always true - return value is a trick to be able to call such a method directly in an anonymous namespace.
         */
        template<class SolverT>
        bool Register(FunctionPrototype function);

        /*!
         * \brief Register that a solver is known generally to MoReFEM... but is not known in current local settings.
         *
         * This overload may typically used for an external dependency in PETSc (such as Mumps or SuperLU_dist)
         * which might or might not have been installed along with PETSc. If not installed, or if you chose not to
         * activate it in the build script to configure MoReFEM installation, the solver can't be used but we
         * nonetheless want it registered in the factory so that a graceful exception may be thrown to explain to the
         * end-user why it can't run his or her model with this solver.
         *
         * \param solver_name Name of the solver to be added to the factory.
         *
         * \return Always true - return value is a trick to be able to call such a method directly in an anonymous namespace.
         */
        bool RegisterUnavailableSolver(const std::string& solver_name);


        /*!
         * \brief Create a new \a Solver object.
         *
         * \copydoc doxygen_hide_solver_settings_param

         * The name of the solver to use is extracted from \a settings. If invalid, it is up to \a Factory
         * to handle it - \a Settings object doesn't perform such check itself.
         *
         * \return Smart pointer to the created solver object.
         */
        Solver::unique_ptr Create(Settings&& solver_settings) const;

        //! Number of elements registered in the factory.
        inline CallBack::size_type Nvariable() const;

        //! Returns the list of all solvers supported.
        std::vector<std::string> GenerateSolverList() const;


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        Factory();

        //! Destructor.
        virtual ~Factory() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<Factory>;
        ///@}


      private:
        /*!
         * \brief Associative container to choose the right function given its string identifier.
         *
         *
         */
        CallBack callbacks_;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_FACTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
