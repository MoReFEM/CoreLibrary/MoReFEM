// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_FACTORY_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_FACTORY_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
// *** MoReFEM header guards *** < //


#include <utility>

#include "Utilities/Exceptions/Factory.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    template<class SolverT>
    bool Factory::Register(FunctionPrototype callback)
    {
        if (!callbacks_.insert(std::make_pair(SolverT::Name(), callback)).second)
        {
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(SolverT::Name(), "solver"));
        }

        return true;
    }


    inline auto Factory::Nvariable() const -> CallBack::size_type
    {
        return callbacks_.size();
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_FACTORY_DOT_HXX_
// *** MoReFEM end header guards *** < //
