// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <source_location>
#include <string> // IWYU pragma: keep
#include <utility>

// IWYU pragma: no_include <iosfwd>
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/Gmres.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: keep


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<Gmres>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        // NOLINTBEGIN(cert-err58-cpp)
        // We accept here exception might occur and not be caught - it is both very unlikely
        // and would happen anyway before we enter the main() program if it happens nonetheless.
        [[maybe_unused]] const bool registered = SolverNS::Factory::CreateOrGetInstance().Register<Gmres>(Create);
        // NOLINTEND(cert-err58-cpp)

    } // namespace


    const std::string& Gmres::Name()
    {
        static const std::string ret("Gmres");
        return ret;
    }


    Gmres::Gmres(SolverNS::Settings&& solver_settings)
    : parent(solver_type::iterative, parallel_support::yes, std::move(solver_settings))
    { }


    void Gmres::SetSolveLinearOptions([[maybe_unused]] Snes& snes, [[maybe_unused]] const std::source_location location)
    { }


    void Gmres::SupplInitOptions([[maybe_unused]] Snes& snes, [[maybe_unused]] const std::source_location location)
    {
        auto* ksp = snes.GetKsp(location);

        auto error_code = KSPGMRESSetRestart(ksp, GetRestart());

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "KSPGMRESSetRestart", location);
    }


    void Gmres::SupplPrintSolverInfos([[maybe_unused]] Snes& snes,
                                      [[maybe_unused]] const std::source_location location) const
    { }


    const std::string& Gmres::GetPetscName() const
    {
        static const std::string ret(KSPGMRES);
        return ret;
    }


    PetscInt Gmres::GetRestart() const noexcept
    {
        return static_cast<PetscInt>(GetSettings().GetRestart().Get());
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
