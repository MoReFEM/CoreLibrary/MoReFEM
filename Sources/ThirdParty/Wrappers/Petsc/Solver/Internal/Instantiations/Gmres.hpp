// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_GMRES_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_GMRES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <source_location>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    /*!
     * \brief Wrappers over Gmres solver within Petsc.
     */
    class Gmres final : public Internal::Wrappers::Petsc::Solver
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Gmres;

        //! Alias to parent.
        using parent = Internal::Wrappers::Petsc::Solver;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Class name
        static const std::string& Name();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \copydoc doxygen_hide_solver_settings_param
        explicit Gmres(SolverNS::Settings&& solver_settings);

        //! Destructor.
        ~Gmres() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        Gmres(const Gmres& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Gmres(Gmres&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Gmres& operator=(const Gmres& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Gmres& operator=(Gmres&& rhs) = delete;

        ///@}

      private:
        /*!
         * \copydoc doxygen_hide_solver_set_solve_linear_option
         *
         * Currently nothing is done at this stage for Gmres solver.
         */
        void SetSolveLinearOptions(Snes& snes,
                                   const std::source_location location = std::source_location::current()) override;

        /*!
         * \copydoc doxygen_hide_solver_suppl_init_option
         */
        void SupplInitOptions(Snes& snes,
                              const std::source_location location = std::source_location::current()) override;


        /*!
         * \copydoc doxygen_hide_solver_print_infos
         *
         * Currently nothing is done at this stage for Gmres solver.
         */
        void
        SupplPrintSolverInfos(Snes& snes,
                              const std::source_location location = std::source_location::current()) const override;

        //! \copydoc doxygen_hide_petsc_solver_name
        const std::string& GetPetscName() const override;

      private:
        //! Restart.
        PetscInt GetRestart() const noexcept;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_GMRES_DOT_HPP_
// *** MoReFEM end header guards *** < //
