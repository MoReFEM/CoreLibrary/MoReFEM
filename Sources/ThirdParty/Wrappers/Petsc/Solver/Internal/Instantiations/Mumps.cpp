// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <memory>
#include <source_location>
#include <sstream>
#include <string> // IWYU pragma: keep
#include <string_view>
#include <utility>
#include <vector>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/Mumps.hpp"

#include "Utilities/Containers/Print.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: keep

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {


        std::string MumpsName()
        {
            return "Mumps";
        }

        // NOLINTBEGIN(cert-err58-cpp)
        // We accept here exception might occur and not be caught - it is both very unlikely
        // and would happen anyway before we enter the main() program if it happens nonetheless.

#if not defined(MOREFEM_WITH_MUMPS)

        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        [[maybe_unused]] const bool registered =
            SolverNS::Factory::CreateOrGetInstance().RegisterUnavailableSolver(MumpsName());

#else // if not defined (MOREFEM_WITH_MUMPS)

#if not PetscDefined(HAVE_MUMPS)
        static_assert(
            false,
            "If MOREFEM_WITH_MUMPS is set to true, PETSc should have been installed with this solver configured!"
            " (typically with an option such as --download-mumps)");
#endif

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<Mumps>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        [[maybe_unused]] const bool registered = SolverNS::Factory::CreateOrGetInstance().Register<Mumps>(Create);

#endif // if not defined (MOREFEM_WITH_MUMPS)
       // NOLINTEND(cert-err58-cpp)

    } // namespace


#ifdef MOREFEM_WITH_MUMPS

    const std::string& Mumps::Name()
    {
        static const std::string ret{ MumpsName() };
        return ret;
    }


    Mumps::Mumps(SolverNS::Settings&& solver_settings)
    : parent(solver_type::direct, parallel_support::yes, std::move(solver_settings))
    {
#if defined(__apple_build_version__)
        std::cerr << "\n===============================================================================\n";
        std::cerr << "[WARNING] Since PETSc 3.16, Mumps may lead to a crash in parallel mode for macOS"
                     "(see #1705 for more details). You should consider using SuperLU_dist instead!"
                  << std::endl;
        std::cerr << "================================================================================\n";
#endif // APPLE
    }


    void Mumps::SetSolveLinearOptions([[maybe_unused]] Snes& snes, [[maybe_unused]] const std::source_location location)
    {
        auto* pc = snes.GetPreconditioner(location);

#if PETSC_VERSION_GE(3, 9, 0)
        auto error_code = PCFactorSetUpMatSolverType(pc); /* call MatGetFactor() to create F */
#else
        auto error_code = PCFactorSetUpMatSolverPackage(pc); /* call MatGetFactor() to create F */
#endif

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(
                error_code, "PCFactorSetUpMatSolverType", location);

        Mat F = nullptr;
        error_code = PCFactorGetMatrix(pc, &F);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "PCFactorGetMatrix", location);

        // Here are few values that were recommended; for others default valuea are used.
        // I have not thoroughly investigated what is best here; it could be nice at some point to
        // make publicly available in the input data file those that could gain to be customized for
        // a model.
        const std::vector<std::pair<PetscInt, PetscInt>> customized_icntl{
            { 4, 0 },  // level of printing in 0-4
            { 7, 3 },  // computes a symmetric permutation (ordering) to determine the pivot order to be used
                       // for the factorization in case of sequential analysis (ICNTL(28)=1).
                       // 3 is for Scotch \todo #1015
            { 29, 1 }, // 1 is for PT-Scotch
            { 24, 1 }, // detection of null pivot rows
            { 33, 1 }, // compute determinant of A
        };

        for (const auto& pair : customized_icntl)
        {
            error_code = MatMumpsSetIcntl(F, pair.first, pair.second);
            if (error_code)
            {
                std::ostringstream oconv;
                oconv << "MatMumpsSetIcntl with pair ";
                Utilities::PrintTuple(pair,
                                      oconv,
                                      ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                      ::MoReFEM::PrintNS::Delimiter::opener("("),
                                      ::MoReFEM::PrintNS::Delimiter::closer(")"));

                throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, oconv.str(), location);
            }
        }

        // Same for real values.
        const std::vector<std::pair<PetscInt, PetscScalar>> customized_cntl{ { 3, 1.e-6 } };


        for (const auto& pair : customized_cntl)
        {
            error_code = MatMumpsSetCntl(F, pair.first, pair.second);
            if (error_code)
            {
                std::ostringstream oconv;
                oconv << "MatMumpsSetCntl with pair ";
                Utilities::PrintTuple(pair,
                                      oconv,
                                      ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                      ::MoReFEM::PrintNS::Delimiter::opener("("),
                                      ::MoReFEM::PrintNS::Delimiter::closer(")"));

                throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, oconv.str(), location);
            }
        }
    }


    void Mumps::SupplInitOptions([[maybe_unused]] Snes& snes, [[maybe_unused]] const std::source_location location)
    { }


    void Mumps::SupplPrintSolverInfos([[maybe_unused]] Snes& snes,
                                      [[maybe_unused]] const std::source_location location) const
    {
        auto* pc = snes.GetPreconditioner(location);

        Mat F = nullptr;
        int error_code = PCFactorGetMatrix(pc, &F);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "PCFactorGetMatrix", location);

        PetscInt ival = 0;
        error_code = MatMumpsGetInfog(F, 28, &ival); // 28 is the index of the wanted info
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatMumpsGetInfog", location);

        ::MoReFEM::Wrappers::Petsc::PrintMessageOnFirstProcessor(
            "After factorization: number of null pivots encountered: %d.\n",
            snes.GetMpi(),
            location,
            static_cast<int>(ival));
    }


    const std::string& Mumps::GetPetscName() const
    {
        static const std::string ret(MATSOLVERMUMPS);
        return ret;
    }

#endif // ifdef MOREFEM_WITH_MUMPS


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
