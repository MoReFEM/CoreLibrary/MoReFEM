// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_PETSC_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_PETSC_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/BasicSolver.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS { class Settings; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    /*!
     * \brief Wrappers over Petsc solver within Petsc.
     */
    class Petsc final : public Internal::Wrappers::Petsc::BasicSolver
    {

      public:
        //! Alias to parent.
        using parent = Internal::Wrappers::Petsc::BasicSolver;

        //! \copydoc doxygen_hide_alias_self
        using self = Petsc;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Class name
        static const std::string& Name();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \copydoc doxygen_hide_solver_settings_param
        explicit Petsc(SolverNS::Settings&& solver_settings);

        //! Destructor.
        ~Petsc() override;

        //! \copydoc doxygen_hide_copy_constructor
        Petsc(const Petsc& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Petsc(Petsc&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Petsc& operator=(const Petsc& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Petsc& operator=(Petsc&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_PETSC_DOT_HPP_
// *** MoReFEM end header guards *** < //
