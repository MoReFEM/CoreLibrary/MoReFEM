// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <memory>
#include <source_location>
#include <string> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/SuperLU_dist.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {


        std::string SuperLU_distName()
        {
            return "SuperLU_dist";
        }

        // NOLINTBEGIN(cert-err58-cpp)
        // We accept here exception might occur and not be caught - it is both very unlikely
        // and would happen anyway before we enter the main() program if it happens nonetheless.

#if not defined(MOREFEM_WITH_SUPERLU_DIST)

        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        [[maybe_unused]] const bool registered =
            SolverNS::Factory::CreateOrGetInstance().RegisterUnavailableSolver(SuperLU_distName());

#else // if not defined (MOREFEM_WITH_SUPERLU_DIST)

#if not PetscDefined(HAVE_SUPERLU_DIST)
        static_assert(
            false,
            "If MOREFEM_WITH_SUPERLU_DIST is set to true, PETSc should have been installed with this solver configured!"
            " (typically with an option such as --download-superlu_dist)");
#endif

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<SuperLU_dist>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        [[maybe_unused]] const bool registered =
            SolverNS::Factory::CreateOrGetInstance().Register<SuperLU_dist>(Create);

#endif // if not defined (MOREFEM_WITH_SUPERLU_DIST)
       // NOLINTEND(cert-err58-cpp)

    } // namespace

#ifdef MOREFEM_WITH_SUPERLU_DIST

    const std::string& SuperLU_dist::Name()
    {
        static const std::string ret{ SuperLU_distName() };
        return ret;
    }


    SuperLU_dist::SuperLU_dist(SolverNS::Settings&& solver_settings)
    : parent(solver_type::direct, parallel_support::yes, std::move(solver_settings))
    { }


    void SuperLU_dist::SetSolveLinearOptions([[maybe_unused]] Snes& snes,
                                             [[maybe_unused]] const std::source_location location)
    { }


    void SuperLU_dist::SupplInitOptions([[maybe_unused]] Snes& snes,
                                        [[maybe_unused]] const std::source_location location)
    { }


    void SuperLU_dist::SupplPrintSolverInfos([[maybe_unused]] Snes& snes,
                                             [[maybe_unused]] const std::source_location location) const
    { }


    const std::string& SuperLU_dist::GetPetscName() const
    {
        static const std::string ret(MATSOLVERSUPERLU_DIST);
        return ret;
    }

#endif // MOREFEM_WITH_SUPERLU_DIST

} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
