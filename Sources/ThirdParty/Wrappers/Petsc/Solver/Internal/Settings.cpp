// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <utility>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    Settings::~Settings() = default;


    Settings::Settings(::MoReFEM::Wrappers::Petsc::absolute_tolerance_type absolute_tolerance,
                       ::MoReFEM::Wrappers::Petsc::relative_tolerance_type relative_tolerance,
                       ::MoReFEM::Wrappers::Petsc::set_restart_type set_restart,
                       ::MoReFEM::Wrappers::Petsc::max_iteration_type max_iteration,
                       ::MoReFEM::Wrappers::Petsc::preconditioner_name_type preconditioner_name,
                       ::MoReFEM::Wrappers::Petsc::solver_name_type solver_name,
                       ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type step_size_tolerance)
    : absolute_tolerance_(absolute_tolerance), relative_tolerance_(relative_tolerance), set_restart_(set_restart),
      max_iteration_(max_iteration), preconditioner_name_(std::move(preconditioner_name)),
      solver_name_(std::move(solver_name)), step_size_tolerance_(step_size_tolerance)
    { }


    Settings::Settings(::MoReFEM::Wrappers::Petsc::solver_name_type solver_name,
                       ::MoReFEM::Wrappers::Petsc::preconditioner_name_type preconditioner_name)
    : preconditioner_name_(std::move(preconditioner_name)), solver_name_(std::move(solver_name))
    { }

} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
