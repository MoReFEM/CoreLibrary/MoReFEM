// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_SETTINGS_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_SETTINGS_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Advanced/Concept.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    template<Advanced::Concept::InputDataNS::SolverSectionType SectionT>
    Settings::Settings(const SectionT& section)
    : Settings(::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename SectionT::AbsoluteTolerance>(section),
               ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename SectionT::RelativeTolerance>(section),
               ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename SectionT::GmresRestart>(section),
               ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename SectionT::MaxIteration>(section),
               ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename SectionT::Preconditioner>(section),
               ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename SectionT::Solver>(section),
               ::MoReFEM::Internal::InputDataNS::ExtractLeafFromSection<typename SectionT::StepSizeTolerance>(section))
    { }

    inline ::MoReFEM::Wrappers::Petsc::absolute_tolerance_type Settings::GetAbsoluteTolerance() const noexcept
    {
        return absolute_tolerance_;
    }


    inline ::MoReFEM::Wrappers::Petsc::relative_tolerance_type Settings::GetRelativeTolerance() const noexcept
    {
        return relative_tolerance_;
    }


    inline ::MoReFEM::Wrappers::Petsc::set_restart_type Settings::GetRestart() const noexcept
    {
        return set_restart_;
    }


    inline ::MoReFEM::Wrappers::Petsc::max_iteration_type Settings::GetMaxIterations() const noexcept
    {
        return max_iteration_;
    }


    inline ::MoReFEM::Wrappers::Petsc::preconditioner_name_type Settings::GetPreconditionerName() const noexcept
    {
        return preconditioner_name_;
    }


    inline ::MoReFEM::Wrappers::Petsc::solver_name_type Settings::GetSolverName() const noexcept
    {
        return solver_name_;
    }


    inline ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type Settings::GetStepSizeTolerance() const noexcept
    {
        return step_size_tolerance_;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_SETTINGS_DOT_HXX_
// *** MoReFEM end header guards *** < //
