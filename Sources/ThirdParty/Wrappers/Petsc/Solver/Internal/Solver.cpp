// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <utility>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{


    Solver::~Solver() = default;


    Solver::Solver(solver_type type, parallel_support is_parallel_supported, SolverNS::Settings&& solver_settings)
    : type_(type), is_parallel_supported_(is_parallel_supported), settings_(std::move(solver_settings))
    { }


    auto Solver::GetSolverType() const noexcept -> solver_type
    {
        return type_;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
