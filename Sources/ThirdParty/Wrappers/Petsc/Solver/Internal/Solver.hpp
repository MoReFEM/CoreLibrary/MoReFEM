// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_SOLVER_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_SOLVER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <source_location>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Petsc { enum class solver_type; }
namespace MoReFEM::Wrappers::Petsc { class Snes; } // IWYU pragma: keep
namespace MoReFEM::Wrappers { class Mpi; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc
{


    //! Convenient enum class to tell whether a solver support parallel linear algebra or not.
    //! (most PETSc solvers don't)
    enum class parallel_support { yes, no };

    /*!
     * \class doxygen_hide_solver_is_parallel_supported
     *
     * \param[in] is_parallel_supported If 'yes', it means this solver may be run in parallel.
     * If 'no', an attempt to use it in a parallel run will fail more gracefully than a direct PETSc error.
     */


    /*!
     * \brief Polymorphic base class over a solver used in Petsc.
     *
     * Each solver is not set the same way with the same function calls, hence the need for such a class.
     */
    class Solver
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Solver;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Convenient alias.
        using solver_type = ::MoReFEM::Wrappers::Petsc::solver_type;

      protected:
        //! Convenient alias to avoid repeating namespaces.
        using Snes = ::MoReFEM::Wrappers::Petsc::Snes;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] type An enum in MoReFEM which tells which kind of solver is considered (direct or
        //! iterative).
        //! \copydoc doxygen_hide_solver_settings_param
        //!
        //! \copydoc doxygen_hide_solver_is_parallel_supported
        //! Check about whether parallel is supported is postponed when \a Snes object is created.
        explicit Solver(solver_type type, parallel_support is_parallel_supported, SolverNS::Settings&& solver_settings);

        //! Destructor.
        virtual ~Solver();

        //! \copydoc doxygen_hide_copy_constructor
        Solver(const Solver& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Solver(Solver&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Solver& operator=(const Solver& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Solver& operator=(Solver&& rhs) = delete;

        ///@}

        //! Set the options that are specific to the solver in SolveLinear().
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_source_location
        virtual void SetSolveLinearOptions(Snes& snes,
                                           const std::source_location location = std::source_location::current()) = 0;

        //! Set the options that are specific to the solver.
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_source_location
        virtual void SupplInitOptions(Snes& snes,
                                      const std::source_location location = std::source_location::current()) = 0;

        //! Print informtations after solve that are specific to the solver.
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_source_location
        virtual void
        SupplPrintSolverInfos(Snes& snes,
                              const std::source_location location = std::source_location::current()) const = 0;

        //! \copydoc doxygen_hide_petsc_solver_name
        virtual const std::string& GetPetscName() const = 0;

        //! Get the type of solver.
        solver_type GetSolverType() const noexcept;

        //! Accessor to solver settings.
        const SolverNS::Settings& GetSettings() const noexcept;

        //! Tells whether the solver supports parallel linear algebra or not.
        bool IsParallel() const noexcept;

      private:
        //! Type of solver.
        const solver_type type_;

        //! Whether the solver supports parallel linear algebra or not.
        const parallel_support is_parallel_supported_;

        //! Solver settings.
        SolverNS::Settings settings_;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_SOLVER_DOT_HPP_
// *** MoReFEM end header guards *** < //
