// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory> // IWYU pragma: keep
#include <source_location>
#include <sstream>
#include <string>
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    Snes::Snes(const Mpi& mpi,
               Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings,
               SNESFunction snes_function,
               SNESJacobian snes_jacobian,
               SNESViewer snes_viewer,
               SNESConvergenceTestFunction snes_convergence_test_function,
               const std::source_location location)
    : mpi_(mpi), snes_function_(snes_function), snes_jacobian_(snes_jacobian), snes_viewer_(snes_viewer),
      snes_convergence_test_function_(snes_convergence_test_function)
    {
        const auto solver_name = solver_settings.GetSolverName();
        const auto preconditioner_name = solver_settings.GetPreconditionerName();
        const auto relative_tolerance = solver_settings.GetRelativeTolerance();
        const auto absolute_tolerance = solver_settings.GetAbsoluteTolerance();
        const auto max_iteration = solver_settings.GetMaxIterations();
        const auto step_size_tolerance = solver_settings.GetStepSizeTolerance();

        decltype(auto) solver_factory = Internal::Wrappers::Petsc::SolverNS::Factory::CreateOrGetInstance(location);

        solver_ = solver_factory.Create(std::move(solver_settings));

        const auto& solver = GetSolver();

        if ((mpi.Nprocessor<int>() > 1) && (!solver.IsParallel()))
            throw ExceptionNS::SolverNotParallel(solver.GetPetscName(), location);

        auto* communicator = mpi.GetCommunicator();

        // By extracting the  KSP, KSP, and PC contexts from the SNES context, we can then
        // directly call any KSP, KSP, and PC routines to set various options.
        int error_code = SNESCreate(communicator, &snes_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESCreate", location);

        auto* ksp = GetKsp(location);

        error_code = SNESSetFromOptions(snes_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESSetFromOptions", location);

        // If the solver is a direct one, type must be 'preonly' and preconditioner 'lu'; actual solver
        // used is determined by PCFactorSetMatSolverPackage().
        if (solver.GetSolverType() == solver_type::direct)
        {
            error_code = KSPSetType(ksp, KSPPREONLY);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "KSPSetType", location);

            if (preconditioner_name.Get() != PCLU)
            {
                std::ostringstream oconv;
                oconv << "Error: for a direct solver preconditioner must be " << PCLU << '.';

                throw Exception(oconv.str(), location);
            }

            auto* pc = GetPreconditioner(location);

            error_code = PCSetType(pc, preconditioner_name.Get().c_str());
            if (error_code)
                throw ExceptionNS::Exception(error_code, "PCSetType", location);

#if PETSC_VERSION_GE(3, 9, 0)
            error_code = PCFactorSetMatSolverType(pc, solver.GetPetscName().c_str());
#else
            error_code = PCFactorSetMatSolverPackage(pc, solver.GetPetscName().c_str());
#endif

            if (error_code)
                throw ExceptionNS::Exception(error_code, "PCFactorSetMatSolverType", location);
        } else
        {
            error_code = KSPSetType(ksp, solver.GetPetscName().c_str());
            if (error_code)
                throw ExceptionNS::Exception(error_code, "KSPSetType", location);


            auto* pc = GetPreconditioner(location);

            error_code = PCSetType(pc, preconditioner_name.Get().c_str());
            if (error_code)
                throw ExceptionNS::Exception(error_code, "PCSetType", location);
        }


        GetNonCstSolver().SupplInitOptions(*this, location);

        error_code = KSPSetTolerances(
            ksp, relative_tolerance.Get(), absolute_tolerance.Get(), PETSC_DEFAULT, max_iteration.Get());
        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPSetTolerances", location);

        error_code = SNESSetTolerances(snes_,
                                       absolute_tolerance.Get(),
                                       relative_tolerance.Get(),
                                       step_size_tolerance.Get(),
                                       max_iteration.Get(),
                                       PETSC_DEFAULT);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESSetTolerances", location);

        auto* pc = GetPreconditioner(location);
        error_code = PCFactorSetReuseFill(pc, PETSC_FALSE);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PCFactorSetReuseFill", location);
    }


    Snes::~Snes()
    {
        [[maybe_unused]] const int error_code = SNESDestroy(&snes_);
        assert(error_code == 0); // Can't throw exception in a destructor.
    }


    void Snes::SolveNonLinear(void* context,
                              const Vector& rhs,
                              const Matrix& jacobian_matrix,
                              const Matrix& preconditioner_matrix,
                              Vector& solution,
                              check_convergence do_check_convergence,
                              const std::source_location location)
    {
        int error_code = SNESSetFunction(snes_, rhs.InternalForReadOnly(location), GetSnesFunction(), context);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESSetFunction", location);

        error_code = SNESSetJacobian(snes_,
                                     jacobian_matrix.InternalForReadOnly(location),
                                     preconditioner_matrix.InternalForReadOnly(location),
                                     GetSnesJacobian(),
                                     context);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESSetJacobian", location);

        if (GetSnesConvergenceTestFunction() != nullptr)
        {
            error_code = SNESSetConvergenceTest(snes_, GetSnesConvergenceTestFunction(), context, nullptr);

            if (error_code)
                throw ExceptionNS::Exception(error_code, "SNESSetConvergenceTest", location);
        }

        error_code = SNESMonitorSet(snes_, GetSnesViewer(), context, MOREFEM_PETSC_NULL);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESMonitorSet", location);

        error_code = SNESSolve(snes_, nullptr, solution.Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESSolve", location);

        if (do_check_convergence == check_convergence::yes)
        {
            const auto convergence = GetNonLinearConvergenceReason(location);

            switch (convergence)
            {
            case convergence_status::yes:
            case convergence_status::unspecified:
                break;
            case convergence_status::no:
            {
                throw Exception(std::string("Newton solve didn't converge due to: ") + *convergence_reason_, location);
            }
            case convergence_status::pending:
            {
                assert(false && "Should never happen: this call is explicitly placed after SNESSolve is done!");
                exit(EXIT_FAILURE);
            }
            }
        }
    }


    KSP Snes::GetKsp(const std::source_location location) const
    {
        KSP ksp = nullptr;
        const int error_code = SNESGetKSP(snes_, &ksp);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESGetKSP", location);
        return ksp;
    }


    PC Snes::GetPreconditioner(const std::source_location location) const
    {
        PC preconditioner = nullptr;

        auto* ksp = GetKsp(location);

        // NOTE: There is a Petsc a SNESGetPC but weirdly enough it returns a SNES object rather than a PC one.
        // KSPGetPC on the other hand returns correctly the expected type.
        const int error_code = KSPGetPC(ksp, &preconditioner);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPGetPC", location);

        return preconditioner;
    }


    void Snes::SolveLinear(const Vector& rhs,
                           Vector& solution,
                           print_solver_infos do_print_solver_infos,
                           const std::source_location location)
    {
        KSP ksp = GetKsp(location);

        /*if (do_print_solver_infos == print_solver_infos::yes)
        {
            KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);
        }*/

        int error_code = ::KSPSolve(ksp, rhs.InternalForReadOnly(location), solution.Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPSolve", location);
        const auto convergence = GetLinearConvergenceReason(location);

        switch (convergence)
        {
        case convergence_status::yes:
        case convergence_status::unspecified:
            break;
        case convergence_status::no:
        {
            throw Exception(std::string("Linear solve didn't converge due to: ") + *convergence_reason_, location);
        }
        case convergence_status::pending:
        {
            assert(false && "Should never happen: this call is explicitly placed after KSPSolve is done!");
            exit(EXIT_FAILURE);
        }
        }

        if (do_print_solver_infos == print_solver_infos::yes)
        {
            PetscInt Niteration = 0;
            error_code = KSPGetIterationNumber(ksp, &Niteration);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "KSPGetIterationNumber", location);

            // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
            PetscReal residual;
            error_code = KSPGetResidualNorm(ksp, &residual);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "KSPGetResidualNorm", location);

            PrintMessageOnFirstProcessor("%s-%s converged in %d iterations (residual = %e)\n",
                                         GetMpi(),
                                         location,
                                         GetKspType(location).c_str(),
                                         GetPreconditionerType(location).c_str(),
                                         static_cast<int>(Niteration),
                                         static_cast<double>(residual));

            GetSolver().SupplPrintSolverInfos(*this, location);
        }
    }


    std::size_t Snes::GetSnesIteration(const std::source_location location) const
    {
        PetscInt value = 0;
        const int error_code = SNESGetLinearSolveIterations(snes_, &value);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESGetLinearSolveIterations", location);

        return static_cast<std::size_t>(value);
    }


    const Mpi& Snes::GetMpi() const
    {
        return mpi_;
    }


    std::string Snes::GetKspType(const std::source_location location) const
    {
        auto* ksp = GetKsp(location);

        KSPType type = nullptr;
        const int error_code = KSPGetType(ksp, &type);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPGetType", location);

        return { type };
    }


    std::string Snes::GetPreconditionerType(const std::source_location location) const
    {
        auto* ksp = GetPreconditioner(location);

        PCType type = nullptr;
        const int error_code = PCGetType(ksp, &type);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PCGetType", location);

        return { type };
    }


    std::string Snes::GetSnesType(const std::source_location location) const
    {
        SNESType type = nullptr;
        const int error_code = SNESGetType(snes_, &type);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "GetSnesType", location);

        return { type };
    }


    convergence_status Snes::GetNonLinearConvergenceReason(const std::source_location location) const
    {
        SNESConvergedReason reason{};

        const int error_code = SNESGetConvergedReason(snes_, &reason);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESGetConvergedReason", location);

        switch (reason)
        {
        case SNES_CONVERGED_FNORM_ABS:
            return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_FNORM_ABS>();
        case SNES_CONVERGED_FNORM_RELATIVE:
            return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_FNORM_RELATIVE>();
        case SNES_CONVERGED_SNORM_RELATIVE:
            return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_SNORM_RELATIVE>();
        case SNES_CONVERGED_ITS:
            return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_ITS>();
#if PETSC_VERSION_LT(3, 12, 0)
        case SNES_CONVERGED_TR_DELTA:
            return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_TR_DELTA>();
#endif
        case SNES_DIVERGED_FUNCTION_DOMAIN:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_FUNCTION_DOMAIN>();
        case SNES_DIVERGED_FUNCTION_COUNT:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_FUNCTION_COUNT>();
        case SNES_DIVERGED_LINEAR_SOLVE:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_LINEAR_SOLVE>();
        case SNES_DIVERGED_FNORM_NAN:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_FNORM_NAN>();
        case SNES_DIVERGED_MAX_IT:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_MAX_IT>();
        case SNES_DIVERGED_LINE_SEARCH:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_LINE_SEARCH>();
        case SNES_DIVERGED_INNER:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_INNER>();
        case SNES_DIVERGED_LOCAL_MIN:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_LOCAL_MIN>();
        case SNES_CONVERGED_ITERATING:
            return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_ITERATING>();
#if PETSC_VERSION_GE(3, 8, 0)
        case SNES_DIVERGED_DTOL:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_DTOL>();
#endif // PETSC_VERSION_GE(3, 8, 0)
#if PETSC_VERSION_GE(3, 11, 0)
        case SNES_DIVERGED_JACOBIAN_DOMAIN:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_JACOBIAN_DOMAIN>();
#endif // PETSC_VERSION_GE(3, 11, 0)
#if PETSC_VERSION_GE(3, 12, 0)
        case SNES_DIVERGED_TR_DELTA:
            return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_TR_DELTA>();
#endif // PETSC_VERSION_GE(3, 12, 0)
#if PETSC_VERSION_GE(3, 17, 0)
        case SNES_BREAKOUT_INNER_ITER:
            assert(false && "Not handled (never used so far with the library");
            exit(EXIT_FAILURE);
#endif // PETSC_VERSION_GE(3, 17, 0)


            // You might have to add or remove an entry after updating Petsc version; look after the warnings
            // about missing switch entry!
        }

        assert(false && "Make sure all entries are properly covered!");
        exit(EXIT_FAILURE);
    }


    convergence_status Snes::GetNonLinearConvergenceReason(std::string& explanation,
                                                           const std::source_location location) const
    {
        const auto ret = GetNonLinearConvergenceReason(location);
        explanation = *convergence_reason_;
        return ret;
    }


    convergence_status Snes::GetLinearConvergenceReason(const std::source_location location) const
    {
        KSPConvergedReason reason{};

        const int error_code = KSPGetConvergedReason(GetKsp(location), &reason);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPGetConvergedReason", location);

        switch (reason)
        {
        case KSP_CONVERGED_RTOL_NORMAL:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_RTOL_NORMAL>();
        case KSP_CONVERGED_ATOL_NORMAL:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ATOL_NORMAL>();
        case KSP_CONVERGED_RTOL:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_RTOL>();
        case KSP_CONVERGED_ATOL:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ATOL>();
        case KSP_CONVERGED_ITS:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ITS>();

#if PETSC_VERSION_GE(3, 19, 0)
            PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated-declarations.hpp" // IWYU pragma: keep
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated.hpp"              // IWYU pragma: keep
#endif
        case KSP_CONVERGED_CG_NEG_CURVE:
            assert(false && "This deprecated case (in PETSc 3.19) shouldn't occur");
            return convergence_status::unspecified; // deprecated, but I'd rather let it for
                                                    // the time being than neutralizing the -Wswitch value which is
                                                    // triggered as the value is still present in PETSc.
#if PETSC_VERSION_LT(3, 19, 0)
        // If PETSc 3.19 same underlying value as KSP_CONVERGED_STEP_LENGTH
        // Warning message makes it clear this is KSP_CONVERGED_CG_CONSTRAINED which is deprecated
        case KSP_CONVERGED_CG_CONSTRAINED:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_CG_CONSTRAINED>();
#endif
        case KSP_CONVERGED_STEP_LENGTH:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_STEP_LENGTH>();
        case KSP_CONVERGED_HAPPY_BREAKDOWN:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_HAPPY_BREAKDOWN>();
        case KSP_DIVERGED_NULL:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_NULL>();
        case KSP_DIVERGED_ITS:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_ITS>();
        case KSP_DIVERGED_DTOL:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_DTOL>();
        case KSP_DIVERGED_BREAKDOWN:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_BREAKDOWN>();
        case KSP_DIVERGED_BREAKDOWN_BICG:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_BREAKDOWN_BICG>();
        case KSP_DIVERGED_NONSYMMETRIC:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_NONSYMMETRIC>();
        case KSP_DIVERGED_INDEFINITE_PC:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_INDEFINITE_PC>();
        case KSP_DIVERGED_NANORINF:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_NANORINF>();
        case KSP_DIVERGED_INDEFINITE_MAT:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_INDEFINITE_MAT>();
#if PETSC_VERSION_LT(3, 11, 0)
        case KSP_DIVERGED_PCSETUP_FAILED:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_PCSETUP_FAILED>();
#endif
#if PETSC_VERSION_GE(3, 11, 0)
        case KSP_DIVERGED_PC_FAILED:
            return GetLinearConvergenceReasonHelper<KSP_DIVERGED_PC_FAILED>();
#endif // PETSC_VERSION_GE(3, 11, 0)
        case KSP_CONVERGED_ITERATING:
            return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ITERATING>();
            // You might have to add or remove an entry after updating Petsc version; look after the warnings
            // about missing switch entry!
        }

        assert(false && "Make sure all entries are properly covered!");
        exit(EXIT_FAILURE);
    }


    convergence_status Snes::GetLinearConvergenceReason(std::string& explanation,
                                                        const std::source_location location) const
    {
        const auto ret = GetLinearConvergenceReason(location);
        explanation = *convergence_reason_;
        return ret;
    }


    double Snes::GetAbsoluteTolerance(const std::source_location location) const
    {
        // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
        PetscReal value;
        const int error_code = SNESGetTolerances(snes_, &value, nullptr, nullptr, nullptr, nullptr);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESGetTolerances", location);


        return static_cast<double>(value);
    }


    double Snes::GetRelativeTolerance(const std::source_location location) const
    {
        // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
        PetscReal value;
        const int error_code = SNESGetTolerances(snes_, nullptr, &value, nullptr, nullptr, nullptr);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESGetTolerances", location);


        return static_cast<double>(value);
    }


    std::size_t Snes::NmaxIteration(const std::source_location location) const
    {
        PetscInt value = 0;
        const int error_code = SNESGetTolerances(snes_, nullptr, nullptr, nullptr, &value, nullptr);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESGetTolerances", location);

        return static_cast<std::size_t>(value);
    }


    void Snes::SetLineSearchType(SNESLineSearchType type)
    {
        SNESLineSearch linesearch = nullptr;
        SNESGetLineSearch(Internal(), &linesearch);
        SNESLineSearchSetType(linesearch, type);
    }


    void Snes::SetDivergenceTolerance(const double tolerance, const std::source_location location)
    {
        const int error_code = SNESSetDivergenceTolerance(Internal(), tolerance);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "SNESSetDivergenceTolerance", location);
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
