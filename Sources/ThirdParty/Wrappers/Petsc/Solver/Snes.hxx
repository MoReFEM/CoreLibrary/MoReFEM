// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_SNES_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_SNES_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <memory>      // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Petsc { class Matrix; }
namespace MoReFEM::Wrappers::Petsc { class Vector; }
namespace MoReFEM::Wrappers::Petsc { enum class print_solver_infos; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


    inline Snes::SNESFunction Snes::GetSnesFunction() const
    {
        assert(!(!snes_function_));
        return snes_function_;
    }


    inline Snes::SNESJacobian Snes::GetSnesJacobian() const
    {
        assert(!(!snes_jacobian_));
        return snes_jacobian_;
    }


    inline Snes::SNESViewer Snes::GetSnesViewer() const
    {
        assert(!(!snes_viewer_));
        return snes_viewer_;
    }


    inline Snes::SNESConvergenceTestFunction Snes::GetSnesConvergenceTestFunction() const
    {
        // Might be nullptr.
        return snes_convergence_test_function_;
    }


    inline const Internal::Wrappers::Petsc::Solver& Snes::GetSolver() const noexcept
    {
        assert(!(!solver_));
        return *solver_;
    }


    inline Internal::Wrappers::Petsc::Solver& Snes::GetNonCstSolver() noexcept
    {
        return const_cast<Internal::Wrappers::Petsc::Solver&>(GetSolver());
    }


    template<Concept::PetscMatrix MatrixT>
    void Snes::SolveLinear(const MatrixT& matrix,
                           const MatrixT& preconditioner,
                           const Vector& rhs,
                           Vector& solution,
                           print_solver_infos do_print_solver_infos,
                           const std::source_location location)
    {
        KSP ksp = GetKsp(location);

        int error_code =
            KSPSetOperators(ksp, matrix.InternalForReadOnly(location), preconditioner.InternalForReadOnly(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPSetOperators", location);

        // If there was a previous call to this very function, this argument is set to PETSC_TRUE.
        // That's not what we want: matrix and preconditioner may not be the same as in previous call.
        error_code = KSPSetReusePreconditioner(ksp, PETSC_FALSE);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPSetReusePreconditioner", location);

        GetNonCstSolver().SetSolveLinearOptions(*this, location);

        // This optional line (would be called in KSPSolve) can lead to more explicit messages.
        // Petsc documentation quote: 'The explicit call of this routine enables the separate monitoring of any
        // computations performed during the set up phase, such as incomplete factorization for the ILU
        // preconditioner.' (this quote has since disappeared from Petsc doc...).
        // However, this triggers a Petsc error if the matrix is a shell one, hence the test below that is
        // false for such a matrix!
        // It seems numerical values change slightly if this method is called after the
        // KSPSetReusePreconditioner that sets PETSC_TRUE below.
        if (std::is_base_of<Wrappers::Petsc::Matrix, MatrixT>())
        {
            error_code = ::KSPSetUp(ksp);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "KSPSetUp", location);
        }

        // It seems I need this step to ensure Petsc doesn't do again the preconditioning; this is weird
        // because Petsc documentation doesn't mention it at all (this function is there in cases we want
        // to force to reuse the preconditioning even if the matrix or preconditioner has changed).
        error_code = KSPSetReusePreconditioner(ksp, PETSC_TRUE);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "KSPSetReusePreconditioner", location);


        // Now call the overload which works assuming matrices are well-defined - they should be by now!
        SolveLinear(rhs, solution, do_print_solver_infos, location);
    }


    template<SNESConvergedReason SNESConvergedReasonT>
    convergence_status Snes::GetNonLinearConvergenceReasonHelper() const noexcept
    {
        using type = Internal::Wrappers::Petsc::SnesConvergenceReason<SNESConvergedReasonT>;
        convergence_reason_ = &type::Explanation();
        return type::GetConvergenceStatus();
    }


    template<KSPConvergedReason KSPConvergedReasonT>
    convergence_status Snes::GetLinearConvergenceReasonHelper() const noexcept
    {
        using type = Internal::Wrappers::Petsc::KspConvergenceReason<KSPConvergedReasonT>;
        convergence_reason_ = &type::Explanation();
        return type::GetConvergenceStatus();
    }


    inline SNES Snes::Internal() noexcept
    {
        return snes_;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_SNES_DOT_HXX_
// *** MoReFEM end header guards *** < //
