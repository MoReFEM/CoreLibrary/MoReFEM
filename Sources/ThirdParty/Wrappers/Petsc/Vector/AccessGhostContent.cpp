// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert> // IWYU pragma: keep
#include <cstdlib>
#include <iostream>
#include <memory> // IWYU pragma: keep
#include <source_location>
#include <sstream>

#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    AccessGhostContent::AccessGhostContent(const Vector& vector, const std::source_location location)
    : vector_without_ghost_(vector)
    //            local_content_(nullptr)
    {
        const int error_code =
            VecGhostGetLocalForm(vector_without_ghost_.InternalForReadOnly(location), &petsc_vector_with_ghost_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGhostGetLocalForm", location);
    }


    AccessGhostContent::~AccessGhostContent()
    {
#ifndef NDEBUG
        try
        {
#endif // NDEBUG

            assert(petsc_vector_with_ghost_ != MOREFEM_PETSC_NULL);
            assert(vector_without_ghost_.InternalForReadOnly(std::source_location::current()) != MOREFEM_PETSC_NULL);

            [[maybe_unused]] const int error_code = VecGhostRestoreLocalForm(
                vector_without_ghost_.InternalForReadOnly(std::source_location::current()), &petsc_vector_with_ghost_);
            assert(error_code == 0); // error code should be 0; exception can't be thrown in a destructor!

#ifndef NDEBUG
        }
        catch (const Advanced::Assertion& e)
        {
            std::ostringstream oconv;
            oconv << "MoReFEM Assertion caught: " << e.what() << '\n';
            std::cerr << oconv.str();
            exit(EXIT_FAILURE);
        }
#endif // NDEBUG
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
