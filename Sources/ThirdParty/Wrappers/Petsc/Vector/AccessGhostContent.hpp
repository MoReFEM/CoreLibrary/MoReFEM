// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSGHOSTCONTENT_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSGHOSTCONTENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>

#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief The purpose of this class is to enable access to the whole content of the vector, including its
     ghost.
     *
     * It is really a RAII around Petsc's VecGhostGetLocalForm/VecGhostRestoreLocalForm.
     *
     * The call should be as follow:
     *
     * \code
     *
     *  Vector vector(...); // the vector we want to probe.
     *
     *  { // important: ensure destructor called as soon as possible!
     *      Wrappers::Petsc::AccessGhostContent access_ghost_content(vector);
     *
     *      const Vector& vector_with_ghost = access_ghost_content.GetVectorWithGhost(); // need to be separated
     *                                                                                   // from following line;
     *                                                                                   // don't merge them!
     *
     *      Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>
     *          vector_with_ghost_content(vector_with_ghost);
     *
     *      // Hereafter whatever operation for which you needed the content, for instance:
     *      for (auto global_index : local_2_global)
                ret.push_back(ghost_vector_content.GetValue(global_index));
     * }
     *
     * \attention This class assumes the purpose to access it is for read-only purpose. If you want to
     * modifying it, you probably seek \a AccessVectorContent, which doesn't get to access ghost values (as they
     * shouldn't be modified directly - it is the task of \a Vector::UpdateGhosts() ).
     *
     * \endcode
     *
     */
    class AccessGhostContent
    {

      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] vector Vector for which local access is granted.
         *  \copydoc doxygen_hide_source_location
         *
         */
        explicit AccessGhostContent(const Vector& vector,
                                    const std::source_location location = std::source_location::current());

        //! Destructor
        ~AccessGhostContent();

        /*!
         * \brief Return an extended Vector object for which it is possible to access ghost data.
         */
        Vector GetVectorWithGhost() const;


      private:
        //! Local vector given in input.
        const Vector& vector_without_ghost_;

        //! Local vector with ghost values added.
        Vec petsc_vector_with_ghost_{};
    };


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSGHOSTCONTENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
