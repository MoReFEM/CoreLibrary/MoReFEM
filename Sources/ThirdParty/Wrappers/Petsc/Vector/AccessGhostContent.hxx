// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSGHOSTCONTENT_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSGHOSTCONTENT_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    inline Vector AccessGhostContent::GetVectorWithGhost() const
    {
        assert(petsc_vector_with_ghost_ != MOREFEM_PETSC_NULL);
        return Vector(petsc_vector_with_ghost_,
                      Advanced::Wrappers::Petsc::call_petsc_destroy::no,
                      "Temporary vector for AccessVectorContent use.",
                      Advanced::Wrappers::Petsc::print_linalg_destruction::no);
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSGHOSTCONTENT_DOT_HXX_
// *** MoReFEM end header guards *** < //
