// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep


// IWYU pragma: begin_exports
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::Wrappers::Petsc
{


    template<Utilities::Access AccessT>
    inline auto AccessVectorContent<AccessT>::GetArray() const -> typename VectorForAccess<AccessT>::scalar_array_type
    {
        return values_;
    }


    template<Utilities::Access AccessT>
    inline auto AccessVectorContent<AccessT>::GetValue(vector_processor_wise_index_type i) const -> PetscScalar
    {
        assert(values_ != NULL);
        assert(i < vector_.GetProcessorWiseSize());
        return values_[i.Get()];
    }


    template<Utilities::Access AccessT>
    inline auto AccessVectorContent<AccessT>::GetSize(const std::source_location location) const
        -> vector_processor_wise_index_type
    {
        AssertionNS::InternalNotNull(vector_.template InternalForReadOnly<Vector::check_non_null_ptr::no>(location),
                                     location);

        return vector_.GetProcessorWiseSize(location);
    }


    template<Utilities::Access AccessT>
    auto AccessVectorContent<AccessT>::operator[](vector_processor_wise_index_type i) -> PetscScalar&
    {
        static_assert(AccessT != Utilities::Access::read_only,
                      "This method should only be called when read/write rights are given!");
        assert(values_ != NULL);
        assert(i <= vector_.GetProcessorWiseSize());
        return values_[i.Get()];
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HXX_
// *** MoReFEM end header guards *** < //
