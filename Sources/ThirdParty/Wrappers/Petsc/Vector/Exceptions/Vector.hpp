// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_EXCEPTIONS_VECTOR_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_EXCEPTIONS_VECTOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    //! When loading an ascii file.

    struct InvalidAsciiFile : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] file Ascii dile from which we tried to load vector content.
         * \copydoc doxygen_hide_source_location

         */
        explicit InvalidAsciiFile(const FilesystemNS::File& file,
                                  const std::source_location location = std::source_location::current());


        //! Destructor.
        virtual ~InvalidAsciiFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidAsciiFile(const InvalidAsciiFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InvalidAsciiFile(InvalidAsciiFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidAsciiFile& operator=(const InvalidAsciiFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InvalidAsciiFile& operator=(InvalidAsciiFile&& rhs) = delete;
    };


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_EXCEPTIONS_VECTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
