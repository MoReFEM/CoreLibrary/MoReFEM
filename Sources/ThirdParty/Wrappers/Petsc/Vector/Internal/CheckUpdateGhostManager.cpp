// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE

#include <algorithm>
#include <iostream>

#include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{


    CheckUpdateGhostManager::~CheckUpdateGhostManager() = default;


    const std::string& CheckUpdateGhostManager::ClassName()
    {
        static const std::string ret("CheckUpdateGhostManager");
        return ret;
    }


    void CheckUpdateGhostManager::UnneededCall(const std::source_location location)
    {
        decltype(auto) log = GetNonCstLog();

        auto&& pair = std::make_pair(location);

        const auto it = log.find(pair);

        // Insert the entry with false only if it doesn't exist.
        // If it exists, keep current value.
        if (it == log.cend())
            log.insert({ pair, false });
    }


    void CheckUpdateGhostManager::NeededCall(const std::source_location location)
    {
        decltype(auto) log = GetNonCstLog();

        auto&& pair = std::make_pair(location);

        const auto it = log.find(pair);

        if (it != log.cend())
            it->second = true;
        else
            log.insert({ pair, true });
    }


    void CheckUpdateGhostManager::Print() const
    {
        decltype(auto) log = GetLog();

        if (std::all_of(log.cbegin(),
                        log.cend(),
                        [](const auto& pair)
                        {
                            return pair.second == true;
                        }))
            std::cout << "[NOTE] No unneeded call to UpdateGhosts()." << std::endl;
        else
        {
            std::cout << "[WARNING] The following calls to UpdateGhosts() were probably unneeded. "
                         "However check your model was running with sufficient runs and a big enough mesh, and "
                         "proceed with caution: this is a mere indicator, that might be wrong. Check whether you "
                         "obtain the same result after removing the incriminated lines!"
                      << std::endl;

            for (const auto& pair : log)
            {
                if (!pair.second)
                {
                    const auto& file_line_pair = pair.first;
                    std::cout << "\t" << file_line_pair.first << ", line " << file_line_pair.second << std::endl;
                }
            }
        }
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
