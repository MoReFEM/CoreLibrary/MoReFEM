// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_CHECKUPDATEGHOSTMANAGER_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_CHECKUPDATEGHOSTMANAGER_DOT_HPP_
// *** MoReFEM header guards *** < //

#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE

#include <map>
#include <memory>
#include <vector>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::Wrappers::Petsc
{


    /*!
     * \brief Helper object used to detect unneeded calls to UpdateGhosts().
     *
     * This object is to use only in debug mode when MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE macro
     * is defined. Its purpose is to check whether there are some undue calls to
     * Wrappers::Petsc::Vector::UpdateGhosts() somewhere down the line. It works only in parallel, as
     * there are no ghosts ar all in sequential to detect.
     *
     * Whenever a useless call to Wrappers::Petsc::Vector::UpdateGhosts() is issued, there is a check
     * whether the ghosts are actually modified or not. If not, the file and line of calls gets registered
     * here; if a subsequent call modify the ghost values it is removed.
     *
     * \warning The output is produced only in root processor with data produced there; it is assumed all
     * processors exhibit the same behaviour. This seems a rather same assumption, but you should anyway
     * check your output isn't changed by removing one of the flagged UpdateGhosts.
     *
     */
    class CheckUpdateGhostManager : public Utilities::Singleton<CheckUpdateGhostManager>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! Convenient alias.
        using log_type = std::map<std::pair<std::string, int>, bool>;


      public:
        /*!
         * \brief Add a new entry.
         */
        void UnneededCall(const std::string& file, int line);

        /*!
         * \brief Remove if necessary an entry.
         */
        void NeededCall(const std::string& file, int line);

        //! Print the content (to call at Model destruction).
        void Print() const;

      private:
        /*!
         * \class doxygen_hide_check_update_ghost_manager_log
         *
         * \brief Store for each (file, line) pair in which calls to UpdateGhosts() whether they modify
         * or not the ghost values.
         *
         * Key is the pair (file, line)
         * Value is true if the call is required, false otherwise.
         */


        //! \copydoc doxygen_hide_check_update_ghost_manager_log
        log_type& GetNonCstLog() noexcept;


        //! \copydoc doxygen_hide_check_update_ghost_manager_log
        const log_type& GetLog() const noexcept;


      private:
        //! \name Singleton requirements.
        ///@{
        //! Constructor.
        CheckUpdateGhostManager() = default;

        //! Destructor.
        virtual ~CheckUpdateGhostManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<CheckUpdateGhostManager>;
        ///@}


      private:
        /*!
         * \brief Store for each (file, line) pair in which calls to UpdateGhosts() whether they modify
         * or not the ghost values.
         *
         * Key is the pair (file, line)
         * Value is true if the call is required, false otherwise.
         */
        log_type log_;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hxx" // IWYU pragma: export

// clang-format off
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_CHECKUPDATEGHOSTMANAGER_DOT_HPP_
// *** MoReFEM end header guards *** < //

// clang-format on
