// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_CHECKUPDATEGHOSTMANAGER_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_CHECKUPDATEGHOSTMANAGER_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp"
// *** MoReFEM header guards *** < //


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


namespace MoReFEM::Internal::Wrappers::Petsc
{


    inline const CheckUpdateGhostManager::log_type& CheckUpdateGhostManager ::GetLog() const noexcept
    {
        return log_;
    }


    inline CheckUpdateGhostManager::log_type& CheckUpdateGhostManager ::GetNonCstLog() noexcept
    {
        return log_;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_CHECKUPDATEGHOSTMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
