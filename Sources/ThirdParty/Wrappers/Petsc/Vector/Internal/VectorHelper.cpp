// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <cstring> // IWYU pragma: keep // required by gcc for std::memcpy.
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <ostream>
#include <source_location>

#include "ThirdParty/Wrappers/Petsc/Vector/Internal/VectorHelper.hpp"

#include "Utilities/AsciiOrBinary/BinarySerialization.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{


    void PrintPerProcessor(const Vector& vector,
                           const FilesystemNS::File& output_file,
                           const std::source_location location,
                           binary_or_ascii binary_or_ascii_choice)
    {
        const ::MoReFEM::Wrappers::Petsc ::AccessVectorContent<Utilities::Access::read_only> content(vector, location);

        const auto Nvalue = content.GetSize(location);

        switch (binary_or_ascii_choice)
        {
        case binary_or_ascii::binary:
        {
            const auto* array = content.GetArray();
            Advanced::SerializeCArrayIntoBinaryFile(output_file, array, static_cast<std::size_t>(Nvalue.Get()));
            break;
        }
        case binary_or_ascii::ascii:
        {
            std::ofstream out{ output_file.NewContent() };

            for (auto i = vector_processor_wise_index_type{ 0UL }; i < Nvalue; ++i)
            {
                // NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
                if (std::fabs(content.GetValue(i)) <= NumericNS::DefaultEpsilon<double>())
                    out << std::setw(12) << std::scientific << 0. << '\n';
                else
                    out << std::setw(12) << std::scientific << std::setprecision(20) << content.GetValue(i) << '\n';
                // NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
            }
            break;
        }
        case binary_or_ascii::from_input_data:
        {
            assert(false
                   && "This function should be called from Vector class, and this specific case "
                      "should have been addressed prior to this call (see Vector::Print() for instance "
                      "to see how).");
            exit(EXIT_FAILURE);
        }
        } // switch
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
