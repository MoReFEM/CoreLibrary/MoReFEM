// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_VECTORHELPER_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_VECTORHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Wrappers::Petsc { class Vector; } // IWYU pragma: keep
namespace MoReFEM { enum class binary_or_ascii; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc
{


    //! Convenient alias to avoid repeating namespaces.
    using Vector = ::MoReFEM::Wrappers::Petsc::Vector;


    /*!
     * \brief The function called when Print() is called in processor-wise mode.
     *
     * \param[in] vector Vector which processor-wise content is to be written.
     * \param[in] output_file Output file into which the values will be written. Beware: this file should
     * be named differently for each rank!
     * \param[in] binary_or_ascii_choice Whether the vector should be printed as binary or ascii. Default
     * value takes its cue from the choice written in the input data file.
     *
     * \copydoc doxygen_hide_source_location
     *
     */
    void PrintPerProcessor(const Vector& vector,
                           const ::MoReFEM::FilesystemNS::File& output_file,
                           const std::source_location location,
                           binary_or_ascii binary_or_ascii_choice);


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_INTERNAL_VECTORHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
