// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_VECTOR_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_VECTOR_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp" // IWYU pragma: keep
#include "Utilities/LinearAlgebra/StrongType.hpp"    // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"               // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp" // IWYU pragma: export
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"                       // IWYU pragma: keep
#include "ThirdParty/Wrappers/Mpi/StrongType.hpp"                     // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Advanced/Enum.hpp"                // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"             // IWYU pragma: export // IWYU pragma: keep
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/VectorHelper.hpp" // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Wrappers { class Mpi; }
namespace MoReFEM::Wrappers::Petsc { template <Utilities::Access AccessT> class AccessVectorContent; } // IWYU pragma: keep

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    //! Enum class to specify if in a copy ghosts are updated or not.
    enum class update_ghost { yes, no };


} // namespace MoReFEM


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief A wrapper class over Petsc Vec objects.
     *
     * Most of the Petsc functions used over Petsc vectors have been included as methods in this class, which
     * also acts as RAII over Petsc Vec object.
     *
     * \internal <b><tt>[internal]</tt></b> This class is way more trickier to implement that it might seems
     * because of Petsc's internal structure: Vec objects are in fact pointers over an internal Petsc type.
     * So copy and destruction operations must be made with great care! That's why several choices have been
     * made:
     *
     * - No implicit conversion to the internal Vec. It seems alluring at first sight to allow it but can
     * lead very fastly to runtime problems (covered by asserts in debug mode).
     * - No copy semantic for this class.
     * - The most usual way to proceed is to construct with the default constructor and then init it either
     * by a 'Init***()' method or by using 'DuplicateLayout', 'Copy' or 'CompleteCopy' methods.
     * - There is a constructor that takes as argument a Petsc Vec object. It should be avoided as much
     * as possible (current class should oversee most of vector operations) but is nonetheless required
     * by user-defined Snes functions. When this constructor is used VecDestroy() is NOT called upon
     * destruction, so that the vector taken as argument still lives.
     * \endinternal
     *
     */
    class Vector
    {
      public:
        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<Vector>;

        //! Convenient alias.
        using call_petsc_destroy = Advanced::Wrappers::Petsc::call_petsc_destroy;

        static_assert(std::is_same<PetscScalar, double>(),
                      "Check PETSc was properly installed with 'double' setting for its floating point type.");

      public:
        /// \name Special members.

        ///@{


        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_petsc_vector_name_arg
         *
         * \copydetails doxygen_hide_print_linalg_arg
         */
        explicit Vector(std::string&& name,
                        Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction =
                            Advanced::Wrappers::Petsc::print_linalg_destruction::no);

        /*!
         * \brief Constructor from an existing Petsc Vec.
         *
         * \internal <b><tt>[internal]</tt></b> Avoid this as much as possible (current class should hold all
         * Petsc Vector information) but in some very specific case (for instance defining a SNES function) it
         * is easier at the moment to resort to this one.
         *
         * \endinternal
         * \param[in] petsc_vector The petsc vector encapsulated within the object.
         * \param[in] do_call_petsc_destroy Whether the underlying Petsc Vec must be destroyed or not in the
         * destructor.
         * \copydetails doxygen_hide_petsc_vector_name_arg
         *
         * \copydetails doxygen_hide_print_linalg_arg
         *
         * We are here at a fairly low level - for most linear algebra it is advised to use \a GlobalVector hence the
         * default value that choose not to print destruction of the object. The flag in command line concerns the
         * matrices defined with \a GlobalVector only - if for some reason you need specifically a \a
         * Wrappers::Petsc::Vector you have to give it explicitly to the constructor (the value handled in the \a
         * CommandLineFlags singleton may of course be used but it's not baked in as for \a GlobalVector (the reason is
         * the hierarchy of modules: the singleton is defined in Core and here we are at a lower level than that)).
         */
        explicit Vector(const Vec& petsc_vector,
                        call_petsc_destroy do_call_petsc_destroy,
                        std::string&& name,
                        Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction =
                            Advanced::Wrappers::Petsc::print_linalg_destruction::no);

        //! Destructor.
        virtual ~Vector();

        /*!
         * \copydoc doxygen_hide_copy_constructor
         *
         * Both layout and data are copied.
         *
         * \copydetails doxygen_hide_petsc_vector_name_arg
         *
         * \attention Standard mandates that additional parameters for copy constructor get default value.
         *
         * \attention This copy constructor assumes the \a rhs vector has been properly initialized (through a call to one of the \a InitXXX() method.
         * If not an `Advanced::Assertion` is raised (in debug mode) or PETSc underlying function will return an error.
         */
        Vector(const Vector& rhs, std::string&& name = "Default value");

        /*!
         * \copydoc doxygen_hide_move_constructor
         */
        Vector(Vector&& rhs) noexcept;

        //! \copydoc doxygen_hide_copy_affectation
        Vector& operator=(const Vector& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Vector& operator=(Vector&& rhs) noexcept = delete;

        ///@}


      public:
        /*!
         * \brief Set the vector as sequential.
         *
         * \attention This method is to be called just after creation of the object, which should still
         * be a blank state.
         *
         * \param[in] size Number of elements in the vector.
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         */
        void InitSequentialVector(const Mpi& mpi,
                                  vector_processor_wise_index_type size,
                                  const std::source_location location = std::source_location::current());

        /*!
         * \brief Set the vector as sequential.
         *
         * \attention This method is to be called just after creation of the object, which should still
         * be a blank state.
         *
         * \copydetails doxygen_hide_parallel_size_arg
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         */
        void InitMpiVector(const Mpi& mpi,
                           vector_processor_wise_index_type processor_wise_size,
                           vector_program_wise_index_type program_wise_size,
                           const std::source_location location = std::source_location::current());

        /*!
         * \brief Create a parallel vector with ghost padding.
         *
         * \copydetails doxygen_hide_parallel_with_ghosts_arg
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         */
        void InitMpiVectorWithGhost(const Mpi& mpi,
                                    vector_processor_wise_index_type processor_wise_size,
                                    vector_program_wise_index_type program_wise_size,
                                    const std::vector<PetscInt>& ghost_padding,
                                    const std::source_location location = std::source_location::current());


        /*!
         * \brief Init a sequential vector with the data read in the file.
         *
         * This file is assumed to have been created with Print() method for a sequential vector.
         *
         * \param[in] file File from which vector content is read.
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         */
        void InitSequentialFromAsciiFile(const Mpi& mpi,
                                         const FilesystemNS::File& file,
                                         const std::source_location location = std::source_location::current());


        /*!
         * \brief Init a vector from the data read in the file.
         *
         * This file is assumed to have been created with Print() method.
         *
         * Current method is in fact able to create a sequential vector as well, but use rather
         * InitSequentialFromAsciiFile() with its more friendly API if you need only the sequential case.
         *
         * \param[in] ascii_file File from which vector content is read.
         * \copydetails doxygen_hide_parallel_with_ghosts_arg
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         *
         */
        void
        InitParallelFromProcessorWiseAsciiFile(const Mpi& mpi,
                                               vector_processor_wise_index_type processor_wise_size,
                                               vector_program_wise_index_type program_wise_size,
                                               const std::vector<PetscInt>& ghost_padding,
                                               const FilesystemNS::File& ascii_file,
                                               const std::source_location location = std::source_location::current());

        /*!
         * \brief Init a vector from the data read in the file.
         *
         * This file is assumed to have been created with Print() method.
         *
         * \param[in] binary_file File from which vector content is read.
         * \copydetails doxygen_hide_parallel_with_ghosts_arg
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         */
        void
        InitParallelFromProcessorWiseBinaryFile(const Mpi& mpi,
                                                vector_processor_wise_index_type processor_wise_size,
                                                vector_program_wise_index_type program_wise_size,
                                                const std::vector<PetscInt>& ghost_padding,
                                                const FilesystemNS::File& binary_file,
                                                const std::source_location location = std::source_location::current());

        /*!
         * \brief Init from a program-wise binary file: load a vector dumped with View() method.
         *
         * \param[in] binary_file File from which the vector must be loaded. This file must be in binary format.
         * \copydetails doxygen_hide_parallel_with_ghosts_arg
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         */
        void InitFromProgramWiseBinaryFile(const Mpi& mpi,
                                           vector_processor_wise_index_type processor_wise_size,
                                           vector_program_wise_index_type program_wise_size,
                                           const std::vector<PetscInt>& ghost_padding,
                                           const FilesystemNS::File& binary_file,
                                           const std::source_location location = std::source_location::current());

        //! Convenient enum class for \a Internal() and \a InternalForReadOnly() template argument.
        enum class check_non_null_ptr { no, yes };


        /*!
         * \brief Handle over the internal \a Vec object.
         *
         * \tparam do_check_non_null_ptr If 'yes', check with an assert in debug mode whether the underlying
         * pointer has been defined or not. 'yes' is really the go to value; 'no' is used only in some low level
         * functions.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Internal \a Vec object, which is indeed a pointer in Petsc.
         *
         *
         * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
         * method should be implemented over the function that might need access to the \a Vec internal object.
         */
        template<check_non_null_ptr do_check_non_null_ptr = check_non_null_ptr::yes>
        Vec Internal(std::source_location location);


        /*!
         * \brief Handle over the internal \a Vec object - when you can guarantee the call is only to read the
         * value, not act upon it.
         *
         * \tparam do_check_non_null_ptr If 'yes', check with an assert in debug mode whether the underlying
         * pointer has been defined or not. 'yes' is really the go to value; 'no' is used only in some low level
         * functions.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Internal \a Vec object, which is indeed a pointer in Petsc.
         *
         * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
         * method should be implemented over the function that might need access to the \a Vec internal object.
         */
        template<check_non_null_ptr do_check_non_null_ptr = check_non_null_ptr::yes>
        Vec InternalForReadOnly(std::source_location location) const;


        /*!
         * \brief Duplicate layout (in memory, processor repartition, etc...)
         *
         * \param[in] original Vector which layout is copied.
         * \copydoc doxygen_hide_source_location
         */
        void DuplicateLayout(const Vector& original,
                             const std::source_location location = std::source_location::current());


        /*!
         * \brief Get the number of elements in the local vector.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Number of processor-wise elements of the vector (ghost excluded).
         */
        vector_processor_wise_index_type
        GetProcessorWiseSize(const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Get the number of elements in the global vector.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Number of program-wise elements of the vector.
         */
        vector_program_wise_index_type
        GetProgramWiseSize(const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Set all the entries to zero.
         *
         * \copydoc doxygen_hide_source_location
         * \copydoc doxygen_hide_do_update_ghost_arg
         */
        void ZeroEntries(const std::source_location location = std::source_location::current(),
                         update_ghost do_update_ghost = update_ghost::yes);


        /*!
         * \class doxygen_hide_petsc_vec_assembly
         *
         * You have to call Assembly() method after you're done with all your SetXXX() calls; otherwise you
         * will in all likelihood get error message in parallel about wrong state of the vector (but nothing in
         * sequential run).
         * It has not been put directly inside these SetXXX() methods as usually several of them are called in
         * a row.
         * For more details, see original explanation in Petsc page about vecSetValues:
         * http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecSetValues.html#VecSetValues
         *
         */


        /*!
         * \brief Petsc Assembling of the vector.
         *
         * \copydoc doxygen_hide_petsc_vec_assembly
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_do_update_ghost_arg
         * This argument must be set to  `update_ghost::no` if you're considering a parallel vector without ghost.
         *
         * \internal There is a hidden call to it in boundary condition appliance; so maybe you
         * \endinternal
         *
         */
        void Assembly(const std::source_location location = std::source_location::current(),
                      update_ghost do_update_ghost = update_ghost::yes);

        /*!
         * \brief Add or modify values inside a Petsc vector.
         *
         * \copydoc doxygen_hide_petsc_vec_assembly
         *
         * \param[in] indexing All indexes (program-wise) that have to be modified in the vector are stored here.
         *
         * \internal We do not use strong type here for performance reason: the underlying C array is propagated
         * directly to PETSc C API, and we would lose that with a vector of strong types.
         *
         * \param[in] values Values to put in the vector. This array should be the same size as \a indexing
         * (unfortunately we can't check that here as it is a C array)
         * \param [in] insertOrAppend Petsc ADD_VALUES or INSERT_VALUES (see Petsc documentation).
         * \copydoc doxygen_hide_source_location
         */
        void SetValues(const std::vector<PetscInt>& indexing,
                       const PetscScalar* values,
                       InsertMode insertOrAppend,
                       const std::source_location location = std::source_location::current());


        /*!
         * \brief Add or modify values inside a Petsc vector.
         *
         * \copydoc doxygen_hide_petsc_vec_assembly
         *
         * \param[in] indexing All indexes (program-wise) (program-wise) that have to be modified in the vector are stored here.
         * \internal We do not use strong type here for performance reason: the underlying C array is propagated
         * directly to PETSc C API, and we would lose that with a vector of strong types.
         *
         * \param[in] local_vec Local vector which values will be put inside vector.
         * \param [in] insertOrAppend Petsc ADD_VALUES or INSERT_VALUES (see Petsc documentation).
         * \copydoc doxygen_hide_source_location
         */
        template<Utilities::Access AccessT>
        void SetValues(const std::vector<PetscInt>& indexing,
                       const AccessVectorContent<AccessT>& local_vec,
                       InsertMode insertOrAppend,
                       const std::source_location location = std::source_location::current());

        /*!
         * \brief Set the values of a vector from a Petsc Vec object.
         *
         * \attention This method should be used as little as possible: the purpose of current class
         * is to avoid interacting at all with native Petsc objects. However, in some cases we do not have
         * the choice: for instance in the definition of a Snes function (for Newton method) we get
         * a Vec argument.
         * \param[in] petsc_vector Petsc \a Vec object.
         * \copydoc doxygen_hide_source_location
         */
        void SetFromPetscVec(const Vec& petsc_vector,
                             const std::source_location location = std::source_location::current());


        /*!
         * \brief Get the values of a vector on a current process.
         *
         * This method allocates a vector at each call, another method with the same name does the same thing
         * without the allocation at each time and should be used if the calls happen several times for the same
         * underlying data.
         *
         * Used to get the values of a sequential vector (see \a AccessVectorContent for mpi vectors).
         *
         * \param[in] indexing All program-wise indexes which are required from the vector.
         * \internal We do not use strong type here for performance reason: the underlying C array is propagated
         * directly to PETSc C API, and we would lose that with a vector of strong types.

         * \copydoc doxygen_hide_source_location
         *
         * \return The values for all the indexes given in input.
         */
        std::vector<PetscScalar> GetValues(const std::vector<PetscInt>& indexing,
                                           const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Get the values of a vector on a current process.

         * Same as the other method with the same name but the vector to contain the values is not allocated
         * at each call. This method should be called if you need to get the values of a vector with the same
         * size a lot of time.
         *
         * Used to get the values of a sequential vector (see \a AccessVectorContent for mpi vectors).
         *
         * \param[in] indexing All program-wise indexes which are required from the vector.
         * \internal We do not use strong type here for performance reason: the underlying C array is propagated
         * directly to PETSc C API, and we would lose that with a vector of strong types.
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] values Vector used to contain the values. Should have been allocated before the call.
         *
         */
        void GetValues(const std::vector<PetscInt>& indexing,
                       std::vector<PetscScalar>& values,
                       const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Same as GetValues() for a unique index.
         *
         * \param[in] index Program-wise index which associated value is sought.
         * \copydoc doxygen_hide_source_location
         *
         * \return Associated value.
         */
        PetscScalar GetValue(vector_program_wise_index_type index,
                             const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Add or modify one value inside a Petsc vector.
         *
         * \copydoc doxygen_hide_petsc_vec_assembly
         *
         * \param[in] index Index (program-wise) to be modified.
         * \param[in] value Value to set.
         * \param[in] insertOrAppend Petsc ADD_VALUES or INSERT_VALUES (see Petsc documentation).
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_do_update_ghost_arg
         */
        void SetValue(vector_program_wise_index_type index,
                      PetscScalar value,
                      InsertMode insertOrAppend,
                      const std::source_location location = std::source_location::current(),
                      update_ghost do_update_ghost = update_ghost::yes);


        /*!
         * \brief Set the same value to all entries of the vector.
         *
         * \copydoc doxygen_hide_petsc_vec_assembly
         *
         * \param[in] value Value to set.
         * \copydoc doxygen_hide_source_location
         * \copydoc doxygen_hide_do_update_ghost_arg
         */
        void SetUniformValue(PetscScalar value,
                             const std::source_location location = std::source_location::current(),
                             update_ghost do_update_ghost = update_ghost::yes);


        /*!
         * \class doxygen_hide_do_update_ghost_arg
         *
         * \param[in] do_update_ghost Whether the target gets its ghost automatically updated or not.
         * Default is yes.
         */

        /*!
         * \brief A wrapper over VecCopy, which assumed target already gets the right layout.
         *
         * Doesn't do much except check the return value.
         * \param[in] source Original vector which content is copied. Layout is assumed to be already
         * the same between object for which the method is called and \a source.
         * \copydoc doxygen_hide_source_location
         * \copydoc doxygen_hide_do_update_ghost_arg
         */
        void Copy(const Vector& source,
                  const std::source_location location = std::source_location::current(),
                  update_ghost do_update_ghost = update_ghost::yes);


        /*!
         * \brief A complete copy: layout is copied first and then the values.
         *
         * \param[in] source Original vector which layout AND content is copied.
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_do_update_ghost_arg
         */
        void CompleteCopy(const Vector& source,
                          const std::source_location location = std::source_location::current(),
                          update_ghost do_update_ghost = update_ghost::yes);


        /*!
         * \brief Wrapper over VecScale, that performs Y = a * Y.
         *
         * \param[in] a Factor by which the vector is scaled.
         * \copydoc doxygen_hide_source_location
         * \copydoc doxygen_hide_do_update_ghost_arg
         */
        void Scale(PetscScalar a,
                   const std::source_location location = std::source_location::current(),
                   update_ghost do_update_ghost = update_ghost::yes);


        /*!
         * \brief Wrapper over VecShift, that performs Y = Y + a * 1.
         *
         * \param[in] a Factor by which the vector is shifted.
         * \copydoc doxygen_hide_do_update_ghost_arg
         *
         * \copydoc doxygen_hide_source_location
         */
        void Shift(PetscScalar a,
                   const std::source_location location = std::source_location::current(),
                   update_ghost do_update_ghost = update_ghost::yes);


        /*!
         * \brief Wrapper over VecView.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         */
        void View(const Mpi& mpi, const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Wrapper over VecView in the case the viewer is a file.
         *
         * \param[in] format Format in which the matrix is written. See Petsc manual pages to get all the
         * formats available; relevant ones so far are PETSC_VIEWER_DEFAULT and PETSC_VIEWER_ASCII_MATLAB.
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \param[in] output_file File into which the vector content will be written.
         *
         * \attention To my knowledge there are no way to reload a PETSc vector from the file; if you need
         * to do so rather use binary format.
         *
         */
        void View(const Mpi& mpi,
                  const FilesystemNS::File& output_file,
                  const std::source_location location = std::source_location::current(),
                  PetscViewerFormat format = PETSC_VIEWER_DEFAULT) const;


        /*!
         * \brief Wrapper over MatView in the case the viewer is a binary file.
         *
         * \param[in] output_file File into which the vector content will be written.
         * \copydoc doxygen_hide_source_location
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \attention If you want to be able to reload the vector later with the exact same structure,
         * you have to also store its local and global sizes (called processor- and program-wise sizes
         * within MoReFEM) and the ghost padding.
         */
        void ViewBinary(const Mpi& mpi,
                        const FilesystemNS::File& output_file,
                        const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Print the content of a vector in a file.
         *
         * \tparam MpiScaleT Whether we want to print program-wise (in which case View function above is called)
         * or processor-wise data.
         * If processor-wise is chosen, this function does not rely on VecView: what I want to achieve is write
         * to a different file for each processor and VecView doesn't seem to be able to do so.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] output_file File into which the vector content will be written.
         * \param[in] binary_or_ascii_choice Whether the vector should be printed as binary or ascii. Default
         * value takes its cue from the choice written in the input data file.
         *
         * \attention If the purpose is to be able to reload the file from disk later on, format MUST be
         * binary (PETSc
         * [VecLoad](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecLoad.html) works only
         * with binary or HDF5). The file is not enough to rebuild the vector: you will also need to know:
         * - The number of elements processor-wise.
         * - The number of elements program-wise.
         * - The program-wise indexes on the ghost elements.
         */
        template<MpiScale MpiScaleT>
        void Print(const Mpi& mpi,
                   const FilesystemNS::File& output_file,
                   binary_or_ascii binary_or_ascii_choice = binary_or_ascii::from_input_data,
                   const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Get the minimum.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return First element is the position of the minimum found, second is its value.
         */
        std::pair<vector_processor_wise_index_type, PetscReal>
        Min(const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Get the maximum.
         *
         * \copydoc doxygen_hide_source_location
         * \return First element is the position of the maximum found, second is its value.
         */
        std::pair<vector_processor_wise_index_type, PetscReal>
        Max(const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Wrapper over VecNorm.
         *
         * Available norms are NORM_1, NORM_2 and NORM_INFINITY.
         *
         * \param[in] type NORM_1, NORM_2 or NORM_INFINITY.
         * \copydoc doxygen_hide_source_location
         *
         * \return Value of the norm.
         */
        double Norm(NormType type, const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Update the ghost values.
         *
         * \copydoc doxygen_hide_source_location
         *
         * Prior to 2024, behaviour when applied upon a vector without ghost was left to PETSc. I have now changed
         * that: when it is unduly called (i.e. when `IsGhosted()` returns false) it just does nothing.
         *
         * Beware: all processor must make the call to this method! If for instance you're in a loop
         * and one processor leaves it before the other, the code will be deadlocked...
         */
        void UpdateGhosts(const std::source_location location = std::source_location::current());


        /*!
         * \brief Update the ghost values if do_update_ghost is set to yes.
         *
         * This convenient method should be used only in Vector or Matrix related functions that
         * provides the possibility to automatically update ghosts through an ad hoc argument (see
         * for instance Copy() method).
         *
         * \copydoc doxygen_hide_source_location
         *
         * Prior to 2024, behaviour when applied upon a vector without ghost was left to PETSc. I have now changed
         * that: when it is unduly called (i.e. when `IsGhosted()` returns false) it just does nothing.
         *
         * Beware: all processor must make the call to this method! If for instance you're in a loop
         * and one processor leaves it before the other, the code will be deadlocked...
         *
         * \copydoc doxygen_hide_do_update_ghost_arg
         */
        void UpdateGhosts(const std::source_location location, update_ghost do_update_ghost);


        /*!
         * \brief Tells whether a vector is sequential or parallel.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return True if the vector is parallel, false if it is sequential.
         */
        bool IsParallel(const std::source_location location) const;

        /*!
         * \brief Tells whether a vector is ghosted or not.
         *
         * PETSc does not provide a direct function to do so by choice (see
         * [this discussion](https://lists.mcs.anl.gov/pipermail/petsc-dev/2011-November/006342.html)).
         *
         * However they provide nonetheless a way: `VecGhostGetLocalForm` provides a null pointer if invoked on ghost
         * element.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return True if the vector gets some ghost (in general, not specifically on current rank).
         */
        bool IsGhosted(const std::source_location location) const;


        /*!
         * \brief Accessor to the list of program-wise index of values that are ghost
         *
         * (i.e. required processor-wise but owned by another processor).
         *
         * Shouldn't be called if not a vector with ghost (an assert checks that).
         */
        const std::vector<PetscInt>& GetGhostPadding() const noexcept;


        /*!
         * \brief Tells the class not to destroy the underlying vector through a call to \a VecDestroy().
         *
         * \attention This method should be avoided most of the time; the only cases in which it's relevant are:
         * - the one exposed in the documentation of member function \a Swap.
         * - when wrapping a \a Vec that is owned by PETSc (typically in non linear interface - \a Snes -
         * where PETSc provides so-called \a evaluation_state and \a residual objects for each iteration).
         * - temporarily, in \a FiberList to avoid crash at deallocation. This is however temporary and should be fixed
         * (see #1895).
         */
        void SetDoNotDestroyPetscVector();

      public:
        /*!
         * \brief  Get the name given to the vector at construction.
         *
         * This is intended as a developer tool for debug, especially when something goes awry with PetscFinalize().
         *
         * \return Name given as construction.
         *
         * It should be noticed that inherited class \a GlobalVector may provide a default name if none was given; no
         * default name however is foreseen in current base class.
         */
        const std::string& GetName() const noexcept;

      protected:
        /*!
         * \brief Change the underlying PETSc pointer .
         *
         * \param[in] new_petsc_vector The new underlying PETSc \a Vec (which is a typedef to a pointer).
         *
         * This functionality was introduced after version 3.12 of PETSc: I realized after the non linear code
         * failed that I didn't set it up properly. The functions given to \a SNESSetFunction and \a
         * SNESSetJacobian must work on \a Vec and \a Mat objects handled completely by PETSc; it just happened
         * in prior versions of the library it used up the system matrices and vectors given to \a
         * SolveNonLinear. So these functions now start by creating on the stack respectively a \a GlobalVector
         * and a \a GlobalMatrix; \a ChangeInternal is then called so that these temporary objects handle
         * internally the objects provided by PETSc API.
         *
         */
        void ChangeInternal(Vec new_petsc_vector);

        /*!
         * \brief Tells the type of the current vector.
         *
         * \copydoc doxygen_hide_source_location
         *
         * This is a thin wrapper over [VecGetType](https://petsc.org/release/manualpages/Vec/VecGetType/).
         *
         * \return A PETSc \a [VecType](https://petsc.org/release/manualpages/Vec/VecType/) object which tells which
         * type of vector it is.
         *
         * \internal As the API is not great (returns \a VecType that is a pointer so comparison to a \a VecType such
         *  as \a VECSEQ could yield unexpected result) I make it provate. More sugary methods such as `IsSequential`
         *  or `IsParallel` will be introduced instead.
         *
         */
        VecType GetType(const std::source_location location) const;

      private:
        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Friendship.
        // ============================


        template<Utilities::Access AccessT>
        friend class AccessVectorContent;

        friend class AccessGhostContent;

        friend void Swap(Vector&, Vector&);

        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


      private:
        // ======================================================================================================
        // \attention Do not forget to update Swap() and the recopy constructors if a new data member is added!
        // ======================================================================================================

        //! Underlying Petsc vector.
        Vec petsc_vector_;

        /*!
         * \brief List of program-wise index of values that are ghost (i.e. required
         * processor-wise but owned by another processor).
         *
         * Left empty if sequential or mpi without ghost.
         */
        std::vector<PetscInt> ghost_padding_;

        /*!
         * \brief Whether the underlying Petsc vector will be destroyed upon destruction of the object.
         *
         * Default behaviour is to do so, but in some cases (for instance when vector has been built from
         * an existing Petsc Vec) it is wiser not to.
         */
        call_petsc_destroy do_call_petsc_destroy_ = call_petsc_destroy::yes;

        //! \copydetails doxygen_hide_petsc_vector_name_text
        std::string name_;

        //! Whether there is a print on standard output to inform of the deletion of the \a Matrix.
        Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction_{
            Advanced::Wrappers::Petsc::print_linalg_destruction::no
        };
    };


    /*!
     * \brief Swap two Vectors.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     */
    void Swap(Vector& lhs, Vector& rhs);


    /*!
     * \brief A quick and dirty way to display some values of a petsc vector (for debug purposes)
     *
     * I'm not even sure it works as intended in parallelism context, but it is quite useful in sequential.
     *
     *
     * \param[in,out] stream Stream onto which the values are written.
     * \param[in] vector Vector being investigated.
     * \param[in] first_index First (processor-wise) index to be printed.
     * \param[in] last_index Last (processor-wise) index to be printed.
     * \param[in] rank Mpi rank of the current processor.
     * \copydoc doxygen_hide_source_location
     */
    void DisplaySomeValues(std::ostream& stream,
                           const Vector& vector,
                           PetscInt first_index,
                           PetscInt last_index,
                           rank_type rank,
                           const std::source_location location = std::source_location::current());

    /*!
     * \brief Checks whether Petsc vectors are (almost) equal
     *
     * Note: Petsc proposes VecEqual, but it is not designed to compare two vectors obtained independently
     * (their documentation has been updated following mail exchanges I had with them).
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     * \param[in] epsilon Precision actually required to get an equality (a == b if fabs(a - b) < epsilon)
     * \param[out] inequality_description Description of when the discrepancy happened. Empty if true is
     * returned. \copydoc doxygen_hide_source_location
     *
     * \return True if lhs and rhs are identical at a given numerical imprecision (dubbed \a epsilon).
     */
    bool AreEqual(const Vector& lhs,
                  const Vector& rhs,
                  double epsilon,
                  std::string& inequality_description,
                  const std::source_location location = std::source_location::current());


    /*!
     * \class doxygen_hide_vec_axpy
     *
     * \brief Wrapper over VecAXPY, that performs Y = alpha * X + Y.
     *
     * \copydoc doxygen_hide_do_update_ghost_arg
     *
     * \copydoc doxygen_hide_source_location
     *
     * \param[in] alpha See above formula.
     * \param[in] x See above formula.
     * \param[in] y See above formula.
     */

    //! \copydoc doxygen_hide_vec_axpy
    void AXPY(PetscScalar alpha,
              const Vector& x,
              Vector& y,
              const std::source_location location = std::source_location::current(),
              update_ghost do_update_ghost = update_ghost::yes);


    /*!
     * \class doxygen_hide_dot_product_function
     *
     * \brief Wrapper over VecDot.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \param[in] x First term in scalar product.
     * \param[in] y Second term in scalar product.
     *
     * \attention Petsc man page mentions performance issues with this one.
     * \verbatim
     per-processor memory bandwidth
     interprocessor latency
     work load imbalance that causes certain processes to arrive much earlier than others
     \endverbatim
     *
     * \note This function may work for parallel vectors; no need to reduce the result obtained on each process
     * (on the contrary you would compute number of proc * dot_product...).
     *
     * \return Scalar product of both vectors.
     */

    //! \copydoc doxygen_hide_dot_product_function
    double
    DotProduct(const Vector& x, const Vector& y, const std::source_location location = std::source_location::current());


    /*!
     * \brief Gather several mpi vectors into a sequential one.
     *
     * \note This was used a long time ago in the beginnings of MoReFEM, when ghost values weren't yet
     * implemented. It is kept because it might be useful for debug purposes, but it's a really poor idea to
     * use it: it breaks down the parallel performance...
     *
     * \attention Calling GatherVector in sequential is not efficient at all; you should rather try in client
     * code to distinguish sequential and parallel cases.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \copydetails doxygen_hide_mpi_param
     *
     * \param[in] local_parallel_vector Vector which contains the data concerning the current rank.
     * \param[out] sequential_vector Sequential vector being rebuilt from all the processors.
     * Both vectors are assumed to be already initialized and allocated properly prior to the function call.
     */
    void GatherVector(const Mpi& mpi,
                      const Wrappers::Petsc::Vector& local_parallel_vector,
                      Wrappers::Petsc::Vector& sequential_vector,
                      const std::source_location location = std::source_location::current());


    //! Traits class which provides adequate types depending on the kind of access granted.
    template<Utilities::Access AccessT>
    struct VectorForAccess;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Template class specializations.
    // ============================

    template<>
    struct VectorForAccess<Utilities::Access::read_and_write>
    {
        using Type = Vector;

        using scalar_array_type = PetscScalar*;

        using scalar_type = PetscScalar;
    };


    template<>
    struct VectorForAccess<Utilities::Access::read_only>
    {
        using Type = const Vector;

        using scalar_array_type = const PetscScalar*;

        using scalar_type = const PetscScalar;
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_VECTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
