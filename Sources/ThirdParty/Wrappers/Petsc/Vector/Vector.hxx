// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_VECTOR_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_VECTOR_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstdlib>
#include <vector>

// IWYU pragma: begin_exports
#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Miscellaneous.hpp" // IWYU pragma: keep // IWYU doesn't manage gracefully Utilities::Access enum as forward declaration

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/VectorHelper.hpp"
// IWYU pragma: end_exports


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Wrappers { class Mpi; }
namespace MoReFEM::Wrappers::Petsc { template <Utilities::Access AccessT> class AccessVectorContent; }
namespace MoReFEM { enum class update_ghost; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


    template<Vector::check_non_null_ptr do_check_non_null_ptr>
    inline Vec Vector::Internal([[maybe_unused]] std::source_location location)
    {
        if constexpr (do_check_non_null_ptr == check_non_null_ptr::yes)
            AssertionNS::InternalNotNull(petsc_vector_, location);

        return petsc_vector_;
    }


    template<Vector::check_non_null_ptr do_check_non_null_ptr>
    inline Vec Vector::InternalForReadOnly(std::source_location location) const
    {
        AssertionNS::InternalNotNull(petsc_vector_, location);

        return petsc_vector_;
    }


    template<Utilities::Access AccessT>
    inline void Vector::SetValues(const std::vector<PetscInt>& indexing,
                                  const AccessVectorContent<AccessT>& local_vec,
                                  InsertMode insertOrAppend,
                                  const std::source_location location)
    {
        this->SetValues(indexing, local_vec.GetArray(), insertOrAppend, location);
    }


    template<MpiScale MpiScaleT>
    void Vector::Print(const Mpi& mpi,
                       const FilesystemNS::File& output_file,
                       binary_or_ascii binary_or_ascii_choice,
                       const std::source_location location) const
    {
        if (binary_or_ascii_choice == binary_or_ascii::from_input_data)
            binary_or_ascii_choice = Utilities::AsciiOrBinary::GetInstance().IsBinaryOutput();

        switch (MpiScaleT)
        {
        case MpiScale::program_wise:
        {
            switch (binary_or_ascii_choice)
            {
            case binary_or_ascii::ascii:
                View(mpi, output_file, location, PETSC_VIEWER_ASCII_MATLAB);
                break;
            case binary_or_ascii::binary:
                ViewBinary(mpi, output_file, location);
                break;
            case binary_or_ascii::from_input_data:
            {
                assert(false && "SHould have been handled at the beginning of current method.");
                exit(EXIT_FAILURE);
            }
            }
            break;
        }
        case MpiScale::processor_wise:
            Internal::Wrappers::Petsc::PrintPerProcessor(*this, output_file, location, binary_or_ascii_choice);
            break;
        } // switch
    }


    inline void Vector::UpdateGhosts(const std::source_location location, update_ghost do_update_ghost)
    {
        switch (do_update_ghost)
        {
        case update_ghost::yes:
            UpdateGhosts(location);
            break;
        case update_ghost::no:
            break;
        }
    }


    inline const std::vector<PetscInt>& Vector::GetGhostPadding() const noexcept
    {
        assert(!ghost_padding_.empty() && "Should only be called for Vector initialized with ghosts!");
        return ghost_padding_;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_VECTOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
