// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#ifdef MOREFEM_WITH_SLEPC

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Slepc/EigenPair.hpp"


namespace MoReFEM::Wrappers::Slepc
{


    EigenPair::EigenPair(const Wrappers::Petsc::Matrix& matrix, const std::source_location location)
    {
        Vec vec = nullptr;
        const int error_code = MatCreateVecs(matrix.InternalForReadOnly(location), MOREFEM_PETSC_NULL, &vec);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatCreateVecs", location);

        eigen_vector_ =
            std::make_unique<Wrappers::Petsc::Vector>(vec,
                                                      Advanced::Wrappers::Petsc::call_petsc_destroy::yes,
                                                      "Eigen vector in pair",
                                                      Advanced::Wrappers::Petsc::print_linalg_destruction::no);
    }


    void EigenPair::SetEigenValue(PetscScalar eigenvalue)
    {
        eigenvalue_ = static_cast<double>(eigenvalue);
    }


} // namespace MoReFEM::Wrappers::Slepc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // MOREFEM_WITH_SLEPC
