// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_EIGENPAIR_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_EIGENPAIR_DOT_HPP_
// *** MoReFEM header guards *** < //

#ifdef MOREFEM_WITH_SLEPC

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp" // IWYU pragma: export // IWYU pragma: keep

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Slepc { class Eps; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Slepc
{


    /*!
     *\brief This class wraps a pair of eigenvector and eigenvalue.
     *
     *
     */
    class EigenPair final
    {
      public:
        //! Unique smart pointer.
        using unique_ptr = std::unique_ptr<EigenPair>;

        //! Friendhip to \a Eps, to access non constant vector.
        friend class Eps;

        /// \name Constructors and destructor.
        ///@{

      private:
        /*!
         * \brief Constructor.
         *
         * \param[in] matrix Eigensystem matrix
         * \copydoc doxygen_hide_source_location
         *
         * The constructor is private as it is intended to be used only internally in EPS class;
         * a developer is expected to use a fully formed object obtained through Eps::GetEigenPair().
         */
        EigenPair(const Wrappers::Petsc::Matrix& matrix,
                  const std::source_location location = std::source_location::current());

      public:
        //! Destructor.
        ~EigenPair() = default;

        //! \copydoc doxygen_hide_copy_constructor
        EigenPair(const EigenPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        EigenPair(EigenPair&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        EigenPair& operator=(const EigenPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        EigenPair& operator=(EigenPair&& rhs) = delete;

        ///@}

        /*!
         * \brief Accessor to the eigenvector.
         *
         * \return The eigenvector.
         */
        const Wrappers::Petsc::Vector& GetEigenVector() const noexcept;

        /*!
         * \brief Accessor to the eigenvalue.
         *
         * \return The eigenvalue.
         */
        double GetEigenValue() const noexcept;


      private:
        /*!
         * \brief Non constant accessor to the eigenvector.
         *
         * \return The eigenvector.
         */
        Wrappers::Petsc::Vector& GetNonCstEigenVector() noexcept;

        /*!
         * \brief Mutator to the eigenvalue.
         *
         * \param[in] eigenvalue New value.
         */
        void SetEigenValue(PetscScalar eigenvalue);


      private:
        //! Eigenvector.
        Wrappers::Petsc::Vector::unique_ptr eigen_vector_{ nullptr };

        //! Eigenvalue.
        double eigenvalue_{};
    };


} // namespace MoReFEM::Wrappers::Slepc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

#include "ThirdParty/Wrappers/Slepc/EigenPair.hxx" // IWYU pragma: export

#endif // MOREFEM_WITH_SLEPC

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_EIGENPAIR_DOT_HPP_
// *** MoReFEM end header guards *** < //
