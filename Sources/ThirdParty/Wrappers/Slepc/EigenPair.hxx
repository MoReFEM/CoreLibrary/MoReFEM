// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_EIGENPAIR_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_EIGENPAIR_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Slepc/EigenPair.hpp"
// *** MoReFEM header guards *** < //


#ifdef MOREFEM_WITH_SLEPC

#include <cassert>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Petsc { class Vector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Slepc
{


    inline const Wrappers::Petsc::Vector& EigenPair::GetEigenVector() const noexcept
    {
        assert(!(!eigen_vector_));
        return *eigen_vector_;
    }


    inline Wrappers::Petsc::Vector& EigenPair::GetNonCstEigenVector() noexcept
    {
        return const_cast<Wrappers::Petsc::Vector&>(GetEigenVector());
    }


    inline double EigenPair::GetEigenValue() const noexcept
    {
        return eigenvalue_;
    }


} // namespace MoReFEM::Wrappers::Slepc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // MOREFEM_WITH_SLEPC

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_EIGENPAIR_DOT_HXX_
// *** MoReFEM end header guards *** < //
