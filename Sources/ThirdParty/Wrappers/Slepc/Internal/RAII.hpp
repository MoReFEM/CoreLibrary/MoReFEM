// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_INTERNAL_RAII_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_INTERNAL_RAII_DOT_HPP_
// *** MoReFEM header guards *** < //

#ifdef MOREFEM_WITH_SLEPC

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::SlepcNS
{


    /*!
     * \brief RAII class to initialize / close properly Slepc.
     *
     * Only relevant if MOREFEM_WITH_SLEPC build option is set to true.
     */
    class RAII final : public Utilities::Singleton<RAII>
    {

      private:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        explicit RAII();

        //! Destructor.
        virtual ~RAII() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<RAII>;

        //! Name of the class.
        static const std::string& ClassName();

        ///@}
    };


} // namespace MoReFEM::Internal::SlepcNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
#endif // MOREFEM_WITH_SLEPC

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_INTERNAL_RAII_DOT_HPP_
// *** MoReFEM end header guards *** < //
