// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <functional>
#include <optional>
#include <source_location>
#include <utility>

#include "Utilities/Mpi/Mpi.hpp"
#ifdef MOREFEM_WITH_SLEPC


// IWYU pragma: no_include <__tree>
#include <cassert>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Slepc/Solver/Eps.hpp"


namespace MoReFEM::Wrappers::Slepc
{


    Eps::Eps(const Mpi& mpi,
             const Wrappers::Petsc::Matrix& eigensystem_matrix,
             problem_type chosen_problem_type,
             type chosen_type,
             ::MoReFEM::Wrappers::Petsc::solver_name_type petsc_solver_name,
             ::MoReFEM::Wrappers::Petsc::preconditioner_name_type petsc_preconditioner_name,
             const std::source_location location)
    : Crtp::CrtpMpi<Eps>(mpi), eigensystem_matrix_(eigensystem_matrix)
    {
        Construct(eigensystem_matrix,
                  MOREFEM_PETSC_NULL,
                  chosen_problem_type,
                  chosen_type,
                  location,
                  std::move(petsc_solver_name),
                  std::move(petsc_preconditioner_name));
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    Eps::Eps(const Mpi& mpi,
             const Wrappers::Petsc::Matrix& eigensystem_matrix,
             const Wrappers::Petsc::Matrix& eigensystem_second_matrix,
             problem_type chosen_problem_type,
             type chosen_type,
             ::MoReFEM::Wrappers::Petsc::solver_name_type petsc_solver_name,
             ::MoReFEM::Wrappers::Petsc::preconditioner_name_type petsc_preconditioner_name,
             const std::source_location location)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    : Crtp::CrtpMpi<Eps>(mpi), eigensystem_matrix_(eigensystem_matrix),
      second_matrix_for_generalized_eigenproblems_(eigensystem_second_matrix)
    {
        decltype(auto) second_matrix = GetSecondMatrix();

        Construct(eigensystem_matrix,
                  second_matrix.InternalForReadOnly(location),
                  chosen_problem_type,
                  chosen_type,
                  location,
                  std::move(petsc_solver_name),
                  std::move(petsc_preconditioner_name));
    }


    void Eps::Construct(const Wrappers::Petsc::Matrix& eigensystem_matrix,
                        Mat second_matrix,
                        problem_type chosen_problem_type,
                        type chosen_type,
                        const std::source_location location,
                        ::MoReFEM::Wrappers::Petsc::solver_name_type&& petsc_solver_name,
                        ::MoReFEM::Wrappers::Petsc::preconditioner_name_type&& petsc_preconditioner_name)
    {
        decltype(auto) mpi = GetMpi();
        auto* communicator = mpi.GetCommunicator();

        int error_code = EPSCreate(communicator, &eps_);
        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSCreate", location);

        auto* eps = Internal();

        error_code = EPSSetOperators(eps, eigensystem_matrix.InternalForReadOnly(location), second_matrix);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSSetOperators", location);

        {
            const EPSProblemType eps_problem_type = InternalMatching(chosen_problem_type);
            error_code = EPSSetProblemType(eps, eps_problem_type);
        }

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSSetProblemType", location);

        {
            EPSType eps_type = InternalMatching(chosen_type);
            error_code = EPSSetType(eps, eps_type);
        }

        SetEigenSpectrum(which_type::smallest_magnitude, location);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSSetType", location);

        SetPetscSolver(std::move(petsc_solver_name), std::move(petsc_preconditioner_name), location);

        work_real_eigenpair_ = Internal::WrapUnique(new EigenPair(eigensystem_matrix, location));
        work_imaginary_eigenpair_ = Internal::WrapUnique(new EigenPair(eigensystem_matrix, location));
    }


    Eps::~Eps()
    {
        [[maybe_unused]] const int error_code = EPSDestroy(&eps_);
        assert(error_code == 0); // Can't throw exception in a destructor.
    }


    void Eps::Solve(const std::source_location location, std::optional<std::size_t> Neigenvalues_to_compute)
    {
        auto* eps = Internal();

        int error_code = 0;

        if (Neigenvalues_to_compute.has_value())
        {
            error_code = EPSSetDimensions(
                eps, static_cast<PetscInt>(Neigenvalues_to_compute.value()), PETSC_DEFAULT, PETSC_DEFAULT);

            if (error_code)
                throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSSetDimensions", location);
        }

        error_code = EPSSolve(eps);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSSolve", location);
    }


    std::size_t Eps::GetIterationNumber(const std::source_location location) const
    {
        PetscInt ret = 0;
        const int error_code = EPSGetIterationNumber(InternalForReadOnly(), &ret);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetIterationNumber", location);

        return static_cast<std::size_t>(ret);
    }


    std::size_t Eps::GetLinearIterationNumber(const std::source_location location) const
    {
        PetscInt ret = 0;

        ST st = nullptr;
        KSP ksp = nullptr;

        int error_code = EPSGetST(InternalForReadOnly(), &st);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetST", location);

        error_code = STGetKSP(st, &ksp);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "STGetKSP", location);

        error_code = KSPGetTotalIterations(ksp, &ret);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "KSPGetTotalIterations", location);

        return static_cast<std::size_t>(ret);
    }


    std::string Eps::GetType(const std::source_location location) const
    {
        static_assert(std::is_same<EPSType, const char*>(),
                      "Workaround implemented to avoid consequences of this choice so better knowing "
                      "if it changes...");

        EPSType ret = nullptr;
        const int error_code = EPSGetType(InternalForReadOnly(), &ret);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetType", location);

        return { ret };
    }


    std::size_t Eps::NeigenValues(const std::source_location location) const
    {
        PetscInt ret = 0;

        const int error_code = EPSGetDimensions(InternalForReadOnly(), &ret, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetDimensions", location);

        return static_cast<std::size_t>(ret);
    }


    double Eps::GetConvergenceTolerance(const std::source_location location) const
    {
        // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
        PetscScalar ret;
        const int error_code = EPSGetTolerances(InternalForReadOnly(), &ret, MOREFEM_PETSC_NULL);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetTolerances", location);

        return static_cast<double>(ret);
    }


    std::size_t Eps::NmaxIterations(const std::source_location location) const
    {
        PetscInt ret = 0;

        const int error_code = EPSGetTolerances(InternalForReadOnly(), MOREFEM_PETSC_NULL, &ret);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetTolerances", location);

        return static_cast<std::size_t>(ret);
    }


    std::size_t Eps::NconvergedEigenPairs(const std::source_location location) const
    {
        PetscInt ret = 0;

        const int error_code = EPSGetConverged(InternalForReadOnly(), &ret);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetConverged", location);

        return static_cast<std::size_t>(ret);
    }


    double Eps::ComputeRelativeError(std::size_t index, const std::source_location location) const
    {
        // NOLINTNEXTLINE(cppcoreguidelines-init-variables)
        PetscScalar ret;
        const int error_code =
            EPSComputeError(InternalForReadOnly(), static_cast<PetscInt>(index), EPS_ERROR_RELATIVE, &ret);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSComputeError", location);

        return static_cast<double>(ret);
    }


    std::pair<const EigenPair&, const EigenPair&> Eps::GetEigenPair(std::size_t index,
                                                                    const std::source_location location) const
    {
        decltype(auto) real_eigenpair = GetNonCstWorkRealEigenPair();
        decltype(auto) imaginary_eigenpair = GetNonCstWorkImaginaryEigenPair();

        assert(index < NconvergedEigenPairs(location));

        PetscScalar real_eigenvalue{};
        PetscScalar imaginary_eigenvalue{};

#if defined(PETSC_USE_COMPLEX)
        {
            static_assert(false,
                          "If this is used, EPSGetEigenpair first vector and value are complex ones, "
                          "and second ones are not considered. I don't have time now to check all is right "
                          "in this case, so at the moment it makes compilation break. If you need to use "
                          "MoReFEM with Slepc and Petsc with complex, please open an issue in MoReFEM.");
        }
#endif


        const int error_code = EPSGetEigenpair(InternalForReadOnly(),
                                               static_cast<PetscInt>(index),
                                               &real_eigenvalue,
                                               &imaginary_eigenvalue,
                                               real_eigenpair.GetNonCstEigenVector().Internal(location),
                                               imaginary_eigenpair.GetNonCstEigenVector().Internal(location));

        real_eigenpair.SetEigenValue(real_eigenvalue);
        imaginary_eigenpair.SetEigenValue(imaginary_eigenvalue);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetEigenpair", location);

        return std::make_pair(std::ref(real_eigenpair), std::ref(imaginary_eigenpair));
    }


    const Wrappers::Petsc::Matrix& Eps::GetSecondMatrix() const noexcept
    {
        assert(second_matrix_for_generalized_eigenproblems_.has_value());
        if (!second_matrix_for_generalized_eigenproblems_.has_value())
        {
            std::cerr << "No second matrix defined but it was called - please check first your models "
                         "in debug mode!"
                      << '\n';
            exit(EXIT_FAILURE);
        }

        return second_matrix_for_generalized_eigenproblems_.value();
    }


    EPSProblemType Eps::InternalMatching(problem_type chosen_problem_type)
    {
        auto it = matching_problem_type_.find(chosen_problem_type);

        assert(it != matching_problem_type_.cend());

        return it->second;
    }


    EPSType Eps::InternalMatching(type chosen_type)
    {
        auto it = matching_type_.find(chosen_type);

        assert(it != matching_type_.cend());

        return it->second;
    }


    EPSWhich Eps::InternalMatching(which_type which)
    {
        auto it = matching_which_.find(which);

        assert(it != matching_which_.cend());

        return it->second;
    }


    void Eps::SetEigenSpectrum(which_type which, const std::source_location location)
    {
        auto eps_which = InternalMatching(which);

        const int error_code = EPSSetWhichEigenpairs(Internal(), eps_which);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSSetWhichEigenpairs", location);
    }


    void Eps::SetPetscSolver(::MoReFEM::Wrappers::Petsc::solver_name_type&& petsc_solver_name,
                             ::MoReFEM::Wrappers::Petsc::preconditioner_name_type&& petsc_preconditioner_name,
                             const std::source_location location)
    {
        {
            decltype(auto) solver_factory = Internal::Wrappers::Petsc::SolverNS::Factory::CreateOrGetInstance();
            Internal::Wrappers::Petsc::SolverNS::Settings solver_settings(std::move(petsc_solver_name),
                                                                          std::move(petsc_preconditioner_name));
            solver_ = solver_factory.Create(std::move(solver_settings));
        }

        ST st = nullptr;
        KSP ksp = nullptr;
        PC pc = nullptr;

        int error_code = EPSGetST(Internal(), &st);
        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "EPSGetST", location);

        error_code = STGetKSP(st, &ksp);
        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "STGetKSP", location);

        error_code = KSPGetPC(ksp, &pc);
        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "KSPGetPC", location);

        error_code = PCSetType(pc, GetSolver().GetSettings().GetPreconditionerName().Get().c_str());

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "PCSetType", location);

        error_code = PCFactorSetMatSolverType(pc, GetSolver().GetSettings().GetSolverName().Get().c_str());

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "PCFactorSetMatSolverType", location);
    }


} // namespace MoReFEM::Wrappers::Slepc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // MOREFEM_WITH_SLEPC
