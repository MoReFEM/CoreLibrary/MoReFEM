// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_SOLVER_EPS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_SOLVER_EPS_DOT_HPP_
// *** MoReFEM header guards *** < //

#ifdef MOREFEM_WITH_SLEPC

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <utility>

#include "Utilities/Mpi/Mpi.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"                      // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"            // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Slepc/EigenPair.hpp"              // IWYU pragma: export


namespace MoReFEM::Wrappers::Slepc
{


    /*!
     * \brief Enum class to wrap the different ProblemType from Slepc.
     *
     * The original problem types are:
     * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSProblemType.html
     */
    enum class problem_type {
        hermitian,
        generalized_hermitian,
        non_hermitian,
        generalized_non_hermitian,
        positive_generalized_non_hermitian,
        indefinite
    };


    /*!
     * \brief Enum class to wrap the different Type from Slepc.
     *
     * The original problem types are:
     * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSType.html
     */
    enum class type {
        power,
        subspace,
        arnoldi,
        lanczos,
        krylovschur,
        gd,
        jd,
        rqcg,
        lobpcg,
        ciss,
        lyapii,
        lapack,
        arpack,
        trlan,
        blopex,
        primme,
        feast,
        scalapack,
        elpa,
        elemental,
        evsl
    };


    /*!
     * \brief Enum class to wrap the different ways to sort eigenvalues in Slepc.
     *
     * The original values are:
     * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSWhich.html
     *
     * \attention At the moment I provide only two choices; if you need more please contact me or submit an issue.
     *
     * \internal If new entries are added make sure to update accordingly \a matching_which.
     */
    enum class which_type { largest_magnitude, smallest_magnitude };


    /*!
     * \class doxygen_hide_eps_problem_type_param
     *
     * \param[in] chosen_problem_type The type of eigen problem you want to solve. Make sure your choice is
     * sensical: if you provide a wrong value (for instance an hermitian problem when a non-hermitian is expected) the
     * error message given by Slepc is rather terse and not explicit enough,
     */


    /*!
     * \class doxygen_hide_eps_type_param
     *
     * \param[in] chosen_type The type of the solver.
     */


    /*!
     * \class doxygen_hide_eps_petsc_solver_name
     *
     * \param[in] petsc_solver_name Name of the PETSc solver to use under the hood. Default is 'SuperLU_dist'
     * (default in Slepc was Mumps but Mumps doesn't behave properly in parallel on macOS on its more recent versions
     * and the devs were unable to fix the issue).
     * \param[in] petsc_preconditioner_name Name of the preconditioner to use.
     */


    /*!
     * \class doxygen_hide_eps_which_param
     *
     * \param[in] which Portion of the spectrum of eigenvalues is to be sought.
     */


    /*!
     *\brief This class wraps all the Slepc objects related to EPS.
     *
     */
    class Eps final : public Crtp::CrtpMpi<Eps>
    {
      public:
        //! Unique smart pointer.
        using unique_ptr = std::unique_ptr<Eps>;

        /// \name Constructors and destructor.
        ///@{

        /*!
         * \brief Constructor for Ax=kx eigensystems.
         *
         * Please notice that contrary to Slepc internal choice, by default the sorting criterion chosen is to put
         * smallest eigenvalues first.
         *
         * \copydoc doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] eigensystem_matrix Matrix associated to the eigensystem - A in the example above.
         * \copydoc doxygen_hide_eps_problem_type_param
         *
         * \copydoc doxygen_hide_eps_type_param
         *
         * \copydoc doxygen_hide_eps_petsc_solver_name
         */
        Eps(const Mpi& mpi,
            const Wrappers::Petsc::Matrix& eigensystem_matrix,
            problem_type chosen_problem_type,
            type chosen_type = type::krylovschur,
            ::MoReFEM::Wrappers::Petsc::solver_name_type petsc_solver_name =
                ::MoReFEM::Wrappers::Petsc::solver_name_type{ "SuperLU_dist" },
            ::MoReFEM::Wrappers::Petsc::preconditioner_name_type petsc_preconditioner_name =
                ::MoReFEM::Wrappers::Petsc::preconditioner_name_type{ PCLU },
            const std::source_location location = std::source_location::current());

        /*!
         * \brief Constructor for Ax=kBx eigensystems.
         *
         * Please notice that contrary to Slepc internal choice, by default the sorting criterion chosen is to put
         * smallest eigenvalues first.
         *
         * \copydoc doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] eigensystem_matrix Matrix associated to the eigensystem - A in the example above.
         * \param[in] eigensystem_second_matrix Second matrix associated to the eigensystem - B in the example above.
         * \copydoc doxygen_hide_eps_problem_type_param
         *
         * \copydoc doxygen_hide_eps_type_param
         *
         * \copydoc doxygen_hide_eps_petsc_solver_name
         */
        Eps(const Mpi& mpi,
            const Wrappers::Petsc::Matrix& eigensystem_matrix,
            const Wrappers::Petsc::Matrix& eigensystem_second_matrix,
            problem_type chosen_problem_type,
            type chosen_type = type::krylovschur,
            ::MoReFEM::Wrappers::Petsc::solver_name_type petsc_solver_name =
                ::MoReFEM::Wrappers::Petsc::solver_name_type{ "SuperLU_dist" },
            ::MoReFEM::Wrappers::Petsc::preconditioner_name_type petsc_preconditioner_name =
                ::MoReFEM::Wrappers::Petsc::preconditioner_name_type{ PCLU },
            const std::source_location location = std::source_location::current());


        //! Destructor.
        ~Eps();

        //! \copydoc doxygen_hide_copy_constructor
        Eps(const Eps& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Eps(Eps&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Eps& operator=(const Eps& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Eps& operator=(Eps&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Solve the eigensystem.
         *
         * This is a thin wrapper over
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSSolve.html
         *
         * \copydoc doxygen_hide_source_location
         * \param[in] Neigenvalues_to_compute If specified, number of eigenvalues to compute.
         */
        void Solve(const std::source_location location = std::source_location::current(),
                   std::optional<std::size_t> Neigenvalues_to_compute = std::nullopt);


        /*!
         * \brief Specifies which portion of the spectrum is to be sought.
         *
         * This is a thin wrapper over
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSSetWhichEigenpairs.html
         *
         * \attention Please notice that contrary to Slepc internal choice, by default the sorting criterion chosen is to put smallest eigenvalues first.
         *
         * \copydoc doxygen_hide_eps_which_param
         * \copydoc doxygen_hide_source_location
         */
        void SetEigenSpectrum(which_type which, const std::source_location location = std::source_location::current());


      private:
        /*!
         * \brief Returns the \a EPS object used by Slepc.
         *
         * \internal The type is returned by value directly as it is in fact a typedef to a pointer.
         *
         * \return The underlying Slepc object.
         */
        EPS Internal() noexcept;

        /*!
         * \brief Returns the \a EPS object used by Slepc in const methods.
         *
         * \internal The type is returned by value directly as it is in fact a typedef to a pointer.
         *
         * \return The underlying Slepc object.
         */

        EPS InternalForReadOnly() const noexcept;

      public:
        //! Accessor to the eigensystem matrix.
        const Wrappers::Petsc::Matrix& GetMatrix() const noexcept;

        /*!
         * \brief Accessor to the eigensystem second matrix.
         *
         * i.e. B in Ax=kBx eigensystems. If it is not defined, an assert is issued in debug mode (and there is
         * an abortion in any case).
         */
        const Wrappers::Petsc::Matrix& GetSecondMatrix() const noexcept;


        /*!
         * \brief Returns the iteration number.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Iteration number as computed by
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSGetIterationNumber.html
         */
        std::size_t GetIterationNumber(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Returns the number of linear iterations of the method.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return The value returned by the PETSc function
         * https://petsc.org/release/docs/manualpages/KSP/KSPGetTotalIterations/
         * called for the KSP object related to the EPS eigenproblem.
         *
         * \attention I have implemented it quickly to be able to propose a MoReFEM API over the hands on 3 proposed
         * on Slepc website. I do not guarantee it works in any case - but if if doesn't there will be anyway the
         * handling of error by PETSc and/or Slepc (depending of the operation that fails).
         *
         * \internal If more operations need the underlying KSP object, a dedicated methods should of course be
         *  created.
         */
        std::size_t
        GetLinearIterationNumber(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Returns the type of eigensystem solver.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Type as computed by
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSGetType.html
         *
         * Slepc type EPSType is in fact an alias toward... a const char*, which may lead to very problematic behaviour
         * (the local variable may have disappeared when we reach calling site). This is the reason a \a std::string has
         * been chosen here instead; if you need to provide a EPSType to a Slepc function just call `c_str()` method
         * upon the result here.
         */
        std::string GetType(const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Returns the number of eigenvalues to compute.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Number of eigenvalues to compute, as computed by
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSGetDimensions.html
         */
        std::size_t NeigenValues(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Returns the tolerance used by the EPS convergence tests.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Tolerance, as computed by
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSGetTolerances.html
         */
        double GetConvergenceTolerance(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Returns the maximum iteration counts  used by the EPS convergence tests.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Maximum iteration counts, as computed by
         *  https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSGetTolerances.html
         */
        std::size_t NmaxIterations(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Returns the number of converged eigenpairs.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Number of converged eigenpairs., as computed by
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSGetConverged.html
         */
        std::size_t NconvergedEigenPairs(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief  Returns the eigen vector and eigenvalues of the \a index -th solution.
         *
         * \param[in] index Index of the sought solution.
         * \copydoc doxygen_hide_source_location
         *
         * \return A pair of \a EigenPair: first one represents real part and second one imaginary part.
         *
         * This is a sophisticated wrapper over
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSGetEigenpair.html
         *
         * \attention To be as efficient as possible, the \a Wrappers::Petsc::Vector used to store the solution are
         * work variables which memory layout has been allocated once and for all in the object construction. The value
         * for current \a index are copied inside; if you call this function again for another index they will be
         * modified.
         *
         */
        std::pair<const EigenPair&, const EigenPair&>
        GetEigenPair(std::size_t index, const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Computes the error (based on the residual norm) associated with the \a index -th computed eigenpair.
         *
         * \param[in] index Index of the sought solution.
         * \copydoc doxygen_hide_source_location
         *
         * \return The relative error as computed by
         * https://slepc.upv.es/documentation/current/docs/manualpages/EPS/EPSComputeError.html
         */
        double ComputeRelativeError(std::size_t index,
                                    const std::source_location location = std::source_location::current()) const;


      private:
        //! Non constant accessor to the work variable which stores the real part of an eigensystem solution.
        //! Should not be used directly: it is intended as an internal helper for \a GetEigenPair().
        EigenPair& GetNonCstWorkRealEigenPair() const noexcept;

        //! Non constant accessor to the work variable which stores the imaginary part of an eigensystem solution.
        //! Should not be used directly: it is intended as an internal helper for \a GetEigenPair().
        EigenPair& GetNonCstWorkImaginaryEigenPair() const noexcept;

        /*!
         * \brief Helper method to construct the object.
         *
         * There are (at the time of this writing) two constructors for the object: one which specifies one matrix by
         reference,
         * and the other one that specify two of thems, also by reference.
         * In the underlying Slepc API, pointers are needed.
         * The role of this method is to take the pointers as parameters, so same code has not to be duplicated between
         both.
         *
         * So its intent is to be called by the constructors to make most of the construction job.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] eigensystem_matrix Matrix associated to the eigensystem - A in Ax=kx or Ax=kbx eigensystems.
         * \param[in] second_matrix Second matrix associated to the eigensystem - B in Ax=kBx eigensystems. Choose
         MOREFEM_PETSC_NULL
         * if you're considering a Ax=kx eigensystem. Given in native PETSc format to enable this later case...
         *
         * \copydoc doxygen_hide_eps_problem_type_param
         *
         * \copydoc doxygen_hide_eps_type_param
         *
         * \copydoc doxygen_hide_eps_petsc_solver_name
         *
         * Please notice that contrary to Slepc internal choice, by default the sorting criterion chosen is to put
         smallest eigenvalues first.
         */
        void Construct(const Wrappers::Petsc::Matrix& eigensystem_matrix,
                       Mat second_matrix,
                       problem_type chosen_problem_type,
                       type chosen_type,
                       const std::source_location location,
                       ::MoReFEM::Wrappers::Petsc::solver_name_type&& petsc_solver_name,
                       ::MoReFEM::Wrappers::Petsc::preconditioner_name_type&& petsc_preconditioner_name);


        //! Internal matching between enum class \a problem_type and Slepc internal structure.
        //! \copydoc doxygen_hide_eps_problem_type_param
        static EPSProblemType InternalMatching(problem_type chosen_problem_type);

        //! Internal matching between enum class \a type and Slepc internal structure.
        //! \copydoc doxygen_hide_eps_type_param
        static EPSType InternalMatching(type chosen_type);

        /*!
         * \brief Internal matching between enum class \a which_type and Slepc internal structure.
         *
         * \copydoc doxygen_hide_eps_which_param
         *
         * \return The Slepc enum.
         */
        static EPSWhich InternalMatching(which_type which);


        /*!
         * \brief Set the PETSc solver used under the hood by Slepc.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_eps_petsc_solver_name
         */
        void SetPetscSolver(::MoReFEM::Wrappers::Petsc::solver_name_type&& petsc_solver_name,
                            ::MoReFEM::Wrappers::Petsc::preconditioner_name_type&& petsc_preconditioner_name,
                            const std::source_location location = std::source_location::current());

      private:
        //! Access to the object that holds information specific to the solver chosen.
        const Internal::Wrappers::Petsc::Solver& GetSolver() const noexcept;

        //! Non constant access to the object that holds information specific to the solver chosen.
        Internal::Wrappers::Petsc::Solver& GetNonCstSolver() noexcept;

      private:
        //! Slepc's underlying object.
        EPS eps_{};

        //! Object that holds information specific to the solver chosen.
        Internal::Wrappers::Petsc::Solver::unique_ptr solver_{ nullptr };

        //! The matrix associated with the eigensystem
        const Wrappers::Petsc::Matrix& eigensystem_matrix_;

        //! The second matrix in the case of generalized eigenproblems.
        const std::optional<std::reference_wrapper<const Wrappers::Petsc::Matrix>>
            second_matrix_for_generalized_eigenproblems_ = std::nullopt;

        //! Work variable which stores the real part of an eigensystem solution.
        mutable EigenPair::unique_ptr work_real_eigenpair_{ nullptr };

        //! Work variable which stores the imaginary part of an eigensystem solution.
        mutable EigenPair::unique_ptr work_imaginary_eigenpair_{ nullptr };

        // clang-format off
        /*!
         * \brief Matching between our enum class and Slepc enum for problem_type.
         */
        static inline std::map<problem_type, EPSProblemType> matching_problem_type_
        {
            { problem_type::hermitian, EPS_HEP },
            { problem_type::generalized_hermitian, EPS_GHEP },
            { problem_type::non_hermitian, EPS_NHEP },
            { problem_type::generalized_non_hermitian, EPS_GNHEP },
            { problem_type::positive_generalized_non_hermitian, EPS_PGNHEP },
            { problem_type::indefinite, EPS_GHIEP }
        };


        /*!
         * \brief Matching between our enum class and Slepc enum for type.
         */
        static inline std::map<type, EPSType> matching_type_
        {
            { type::power, EPSPOWER },
            { type::subspace, EPSSUBSPACE },
            { type::arnoldi, EPSARNOLDI },
            { type::lanczos, EPSLANCZOS },
            { type::krylovschur, EPSKRYLOVSCHUR },
            { type::gd, EPSGD },
            { type::jd, EPSJD },
            { type::rqcg, EPSRQCG },
            { type::lobpcg, EPSLOBPCG },
            { type::ciss, EPSCISS },
            { type::lyapii, EPSLYAPII },
            { type::lapack, EPSLAPACK },
            { type::arpack, EPSARPACK },
            { type::trlan, EPSTRLAN },
            { type::blopex, EPSBLOPEX },
            { type::primme, EPSPRIMME },
            { type::feast, EPSFEAST },
            { type::scalapack, EPSSCALAPACK },
            { type::elpa, EPSELPA },
            { type::elemental, EPSELEMENTAL },
            { type::evsl, EPSEVSL },

        };


        /*!
         * \brief Matching between our enum class and Slepc enum for sorting criterions.
         */
        static inline std::map<which_type, EPSWhich> matching_which_
        {
            { which_type::largest_magnitude,  EPS_LARGEST_MAGNITUDE },
            { which_type::smallest_magnitude,  EPS_SMALLEST_MAGNITUDE },
        };
        // clang-format on
    };


} // namespace MoReFEM::Wrappers::Slepc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Slepc/Solver/Eps.hxx" // IWYU pragma: export

#endif // MOREFEM_WITH_SLEPC

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_SOLVER_EPS_DOT_HPP_
// *** MoReFEM end header guards *** < //
