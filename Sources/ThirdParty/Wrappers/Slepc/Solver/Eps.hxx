// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_SOLVER_EPS_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_SOLVER_EPS_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Slepc/Solver/Eps.hpp"
// *** MoReFEM header guards *** < //


#ifdef MOREFEM_WITH_SLEPC

#include <cassert>
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Petsc { class Matrix; }
namespace MoReFEM::Wrappers::Slepc { class EigenPair; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Slepc
{


    inline EPS Eps::Internal() noexcept
    {
        return eps_;
    }


    inline const Wrappers::Petsc::Matrix& Eps::GetMatrix() const noexcept
    {
        return eigensystem_matrix_;
    }


    inline EPS Eps::InternalForReadOnly() const noexcept
    {
        return eps_;
    }


    inline EigenPair& Eps::GetNonCstWorkRealEigenPair() const noexcept
    {
        assert(!(!work_real_eigenpair_));
        return *work_real_eigenpair_;
    }

    inline EigenPair& Eps::GetNonCstWorkImaginaryEigenPair() const noexcept
    {
        assert(!(!work_imaginary_eigenpair_));
        return *work_imaginary_eigenpair_;
    }


    inline auto Eps::GetSolver() const noexcept -> const Internal::Wrappers::Petsc::Solver&
    {
        assert(!(!solver_));
        return *solver_;
    }

    inline auto Eps::GetNonCstSolver() noexcept -> Internal::Wrappers::Petsc::Solver&
    {
        return const_cast<Internal::Wrappers::Petsc::Solver&>(GetSolver());
    }


} // namespace MoReFEM::Wrappers::Slepc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // MOREFEM_WITH_SLEPC

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_SLEPC_SOLVER_EPS_DOT_HXX_
// *** MoReFEM end header guards *** < //
