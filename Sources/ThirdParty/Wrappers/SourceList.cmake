### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Eigen/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Libmesh/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Lua/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Mpi/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Parmetis/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Petsc/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Slepc/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Tclap/SourceList.cmake)
