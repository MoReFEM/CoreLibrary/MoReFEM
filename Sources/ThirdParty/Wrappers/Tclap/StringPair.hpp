// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_TCLAP_STRINGPAIR_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_TCLAP_STRINGPAIR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string> // IWYU pragma: keep
#include <utility>
// IWYU pragma: no_include <iosfwd>

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"


namespace MoReFEM::Wrappers::Tclap
{


    /*!
     * \brief This class aims to provide a new accepted type on command lines like: '-e FOO=BAR'.
     *
     * Internally, the string "FOO=BAR" is read and assigned in a std::pair<std::string, std::string>.
     * The class is minimalist on purpose: it is intended to be used only in Tclap context.
     */
    class StringPair
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = StringPair;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit StringPair() = default;

        /*!
         * \brief The only useful one: assignment from a std::string.
         *
         * \param[in] rhs String which is converted into a Pair object. Expected format is a string without
         * spaces with a '=' somewhere. An empty string is accepted as well and leave the internal pair empty.
         *
         * \return Reference to the \a Pair object updated with the content from \a rhs.
         */
        StringPair& operator=(std::string rhs);

        //! Destructor.
        ~StringPair() = default;

        //! \copydoc doxygen_hide_copy_constructor
        StringPair(const StringPair& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        StringPair(StringPair&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        StringPair& operator=(const StringPair& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        StringPair& operator=(StringPair&& rhs) = default;

        ///@}

        /*!
         * \brief Accessor to the underlying pair.
         *
         * \return The pair key/value read from the command line.
         */
        const std::pair<std::string, std::string>& GetValue() const noexcept;

      private:
        //! The underlying pair.
        std::pair<std::string, std::string> value_;
    };


} // namespace MoReFEM::Wrappers::Tclap


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

namespace TCLAP
{


    /*!
     * \brief Traits class to enable the use of StringPair in TClap.
     */
    template<>
    struct ArgTraits<MoReFEM::Wrappers::Tclap::StringPair>
    {

        //! The value to set for the traits class.
        using ValueCategory = StringLike;
    };


} // namespace TCLAP


#include "ThirdParty/Wrappers/Tclap/StringPair.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_TCLAP_STRINGPAIR_DOT_HPP_
// *** MoReFEM end header guards *** < //
