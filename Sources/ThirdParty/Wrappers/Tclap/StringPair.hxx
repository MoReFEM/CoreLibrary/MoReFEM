// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_TCLAP_STRINGPAIR_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_TCLAP_STRINGPAIR_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Tclap/StringPair.hpp"
// *** MoReFEM header guards *** < //


#include <string>
#include <utility>


namespace MoReFEM::Wrappers::Tclap
{


    inline const std::pair<std::string, std::string>& StringPair::GetValue() const noexcept
    {
        return value_;
    }


} // namespace MoReFEM::Wrappers::Tclap


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_TCLAP_STRINGPAIR_DOT_HXX_
// *** MoReFEM end header guards *** < //
