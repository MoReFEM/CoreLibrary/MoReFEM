// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <string>

#include "Utilities/AsciiOrBinary/Advanced/DatafileExtension.hpp"

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/AsciiOrBinary/Enum.hpp"


namespace MoReFEM::Advanced::AsciiOrBinaryNS
{

    std::string DatafileExtension(binary_or_ascii is_binary_or_ascii)
    {
        switch (is_binary_or_ascii)
        {
        case binary_or_ascii::ascii:
            return ".hhdata";
        case binary_or_ascii::binary:
            return ".binarydata";
        case binary_or_ascii::from_input_data:
        {
            assert(false
                   && "This option is used internally in the library and should "
                      "not have been called in current context.");
            exit(EXIT_FAILURE);
        }
        }

        std::abort(); // to please gcc warning in release mode - should never been called.
    }


} // namespace MoReFEM::Advanced::AsciiOrBinaryNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
