// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ASCIIORBINARY_ADVANCED_DATAFILEEXTENSION_DOT_HPP_
#define MOREFEM_UTILITIES_ASCIIORBINARY_ADVANCED_DATAFILEEXTENSION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp" // IWYU pragma: export
#include "Utilities/Exceptions/Exception.hpp"        // IWYU pragma: export
#include "Utilities/Singleton/Singleton.hpp"         // IWYU pragma: export


namespace MoReFEM::Advanced::AsciiOrBinaryNS
{


    /*!
     * \brief Returns the proper extension to use depending on \a is_binary_or_ascii
     *
     * \param[in] is_binary_or_ascii Specifies whether ascii or binry extension is to be provided.
     *
     * \attention Must not be called if binary_or_ascii is neither ascii nor binary.
     *
     * \return ".hhdata" for ascii files, ".binarydata" for binary ones.
     */
    std::string DatafileExtension(binary_or_ascii is_binary_or_ascii);


} // namespace MoReFEM::Advanced::AsciiOrBinaryNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ASCIIORBINARY_ADVANCED_DATAFILEEXTENSION_DOT_HPP_
// *** MoReFEM end header guards *** < //
