// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"

#include "Utilities/AsciiOrBinary/Advanced/DatafileExtension.hpp"


namespace MoReFEM::Utilities
{

    AsciiOrBinary::~AsciiOrBinary() = default;


    AsciiOrBinary::AsciiOrBinary(bool binary_output)
    : binary_output_(binary_output ? binary_or_ascii::binary : binary_or_ascii::ascii)
    { }


    const std::string& AsciiOrBinary::ClassName()
    {
        static const std::string ret("AsciiOrBinary");
        return ret;
    }


    std::string AsciiOrBinary::DatafileExtension() const
    {
        return ::MoReFEM::Advanced::AsciiOrBinaryNS::DatafileExtension(IsBinaryOutput());
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
