// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ASCIIORBINARY_ASCIIORBINARY_DOT_HPP_
#define MOREFEM_UTILITIES_ASCIIORBINARY_ASCIIORBINARY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <string>

// IWYU pragma: begin_exports
#include "Utilities/AsciiOrBinary/Enum.hpp"
#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Singleton/Singleton.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::Utilities
{


    /*!
     * \brief Provides a way to specify the output format of the solution vectors corresponding to each
     * unknown.
     *
     * This singleton acts as a global variable in order to store the boolean value related to the format
     * of the output. Its only purpose is to propagate this information throughout the whole code.
     *
     */
    class AsciiOrBinary final : public Utilities::Singleton<AsciiOrBinary>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = AsciiOrBinary;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] binary_output True to write resulting vectors in binary, false to write them in ascii.
         */
        explicit AsciiOrBinary(bool binary_output);

        //! Destructor.
        virtual ~AsciiOrBinary() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<AsciiOrBinary>;

        //! Name of the class.
        static const std::string& ClassName();


        ///@}


      public:
        //! Output format for solution vectors.
        binary_or_ascii IsBinaryOutput() const noexcept;

        /*!
         * \brief Returns the proper extension to use depending on the choice.
         *
         * \attention Must not be called if binary_or_ascii is neither ascii nor binary.
         *
         * \return ".hhdata" for ascii files, ".binarydata" for binary ones.
         */
        std::string DatafileExtension() const;

      private:
        //! Used to define whether the output is ascii or binary.
        binary_or_ascii binary_output_ = binary_or_ascii::ascii;
    };


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/AsciiOrBinary/AsciiOrBinary.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ASCIIORBINARY_ASCIIORBINARY_DOT_HPP_
// *** MoReFEM end header guards *** < //
