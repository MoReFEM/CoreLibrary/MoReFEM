// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ASCIIORBINARY_ASCIIORBINARY_DOT_HXX_
#define MOREFEM_UTILITIES_ASCIIORBINARY_ASCIIORBINARY_DOT_HXX_
// IWYU pragma: private, include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
// *** MoReFEM header guards *** < //


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class binary_or_ascii; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Utilities
{


    inline binary_or_ascii AsciiOrBinary::IsBinaryOutput() const noexcept
    {
        return binary_output_;
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ASCIIORBINARY_ASCIIORBINARY_DOT_HXX_
// *** MoReFEM end header guards *** < //
