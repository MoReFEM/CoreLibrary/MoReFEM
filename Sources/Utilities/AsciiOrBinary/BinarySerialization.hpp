// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ASCIIORBINARY_BINARYSERIALIZATION_DOT_HPP_
#define MOREFEM_UTILITIES_ASCIIORBINARY_BINARYSERIALIZATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <optional>
#include <string> // IWYU pragma: keep
#include <vector>

// IWYU pragma: no_include <iosfwd>

#include "Utilities/Filesystem/File.hpp"


namespace MoReFEM::Advanced
{


    /*!
     * \brief Write the content of \a vector in \a binary_file in binary format.
     *
     * This usage is intended if the file is to be reloaded later on the same machine (or at least on a machine with
     * same OS and architecture).
     *
     * \param[in] binary_file File into which data will be written.
     * \param[in] vector Vector which content is to be dropped in the file.
     *
     * The values are written consecutively in binary format with no separators.
     *
     * \tparam T An integer or floating point type.
     */
    template<class T>
    void SerializeVectorIntoBinaryFile(const FilesystemNS::File& binary_file, const std::vector<T>& vector)
        requires(std::is_arithmetic<T>::value);


    /*!
     * \brief Write the content of a C \a array  in \a binary_file in binary format.
     *
     * This usage is intended if the file is to be reloaded later on the same machine (or at least on a machine with
     * same OS and architecture).
     *
     * \param[in] binary_file File into which data will be written.
     * \param[in] array Address of the first element of the array.
     * \param[in] Nvalues Number of values to write into the file.
     *
     * The values are written consecutively in binary format with no separators.
     *
     * \tparam T An integer or floating point type.
     */
    template<class T>
    void SerializeCArrayIntoBinaryFile(const FilesystemNS::File& binary_file, const T* const array, std::size_t Nvalues)
        requires(std::is_arithmetic<T>::value);


    /*!
     * \brief Load a vector with values that were dumped in \a binary_file.
     *
     * This usage is intended if the file was previously written on the same machine (or at least on a machine with
     * same OS and architecture).
     *
     * \copydoc doxygen_hide_unserialize_binary_file_param
     *
     * \param[in] expected_size If the number of elements expected in the vector is known, please specify
     * it there. This enables a much greater safety: we can't completely rule out mistakes (a file with the exact
     * expected size but garbage content would still pass) but we limit greatly the risk (without this indication
     * the check is just that the file of the size is a multiple of the size of one \a T).
     * \copydoc doxygen_hide_source_location
     *
     * \tparam T An integer or floating point type.
     *
     * \return The vector filled with the content from the binary file.
     *
     * \attention As indicated in \a expected_size documentation, this is a pretty low level operator and we can't completely
     * rule out an inadequate \a binary_file to be loaded in place of what is really intended. It is **strongly**
     * advised to use the optional argument \a expected_size to limit the risk as much as possible.
     */
    template<class T>
    std::vector<T>
    UnserializeVectorFromBinaryFile(const FilesystemNS::File& binary_file,
                                    std::optional<std::size_t> expected_size = std::nullopt,
                                    const std::source_location location = std::source_location::current())
        requires(std::is_arithmetic<T>::value);


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/AsciiOrBinary/BinarySerialization.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ASCIIORBINARY_BINARYSERIALIZATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
