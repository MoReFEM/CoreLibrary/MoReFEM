// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ASCIIORBINARY_ENUM_DOT_HPP_
#define MOREFEM_UTILITIES_ASCIIORBINARY_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM
{

    //! Enum class to specify whether the choice of output (ascii or binary)
    enum class binary_or_ascii {
        from_input_data, // takes the value specified in the input data file.
        ascii,
        binary
    };

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ASCIIORBINARY_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
