// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef>
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/AsciiOrBinary/Exceptions/BinarySerialization.hpp"

#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{


    std::string LoadBinaryFileExceptionMsg(const MoReFEM::FilesystemNS::File& binary_file);

    std::string NotExistingFileMsg(const MoReFEM::FilesystemNS::File& binary_file);


    std::string InconsistentFileSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                        std::string_view type_name,
                                        std::size_t sizeof_type,
                                        std::size_t file_size);


    std::string NotMatchingSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                   std::size_t Nexpected_values,
                                   std::size_t Nread_values);


} // namespace


namespace MoReFEM::ExceptionNS::BinaryNS
{


    LoadBinaryFileException::~LoadBinaryFileException() = default;


    LoadBinaryFileException::LoadBinaryFileException(const FilesystemNS::File& binary_file,
                                                     const std::source_location location)
    : ::MoReFEM::Exception(LoadBinaryFileExceptionMsg(binary_file), location)
    { }


    LoadBinaryFileException::LoadBinaryFileException(const std::string& msg, const std::source_location location)
    : ::MoReFEM::Exception(msg, location)
    { }


    NotExistingFile::~NotExistingFile() = default;

    NotExistingFile::NotExistingFile(const FilesystemNS::File& binary_file, const std::source_location location)
    : LoadBinaryFileException(NotExistingFileMsg(binary_file), location)
    { }


    InconsistentFileSize::~InconsistentFileSize() = default;

    InconsistentFileSize::InconsistentFileSize(const MoReFEM::FilesystemNS::File& binary_file,
                                               std::string_view type_name,
                                               std::size_t sizeof_type,
                                               std::size_t file_size,
                                               const std::source_location location)
    : LoadBinaryFileException(InconsistentFileSizeMsg(binary_file, type_name, sizeof_type, file_size), location)
    { }


    NotMatchingSize::~NotMatchingSize() = default;

    NotMatchingSize::NotMatchingSize(const MoReFEM::FilesystemNS::File& binary_file,
                                     std::size_t Nexpected_values,
                                     std::size_t Nread_values,
                                     const std::source_location location)
    : LoadBinaryFileException(NotMatchingSizeMsg(binary_file, Nexpected_values, Nread_values), location)
    { }


} // namespace MoReFEM::ExceptionNS::BinaryNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string LoadBinaryFileExceptionMsg(const MoReFEM::FilesystemNS::File& binary_file)
    {
        std::ostringstream oconv;
        oconv << "An error occured while loading binary file " << binary_file << "; the file was probably ill-formed.";
        return oconv.str();
    }


    std::string NotExistingFileMsg(const MoReFEM::FilesystemNS::File& binary_file)
    {
        std::ostringstream oconv;
        oconv << "Binary file " << binary_file << " wasn't found on the filesystem.";
        return oconv.str();
    }


    std::string InconsistentFileSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                        std::string_view type_name,
                                        std::size_t sizeof_type,
                                        std::size_t file_size)
    {
        std::ostringstream oconv;
        oconv << "Binary file " << binary_file << " contains " << file_size
              << " bytes, which is not a multiple of "
                 "the size of "
              << type_name << " (which is on current architecture " << sizeof_type << " bytes).";
        return oconv.str();
    }


    std::string NotMatchingSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                   std::size_t Nexpected_values,
                                   std::size_t Nread_values)
    {
        std::ostringstream oconv;
        oconv << "Binary file " << binary_file << " was expected to hold " << Nexpected_values << " values, but "
              << Nread_values << " were found.";
        return oconv.str();
    }


} // namespace
