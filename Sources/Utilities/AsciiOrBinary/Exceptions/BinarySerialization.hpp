// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ASCIIORBINARY_EXCEPTIONS_BINARYSERIALIZATION_DOT_HPP_
#define MOREFEM_UTILITIES_ASCIIORBINARY_EXCEPTIONS_BINARYSERIALIZATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::BinaryNS
{


    /*!
     * \brief Generic exception when a problem occurred while loading a binary vector with \a UnserializeVectorFromBinaryFile().
     *
     */
    class LoadBinaryFileException : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor when this base exception is directly thrown.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \copydoc doxygen_hide_source_location
         *
         */
        explicit LoadBinaryFileException(const FilesystemNS::File& binary_file,
                                         const std::source_location location = std::source_location::current());

        /*!
         * \brief Constructor used by derived exceptions.
         *
         * \param[in] msg Specific message crafted for the derived exception class.
         *
         * \copydoc doxygen_hide_source_location
         *
         */
        explicit LoadBinaryFileException(const std::string& msg,
                                         const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~LoadBinaryFileException() override;

        //! \copydoc doxygen_hide_copy_constructor
        LoadBinaryFileException(const LoadBinaryFileException& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        LoadBinaryFileException(LoadBinaryFileException&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        LoadBinaryFileException& operator=(const LoadBinaryFileException& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        LoadBinaryFileException& operator=(LoadBinaryFileException&& rhs) = default;
    };


    /*!
     * \brief When indicated file was not found.
     *
     */
    class NotExistingFile final : public LoadBinaryFileException
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \copydoc doxygen_hide_source_location
         *
         */
        explicit NotExistingFile(const FilesystemNS::File& binary_file,
                                 const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~NotExistingFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        NotExistingFile(const NotExistingFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        NotExistingFile(NotExistingFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NotExistingFile& operator=(const NotExistingFile& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        NotExistingFile& operator=(NotExistingFile&& rhs) = default;
    };


    /*!
     * \brief When the file doesn't include the expected number of values (when the latter is available)
     *
     */
    class InconsistentFileSize final : public LoadBinaryFileException
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \param[in] type_name Name of the type \a T which was expected in \a binary_file, as returned by MoReFEM::GetTypeName<T>().
         * \param[in] sizeof_type Result of `sizeof(T)`
         * \param[in] file_size Size of the file in bytes.
         *
         * \copydoc doxygen_hide_source_location
         *
         */
        explicit InconsistentFileSize(const FilesystemNS::File& binary_file,
                                      std::string_view type_name,
                                      std::size_t sizeof_type,
                                      std::size_t file_size,
                                      const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentFileSize() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentFileSize(const InconsistentFileSize& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentFileSize(InconsistentFileSize&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentFileSize& operator=(const InconsistentFileSize& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentFileSize& operator=(InconsistentFileSize&& rhs) = default;
    };


    /*!
     * \brief When the file doesn't include the expected number of values (when the latter is available)
     *
     */
    class NotMatchingSize final : public LoadBinaryFileException
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \param[in] Nexpected_values Number of values expected in the vector filled from the content of the file.
         * \param[in] Nread_values Number of values actually read from \a binary_file.
         *
         * \copydoc doxygen_hide_source_location
         *
         */
        explicit NotMatchingSize(const FilesystemNS::File& binary_file,
                                 std::size_t Nexpected_values,
                                 std::size_t Nread_values,
                                 const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~NotMatchingSize() override;

        //! \copydoc doxygen_hide_copy_constructor
        NotMatchingSize(const NotMatchingSize& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        NotMatchingSize(NotMatchingSize&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NotMatchingSize& operator=(const NotMatchingSize& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        NotMatchingSize& operator=(NotMatchingSize&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::BinaryNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ASCIIORBINARY_EXCEPTIONS_BINARYSERIALIZATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
