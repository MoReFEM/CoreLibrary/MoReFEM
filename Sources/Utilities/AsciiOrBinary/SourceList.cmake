### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/AsciiOrBinary.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/AsciiOrBinary.hpp
		${CMAKE_CURRENT_LIST_DIR}/AsciiOrBinary.hxx
		${CMAKE_CURRENT_LIST_DIR}/BinarySerialization.hpp
		${CMAKE_CURRENT_LIST_DIR}/BinarySerialization.hxx
		${CMAKE_CURRENT_LIST_DIR}/Enum.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
