// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_ARRAY_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_ARRAY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <algorithm>
#include <array>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Miscellaneous.hpp"   // IWYU pragma: export
#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export


namespace MoReFEM::Utilities
{


    /*!
     * \brief Function to set to nullptr all the content of a defined array.
     *
     * e.g.
     * \code
     * std::array<GlobalVector::unique_ptr, 5> = Utilities::NullptrArray<GlobalVector::unique_ptr, 5>();
     * \endcode
     *
     * in a class declaration of data attribute will init it with 5 nullptr.
     *
     * \return Array which includes \a N nullptr terms.
     */
    template<class ItemPtrT, std::size_t N>
    std::array<ItemPtrT, N> NullptrArray();


    /*!
     * \brief Init an array with \a NumericNS::UninitializedIndex for all of its elements.
     *
     * \tparam T An integral type.
     * \tparam N Size of the array.
     */
    template<class T, std::size_t N>
    std::array<T, N> FilledWithUninitializedIndex();


    /*!
     * \brief Helper struct to return statically the size of an array.
     *
     * Only the specialization matters; the generic declaration is intentionally left undefined.
     *
     */
    template<class T>
    struct ArraySize;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    template<class T, std::size_t N>
    struct ArraySize<std::array<T, N>>
    {

        static constexpr std::size_t GetValue() noexcept;
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Containers/Array.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_ARRAY_DOT_HPP_
// *** MoReFEM end header guards *** < //
