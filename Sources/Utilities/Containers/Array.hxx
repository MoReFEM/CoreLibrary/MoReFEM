// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_ARRAY_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_ARRAY_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/Array.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Utilities
{


    template<class ItemPtrT, std::size_t N>
    std::array<ItemPtrT, N> NullptrArray()
    {
        static_assert(IsSharedPtr<ItemPtrT>() || std::is_pointer<ItemPtrT>() || IsUniquePtr<ItemPtrT>(),
                      "ItemPtrT must behaves like a pointer!");

        std::array<ItemPtrT, N> ret;

        std::fill(ret.begin(), ret.end(), nullptr);

        return ret;
    }


    template<class T, std::size_t N>
    std::array<T, N> FilledWithUninitializedIndex()
    {
        std::array<T, N> ret;

        constexpr T value = NumericNS::UninitializedIndex<T>();

        std::fill(ret.begin(), ret.end(), value);

        return ret;
    }


    template<class T, std::size_t N>
    constexpr std::size_t ArraySize<std::array<T, N>>::GetValue() noexcept
    {
        return N;
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_ARRAY_DOT_HXX_
// *** MoReFEM end header guards *** < //
