// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <initializer_list>
#include <ostream>
#include <vector>

#include "Utilities/Containers/BoolArray.hpp"


namespace MoReFEM::Utilities
{


    BoolArray::BoolArray() : array_(nullptr), Nelt_(0UL)
    { }


    BoolArray::BoolArray(std::size_t Nelt) : array_(new bool[Nelt]), Nelt_(Nelt)
    { }


    BoolArray::BoolArray(std::initializer_list<bool> list) : array_(nullptr), Nelt_(list.size())
    {
        array_ = new bool[Nelt_];

        std::size_t index(0);

        for (const bool it : list)
            array_[index++] = it;
    }


    BoolArray::BoolArray(const std::vector<bool>& vector) : array_(nullptr), Nelt_(vector.size())
    {
        assert(!vector.empty());
        array_ = new bool[Nelt_];

        for (std::size_t i = 0; i < Nelt_; ++i)
            array_[i] = vector[i];
    }


    void BoolArray::AllocateSize(std::size_t Nelt)
    {
        assert(Nelt_ == 0UL);
        Nelt_ = Nelt;
        array_ = new bool[Nelt];
    }


    BoolArray::~BoolArray()
    {
        delete[] array_;
    }


    BoolArray::BoolArray(const BoolArray& rhs) : array_(nullptr), Nelt_(rhs.Nelt_)
    {
        array_ = new bool[Nelt_];
        for (std::size_t i = 0; i < Nelt_; ++i)
            array_[i] = rhs.array_[i];
    }


    BoolArray& BoolArray::operator=(const BoolArray& rhs)
    {
        if (this != &rhs)
        {
            Nelt_ = rhs.Nelt_;
            array_ = new bool[Nelt_];
            for (std::size_t i = 0; i < Nelt_; ++i)
                array_[i] = rhs.array_[i];
        }

        return *this;
    }


    void BoolArray::Print(std::ostream& stream) const
    {
        if (Nelt_ == 0)
        {
            stream << "{ }" << '\n';
            return;
        }

        stream << "{ ";

        for (std::size_t i = 0; i < Nelt_ - 1; ++i)
            stream << array_[i] << ", ";

        stream << array_[Nelt_ - 1] << " }";
    }


    std::vector<bool> VectorFromBoolArray(const BoolArray& array)
    {
        const std::size_t Nelt = array.Size();

        std::vector<bool> ret(Nelt);

        for (std::size_t i = 0; i < Nelt; ++i)
            ret[i] = array[i];

        return ret;
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
