// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_BOOLARRAY_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_BOOLARRAY_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cassert> // IWYU pragma: keep
#include <cstddef> // IWYU pragma: keep
#include <fstream>
#include <initializer_list>
#include <vector>


namespace MoReFEM::Utilities
{


    /*!
     * \brief A class to handle a dynamic array of bool with some safety.
     *
     * std::vector<bool> is an anomaly in the STL: contrary to what we could expect, this vector does not
     * contains bool but rather bits. It spares thus some memory, but is troublesome to use in
     * lots of situation... Moreover, its gain is rather dubious: what is saved in memory can very easily
     * be lost in execution time!
     *
     * The common trick is to use either a std::bitset (much more efficient, but size is fixed and known
     * at compile-time) or a std::deque<bool> (no trick in it: really bool inside).
     *
     * However, with MPI use I do not know the size at compile-time and I absolutely need contiguity
     * in memory, thus ruling out std::deque.
     *
     * Hence this class that is essentially a wrapper over a dynamic array, with RAII.
     *
     * \internal <b><tt>[internal]</tt></b> Due to its specific role, this class is one of the very few that does
     * not follow the coding style: names of the methods match their STL counterpart instead of respecting MoReFEM
     * coding style.
     * \endinternal
     *
     */
    class BoolArray final
    {
      public:
        //! \name Special members.
        ///@{

        //! Bare constructor.
        explicit BoolArray();

        //! This constructor allocates the memory for \a Nelt.
        //! \param[in] Nelt Number of elements to allocate for.
        explicit BoolArray(std::size_t Nelt);

        /*!
         * Constructor from an initializer list \a list.
         *
         * \param[in] list List used for creation.
         *
         * For instance:
         * \code
         * BoolArray array { true, false, true };
         * \endcode
         */
        explicit BoolArray(std::initializer_list<bool> list);

        /*!
         * \brief Constructor from a std::vector<bool>.
         *
         * \param[in] vector The std::vector<bool> from which we want to initialize.
         */
        explicit BoolArray(const std::vector<bool>& vector);


        //! Destructor, in charge of deallocating underlying dynamic array.
        ~BoolArray();

        /*!
         * \copydoc doxygen_hide_copy_constructor
         *
         * A new dynamic array is allocated for the new object.
         */
        BoolArray(const BoolArray& rhs);

        //! \copydoc doxygen_hide_move_constructor
        BoolArray(BoolArray&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        BoolArray& operator=(const BoolArray& rhs);

        //! \copydoc doxygen_hide_move_affectation
        BoolArray& operator=(BoolArray&& rhs) = default;


        ///@}

        /*!
         * \brief Allocate the underlying dynamic array with \a Nelt.
         *
         * \param[in] Nelt Allocate the space for \a Nelt in the BoolArray container.
         *
         * This method should only be called within an object in which no memory was previously allocated.
         */
        void AllocateSize(std::size_t Nelt);


        /*!
         * \brief Allow explicit conversion to a bool* object.
         *
         * \code
         * BoolArray array { true, false, true };
         * ...
         * static_cast<bool*>(array); // yields the underlying pointer to the first element of the dynamic array.
         * \endcode
         */
        explicit operator bool*() const;


        //! \copydoc doxygen_hide_const_subscript_operator
        bool operator[](std::size_t index) const;

        //! \copydoc doxygen_hide_non_const_subscript_operator
        bool& operator[](std::size_t index);

        //! Returns the number of elements in the array.
        //! \return Number of bool in the array.
        std::size_t Size() const;

        //! Print to \a out the content of the array.
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;


      public:
        //! \name STL counterparts.
        //! The purpose is to provide the same interface as the STL for some operations; it is actually an inline
        //! call to other methods defined previously.
        ///@{

        //! Alias to the underlying type.
        using value_type = bool;

        //! Access to underlying pointer.
        bool* data();

        //! Const access to underlying pointer.
        const bool* data() const;


        /*!
         * \brief Resize the container.
         *
         * \param[in] Nelt Size requested.
         *
         * \internal <b><tt>[internal]</tt></b> Same as AllocateSize().
         * \endinternal
         */
        void resize(std::size_t Nelt);


        /*!
         * \brief Returns the number of elements in the array.
         *
         * \return Number of elements in the array.
         *
         * \internal <b><tt>[internal]</tt></b> Same as Size().
         * \endinternal
         */
        std::size_t size() const;

        //! Whether the array is empty or not.
        bool empty() const;


        ///@}


      private:
        //! Underlying dynamic bool array.
        bool* array_;

        //! Number of elements in the array.
        std::size_t Nelt_;
    };


    /*!
     * \brief Create a std::vector<bool> with the same content as \a array object.
     *
     * \param[in] array The \a BoolArray object.
     *
     * \return The newly-minter std::vector.
     */
    std::vector<bool> VectorFromBoolArray(const BoolArray& array);


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Containers/BoolArray.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_BOOLARRAY_DOT_HPP_
// *** MoReFEM end header guards *** < //
