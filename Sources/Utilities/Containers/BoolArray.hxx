// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_BOOLARRAY_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_BOOLARRAY_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/BoolArray.hpp"
// *** MoReFEM header guards *** < //


#include <cassert> // IWYU pragma: keep
#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Utilities
{


    inline BoolArray::operator bool*() const
    {
        return array_;
    }


    inline bool* BoolArray::data()
    {
        return array_;
    }


    inline const bool* BoolArray::data() const
    {
        return array_;
    }


    inline void BoolArray::resize(std::size_t Nelt)
    {
        AllocateSize(Nelt);
    }


    inline bool BoolArray::operator[](std::size_t i) const
    {
        assert(i < Nelt_);
        return array_[i];
    }


    inline bool& BoolArray::operator[](std::size_t i)
    {
        assert(i < Nelt_);
        return array_[i];
    }


    inline std::size_t BoolArray::Size() const
    {
        return Nelt_;
    }


    inline std::size_t BoolArray::size() const
    {
        assert(Nelt_ == Size());
        return Nelt_;
    }


    inline bool BoolArray::empty() const
    {
        return Nelt_ == 0UL;
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_BOOLARRAY_DOT_HXX_
// *** MoReFEM end header guards *** < //
