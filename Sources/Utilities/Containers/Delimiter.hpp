// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_DELIMITER_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_DELIMITER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string_view>

#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export


namespace MoReFEM::PrintNS::Delimiter
{


    //! Strong type for the type of the string that opens a container (e.g. "[ " for "[ 0, 1, 2, 3 ]"
    using opener = StrongType<std::string_view, struct OpenerTag>;

    //! Strong type for the type of the string that separates itemsr (e.g. ", " for "[ 0, 1, 2, 3 ]"
    using separator = StrongType<std::string_view, struct SeparatorTag>;

    //! Strong type for the type of the string that closes a container (e.g. " ]" for "[ 0, 1, 2, 3 ]"
    using closer = StrongType<std::string_view, struct CloserTag>;


} // namespace MoReFEM::PrintNS::Delimiter


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_DELIMITER_DOT_HPP_
// *** MoReFEM end header guards *** < //
