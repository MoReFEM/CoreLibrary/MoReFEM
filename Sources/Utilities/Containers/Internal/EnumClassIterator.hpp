// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_INTERNAL_ENUMCLASSITERATOR_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_INTERNAL_ENUMCLASSITERATOR_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp"


namespace MoReFEM::Internal::EnumClassNS
{


    /*!
     * \brief Helper class to be able to use enum class in metaprogramming.
     *
     * Enum classes are tricky to use in metaprogramming, as arithmetic can't be used on them: you can't
     * directly call the next instance such as this example from MoReFEM:
     * \code
     * template
     * <
     *     MoReFEM::Advanced::GeometricEltEnum,
     *     Internal::MeshNS::FormatNS::Type N,
     *     Internal::MeshNS::FormatNS::Type EndT
     * >
     * struct IsAtLeastOneFormatSupported
     * {
     * constexpr operator bool() const
     * {
     * return Format::Support<N, IdentifierT>()
     * || IsAtLeastOneFormatSupported<N + 1, EndT>(); // N + 1 doesn't compile!
     * }
     *
     * };
     * \endcode
     *
     * The new class bypass this limitation, with the replacement call hereafter:
     *
     * \code
     * constexpr operator bool() const
     * {
     * return Format::Support<N, IdentifierT>()
     * IsAtLeastOneFormatSupported<Internal::EnumClassNS::Iterator<Internal::MeshNS::FormatNS::Type,
     * N>::Increment(), EndT>();
     * }
     * \endcode
     *
     * or to make it prettier with an alias
     *
     * \code
     * using CurrentType = Internal::EnumClassNS::Iterator<Internal::MeshNS::FormatNS::Type, N>;
     *
     * constexpr operator bool() const
     * {
     * return Format::Support<N, IdentifierT>()
     * || IsAtLeastOneFormatSupported<CurrentType::Increment(), EndT>();
     * }
     * \endcode
     *
     * \tparam EnumT Must be an enum (but in this case N + 1 above was fine as well) or an enum class. It is
     * expected its internal layout is: \code enum class T { Begin, *MyFirstElt* = Begin, ..., End }; \endcode
     * \tparam CurrentValueT Current value of the enum.
     */

    template<class EnumT, EnumT CurrentValueT>
    struct Iterator
    {

        //! Returns the value immediately after \a CurrentValueT.
        static constexpr EnumT Increment()
        {
            static_assert(CurrentValueT >= EnumT::Begin, "The input type is invalid!");
            static_assert(CurrentValueT < EnumT::End,
                          "Incrementing this Internal::MeshNS::FormatNS::Type object would make it go out of bounds!");
            return static_cast<EnumT>(::MoReFEM::EnumUnderlyingType(CurrentValueT) + 1);
        }


        //! Returns the value immediately before \a CurrentValueT.
        static constexpr EnumT Decrement()
        {
            static_assert(CurrentValueT > EnumT::Begin, "Could not decrement when starting at Begin or below!");
            static_assert(CurrentValueT <= EnumT::End,
                          "Decrementing an Iterator outside of boundaries is meaningless...");
            return static_cast<EnumT>(::MoReFEM::EnumUnderlyingType(CurrentValueT) - 1);
        }
    };


} // namespace MoReFEM::Internal::EnumClassNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_INTERNAL_ENUMCLASSITERATOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
