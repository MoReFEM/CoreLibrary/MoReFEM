// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_INTERNAL_IMPL_TUPLEHELPER_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_INTERNAL_IMPL_TUPLEHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <tuple>

#include "Utilities/Containers/Tuple/Concept.hpp" // IWYU pragma: export
#include "Utilities/Numeric/Numeric.hpp"          // IWYU pragma: export


namespace MoReFEM::Internal::Tuple::Impl
{


    /*!
     * \brief Check that elements at \a I -th and \a J -th position in \a TupleT don't share the same
     * type.
     *
     * \tparam TupleT Tuple being checked.
     * \tparam I Index of the first element in the comparison.
     * \tparam J Index of the second element in the comparison. By construct should be higher than I.
     * \tparam TupleSizeT Size of the tuple; it is const but required nonetheless to implement the
     * specialization that ends the recursion.
     */
    template<::MoReFEM::Concept::Tuple TupleT, std::size_t I, std::size_t J, std::size_t TupleSizeT>
    struct CompareRemainingToI
    {


        //! Type of the \a I -th element in the tuple.
        using TypeI = typename std::tuple_element<I, TupleT>::type;

        //! Type of the \a J -th element in the tuple.
        using TypeJ = typename std::tuple_element<J, TupleT>::type;

        //! Static function that does the actual work.
        static void Perform()
        {
            static_assert(I < J, "If not ill-defined call to this static method!");
            static_assert(J != TupleSizeT, "Equality should be handled in a specialization.");
            static_assert(J < TupleSizeT, "If not ill-defined call to this static method!");

            static_assert(!std::is_same<TypeI, TypeJ>::value, "No type should be present twice in this tuple!");

            CompareRemainingToI<TupleT, I, J + 1, TupleSizeT>::Perform();
        }
    };


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    template<::MoReFEM::Concept::Tuple TupleT, std::size_t I, std::size_t TupleSizeT>
    struct CompareRemainingToI<TupleT, I, TupleSizeT, TupleSizeT>
    {


        static void Perform()
        {
            // End recursion.
        }
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::Tuple::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_INTERNAL_IMPL_TUPLEHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
