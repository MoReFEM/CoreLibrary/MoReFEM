// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_INTERNAL_PRINT_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_INTERNAL_PRINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>
#include <tuple>
#include <variant>

#include "Utilities/Containers/Delimiter.hpp"
#include "Utilities/Containers/Tuple/Concept.hpp"
#include "Utilities/String/Traits.hpp"


namespace MoReFEM::Internal::PrintNS
{


    /*!
     * \brief Facility to handle properly the interval issue (we want separator between elements but not at the
     * end of the list.
     *
     * I recommend [this blog post](https://www.fluentcpp.com/2019/05/07/output-strings-separated-commas-cpp) which was
     * a direct inspiration.
     *
     */
    template<class PrintPolicyT>
    class SeparatorFacility
    {
      public:
        //! Convenient alias.
        using self = SeparatorFacility<PrintPolicyT>;

        /*!
         * \brief Constructor.
         *
         * \param[in] stream Stream onto which the content is used.
         * \param[in] separator Type of separator to be used in the list.
         */
        explicit SeparatorFacility(std::ostream& stream, ::MoReFEM::PrintNS::Delimiter::separator separator);

        //! Destructor.
        ~SeparatorFacility() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SeparatorFacility(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SeparatorFacility(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SeparatorFacility& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SeparatorFacility& operator=(self&& rhs) = delete;

        /*!
         * \brief Overload of operator<< for the class.
         *
         * \param[in,out] facility The facility object; it's on its internal stream that the new content is written.
         * \param[in] value Value to print. May be a std::variant, in which case a visitor is called to identify the
         * actual type to use for operator<<.
         *
         * \return \a facility argument (to enable chained calls).
         */
        template<class PrintPolicyU, class T>
        friend SeparatorFacility<PrintPolicyU>& operator<<(SeparatorFacility<PrintPolicyU>& facility, const T& value);

      private:
        //! The stream onto which content is printed.
        std::ostream& stream_;

        //! Separator to use between elements of the list.
        ::MoReFEM::PrintNS::Delimiter::separator separator_;

        //! True when we still have to print the first element.
        bool is_first_ = true;
    };


    /*!
     * \brief Overload of operator<< for the class.
     *
     * \param[in,out] facility The facility object; it's on its internal stream that the new content is written.
     * \param[in] value Value to print. May be a std::variant, in which case a visitor is called to identify the
     * actual type to use for operator<<.
     *
     * \return \a facility argument (to enable chained calls).
     */
    template<class PrintPolicyT, class T>
    SeparatorFacility<PrintPolicyT>& operator<<(SeparatorFacility<PrintPolicyT>& facility, const T& value);


    /*!
     ** \brief Facility to print elements of a tuple
     **
     ** Inspired by Nicolai M. Josuttis "The C++ standard library" page 74
     */
    template<class StreamT, std::size_t Index, std::size_t Max, ::MoReFEM::Concept::Tuple TupleT>
    struct PrintTupleHelper
    {


        /*!
         * \brief Static function that does the actual work.
         *
         * \param[in,out] stream Stream to which the tuple will be printed.
         * \param[in] t Tuple to display.
         * \param[in] separator Separator between all elements of the tuple.
         */
        static void Print(StreamT& stream, const TupleT& t, ::MoReFEM::PrintNS::Delimiter::separator separator);
    };


    //! Specialization used to stop recursive call.
    template<class StreamT, std::size_t Max, ::MoReFEM::Concept::Tuple TupleT>
    struct PrintTupleHelper<StreamT, Max, Max, TupleT>
    {

        //! Nothing done here in the specialization.
        static void Print(StreamT&, const TupleT&, ::MoReFEM::PrintNS::Delimiter::separator);
    };


} // namespace MoReFEM::Internal::PrintNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/Containers/Internal/Print.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_INTERNAL_PRINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
