// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_INTERNAL_PRINT_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_INTERNAL_PRINT_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/Internal/Print.hpp"
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <filesystem>


namespace MoReFEM::Internal::PrintNS
{


    template<class PrintPolicyT>
    SeparatorFacility<PrintPolicyT>::SeparatorFacility(std::ostream& stream,
                                                       ::MoReFEM::PrintNS::Delimiter::separator separator)
    : stream_(stream), separator_(separator)
    { }


    template<class PrintPolicyT, class T>
    SeparatorFacility<PrintPolicyT>& operator<<(SeparatorFacility<PrintPolicyT>& facility, const T& value)
    {
        if (facility.is_first_)
            facility.is_first_ = false;
        else
            facility.stream_ << facility.separator_.Get();

        PrintPolicyT::Do(facility.stream_, value);

        return facility;
    }


    template<class StreamT, std::size_t Index, std::size_t Max, ::MoReFEM::Concept::Tuple TupleT>
    void PrintTupleHelper<StreamT, Index, Max, TupleT>::Print(StreamT& stream,
                                                              const TupleT& t,
                                                              ::MoReFEM::PrintNS::Delimiter::separator separator)
    {
        using EltTupleType = typename std::tuple_element<Index, TupleT>::type;

        const auto quote = Utilities::String::IsString<EltTupleType>::value ? "\"" : "";

        stream << quote << std::get<Index>(t) << quote
               << (Index + 1 == Max ? ::MoReFEM::PrintNS::Delimiter::separator("") : separator).Get();
        PrintTupleHelper<StreamT, Index + 1, Max, TupleT>::Print(stream, t, separator);
    };


    template<class StreamT, std::size_t Max, ::MoReFEM::Concept::Tuple TupleT>
    void PrintTupleHelper<StreamT, Max, Max, TupleT>::Print(StreamT&,
                                                            const TupleT&,
                                                            ::MoReFEM::PrintNS::Delimiter::separator)
    {
        // Do nothing!
    }


} // namespace MoReFEM::Internal::PrintNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_INTERNAL_PRINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
