// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_INTERNAL_SORT_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_INTERNAL_SORT_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cassert>
#include <tuple>

#include "Utilities/Containers/Tuple/Concept.hpp"


namespace MoReFEM::Internal::SortNS
{


    /*!
     * \brief Compare whether two objects of same type are equal regarding one unary function.
     *
     * \tparam T Type of the objects involved.
     * \tparam UnaryT Structure which is expected to define a static unary function named
     * Value which sole parameter is a \a T object.
     * Typically UnaryT will be a trick to get the results of a method of T object.
     *
     * For instance:
     * \code{.cpp}
     struct Dimension
     {
        static int Value(const T& element)
        {
            return T.GetDimension();
        }
     };

     T foo, bar;

     bool are_equal = IsEqual<T, Dimension>(foo, bar);
     //< actually performs foo.GetDimension() == bar.GetDimension();
     \endcode
     *
     * In the above example it may seem pointlessly complicated, but the highlight
     * is that it can be used in metaprogramming context (see for instance Sort
     * implementation).
     *
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     * \return True of both lhs and rhs are equal considering \a UnaryT function.
     */
    template<class T, class UnaryT>
    bool IsEqual(const T& lhs, const T& rhs);


    /*!
     * \brief Helper function for SortingCriterion.
     *
     * See SortingCriterion documentation to understand what is the goal.
     *
     * \tparam T Type of the objects to sort. Operators < and == must be defined for this type.
     * \tparam IndexT This index is going downward from the size of the tuple minus 1 to 0 (reason
     * for this awful trick is that to end the recursion I can use 0 but not std::tuple_size<TupleT>::value).
     * So SortHelper is first called with IndexT = Size - 1, which means in fact very first element
     * of the tuple. Then with Size - 2, which means second element of tuple... and so forth!
     */
    template<class T, int IndexT, ::MoReFEM::Concept::Tuple TupleT>
    struct SortHelper
    {

        //! Actual function (wrapped into a class for template specialization).
        //! \copydoc doxygen_hide_lhs_rhs_arg
        static bool Compare(const T& lhs, const T& rhs);
    };


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    //! Same as above, used to end the recursion.
    template<class T, ::MoReFEM::Concept::Tuple TupleT>
    struct SortHelper<T, 0, TupleT>
    {

        //! Actual function (wrapped into a class for template specialization).
        static bool Compare(const T& lhs, const T& rhs);
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::SortNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Containers/Internal/Sort.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_INTERNAL_SORT_DOT_HPP_
// *** MoReFEM end header guards *** < //
