// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_INTERNAL_SORT_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_INTERNAL_SORT_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/Internal/Sort.hpp"
// *** MoReFEM header guards *** < //

#include <cassert>
#include <tuple>

#include "Utilities/Containers/Tuple/Concept.hpp"


namespace MoReFEM::Internal::SortNS
{


    template<class T, class UnaryT>
    bool IsEqual(const T& lhs, const T& rhs)
    {
        return UnaryT::Value(lhs) == UnaryT::Value(rhs);
    }


    template<class T, int IndexT, ::MoReFEM::Concept::Tuple TupleT>
    bool SortHelper<T, IndexT, TupleT>::Compare(const T& lhs, const T& rhs)
    {
        enum { size = std::tuple_size<TupleT>::value };

        using Criterion = typename std::tuple_element<size - 1 - IndexT, TupleT>::type;

        if (!IsEqual<T, Criterion>(lhs, rhs))
        {
            return typename Criterion::StrictOrderingOperator()(Criterion::Value(lhs), Criterion::Value(rhs));
        }

        return SortHelper<T, IndexT - 1, TupleT>::Compare(lhs, rhs);
    }


    template<class T, ::MoReFEM::Concept::Tuple TupleT>
    bool SortHelper<T, 0, TupleT>::Compare(const T& lhs, const T& rhs)
    {
        enum { size = std::tuple_size<TupleT>::value };

        using Criterion = typename std::tuple_element<size - 1, TupleT>::type;

        if (!IsEqual<T, Criterion>(lhs, rhs))
            return typename Criterion::StrictOrderingOperator()(Criterion::Value(lhs), Criterion::Value(rhs));

        // False is returned here as we consider the sorting criterion
        // expects a strict ordering criterion: typically STL functions
        // expect < and not <=.
        // So when \a lhs and \a rhs are completely equal for all criteria,
        // we do not want to return true (which would mean there is
        // actually a difference in one of the sorting criterion).
        return false;
    }


} // namespace MoReFEM::Internal::SortNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_INTERNAL_SORT_DOT_HXX_
// *** MoReFEM end header guards *** < //
