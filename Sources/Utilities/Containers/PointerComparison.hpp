// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_POINTERCOMPARISON_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_POINTERCOMPARISON_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export


namespace MoReFEM::Utilities::PointerComparison
{


    /*!
     * \class doxygen_hide_pointer_comp_type
     *
     * \brief Use reference if the type being compared is a shared or unique pointer.
     *
     * For the shared pointer it's only a matter of efficiency, for the unique one comparison couldn't occur
     * otherwise.
     */


    /*!
     * \brief The equivalent of std::less<> for raw or shared pointers: except that what is compared
     * is the underlying object rather than just the address of the pointer.
     *
     * For instance:
     * \code
     * std::vector<std::shared_ptr<Foo>> my_list;
     * ... fill the list ...
     * std::sort(my_list.begin(), my_list.end(), PointerComparison::Less<std::shared_ptr<Foo>>());
     * \endcode
     *
     * will use the std::less<Foo> to decide which is the order (and not compile if there is no such order).
     *
     * On the contrary,
     *
     * \code
     * std::sort(my_list.begin(), my_list.end(), std::less<std::shared_ptr<Foo>>);
     * \endcode
     *
     * would sort the elements of the list according to their address; it would work each time but the
     * ordering would change at each run of the code (which could lead to very tricky bugs to solve, as they
     * appear only once in a while for specific orderings...).
     */
    template<class T>
    struct Less
    {


        //! \copydoc doxygen_hide_pointer_comp_type
        // clang-format off
                using comparison_type =
                    std::conditional_t
                    <
                        Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>(),
                        const T&,
                        T
                    >;
        // clang-format on


        //! Overload operator() so that Less might be used as a functor.
        //! \copydoc doxygen_hide_lhs_rhs_arg
        bool operator()(comparison_type lhs, comparison_type rhs) const;
    };


    //! See Less for explanation.
    template<class T>
    struct Greater
    {


        //! \copydoc doxygen_hide_pointer_comp_type
        // clang-format off
        using comparison_type =
            std::conditional_t
            <
                Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>(),
                const T&,
                T
            >;
        // clang-format on

        //! Overload operator() so that Great might be used as a functor.
        //! \copydoc doxygen_hide_lhs_rhs_arg
        bool operator()(comparison_type lhs, comparison_type rhs) const;
    };


    //! See Less for explanation.
    template<class T>
    struct Equal
    {


        //! \copydoc doxygen_hide_pointer_comp_type
        // clang-format off
        using comparison_type =
            std::conditional_t
            <
                Utilities::IsSharedPtr<T>() || Utilities::IsUniquePtr<T>(),
                const T&,
                T
            >;
        // clang-format on

        //! Overload operator() so that Less might be used as a functor.
        //! \copydoc doxygen_hide_lhs_rhs_arg
        bool operator()(comparison_type lhs, comparison_type rhs) const;
    };


    /*!
     * \brief A std::set that stores pointers and use the pointer comparisons.
     */
    template<class T, class CompareT = Less<T>>
    using Set = std::set<T, CompareT>;


    /*!
     * \brief A std::map that stores pointers and use the pointer comparisons.
     */
    template<class KeyT, class ValueT, class CompareT = Less<KeyT>>
    using Map = std::map<KeyT, ValueT, CompareT>;


} // namespace MoReFEM::Utilities::PointerComparison


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Containers/PointerComparison.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_POINTERCOMPARISON_DOT_HPP_
// *** MoReFEM end header guards *** < //
