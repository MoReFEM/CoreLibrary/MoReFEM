// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_POINTERCOMPARISON_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_POINTERCOMPARISON_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/PointerComparison.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Utilities::PointerComparison
{


    template<class T>
    bool Less<T>::operator()(comparison_type lhs, comparison_type rhs) const
    {
        static_assert(IsSharedPtr<T>() || std::is_pointer<T>() || IsUniquePtr<T>(), "T must behaves like a pointer!");


        // First handle cases with nullptr involved.
        // Rule is nullptr is the lowest value possible.
        if (!lhs || !rhs)
        {
            if (!lhs && !rhs)
                return false;

            if (!lhs)
                return true;

            if (!rhs)
                return false;
        }

        return *lhs < *rhs;
    }


    template<class T>
    bool Greater<T>::operator()(comparison_type lhs, comparison_type rhs) const
    {
        static_assert(IsSharedPtr<T>() || std::is_pointer<T>() || IsUniquePtr<T>(), "T must behaves like a pointer!");

        // Return false if both are equal.
        if (Equal<T>()(lhs, rhs))
            return false;

        // If not equal the result is the opposite of the one of Less.
        return !Less<T>()(lhs, rhs);
    }


    template<class T>
    bool Equal<T>::operator()(comparison_type lhs, comparison_type rhs) const
    {
        static_assert(IsSharedPtr<T>() || std::is_pointer<T>() || IsUniquePtr<T>(), "T must behaves like a pointer!");

        // Handle nullptr cases
        if (!lhs || !rhs)
        {
            // If both are nullptr consider them equal.
            if (!lhs && !rhs)
                return true;

            return false;
        }

        return *lhs == *rhs;
    }


} // namespace MoReFEM::Utilities::PointerComparison


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_POINTERCOMPARISON_DOT_HXX_
// *** MoReFEM end header guards *** < //
