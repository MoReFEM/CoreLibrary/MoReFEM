// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_PRINT_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_PRINT_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <iostream> // mandatory due to std::cout as default parameter
#include <map>
#include <sstream>
#include <string_view>
#include <tuple>
#include <variant>

#include "Utilities/Containers/Delimiter.hpp"          // IWYU pragma: export
#include "Utilities/Containers/Internal/Print.hpp"     // IWYU pragma: export
#include "Utilities/Containers/PrintPolicy/Normal.hpp" // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"                 // IWYU pragma: export
#include "Utilities/String/Traits.hpp"


namespace MoReFEM::Utilities
{


    /*!
     * \brief Print the content of a container (that might be associative or not - see \a PrintPolicyT).
     *
     * \tparam PrintPolicyT A policy which determines how the printing of the element of the container behaves.
     * Several policies are proposed in Utilities/Containers/PrintPolicy directory:
     * - 'Normal': which prints a non-associative container which elements may be displayed directory with
     * operator<<.
     * - 'Quoted': same as Normal except that each element is inside single quotes.
     * - 'Variant': same as Normal, except that if type is std::variant a visitor is used to print it properly.
     * - 'Pointer': for a non-associative container of (smart) pointers; the element is dereferenced before being
     * printed.
     * - 'Associative': for an associative container; each key/value is printed under format (key, value) (i.e.
     * parenthesis as open/close and ", " as separator).
     * - 'Key': prints only the keys of an associative containers.
     */
    template<class PrintPolicyT = PrintPolicyNS::Normal>
    struct PrintContainer
    {


        /*!
         * \class doxygen_hide_print_container_common_arg
         *
         * \tparam StreamT Type of output stream considered
         * \tparam ContainerT Type of the container to be displayed (also see class template parameter
         * \a PrintPolicyT for more details).
         *
         * \param[in,out] stream Output stream in which container will be displayed
         * \param[in] container Container displayed
         * \param[in] separator Separator between two entries of the contained
         * \param[in] opener Prefix used while displaying the container
         * \param[in] closer Suffix used while displaying the container
         */


        /*!
         * \brief Function to actually print the content of a container.
         *
         * \internal I am using struct/static method only to deal more finely with default template parameters:
         * we want the container type to be inferred automatically but the policy to use to be customizable.
         * \endinternal
         *
         * \copydoc doxygen_hide_print_container_common_arg
         *
         * In most cases templates parameters can be determined implicitly at compile time:
         * \code
         * std::vector<double> foo { 1., 2., 3., 10., 42. };
         * std::ostringstream oconv;
         * PrintContainer<>::Do(foo, oconv, PrintNS::Delimiter::separator(" "), PrintNS::Delimiter::opener
         * opener("---"), PrintNS::Delimiter::closer("---")) \endcode This code yields:
         *   ---1. 2. 3. 10. 42.---
         */
        template<class ContainerT, typename StreamT = std::ostream>
        static void Do(const ContainerT& container,
                       StreamT& stream = std::cout,
                       PrintNS::Delimiter::separator separator = PrintNS::Delimiter::separator(", "),
                       PrintNS::Delimiter::opener opener = PrintNS::Delimiter::opener("["),
                       PrintNS::Delimiter::closer closer = PrintNS::Delimiter::closer("]\n"));


        /*!
         * \brief Function to print the \a N first elements of a container (or the whole container if N is greater
         * than its size).
         *
         * \copydoc doxygen_hide_print_container_common_arg
         * \tparam N Maximum number of elements to display.
         */
        template<std::size_t N, class ContainerT, typename StreamT = std::ostream>
        static void Nelt(const ContainerT& container,
                         StreamT& stream = std::cout,
                         PrintNS::Delimiter::separator separator = PrintNS::Delimiter::separator(", "),
                         PrintNS::Delimiter::opener opener = PrintNS::Delimiter::opener("["),
                         PrintNS::Delimiter::closer closer = PrintNS::Delimiter::closer("]\n"));
    };


    /*!
     * \brief Print the content of a tuple or a pair.
     *
     * Inspired by Nicolai M. Josuttis "The C++ standard library" page 74.
     *
     * \tparam StreamT Type of output stream considered
     *
     * \param[in,out] stream Output stream in which tuple content will be displayed. All tuple elements must
     * define operator<<.
     * \param[in] tuple Tuple which content is  displayed.
     * \param[in] separator Separator between two entries of the tuple.
     * \param[in] opener Prefix used while displaying the tuple.
     * \param[in] closer Suffix used while displaying the tuple.
     */
    template<::MoReFEM::Concept::Tuple TupleT, typename StreamT = std::ostream>
    void PrintTuple(const TupleT& tuple,
                    StreamT& stream = std::cout,
                    PrintNS::Delimiter::separator separator = PrintNS::Delimiter::separator(", "),
                    PrintNS::Delimiter::opener opener = PrintNS::Delimiter::opener("["),
                    PrintNS::Delimiter::closer closer = PrintNS::Delimiter::closer("]\n"));


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Containers/Print.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_PRINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
