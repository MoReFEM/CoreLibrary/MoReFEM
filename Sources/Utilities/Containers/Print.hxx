// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_PRINT_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_PRINT_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/Print.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Utilities
{


    template<class PrintPolicyT>
    template<class ContainerT, typename StreamT>
    void PrintContainer<PrintPolicyT>::Do(const ContainerT& container,
                                          StreamT& stream,
                                          PrintNS::Delimiter::separator separator,
                                          PrintNS::Delimiter::opener opener,
                                          PrintNS::Delimiter::closer closer)
    {
        std::ostringstream oconv;
        oconv.copyfmt(stream);
        oconv << opener.Get();

        Internal::PrintNS::SeparatorFacility<PrintPolicyT> separator_facility(oconv, separator);

        for (const auto& element : container)
            separator_facility << element;

        oconv << closer.Get();
        stream << oconv.str();
    }


    template<class PrintPolicyT>
    template<std::size_t N, class ContainerT, typename StreamT>
    void PrintContainer<PrintPolicyT>::Nelt(const ContainerT& container,
                                            StreamT& stream,
                                            PrintNS::Delimiter::separator separator,
                                            PrintNS::Delimiter::opener opener,
                                            PrintNS::Delimiter::closer closer)
    {
        std::ostringstream oconv;
        oconv.copyfmt(stream);
        oconv << opener.Get();

        Internal::PrintNS::SeparatorFacility<PrintPolicyT> separator_facility(oconv, separator);

        auto it = container.cbegin();
        auto end = it;
        std::advance(end, static_cast<typename ContainerT::difference_type>(std::min(N, container.size())));

        for (; it != end; ++it)
            separator_facility << *it;

        oconv << closer.Get();
        stream << oconv.str();
    }


    template<::MoReFEM::Concept::Tuple TupleT, typename StreamT>
    void PrintTuple(const TupleT& tuple,
                    StreamT& stream,
                    PrintNS::Delimiter::separator separator,
                    PrintNS::Delimiter::opener opener,
                    PrintNS::Delimiter::closer closer)
    {
        std::ostringstream oconv;
        oconv.copyfmt(stream);
        oconv << opener.Get();
        enum { size = std::tuple_size<TupleT>::value };
        Internal::PrintNS::PrintTupleHelper<StreamT, 0, size, TupleT>::Print(oconv, tuple, separator);
        oconv << closer.Get();
        stream << oconv.str();
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_PRINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
