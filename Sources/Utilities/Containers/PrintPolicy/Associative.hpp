// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_ASSOCIATIVE_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_ASSOCIATIVE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <memory>
#include <vector>

#include "Utilities/String/Traits.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{

    /*!
     * \brief Enum class to decide the format to adopt.
     *
     * Two are proposed so far:
     * - Default: the format is (key, value), i.e.:
     *      * Opening by '('
     *      * Closing by ')'
     *      * Separation by ", ".
     * - Lua: the format is [key] = value (or ['key'] = value if is_key_string is set to yes.
     */
    enum class associative_format { Default, Lua };

    /*!
     * \brief Policy to handle the an associative container.
     *
     * \tparam FormatT Format to use for the output. It is rather rigid now; it is easy to make it more customizable
     * if need be (for instance default one could use different opening, closing or separation characters).
     */
    template<associative_format FormatT = associative_format::Default>
    struct Associative
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/Containers/PrintPolicy/Associative.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_ASSOCIATIVE_DOT_HPP_
// *** MoReFEM end header guards *** < //
