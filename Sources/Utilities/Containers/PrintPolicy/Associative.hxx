// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_ASSOCIATIVE_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_ASSOCIATIVE_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Associative.hpp"
// *** MoReFEM header guards *** < //

namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<associative_format FormatT>
    template<class ElementTypeT>
    void Associative<FormatT>::Do(std::ostream& stream, ElementTypeT&& element)
    {
        if constexpr (FormatT == associative_format::Default)
            stream << '(' << element.first << ", " << element.second << ')';
        else if constexpr (FormatT == associative_format::Lua)
        {
            stream << '[';

            using key_type = typename std::decay_t<ElementTypeT>::first_type;

            constexpr bool is_string_or_path =
                (String::IsString<key_type>() || std::is_same_v<key_type, std::filesystem::path>);

            if constexpr (is_string_or_path)
                stream << "'";

            stream << element.first;

            if constexpr (is_string_or_path)
                stream << "'";

            stream << "] = " << element.second;
        } else
        {
            // Unfortunately static_assert(false) is not supported, so we'll have to do with runtime error.
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_ASSOCIATIVE_DOT_HXX_
// *** MoReFEM end header guards *** < //
