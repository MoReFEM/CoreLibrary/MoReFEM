// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_KEY_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_KEY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>


namespace MoReFEM::Utilities::PrintPolicyNS
{


    /*!
     * \brief Policy to print only the keys of an associative container.
     *
     */
    struct Key
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/Containers/PrintPolicy/Key.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_KEY_DOT_HPP_
// *** MoReFEM end header guards *** < //
