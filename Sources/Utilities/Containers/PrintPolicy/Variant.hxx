// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_VARIANT_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_VARIANT_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Variant.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<class T>
    void Variant::Do(std::ostream& stream, T&& value)
    {
        using type = std::decay_t<T>;

        if constexpr (Utilities::IsSpecializationOf<std::variant, type>())
        {
            std::visit(
                [&stream](auto&& arg)
                {
                    stream << arg;
                },
                value);
        } else
            stream << value;
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_VARIANT_DOT_HXX_
// *** MoReFEM end header guards *** < //
