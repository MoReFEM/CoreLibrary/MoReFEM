// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_SORT_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_SORT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <tuple>

#include "Utilities/Containers/Internal/Sort.hpp"


namespace MoReFEM::Utilities
{


    /*!
     * \brief Defines the ordering of two different objects.
     *
     * \tparam T Type of the objects.
     * \tparam FirstCriterionT First criterion to be considered to sort both objects. If the criterion is enough
     * to make a difference between both objects, result is returned. A criterion object must provide:
     *   - A static method Value() which returns something (let's name this type U) and take as argument a
     * T::shared_ptr.
     *   - A binary comparison operator StrictOrderingOperator which acts upon two U types object.
     *
     * \tparam OtherCriteriaT If first criterion isn't enough, iterate through \a OtherCriteriaT until one criterion
     * is able to separate both objects, or until all criteria have been tried.
     *
     * An example is probably the best way to explain the purpose of sort.
     *
     * \code
     *
     * class ClassToSort
     * {
     *      [...]
     * public:
     *
     *      int FirstMethod() const;
     *      std::string SecondMethod() const;
     * };
     *
     * template<class ComparisonT = std::less<int>>
     * struct SortCriterion1
     * {
     *      static int Value(const ClassToSort& elem)
     *      {
     *          return elem.Method1();
     *      }
     *
     *using StrictOrderingOperator = ComparisonT;
     * };
     *
     * template<class ComparisonT = std::less<std::string>>
     * struct SortCriterion2
     * {
     *      static std::string Value(const ClassToSort& elem)
     *      {
     *          return elem.Method2();
     *      }
     *
     * using StrictOrderingOperator = ComparisonT;
     * };
     *
     * std::vector<ClassToSort> list_of_objects;
     * [... fill list of objects ...]
     *
     * // Sort the objects with criterion1 as primary criterion and criterion2 as secondary criterion.
     * std::stable_sort(list_of_objects.begin(), list_of_objects.end(), Sort<ClassToSort, SortCriterion1<>,
     SortCriterion2<>);
     *
     * // Sort the objects with criterion2 as primary criterion and criterion1 as secondary criterion.
     . // Consider decreasing order for criterion 1.
     * std::stable_sort(list_of_objects.begin(), list_of_objects.end(), Sort<ClassToSort, SortCriterion2<>,
     SortCriterion1<std::greater<int>>);
     *
     * \endcode
     *
     * As variadic template is used, more than 2 criteria might be used (as many as you like in fact).
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return True if \a lhs is before \a rhs with respect to all sort criteria.
     */
    template<class T, class FirstCriterionT, typename... OtherCriteriaT>
    bool Sort(const T& lhs, const T& rhs)
    {
        // Variadic template require that first argument is written independently, but for sort there are no reason
        // first criterion is considered differently that the others; so all are assembled in a unique tuple.
        using Criteria = std::tuple<FirstCriterionT, OtherCriteriaT...>;

        enum { size = std::tuple_size<Criteria>::value };

        return ::MoReFEM::Internal::SortNS::SortHelper<T, size - 1, Criteria>::Compare(lhs, rhs);
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_SORT_DOT_HPP_
// *** MoReFEM end header guards *** < //
