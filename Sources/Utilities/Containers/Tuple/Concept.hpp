// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_TUPLE_CONCEPT_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_TUPLE_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <tuple>


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify a type is an instantiation of a std::tuple.
     *
     * \internal Rather crude check here: just working with tuple size is enough to pass.
     * But it is more than enough for my needs: still better than just putting 'class TupleT'!
     *
     */
    template<typename T>
    concept Tuple = requires { std::tuple_size<T>::value; };


} // namespace MoReFEM::Concept


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_TUPLE_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
