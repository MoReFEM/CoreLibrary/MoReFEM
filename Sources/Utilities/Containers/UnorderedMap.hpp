// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_UNORDEREDMAP_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_UNORDEREDMAP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <functional>


namespace MoReFEM::Utilities
{


    /*!
     * \brief Value of std::unordered_map::max_load_factor() that is recommended in "The C++ Standard Library"
     * by N. Josuttis.
     *
     * \return 0.7 (either that or .8 was suggested).
     */
    constexpr float DefaultMaxLoadFactor() noexcept
    {
        return 0.7f;
    }


    /*!
     * \brief Function lifted from Boost (http://www.boost.org) to combine several hash keys together.
     *
     * Apparently this formula stems from Knuth's work.
     *
     * For instance to yield a hash key for a std::pair<std::size_t, std::size_t>:
     *
     * \code
     * std::pair<std::size_t, std::size_t> pair;
     * ...
     * std::size_t key = std::hash<std::size_t>()(pair.first);
     * Utilities::HashCombine(key, pair.second);
     * \endcode
     *
     * \tparam T Type for which a std::hash specialization already exists.
     *
     * \param[in,out] seed In input current value of the hash; in output modified value.
     * \param[in] value New element which hash key is combined to the \a seed to provide a new has key.
     *
     * \return \a seed after the modification.
     */
    template<class T>
    std::size_t HashCombine(std::size_t& seed, const T& value);


    /*!
     * \brief Struct used to generate a hash key for a container with a direct accessor.
     *
     * \internal It is defined as a struct rather than a free function to make it easily usable as
     * std::unordered_map third argument. \endinternal
     */
    struct ContainerHash
    {


        /*!
         * \brief Combine all values of a container into a single hash key.
         *
         * \tparam ContainerT A container type. It must define a 'value_type' alias, a direct iterator and a size()
         * method.
         *
         * \param[in] container Container for which a hash key is sought.
         *
         * \return Hash key for the container.
         */
        template<class ContainerT>
        std::size_t operator()(const ContainerT& container) const;
    };


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Containers/UnorderedMap.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_UNORDEREDMAP_DOT_HPP_
// *** MoReFEM end header guards *** < //
