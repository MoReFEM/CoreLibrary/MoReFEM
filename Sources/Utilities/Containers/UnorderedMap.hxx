// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_UNORDEREDMAP_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_UNORDEREDMAP_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/UnorderedMap.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Utilities
{


    template<class T>
    inline std::size_t HashCombine(std::size_t& seed, const T& value)
    {
        seed ^= std::hash<T>()(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
        return seed;
    }


    template<class ContainerT>
    std::size_t ContainerHash::operator()(const ContainerT& container) const
    {
        assert(!container.empty());

        using type = typename ContainerT::value_type;

        std::size_t ret = std::hash<type>()(container[0]);

        const auto size = container.size();

        for (auto i = 1UL; i < size; ++i)
            HashCombine(ret, container[i]);

        return ret;
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_UNORDEREDMAP_DOT_HXX_
// *** MoReFEM end header guards *** < //
