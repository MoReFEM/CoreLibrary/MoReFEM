// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_VECTOR_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_VECTOR_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <vector>


namespace MoReFEM::Utilities
{


    /*!
     * \class doxygen_hide_utilities_eliminate_duplicate
     *
     * \brief Sort a std::vector and delete the possible duplicates.
     *
     * \tparam T Type of the data stored within the std::vector.
     * \param[in,out] vector Vector which should be sort and from which duplicates should be removed.
     */

    /*!
     * \copydoc doxygen_hide_utilities_eliminate_duplicate
     *
     * This version uses default comparison (less) and equality (==) predicates.
     */
    template<class T>
    void EliminateDuplicate(std::vector<T>& vector)
    {
        std::sort(vector.begin(), vector.end());
        vector.erase(std::unique(vector.begin(), vector.end()), vector.end());
    }


    /*!
     * \copydoc doxygen_hide_utilities_eliminate_duplicate
     *
     * \tparam ComparePredicateT Functor used to compare two values of the vector.
     * \tparam EqualPredicateT Predicate used to determine two quantities are equal.
     *
     * \param[in] comp Binary functor used to sort two values.
     * \param[in] equal Predicate to use to determine whether two values are equal.
     */
    template<class T, class ComparePredicateT, class EqualPredicateT>
    void EliminateDuplicate(std::vector<T>& vector, ComparePredicateT comp, EqualPredicateT equal)
    {
        std::sort(vector.begin(), vector.end(), comp);
        vector.erase(std::unique(vector.begin(), vector.end(), equal), vector.end());
    }


    /*!
     * \brief A slightly extended static_cast.
     *
     * \tparam SourceT Original type.
     * \tparam TargetT Type to which we want to cast.
     *
     * This function also handles std::vector: you can cast a std::vector<int> into a std::vector<std::size_t>
     * for instance. No other extensions are provided for other containers as there are currently no need for it.
     *
     */
    template<class SourceT, class TargetT>
    struct StaticCast
    {


        //! Function that does the actual work.
        //! \param[in] source Value which is to be cast into a \a TargetT.
        static TargetT Perform(SourceT source)
        {
            return static_cast<TargetT>(source);
        }
    };


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    //! Case when types is to be cast into itself.
    template<class T>
    struct StaticCast<T, T>
    {

        static T Perform(T source)
        {
            return source;
        }
    };


    //! Specialization for std::vector.
    template<class T, class U>
    struct StaticCast<std::vector<T>, std::vector<U>>
    {

        static std::vector<U> Perform(const std::vector<T>& source)
        {
            std::vector<U> ret(source.size());

            std::transform(source.cbegin(),
                           source.cend(),
                           ret.begin(),
                           [](T value)
                           {
                               return static_cast<U>(value);
                           });

            return ret;
        }
    };

    //! Case in which the vector is to be cast into its original type, in which case nothing is done.
    template<class T>
    struct StaticCast<std::vector<T>, std::vector<T>>
    {
      private:
        using type = std::vector<T>;

      public:
        static type Perform(type source)
        {
            return source;
        }
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_VECTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
