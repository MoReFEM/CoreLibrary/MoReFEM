// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <cassert>
#include <ctime>
// IWYU pragma: no_include <iosfwd>

#include "Utilities/Datetime/Now.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM::Utilities
{


    std::string Now(const Wrappers::Mpi& mpi)
    {
        struct tm* timeinfo{ nullptr };

        constexpr auto buffer_size = 20UL;

        std::array<char, buffer_size> buffer{};

        time_t rawtime{};

        if (mpi.IsRootProcessor())
            rawtime = std::time(nullptr);

        mpi.Broadcast(rawtime);

        timeinfo = std::localtime(&rawtime);

        [[maybe_unused]] auto check = strftime(buffer.data(), buffer_size, "%Y-%m-%d_%H:%M:%S", timeinfo);
        assert(check != 0);

        return { buffer.data() };
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
