// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_DATETIME_NOW_DOT_HPP_
#define MOREFEM_UTILITIES_DATETIME_NOW_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string> // IWYU pragma: export
// IWYU pragma: no_include <iosfwd>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Utilities
{


    /*!
     * \brief Write the current date in format 'YYYY-MM-DD_HH:MM:SS'.
     *
     * \param[in] mpi The mpi facility. Current function is there to tag output directories, so we do not want a
     * mismatcch among ranks due to slightly different time. So root processor transmits its time to all ranks.
     *
     * \return The string at the format specified above.
     *
     * \internal I could probably have used std::chrono library, but I'm not very familiar with it and lots of
     * features (including some that I think would do in a breeze what this function does) are set to appear in
     * C++20. \endinternal
     */
    std::string Now(const Wrappers::Mpi& mpi);


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_DATETIME_NOW_DOT_HPP_
// *** MoReFEM end header guards *** < //
