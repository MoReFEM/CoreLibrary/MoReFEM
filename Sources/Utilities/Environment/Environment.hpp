// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ENVIRONMENT_ENVIRONMENT_DOT_HPP_
#define MOREFEM_UTILITIES_ENVIRONMENT_ENVIRONMENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <filesystem>
#include <memory>
#include <string> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

// IWYU pragma: no_include <iosfwd>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Singleton/Singleton.hpp"  // IWYU pragma: export


namespace MoReFEM::Utilities
{


    /*!
     * \brief Provides access to shell environment variables and also some internal data that act
     * likewise.
     *
     * In Lua files, it is very convenient to provide paths using environment variables:
     *
     * \verbatim
     output_directory = "${MOREFEM_RESULT_DIR}/Heat/1D",
     \endverbatim
     *
     * This way, same file might be used to write in different outputs depending on compiler, mode, mpi or not and
     * so forth...
     *
     * However, we would like to be able to provide on command line the values to use, e.g.
     *
     \verbatim
     MoReFEM4Heat -i lua_file.lua -e MOREFEM_RESULT_DIR=/path/to/my/output/dir
     \endverbatim
     * This is impeded by the fact there are no std::setenv.
     *
     * So current class mimics this by storing pairs of strings to provide additional environment variables.
     * To avoid any ambiguity, it is forbidden to set internally a variable if the key is already known in the
     shell;
     * this test is performed on creation.
     */
    class Environment final : public Utilities::Singleton<Environment>
    {


      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Environment;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        explicit Environment();

        //! Destructor.
        virtual ~Environment() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<Environment>;

        //! Name of the class.
        static const std::string& ClassName();


        ///@}


      public:
        /*!
         * \brief Returns whether \a variable do exist or not.
         *
         * \param[in] variable Environment variable sought (either in shell or in internal storage).
         *
         * \tparam std::string: this is the forwarding reference trick.
         *
         * \return True if found.
         */
        template<class T>
        bool DoExist(T&& variable) const;


        /*!
         * \brief Get the value of an environment variable (either in shell or in internal storage).
         *
         * \param[in] variable Environment variable which value is sought.
         * If the environment variable doesn't exist, an exception is thrown.
         *
         * \tparam std::string: this is the forwarding reference trick.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Value of the \a variable environment variable.
         */
        template<class T>
        std::string GetEnvironmentVariable(T&& variable,
                                           const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Introduce a new "environment variable" in the internal storage.
         *
         * \param[in] variable The key/value new environment variable to set. The key shouldn't exist in the shell!
         *
         * \copydoc doxygen_hide_source_location
         *
         */
        void SetEnvironmentVariable(std::pair<std::string, std::string> variable,
                                    const std::source_location location = std::source_location::current());


        /*!
         * \brief Replace in the string the environment variables by their values.
         *
         * Expected format for environment variables is ${NAME} where NAME stands for the name of the environment
         * variable.
         *
         * \param[in] string Original string. Pass by value is important here; do not replace by reference!
         *
         * \return String with the environment variables replaced by their values.
         *
         * \internal This method is far from being efficient, with two string allocations; this is completely
         * assumed as it should be called only a handful of times in the initialization phase of the code.
         * \endinternal
         */
        std::string SubstituteValues(std::string string) const;


        /*!
         * \brief Same as \a SubstituteValues for a \a std::filesystem::path.
         *
         * Expected format for environment variables is ${NAME} where NAME stands for the name of the environment
         * variable.
         *
         * \param[in] path Original path.
         *
         * \return Path with the environment variables replaced by their values.
         *
         * \internal This method is far from being efficient, with two string allocations; this is completely
         * assumed as it should be called only a handful of times in the initialization phase of the code.
         * \endinternal
         */
        std::filesystem::path SubstituteValuesInPath(const std::filesystem::path& path) const;


      private:
        /*!
         * \brief "Environment" variables stored internally.
         *
         * By convention the keys present there must NOT match environment variables from the shell.
         */
        std::unordered_map<std::string, std::string> values_;
    };


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Environment/Environment.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ENVIRONMENT_ENVIRONMENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
