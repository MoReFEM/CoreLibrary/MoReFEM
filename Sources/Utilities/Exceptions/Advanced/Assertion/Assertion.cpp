// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#ifndef NDEBUG

#include <cstring>
#include <sstream> // IWYU pragma: keep
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp"


namespace // anonymous
{


    //! Call this function to set the message displayed by the assertion.
    std::string AssembleWhatMessage(std::string&& msg, const std::source_location location)
    {
        std::string ret;
        std::ostringstream stream;
        stream << "Assertion found in file " << location.file_name() << " line " << location.line();

        if (strcmp(location.function_name(), "") == 0) // currently Apple clang returns empty!
            stream << " in function " << location.function_name();

        stream << ": " << msg;
        ret = stream.str();

        return ret;
    }


} // namespace


namespace MoReFEM::Advanced
{


    Assertion::Assertion(std::string&& msg, const std::source_location location)
    : what_message_(AssembleWhatMessage(std::move(msg), location))
    { }


    Assertion::~Assertion() noexcept = default;


    const std::string& Assertion::what() const noexcept
    {
        return what_message_;
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // NDEBUG
