// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_EXCEPTIONS_ADVANCED_ASSERTION_ASSERTION_DOT_HPP_
#define MOREFEM_UTILITIES_EXCEPTIONS_ADVANCED_ASSERTION_ASSERTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#ifndef NDEBUG

#include <source_location> // IWYU pragma: export
#include <string>          // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>


namespace MoReFEM::Advanced
{


    /*!
     * \brief Class used when we want a bit more intel than the one provided by an `assert`.
     *
     * Sometimes, an `assert` doesn't provide enough information - typically we might want to know exactly where the
     * problem occurred, by leveraging `std::source_location` - or result in a gibberish output message a bit cumbersome
     * to process.
     *
     * The idea here is to introduce a dedicated class in these cases to work around both limitations aforementioned.
     *
     * The point is **not** to use exceptions everywhere: assertions and exceptions have very different goals, and we do
     * not want to hinder the speed of production code with checks that are aimed at the model developer, not the model
     * user.
     *
     * So the `Assertion` class:
     *
     * - Should **not** derive from `std::exception` (even if we will use the same API for ease of use).
     * - Will be only present within `NDEBUG` blocks.
     *
     * The point is not to replace every asserts; only those that might benefit from it (the case for which the need
     * arose is in `Wrappers::Petsc::Matrix::Internal()` - when it fails it's better to know in which matrix operation
     * exactly it occurred).
     */
    class Assertion
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message.
         * \copydoc doxygen_hide_source_location
         */

        //@}
        explicit Assertion(std::string&& msg, const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Assertion() noexcept;

        //! \copydoc doxygen_hide_copy_constructor
        Assertion(const Assertion& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Assertion(Assertion&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Assertion& operator=(const Assertion& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Assertion& operator=(Assertion&& rhs) = delete;

        ///@}

        /*!
         * \brief Display the what message from std::exception.
         *
         * \return The what() message as a char* (which reads the internal std::string so no risk of deallocation
         * issue).
         *
         * \internal As we do not inherit from `std::exception` we take the liberty to use `std::string`
         * instead of more cumbersome `char*`.
         */
        const std::string& what() const noexcept;


      private:
        //! The complete what() message.
        const std::string what_message_;
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#endif // NDEBUG

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_EXCEPTIONS_ADVANCED_ASSERTION_ASSERTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
