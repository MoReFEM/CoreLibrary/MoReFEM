// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstring>
#include <iostream>
#include <sstream> // IWYU pragma: keep
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Exceptions/Exception.hpp"


namespace // anonymous
{


    //! Call this function to set the message displayed by the exception.
    void SetWhatMessage(const std::string& msg, std::string& what_message, const std::source_location location)
    {
        std::ostringstream stream;
        stream << "Exception found in file " << location.file_name() << " line " << location.line();

        if (strcmp(location.function_name(), "") == 0) // currently Apple clang returns empty!
            stream << " in function " << location.function_name();

        stream << ": " << msg;
        what_message = stream.str();
    }


} // namespace


namespace MoReFEM
{


    Exception::Exception(const std::string& msg, const std::source_location location) : raw_message_(msg)
    {
        SetWhatMessage(msg, what_message_, location);
    }


    Exception::~Exception() noexcept = default;


    const char* Exception::what() const noexcept
    {
        return what_message_.c_str();
    }


    void ThrowBeforeMain(Exception&& exception)
    {
        std::cerr << "ERROR before main(): " << exception.what() << '\n';

        // Throw anyway in case one of the exception appears within main() function; however it
        // is foreseen to be called in functions BEFORE the main, and hence result in abortion.
        throw std::move(exception);
    }


    const std::string& Exception::GetRawMessage() const noexcept
    {
        return raw_message_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
