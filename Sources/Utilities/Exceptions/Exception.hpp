// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_EXCEPTIONS_EXCEPTION_DOT_HPP_
#define MOREFEM_UTILITIES_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <exception>       // IWYU pragma: export
#include <source_location> // IWYU pragma: export
#include <string>          // IWYU pragma: keep
// IWYU pragma: no_include <iosfwd>


namespace MoReFEM
{


    /*!
     * \brief Generic class for MoReFEM exceptions.
     */
    class Exception : public std::exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message.
         * \copydoc doxygen_hide_source_location
         */

        //@}
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() noexcept override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;

        ///@}

        /*!
         * \brief Display the what message from std::exception.
         *
         * \return The what() message as a char* (which reads the internal std::string so no risk of deallocation
         * issue).
         */
        virtual const char* what() const noexcept override final;

        /*!
         * \brief Display the raw message (Without file and line).
         *
         * Might be useful if exception is caught to rewrite a more refined message.
         *
         * Before introducing this, we could end up in some cases with something like:
         * \verbatim
         * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined
         * finite element space 1: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 101:
         * Domain 1 is not defined!
         * \endverbatim
         *
         * Clearly it is nicer to provide:
         * \verbatim
         * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined
         * finite element space 1: Domain 1 is not defined!
         * \endverbatim
         *
         * \return Exception error message without information about file and line in which the exception was invoked.
         */
        const std::string& GetRawMessage() const noexcept;


      private:
        //! The complete what() message (with the location part)
        std::string what_message_;

        //! Incomplete message (might be useful if we catch an exception to tailor a more specific message).
        std::string raw_message_;
    };


    /*!
     * \brief Print the what() of an exception that occurred before the main() call.
     *
     * In the Factory idiom, some registering functions are intended to be called BEFORE the main().
     *
     * For this reason, an additional step is added (namely print to std::cerr the explanation of the exception)
     * so that developer might find quickly what happened.
     *
     * \param[in] exception Exception to throw.
     */
    [[noreturn]] void ThrowBeforeMain(Exception&& exception);


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_EXCEPTIONS_EXCEPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
