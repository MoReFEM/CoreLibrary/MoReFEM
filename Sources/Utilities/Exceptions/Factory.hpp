// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_EXCEPTIONS_FACTORY_DOT_HPP_
#define MOREFEM_UTILITIES_EXCEPTIONS_FACTORY_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::ExceptionNS::Factory
{

    //! Generic exception for factory.
    class Exception : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message.
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;

        //! Destructor
        virtual ~Exception() override;
    };


    //! Thrown when a new element try to register with an already existing name.
    class UnableToRegister final : public Exception
    {
      public:
        /*!
         * \brief Called when the code was unable to register a new object.
         *
         * Insertion failed, so the most likely explanation is that another object with the same name already
         existed.
         *
         * \param[in] object_name Name of the object that failed to be inserted in the factory_content.
         * \param[in] factory_content Content of the factory for which the problem arose. For instance,
         "GeometricElt".
         * \copydoc doxygen_hide_source_location

         */
        explicit UnableToRegister(const std::string& object_name,
                                  const std::string& factory_content,
                                  const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        UnableToRegister(const UnableToRegister& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnableToRegister(UnableToRegister&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnableToRegister& operator=(const UnableToRegister& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnableToRegister& operator=(UnableToRegister&& rhs) = default;

        //! Destructor
        virtual ~UnableToRegister() override;
    };


    //! Thrown when trying to create an object which name is not registered.
    class UnregisteredName final : public Exception
    {
      public:
        /*!
         * \brief Thrown when trying to create an object which name is not registered.
         *
         * \param[in] object_name Name provided to create a new object.
         * \param[in] factory_content Content of the factory for which the problem arose. For instance,
         "GeometricElt".
         * \copydoc doxygen_hide_source_location
         */
        explicit UnregisteredName(const std::string& object_name,
                                  const std::string& factory_content,
                                  const std::source_location location = std::source_location::current());


        //! Destructor
        virtual ~UnregisteredName() override;

        //! \copydoc doxygen_hide_copy_constructor
        UnregisteredName(const UnregisteredName& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnregisteredName(UnregisteredName&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnregisteredName& operator=(const UnregisteredName& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnregisteredName& operator=(UnregisteredName&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::Factory


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_EXCEPTIONS_FACTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
