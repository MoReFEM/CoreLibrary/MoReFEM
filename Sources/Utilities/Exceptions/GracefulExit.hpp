// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_EXCEPTIONS_GRACEFULEXIT_DOT_HPP_
#define MOREFEM_UTILITIES_EXCEPTIONS_GRACEFULEXIT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <exception>
#include <source_location> // IWYU pragma: export
#include <string>


namespace MoReFEM::ExceptionNS
{


    //! Exception when we want to exit graceful with a EXIT_SUCCESS return code.
    class GracefulExit : private std::exception
    {
      public:
        //! Alias to parent.
        using parent = std::exception;

      public:
        /*!
         * \brief Constructor with simple message.
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit GracefulExit(const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        GracefulExit(const GracefulExit& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        GracefulExit(GracefulExit&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        GracefulExit& operator=(const GracefulExit& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        GracefulExit& operator=(GracefulExit&& rhs) = default;

        //! Destructor
        virtual ~GracefulExit() override;

        /*!
         * \brief Display the what message from std::exception.
         *
         * \return The what() message as a char* (which reads the internal std::string so no risk of deallocation
         * issue).
         */
        virtual const char* what() const noexcept override final;

      private:
        //! The complete what() message (with the location part)
        std::string what_message_;
    };


} // namespace MoReFEM::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_EXCEPTIONS_GRACEFULEXIT_DOT_HPP_
// *** MoReFEM end header guards *** < //
