// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <iostream>
#include <sstream>
#include <string>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM::ExceptionNS
{


    void PrintAndAbort(const Wrappers::Mpi& mpi, const std::string& exception_message)
    {

        // Trick to ensure there is no interleaving between processors: use a ostringstream so that only one
        // operation is given to std::cout. std::cout is used rather than std::cerr because it is buffered
        // (std::cerr here could lead to interleaving).
        std::ostringstream oconv;
        oconv << mpi.GetRankPrefix() << " Exception caught: " << exception_message << '\n';
        oconv << "Program has therefore been terminated." << '\n';
        std::cout << oconv.str();

        // Call MPI_Abort to shut down all process.
        // I followed a recommendation of
        // http://www.ucs.cam.ac.uk/docs/course-notes/unix-courses/MPI/files/mphil/talk-01b.pdf and use it on
        // MPI_COMM_WORLD whatever the communicator in mpi local variable.
        PRAGMA_DIAGNOSTIC(push)
        PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
        // NOLINTNEXTLINE(bugprone-casting-through-void)
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);
        PRAGMA_DIAGNOSTIC(pop)
    }


} // namespace MoReFEM::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
