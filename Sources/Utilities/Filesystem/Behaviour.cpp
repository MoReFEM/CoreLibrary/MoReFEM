// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <ostream>

#include "Utilities/Filesystem/Behaviour.hpp"


namespace MoReFEM::FilesystemNS
{

    std::ostream& operator<<(std::ostream& stream, behaviour rhs)
    {
        switch (rhs)
        {
        case behaviour::overwrite:
            stream << "overwrite";
            break;
        case behaviour::ask:
            stream << "ask";
            break;
        case behaviour::quit:
            stream << "quit";
            break;
        case behaviour::ignore:
            stream << "ignore";
            break;
        case behaviour::read:
            stream << "read";
            break;
        case behaviour::create:
            stream << "create";
            break;
        case behaviour::create_if_necessary:
            stream << "create_if_necessary";
            break;
        case behaviour::undefined:
            stream << "undefined";
            break;
        }

        return stream;
    }


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
