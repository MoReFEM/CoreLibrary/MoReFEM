// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_BEHAVIOUR_DOT_HPP_
#define MOREFEM_UTILITIES_FILESYSTEM_BEHAVIOUR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::FilesystemNS
{


    /*!
     * \class doxygen_hide_directory_behaviour_desc
     *
     * <ul>
     * <li> overwrite: Remove the pre-existing one and recreate it.</li>
     * <li> quit: Quit the program if the directory exists.</li>
     * <li> read: this mode expects the directory to exist and throws an exception otherwise.</li>
     * <li> create: create a new directory - it is expected here it doesn't exist yet (and an exception is thrown if it
     * does). </li>
     * <li> create_if_necessary: create the directory if it doesn't exist yet
     * <li> ask: Ask the end user if he wants to override or not. If he chooses not to do so, the program ends.
     * Mpi is properly handled (the interface is properly rerouted to root processor which is the sole able to
     * communicate with stdin).</li>
     * <li> ignore: Do nothing - whether the directory exist or not. This might be useful if you want to set the
     * behaviour later; I would nonetheless not recommend using this possibility much as we lose part of the appeal of
     * using a class such as \a Directory doing so.</li>
     * </ul>
     *
     * Remember that \a Directory constructor <b>never</b> acts directly on the filesystem; you need to call \a
     * ActOnFilesystem method if you want an action on the filesystem (typically create a directory).
     */


    /*!
     * \brief Enum class to determine how to handle the case
     *
     * \copydoc doxygen_hide_directory_behaviour_desc
     */
    enum class behaviour { overwrite, ask, quit, read, create, create_if_necessary, ignore, undefined };

    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, behaviour rhs);


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_BEHAVIOUR_DOT_HPP_
// *** MoReFEM end header guards *** < //
