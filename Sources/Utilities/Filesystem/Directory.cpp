// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdint>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <optional>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Filesystem/Directory.hpp"

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"


namespace MoReFEM::FilesystemNS
{


    namespace // anonymous
    {


        enum class ask_status : std::uint8_t { no_preexisting = 0, remove_yes, remove_no };

        ask_status AskCaseHelper(const std::string& message);

        ask_status AskCaseRootProcessor(const std::string& wildcard_path, const std::vector<int>& result);


        std::string ComputeWildcardPath(const Directory& directory);

        std::string XCodeWarning();

        void AlreadyExistingCase(std::filesystem::directory_entry& path,
                                 behaviour directory_behaviour,
                                 std::source_location location);

        void NotExistingCase(std::filesystem::directory_entry& path,
                             behaviour directory_behaviour,
                             std::source_location location = std::source_location::current());


        // Create a directory_entry with canonical formatting and with trailing slash.
        // Also substitute adequately environment variables.
        std::filesystem::directory_entry PreparePath(std::filesystem::path&& path, std::source_location location);

    } // namespace


    Directory::Directory(const Wrappers::Mpi& mpi,
                         const std::filesystem::path& a_path,
                         behaviour directory_behaviour,
                         add_rank do_add_rank,
                         const std::source_location location)
    : mpi_(&mpi), with_rank_(do_add_rank == add_rank::yes)
    {
        SetBehaviour(directory_behaviour, location);
        std::filesystem::path path{ a_path };

        switch (do_add_rank)
        {
        case add_rank::no:
            break;
        case add_rank::yes:
        {
            std::ostringstream oconv;
            oconv << "Rank_" << mpi.GetRank<std::size_t>();
            path /= std::filesystem::path(oconv.str());
            break;
        }
        }

        directory_entry_ = PreparePath(std::move(path), location);
    }


    Directory::Directory(const std::filesystem::path& a_path,
                         behaviour directory_behaviour,
                         const std::source_location location)
    : mpi_(nullptr), with_rank_(false)
    {
        SetBehaviour(directory_behaviour, location);
        auto path = a_path;
        directory_entry_ = PreparePath(std::move(path), location);
    }


    Directory::Directory(const Directory& parent_directory,
                         std::filesystem::path&& subdirectory,
                         std::optional<behaviour> directory_behaviour,
                         const std::source_location location)
    : mpi_(parent_directory.GetMpiPtr()),
      directory_behaviour_(directory_behaviour.has_value() ? directory_behaviour.value()
                                                           : parent_directory.GetBehaviour()),
      with_rank_(parent_directory.IsWithRank())
    {
        auto path = parent_directory.GetDirectoryEntry().path();
        path /= subdirectory;
        directory_entry_ = PreparePath(std::move(path), location);
    }


    bool IsSameDirectory(const Directory& lhs, const Directory& rhs)
    {
        return (lhs.GetDirectoryEntry().path().lexically_normal() == rhs.GetDirectoryEntry().path().lexically_normal());
    }


    void Directory::ActOnFilesystem(const std::source_location location) const
    {
        // First tackle - if relevant - the 'ask' case policy:
        // - The question of overwriting can only be answered on the root rank.
        // - So the information about the existence or not of the path for a given rank must be first send on root
        // processor.
        // - Root processor asks whether the directories must be overwritten or not. If 'no', a GracefulExit occurs.
        // - Then root processor must tell the other processors to remove the directories.
        // All the other policies may be handled without interprocessor communication.
        if (directory_behaviour_ == behaviour::ask)
        {
            assert(IsMpi()
                   && "Directory creation can only be done with IsMpi() (see documentation "
                      "to understand why).");
            CollectAnswer(location);
        }

        // At this point, each rank may do its own bidding.
        directory_entry_.refresh();

        if (directory_entry_.exists())
        {
            if (directory_entry_.is_directory())
                AlreadyExistingCase(directory_entry_, directory_behaviour_, location);
            else
                throw FilesystemNS::DirectoryNS::ExceptionNS::AlreadyExistsButNotAsDirectory(directory_entry_,
                                                                                             location);
        } else
            NotExistingCase(directory_entry_, directory_behaviour_, location);
    }


    void Directory::CollectAnswer(const std::source_location location) const
    {
        assert(directory_behaviour_ == behaviour::ask);

        decltype(auto) mpi = GetMpi();
        const auto rank = mpi.GetRank<int>();

        directory_entry_.refresh();

        const bool do_directory_entry_exist = directory_entry_.is_directory();

        const std::vector<int> sent_data{ do_directory_entry_exist ? rank : -1 };
        std::vector<int> gathered_data;

        mpi.Gather(sent_data, gathered_data);

        ask_status ret = ask_status::no_preexisting;

        if (mpi.IsRootProcessor())
        {
            if (std::ranges::any_of(gathered_data,

                                    [](auto i)
                                    {
                                        return i != -1;
                                    }))
            {
                ret = AskCaseRootProcessor(ComputeWildcardPath(*this), gathered_data);
            }
        }

        mpi.Barrier();

        // Tell the ranks whether they may remove the directory.
        int do_remove{};

        if (mpi.IsRootProcessor())
            do_remove = EnumUnderlyingType(ret);

        mpi.Broadcast(do_remove);

        if (do_remove == EnumUnderlyingType(ask_status::remove_no))
            throw ExceptionNS::GracefulExit(location);
    }


    void Directory::Remove(const std::source_location location)
    {
        if (!DoExist())
            throw FilesystemNS::DirectoryNS::ExceptionNS::Unexisting(GetDirectoryEntry(), location);

        std::filesystem::remove_all(GetDirectoryEntry().path());
        GetNonCstDirectoryEntry().refresh();
    }


    std::ostream& operator<<(std::ostream& stream, const Directory& rhs)
    {
        stream << rhs.GetPath();
        return stream;
    }


    File Directory::AddFile(std::string_view filename) const
    {
        auto ret = GetDirectoryEntry().path();

        ret /= std::filesystem::path(filename);

        return File(std::move(ret));
    }


    void Directory::SetBehaviour(behaviour new_behaviour, const std::source_location location)
    {
        if (!IsWithRank())
        {
            switch (new_behaviour)
            {
            case behaviour::overwrite:
            case behaviour::ask:
            case behaviour::quit:
            case behaviour::create:
            case behaviour::create_if_necessary:
                throw FilesystemNS::DirectoryNS::ExceptionNS::SetBehaviourNoRank(new_behaviour, location);
            case behaviour::ignore:
            case behaviour::read:
                break;
            case behaviour::undefined:
            {
                assert(false && "This value is onlyt meant to provide a value before initialization.");
                exit(EXIT_FAILURE);
            }
            }
        }

        directory_behaviour_ = new_behaviour;
    }


    void Directory::AddSubdirectory(std::string_view subdirectory)
    {
        const std::filesystem::path new_path{ GetDirectoryEntry().path() / std::filesystem::path{ subdirectory } };
        directory_entry_ = std::filesystem::directory_entry{ new_path };
    }


    bool IsFirstSubfolderOfSecond(const Directory& possible_enclosed_directory,
                                  const Directory& possible_enclosing_directory)
    {
        // Here I didn't manage to implement it properly while acting on std::filesystem::path: I had issues with the
        // facts that:
        // - Trailing slashes presence or not could lose it (see the following entry for description of the issue:
        // https://www.reddit.com/r/cpp_questions/comments/g3tqjn/stdfilesystempath_comparison_normalization/
        // This issue has been circumvented by adding explicitly in \a Directory a trailing slash
        // - The fact that for some reason the last real element of iteration was "" (before the end iterator). Due
        // to this behaviour, the result was never true...
        // I therefore worked aroung by working on the underlying string; I hope it is robust enough (there
        // are many safeties in Directory: make_preferred() is used and `std::weakly_canonical` is called to ensure
        // symbolic links and relative paths are also handled properly).
        const auto& possible_enclosed_directory_as_path =
            possible_enclosed_directory.GetDirectoryEntry().path().string();
        const auto& possible_enclosing_directory_as_path =
            possible_enclosing_directory.GetDirectoryEntry().path().string();

        auto [iterator1, iterator2] = std::mismatch(possible_enclosing_directory_as_path.begin(),
                                                    possible_enclosing_directory_as_path.end(),
                                                    possible_enclosed_directory_as_path.begin());

        return iterator1 == possible_enclosing_directory_as_path.end();
    }


    namespace // anonymous
    {


        std::string RankList(const std::vector<int>& result)
        {
            std::vector<int> helper;
            helper.reserve(result.size());

            std::ranges::copy_if(result,

                                 std::back_inserter(helper),
                                 [](auto i)
                                 {
                                     return i != -1;
                                 });

            std::ostringstream oconv;
            Utilities::PrintContainer<>::Do(helper, oconv);
            return oconv.str();
        }


        ask_status AskCaseRootProcessor(const std::string& wildcard_path, const std::vector<int>& result)
        {
            std::ostringstream oconv;

            const std::string rank_list = RankList(result);

            oconv << "Directories '" << wildcard_path << "' already exist for ranks " << rank_list
                  << ". Do you want to remove them? [y/n]" << '\n';
            oconv << XCodeWarning();

            return AskCaseHelper(oconv.str());
        }


        ask_status AskCaseHelper(const std::string& message)
        {
            std::string answer;

            while (answer != "y" && answer != "n")
            {
                do
                {
                    if (!std::cin)
                    {
                        constexpr auto arbitrary_high_value{ 10000 };
                        std::cin.clear(); // clear the states of std::cin, putting it back to `goodbit`.
                        std::cin.ignore(arbitrary_high_value,
                                        '\n'); // clean-up what might remain in std::cin before using it again.
                    }

                    std::cout << message << '\n';

                    std::cin >> answer;

#ifndef NDEBUG
                    std::cout << "\t answer read = |" << answer << '|' << '\n';
#endif // NDEBUG


                } while (!std::cin);
            }

            return answer == "y" ? ask_status::remove_yes : ask_status::remove_no;
        }


        std::string ComputeWildcardPath(const Directory& directory)
        {
            decltype(auto) mpi = directory.GetMpi();
            assert(mpi.IsRootProcessor());

            auto ret = directory.GetPath();

            [[maybe_unused]] auto Nmodif =
                Utilities::String::Replace("Rank_" + std::to_string(mpi.GetRank<int>()), "Rank_*", ret);

            assert(Nmodif == 1 && "Should be relevant only for directory created with add_rank::yes.");

            return ret;
        }


        std::string XCodeWarning()
        {
            return "\tNote for XCode user: doesn't work in XCode  in parallel.. You should consider "
                   "--overwrite_directory flag in the arguments on command line.";
        }


        void AlreadyExistingCase(std::filesystem::directory_entry& path,
                                 behaviour directory_behaviour,
                                 const std::source_location location)
        {
            switch (directory_behaviour)
            {
            case behaviour::ask:
            case behaviour::overwrite:
                assert(path.exists());
                std::filesystem::remove_all(path.path());
                std::filesystem::create_directories(path);
                path.refresh();
                break;
            case behaviour::quit:
            {
                std::cout << "Directory '" << path.path().native()
                          << "' already exists; an abortion of the program is therefore "
                             "scheduled."
                          << '\n';
                throw ExceptionNS::GracefulExit(location);
            }
            case behaviour::read:
            case behaviour::create_if_necessary:
            case behaviour::ignore:
                break;
            case behaviour::create:
                throw FilesystemNS::DirectoryNS::ExceptionNS::ShouldntExist(path, location);
            case behaviour::undefined:
            {
                assert(false && "Function shouldn't have been called with this behaviour value!");
                exit(EXIT_FAILURE);
            }
            }
        }


        void NotExistingCase(std::filesystem::directory_entry& path,
                             behaviour directory_behaviour,
                             const std::source_location location)
        {
            switch (directory_behaviour)
            {
            case behaviour::ask:
            case behaviour::overwrite:
            case behaviour::quit:
            case behaviour::create:
            case behaviour::create_if_necessary:
            {
                std::filesystem::create_directories(path);
                path.refresh();
                break;
            }
            case behaviour::read:
            {
                path.refresh();

                if (!path.exists())
                    throw FilesystemNS::DirectoryNS::ExceptionNS::Unexisting(path, location);
            }
            break;
            case behaviour::ignore:
                break;
            case behaviour::undefined:
            {
                assert(false && "Function shouldn't have been called with this behaviour value!");
                exit(EXIT_FAILURE);
            }
            }
        }


        std::filesystem::directory_entry PreparePath(std::filesystem::path&& path,
                                                     [[maybe_unused]] std::source_location location)
        {
            // Unfortunately C++ API is not fantastic here: canonical form can't decide whether there is a
            // trailing slash or not... and lexicographical comparison may fail due to presence / absence
            // of such a trailing slash. So we add one to ensure normalized version gets one (if there is
            // already one it will be removed by the next lines).
            // See this Reddit question
            // https://www.reddit.com/r/cpp_questions/comments/g3tqjn/stdfilesystempath_comparison_normalization/
            // from someone who reached the same conclusion.

#ifndef NDEBUG
            if (path.empty())
                throw FilesystemNS::DirectoryNS::ExceptionNS::EmptyPath(location);
#endif

            path.make_preferred();

            decltype(auto) env = Utilities::Environment::GetInstance();
            path = env.SubstituteValuesInPath(path);

            // std::filesystem::weakly_canonical presents a weird behaviour regarding trailing slash depending
            // on the existence of the filesystem or not (see #1773 for an illustration); hence this clumsy code
            // below to guarantee unicity in both existing / not existing cases.
            {
                auto str = path.string();
                Utilities::String::StripRight(str, "/");
                path = std::filesystem::path{ str };
            }

            return std::filesystem::directory_entry{ std::filesystem::weakly_canonical(path) };
        }


    } // namespace


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
