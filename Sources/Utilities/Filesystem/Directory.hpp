// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_DIRECTORY_DOT_HPP_
#define MOREFEM_UTILITIES_FILESYSTEM_DIRECTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <optional>
#include <vector>

#include "Utilities/Filesystem/Behaviour.hpp"            // IWYU pragma: export
#include "Utilities/Filesystem/Exceptions/Directory.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM::FilesystemNS
{


    /*!
     * \brief Whether a subdirectory with 'Rank_' + * rank id *  must be added to the directory or not.
     *
     * This should be the standard case for output directories, but it is obviously not relevant for most of the
     * input ones...
     */
    enum class add_rank { no, yes };


    /*!
     * \class doxygen_hide_directory_behaviour_param
     *
     * \param[in] directory_behaviour Behaviour of the directory, among:
     *
     * \copydoc doxygen_hide_directory_behaviour_desc
     */


    /*!
     * \class doxygen_hide_filesystem_rant
     *
     * This class is built upon \a std::filesystem, but as I am not fond of the API provided in the STL I encapsulated
     it here.
     *
     * Among the thing I don't like in \a std::filesystem:
     * - \a std::filesystem::directory_entry is used to represent directories, files, symbolic links, etc... and
     therefore doesn't carry much semantic information.
     * Using classes named \a Directory or \a File is much more straightforward.
     * - There are two kind of objects: \a std::filesystem::directory_entry and \a std::filesystem::path, which are
     related (the former gets a \a path() method
     * which returns the later) but the relationship between both is not straightforward enough for my taste.
     * - There is a quirk in \a std::filesystem::directory_entry: the information returned through methods such as \a
     exists may not be up-to date: if you
     * do something like:
     *
     * \code
     std::filesystem::path path { ... };
     std::filesystem::directory_entry dir_entry { path };

     std::cout << dir_entry.exists() << std::endl; // Assuming the path exists, print true
     bool is_properly_deleted = std::filesystem::remove(path);
     assert(is_properly_deleted);
     std::cout << dir_entry.exists() << std::endl; // Still print true!!!
     \endcode
     * You need to call \a refresh if you want the information to be up-to-date, but this is a non constant method...
     meaning you need to use non constant
     * references for your filesystem-related objects!
     *
     * The latter is the reason \a directory_entry_ data attribute is mutable: I want to be able to refresh properly
     under the hood but indicate clearly the filesystem
     * object is not to be manipulated carelessly.
     *
     */


    /*!
     * \brief Class used to manage directories in MoReFEM.
     *
     * \copydoc doxygen_hide_filesystem_rant
     *
     * Moreover, this \a Directory class is also used to deal properly with the parallel nature of the library: we do
     not want to introduce race conditions when
     * we want to create a directory, and we don't want either to make assumption upon the nature of the filesystem used
     in parallel (e.g. does each rank
     * write on the same filesystem or on a different one?)
     *
     * A simple way to do so which alleviate many problems is simply to automatically add a subdirectory indicating the
     rank; the convention in MoReFEM
     * is to simply call it \a Rank_i where \a i is the rank. This way, each rank manage its outputs as it wishes.
     *
     * Of course, it is not always what we want: for input data there is not much sense doing this. For some \a
     behaviour such as \a read or \a ignore, it is
     * therefore possible to use in some constructors an argument to specify you do not want to extend with such Rank
     subdir your underlying path.
     *
     * It can't be done for all \a behaviour: we really want to force users to use the safety net we provided within the
     class. If you really wat to circumvent it
     * (might be the case for some tests, but please really don't do that in a model...) you may use the STL \a
     filesystem :
     *
     * \code
     std::filesystem::path directory_to_create { ... };
     std::filesystem::create_directories(directory_to_create);
     FilesystemNS::Directory directory { directory_to_create, FilesystemNS::behaviour::ignore);
     \endcode
     *
     * You may then benefit from \a Directory API such as \a AddFile() method,
     */
    class Directory
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Directory;

        //! Smart pointer to hold it.
        using unique_ptr = std::unique_ptr<self>;

        //! Smart pointer to hold it.
        using const_unique_ptr = std::unique_ptr<const self>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_directory_behaviour_param
         *
         * \param[in] path Unix path of the directory.
         * \param[in] do_add_rank Whether a "Rank_" part is added to the path.
         * \copydoc doxygen_hide_filesystem_thread_safe
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit Directory(const Wrappers::Mpi& mpi,
                           const std::filesystem::path& path,
                           behaviour directory_behaviour,
                           add_rank do_add_rank = add_rank::yes,
                           const std::source_location location = std::source_location::current());


        /*!
         * \brief Simple constructor: no mpi involved - so no rank whatsoever added.
         *
         * This constructor can only be used with 'read' and 'ignore' policy (this evacuates race conditions and
         * management on whether each rank is on the same filesystem or not).
         *
         * \copydoc doxygen_hide_directory_behaviour_param
         *
         * \param[in] path Path of the directory. Will be adjusted to match your OS filesystem.
         * \copydoc doxygen_hide_filesystem_thread_safe
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit Directory(const std::filesystem::path& path,
                           behaviour directory_behaviour,
                           const std::source_location location = std::source_location::current());

        /*!
         * \class doxygen_hide_subdirectory_constructor
         *
         * \param[in] parent_directory The directory into which the new one is created (or read - depends on behaviour).
         * The constructed directory takes the same attributes than its parent directory - except obviously for the
         * path.
         * \param[in] directory_behaviour Directory behaviour; if \a
         * nullopt the behaviour of the parent directory is used (you should stick with this choice most of the time),
         *
         */

        /*!
         * \brief Constructor of a \a Directory which is a subdirectory on an already existing \a parent_directory.
         *
         * \param[in] subdirectory Name of the subdirectory relative to \a parent_directory.
         * \copydetails doxygen_hide_subdirectory_constructor
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit Directory(const Directory& parent_directory,
                           std::filesystem::path&& subdirectory,
                           std::optional<behaviour> directory_behaviour = std::nullopt,
                           const std::source_location location = std::source_location::current());


        /*!
         * \brief Constructor of a \a Directory which is several layers of directories inside an already existing
         * \a parent_directory.
         *
         * \param[in] layers Names of the layers of subdirectories. For instance if { "Foo", "Bar"}, it will create
         * inside \a parent_directory the subdirectory "Foo/Bar/".
         * \copydetails doxygen_hide_subdirectory_constructor
         * \copydoc doxygen_hide_filesystem_thread_safe
         *
         * \tparam StringT Type of \a subdirectory, which may actually be anything for which operator<< has been
         * overloaded (avoid nonetheless values with spaces inside...)
         * \copydoc doxygen_hide_source_location
         */
        template<class StringT>
        explicit Directory(const Directory& parent_directory,
                           std::vector<StringT>&& layers,
                           std::optional<behaviour> directory_behaviour = std::nullopt,
                           const std::source_location location = std::source_location::current());

        //! Destructor.
        ~Directory() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Directory(const Directory& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Directory(Directory&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Directory& operator=(const Directory& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Directory& operator=(Directory&& rhs) = delete;

        ///@}

        //! Get the underlying Unix path.
        const std::string& GetPath() const noexcept;

        /*!
         * \brief Define path to a file in the directory.
         *
         * \param[in] filename Name of the file.
         *
         * \return \a File object.
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        File AddFile(std::string_view filename) const;

        //! Returns the behaviour.
        behaviour GetBehaviour() const noexcept;

        /*!
         * \brief Changes the behaviour
         *
         * \param[in] new_behaviour As written on the tag...
         *
         * \copydoc doxygen_hide_source_location
         *
         * \attention You should be cautious before using this one; this was introduced to soften the behaviour (i.e.
         * overwrite -> read) for some tests which create first directories and then use some post-processing on them.
         */
        void SetBehaviour(behaviour new_behaviour,
                          const std::source_location location = std::source_location::current());

        //! Accessor to Mpi object (if relevant).
        const Wrappers::Mpi& GetMpi() const noexcept;

        //! Whether \a mpi is considered or not.
        bool IsMpi() const noexcept;

        //! Whether a rank was given in construction.
        bool IsWithRank() const noexcept;

        //! Tells whether the folder exists on the filesystem and is properly a directory.
        bool DoExist() const noexcept;

        /*!
         * \brief Remove the directory on disk.
         *
         * \copydoc doxygen_hide_source_location
         */
        void Remove(const std::source_location location = std::source_location::current());


      public:
        /*!
         * \brief Modify current directory so that it points to a subdirectory.
         *
         * \attention It is NOT the preferred way to create a subdirectory: it is much better to create another
         * \a Directory object with the proper constructor to do so. However, it is handy as it cut short the code
         * for testing models outputs and comparing them to the stored expected results.
         *
         * \param[in] subdirectory Name of the subdirectory layer that will hence be pointed by current object.
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        void AddSubdirectory(std::string_view subdirectory);

        //! Constant accessor to the underlying \a std::filesystem::directory_entry object.
        const std::filesystem::directory_entry& GetDirectoryEntry() const noexcept;

        /*!
         * \brief The method which will enact on the filesystem what has been set in the object.
         *
         * For instance, if \a behaviour_ is \a behaviour::create the directory will be created on the filesystem
         * (or STL will throw an exception).
         *
         * \copydoc doxygen_hide_source_location
         */
        void ActOnFilesystem(const std::source_location location = std::source_location::current()) const;

      private:
        //! Non constant accessor to the underlying \a std::filesystem::directory_entry object.
        std::filesystem::directory_entry& GetNonCstDirectoryEntry() noexcept;

      private:
        /*!
         * \brief Helper method for the 'ask' behaviour.
         *
         * This behaviour requires inter-processor communication: each rank must tell root processor whether the
         * directory already exist or not, and if some do root processor must ask the user what to do. Then
         * it musts transmit back answers to each rank, which then must take action.
         *
         * \copydoc doxygen_hide_source_location
         */

        void CollectAnswer(const std::source_location location = std::source_location::current()) const;

        //! Accessor to Mpi pointer - required to define some constructors.
        const Wrappers::Mpi* GetMpiPtr() const noexcept;

      private:
        /*!
         * \brief Path of the directory.
         *
         * The 'mutable' is due to a quirk in STL I dislike: some methods of \a std::filesystem::directory_entry such as
         * \a exists may provide a wrong answer if \a refresh() is not called first... but the latter is non constant.
         * To circumvent the potential wrong answer I this need to call \a refresh() first, but  aside from that the
         * methods such as \a DoExist() are morally const...
         */
        mutable std::filesystem::directory_entry directory_entry_;

        //! \a Mpi object.
        const Wrappers::Mpi* const mpi_;

        /*!
         * \brief What to do at the construction of the \a Directory object.
         *
         * Options are:
         *
         * \copydoc doxygen_hide_directory_behaviour_desc
         */
        behaviour directory_behaviour_{ behaviour::undefined };

        //! Whether the rank was given in construction.
        const bool with_rank_;
    };


    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const Directory& rhs);

    /*!
     * \brief Check whether two \a Directory refers to the same directory on the filesystem.
     *
     * \param[in] lhs First \a File object
     * \param[in] rhs Second \a File object
     *
     * \return True of both actually shares the same path. If one is a symbolic link toward the other, false will be written: we really check only the
     * lexicographic value of the path.
     *
     * \internal std::filesystem::equivalent() is not used as one of its prerequisite is that both files exist on the filesystem.
     */
    bool IsSameDirectory(const Directory& lhs, const Directory& rhs);

    /*!
     * \brief Create a \a Directory which contains path related to \a rank - where rank is NOT necessarily the one
     * held by mpi object.
     *
     * \attention Should only be used on mpi root processor!
     *
     * This is only for some specific functions and should be used with great care; behaviour is set to 'read'
     * and should be changed with even greater care...
     *
     * \param[in] root_directory The directory matching the root rank.
     * \param[in] rank Rank for which the new directory is required.
     *
     * \return The directory related to the chosen \a rank argument. It is created with the 'read' behaviour - and
     * this one should changed with great care! (or better left unchanged).
     */
    Directory GetRankDirectory(const Directory& root_directory, rank_type rank);


    /*!
     * \brief Check whether \a possible_enclosed_directory is a directory inside \a possible_enclosing_directory
     *
     * This function use the canonical path so is assumed to handle properly the symbolic links or relative paths.
     *
     * \param[in] possible_enclosing_directory The \a Directory that might be enclosed by the other.
     * \param[in] possible_enclosed_directory The \a Directory that might be enclosing by the other.
     *
     * \return True if \a possible_enclosed_directory is inside \a possible_enclosing_directory
     *
     * \internal `std::filesystem::path` iterator doesn't behave as I want, so I perform the `std::mismatch`
     * check upon the strings, not the folder parts. That is not very satisfactory and may prove to be brittle (even if
     * of course I took all the precaution I could think of about the normalization of the path); see the implementation
     * for more details put in a comment there.
     */
    bool IsFirstSubfolderOfSecond(const Directory& possible_enclosed_directory,
                                  const Directory& possible_enclosing_directory);


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Filesystem/Directory.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_DIRECTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
