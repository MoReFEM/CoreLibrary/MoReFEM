// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_DIRECTORY_DOT_HXX_
#define MOREFEM_UTILITIES_FILESYSTEM_DIRECTORY_DOT_HXX_
// IWYU pragma: private, include "Utilities/Filesystem/Directory.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <filesystem>
#include <iosfwd> // IWYU pragma: keep
#include <optional>
#include <source_location>
#include <sstream>
// IWYU pragma: no_include <string>
#include <string_view>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Internal/CheckForSubdirectoryConstructor.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::FilesystemNS { enum class behaviour; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::FilesystemNS
{


    template<class StringT>
    Directory::Directory(const Directory& parent_directory,
                         std::vector<StringT>&& subdirectories,
                         std::optional<behaviour> directory_behaviour,
                         const std::source_location location)
    : mpi_(parent_directory.GetMpiPtr()), with_rank_(parent_directory.IsWithRank())
    {
        SetBehaviour(directory_behaviour.has_value() ? directory_behaviour.value() : parent_directory.GetBehaviour(),
                     location);

        ::MoReFEM::Internal::FilesystemNS::CheckForSubdirectoryConstructor(parent_directory, location);

        std::ostringstream oconv;
        oconv << parent_directory.GetPath() << "/";
        Utilities::PrintContainer<>::Do(subdirectories,
                                        oconv,
                                        PrintNS::Delimiter::separator("/"),
                                        PrintNS::Delimiter::opener(""),
                                        PrintNS::Delimiter::closer(""));

        auto path = std::filesystem::path{ oconv.str() };
        path = std::filesystem::weakly_canonical(path);
        directory_entry_ = std::filesystem::directory_entry{ path };
    }


    inline const std::string& Directory::GetPath() const noexcept
    {
        return directory_entry_.path().native();
    }


    inline const std::filesystem::directory_entry& Directory::GetDirectoryEntry() const noexcept
    {
        return directory_entry_;
    }


    inline std::filesystem::directory_entry& Directory::GetNonCstDirectoryEntry() noexcept
    {
        return const_cast<std::filesystem::directory_entry&>(GetDirectoryEntry());
    }


    inline const Wrappers::Mpi& Directory::GetMpi() const noexcept
    {
        assert(IsMpi());
        return *mpi_;
    }


    inline bool Directory::DoExist() const noexcept
    {
        directory_entry_.refresh();
        return directory_entry_.is_directory();
    }


    inline bool Directory::IsMpi() const noexcept
    {
        return mpi_ != nullptr;
    }


    inline const Wrappers::Mpi* Directory::GetMpiPtr() const noexcept
    {
        return mpi_; // might be nullptr
    }


    inline behaviour Directory::GetBehaviour() const noexcept
    {
        return directory_behaviour_;
    }


    inline bool Directory::IsWithRank() const noexcept
    {
        return with_rank_;
    }


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_DIRECTORY_DOT_HXX_
// *** MoReFEM end header guards *** < //
