// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <filesystem>
#include <source_location>
#include <sstream>
#include <string>

#include "Utilities/Filesystem/Exceptions/Directory.hpp"

#include "Utilities/Filesystem/Behaviour.hpp"


namespace // anonymous
{


    std::string AlreadyExistsButNotAsDirectoryMsg(const std::filesystem::directory_entry& directory_entry);

    std::string UnexistingMsg(const std::filesystem::directory_entry& directory_entry);

    std::string ShouldntExistMsg(const std::filesystem::directory_entry& directory_entry);

    std::string SetBehaviourNoRankMsg(MoReFEM::FilesystemNS::behaviour new_behaviour);

    std::string EmptyPathMsg();

} // namespace


namespace MoReFEM::FilesystemNS::DirectoryNS::ExceptionNS
{

    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : ::MoReFEM::Exception(msg, location)
    { }


    AlreadyExistsButNotAsDirectory::~AlreadyExistsButNotAsDirectory() = default;

    AlreadyExistsButNotAsDirectory::AlreadyExistsButNotAsDirectory(
        const std::filesystem::directory_entry& directory_entry,
        const std::source_location location)
    : Exception(AlreadyExistsButNotAsDirectoryMsg(directory_entry), location)
    { }


    Unexisting::~Unexisting() = default;

    Unexisting::Unexisting(const std::filesystem::directory_entry& directory_entry, const std::source_location location)
    : Exception(UnexistingMsg(directory_entry), location)
    { }


    ShouldntExist::~ShouldntExist() = default;

    ShouldntExist::ShouldntExist(const std::filesystem::directory_entry& directory_entry,
                                 const std::source_location location)
    : Exception(ShouldntExistMsg(directory_entry), location)
    { }


    SetBehaviourNoRank::~SetBehaviourNoRank() = default;

    SetBehaviourNoRank::SetBehaviourNoRank(FilesystemNS::behaviour new_behaviour, const std::source_location location)
    : Exception(SetBehaviourNoRankMsg(new_behaviour), location)
    { }

    EmptyPath::~EmptyPath() = default;

    EmptyPath::EmptyPath(const std::source_location location) : Exception(EmptyPathMsg(), location)
    { }


} // namespace MoReFEM::FilesystemNS::DirectoryNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string AlreadyExistsButNotAsDirectoryMsg(const std::filesystem::directory_entry& directory_entry)
    {
        std::ostringstream oconv;

        oconv << "Can't create directory " << directory_entry.path()
              << ": it already exists "
                 "but is not a directory.";

        std::string ret = oconv.str();
        return ret;
    }


    std::string UnexistingMsg(const std::filesystem::directory_entry& directory_entry)
    {
        std::ostringstream oconv;
        oconv << "Directory '" << directory_entry << "' was expected to exist and could not be found.";

        std::string ret = oconv.str();
        return ret;
    }


    std::string ShouldntExistMsg(const std::filesystem::directory_entry& directory_entry)
    {
        std::ostringstream oconv;
        oconv << "Directory " << directory_entry
              << " already exists whereas it was expected not to (maybe the "
                 "behaviour set at call site was not the proper one).";

        std::string ret = oconv.str();
        return ret;
    }


    std::string SetBehaviourNoRankMsg(MoReFEM::FilesystemNS::behaviour new_behaviour)
    {
        std::ostringstream oconv;
        oconv << "Error in Directory: the possibility to deal with path without rank "
                 "is limited to very few possible 'behaviour' (please read the documentation "
                 "of the class to understand why). Here you tried to deal with a directory without "
                 "rank with the '"
              << new_behaviour << "' behaviour, which is not allowed.";

        std::string ret = oconv.str();
        return ret;
    }


    std::string EmptyPathMsg()
    {
        return "Path provided to `Directory` object was empty!";
    }


} // namespace
