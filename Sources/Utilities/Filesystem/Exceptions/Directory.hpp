// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_EXCEPTIONS_DIRECTORY_DOT_HPP_
#define MOREFEM_UTILITIES_FILESYSTEM_EXCEPTIONS_DIRECTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <filesystem>
#include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { enum class behaviour; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::FilesystemNS::DirectoryNS::ExceptionNS
{


    //! Generic class
    class Exception : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;

        //! Destructor
        virtual ~Exception() override;
    };

    /*!
     * \brief Thrown when an object was found on the filesystem but is not a directory.
     */
    class AlreadyExistsButNotAsDirectory final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] directory_entry Path on the filesystem
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit AlreadyExistsButNotAsDirectory(const std::filesystem::directory_entry& directory_entry,
                                                const std::source_location location);

        //! \copydoc doxygen_hide_copy_constructor
        AlreadyExistsButNotAsDirectory(const AlreadyExistsButNotAsDirectory& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        AlreadyExistsButNotAsDirectory(AlreadyExistsButNotAsDirectory&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        AlreadyExistsButNotAsDirectory& operator=(const AlreadyExistsButNotAsDirectory& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        AlreadyExistsButNotAsDirectory& operator=(AlreadyExistsButNotAsDirectory&& rhs) = default;


        //! Destructor
        virtual ~AlreadyExistsButNotAsDirectory() override;
    };


    /*!
     * \brief Thrown when a directory that is assumed to exist on the filesystem couln't be found.
     */
    class Unexisting final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] directory_entry Path on the filesystem
         * \copydoc doxygen_hide_source_location
         */
        explicit Unexisting(const std::filesystem::directory_entry& directory_entry,
                            const std::source_location location);

        //! \copydoc doxygen_hide_copy_constructor
        Unexisting(const Unexisting& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Unexisting(Unexisting&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Unexisting& operator=(const Unexisting& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Unexisting& operator=(Unexisting&& rhs) = default;


        //! Destructor
        virtual ~Unexisting() override;
    };


    /*!
     * \brief Thrown when a directoryalready exists on the filesystem whereas it was expected not to.
     */
    class ShouldntExist final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] directory_entry Path on the filesystem
         * \copydoc doxygen_hide_source_location
         */
        explicit ShouldntExist(const std::filesystem::directory_entry& directory_entry,
                               const std::source_location location);

        //! \copydoc doxygen_hide_copy_constructor
        ShouldntExist(const ShouldntExist& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        ShouldntExist(ShouldntExist&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ShouldntExist& operator=(const ShouldntExist& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        ShouldntExist& operator=(ShouldntExist&& rhs) = default;


        //! Destructor
        virtual ~ShouldntExist() override;
    };


    /*!
     * \brief Thrown when SetBehaviour is called for a behaviour not allowed with no ranks.
     */
    class SetBehaviourNoRank final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] behaviour Behaviour the programmer wanted to set.
         * \copydoc doxygen_hide_source_location
         */
        explicit SetBehaviourNoRank(FilesystemNS::behaviour behaviour, const std::source_location location);

        //! \copydoc doxygen_hide_copy_constructor
        SetBehaviourNoRank(const SetBehaviourNoRank& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        SetBehaviourNoRank(SetBehaviourNoRank&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        SetBehaviourNoRank& operator=(const SetBehaviourNoRank& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        SetBehaviourNoRank& operator=(SetBehaviourNoRank&& rhs) = default;


        //! Destructor
        virtual ~SetBehaviourNoRank() override;
    };


    /*!
     * \brief Thrown when path provided to `Directory` is empty.
     *
     * This exception is mostly intended for developers of the models, but when needed the `assert`
     * tended not to provide the required information to understand quickly what was amiss.
     */
    class EmptyPath final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit EmptyPath(const std::source_location location);

        //! \copydoc doxygen_hide_copy_constructor
        EmptyPath(const EmptyPath& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        EmptyPath(EmptyPath&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        EmptyPath& operator=(const EmptyPath& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        EmptyPath& operator=(EmptyPath&& rhs) = default;

        //! Destructor
        virtual ~EmptyPath() override;
    };


} // namespace MoReFEM::FilesystemNS::DirectoryNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_EXCEPTIONS_DIRECTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
