// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// IWYU pragma: no_include <iosfwd>
// IWYU pragma: no_include <arm/_types.h>

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <ios>
#include <ostream>
#include <source_location>
#include <string>
#include <string_view>
#include <vector>

#include "Utilities/Filesystem/File.hpp"

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/AsciiOrBinary/Enum.hpp"
#include "Utilities/Environment/Environment.hpp"


namespace MoReFEM::FilesystemNS
{


    namespace // anonymous
    {

        //! Returns the proper mask for interacting with the file
        //! \tparam InOrOutT Either std::ios::in or std::ios::out
        //! \param[in] format If binary, add binary to the mask
        //! \return The mask.
        template<class InOrOutT>
        std::ios::openmode Openmode(InOrOutT in_or_out, binary_or_ascii format);


    } // namespace


    File::File(std::filesystem::path&& path)
    {
        decltype(auto) env = Utilities::Environment::GetInstance();
        path = env.SubstituteValuesInPath(path);
        path = std::filesystem::weakly_canonical(path.make_preferred());

        directory_entry_ = std::filesystem::directory_entry{ path };
    }


    std::ofstream File::Append(binary_or_ascii format, const std::source_location location) const
    {
        decltype(auto) dir_entry = GetDirectoryEntry();

        auto openmode = Openmode(std::ios::out, format);

        std::ofstream ret(dir_entry.path(), openmode | std::ios::app);

        if (!ret)
            throw Exception("Unable to read/modify file \"" + dir_entry.path().native() + "\".", location);

        return ret;
    }


    void File::Remove(const std::source_location location) const
    {
        decltype(auto) filename = GetDirectoryEntry().path();

        const bool was_properly_deleted = std::filesystem::remove(filename);

        if (!was_properly_deleted)
            throw Exception("Unable to remove " + filename.native(), location);
    }


    std::ofstream File::NewContent(binary_or_ascii format, const std::source_location location) const
    {
        decltype(auto) filename = GetDirectoryEntry().path();

        auto openmode = Openmode(std::ios::out, format);

        std::ofstream ret(filename, openmode);

        if (!ret)
            throw Exception("Unable to create file " + filename.native(), location);

        return ret;
    }

    bool File::DoExist() const
    {
        decltype(auto) dir_entry = GetNonCstDirectoryEntry();
        dir_entry.refresh();

        return dir_entry.is_regular_file();
    }


    std::ostream& operator<<(std::ostream& stream, const File& rhs)
    {
        rhs.Print(stream);
        return stream;
    }


    void File::Print(std::ostream& stream) const
    {
        stream << GetDirectoryEntry().path().native();
    }


    std::ifstream File::Read(binary_or_ascii format, const std::source_location location) const
    {
        decltype(auto) filename = GetDirectoryEntry().path();

        auto openmode = Openmode(std::ios::in, format);
        std::ifstream ret(filename, openmode);

        if (!ret)
            throw Exception("Unable to read file " + filename.native(), location);

        return ret;
    }


    bool IsSameFile(const File& lhs, const File& rhs)
    {
        return (lhs.GetDirectoryEntry().path().lexically_normal() == rhs.GetDirectoryEntry().path().lexically_normal());
    }


    void Copy(const File& source,
              const File& target,
              fail_if_already_exist do_fail_if_already_exist,
              autocopy allow_autocopy,
              const std::source_location location)
    {
        if (IsSameFile(source, target))
        {
            switch (allow_autocopy)
            {
            case autocopy::yes:
                return;
            case autocopy::no:
                throw Exception("Trying to copy file \"" + static_cast<std::string>(source) + "\" onto itself!",
                                location);
            }
        }

        const auto copy_option = do_fail_if_already_exist == fail_if_already_exist::yes
                                     ? std::filesystem::copy_options::none
                                     : std::filesystem::copy_options::overwrite_existing;

        const std::filesystem::path& source_path{ source.GetDirectoryEntry().path() };
        const std::filesystem::path& target_path{ target.GetDirectoryEntry().path() };

        std::filesystem::copy_file(source_path, target_path, copy_option);
    }


    void ConcatenateAsciiFiles(const std::vector<File>& file_list,
                               const File& amalgamated_file,
                               const std::source_location location)
    {
        auto it = std::ranges::find_if(file_list,

                                       [&amalgamated_file](const File& current_file)
                                       {
                                           return IsSameFile(current_file, amalgamated_file);
                                       });

        if (it != file_list.cend())
            throw Exception("Error in ConcatenateAsciiFiles: the target file can't belong to the input list.",
                            location);

        // Left intentionally here after the first exception: this one would also be triggered but the
        // message to prioritize is the one associated with exception above.
        if (amalgamated_file.DoExist())
            throw Exception("Error in ConcatenateAsciiFiles: the target file already exists on the filesystem!",
                            location);

        std::ofstream out{ amalgamated_file.NewContent(binary_or_ascii::ascii, location) };

        std::string line;

        for (const auto& input_file : file_list)
        {
            std::ifstream in{ input_file.Read(binary_or_ascii::ascii, location) };

            while (getline(in, line))
                out << line << '\n';

            in.close();
        }
    }


    bool AreEquals(const File& lhs, const File& rhs, const std::source_location location)
    {
        if (IsSameFile(lhs, rhs)) // if exact same file content is the same!
            return true;

        // Load the stream, and check both exists.
        std::ifstream lhs_stream{ lhs.Read(binary_or_ascii::ascii, location) };
        std::ifstream rhs_stream{ rhs.Read(binary_or_ascii::ascii, location) };

        std::ifstream::pos_type size1;
        std::ifstream::pos_type size2;

        size1 = lhs_stream.seekg(0, std::ifstream::end).tellg();
        lhs_stream.seekg(0, std::ifstream::beg);

        size2 = rhs_stream.seekg(0, std::ifstream::end).tellg();
        rhs_stream.seekg(0, std::ifstream::beg);

        if (size1 != size2)
            return false;

        constexpr auto block_size = 4096UL;
        auto remaining = static_cast<std::streamsize>(size1);

        while (remaining != 0)
        {
            std::array<char, block_size> buffer1{};
            std::array<char, block_size> buffer2{};

            const auto size = std::min(static_cast<std::streamsize>(block_size), remaining);

            lhs_stream.read(buffer1.data(), size);
            rhs_stream.read(buffer2.data(), size);

            if (0 != memcmp(buffer1.data(), buffer2.data(), static_cast<std::size_t>(size)))
                return false;

            remaining -= size;
        }

        return true;
    }


    File::operator std::string() const
    {
        return GetDirectoryEntry().path().native();
    }


    File::operator std::string_view() const
    {
        return GetDirectoryEntry().path().native();
    }


    namespace // anonymous
    {

        template<class InOrOutT>
        std::ios::openmode Openmode(InOrOutT in_or_out, binary_or_ascii format)
        {
            std::ios::openmode openmode{};

            switch (in_or_out)
            {
            case std::ios::in:
            case std::ios::out:
                openmode = in_or_out;
                break;
            default:
                assert(false);
                exit(EXIT_FAILURE);
                break;
            }

            switch (format)
            {
            case binary_or_ascii::binary:
                openmode |= std::ios::binary;
                break;
            case binary_or_ascii::ascii:
                break;
            case binary_or_ascii::from_input_data:
            {
                assert(false);
                exit(EXIT_FAILURE);
            }
            }

            return openmode;
        }


    } // namespace


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
