// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_FILE_DOT_HPP_
#define MOREFEM_UTILITIES_FILESYSTEM_FILE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <filesystem>
#include <fstream> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
#include <vector>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp" // IWYU pragma: export


namespace MoReFEM::FilesystemNS
{


    //! Convenient enum class for copy.
    enum class autocopy { yes, no };

    //! Convenient enum class for copy.
    enum class fail_if_already_exist { yes, no };


    /*!
     * \brief Class used in MoReFEM to represent files on the filesystem
     *
     * \copydoc doxygen_hide_filesystem_rant
     *
     * The core idea beneath this class is that basically this class encapsulates nicely the path to the file you
     * want to manipulate (whether it already exists or not...) and provide friendly methods such as \a Create to
     * manipulate this file.
     *
     * \attention I tried to propose a slightly better API than the one proposed in \a std::filesystem, but it doesn't magically solve some issues
     * you may encounter if you're not careful enough. So for instance don't open several streams on the same \a
     * FIle object: the library won't prevent you to do so but that doesn't make it a good idea...
     */
    class File
    {
      public:
        //! Friendship.
        friend bool IsSameFile(const File& lhs, const File& rhs);

        //! Friendship.
        friend void Copy(const File& source,
                         const File& target,
                         fail_if_already_exist do_fail_if_already_exist,
                         autocopy allow_autocopy,
                         const std::source_location location);
        //! Friendship.
        friend bool AreEquals(const File& lhs, const File& rhs, const std::source_location location);

      public:
        //! Default constructor.
        explicit File() = default;

        /*!
         * \brief Constructor.
         *
         * \param[in] path Path on the filesystem.
         */
        explicit File(std::filesystem::path&& path);

        //! Destructor.
        ~File() = default;

        //! \copydoc doxygen_hide_copy_constructor
        File(const File& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        File(File&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        File& operator=(const File& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        File& operator=(File&& rhs) = default;

      public:
        /*!
         * \brief A wrapper over ifstream::open with std::ios::append that throws an exception in case the operation
         * fails.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         *
         * \copydoc doxygen_hide_file_format
         */
        std::ofstream Append(binary_or_ascii format = binary_or_ascii::ascii,
                             const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Remove the file on the filesystem.
         *
         * \copydoc doxygen_hide_source_location
         */
        void Remove(const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief A wrapper over ofstream::open that throws an exception in case the operation fails.
         *
         * \copydoc doxygen_hide_file_format
         *
         * \copydoc doxygen_hide_source_location
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         *
         * Reminder: the file is effectively created when the stream is closed (through \a close() or destruction of
         * the \a  File object).
         */
        std::ofstream NewContent(binary_or_ascii format = binary_or_ascii::ascii,
                                 const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief A wrapper over ifstream::open that throws an exception in case the operation fails.
         *
         * \copydoc doxygen_hide_source_location
         * \copydoc doxygen_hide_file_format
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        std::ifstream Read(binary_or_ascii format = binary_or_ascii::ascii,
                           const std::source_location location = std::source_location::current()) const;

        /*!
         * \brief Check whether the encapsulated \a directory_entry exist on the filesystem and is properly a file.
         *
         * \return True if the directory_entry exists and is a regular file.
         */
        bool DoExist() const;

        /*!
         * \brief Helper method used to define operator<< overload.
         *
         * \param[in,out] stream Stream onto which path is written.
         */
        void Print(std::ostream& stream) const;

        //! Conversion operator toward \a std::string.
        explicit operator std::string() const;

        //! Conversion operator toward \a std::string_view.
        explicit operator std::string_view() const;

        /*!
         * \brief Extract the extension from a filename.
         *
         * \return View to the extension. By convention the '.' is conserved (as it is what is done in \a std::filesystem::path::extension()).
         *
         * \copydoc doxygen_hide_filesystem_thread_safe
         */
        std::string Extension() const;

        //! Constant accessor to the underlying \a std::filesystem::directory_entry object.
        const std::filesystem::directory_entry& GetDirectoryEntry() const noexcept;

      private:
        /*!
         * \brief Non constant accessor to the underlying \a std::filesystem::directory_entry object.
         *
         * This method is declared \a const as it is used uder the hood in methods that are morally constant (the
         * data attribute pointed is mutable - see the class description to understand why).
         */
        std::filesystem::directory_entry& GetNonCstDirectoryEntry() const noexcept;

      private:
        /*!
         * \brief The underlying \a std::filesystem::directory_entry object.
         *
         * See the class description to understand why it is declared mutable.
         */
        mutable std::filesystem::directory_entry directory_entry_;
    };


    /*!
     * \brief Check whether two \a File refers to the same file on the filesystem.
     *
     * This is <b>not</b> a check upon the content: you are looking for \a AreEqual if you want to check that.
     *
     * \param[in] lhs First \a File object
     * \param[in] rhs Second \a File object
     *
     * \return True of both actually shares the same path. If one is a symbolic link toward the other, false will be written: we really check only the
     * lexicographic value of the path.
     *
     * \internal std::filesystem::equivalent() is not used as one of its prerequisite is that both files exist on the filesystem.
     */
    bool IsSameFile(const File& lhs, const File& rhs);


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     *
     * The underlying path of \a File class is written.
     */
    std::ostream& operator<<(std::ostream& stream, const File& rhs);


    /*!
     * \brief Copy a file.
     *
     * \param[in] source Path of the source file.
     * \param[in] target Path of the target file.
     * \param[in] do_fail_if_already_exist If yes, the target must not exist yet.
     * \param[in] allow_autocopy If no, an exception is thrown is source is the same path as target. If yes,
     * nothing is done. The reason for this is that I'm not sure underlying library Yuni can cope with this
     * case. It will be replaced by future STL filesystem library that should be shipped along with C++17.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \copydoc doxygen_hide_filesystem_thread_safe
     */
    void Copy(const File& source,
              const File& target,
              fail_if_already_exist do_fail_if_already_exist,
              autocopy allow_autocopy,
              const std::source_location location = std::source_location::current());

    /*!
     * \brief Concatenate several files assumed to be in ascii format.
     *
     *
     * \param[in] file_list List of files to concatenate.
     * \param[out] amalgamated_file File into which the amalgated files are written.
     *
     * \copydoc doxygen_hide_source_location
     *
     * All files must be valid (or in a valid path for output one); if not an exception is raised.
     *
     * \copydoc doxygen_hide_filesystem_thread_safe
     */
    void ConcatenateAsciiFiles(const std::vector<File>& file_list,
                               const File& amalgamated_file,
                               const std::source_location location = std::source_location::current());

    /*! \class doxygen_hide_file_format
     *
     * \param[in] format Whether the file must be treated as ascii or binary. The third choice of the enum is not accepted here.
     */


    /*! \class doxygen_hide_filesystem_thread_safe
     *
     * \attention This function is syntactic sugar for file or directory manipulation and has no pretension to
     * handle by itself thread safety.
     */


    /*!
     * \brief Check two files share exactly the same content.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \copydoc doxygen_hide_source_location
     *
     * This function checks both files do exist first.
     *
     * This code is lifted from https://stackoverflow.com/questions/6163611/compare-two-files with
     * very small adaptation; http://www.cplusplus.com/forum/general/94032/ provides a Boost-dependent
     * implementation which is seemingly more efficient.
     *
     * Upcoming STL filesystem library doesn't seem to provide the functionality; currently it is used only
     * for a test with very small files involved. So if this is deemed to be used more often and on much bigger
     * file some profiling would be interesting here!
     *
     * \return True if the files share exactly the same content.
     *
     * \copydoc doxygen_hide_filesystem_thread_safe
     */
    bool
    AreEquals(const File& lhs, const File& rhs, const std::source_location location = std::source_location::current());


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Filesystem/File.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_FILE_DOT_HPP_
// *** MoReFEM end header guards *** < //
