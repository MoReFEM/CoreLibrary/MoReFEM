// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_FILE_DOT_HXX_
#define MOREFEM_UTILITIES_FILESYSTEM_FILE_DOT_HXX_
// IWYU pragma: private, include "Utilities/Filesystem/File.hpp"
// *** MoReFEM header guards *** < //


#include <filesystem>
#include <string>


namespace MoReFEM::FilesystemNS
{


    inline const std::filesystem::directory_entry& File::GetDirectoryEntry() const noexcept
    {
        // assert(!directory_entry_.path().empty());
        // < This assert has been removed as it is not the right granularity: we get some cases
        // < in which we may want to use an empty file path. If this proves too dangerous,
        // < an exception could be considered, but an assert was anyway unwieldy (it was not
        // < obvious where the problem was - typically when a \a File object was converted
        // < into a std::string it could happen).
        return directory_entry_;
    }


    inline std::filesystem::directory_entry& File::GetNonCstDirectoryEntry() const noexcept
    {
        return const_cast<std::filesystem::directory_entry&>(GetDirectoryEntry());
    }


    inline std::string File::Extension() const
    {
        return GetDirectoryEntry().path().extension().native();
    }


} // namespace MoReFEM::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_FILE_DOT_HXX_
// *** MoReFEM end header guards *** < //
