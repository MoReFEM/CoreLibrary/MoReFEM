// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>

#include "Utilities/Filesystem/Internal/CheckForSubdirectoryConstructor.hpp"

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Filesystem/Directory.hpp"


namespace MoReFEM::Internal::FilesystemNS
{


    void CheckForSubdirectoryConstructor(const ::MoReFEM::FilesystemNS::Directory& parent_directory,
                                         const std::source_location location)
    {
        if (!parent_directory.DoExist())
        {
            std::ostringstream oconv;
            oconv << "Directory '" << parent_directory.GetPath()
                  << "' couldn't be found whereas we were trying to build a "
                     "subdirectory from it (so the directory has been created at some point and then removed).";
            throw Exception(oconv.str(), location);
        }
    }


} // namespace MoReFEM::Internal::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
