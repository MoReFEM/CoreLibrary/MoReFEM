// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_INTERNAL_CHECKFORSUBDIRECTORYCONSTRUCTOR_DOT_HPP_
#define MOREFEM_UTILITIES_FILESYSTEM_INTERNAL_CHECKFORSUBDIRECTORYCONSTRUCTOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FilesystemNS
{


    /*!
     * \brief Helper function for subdirectory constructors.
     *
     * \param[in] parent_directory The directory from which a subdirectory is to be constructed.
     * \copydoc doxygen_hide_source_location
     */
    void CheckForSubdirectoryConstructor(const ::MoReFEM::FilesystemNS::Directory& parent_directory,
                                         const std::source_location location = std::source_location::current());


} // namespace MoReFEM::Internal::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_INTERNAL_CHECKFORSUBDIRECTORYCONSTRUCTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
