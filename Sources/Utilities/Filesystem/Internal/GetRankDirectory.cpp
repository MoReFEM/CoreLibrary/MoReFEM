// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <optional>
#include <source_location>
#include <string>

// IWYU pragma: no_include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/Internal/GetRankDirectory.hpp"

#include "ThirdParty/Wrappers/Mpi/StrongType.hpp"


namespace MoReFEM::Internal::FilesystemNS
{


    namespace // anonymous
    {


        using Directory = ::MoReFEM::FilesystemNS::Directory;

        using behaviour = ::MoReFEM::FilesystemNS::behaviour;


    } // namespace


    Directory GetRankDirectory(const Directory& root_directory, rank_type rank, std::source_location location)
    {
        assert(root_directory.GetMpi().IsRootProcessor());
        assert(root_directory.IsWithRank());

        // I usually don't like the '..' trick but exceptionally it is the best way to do it without complexifying
        // further Directory API.
        const Directory one_step_above(root_directory, "..", behaviour::read, location);

        Directory ret(one_step_above, "Rank_" + std::to_string(rank.Get()));

        return ret;
    }


} // namespace MoReFEM::Internal::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
