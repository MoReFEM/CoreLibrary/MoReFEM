// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_FILESYSTEM_INTERNAL_GETRANKDIRECTORY_DOT_HPP_
#define MOREFEM_UTILITIES_FILESYSTEM_INTERNAL_GETRANKDIRECTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/Wrappers/Mpi/StrongType.hpp"


namespace MoReFEM::Internal::FilesystemNS
{


    /*!
     * \brief Create a \a Directory which contains path related to \a rank - where rank is NOT necessarily the one
     * held by mpi object.
     *
     * \attention Should only be used on mpi root processor!
     *
     * This is only for some specific functions and should be used with great care; behaviour is set to 'read'
     * and should be changed with even greater care...
     *
     * \param[in] root_directory The directory matching the root rank.
     * \param[in] rank Rank for which the new directory is required.
     * \copydoc doxygen_hide_source_location
     *
     * \return The directory related to the chosen \a rank argument. It is created with the 'read' behaviour - and
     * this one should changed with great care! (or better left unchanged).
     */
    ::MoReFEM::FilesystemNS::Directory
    GetRankDirectory(const ::MoReFEM::FilesystemNS::Directory& root_directory,
                     rank_type rank,
                     std::source_location location = std::source_location::current());


} // namespace MoReFEM::Internal::FilesystemNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_FILESYSTEM_INTERNAL_GETRANKDIRECTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
