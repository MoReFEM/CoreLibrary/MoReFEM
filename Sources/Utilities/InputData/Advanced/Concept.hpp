// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CONCEPT_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <optional>

#include "Utilities/Miscellaneous.hpp"


namespace MoReFEM::Advanced::Concept::InputDataNS
{


    /*!
     * \brief Defines a concept to identify a type is a section of an \a InputData object.
     *
     */
    template<typename T>
    concept SectionType = requires {
        { T::ConceptIsSection == true };
    };


    /*!
     * \brief Defines a concept to identify a type is an indexed section of an \a InputData object.
     *
     */
    template<typename T>
    concept IndexedSectionType = requires {
        requires SectionType<T>;
        T::is_indexed_section == true;
    };


    /*!
     * \brief Defines a concept to identify a type is a leaf of an \a InputData object.
     *
     */
    template<typename T>
    concept LeafType = requires {
        { T::ConceptIsLeaf == true };
    };


    /*!
     * \brief Defines a concept that encompasses both sections and lead.
     *
     */
    template<class T>
    concept SectionOrLeafType = SectionType<T> || LeafType<T>;


} // namespace MoReFEM::Advanced::Concept::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
