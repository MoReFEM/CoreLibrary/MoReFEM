// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_SECTION_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_SECTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>

#include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"
#include "Utilities/InputData/Advanced/NoEnclosingSection.hpp" // IWYU pragma: export
#include "Utilities/InputData/Enum.hpp"                        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"      // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"            // IWYU pragma: export


namespace MoReFEM::Advanced::InputDataNS::Crtp
{

    /*!
     * \brief Any section in the input data file list tuple should derive from this.
     *
     * \copydoc doxygen_hide_tparam_enclosing_section_type
     *
     */
    template<class DerivedT, class EnclosingSectionT = NoEnclosingSection>
    struct Section
    {

        //! Enclosing section (mirror the template parameter)
        using enclosing_section_type = EnclosingSectionT;

        //! Helper variable to define the \a MoReFEM::Advanced::Concept::InputDataNS concept.
        static inline constexpr bool ConceptIsSection = true;

        //! Specifies the nature is 'section'.
        static constexpr Nature GetNature() noexcept;


        /*!
         * \brief Full name of the section, including names of enclosing section(s) if relevant.
         *
         * Separator is a '.', so a section might be for instance 'Foo.Bar'.
         *
         * \return Full name of the section.
         */
        static const std::string& GetFullName();


        /*!
         * \brief Identifier is an alias to GetFullName()
         *
         * \internal <b><tt>[internal]</tt></b> Both are necessary as included struct get their own
         * 'Identifier()', whereas they do not define any GetName().
         * \endinternal
         *
         * \return Full name of the section.
         */
        static std::string GetIdentifier() noexcept;


        /*!
         * \brief Accessor to the tuple that lists the content of the section.
         *
         * \internal <b><tt>[internal]</tt></b> auto is used on purpose here: CRTP can't read a type from
         * DerivedT.
         * \endinternal
         *
         * \return Tuple that lists the content of the section.
         */
        const auto& GetSectionContent() const noexcept;

        /*!
         * \brief Non constant accessor to the tuple that lists the content of the section.
         *
         * \internal <b><tt>[internal]</tt></b> auto is used on purpose here: CRTP can't read a type from
         * DerivedT. \endinternal
         *
         * \return Tuple that lists the content of the section.
         */
        auto& GetNonCstSectionContent() noexcept;
    };


    /*!
     * \brief CRTP for special section that might be present several times for a given \a Model.
     *
     * Some sections may be present several times for a given \a Model: we may have to deal with several
     * \a Mesh, \a Unknown or \a FEltSpace for instance.
     *
     * In this case, we are using an index to tag each specific instance: we may have for instance a \a Mesh10
     * and a \a Mesh20 objects (indexes may be whatever the author of the model wants, provided same index
     * is not used twice).
     *
     * Such indexed section are in this case derived from current template CRTP class, which specifically defines
     * a specific `IndexedSectionDescription` struct which is used internally to fetch the required leaves values from
     * both \a InputData and \a ModelSettings object to properly build the object.
     *
     * An instance of this `IndexedSectionDescription` class should be put in the \a ModelSettings tuple.
     *
     * \internal This may seem a bit convoluted, but part of the reason is that the separation between \a InputData
     * (i.e. data modifiable by the end-user through the model Lua file) and \a ModelSettings (data not modifiable by
     * the end user) was introduced lately. Rewriting from scratch would have taken even more time that current version,
     * which is a compromise to provide a neat if convoluted way to separate properly both types of data without being
     * too cryptic for the end user.
     *
     * \tparam DerivedT Class upon which the CRTP is applied. This is by construct here a template class which at least
     * one argument: \a IndexT. For instance `Mesh<10>`
     * \tparam IndexT Unique id to tag the specific instance being under constructor. It should be noted that in models
     * I typically use more expressive enum classes, to more often than not \a IndexT will be provided with code such as
     * `EnumUnderlyingType(MeshIndex::solid)` rather than shorter but less expressive number.
     * \tparam TagT A tag used to underline to which family of section the section under construction belongs to. For
     * instance a mesh will provide for this parameter `Internal::InputDataNS::MeshNS::Tag` here, which will help the
     * `MeshManager` to figure out it should add a `Mesh` from the data provided in \a InputData and/or \a
     * ModelSettings. If your section is not handled by a Manager of some kind, you may provide `std::false_type`. If it
     * is, the struct submitted here is typically empty - what matters is that the Manager class defines a traits
     * `indexed_section_tag` which points to the same class as the one provided here.
     * \copydoc doxygen_hide_tparam_enclosing_section_type
     */
    template<class DerivedT, std::size_t IndexT, class TagT, class EnclosingSectionT = NoEnclosingSection>
    struct IndexedSection : public Section<DerivedT, EnclosingSectionT>
    {
        /*!
         * brief Inner variable used to identify \a Indexed Sections when sifting through all input data and model
        //! settings.
         *
         * It is used to define `Advanced::Concept::InputDataNS::IndexedSectionType` and to
         * perform specific operations upon these sections in the `Internal::InputDataNS::TupleIteration`
         * facility.
         */
        static inline constexpr bool is_indexed_section = true;

        //! Return the unique id (i.e. 'IndexT').
        static inline constexpr std::size_t GetUniqueId() noexcept;

        //! Returns a unique name for current section, which is computed by appending \a IndexT to a base name
        //! that must be defined in \a DerivedT.
        static std::string GetName();

        /*!
         * \brief A class which is used internally to collect properly all the leaves requested to build the
         * \a DerivedT object.
         *
         * IndexedSectionDescriptions have two specific roles:
         * - The presence of the `IndexedSectionDescription` object in the model-specific `ModelSettings` tuple tells
         * data concerning the enclosing \a IndexedSection are expected. In full-fledged models (but not necessarily in
         * tests...) there is a check that will warn explicitly the author of a model if he/she forgot to provide this
         * specific leaf.
         * - Holding the description of the whereabout of the specific \a IndexedSection in the Lua file (if some of the
         * leaves are modifiable). If a model for instances defines two meshes, the author of the model must provide in
         * the overload `ModelSettings::Init()` a description of what entails each of the mesh. The description
         * will be reported when the Lua file is generated or refreshed (through UpdateLuaFiles executable) so that
         * end-user will benefit from a clear description of each, rather than having to open another file to figure out
         * if ' Mesh1' is meant to represent the solid or the fluid mesh of its model.
         */
        struct IndexedSectionDescription : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                               IndexedSectionDescription<IndexedSectionDescription, DerivedT, TagT>
        { };
    };


} // namespace MoReFEM::Advanced::InputDataNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/Advanced/Crtp/Section.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_SECTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
