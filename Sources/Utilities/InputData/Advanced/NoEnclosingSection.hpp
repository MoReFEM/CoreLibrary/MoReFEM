// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_ADVANCED_NOENCLOSINGSECTION_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_ADVANCED_NOENCLOSINGSECTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <string> // IWYU pragma: keep


namespace MoReFEM::Advanced::InputDataNS
{


    //! Enum to tell whether current item of input data class is a section or a leaf.
    enum class Nature { section = 0, leaf, undefined };


    /*!
     * \brief Placeholder class to use as one of the template parameter for sections and end parameters at root level.
     *
     * Should be used as Crtp::Section or Crtp::Leaf template argument.
     */
    struct NoEnclosingSection
    { };


} // namespace MoReFEM::Advanced::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_ADVANCED_NOENCLOSINGSECTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
