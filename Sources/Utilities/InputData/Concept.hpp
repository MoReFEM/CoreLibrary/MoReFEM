// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_CONCEPT_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_CONCEPT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <optional>

#include "Utilities/InputData/Advanced/Concept.hpp" // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify a type is an \a InputData class instance.
     *
     * \internal \a Utilities::IsSpecializationOf can't be used here as there is a non-type
     * template argument in \a InputData template class.
     */
    template<typename T>
    concept InputDataType = requires {
        { T::ConceptIsInputData };
    };

    //! Same as \a InputDataType except that std::nullopt is also an option.
    template<class T>
    concept InputDataTypeOrNullopt = InputDataType<T> || std::is_same_v<std::nullopt_t, T>;


    /*!
     * \brief Defines a concept to identify a type is an \a ModelSettings class instance.
     *
     * \internal \a Utilities::IsSpecializationOf can't be used here as there is a non-type
     * template argument in \a ModelSettings template class.
     */
    template<typename T>
    concept ModelSettingsType = requires {
        { T::ConceptIsModelSettings };
    };


    /*!
     * \brief Self explaining!
     *
     * Used for facilities we would like to work with both \a InputData and \a ModelSettings.
     */
    template<typename T>
    concept InputDataOrModelSettingsType = InputDataType<T> || ModelSettingsType<T>;


} // namespace MoReFEM::Concept


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_CONCEPT_DOT_HPP_
// *** MoReFEM end header guards *** < //
