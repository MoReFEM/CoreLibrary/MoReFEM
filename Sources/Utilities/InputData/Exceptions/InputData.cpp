// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <list>
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include "Utilities/InputData/Exceptions/InputData.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hpp"
#include "Utilities/String/String.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string DuplicateInInputFileMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                        const std::string& section,
                                        const std::vector<std::string>& variable_list);


    std::string UnboundInputDataMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                    std::string_view section,
                                    std::string_view variable);


    std::string DuplicateInTupleMsg(const std::string& key);


    std::string MpiNotInitializedMsg();


    std::string FolderDoesntExistMsg(const std::string& folder);


    std::string NoEntryInModelSettingsMsg(const std::string& entry_identifier);


    std::string ValueCantBeSetTwiceMsg(const std::string& entry_identifier);


    std::string ModelSettingsNotCompletelyFilledMsg(const std::vector<std::string>& missing_entries);


    std::string MissingIndexedSectionDescriptionInModelSettingsTupleMsg(std::string_view section_name);

} // namespace


namespace MoReFEM::InputDataNS::ExceptionNS
{

    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : ::MoReFEM::Exception(msg, location)
    { }


    DuplicateInInputFile::~DuplicateInInputFile() = default;


    DuplicateInInputFile::DuplicateInInputFile(const ::MoReFEM::FilesystemNS::File& filename,
                                               const std::string& section,
                                               const std::vector<std::string>& variable_list,
                                               const std::source_location location)
    : Exception(DuplicateInInputFileMsg(filename, section, variable_list), location)
    { }


    UnboundInputData::~UnboundInputData() = default;


    UnboundInputData::UnboundInputData(const ::MoReFEM::FilesystemNS::File& filename,
                                       std::string_view section,
                                       std::string_view variable,
                                       const std::source_location location)
    : Exception(UnboundInputDataMsg(filename, section, variable), location)
    { }


    DuplicateInTuple::~DuplicateInTuple() = default;


    DuplicateInTuple::DuplicateInTuple(const std::string& key, const std::source_location location)
    : Exception(DuplicateInTupleMsg(key), location)
    { }


    MpiNotInitialized::~MpiNotInitialized() = default;


    MpiNotInitialized::MpiNotInitialized(const std::source_location location)
    : Exception(MpiNotInitializedMsg(), location)
    { }


    FolderDoesntExist::~FolderDoesntExist() = default;


    FolderDoesntExist::FolderDoesntExist(const std::string& folder, const std::source_location location)
    : Exception(FolderDoesntExistMsg(folder), location)
    { }


    NoEntryInModelSettings::~NoEntryInModelSettings() = default;


    NoEntryInModelSettings::NoEntryInModelSettings(const std::string& entry_identifier,
                                                   const std::source_location location)
    : Exception(NoEntryInModelSettingsMsg(entry_identifier), location)
    { }


    ValueCantBeSetTwice::~ValueCantBeSetTwice() = default;


    ValueCantBeSetTwice::ValueCantBeSetTwice(const std::string& entry_identifier, const std::source_location location)
    : Exception(ValueCantBeSetTwiceMsg(entry_identifier), location)
    { }


    ModelSettingsNotCompletelyFilled::~ModelSettingsNotCompletelyFilled() = default;


    ModelSettingsNotCompletelyFilled::ModelSettingsNotCompletelyFilled(const std::vector<std::string>& missing_entries,
                                                                       const std::source_location location)
    : Exception(ModelSettingsNotCompletelyFilledMsg(missing_entries), location)
    { }


    MissingIndexedSectionDescriptionInModelSettingsTuple::~MissingIndexedSectionDescriptionInModelSettingsTuple() =
        default;

    MissingIndexedSectionDescriptionInModelSettingsTuple::MissingIndexedSectionDescriptionInModelSettingsTuple(
        std::string_view section_name,
        const std::source_location location)
    : Exception(MissingIndexedSectionDescriptionInModelSettingsTupleMsg(section_name), location)
    { }


} // namespace MoReFEM::InputDataNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file

    std::string DuplicateInInputFileMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                        const std::string& section,
                                        const std::vector<std::string>& variable_list)
    {
        std::list<std::string> buf(variable_list.cbegin(), variable_list.cend());
        buf.sort();
        buf.unique();

        auto it_duplicate = buf.cbegin();

        // Find the duplicate.
        {
            for (auto end = buf.cend(); it_duplicate != end; ++it_duplicate)
            {
                if (std::count(variable_list.cbegin(), variable_list.cend(), *it_duplicate) >= 2)
                    break;
            }
            assert(it_duplicate != buf.cend());
        }


        std::ostringstream oconv;
        oconv << "Error in input file " << filename
              << ": "
                 "at least one duplicate found ('"
              << *it_duplicate << "')";

        if (!section.empty())
            oconv << " in section '" << section << '\'';
        else
            oconv << "outside of any section";

        oconv << '.';

        return oconv.str();
    }


    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    std::string UnboundInputDataMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                    std::string_view section,
                                    std::string_view variable)
    // NOLINTEND(bugprone-easily-swappable-parameters)
    {
        std::ostringstream oconv;
        oconv << "In the input file ' " << filename << "' there was a variable '" << variable << '\'';

        if (section.empty())
            oconv << " outside any section";
        else
            oconv << " in the section \'" << section << '\'';

        oconv << "; no tuple element claims this one. It means this input datum is "
                 "completely useless; please remove it from the input file (or check your "
                 "input data tuple is up-to-date).";

        return oconv.str();
    }


    std::string DuplicateInTupleMsg(const std::string& key)
    {
        std::ostringstream oconv;
        oconv << "The key " << key
              << " has been found at least twice in the input data list; "
                 "the tuple that built it is therefore ill-formed.\n";
        oconv << "It might be either the same entry is present twice in the tuple, or two entries share "
                 "the same couple Section() / NameInFile()";

        return oconv.str();
    }


    std::string MpiNotInitializedMsg()
    {
        return "MPI was not initialized properly!";
    }


    std::string FolderDoesntExistMsg(const std::string& folder)
    {
        return "Folder " + folder + " doesn't exist whereas is was expected to.";
    }


    std::string NoEntryInModelSettingsMsg(const std::string& entry_identifier)
    {
        std::ostringstream oconv;

        // Check if we're talking about a IndexedSectionDescription - if so adapt the message accordingly.
        const auto section_description_suffix = MoReFEM::Internal::InputDataNS::IndexedSectionDescriptionSuffix();

        if (MoReFEM::Utilities::String::EndsWith(entry_identifier, section_description_suffix))
        {
            const auto pos = entry_identifier.rfind('.');
            assert(pos != std::string::npos);
            auto section_name = entry_identifier.substr(0UL, pos);

            oconv << "ModelSettings::SetDescription() was called for section " << section_name
                  << ", but "
                     "the related field wasn't found in the model settings tuple.";
        } else
        {
            oconv << "In ModelSettings::Add() method, provided leaf which key is " << entry_identifier
                  << " wasn't found in the underlying tuple.";
        }

        return oconv.str();
    }


    std::string ValueCantBeSetTwiceMsg(const std::string& entry_identifier)
    {
        std::ostringstream oconv;

        // Check if we're talking about a IndexedSectionDescription - if so adapt the message accordingly.
        const auto section_description_suffix = MoReFEM::Internal::InputDataNS::IndexedSectionDescriptionSuffix();

        if (MoReFEM::Utilities::String::EndsWith(entry_identifier, section_description_suffix))
        {
            const auto pos = entry_identifier.rfind('.');
            assert(pos != std::string::npos);
            auto section_name = entry_identifier.substr(0UL, pos);

            oconv << "ModelSettings::SetDescription() can't be called twice for the same section (namely "
                  << section_name << ").";
        } else
        {
            oconv << "ModelSettings::Add() can't be called twice over the same entry (namely " << entry_identifier
                  << ").";
        }

        return oconv.str();
    }

    std::string ModelSettingsNotCompletelyFilledMsg(const std::vector<std::string>& missing_entries)
    {
        std::ostringstream oconv;
        oconv << "Some entries in ModelSettings tuple were not properly filled." << '\n';


        std::vector<std::string> indexed_sections_without_description_list;
        std::vector<std::string> others_missing_entries_list;

        indexed_sections_without_description_list.reserve(missing_entries.size());
        others_missing_entries_list.reserve(missing_entries.size());

        const auto section_description_suffix = MoReFEM::Internal::InputDataNS::IndexedSectionDescriptionSuffix();

        for (const auto& entry : missing_entries)
        {
            if (MoReFEM::Utilities::String::EndsWith(entry, section_description_suffix))
            {
                const auto pos = entry.rfind('.');
                assert(pos != std::string::npos);

                auto section_name = entry.substr(0UL, pos);

                indexed_sections_without_description_list.push_back(section_name);
            } else
                others_missing_entries_list.push_back(entry);
        }

        if (!others_missing_entries_list.empty())
        {
            oconv
                << "Please add the values (through the `Add()` template method) to the leaves which identifiers are: ";

            MoReFEM::Utilities::PrintContainer<>::Do(others_missing_entries_list,
                                                     oconv,
                                                     MoReFEM::PrintNS::Delimiter::separator("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::opener("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::closer("\n"));
        }

        if (!indexed_sections_without_description_list.empty())
        {
            oconv << "Please add the description (through the `SetDescription()` template method) for the following "
                     "indexed sections: ";

            MoReFEM::Utilities::PrintContainer<>::Do(indexed_sections_without_description_list,
                                                     oconv,
                                                     MoReFEM::PrintNS::Delimiter::separator("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::opener("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::closer("\n"));
        }

        return oconv.str();
    }


    std::string MissingIndexedSectionDescriptionInModelSettingsTupleMsg(std::string_view section_name)
    {
        std::ostringstream oconv;

        oconv << "The model is flawed: there should be a 'IndexedSectionDescription' leaf in the 'ModelSettings' tuple "
                 "for section "
              << std::string(section_name)
              << ". Please ask the author of the model to fix it; the error is not caused "
                 "by data you provided in your Lua file.";

        return oconv.str();
    }


} // namespace
