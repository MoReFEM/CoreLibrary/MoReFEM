// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_EXCEPTIONS_INPUTDATA_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_EXCEPTIONS_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>
#include <string>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InputDataNS::ExceptionNS
{


    //! Generic class
    class Exception : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;

        //! Destructor
        virtual ~Exception() override;
    };


    /*!
     * \brief Thrown when an input datum appears twice in the input data file.
     */
    class DuplicateInInputFile final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] filename Name of the input file from which input data were read.
         * \param[in] section Section in the input file.
         * \param[in] variable_list All variables defined in the section of this input file.
         * When exception is thrown we know there are duplicates but we till don't know
         * which; it will be determined here.
         * \copydoc doxygen_hide_source_location
         */
        explicit DuplicateInInputFile(const FilesystemNS::File& filename,
                                      const std::string& section,
                                      const std::vector<std::string>& variable_list,
                                      const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        DuplicateInInputFile(const DuplicateInInputFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        DuplicateInInputFile(DuplicateInInputFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DuplicateInInputFile& operator=(const DuplicateInInputFile& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        DuplicateInInputFile& operator=(DuplicateInInputFile&& rhs) = default;


        //! Destructor
        virtual ~DuplicateInInputFile() override;
    };


    /*!
     * \brief Thrown when a same key (section/name) appears twice in the tuple elements.
     */
    class DuplicateInTuple final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] key The key found at least twice.
         * Key is section.name is section is not empty and name otherwise.
         * \copydoc doxygen_hide_source_location
         */
        explicit DuplicateInTuple(const std::string& key,
                                  const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        DuplicateInTuple(const DuplicateInTuple& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        DuplicateInTuple(DuplicateInTuple&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DuplicateInTuple& operator=(const DuplicateInTuple& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        DuplicateInTuple& operator=(DuplicateInTuple&& rhs) = default;


        //! Destructor
        virtual ~DuplicateInTuple() override;
    };


    /*!
     * \brief Thrown when a data in the input data file is not related to an
     * element in the tuple list.
     */
    class UnboundInputData final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] filename Name of the input file from which input data were read.
         * \param[in] section Section in the input file.
         * \param[in] variable Variable read in the input file.
         * \copydoc doxygen_hide_source_location
         */
        explicit UnboundInputData(const FilesystemNS::File& filename,
                                  std::string_view section,
                                  std::string_view variable,
                                  const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        UnboundInputData(const UnboundInputData& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnboundInputData(UnboundInputData&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnboundInputData& operator=(const UnboundInputData& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnboundInputData& operator=(UnboundInputData&& rhs) = default;

        //! Destructor
        virtual ~UnboundInputData() override;
    };


    /*!
     * \brief Thrown when a data in the input data file is not related to an
     * element in the tuple list.
     *
     * \tparam SubTupleT Tuple type which includes several input data which vectors should
     * have been the same size.
     *
     */
    template<class SubTupleT>
    class MismatchedVectorSize final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit MismatchedVectorSize(const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        MismatchedVectorSize(const MismatchedVectorSize& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        MismatchedVectorSize(MismatchedVectorSize&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        MismatchedVectorSize& operator=(const MismatchedVectorSize& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        MismatchedVectorSize& operator=(MismatchedVectorSize&& rhs) = default;

        //! Destructor
        virtual ~MismatchedVectorSize() override;
    };


    /*!
     * \brief Thrown when MPI has not been initialized properly.
     *
     */
    class MpiNotInitialized final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit MpiNotInitialized(const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        MpiNotInitialized(const MpiNotInitialized& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        MpiNotInitialized(MpiNotInitialized&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        MpiNotInitialized& operator=(const MpiNotInitialized& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        MpiNotInitialized& operator=(MpiNotInitialized&& rhs) = default;

        //! Destructor
        virtual ~MpiNotInitialized() override;
    };


    /*!
     * \brief Thrown when a folder specified in the input file doesn't exist whereas it was
     * expected to.
     *
     */
    class FolderDoesntExist final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] folder Name of the folder.
         * \copydoc doxygen_hide_source_location
         */
        explicit FolderDoesntExist(const std::string& folder,
                                   const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        FolderDoesntExist(const FolderDoesntExist& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        FolderDoesntExist(FolderDoesntExist&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        FolderDoesntExist& operator=(const FolderDoesntExist& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        FolderDoesntExist& operator=(FolderDoesntExist&& rhs) = default;

        //! Destructor
        virtual ~FolderDoesntExist() override;
    };


    /*!
     * \brief Thrown when ModelSettings want to modify an entry not present in the tuple.
     *
     */
    class NoEntryInModelSettings final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] entry_identifier The entry that couldn't be found in ModelSettings.
         * \copydoc doxygen_hide_source_location
         */
        explicit NoEntryInModelSettings(const std::string& entry_identifier,
                                        const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        NoEntryInModelSettings(const NoEntryInModelSettings& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        NoEntryInModelSettings(NoEntryInModelSettings&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NoEntryInModelSettings& operator=(const NoEntryInModelSettings& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        NoEntryInModelSettings& operator=(NoEntryInModelSettings&& rhs) = default;

        //! Destructor
        virtual ~NoEntryInModelSettings() override;
    };


    /*!
     * \brief Thrown when ModelSettings::Add() is called twice upon the same entry.
     *
     */
    class ValueCantBeSetTwice final : public Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] entry_identifier The entry that couldn't be found in ModelSettings.
         * \copydoc doxygen_hide_source_location
         */
        explicit ValueCantBeSetTwice(const std::string& entry_identifier,
                                     const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        ValueCantBeSetTwice(const ValueCantBeSetTwice& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        ValueCantBeSetTwice(ValueCantBeSetTwice&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ValueCantBeSetTwice& operator=(const ValueCantBeSetTwice& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        ValueCantBeSetTwice& operator=(ValueCantBeSetTwice&& rhs) = default;

        //! Destructor
        virtual ~ValueCantBeSetTwice() override;
    };


    /*!
     * \brief Thrown when ModelSettings tuple is not completely filled when there is a call to \a ModelSettings::CheckTupleCompletelyFilled().
     *
     */
    class ModelSettingsNotCompletelyFilled final : public Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] missing_entries List of `ModelSettings` entries for which a value was not properly provided.
         * \copydoc doxygen_hide_source_location
         */
        explicit ModelSettingsNotCompletelyFilled(
            const std::vector<std::string>& missing_entries,
            const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        ModelSettingsNotCompletelyFilled(const ModelSettingsNotCompletelyFilled& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        ModelSettingsNotCompletelyFilled(ModelSettingsNotCompletelyFilled&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ModelSettingsNotCompletelyFilled& operator=(const ModelSettingsNotCompletelyFilled& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        ModelSettingsNotCompletelyFilled& operator=(ModelSettingsNotCompletelyFilled&& rhs) = default;

        //! Destructor
        virtual ~ModelSettingsNotCompletelyFilled() override;
    };


    /*!
     * \brief Thrown when the model developer forgot to add a `IndexedSectionDescription` object in the model-specific
     * \a ModelSettings tuple.
     *
     */
    class MissingIndexedSectionDescriptionInModelSettingsTuple final : public Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] section_name Section for which the \a IndexedSectionDescription is missing.
         * \copydoc doxygen_hide_source_location
         */
        explicit MissingIndexedSectionDescriptionInModelSettingsTuple(
            std::string_view section_name,
            const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        MissingIndexedSectionDescriptionInModelSettingsTuple(
            const MissingIndexedSectionDescriptionInModelSettingsTuple& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        MissingIndexedSectionDescriptionInModelSettingsTuple(
            MissingIndexedSectionDescriptionInModelSettingsTuple&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        MissingIndexedSectionDescriptionInModelSettingsTuple&
        operator=(const MissingIndexedSectionDescriptionInModelSettingsTuple& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        MissingIndexedSectionDescriptionInModelSettingsTuple&
        operator=(MissingIndexedSectionDescriptionInModelSettingsTuple&& rhs) = default;

        //! Destructor
        virtual ~MissingIndexedSectionDescriptionInModelSettingsTuple() override;
    };


} // namespace MoReFEM::InputDataNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/Exceptions/InputData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_EXCEPTIONS_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
