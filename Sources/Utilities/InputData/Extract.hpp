// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_EXTRACT_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_EXTRACT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <string>

#include "Utilities/Filesystem/File.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Enum.hpp"             // IWYU pragma: export
#include "Utilities/InputData/Internal/Extract.hpp" // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"              // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    /*!
     * \brief Extract a model-related specific information from a leaf either from input data (exclusive-)OR from model settings.
     *
     * \tparam LeafT Full type of the leaf which value is sought. Might be for instance
     * InputDataNS::FEltSpace<3>::DomainIndex.
     * \tparam InputDataT Type of \a input_data, which holds data the end-user may modify in the Lua file.
     * \tparam ModelSettingsT Type of \a model_settings, which holds data that are specified (and hardcoded) by the
     * author of the model; end-user is not supposed to modify it.
     *
     * \copydoc doxygen_hide_model_settings_input_data_arg
     *
     * \return Value found either in \a input_data or in \a model_settings. Can't be found in both - in this case the
     * model would not even compile.
     *
     * \attention There is a namesake function in Core library that provides syntactic sugar, but requires more stuff not available
     * in current Utilities library. The overload is used more often as it is more convenient (less arguments to
     * provide); make sure however to provide the proper include if you're using one or the other.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    decltype(auto) ExtractLeaf(const ModelSettingsT& model_settings, const InputDataT& input_data);


    /*!
     * \brief Convenient enum class
     *
     * it is used to decide whether \a ExtractLeafAsPath environment
     * variables (materialized through syntax `${ENVIRONEMENT_VARIABLE}`) must be replaced by the
     * actual related value of the environment variable or not.
     *
     */
    enum class SubstituteEnvironmentVariables { yes, no };

    /*!
     * \brief Mostly the same as \a ExtractLeaf, except that the string read
     * is converted as a  `std::filesystem_path` with optionally environment variable interpreted.
     *
     *
     * \tparam DoSubstituteEnvironmentVariablesT Choose no if you want to keep the string as it is,
     * and yes if you want to substitute all environment variables by their values.
     *
     * \copydoc doxygen_hide_model_settings_input_data_arg
     *
     * \attention There is a namesake function in Core library that provides syntactic sugar, but requires more stuff not available
     * in current Utilities library. The overload is used more often as it is more convenient (less arguments to
     * provide); make sure however to provide the proper include if you're using one or the other.
     */

    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
             ::MoReFEM::Concept::InputDataType InputDataT,
             SubstituteEnvironmentVariables DoSubstituteEnvironmentVariablesT = SubstituteEnvironmentVariables::yes>
    // clang-format on
    decltype(auto) ExtractLeafAsPath(const ModelSettingsT& model_settings, const InputDataT& input_data);


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/InputData/Extract.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_EXTRACT_DOT_HPP_
// *** MoReFEM end header guards *** < //
