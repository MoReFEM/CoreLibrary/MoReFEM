// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INPUTDATA_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

// IWYU pragma: begin_exports
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: keep
// IWYU pragma: end_exports

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
#include "Utilities/String/String.hpp"

// IWYU pragma: begin_exports
#include "Utilities/InputData/Enum.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
#include "Utilities/Mpi/Mpi.hpp"
// IWYU pragma: end_exports

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::InputDataNS { template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT> struct ExtractLeaf; }
namespace MoReFEM::Internal::InputDataNS { template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT> struct ExtractSection; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief This class read the input data file and then is in charge of holding the values read.
     *
     * It hold the charge of checking that mandatory parameters are correctly filled in the data file as soon
     * as possible: we want for instance to avoid a program failure at the end because a path for an output
     * file is invalid.
     *
     * \copydoc doxygen_hide_tparam_do_update_lua_file
     *
     * \tparam TupleT A tuple which should include a class for each input data to consider in the file.
     * The class for each input datum should derive from InputData::Crtp::InputData and defines several
     * static methods such as Description(), NameInFile() or Section(). Compiler won't let you not define one
     * of those anyway; to see an example of such a class see in Core/InputData or in MoReFEM documentation.
     *
     * \copydoc doxygen_hide_model_settings_input_data
     */
    // clang-format off
    template
    <
        Concept::Tuple TupleT,
        InputDataNS::do_update_lua_file DoUpdateLuaFileT = InputDataNS::do_update_lua_file::no
    >
    class InputData
    : public Internal::InputDataNS::AbstractClass<TupleT>
    // clang-format on
    {
      public:
        //! Helper variable to define the \a InputDataType concept.
        static inline constexpr bool ConceptIsInputData = true;

      public:
        //! Friendship.
        template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
        friend struct ::MoReFEM::Internal::InputDataNS::ExtractLeaf;

        //! Friendship.
        template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
        friend struct ::MoReFEM::Internal::InputDataNS::ExtractSection;


      public:
        //! Self.
        using self = InputData<TupleT, DoUpdateLuaFileT>;

        //! Abstract parent.
        using parent = Internal::InputDataNS::AbstractClass<TupleT>;

        //! Alias to smart pointers storing objects.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! The underlying tuple type.
        using underlying_tuple_type = TupleT;

        //! Special members.
        //@{

        /*!
         * \brief Constructor.
         *
         * \param[in] filename Name of the Lua input data list to be read.
         * \param[in] do_track_unused_fields Whether a field found in the file but not referenced in the tuple
         * yields an exception or not. Should be 'yes' most of the time; I have introduced the 'no' option for
         * cases in which we need only a handful of parameters shared by all models for post-processing
         * purposes (namely mesh-related ones).
         * \copydoc doxygen_hide_model_settings_arg
         */
        template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
        explicit InputData(
            const ModelSettingsT& model_settings,
            const FilesystemNS::File& filename,
            InputDataNS::DoTrackUnusedFields do_track_unused_fields = InputDataNS::DoTrackUnusedFields::yes);

        //! \copydoc doxygen_hide_copy_constructor
        InputData(const InputData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InputData(InputData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InputData& operator=(const InputData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InputData& operator=(InputData&& rhs) = delete;


        //! Destructor.
        virtual ~InputData() override;
        //@}


        //! Get the path to the input data file.
        //! \return Path to the input data file.
        const FilesystemNS::File& GetInputFile() const;

      private:
        /*!
         * \brief Helper function used to handle a path: it is basically handled as a mere string, except
         * that environment variables are replaced on the fly if they respect the format ${...}.
         *
         * \warning Contrary to what the const suggests (it is there to give the same interface whatever the string
         * input datum is), this method might modify the value stored in the tuple. On first call all
         * environment variables are replaced, and what is stored afterwards is the result after the
         * substitution.
         *
         * For instance "${HOME}/Codes/MoReFEM" will be resolved on a Mac in /Users/ *username* /Codes/MoReFEM.
         *
         * \return The requested element in the tuple.
         */
        template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
        std::filesystem::path ExtractLeafAsPath() const;

      private:
        /*!
         * \brief Check no input data present in the Lua file is actually not known by the input data
         * file tuple.
         *
         * \param[in] filename Path to the input file.
         * \param[in] lua_option_file The object that helps interpreting the content of the Lua file.
         * \param[in] do_track_unused_fields See constructor.
         *
         */
        void CheckUnboundInputData(const FilesystemNS::File& filename,
                                   ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
                                   InputDataNS::DoTrackUnusedFields do_track_unused_fields) const;


      private:
        //! Path of the input data file.
        FilesystemNS::File input_data_file_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/InputData.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
