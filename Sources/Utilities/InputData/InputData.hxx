// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INPUTDATA_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_INPUTDATA_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/InputData.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <source_location>

#include "Utilities/Environment/Environment.hpp" // IWYU pragma: export


namespace MoReFEM
{


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    // clang-format on
    InputData<TupleT, DoUpdateLuaFileT>::InputData(
        const ModelSettingsT& model_settings,
        const FilesystemNS::File& filename,
        [[maybe_unused]] InputDataNS::DoTrackUnusedFields do_track_unused_fields)
    : parent(), input_data_file_(filename)
    {
        int is_initialized;

        MPI_Initialized(&is_initialized);

        if (!is_initialized)
            throw ::MoReFEM::InputDataNS::ExceptionNS::MpiNotInitialized();

        static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value,
                      "Template argument is expected to be a std::tuple.");

        ::MoReFEM::Wrappers::Lua::OptionFile lua_option_file(filename);

        if constexpr (DoUpdateLuaFileT == ::MoReFEM::InputDataNS::do_update_lua_file::no)
            CheckUnboundInputData(filename, lua_option_file, do_track_unused_fields);

        // Fill from the file all the objects in tuple_.
        Internal::InputDataNS::FillTuple<DoUpdateLuaFileT>(
            model_settings, this, lua_option_file, parent::GetNonCstTuple());
    }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    InputData<TupleT, DoUpdateLuaFileT>::~InputData()
    { }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    inline const FilesystemNS::File& InputData<TupleT, DoUpdateLuaFileT>::GetInputFile() const
    {
        return input_data_file_;
    }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
    std::filesystem::path InputData<TupleT, DoUpdateLuaFileT>::ExtractLeafAsPath() const
    {
        decltype(auto) string = parent::template ExtractLeaf<LeafT, ::MoReFEM::InputDataNS::CountAsUsed::yes>();

        if constexpr (DoUpdateLuaFileT == ::MoReFEM::InputDataNS::do_update_lua_file::yes)
        {
            return std::filesystem::path{ string };
        } else
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();
            return std::filesystem::path{ environment.SubstituteValues(string) };
        }
    }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    void InputData<TupleT, DoUpdateLuaFileT>::CheckUnboundInputData(
        const FilesystemNS::File& filename,
        ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
        InputDataNS::DoTrackUnusedFields do_track_unused_fields) const
    {
        // Check there are no items undefined in the tuple.
        if (do_track_unused_fields == InputDataNS::DoTrackUnusedFields::yes)
        {
            decltype(auto) entry_key_list = lua_option_file.GetEntryKeyList();

            for (decltype(auto) entry_key : entry_key_list)
            {
                const auto pos = entry_key.rfind(".");

                std::string_view section_name, variable;

                if (pos == std::string::npos)
                {
                    section_name = "";
                    variable = entry_key;
                } else
                {
                    section_name = std::string_view(entry_key.data(), pos);
                    variable = std::string_view(&entry_key.at(pos + 1), entry_key.size() - pos - 1);
                }

                if (!parent::tuple_iteration::DoMatchIdentifier(section_name, variable))
                    throw ::MoReFEM::InputDataNS::ExceptionNS::UnboundInputData(filename, section_name, variable);
            }
        }
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INPUTDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
