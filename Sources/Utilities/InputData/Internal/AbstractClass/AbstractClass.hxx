// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_ABSTRACTCLASS_ABSTRACTCLASS_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_ABSTRACTCLASS_ABSTRACTCLASS_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::InputDataNS
{


    template<::MoReFEM::Concept::Tuple TupleT>
    AbstractClass<TupleT>::AbstractClass()
    {
        CheckNoDuplicateKeysInTuple();
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    AbstractClass<TupleT>::~AbstractClass()
    { }


    template<::MoReFEM::Concept::Tuple TupleT>
    constexpr std::size_t AbstractClass<TupleT>::Size()
    {
        return std::tuple_size<TupleT>::value;
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    constexpr std::size_t AbstractClass<TupleT>::Nleaves()
    {
        return tuple_iteration::Nleaves();
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    const TupleT& AbstractClass<TupleT>::GetTuple() const noexcept
    {
        return tuple_;
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    TupleT& AbstractClass<TupleT>::GetNonCstTuple()
    {
        return const_cast<TupleT&>(GetTuple());
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT,
             ::MoReFEM::InputDataNS::CountAsUsed CountAsUsedT>
    auto AbstractClass<TupleT>::ExtractLeaf() const ->
        typename Utilities::ConstRefOrValue<typename LeafT::return_type>::type
    {
        static_assert(tuple_iteration::template Find<LeafT>(),
                      "InputData not defined in the tuple! To find out which in the admittedly poor "
                      "error message, seek in the block reported in Extract.hxx file below the class name "
                      "between the template brackets of 'ExtractLeaf' (just above the line "
                      "'ExtractLeaf<LeafT, CountAsUsedT>()'");

        const auto& optional_item = tuple_iteration::template ExtractLeafHandler<LeafT>(tuple_);

        assert(optional_item.has_value()
               && "If the item is not defined in the tuple static assert two lines "
                  "earlier should have been triggered");

        const auto& item = optional_item.value().get();

        if (CountAsUsedT == ::MoReFEM::InputDataNS::CountAsUsed::yes)
            item.SetAsUsed();

        return item.GetTheValue();
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    template<Advanced::Concept::InputDataNS::SectionType SectionT>
    auto& AbstractClass<TupleT>::ExtractSection() const
    {
        static_assert(tuple_iteration::template Find<SectionT>());

        auto optional_item = tuple_iteration::template ExtractSectionHandler<SectionT>(tuple_);

        assert(optional_item.has_value()
               && "If the item is not defined in the tuple static assert two lines "
                  "earlier should have been triggered");

        return optional_item.value().get();
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    void AbstractClass<TupleT>::CheckNoDuplicateKeysInTuple() const
    {
        // Check there are no type duplicated in the tuple.
        Utilities::Tuple::AssertNoDuplicate<TupleT>::Perform();

        // Check there are no duplicated keys in the tuple (two different types that share the same key for
        // instance).
        std::unordered_set<std::string> buf;
        tuple_iteration::CheckNoDuplicateKeysInTuple(buf);
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    void AbstractClass<TupleT>::PrintKeys(std::ostream& stream) const
    {
        const auto keys = ExtractKeys();

        Utilities::PrintContainer<>::Do(keys,
                                        stream,
                                        ::MoReFEM::PrintNS::Delimiter::separator("\n\t"),
                                        ::MoReFEM::PrintNS::Delimiter::opener("\t"),
                                        ::MoReFEM::PrintNS::Delimiter::closer("\n"));
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    std::vector<std::string> AbstractClass<TupleT>::ExtractKeys() const
    {
        std::vector<std::string> ret;
        tuple_iteration::ExtractKeys(ret);
        std::sort(ret.begin(), ret.end());
        return ret;
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    std::vector<std::string> AbstractClass<TupleT>::ComputeUnusedLeafList(const ::MoReFEM::Wrappers::Mpi& mpi) const
    {
        // First gather the data from all processors: it is possible (though unlikely) than a given
        // input item is read only in some of the processors.
        std::vector<bool> stats;
        std::vector<std::string> identifiers;

        tuple_iteration::IsUsed(tuple_, identifiers, stats);

        const auto size = identifiers.size();
        assert(size == stats.size());

        // Then reduce it on the root processor.
        auto&& reduced_stats = mpi.ReduceOnRootProcessor(stats, ::MoReFEM::Wrappers::MpiNS::Op::LogicalOr);
        assert(reduced_stats.size() == size);

        // On non root processors, return an empty list (by design - point is to return otherwise
        // only on root processor).
        if (!mpi.IsRootProcessor())
            return std::vector<std::string>{};

        std::vector<std::string> ret;
        ret.reserve(size);

        for (auto index = 0UL; index < size; ++index)
        {
            if (!reduced_stats[index])
                ret.push_back(identifiers[index]);
        }

        std::sort(ret.begin(), ret.end());

        return ret;
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    void AbstractClass<TupleT>::PrintUnusedLeafs(std::ostream& stream, const ::MoReFEM::Wrappers::Mpi& mpi) const
    {
        auto unused_leaf_list = ComputeUnusedLeafList(mpi);

        if (!unused_leaf_list.empty())
        {
            assert(mpi.IsRootProcessor() && "List is designed to be empty on non root ranks.");

            stream << "Note about InputData: some of the input data weren't "
                      "actually used by the program:"
                   << std::endl;

            ::MoReFEM::Utilities::PrintContainer<>::Do(unused_leaf_list,
                                                       stream,
                                                       ::MoReFEM::PrintNS::Delimiter::separator("\n\t - "),
                                                       ::MoReFEM::PrintNS::Delimiter::opener("\t - "),
                                                       ::MoReFEM::PrintNS::Delimiter::closer("\n"));
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    template<class ItemT>
    constexpr bool AbstractClass<TupleT>::Find()
    {
        if constexpr (std::tuple_size_v<TupleT> == 0UL)
            return false;

        return tuple_iteration::template Find<ItemT>();
    }

} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_ABSTRACTCLASS_ABSTRACTCLASS_DOT_HXX_
// *** MoReFEM end header guards *** < //
