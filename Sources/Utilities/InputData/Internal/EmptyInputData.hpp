// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_EMPTYINPUTDATA_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_EMPTYINPUTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <tuple>

#include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp" // IWYU pragma: export

namespace MoReFEM::Internal::InputDataNS
{


    /*!
     * \brief Placeholder when there are no \a InputData.
     *
     */
    struct EmptyInputData : public Internal::InputDataNS::AbstractClass<std::tuple<>>
    {
        //! Helper variable to define the \a InputDataType concept.
        static inline constexpr bool ConceptIsInputData = true;

        //! Traits expected in an \a InputData object.
        using underlying_tuple_type = std::tuple<>;

        //! Traits expected in an \a InputData object.
        using const_unique_ptr = std::unique_ptr<const EmptyInputData>;

        //! Destructor
        virtual ~EmptyInputData() override;
    };


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_EMPTYINPUTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
