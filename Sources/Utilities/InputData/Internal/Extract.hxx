// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_EXTRACT_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_EXTRACT_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Internal/Extract.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"

namespace MoReFEM::Internal::InputDataNS
{


    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
    template<::MoReFEM::Concept::InputDataOrModelSettingsType InputDataT,
             ::MoReFEM::InputDataNS::CountAsUsed CountAsUsedT>
    typename Utilities::ConstRefOrValue<typename ExtractLeaf<LeafT>::return_type>::type
    ExtractLeaf<LeafT>::Value(const InputDataT& input_data)
    {
        return input_data.template ExtractLeaf<LeafT, CountAsUsedT>();
    }


    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
    template<::MoReFEM::Concept::InputDataType InputDataT>
    std::filesystem::path ExtractLeaf<LeafT>::Path(const InputDataT& input_data)
    {
        return input_data.template ExtractLeafAsPath<LeafT>();
    }


    template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
    template<::MoReFEM::Concept::InputDataOrModelSettingsType InputDataT>
    const auto& ExtractSection<SectionT>::Value(const InputDataT& input_data)
    {
        return input_data.template ExtractSection<SectionT>();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::InputDataNS::CountAsUsed CountAsUsedT
    >
    // clang-format on
    decltype(auto)
    ExtractLeafFromSection(const ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section)
    {
        using tuple_iteration =
            Internal::InputDataNS::TupleIteration<std::decay_t<decltype(section.GetSectionContent())>, 0UL>;

        const auto& section_content_tuple = section.GetSectionContent();

        using tuple_type = std::decay_t<decltype(section_content_tuple)>;

        static_assert(tuple_iteration::template Find<LeafNameT>());

        static_assert(::MoReFEM::Utilities::Tuple::IndexOf<LeafNameT, tuple_type>::value
                          < std::tuple_size<std::decay_t<decltype(section_content_tuple)>>(),
                      "Leaf is expected to be found directly in the section, whereas it is here enclosed in a "
                      "subsection (if not found at all the previous static assert would have failed).");

        const auto& leaf =
            std::get<::MoReFEM::Utilities::Tuple::IndexOf<LeafNameT, tuple_type>::value>(section_content_tuple);

        if (CountAsUsedT == ::MoReFEM::InputDataNS::CountAsUsed::yes)
            leaf.SetAsUsed();

        return leaf.GetTheValue();
    }


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_EXTRACT_DOT_HXX_
// *** MoReFEM end header guards *** < //
