// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_MODELSETTINGS_INDEXEDSECTIONDESCRIPTION_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_MODELSETTINGS_INDEXEDSECTIONDESCRIPTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>

namespace MoReFEM::Internal::InputDataNS
{

    /*!
     * \brief Suffix applied to \a NameInFile() to identify a \a IndexedSectionDescription.
     *
     * The suffix is chosen so that it is unlikely a model developer would come with a name enacting
     * the same suffix.
     */
    static std::string IndexedSectionDescriptionSuffix();


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
#include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_MODELSETTINGS_INDEXEDSECTIONDESCRIPTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
