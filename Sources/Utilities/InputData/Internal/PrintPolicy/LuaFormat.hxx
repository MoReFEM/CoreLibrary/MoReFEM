// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_PRINTPOLICY_LUAFORMAT_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_PRINTPOLICY_LUAFORMAT_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Internal/PrintPolicy/LuaFormat.hpp"
// *** MoReFEM header guards *** < //


#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"


namespace MoReFEM::Internal::PrintPolicyNS
{


    template<class ElementTypeT>
    void LuaFormat::Do(std::ostream& stream, ElementTypeT&& entry)
    {
        using type = std::decay_t<ElementTypeT>;

        if constexpr (Utilities::IsSpecializationOf<std::variant, type>())
        {
            std::visit(
                [&stream](auto&& arg)
                {
                    LuaFormat::Do(stream, arg);
                },
                entry);
        } else if constexpr (Utilities::IsSpecializationOf<::MoReFEM::Wrappers::Lua::Function, type>())
        {
            stream << "\n[[\n";
            stream << entry.GetString();
            stream << "]]";
        } else if constexpr (std::is_same<type, std::string>())
        {
            stream << "'" << entry << "'";
        } else if constexpr (std::is_same<type, std::filesystem::path>())
        {
            // The STL prints a filesystem::path with enclosing double quotes
            stream << entry;
        } else if constexpr (std::is_arithmetic<type>())
        {
            if constexpr (std::is_same<type, bool>())
                stream << std::boolalpha << entry;
            else
                stream << std::setprecision(15) << entry;
        } else if constexpr (Utilities::IsSpecializationOf<std::vector, type>())
        {
            // Trick here: current class is used as policy to that the choices here will be applied as well on
            // each element of the vector.
            Utilities::PrintContainer<LuaFormat>::Do(entry,
                                                     stream,
                                                     ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                     ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                     ::MoReFEM::PrintNS::Delimiter::closer(" }"));

        } else if constexpr (Utilities::IsSpecializationOf<std::map, type>())
        {
            // Map can only called on the end of the recursion, contrary to vector.
            using policy =
                ::MoReFEM::Utilities::PrintPolicyNS::Associative<Utilities::PrintPolicyNS::associative_format::Lua>;
            Utilities::PrintContainer<policy>::Do(entry,
                                                  stream,
                                                  ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                                  ::MoReFEM::PrintNS::Delimiter::opener("{ "),
                                                  ::MoReFEM::PrintNS::Delimiter::closer("} "));
        } else if constexpr (std::is_same<std::nullptr_t,
                                          type>()) // might happen: the 'ignore' parameters proceed that way
        {
            stream << 0;
        } else if constexpr (IsStrongType<type>())
        {
            LuaFormat::Do(stream, entry.Get());
        } else
        {
            std::cerr << "Type not handled: " << ::MoReFEM::GetTypeName<type>() << std::endl;
            assert(false && "Case not foreseen...");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::PrintPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_PRINTPOLICY_LUAFORMAT_DOT_HXX_
// *** MoReFEM end header guards *** < //
