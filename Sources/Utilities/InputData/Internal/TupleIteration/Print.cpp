// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <deque>
#include <memory>
#include <ostream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/InputData/Internal/TupleIteration/Print.hpp"

#include "Utilities/InputData/Internal/Write/Enum.hpp"
#include "Utilities/String/String.hpp"


namespace MoReFEM::Internal::InputDataNS
{

    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    namespace // anonymous
    {


        struct AnalyzePath
        {

            using unique_ptr = std::unique_ptr<AnalyzePath>;

            using vector_unique_ptr = std::vector<unique_ptr>;

            explicit AnalyzePath(const std::pair<std::string, std::string>& key_and_content);

            std::string item_name_;

            std::string content_in_file_;

            std::vector<std::string> section_list_;
        };


        struct Section
        {

            using unique_ptr = std::unique_ptr<Section>;

            using vector_unique_ptr = std::vector<unique_ptr>;

            explicit Section(std::string name);

            std::string name;

            Section::vector_unique_ptr section_list_;

            AnalyzePath::vector_unique_ptr item_list_;
        };


        void SortPerSection(const std::vector<std::pair<std::string, std::string>>& content,
                            Section::vector_unique_ptr& section_hierarchy,
                            AnalyzePath::vector_unique_ptr& root_level_item_list);


        void PrintItemList(const AnalyzePath::vector_unique_ptr& item_list,
                           std::size_t depth_level,
                           std::ostream& stream,
                           verbosity verbosity_level);

        /*!
         * \brief Print the content of all subsections within a section.
         *
         * \internal <b><tt>[internal]</tt></b> This function calls itself recursively on purpose: we do not
         * want to miss one item in the hierarchy. \endinternal
         */
        void PrintSubsectionContent(const Section::vector_unique_ptr& subsection_list,
                                    std::size_t& depth_level,
                                    std::ostream& stream,
                                    verbosity verbosity_level);


    } // namespace
    // NOLINTEND(misc-non-private-member-variables-in-classes)


    const std::string& GetIndentPlaceholder()
    {
        static const std::string ret("INDENT_PLACEHOLDER");
        return ret;
    }


    void PrintInFile(const std::vector<std::pair<std::string, std::string>>& item_block_per_identifier,
                     std::ostream& stream,
                     const std::unordered_map<std::string, std::deque<std::string>>& section_description_list,
                     verbosity verbosity_level)
    {

        Section::vector_unique_ptr section_hierarchy;
        AnalyzePath::vector_unique_ptr root_level_item_list;

        SortPerSection(item_block_per_identifier, section_hierarchy, root_level_item_list);

        std::size_t depth_level = 0UL;

        constexpr auto max_length = 105UL;

        // Iterate through the section hierarchy.
        for (const auto& section_ptr : section_hierarchy)
        {
            assert(!(!section_ptr));

            const auto& section = *section_ptr;

            auto it = section_description_list.find(section.name);

            if (it != section_description_list.cend())
            {
                const auto& section_description = it->second;

                for (const auto& line : section_description)
                    stream << "-- " << Utilities::String::Reformat(line, max_length, "-- ");
            }

            stream << section.name << " = {\n\n";
            ++depth_level;

            PrintSubsectionContent(section.section_list_, depth_level, stream, verbosity_level);
            PrintItemList(section.item_list_, depth_level, stream, verbosity_level);
            assert(depth_level > 0UL);
            --depth_level;
            stream << "} -- " << section.name << "\n\n";
        }

        // Then print the root level items, which were not in the hierarchy considered so far.
        assert(depth_level == 0UL);
        PrintItemList(root_level_item_list, depth_level, stream, verbosity_level);
    }


    namespace // anonymous
    {


        AnalyzePath::AnalyzePath(const std::pair<std::string, std::string>& key_and_content)
        : content_in_file_(key_and_content.second)
        {
            Utilities::String::StripLeft(content_in_file_, "\n");

            const auto& identifier = key_and_content.first;

            auto pos = identifier.rfind('.');

            if (pos == std::string::npos)
                item_name_ = identifier;
            else
            {
                item_name_ = std::string(identifier, pos + 1, std::string::npos);

                std::string parent(identifier, 0, pos);

                pos = parent.find('.');

                while (pos != std::string::npos)
                {
                    section_list_.emplace_back(parent, 0, pos);
                    parent.erase(0, pos + 1);

                    pos = parent.find('.');
                }

                section_list_.push_back(parent);
            }
        }


        Section::Section(std::string a_name) : name(std::move(std::move(a_name)))
        { }


        Section* Helper(AnalyzePath::unique_ptr& current_object_ptr,
                        std::size_t depth_level,
                        Section::vector_unique_ptr& section_list,
                        AnalyzePath::vector_unique_ptr& end_item_list);


        Section* Helper(AnalyzePath::unique_ptr& current_object_ptr,
                        std::size_t depth_level,
                        Section::vector_unique_ptr& section_list,
                        AnalyzePath::vector_unique_ptr& end_item_list)

        {
            assert(!(!current_object_ptr));

            auto& item = *current_object_ptr;

            if (depth_level < item.section_list_.size())
            {
                const auto& name = item.section_list_[depth_level];

                auto it = std::ranges::find_if(section_list,

                                               [&name](const auto& section)
                                               {
                                                   return section->name == name;
                                               });

                if (it == section_list.cend())
                {
                    section_list.emplace_back(std::make_unique<Section>(name));
                    return section_list.back().get();
                }

                return it->get();
            }

            end_item_list.push_back(nullptr);
            std::swap(current_object_ptr, end_item_list.back());
            return nullptr;
        }


        void SortPerSection(const std::vector<std::pair<std::string, std::string>>& content,
                            Section::vector_unique_ptr& section_hierarchy,
                            AnalyzePath::vector_unique_ptr& root_level_item_list)
        {
            AnalyzePath::vector_unique_ptr break_into_sections;

            std::size_t maximum_depth = 0UL;


            // Iterate through the keys and split content into sections.
            for (const auto& item : content)
            {
                auto&& current_object = std::make_unique<AnalyzePath>(item);

                maximum_depth = std::max(current_object->section_list_.size(), maximum_depth);

                break_into_sections.emplace_back(std::move(current_object));
            }

            // Create all root level sections and end-items.


            for (auto& item_ptr : break_into_sections)
            {
                std::size_t index = 0UL;
                auto* next_section_ptr = Helper(item_ptr, index, section_hierarchy, root_level_item_list);

                while (next_section_ptr != nullptr)
                {
                    auto& next_section = *next_section_ptr;

                    next_section_ptr = Helper(item_ptr, ++index, next_section.section_list_, next_section.item_list_);
                }
            }
        }


        void PrintItemList(const AnalyzePath::vector_unique_ptr& item_list,
                           std::size_t depth_level,
                           std::ostream& stream,
                           verbosity verbosity_level)
        {

            const auto Nitem = item_list.size();
            std::size_t index = 0UL;

            for (const auto& item_ptr : item_list)
            {
                ++index;

                assert(!(!item_ptr));

                auto& content = item_ptr->content_in_file_;

                // Replace placeholder by the correct indent.
                const auto indent = Utilities::String::Repeat('\t', depth_level);

                Utilities::String::Replace(GetIndentPlaceholder(), indent, content);

                stream << item_ptr->content_in_file_;

                if (index < Nitem && depth_level > 0UL)
                    stream << ',';
                else if (verbosity_level == verbosity::compact) // not a mistake
                    stream << "\n";

                if (verbosity_level == verbosity::verbose)
                    stream << "\n";

                stream << "\n";
            }
        }


        /*!
         * \brief Print the content of all subsections within a section.
         *
         * \internal <b><tt>[internal]</tt></b> This function calls itself recursively on purpose: we do not
         * want to miss one item in the hierarchy. \endinternal
         */
        // NOLINTBEGIN(misc-no-recursion)
        void PrintSubsectionContent(const Section::vector_unique_ptr& subsection_list,
                                    std::size_t& depth_level,
                                    std::ostream& stream,
                                    verbosity verbosity_level)
        {
            for (const auto& subsection_ptr : subsection_list)
            {
                assert(!(!subsection_ptr));
                const auto& subsection = *subsection_ptr;

                const auto indent = Utilities::String::Repeat('\t', depth_level);

                stream << indent << subsection.name << " = {\n";

                PrintSubsectionContent(subsection.section_list_, ++depth_level, stream, verbosity_level);
                PrintItemList(subsection.item_list_, depth_level, stream, verbosity_level);
                assert(depth_level > 0UL);
                --depth_level;

                stream << indent << "}, -- " << subsection.name << "\n\n";
            }
        }
        // NOLINTEND(misc-no-recursion)


    } // namespace


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
