// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <filesystem>
#include <string> // IWYU pragma: keep
#include <vector>
// IWYU pragma:  no_include <iosfwd>

#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"


namespace MoReFEM::Internal::InputDataNS::Traits
{


    std::string Format<std::string>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return "\"VALUE\"";
    };


    std::string Format<std::filesystem::path>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return "\"VALUE\"";
    };


    std::string Format<std::vector<std::string>>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return R"({"VALUE1", "VALUE2", ...})";
    };


    std::string Format<bool>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return "'true' or 'false' (without the quote)";
    };


    std::string Format<std::vector<bool>>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return "{\"VALUE1\", \"VALUE2\", ...} where values are either 'true' or 'false' (without the "
               "quote)";
    };


} // namespace MoReFEM::Internal::InputDataNS::Traits


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
