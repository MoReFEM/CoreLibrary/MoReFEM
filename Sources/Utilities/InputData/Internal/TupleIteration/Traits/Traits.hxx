// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TRAITS_TRAITS_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TRAITS_TRAITS_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
// *** MoReFEM header guards *** < //


#include <map>
#include <sstream>
#include <string>
#include <variant>
#include <vector>

#include "Utilities/Type/StrongType/StrongType.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Lua { template <class T> struct Function; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InputDataNS::Traits
{


    template<class return_type>
    std::string Format<return_type>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        if constexpr (::MoReFEM::IsStrongType<return_type>())
            return Format<typename return_type::underlying_type>::Print(indent_comment);
        else
            return "VALUE";
    };


    template<class T, class U>
    std::string Format<std::map<T, U>>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return "{[KEY1] = VALUE1, [KEY2] = VALUE2, ...}";
    };


    template<class T>
    std::string Format<std::vector<T>>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return "{ VALUE1, VALUE2, ...}";
    };


    template<class T>
    std::string Format<::MoReFEM::Wrappers::Lua::Function<T>>::Print(const std::string& indent_comment, bool none_desc)
    {
        std::ostringstream oconv;

        oconv << "function in Lua language, for instance:\n"
              << indent_comment << "function(arg1, arg2, arg3)\n"
              << indent_comment << "return arg1 + arg2 - arg3\n"
              << indent_comment << "end\n"
              << indent_comment << "sin, cos and tan require a 'math.' prefix.\n";

        if (none_desc)
            oconv << indent_comment
                  << "If you do not wish to provide one, put anything you want (e.g. 'none'): "
                     "the content is not interpreted by OptionFile until an actual use of the "
                     "underlying function.";

        return oconv.str();
    };


    template<class... Args>
    std::string Format<std::variant<Args...>>::Print([[maybe_unused]] const std::string& indent_comment)
    {
        return "see the variant description...";
    };


} // namespace MoReFEM::Internal::InputDataNS::Traits


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TRAITS_TRAITS_DOT_HXX_
// *** MoReFEM end header guards *** < //
