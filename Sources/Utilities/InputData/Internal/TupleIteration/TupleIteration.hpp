// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TUPLEITERATION_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TUPLEITERATION_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <bitset>
#include <cstddef> // IWYU pragma: keep
#include <deque>
#include <list>
#include <map>
#include <tuple>
#include <unordered_set>
#include <vector>

#include "Utilities/Containers/Tuple/Tuple.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/Concept.hpp" // IWYU pragma: export
#include "Utilities/InputData/Enum.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"           // IWYU pragma: export
#include "Utilities/InputData/Internal/PrintPolicy/LuaFormat.hpp" // IWYU pragma: export
#include "Utilities/InputData/Internal/TupleIteration/Print.hpp"  // IWYU pragma: export
#include "Utilities/InputData/Internal/Write/Enum.hpp"            // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"


namespace MoReFEM::Internal::InputDataNS
{


    //! Enum class used in some methods that might either print default value or a value stored elsewhere.
    enum class print_default_value { yes, no, if_none_found };


    //! Convenient alias to avoid repeating endlessly namespaces.
    using Nature = ::MoReFEM::Advanced::InputDataNS::Nature;

    /*!
     * \class doxygen_hide_input_data_extract_leaf
     *
     * \param[in] tuple Tuple from which the input data are extracted.
     *
     * \return An optional to a reference to the leaf if found, or `std::nullopt` otherwise.
     * This means if you want to access the leaf itself you should apply `.value().get()` to respectively
     * distentangle the `std::optional` and the `std::reference_wrapper`.
     */


    /*!
     * \brief An helper class used to perform some operations recursively for all elements of \a TupleT
     *
     * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input data.
     * \tparam Index Index of the current element considered in the tuple.
     */
    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    struct TupleIteration
    {


        /*!
         * \brief Get an handler to get read access to the \a LeafT expected to be present in the \a tuple.
         *
         * If not, `std::nullopt` is returned.
         *
         * \copydoc doxygen_hide_input_data_extract_leaf
         */
        template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
        static constexpr Utilities::const_ref_or_nullopt<LeafT> ExtractLeafHandler(const TupleT& tuple);

        /*!
         * \brief Get an handler to get read/write access to the \a LeafT expected to be present in the \a tuple.
         *
         * If not, `std::nullopt` is returned.
         *
         * \copydoc doxygen_hide_input_data_extract_leaf
         */
        template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
        static constexpr Utilities::ref_or_nullopt<LeafT> ExtractModifyableLeafHandler(TupleT& tuple);

        /*!
         * \brief Get an handler to get read access to the \a SectionT expected to be present in the \a tuple.
         *
         * \param[in] tuple Tuple from which the section is extracted.
         *
         * \return An optional to a reference to the section if found, or `std::nullopt` otherwise.
         * This means if you want to access the leaf itself you should apply `.value().get()` to respectively
         * distentangle the `std::optional` and the `std::reference_wrapper`.
         */
        template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
        static Utilities::const_ref_or_nullopt<SectionT> ExtractSectionHandler(const TupleT& tuple);

        /*!
         * \brief Static method which looks for \a SectionOrLeafT within \a TupleT.
         *
         * \return True when current element of the tuple (at \a Index position) is \a SectionOrLeafT.
         *
         */
        template<::MoReFEM::Advanced::Concept::InputDataNS::SectionOrLeafType SectionOrLeafT>
        static constexpr bool Find();


        /*!
         * \brief Returns the number of leaves in the tuple.
         *
         * It is not the same as the size of the tuple: this method explorates the sections to count how many leaves
         * they include.
         *
         * \return Number of leaves found.
         */
        static constexpr std::size_t Nleaves();

        /*!
         * \brief Set the content of the tuple.
         *
         * \copydoc doxygen_hide_tparam_do_update_lua_file
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \param[in] lua_option_file \a OptionFile object that gives the requested information.
         * \param[in,out] tuple The tuple into which the information is stored.
         */
        // clang-format off
        template
        <
            ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        static void Fill(const ModelSettingsT& model_settings,
                         const InputDataT* input_data,
                         ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
                         TupleT& tuple);


        /*!
         * \brief Prepare entry for a given input item.
         *
         * \tparam DoPrintDefaultValueT If yes, print the default value for each tuple item. If false, print
         * the value from \a input_data
         * \param[in,out] block_per_identifier List of key/default value for input data items.
         * \param[in] input_data Should be nullptr if DoPrintDefaultValueT is yes. Otherwise, object which
         * values are to be printed.
         * \copydoc doxygen_hide_input_data_rewrite_lua_verbosity
         */
        template<print_default_value DoPrintDefaultValueT, ::MoReFEM::Concept::InputDataTypeOrNullopt InputDataT>
        static void PreparePrint(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                                 const Utilities::const_ref_or_nullopt<InputDataT> input_data,
                                 verbosity verbosity_level);

        /*!
         * \brief Extract the list of all keys found in the tuple
         *
         * \param[in,out] list List of keys. The key for the current one is added and next one will be
         * recursively called.
         */
        static void ExtractKeys(std::vector<std::string>& list);

        /*!
         * \brief Skim through the tuple to identify all the leaves that are `IndexedSectionDescription` for indexed sections.
         *
         * \param[in,out] list All the identifiers (as returned by `GetIdentifier()` method) of \a IndexedSectionDescription found.
         * If current element is deemed to be a \a IndexedSectionDescription, it is added to this list.
         */
        static void ExtractIndexedSectionDescriptionKeys(std::vector<std::string>& list);


        /*!
         * \brief Collect statistics about usage of input items.
         *
         * \param[in] tuple Tuple considered.
         * \param[in,out] identifier_list List of all identifiers met, whether they are used or not.
         * Depth is taken into account here, so this list might be bigger than tuple size if sections are
         * involved in the tuple considered.
         * \param[in,out] usage_statistics This vector is the same size as \a identifier_list and is read
         * alongside it; it tells whether the item was used or not.
         */
        static void
        IsUsed(const TupleT& tuple, std::vector<std::string>& identifier_list, std::vector<bool>& usage_statistics);


        /*!
         * \brief Helper that check current key has not yet been used.
         *
         * \param[in,out] keys List of keys found so far. New key will be added if the exception wasn't
         * triggered first.
         */
        static void CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& keys);

        /*!
         * \brief Check whether the \a section_chaining and \a leaf read while parsing the input data file correctly match
         * an expected leaf entry of the \a InputData class,
         *
         * \param[in] section_chaining Section(s) read while parsing the input data file. May encompass several levels
         * separated by '.', e.g. "Section1.SubsectionInSection1". May be empty.
         * \param[in] leaf Leaf read while parsing the input data file, e.g. "LeafInSubSection1"
         *
         * \return Whether the combination of section and leaf match an expected entry in the input data tuple.
         */
        static bool DoMatchIdentifier(std::string_view section_chaining, std::string_view leaf);


        /*!
         * \brief Performs the \a action on the tuple element only if it is a leaf that inherits from \a ParentT.
         *
         * This static method is used to perform the same action over all \a IndexedSection of a given type (for
         * instance all the \a Mesh sections defined in the input data / model settings).
         *
         * \param[in] tuple Tuple considered.
         * \param[in] action Functor applied to the section.
         * \copydoc doxygen_hide_cplusplus_variadic_args
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \tparam ParentT If current leaf inherits from it, trigger \a action upon current tuple item.
         * `MoReFEM::Internal::InputDataNS::FEltSpaceNS::IndexedSectionDescription` in the example above. \a ParentT is
         * expected to be what is the third template argument of \a IndexedSection, which is an empty class used only to
         * tag a specific kind of section. For instance `MoReFEM::Internal::InputDataNS::MeshNS::Tag` to identify a
         * `Mesh` section.
         * \tparam ActionT Type of ``action`.  As `action` is often a lambda, let the compiler figure it out!
         */
        // clang-format off
        template
        <
            class ParentT,
            ::MoReFEM::Concept::InputDataType InputDataT,
            class ActionT,
            typename... Args
        >
        // clang-format on
        static void
        ActIfLeafInheritsFrom(const TupleT& tuple, const InputDataT& input_data, ActionT action, Args&&... args);


        /*!
         *
         * \class doxygen_hide_indexed_section_description_list_extended_arg_description
         *
         * The description consists of:
         * - A general description of the section, which tells end-user what it does represent. This description
         * is set through the \a ModelSettings::SetDescription() method.
         * - The values filled in \a ModelSettings that should be shared with the end-user (for instance in a
         * \a FEltSpace section end-user needs to know what are the unknowns and in which order they are
         * registered to be able to assign to each of them the shape function to use. Not all values defined in
         * \a ModelSettings are thus written in the Lua file, just those that were deemed relevant (internally
         * this is the classes that define the `model_settings_token` alias).
         */
        // Note: param is not put in this Doxygen block as it might be [in] or [in,out] depending on the
        // function it which it is required.


        /*!
         * \brief Prepare the descriptions of all the indexed sections present in the Lua file.
         *
         * \attention This method should only be called upon the \a ModelSettings tuple, as  classes derived from \a
         IndexedSectionDescriptionare supposed
         * to be defined there.
         *
         * This static method is used when a Lua file is created or updated.
         *
         * \param[in] model_settings_tuple Tuple being sifted
         * \param[in,out] out Key is the name of the indexed section, value is the list of all relevant lines
         * as indicated below (first come the general description, then one entry for each \a ModelSettings
         * leaf which value is to be displayed in the generated Lua file.
         *
         * \copydoc doxygen_hide_indexed_section_description_list_extended_arg_description
         *

         */
        static void
        PrepareIndexedIndexedSectionDescriptionList(const TupleT& model_settings_tuple,
                                                    std::unordered_map<std::string, std::deque<std::string>>& out);


        /*!
         * \class doxygen_hide_check_no_missing_indexed_section_description
         *
         * \brief Check all indexed sections get properly the related \a IndexedSectionDescription class.
         *
         * This function (or static method) should be run twice (and is in \a
         * Model::CheckNoMissingIndexedSectionDescriptions() method): once over \a InputData, and another over \a
         * ModelSettings.
         *
         * \internal It is not done in the most efficient way - but most of it is in compile time and anyway it is
         * done once at the beginning of the program: for each \a Leaf, there is a check for its enclosing
         * section (if any). So a given section is checked several times.
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \tparam ModelSettingsT Type of \a model_settings, which is automatically inferred from the argument.
         */

        /*!
         * \copydoc doxygen_hide_check_no_missing_indexed_section_description
         *
         * \param[in] tuple Tuple being investigated - it might be either \a InputData one or \a ModelSettings one.
         * \param[in] indexed_section_list List of \a IndexedSection identified by \a ModelSettings
         * (through `ModelSettings::ExtractIndexedSectionNames()` method). This is given as argument rather than
         * extracted from \a model_settings to avoid recomputing it at each step in the tuple iteration.
         */
        template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
        static void
        CheckNoMissingIndexedSectionDescriptions(const TupleT& tuple,
                                                 const ModelSettingsT& model_settings,
                                                 const std::unordered_set<std::string>& indexed_section_list);


      private:
        /*!
         * \brief Check whether the current tuple element pointed at by \a Index in \a TupleT is a valid one.
         *
         * \returns True if \a tuple_elt_type is not std::false_type.
         */
        static constexpr bool IsValidEltType();

        /*!
         * \brief Check whether the nex tuple element pointed at by \a Index + 1 in \a TupleT is a valid one.
         *
         * \returns True if \a next_item is not std::false_type.
         */
        static constexpr bool IsCurrentEltTheLast();


      private:
        //! Convenient alias.
        using tuple_elt_type = typename Utilities::Tuple::tuple_element_with_fallback<Index, TupleT>::type;

        //! Convenient alias used to explore the content of a \a SectionT recursively
        template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
        using section_enclosed_tuple_type = typename SectionT::section_content_type;

        // clang-format off
        //! \a TupleIteration object used  explore the content of a \a SectionT recursively.
        template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
        using explore_section_tuple =
            TupleIteration
            <
                section_enclosed_tuple_type<SectionT>,
                0UL
            >;
        // clang-format on

        //! Whether the current element is a leaf or a section.
        static constexpr Nature GetNature() noexcept;

        //! Alias to the next element in the tuple, or to std::false_type if current element was the last one.
        // clang-format off
        using next_item =
            std::conditional_t
            <
                Index<std::tuple_size<TupleT>::value - 1,
                TupleIteration<TupleT, Index + 1>,
                std::false_type
            >;
        // clang-format on

    }; // struct TupleIteration


    /*!
     * \brief An helper function for \a TupleIteration::PreparePrint().
     *
     * \tparam DoPrintDefaultValueT If yes, print the default value for each tuple item. If false, print
     * the value from \a input_data
     *
     * \param[in,out] block_per_identifier List of key/default value for input data items.
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_input_data_rewrite_lua_verbosity
     */

    template<print_default_value DoPrintDefaultValueT, ::MoReFEM::Concept::InputDataTypeOrNullopt InputDataT>
    void PreparePrintForLeaf(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                             const Utilities::const_ref_or_nullopt<InputDataT> input_data,
                             verbosity verbosity_level);


    /*!
     * \brief Function that will recursively fill the values read in the input file for each input item.
     *
     *
     * \tparam TupleT Same tuple as the one passed to Base, that specify all relevant input items.
     * \tparam InputDataT Type of \a input_data.
     * \copydoc doxygen_hide_tparam_do_update_lua_file
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     * \param[in] lua_option_file \a OptionFile object.
     * \param[in,out] tuple Should be the data attribute tuple_ nested in Base class.
     */
    // clang-format off
    template
    <
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT,
        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void FillTuple(const ModelSettingsT& model_settings,
                   const InputDataT* input_data,
                   ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
                   TupleT& tuple);


    /*!
     * \brief Prepare the entries to put in the default input file.
     *
     * \tparam InputDataT Type of \a input_data, which holds data the end-user may modify in the Lua file.
     * \tparam ModelSettingsT Type of \a model_settings, which holds data that are specified (and hardcoded) by the
     * author of the model; end-user is not supposed to modify it.
     *
     * \copydoc doxygen_hide_stream_inout
     *
     * \copydoc doxygen_hide_model_settings_arg
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    void PrepareDefaultEntries(std::ostream& stream, const ModelSettingsT& model_settings);


    /*!
     * \brief Print the content of \a input_data onto the \a stream.
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     * \copydoc doxygen_hide_stream_inout
     *
     * \copydoc doxygen_hide_input_data_rewrite_lua_verbosity
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void PrintContent(const ModelSettingsT& model_settings,
                      const InputDataT& input_data,
                      std::ostream& stream,
                      verbosity verbosity_level);


    /*!
     * \brief Helper function to set the value associated to a given leaf.
     *
     * \copydoc doxygen_hide_tparam_do_update_lua_file
     *
     * \param[in,out] leaf Leaf which value is set.
     * \param[in] full_name Name of the key in Lua file, with its possible sections included. E.g.
     * "domain4.unique_id".
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     * \param[in] lua_option_file All the values read from the input data
     * file are stored within this \a OptionFile object. Not constant due to OptionFile interface.
     *
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void SetLeaf(const ModelSettingsT& model_settings,
                 const InputDataT* input_data,
                 const std::string& full_name,
                 LeafT& leaf,
                 ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file);


    /*!
     * \brief Helper function to \a PrepareIndexedIndexedSectionDescriptionList.
     *
     * For a given section, we may want to print in the input data file:
     * - The generic description of the section (which is expected to be detailed in the \a ModelSettings object.
     * For instance we expect here the explanation of what a given \a FEltSpace covers.
     * - Description of some useful values also defined in the \a ModelSettings object, which are deemed
     * useful for the user of the model to fill its Lua file. For instance list of unknowns in a \a FEltSpace:
     * the list of unknowns is defined by the author of the model in \a ModelSettings but the end user needs
     * to know which unknowns are there and in which order to be able to specify for each which shape
     * function should be used.
     *
     * \param[in] key Key of the section considered. Typically something as `FEltSpace*N*` where N is the index
     * of the finite element space considered.
     * \param[in] value Associated value, as a string.
     * \param[in,out] out The object being filled, which for all sections that may be present several times
     * (with an integer template parameter - for instance \a FEltSpace). For each section, associated values are
     * ordered so that main description appears above the explanation of the relevant model settings values.
     *
     * \tparam IsMainIndexedSectionDescriptionT True if we are filling the part extracted from the \a IndexedSectionDescription
     * struct, which provides a generic description of the current object (e.g. explaining what a specific \a FEltSpace
     * aims to describe). If false, il will add information about a specific value filled in \a ModelSettings which is
     * useful for the end user (as explained above for instance nameand ordering of the unknowns involved).
     *
     *
     */
    template<bool IsMainIndexedSectionDescriptionT>
    void AddToIndexedSectionDescription(const std::string& key,
                                        const std::string& value,
                                        std::unordered_map<std::string, std::deque<std::string>>& out);


    /*!
     * \copydoc doxygen_hide_check_no_missing_indexed_section_description
     *
     * \param[in] input_data_or_model_settings Object  being investigated - it might be either
     * a \a InputData one or a \a ModelSettings one. When run for \a ModelSettings it means same argument
     * is provided twice in the function call.
     *
     * \tparam InputDataOrModelSettingsT Type of input_data_or_model_settings. Should be automatically
     * inferred at function call.
     *
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataOrModelSettingsType InputDataOrModelSettingsT
    >
    // clang-format on
    void CheckNoMissingIndexedSectionDescriptions(const ModelSettingsT& model_settings,
                                                  const InputDataOrModelSettingsT& input_data_or_model_settings);


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TUPLEITERATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
