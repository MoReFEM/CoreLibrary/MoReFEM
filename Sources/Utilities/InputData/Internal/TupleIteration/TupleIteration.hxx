// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TUPLEITERATION_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TUPLEITERATION_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/UnorderedMap.hpp"


namespace MoReFEM::Internal::InputDataNS
{


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    constexpr bool TupleIteration<TupleT, Index>::IsValidEltType()
    {
        return !std::is_same<tuple_elt_type, std::false_type>();
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    constexpr bool TupleIteration<TupleT, Index>::IsCurrentEltTheLast()
    {
        return std::is_same<next_item, std::false_type>();
    }


    // ============================
    // Find()
    // ============================

    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<::MoReFEM::Advanced::Concept::InputDataNS::SectionOrLeafType SoughtLeafOrSectionT>
    constexpr bool TupleIteration<TupleT, Index>::Find()
    {
        if constexpr (!IsValidEltType())
            return false;
        else
        {
            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if constexpr (std::is_same<tuple_elt_type, SoughtLeafOrSectionT>())
                    return true;
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                if constexpr (std::is_same<tuple_elt_type, SoughtLeafOrSectionT>())
                    return true;

                using explore_section = explore_section_tuple<tuple_elt_type>;

                if constexpr (explore_section::template Find<SoughtLeafOrSectionT>())
                    return true;
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::template Find<SoughtLeafOrSectionT>();

            return false;
        }
    }


    // ============================
    // ExtractLeaf()
    // ============================


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType SoughtLeafT>
    constexpr Utilities::const_ref_or_nullopt<SoughtLeafT>
    TupleIteration<TupleT, Index>::ExtractLeafHandler(const TupleT& tuple)
    {
        if constexpr (!IsValidEltType())
            return std::nullopt;
        else
        {
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if constexpr (std::is_same<tuple_elt_type, SoughtLeafT>())
                {
                    const auto& item = std::get<Index>(tuple);
                    return item;
                }
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;

                if constexpr (explore_section::template Find<SoughtLeafT>())
                {
                    const auto& current_section = std::get<Index>(tuple);
                    return explore_section::template ExtractLeafHandler<SoughtLeafT>(
                        current_section.GetSectionContent());
                }
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::template ExtractLeafHandler<SoughtLeafT>(tuple);

            return std::nullopt;
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType SoughtLeafT>
    constexpr Utilities::ref_or_nullopt<SoughtLeafT>
    TupleIteration<TupleT, Index>::ExtractModifyableLeafHandler(TupleT& tuple)
    {
        if constexpr (!IsValidEltType())
            return std::nullopt;
        else
        {
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if constexpr (std::is_same<tuple_elt_type, SoughtLeafT>())
                {
                    auto& item = std::get<Index>(tuple);
                    return item;
                }
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;

                if constexpr (explore_section::template Find<SoughtLeafT>())
                {
                    auto& current_section = std::get<Index>(tuple);
                    return explore_section::template ExtractModifyableLeafHandler<SoughtLeafT>(
                        current_section.GetNonCstSectionContent());
                }
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::template ExtractModifyableLeafHandler<SoughtLeafT>(tuple);

            return std::nullopt;
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SoughtSectionT>
    Utilities::const_ref_or_nullopt<SoughtSectionT>
    TupleIteration<TupleT, Index>::ExtractSectionHandler(const TupleT& tuple)
    {
        if constexpr (!IsValidEltType())
            return std::nullopt;
        else
        {
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::section)
            {
                if constexpr (std::is_same<tuple_elt_type, SoughtSectionT>())
                {
                    const auto& item = std::get<Index>(tuple);
                    return item;
                }

                using explore_section = explore_section_tuple<tuple_elt_type>;

                if constexpr (explore_section::template Find<SoughtSectionT>())
                {
                    const auto& current_section = std::get<Index>(tuple);
                    return explore_section::template ExtractSectionHandler<SoughtSectionT>(
                        current_section.GetSectionContent());
                }
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::template ExtractSectionHandler<SoughtSectionT>(tuple);

            return std::nullopt;
        }
    }


    // ============================
    // Fill()
    // ============================


    // clang-format off
    template
    <
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT,
        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void FillTuple(const ModelSettingsT& model_settings,
                   const InputDataT* input_data,
                   ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
                   TupleT& tuple)
    {
        TupleIteration<TupleT, 0UL>::template Fill<DoUpdateLuaFileT>(
            model_settings, input_data, lua_option_file, tuple);
    }


    // clang-format off
    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template
    <
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void TupleIteration<TupleT, Index>::Fill(const ModelSettingsT& model_settings,
                                             const InputDataT* input_data,
                                             ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
                                             TupleT& tuple)
    {
        static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");

        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                // Read in the input data file the whole identifier, which is 'section.name' or 'name'
                // if no section is provided.
                const std::string& full_name = tuple_elt_type::GetIdentifier();
                auto& leaf = std::get<Index>(tuple);

                SetLeaf<tuple_elt_type, DoUpdateLuaFileT>(model_settings, input_data, full_name, leaf, lua_option_file);
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;

                auto& current_section = std::get<Index>(tuple);
                explore_section::template Fill<DoUpdateLuaFileT>(
                    model_settings, input_data, lua_option_file, current_section.GetNonCstSectionContent());
            }

            if constexpr (!IsCurrentEltTheLast())
                next_item::template Fill<DoUpdateLuaFileT>(model_settings, input_data, lua_option_file, tuple);
        }
    }


    // ============================
    // PrepareDefaultEntries()
    // ============================


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    void PrepareDefaultEntries(std::ostream& stream, const ModelSettingsT& model_settings)
    {
        using input_data_tuple = typename InputDataT::underlying_tuple_type;
        std::vector<std::pair<std::string, std::string>> block_per_identifier;

        // Extract all input data from \a TupleT (including those enclosed in sections) and
        // generate their content which is stored in a map.
        TupleIteration<input_data_tuple, 0UL>::template PreparePrint<print_default_value::yes, std::nullopt_t>(
            block_per_identifier, std::nullopt, verbosity::verbose);

        std::unordered_map<std::string, std::deque<std::string>> section_description_list;
        section_description_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

        using model_settings_tuple = typename ModelSettingsT::underlying_tuple_type;

        TupleIteration<model_settings_tuple, 0UL>::PrepareIndexedIndexedSectionDescriptionList(
            model_settings.GetTuple(), section_description_list);

        PrintInFile(block_per_identifier, stream, section_description_list);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void PrintContent(const ModelSettingsT& model_settings,
                      const InputDataT& input_data,
                      std::ostream& stream,
                      verbosity verbosity_level)
    {
        using input_data_tuple = typename InputDataT::underlying_tuple_type;

        std::vector<std::pair<std::string, std::string>> block_per_identifier;

        // Extract all input data from \a TupleT (including those enclosed in sections) and
        // generate their content which is stored in a map.
        TupleIteration<input_data_tuple, 0UL>::template PreparePrint<print_default_value::if_none_found, InputDataT>(
            block_per_identifier, input_data, verbosity_level);

        std::unordered_map<std::string, std::deque<std::string>> section_description_list;
        section_description_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

        using model_settings_tuple = typename ModelSettingsT::underlying_tuple_type;

        TupleIteration<model_settings_tuple, 0UL>::PrepareIndexedIndexedSectionDescriptionList(
            model_settings.GetTuple(), section_description_list);

        PrintInFile(block_per_identifier, stream, section_description_list, verbosity_level);
    }


    template<class TupleEltTypeT>
    void UseDefaultValueIfPossible(std::ostream& oconv)
    {
        if constexpr (requires { TupleEltTypeT::DefaultValue(); })
            oconv << TupleEltTypeT::DefaultValue();
        else
            oconv << "No default value was provided!";
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT,
        print_default_value DoPrintDefaultValueT,
        ::MoReFEM::Concept::InputDataTypeOrNullopt InputDataT
    >
    // clang-format on
    void PreparePrintForLeaf(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                             [[maybe_unused]] const Utilities::const_ref_or_nullopt<InputDataT> input_data,
                             verbosity verbosity_level)
    {
        std::ostringstream oconv;

        const std::string& indent = GetIndentPlaceholder(); // will be replaced later by the correct indentation.
        const auto indent_comment = indent + "-- ";

        constexpr std::size_t max_length = 105;

        if (verbosity_level == verbosity::verbose)
        {
            oconv << "\n";
            oconv << indent_comment;

            if (LeafT::Description().empty())
                oconv << "No description was provided!";
            else
                oconv << Utilities::String::Reformat(LeafT::Description(), max_length, indent_comment);


            oconv << indent_comment << "Expected format: "
                  << Utilities::String::Reformat(Traits::Format<typename LeafT::return_type>::Print(indent_comment),
                                                 max_length,
                                                 indent_comment);

            if constexpr (requires { LeafT::Constraint(); })
                oconv << indent_comment << "Constraint: " << LeafT::Constraint() << '\n';
        }

        oconv << indent << LeafT::NameInFile() << " = ";

        if constexpr (DoPrintDefaultValueT == print_default_value::yes)
        {
            assert(!input_data.has_value());
            UseDefaultValueIfPossible<LeafT>(oconv);
        } else
        {
            assert(input_data.has_value());

            using tuple = typename InputDataT::underlying_tuple_type;
            using tuple_iteration = MoReFEM::Internal::InputDataNS::TupleIteration<tuple, 0>;

            auto optional_value =
                tuple_iteration::template ExtractLeafHandler<LeafT>(input_data.value().get().GetTuple());

            assert(optional_value.has_value());

            auto& value = optional_value.value().get();

            if (value.HasValue())
                Internal::PrintPolicyNS::LuaFormat::Do(oconv, value.GetTheValue());
            else
                UseDefaultValueIfPossible<LeafT>(oconv);
        }

        block_per_identifier.emplace_back(std::make_pair(LeafT::GetIdentifier(), oconv.str()));
    }

    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<print_default_value DoPrintDefaultValueT, ::MoReFEM::Concept::InputDataTypeOrNullopt InputDataT>
    void
    TupleIteration<TupleT, Index>::PreparePrint(std::vector<std::pair<std::string, std::string>>& block_per_identifier,
                                                Utilities::const_ref_or_nullopt<InputDataT> input_data,
                                                verbosity verbosity_level)
    {
        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
                PreparePrintForLeaf<tuple_elt_type, DoPrintDefaultValueT>(
                    block_per_identifier, input_data, verbosity_level);
            else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;
                explore_section::template PreparePrint<DoPrintDefaultValueT>(
                    block_per_identifier, input_data, verbosity_level);
            }

            if constexpr (!IsCurrentEltTheLast())
                next_item::template PreparePrint<DoPrintDefaultValueT>(
                    block_per_identifier, input_data, verbosity_level);
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    void TupleIteration<TupleT, Index>::ExtractKeys(std::vector<std::string>& list)
    {
        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");

            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                list.push_back(tuple_elt_type::GetIdentifier());
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;
                explore_section::ExtractKeys(list);
            }

            if constexpr (!IsCurrentEltTheLast())
                next_item::ExtractKeys(list);
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    void TupleIteration<TupleT, Index>::ExtractIndexedSectionDescriptionKeys(std::vector<std::string>& list)
    {
        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");

            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if constexpr (requires { tuple_elt_type::indexed_section_description_class; })
                    list.push_back(tuple_elt_type::GetIdentifier());
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;
                explore_section::ExtractIndexedSectionDescriptionKeys(list);
            }

            if constexpr (!IsCurrentEltTheLast())
                next_item::ExtractIndexedSectionDescriptionKeys(list);
        }
    }


    // ============================
    // Unused tracking
    // ============================


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    void TupleIteration<TupleT, Index>::IsUsed(const TupleT& tuple,
                                               std::vector<std::string>& identifier_list,
                                               std::vector<bool>& usage_statistics)
    {
        if constexpr (!IsValidEltType())
            return;
        else
        {

            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");

            static_assert(Index < std::tuple_size<TupleT>::value);

            const auto& element = std::get<Index>(tuple);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                identifier_list.push_back(tuple_elt_type::GetIdentifier());
                usage_statistics.push_back(element.IsUsed());
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;
                explore_section::IsUsed(element.GetSectionContent(), identifier_list, usage_statistics);
            }

            if constexpr (!IsCurrentEltTheLast())
                next_item::IsUsed(tuple, identifier_list, usage_statistics);
        }
    }


    // ============================
    // Duplicates.
    // ============================


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    void TupleIteration<TupleT, Index>::CheckNoDuplicateKeysInTuple(std::unordered_set<std::string>& known_keys)
    {
        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");

            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                decltype(auto) current_key = tuple_elt_type::GetIdentifier();

                if (known_keys.find(current_key) != known_keys.cend())
                    throw ::MoReFEM::InputDataNS::ExceptionNS::DuplicateInTuple(current_key);

                known_keys.insert(current_key);
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;
                explore_section::CheckNoDuplicateKeysInTuple(known_keys);
            }

            if constexpr (!IsCurrentEltTheLast())
                next_item::CheckNoDuplicateKeysInTuple(known_keys);
        }
    }


    // ============================
    // Lookup functions.
    // ============================


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    bool TupleIteration<TupleT, Index>::DoMatchIdentifier(std::string_view section_chaining, std::string_view leaf)
    {
        if constexpr (!IsValidEltType())
            return false;
        else
        {
            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");

            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if (leaf == tuple_elt_type::NameInFile())
                {
                    if constexpr (std::is_same<typename tuple_elt_type::enclosing_section_type,
                                               ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>())
                    {
                        if (section_chaining.empty())
                            return true;
                    } else
                    {
                        if (section_chaining == tuple_elt_type::enclosing_section_type::GetFullName())
                            return true;
                    }
                }
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;
                if (explore_section::DoMatchIdentifier(section_chaining, leaf))
                    return true;
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::DoMatchIdentifier(section_chaining, leaf);

            return false;
        }
    }


    // ============================
    // Whether section or leaf is considered.
    // ============================

    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    constexpr Nature TupleIteration<TupleT, Index>::GetNature() noexcept
    {
        if constexpr (!IsValidEltType())
            return Nature::undefined;
        else
            return tuple_elt_type::GetNature();
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    // clang-format off
    template
    <
        class ParentT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        class ActionT,
        typename... Args
    >
    // clang-format on
    void TupleIteration<TupleT, Index>::ActIfLeafInheritsFrom(const TupleT& tuple,
                                                              const InputDataT& input_data,
                                                              ActionT action,
                                                              Args&&... args)
    {
        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if constexpr (std::is_base_of_v<ParentT, tuple_elt_type>)
                {
                    auto& leaf = std::get<Index>(tuple);
                    action(leaf);
                }
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);
                auto& section = std::get<Index>(tuple);
                using explore_section = explore_section_tuple<tuple_elt_type>;
                explore_section::template ActIfLeafInheritsFrom<ParentT>(
                    section.GetSectionContent(), input_data, action, std::forward<Args>(args)...);
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::template ActIfLeafInheritsFrom<ParentT>(
                    tuple, input_data, action, std::forward<Args>(args)...);
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void SetLeaf(const ModelSettingsT& model_settings,
                 [[maybe_unused]] const InputDataT* input_data,
                 const std::string& full_name,
                 LeafT& element,
                 ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file)
    {
        using return_type = typename LeafT::return_type;

        {
            return_type ret;

            constexpr bool allow_missing_entry =
                DoUpdateLuaFileT == ::MoReFEM::InputDataNS::do_update_lua_file::yes ? true : false;

            bool was_properly_read{ false };

            std::string constraint{};

            if constexpr (requires { LeafT::Constraint(); })
                constraint = LeafT::Constraint();

            if constexpr (requires(LeafT& leaf) { leaf.Selector(model_settings, input_data); })
            {
                was_properly_read = lua_option_file.Read<return_type, allow_missing_entry>(
                    full_name, constraint, ret, LeafT::Selector(model_settings, input_data));
            } else
            {
                was_properly_read =
                    lua_option_file.Read<return_type, allow_missing_entry>(full_name, constraint, ret, nullptr);
            }

            if (was_properly_read)
                element.SetValue(ret);
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    constexpr std::size_t TupleIteration<TupleT, Index>::Nleaves()
    {
        if constexpr (!IsValidEltType())
            return 0UL;
        else
        {
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if constexpr (!IsCurrentEltTheLast())
                    return 1UL + next_item::Nleaves();

                return 1UL;
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;

                if constexpr (!IsCurrentEltTheLast())
                    return explore_section::Nleaves() + next_item::Nleaves();

                return explore_section::Nleaves();
            }
        }
    }


    template<bool IsMainIndexedSectionDescriptionT>
    void AddToIndexedSectionDescription(const std::string& key,
                                        const std::string& value,
                                        std::unordered_map<std::string, std::deque<std::string>>& out)
    {
        auto it = out.find(key);

        if (it != out.end())
        {
            if constexpr (IsMainIndexedSectionDescriptionT)
                it->second.push_front(value);
            else
                it->second.push_back(value);
        } else
        {
            [[maybe_unused]] auto [it2, is_correctly_inserted] = out.insert({ key, { value } });

            assert(is_correctly_inserted
                   && "Each identifier is meant to be present only once - "
                      "if not a compilation error should underline it.");
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    void TupleIteration<TupleT, Index>::PrepareIndexedIndexedSectionDescriptionList(
        const TupleT& tuple,
        std::unordered_map<std::string, std::deque<std::string>>& out)
    {
        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if constexpr (requires { tuple_elt_type::indexed_section_description_class; })
                {
                    Utilities::const_ref_or_nullopt<tuple_elt_type> optional_item = std::get<Index>(tuple);
                    assert(optional_item.has_value());
                    const auto& item = optional_item.value().get();
                    const auto& key = tuple_elt_type::enclosing_section_type::GetIdentifier();
                    const auto& value = item.GetTheValue();

                    AddToIndexedSectionDescription<true>(key, value, out);
                } else if constexpr (requires { typename tuple_elt_type::model_settings_token; })
                {
                    Utilities::const_ref_or_nullopt<tuple_elt_type> optional_item = std::get<Index>(tuple);
                    assert(optional_item.has_value());
                    const auto& item = optional_item.value().get();
                    const auto& key = tuple_elt_type::model_settings_token::enclosing_section_type::GetIdentifier();

                    std::ostringstream oconv;
                    oconv << tuple_elt_type::NameInFile() << ": ";
                    Internal::PrintPolicyNS::LuaFormat::Do(oconv, item.GetTheValue());
                    const auto value = oconv.str();
                    AddToIndexedSectionDescription<false>(key, value, out);
                }
            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                using explore_section = explore_section_tuple<tuple_elt_type>;
                const auto& current_section = std::get<Index>(tuple);
                explore_section::PrepareIndexedIndexedSectionDescriptionList(current_section.GetSectionContent(), out);
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::PrepareIndexedIndexedSectionDescriptionList(tuple, out);
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    void TupleIteration<TupleT, Index>::CheckNoMissingIndexedSectionDescriptions(
        const TupleT& tuple,
        const ModelSettingsT& model_settings,
        const std::unordered_set<std::string>& indexed_section_list)
    {
        assert("It is given as argument only to avoir recomputing it at each step of the recursion"
               && indexed_section_list == model_settings.ExtractIndexedSectionNames());

        if constexpr (!IsValidEltType())
            return;
        else
        {
            static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value, "TupleT should be a std::tuple.");
            static_assert(Index < std::tuple_size<TupleT>::value);

            if constexpr (tuple_elt_type::GetNature() == Nature::leaf)
            {
                if (!std::is_same<typename tuple_elt_type::enclosing_section_type,
                                  ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>())
                {
                    if constexpr (requires { tuple_elt_type::enclosing_section_type::is_indexed_section; })
                    {

                        if (indexed_section_list.find(tuple_elt_type::enclosing_section_type::GetFullName())
                            == indexed_section_list.cend())
                            throw ::MoReFEM::InputDataNS::ExceptionNS::
                                MissingIndexedSectionDescriptionInModelSettingsTuple(
                                    tuple_elt_type::enclosing_section_type::GetFullName());
                    }
                }


            } else
            {
                static_assert(tuple_elt_type::GetNature() == Nature::section);

                if constexpr (requires { tuple_elt_type::is_indexed_section; })
                {
                    if (indexed_section_list.find(tuple_elt_type::GetFullName()) == indexed_section_list.cend())
                        throw ::MoReFEM::InputDataNS::ExceptionNS::MissingIndexedSectionDescriptionInModelSettingsTuple(
                            tuple_elt_type::GetFullName());
                }


                using explore_section = explore_section_tuple<tuple_elt_type>;

                const auto& current_section = std::get<Index>(tuple);
                explore_section::CheckNoMissingIndexedSectionDescriptions(
                    current_section.GetSectionContent(), model_settings, indexed_section_list);
            }

            if constexpr (!IsCurrentEltTheLast())
                return next_item::CheckNoMissingIndexedSectionDescriptions(tuple, model_settings, indexed_section_list);
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataOrModelSettingsType InputDataOrModelSettingsT
    >
    // clang-format on
    void CheckNoMissingIndexedSectionDescriptions(const ModelSettingsT& model_settings,
                                                  const InputDataOrModelSettingsT& input_data_or_model_settings)
    {
        using input_data_tuple_iteration =
            Internal::InputDataNS::TupleIteration<typename InputDataOrModelSettingsT::underlying_tuple_type, 0UL>;

        const auto indexed_section_list = model_settings.ExtractIndexedSectionNames();

        input_data_tuple_iteration::CheckNoMissingIndexedSectionDescriptions(
            input_data_or_model_settings.GetTuple(), model_settings, indexed_section_list);
    }


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TUPLEITERATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
