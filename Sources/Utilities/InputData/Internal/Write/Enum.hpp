// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_ENUM_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::InputDataNS
{

    //! Enum class to tell how to rewrite the input lua files.
    //! In verbose mode, description, constraints and so on are written.
    //! In compact mode, comments are skipped and there are less newlines.
    enum class verbosity { verbose, compact };

    /*!
     * \class doxygen_hide_input_data_rewrite_lua_verbosity
     *
     * \param[in] verbosity_level In verbose mode, description, constraints and so on are written.
     * In compact mode, comments are skipped and there are less newlines.
     */


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
