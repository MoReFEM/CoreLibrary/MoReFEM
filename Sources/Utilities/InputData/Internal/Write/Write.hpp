// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_WRITE_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_WRITE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"


namespace MoReFEM::Internal::InputDataNS
{


    /*!
     * \brief Create a default input file, with all relevant input data provided in \a InputDataT.
     *
     * If a default parameter is provided in InputData class, it will be written in the file, otherwise
     * a filler text will be written to remind it should be filled.
     *
     * \tparam InputDataT Type of \a input_data, which holds data the end-user may modify in the Lua file.
     * \tparam ModelSettingsT Type of \a model_settings, which holds data that are specified (and hardcoded) by the
     * author of the model; end-user is not supposed to modify it.
     *
     * \param[in] path Path to the file that will contain the result. An exception is thrown if the path is
     * invalid.
     * \param[in] model_settings ModelSettings instance
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    void CreateDefaultInputFile(const ::MoReFEM::FilesystemNS::File& path, const ModelSettingsT& model_settings);


    /*!
     * \brief Write in a file the content of \a input_data.
     *
     * \tparam InputDataT The object which holds the information about input data file content.
     *
     * \param[in] target Path to the file into which the data will be written in the proper format (so it might
     * be reused as input for a MoReFEM model). The file must not exist yet but the directory in which it is
     * located must already exist.
     *
     * \copydoc doxygen_hide_model_settings_input_data_arg
     *
     * \copydoc doxygen_hide_input_data_rewrite_lua_verbosity
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT

    >
    // clang-format on
    void Write(const ModelSettingsT& model_settings,
               const InputDataT& input_data,
               const ::MoReFEM::FilesystemNS::File& target,
               verbosity verbosity_level = verbosity::verbose);


    /*!
     * \brief Rewrite the input data file that was used to load the input data in the first place.
     *
     * Doing so might be useful to make sure the comments are up-to-date: if the documentation of the fields has changed
     * you will get the latest version this way. It will also trim fields that were in fact not used in the model.
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     * \copydoc doxygen_hide_input_data_rewrite_lua_verbosity
     */
    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void RewriteInputDataFile(const ModelSettingsT& model_settings,
                              const InputDataT& input_data,
                              verbosity verbosity_level = verbosity::verbose);


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/Internal/Write/Write.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_WRITE_DOT_HPP_
// *** MoReFEM end header guards *** < //
