// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_WRITE_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_WRITE_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Internal/Write/Write.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <fstream>

#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
#include "Utilities/InputData/Internal/Write/Enum.hpp"


namespace MoReFEM::Internal::InputDataNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    void CreateDefaultInputFile(const ::MoReFEM::FilesystemNS::File& file, const ModelSettingsT& model_settings)
    {
        std::ofstream out{ file.NewContent() };

        out << "-- Comment lines are introduced by \"--\".\n";
        out << "-- In a section (i.e. within braces), all entries must be separated by a comma.\n\n";

        PrepareDefaultEntries<InputDataT>(out, model_settings);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT

    >
    // clang-format on
    void Write(const ModelSettingsT& model_settings,
               const InputDataT& input_data,
               const ::MoReFEM::FilesystemNS::File& file,
               verbosity verbosity_level)
    {
        std::ofstream out{ file.NewContent() };

        if (verbosity_level == verbosity::verbose)
        {
            out << "-- Comment lines are introduced by \"--\".\n";
            out << "-- In a section (i.e. within braces), all entries must be separated by a comma.\n\n";
        }

        PrintContent(model_settings, input_data, out, verbosity_level);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void
    RewriteInputDataFile(const ModelSettingsT& model_settings, const InputDataT& input_data, verbosity verbosity_level)
    {
        decltype(auto) original_file = input_data.GetInputFile();

        auto backup_path = original_file.GetDirectoryEntry().path();
        backup_path.replace_extension(".lua.bak");

        auto backup_file = ::MoReFEM::FilesystemNS::File{ std::move(backup_path) };

        ::MoReFEM::FilesystemNS::Copy(original_file,
                                      backup_file,
                                      ::MoReFEM::FilesystemNS::fail_if_already_exist::no,
                                      ::MoReFEM::FilesystemNS::autocopy::no,
                                      std::source_location::current());

        try
        {
            original_file.Remove();

            Internal::InputDataNS::Write(model_settings, input_data, original_file, verbosity_level);
        }
        catch (const Exception& e)
        {
            std::cerr << "Exception caught: " << e.what() << std::endl;
            std::cerr << "File " << original_file
                      << " will be put back in its original state and exception will be"
                         " thrown again."
                      << std::endl;

            ::MoReFEM::FilesystemNS::Copy(backup_file,
                                          original_file,
                                          ::MoReFEM::FilesystemNS::fail_if_already_exist::no,
                                          ::MoReFEM::FilesystemNS::autocopy::no,
                                          std::source_location::current());

            throw;
        }

        std::cout << "File " << original_file
                  << " has been updated; a copy of the former version has been made "
                     "(with an additional .bak extension). Please review it before validating it! "
                  << std::endl;
        std::cout
            << "It is possible that some fields have disappeared: if there was in the data file a field which "
               "was not considered at all (i.e. had no match in the InputData tuple) they were in fact unused and were "
               "dropped here."
            << std::endl;
    }


} // namespace MoReFEM::Internal::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_WRITE_WRITE_DOT_HXX_
// *** MoReFEM end header guards *** < //
