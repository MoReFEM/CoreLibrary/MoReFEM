// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_MODELSETTINGS_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_MODELSETTINGS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef>
#include <memory>
#include <string>
#include <unordered_set>
#include <vector>

// IWYU pragma: begin_exports
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: keep
// IWYU pragma: end_exports

#include "Utilities/Containers/Tuple/Concept.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp" // IWYU pragma: export
#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"


namespace MoReFEM
{


    /*!
     * \brief Class which purpose is to store model-related data that are intended to be set exclusively
     * by the author of the model.
     *
     * \copydoc doxygen_hide_model_settings_input_data
     */
    template<Concept::Tuple TupleT>
    class ModelSettings : public Internal::InputDataNS::AbstractClass<TupleT>
    {
      public:
        //! Helper variable to define the \a ModelSettingsType concept.
        static inline constexpr bool ConceptIsModelSettings = true;

      public:
        //! Self.
        using self = ModelSettings;

        //! Parent
        using parent = Internal::InputDataNS::AbstractClass<TupleT>;

        //! Alias to smart pointers storing objects.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! The underlying tuple type.
        using underlying_tuple_type = TupleT;

        //! Size of the tuple.
        static constexpr std::size_t Size();

        //! Special members.
        //@{

        /*!
         * \brief Constructor.
         *
         */
        explicit ModelSettings();

        //! \copydoc doxygen_hide_copy_constructor
        ModelSettings(const ModelSettings& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ModelSettings(ModelSettings&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ModelSettings& operator=(const ModelSettings& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ModelSettings& operator=(ModelSettings&& rhs) = default;


        //! Destructor.
        virtual ~ModelSettings() override;
        //@}

        /*!
         * \brief Fill the value related to the leaf \a LeafT.
         *
         * \tparam LeafT Leaf for which the value is set. An exception is thrown if \a LeafT is not in \a TupleT.
         *
         * \param[in] value Value to be put in the tuple. Its type is defined in \a LeafT.
         */
        template<Advanced::Concept::InputDataNS::LeafType LeafT>
        void Add(typename LeafT::return_type&& value);

        /*!
         * \brief Specify the description for a given \a IndexedSection.
         *
         * The description will appear above the section in the input data file (if some leaves are still handled
         * there and thus modifiable by the end-user).
         *
         * Current function is essentially syntactic sugar over \a Add() method concerning the \a
         * IndexedSectionDescription special leaf of the \a IndexedSection.
         *
         * \param[in] description Description of the specific instance of \a SectionT, which will
         * appear as such before the section within the Lua file (if it is automatically generated or updated
         * through the specific \a UpdateLuaFile executable for your model).
         */
        template<Advanced::Concept::InputDataNS::IndexedSectionType SectionT>
        void SetDescription(std::string&& description);


        /*!
         * \brief Check all the values in the tuple have been properly filled.
         *
         * This method must be add explicitly - I'm not proud of it but baring introducing an `Init()` method which
         * is not much better I have not better way to do so.
         */
        void CheckTupleCompletelyFilled() const;

        //! Virtual method to be overriden in child classes. In this method the content of the values of the tuple must
        //! be filled.
        virtual void Init() = 0;

        /*!
         * \brief Extract from the tuple the names of the indexed sections.
         *
         * \return List of the full names (i.e. with enclosing section name present as well) with '.' as separator)
         * of indexed sections.
         *
         * \internal The way to tag these is to use the special leaves than inherits from `IndexedSectionDescription` CRTP. If not
         * they won't be found properly - but there are safeties in the code to pinpoint this to the developer of a
         * model (see \a Model::CheckNoMissingIndexedSectionDescriptions()).
         *
         * Current method is public but isn't intended to be used publicly - in other words if you're not a developer
         * of the main library you shouldn't bother.
         */
        std::unordered_set<std::string> ExtractIndexedSectionNames() const;

      private:
        //! Accessor to the list of identifiers that have been filled with `Add()` method.
        //! This is a purely internal data attribute that shouldn't be exposed publicly.
        const std::vector<std::string>& GetListIdentifiersProperlySet() const;

        //! Non constant accessor to the list of identifiers that have been filled with `Add()` method.
        //! This is a purely internal data attribute that shouldn't be exposed publicly.
        std::vector<std::string>& GetNonCstListIdentifiersProperlySet();


      private:
        //! Helper object to iterate upon the tuple (including the in-depth sections).
        using tuple_iteration = Internal::InputDataNS::TupleIteration<underlying_tuple_type, 0UL>;

        //! List of identifiers that have been filled with `Add()` method.
        //! This is a purely internal data attribute that shouldn't be exposed publicly.
        std::vector<std::string> list_identifiers_properly_set_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/ModelSettings.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_MODELSETTINGS_DOT_HPP_
// *** MoReFEM end header guards *** < //
