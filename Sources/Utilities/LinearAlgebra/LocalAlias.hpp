// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_LOCALALIAS_DOT_HPP_
#define MOREFEM_UTILITIES_LINEARALGEBRA_LOCALALIAS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_deprecated_local_linalg_alias
     *
     * Up to summer 2024, there were convenient aliases to designate local matrix and vector.
     *
     * However, when we switched from Xtensor to Eigen we leverage the latter's facilities to delimit more finely
     * matrices, for instance by telling when we know maximum size a matrix or a vector can reach.
     * So it makes little sense with that to keep providing, especially considering existing types in Eigen are not
     * that hard to type and convey more information.
     *
     * The aliases are kept only for backward compatibility, but I do not advise you keep using them.
     */

    //! Convenient alias for the type used in MoReFEM for local vectors.
    //! \copydoc doxygen_hide_deprecated_local_linalg_alias
    using LocalVector = Eigen::VectorXd;

    //! Convenient alias for the type used most of the time in MoReFEM for local matrices.
    //! \copydoc doxygen_hide_deprecated_local_linalg_alias
    using LocalMatrix = Eigen::MatrixXd;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_LOCALALIAS_DOT_HPP_
// *** MoReFEM end header guards *** < //
