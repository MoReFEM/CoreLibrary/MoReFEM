// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_SPARSEMATRIX_CSRPATTERN_DOT_HPP_
#define MOREFEM_UTILITIES_LINEARALGEBRA_SPARSEMATRIX_CSRPATTERN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Exceptions/Exception.hpp"     // IWYU pragma: export
#include "Utilities/LinearAlgebra/StrongType.hpp" // IWYU pragma: export


namespace MoReFEM::Utilities
{


    /*!
     * \brief Objects that store information about the CSR pattern of a sparse matrix.
     *
     * To put in a nutshell, iCSR tells for each line how much non zero values there are, and jCSR indicates
     * their position.
     *
     * Wikipedia explanation is very enlightning:
     * https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_row_.28CSR.2C_CRS_or_Yale_format.29
     */
    template<typename T>
    class CSRPattern final
    {
      public:
        /// \name Constructors and destructor.
        ///@{

        //! Empty constructor.
        CSRPattern() = default;

        /*!
         * \brief Constructor with CSR pattern already calculated.
         *
         * \param[in] iCSR iCSR vector (see wikipedia link given in class general documentation).
         * \param[in] jCSR jCSR vector (see wikipedia link given in class general documentation).
         */
        explicit CSRPattern(std::vector<T>&& iCSR, std::vector<T>&& jCSR);

        //! \copydoc doxygen_hide_copy_constructor
        CSRPattern(const CSRPattern& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        CSRPattern(CSRPattern&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        CSRPattern& operator=(const CSRPattern& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        CSRPattern& operator=(CSRPattern&& rhs) = default;

        //! Destructor.
        ~CSRPattern() = default;
        ///@}


        //! Const accessor to iCSR.
        const std::vector<T>& iCSR() const;

        //! Const accessor to iCSR.
        const std::vector<T>& jCSR() const;

        /*!
         * \brief Non-const accessor to iCSR.
         *
         * \return Reference to internal storage of iCSR.
         *
         * \internal <b><tt>[internal]</tt></b> I don't like providing such a direct accessor, but Parmetis
         * interface for instance does not specify constness of arguments... Hence the name that is not iCSR(): user
         * can't provide direct accessor without his knowledge. \endinternal
         */
        std::vector<T>& NonCstiCSR();

        /*!
         * \brief Non-const accessor to jCSR.
         *
         * \return Reference to internal storage of jCSR.
         *
         * \internal <b><tt>[internal]</tt></b> I don't like providing such a direct accessor, but Parmetis
         * interface for instance does not specify constness of arguments... Hence the name that is not ijSR(): user
         * can't provide direct accessor without his knowledge. \endinternal
         */
        std::vector<T>& NonCstjCSR();


        //! Number of elements in iCSR.
        std::size_t NiCSR() const;

        //! Number of rows (NiCSR - 1 by construct).
        small_matrix_row_index_type Nrow() const;

        //! Number of elements in jCSR.
        std::size_t NjCSR() const;

        //! Returns the value of the \a i -th element in iCSR (with C numbering beginning at 0).
        //! \param[in] i Index to the element considered within the iCSR vector.
        T iCSR(std::size_t i) const;

        //! Returns the value of the \a j -th element in jCSR (with C numbering beginning at 0).
        //! \param[in] j Index to the element considered within the jCSR vector.
        T jCSR(std::size_t j) const;

        //! Returns the total numbers of non-zero terms per row.
        template<typename IntT>
        std::vector<IntT> NnonZeroTermsPerRow() const;


        /*!
         * \brief Returns the numbers of non-zero terms per row, separating those that are on the
         * same processor as the row index and those that are not.
         *
         * We will use Petsc denomination for it:
         * Diagonal terms for the ones on the same processor as the row index (diagonal because the submatrix is
         * on the diagonal).
         * Off-diagonal terms if column index is not on the same processor.
         *
         * \param[in] is_on_local_proc Function that tells whether a given index is on the local processor or not.
         * \param[in] Ndiagonal_non_zero Number of non-zero diagonal terms.
         * \param[in] Noff_diagonal_non_zero Number of non-zero off-diagonal terms.
         */
        template<typename IntT>
        void NnonZeroTermsPerRow(std::function<bool(std::size_t)> is_on_local_proc,
                                 std::vector<IntT>& Ndiagonal_non_zero,
                                 std::vector<IntT>& Noff_diagonal_non_zero) const;


      private:
        //! Check the consistency of the given iCSR_ and jCSR_.
        void CheckConsistency() const;


      private:
        //! Storage of iCSR vector.
        std::vector<T> iCSR_;

        //! Storage of jCSR vector.
        std::vector<T> jCSR_;
    };


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/LinearAlgebra/SparseMatrix/CSRPattern.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_SPARSEMATRIX_CSRPATTERN_DOT_HPP_
// *** MoReFEM end header guards *** < //
