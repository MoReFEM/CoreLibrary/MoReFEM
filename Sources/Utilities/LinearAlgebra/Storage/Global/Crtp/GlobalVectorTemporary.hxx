// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_CRTP_GLOBALVECTORTEMPORARY_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_CRTP_GLOBALVECTORTEMPORARY_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Global/Crtp/GlobalVectorTemporary.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::GlobalLinearAlgebraNS
{


    template<class DerivedT, class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
    void GlobalVectorTemporary<DerivedT, GlobalVectorT, N, IdentifierT>::InitGlobalVectorTemporary(
        const GlobalVectorT& model)
    {
        storage_ = std::make_unique<global_vector_temporary_manager_storage>(model);
    }


    template<class DerivedT, class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
    inline typename GlobalVectorTemporary<DerivedT, GlobalVectorT, N, IdentifierT>::
        global_vector_temporary_manager_storage&
        GlobalVectorTemporary<DerivedT, GlobalVectorT, N, IdentifierT>::GetNonCstTemporaryGlobalVectorManager(
            [[maybe_unused]] typename std::integral_constant<std::size_t, IdentifierT>::type placeholder) const noexcept
    {
        assert(!(!storage_) && "Make sure InitGlobalVectorTemporary() was properly called!");
        return *storage_;
    }


} // namespace MoReFEM::GlobalLinearAlgebraNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_CRTP_GLOBALVECTORTEMPORARY_DOT_HXX_
// *** MoReFEM end header guards *** < //
