// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYACCESS_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYACCESS_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Global/Internal/GlobalVectorTemporaryAccess.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::GlobalLinearAlgebraNS
{


    template<class WorkVariableT>
    template<class DataT>
    GlobalVectorTemporaryAccess<WorkVariableT>::GlobalVectorTemporaryAccess(const DataT& data)
    : GlobalVectorTemporaryAccess<WorkVariableT>(
          data.GetNonCstTemporaryGlobalVectorManager(typename WorkVariableT::identifier_type()))
    { }


    template<class WorkVariableT>
    GlobalVectorTemporaryAccess<WorkVariableT>::GlobalVectorTemporaryAccess(WorkVariableT& work_variable)
    : work_variable_(work_variable), vector_index_(work_variable.DeterminedUnusedIndex())
    {
        work_variable_.usage_.set(vector_index_);
    }


    template<class WorkVariableT>
    GlobalVectorTemporaryAccess<WorkVariableT>::~GlobalVectorTemporaryAccess()
    {
        work_variable_.usage_.reset(vector_index_);
    }


    template<class WorkVariableT>
    typename WorkVariableT::vector_type& GlobalVectorTemporaryAccess<WorkVariableT>::GetNonCstVector() noexcept
    {
        assert(!(!work_variable_.storage_[vector_index_]));

        return *work_variable_.storage_[vector_index_];
    }


} // namespace MoReFEM::Internal::GlobalLinearAlgebraNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYACCESS_DOT_HXX_
// *** MoReFEM end header guards *** < //
