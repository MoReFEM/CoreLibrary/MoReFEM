// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYSTORAGE_DOT_HPP_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <bitset>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::GlobalLinearAlgebraNS
{
    template<class GlobalVectorTemporaryStorageT>
    class GlobalVectorTemporaryAccess;
}

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GlobalLinearAlgebraNS
{


    /*!
     * \brief Class which actually stores the \a GlobalVector needed in GlobalVectorTemporary CRTP.
     *
     * \copydetails doxygen_hide_global_vector_temporary_manager_class
     */
    template<class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
    class GlobalVectorTemporaryStorage
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GlobalVectorTemporaryStorage<GlobalVectorT, N, IdentifierT>;

        //! Alias to unique ptr.
        using unique_ptr = std::unique_ptr<self>;

        //! Friendship to the class that provides the RAII access.
        friend GlobalVectorTemporaryAccess<self>;

        //! Alias to \a GlobalVectorT.
        using vector_type = GlobalVectorT;

        //! Alias to a type that help disambiguate some method calls.
        using identifier_type = typename std::integral_constant<std::size_t, IdentifierT>::type;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] model Work vector will be built from this \a GlobalVector: it will retain same
         * \a NumberingSubset and \a GodOfDof.
         */
        explicit GlobalVectorTemporaryStorage(const GlobalVectorT& model);

        //! Destructor.
        ~GlobalVectorTemporaryStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalVectorTemporaryStorage(const GlobalVectorTemporaryStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalVectorTemporaryStorage(GlobalVectorTemporaryStorage&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalVectorTemporaryStorage& operator=(const GlobalVectorTemporaryStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalVectorTemporaryStorage& operator=(GlobalVectorTemporaryStorage&& rhs) = delete;

        ///@}


      private:
        /*!
         * \brief Determine an index that points to a \a GlobalVector currently unused.
         *
         * \return Index that will provide a currently unused \a GlobalVector (it's up to
         * GlobalVectorTemporaryAccess to handle it).
         */
        std::size_t DeterminedUnusedIndex() noexcept;


      private:
        //! Storage.
        typename GlobalVectorT::template array_unique_ptr<N> storage_;

        //! Holds a 1 if the value is currently used somewhere. Released when
        std::bitset<N> usage_;
    };


} // namespace MoReFEM::Internal::GlobalLinearAlgebraNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/LinearAlgebra/Storage/Global/Internal/GlobalVectorTemporaryStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
