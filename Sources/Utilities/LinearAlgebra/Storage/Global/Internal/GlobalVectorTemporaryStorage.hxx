// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYSTORAGE_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYSTORAGE_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Global/Internal/GlobalVectorTemporaryStorage.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::GlobalLinearAlgebraNS
{


    template<class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
    GlobalVectorTemporaryStorage<GlobalVectorT, N, IdentifierT>::GlobalVectorTemporaryStorage(
        const GlobalVectorT& model)
    {
        for (auto i = 0UL; i < N; ++i)
            storage_[i] = std::make_unique<GlobalVectorT>(model);

        usage_.reset();
    }


    template<class GlobalVectorT, std::size_t N, std::size_t IdentifierT>
    std::size_t GlobalVectorTemporaryStorage<GlobalVectorT, N, IdentifierT>::DeterminedUnusedIndex() noexcept
    {
        // Look for an unused vector.
        std::size_t unused_vector_index = 0;

        assert(!usage_.all()
               && "If this assert shows up, it means you are using at once all availables "
                  "work vectors. Consider incrementing the template parameter N, or check you release "
                  "correctly the vectors after usage! (release is automatic as soon as RAII "
                  "AccessGlobalVectorTemporaryStorage "
                  "goes out of scope).");

        while (usage_[unused_vector_index])
            ++unused_vector_index;

        return unused_vector_index;
    }


} // namespace MoReFEM::Internal::GlobalLinearAlgebraNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_GLOBAL_INTERNAL_GLOBALVECTORTEMPORARYSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
