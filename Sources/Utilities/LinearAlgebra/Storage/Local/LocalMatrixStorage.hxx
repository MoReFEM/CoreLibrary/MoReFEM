// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Crtp
{


    template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
    constexpr std::size_t LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::Size()
    {
        return NlocalMatricesT;
    }


    template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
    void LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::InitLocalMatrixStorage(
        const std::array<std::pair<Eigen::Index, Eigen::Index>, NlocalMatricesT>& matrices_dimension)
    {
        for (std::size_t i = 0UL; i < NlocalMatricesT; ++i)
        {
            auto& matrix = matrix_list_[i];
            matrix.resize(matrices_dimension[i].first, matrices_dimension[i].second);
            matrix.setZero();
        }
    }


    template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
    template<std::size_t IndexT>
    inline auto LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::GetLocalMatrix() const noexcept
        -> LocalMatrixT&
    {
        static_assert(IndexT < NlocalMatricesT, "Check index is within bounds!");
        return matrix_list_[IndexT];
    }


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
