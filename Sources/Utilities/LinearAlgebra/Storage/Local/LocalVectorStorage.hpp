// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HPP_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::Crtp
{


    /*!
     * \brief CRTP to give access to \a NlocalVectorT local vectors.
     *
     * \tparam DerivedT Name of the base class for which the CRTP is deployed.
     * \tparam NlocalVectorT Number of local vectors to add.
     *
     * It is advised to declare in \a DerivedT an enum class to tag the local vectors, e.g.:
     * \code
     *  enum class LocalVectorIndex : std::size_t
     *  {
     *      new_contribution = 0,
     *      local_rhs,
     *      dW,
     *      ...
     *  };
     * \endcode
     */

    template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT = Eigen::VectorXd>
    class LocalVectorStorage
    {

      public:
        //! Return the number of local vectors.
        //! \return Number of local vectors.
        static constexpr std::size_t Size();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * Does nothing: the computation of the dimension in the constructor of DerivedT proved to be unreadable.
         * So InitLocalVectorStorage() must absolutely be called to set appropriately the dimensions.
         */
        explicit LocalVectorStorage() = default;


      protected:
        //! Destructor.
        ~LocalVectorStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LocalVectorStorage(const LocalVectorStorage& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        LocalVectorStorage(LocalVectorStorage&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        LocalVectorStorage& operator=(const LocalVectorStorage& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        LocalVectorStorage& operator=(LocalVectorStorage&& rhs) = default;


        ///@}


      public:
        /*!
         * \brief This method must be called in the constructor of DerivedT, once the dimensions have been
         * computed.
         *
         * For safety reasons, vector is filled with 0 to avoid undefined behaviour if no value is actually given.
         *
         * \param[in] vectors_dimension For each \a LocalVectorT stored this way, the integer is its size.
         */
        void InitLocalVectorStorage(const std::array<Eigen::Index, NlocalVectorT>& vectors_dimension);

        /*!
         * \brief Access to the \a IndexT -th local vector.
         *
         * \return Access to the \a IndexT -th local vector.
         *
         * The idea is to use it as such:
         * \code
         * auto& local_vector = vector_parent::template GetLocalVector<0>();
         * ... (use local_vector variable in the following)
         * \endcode
         *
         * \internal <b><tt>[internal]</tt></b> This method is const because we might want to use local vectors in
         * const methods of DerivedT; as local vectors are bound to be used within a single method they are
         * declared as mutable.
         * \endinternal
         */
        template<std::size_t IndexT>
        LocalVectorT& GetLocalVector() const;


      private:
        //! Local vectors stored.
        mutable std::array<LocalVectorT, NlocalVectorT> vector_list_;
    };


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
