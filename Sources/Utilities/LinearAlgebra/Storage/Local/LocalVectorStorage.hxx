// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Crtp
{


    template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
    constexpr std::size_t LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::Size()
    {
        return NlocalVectorT;
    }


    template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
    void LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::InitLocalVectorStorage(
        const std::array<Eigen::Index, NlocalVectorT>& vectors_dimension)
    {
        for (std::size_t i = 0UL; i < NlocalVectorT; ++i)
        {
            auto& vector = vector_list_[i];
            vector.resize(vectors_dimension[i]);
            vector.setZero();
        }
    }


    template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
    template<std::size_t IndexT>
    inline LocalVectorT& LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::GetLocalVector() const
    {
        static_assert(IndexT < NlocalVectorT, "Check index is within bounds!");
        return vector_list_[IndexT];
    }


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
