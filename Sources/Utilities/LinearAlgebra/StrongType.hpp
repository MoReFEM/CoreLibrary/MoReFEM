// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STRONGTYPE_DOT_HPP_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STRONGTYPE_DOT_HPP_
// *** MoReFEM header guards *** < //

// IWYU pragma: begin_exports
#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"
// IWYU pragma: end_exports

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"


namespace MoReFEM
{

    // clang-format off

    /*!
     * \class doxygen_hide_strong_type_conversion_proc_wise_index
     *
     * \attention \a MoReFEM::DofNS::processor_wise_or_ghost_index is almost the same as this strong type
     * - current one just adds an additional  semantic information in the use within global linear algebra.
     * Don't be astonished if there are at some point conversion these types.
     */


    /*!
     * \class doxygen_hide_strong_type_conversion_program_wise_index
     *
     * \attention \a MoReFEM::DofNS::procgram_wise_index is almost the same as this strong type
     * - current one just adds an additional  semantic information in the use within global linear algebra.
     * Don't be astonished if there are at some point conversion these types.
     */



    /*!
     * \brief Strong type for processor-wise (or "local" in HPC community) row index.
     *
     *  \copydetails doxygen_hide_strong_type_quick_explanation
     *
     * \copydetails doxygen_hide_strong_type_conversion_proc_wise_index
     */
    using row_processor_wise_index_type =
        StrongType
        <
            PetscInt,
            struct row_processor_wise_index_type_tag,
            StrongTypeNS::Addable,
            StrongTypeNS::Comparable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;

    /*!
     * \brief trong type for processor-wise (or "global" in HPC community) row index.
     *
     *  \copydetails doxygen_hide_strong_type_quick_explanation
     *
     * \copydetails doxygen_hide_strong_type_conversion_program_wise_index
     */
    using row_program_wise_index_type =
        StrongType
        <
            PetscInt,
            struct row_program_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Addable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;

  /*!
   * \brief Strong type for processor-wise (or "local" in HPC community) column index.
   *
   *  \copydetails doxygen_hide_strong_type_quick_explanation
   *
   * \copydetails doxygen_hide_strong_type_conversion_proc_wise_index
   */
    using col_processor_wise_index_type =
        StrongType
        <
            PetscInt,
            struct col_processor_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Addable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;


    /*!
     * \brief Strong type for program-wise (or "global" in HPC community) colum index.
     *
     *  \copydetails doxygen_hide_strong_type_quick_explanation
     *
     * \copydetails doxygen_hide_strong_type_conversion_program_wise_index
     */
    using col_program_wise_index_type =
        StrongType
        <
            PetscInt,
            struct col_program_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Addable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;


    /*!
     * \brief Strong type for processor-wise (or "local" in HPC community) vector index.
     *
     *  \copydetails doxygen_hide_strong_type_quick_explanation
     *
     * \copydetails doxygen_hide_strong_type_conversion_proc_wise_index
     */
    using vector_processor_wise_index_type =
        StrongType
        <
            PetscInt,
            struct vector_processor_wise_index_type_tag,
            StrongTypeNS::Addable,
            StrongTypeNS::Comparable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;


    /*!
     * \brief Strong type for program-wise (or "global" in HPC community) vector index.
     *
     *  \copydetails doxygen_hide_strong_type_quick_explanation
     *
     * \copydetails doxygen_hide_strong_type_conversion_program_wise_index
     */
    using vector_program_wise_index_type =
        StrongType
        <
            PetscInt,
            struct vector_program_wise_index_type_tag,
            StrongTypeNS::Addable,
            StrongTypeNS::Comparable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;


    //! Strong type for rows of small matrices (used for \a LocalOperator, \a Parameter, etc...)
    //! \copydetails doxygen_hide_strong_type_quick_explanation
    using small_matrix_row_index_type =
        StrongType
        <
            Eigen::Index,
            struct small_matrix_row_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Addable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;


    //! Strong type for columns of small matrices (used for \a LocalOperator, \a Parameter, etc...)
    //! \copydetails doxygen_hide_strong_type_quick_explanation
    using small_matrix_col_index_type =
        StrongType
        <
            Eigen::Index,
            struct small_matrix_col_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::Addable,
            StrongTypeNS::Printable,
            StrongTypeNS::DefaultConstructible
        >;


    // clang-format on


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STRONGTYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
