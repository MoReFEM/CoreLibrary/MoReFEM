// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_MISCELLANEOUS_DOT_HPP_
#define MOREFEM_UTILITIES_MISCELLANEOUS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cassert>
#include <memory>
#include <optional>
#include <type_traits> // IWYU pragma: keep
#include <vector>


namespace MoReFEM::Utilities
{


    /*!
     * \brief Create an hollow object that might be useful to mimic template function overloading.
     *
     * In C++ we can't specialize partially a template function, so a common enough trick is to rely on overloading
     * to perform a dispatch, by passing a dummy object as an additional argument:
     *
     * \code
     * template<class T, class U>
     * void MyFunction(T relevant_argument, U); // U is there as a type, no true argument!
     *
     * class SpecialType;
     *
     * template<class T>
     * void MyFunction(T relevant_argument, SpecialType); // mimics a specialization of the function for U =
     * SpecialType.
     *
     * ... (later in the code) ...
     * double x;
     * SpecialType dummy;
     * MyFunction(x, dummy);
     *
     * \endcode
     *
     * This is fine, but there are two problems with this:
     * - The type SpecialType must have a default constructor, without any argument.
     * - A dummy object must be constructed, which is inefficient peculiarly if the object is big enough.
     *
     * So a solution for both problems is to use an intermediate template class which:
     * - Is empty
     * - But still possess a default constructor.
     *
     * So the code above becomes:
     *
     * \code
     * template<class T, class U>
     * void MyFunction(T relevant_argument, Type2Type<U>); // U is there as a type, no true argument!
     *
     * class SpecialType;
     *
     * template<class T>
     * void MyFunction(T relevant_argument, Type2Type<SpecialType>); // mimics a specialization of the function for
     * U = SpecialType.
     *
     * ... (later in the code) ...
     * double x;
     * Type2Type<SpecialType> dummy;
     * MyFunction(x, dummy);
     *
     * \endcode
     *
     * From "Modern C++ Design", Andrei Alexandrescu.
     */
    template<class T>
    struct Type2Type
    {
        //! Alias to \a T.
        using type = T;
    };


    /*!
     * \brief Yields most adapted return type: by value or by const reference.
     *
     * The criterion of choice so far is whether \a T is a plain old data type or not.
     *
     * \tparam T Type for which the choice is made.
     *
     */
    // clang-format off
    template<typename T>
    struct ConstRefOrValue
    {
        //! Choice of type depending on whether \a T is plain old data (POD) or not.
        using type =
            std::conditional_t
            <
                std::is_standard_layout_v<T> && std::is_trivial_v<T>,
                T,
                const T&
            >;
    };
    // clang-format on


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    //! Specialization for reference.
    template<typename T>
    struct ConstRefOrValue<T&>
    {
        using type = const T&;
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    /*!
     * \brief Yields true_type if \a T is a specialization of \a Template.
     *
     * For instance,
     * \code
     * static_assert(IsSpecializationOf<std::tuple, TupleT>::value, "Compilation error if TupleT is not a
     * std::tuple.");
     * \endcode
     *
     * \attention Works only if all parameters of \a Template are type parameters.
     *
     */
    template<template<typename...> class Template, typename T>
    struct IsSpecializationOf : std::false_type
    { };


    template<template<typename...> class Template, typename... Args>
    struct IsSpecializationOf<Template, Template<Args...>> : std::true_type
    { };


    /*!
     * \brief Enum used to enforce constness in third party libraries that didn't do it natively.
     *
     * It is currently used in Petsc::AccessVectorContent for instance.
     */
    enum class Access { read_only, read_and_write };


    /*!
     * \brief Determines whether an object is a shared_ptr or not.
     */
    template<typename T>
    struct IsSharedPtr : public std::false_type
    { };


    /*!
     * \brief Determines whether an object is a unique_ptr or not.
     */
    template<typename T>
    struct IsUniquePtr : public std::false_type
    { };


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    template<typename T>
    struct IsSharedPtr<std::shared_ptr<T>> : public std::true_type
    { };

    template<typename T>
    struct IsSharedPtr<std::shared_ptr<const T>> : public std::true_type
    { };

    template<typename T>
    struct IsSharedPtr<const std::shared_ptr<T>> : public std::true_type
    { };

    template<typename T>
    struct IsSharedPtr<const std::shared_ptr<const T>> : public std::true_type
    { };

    template<typename T>
    struct IsUniquePtr<std::unique_ptr<T>> : public std::true_type
    { };

    template<typename T>
    struct IsUniquePtr<std::unique_ptr<const T>> : public std::true_type
    { };

    template<typename T>
    struct IsUniquePtr<const std::unique_ptr<T>> : public std::true_type
    { };

    template<typename T>
    struct IsUniquePtr<const std::unique_ptr<const T>> : public std::true_type
    { };


    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    /*!
     * \brief A trivial function to check whether a pointer is nullptr or not.
     *
     * \internal <b><tt>[internal]</tt></b> The point is just to avoid a lambda in algorithms...
     * <b><tt>[internal]</tt></b> Argument is a const reference to make it work also for unique_ptr...
     * \endinternal
     *
     * \tparam PointerT Type of the variable being scrutinized; it must be a pointer or a pointer-like type
     * (currently only pointer or shared/unique smart pointers may be used in this function).
     *
     * \param[in] pointer Pointer or pointer-like variable being under investigation.
     *
     * \return True if \a pointer is equal to nullptr.
     */
    template<class PointerT>
    inline bool IsNullptr(const PointerT& pointer)
    {
        static_assert(IsSharedPtr<PointerT>() || IsUniquePtr<PointerT>() || std::is_pointer<PointerT>(),
                      "PointerT must be a pointer type!");
        return pointer == nullptr;
    }


    //! Convenient alias when an argument is either (almost) `const T&` or `std::nullopt`.
    template<class T>
    using const_ref_or_nullopt = std::optional<std::reference_wrapper<const T>>;

    //! Convenient alias when an argument is either (almost) `T&` or `std::nullopt`.
    template<class T>
    using ref_or_nullopt = std::optional<std::reference_wrapper<T>>;

/*!
* \brief Macro to avoid repeating twice a same command when checking through `requires` clause whether it exists.
*
* An example is better here:
\code
if constexpr (requires { invariant_holder.parent_to_use::UpdateFirstDerivates(cauchy_green_tensor, quad_pt, geom_elt);
}) invariant_holder.parent_to_use::UpdateFirstDerivates(cauchy_green_tensor, quad_pt, geom_elt);
\endcode
*
* is rather verbosy and repeat the actual command twice.
* The point of this macro is to avoid this repetition.
*/
#define RUN_IF_EXISTING(command)                                                                                       \
    if constexpr (requires { command; })                                                                               \
        command;

} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_MISCELLANEOUS_DOT_HPP_
// *** MoReFEM end header guards *** < //
