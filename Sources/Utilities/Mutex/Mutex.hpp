// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_MUTEX_MUTEX_DOT_HPP_
#define MOREFEM_UTILITIES_MUTEX_MUTEX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <memory>
#include <mutex>


namespace MoReFEM::Crtp
{


    /*!
     * \brief This CRTP class defines a mutex each object of the DerivedT class.
     *
     */
    template<class DerivedT>
    class Mutex
    {

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Mutex() = default;

        //! Destructor.
        ~Mutex() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Mutex(const Mutex& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Mutex(Mutex&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Mutex& operator=(const Mutex& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Mutex& operator=(Mutex&& rhs) = delete;

        ///@}

        //! Get access to the mutex.
        //! \return Non constant reference to std::mutex underlying object.
        std::mutex& GetMutex() const;


      private:
        /*!
         * \brief Mutex object.
         *
         * This follows item 16 of Scott Meyers's "Effective Modern C++", which advises to make const member
         * functions thread safe.
         *
         * However, it is here enclosed in a unique_ptr as mutex doesn't seem to be movable (current implementations
         * of both clang and gcc don't define the move constructor and the move assignation; the copy is deleted).
         */
        mutable std::unique_ptr<std::mutex> mutex_ = std::make_unique<std::mutex>();
    };


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Mutex/Mutex.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_MUTEX_MUTEX_DOT_HPP_
// *** MoReFEM end header guards *** < //
