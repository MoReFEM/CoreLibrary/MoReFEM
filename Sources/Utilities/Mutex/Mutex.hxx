// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_MUTEX_MUTEX_DOT_HXX_
#define MOREFEM_UTILITIES_MUTEX_MUTEX_DOT_HXX_
// IWYU pragma: private, include "Utilities/Mutex/Mutex.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Crtp
{


    template<class DerivedT>
    inline std::mutex& Mutex<DerivedT>::GetMutex() const
    {
        assert(!(!mutex_));
        return *mutex_;
    }


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_MUTEX_MUTEX_DOT_HXX_
// *** MoReFEM end header guards *** < //
