// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HXX_
#define MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HXX_
// IWYU pragma: private, include "Utilities/Numeric/Numeric.hpp"
// *** MoReFEM header guards *** < //


#include <cmath>
#include <limits>
#include <memory>
#include <sstream>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Exceptions/Exception.hpp"

#include "ThirdParty/IncludeWithoutWarning/Eigen/Eigen.hpp"


namespace MoReFEM::NumericNS
{


    template<class T>
    constexpr T UninitializedIndex() noexcept
    {
        if constexpr (std::is_integral<T>())
            return std::numeric_limits<T>::max();
        else // specialization of StrongType; needs to be more explicit...
            return T(std::numeric_limits<typename T::underlying_type>::max());
    }


    template<class T>
    constexpr T Square(T value) noexcept
    {
        static_assert(std::is_arithmetic<T>());
        return value * value;
    }


    template<class T>
    constexpr T Cube(T value) noexcept
    {
        return value * value * value;
    }


    template<class T>
    constexpr T PowerFour(T value) noexcept
    {
        return value * value * value * value;
    }


    template<class T>
    constexpr T AbsPlus(T value) noexcept
    {
        constexpr T zero = static_cast<T>(0);

        if (value < zero)
            return zero;
        else
            return value;
    }


    template<class T>
    constexpr T Sign(T value) noexcept
    {
        constexpr T zero = static_cast<T>(0);
        constexpr T minus_one = static_cast<T>(-1);
        constexpr T one = static_cast<T>(1);

        if (IsZero(value))
            return zero;
        else if (value < zero)
            return minus_one;
        else
            return one;
    }


    template<class T>
    constexpr T TrueSign(T value) noexcept
    {
        constexpr T zero = static_cast<T>(0);
        constexpr T minus_one = static_cast<T>(-1);
        constexpr T one = static_cast<T>(1);

        if (value < zero)
            return minus_one;
        else
            return one;
    }


    template<class T>
    constexpr T Heaviside(T value) noexcept
    {
        static_assert(std::is_floating_point<T>(), "Heaviside doesn't make any sense for integers.");

        constexpr T zero = static_cast<T>(0);
        constexpr T one_half = static_cast<T>(1. / 2.);
        constexpr T one = static_cast<T>(1.);

        if (value < zero)
            return zero;
        else if (IsZero(value))
            return one_half;
        else
            return one;
    }


    template<std::floating_point T>
    constexpr T DefaultEpsilon() noexcept
    {
        return std::numeric_limits<T>::epsilon();
    }


    template<std::floating_point T>
    bool IsZero(T value, T epsilon) noexcept
    {
        return std::fabs(value) < epsilon;
    }


    template<std::integral T>
    bool IsZero(T value) noexcept
    {
        return value == static_cast<T>(0);
    }


    template<std::floating_point T>
    bool AreEqual(T lhs, T rhs, T epsilon) noexcept
    {
        return IsZero(lhs - rhs, epsilon);
    }


    template<std::floating_point T>
    bool AreEquivalentThroughRelativeComparison(T lhs, T rhs, T epsilon) noexcept
    {
        return std::fabs(lhs - rhs) <= epsilon * std::fmax(std::fabs(rhs), std::fabs(rhs));
    }


    template<class T>
    T Pow(T base, T exponent, const std::source_location location)
    {
        static_assert(std::is_floating_point<T>());

        const T ret = std::pow(base, exponent);

        if (!std::isfinite(ret))
        {
            std::ostringstream oconv;
            oconv << "Invalid std::pow operation (base = " << base << ", exponent = " << exponent << ").";
            throw MoReFEM::Exception(oconv.str(), location);
        }

        return ret;
    }


} // namespace MoReFEM::NumericNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HXX_
// *** MoReFEM end header guards *** < //
