// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <string>

#include "Utilities/Singleton/Exceptions/Singleton.hpp"


namespace // anonymous
{
    // Forward declarations here; definitions are at the end of the file

    std::string DeadReferenceMsg(const std::string& name);


    std::string NotYetCreatedMsg(const std::string& name);


} // namespace


namespace MoReFEM::Utilities::ExceptionNS::Singleton
{


    DeadReference::DeadReference(const std::string& name, const std::source_location location)
    : MoReFEM::Exception(DeadReferenceMsg(name), location)
    { }


    DeadReference::~DeadReference() = default;


    NotYetCreated::~NotYetCreated() = default;


    NotYetCreated::NotYetCreated(const std::string& name, const std::source_location location)
    : MoReFEM::Exception(NotYetCreatedMsg(name), location)
    { }


} // namespace MoReFEM::Utilities::ExceptionNS::Singleton


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{
    // Definitions of functions defined at the beginning of the file


    std::string DeadReferenceMsg(const std::string& name)
    {
        return "Dead reference found: singleton " + name + " was invoked after its demise...";
    }


    std::string NotYetCreatedMsg(const std::string& name)
    {
        return "Singleton was invoked while underlying object (" + name
               + ") not yet created. "
                 "Singleton<T>::CreateOrGetInstance() "
                 "creates the object if not yet existing, while Singleton<T>::GetInstance() you "
                 "called "
                 "expects the object to already have been created. The subtlety exists so that the user "
                 "is not forced to create a default constructor, which could be error prone.";
    }


} // namespace
