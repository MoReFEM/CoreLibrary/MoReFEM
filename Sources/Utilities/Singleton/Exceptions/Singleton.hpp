// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SINGLETON_EXCEPTIONS_SINGLETON_DOT_HPP_
#define MOREFEM_UTILITIES_SINGLETON_EXCEPTIONS_SINGLETON_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::Utilities::ExceptionNS::Singleton
{


    //! This exception is thrown if a call is made after singleton destruction.
    class DeadReference final : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] name Name of the \a Singleton for which the exception was thrown.
         * \copydoc doxygen_hide_source_location
         */
        explicit DeadReference(const std::string& name,
                               const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        DeadReference(const DeadReference& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        DeadReference(DeadReference&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        DeadReference& operator=(const DeadReference& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        DeadReference& operator=(DeadReference&& rhs) = default;


        //! Destructor
        virtual ~DeadReference() override;
    };


    /*!
     * This exception is thrown when Singleton<T>::GetInstance() is called while singleton
     * has not yet been created.
     */
    class NotYetCreated final : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] name Name of the \a Singleton for which the exception was thrown.
         * \copydoc doxygen_hide_source_location
         */
        explicit NotYetCreated(const std::string& name,
                               const std::source_location location = std::source_location::current());

        //! \copydoc doxygen_hide_copy_constructor
        NotYetCreated(const NotYetCreated& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        NotYetCreated(NotYetCreated&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NotYetCreated& operator=(const NotYetCreated& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        NotYetCreated& operator=(NotYetCreated&& rhs) = default;

        //! Destructor
        virtual ~NotYetCreated() override;
    };


} // namespace MoReFEM::Utilities::ExceptionNS::Singleton


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SINGLETON_EXCEPTIONS_SINGLETON_DOT_HPP_
// *** MoReFEM end header guards *** < //
