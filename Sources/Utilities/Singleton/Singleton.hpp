// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SINGLETON_SINGLETON_DOT_HPP_
#define MOREFEM_UTILITIES_SINGLETON_SINGLETON_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>
#include <cstdlib>
#include <memory>
#include <mutex>

#include "Utilities/Singleton/Exceptions/Singleton.hpp"


namespace MoReFEM::Utilities
{


    /*!
     * \brief Provides part of the singleton interface to \a T through a CRTP.
     *
     * \tparam T Class to which the singleton behaviour is given.
     *
     * It should be noticed that all the behaviour of the singleton can't be assigned through this class;
     * for instance the following code unfortunately works:
     *
     * \code
     * // In .hpp file
     * class Single : public Singleton<Single>
     * {
     *      public:
     *          Single();
     * };
     *
     * // In main.cpp
     * Single single; // no compile error.
     *
     * \endcode
     *
     * So this class only helps to enforce part of the singleton behaviour; you have to add two steps if you want
     * it to be completely ensured:
     *
     * . Make the constructor of the derived class private.
     * . Add a friendship line to Utilities::Singleton<T> in T class declaration.
     *
     * As a rule, I would advise to define only ONE constructor in the derived class to avoid any ambiguity.
     *
     * This Singleton is what Andrei Alexandrescu calls a 'Meyers singleton' in "Modern C++ Design"
     *  (see chapter 6, section 6.4).
     */
    template<class T>
    class Singleton
    {
      public:
        //! Convenient alias.
        using self = Singleton<T>;

        /*!
         * \brief Call instance of the singleton.
         *
         * If not already existing it will be created on the fly, otherwise the existing one will be used.
         * This method should be called in two cases:
         * - If we do not know whether the instance already exists or not. The most typical example is
         * when a value is registered within the singleton in an anonymous namespace.
         * - If we know for sure it is the first call to the singleton. For instance the initialization
         * of TimeKeep sinfgleton class that sets the initial time.
         *
         * In all other cases, call instead GetInstance().
         *
         * \tparam Args Variadic template arguments.
         * \param[in] args Arguments passed to the constructor if the object is to be built.
         * \copydoc doxygen_hide_source_location
         *
         * \return Instance of the singleton.
         */
        template<class... Args>
        static T& CreateOrGetInstance(const std::source_location location = std::source_location::current(),
                                      Args&&... args);


        /*!
         * \brief Call an instance of the singleton that must already have been created.
         *
         * This must be called instead of CreateOrGetInstance(location) if T doesn't get a
         * default constructor.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Instance of the singleton.
         */
        static T& GetInstance(const std::source_location location = std::source_location::current());

        /*!
         * \brief Destroy the singleton.
         */
        static void Destroy();


      protected:
        //! Ensure singleton behaviour.
        //@{
        //! Disable constructor call.
        Singleton() = default;

        /*!
         * \brief Destructor.
         *
         * Disable public destruction: destruction of a Singleton must occur either
         * through atexit() stack or through a call to Destroy().
         *
         * Destructor is virtual: the way destruction occurs is deleting a pointer to a Singleton<T>,
         * which means without the virtuality we would get destroy properly the data attributes defined in T.
         *
         */
        virtual ~Singleton() = default;


      private:
        //! \copydoc doxygen_hide_copy_constructor
        Singleton(const Singleton& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Singleton(Singleton&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Singleton& operator=(const Singleton& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Singleton& operator=(Singleton&& rhs) = delete;

        //@}


        /*!
         * \brief Create the singleton in first invocation.
         *
         * \tparam Args Variadic template arguments.
         * \param[in] args Arguments passed to the constructor.
         */
        template<class... Args>
        static void Create(Args&&... args);


        /*!
         * \brief Gets called if dead reference detected.
         *
         * \copydoc doxygen_hide_source_location
         */
        [[noreturn]] static void OnDeadReference(const std::source_location location = std::source_location::current());

        //! Get reference to the singleton mutex.
        static std::mutex& GetNonCstSingletonMutex();


      private:
        //! Internal pointer to the actual instance.
        static T* instance_;


        /*!
         * \brief Used for protection against dead reference problem (invocation while the instance has been
         * destroyed).
         *
         * Should not occur in our case, but it is much safer to put the protection there anyway.
         */
        static bool destroyed_;

        /*!
         * \brief Mutex object.
         *
         * This follows item 16 of Scott Meyers's "Effective Modern C++", which advises to make const member
         * functions thread safe.
         *
         */
        static std::mutex singleton_mutex_;
    };


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Singleton/Singleton.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SINGLETON_SINGLETON_DOT_HPP_
// *** MoReFEM end header guards *** < //
