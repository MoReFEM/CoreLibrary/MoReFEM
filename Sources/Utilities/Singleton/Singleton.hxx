// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SINGLETON_SINGLETON_DOT_HXX_
#define MOREFEM_UTILITIES_SINGLETON_SINGLETON_DOT_HXX_
// IWYU pragma: private, include "Utilities/Singleton/Singleton.hpp"
// *** MoReFEM header guards *** < //


#include <iostream>

namespace MoReFEM::Utilities
{


    template<class T>
    T* Singleton<T>::instance_ = nullptr;


    template<class T>
    bool Singleton<T>::destroyed_ = false;


    template<class T>
    std::mutex Singleton<T>::singleton_mutex_;


    template<class T>
    void Singleton<T>::Destroy()
    {
        // See http://codeofthedamned.com/index.php/unit-testing-a-singleton-in for an explanation about this...
        if (!(!instance_))
        {
            self* buf = instance_;
            instance_ = nullptr;
            delete buf;
        }

        destroyed_ = true;
    }


    template<class T>
    template<class... Args>
    T& Singleton<T>::CreateOrGetInstance(const std::source_location location, Args&&... args)
    {
        std::lock_guard<std::mutex> lock(GetNonCstSingletonMutex());

        if (!instance_)
        {
            // Check for dead reference.
            if (destroyed_)
                OnDeadReference(location);
            else
            {
                // First call initialize.
                Create(std::forward<Args>(args)...);
            }
        }

        return *instance_;
    }


    template<class T>
    T& Singleton<T>::GetInstance(const std::source_location location)
    {
        if (!instance_) // whichever the case it is an abnormal stance of the program...
        {
            // Check for dead reference.
            if (destroyed_)
                OnDeadReference(location);
            else
                throw ExceptionNS::Singleton::NotYetCreated(T::ClassName(), location);
        }

        return *instance_;
    }


    template<class T>
    template<class... Args>
    void Singleton<T>::Create(Args&&... args)
    {
        assert(!instance_);
        instance_ = new T(std::forward<Args>(args)...);

        int error_code = std::atexit(Destroy); // to destroy the singleton at the end of the program.

        if (error_code != 0)
            throw Exception("Failing to register " + T::ClassName() + " in atexit destruction stack.");
    }


    template<class T>
    std::mutex& Singleton<T>::GetNonCstSingletonMutex()
    {
        return singleton_mutex_;
    }


    template<class T>
    void Singleton<T>::OnDeadReference(const std::source_location location)
    {
        throw ExceptionNS::Singleton::DeadReference(T::ClassName(), location);
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SINGLETON_SINGLETON_DOT_HXX_
// *** MoReFEM end header guards *** < //
