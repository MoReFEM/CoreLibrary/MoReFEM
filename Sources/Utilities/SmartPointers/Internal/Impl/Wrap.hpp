// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_IMPL_WRAP_DOT_HPP_
#define MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_IMPL_WRAP_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::Impl
{


    /*!
     * \brief Common implementation under the hood for MoReFEM::Internal::WrapUnique(), MoReFEM::Internal::WrapShared(),
     * MoReFEM::Internal::WrapUniqueToConst(), MoReFEM::Internal::WrapSharedToConst().
     *
     * \tparam ReturnTypeT Expected return type - a smart pointer to an object that might or might not be constant.
     *
     * \param[in] ptr Raw pointer built with a `new` call that will be immediately owned by the chosen smart pointer type.
     *
     */
    template<class ReturnTypeT>
    ReturnTypeT WrapSmartPointer(typename ReturnTypeT::element_type* ptr);


} // namespace MoReFEM::Internal::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/SmartPointers/Internal/Impl/Wrap.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_IMPL_WRAP_DOT_HPP_
// *** MoReFEM end header guards *** < //
