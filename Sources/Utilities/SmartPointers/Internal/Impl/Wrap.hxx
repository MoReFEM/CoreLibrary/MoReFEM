// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_IMPL_WRAP_DOT_HXX_
#define MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_IMPL_WRAP_DOT_HXX_
// IWYU pragma: private, include "Utilities/SmartPointers/Internal/Impl/Wrap.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::Impl
{


    template<class ReturnTypeT>
    ReturnTypeT WrapSmartPointer(typename ReturnTypeT::element_type* ptr)
    {
        using element_type = typename ReturnTypeT::element_type;
        static_assert(!std::is_array<element_type>::value);
        static_assert(std::is_object<element_type>::value);

        return ReturnTypeT(ptr);
    }


} // namespace MoReFEM::Internal::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_IMPL_WRAP_DOT_HXX_
// *** MoReFEM end header guards *** < //
