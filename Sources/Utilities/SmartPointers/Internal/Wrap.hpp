// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_WRAP_DOT_HPP_
#define MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_WRAP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>

#include "Utilities/SmartPointers/Internal/Impl/Wrap.hpp"


namespace MoReFEM::Internal
{


    /*!
     * \class doxygen_hide_wrap_pointers
     *
     * \tparam T Type of the object built by the factory.
     *
     * \param[in] ptr Raw pointer built with a `new` call
     *
     * \return The smart pointer which owns \a ptr.
     *
     * \note Inspired by the namesake function in Abseil - see https://github.com/abseil/abseil-cpp/blob/master/absl/memory/memory.h
     */


    /*!
     * \brief Provide a functionality to build in one line a unique pointer when the underlying constructor is private.
     *
     * The recommended way to build a smart pointer is to use the dedicated STL facilities - `std::make_unique<T>` for a
     unique pointer for instance.
     *
     * However, in the factory pattern:
     * - the factory is in charge of building all the objects of type \a T
     * - to prevent construction outside of the factory, constructors of \a T are private.
     * - Factory is able to build them through a friendship given in \a T declaration.
     *
     * The issue is that `std::make_unique` doesn't propagate friendship in this case.
     *
     * This is where current functionality provides syntactic sugar to limit the boilerplate:
     *
     * \code
     auto ptr = WrapUnique(new T(...));
     \endcode
     *
     * is slightly more palatable than:
     *
     * \code
     auto ptr = std::make_unique<T>(new T(...));
     \endcode
     *
     * as we do not need to repeat \a T explicitly as template parameter.
     *
     * You should however use first and foremost `std::make_unique`; current functionality is only for the edge case
     described above.
     *
     * \copydoc doxygen_hide_wrap_pointers
     */
    template<class T>
    std::unique_ptr<T> WrapUnique(T* ptr);


    /*!
     * \brief Same as MoReFEM::Internal::WrapUnique() for unique_ptr owning a constant object.
     *
     * \copydoc doxygen_hide_wrap_pointers
     */
    template<class T>
    std::unique_ptr<const T> WrapUniqueToConst(const T* ptr);


    /*!
     * \brief Same as MoReFEM::Internal::WrapUnique() for shared_ptr.
     *
     * \copydoc doxygen_hide_wrap_pointers
     */
    template<class T>
    std::shared_ptr<T> WrapShared(T* ptr);


    /*!
     * \brief Same as MoReFEM::Internal::WrapUnique() for shared_ptr owning a constant object.
     *
     * \copydoc doxygen_hide_wrap_pointers
     */
    template<class T>
    std::shared_ptr<const T> WrapSharedToConst(const T* ptr);


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/SmartPointers/Internal/Wrap.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_WRAP_DOT_HPP_
// *** MoReFEM end header guards *** < //
