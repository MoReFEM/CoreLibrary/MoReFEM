// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_WRAP_DOT_HXX_
#define MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_WRAP_DOT_HXX_
// IWYU pragma: private, include "Utilities/SmartPointers/Internal/Wrap.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal
{


    template<class T>
    std::unique_ptr<T> WrapUnique(T* ptr)
    {
        using return_type = std::unique_ptr<T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }


    template<class T>
    std::unique_ptr<const T> WrapUniqueToConst(const T* ptr)
    {
        using return_type = std::unique_ptr<const T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }


    template<class T>
    std::shared_ptr<T> WrapShared(T* ptr)
    {
        using return_type = std::shared_ptr<T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }


    template<class T>
    std::shared_ptr<const T> WrapSharedToConst(const T* ptr)
    {
        using return_type = std::shared_ptr<const T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }

} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SMARTPOINTERS_INTERNAL_WRAP_DOT_HXX_
// *** MoReFEM end header guards *** < //
