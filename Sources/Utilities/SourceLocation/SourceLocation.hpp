// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_SOURCELOCATION_SOURCELOCATION_DOT_HPP_
#define MOREFEM_UTILITIES_SOURCELOCATION_SOURCELOCATION_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <iosfwd>
#include <source_location>


namespace MoReFEM::Utilities
{


    /*!
     * \brief Create an identifier for \a std::source_location that may be used in a container.
     *
     * \copydoc doxygen_hide_source_location
     *
     * \return Unique string representing a given source location.
     */
    std::string GenerateSourceLocationKey(const std::source_location& location);


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_SOURCELOCATION_SOURCELOCATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
