// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_STATICASSERT_STATICASSERTFALSE_DOT_HPP_
#define MOREFEM_UTILITIES_STATICASSERT_STATICASSERTFALSE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <type_traits>


namespace MoReFEM::Advanced
{

    /*!
    * \brief Helper struct to mimic `static_assert(false)`.
    *
    * `static_assert(false)` will be supported in C++23 standard; currently it won't work.
    * It might be useful when the call is in a `if constexpr` branch that should never happen for instance.
    *
    * Current struct enables mimicking this with
    \code
    static_assert<Advanced::False<T>>;
    \endcode
    *
    * call, where T is a template parameter in the call site (or you might provide whatever you want -
    * the geist is not to define a specialization that would provide a `true` value).
    *
    * It follows a [Stack Overflow answer](https://stackoverflow.com/a/44065093).
    */
    template<class T>
    struct False : std::bool_constant<false>
    { };


    /*!
     * \brief Same as `False` when the template parameter is an enum one.
     *
     * For instance:
     \code
     enum foo { bar, baz };

     ...

     static_assert(Advanced::FalseEnum<foo, foo::bar>());
     \endcode
     */
    template<class EnumT, EnumT ValueT>
    struct FalseEnum : std::bool_constant<false>
    { };

} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_STATICASSERT_STATICASSERTFALSE_DOT_HPP_
// *** MoReFEM end header guards *** < //
