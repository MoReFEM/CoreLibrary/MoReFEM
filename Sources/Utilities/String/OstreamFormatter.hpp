// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_STRING_OSTREAMFORMATTER_DOT_HPP_
#define MOREFEM_UTILITIES_STRING_OSTREAMFORMATTER_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <format>
#include <sstream>
#include <string_view>

namespace MoReFEM
{


    /*!
     * \brief Facility to make an object which provides `operator<<` overload usable in `std::format`.
     *
     * To enable the behaviour for a given class `ClassT` you just need to define for it:
     *
     \code
     namespace std
     {
         template <>
         struct formatter<ClassT> : ::MoReFEM::basic_ostream_formatter {};
     }
     \endcode
     *
     * Said class must provide an operator <<.
     *
     * From [this StackOverflow answer](https://stackoverflow.com/a/75738462); I have just removed the template
     * helper class which was not used outside of specific `char` instantiation.
     */
    struct basic_ostream_formatter : std::formatter<std::basic_string_view<char>, char>
    {

        /*!
         * \brief The method expected by [std::formatter](https://en.cppreference.com/w/cpp/utility/format/formatter).
         *
         * \param[in] object Object to print in the format expression.
         * \param[in] ctx Context used by the formatter class.
         *
         * \return Expected output iterator.
         */
        template<typename T, typename OutputIt>
        OutputIt format(const T& object, std::basic_format_context<OutputIt, char>& ctx) const;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/String/OstreamFormatter.hxx" // IWYU pragma: export

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_STRING_OSTREAMFORMATTER_DOT_HPP_
// *** MoReFEM end header guards *** < //
