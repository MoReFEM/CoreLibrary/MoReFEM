// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_STRING_OSTREAMFORMATTER_DOT_HXX_
#define MOREFEM_UTILITIES_STRING_OSTREAMFORMATTER_DOT_HXX_
// IWYU pragma: private, include "Utilities/String/OstreamFormatter.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM
{


    template<typename T, typename OutputIt>
    auto basic_ostream_formatter::format(const T& value, std::basic_format_context<OutputIt, char>& ctx) const
        -> OutputIt
    {
        std::basic_stringstream<char> ss;
        ss << value;
        return std::formatter<std::basic_string_view<char>, char>::format(ss.view(), ctx);
    };


} // namespace MoReFEM

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_STRING_OSTREAMFORMATTER_DOT_HXX_
// *** MoReFEM end header guards *** < //
