### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/EmptyString.cpp
		${CMAKE_CURRENT_LIST_DIR}/String.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/EmptyString.hpp
		${CMAKE_CURRENT_LIST_DIR}/OstreamFormatter.hpp
		${CMAKE_CURRENT_LIST_DIR}/OstreamFormatter.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/String.hpp
		${CMAKE_CURRENT_LIST_DIR}/String.hxx
		${CMAKE_CURRENT_LIST_DIR}/Traits.hpp
)

