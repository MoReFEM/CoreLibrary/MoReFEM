// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <random>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/String/String.hpp"


namespace MoReFEM::Utilities::String
{


    void StripLeft(std::string& string, std::string_view char_to_strip)
    {
        if (string.empty())
            return;

        std::size_t Nchar_to_erase{};
        const std::size_t length = string.size();
        bool done = false;

        while (!done)
        {
            if (char_to_strip.find(string[Nchar_to_erase]) == std::string::npos)
                done = true;
            else
            {
                if (++Nchar_to_erase == length)
                    done = true;
            }
        }

        if (Nchar_to_erase != 0UL)
            string.erase(0, Nchar_to_erase);
    }


    void StripRight(std::string& string, std::string_view char_to_strip)
    {
        while (!string.empty())
        {
            const char last_character = string.back();

            if (char_to_strip.find(last_character) == std::string::npos)
                return;

            string.pop_back();
        }
    }


    bool EndsWith(std::string_view string, std::string_view sequence)
    {
        const std::size_t size_sequence = sequence.size();
        const std::size_t size_string = string.size();

        if (size_sequence > size_string)
            return false;

        return 0 == string.compare(size_string - size_sequence, size_string, sequence);
    }


    std::size_t Replace(std::string_view to_replace, std::string_view replacement, std::string& string_to_modify)
    {
        std::size_t Noccurrence_modified = 0;

        auto pos = string_to_modify.find(to_replace);

        const auto Nchar_in_to_replace = to_replace.size();
        const auto Nchar_in_replacement = replacement.size();

        while (pos != std::string::npos)
        {
            string_to_modify.replace(pos, Nchar_in_to_replace, replacement);
            ++Noccurrence_modified;
            pos = string_to_modify.find(to_replace, pos + Nchar_in_replacement);
        }

        return Noccurrence_modified;
    }


    namespace // anonymous
    {


        /*!
         * \brief Helper function for Reformat().
         *
         * \param[in] word_list The list of all the words in the string to reformat.
         * \param[in] indexes The indexes of the words in the list to print on current line.
         * \param[in] prefix Prefix to add to each line save the very first one.
         * \param[in,out] oconv Stream to which the content is flushed.
         * \param[in,out] is_first_line Whether it is the first line printed. False in exit.
         */
        void PrintLine(const std::vector<std::string>& word_list,
                       const std::vector<std::size_t>& indexes,
                       std::string_view prefix,
                       std::ostringstream& oconv,
                       bool& is_first_line)
        {
            if (!is_first_line)
            {
                oconv << prefix;
            }

            is_first_line = false;

            for (auto index : indexes)
            {
                assert(index < word_list.size());
                oconv << word_list[index] << ' ';
            }

            oconv << '\n';
        }


        const std::string& EndlinePlaceholder()
        {
            static const std::string ret("ENDLINE_PLACEHOLDER");
            return ret;
        }


    } // namespace


    std::string
    Reformat(std::string_view original, std::size_t max_length, std::string_view prefix, const char separator)
    {
        const auto size = original.size();

        // Easier case: the string already fits the bill!
        if (size < max_length)
        {
            std::ostringstream oconv;
            oconv << original << '\n';
            const std::string ret = oconv.str();
            return ret;
        }

        std::vector<std::string> word_list;

        std::string copy(original);

        std::string placeholder(" ");
        placeholder += EndlinePlaceholder() + " ";

        Replace("\n", placeholder, copy);

        auto begin = copy.cbegin();
        auto end = copy.cend();

        auto it_prev(begin);
        auto it_next(begin);

        // Separate the original string into words.
        while (it_prev != end && it_next != end)
        {
            it_next = std::find_if(it_prev,
                                   end,
                                   [separator](const char character)
                                   {
                                       return character == separator;
                                   });
            std::string word(it_prev, it_next);

            word_list.emplace_back(std::move(word));

            it_prev = it_next;
            ++it_prev;
        }

        // Now write it so if possible no line exceeds the maximum number of characters.
        // It might be impossible of course if a word is long enough...
        std::ostringstream oconv;
        std::size_t current_size = 0;

        std::vector<std::size_t> current_line_list;

        const auto Nword = word_list.size();

        bool is_first_line(true);

        for (std::size_t i = 0UL; i < Nword; ++i)
        {
            const auto& word = word_list[i];

            // If word is EndlinePlaceholder() (i.e. in original a \n); issue a newline.
            if (word == EndlinePlaceholder())
            {
                PrintLine(word_list, current_line_list, prefix, oconv, is_first_line);
                current_line_list.clear();
                current_size = 0UL;
                continue;
            }

            current_size += word.size() + 1; // +1 to take into account the separator!

            // If size exceed max length, it's time to flush. Likewise if 'word' is indeed '\n'.
            if (current_size > max_length)
            {
                // Case in which the word alone is longer than the max length... Flush the current word.
                if (current_line_list.empty())
                    PrintLine(word_list, { i }, prefix, oconv, is_first_line);
                // If not, flush the previous words and pick current word for next line.
                else
                {
                    PrintLine(word_list, current_line_list, prefix, oconv, is_first_line);

                    current_line_list.clear();
                    current_line_list.push_back(i);
                    current_size = word.size();
                }
            }
            // If not just add current word to line content.
            else
            {
                current_line_list.push_back(i);

                // If in the text there is a \n, respect it!
                if (EndsWith(word, "\n"))
                {
                    PrintLine(word_list, current_line_list, prefix, oconv, is_first_line);
                    current_line_list.clear();
                    current_size = 0UL;
                }
            }
        }

        if (!current_line_list.empty())
        {
            PrintLine(word_list, current_line_list, prefix, oconv, is_first_line);
        }

        const auto ret = oconv.str();

        return ret;
    }


    std::string ConvertCharArray(const std::vector<char>& array)
    {
        std::string ret;
        ret.reserve(array.size());

        std::ranges::copy_if(array,

                             std::back_inserter(ret),
                             [](char character)
                             {
                                 return (character != '\0');
                             });

        ret.shrink_to_fit();

        return ret;
    }


    std::string GenerateRandomString(std::size_t length, std::string_view charset)
    {
        assert(charset.size() >= 2UL);

        std::random_device random_device;
        std::default_random_engine random_engine(random_device());
        std::uniform_int_distribution<std::size_t> distribution(0UL, charset.size() - 1UL);

        auto randchar = [&charset, &distribution, &random_engine]()
        {
            assert(distribution.max() == charset.size() - 1UL);
            return charset[distribution(random_engine)];
        };

        std::string ret(length, 0);
        std::generate_n(ret.begin(), length, randchar);
        return ret;
    }


    std::vector<std::string_view> Split(std::string_view string, std::string_view delimiter)
    {
        auto next = string.find(delimiter);

        // Case delimiter simply not there...
        if (next == std::string::npos)
            return { string };

        auto current = 0UL;
        std::vector<std::string_view> ret;

        while (next != std::string::npos)
        {
            assert(next >= current);
            ret.push_back(string.substr(current, next - current));
            current = next + 1;
            next = string.find(delimiter, current);
        }

        ret.push_back(string.substr(current, next - current));

        return ret;
    }


} // namespace MoReFEM::Utilities::String


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
