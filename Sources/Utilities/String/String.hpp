// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_STRING_STRING_DOT_HPP_
#define MOREFEM_UTILITIES_STRING_STRING_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <string> // IWYU pragma: keep
#include <vector>


namespace MoReFEM::Utilities::String
{


    /*!
     * \brief Strip from the left all characters that belongs to \a char_to_strip.
     *
     * \param[in,out] string String to be stripped.
     * \param[in] char_to_strip All the characters that should be deleted should they be found on the left of \a
     * string.
     *
     * For instance,
     * \code
     * std::string str = "  \t\n      TEST    \t \n";
     * StripLeft(str, "\t\n "); // => yields "TEST    \t \n";
     *
     * \endcode
     */
    void StripLeft(std::string& string, std::string_view char_to_strip = "\t\n ");


    /*!
     * \brief Strip from the right all characters that belongs to \a char_to_strip.
     *
     * \param[in,out] string String to be stripped.
     * \param[in] char_to_strip All the characters that should be deleted should they be found on the right of
     * \a string.
     *
     * For instance,
     * \code
     * std::string str = "  \t\n      TEST    \t \n";
     * StripRight(str, "\t\n "); // => yields "  \t\n      TEST";
     *
     * \endcode
     */
    void StripRight(std::string& string, std::string_view char_to_strip = "\t\n ");


    /*!
     * \brief Strip from the left and the right all characters that belongs to \a char_to_strip.
     *
     * \param[in,out] string String to be stripped.
     * \param[in] char_to_strip All the characters that should be deleted should they be found on the
     * left and the right of \a string.
     *
     * For instance,
     * \code
     * std::string str = "  \t\n      TEST    \t \n";
     * Strip(str, "\t\n "); // => yields "TEST";
     *
     * \endcode
     */
    void Strip(std::string& string, std::string_view char_to_strip = "\t\n ");


    /*!
     * \brief Tells whether \a string starts with \a sequence.
     *
     * \param[in] string String considered.
     * \param[in] sequence Sequence that is searched at the beginning of \a string.
     *
     * \return True if \a string starts with \a sequence.
     *
     * \attention C++ 20 will finally introduce that feature (see
     * https://en.cppreference.com/w/cpp/string/basic_string_view)
     */
    bool StartsWith(std::string_view string, std::string_view sequence);


    /*!
     * \brief Tells whether \a string ends with \a sequence.
     *
     * \param[in] string String considered.
     * \param[in] sequence Sequence that is searched at the end of \a string.
     *
     * \return True if \a string ends with \a sequence.
     *
     * \attention C++ 20 will finally introduce that feature (see
     * https://en.cppreference.com/w/cpp/string/basic_string_view)
     */
    bool EndsWith(std::string_view string, std::string_view sequence);


    /*!
     * \brief Replace all occurrences of \a to_replace in \a string_to_modify by \a replacement.
     *
     * \param[in] to_replace Sequence looked at in the string. Beware: this look-up is case-sensitive!
     * \param[in] replacement String to replace the previous one.
     * \param[in,out] string_to_modify String into which the substitution will occur.
     *
     * \return Number of occurrences found.
     */
    std::size_t Replace(std::string_view to_replace, std::string_view replacement, std::string& string_to_modify);


    /*!
     * \brief Reformat a string so that it doesn't exceed a certain length per line.
     *
     * \param[in] original String considered.
     * \param[in] prefix Prefix to add to each new line. For instance "\t--" in the case of a comment
     * in an input data file.
     * \param[in] max_length Maximum number of characters on a new line. Prefix size is not included in this
     * count.
     * \param[in] separator Acceptable line separator. Space by default.
     *
     * \attention Implementation is a bit rough and it might not be generic enough; it's just good enough
     * for the current usage.
     *
     * \return Reformatted string.
     */
    std::string
    Reformat(std::string_view original, std::size_t max_length, std::string_view prefix, const char separator = ' ');


    /*!
     * \brief Repeat the same \a string \a times.
     *
     * \code
     * Repeat('\t', 4)
     * \endcode
     *
     * yields '\\t\\t\\t\\t'.
     *
     * \param[in] string String to repeat.
     * \param[in] times Number of times the string is to be repeated.
     *
     * \return The result (i.e. \a string repeated \a times times).
     */
    template<class StringT>
    std::string Repeat(const StringT& string, std::size_t times);


    /*!
     * \brief Convert a std::vector<char> into a string.
     *
     * This is useful when using functions with a C API. in this case you should use a std::vector<char> to
     * collect char*, as seen in this example with MPI API;
     *
     * \code
     * std::vector<char> error_string_char(MPI_MAX_ERROR_STRING);
     * int length_of_error_string;
     *
     * MPI_Error_string(error_code, error_string_char.data(), &length_of_error_string);
     * \endcode
     *
     * This char array might be required as a string if another operation is asked, hence this function, which
     * takes care of the terminating null character (std::string expects '\0' as final character).
     *
     * \param[in] array C-string array to be converted.
     * \return The C++ string.
     */
    std::string ConvertCharArray(const std::vector<char>& array);


    /*!
     * \brief Generates a random string of size \a length (useful for tests...).
     *
     * \param[in] length Size of the expected string (as returned by size() method).
     * \param[in] charset All characters in this string may be used in the generated string. If a given char
     * is present more than once, it is more likely to be picked.
     *
     * This code is inspired from
     * https://stackoverflow.com/questions/440133/how-do-i-create-a-random-alpha-numeric-string-in-c
     *
     * \return The random string.
     */
    std::string
    GenerateRandomString(std::size_t length,
                         std::string_view charset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");


    /*!
     * \brief Split a string into several parts given a separator.
     *
     * This is really a facilitator and does not aim to provide the best performance possible.
     *
     * \param[in] string The string to be split in several parts.
     * \param[in] delimiter The delimiter to separate the parts.
     *
     * \return A view over the different parts.
     *
     * Example: Split("Triangle3; Domain1; 5", ";")
     *
     * will return { "Triangle3", " Domain1" and " 5" } (the space is not stripped).
     */
    std::vector<std::string_view> Split(std::string_view string, std::string_view delimiter);


} // namespace MoReFEM::Utilities::String


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/String/String.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_STRING_STRING_DOT_HPP_
// *** MoReFEM end header guards *** < //
