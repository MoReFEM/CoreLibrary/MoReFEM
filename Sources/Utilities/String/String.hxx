// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_STRING_STRING_DOT_HXX_
#define MOREFEM_UTILITIES_STRING_STRING_DOT_HXX_
// IWYU pragma: private, include "Utilities/String/String.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>


namespace MoReFEM::Utilities::String
{


    inline void Strip(std::string& string, std::string_view char_to_strip)
    {
        StripLeft(string, char_to_strip);
        StripRight(string, char_to_strip);
    }


    inline bool StartsWith(std::string_view string, std::string_view sequence)
    {
        return 0 == string.compare(0, sequence.size(), sequence);
    }


    template<class StringT>
    std::string Repeat(const StringT& string, std::size_t times)
    {
        std::ostringstream oconv;

        for (std::size_t i = 0; i < times; ++i)
            oconv << string;

        return oconv.str();
    }


} // namespace MoReFEM::Utilities::String


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_STRING_STRING_DOT_HXX_
// *** MoReFEM end header guards *** < //
