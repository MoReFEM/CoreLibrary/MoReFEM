// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_STRING_TRAITS_DOT_HPP_
#define MOREFEM_UTILITIES_STRING_TRAITS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <experimental/type_traits>
#include <filesystem>
#include <string>


namespace MoReFEM::Utilities::String
{


    //! Convenient enum class for IsString.
    enum class CharAllowed { yes, no };

    //! Helper type which helps to check whether class \a T defines an attribute named \a identified_as_string.
    template<typename T>
    using identified_as_string_helper = decltype(std::declval<T>().identified_as_string);

    /*!
     * \brief Is evaluated to true if there is an attribute named \a identified_as_string in \a T.
     *
     * For instance:
     * \code
     * supports_identified_as_string<int>::value; // evaluates to false
     * supports_identified_as_string<std::string>::value; // evaluates to false - the attribute is a convention
     * within MoReFEM!
     *
     * struct Foo
     * {
     *      static constexpr bool identified_as_string = true;
     * };
     *
     * supports_identified_as_string<Foo>::value; // evaluates to true
     *
     * \endcode
     */
    template<typename T>
    using supports_identified_as_string = std::experimental::is_detected<identified_as_string_helper, T>;


    /*!
     * \brief Traits class which tells whether T is a string class.
     *
     * Currently recognized string classes are:
     * - When std::decay_t<T> is either char* or const char*.
     * - When std::decay_t<T> is std::string.
     * - When std::decay_t<T> is a char in the case \a IsCharAllowedT is CharAllowed::yes.
     * - When T defines a trait named identified_as_string. The extent to which it really does is the
     * responsibility of the developer of \a T.
     *
     * \tparam IsCharAllowedT Whether a char should be recognized as a string or not.
     *
     * This was most inspired by http://stackoverflow.com/questions/8097534/type-trait-for-strings
     * with few tweaks of my own.
     *
     * \attention There are no strong guarantee about what \a T might do or not! The fact char* returns true
     * rules out for instance asking for size() attribute or += operator (and the \a identified_as_string case
     * blrus the line even further). As a rule of thumb, what it should at least do is being printable with
     * operator<<.
     */
    // clang-format off
            template<class T, CharAllowed IsCharAllowedT = CharAllowed::yes>
            struct IsString
            : public std::integral_constant<
                  bool,
                  std::is_same<char*, std::decay_t<T> >::value || std::is_same<const char*, std::decay_t<T> >::value
                      || std::is_same<std::string, std::decay_t<T> >::value
                      || (IsCharAllowedT == CharAllowed::yes && std::is_same<char, std::decay_t<T> >::value)
                      || supports_identified_as_string<T>::value>
            { };
    // clang-format on

    static_assert(!IsString<int>::value);
    static_assert(!IsString<int*>::value);
    static_assert(!IsString<std::filesystem::path>::value);
    static_assert(IsString<char*>::value);
    static_assert(IsString<const char*>::value);
    static_assert(IsString<char* const>::value);
    static_assert(IsString<const char* const>::value);
    static_assert(IsString<char (&)[5]>::value);
    static_assert(IsString<const char (&)[5]>::value);
    static_assert(IsString<std::string>::value);
    static_assert(IsString<const std::string>::value);
    static_assert(IsString<const std::string&>::value);


} // namespace MoReFEM::Utilities::String


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_STRING_TRAITS_DOT_HPP_
// *** MoReFEM end header guards *** < //
