// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TIMEKEEP_TIMEKEEP_DOT_HPP_
#define MOREFEM_UTILITIES_TIMEKEEP_TIMEKEEP_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <chrono>
#include <fstream> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
#include <string>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Class used to profile crudely (through prints) the code.
     *
     * In 'normal' release mode, this class should be kept at minimum: just keep in memory the starting date, and
     * write the total elapsed time at the end of the simulation.
     * If you define macro MOREFEM_EXTENDED_TIME_KEEP, an additional method named PrintTimeElapsed() is made
     * available so that prints can be added to profile finely time elapsed between each call of this method.
     */
    class TimeKeep : public Utilities::Singleton<TimeKeep>
    {
      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

#ifdef MOREFEM_EXTENDED_TIME_KEEP
        /*!
         * \brief Print the time elapsed since Init() has been called, in milliseconds.
         *
         * \param[in] label Specify whatever you want to specify where the call occurs.
         */
        void PrintTimeElapsed(const std::string& label);
#endif // MOREFEM_EXTENDED_TIME_KEEP

        //! Return the time elapsed since the beginning of the simulation.
        //! \return Time elapsed since the beginning of the simulation, under the format HH::MM::SS.
        std::string TimeElapsedSinceBeginning() const;

        /*!
         * \brief At the end of the program, write in the stream the time elapsed since beginning.
         *
         * Should be called at the end of a Model.
         */
        void PrintEndProgram();


      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in,out] stream Stream upon which time information will be written.
         */
        explicit TimeKeep(std::ofstream&& stream);

        //! Destructor.
        virtual ~TimeKeep() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<TimeKeep>;
        ///@}


      private:
        //! Stream into which the prints are logged.
        std::ofstream stream_;

        //! Time at which Init() has been called.
        const std::chrono::time_point<std::chrono::steady_clock> init_time_;

        //! Time of the previous call to PrintTimeElapsed.
        std::chrono::time_point<std::chrono::steady_clock> previous_call_time_;
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TIMEKEEP_TIMEKEEP_DOT_HPP_
// *** MoReFEM end header guards *** < //
