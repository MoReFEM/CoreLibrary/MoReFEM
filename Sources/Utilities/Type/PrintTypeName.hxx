// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_PRINTTYPENAME_DOT_HXX_
#define MOREFEM_UTILITIES_TYPE_PRINTTYPENAME_DOT_HXX_
// IWYU pragma: private, include "Utilities/Type/PrintTypeName.hpp"
// *** MoReFEM header guards *** < //

#include <cassert>
#include <cstdlib>
#include <source_location>


namespace MoReFEM
{


    template<class T>
    constexpr std::string_view GetTypeName()
    {
        std::string_view p = std::source_location::current().function_name();

#ifdef __clang__
        constexpr auto magic_number = 45UL;
        return std::string_view(p.data() + magic_number, p.size() - magic_number - 1);
#elif defined(__GNUC__)
        constexpr auto magic_number = 60UL;
        return std::string_view(p.data() + magic_number, p.find(';', magic_number) - magic_number);
#endif

        assert(false
               && "All compilers should be handled here explicitly! Please contact developers "
                  "of library to add support for yours; currently MoReFEM suppports gcc, clang and AppleClang");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_PRINTTYPENAME_DOT_HXX_
// *** MoReFEM end header guards *** < //
