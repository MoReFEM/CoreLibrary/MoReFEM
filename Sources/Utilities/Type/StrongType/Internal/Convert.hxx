// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_INTERNAL_CONVERT_DOT_HXX_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_INTERNAL_CONVERT_DOT_HXX_
// IWYU pragma: private, include "Utilities/Type/StrongType/Internal/Convert.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm> // IWYU pragma: keep
#include <vector>


namespace MoReFEM::Internal::StrongTypeNS
{

    template<class StrongTypeT>
    std::vector<StrongTypeT> Convert(const std::vector<typename StrongTypeT::underlying_type>& pod_vector)
    {
        std::vector<StrongTypeT> ret;
        ret.reserve(pod_vector.size());

        std::transform(pod_vector.cbegin(),
                       pod_vector.cend(),
                       std::back_inserter(ret),
                       [](const auto pod)
                       {
                           return StrongTypeT{ pod };
                       });

        return ret;
    }


} // namespace MoReFEM::Internal::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_INTERNAL_CONVERT_DOT_HXX_
// *** MoReFEM end header guards *** < //
