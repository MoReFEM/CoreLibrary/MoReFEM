// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_ASMPIDATATYPE_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_ASMPIDATATYPE_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType, the facilities related to \a Mpi may be
     * used with the \a StrongType.
     *
     */
    template<typename StrongTypeT>
    struct AsMpiDatatype
    {


        //! Token.
        static constexpr bool use_in_mpi_datatype = true;
    };


} // namespace MoReFEM::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_ASMPIDATATYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
