// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_COMPARABLE_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_COMPARABLE_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator< and == are enabled for the \a
     * StrongType.
     *
     * \todo #1553 Use spaceship operator when C++ 20 is out.
     *
     */
    template<typename StrongTypeT>
    struct Comparable
    {

        /*!
         * \brief Operator< for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        constexpr bool operator<(const Comparable<StrongTypeT>& other) const noexcept;


        /*!
         * \brief Operator== for the \a StrongTypeT.
         *
         * \param[in] other Second member of the operator.
         *
         * \return The result.
         */
        constexpr bool operator==(const Comparable<StrongTypeT>& other) const noexcept;

        /*!
         * \brief Operator> for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        constexpr bool operator>(const Comparable<StrongTypeT>& other) const noexcept;

        /*!
         * \brief Operator<= for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        constexpr bool operator<=(const Comparable<StrongTypeT>& other) const noexcept;


        /*!
         * \brief Operator>= for the \a StrongTypeT.
         *
         * \param[in] other Second member of the < operator.
         *
         * \return The result.
         *
         * \todo #1553 Spaceship operator when possible!
         */
        constexpr bool operator>=(const Comparable<StrongTypeT>& other) const noexcept;


        /*!
         * \brief Operator!= for the \a StrongTypeT.
         *
         * \param[in] other Second member of the operator.
         *
         * \return The result.
         */
        constexpr bool operator!=(const Comparable<StrongTypeT>& other) const noexcept;
    };


} // namespace MoReFEM::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/Type/StrongType/Skills/Comparable.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_COMPARABLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
