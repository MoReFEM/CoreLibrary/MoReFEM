// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_DIVISIBLE_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_DIVISIBLE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <type_traits> // IWYU pragma: keep

namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator% is enabled for the \a
     * StrongType.
     */
    template<typename StrongTypeT>
    struct Divisible
    {


        /*!
         * \brief Operator/ for the \a StrongTypeT.
         *
         * \param[in] other Second member of the / operator.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT operator/(const StrongTypeT& other) const;

        /*!
         * \brief Operator% for the \a StrongTypeT.
         *
         * \param[in] other Second member of the % operator.
         *
         * This operator is defined only for integral types, as % yields a compilation error for floating-point types.
         *
         * \tparam StrongTypeT_ Artificial type introduced to use SFINAE here; never use it with something other than \a StrongTypeT !
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        template<class StrongTypeT_ = StrongTypeT>
        std::enable_if_t<std::is_integral_v<typename StrongTypeT_::underlying_type>, StrongTypeT>
        operator%(const StrongTypeT& other) const;
    };


} // namespace MoReFEM::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/Type/StrongType/Skills/Divisible.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_DIVISIBLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
