// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_HASHABLE_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_HASHABLE_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType, a hash specialization is provided
     * (using the underlying type hash)
     *
     * See https://www.fluentcpp.com/2017/05/30/implementing-a-hash-function-for-strong-types/ for more explanation.
     */
    template<typename StrongTypeT>
    struct Hashable
    {


        //! IndexedSectionDescription: the heavy work is done in std::hash specialization in \a StrongType, which is
        //! enabled only if there is such a token.
        static constexpr bool is_hashable = true;
    };


} // namespace MoReFEM::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_HASHABLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
