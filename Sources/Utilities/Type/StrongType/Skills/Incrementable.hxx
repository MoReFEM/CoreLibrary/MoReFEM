// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_INCREMENTABLE_DOT_HXX_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_INCREMENTABLE_DOT_HXX_
// IWYU pragma: private, include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::StrongTypeNS
{


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator+=(const StrongTypeT& other)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        underlying.Get() += other.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator++()
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        ++underlying.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT Incrementable<StrongTypeT>::operator++(int)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        StrongTypeT temp = underlying;
        ++underlying.Get();
        return temp;
    }


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator-=(const StrongTypeT& other)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        underlying.Get() -= other.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT& Incrementable<StrongTypeT>::operator--()
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        --underlying.Get();
        return underlying;
    }


    template<class StrongTypeT>
    StrongTypeT Incrementable<StrongTypeT>::operator--(int)
    {
        auto& underlying = static_cast<StrongTypeT&>(*this);
        StrongTypeT temp = underlying;
        --underlying.Get();
        return temp;
    }


} // namespace MoReFEM::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_INCREMENTABLE_DOT_HXX_
// *** MoReFEM end header guards *** < //
