// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_PRINTABLE_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_PRINTABLE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator<< is enabled for the \a
     * StrongType.
     *
     * It assumes the type hidden in \a StrongTypeT provides itself an overload to operator<</
     */
    template<typename StrongTypeT>
    struct Printable
    {
        //! A method used in a required() clause to provide better error message
        //! when a strong type without the adequate skill is required.
        static inline constexpr bool IsPrintable();

        //! Print the strong type on stteam \a os.
        //! \param[in,out] os Stream onto which the strong type will be written.
        void Print(std::ostream& os) const;
    };


} // namespace MoReFEM::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/Type/StrongType/Skills/Printable.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_PRINTABLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
