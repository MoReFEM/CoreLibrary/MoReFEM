// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iostream>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/String/OstreamFormatter.hpp"


namespace MoReFEM
{

    /*!
     * \class doxygen_hide_strong_type_quick_explanation
     *
     * When you want to extract the type hidden beneath the strong type, you must use Get() method.
     *
     * For instance:
     \code
     using my_strong_type =
         StrongType
         <
             std::size_t,
             struct my_strong_type_tag,
         >;

     my_strong_type a { 5UL; }
     std::size_t s { 3UL };
     auto sum = a.Get() + s; // a.Get() yields a std::size_t object.
     \endcode
     *
     * Don't worry about efficiency loss: at least in release mode there are no incurred cost using them
     * (see [this blog post](https://www.fluentcpp.com/2017/05/05/news-strong-types-are-free/).
     *
     * Strong types are used to provide additional safety, by limiting the risk of passing a variable where
     * it gets no business at all (consider for instance mesh_index and dof_index: both are `std::size_t` underneath
     * but we really don't want to provide one where the other is expected).
     */


    /*!
     * \brief Abstract class used to define a StrongType, which allows more expressive code and ensures that the order
     * of arguments of the same underlying type is respected at call sites.
     *
     * Adapted from https://github.com/joboccara/NamedType/
     *
     * \tparam T Type of the parameter.
     * \tparam Parameter This is a phantom type used to specialize the type deduced for SrongType, it is not actually
     * used in the implementation.
     * \tparam Skills Optional arguments to grant additional functionalities to the created
     * StrongType (see below and
     * https://www.fluentcpp.com/2017/05/23/strong-types-inheriting-functionalities-from-underlying/ post).
     *
     * As an example we could create a class Rectangle with strong types to differentiate its width from its length as:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag>;
     * using Height = StrongType<double, struct HeightTag>;
     *
     * class StrongRectangle
     * {
     * public:
     *     StrongRectangle (Width width, Height height) : width_(width.Get()), height_(height.Get()) {}
     *     double getWidth() const {return width_;}
     *     double getHeight() const {return height_;}
     *
     * private:
     *     double width_;
     *     double height_;
     * };
     * \endcode
     *
     * At the calling site we would have:
     *
     * \code
     * StrongRectangle rectangle((Width(5.0)), (Height((2.0))));
     * \endcode
     *
     * Note that the extra parenthesis are only required for constructors due to the most vexing parse.
     *
     * As for \a Skills: a StrongType does not allow direct operations upon its instances. For instance the following
     * code is invalid:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag>;
     * Width w1(5.);
     * Width w2(10.);
     * Width sum = w1 + w2; // does not compile!
     * \endcode
     *
     * The new arguments enable to add support for it; to do so we provide a new Crtp (called Addable here) which grants
     * support for operator+:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag, Addable>;
     * Width w1(5.);
     * Width w2(10.);
     * Width sum = w1 + w2; // ok!
     * \endcode
     *
     * The list of possible options is in the 'Skills' subdirectory; you may define your own if needed (it is basically
     * a Crtp). Several may be added in the declaration:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag, Addable, Incrementable>;
     * \endcode
     */
    template<class T, class Parameter, template<typename> class... Skills>
    class StrongType : public Skills<StrongType<T, Parameter, Skills...>>...
    {
      private:
        //! Convenient alias.
        using self = StrongType<T, Parameter, Skills...>;


      public:
        //! Traits to indicate the object is a strong type.
        static constexpr bool IsStrongType{ true };


        //! Convenient alias.
        using underlying_type = T;

        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor.
         *
         * \param[in] value Underlying value held by the strong type.
         *
         */
        explicit constexpr StrongType(const T& value);

        /*!
         * \brief Move constructor.
         *
         * \param[in] value Underlying value held by the strong type.
         *
         * \tparam T_ ArtificIal template parameter to ensure that SFINAE applies in case \a T  is  a reference.
         *
         */
        explicit constexpr StrongType(T&& value)
            requires(!std::is_reference_v<T>);


        /*!
         * \brief Default constructor, which is defined only when enable_default_constructor is defined.
         *
         * This should happen only when skill \a DefaultConstructible is chosen.
         *
         * \tparam T_ ArtificIal template parameter to ensure that SFINAE applies.
         *
         */
        explicit constexpr StrongType()
            requires(self::is_default_constructible);


        //! Destructor.
        ~StrongType() = default;

        //! \copydoc doxygen_hide_copy_constructor
        StrongType(const StrongType& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        StrongType(StrongType&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        StrongType& operator=(const StrongType& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        StrongType& operator=(StrongType&& rhs) = default;


        ///@}


      public:
        //! Conversion operator to the underlying type.
        //! \internal The \a explicit is there on purpose, please don't remove it!
        //! (otherwise the whole point of using strong types is lost...)
        explicit operator T() const noexcept;

        //! Constant accessor to the underlying value held by the strong type.
        constexpr const T& Get() const noexcept;

        //! Non constant accessor to the underlying value held by the strong type.
        T& Get() noexcept;

      private:
        //! Underlying value held by the strong type.
        T value_;
    };


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     *
     * This one works only if a \a Print(std::ostream&) method exists in \a StrongType class; skill \a Printable defines
     * such a method.
     */
    template<class T, class ParameterT, template<class> class... Skills>
    std::ostream& operator<<(std::ostream& stream, const StrongType<T, ParameterT, Skills...>& rhs);


    /*!
     * \brief Facility to determine whether a class \a T is a \a StrongType or not.
     *
     * \internal Utilities::IsSpecializationOf doesn't work with \a StrongType and my metaprogramming skill is not high enough to figure out how
     * to make it work in a reasonable time hence this dedicated one.
     */
    template<typename T>
    struct IsStrongType : std::false_type
    { };

    template<class T, class Parameter, template<typename> class... Skills>
    struct IsStrongType<StrongType<T, Parameter, Skills...>> : std::true_type
    { };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace std
{


    /*!
     * \brief Provide hash function for \a StrongType only if enabled explicitly in \a Skills.
     */
    template<typename T, typename Parameter, template<typename> class... Skills>
    struct hash<::MoReFEM::StrongType<T, Parameter, Skills...>>
    {

        //! Alias to the \a StrongType considered.
        using type = ::MoReFEM::StrongType<T, Parameter, Skills...>;

        //! Activated only is is_hashable token is defined (SFINAE)
        //! See https://www.fluentcpp.com/2017/05/30/implementing-a-hash-function-for-strong-types/.
        using check_is_hashable = typename std::enable_if<type::is_hashable, void>::type;

        /*!
         * \brief Operator which does the bulk of the work and returns the hash.
         *
         * \param[in] x Strong-typed value for which a hash is requested.
         *
         * \return Hash of the value hidden within the \a StrongType.
         */
        size_t operator()(const ::MoReFEM::StrongType<T, Parameter, Skills...>& x) const
        {
            return std::hash<T>()(x.Get());
        }
    };


    /*!
     * \brief This declaration makes possible to use Printable strong types in `std::format` functions.
     */
    template<typename T, typename ParameterT, template<typename> class... Skills>
    struct formatter<::MoReFEM::StrongType<T, ParameterT, Skills...>> : public ::MoReFEM::basic_ostream_formatter
    { };


} // namespace std


#include "Utilities/Type/StrongType/StrongType.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
