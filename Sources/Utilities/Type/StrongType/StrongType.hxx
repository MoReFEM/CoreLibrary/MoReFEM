// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HXX_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HXX_
// IWYU pragma: private, include "Utilities/Type/StrongType/StrongType.hpp"
// *** MoReFEM header guards *** < //


#include <vector>

#include "Utilities/Type/StrongType/Skills/Printable.hpp"


namespace MoReFEM
{


    template<class T, class Parameter, template<typename> class... Skills>
    constexpr StrongType<T, Parameter, Skills...>::StrongType(const T& value) : value_(value)
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    // clang-format off
    constexpr StrongType<T, Parameter, Skills...>
    ::StrongType(T&& value)
    requires(!std::is_reference_v<T>)
    // clang-format on
    : value_(std::move(value))
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    constexpr StrongType<T, Parameter, Skills...>::StrongType()
        requires(self::is_default_constructible)
    : value_{}
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    inline constexpr const T& StrongType<T, Parameter, Skills...>::Get() const noexcept
    {
        return value_;
    }


    template<class T, class Parameter, template<typename> class... Skills>
    inline T& StrongType<T, Parameter, Skills...>::Get() noexcept
    {
        return value_;
    }


    template<class T, class Parameter, template<typename> class... Skills>
    StrongType<T, Parameter, Skills...>::operator T() const noexcept
    {
        return value_;
    }


    template<class T, class ParameterT, template<typename> class... Skills>
    std::ostream& operator<<(std::ostream& stream, const StrongType<T, ParameterT, Skills...>& object)
    {
        using strong_type = StrongType<T, ParameterT, Skills...>;

        if constexpr (requires { strong_type::IsPrintable(); })
        {
            object.Print(stream);
            return stream;
        } else
        {
            constexpr bool no_printable = std::is_same_v<T, std::false_type>; // nonsense expression that returns false
            static_assert(no_printable, "operator<< can only be used with a `StrongType` with `Printable` skill!");
        }
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HXX_
// *** MoReFEM end header guards *** < //
