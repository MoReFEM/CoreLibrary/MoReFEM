// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_UNIQUEID_ENUM_DOT_HPP_
#define MOREFEM_UTILITIES_UNIQUEID_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::UniqueIdNS
{


    /*!
     * \brief Whether the unique id is given in constructor or generated automatically by incrementing a static
     * counter.
     *
     * Details for each mode:
     * - \a automatic: Each time a new instance of \a DerivedT is created, it is assigned a value (which is the
     * number of already created instance). This value is accessible through the method GetUniqueId().
     * - \a manual: The ID is given by the user, and there is a check the id remains effectively unique.
     */
    enum class AssignationMode { automatic, manual };


    /*!
     * \brief Whether it is possible to build an object without assigning to it a unique id.
     *
     * \attention Relevant only for a \a UniqueId which \a AssignationMode is manual.
     */
    enum class DoAllowNoId { no, yes };


} // namespace MoReFEM::UniqueIdNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_UNIQUEID_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
