// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// IWYU pragma: no_include <__tree>

#include <cstddef> // IWYU pragma: keep
#include <source_location>
#include <sstream>
#include <string>
#include <string_view>

#include "Utilities/UniqueId/Exceptions/UniqueId.hpp"

#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM::Internal::ExceptionsNS::UniqueId
{


    namespace // anonymous
    {

        std::string ReservedValueMsg();


        std::string AlreadyExistingIdMsg(std::size_t new_unique_id, std::string_view class_considered);

    } // namespace


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const std::source_location location)
    : ::MoReFEM::Exception(msg, location)
    { }


    ReservedValue::~ReservedValue() = default;

    ReservedValue::ReservedValue(const std::source_location location) : Exception(ReservedValueMsg(), location)
    { }


    AlreadyExistingId::~AlreadyExistingId() = default;

    AlreadyExistingId::AlreadyExistingId(std::size_t new_unique_id,
                                         std::string_view class_considered,
                                         const std::source_location location)
    : Exception(AlreadyExistingIdMsg(new_unique_id, class_considered), location)
    { }


    namespace // anonymous
    {


        std::string ReservedValueMsg()
        {
            std::ostringstream oconv;
            oconv << "Identifier " << NumericNS::UninitializedIndex<std::size_t>()
                  << " is reserved; please choose any other std::size_t!";
            return oconv.str();
        }


        std::string AlreadyExistingIdMsg(std::size_t new_unique_id, std::string_view class_considered)
        {
            std::ostringstream oconv;

            oconv << "The id " << new_unique_id << " has already been given to another " << class_considered
                  << " object.";

            return oconv.str();
        }


    } // namespace


} // namespace MoReFEM::Internal::ExceptionsNS::UniqueId


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
