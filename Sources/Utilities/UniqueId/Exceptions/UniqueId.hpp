// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_UNIQUEID_EXCEPTIONS_UNIQUEID_DOT_HPP_
#define MOREFEM_UTILITIES_UNIQUEID_EXCEPTIONS_UNIQUEID_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::ExceptionsNS::UniqueId
{


    //! Generic exception thrown for unique id related operations.
    class Exception : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location
         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Specific unique id exception.
    class ReservedValue : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit ReservedValue(const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~ReservedValue() override;
    };


    //! Specific unique id exception.
    class AlreadyExistingId : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] new_unique_id The unique id which was already existing.
         * \param[in] class_considered The name of the class which unique id is considered (should be the result of a
         GetTypeName<T> specializationn).
         * \copydoc doxygen_hide_source_location

         */
        explicit AlreadyExistingId(std::size_t new_unique_id,
                                   std::string_view class_considered,
                                   const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~AlreadyExistingId() override;
    };

} // namespace MoReFEM::Internal::ExceptionsNS::UniqueId


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_UNIQUEID_EXCEPTIONS_UNIQUEID_DOT_HPP_
// *** MoReFEM end header guards *** < //
