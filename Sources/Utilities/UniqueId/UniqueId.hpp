// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_UNIQUEID_UNIQUEID_DOT_HPP_
#define MOREFEM_UTILITIES_UNIQUEID_UNIQUEID_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <set>
#include <sstream>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Numeric/Numeric.hpp"      // IWYU pragma: export
#include "Utilities/UniqueId/Enum.hpp"        // IWYU pragma: export


namespace MoReFEM::Crtp
{


    /*!
     * \brief This CRTP class defines a unique ID for each object of the DerivedT class.
     *
     * \tparam AssignationModeT See \a AssignationMode enum.
     * \tparam DoAllowNoIdT See \a DoAllowNoId enum.
     * \tparam UnderlyingTypeT Type used to store the unique id. The default choice is to use a `std::size_t`, but it might be even
     * better to use a \a StrongType.
     */
    // clang-format off
        template
        <
            class DerivedT,
            typename UnderlyingTypeT = std::size_t,
            UniqueIdNS::AssignationMode AssignationModeT = UniqueIdNS::AssignationMode::automatic,
            UniqueIdNS::DoAllowNoId DoAllowNoIdT = UniqueIdNS::DoAllowNoId::no
        >
    // clang-format on
    class UniqueId
    {

      public:
        //! Type used for the unique id.
        using underlying_type = UnderlyingTypeT;

        /// \name Special members.
        ///@{

        //! Constructor for AssignationMode::automatic.
        explicit UniqueId();

        //! Constructor for AssignationMode::manual.
        //! \param[in] id Unique id to assign. If invalid (e.g. already existing) an exception is thrown.
        explicit UniqueId(UnderlyingTypeT id);

        //! Constructor for AssignationMode::manual when no id is to be assigned (only if DoAllowNoIdT is yes).
        explicit UniqueId(std::nullptr_t);

        //! Destructor.
        ~UniqueId() = default;

        //! \copydoc doxygen_hide_copy_constructor
        UniqueId(const UniqueId& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        UniqueId(UniqueId&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UniqueId& operator=(const UniqueId& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        UniqueId& operator=(UniqueId&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Get the value of the internal unique ID.
         *
         * \return Unique id.
         */
        UnderlyingTypeT GetUniqueId() const;


        /*!
         * \brief Generate a unique id.
         *
         * No new objects is created, and the new identifier is not counted in the list - the purpose here is just
         * to provide a value that don't collide with existing objects. This is really used in a very edge case;
         * think twice before using this functionality as it really might not be what you really seek.
         *
         * \return A unique id that is not in use yet.
         */
        static UnderlyingTypeT GenerateNewEligibleId();

        /*!
         * \brief Clear all unique ids.
         *
         * \attention This functionality has been put there only for the sake of writing tests; please do not use it!
         * It should be considered as private, even if unfortunately I can't easily put it that way.
         */
        static void ClearUniqueIdList();


      private:
        /*!
         * \brief If AssignationMode is automatic, generates a new unique identifier.
         *
         * \return The new unique id, which is just an increment from the previously assigned one.
         */
        static UnderlyingTypeT AssignUniqueId();

        /*!
         * \brief If AssignationMode is manual, checks the unique identifier provided is valid.
         *
         * \param[in] new_unique_id The proposed new unique id. If it already exists an exception will  be thrown.
         *
         * \return The unmodified unique id.
         */
        static UnderlyingTypeT NewUniqueId(UnderlyingTypeT new_unique_id);

      private:
        /*!
         * \brief The value of the unique id for the current \a DerivedT object.
         */
        const UnderlyingTypeT unique_id_;

        //! List of all identifiers existing for \a DerivedT.
        static std::set<UnderlyingTypeT>& StaticUniqueIdList();
    };


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/UniqueId/UniqueId.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_UNIQUEID_UNIQUEID_DOT_HPP_
// *** MoReFEM end header guards *** < //
