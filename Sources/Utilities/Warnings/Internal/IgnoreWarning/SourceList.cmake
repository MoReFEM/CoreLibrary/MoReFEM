### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/alloca.hpp
		${CMAKE_CURRENT_LIST_DIR}/array-bounds.hpp
		${CMAKE_CURRENT_LIST_DIR}/cast-function-type.hpp
		${CMAKE_CURRENT_LIST_DIR}/class-memaccess.hpp
		${CMAKE_CURRENT_LIST_DIR}/comma.hpp
		${CMAKE_CURRENT_LIST_DIR}/covered-switch-default.hpp
		${CMAKE_CURRENT_LIST_DIR}/deprecated-builtins.hpp
		${CMAKE_CURRENT_LIST_DIR}/deprecated-copy-with-dtor.hpp
		${CMAKE_CURRENT_LIST_DIR}/deprecated-declarations.hpp
		${CMAKE_CURRENT_LIST_DIR}/deprecated-dynamic-exception-spec.hpp
		${CMAKE_CURRENT_LIST_DIR}/deprecated.hpp
		${CMAKE_CURRENT_LIST_DIR}/disabled-macro-expansion.hpp
		${CMAKE_CURRENT_LIST_DIR}/div-by-zero.hpp
		${CMAKE_CURRENT_LIST_DIR}/extra-semi-stmt.hpp
		${CMAKE_CURRENT_LIST_DIR}/extra-semi.hpp
		${CMAKE_CURRENT_LIST_DIR}/float-equal.hpp
		${CMAKE_CURRENT_LIST_DIR}/format-nonliteral.hpp
		${CMAKE_CURRENT_LIST_DIR}/format-security.hpp
		${CMAKE_CURRENT_LIST_DIR}/implicit-int-conversion.hpp
		${CMAKE_CURRENT_LIST_DIR}/maybe-uninitialized.hpp
		${CMAKE_CURRENT_LIST_DIR}/missing-noreturn.hpp
		${CMAKE_CURRENT_LIST_DIR}/missing-prototypes.hpp
		${CMAKE_CURRENT_LIST_DIR}/newline-eof.hpp
		${CMAKE_CURRENT_LIST_DIR}/non-virtual-dtor.hpp
		${CMAKE_CURRENT_LIST_DIR}/old_style_cast.hpp
		${CMAKE_CURRENT_LIST_DIR}/redundant-decls.hpp
		${CMAKE_CURRENT_LIST_DIR}/redundant-parens.hpp
		${CMAKE_CURRENT_LIST_DIR}/reorder.hpp
		${CMAKE_CURRENT_LIST_DIR}/reserved-id-macro.hpp
		${CMAKE_CURRENT_LIST_DIR}/reserved-identifier.hpp
		${CMAKE_CURRENT_LIST_DIR}/reserved-macro-identifier.hpp
		${CMAKE_CURRENT_LIST_DIR}/return-std-move-in-c++11.hpp
		${CMAKE_CURRENT_LIST_DIR}/shadow-field-in-constructor.hpp
		${CMAKE_CURRENT_LIST_DIR}/shadow-field.hpp
		${CMAKE_CURRENT_LIST_DIR}/shorten-64-to-32.hpp
		${CMAKE_CURRENT_LIST_DIR}/sign-conversion.hpp
		${CMAKE_CURRENT_LIST_DIR}/sometimes-uninitialized.hpp
		${CMAKE_CURRENT_LIST_DIR}/stack-protector.hpp
		${CMAKE_CURRENT_LIST_DIR}/strict-aliasing.hpp
		${CMAKE_CURRENT_LIST_DIR}/stringop-overflow.hpp
		${CMAKE_CURRENT_LIST_DIR}/suggest-destructor-override.hpp
		${CMAKE_CURRENT_LIST_DIR}/suggest-override.hpp
		${CMAKE_CURRENT_LIST_DIR}/undef.hpp
		${CMAKE_CURRENT_LIST_DIR}/unused-function.hpp
		${CMAKE_CURRENT_LIST_DIR}/unused-local-typedef.hpp
		${CMAKE_CURRENT_LIST_DIR}/unused-member-function.hpp
		${CMAKE_CURRENT_LIST_DIR}/unused-template.hpp
		${CMAKE_CURRENT_LIST_DIR}/unused-variable.hpp
		${CMAKE_CURRENT_LIST_DIR}/used-but-marked-unused.hpp
		${CMAKE_CURRENT_LIST_DIR}/weak-vtables.hpp
		${CMAKE_CURRENT_LIST_DIR}/zero-as-null-pointer-constant.hpp
)

