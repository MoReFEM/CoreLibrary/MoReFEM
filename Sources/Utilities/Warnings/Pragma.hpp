// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * You may contact its developers by writing at morefem-maint@inria.fr.
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_WARNINGS_PRAGMA_DOT_HPP_
#define MOREFEM_UTILITIES_WARNINGS_PRAGMA_DOT_HPP_
// *** MoReFEM header guards *** < //


#ifdef SONARQUBE_4_MOREFEM
#define PRAGMA_DIAGNOSTIC(tok)
#else // SONARQUBE_4_MOREFEM

// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Helper macros.
// ============================


//! Helper of PRAGMA_DIAGNOSTIC.
#define STR_EXPAND(tok) #tok

/*!
 * \brief STR transform into a string literal the tok argument.
 *
 * Why we need STR_EXPAND is honestly a bit blurry for me, but we really need it.
 */
#define STR(tok) STR_EXPAND(tok)


/*!
 * \brief Helper to define PRAGMA_DIAGNOSTIC.
 *
 * It will choose the syntax to employ depending on the compiler used (clang and gcc supported at the moment).
 *
 * See
 * http://nadeausoftware.com/articles/2012/10/c_c_tip_how_detect_compiler_name_and_version_using_compiler_predefined_macros
 * for the macro used to detect each compiler.
 */
#ifdef __clang__
#define PREPARE_PRAGMA_STRING(tok) clang diagnostic tok
#else
#define MOREFEM_GCC // there is no macro to identify gcc, so I introduce one...
#define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#if GCC_VERSION >= 40600
#define PREPARE_PRAGMA_STRING(tok) GCC diagnostic tok
#else
#define PREPARE_PRAGMA_STRING(tok) message("Diagnostic pragmas not activated; requires gcc 4.6 or clang.")
#endif // __GNUC__
#endif // __clang__


// ============================
// Helper macros.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


/*!
 * \brief Apply a pragma diagnostic directive.
 *
 * \param[in] tok IndexedSectionDescription given to the macro. It is not a string literal yet; do no put double quotes
 around this argument.
 *
 * \code
 * PRAGMA_DIAGNOSTIC(push)
 * PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
 * PRAGMA_DIAGNOSTIC(ignored "-Wcast-align")
 * #include "parmetis.h"
 * PRAGMA_DIAGNOSTIC(pop)
 *
 * \endcode
 *
 * The advantage over a direct call to _Pragma is that the compiler is automatically handled.
 *
 * Without that the line would be
 *
 \verbatim
 #pragma clang diagnostic ignored "-Wconversion"
 \endverbatim
 *
 * for clang and
 *
 \verbatim
 #pragma GCC diagnostic ignored "-Wconversion"
 \endverbatim
 *
 * for GCC, with a directive condition to rule out between both.
 */
#define PRAGMA_DIAGNOSTIC(tok) _Pragma(STR(PREPARE_PRAGMA_STRING(tok)))

#endif // SONARQUBE_4_MOREFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_WARNINGS_PRAGMA_DOT_HPP_
// *** MoReFEM end header guards *** < //
