
message(STATUS "C compiler identifier found to be ${CMAKE_C_COMPILER_ID}; settings (warnings, STL, etc...) will be set accordingly.")


# From http://cmake.3232098.n2.nabble.com/Default-value-for-CMAKE-BUILD-TYPE-td7550756.html#a7550797
# Set a default build type if none was specified
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to 'Debug' as none was specified.")
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()


MESSAGE("Generated with config types: ${CMAKE_CONFIGURATION_TYPES}")

include(CheckIPOSupported)
cmake_policy(SET CMP0069 NEW) # Policy concerning IPO support

if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    add_definitions(-DDEBUG=1)    
endif()        
 
#add_cxx_compiler_flag("-fvisibility-inlines-hidden")
#add_cxx_compiler_flag("-fvisibility=default")


if (${OPTIMIZE_FOR_NATIVE})
    message("-- Building and optimizing for current architecture (with '-march=native').")
    add_compile_options(
        "$<$<COMPILE_LANGUAGE:CXX>:-march=native;-ftree-vectorize>"
    )
else()
    message("-- The build is not optimized for native architecture (no '-march=native' given to the compiler).")
endif()



if(${CMAKE_C_COMPILER_ID} STREQUAL "AppleClang" OR ${CMAKE_C_COMPILER_ID} STREQUAL "Clang")    
    add_cxx_compiler_flag("-Weverything")
    add_cxx_compiler_flag("-Wno-c++98-compat")
    add_cxx_compiler_flag("-Wno-c++98-compat-pedantic")
    add_cxx_compiler_flag("-Wno-padded")
    add_cxx_compiler_flag("-Wno-exit-time-destructors")
    add_cxx_compiler_flag("-Wno-global-constructors")
    add_cxx_compiler_flag("-Wno-documentation")
    add_cxx_compiler_flag("-Wno-documentation-unknown-command")
    add_cxx_compiler_flag("-Wno-undefined-func-template")
    add_cxx_compiler_flag("-Wno-c11-extensions")
    add_cxx_compiler_flag("-Wno-c++1z-extensions")
    add_cxx_compiler_flag("-Wno-used-but-marked-unused") # present in Boost test macro; too painful to excise properly the warnings...
    add_cxx_compiler_flag("-Wno-unsafe-buffer-usage") # see #1812
    add_cxx_compiler_flag("-Wno-switch-default") # no default in our switches on purpose...
    add_cxx_compiler_flag("-Wno-c++20-compat")
    
    
    if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
        add_definitions(-D_LIBCPP_DEBUG2=0) # Additional checks such as index out of bounds in vectors in libc++; might 
                                            # be removed at any point from libc++.
    endif()
    
    # Convenient macro to tag LLVM clang; useful as some warnings needs to be suppressed only for this more recent version of clang.
    if (${CMAKE_C_COMPILER_ID} STREQUAL "Clang")
        add_cxx_compiler_flag("-Wno-unused-template")
        add_definitions(-DMOREFEM_LLVM_CLANG)        
    endif()
    
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -stdlib=libc++ -lc++ -lc++abi")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++ ")
    
    # GOMP is required when sanitizer is used for gcc, but is not known is macOS hence this variable.
    set(MOREFEM_GOMP_DEPENDENCY "")
    
elseif(${CMAKE_C_COMPILER_ID} STREQUAL "GNU")    
    add_cxx_compiler_flag("-Wall")
    add_cxx_compiler_flag("-Wextra")
    #add_cxx_compiler_flag("-Wpedantic") Can't be activated because of third-party error; pragma doesn't work to disable it only for third party.
    add_cxx_compiler_flag("-Wcast-align")
    add_cxx_compiler_flag("-Wcast-qual")
    add_cxx_compiler_flag("-Wconversion")
    add_cxx_compiler_flag("-Wdisabled-optimization")
    add_cxx_compiler_flag("-Wfloat-equal")
    add_cxx_compiler_flag("-Wformat=2")
    add_cxx_compiler_flag("-Wformat-nonliteral")
    add_cxx_compiler_flag("-Wformat-security")
    add_cxx_compiler_flag("-Wformat-y2k")
    add_cxx_compiler_flag("-Wimport")
    add_cxx_compiler_flag("-Winit-self")
    # add_cxx_compiler_flag("-Winline") Tag destructor defined with = default as inline...
    add_cxx_compiler_flag("-Winvalid-pch")
    add_cxx_compiler_flag("-Wmissing-field-initializers")
    add_cxx_compiler_flag("-Wmissing-format-attribute")
    add_cxx_compiler_flag("-Wmissing-include-dirs")

#    add_cxx_compiler_flag("-Wmissing-noreturn") False positive...
    add_cxx_compiler_flag("-Wpacked")
    add_cxx_compiler_flag("-Wpointer-arith")
    add_cxx_compiler_flag("-Wredundant-decls")
    # add_cxx_compiler_flag("-Wshadow") g++ shadow is too cumbersome: can't name function argument same as a method for instance...
    add_cxx_compiler_flag("-Wstack-protector")
    add_cxx_compiler_flag("-Wstrict-aliasing=2")
    add_cxx_compiler_flag("-Wswitch-enum")
    add_cxx_compiler_flag("-Wunreachable-code")
    add_cxx_compiler_flag("-Wunused")
    add_cxx_compiler_flag("-Wunused-parameter")
    add_cxx_compiler_flag("-Wvariadic-macros")
    add_cxx_compiler_flag("-Wwrite-strings")
    add_cxx_compiler_flag("-Wno-psabi") # To disable pesky warnings in macOS on M1 Mac: see https://stackoverflow.com/questions/48149323/what-does-the-gcc-warning-project-parameter-passing-for-x-changed-in-gcc-7-1-m
    add_cxx_compiler_flag("-Wno-restrict") # To disable false positive warning with gcc 12 (see https://bugzilla.redhat.com/show_bug.cgi?id=2057597)

    # GOMP is required when sanitizer is used for gcc, but is not known is macOS hence this variable.
    set(MOREFEM_GOMP_DEPENDENCY "-lgomp")
    
else()
    message(FATAL_ERROR "Sorry, your compiler family wasn't recognized. If CMake has updated its flag, please modify the cmake/Compiler.cmake file in MoReFEM accoerdingly. See CMAKE_LANG_COMPILER_ID in CMake tutorial to find out the valid options.")
endif()


if(MOREFEM_ASAN)
    add_cxx_compiler_debug_flag("-fno-omit-frame-pointer")
    add_cxx_compiler_debug_flag("-fsanitize=address")
    set (CMAKE_LINKER_FLAGS_DEBUG "${CMAKE_LINKER_FLAGS_DEBUG} -fno-omit-frame-pointer -fsanitize=address")
endif()


set(MOREFEM_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/MoReFEM)

set(MOREFEM_INSTALL_DIR_EXE ${MOREFEM_INSTALL_DIR}/bin)
set(MOREFEM_INSTALL_DIR_LIB ${MOREFEM_INSTALL_DIR}/lib)
set(MOREFEM_INSTALL_DIR_INCL ${MOREFEM_INSTALL_DIR}/include)
set(MOREFEM_INSTALL_DIR_CMAKE ${MOREFEM_INSTALL_DIR}/cmake)
set(MOREFEM_INSTALL_DIR_SHARE ${MOREFEM_INSTALL_DIR}/share)

if (BUILD_MOREFEM_UNIQUE_LIBRARY)
    set(MOREFEM_MAIN_LIBS MoReFEM)
else()
    set(MOREFEM_MAIN_LIBS MoReFEM_utilities MoReFEM_core MoReFEM_geometry MoReFEM_felt MoReFEM_param MoReFEM_op
		MoReFEM_param_instances MoReFEM_op_instances MoReFEM_formulation_solver MoReFEM_model)
endif()

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)


# Option to color the outputs with ninja.
# Adapted from https://medium.com/@alasher/colored-c-compiler-output-with-ninja-clang-gcc-10bfe7f2b949
option (FORCE_COLORED_OUTPUT "Always produce ANSI-colored output (GNU/Clang only)." True)

if (${FORCE_COLORED_OUTPUT})
    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
       add_compile_options (-fdiagnostics-color=always)
    elseif (("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang") OR ("${CMAKE_C_COMPILER_ID}" STREQUAL "AppleClang"))
       add_compile_options (-fcolor-diagnostics)
    endif ()
endif ()

add_compile_options(-fdiagnostics-show-option)