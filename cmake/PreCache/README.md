Examples of pre-cache files that might be provided to cmake with the -C alias, e.g.:

````
cmake -G Ninja -C ../cmake/PreCache/mmacos_apple_clang.cmake
````

The idea is not to clutter the command line with too much arguments; arguably only one pre-cache file would have been enough but for instance switching the BLAS between macos (where usually framework Accelerate is used) and Linux (for which default is using Openblas) would be annoying.

The default paths inside reflects the default convention within the M3DISIM team, which are:

. On macOS, which is the work environment for most team members, providing the third-party libraries in /Users/Shared/LibraryVersions/*compiler tag*. This way, the code might be tested with several compilers on a developer'
s laptop.
. On Linux (typically for cluster use or continuous integration), only one flavor of the third party libraries is installed, so /opt/Library is used.

Obviously you may very easily change this behaviour by copying an existing file and modifying it to suit your needs! You may also not modify it and specify the options you want changed in command line, e.g:


````
cmake -G Ninja -C ../cmake/PreCache/macos_apple_clang.cmake -DMOREFEM_THIRD_PARTY_LIBRARIES_DIR /home/user/my_lib)
````