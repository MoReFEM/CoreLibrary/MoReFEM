set(CMAKE_INSTALL_PREFIX /placeholder_value CACHE PATH "Installation directory for executables and libraries. A MoReFEM folder will be created there when install is invoked. This value should be overridden in command line!")

set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose either 'Debug' or 'Release'")

set(MOREFEM_THIRD_PARTY_LIBRARIES_DEBUG_DIR /Volumes/Data/opt/clang_debug CACHE STRING "Path to third party directory compiled in debug mode - used only if CMAKE_BUILD_TYPE is 'Debug'")

set(MOREFEM_THIRD_PARTY_LIBRARIES_RELEASE_DIR /Volumes/Data/opt/clang_release CACHE STRING "Path to third party directory compiled in release mode - used only if CMAKE_BUILD_TYPE is 'Release'")

set(CMAKE_C_COMPILER ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/bin/mpicc CACHE FILEPATH "C compiler. Prefer to use an openmpi wrapper.")
set(CMAKE_CXX_COMPILER ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/bin/mpic++ CACHE FILEPATH "C++ compiler. Prefer to use an openmpi wrapper.")

set(OPTIMIZE_FOR_NATIVE False CACHE BOOL "If this option is set, compilation will optimize for your architecture (especially with '-march=native' flag)."


set(MOREFEM_ASAN False CACHE BOOL "If true address sanitizer is enabled.")

set(MOREFEM_WITH_SLEPC True CACHE BOOL "Choose true if you want to activate optional functionalities using Slepc library.")

set(MOREFEM_WITH_MUMPS True CACHE BOOL "Choose true if you want to activate Petsc optional Mumps dependency.")

set(MOREFEM_WITH_UMFPACK True CACHE BOOL "Choose true if you want to activate Petsc optional Umfpack dependency.")

set(MOREFEM_WITH_SUPERLU_DIST True CACHE BOOL "Choose true if you want to activate Petsc optional SuperLU_dist dependency.")

set(CMAKE_CXX_STANDARD 20 CACHE STRING "C++ standard; at least 20 is expected.")
set(CMAKE_CXX_STANDARD_REQUIRED ON CACHE STRING "Leave this one active.")
set(CMAKE_CXX_EXTENSIONS OFF CACHE STRING "If ON you might be using gnu++XX; with OFF you'll use c++XX (where XX is the C++ standard, e.g. 20).")

set(LIBRARY_TYPE STATIC CACHE BOOL "Choose either STATIC or SHARED.")

set(BUILD_MOREFEM_UNIQUE_LIBRARY True CACHE BOOL "Whether a unique library is built for MoReFEM core libraries or on the contrary if it is splitted in modules.")

set(MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE False CACHE BOOL "If true, add a (costly) method that gives an hint whether an UpdateGhost() call was relevant or not.")

set(MOREFEM_EXTENDED_TIME_KEEP False CACHE BOOL "If true, TimeKeep gains the ability to track times between each call of PrintTimeElapsed(). If not, PrintTimeElapsed() is flatly ignored. False is the best choice in production!")
    
set(MOREFEM_NO_TRAP_SNES_EXCEPTION False CACHE BOOL "If true, exceptions aren't caught in the three SNES functions I have to define for a Petsc Newton (at least the default ones; if you define your own it's up to you to introduce the macro in your code). If not caught, an eventual exception will be written properly but the exception is not guaranteed to be caught and it might result in a rather messy output. I therefore advise not to set it to True in debug mode; in release mode it is ok to do so as such exceptios are rare.")

set(BLAS_CUSTOM_LINKER True CACHE BOOL "If BLAS_CUSTOM_LINKER is true, BLAS_LIB field must give the command use to link with Blas. For instance on macOS it is usually \"-framework Accelerate\" (Beware: Without the quotes CMake will mute this into -framework -Accelerate). If False, FindLibrary is used to find the Blas library to be used, as for the other libraries in this file. The difference is that the name of the .a, .so or .dylib is not known, so it must be given in BLAS_LIB_NAME field. For instance openblas to find libopenblas.a in BLAS_LIB_DIR.")
set(BLAS_LIB "-framework Accelerate" CACHE STRING "Name of the Blas lib (e.g. openblas) or command to pass if custom linker is used; see BLAS_CUSTOM_LINKER." )
set(BLAS_LIB_DIR None CACHE STRING "None or path to the lib directory of Blas (see BLAS_CUSTOM_LINKER).")

set(OPEN_MPI_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/include CACHE PATH "Include directory of Openmpi library.")
set(OPEN_MPI_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/lib CACHE PATH "Lib directory of Openmpi library." )

set(PETSC_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/include CACHE PATH "Include directory of Petsc library.")
set(PETSC_INSTALL_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/config_morefem/include/ CACHE PATH "Include directory generated when Petsc library was installed, with configuration details here.")
set(PETSC_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/config_morefem/lib CACHE PATH "Library directory of Petsc.")

set(SLEPC_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Slepc/include CACHE PATH "Include directory of Slepc library.")
set(SLEPC_INSTALL_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Slepc/config_morefem/include/ CACHE PATH "Include directory generated when Petsc library was installed, with configuration details here.")
set(SLEPC_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Slepc/config_morefem/lib CACHE PATH "Library directory of Slepc.")

set(SCOTCH_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Scotch/include CACHE PATH "Include directory of Scotch library .")
set(SCOTCH_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Scotch/lib CACHE PATH "Lib directory of Scotch library (which also includes Metis/Parmetis stubs).")

set(LUA_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Lua/include CACHE PATH "Include directory of Lua library.")
set(LUA_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Lua/lib CACHE PATH "Lib directory of Lua library.")

set(BOOST_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Boost/include CACHE PATH "Include directory of Boost library.")
set(BOOST_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Boost/lib CACHE PATH "Lib directory of Boost library.")

set(EIGEN_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Eigen/include CACHE PATH "Include directory of Eigen library.")

set(XTENSOR_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Xtensor/include CACHE PATH "Include directory of Xtensor library.")

set(TCLAP_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Tclap/include CACHE PATH "Include directory of Tclap library.")

set(LIBMESH_INCL_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Libmeshb/include CACHE PATH "Include directory of Libmeshb library.")
set(LIBMESH_LIB_DIR ${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Libmeshb/lib CACHE PATH "Lib directory of Libmeshb library.")

set(PHILLIPS_DIR False CACHE BOOL "If you want to couple Morefem with Phillips library. False in most of the cases! Beware: it is not put in MOREFEM_COMMON_DEP; if you need it you must add it in your add_executable command.")


