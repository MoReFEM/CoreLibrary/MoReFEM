# #################################################################################################################
# TO UNDERSTAND THE TWO DIFFERENT TYPES OF TESTS, PLEASE READ THE COMMENT IN TestCustomCommands.cmake !
# #################################################################################################################


# #################################################################################################################
# Helper function which parses and interpret the arguments provided to the actual function (this one is not 
# expected to be used directly)
#
# - options
#    . BINARY: choose BINARY if you chose to run the model with binary outputs in its Lua input file. If this option
# is set, the output directories of models will be inside a "Binary" folder (if not, it will be an "Ascii" folder). 
# The name of the test will also add a suffix to indicate it is a binary. It is not unusual to run same model with
# and without this option (albeit with a different Lua file as it is there the kind of outputs is chosen).
#    . FROM_PREPARTITIONED_DATA: choose this option if you're running a model which partition was run beforehand.
# As for binary, the incidence of this choice is just in the test name and in the location of outputs; it is inside
# the Lua input file that you truly choose to run a model from prepartitioned data. This option may be combined with
# BINARY. It is not unusual to run same model with
# and without this option (albeit with a different Lua file as it is there the kind of outputs is chosen).
#    . RESTART: choose this option if you're running a model in restart mode, i.e. that you run from a state reachec
# in a previous run. RESTART can't be used with FROM_PREPARTITIONED_DATA option; it is implied that when restarting
# a parallel model prepartitioned data are available. As for BINARY and FROM_PREPARTITIONED_DATA, it is truly in the 
# Lua file that the choice is made; here the outcome is to change test name and where the outputs are written. This 
# option may be combined with BINARY. It is not unusual to run same model with
# and without this option (albeit with a different Lua file as it is there the kind of outputs is chosen).
#    . NO_ENSIGHT_OUTPUT: by default, when a model is run there is also another executable called that translate
# the output data into Ensight formats (using in this case an executable which name is derived from the EXE argument
# we will see shortly - if EXE is foo then the executable is fooEnsightOutput). If you do not want to call it (for 
# instance if your data are not compatible with Ensight 6 format) you may specify NO_ENSIGHT_OUTPUT.
#    . BOOST_TEST: I don't like it much, but some tests regarding operators are actually implemented as almost full
# fledged model instances and we nonetheless need to specify outputs should be in the main test output directory, 
# not the directory intended for model instances. In this case, specify this option.
#
# - oneValueArgs
#
#    . LUA Path to the Lua input file; may use environment variables.
#    . NAME Base name of the test. This name may be completed with some of the options: BINARY, RESTART 
# and FROM_PREPARTITIONED_DATA; likewise for instance tests that run post-processing to convert into Ensight 
# format will add a suffix.
#    . EXE Name of the executable considered (created with a `add_executable` command). As for NAME, somne other
# executable names may be derived from this one if they follow the adequate convention (e.g. for the aforementioned
# post processing into Ensight convention is to add suffix "EnsightOutput").
#    . TIMEOUT Time out of the test, in seconds, to avoid dangling program.
# 
#
# #################################################################################################################


function(morefem_test_run_model_helper)
    set(options BINARY RESTART FROM_PREPARTITIONED_DATA NO_ENSIGHT_OUTPUT BOOST_TEST)
    set(oneValueArgs LUA NAME EXE TIMEOUT)
    set(multiValueArgs "")
    cmake_parse_arguments(MY_COMMAND "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if (MY_COMMAND_BINARY)
        set(ascii_or_binary "Binary")
        set(binary_suffix "-binary")
    else()
        set(ascii_or_binary "Ascii")
        set(binary_suffix "")
    endif()

    if (MY_COMMAND_RESTART AND MY_COMMAND_FROM_PREPARTITIONED_DATA)
        message(FATAL_ERROR "RESTART and FROM_PREPARTITIONED_DATA can't be mentioned in the same command; you probably intend to use RESTART (which uses up prepartitioned data)." )
    endif()

    if (MY_COMMAND_RESTART)
        set(complete_result_seq_or_par "_Restart")
        set(restart_or_prepartitioned_data_suffix "-restart")
        set(is_restart_mode True)
    elseif (MY_COMMAND_FROM_PREPARTITIONED_DATA)
        set(complete_result_seq_or_par "_FromPrepartitionedData")
        set(restart_or_prepartitioned_data_suffix "-from-prepartitioned-data")
        set(is_restart_mode False)
    else()
        set(complete_result_seq_or_par "")
        set(restart_or_prepartitioned_data_suffix "")
        set(is_restart_mode False)
    endif()

    if (MY_COMMAND_NO_ENSIGHT_OUTPUT)
        set(ensight_output False)
    else()
        set(ensight_output True)
    endif()

    if (NOT MY_COMMAND_TIMEOUT)
        message(FATAL_ERROR "Missing argument TIMEOUT!")
    endif()

    if (MY_COMMAND_BOOST_TEST)
        set(is_boost_test True)
    endif()

    set(ascii_or_binary ${ascii_or_binary} PARENT_SCOPE)
    set(complete_result_seq_or_par ${complete_result_seq_or_par} PARENT_SCOPE)
    set(test_name ${MY_COMMAND_NAME}${binary_suffix}${restart_or_prepartitioned_data_suffix} PARENT_SCOPE)
    set(timeout ${MY_COMMAND_TIMEOUT} PARENT_SCOPE)
    set(lua_file ${MY_COMMAND_LUA} PARENT_SCOPE)
    set(executable ${MY_COMMAND_EXE} PARENT_SCOPE)
    set(ensight_output ${ensight_output} PARENT_SCOPE)
    set(is_boost_test ${is_boost_test} PARENT_SCOPE)
    set(is_restart_mode ${is_restart_mode} PARENT_SCOPE)

endfunction()



# #################################################################################################################
# This function adds tests regarding ONLY sequential mode:
# - One test which runs the executable over the given LUA file.
# - If NO_ENSIGHT_OUTPUT is NOT defined, another which runs the post processing to create Ensight outputs
# from the first test.
#
# Please have a look at morefem_test_run_model_helper() function above to see the expected arguments.
#
# #################################################################################################################

function(morefem_test_run_model_in_sequential_mode)
    morefem_test_run_model_helper(${ARGN})

    if (NOT is_boost_test)
        set(result_directory ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}/Seq${complete_result_seq_or_par}/${ascii_or_binary})
    else()
        set(result_directory ${MOREFEM_TEST_OUTPUT_DIR}/Seq${complete_result_seq_or_par}/${ascii_or_binary})    
    endif()

    set(prepartitioned_directory ${MOREFEM_PREPARTITIONED_DATA_DIR}/Seq/${ascii_or_binary})

    if (is_restart_mode)
        set(restart_from_directory ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}/Seq/${ascii_or_binary}) 
    else()
        set(restart_from_directory "")
    endif()
    
    # ###########################################################################
    # Here we want to provide values for the XCode scheme for the first 
    # time executable is encountered.
    get_property(generated_xcode_scheme GLOBAL PROPERTY generated_xcode_scheme)

    if (NOT ${executable} IN_LIST generated_xcode_scheme)
        set_property(TARGET ${executable} PROPERTY XCODE_SCHEME_ARGUMENTS 
                    "-i ${lua_file}"
                    "--overwrite_directory")

        set_property(TARGET ${executable} PROPERTY XCODE_SCHEME_ENVIRONMENT 
                    "MOREFEM_RESULT_DIR=${result_directory}"
                    "MOREFEM_ROOT=${MOREFEM_ROOT}" 
                    "EXTERNAL_MODEL_ROOT=${EXTERNAL_MODEL_ROOT}"
                    "MOREFEM_PREPARTITIONED_DATA_DIR=${prepartitioned_directory}")
    else()
        get_property(arguments TARGET ${executable} PROPERTY XCODE_SCHEME_ARGUMENTS)
        set_property(TARGET ${executable} PROPERTY XCODE_SCHEME_ARGUMENTS 
                    "${arguments})" "-i ${lua_file}")
    endif()

    # ###################################

    add_test(${test_name}
            ${executable}
            --overwrite_directory             
            -e MOREFEM_ROOT=${MOREFEM_ROOT}             
            -e EXTERNAL_MODEL_ROOT=${EXTERNAL_MODEL_ROOT}
            -i ${lua_file}
            -e MOREFEM_RESULT_DIR=${result_directory}
            -e MOREFEM_RESTART_FROM_DIR=${restart_from_directory}
            -e MOREFEM_PREPARTITIONED_DATA_DIR=${prepartitioned_directory})

    set_tests_properties(${test_name} PROPERTIES TIMEOUT ${timeout})

    if (ensight_output)
        add_test(${test_name}-EnsightOutput
                ${executable}EnsightOutput
                -e MOREFEM_ROOT=${MOREFEM_ROOT}          
                -e EXTERNAL_MODEL_ROOT=${EXTERNAL_MODEL_ROOT}   
                -i ${lua_file}
                -e MOREFEM_RESULT_DIR=${result_directory}
                -e MOREFEM_RESTART_FROM_DIR=${restart_from_directory}
                -e MOREFEM_PREPARTITIONED_DATA_DIR=${prepartitioned_directory})

        set_tests_properties(${test_name}-EnsightOutput PROPERTIES TIMEOUT ${timeout})                 

        if (NOT ${executable} IN_LIST generated_xcode_scheme)
            set_property(TARGET ${executable}EnsightOutput PROPERTY XCODE_SCHEME_ARGUMENTS
                        "-i ${lua_file} ")

            set_property(TARGET ${executable}EnsightOutput PROPERTY XCODE_SCHEME_ENVIRONMENT 
                        "MOREFEM_RESULT_DIR=${result_directory}"
                        "MOREFEM_ROOT=${MOREFEM_ROOT}" 
                        "EXTERNAL_MODEL_ROOT=${EXTERNAL_MODEL_ROOT}"
                        "MOREFEM_PREPARTITIONED_DATA_DIR=${prepartitioned_directory}")
        else()
            get_property(arguments TARGET ${executable} PROPERTY XCODE_SCHEME_ARGUMENTS)
            set_property(TARGET ${executable} PROPERTY XCODE_SCHEME_ARGUMENTS 
                        "${arguments})" "-i ${lua_file}")
        endif()

    endif()

    # These commands ensure the values won't be overwritten by a subsequent call upon
    # same executable.
    list(APPEND generated_xcode_scheme ${executable})
    list(REMOVE_DUPLICATES generated_xcode_scheme)
    set_property(GLOBAL PROPERTY generated_xcode_scheme ${generated_xcode_scheme})
            
endfunction()


# #################################################################################################################
# This function adds tests regarding ONLY parallel mode:
# - One test which runs the executable over the given LUA file in parallel mode over 4 ranks.
# - If NO_ENSIGHT_OUTPUT is NOT defined, another which runs the post processing to create Ensight outputs
# from the first test.
#
# Please have a look at morefem_test_run_model_helper() function above to see the expected arguments.
#
# #################################################################################################################
function(morefem_test_run_model_in_parallel_mode)
    morefem_test_run_model_helper(${ARGN})

    if (NOT is_boost_test)
        set(result_directory ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}/Mpi4${complete_result_seq_or_par}/${ascii_or_binary})
    else()
        set(result_directory ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4${complete_result_seq_or_par}/${ascii_or_binary})
    endif()

    set(prepartitioned_directory ${MOREFEM_PREPARTITIONED_DATA_DIR}/Mpi4/${ascii_or_binary})

    if (is_restart_mode)
        set(restart_from_directory ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}/Mpi4/${ascii_or_binary}) 
    else()
        set(restart_from_directory "")
    endif()

    set(test_name ${test_name}-mpi)

    add_test(${test_name}
            ${OPEN_MPI_INCL_DIR}/../bin/mpirun
            --oversubscribe
            -np 4
            ${executable}
            --overwrite_directory             
            -e MOREFEM_ROOT=${MOREFEM_ROOT}         
            -e EXTERNAL_MODEL_ROOT=${EXTERNAL_MODEL_ROOT}    
            -i ${lua_file}
            -e MOREFEM_RESULT_DIR=${result_directory}
            -e MOREFEM_RESTART_FROM_DIR=${restart_from_directory}
            -e MOREFEM_PREPARTITIONED_DATA_DIR=${prepartitioned_directory})

    set_tests_properties(${test_name} PROPERTIES TIMEOUT ${timeout})

    if (ensight_output)
        add_test(${test_name}-EnsightOutput
                ${executable}EnsightOutput
                -e MOREFEM_ROOT=${MOREFEM_ROOT}          
                -e EXTERNAL_MODEL_ROOT=${EXTERNAL_MODEL_ROOT}   
                -i ${lua_file}
                -e MOREFEM_RESULT_DIR=${result_directory}
                -e MOREFEM_RESTART_FROM_DIR=${restart_from_directory}
                -e MOREFEM_PREPARTITIONED_DATA_DIR=${prepartitioned_directory})
        set_tests_properties(${test_name}-EnsightOutput PROPERTIES TIMEOUT ${timeout})                 
    endif()

endfunction()


# #################################################################################################################
# This function adds tests for both sequential and parallel mode (with 4 ranks for the latter).
# 
# If NO_ENSIGHT_OUTPUT is NOT defined, two other tests run post-processing over the resulting data to create 
# outputs in Ensight6 format.
#
# Please have a look at morefem_test_run_model_helper() function above to see the expected arguments.
#
# #################################################################################################################
function(morefem_test_run_model_in_both_modes)
    morefem_test_run_model_in_sequential_mode(${ARGN})
    morefem_test_run_model_in_parallel_mode(${ARGN})
endfunction()

